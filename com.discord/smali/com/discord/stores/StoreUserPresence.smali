.class public final Lcom/discord/stores/StoreUserPresence;
.super Lcom/discord/stores/StoreV2;
.source "StoreUserPresence.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreUserPresence$TimestampedPresence;,
        Lcom/discord/stores/StoreUserPresence$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreUserPresence$Companion;

.field private static final LocalPresenceUpdateSource:Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private localPresence:Lcom/discord/models/domain/ModelPresence;

.field private localPresenceSnapshot:Lcom/discord/models/domain/ModelPresence;

.field private meUser:Lcom/discord/models/domain/ModelUser;

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private final presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation
.end field

.field private presencesSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation
.end field

.field private final stream:Lcom/discord/stores/StoreStream;

.field private final userGuildPresences:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreUserPresence$TimestampedPresence;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreUserPresence$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreUserPresence$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreUserPresence;->Companion:Lcom/discord/stores/StoreUserPresence$Companion;

    new-instance v0, Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreUserPresence;->LocalPresenceUpdateSource:Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;

    return-void
.end method

.method public constructor <init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 10

    const-string v0, "clock"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "stream"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p2, p0, Lcom/discord/stores/StoreUserPresence;->stream:Lcom/discord/stores/StoreStream;

    iput-object p3, p0, Lcom/discord/stores/StoreUserPresence;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    new-instance p1, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    const/4 p2, 0x0

    const/4 p3, 0x1

    const/4 v0, 0x0

    invoke-direct {p1, p2, p3, v0}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->userGuildPresences:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->presencesSnapshot:Ljava/util/Map;

    new-instance p1, Lcom/discord/models/domain/ModelPresence;

    sget-object v1, Lcom/discord/models/domain/ModelPresence$Status;->ONLINE:Lcom/discord/models/domain/ModelPresence$Status;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/16 v8, 0x3c

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    const/4 v1, 0x0

    const/16 v8, 0x3f

    invoke-static/range {v0 .. v9}, Lcom/discord/models/domain/ModelPresence;->copy$default(Lcom/discord/models/domain/ModelPresence;Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILjava/lang/Object;)Lcom/discord/models/domain/ModelPresence;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->localPresenceSnapshot:Lcom/discord/models/domain/ModelPresence;

    return-void
.end method

.method public static final synthetic access$getLocalPresenceSnapshot$p(Lcom/discord/stores/StoreUserPresence;)Lcom/discord/models/domain/ModelPresence;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreUserPresence;->localPresenceSnapshot:Lcom/discord/models/domain/ModelPresence;

    return-object p0
.end method

.method public static final synthetic access$getPresencesSnapshot$p(Lcom/discord/stores/StoreUserPresence;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreUserPresence;->presencesSnapshot:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$setLocalPresenceSnapshot$p(Lcom/discord/stores/StoreUserPresence;Lcom/discord/models/domain/ModelPresence;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->localPresenceSnapshot:Lcom/discord/models/domain/ModelPresence;

    return-void
.end method

.method public static final synthetic access$setPresencesSnapshot$p(Lcom/discord/stores/StoreUserPresence;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->presencesSnapshot:Ljava/util/Map;

    return-void
.end method

.method private final clearPresences(J)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->userGuildPresences:Ljava/util/HashMap;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map$Entry;

    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreUserPresence;->flattenPresence(J)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method private final flattenPresence(J)V
    .locals 13
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->userGuildPresences:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v2, v1

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    move-object v3, v2

    check-cast v3, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;

    invoke-virtual {v3}, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;->getTimestamp()J

    move-result-wide v3

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;

    invoke-virtual {v6}, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;->getTimestamp()J

    move-result-wide v6

    cmp-long v8, v3, v6

    if-gez v8, :cond_3

    move-object v2, v5

    move-wide v3, v6

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    :goto_0
    check-cast v2, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v2

    if-eqz v2, :cond_5

    goto :goto_2

    :cond_5
    sget-object v2, Lcom/discord/models/domain/ModelPresence$Status;->OFFLINE:Lcom/discord/models/domain/ModelPresence$Status;

    :goto_2
    move-object v4, v2

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_6

    sget-object v3, Lcom/discord/utilities/presence/PresenceUtils;->INSTANCE:Lcom/discord/utilities/presence/PresenceUtils;

    invoke-virtual {v3}, Lcom/discord/utilities/presence/PresenceUtils;->getACTIVITY_COMPARATOR$app_productionDiscordExternalRelease()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v2, v3}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->reversed(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    move-object v5, v2

    goto :goto_3

    :cond_6
    move-object v5, v1

    :goto_3
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getClientStatuses()Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    move-result-object v0

    goto :goto_4

    :cond_7
    move-object v0, v1

    :goto_4
    sget-object v2, Lcom/discord/models/domain/ModelPresence$Status;->OFFLINE:Lcom/discord/models/domain/ModelPresence$Status;

    if-ne v4, v2, :cond_8

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->userGuildPresences:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_c

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    goto :goto_6

    :cond_8
    iget-object v2, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelPresence;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->getStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v1

    :cond_9
    if-ne v1, v4, :cond_a

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v1

    invoke-static {v1, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_a

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->getClientStatuses()Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    move-result-object v1

    invoke-static {v1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_c

    :cond_a
    iget-object v1, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/models/domain/ModelPresence;

    if-eqz v0, :cond_b

    goto :goto_5

    :cond_b
    sget-object v0, Lcom/discord/models/domain/ModelPresence$ClientStatuses;->Companion:Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence$ClientStatuses$Companion;->empty()Lcom/discord/models/domain/ModelPresence$ClientStatuses;

    move-result-object v0

    :goto_5
    move-object v6, v0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/16 v11, 0x38

    const/4 v12, 0x0

    move-object v3, p2

    invoke-direct/range {v3 .. v12}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_c
    :goto_6
    return-void
.end method

.method private final getCustomStatusActivityFromSetting(Lcom/discord/models/domain/ModelCustomStatusSetting;)Lcom/discord/models/domain/activity/ModelActivity;
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/models/domain/ModelCustomStatusSetting;->Companion:Lcom/discord/models/domain/ModelCustomStatusSetting$Companion;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelCustomStatusSetting$Companion;->getCLEAR()Lcom/discord/models/domain/ModelCustomStatusSetting;

    move-result-object v0

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getExpiresAt()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getExpiresAt()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/time/TimeUtils;->parseUTCDate(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getEmojiId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getEmojis$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreEmoji;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getEmojiId()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/discord/stores/StoreEmoji;->getCustomEmoji(J)Lcom/discord/models/domain/emoji/ModelEmojiCustom;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v1, Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    invoke-virtual {v0}, Lcom/discord/models/domain/emoji/ModelEmojiCustom;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/models/domain/emoji/ModelEmojiCustom;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/models/domain/emoji/ModelEmojiCustom;->isAnimated()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getEmojiName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getEmojis$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreEmoji;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreEmoji;->getUnicodeEmojiSurrogateMap()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getEmojiName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/emoji/ModelEmojiUnicode;

    if-eqz v0, :cond_3

    new-instance v2, Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    invoke-virtual {v0}, Lcom/discord/models/domain/emoji/ModelEmojiUnicode;->getSurrogates()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v1, v2

    :cond_3
    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/discord/models/domain/activity/ModelActivity;->createForCustom(Ljava/lang/String;Lcom/discord/models/domain/ModelMessageReaction$Emoji;)Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p1

    return-object p1
.end method

.method private final removeActivityInList(ILjava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-static {p2}, Lx/h/f;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    const/4 v1, -0x1

    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {v4}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v4

    if-ne v4, p1, :cond_1

    const/4 v4, 0x1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_2

    goto :goto_3

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, -0x1

    :goto_3
    if-eq v3, v1, :cond_4

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_4
    return-object v0
.end method

.method private final replaceActivityInList(Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/discord/stores/StoreUserPresence;->removeActivityInList(ILjava/util/List;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p2
.end method

.method public static synthetic updateActivity$default(Lcom/discord/stores/StoreUserPresence;ILcom/discord/models/domain/activity/ModelActivity;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/StoreUserPresence;->updateActivity(ILcom/discord/models/domain/activity/ModelActivity;Z)V

    return-void
.end method

.method private final updateSelfPresence(Lcom/discord/models/domain/ModelUserSettings;Ljava/util/List;Z)V
    .locals 16
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUserSettings;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelSession;",
            ">;Z)V"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/stores/StoreUserPresence;->meUser:Lcom/discord/models/domain/ModelUser;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserSettings;->getPresenceStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPresence;->getStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v3

    :goto_0
    const-string/jumbo v4, "userSettings?.presenceSt\u2026  ?: localPresence.status"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    if-eqz p2, :cond_3

    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/discord/models/domain/ModelSession;

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelSession;->isActive()Z

    move-result v7

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_2
    move-object v6, v4

    :goto_1
    check-cast v6, Lcom/discord/models/domain/ModelSession;

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelSession;->getActivities()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_3

    move-object v1, v5

    goto :goto_2

    :cond_3
    iget-object v5, v0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelPresence;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v1

    goto :goto_2

    :cond_4
    move-object v1, v4

    :goto_2
    if-eqz p1, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserSettings;->getCustomStatus()Lcom/discord/models/domain/ModelCustomStatusSetting;

    move-result-object v4

    :cond_5
    if-eqz v4, :cond_7

    invoke-direct {v0, v4}, Lcom/discord/stores/StoreUserPresence;->getCustomStatusActivityFromSetting(Lcom/discord/models/domain/ModelCustomStatusSetting;)Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v4, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/discord/stores/StoreUserPresence;->replaceActivityInList(Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    goto :goto_3

    :cond_6
    const/4 v2, 0x4

    iget-object v4, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/discord/stores/StoreUserPresence;->removeActivityInList(ILjava/util/List;)Ljava/util/List;

    move-result-object v2

    goto :goto_3

    :cond_7
    iget-object v2, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v2

    :goto_3
    move-object v6, v2

    iget-object v2, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->getStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v2

    const/4 v14, 0x1

    if-ne v3, v2, :cond_8

    iget-object v2, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v2

    invoke-static {v6, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v14

    if-eqz v2, :cond_9

    :cond_8
    new-instance v2, Lcom/discord/models/domain/ModelPresence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/16 v12, 0x3c

    const/4 v13, 0x0

    move-object v4, v2

    move-object v5, v3

    invoke-direct/range {v4 .. v13}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    new-array v2, v14, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v4, 0x0

    sget-object v5, Lcom/discord/stores/StoreUserPresence;->LocalPresenceUpdateSource:Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;

    aput-object v5, v2, v4

    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    :cond_9
    const-wide v14, 0x7fffffffffffffffL

    new-instance v2, Lcom/discord/models/domain/ModelPresence;

    const/4 v7, 0x0

    iget-object v8, v0, Lcom/discord/stores/StoreUserPresence;->meUser:Lcom/discord/models/domain/ModelUser;

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/16 v12, 0x34

    const/4 v13, 0x0

    move-object v4, v2

    move-object v5, v3

    move-object v6, v1

    invoke-direct/range {v4 .. v13}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v0, v14, v15, v2}, Lcom/discord/stores/StoreUserPresence;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    if-nez p3, :cond_a

    iget-object v1, v0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelPresence;->getCustomStatusActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v1

    if-nez v1, :cond_a

    sget-object v1, Lcom/discord/stores/StoreUserPresence;->LocalPresenceUpdateSource:Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreV2;->markUnchanged(Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    :cond_a
    return-void
.end method


# virtual methods
.method public final getLocalPresence$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelPresence;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    return-object v0
.end method

.method public final getObservationDeck()Lcom/discord/stores/updates/ObservationDeck;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    return-object v0
.end method

.method public final getPresences()Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    return-object v0
.end method

.method public final getStream()Lcom/discord/stores/StoreStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->stream:Lcom/discord/stores/StoreStream;

    return-object v0
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->userGuildPresences:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-virtual {v0}, Lcom/discord/utilities/collections/ShallowPartitionMap$CopiablePartitionMap;->clear()V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreUserPresence;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuilds()Ljava/util/List;

    move-result-object v0

    const-string v1, "payload.guilds"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p0, v1}, Lcom/discord/stores/StoreUserPresence;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getPresences()Ljava/util/List;

    move-result-object v0

    const-string v1, "payload.presences"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelPresence;

    const-wide v2, 0x7fffffffffffffffL

    const-string v4, "presence"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3, v1}, Lcom/discord/stores/StoreUserPresence;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getUserSettings()Lcom/discord/models/domain/ModelUserSettings;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getSessions()Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/discord/stores/StoreUserPresence;->updateSelfPresence(Lcom/discord/models/domain/ModelUserSettings;Ljava/util/List;Z)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPresences()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    const-string v4, "presence"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3, v1}, Lcom/discord/stores/StoreUserPresence;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final handleGuildMemberRemove(Lcom/discord/models/domain/ModelGuildMember;)V
    .locals 13
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "member"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMember;->getGuildId()J

    move-result-wide v0

    new-instance v12, Lcom/discord/models/domain/ModelPresence;

    sget-object v3, Lcom/discord/models/domain/ModelPresence$Status;->OFFLINE:Lcom/discord/models/domain/ModelPresence$Status;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMember;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/16 v10, 0x34

    const/4 v11, 0x0

    move-object v2, v12

    invoke-direct/range {v2 .. v11}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, v0, v1, v12}, Lcom/discord/stores/StoreUserPresence;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    return-void
.end method

.method public final handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreUserPresence;->clearPresences(J)V

    return-void
.end method

.method public final handlePresenceReplace(Ljava/util/List;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;)V"
        }
    .end annotation

    const-string v0, "presencesList"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide v0, 0x7fffffffffffffffL

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreUserPresence;->clearPresences(J)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p0, v0, v1, v2}, Lcom/discord/stores/StoreUserPresence;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "presence"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelPresence;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v6, p1, v2

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_0
    move-wide p1, v4

    :goto_0
    iget-object v2, p0, Lcom/discord/stores/StoreUserPresence;->meUser:Lcom/discord/models/domain/ModelUser;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    cmp-long v6, v2, v0

    if-nez v6, :cond_1

    cmp-long v2, p1, v4

    if-eqz v2, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/discord/stores/StoreUserPresence;->userGuildPresences:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    check-cast v4, Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;

    iget-object v2, p0, Lcom/discord/stores/StoreUserPresence;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p2, p3, v2, v3}, Lcom/discord/stores/StoreUserPresence$TimestampedPresence;-><init>(Lcom/discord/models/domain/ModelPresence;J)V

    invoke-interface {v4, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreUserPresence;->flattenPresence(J)V

    :cond_3
    return-void
.end method

.method public final handleSessionsReplace(Ljava/util/List;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelSession;",
            ">;)V"
        }
    .end annotation

    const-string v0, "sessions"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/discord/stores/StoreUserPresence;->updateSelfPresence(Lcom/discord/models/domain/ModelUserSettings;Ljava/util/List;Z)V

    return-void
.end method

.method public final handleUserSettingsUpdate(Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "userSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/stores/StoreUserPresence;->updateSelfPresence(Lcom/discord/models/domain/ModelUserSettings;Ljava/util/List;Z)V

    return-void
.end method

.method public final observeAllPresences()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreUserPresence$observeAllPresences$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreUserPresence$observeAllPresences$1;-><init>(Lcom/discord/stores/StoreUserPresence;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeApplicationActivity(JJ)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreUserPresence;->observePresenceForUser(J)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreUserPresence$observeApplicationActivity$1;

    invoke-direct {p2, p3, p4}, Lcom/discord/stores/StoreUserPresence$observeApplicationActivity$1;-><init>(J)V

    invoke-virtual {p1, p2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observePresenceForUser(u\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeLocalPresence()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object v2, Lcom/discord/stores/StoreUserPresence;->LocalPresenceUpdateSource:Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v5, Lcom/discord/stores/StoreUserPresence$observeLocalPresence$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreUserPresence$observeLocalPresence$1;-><init>(Lcom/discord/stores/StoreUserPresence;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "observationDeck\n      .c\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observePresenceForUser(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserPresence;->observeAllPresences()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreUserPresence$observePresenceForUser$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreUserPresence$observePresenceForUser$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observeAllPresences()\n  \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observePresencesForUsers(Ljava/util/Collection;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "userIds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserPresence;->observeAllPresences()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreUserPresence$observePresencesForUsers$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreUserPresence$observePresencesForUsers$1;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "observeAllPresences()\n  \u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public snapshotData()V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->fastCopy()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreUserPresence;->presencesSnapshot:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->getUpdateSources()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreUserPresence;->LocalPresenceUpdateSource:Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGatewaySocket()Lcom/discord/stores/StoreGatewayConnection;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/stores/StoreGatewayConnection;->presenceUpdate$default(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;ILjava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final updateActivity(ILcom/discord/models/domain/activity/ModelActivity;Z)V
    .locals 12
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p3, :cond_4

    iget-object p3, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object p3

    if-eqz p3, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {v4}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v4

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivity;->getType()I

    move-result v5

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :cond_3
    invoke-static {p2, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    xor-int/2addr p3, v1

    if-eqz p3, :cond_6

    :cond_4
    if-eqz p2, :cond_5

    iget-object p1, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/discord/stores/StoreUserPresence;->replaceActivityInList(Lcom/discord/models/domain/activity/ModelActivity;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    goto :goto_2

    :cond_5
    iget-object p2, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPresence;->getActivities()Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreUserPresence;->removeActivityInList(ILjava/util/List;)Ljava/util/List;

    move-result-object p1

    :goto_2
    move-object v4, p1

    new-instance p1, Lcom/discord/models/domain/ModelPresence;

    iget-object p2, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelPresence;->getStatus()Lcom/discord/models/domain/ModelPresence$Status;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/16 v10, 0x3c

    const/4 v11, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v11}, Lcom/discord/models/domain/ModelPresence;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/util/List;Lcom/discord/models/domain/ModelPresence$ClientStatuses;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    new-array p1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object p2, Lcom/discord/stores/StoreUserPresence;->LocalPresenceUpdateSource:Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;

    aput-object p2, p1, v0

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    iget-object p1, p0, Lcom/discord/stores/StoreUserPresence;->meUser:Lcom/discord/models/domain/ModelUser;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide p1

    iget-object p3, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/models/domain/ModelPresence;

    iget-object v0, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-static {p3, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    xor-int/2addr p3, v1

    if-eqz p3, :cond_6

    iget-object p3, p0, Lcom/discord/stores/StoreUserPresence;->presences:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/stores/StoreUserPresence;->localPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_6
    return-void
.end method
