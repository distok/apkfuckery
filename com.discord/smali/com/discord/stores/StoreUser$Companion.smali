.class public final Lcom/discord/stores/StoreUser$Companion;
.super Ljava/lang/Object;
.source "StoreUser.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreUser$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getMeUpdate()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreUser;->access$getMeUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    move-result-object v0

    return-object v0
.end method

.method public final getUsersUpdate()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreUser;->access$getUsersUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    move-result-object v0

    return-object v0
.end method
