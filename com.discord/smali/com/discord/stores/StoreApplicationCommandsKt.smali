.class public final Lcom/discord/stores/StoreApplicationCommandsKt;
.super Ljava/lang/Object;
.source "StoreApplicationCommands.kt"


# direct methods
.method public static final getDescriptionText(Lcom/discord/stores/ModelApplicationCommand;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getDescriptionText"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommand;->getDescriptionRes()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommand;->getDescription()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public static final getDescriptionText(Lcom/discord/stores/ModelApplicationCommandOption;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getDescriptionText"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommandOption;->getDescriptionRes()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/ModelApplicationCommandOption;->getDescription()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public static final toSlashCommand(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;)Lcom/discord/stores/ModelApplicationCommand;
    .locals 14

    const-string v0, "$this$toSlashCommand"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->getId()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->getApplicationId()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;->getOptionGateways()Ljava/util/List;

    move-result-object p0

    new-instance v9, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p0, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationCommandsKt;->toSlashCommandOption(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;)Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xd0

    const/4 v13, 0x0

    new-instance p0, Lcom/discord/stores/ModelApplicationCommand;

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v13}, Lcom/discord/stores/ModelApplicationCommand;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;ZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p0
.end method

.method public static final toSlashCommandOption(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;)Lcom/discord/stores/ModelApplicationCommandOption;
    .locals 12

    const-string v0, "$this$toSlashCommandOption"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->getType()Lcom/discord/models/slashcommands/ApplicationCommandType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->getRequired()Z

    move-result v6

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->getDefault()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->getChoices()Ljava/util/List;

    move-result-object v8

    invoke-virtual {p0}, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;->getOptionGateways()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;

    invoke-static {v1}, Lcom/discord/stores/StoreApplicationCommandsKt;->toSlashCommandOption(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommandOption;)Lcom/discord/stores/ModelApplicationCommandOption;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v9, v0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    move-object v9, p0

    :goto_1
    const/16 v10, 0x8

    const/4 v11, 0x0

    new-instance p0, Lcom/discord/stores/ModelApplicationCommandOption;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v11}, Lcom/discord/stores/ModelApplicationCommandOption;-><init>(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p0
.end method
