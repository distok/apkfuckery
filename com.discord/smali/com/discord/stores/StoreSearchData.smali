.class public final Lcom/discord/stores/StoreSearchData;
.super Ljava/lang/Object;
.source "StoreSearchData.kt"


# instance fields
.field private final searchDataSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Lcom/discord/utilities/search/validation/SearchData;",
            "Lcom/discord/utilities/search/validation/SearchData;",
            ">;"
        }
    .end annotation
.end field

.field private subscription:Lrx/Subscription;


# direct methods
.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v8, Lcom/discord/utilities/search/validation/SearchData;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/utilities/search/validation/SearchData;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v8}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "BehaviorSubject.create(SearchData())"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/stores/StoreSearchData;->searchDataSubject:Lrx/subjects/Subject;

    return-void
.end method

.method public static final synthetic access$handleNewData(Lcom/discord/stores/StoreSearchData;Lcom/discord/utilities/search/validation/SearchData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreSearchData;->handleNewData(Lcom/discord/utilities/search/validation/SearchData;)V

    return-void
.end method

.method public static final synthetic access$handleSubscription(Lcom/discord/stores/StoreSearchData;Lrx/Subscription;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreSearchData;->handleSubscription(Lrx/Subscription;)V

    return-void
.end method

.method private final getChannelSearchData(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/search/validation/SearchData;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object p1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/discord/stores/StoreSearchData$getChannelSearchData$1;

    new-instance v1, Lcom/discord/utilities/search/validation/SearchData$Builder;

    invoke-direct {v1}, Lcom/discord/utilities/search/validation/SearchData$Builder;-><init>()V

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreSearchData$getChannelSearchData$1;-><init>(Lcom/discord/utilities/search/validation/SearchData$Builder;)V

    new-instance v1, Lcom/discord/stores/StoreSearchData$sam$rx_functions_Func2$0;

    invoke-direct {v1, v0}, Lcom/discord/stores/StoreSearchData$sam$rx_functions_Func2$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    invoke-static {p1, p2, v1}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026()::buildForChannel\n    )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getGuildSearchData(J)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/search/validation/SearchData;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeAllUsers()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-wide v5, p1

    invoke-static/range {v4 .. v9}, Lcom/discord/stores/StoreChannels;->observeChannelsForGuild$default(Lcom/discord/stores/StoreChannels;JLjava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    sget-object v4, Lcom/discord/stores/StoreSearchData$getGuildSearchData$1;->INSTANCE:Lcom/discord/stores/StoreSearchData$getGuildSearchData$1;

    invoke-virtual {v1, v4}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StorePermissions;->observeChannelPermissionsForGuild(J)Lrx/Observable;

    move-result-object v5

    new-instance p1, Lcom/discord/stores/StoreSearchData$getGuildSearchData$2;

    new-instance p2, Lcom/discord/utilities/search/validation/SearchData$Builder;

    invoke-direct {p2}, Lcom/discord/utilities/search/validation/SearchData$Builder;-><init>()V

    invoke-direct {p1, p2}, Lcom/discord/stores/StoreSearchData$getGuildSearchData$2;-><init>(Lcom/discord/utilities/search/validation/SearchData$Builder;)V

    new-instance v6, Lcom/discord/stores/StoreSearchData$sam$rx_functions_Func4$0;

    invoke-direct {v6, p1}, Lcom/discord/stores/StoreSearchData$sam$rx_functions_Func4$0;-><init>(Lkotlin/jvm/functions/Function4;)V

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v7, 0x3

    invoke-static/range {v2 .. v9}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    const-string p2, "ObservableWithLeadingEdg\u20263, TimeUnit.SECONDS\n    )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final handleNewData(Lcom/discord/utilities/search/validation/SearchData;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreSearchData;->searchDataSubject:Lrx/subjects/Subject;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final declared-synchronized handleSubscription(Lrx/Subscription;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreSearchData;->subscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iput-object p1, p0, Lcom/discord/stores/StoreSearchData;->subscription:Lrx/Subscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final clear()V
    .locals 9

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreSearchData;->handleSubscription(Lrx/Subscription;)V

    new-instance v0, Lcom/discord/utilities/search/validation/SearchData;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1f

    const/4 v8, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/discord/utilities/search/validation/SearchData;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreSearchData;->handleNewData(Lcom/discord/utilities/search/validation/SearchData;)V

    return-void
.end method

.method public final get()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/utilities/search/validation/SearchData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreSearchData;->searchDataSubject:Lrx/subjects/Subject;

    return-object v0
.end method

.method public final init(Lcom/discord/stores/StoreSearch$SearchTarget;)V
    .locals 9

    const-string v0, "searchTarget"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreSearch$SearchTarget;->getType()Lcom/discord/stores/StoreSearch$SearchTarget$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/discord/stores/StoreSearch$SearchTarget;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreSearchData;->getChannelSearchData(J)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    invoke-virtual {p1}, Lcom/discord/stores/StoreSearch$SearchTarget;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreSearchData;->getGuildSearchData(J)Lrx/Observable;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string p1, "searchDataObservable\n   \u2026  .distinctUntilChanged()"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v1, Lcom/discord/stores/StoreSearchData;

    const/4 v2, 0x0

    new-instance v3, Lcom/discord/stores/StoreSearchData$init$1;

    invoke-direct {v3, p0}, Lcom/discord/stores/StoreSearchData$init$1;-><init>(Lcom/discord/stores/StoreSearchData;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/stores/StoreSearchData$init$2;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreSearchData$init$2;-><init>(Lcom/discord/stores/StoreSearchData;)V

    const/16 v7, 0x1a

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
