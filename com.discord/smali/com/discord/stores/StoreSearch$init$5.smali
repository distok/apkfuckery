.class public final Lcom/discord/stores/StoreSearch$init$5;
.super Lx/m/c/k;
.source "StoreSearch.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreSearch;->init(Lcom/discord/stores/StoreSearch$SearchTarget;Lcom/discord/utilities/search/strings/SearchStringProvider;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreSearch$DisplayState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreSearch;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreSearch;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreSearch$init$5;->this$0:Lcom/discord/stores/StoreSearch;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreSearch$DisplayState;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreSearch$init$5;->invoke(Lcom/discord/stores/StoreSearch$DisplayState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/stores/StoreSearch$DisplayState;)V
    .locals 2

    sget-object v0, Lcom/discord/stores/StoreSearch$DisplayState;->RESULTS:Lcom/discord/stores/StoreSearch$DisplayState;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreSearch$init$5;->this$0:Lcom/discord/stores/StoreSearch;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearch;->getStoreSearchQuery()Lcom/discord/stores/StoreSearchQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearchQuery;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreSearch$init$5;->this$0:Lcom/discord/stores/StoreSearch;

    const-string v1, "it"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/discord/stores/StoreSearch;->access$onStateChanged(Lcom/discord/stores/StoreSearch;Lcom/discord/stores/StoreSearch$DisplayState;)V

    return-void
.end method
