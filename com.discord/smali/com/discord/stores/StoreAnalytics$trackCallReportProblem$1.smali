.class public final Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;
.super Lx/m/c/k;
.source "StoreAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAnalytics;->trackCallReportProblem(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

.field public final synthetic this$0:Lcom/discord/stores/StoreAnalytics;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    iput-object p2, p0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v1}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    iget-object v1, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v1}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v2}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getChannelId()J

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v1, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v1}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v5, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v5}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getRtcConnectionId()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v7}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/stores/StoreStream;->getVoiceStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/stores/StoreVoiceStates;->getMediaStatesBlocking()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    if-eqz v7, :cond_0

    goto :goto_0

    :cond_0
    sget-object v7, Lx/h/m;->d:Lx/h/m;

    :goto_0
    iget-object v8, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v8}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getDurationMs()Ljava/lang/Long;

    move-result-object v8

    iget-object v9, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v9}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getMediaSessionId()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v10}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-result-object v10

    iget-object v11, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v11}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getReasonCode()Ljava/lang/Integer;

    move-result-object v11

    iget-object v12, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v12}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getReasonDescription()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings;->getVoiceConfigurationBlocking()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v13

    iget-object v14, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v14}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v14

    invoke-virtual {v14}, Lcom/discord/stores/StoreStream;->getAudioDevices$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAudioDevices;

    move-result-object v14

    invoke-virtual {v14}, Lcom/discord/stores/StoreAudioDevices;->getAudioDevicesState$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object v14

    invoke-virtual {v14}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v14

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings;->getVideoHardwareScalingBlocking()Z

    move-result v15

    iget-object v1, v0, Lcom/discord/stores/StoreAnalytics$trackCallReportProblem$1;->$pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v1}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->getIssueDetails()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v2 .. v16}, Lcom/discord/utilities/analytics/AnalyticsTracker;->callReportProblem(JLjava/lang/String;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreAudioDevices$OutputDevice;ZLjava/lang/String;)V

    :cond_1
    return-void
.end method
