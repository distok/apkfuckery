.class public final Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;
.super Lx/m/c/k;
.source "StoreChangeLog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreChangeLog;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreChangeLog$StorePayload;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $targetLanguage:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreChangeLog;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreChangeLog;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->this$0:Lcom/discord/stores/StoreChangeLog;

    iput-object p2, p0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->$targetLanguage:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreChangeLog$StorePayload;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->invoke(Lcom/discord/stores/StoreChangeLog$StorePayload;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/stores/StoreChangeLog$StorePayload;)V
    .locals 7

    iget-object v0, p0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->this$0:Lcom/discord/stores/StoreChangeLog;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChangeLog;->getApp()Landroid/app/Application;

    move-result-object v2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    const-string v1, "2020-10_stickers_user_android"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v6

    iget-object v1, p0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->this$0:Lcom/discord/stores/StoreChangeLog;

    invoke-virtual {p1}, Lcom/discord/stores/StoreChangeLog$StorePayload;->getUserId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->$targetLanguage:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/discord/stores/StoreChangeLog;->access$shouldShowChangelog(Lcom/discord/stores/StoreChangeLog;Landroid/content/Context;JLjava/lang/String;Lcom/discord/models/experiments/domain/Experiment;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->this$0:Lcom/discord/stores/StoreChangeLog;

    invoke-static {p1}, Lcom/discord/stores/StoreChangeLog;->access$getNotices$p(Lcom/discord/stores/StoreChangeLog;)Lcom/discord/stores/StoreNotices;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/stores/StoreChangeLog$handleConnectionOpen$2;->this$0:Lcom/discord/stores/StoreChangeLog;

    invoke-static {v0}, Lcom/discord/stores/StoreChangeLog;->access$createChangeLogNotice(Lcom/discord/stores/StoreChangeLog;)Lcom/discord/stores/StoreNotices$Notice;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    :cond_0
    return-void
.end method
