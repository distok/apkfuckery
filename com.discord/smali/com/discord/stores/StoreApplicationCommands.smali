.class public final Lcom/discord/stores/StoreApplicationCommands;
.super Lcom/discord/stores/StoreV2;
.source "StoreApplicationCommands.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreApplicationCommands$Companion;
    }
.end annotation


# static fields
.field public static final COMMANDS_LIMIT_PER_REQUEST:I = 0x14

.field public static final Companion:Lcom/discord/stores/StoreApplicationCommands$Companion;

.field private static final DiscoverCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

.field private static final GuildApplicationsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

.field private static final QueryCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;


# instance fields
.field private applicationCommandIndexes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private applicationNonce:Ljava/lang/String;

.field private final applications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation
.end field

.field private applicationsMapSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation
.end field

.field private applicationsSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation
.end field

.field private final builtInCommandsProvider:Lcom/discord/stores/BuiltInCommandsProvider;

.field private currentStartOffset:I

.field private discoverCommands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation
.end field

.field private discoverCommandsNonce:Ljava/lang/String;

.field private discoverCommandsSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation
.end field

.field private discoverGuildId:Ljava/lang/Long;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private loadDirectionUp:Z

.field private nonceCounter:I

.field private numRemoteCommands:I

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private query:Ljava/lang/String;

.field private final queryCommands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation
.end field

.field private queryCommandsSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation
.end field

.field private queryGuildId:Ljava/lang/Long;

.field private queryNonce:Ljava/lang/String;

.field private final storeGatewayConnection:Lcom/discord/stores/StoreGatewayConnection;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreApplicationCommands$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreApplicationCommands$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreApplicationCommands;->Companion:Lcom/discord/stores/StoreApplicationCommands$Companion;

    new-instance v0, Lcom/discord/stores/StoreApplicationCommands$Companion$DiscoverCommandsUpdate$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreApplicationCommands$Companion$DiscoverCommandsUpdate$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreApplicationCommands;->DiscoverCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    new-instance v0, Lcom/discord/stores/StoreApplicationCommands$Companion$QueryCommandsUpdate$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreApplicationCommands$Companion$QueryCommandsUpdate$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreApplicationCommands;->QueryCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    new-instance v0, Lcom/discord/stores/StoreApplicationCommands$Companion$GuildApplicationsUpdate$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreApplicationCommands$Companion$GuildApplicationsUpdate$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreApplicationCommands;->GuildApplicationsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/BuiltInCommandsProvider;)V
    .locals 1

    const-string/jumbo v0, "storeGatewayConnection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builtInCommandsProvider"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->storeGatewayConnection:Lcom/discord/stores/StoreGatewayConnection;

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationCommands;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreApplicationCommands;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    iput-object p4, p0, Lcom/discord/stores/StoreApplicationCommands;->builtInCommandsProvider:Lcom/discord/stores/BuiltInCommandsProvider;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommandsSnapshot:Ljava/util/List;

    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationCommandIndexes:Ljava/util/Map;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationCommands;->applications:Ljava/util/List;

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationsSnapshot:Ljava/util/List;

    sget-object p2, Lx/h/m;->d:Lx/h/m;

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationsMapSnapshot:Ljava/util/Map;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommands:Ljava/util/List;

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommandsSnapshot:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/BuiltInCommandsProvider;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p3

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    new-instance p4, Lcom/discord/stores/BuiltInCommands;

    invoke-direct {p4}, Lcom/discord/stores/BuiltInCommands;-><init>()V

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreApplicationCommands;-><init>(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/BuiltInCommandsProvider;)V

    return-void
.end method

.method public static final synthetic access$generateNonce(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreApplicationCommands;->generateNonce()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getApplicationCommandIndexes$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationCommandIndexes:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getApplicationNonce$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationNonce:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getApplications$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->applications:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getBuiltInCommandsProvider$p(Lcom/discord/stores/StoreApplicationCommands;)Lcom/discord/stores/BuiltInCommandsProvider;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->builtInCommandsProvider:Lcom/discord/stores/BuiltInCommandsProvider;

    return-object p0
.end method

.method public static final synthetic access$getDiscoverCommands$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getDiscoverCommandsNonce$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommandsNonce:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getDiscoverCommandsUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreApplicationCommands;->DiscoverCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    return-object v0
.end method

.method public static final synthetic access$getDiscoverGuildId$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/Long;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverGuildId:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getGuildApplicationsUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreApplicationCommands;->GuildApplicationsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    return-object v0
.end method

.method public static final synthetic access$getQuery$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->query:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getQueryCommands$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommands:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getQueryCommandsUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreApplicationCommands;->QueryCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    return-object v0
.end method

.method public static final synthetic access$getQueryGuildId$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/Long;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->queryGuildId:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getQueryNonce$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->queryNonce:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getStoreGatewayConnection$p(Lcom/discord/stores/StoreApplicationCommands;)Lcom/discord/stores/StoreGatewayConnection;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationCommands;->storeGatewayConnection:Lcom/discord/stores/StoreGatewayConnection;

    return-object p0
.end method

.method public static final synthetic access$handleDiscoverCommandsUpdate(Lcom/discord/stores/StoreApplicationCommands;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreApplicationCommands;->handleDiscoverCommandsUpdate(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$setApplicationCommandIndexes$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationCommandIndexes:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$setApplicationNonce$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationNonce:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$setDiscoverCommands$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$setDiscoverCommandsNonce$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommandsNonce:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$setDiscoverGuildId$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverGuildId:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$setQuery$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->query:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$setQueryGuildId$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->queryGuildId:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$setQueryNonce$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->queryNonce:Ljava/lang/String;

    return-void
.end method

.method private final generateNonce()Ljava/lang/String;
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget v0, p0, Lcom/discord/stores/StoreApplicationCommands;->nonceCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/discord/stores/StoreApplicationCommands;->nonceCounter:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final handleDiscoverCommandsUpdate(Ljava/util/List;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/stores/ModelApplicationCommand;

    iget-object v4, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v2, v3

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iget-boolean v1, p0, Lcom/discord/stores/StoreApplicationCommands;->loadDirectionUp:Z

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    invoke-static {v0}, Lx/h/f;->asReversed(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/ModelApplicationCommand;

    iget-object v4, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-interface {v4, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/discord/stores/StoreApplicationCommands;->currentStartOffset:I

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, p1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/discord/stores/StoreApplicationCommands;->currentStartOffset:I

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-static {p1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    iget v0, p0, Lcom/discord/stores/StoreApplicationCommands;->currentStartOffset:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/2addr p1, v0

    iget v0, p0, Lcom/discord/stores/StoreApplicationCommands;->numRemoteCommands:I

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->builtInCommandsProvider:Lcom/discord/stores/BuiltInCommandsProvider;

    invoke-interface {v0}, Lcom/discord/stores/BuiltInCommandsProvider;->getBuiltInCommands()Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_4
    :goto_2
    new-array p1, v2, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object v0, Lcom/discord/stores/StoreApplicationCommands;->DiscoverCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    aput-object v0, p1, v3

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    return-void
.end method

.method private final handleGuildApplicationsUpdate(Ljava/util/List;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->applications:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 p1, 0x0

    iput p1, p0, Lcom/discord/stores/StoreApplicationCommands;->numRemoteCommands:I

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->applications:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/slashcommands/ModelApplication;

    iget-object v3, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationCommandIndexes:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/discord/models/slashcommands/ModelApplication;->getCommandCount()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {v2}, Lcom/discord/models/slashcommands/ModelApplication;->getBuiltIn()Z

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/discord/stores/StoreApplicationCommands;->numRemoteCommands:I

    invoke-virtual {v2}, Lcom/discord/models/slashcommands/ModelApplication;->getCommandCount()I

    move-result v2

    add-int/2addr v2, v3

    iput v2, p0, Lcom/discord/stores/StoreApplicationCommands;->numRemoteCommands:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object v1, Lcom/discord/stores/StoreApplicationCommands;->GuildApplicationsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    aput-object v1, v0, p1

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    return-void
.end method

.method private final handleQueryCommandsUpdate(Ljava/util/List;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommands:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->query:Ljava/lang/String;

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommands:Ljava/util/List;

    iget-object v2, p0, Lcom/discord/stores/StoreApplicationCommands;->builtInCommandsProvider:Lcom/discord/stores/BuiltInCommandsProvider;

    invoke-interface {v2}, Lcom/discord/stores/BuiltInCommandsProvider;->getBuiltInCommands()Ljava/util/List;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/discord/stores/ModelApplicationCommand;

    invoke-virtual {v5}, Lcom/discord/stores/ModelApplicationCommand;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1, v0}, Lx/s/m;->startsWith(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    new-array p1, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    sget-object v1, Lcom/discord/stores/StoreApplicationCommands;->QueryCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    aput-object v1, p1, v0

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    return-void
.end method

.method private final requestApplicationCommands(JII)V
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v7, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommands$1;

    move-object v1, v7

    move-object v2, p0

    move-wide v3, p1

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommands$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;JII)V

    invoke-virtual {v0, v7}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic requestApplicationCommands$default(Lcom/discord/stores/StoreApplicationCommands;JIIILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    const/16 p4, 0x14

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreApplicationCommands;->requestApplicationCommands(JII)V

    return-void
.end method


# virtual methods
.method public final clearQueryCommands()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreApplicationCommands$clearQueryCommands$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreApplicationCommands$clearQueryCommands$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final getApplicationMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationsMapSnapshot:Ljava/util/Map;

    return-object v0
.end method

.method public final getApplications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationsSnapshot:Ljava/util/List;

    return-object v0
.end method

.method public final getDiscoverCommands()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommandsSnapshot:Ljava/util/List;

    return-object v0
.end method

.method public final getQueryCommands()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommandsSnapshot:Ljava/util/List;

    return-object v0
.end method

.method public final handleApplicationCommandsUpdate(Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "commandsGateway"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->getNonce()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationNonce:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->getApplications()Ljava/util/List;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/StoreApplicationCommands$handleApplicationCommandsUpdate$$inlined$sortedBy$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreApplicationCommands$handleApplicationCommandsUpdate$$inlined$sortedBy$1;-><init>()V

    invoke-static {p1, v0}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->builtInCommandsProvider:Lcom/discord/stores/BuiltInCommandsProvider;

    invoke-interface {v0}, Lcom/discord/stores/BuiltInCommandsProvider;->getBuiltInApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object v0

    invoke-static {p1, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreApplicationCommands;->handleGuildApplicationsUpdate(Ljava/util/List;)V

    goto :goto_2

    :cond_0
    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->queryNonce:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/16 v2, 0xa

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->getGatewayApplicationCommands()Ljava/util/List;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    invoke-static {v1}, Lcom/discord/stores/StoreApplicationCommandsKt;->toSlashCommand(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;)Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationCommands;->handleQueryCommandsUpdate(Ljava/util/List;)V

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommandsNonce:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;->getGatewayApplicationCommands()Ljava/util/List;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;

    invoke-static {v1}, Lcom/discord/stores/StoreApplicationCommandsKt;->toSlashCommand(Lcom/discord/models/slashcommands/ModelGatewayApplicationCommand;)Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationCommands;->handleDiscoverCommandsUpdate(Ljava/util/List;)V

    :cond_4
    :goto_2
    return-void
.end method

.method public final observeDiscoverCommands()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreApplicationCommands$observeDiscoverCommands$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreApplicationCommands$observeDiscoverCommands$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "observationDeck\n        \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeGuildApplications()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/slashcommands/ModelApplication;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreApplicationCommands$observeGuildApplications$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreApplicationCommands$observeGuildApplications$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "observationDeck\n        \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeQueryCommands()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreApplicationCommands$observeQueryCommands$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreApplicationCommands$observeQueryCommands$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "observationDeck\n        \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final requestApplicationCommandsQuery(JLjava/lang/String;)V
    .locals 2

    const-string v0, "query"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final requestApplications(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreApplicationCommands$requestApplications$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreApplicationCommands$requestApplications$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final requestInitialApplicationCommands(JLjava/lang/Long;)V
    .locals 9

    new-instance v0, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    iget-object p3, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    iget-object p3, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationCommandIndexes:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    iget-object p3, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationCommandIndexes:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    iput p3, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iput p3, p0, Lcom/discord/stores/StoreApplicationCommands;->currentStartOffset:I

    :cond_1
    iget-object p3, p0, Lcom/discord/stores/StoreApplicationCommands;->builtInCommandsProvider:Lcom/discord/stores/BuiltInCommandsProvider;

    invoke-interface {p3}, Lcom/discord/stores/BuiltInCommandsProvider;->getBuiltInApplication()Lcom/discord/models/slashcommands/ModelApplication;

    move-result-object p3

    invoke-virtual {p3}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v4

    cmp-long p3, v2, v4

    if-nez p3, :cond_2

    iget-object p1, p0, Lcom/discord/stores/StoreApplicationCommands;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p2, Lcom/discord/stores/StoreApplicationCommands$requestInitialApplicationCommands$$inlined$let$lambda$1;

    invoke-direct {p2, p0, v0}, Lcom/discord/stores/StoreApplicationCommands$requestInitialApplicationCommands$$inlined$let$lambda$1;-><init>(Lcom/discord/stores/StoreApplicationCommands;Lkotlin/jvm/internal/Ref$IntRef;)V

    invoke-virtual {p1, p2}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void

    :cond_2
    iget v5, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p0

    move-wide v3, p1

    invoke-static/range {v2 .. v8}, Lcom/discord/stores/StoreApplicationCommands;->requestApplicationCommands$default(Lcom/discord/stores/StoreApplicationCommands;JIIILjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/discord/stores/StoreApplicationCommands;->loadDirectionUp:Z

    return-void
.end method

.method public final requestLoadMoreDown()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverGuildId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    iget v2, p0, Lcom/discord/stores/StoreApplicationCommands;->currentStartOffset:I

    iget-object v3, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v2

    const/16 v2, 0x14

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/discord/stores/StoreApplicationCommands;->requestApplicationCommands(JII)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreApplicationCommands;->loadDirectionUp:Z

    :cond_0
    return-void
.end method

.method public final requestLoadMoreUp()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverGuildId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    iget v2, p0, Lcom/discord/stores/StoreApplicationCommands;->currentStartOffset:I

    add-int/lit8 v3, v2, -0x14

    if-gez v3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x14

    :goto_0
    invoke-direct {p0, v0, v1, v3, v2}, Lcom/discord/stores/StoreApplicationCommands;->requestApplicationCommands(JII)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreApplicationCommands;->loadDirectionUp:Z

    :cond_1
    return-void
.end method

.method public snapshotData()V
    .locals 5

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->getUpdateSources()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreApplicationCommands;->DiscoverCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommands:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->discoverCommandsSnapshot:Ljava/util/List;

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->getUpdateSources()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreApplicationCommands;->QueryCommandsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommands:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->queryCommandsSnapshot:Ljava/util/List;

    :cond_1
    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->getUpdateSources()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreApplicationCommands;->GuildApplicationsUpdate:Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands;->applications:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationsSnapshot:Ljava/util/List;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_2

    const/16 v1, 0x10

    :cond_2
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/models/slashcommands/ModelApplication;

    invoke-virtual {v3}, Lcom/discord/models/slashcommands/ModelApplication;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lcom/discord/stores/StoreApplicationCommands;->applicationsMapSnapshot:Ljava/util/Map;

    :cond_4
    return-void
.end method
