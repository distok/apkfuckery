.class public final Lcom/discord/stores/RtcLatencyTestResult;
.super Ljava/lang/Object;
.source "StoreRtcRegion.kt"


# instance fields
.field private final geoRankedRegions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final lastTestTimestampMs:J

.field private final latencyRankedRegions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/RtcLatencyTestResult;-><init>(Ljava/util/List;Ljava/util/List;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    const-string v0, "latencyRankedRegions"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "geoRankedRegions"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    iput-wide p3, p0, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Ljava/util/List;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    sget-object p6, Lx/h/l;->d:Lx/h/l;

    and-int/lit8 v0, p5, 0x1

    if-eqz v0, :cond_0

    move-object p1, p6

    :cond_0
    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_1

    move-object p2, p6

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    const-wide/16 p3, 0x0

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/RtcLatencyTestResult;-><init>(Ljava/util/List;Ljava/util/List;J)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/RtcLatencyTestResult;Ljava/util/List;Ljava/util/List;JILjava/lang/Object;)Lcom/discord/stores/RtcLatencyTestResult;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-wide p3, p0, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/RtcLatencyTestResult;->copy(Ljava/util/List;Ljava/util/List;J)Lcom/discord/stores/RtcLatencyTestResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    return-wide v0
.end method

.method public final copy(Ljava/util/List;Ljava/util/List;J)Lcom/discord/stores/RtcLatencyTestResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;J)",
            "Lcom/discord/stores/RtcLatencyTestResult;"
        }
    .end annotation

    const-string v0, "latencyRankedRegions"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "geoRankedRegions"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/RtcLatencyTestResult;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/stores/RtcLatencyTestResult;-><init>(Ljava/util/List;Ljava/util/List;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/RtcLatencyTestResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/RtcLatencyTestResult;

    iget-object v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    iget-wide v2, p1, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGeoRankedRegions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    return-object v0
.end method

.method public final getLastTestTimestampMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    return-wide v0
.end method

.method public final getLatencyRankedRegions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "RtcLatencyTestResult(latencyRankedRegions="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/RtcLatencyTestResult;->latencyRankedRegions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", geoRankedRegions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/RtcLatencyTestResult;->geoRankedRegions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastTestTimestampMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/stores/RtcLatencyTestResult;->lastTestTimestampMs:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
