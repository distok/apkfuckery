.class public final Lcom/discord/stores/StoreMessageReplies;
.super Lcom/discord/stores/StoreV2;
.source "StoreMessageReplies.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreMessageReplies$MessageState;,
        Lcom/discord/stores/StoreMessageReplies$MessageCache;,
        Lcom/discord/stores/StoreMessageReplies$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreMessageReplies$Companion;

.field private static final NO_RESULTS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final channelMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private repliedChannelMessagesCacheSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/stores/StoreMessageReplies$MessageCache<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;"
        }
    .end annotation
.end field

.field private repliedMessagesCacheSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;"
        }
    .end annotation
.end field

.field private final storeMessages:Lcom/discord/stores/StoreMessages;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreMessageReplies$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreMessageReplies$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreMessageReplies;->Companion:Lcom/discord/stores/StoreMessageReplies$Companion;

    sget-object v0, Lx/h/m;->d:Lx/h/m;

    sput-object v0, Lcom/discord/stores/StoreMessageReplies;->NO_RESULTS:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeMessages"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReplies;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreMessageReplies;->storeMessages:Lcom/discord/stores/StoreMessages;

    iput-object p3, p0, Lcom/discord/stores/StoreMessageReplies;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    new-instance p1, Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-direct {p1}, Lcom/discord/stores/StoreMessageReplies$MessageCache;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReplies;->channelMap:Ljava/util/HashMap;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReplies;->repliedChannelMessagesCacheSnapshot:Ljava/util/Map;

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCacheSnapshot:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreMessageReplies;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/updates/ObservationDeck;)V

    return-void
.end method

.method public static final synthetic access$getCachedChannelMessages(Lcom/discord/stores/StoreMessageReplies;J)Ljava/util/Map;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessageReplies;->getCachedChannelMessages(J)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private final deleteMessage(JJ)Z
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v6, Lcom/discord/stores/StoreMessageReplies$MessageState$Deleted;->INSTANCE:Lcom/discord/stores/StoreMessageReplies$MessageState$Deleted;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreMessageReplies;->updateCache(JJLcom/discord/stores/StoreMessageReplies$MessageState;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private final getCachedChannelMessages(J)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedChannelMessagesCacheSnapshot:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/stores/StoreMessageReplies;->NO_RESULTS:Ljava/util/Map;

    :goto_0
    return-object p1
.end method

.method private final processMessage(Lcom/discord/models/domain/ModelMessage;)Z
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v6, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v3

    new-instance v5, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;

    invoke-direct {v5, p1}, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreMessageReplies;->updateCache(JJLcom/discord/stores/StoreMessageReplies$MessageState;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v1

    const/16 v2, 0x13

    if-ne v1, v2, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v2, "message.messageReference ?: return updated"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getReferencedMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getChannelId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getMessageId()Ljava/lang/Long;

    move-result-object v1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v7

    new-instance v5, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;

    invoke-direct {v5, v2}, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    move-object v0, p0

    move-wide v1, v3

    move-wide v3, v7

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreMessageReplies;->updateCache(JJLcom/discord/stores/StoreMessageReplies$MessageState;)V

    goto :goto_1

    :cond_1
    if-eqz v3, :cond_4

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->storeMessages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v0, v4, v5, v7, v8}, Lcom/discord/stores/StoreMessages;->getMessage(JJ)Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v3

    new-instance v5, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;

    invoke-direct {v5, v0}, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreMessageReplies;->updateCache(JJLcom/discord/stores/StoreMessageReplies$MessageState;)V

    return v6

    :cond_2
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sget-object v5, Lcom/discord/stores/StoreMessageReplies$MessageState$Unloaded;->INSTANCE:Lcom/discord/stores/StoreMessageReplies$MessageState$Unloaded;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreMessageReplies;->updateCache(JJLcom/discord/stores/StoreMessageReplies$MessageState;)V

    goto :goto_1

    :cond_3
    return v0

    :cond_4
    move v6, v0

    :goto_1
    return v6
.end method

.method private final snapShotAllMessages()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCacheSnapshot:Ljava/util/Map;

    return-void
.end method

.method private final snapShotChannelMessages()V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->channelMap:Ljava/util/HashMap;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    new-instance v4, Ljava/util/LinkedHashMap;

    const/16 v5, 0xa

    invoke-static {v2, v5}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-static {v5}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v5

    const/16 v6, 0x10

    if-ge v5, v6, :cond_0

    const/16 v5, 0x10

    :cond_0
    invoke-direct {v4, v5}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    iget-object v8, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/stores/StoreMessageReplies$MessageState;

    if-eqz v6, :cond_1

    goto :goto_2

    :cond_1
    sget-object v6, Lcom/discord/stores/StoreMessageReplies$MessageState$Unloaded;->INSTANCE:Lcom/discord/stores/StoreMessageReplies$MessageState$Unloaded;

    :goto_2
    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iput-object v1, p0, Lcom/discord/stores/StoreMessageReplies;->repliedChannelMessagesCacheSnapshot:Ljava/util/Map;

    return-void
.end method

.method private final updateCache(JJLcom/discord/stores/StoreMessageReplies$MessageState;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p5, p0, Lcom/discord/stores/StoreMessageReplies;->channelMap:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/stores/StoreMessageReplies;->channelMap:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Set;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    sget-object p2, Lx/h/n;->d:Lx/h/n;

    :goto_0
    const-string p3, "$this$union"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "other"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lx/h/f;->toMutableSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    invoke-static {p1, p2}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    invoke-interface {p5, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getAllMessageReferences()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCacheSnapshot:Ljava/util/Map;

    return-object v0
.end method

.method public final handleLoadMessages(Ljava/util/Collection;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "messages"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelMessage;

    invoke-direct {p0, v1}, Lcom/discord/stores/StoreMessageReplies;->processMessage(Lcom/discord/models/domain/ModelMessage;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_1
    return-void
.end method

.method public final handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessageReplies;->processMessage(Lcom/discord/models/domain/ModelMessage;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "messageDeleteBulk"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getMessageIds()Ljava/util/List;

    move-result-object v0

    const-string v1, "messageDeleteBulk.messageIds"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    const-string v4, "messageId"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getChannelId()J

    move-result-wide v5

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/discord/stores/StoreMessageReplies;->deleteMessage(JJ)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v2, :cond_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_3
    return-void
.end method

.method public final handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->repliedMessagesCache:Lcom/discord/stores/StoreMessageReplies$MessageCache;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreMessageReplies$MessageState;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/discord/models/domain/ModelMessage;

    check-cast v0, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;->getModelMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Lcom/discord/models/domain/ModelMessage;-><init>(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v5

    new-instance v7, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;

    invoke-direct {v7, v1}, Lcom/discord/stores/StoreMessageReplies$MessageState$Loaded;-><init>(Lcom/discord/models/domain/ModelMessage;)V

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/discord/stores/StoreMessageReplies;->updateCache(JJLcom/discord/stores/StoreMessageReplies$MessageState;)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final observeMessageReferencesForChannel(J)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;

    invoke-direct {v5, p0, p1, p2}, Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;-><init>(Lcom/discord/stores/StoreMessageReplies;J)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observationDeck\n        \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public snapshotData()V
    .locals 0

    invoke-super {p0}, Lcom/discord/stores/StoreV2;->snapshotData()V

    invoke-direct {p0}, Lcom/discord/stores/StoreMessageReplies;->snapShotChannelMessages()V

    invoke-direct {p0}, Lcom/discord/stores/StoreMessageReplies;->snapShotAllMessages()V

    return-void
.end method
