.class public final Lcom/discord/stores/StoreUser$observeMe$3;
.super Ljava/lang/Object;
.source "StoreUser.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUser;->observeMe(Z)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelUser$Me;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $emitNullOrEmpty:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreUser$observeMe$3;->$emitNullOrEmpty:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser$Me;)Ljava/lang/Boolean;
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/discord/models/domain/ModelUser$Me;->EMPTY:Lcom/discord/models/domain/ModelUser$Me;

    if-ne p1, v0, :cond_1

    :cond_0
    iget-boolean p1, p0, Lcom/discord/stores/StoreUser$observeMe$3;->$emitNullOrEmpty:Z

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser$Me;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreUser$observeMe$3;->call(Lcom/discord/models/domain/ModelUser$Me;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
