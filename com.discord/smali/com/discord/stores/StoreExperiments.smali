.class public final Lcom/discord/stores/StoreExperiments;
.super Lcom/discord/stores/StoreV2;
.source "StoreExperiments.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreExperiments$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreExperiments$Companion;

.field private static final DISCORD_TESTERS_GUILD_ID:J = 0x2bc056ab0800006L

.field private static final EXPERIMENT_OVERRIDES_CACHE_KEY:Ljava/lang/String; = "EXPERIMENT_OVERRIDES_CACHE_KEY"

.field private static final EXPERIMENT_TRIGGER_TIMESTAMPS_CACHE_KEY:Ljava/lang/String; = "EXPERIMENT_TRIGGER_TIMESTAMPS_CACHE_KEY"

.field private static final ExperimentOverridesUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

.field private static final GUILD_EXPERIMENTS_CACHE_KEY:Ljava/lang/String; = "GUILD_EXPERIMENTS_CACHE_KEY"

.field private static final InitializedUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1;

.field private static final UNINITIALIZED:Ljava/lang/String; = "UNINITIALIZED"

.field private static final USER_EXPERIMENTS_CACHE_KEY:Ljava/lang/String; = "USER_EXPERIMENTS_CACHE_KEY"


# instance fields
.field private authToken:Ljava/lang/String;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final experimentOverrides:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final experimentOverridesCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private experimentOverridesSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final experimentTriggerTimestamps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private fingerprint:Ljava/lang/String;

.field private final guildExperiments:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/experiments/dto/GuildExperimentDto;",
            ">;"
        }
    .end annotation
.end field

.field private final guildExperimentsCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/List<",
            "Lcom/discord/models/experiments/dto/GuildExperimentDto;",
            ">;>;"
        }
    .end annotation
.end field

.field private guildExperimentsSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/experiments/dto/GuildExperimentDto;",
            ">;"
        }
    .end annotation
.end field

.field private initialized:Z

.field private final memoizedGuildExperiments:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/models/experiments/domain/Experiment;",
            ">;"
        }
    .end annotation
.end field

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private final storeAuthentication:Lcom/discord/stores/StoreAuthentication;

.field private final storeGuildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

.field private final storeGuilds:Lcom/discord/stores/StoreGuilds;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final userExperiments:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/experiments/dto/UserExperimentDto;",
            ">;"
        }
    .end annotation
.end field

.field private final userExperimentsCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/experiments/dto/UserExperimentDto;",
            ">;>;"
        }
    .end annotation
.end field

.field private userExperimentsSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/experiments/dto/UserExperimentDto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreExperiments$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreExperiments$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreExperiments;->Companion:Lcom/discord/stores/StoreExperiments$Companion;

    new-instance v0, Lcom/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreExperiments;->InitializedUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1;

    new-instance v0, Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreExperiments;->ExperimentOverridesUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

    return-void
.end method

.method public constructor <init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreGuildMemberCounts;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string v0, "clock"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuilds"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAuthentication"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuildMemberCounts"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p2, p0, Lcom/discord/stores/StoreExperiments;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreExperiments;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p4, p0, Lcom/discord/stores/StoreExperiments;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object p5, p0, Lcom/discord/stores/StoreExperiments;->storeAuthentication:Lcom/discord/stores/StoreAuthentication;

    iput-object p6, p0, Lcom/discord/stores/StoreExperiments;->storeGuildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    iput-object p7, p0, Lcom/discord/stores/StoreExperiments;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const-string p1, "UNINITIALIZED"

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->authToken:Ljava/lang/String;

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->fingerprint:Ljava/lang/String;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->userExperimentsSnapshot:Ljava/util/Map;

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->guildExperimentsSnapshot:Ljava/util/Map;

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->experimentOverridesSnapshot:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->userExperiments:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->guildExperiments:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->experimentOverrides:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->memoizedGuildExperiments:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->experimentTriggerTimestamps:Ljava/util/HashMap;

    new-instance p1, Lcom/discord/utilities/persister/Persister;

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    const-string p3, "USER_EXPERIMENTS_CACHE_KEY"

    invoke-direct {p1, p3, p2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->userExperimentsCache:Lcom/discord/utilities/persister/Persister;

    new-instance p1, Lcom/discord/utilities/persister/Persister;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    const-string p3, "GUILD_EXPERIMENTS_CACHE_KEY"

    invoke-direct {p1, p3, p2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->guildExperimentsCache:Lcom/discord/utilities/persister/Persister;

    new-instance p1, Lcom/discord/utilities/persister/Persister;

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    const-string p3, "EXPERIMENT_OVERRIDES_CACHE_KEY"

    invoke-direct {p1, p3, p2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->experimentOverridesCache:Lcom/discord/utilities/persister/Persister;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreGuildMemberCounts;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v0

    move-object v8, v0

    goto :goto_0

    :cond_0
    move-object/from16 v8, p7

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/stores/StoreExperiments;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreGuildMemberCounts;Lcom/discord/stores/updates/ObservationDeck;)V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreExperiments;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreExperiments;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getExperimentOverrides$p(Lcom/discord/stores/StoreExperiments;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreExperiments;->experimentOverrides:Ljava/util/HashMap;

    return-object p0
.end method

.method public static final synthetic access$getInitialized$p(Lcom/discord/stores/StoreExperiments;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreExperiments;->initialized:Z

    return p0
.end method

.method public static final synthetic access$getStoreAuthentication$p(Lcom/discord/stores/StoreExperiments;)Lcom/discord/stores/StoreAuthentication;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreExperiments;->storeAuthentication:Lcom/discord/stores/StoreAuthentication;

    return-object p0
.end method

.method public static final synthetic access$getStoreGuilds$p(Lcom/discord/stores/StoreExperiments;)Lcom/discord/stores/StoreGuilds;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreExperiments;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    return-object p0
.end method

.method public static final synthetic access$getStoreUser$p(Lcom/discord/stores/StoreExperiments;)Lcom/discord/stores/StoreUser;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreExperiments;->storeUser:Lcom/discord/stores/StoreUser;

    return-object p0
.end method

.method public static final synthetic access$handleClearOverride(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreExperiments;->handleClearOverride(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleLoadedUserExperiments(Lcom/discord/stores/StoreExperiments;Ljava/util/Map;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreExperiments;->handleLoadedUserExperiments(Ljava/util/Map;Z)V

    return-void
.end method

.method public static final synthetic access$handleSetOverride(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreExperiments;->handleSetOverride(Ljava/lang/String;I)V

    return-void
.end method

.method public static final synthetic access$setInitialized(Lcom/discord/stores/StoreExperiments;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->setInitialized()V

    return-void
.end method

.method public static final synthetic access$setInitialized$p(Lcom/discord/stores/StoreExperiments;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreExperiments;->initialized:Z

    return-void
.end method

.method public static final synthetic access$triggerGuildExperiment(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;JII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/stores/StoreExperiments;->triggerGuildExperiment(Ljava/lang/String;JII)V

    return-void
.end method

.method public static final synthetic access$triggerUserExperiment(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreExperiments;->triggerUserExperiment(Ljava/lang/String;III)V

    return-void
.end method

.method private final cacheExperimentTriggerTimestamps()V
    .locals 3

    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->experimentTriggerTimestamps:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->k(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "EXPERIMENT_TRIGGER_TIMESTAMPS_CACHE_KEY"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private final cacheGuildExperiments()V
    .locals 12

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->guildExperiments:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "guildExperiments.values"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/discord/models/experiments/dto/GuildExperimentDto;

    sget-object v5, Lcom/discord/utilities/experiments/ExperimentRegistry;->INSTANCE:Lcom/discord/utilities/experiments/ExperimentRegistry;

    invoke-virtual {v5}, Lcom/discord/utilities/experiments/ExperimentRegistry;->getRegisteredExperiments()Ljava/util/LinkedHashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    const-string v6, "ExperimentRegistry.registeredExperiments.values"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/utilities/experiments/RegisteredExperiment;

    sget-object v8, Lcom/discord/models/experiments/domain/ExperimentHash;->INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;

    invoke-virtual {v6}, Lcom/discord/utilities/experiments/RegisteredExperiment;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v8

    invoke-virtual {v4}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getExperimentIdHash()J

    move-result-wide v10

    cmp-long v6, v8, v10

    if-nez v6, :cond_4

    const/4 v6, 0x1

    goto :goto_1

    :cond_4
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_3

    :goto_2
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->guildExperimentsCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0, v1, v3}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    return-void
.end method

.method private final cacheUserExperiments()V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->userExperiments:Ljava/util/HashMap;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    sget-object v4, Lcom/discord/utilities/experiments/ExperimentRegistry;->INSTANCE:Lcom/discord/utilities/experiments/ExperimentRegistry;

    invoke-virtual {v4}, Lcom/discord/utilities/experiments/ExperimentRegistry;->getRegisteredExperiments()Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    const-string v5, "ExperimentRegistry.registeredExperiments.values"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/utilities/experiments/RegisteredExperiment;

    sget-object v7, Lcom/discord/models/experiments/domain/ExperimentHash;->INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;

    invoke-virtual {v5}, Lcom/discord/utilities/experiments/RegisteredExperiment;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v7

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/experiments/dto/UserExperimentDto;

    invoke-virtual {v5}, Lcom/discord/models/experiments/dto/UserExperimentDto;->getNameHash()J

    move-result-wide v9

    cmp-long v5, v7, v9

    if-nez v5, :cond_4

    const/4 v5, 0x1

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_3

    :goto_2
    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->userExperimentsCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0, v1, v3}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    return-void
.end method

.method private final didTriggerExperiment(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->experimentTriggerTimestamps:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->cacheExperimentTriggerTimestamps()V

    return-void
.end method

.method private final handleClearOverride(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->experimentOverrides:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object v0, Lcom/discord/stores/StoreExperiments;->ExperimentOverridesUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    return-void
.end method

.method private final handleLoadedGuildExperiments(Ljava/util/Collection;Z)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/discord/models/experiments/dto/GuildExperimentDto;",
            ">;Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->guildExperiments:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->memoizedGuildExperiments:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->guildExperiments:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/models/experiments/dto/GuildExperimentDto;

    invoke-virtual {v2}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getExperimentIdHash()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->cacheGuildExperiments()V

    :cond_1
    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleLoadedUserExperiments(Ljava/util/Map;Z)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/experiments/dto/UserExperimentDto;",
            ">;Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->userExperiments:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->userExperiments:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->cacheUserExperiments()V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleSetOverride(Ljava/lang/String;I)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->experimentOverrides:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object p2, Lcom/discord/stores/StoreExperiments;->ExperimentOverridesUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    return-void
.end method

.method private final loadCachedExperimentTriggerTimestamps()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "EXPERIMENT_TRIGGER_TIMESTAMPS_CACHE_KEY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/stores/StoreExperiments$loadCachedExperimentTriggerTimestamps$typeToken$1;

    invoke-direct {v1}, Lcom/discord/stores/StoreExperiments$loadCachedExperimentTriggerTimestamps$typeToken$1;-><init>()V

    invoke-virtual {v1}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v2, v0, v1}, Lcom/google/gson/Gson;->f(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "Gson().fromJson(json, typeToken)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Map;

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    return-object v0
.end method

.method private final declared-synchronized memoizeGuildExperiment(Ljava/lang/String;JLcom/discord/models/experiments/domain/Experiment;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3a

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/stores/StoreExperiments;->memoizedGuildExperiments:Ljava/util/HashMap;

    invoke-interface {p2, p1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final reset()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->authToken:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->userExperiments:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->guildExperiments:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->userExperimentsCache:Lcom/discord/utilities/persister/Persister;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->guildExperimentsCache:Lcom/discord/utilities/persister/Persister;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreExperiments;->initialized:Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method private final setInitialized()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreExperiments;->initialized:Z

    new-array v0, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object v1, Lcom/discord/stores/StoreExperiments;->InitializedUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    return-void
.end method

.method private final declared-synchronized triggerGuildExperiment(Ljava/lang/String;JII)V
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->wasExperimentTriggeredRecently(Ljava/lang/String;J)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {p1, p5, p4, p2, p3}, Lcom/discord/utilities/analytics/AnalyticsTracker;->guildExperimentTriggered(Ljava/lang/String;IIJ)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreExperiments;->didTriggerExperiment(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized triggerUserExperiment(Ljava/lang/String;III)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/stores/StoreExperiments;->wasExperimentTriggeredRecently(Ljava/lang/String;J)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {p1, p4, p3, p2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->userExperimentTriggered(Ljava/lang/String;III)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreExperiments;->didTriggerExperiment(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final tryInitializeExperiments()V
    .locals 13
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreExperiments;->initialized:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->authToken:Ljava/lang/String;

    const-string v1, "UNINITIALIZED"

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->fingerprint:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreExperiments;->initialized:Z

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->authToken:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->setInitialized()V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI;->getExperiments()Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-wide/16 v1, 0x7d0

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lrx/Observable;->X(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v4

    const-string v0, "RestAPI\n          .api\n \u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v5, Lcom/discord/stores/StoreExperiments;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v10, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$1;-><init>(Lcom/discord/stores/StoreExperiments;)V

    const/4 v9, 0x0

    new-instance v8, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreExperiments$tryInitializeExperiments$2;-><init>(Lcom/discord/stores/StoreExperiments;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final wasExperimentTriggeredRecently(Ljava/lang/String;J)Z
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->experimentTriggerTimestamps:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    :goto_0
    const-string v0, "experimentTriggerTimesta\u2026y] ?: Timestamp.MIN_VALUE"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0x240c8400

    sub-long/2addr p2, v2

    cmp-long p1, p2, v0

    if-gez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method


# virtual methods
.method public final clearOverride(Ljava/lang/String;)V
    .locals 2

    const-string v0, "experimentName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreExperiments$clearOverride$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreExperiments$clearOverride$1;-><init>(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final getExperimentalAlpha()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    iget-object v2, p0, Lcom/discord/stores/StoreExperiments;->storeUser:Lcom/discord/stores/StoreUser;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreExperiments;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v5, Lcom/discord/stores/StoreExperiments$getExperimentalAlpha$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreExperiments$getExperimentalAlpha$1;-><init>(Lcom/discord/stores/StoreExperiments;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getGuildExperiment(Ljava/lang/String;JZ)Lcom/discord/models/experiments/domain/Experiment;
    .locals 18

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-wide/from16 v9, p2

    const-string v0, "experimentName"

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/experiments/domain/ExperimentHash;->INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;

    invoke-virtual {v0, v8}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v0

    iget-object v2, v7, Lcom/discord/stores/StoreExperiments;->experimentOverridesSnapshot:Ljava/util/Map;

    invoke-interface {v2, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v3, v7, Lcom/discord/stores/StoreExperiments;->guildExperimentsSnapshot:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/discord/models/experiments/dto/GuildExperimentDto;

    if-eqz v2, :cond_1

    new-instance v0, Lcom/discord/models/experiments/domain/Experiment;

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getRevision()I

    move-result v1

    move v9, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v9, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x1

    sget-object v13, Lcom/discord/stores/StoreExperiments$getGuildExperiment$1;->INSTANCE:Lcom/discord/stores/StoreExperiments$getGuildExperiment$1;

    move-object v8, v0

    invoke-direct/range {v8 .. v13}, Lcom/discord/models/experiments/domain/Experiment;-><init>(IIIZLkotlin/jvm/functions/Function0;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0, v9, v10}, Lcom/discord/stores/StoreExperiments;->getMemoizedGuildExperiment$app_productionDiscordExternalRelease(Ljava/lang/String;J)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    if-eqz v0, :cond_2

    return-object v0

    :cond_2
    if-nez v6, :cond_3

    const/4 v0, 0x0

    return-object v0

    :cond_3
    sget-object v0, Lcom/discord/utilities/experiments/ExperimentUtils;->INSTANCE:Lcom/discord/utilities/experiments/ExperimentUtils;

    iget-object v1, v7, Lcom/discord/stores/StoreExperiments;->storeGuildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-virtual {v1, v9, v10}, Lcom/discord/stores/StoreGuildMemberCounts;->getApproximateMemberCount(J)I

    move-result v4

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-object v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/experiments/ExperimentUtils;->computeGuildExperimentBucket(Ljava/lang/String;JILcom/discord/models/experiments/dto/GuildExperimentDto;)I

    move-result v13

    invoke-virtual {v6}, Lcom/discord/models/experiments/dto/GuildExperimentDto;->getRevision()I

    move-result v12

    const/4 v14, 0x0

    new-instance v15, Lcom/discord/models/experiments/domain/Experiment;

    const/16 v16, 0x0

    new-instance v17, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v3, p2

    move v5, v13

    move v6, v12

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;-><init>(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;JII)V

    move-object v11, v15

    move-object v0, v15

    move/from16 v15, v16

    move-object/from16 v16, v17

    invoke-direct/range {v11 .. v16}, Lcom/discord/models/experiments/domain/Experiment;-><init>(IIIZLkotlin/jvm/functions/Function0;)V

    if-eqz p4, :cond_4

    invoke-virtual {v0}, Lcom/discord/models/experiments/domain/Experiment;->getTrigger()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_4
    invoke-direct {v7, v8, v9, v10, v0}, Lcom/discord/stores/StoreExperiments;->memoizeGuildExperiment(Ljava/lang/String;JLcom/discord/models/experiments/domain/Experiment;)V

    return-object v0
.end method

.method public final declared-synchronized getMemoizedGuildExperiment$app_productionDiscordExternalRelease(Ljava/lang/String;J)Lcom/discord/models/experiments/domain/Experiment;
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "experimentName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3a

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/stores/StoreExperiments;->memoizedGuildExperiments:Ljava/util/HashMap;

    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/experiments/domain/Experiment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;
    .locals 12

    const-string v0, "name"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/experiments/domain/ExperimentHash;->INSTANCE:Lcom/discord/models/experiments/domain/ExperimentHash;

    invoke-virtual {v0, p1}, Lcom/discord/models/experiments/domain/ExperimentHash;->from(Ljava/lang/CharSequence;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/stores/StoreExperiments;->experimentOverridesSnapshot:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/discord/stores/StoreExperiments;->userExperimentsSnapshot:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/experiments/dto/UserExperimentDto;

    if-eqz v2, :cond_2

    new-instance p1, Lcom/discord/models/experiments/domain/Experiment;

    const/4 p2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/experiments/dto/UserExperimentDto;->getRevision()I

    move-result v1

    move v4, v1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/experiments/dto/UserExperimentDto;->getPopulation()I

    move-result p2

    move v6, p2

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    :goto_1
    const/4 v7, 0x1

    sget-object v8, Lcom/discord/stores/StoreExperiments$getUserExperiment$1;->INSTANCE:Lcom/discord/stores/StoreExperiments$getUserExperiment$1;

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/discord/models/experiments/domain/Experiment;-><init>(IIIZLkotlin/jvm/functions/Function0;)V

    return-object p1

    :cond_2
    if-nez v0, :cond_3

    const/4 p1, 0x0

    return-object p1

    :cond_3
    invoke-virtual {v0}, Lcom/discord/models/experiments/dto/UserExperimentDto;->getBucket()I

    move-result v6

    invoke-virtual {v0}, Lcom/discord/models/experiments/dto/UserExperimentDto;->getPopulation()I

    move-result v7

    invoke-virtual {v0}, Lcom/discord/models/experiments/dto/UserExperimentDto;->getRevision()I

    move-result v8

    new-instance v9, Lcom/discord/models/experiments/domain/Experiment;

    const/4 v10, 0x0

    new-instance v11, Lcom/discord/stores/StoreExperiments$getUserExperiment$experiment$1;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move v3, v6

    move v4, v7

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreExperiments$getUserExperiment$experiment$1;-><init>(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;III)V

    move-object v0, v9

    move v1, v8

    move v2, v6

    move v3, v7

    move v4, v10

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lcom/discord/models/experiments/domain/Experiment;-><init>(IIIZLkotlin/jvm/functions/Function0;)V

    if-eqz p2, :cond_4

    invoke-virtual {v9}, Lcom/discord/models/experiments/domain/Experiment;->getTrigger()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_4
    return-object v9
.end method

.method public final handleAuthToken(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->authToken:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->reset()V

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->tryInitializeExperiments()V

    return-void
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getExperiments()Ljava/util/Map;

    move-result-object v0

    const-string v1, "payload.experiments"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreExperiments;->handleLoadedUserExperiments(Ljava/util/Map;Z)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuildExperiments()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v1}, Lcom/discord/stores/StoreExperiments;->handleLoadedGuildExperiments(Ljava/util/Collection;Z)V

    :cond_0
    return-void
.end method

.method public final handleFingerprint(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments;->fingerprint:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->tryInitializeExperiments()V

    return-void
.end method

.method public final handlePreLogout()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->clearCachedExperiment()V

    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/discord/stores/StoreExperiments;->experimentTriggerTimestamps:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/discord/stores/StoreExperiments;->loadCachedExperimentTriggerTimestamps()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iget-object p1, p0, Lcom/discord/stores/StoreExperiments;->experimentOverrides:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->experimentOverridesCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iget-object p1, p0, Lcom/discord/stores/StoreExperiments;->userExperimentsCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {p1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/stores/StoreExperiments;->handleLoadedUserExperiments(Ljava/util/Map;Z)V

    iget-object p1, p0, Lcom/discord/stores/StoreExperiments;->guildExperimentsCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {p1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-direct {p0, p1, v0}, Lcom/discord/stores/StoreExperiments;->handleLoadedGuildExperiments(Ljava/util/Collection;Z)V

    const/4 p1, 0x2

    new-array p1, p1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    aput-object p0, p1, v0

    sget-object v0, Lcom/discord/stores/StoreExperiments;->ExperimentOverridesUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

    const/4 v1, 0x1

    aput-object v0, p1, v1

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    return-void
.end method

.method public final isInitialized()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object v2, Lcom/discord/stores/StoreExperiments;->InitializedUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v5, Lcom/discord/stores/StoreExperiments$isInitialized$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreExperiments$isInitialized$1;-><init>(Lcom/discord/stores/StoreExperiments;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeGuildExperiment(Ljava/lang/String;JZ)Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JZ)",
            "Lrx/Observable<",
            "Lcom/discord/models/experiments/domain/Experiment;",
            ">;"
        }
    .end annotation

    const-string v0, "experimentName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p0, v2, v0

    new-instance v0, Lcom/discord/stores/StoreExperiments$observeGuildExperiment$1;

    move-object v3, v0

    move-object v4, p0

    move-object v5, p1

    move-wide v6, p2

    move v8, p4

    invoke-direct/range {v3 .. v8}, Lcom/discord/stores/StoreExperiments$observeGuildExperiment$1;-><init>(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;JZ)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v6, v0

    invoke-static/range {v1 .. v8}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final observeOverrides()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    sget-object v2, Lcom/discord/stores/StoreExperiments;->ExperimentOverridesUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v5, Lcom/discord/stores/StoreExperiments$observeOverrides$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreExperiments$observeOverrides$1;-><init>(Lcom/discord/stores/StoreExperiments;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/discord/models/experiments/domain/Experiment;",
            ">;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p0, v2, v0

    new-instance v6, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;

    invoke-direct {v6, p0, p1, p2}, Lcom/discord/stores/StoreExperiments$observeUserExperiment$1;-><init>(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;Z)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observationDeck.connectR\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final setOverride(Ljava/lang/String;I)V
    .locals 2

    const-string v0, "experimentName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreExperiments$setOverride$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/discord/stores/StoreExperiments$setOverride$1;-><init>(Lcom/discord/stores/StoreExperiments;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public snapshotData()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-super {p0}, Lcom/discord/stores/StoreV2;->snapshotData()V

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->userExperiments:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreExperiments;->userExperimentsSnapshot:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->guildExperiments:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreExperiments;->guildExperimentsSnapshot:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->getUpdateSources()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreExperiments;->ExperimentOverridesUpdateSource:Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->experimentOverrides:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreExperiments;->experimentOverridesSnapshot:Ljava/util/Map;

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments;->experimentOverridesCache:Lcom/discord/utilities/persister/Persister;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments;->experimentOverrides:Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    :cond_0
    return-void
.end method
