.class public final Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;
.super Lx/m/c/k;
.source "StoreStreamRtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStreamRtcConnection;->updateStreamVolume(F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $volume:F

.field public final synthetic this$0:Lcom/discord/stores/StoreStreamRtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStreamRtcConnection;F)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    iput p2, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->$volume:F

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    iget v1, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->$volume:F

    invoke-static {v0, v1}, Lcom/discord/stores/StoreStreamRtcConnection;->access$setStreamVolume$p(Lcom/discord/stores/StoreStreamRtcConnection;F)V

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {v0}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getStreamOwner$p(Lcom/discord/stores/StoreStreamRtcConnection;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {v2}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getRtcConnection$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/rtcconnection/RtcConnection;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->$volume:F

    invoke-virtual {v2, v0, v1, v3}, Lcom/discord/rtcconnection/RtcConnection;->o(JF)V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/discord/stores/StoreStreamRtcConnection;->access$setDirty$p(Lcom/discord/stores/StoreStreamRtcConnection;Z)V

    return-void
.end method
