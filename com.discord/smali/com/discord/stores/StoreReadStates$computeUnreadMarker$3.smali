.class public final Lcom/discord/stores/StoreReadStates$computeUnreadMarker$3;
.super Lx/m/c/k;
.source "StoreReadStates.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreReadStates;->computeUnreadMarker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lrx/Observable<",
        "Lcom/discord/models/application/Unread$Marker;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreReadStates$computeUnreadMarker$3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$3;

    invoke-direct {v0}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$3;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$3;->INSTANCE:Lcom/discord/stores/StoreReadStates$computeUnreadMarker$3;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$3;->invoke(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(J)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/application/Unread$Marker;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;->INSTANCE:Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;->invoke(J)Lrx/Observable;

    move-result-object p1

    const-string p2, "getMarker(channelId)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
