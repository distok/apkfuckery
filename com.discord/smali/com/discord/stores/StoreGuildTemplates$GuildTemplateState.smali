.class public abstract Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;
.super Ljava/lang/Object;
.source "StoreGuildTemplates.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreGuildTemplates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "GuildTemplateState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;,
        Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;,
        Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;,
        Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Invalid;,
        Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;-><init>()V

    return-void
.end method
