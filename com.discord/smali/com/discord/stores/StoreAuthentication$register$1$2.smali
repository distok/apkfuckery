.class public final Lcom/discord/stores/StoreAuthentication$register$1$2;
.super Ljava/lang/Object;
.source "StoreAuthentication.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAuthentication$register$1;->call(Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreAuthentication$AuthRequestParams;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/models/domain/ModelUser$Token;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $fingerprint:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreAuthentication$register$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAuthentication$register$1;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAuthentication$register$1$2;->this$0:Lcom/discord/stores/StoreAuthentication$register$1;

    iput-object p2, p0, Lcom/discord/stores/StoreAuthentication$register$1$2;->$fingerprint:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreAuthentication$AuthRequestParams;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreAuthentication$register$1$2;->call(Lcom/discord/stores/StoreAuthentication$AuthRequestParams;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/stores/StoreAuthentication$AuthRequestParams;)Lrx/Observable;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreAuthentication$AuthRequestParams;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/models/domain/ModelUser$Token;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/stores/StoreAuthentication$AuthRequestParams;->getInviteCode()Lcom/discord/stores/StoreInviteSettings$InviteCode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreInviteSettings$InviteCode;->getInviteCode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v7, v0

    const/4 v0, 0x1

    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getNux()Lcom/discord/stores/StoreNux;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreNux;->setPostRegister(Z)V

    :cond_3
    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    new-instance v11, Lcom/discord/restapi/RestAPIParams$AuthRegister;

    iget-object v2, p0, Lcom/discord/stores/StoreAuthentication$register$1$2;->$fingerprint:Ljava/lang/String;

    iget-object v1, p0, Lcom/discord/stores/StoreAuthentication$register$1$2;->this$0:Lcom/discord/stores/StoreAuthentication$register$1;

    iget-object v3, v1, Lcom/discord/stores/StoreAuthentication$register$1;->$username:Ljava/lang/String;

    iget-object v4, v1, Lcom/discord/stores/StoreAuthentication$register$1;->$email:Ljava/lang/String;

    iget-object v5, v1, Lcom/discord/stores/StoreAuthentication$register$1;->$password:Ljava/lang/String;

    iget-object v6, v1, Lcom/discord/stores/StoreAuthentication$register$1;->$captchaKey:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/discord/stores/StoreAuthentication$AuthRequestParams;->getGuildTemplateCode()Ljava/lang/String;

    move-result-object v8

    iget-object p1, p0, Lcom/discord/stores/StoreAuthentication$register$1$2;->this$0:Lcom/discord/stores/StoreAuthentication$register$1;

    iget-boolean v9, p1, Lcom/discord/stores/StoreAuthentication$register$1;->$consent:Z

    iget-object v10, p1, Lcom/discord/stores/StoreAuthentication$register$1;->$dateOfBirth:Ljava/lang/String;

    move-object v1, v11

    invoke-direct/range {v1 .. v10}, Lcom/discord/restapi/RestAPIParams$AuthRegister;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v0, v11}, Lcom/discord/utilities/rest/RestAPI;->postAuthRegister(Lcom/discord/restapi/RestAPIParams$AuthRegister;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
