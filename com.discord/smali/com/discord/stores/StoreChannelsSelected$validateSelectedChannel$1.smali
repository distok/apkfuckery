.class public final Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;
.super Lx/m/c/k;
.source "StoreChannelsSelected.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreChannelsSelected;->validateSelectedChannel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelsStore:Lcom/discord/stores/StoreChannels;

.field public final synthetic $permissionsStore:Lcom/discord/stores/StorePermissions;

.field public final synthetic this$0:Lcom/discord/stores/StoreChannelsSelected;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->this$0:Lcom/discord/stores/StoreChannelsSelected;

    iput-object p2, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->$channelsStore:Lcom/discord/stores/StoreChannels;

    iput-object p3, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->$permissionsStore:Lcom/discord/stores/StorePermissions;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;
    .locals 7

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->this$0:Lcom/discord/stores/StoreChannelsSelected;

    invoke-static {v0}, Lcom/discord/stores/StoreChannelsSelected;->access$getStream$p(Lcom/discord/stores/StoreChannelsSelected;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getGuildSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildSelected;->getSelectedGuildId()J

    move-result-wide v4

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->this$0:Lcom/discord/stores/StoreChannelsSelected;

    invoke-static {v0}, Lcom/discord/stores/StoreChannelsSelected;->access$getSelectedChannelIds$p(Lcom/discord/stores/StoreChannelsSelected;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/Long;

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->$channelsStore:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannels;->getAllChannels()Ljava/util/Map;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->$permissionsStore:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0}, Lcom/discord/stores/StorePermissions;->getPermissionsByChannel()Ljava/util/Map;

    move-result-object v6

    iget-object v1, p0, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->this$0:Lcom/discord/stores/StoreChannelsSelected;

    invoke-static/range {v1 .. v6}, Lcom/discord/stores/StoreChannelsSelected;->access$resolveSelectedChannel(Lcom/discord/stores/StoreChannelsSelected;Ljava/lang/Long;Ljava/util/Map;JLjava/util/Map;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;->invoke()Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    move-result-object v0

    return-object v0
.end method
