.class public final Lcom/discord/stores/NotificationTextUtils;
.super Ljava/lang/Object;
.source "NotificationTextUtils.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/stores/NotificationTextUtils;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/NotificationTextUtils;

    invoke-direct {v0}, Lcom/discord/stores/NotificationTextUtils;-><init>()V

    sput-object v0, Lcom/discord/stores/NotificationTextUtils;->INSTANCE:Lcom/discord/stores/NotificationTextUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final isNotificationAllowed(Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/models/domain/ModelUser$Me;Ljava/util/Collection;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;JJ)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            "Lcom/discord/models/domain/ModelUser$Me;",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelGuild;",
            "JJ)Z"
        }
    .end annotation

    move-object v7, p0

    move-object v0, p1

    move-wide v1, p6

    move-wide/from16 v3, p8

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->isMobilePush()Z

    move-result v5

    const/4 v6, 0x0

    if-nez v5, :cond_0

    return v6

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->isMuted()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {p1, v3, v4}, Lcom/discord/models/domain/ModelNotificationSettings;->getChannelOverride(J)Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;

    move-result-object v5

    const/4 v8, 0x1

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->isMuted()Z

    move-result v5

    if-eq v5, v8, :cond_9

    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/discord/models/domain/ModelNotificationSettings;->getChannelOverride(J)Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->isMuted()Z

    move-result v5

    if-ne v5, v8, :cond_2

    goto :goto_3

    :cond_2
    invoke-direct {p0, p1, v1, v2}, Lcom/discord/stores/NotificationTextUtils;->messageNotifications(Lcom/discord/models/domain/ModelNotificationSettings;J)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, v3, v4}, Lcom/discord/stores/NotificationTextUtils;->messageNotifications(Lcom/discord/models/domain/ModelNotificationSettings;J)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->getMessageNotifications()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v3

    sget v4, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_UNSET:I

    if-eq v3, v4, :cond_5

    const/4 v6, 0x1

    :cond_5
    if-eqz v6, :cond_6

    goto :goto_1

    :cond_6
    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_7

    goto :goto_2

    :cond_7
    if-eqz p5, :cond_8

    invoke-virtual {p5}, Lcom/discord/models/domain/ModelGuild;->getDefaultMessageNotifications()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2

    :cond_8
    move-object v1, v2

    :goto_2
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->isSuppressEveryone()Z

    move-result v5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->isSuppressRoles()Z

    move-result v6

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/NotificationTextUtils;->shouldNotifyForLevel(Ljava/lang/Integer;Lcom/discord/models/domain/ModelUser$Me;Ljava/util/Collection;Lcom/discord/models/domain/ModelMessage;ZZ)Z

    move-result v0

    return v0

    :cond_9
    :goto_3
    return v6
.end method

.method private final messageNotifications(Lcom/discord/models/domain/ModelNotificationSettings;J)Ljava/lang/Integer;
    .locals 1

    invoke-virtual {p1, p2, p3}, Lcom/discord/models/domain/ModelNotificationSettings;->getChannelOverride(J)Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->getMessageNotifications()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p3

    sget v0, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_UNSET:I

    if-ne p3, v0, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-nez p3, :cond_1

    move-object p2, p1

    :cond_1
    return-object p2
.end method

.method private final shouldNotifyForLevel(Ljava/lang/Integer;Lcom/discord/models/domain/ModelUser$Me;Ljava/util/Collection;Lcom/discord/models/domain/ModelMessage;ZZ)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelUser$Me;",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/discord/models/domain/ModelMessage;",
            "ZZ)Z"
        }
    .end annotation

    sget v0, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_ALL:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v0, :cond_1

    goto/16 :goto_5

    :cond_1
    :goto_0
    sget v0, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_NOTHING:I

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v0, :cond_4

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_4
    :goto_1
    sget v0, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_MENTIONS:I

    if-nez p1, :cond_5

    goto/16 :goto_5

    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v0, :cond_e

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelMessage;->isMentionEveryone()Z

    move-result p1

    if-eqz p1, :cond_6

    if-eqz p5, :cond_e

    :cond_6
    invoke-virtual {p4}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object p1

    const-string p5, "message.mentions"

    invoke-static {p1, p5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p5

    if-eqz p5, :cond_8

    :cond_7
    const/4 p1, 0x0

    goto :goto_3

    :cond_8
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_9
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    if-eqz p5, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/discord/models/domain/ModelUser;

    const-string v0, "it"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    cmp-long p5, v3, v5

    if-nez p5, :cond_a

    const/4 p5, 0x1

    goto :goto_2

    :cond_a
    const/4 p5, 0x0

    :goto_2
    if-eqz p5, :cond_9

    const/4 p1, 0x1

    :goto_3
    if-nez p1, :cond_e

    if-nez p6, :cond_3

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelMessage;->getMentionRoles()Ljava/util/List;

    move-result-object p1

    const-string p2, "message.mentionRoles"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_c

    :cond_b
    const/4 p1, 0x0

    goto :goto_4

    :cond_c
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_d
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    invoke-interface {p3, p2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_d

    const/4 p1, 0x1

    :goto_4
    if-eqz p1, :cond_3

    :cond_e
    :goto_5
    return v1
.end method

.method public static synthetic shouldNotifyForLevel$default(Lcom/discord/stores/NotificationTextUtils;Ljava/lang/Integer;Lcom/discord/models/domain/ModelUser$Me;Ljava/util/Collection;Lcom/discord/models/domain/ModelMessage;ZZILjava/lang/Object;)Z
    .locals 9

    and-int/lit8 v0, p7, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v7, 0x0

    goto :goto_0

    :cond_0
    move v7, p5

    :goto_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_1

    const/4 v8, 0x0

    goto :goto_1

    :cond_1
    move v8, p6

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v2 .. v8}, Lcom/discord/stores/NotificationTextUtils;->shouldNotifyForLevel(Ljava/lang/Integer;Lcom/discord/models/domain/ModelUser$Me;Ljava/util/Collection;Lcom/discord/models/domain/ModelMessage;ZZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final shouldNotifyInAppPopup(Lcom/discord/models/domain/ModelUser$Me;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Lcom/discord/models/domain/ModelGuild;Ljava/util/Map;Ljava/util/Map;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser$Me;",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;)Z"
        }
    .end annotation

    move-object/from16 v0, p4

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    const-string v3, "msg"

    move-object/from16 v8, p2

    invoke-static {v8, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "channel"

    move-object/from16 v4, p3

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "blockedRelationships"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "guildMembers"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "guildSettings"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->isManaged()Z

    move-result v3

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    return v5

    :cond_0
    if-eqz p1, :cond_9

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    sget-object v6, Lcom/discord/models/domain/ModelUser;->EMPTY:Lcom/discord/models/domain/ModelUser;

    invoke-static {v3, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    const/4 v7, 0x0

    if-nez v6, :cond_1

    goto :goto_0

    :cond_1
    move-object v3, v7

    :goto_0
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v9

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v11

    cmp-long v6, v9, v11

    if-nez v6, :cond_2

    return v5

    :cond_2
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v5

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    return v5

    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuildMember$Computed;

    goto :goto_1

    :cond_5
    move-object v0, v7

    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    goto :goto_2

    :cond_6
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    :goto_2
    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/discord/models/domain/ModelNotificationSettings;

    if-eqz v5, :cond_7

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v10

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v12

    move-object v4, p0

    move-object v6, p1

    move-object v7, v0

    move-object/from16 v8, p2

    move-object/from16 v9, p5

    invoke-direct/range {v4 .. v13}, Lcom/discord/stores/NotificationTextUtils;->isNotificationAllowed(Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/models/domain/ModelUser$Me;Ljava/util/Collection;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;JJ)Z

    move-result v0

    goto :goto_4

    :cond_7
    if-eqz p5, :cond_8

    invoke-virtual/range {p5 .. p5}, Lcom/discord/models/domain/ModelGuild;->getDefaultMessageNotifications()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v5, v1

    goto :goto_3

    :cond_8
    move-object v5, v7

    :goto_3
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x30

    const/4 v12, 0x0

    move-object v4, p0

    move-object v6, p1

    move-object v7, v0

    move-object/from16 v8, p2

    invoke-static/range {v4 .. v12}, Lcom/discord/stores/NotificationTextUtils;->shouldNotifyForLevel$default(Lcom/discord/stores/NotificationTextUtils;Ljava/lang/Integer;Lcom/discord/models/domain/ModelUser$Me;Ljava/util/Collection;Lcom/discord/models/domain/ModelMessage;ZZILjava/lang/Object;)Z

    move-result v0

    :goto_4
    return v0

    :cond_9
    return v5
.end method
