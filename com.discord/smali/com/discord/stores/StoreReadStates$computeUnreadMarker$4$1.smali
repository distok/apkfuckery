.class public final Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$1;
.super Ljava/lang/Object;
.source "StoreReadStates.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4;->call(Lcom/discord/models/application/Unread$Marker;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/domain/ModelMessage;",
        ">;",
        "Lcom/discord/models/application/Unread;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $marker:Lcom/discord/models/application/Unread$Marker;


# direct methods
.method public constructor <init>(Lcom/discord/models/application/Unread$Marker;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$1;->$marker:Lcom/discord/models/application/Unread$Marker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/List;)Lcom/discord/models/application/Unread;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;)",
            "Lcom/discord/models/application/Unread;"
        }
    .end annotation

    new-instance v0, Lcom/discord/models/application/Unread;

    iget-object v1, p0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$1;->$marker:Lcom/discord/models/application/Unread$Marker;

    const-string v2, "messages"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/discord/models/application/Unread;-><init>(Lcom/discord/models/application/Unread$Marker;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$1;->call(Ljava/util/List;)Lcom/discord/models/application/Unread;

    move-result-object p1

    return-object p1
.end method
