.class public final synthetic Lcom/discord/stores/StoreVoiceChannelSelected$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 8

    invoke-static {}, Lcom/discord/utilities/voice/VoiceChannelJoinability;->values()[Lcom/discord/utilities/voice/VoiceChannelJoinability;

    const/4 v0, 0x5

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/stores/StoreVoiceChannelSelected$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CAN_JOIN:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v4, Lcom/discord/utilities/voice/VoiceChannelJoinability;->PERMISSIONS_MISSING:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    const/4 v4, 0x2

    aput v4, v1, v3

    sget-object v5, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CHANNEL_FULL:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    const/4 v5, 0x3

    aput v5, v1, v5

    sget-object v6, Lcom/discord/utilities/voice/VoiceChannelJoinability;->GUILD_VIDEO_AT_CAPACITY:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    const/4 v6, 0x4

    aput v6, v1, v4

    sget-object v7, Lcom/discord/utilities/voice/VoiceChannelJoinability;->CHANNEL_DOES_NOT_EXIST:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    aput v0, v1, v6

    invoke-static {}, Lcom/discord/utilities/voice/VoiceChannelJoinability;->values()[Lcom/discord/utilities/voice/VoiceChannelJoinability;

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/stores/StoreVoiceChannelSelected$WhenMappings;->$EnumSwitchMapping$1:[I

    aput v3, v1, v3

    aput v4, v1, v4

    aput v5, v1, v5

    aput v6, v1, v6

    aput v0, v1, v2

    return-void
.end method
