.class public final Lcom/discord/stores/StoreExperiments$setOverride$1;
.super Lx/m/c/k;
.source "StoreExperiments.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreExperiments;->setOverride(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $bucket:I

.field public final synthetic $experimentName:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreExperiments;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreExperiments;ILjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->this$0:Lcom/discord/stores/StoreExperiments;

    iput p2, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->$bucket:I

    iput-object p3, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->$experimentName:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreExperiments$setOverride$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->this$0:Lcom/discord/stores/StoreExperiments;

    invoke-static {v0}, Lcom/discord/stores/StoreExperiments;->access$getExperimentOverrides$p(Lcom/discord/stores/StoreExperiments;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->$experimentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->$bucket:I

    if-ne v0, v1, :cond_1

    return-void

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->this$0:Lcom/discord/stores/StoreExperiments;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->$experimentName:Ljava/lang/String;

    iget v2, p0, Lcom/discord/stores/StoreExperiments$setOverride$1;->$bucket:I

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->access$handleSetOverride(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;I)V

    return-void
.end method
