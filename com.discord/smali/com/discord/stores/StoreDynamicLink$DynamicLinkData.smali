.class public final Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;
.super Ljava/lang/Object;
.source "StoreDynamicLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreDynamicLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DynamicLinkData"
.end annotation


# instance fields
.field private final authToken:Ljava/lang/String;

.field private final fingerprint:Ljava/lang/String;

.field private final guildTemplateCode:Ljava/lang/String;

.field private final inviteCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAuthToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getFingerprint()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildTemplateCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    return-object v0
.end method

.method public final getInviteCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "DynamicLinkData(fingerprint="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->fingerprint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", inviteCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->inviteCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", guildTemplateCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->guildTemplateCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", authToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;->authToken:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
