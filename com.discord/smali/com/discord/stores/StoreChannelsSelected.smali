.class public final Lcom/discord/stores/StoreChannelsSelected;
.super Lcom/discord/stores/StoreV2;
.source "StoreChannelsSelected.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;,
        Lcom/discord/stores/StoreChannelsSelected$Companion;
    }
.end annotation


# static fields
.field private static final CACHE_KEY_SELECTED_CHANNEL_IDS:Ljava/lang/String; = "CACHE_KEY_SELECTED_CHANNEL_IDS"

.field public static final Companion:Lcom/discord/stores/StoreChannelsSelected$Companion;

.field public static final ID_UNAVAILABLE:J = -0x1L

.field public static final ID_UNSELECTED:J


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final frecency:Lcom/discord/widgets/user/search/ChannelFrecencyTracker;

.field private final frecencyCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Lcom/discord/widgets/user/search/ChannelFrecencyTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private previouslySelectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

.field private selectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

.field private final selectedChannelIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedChannelIdsCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final stream:Lcom/discord/stores/StoreStream;

.field private validateSelectedChannelSubscription:Lrx/Subscription;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreChannelsSelected$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreChannelsSelected$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreChannelsSelected;->Companion:Lcom/discord/stores/StoreChannelsSelected$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->stream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreChannelsSelected;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreChannelsSelected;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    new-instance p1, Lcom/discord/utilities/persister/Persister;

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    const-string p3, "CACHE_KEY_SELECTED_CHANNEL_IDS"

    invoke-direct {p1, p3, p2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIdsCache:Lcom/discord/utilities/persister/Persister;

    sget-object p1, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;->INSTANCE:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->previouslySelectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    new-instance p1, Lcom/discord/utilities/persister/Persister;

    new-instance p2, Lcom/discord/widgets/user/search/ChannelFrecencyTracker;

    invoke-direct {p2}, Lcom/discord/widgets/user/search/ChannelFrecencyTracker;-><init>()V

    const-string p3, "CHANNEL_HISTORY_V2"

    invoke-direct {p1, p3, p2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->frecencyCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {p1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/user/search/ChannelFrecencyTracker;

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->frecency:Lcom/discord/widgets/user/search/ChannelFrecencyTracker;

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreChannelsSelected;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelsSelected;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getPreviouslySelectedChannel$p(Lcom/discord/stores/StoreChannelsSelected;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelsSelected;->previouslySelectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    return-object p0
.end method

.method public static final synthetic access$getSelectedChannel$p(Lcom/discord/stores/StoreChannelsSelected;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    return-object p0
.end method

.method public static final synthetic access$getSelectedChannelIds$p(Lcom/discord/stores/StoreChannelsSelected;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getStream$p(Lcom/discord/stores/StoreChannelsSelected;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelsSelected;->stream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$getValidateSelectedChannelSubscription$p(Lcom/discord/stores/StoreChannelsSelected;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelsSelected;->validateSelectedChannelSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$onSelectedChannelResolved(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreChannelsSelected;->onSelectedChannelResolved(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)V

    return-void
.end method

.method public static final synthetic access$resolveSelectedChannel(Lcom/discord/stores/StoreChannelsSelected;Ljava/lang/Long;Ljava/util/Map;JLjava/util/Map;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/stores/StoreChannelsSelected;->resolveSelectedChannel(Ljava/lang/Long;Ljava/util/Map;JLjava/util/Map;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setPreviouslySelectedChannel$p(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->previouslySelectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    return-void
.end method

.method public static final synthetic access$setSelectedChannel$p(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    return-void
.end method

.method public static final synthetic access$setValidateSelectedChannelSubscription$p(Lcom/discord/stores/StoreChannelsSelected;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->validateSelectedChannelSubscription:Lrx/Subscription;

    return-void
.end method

.method private final getChannelTarget(Ljava/lang/Long;Ljava/util/Map;JLjava/util/Map;)Lcom/discord/models/domain/ModelChannel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/models/domain/ModelChannel;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object p2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long p2, v1, p3

    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isTextChannel()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-static {p1, p5}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;)Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_2

    move-object v0, p1

    :cond_2
    return-object v0
.end method

.method private final getFirstAvailableChannel(Ljava/util/Map;JLjava/util/Map;)Lcom/discord/models/domain/ModelChannel;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/models/domain/ModelChannel;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v5, v3, p2

    if-nez v5, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isGuildTextyChannel()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2, p4}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/discord/models/domain/ModelChannel;->getSortByNameAndType()Ljava/util/Comparator;

    move-result-object p1

    const-string p2, "ModelChannel.getSortByNameAndType()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    return-object p1
.end method

.method private final loadFromCache()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    iget-object v1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIdsCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method private final onSelectedChannelResolved(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    iput-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->previouslySelectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    iput-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    instance-of v0, p1, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    check-cast p1, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;

    invoke-virtual {p1}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "resolvedSelectedChannel.channel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/discord/stores/StoreChannelsSelected;->frecency:Lcom/discord/widgets/user/search/ChannelFrecencyTracker;

    invoke-virtual {p1}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-wide/16 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/frecency/FrecencyTracker;->track$default(Lcom/discord/utilities/frecency/FrecencyTracker;Ljava/lang/Object;JILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getAnalytics$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreAnalytics;->trackChannelOpened(J)V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final resolveSelectedChannel(Ljava/lang/Long;Ljava/util/Map;JLjava/util/Map;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Lcom/discord/stores/StoreChannelsSelected;->getChannelTarget(Ljava/lang/Long;Ljava/util/Map;JLjava/util/Map;)Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance p2, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;

    invoke-direct {p2, p1}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long p1, p3, v0

    if-nez p1, :cond_1

    sget-object p2, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;->INSTANCE:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/discord/stores/StoreChannelsSelected;->getFirstAvailableChannel(Ljava/util/Map;JLjava/util/Map;)Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    if-nez p1, :cond_2

    sget-object p2, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unavailable;->INSTANCE:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unavailable;

    goto :goto_0

    :cond_2
    new-instance p2, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;

    invoke-direct {p2, p1}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    :goto_0
    return-object p2
.end method

.method private final validateSelectedChannel()V
    .locals 11
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->validateSelectedChannelSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getGuildSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreChannelsSelected;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreChannelsSelected;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getPermissions$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePermissions;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/stores/StoreChannelsSelected;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v4, 0x3

    new-array v4, v4, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;

    invoke-direct {v8, p0, v1, v2}, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$1;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)V

    const/16 v9, 0xe

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    const-string v0, "observationDeck\n        \u2026  .distinctUntilChanged()"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v2, Lcom/discord/stores/StoreChannelsSelected;

    const/4 v3, 0x0

    new-instance v4, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$2;

    invoke-direct {v4, p0}, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$2;-><init>(Lcom/discord/stores/StoreChannelsSelected;)V

    const/4 v5, 0x0

    new-instance v7, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$3;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreChannelsSelected$validateSelectedChannel$3;-><init>(Lcom/discord/stores/StoreChannelsSelected;)V

    const/16 v8, 0x1a

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getFrecency()Lcom/discord/widgets/user/search/ChannelFrecencyTracker;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->frecency:Lcom/discord/widgets/user/search/ChannelFrecencyTracker;

    return-object v0
.end method

.method public final getId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannel:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuilds()Ljava/util/List;

    move-result-object p1

    const-string v0, "payload.guilds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Long;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->hashSetOf([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuild;

    const-string v3, "guild"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-static {v2}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {p1, v1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreChannelsSelected;->validateSelectedChannel()V

    return-void
.end method

.method public final init()V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/stores/StoreChannelsSelected;->loadFromCache()V

    invoke-direct {p0}, Lcom/discord/stores/StoreChannelsSelected;->validateSelectedChannel()V

    return-void
.end method

.method public final observeId()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreChannelsSelected$observeId$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreChannelsSelected$observeId$1;-><init>(Lcom/discord/stores/StoreChannelsSelected;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observePreviousId()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreChannelsSelected$observePreviousId$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreChannelsSelected$observePreviousId$1;-><init>(Lcom/discord/stores/StoreChannelsSelected;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeSelectedChannel()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreChannelsSelected$observeSelectedChannel$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreChannelsSelected$observeSelectedChannel$1;-><init>(Lcom/discord/stores/StoreChannelsSelected;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public snapshotData()V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->frecencyCache:Lcom/discord/utilities/persister/Persister;

    iget-object v1, p0, Lcom/discord/stores/StoreChannelsSelected;->frecency:Lcom/discord/widgets/user/search/ChannelFrecencyTracker;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/discord/utilities/persister/Persister;->set$default(Lcom/discord/utilities/persister/Persister;Ljava/lang/Object;ZILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIdsCache:Lcom/discord/utilities/persister/Persister;

    iget-object v1, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    return-void
.end method

.method public final trySelectChannel(JJ)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, v0, p3

    if-nez v2, :cond_1

    return-void

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/discord/stores/StoreChannelsSelected;->selectedChannelIds:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/discord/stores/StoreChannelsSelected;->validateSelectedChannel()V

    return-void
.end method
