.class public final Lcom/discord/stores/StoreGifting;
.super Ljava/lang/Object;
.source "StoreGifting.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGifting$GiftState;,
        Lcom/discord/stores/StoreGifting$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreGifting$Companion;


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final knownGifts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreGifting$GiftState;",
            ">;"
        }
    .end annotation
.end field

.field private final knownGiftsSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreGifting$GiftState;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreGifting$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreGifting$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreGifting;->Companion:Lcom/discord/stores/StoreGifting$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifting;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGifting;->knownGiftsSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$fetchGift(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifting;->fetchGift(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$removeGiftCode(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGifting;->removeGiftCode(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setGifts(Lcom/discord/stores/StoreGifting;Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGifting;->setGifts(Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V

    return-void
.end method

.method private final fetchGift(Ljava/lang/String;)V
    .locals 13
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.stores.StoreGifting.GiftState.Resolved"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGift;->isComplete()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreGifting$GiftState$Loading;->INSTANCE:Lcom/discord/stores/StoreGifting$GiftState$Loading;

    invoke-direct {p0, p1, v0}, Lcom/discord/stores/StoreGifting;->setGifts(Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, v1}, Lcom/discord/utilities/rest/RestAPI;->resolveGiftCode(Ljava/lang/String;ZZ)Lrx/Observable;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreGifting;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v10, Lcom/discord/stores/StoreGifting$fetchGift$1;

    invoke-direct {v10, p0, p1}, Lcom/discord/stores/StoreGifting$fetchGift$1;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    const/4 v9, 0x0

    new-instance v8, Lcom/discord/stores/StoreGifting$fetchGift$2;

    invoke-direct {v8, p0, p1}, Lcom/discord/stores/StoreGifting$fetchGift$2;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static synthetic generateGiftCode$default(Lcom/discord/stores/StoreGifting;JLjava/lang/Long;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/StoreGifting;->generateGiftCode(JLjava/lang/Long;)V

    return-void
.end method

.method private final removeGiftCode(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/stores/StoreGifting;->knownGiftsSubject:Lrx/subjects/BehaviorSubject;

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final setGifts(Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/stores/StoreGifting;->knownGiftsSubject:Lrx/subjects/BehaviorSubject;

    new-instance p2, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-direct {p2, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final acceptGift(Lcom/discord/models/domain/ModelGift;)V
    .locals 2

    const-string v0, "gift"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifting$acceptGift$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifting$acceptGift$1;-><init>(Lcom/discord/stores/StoreGifting;Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final fetchMyGiftsForSku(JLjava/lang/Long;)V
    .locals 11

    sget-object v0, Lcom/discord/stores/StoreGifting;->Companion:Lcom/discord/stores/StoreGifting$Companion;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreGifting$Companion;->makeComboId(JLjava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/discord/stores/StoreGifting;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v2, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$1;

    invoke-direct {v2, p0, v0}, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$1;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/discord/utilities/rest/RestAPI;->resolveSkuIdGift(JLjava/lang/Long;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, p3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/stores/StoreGifting;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$2;

    invoke-direct {v6, p0, v0}, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$2;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;

    invoke-direct {v8, p0, v0}, Lcom/discord/stores/StoreGifting$fetchMyGiftsForSku$3;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final generateGiftCode(JLjava/lang/Long;)V
    .locals 11

    sget-object v0, Lcom/discord/stores/StoreGifting;->Companion:Lcom/discord/stores/StoreGifting$Companion;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreGifting$Companion;->makeComboId(JLjava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/discord/stores/StoreGifting$GiftState$Loading;

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/discord/stores/StoreGifting;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v2, Lcom/discord/stores/StoreGifting$generateGiftCode$1;

    invoke-direct {v2, p0, v0}, Lcom/discord/stores/StoreGifting$generateGiftCode$1;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    new-instance v2, Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;

    invoke-direct {v2, p1, p2, p3}, Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;-><init>(JLjava/lang/Long;)V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/rest/RestAPI;->generateGiftCode(Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, p3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/stores/StoreGifting;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/stores/StoreGifting$generateGiftCode$2;

    invoke-direct {v6, p0, v0}, Lcom/discord/stores/StoreGifting$generateGiftCode$2;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreGifting$generateGiftCode$3;

    invoke-direct {v8, p0, v0}, Lcom/discord/stores/StoreGifting$generateGiftCode$3;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getDispatcher()Lcom/discord/stores/Dispatcher;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object v0
.end method

.method public final getKnownGifts()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreGifting$GiftState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    return-object v0
.end method

.method public final getMyResolvedGifts(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGiftsSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreGifting$getMyResolvedGifts$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreGifting$getMyResolvedGifts$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "knownGiftsSubject\n      \u2026              }\n        }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final handlePreLogout()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public final requestGift(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGifting$GiftState;",
            ">;"
        }
    .end annotation

    const-string v0, "giftCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifting$requestGift$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifting$requestGift$1;-><init>(Lcom/discord/stores/StoreGifting;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGiftsSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreGifting$requestGift$2;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreGifting$requestGift$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "knownGiftsSubject\n      \u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final revokeGiftCode(Lcom/discord/models/domain/ModelGift;)V
    .locals 13

    const-string v0, "gift"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGift;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->knownGifts:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGift;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGifting$GiftState$Revoking;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGifting;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGifting$revokeGiftCode$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifting$revokeGiftCode$1;-><init>(Lcom/discord/stores/StoreGifting;Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGift;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI;->revokeGiftCode(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreGifting;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreGifting$revokeGiftCode$2;

    invoke-direct {v8, p0, p1}, Lcom/discord/stores/StoreGifting$revokeGiftCode$2;-><init>(Lcom/discord/stores/StoreGifting;Lcom/discord/models/domain/ModelGift;)V

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/stores/StoreGifting$revokeGiftCode$3;

    invoke-direct {v10, p0, p1}, Lcom/discord/stores/StoreGifting$revokeGiftCode$3;-><init>(Lcom/discord/stores/StoreGifting;Lcom/discord/models/domain/ModelGift;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
