.class public final Lcom/discord/stores/StoreStream$deferredInit$1;
.super Lx/m/c/k;
.source "StoreStream.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStream;->deferredInit(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/app/Application;

.field public final synthetic this$0:Lcom/discord/stores/StoreStream;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Landroid/app/Application;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreStream$deferredInit$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 7

    new-instance v6, Lcom/discord/utilities/time/TimeElapsed;

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-static {v0}, Lcom/discord/stores/StoreStream;->access$getClock$p(Lcom/discord/stores/StoreStream;)Lcom/discord/utilities/time/Clock;

    move-result-object v1

    const-wide/16 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/time/TimeElapsed;-><init>(Lcom/discord/utilities/time/Clock;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    new-instance v0, Lcom/discord/utilities/networking/NetworkMonitor;

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-direct {v0, v1, v2}, Lcom/discord/utilities/networking/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/discord/utilities/logging/Logger;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMediaEngine$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreMediaEngine;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getExperiments$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreExperiments;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getAccessibility$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAccessibility;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreAccessibility;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getGatewaySocket$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGatewayConnection;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2, v0}, Lcom/discord/stores/StoreGatewayConnection;->init(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getNavigation$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNavigation;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreNavigation;->init(Landroid/app/Application;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getNux$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNux;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannels;->init()V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreUser;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getGuilds$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreGuilds;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getPermissions$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePermissions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StorePermissions;->init()V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getGuildsSorted$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreGuildsSorted;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getGuildsNsfw$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreGuildsNsfw;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getGuildSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreGuildSelected;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChannelsSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelsSelected;->init()V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMessages$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessages;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreMessages;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMessagesLoader$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreMessagesLoader;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMessageAck$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageAck;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreMessageAck;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMessagesMostRecent$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessagesMostRecent;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getNotifications$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNotifications;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreNotifications;->init(Landroid/app/Application;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getVideoSupport$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVideoSupport;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreVideoSupport;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2, v0}, Lcom/discord/stores/StoreRtcConnection;->init(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getReadStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreReadStates;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreReadStates;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getVoiceChannelSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getVoiceSpeaking$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceSpeaking;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreVoiceSpeaking;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getVoiceParticipants$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceParticipants;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreVoiceParticipants;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getConnectivity$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreConnectivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreConnectivity;->init(Lcom/discord/utilities/networking/NetworkMonitor;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getClientVersion$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreClientVersion;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreClientVersion;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreMediaSettings;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getAnalytics$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAnalytics;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreAnalytics;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getStoreChannelCategories$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelCategories;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelCategories;->init()V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-static {v1}, Lcom/discord/stores/StoreStream;->access$getAudioManager$p(Lcom/discord/stores/StoreStream;)Lcom/discord/stores/StoreAudioManager;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreAudioManager;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getGuildSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreUserGuildSettings;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getNotices$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNotices;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreNotices;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getUserConnections$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserConnections;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreUserConnections;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChangeLogStore$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChangeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreChangeLog;->init(Landroid/app/Application;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getReviewRequestStore$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreReviewRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreReviewRequest;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getPresences$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserPresence;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getSpotify$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreSpotify;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreSpotify;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-static {v1}, Lcom/discord/stores/StoreStream;->access$getStreamRtcConnection$p(Lcom/discord/stores/StoreStream;)Lcom/discord/stores/StoreStreamRtcConnection;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreStreamRtcConnection;->init(Lcom/discord/utilities/networking/NetworkMonitor;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getGuildsSorted$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreGuildsSorted;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getExpandedGuildFolders$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreExpandedGuildFolders;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreExpandedGuildFolders;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getAudioDevices$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAudioDevices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices;->init()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getUserRelationships$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships;->init()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getMaskedLinks$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMaskedLinks;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreMaskedLinks;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getRtcRegion$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcRegion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreRtcRegion;->init()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getStickers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreStickers;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->init()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getGooglePlayPurchases$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getAuthentication$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAuthentication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreAuthentication;->getPreLogoutSignal$app_productionDiscordExternalRelease()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$1;

    invoke-direct {v2, p0}, Lcom/discord/stores/StoreStream$deferredInit$1$1;-><init>(Lcom/discord/stores/StoreStream$deferredInit$1;)V

    const-string/jumbo v3, "streamPreLogout"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getAuthentication$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAuthentication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreAuthentication;->getAuthedToken$app_productionDiscordExternalRelease()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$2;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$2;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamAuthedToken"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getAuthentication$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAuthentication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreAuthentication;->getFingerPrint$app_productionDiscordExternalRelease()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$3;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$3;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamAuthedFingerprint"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getMessagesLoader$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessagesLoader;->get()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$4;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$4;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamMessagesLoaded"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getChannelsSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelsSelected;->observeId()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$5;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$5;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamChannelSelected"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getVoiceChannelSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedVoiceChannelId()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$6;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$6;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamVoiceChannelSelected"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getVoiceSpeaking$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceSpeaking;

    move-result-object v1

    iget-object v1, v1, Lcom/discord/stores/StoreVoiceSpeaking;->speakingUsersPublisher:Lrx/subjects/Subject;

    const-string/jumbo v2, "voiceSpeaking\n        .speakingUsersPublisher"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$7;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$7;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamUserSpeaking"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection;->getConnectionState()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$8;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$8;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamRtcConnectionStateChanged"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-static {v0}, Lcom/discord/stores/StoreStream;->access$initGatewaySocketListeners(Lcom/discord/stores/StoreStream;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-static {}, Lcom/miguelgaeta/backgrounded/Backgrounded;->get()Lrx/Observable;

    move-result-object v1

    const-string v2, "Backgrounded\n        .get()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$9;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$9;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v3, "streamBackgrounded"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getExperiments$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreExperiments;->isInitialized()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/persister/Persister;->Companion:Lcom/discord/utilities/persister/Persister$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/persister/Persister$Companion;->isPreloaded()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/stores/StoreStream$deferredInit$1$10;->INSTANCE:Lcom/discord/stores/StoreStream$deferredInit$1$10;

    invoke-static {v1, v2, v3}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreStream$deferredInit$1$11;->INSTANCE:Lcom/discord/stores/StoreStream$deferredInit$1$11;

    invoke-virtual {v1, v2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    const-string v2, "Observable\n        .comb\u2026ed)\n          }\n        }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/stores/StoreStream$deferredInit$1$12;

    iget-object v3, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-static {v3}, Lcom/discord/stores/StoreStream;->access$getInitialized$p(Lcom/discord/stores/StoreStream;)Lrx/subjects/BehaviorSubject;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreStream$deferredInit$1$12;-><init>(Lrx/subjects/BehaviorSubject;)V

    const-string/jumbo v3, "streamInit"

    invoke-static {v0, v1, v3, v2}, Lcom/discord/stores/StoreStream;->access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Application stores initialized in: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lcom/discord/utilities/time/TimeElapsed;->getSeconds()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " seconds."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    sget-object v0, Lcom/discord/stores/SlowTtiExperimentManager;->Companion:Lcom/discord/stores/SlowTtiExperimentManager$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$Companion;->getINSTANCE()Lcom/discord/stores/SlowTtiExperimentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getExperiments$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/SlowTtiExperimentManager;->fetchExperiment(Lcom/discord/stores/StoreExperiments;)V

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->Companion:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;->getINSTANCE()Lcom/discord/utilities/voice/VoiceEngineServiceController;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStream$deferredInit$1;->$context:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->init(Landroid/content/Context;)V

    return-void
.end method
