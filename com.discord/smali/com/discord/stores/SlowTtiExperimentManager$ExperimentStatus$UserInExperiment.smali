.class public final Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;
.super Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus;
.source "SlowTtiExperimentManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserInExperiment"
.end annotation


# instance fields
.field private final bucket:I

.field private final delayMs:J

.field private final population:I

.field private final revision:I


# direct methods
.method public constructor <init>(JIII)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    iput p3, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    iput p4, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    iput p5, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;JIIIILjava/lang/Object;)Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-wide p1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    iget p3, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    :cond_1
    move v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    iget p4, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    :cond_2
    move v4, p4

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    iget p5, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    :cond_3
    move v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->copy(JIII)Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    return-wide v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    return v0
.end method

.method public final copy(JIII)Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;
    .locals 7

    new-instance v6, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;

    move-object v0, v6

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;-><init>(JIII)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;

    iget-wide v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    iget-wide v2, p1, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    iget v1, p1, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    iget v1, p1, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    iget p1, p1, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBucket()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    return v0
.end method

.method public final getDelayMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    return-wide v0
.end method

.method public final getPopulation()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    return v0
.end method

.method public final getRevision()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "UserInExperiment(delayMs="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->delayMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", bucket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->bucket:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", population="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->population:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->revision:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
