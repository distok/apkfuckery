.class public final Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;
.super Lx/m/c/k;
.source "StoreEmojiGuild.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreEmojiGuild;->handleGuildEmojisLoaded(JLjava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $emojis:Ljava/util/List;

.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreEmojiGuild;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreEmojiGuild;Ljava/util/List;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->this$0:Lcom/discord/stores/StoreEmojiGuild;

    iput-object p2, p0, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->$emojis:Ljava/util/List;

    iput-wide p3, p0, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->$guildId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->this$0:Lcom/discord/stores/StoreEmojiGuild;

    invoke-static {v0}, Lcom/discord/stores/StoreEmojiGuild;->access$getGuildEmojis$p(Lcom/discord/stores/StoreEmojiGuild;)Ljava/util/HashMap;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->$guildId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->$emojis:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreEmojiGuild$handleGuildEmojisLoaded$1;->this$0:Lcom/discord/stores/StoreEmojiGuild;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/discord/stores/StoreEmojiGuild;->access$setDirty$p(Lcom/discord/stores/StoreEmojiGuild;Z)V

    return-void
.end method
