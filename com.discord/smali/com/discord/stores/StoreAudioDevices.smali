.class public final Lcom/discord/stores/StoreAudioDevices;
.super Lcom/discord/stores/Store;
.source "StoreAudioDevices.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreAudioDevices$OutputDevice;,
        Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;,
        Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;,
        Lcom/discord/stores/StoreAudioDevices$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreAudioDevices$Companion;

.field private static final DEFAULT_AUDIO_DEVICES_STATE:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

.field private static final DEFAULT_OUTPUT_DEVICE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

.field private static final DEFAULT_OUTPUT_STATE:Lcom/discord/utilities/media/AudioOutputState;


# instance fields
.field private audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

.field private final audioDevicesStateSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            ">;"
        }
    .end annotation
.end field

.field private final audioOutputMonitor:Lcom/discord/utilities/media/AudioOutputMonitor;

.field private audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private final selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

.field private final videoUseDetector:Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;

.field private wasMeUsingVideoInChannel:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/discord/stores/StoreAudioDevices$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreAudioDevices$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreAudioDevices;->Companion:Lcom/discord/stores/StoreAudioDevices$Companion;

    sget-object v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    sput-object v0, Lcom/discord/stores/StoreAudioDevices;->DEFAULT_OUTPUT_DEVICE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    new-instance v7, Lcom/discord/utilities/media/AudioOutputState;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/discord/utilities/media/AudioOutputState;-><init>(ZZZZLjava/lang/String;)V

    sput-object v7, Lcom/discord/stores/StoreAudioDevices;->DEFAULT_OUTPUT_STATE:Lcom/discord/utilities/media/AudioOutputState;

    new-instance v1, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    sget-object v3, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    aput-object v3, v2, v4

    invoke-static {v2}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v7, v0, v2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;-><init>(Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;)V

    sput-object v1, Lcom/discord/stores/StoreAudioDevices;->DEFAULT_AUDIO_DEVICES_STATE:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/media/AudioOutputMonitor;Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;Lcom/discord/stores/StoreVoiceChannelSelected;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioOutputMonitor"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "videoUseDetector"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedVoiceChannelStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreAudioDevices;->audioOutputMonitor:Lcom/discord/utilities/media/AudioOutputMonitor;

    iput-object p3, p0, Lcom/discord/stores/StoreAudioDevices;->videoUseDetector:Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;

    iput-object p4, p0, Lcom/discord/stores/StoreAudioDevices;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    sget-object p1, Lcom/discord/stores/StoreAudioDevices;->DEFAULT_AUDIO_DEVICES_STATE:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    iput-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    new-instance p2, Lrx/subjects/SerializedSubject;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    invoke-direct {p2, p1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p2, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesStateSubject:Lrx/subjects/SerializedSubject;

    return-void
.end method

.method public static final synthetic access$autoSelectNonSpeakerOutput(Lcom/discord/stores/StoreAudioDevices;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices;->autoSelectNonSpeakerOutput()V

    return-void
.end method

.method public static final synthetic access$getDEFAULT_AUDIO_DEVICES_STATE$cp()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreAudioDevices;->DEFAULT_AUDIO_DEVICES_STATE:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-object v0
.end method

.method public static final synthetic access$getDEFAULT_OUTPUT_STATE$cp()Lcom/discord/utilities/media/AudioOutputState;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreAudioDevices;->DEFAULT_OUTPUT_STATE:Lcom/discord/utilities/media/AudioOutputState;

    return-object v0
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreAudioDevices;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreAudioDevices;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$updateAudioOutputState(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/utilities/media/AudioOutputState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAudioDevices;->updateAudioOutputState(Lcom/discord/utilities/media/AudioOutputState;)V

    return-void
.end method

.method public static final synthetic access$updateSelectedOutputDevice(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAudioDevices;->updateSelectedOutputDevice(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    return-void
.end method

.method private final autoSelectAudioOutput()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->getSelectedVoiceChannelId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/utilities/media/AudioOutputState;->isExternalAudioOutputConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices;->autoSelectNonSpeakerOutput()V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/discord/stores/StoreAudioDevices;->videoUseDetector:Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;

    invoke-virtual {v2, v0, v1}, Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;->isAnyoneUsingVideoInChannel(J)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    :goto_0
    invoke-direct {p0, v0}, Lcom/discord/stores/StoreAudioDevices;->updateSelectedOutputDevice(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    :goto_1
    return-void
.end method

.method private final autoSelectNonSpeakerOutput()V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAvailableOutputDevices()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    instance-of v3, v3, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    check-cast v2, Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v2, Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    :goto_1
    invoke-direct {p0, v2}, Lcom/discord/stores/StoreAudioDevices;->updateSelectedOutputDevice(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    return-void
.end method

.method private final refreshAudioOutputState()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreAudioDevices;->updateAudioOutputState(Lcom/discord/utilities/media/AudioOutputState;)V

    :cond_0
    return-void
.end method

.method private final updateAudioOutputState(Lcom/discord/utilities/media/AudioOutputState;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected()Z

    move-result v0

    iget-object v1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    iget-object v3, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v4, p1

    invoke-static/range {v3 .. v8}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->copy$default(Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object v3

    iput-object v3, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    new-array v3, v2, [Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    sget-object v4, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "elements"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/LinkedHashSet;-><init>(I)V

    invoke-static {v3, v4}, Lf/h/a/f/f/n/g;->toCollection([Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;

    invoke-interface {v4, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/discord/stores/StoreAudioDevices;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceChannelSelected;->getSelectedVoiceChannelId()J

    move-result-wide v5

    iget-object v3, p0, Lcom/discord/stores/StoreAudioDevices;->videoUseDetector:Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;

    invoke-virtual {v3, v5, v6}, Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;->isMeUsingVideoInChannel(J)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->getBluetoothDeviceName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_0
    invoke-direct {p0, v4}, Lcom/discord/stores/StoreAudioDevices;->updateAvailableOutputDevices(Ljava/util/Set;)V

    iget-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object p1

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected()Z

    move-result v3

    if-nez v3, :cond_6

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->isBluetoothOutputConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->isHeadsetUnplugged()Z

    move-result p1

    if-eqz p1, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices;->autoSelectAudioOutput()V

    :cond_7
    iput-boolean v2, p0, Lcom/discord/stores/StoreAudioDevices;->isDirty:Z

    return-void
.end method

.method private final updateAvailableOutputDevices(Ljava/util/Set;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->copy$default(Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreAudioDevices;->isDirty:Z

    return-void
.end method

.method private final updateSelectedOutputDevice(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->copy$default(Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreAudioDevices;->isDirty:Z

    return-void
.end method


# virtual methods
.method public final getAudioDevicesState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesStateSubject:Lrx/subjects/SerializedSubject;

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "audioDevicesStateSubject\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getAudioDevicesState$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-object v0
.end method

.method public final handleStreamRtcConnectionStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/utilities/media/AudioOutputState;->isExternalAudioOutputConnected()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    sget-object p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAudioDevices;->updateSelectedOutputDevice(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    :cond_1
    return-void
.end method

.method public final handleVoiceChannelSelected()V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices;->autoSelectAudioOutput()V

    return-void
.end method

.method public final handleVoiceStatesUpdated()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->getSelectedVoiceChannelId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/stores/StoreAudioDevices;->videoUseDetector:Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;

    invoke-virtual {v2, v0, v1}, Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;->isMeUsingVideoInChannel(J)Z

    move-result v0

    iget-boolean v1, p0, Lcom/discord/stores/StoreAudioDevices;->wasMeUsingVideoInChannel:Z

    if-eq v1, v0, :cond_0

    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices;->refreshAudioOutputState()V

    :cond_0
    iget-boolean v1, p0, Lcom/discord/stores/StoreAudioDevices;->wasMeUsingVideoInChannel:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices;->autoSelectAudioOutput()V

    :cond_1
    iput-boolean v0, p0, Lcom/discord/stores/StoreAudioDevices;->wasMeUsingVideoInChannel:Z

    return-void
.end method

.method public final init()V
    .locals 10

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioOutputMonitor:Lcom/discord/utilities/media/AudioOutputMonitor;

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputMonitor;->getOutputState()Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/stores/StoreAudioDevices;

    new-instance v7, Lcom/discord/stores/StoreAudioDevices$init$1;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreAudioDevices$init$1;-><init>(Lcom/discord/stores/StoreAudioDevices;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public onDispatchEnded()V
    .locals 2

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioDevices;->isDirty:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesStateSubject:Lrx/subjects/SerializedSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, v1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreAudioDevices;->isDirty:Z

    return-void
.end method

.method public final selectAudioOutput(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V
    .locals 2

    const-string v0, "outputDevice"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAudioDevices$selectAudioOutput$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAudioDevices$selectAudioOutput$1;-><init>(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final setAudioDevicesState$app_productionDiscordExternalRelease(Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreAudioDevices;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-void
.end method

.method public final toggleSpeakerOutput()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreAudioDevices$toggleSpeakerOutput$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreAudioDevices$toggleSpeakerOutput$1;-><init>(Lcom/discord/stores/StoreAudioDevices;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
