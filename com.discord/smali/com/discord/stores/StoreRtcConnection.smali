.class public final Lcom/discord/stores/StoreRtcConnection;
.super Ljava/lang/Object;
.source "StoreRtcConnection.kt"

# interfaces
.implements Lcom/discord/rtcconnection/RtcConnection$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;
    }
.end annotation


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final connectionState:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/RtcConnection$State;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionStateSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/rtcconnection/RtcConnection$State;",
            "Lcom/discord/rtcconnection/RtcConnection$State;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private hasSelectedVoiceChannel:Z

.field private joinedChannelTimestamp:Ljava/lang/Long;

.field private networkMonitor:Lcom/discord/utilities/networking/NetworkMonitor;

.field private final quality:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/RtcConnection$Quality;",
            ">;"
        }
    .end annotation
.end field

.field private final qualitySubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/rtcconnection/RtcConnection$Quality;",
            ">;"
        }
    .end annotation
.end field

.field private rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

.field private rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

.field private final rtcConnectionMetadataSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

.field private sessionId:Ljava/lang/String;

.field private final speakingUpdates:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final speakingUsersSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Pair<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeRtcRegion:Lcom/discord/stores/StoreRtcRegion;

.field private final stream:Lcom/discord/stores/StoreStream;

.field private voiceServer:Lcom/discord/models/domain/ModelVoice$Server;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreRtcRegion;Lcom/discord/stores/StoreAnalytics;)V
    .locals 9

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeRtcRegion"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeAnalytics"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreRtcConnection;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p4, p0, Lcom/discord/stores/StoreRtcConnection;->storeRtcRegion:Lcom/discord/stores/StoreRtcRegion;

    iput-object p5, p0, Lcom/discord/stores/StoreRtcConnection;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->speakingUsersSubject:Lrx/subjects/PublishSubject;

    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$Quality;->UNKNOWN:Lcom/discord/rtcconnection/RtcConnection$Quality;

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/stores/StoreRtcConnection;->qualitySubject:Lrx/subjects/BehaviorSubject;

    new-instance p3, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, p3

    invoke-direct/range {v0 .. v8}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;-><init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p3, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    new-instance p3, Lrx/subjects/SerializedSubject;

    new-instance p4, Lcom/discord/rtcconnection/RtcConnection$State$d;

    const/4 p5, 0x0

    invoke-direct {p4, p5}, Lcom/discord/rtcconnection/RtcConnection$State$d;-><init>(Z)V

    invoke-static {p4}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p4

    invoke-direct {p3, p4}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p3, p0, Lcom/discord/stores/StoreRtcConnection;->connectionStateSubject:Lrx/subjects/SerializedSubject;

    const/4 p4, 0x0

    invoke-static {p4}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p4

    iput-object p4, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnectionMetadataSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p3

    invoke-virtual {p3}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p3

    const-string p4, "connectionStateSubject\n \u2026  .distinctUntilChanged()"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/discord/stores/StoreRtcConnection;->connectionState:Lrx/Observable;

    const-string p3, "qualitySubject"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    invoke-virtual {p2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p2

    const-string p3, "qualitySubject\n         \u2026  .distinctUntilChanged()"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/discord/stores/StoreRtcConnection;->quality:Lrx/Observable;

    const-string/jumbo p2, "speakingUsersSubject"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string/jumbo p2, "speakingUsersSubject\n   \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->speakingUpdates:Lrx/Observable;

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreRtcConnection;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getStoreAnalytics$p(Lcom/discord/stores/StoreRtcConnection;)Lcom/discord/stores/StoreAnalytics;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreRtcConnection;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getStream$p(Lcom/discord/stores/StoreRtcConnection;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$handleMediaSessionIdReceived(Lcom/discord/stores/StoreRtcConnection;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->handleMediaSessionIdReceived()V

    return-void
.end method

.method public static final synthetic access$handleSelfDeafened(Lcom/discord/stores/StoreRtcConnection;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleSelfDeafened(Z)V

    return-void
.end method

.method public static final synthetic access$handleSelfMuted(Lcom/discord/stores/StoreRtcConnection;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleSelfMuted(Z)V

    return-void
.end method

.method public static final synthetic access$handleSelfVideo(Lcom/discord/stores/StoreRtcConnection;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleSelfVideo(Z)V

    return-void
.end method

.method public static final synthetic access$handleUsersMuted(Lcom/discord/stores/StoreRtcConnection;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleUsersMuted(Ljava/util/Map;)V

    return-void
.end method

.method public static final synthetic access$handleUsersVolume(Lcom/discord/stores/StoreRtcConnection;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleUsersVolume(Ljava/util/Map;)V

    return-void
.end method

.method private final checkForVoiceServerUpdate()V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, p0, Lcom/discord/stores/StoreRtcConnection;->voiceServer:Lcom/discord/models/domain/ModelVoice$Server;

    if-eqz v0, :cond_5

    if-nez v1, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelVoice$Server;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelVoice$Server;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    iget-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    invoke-static {v2, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v3

    if-eqz v2, :cond_1

    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelVoice$Server;->getChannelId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelVoice$Server;->getChannelId()Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, v0, Lcom/discord/rtcconnection/RtcConnection;->x:J

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-eqz v2, :cond_3

    :goto_0
    return-void

    :cond_3
    sget-object v2, Lcom/discord/app/App;->e:Lcom/discord/app/App;

    sget-boolean v2, Lcom/discord/app/App;->d:Z

    const/4 v4, 0x0

    if-eqz v2, :cond_4

    const-string v2, ""

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelVoice$Server;->getEndpoint()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, "voiceServer.endpoint"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v3, v4}, Lcom/discord/utilities/ssl/SecureSocketsLayerUtils;->createSocketFactory$default(Ljavax/net/ssl/TrustManagerFactory;ILjava/lang/Object;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v4

    :goto_1
    const-string v3, "Voice server update, connect to server w/ endpoint: "

    invoke-static {v3, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/discord/stores/StoreRtcConnection;->recordBreadcrumb(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelVoice$Server;->getToken()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lf/a/h/h;

    invoke-direct {v3, v0, v2, v1, v4}, Lf/a/h/h;-><init>(Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;)V

    invoke-virtual {v0, v3}, Lcom/discord/rtcconnection/RtcConnection;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    :cond_5
    :goto_2
    return-void
.end method

.method private final createRtcConnection()V
    .locals 21
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    iget-object v7, v0, Lcom/discord/stores/StoreRtcConnection;->sessionId:Ljava/lang/String;

    iget-object v3, v0, Lcom/discord/stores/StoreRtcConnection;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v6, v4, v8

    if-lez v6, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    move-object v4, v3

    goto :goto_2

    :cond_2
    move-object v4, v2

    :goto_2
    iget-object v3, v0, Lcom/discord/stores/StoreRtcConnection;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_3

    :cond_3
    move-object v3, v2

    :goto_3
    sget-object v5, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v5}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/stores/StoreMediaEngine;->getMediaEngine()Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    move-result-object v12

    if-eqz v1, :cond_a

    if-eqz v7, :cond_a

    if-nez v3, :cond_4

    goto/16 :goto_7

    :cond_4
    iget-object v5, v0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v5, :cond_5

    iget-wide v5, v5, Lcom/discord/rtcconnection/RtcConnection;->x:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_4

    :cond_5
    move-object v5, v2

    :goto_4
    invoke-static {v5, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, v0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v5, :cond_6

    iget-object v5, v5, Lcom/discord/rtcconnection/RtcConnection;->y:Ljava/lang/String;

    goto :goto_5

    :cond_6
    move-object v5, v2

    :goto_5
    invoke-static {v5, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    return-void

    :cond_7
    new-instance v13, Lcom/discord/rtcconnection/RtcConnection;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const/4 v8, 0x1

    if-eqz v4, :cond_8

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    goto :goto_6

    :cond_8
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    :goto_6
    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    iget-object v3, v0, Lcom/discord/stores/StoreRtcConnection;->networkMonitor:Lcom/discord/utilities/networking/NetworkMonitor;

    if-eqz v3, :cond_9

    const/4 v15, 0x0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v14

    iget-object v2, v0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings;->getMutedUsers()Ljava/util/Map;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x1200

    move-object v2, v3

    move-object v3, v13

    move-object/from16 v20, v13

    move-object v13, v1

    move-object/from16 v16, v2

    invoke-direct/range {v3 .. v19}, Lcom/discord/rtcconnection/RtcConnection;-><init>(Ljava/lang/Long;JLjava/lang/String;ZLjava/lang/String;JLcom/discord/rtcconnection/mediaengine/MediaEngine;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/time/Clock;Lcom/discord/rtcconnection/RtcConnection$c;Lcom/discord/utilities/networking/NetworkMonitor;Ljava/util/Map;Ljava/lang/String;I)V

    move-object/from16 v1, v20

    invoke-virtual {v1, v0}, Lcom/discord/rtcconnection/RtcConnection;->b(Lcom/discord/rtcconnection/RtcConnection$b;)V

    iput-object v1, v0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    invoke-direct/range {p0 .. p0}, Lcom/discord/stores/StoreRtcConnection;->updateMetadata()V

    return-void

    :cond_9
    const-string v1, "networkMonitor"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_a
    :goto_7
    return-void
.end method

.method private final destroyRtcConnection()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_1

    const-string v0, "destroying rtc connection"

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreRtcConnection;->recordBreadcrumb(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_0

    new-instance v1, Lf/a/h/i;

    invoke-direct {v1, v0}, Lf/a/h/i;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->updateMetadata()V

    :cond_1
    return-void
.end method

.method private final handleMediaSessionIdReceived()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    iget-object v1, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/discord/rtcconnection/RtcConnection;->t:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->getInstance()Lcom/google/firebase/crashlytics/FirebaseCrashlytics;

    move-result-object v0

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const-string v1, ""

    :goto_1
    const-string v2, "mediaSessionId"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->setCustomKey(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->updateMetadata()V

    return-void
.end method

.method private final handleSelfDeafened(Z)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x37

    const/4 v8, 0x0

    move v4, p1

    invoke-static/range {v0 .. v8}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->copy$default(Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->onVoiceStateUpdated()V

    return-void
.end method

.method private final handleSelfMuted(Z)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3b

    const/4 v8, 0x0

    move v3, p1

    invoke-static/range {v0 .. v8}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->copy$default(Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->onVoiceStateUpdated()V

    return-void
.end method

.method private final handleSelfVideo(Z)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x2f

    const/4 v8, 0x0

    move v5, p1

    invoke-static/range {v0 .. v8}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->copy$default(Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->onVoiceStateUpdated()V

    return-void
.end method

.method private final handleUsersMuted(Ljava/util/Map;)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v3, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/discord/rtcconnection/RtcConnection;->G:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v3, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v3, :cond_0

    invoke-interface {v3, v1, v2, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->r(JZ)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final handleUsersVolume(Ljava/util/Map;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iget-object v3, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v3, :cond_0

    invoke-virtual {v3, v1, v2, v0}, Lcom/discord/rtcconnection/RtcConnection;->o(JF)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final logChannelJoin(Lcom/discord/models/domain/ModelChannel;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_2

    iget-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->a:Ljava/lang/String;

    if-eqz v4, :cond_2

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getVoiceStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceStates;->getMediaStatesBlocking()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    move-object v6, v0

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getMediaEngine$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaEngine;->getSelectedVideoInputDeviceBlocking()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v7

    sget-object v0, Lcom/discord/utilities/io/NetworkUtils;->INSTANCE:Lcom/discord/utilities/io/NetworkUtils;

    iget-object v5, p0, Lcom/discord/stores/StoreRtcConnection;->context:Landroid/content/Context;

    if-eqz v5, :cond_1

    invoke-virtual {v0, v5}, Lcom/discord/utilities/io/NetworkUtils;->getNetworkType(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v8

    move-object v5, p1

    invoke-virtual/range {v1 .. v8}, Lcom/discord/utilities/analytics/AnalyticsTracker;->voiceChannelJoin(JLjava/lang/String;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/Integer;)V

    return-void

    :cond_1
    const-string p1, "context"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_2
    return-void
.end method

.method private final logChannelLeave(Lcom/discord/models/domain/ModelChannel;)V
    .locals 11
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_2

    iget-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->a:Ljava/lang/String;

    if-eqz v4, :cond_2

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getVoiceStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceStates;->getMediaStatesBlocking()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    move-object v6, v0

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->joinedChannelTimestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v7

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v7

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    move-object v7, v0

    move-object v5, p1

    invoke-virtual/range {v1 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->voiceChannelLeave(JLjava/lang/String;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;)V

    :cond_2
    return-void
.end method

.method private final onVoiceStateUpdated()V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreRtcConnection;->hasSelectedVoiceChannel:Z

    if-eqz v0, :cond_0

    const-string v0, "Voice state update: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreRtcConnection;->recordBreadcrumb(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGatewaySocket()Lcom/discord/stores/StoreGatewayConnection;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-virtual {v0}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->getGuild_id()Ljava/lang/Long;

    move-result-object v2

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-virtual {v0}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->getChannel_id()Ljava/lang/Long;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-virtual {v0}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->getSelf_mute()Z

    move-result v4

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-virtual {v0}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->getSelf_deaf()Z

    move-result v5

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-virtual {v0}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->getSelf_video()Z

    move-result v6

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    invoke-virtual {v0}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->getChannel_id()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->storeRtcRegion:Lcom/discord/stores/StoreRtcRegion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreRtcRegion;->getPreferredRegion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move-object v7, v0

    invoke-virtual/range {v1 .. v7}, Lcom/discord/stores/StoreGatewayConnection;->voiceStateUpdate(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)Z

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->checkForVoiceServerUpdate()V

    return-void
.end method

.method private final recordBreadcrumb(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StoreRtcConnection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "rtcconnection"

    invoke-virtual {v0, p1, v1}, Lcom/discord/app/AppLog;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final updateMetadata()V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    iget-object v2, v0, Lcom/discord/rtcconnection/RtcConnection;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->t:Ljava/lang/String;

    iget-wide v4, v0, Lcom/discord/rtcconnection/RtcConnection;->x:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v0, v0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnectionMetadataSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getConnectedGuildId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public final getConnectionState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/RtcConnection$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->connectionState:Lrx/Observable;

    return-object v0
.end method

.method public final getQuality()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/RtcConnection$Quality;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->quality:Lrx/Observable;

    return-object v0
.end method

.method public final getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/rtcconnection/RtcConnection;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    return-object v0
.end method

.method public final getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    return-object v0
.end method

.method public final getSpeakingUpdates()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->speakingUpdates:Lrx/Observable;

    return-object v0
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->sessionId:Ljava/lang/String;

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getSessionId()Ljava/lang/String;

    move-result-object p1

    const-string v1, "payload.sessionId"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "<set-?>"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->y:Ljava/lang/String;

    :cond_0
    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->onVoiceStateUpdated()V

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->checkForVoiceServerUpdate()V

    return-void
.end method

.method public final handleConnectionReady(Z)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleVoiceChannelSelected(Ljava/lang/Long;)V

    :cond_1
    return-void
.end method

.method public final handleVoiceChannelSelected(Ljava/lang/Long;)V
    .locals 12
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    xor-int/2addr v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreRtcConnection;->logChannelLeave(Lcom/discord/models/domain/ModelChannel;)V

    :cond_1
    iput-object v1, p0, Lcom/discord/stores/StoreRtcConnection;->joinedChannelTimestamp:Ljava/lang/Long;

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->destroyRtcConnection()V

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    goto :goto_1

    :cond_3
    move-object p1, v1

    :goto_1
    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->selectedVoiceChannel:Lcom/discord/models/domain/ModelChannel;

    iget-object v3, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_5

    move-object v4, v0

    goto :goto_3

    :cond_5
    move-object v4, v1

    :goto_3
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v5, v0

    goto :goto_4

    :cond_6
    move-object v5, v1

    :goto_4
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x3c

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;->copy$default(Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILjava/lang/Object;)Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->currentVoiceState:Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;

    if-eqz p1, :cond_7

    iput-boolean v2, p0, Lcom/discord/stores/StoreRtcConnection;->hasSelectedVoiceChannel:Z

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->createRtcConnection()V

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->joinedChannelTimestamp:Ljava/lang/Long;

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreRtcConnection;->logChannelJoin(Lcom/discord/models/domain/ModelChannel;)V

    goto :goto_5

    :cond_7
    iput-object v1, p0, Lcom/discord/stores/StoreRtcConnection;->voiceServer:Lcom/discord/models/domain/ModelVoice$Server;

    :goto_5
    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->onVoiceStateUpdated()V

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->checkForVoiceServerUpdate()V

    return-void
.end method

.method public final handleVoiceServerUpdate(Lcom/discord/models/domain/ModelVoice$Server;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "voiceServer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handling voice server update: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreRtcConnection;->recordBreadcrumb(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->voiceServer:Lcom/discord/models/domain/ModelVoice$Server;

    invoke-direct {p0}, Lcom/discord/stores/StoreRtcConnection;->checkForVoiceServerUpdate()V

    return-void
.end method

.method public final init(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "networkMonitor"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/discord/stores/StoreRtcConnection;->networkMonitor:Lcom/discord/utilities/networking/NetworkMonitor;

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings;->isSelfMuted()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreRtcConnection;

    new-instance v6, Lcom/discord/stores/StoreRtcConnection$init$1;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreRtcConnection$init$1;-><init>(Lcom/discord/stores/StoreRtcConnection;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings;->isSelfDeafened()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreRtcConnection;

    new-instance v6, Lcom/discord/stores/StoreRtcConnection$init$2;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreRtcConnection$init$2;-><init>(Lcom/discord/stores/StoreRtcConnection;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreMediaEngine;->getSelectedVideoInputDevice()Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreRtcConnection$init$3;->INSTANCE:Lcom/discord/stores/StoreRtcConnection$init$3;

    invoke-virtual {p1, p2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string p1, "StoreStream\n        .get\u2026viceDescription != null }"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v1, Lcom/discord/stores/StoreRtcConnection;

    new-instance v6, Lcom/discord/stores/StoreRtcConnection$init$4;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreRtcConnection$init$4;-><init>(Lcom/discord/stores/StoreRtcConnection;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreMediaSettings;->getUsersVolume()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreRtcConnection;

    new-instance v6, Lcom/discord/stores/StoreRtcConnection$init$5;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreRtcConnection$init$5;-><init>(Lcom/discord/stores/StoreRtcConnection;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreMediaSettings;->getUsersMuted()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreRtcConnection;

    new-instance v6, Lcom/discord/stores/StoreRtcConnection$init$6;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreRtcConnection$init$6;-><init>(Lcom/discord/stores/StoreRtcConnection;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final observeRtcConnectionMetadata()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->rtcConnectionMetadataSubject:Lrx/subjects/BehaviorSubject;

    const-string v1, "rtcConnectionMetadataSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onAnalyticsEvent(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "properties"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreAnalytics;->trackMediaSessionJoined(Ljava/util/Map;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v0, Lcom/discord/stores/StoreRtcConnection$onAnalyticsEvent$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/stores/StoreRtcConnection$onAnalyticsEvent$1;-><init>(Lcom/discord/stores/StoreRtcConnection;Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreAnalytics;->trackVoiceDisconnect(Ljava/util/Map;)V

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreAnalytics;->trackVoiceConnectionFailure(Ljava/util/Map;)V

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreAnalytics;->trackVoiceConnectionSuccess(Ljava/util/Map;)V

    :goto_0
    return-void
.end method

.method public onFatalClose()V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getVoiceChannelSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->clear()V

    return-void
.end method

.method public onMediaSessionIdReceived()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreRtcConnection$onMediaSessionIdReceived$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreRtcConnection$onMediaSessionIdReceived$1;-><init>(Lcom/discord/stores/StoreRtcConnection;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onQualityUpdate(Lcom/discord/rtcconnection/RtcConnection$Quality;)V
    .locals 1

    const-string v0, "quality"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->qualitySubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onSpeaking(JZ)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->speakingUsersSubject:Lrx/subjects/PublishSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    new-instance p3, Lkotlin/Pair;

    invoke-direct {p3, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, p3}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 2

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connection state change: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreRtcConnection;->recordBreadcrumb(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->connectionStateSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onVideoStream(JLjava/lang/Integer;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreRtcConnection$onVideoStream$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/discord/stores/StoreRtcConnection$onVideoStream$1;-><init>(Lcom/discord/stores/StoreRtcConnection;JLjava/lang/Integer;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
