.class public final Lcom/discord/stores/updates/ObservationDeckProvider;
.super Ljava/lang/Object;
.source "ObservationDeck.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/stores/updates/ObservationDeckProvider;

.field private static final INSTANCE$delegate:Lkotlin/Lazy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/updates/ObservationDeckProvider;

    invoke-direct {v0}, Lcom/discord/stores/updates/ObservationDeckProvider;-><init>()V

    sput-object v0, Lcom/discord/stores/updates/ObservationDeckProvider;->INSTANCE:Lcom/discord/stores/updates/ObservationDeckProvider;

    sget-object v0, Lcom/discord/stores/updates/ObservationDeckProvider$INSTANCE$2;->INSTANCE:Lcom/discord/stores/updates/ObservationDeckProvider$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/stores/updates/ObservationDeckProvider;->INSTANCE$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final get()Lcom/discord/stores/updates/ObservationDeck;
    .locals 1

    sget-object v0, Lcom/discord/stores/updates/ObservationDeckProvider;->INSTANCE:Lcom/discord/stores/updates/ObservationDeckProvider;

    invoke-direct {v0}, Lcom/discord/stores/updates/ObservationDeckProvider;->getINSTANCE()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v0

    return-object v0
.end method

.method private final getINSTANCE()Lcom/discord/stores/updates/ObservationDeck;
    .locals 1

    sget-object v0, Lcom/discord/stores/updates/ObservationDeckProvider;->INSTANCE$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/updates/ObservationDeck;

    return-object v0
.end method
