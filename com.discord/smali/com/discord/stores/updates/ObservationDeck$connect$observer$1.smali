.class public final Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;
.super Lcom/discord/stores/updates/ObservationDeck$Observer;
.source "ObservationDeck.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/updates/ObservationDeck;->connect([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/discord/stores/updates/ObservationDeck$Observer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $observerName:Ljava/lang/String;

.field public final synthetic $onUpdate:Lkotlin/jvm/functions/Function0;

.field public final synthetic $updateSources:[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

.field private final name:Ljava/lang/String;

.field private final observingUpdates:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            ">;"
        }
    .end annotation
.end field

.field private onUpdate:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;Lkotlin/jvm/functions/Function0;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->$updateSources:[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    iput-object p2, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->$onUpdate:Lkotlin/jvm/functions/Function0;

    iput-object p3, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->$observerName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/stores/updates/ObservationDeck$Observer;-><init>()V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->toSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->observingUpdates:Ljava/util/Set;

    iput-object p2, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->onUpdate:Lkotlin/jvm/functions/Function0;

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    :goto_0
    iput-object p3, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getObservingUpdates()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->observingUpdates:Ljava/util/Set;

    return-object v0
.end method

.method public getOnUpdate()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->onUpdate:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public setOnUpdate(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;->onUpdate:Lkotlin/jvm/functions/Function0;

    return-void
.end method
