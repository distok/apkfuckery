.class public final enum Lcom/discord/stores/updates/ObservationDeck$LogLevel;
.super Ljava/lang/Enum;
.source "ObservationDeck.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/updates/ObservationDeck;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LogLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/stores/updates/ObservationDeck$LogLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/stores/updates/ObservationDeck$LogLevel;

.field public static final enum ERROR:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

.field public static final enum NONE:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

.field public static final enum VERBOSE:Lcom/discord/stores/updates/ObservationDeck$LogLevel;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    new-instance v1, Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    const-string v2, "NONE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/updates/ObservationDeck$LogLevel;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->NONE:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    const-string v2, "ERROR"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/updates/ObservationDeck$LogLevel;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->ERROR:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    const-string v2, "VERBOSE"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/updates/ObservationDeck$LogLevel;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->VERBOSE:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->$VALUES:[Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/stores/updates/ObservationDeck$LogLevel;
    .locals 1

    const-class v0, Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    return-object p0
.end method

.method public static values()[Lcom/discord/stores/updates/ObservationDeck$LogLevel;
    .locals 1

    sget-object v0, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->$VALUES:[Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    invoke-virtual {v0}, [Lcom/discord/stores/updates/ObservationDeck$LogLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    return-object v0
.end method
