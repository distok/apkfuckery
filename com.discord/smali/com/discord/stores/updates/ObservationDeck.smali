.class public final Lcom/discord/stores/updates/ObservationDeck;
.super Ljava/lang/Object;
.source "ObservationDeck.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/updates/ObservationDeck$LogLevel;,
        Lcom/discord/stores/updates/ObservationDeck$Observer;,
        Lcom/discord/stores/updates/ObservationDeck$UpdateSource;,
        Lcom/discord/stores/updates/ObservationDeck$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/updates/ObservationDeck$Companion;

.field private static final LOG_CATEGORY:Ljava/lang/String; = "ObservationDeck"

.field private static final ON_UPDATE_EMPTY:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final logLevel:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

.field private final logger:Lcom/discord/utilities/logging/Logger;

.field private observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/updates/ObservationDeck$Observer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/updates/ObservationDeck$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/updates/ObservationDeck$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/updates/ObservationDeck;->Companion:Lcom/discord/stores/updates/ObservationDeck$Companion;

    sget-object v0, Lcom/discord/stores/updates/ObservationDeck$Companion$ON_UPDATE_EMPTY$1;->INSTANCE:Lcom/discord/stores/updates/ObservationDeck$Companion$ON_UPDATE_EMPTY$1;

    sput-object v0, Lcom/discord/stores/updates/ObservationDeck;->ON_UPDATE_EMPTY:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/discord/stores/updates/ObservationDeck;-><init>(Lcom/discord/utilities/logging/Logger;Lcom/discord/stores/updates/ObservationDeck$LogLevel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/utilities/logging/Logger;Lcom/discord/stores/updates/ObservationDeck$LogLevel;)V
    .locals 1

    const-string v0, "logger"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logLevel"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/updates/ObservationDeck;->logger:Lcom/discord/utilities/logging/Logger;

    iput-object p2, p0, Lcom/discord/stores/updates/ObservationDeck;->logLevel:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/updates/ObservationDeck;->observers:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/utilities/logging/Logger;Lcom/discord/stores/updates/ObservationDeck$LogLevel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    sget-object p1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    sget-object p2, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->NONE:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/updates/ObservationDeck;-><init>(Lcom/discord/utilities/logging/Logger;Lcom/discord/stores/updates/ObservationDeck$LogLevel;)V

    return-void
.end method

.method public static final synthetic access$getON_UPDATE_EMPTY$cp()Lkotlin/jvm/functions/Function0;
    .locals 1

    sget-object v0, Lcom/discord/stores/updates/ObservationDeck;->ON_UPDATE_EMPTY:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public static synthetic connect$default(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/updates/ObservationDeck$Observer;ZILjava/lang/Object;)Lcom/discord/stores/updates/ObservationDeck$Observer;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x1

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/updates/ObservationDeck;->connect(Lcom/discord/stores/updates/ObservationDeck$Observer;Z)Lcom/discord/stores/updates/ObservationDeck$Observer;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic connect$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/discord/stores/updates/ObservationDeck$Observer;
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x1

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/updates/ObservationDeck;->connect([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/discord/stores/updates/ObservationDeck$Observer;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x1

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    sget-object p3, Lrx/Emitter$BackpressureMode;->h:Lrx/Emitter$BackpressureMode;

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/updates/ObservationDeck;->connectRx([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;
    .locals 6

    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_0

    const/4 p2, 0x1

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    move v2, p2

    :goto_0
    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_1

    sget-object p3, Lrx/Emitter$BackpressureMode;->h:Lrx/Emitter$BackpressureMode;

    :cond_1
    move-object v3, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_2

    const/4 p4, 0x0

    :cond_2
    move-object v4, p4

    move-object v0, p0

    move-object v1, p1

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/discord/stores/updates/ObservationDeck;->connectRx([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final logBreadcrumb(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/updates/ObservationDeck;->logLevel:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    sget-object v1, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->VERBOSE:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/updates/ObservationDeck;->logger:Lcom/discord/utilities/logging/Logger;

    const-string v1, "ObservationDeck"

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private final logNotifyError(Ljava/lang/Throwable;Ljava/util/Set;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/Set<",
            "+",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/updates/ObservationDeck;->logger:Lcom/discord/utilities/logging/Logger;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/Pair;

    const-string v3, ", "

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    move-object v2, p2

    invoke-static/range {v2 .. v9}, Lx/h/f;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string v4, "Update Sources"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x0

    aput-object v3, v1, v2

    iget-object v3, p0, Lcom/discord/stores/updates/ObservationDeck;->observers:Ljava/util/List;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v12, 0x1

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/discord/stores/updates/ObservationDeck$Observer;

    instance-of v7, p2, Ljava/util/Collection;

    if-eqz v7, :cond_2

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    invoke-virtual {v6}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getObservingUpdates()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    :goto_1
    if-eqz v12, :cond_0

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/discord/stores/updates/ObservationDeck$logNotifyError$2;->INSTANCE:Lcom/discord/stores/updates/ObservationDeck$logNotifyError$2;

    const/16 v11, 0x1e

    const-string v5, "\n"

    invoke-static/range {v4 .. v11}, Lx/h/f;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object p2

    new-instance v2, Lkotlin/Pair;

    const-string v3, "Observers"

    invoke-direct {v2, v3, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v1, v12

    invoke-static {v1}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p2

    const-string v1, "ObservationDeck notify error"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/utilities/logging/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized connect(Lcom/discord/stores/updates/ObservationDeck$Observer;Z)Lcom/discord/stores/updates/ObservationDeck$Observer;
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "observer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connect START. observer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/updates/ObservationDeck;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getOnUpdate()Lkotlin/jvm/functions/Function0;

    move-result-object p2

    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "connect END. observer: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " -- isStale: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/stores/updates/ObservationDeck$Observer;->isStale()Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized connect([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/discord/stores/updates/ObservationDeck$Observer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            "Z",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/discord/stores/updates/ObservationDeck$Observer;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "updateSources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onUpdate"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;

    invoke-direct {v0, p1, p4, p3}, Lcom/discord/stores/updates/ObservationDeck$connect$observer$1;-><init>([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;Lkotlin/jvm/functions/Function0;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/discord/stores/updates/ObservationDeck;->connect(Lcom/discord/stores/updates/ObservationDeck$Observer;Z)Lcom/discord/stores/updates/ObservationDeck$Observer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized connectRx([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            "Z",
            "Lrx/Emitter$BackpressureMode;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "updateSources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backpressureMode"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v7, Lcom/discord/stores/updates/ObservationDeck$connectRx$1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, v0

    move v4, p2

    move-object v5, p4

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/updates/ObservationDeck$connectRx$1;-><init>(Lcom/discord/stores/updates/ObservationDeck;Lkotlin/jvm/internal/Ref$ObjectRef;ZLjava/lang/String;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    invoke-static {v7, p3}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/updates/ObservationDeck$connectRx$2;

    invoke-direct {p2, p0, v0}, Lcom/discord/stores/updates/ObservationDeck$connectRx$2;-><init>(Lcom/discord/stores/updates/ObservationDeck;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    invoke-virtual {p1, p2}, Lrx/Observable;->t(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.create<Unit>(\u2026rver?.let(::disconnect) }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized connectRx([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            "Z",
            "Lrx/Emitter$BackpressureMode;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "updateSources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backpressureMode"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generator"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/updates/ObservationDeck;->connectRx([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/updates/ObservationDeck$connectRx$3;

    invoke-direct {p2, p5}, Lcom/discord/stores/updates/ObservationDeck$connectRx$3;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "connectRx(\n        *upda\u2026    ).map { generator() }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized disconnect(Lcom/discord/stores/updates/ObservationDeck$Observer;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "observer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "disconnect START. observer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/stores/updates/ObservationDeck$Observer;->markStale()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "disconnect END. observer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized notify(Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "updates"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notify START"

    invoke-direct {p0, v0}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/discord/stores/updates/ObservationDeck;->observers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_5

    iget-object v2, p0, Lcom/discord/stores/updates/ObservationDeck;->observers:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/updates/ObservationDeck$Observer;

    invoke-virtual {v2}, Lcom/discord/stores/updates/ObservationDeck$Observer;->isStale()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removing observer: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/stores/updates/ObservationDeck;->observers:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    invoke-virtual {v2}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getObservingUpdates()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/discord/stores/updates/ObservationDeck$Observer;->getOnUpdate()Lkotlin/jvm/functions/Function0;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    :goto_2
    add-int/2addr v1, v4

    goto :goto_0

    :cond_5
    :try_start_2
    const-string p1, "notify END"

    :goto_3
    invoke-direct {p0, p1}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/discord/stores/updates/ObservationDeck;->logLevel:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    sget-object v2, Lcom/discord/stores/updates/ObservationDeck$LogLevel;->ERROR:Lcom/discord/stores/updates/ObservationDeck$LogLevel;

    invoke-virtual {v1, v2}, Ljava/lang/Enum;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-ltz v1, :cond_6

    invoke-direct {p0, v0, p1}, Lcom/discord/stores/updates/ObservationDeck;->logNotifyError(Ljava/lang/Throwable;Ljava/util/Set;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    const-string p1, "notify END"
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_3

    :goto_4
    monitor-exit p0

    return-void

    :cond_6
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception p1

    :try_start_6
    const-string v0, "notify END"

    invoke-direct {p0, v0}, Lcom/discord/stores/updates/ObservationDeck;->logBreadcrumb(Ljava/lang/String;)V

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method
