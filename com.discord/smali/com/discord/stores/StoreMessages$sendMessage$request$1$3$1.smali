.class public final Lcom/discord/stores/StoreMessages$sendMessage$request$1$3$1;
.super Lx/m/c/k;
.source "StoreMessages.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;->invoke(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $uploads:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;

    iput-object p2, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3$1;->$uploads:Ljava/util/List;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    invoke-static {v0}, Lcom/discord/stores/StoreMessages;->access$getStream$p(Lcom/discord/stores/StoreMessages;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getMessageUploads$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageUploads;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    const-string v2, "localMessage"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v2, "localMessage.nonce!!"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$3$1;->$uploads:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreMessageUploads;->bindUpload(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method
