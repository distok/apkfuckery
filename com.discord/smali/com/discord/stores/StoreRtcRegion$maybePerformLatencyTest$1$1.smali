.class public final Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;
.super Lx/m/c/k;
.source "StoreRtcRegion.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $newGeoRankedRegions:Ljava/util/List;

.field public final synthetic $timeNowMs:J

.field public final synthetic this$0:Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;Ljava/util/List;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;->this$0:Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;

    iput-object p2, p0, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;->$newGeoRankedRegions:Ljava/util/List;

    iput-wide p3, p0, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;->$timeNowMs:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;->invoke(Ljava/util/List;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "latencyRankedRegions"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RTC region latency test ranking is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;->this$0:Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;

    iget-object v0, v0, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1;->this$0:Lcom/discord/stores/StoreRtcRegion;

    new-instance v1, Lcom/discord/stores/RtcLatencyTestResult;

    iget-object v2, p0, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;->$newGeoRankedRegions:Ljava/util/List;

    iget-wide v3, p0, Lcom/discord/stores/StoreRtcRegion$maybePerformLatencyTest$1$1;->$timeNowMs:J

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/discord/stores/RtcLatencyTestResult;-><init>(Ljava/util/List;Ljava/util/List;J)V

    invoke-static {v0, v1}, Lcom/discord/stores/StoreRtcRegion;->access$updateLastTestResult(Lcom/discord/stores/StoreRtcRegion;Lcom/discord/stores/RtcLatencyTestResult;)V

    return-void
.end method
