.class public final Lcom/discord/stores/StoreVoiceParticipants$get$1$2;
.super Ljava/lang/Object;
.source "StoreVoiceParticipants.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreVoiceParticipants$get$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        "+",
        "Ljava/util/Collection<",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        ">;>;",
        "Lrx/Observable<",
        "+",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreVoiceParticipants$get$1;JLcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iput-wide p2, p0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->$guildId:J

    iput-object p4, p0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->call(Lkotlin/Pair;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lkotlin/Pair;)Lrx/Observable;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            "+",
            "Ljava/util/Collection<",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;>;)",
            "Lrx/Observable<",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    const-string v3, "otherUsers"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v4, "meUser"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream;->getVoiceStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v4

    iget-wide v5, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->$guildId:J

    iget-object v7, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v7

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/discord/stores/StoreVoiceStates;->get(JJ)Lrx/Observable;

    move-result-object v9

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream;->getVoiceSpeaking$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceSpeaking;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceSpeaking;->get()Lrx/Observable;

    move-result-object v4

    const-string/jumbo v5, "stream\n                 \u2026                   .get()"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v5, 0xfa

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v4, v5, v6, v7}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->leadingEdgeThrottle(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v10

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream;->getCalls$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreCalls;

    move-result-object v4

    iget-object v5, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-wide v5, v5, Lcom/discord/stores/StoreVoiceParticipants$get$1;->$channelId:J

    invoke-virtual {v4, v5, v6}, Lcom/discord/stores/StoreCalls;->get(J)Lrx/Observable;

    move-result-object v4

    sget-object v5, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$1;->INSTANCE:Lcom/discord/stores/StoreVoiceParticipants$get$1$2$1;

    invoke-virtual {v4, v5}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v11

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream;->getVideoStreams$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVideoStreams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreVideoStreams;->observeUserStreams()Lrx/Observable;

    move-result-object v12

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream;->getGuilds$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuilds;

    move-result-object v4

    iget-wide v5, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->$guildId:J

    invoke-virtual {v4, v5, v6}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v4

    const-wide/16 v5, 0x1

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v4, v5, v6, v7}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->leadingEdgeThrottle(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v13

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream;->getApplicationStreaming$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreaming;->getState()Lrx/Observable;

    move-result-object v14

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants;->getStream()Lcom/discord/stores/StoreStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream;->getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreMediaSettings;->getVoiceConfig()Lrx/Observable;

    move-result-object v15

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v4, v4, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    invoke-static {v4, v3}, Lcom/discord/stores/StoreVoiceParticipants;->access$getStreamContextsForUsers(Lcom/discord/stores/StoreVoiceParticipants;Ljava/util/List;)Lrx/Observable;

    move-result-object v16

    new-instance v3, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;

    invoke-direct {v3, v0, v1, v2}, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;-><init>(Lcom/discord/stores/StoreVoiceParticipants$get$1$2;Lcom/discord/models/domain/ModelUser;Ljava/util/Collection;)V

    move-object/from16 v17, v3

    invoke-static/range {v9 .. v17}, Lrx/Observable;->d(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func8;)Lrx/Observable;

    move-result-object v1

    return-object v1
.end method
