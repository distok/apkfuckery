.class public final Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
.super Ljava/lang/Object;
.source "StoreAudioDevices.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreAudioDevices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AudioDevicesState"
.end annotation


# instance fields
.field private final audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

.field private final availableOutputDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            "Ljava/util/Set<",
            "+",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            ">;)V"
        }
    .end annotation

    const-string v0, "audioOutputState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedOutputDevice"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableOutputDevices"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    iput-object p2, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    iput-object p3, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->copy(Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;)Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/utilities/media/AudioOutputState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreAudioDevices$OutputDevice;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    return-object v0
.end method

.method public final component3()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    return-object v0
.end method

.method public final copy(Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;)Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            "Ljava/util/Set<",
            "+",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            ">;)",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;"
        }
    .end annotation

    const-string v0, "audioOutputState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedOutputDevice"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableOutputDevices"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;-><init>(Lcom/discord/utilities/media/AudioOutputState;Lcom/discord/stores/StoreAudioDevices$OutputDevice;Ljava/util/Set;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    iget-object v1, p1, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    iget-object v1, p1, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    iget-object p1, p1, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    return-object v0
.end method

.method public final getAvailableOutputDevices()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/stores/StoreAudioDevices$OutputDevice;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    return-object v0
.end method

.method public final getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputState;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "AudioDevicesState(audioOutputState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedOutputDevice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->selectedOutputDevice:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", availableOutputDevices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->availableOutputDevices:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
