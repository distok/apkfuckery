.class public final Lcom/discord/stores/StoreMessagesLoader;
.super Lcom/discord/stores/Store;
.source "StoreMessagesLoader.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;,
        Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;,
        Lcom/discord/stores/StoreMessagesLoader$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreMessagesLoader$Companion;

.field public static final SCROLL_TO_LAST_UNREAD:J = 0x0L

.field public static final SCROLL_TO_LATEST:J = 0x1L


# instance fields
.field private authed:Z

.field private backgrounded:Z

.field private final channelLoadedStateSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final channelLoadedStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;",
            ">;"
        }
    .end annotation
.end field

.field private final channelMessageChunksSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;",
            ">;"
        }
    .end annotation
.end field

.field private final channelMessagesLoadingSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private delayLoadingMessagesSubscription:Lrx/Subscription;

.field private detachedChannels:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private hasConnected:Z

.field private interactionState:Lcom/discord/stores/StoreChat$InteractionState;

.field private final loadingMessagesRetryDelayDefault:J

.field private loadingMessagesRetryDelayMillis:J

.field private final loadingMessagesRetryJitter:I

.field private loadingMessagesRetryMax:I

.field private loadingMessagesSubscription:Lrx/Subscription;

.field private final messageRequestSize:I

.field private final scrollToSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private selectedChannelId:J

.field private final stream:Lcom/discord/stores/StoreStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreMessagesLoader$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreMessagesLoader$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreMessagesLoader;->Companion:Lcom/discord/stores/StoreMessagesLoader$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreStream;)V
    .locals 2

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->stream:Lcom/discord/stores/StoreStream;

    const/16 p1, 0x32

    iput p1, p0, Lcom/discord/stores/StoreMessagesLoader;->messageRequestSize:I

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    invoke-direct {p1, v0}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->channelMessageChunksSubject:Lrx/subjects/SerializedSubject;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    new-instance v0, Lrx/subjects/SerializedSubject;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    invoke-direct {v0, p1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateSubject:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    const/4 v0, 0x0

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    invoke-direct {p1, v0}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    invoke-direct {p1, v0}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->channelMessagesLoadingSubject:Lrx/subjects/SerializedSubject;

    sget-object p1, Lx/h/n;->d:Lx/h/n;

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->detachedChannels:Ljava/util/Set;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreMessagesLoader;->backgrounded:Z

    const-wide/16 v0, 0x5dc

    iput-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayDefault:J

    const/16 p1, 0x7d0

    iput p1, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryJitter:I

    iput-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayMillis:J

    const/16 p1, 0x7530

    iput p1, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryMax:I

    return-void
.end method

.method public static final synthetic access$channelLoadedStateUpdate(Lcom/discord/stores/StoreMessagesLoader;JLkotlin/jvm/functions/Function1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$getChannelMessagesLoadingSubject$p(Lcom/discord/stores/StoreMessagesLoader;)Lrx/subjects/SerializedSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelMessagesLoadingSubject:Lrx/subjects/SerializedSubject;

    return-object p0
.end method

.method public static final synthetic access$getDelayLoadingMessagesSubscription$p(Lcom/discord/stores/StoreMessagesLoader;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessagesLoader;->delayLoadingMessagesSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getLoadingMessagesSubscription$p(Lcom/discord/stores/StoreMessagesLoader;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getMessageRequestSize$p(Lcom/discord/stores/StoreMessagesLoader;)I
    .locals 0

    iget p0, p0, Lcom/discord/stores/StoreMessagesLoader;->messageRequestSize:I

    return p0
.end method

.method public static final synthetic access$getScrollToSubject$p(Lcom/discord/stores/StoreMessagesLoader;)Lrx/subjects/SerializedSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    return-object p0
.end method

.method public static final synthetic access$getSelectedChannelId$p(Lcom/discord/stores/StoreMessagesLoader;)J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    return-wide v0
.end method

.method public static final synthetic access$getStream$p(Lcom/discord/stores/StoreMessagesLoader;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessagesLoader;->stream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$handleChatDetached(Lcom/discord/stores/StoreMessagesLoader;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessagesLoader;->handleChatDetached(Ljava/util/Set;)V

    return-void
.end method

.method public static final synthetic access$handleChatInteraction(Lcom/discord/stores/StoreMessagesLoader;Lcom/discord/stores/StoreChat$InteractionState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessagesLoader;->handleChatInteraction(Lcom/discord/stores/StoreChat$InteractionState;)V

    return-void
.end method

.method public static final synthetic access$handleLoadMessagesError(Lcom/discord/stores/StoreMessagesLoader;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessagesLoader;->handleLoadMessagesError(J)V

    return-void
.end method

.method public static final synthetic access$handleLoadedMessages(Lcom/discord/stores/StoreMessagesLoader;Ljava/util/List;JJLjava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/discord/stores/StoreMessagesLoader;->handleLoadedMessages(Ljava/util/List;JJLjava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method

.method public static final synthetic access$setDelayLoadingMessagesSubscription$p(Lcom/discord/stores/StoreMessagesLoader;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->delayLoadingMessagesSubscription:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setLoadingMessagesSubscription$p(Lcom/discord/stores/StoreMessagesLoader;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesSubscription:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setSelectedChannelId$p(Lcom/discord/stores/StoreMessagesLoader;J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    return-void
.end method

.method private final declared-synchronized channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;-><init>(ZZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    const-string v1, "channelLoadedStates[chan\u2026] ?: ChannelLoadedState()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateSubject:Lrx/subjects/SerializedSubject;

    new-instance p2, Ljava/util/HashMap;

    iget-object p3, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    invoke-direct {p2, p3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final channelLoadedStatesReset()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateSubject:Lrx/subjects/SerializedSubject;

    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, v1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayDefault:J

    iput-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayMillis:J

    const-string v0, "Disconnected, resetting all message loaded states."

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreMessagesLoader;->log(Ljava/lang/String;)V

    return-void
.end method

.method private final declared-synchronized handleChatDetached(Ljava/util/Set;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->detachedChannels:Ljava/util/Set;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loaded detached channel state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessagesLoader;->log(Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleChatInteraction(Lcom/discord/stores/StoreChat$InteractionState;)V
    .locals 13

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/discord/stores/StoreChat$InteractionState;->getChannelId()J

    move-result-wide v0

    new-instance v2, Lcom/discord/stores/StoreMessagesLoader$handleChatInteraction$1$1;

    invoke-direct {v2, p1}, Lcom/discord/stores/StoreMessagesLoader$handleChatInteraction$1$1;-><init>(Lcom/discord/stores/StoreChat$InteractionState;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->interactionState:Lcom/discord/stores/StoreChat$InteractionState;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3b

    const/4 v12, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v12}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleLoadMessagesError(J)V
    .locals 10

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/discord/stores/StoreMessagesLoader$handleLoadMessagesError$1;->INSTANCE:Lcom/discord/stores/StoreMessagesLoader$handleLoadMessagesError$1;

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V

    iget-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayMillis:J

    iget v2, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryMax:I

    int-to-long v2, v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v2, 0x2

    int-to-long v2, v2

    mul-long v0, v0, v2

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    iget v3, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryJitter:I

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayMillis:J

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load messages for channel ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, "], "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "retrying in "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayMillis:J

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, "ms"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "StringBuilder()\n        \u2026}ms\")\n        .toString()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessagesLoader;->log(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-wide v1, p0, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayMillis:J

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3a

    const/4 v9, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleLoadedMessages(Ljava/util/List;JJLjava/lang/Long;Ljava/lang/Long;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;JJ",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    move-object/from16 v11, p0

    move-wide/from16 v9, p2

    move-wide/from16 v12, p4

    monitor-enter p0

    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, v11, Lcom/discord/stores/StoreMessagesLoader;->messageRequestSize:I

    const/4 v15, 0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p7, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz p6, :cond_2

    const/4 v7, 0x1

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    :goto_2
    sget-object v2, Lcom/discord/stores/StoreMessagesLoader;->Companion:Lcom/discord/stores/StoreMessagesLoader$Companion;

    invoke-virtual {v2, v12, v13}, Lcom/discord/stores/StoreMessagesLoader$Companion;->isScrollToAction(J)Z

    move-result v2

    xor-int/lit8 v16, v2, 0x1

    if-nez v16, :cond_3

    if-nez v1, :cond_3

    if-nez v7, :cond_3

    const/4 v8, 0x1

    goto :goto_3

    :cond_3
    const/4 v8, 0x0

    :goto_3
    if-nez v8, :cond_5

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    goto :goto_4

    :cond_4
    const/4 v6, 0x0

    goto :goto_5

    :cond_5
    :goto_4
    const/4 v6, 0x1

    :goto_5
    new-instance v1, Lcom/discord/stores/StoreMessagesLoader$handleLoadedMessages$1;

    invoke-direct {v1, v8, v7, v0}, Lcom/discord/stores/StoreMessagesLoader$handleLoadedMessages$1;-><init>(ZZZ)V

    invoke-direct {v11, v9, v10, v1}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V

    iget-object v1, v11, Lcom/discord/stores/StoreMessagesLoader;->channelMessagesLoadingSubject:Lrx/subjects/SerializedSubject;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iget-object v1, v1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v1, v2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    iget-object v5, v11, Lcom/discord/stores/StoreMessagesLoader;->channelMessageChunksSubject:Lrx/subjects/SerializedSubject;

    new-instance v4, Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;

    move-object v1, v4

    move-wide/from16 v2, p2

    move-object v14, v4

    move-object/from16 v4, p1

    move-object v15, v5

    move v5, v8

    move/from16 v17, v8

    move/from16 v8, v16

    invoke-direct/range {v1 .. v8}, Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;-><init>(JLjava/util/List;ZZZZ)V

    iget-object v1, v15, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v1, v14}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    const-wide/16 v1, 0x1

    if-eqz v17, :cond_6

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    if-eqz v3, :cond_7

    cmp-long v3, v12, v1

    if-nez v3, :cond_7

    const/4 v3, 0x1

    goto :goto_6

    :cond_6
    const/4 v4, 0x1

    :cond_7
    const/4 v3, 0x0

    :goto_6
    if-eqz v3, :cond_8

    iget-object v5, v11, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, v5, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v2, v1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto :goto_7

    :cond_8
    if-eqz v17, :cond_9

    iget-object v1, v11, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v1, v1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v1, v2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    :cond_9
    :goto_7
    if-eqz v16, :cond_e

    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v5

    cmp-long v7, v5, v12

    if-nez v7, :cond_b

    const/4 v5, 0x1

    goto :goto_8

    :cond_b
    const/4 v5, 0x0

    :goto_8
    if-eqz v5, :cond_a

    goto :goto_9

    :cond_c
    const/4 v2, 0x0

    :goto_9
    check-cast v2, Lcom/discord/models/domain/ModelMessage;

    if-nez v2, :cond_d

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object v1

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreReadStates;->markAsRead(Ljava/lang/Long;)V

    goto :goto_a

    :cond_d
    iget-object v1, v11, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v1, v1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v1, v2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    :cond_e
    :goto_a
    if-nez v16, :cond_f

    if-nez v3, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/discord/stores/StoreMessagesLoader;->clearScrollTo()V

    :cond_f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loaded "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " messages for channel ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "], all loaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Lcom/discord/stores/StoreMessagesLoader;->log(Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-wide v2, v11, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayDefault:J

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3a

    const/4 v10, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v10}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic handleLoadedMessages$default(Lcom/discord/stores/StoreMessagesLoader;Ljava/util/List;JJLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    .locals 10

    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    move-wide v6, v0

    goto :goto_0

    :cond_0
    move-wide v6, p4

    :goto_0
    and-int/lit8 v0, p8, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    move-object v8, v1

    goto :goto_1

    :cond_1
    move-object/from16 v8, p6

    :goto_1
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    move-object v9, v1

    goto :goto_2

    :cond_2
    move-object/from16 v9, p7

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v2 .. v9}, Lcom/discord/stores/StoreMessagesLoader;->handleLoadedMessages(Ljava/util/List;JJLjava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method

.method private final log(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[MessageLoader] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method private final declared-synchronized tryLoadMessages(JZZZLjava/lang/Long;Ljava/lang/Long;)V
    .locals 23

    move-object/from16 v9, p0

    move-wide/from16 v0, p1

    monitor-enter p0

    if-eqz p6, :cond_0

    :try_start_0
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0

    :cond_0
    iget-wide v2, v9, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    :goto_0
    move-wide v11, v2

    iget-object v2, v9, Lcom/discord/stores/StoreMessagesLoader;->delayLoadingMessagesSubscription:Lrx/Subscription;

    if-eqz v2, :cond_2

    if-eqz p5, :cond_2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    const/4 v2, 0x0

    iput-object v2, v9, Lcom/discord/stores/StoreMessagesLoader;->delayLoadingMessagesSubscription:Lrx/Subscription;

    :cond_2
    iget-object v2, v9, Lcom/discord/stores/StoreMessagesLoader;->delayLoadingMessagesSubscription:Lrx/Subscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_3

    if-nez p3, :cond_3

    monitor-exit p0

    return-void

    :cond_3
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_4

    :try_start_1
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n          .ti\u2026y, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const/4 v12, 0x0

    new-instance v0, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$1;

    invoke-direct {v0, v9}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$1;-><init>(Lcom/discord/stores/StoreMessagesLoader;)V

    const/4 v14, 0x0

    const/4 v15, 0x0

    new-instance v13, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$2;

    invoke-direct {v13, v9}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$2;-><init>(Lcom/discord/stores/StoreMessagesLoader;)V

    const/16 v17, 0x1a

    const/16 v18, 0x0

    move-object/from16 v16, v0

    invoke-static/range {v10 .. v18}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_4
    if-eqz p4, :cond_5

    :try_start_2
    iget-wide v0, v9, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayDefault:J

    iput-wide v0, v9, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesRetryDelayMillis:J

    :cond_5
    iget-object v0, v9, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;->isLoadingMessages()Z

    move-result v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v4, v1, :cond_6

    if-nez p3, :cond_6

    monitor-exit p0

    return-void

    :cond_6
    :try_start_3
    iget-object v4, v9, Lcom/discord/stores/StoreMessagesLoader;->loadingMessagesSubscription:Lrx/Subscription;

    if-eqz v4, :cond_7

    invoke-interface {v4}, Lrx/Subscription;->unsubscribe()V

    :cond_7
    cmp-long v4, v11, v2

    if-lez v4, :cond_10

    iget-boolean v4, v9, Lcom/discord/stores/StoreMessagesLoader;->backgrounded:Z

    if-nez v4, :cond_10

    iget-boolean v4, v9, Lcom/discord/stores/StoreMessagesLoader;->authed:Z

    if-nez v4, :cond_8

    goto/16 :goto_4

    :cond_8
    new-instance v10, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$3;

    invoke-direct {v10, v9}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$3;-><init>(Lcom/discord/stores/StoreMessagesLoader;)V

    new-instance v13, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;

    invoke-direct {v13, v9, v10}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;-><init>(Lcom/discord/stores/StoreMessagesLoader;Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$3;)V

    if-eqz p7, :cond_9

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xc

    const/16 v17, 0x0

    move-object/from16 v13, p7

    invoke-static/range {v10 .. v17}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$3;->invoke$default(Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$3;JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V

    goto/16 :goto_3

    :cond_9
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;->isInitialMessagesLoaded()Z

    move-result v4

    if-eq v4, v1, :cond_a

    goto/16 :goto_2

    :cond_a
    iget-object v2, v9, Lcom/discord/stores/StoreMessagesLoader;->channelMessagesLoadingSubject:Lrx/subjects/SerializedSubject;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iget-object v2, v2, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v2, v3}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    iget-object v2, v9, Lcom/discord/stores/StoreMessagesLoader;->interactionState:Lcom/discord/stores/StoreChat$InteractionState;

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/discord/stores/StoreChat$InteractionState;->isAtTop()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;->isOldestMessagesLoaded()Z

    move-result v3

    if-nez v3, :cond_b

    const/4 v3, 0x1

    goto :goto_1

    :cond_b
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2}, Lcom/discord/stores/StoreChat$InteractionState;->isAtBottom()Z

    move-result v5

    if-eqz v5, :cond_c

    iget-object v5, v9, Lcom/discord/stores/StoreMessagesLoader;->detachedChannels:Ljava/util/Set;

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v4, 0x1

    :cond_c
    invoke-virtual {v2}, Lcom/discord/stores/StoreChat$InteractionState;->getChannelId()J

    move-result-wide v5

    cmp-long v1, v5, v11

    if-nez v1, :cond_f

    if-nez v3, :cond_d

    if-eqz v4, :cond_f

    :cond_d
    sget-object v1, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$5$1;->INSTANCE:Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$5$1;

    invoke-direct {v9, v11, v12, v1}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V

    iget-object v1, v9, Lcom/discord/stores/StoreMessagesLoader;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getMessages$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessages;

    move-result-object v1

    invoke-virtual {v1, v11, v12}, Lcom/discord/stores/StoreMessages;->observeMessagesForChannel(J)Lrx/Observable;

    move-result-object v1

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x3

    const/4 v10, 0x0

    move-object/from16 p1, v1

    move-wide/from16 p2, v5

    move/from16 p4, v7

    move/from16 p5, v8

    move-object/from16 p6, v10

    invoke-static/range {p1 .. p6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    invoke-static {v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v14

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v20, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;

    move-object/from16 v1, v20

    move v2, v3

    move v3, v4

    move-object/from16 v4, p0

    move-object v5, v0

    move-wide v6, v11

    move-object v8, v13

    invoke-direct/range {v1 .. v8}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1;-><init>(ZZLcom/discord/stores/StoreMessagesLoader;Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;JLcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;)V

    const/16 v19, 0x0

    new-instance v18, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$2;

    move-object/from16 p1, v18

    move-object/from16 p2, p0

    move-object/from16 p3, v0

    move-wide/from16 p4, v11

    move-object/from16 p6, v13

    invoke-direct/range {p1 .. p6}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$2;-><init>(Lcom/discord/stores/StoreMessagesLoader;Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;JLcom/discord/stores/StoreMessagesLoader$tryLoadMessages$4;)V

    const/16 v21, 0x16

    const/16 v22, 0x0

    invoke-static/range {v14 .. v22}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_3

    :cond_e
    :goto_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xc

    const/16 v17, 0x0

    invoke-static/range {v10 .. v17}, Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$3;->invoke$default(Lcom/discord/stores/StoreMessagesLoader$tryLoadMessages$3;JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_f
    :goto_3
    monitor-exit p0

    return-void

    :cond_10
    :goto_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    .locals 8

    and-int/lit8 v0, p8, 0x1

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    move-wide v0, p1

    :goto_0
    and-int/lit8 v2, p8, 0x2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    move v2, p3

    :goto_1
    and-int/lit8 v4, p8, 0x4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    move v4, p4

    :goto_2
    and-int/lit8 v5, p8, 0x8

    if-eqz v5, :cond_3

    goto :goto_3

    :cond_3
    move v3, p5

    :goto_3
    and-int/lit8 v5, p8, 0x10

    const/4 v6, 0x0

    if-eqz v5, :cond_4

    move-object v5, v6

    goto :goto_4

    :cond_4
    move-object v5, p6

    :goto_4
    and-int/lit8 v7, p8, 0x20

    if-eqz v7, :cond_5

    goto :goto_5

    :cond_5
    move-object v6, p7

    :goto_5
    move-wide p1, v0

    move p3, v2

    move p4, v4

    move p5, v3

    move-object p6, v5

    move-object p7, v6

    invoke-direct/range {p0 .. p7}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages(JZZZLjava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public final clearScrollTo()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final get()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelMessageChunksSubject:Lrx/subjects/SerializedSubject;

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getMessagesLoadedState(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateSubject:Lrx/subjects/SerializedSubject;

    new-instance v1, Lcom/discord/stores/StoreMessagesLoader$getMessagesLoadedState$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreMessagesLoader$getMessagesLoadedState$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "channelLoadedStateSubjec\u2026?: ChannelLoadedState() }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "channelLoadedStateSubjec\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getScrollTo()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    sget-object v1, Lcom/discord/stores/StoreMessagesLoader$getScrollTo$1;->INSTANCE:Lcom/discord/stores/StoreMessagesLoader$getScrollTo$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "scrollToSubject\n      .filter { it != null }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized handleAuthToken(Ljava/lang/String;)V
    .locals 10

    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    :try_start_0
    iput-boolean p1, p0, Lcom/discord/stores/StoreMessagesLoader;->authed:Z

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized handleBackgrounded(Z)V
    .locals 10

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/discord/stores/StoreMessagesLoader;->backgrounded:Z

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized handleChannelSelected(J)V
    .locals 13
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    sget-object v2, Lcom/discord/stores/StoreMessagesLoader$handleChannelSelected$1;->INSTANCE:Lcom/discord/stores/StoreMessagesLoader$handleChannelSelected$1;

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V

    iput-wide p1, p0, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x39

    const/4 v12, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v12}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized handleConnected(Z)V
    .locals 10

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    iget-boolean p1, p0, Lcom/discord/stores/StoreMessagesLoader;->hasConnected:Z

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStatesReset()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreMessagesLoader;->hasConnected:Z

    :cond_1
    :goto_0
    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/discord/stores/StoreMessagesLoader;->hasConnected:Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x33

    const/4 v9, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public init(Landroid/content/Context;)V
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getChat$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChat;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreChat;->getInteractionState()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreMessagesLoader;

    new-instance v6, Lcom/discord/stores/StoreMessagesLoader$init$1;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreMessagesLoader$init$1;-><init>(Lcom/discord/stores/StoreMessagesLoader;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreMessagesLoader;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getMessages$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessages;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreMessages;->getAllDetached()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreMessagesLoader;

    new-instance v6, Lcom/discord/stores/StoreMessagesLoader$init$2;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreMessagesLoader$init$2;-><init>(Lcom/discord/stores/StoreMessagesLoader;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final declared-synchronized jumpToMessage(JJ)V
    .locals 11

    monitor-enter p0

    const-wide/16 v0, 0x0

    cmp-long v2, p3, v0

    if-gtz v2, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    new-instance v0, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$1;

    move-object v3, v0

    move-object v4, p0

    move-wide v5, p1

    move-wide v7, p3

    invoke-direct/range {v3 .. v8}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$1;-><init>(Lcom/discord/stores/StoreMessagesLoader;JJ)V

    new-instance v1, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;-><init>(Lcom/discord/stores/StoreMessagesLoader;J)V

    iget-object v2, p0, Lcom/discord/stores/StoreMessagesLoader;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getChannelsSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreChannelsSelected;->observeId()Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$3;

    invoke-direct {v3, p1, p2}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$3;-><init>(J)V

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-wide/16 v5, 0x3e8

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v3, v4, v5, v6, v7}, Lf/a/b/r;->c(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Lrx/Observable$c;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v2

    new-instance v9, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$4;

    move-object v3, v9

    move-object v4, p0

    move-wide v5, p1

    move-wide v7, p3

    invoke-direct/range {v3 .. v8}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$4;-><init>(Lcom/discord/stores/StoreMessagesLoader;JJ)V

    invoke-virtual {v2, v9}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    const-string/jumbo p3, "stream\n        .channels\u2026lId, messageId)\n        }"

    invoke-static {v3, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object p3

    invoke-static {p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$5;

    invoke-direct {v8, v0}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$5;-><init>(Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$1;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p3, p0, Lcom/discord/stores/StoreMessagesLoader;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream;->getConnectionOpen$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreConnectionOpen;

    move-result-object p3

    const/4 p4, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-static {p3, p4, v2, v0}, Lcom/discord/stores/StoreConnectionOpen;->observeConnectionOpen$default(Lcom/discord/stores/StoreConnectionOpen;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p3

    invoke-virtual {p3, v2}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p3

    new-instance p4, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$6;

    invoke-direct {p4, p0, p1, p2}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$6;-><init>(Lcom/discord/stores/StoreMessagesLoader;J)V

    invoke-virtual {p3, p4}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo p2, "stream\n        .connecti\u2026mes.ONE_SECOND)\n        }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$7;->INSTANCE:Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$7;

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$8;

    invoke-direct {v8, v1}, Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$8;-><init>(Lcom/discord/stores/StoreMessagesLoader$jumpToMessage$2;)V

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final observeChannelMessagesLoading()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelMessagesLoadingSubject:Lrx/subjects/SerializedSubject;

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "channelMessagesLoadingSu\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final declared-synchronized requestNewestMessages()V
    .locals 15

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->detachedChannels:Ljava/util/Set;

    iget-wide v1, p0, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v1, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStates:Ljava/util/HashMap;

    iget-wide v3, p0, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;->isInitialMessagesLoaded()Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMessagesLoader;->scrollToSubject:Lrx/subjects/SerializedSubject;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, v1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-wide v3, p0, Lcom/discord/stores/StoreMessagesLoader;->selectedChannelId:J

    sget-object v0, Lcom/discord/stores/StoreMessagesLoader$requestNewestMessages$1;->INSTANCE:Lcom/discord/stores/StoreMessagesLoader$requestNewestMessages$1;

    invoke-direct {p0, v3, v4, v0}, Lcom/discord/stores/StoreMessagesLoader;->channelLoadedStateUpdate(JLkotlin/jvm/functions/Function1;)V

    const-wide/16 v6, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    const/16 v13, 0x19

    const/4 v14, 0x0

    move-object v5, p0

    invoke-static/range {v5 .. v14}, Lcom/discord/stores/StoreMessagesLoader;->tryLoadMessages$default(Lcom/discord/stores/StoreMessagesLoader;JZZZLjava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
