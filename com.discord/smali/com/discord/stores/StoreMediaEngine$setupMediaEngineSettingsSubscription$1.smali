.class public final Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1;
.super Lx/m/c/k;
.source "StoreMediaEngine.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMediaEngine;->setupMediaEngineSettingsSubscription()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreMediaEngine;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMediaEngine;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1;->this$0:Lcom/discord/stores/StoreMediaEngine;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1;->invoke(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V
    .locals 2

    const-string/jumbo v0, "voiceConfig"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1;->this$0:Lcom/discord/stores/StoreMediaEngine;

    invoke-static {v0}, Lcom/discord/stores/StoreMediaEngine;->access$getDispatcher$p(Lcom/discord/stores/StoreMediaEngine;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1$1;-><init>(Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
