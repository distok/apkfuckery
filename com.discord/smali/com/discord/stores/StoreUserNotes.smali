.class public final Lcom/discord/stores/StoreUserNotes;
.super Ljava/lang/Object;
.source "StoreUserNotes.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreUserNotes$UserNoteState;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final notesByUserId:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreUserNotes$UserNoteState;",
            ">;"
        }
    .end annotation
.end field

.field private final notesByUserIdPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreUserNotes$UserNoteState;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreUserNotes;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserIdPublisher:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreUserNotes;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreUserNotes;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$handleRequestUserNoteError(Lcom/discord/stores/StoreUserNotes;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreUserNotes;->handleRequestUserNoteError(J)V

    return-void
.end method

.method public static final synthetic access$handleRequestUserNoteSuccess(Lcom/discord/stores/StoreUserNotes;Lcom/discord/models/domain/ModelUserNote;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserNotes;->handleRequestUserNoteSuccess(Lcom/discord/models/domain/ModelUserNote;)V

    return-void
.end method

.method public static final synthetic access$loadNote(Lcom/discord/stores/StoreUserNotes;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreUserNotes;->loadNote(J)V

    return-void
.end method

.method private final handleRequestUserNoteError(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreUserNotes$UserNoteState$Empty;->INSTANCE:Lcom/discord/stores/StoreUserNotes$UserNoteState$Empty;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final handleRequestUserNoteSuccess(Lcom/discord/models/domain/ModelUserNote;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserNote;->getNoteUserId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserNote;->getNote()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/stores/StoreUserNotes;->updateNote(JLjava/lang/String;)V

    return-void
.end method

.method private final loadNote(J)V
    .locals 11
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loading;->INSTANCE:Lcom/discord/stores/StoreUserNotes$UserNoteState$Loading;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->getUserNote(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lf/a/b/r;->f(ZI)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v2

    const-string v0, "RestAPI\n          .api\n \u2026ormers.restSubscribeOn())"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/stores/StoreUserNotes;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v8, Lcom/discord/stores/StoreUserNotes$loadNote$1;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreUserNotes$loadNote$1;-><init>(Lcom/discord/stores/StoreUserNotes;)V

    const/4 v7, 0x0

    new-instance v6, Lcom/discord/stores/StoreUserNotes$loadNote$2;

    invoke-direct {v6, p0, p1, p2}, Lcom/discord/stores/StoreUserNotes$loadNote$2;-><init>(Lcom/discord/stores/StoreUserNotes;J)V

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private final updateNote(JLjava/lang/String;)V
    .locals 4

    if-eqz p3, :cond_1

    invoke-static {p3}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    iget-object p3, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreUserNotes$UserNoteState$Empty;->INSTANCE:Lcom/discord/stores/StoreUserNotes$UserNoteState$Empty;

    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;

    new-instance v3, Lcom/discord/models/domain/ModelUserNote;

    invoke-direct {v3, p1, p2, p3}, Lcom/discord/models/domain/ModelUserNote;-><init>(JLjava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;-><init>(Lcom/discord/models/domain/ModelUserNote;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    return-void
.end method


# virtual methods
.method public final handleConnectionOpen()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public final handleNoteUpdate(Lcom/discord/models/domain/ModelUserNote$Update;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "update"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserNote$Update;->getId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserNote$Update;->getNote()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/stores/StoreUserNotes;->updateNote(JLjava/lang/String;)V

    return-void
.end method

.method public final observeUserNote(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreUserNotes$UserNoteState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserNotes;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreUserNotes$observeUserNote$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreUserNotes$observeUserNote$1;-><init>(Lcom/discord/stores/StoreUserNotes;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v0, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserIdPublisher:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreUserNotes$observeUserNote$2;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreUserNotes$observeUserNote$2;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "notesByUserIdPublisher\n \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onDispatchEnded()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserId:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/discord/stores/StoreUserNotes;->notesByUserIdPublisher:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
