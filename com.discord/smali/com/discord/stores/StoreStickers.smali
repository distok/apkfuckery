.class public final Lcom/discord/stores/StoreStickers;
.super Lcom/discord/stores/StoreV2;
.source "StoreStickers.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreStickers$OwnedStickerPackState;,
        Lcom/discord/stores/StoreStickers$StickerPackState;,
        Lcom/discord/stores/StoreStickers$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreStickers$Companion;

.field private static final MAX_FREQUENTLY_USED_STICKERS:I = 0x14

.field private static final STICKER_DIRECTORY_LAYOUT_ID:J = 0xa86ab96a2c20028L


# instance fields
.field private final api:Lcom/discord/utilities/rest/RestAPI;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final frecency:Lcom/discord/utilities/media/MediaFrecencyTracker;

.field private final frecencyCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Lcom/discord/utilities/media/MediaFrecencyTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

.field private ownedStickerPackStateSnapshot:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

.field private final purchasingStickerPacks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private stickerPacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            ">;"
        }
    .end annotation
.end field

.field private stickerPacksSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            ">;"
        }
    .end annotation
.end field

.field private stickersSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation
.end field

.field private stickersStoreDirectoryLayout:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation
.end field

.field private stickersStoreDirectoryLayoutSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation
.end field

.field private final viewedPurchaseablePacksCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final viewedPurchaseablePacksCacheChatInput:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreStickers$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreStickers$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreStickers;->Companion:Lcom/discord/stores/StoreStickers$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/time/Clock;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "api"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->api:Lcom/discord/utilities/rest/RestAPI;

    iput-object p3, p0, Lcom/discord/stores/StoreStickers;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    iput-object p4, p0, Lcom/discord/stores/StoreStickers;->clock:Lcom/discord/utilities/time/Clock;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickerPacksSnapshot:Ljava/util/Map;

    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->purchasingStickerPacks:Ljava/util/Set;

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickersSnapshot:Ljava/util/Map;

    sget-object p2, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Uninitialized;->INSTANCE:Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Uninitialized;

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->ownedStickerPackStateSnapshot:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    new-instance p2, Lcom/discord/utilities/persister/Persister;

    new-instance p3, Lcom/discord/utilities/media/MediaFrecencyTracker;

    invoke-direct {p3}, Lcom/discord/utilities/media/MediaFrecencyTracker;-><init>()V

    const-string p4, "STICKER_HISTORY_V1"

    invoke-direct {p2, p4, p3}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->frecencyCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {p2}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/utilities/media/MediaFrecencyTracker;

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->frecency:Lcom/discord/utilities/media/MediaFrecencyTracker;

    new-instance p2, Lcom/discord/utilities/persister/Persister;

    const-string p3, "CACHE_KEY_VIEWED_PURCHASEABLE_STICKER_PACKS_V3"

    invoke-direct {p2, p3, p1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->viewedPurchaseablePacksCache:Lcom/discord/utilities/persister/Persister;

    new-instance p2, Lcom/discord/utilities/persister/Persister;

    const-string p3, "CACHE_KEY_VIEWED_PURCHASEABLE_STICKER_PACKS_CHAT_INPUT_V2"

    invoke-direct {p2, p3, p1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/stores/StoreStickers;->viewedPurchaseablePacksCacheChatInput:Lcom/discord/utilities/persister/Persister;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayout:Ljava/util/List;

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayoutSnapshot:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/time/Clock;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    sget-object p2, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p2}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p2

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p3

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p4

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreStickers;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/time/Clock;)V

    return-void
.end method

.method public static final synthetic access$getApi$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/rest/RestAPI;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->api:Lcom/discord/utilities/rest/RestAPI;

    return-object p0
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/time/Clock;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getFrecency$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/media/MediaFrecencyTracker;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->frecency:Lcom/discord/utilities/media/MediaFrecencyTracker;

    return-object p0
.end method

.method public static final synthetic access$getFrecencyCache$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->frecencyCache:Lcom/discord/utilities/persister/Persister;

    return-object p0
.end method

.method public static final synthetic access$getOwnedStickerPackState$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/stores/StoreStickers$OwnedStickerPackState;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    return-object p0
.end method

.method public static final synthetic access$getPurchasingStickerPacks$p(Lcom/discord/stores/StoreStickers;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->purchasingStickerPacks:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic access$getStickerPacks$p(Lcom/discord/stores/StoreStickers;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getStickersSnapshot$p(Lcom/discord/stores/StoreStickers;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->stickersSnapshot:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getStickersStoreDirectoryLayout$p(Lcom/discord/stores/StoreStickers;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayout:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getViewedPurchaseablePacksCache$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->viewedPurchaseablePacksCache:Lcom/discord/utilities/persister/Persister;

    return-object p0
.end method

.method public static final synthetic access$getViewedPurchaseablePacksCacheChatInput$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStickers;->viewedPurchaseablePacksCacheChatInput:Lcom/discord/utilities/persister/Persister;

    return-object p0
.end method

.method public static final synthetic access$setOwnedStickerPackState$p(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    return-void
.end method

.method public static final synthetic access$setStickerPacks$p(Lcom/discord/stores/StoreStickers;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$setStickersSnapshot$p(Lcom/discord/stores/StoreStickers;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickersSnapshot:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$setStickersStoreDirectoryLayout$p(Lcom/discord/stores/StoreStickers;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayout:Ljava/util/List;

    return-void
.end method

.method public static synthetic cacheViewedPurchaseableStickerPacks$default(Lcom/discord/stores/StoreStickers;Ljava/util/Set;ZILjava/lang/Object;)V
    .locals 2

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayoutSnapshot:Ljava/util/List;

    new-instance p4, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p4, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {p4}, Lx/h/f;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    :cond_1
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_2

    const/4 p2, 0x0

    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreStickers;->cacheViewedPurchaseableStickerPacks(Ljava/util/Set;Z)V

    return-void
.end method

.method public static synthetic fetchStickerStoreDirectory$default(Lcom/discord/stores/StoreStickers;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreStickers;->fetchStickerStoreDirectory(Z)V

    return-void
.end method

.method private final getStickerPackBySkuId(J)Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 6

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacksSnapshot:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/StoreStickers$StickerPackState;

    instance-of v4, v2, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    if-eqz v4, :cond_0

    check-cast v2, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v3

    :cond_0
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/discord/models/sticker/dto/ModelStickerPack;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getSkuId()J

    move-result-wide v4

    cmp-long v2, v4, p1

    if-nez v2, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    move-object v3, v1

    :cond_4
    check-cast v3, Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v3
.end method

.method public static synthetic observeNewStickerPacks$default(Lcom/discord/stores/StoreStickers;ZILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreStickers;->observeNewStickerPacks(Z)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final observeViewedPurchaseableStickerPacksChatInput()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->viewedPurchaseablePacksCacheChatInput:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final cacheViewedPurchaseableStickerPacks(Ljava/util/Set;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "newStickerPackIdsSeen"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;-><init>(Lcom/discord/stores/StoreStickers;Ljava/util/Set;Z)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final claimFreePack(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v3, p1

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    const-string v2, "pack"

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "userPremiumTier"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "onSuccess"

    move-object/from16 v4, p3

    invoke-static {v4, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "onError"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {v2, v3, v0}, Lcom/discord/utilities/dsti/StickerUtils;->isStickerPackFreeForPremiumTier(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSku;->getId()J

    move-result-wide v7

    iget-object v0, v6, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v2, Lcom/discord/stores/StoreStickers$claimFreePack$1;

    invoke-direct {v2, v6, v3}, Lcom/discord/stores/StoreStickers$claimFreePack$1;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    invoke-virtual {v0, v2}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    new-instance v2, Lcom/discord/restapi/RestAPIParams$EmptyBody;

    invoke-direct {v2}, Lcom/discord/restapi/RestAPIParams$EmptyBody;-><init>()V

    invoke-virtual {v0, v7, v8, v2}, Lcom/discord/utilities/rest/RestAPI;->claimSku(JLcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    invoke-static {v0, v2, v5, v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v10

    const-class v11, Lcom/discord/stores/StoreStickers;

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Lcom/discord/stores/StoreStickers$claimFreePack$2;

    invoke-direct {v14, v6, v1, v3}, Lcom/discord/stores/StoreStickers$claimFreePack$2;-><init>(Lcom/discord/stores/StoreStickers;Lkotlin/jvm/functions/Function0;Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    const/4 v15, 0x0

    new-instance v16, Lcom/discord/stores/StoreStickers$claimFreePack$3;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    move-wide v4, v7

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreStickers$claimFreePack$3;-><init>(Lcom/discord/stores/StoreStickers;Lkotlin/jvm/functions/Function0;Lcom/discord/models/sticker/dto/ModelStickerPack;J)V

    const/16 v17, 0x16

    const/16 v18, 0x0

    invoke-static/range {v10 .. v18}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final completePurchasingStickerPack(J)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreStickers;->getStickerPackBySkuId(J)Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v0, Lcom/discord/stores/StoreStickers$completePurchasingStickerPack$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/stores/StoreStickers$completePurchasingStickerPack$1;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    invoke-virtual {p2, v0}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method public final fetchOwnedStickerPacks()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;-><init>(Lcom/discord/stores/StoreStickers;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final fetchStickerPack(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreStickers$fetchStickerPack$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreStickers$fetchStickerPack$1;-><init>(Lcom/discord/stores/StoreStickers;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final fetchStickerStoreDirectory(Z)V
    .locals 12

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->api:Lcom/discord/utilities/rest/RestAPI;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Locale.getDefault().toString()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide v2, 0xa86ab96a2c20028L

    invoke-virtual {v0, v2, v3, p1, v1}, Lcom/discord/utilities/rest/RestAPI;->getStickerStoreDirectoryLayoutV2(JZLjava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/stores/StoreStickers;

    new-instance v9, Lcom/discord/stores/StoreStickers$fetchStickerStoreDirectory$1;

    invoke-direct {v9, p0}, Lcom/discord/stores/StoreStickers$fetchStickerStoreDirectory$1;-><init>(Lcom/discord/stores/StoreStickers;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getOwnedPacks()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->ownedStickerPackStateSnapshot:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    return-object v0
.end method

.method public final getStickerPack(J)Lcom/discord/stores/StoreStickers$StickerPackState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacksSnapshot:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreStickers$StickerPackState;

    return-object p1
.end method

.method public final getStickerPacks()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacksSnapshot:Ljava/util/Map;

    return-object v0
.end method

.method public final getStickersStoreDirectoryLayout()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayoutSnapshot:Ljava/util/List;

    return-object v0
.end method

.method public final handleNewLoadedStickerPacks(Ljava/util/List;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newStickerPacks"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    invoke-static {v0}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/sticker/dto/ModelStickerPack;

    iget-object v2, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/StoreStickers$StickerPackState;

    if-eqz v2, :cond_1

    instance-of v3, v2, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    if-eqz v3, :cond_1

    check-cast v2, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    move-result-object v2

    if-nez v2, :cond_0

    :cond_1
    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    invoke-direct {v3, v1}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iput-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleNewLoadingStickerPacks(Ljava/util/List;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "stickerPackIds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    invoke-static {v0}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreStickers$StickerPackState$Loading;->INSTANCE:Lcom/discord/stores/StoreStickers$StickerPackState$Loading;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iput-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleNewStickerStoreDirectory(Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "stickerStoreDirectory"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;->getStickerPacks()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreStickers;->handleNewLoadedStickerPacks(Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;->getStickerPacks()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    const/16 v1, 0x10

    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getSkuId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;->getStoreDirectoryLayout()Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;->getAllSkus()Ljava/util/List;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/sticker/dto/ModelStickerPack;

    if-eqz v1, :cond_2

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iput-object v0, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayout:Ljava/util/List;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handlePreLogout()V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->frecencyCache:Lcom/discord/utilities/persister/Persister;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/persister/Persister;->clear$default(Lcom/discord/utilities/persister/Persister;ZILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final handlePurchasingStickerPack(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->purchasingStickerPacks:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handlePurchasingStickerPackFailure(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->purchasingStickerPacks:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handlePurchasingStickerPackSuccess(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->purchasingStickerPacks:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleUserStickerPackUpdate()V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreStickers;->fetchOwnedStickerPacks()V

    return-void
.end method

.method public final init()V
    .locals 3

    sget-object v0, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/discord/stores/StoreStickers;->fetchStickerStoreDirectory$default(Lcom/discord/stores/StoreStickers;ZILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final observeFrequentlyUsedStickers()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->frecencyCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStickers$observeFrequentlyUsedStickers$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStickers$observeFrequentlyUsedStickers$1;-><init>(Lcom/discord/stores/StoreStickers;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "frecencyCache.getObserva\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeNewStickerPacks(Z)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/Observable<",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/stores/StoreStickers;->observeViewedPurchaseableStickerPacksChatInput()Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreStickers;->observeViewedPurchaseableStickerPacks()Lrx/Observable;

    move-result-object p1

    :goto_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreStickers;->observeStickerStoreDirectoryLayout()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreStickers$observeNewStickerPacks$1;->INSTANCE:Lcom/discord/stores/StoreStickers$observeNewStickerPacks$1;

    invoke-static {p1, v0, v1}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable.combineLatest\u2026 }.distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeOwnedStickerPacks()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreStickers$observeOwnedStickerPacks$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreStickers$observeOwnedStickerPacks$1;-><init>(Lcom/discord/stores/StoreStickers;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observePurchasingStickerPacks()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreStickers$observePurchasingStickerPacks$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreStickers$observePurchasingStickerPacks$1;-><init>(Lcom/discord/stores/StoreStickers;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeStickerPack(J)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreStickers$observeStickerPack$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreStickers$observeStickerPack$1;-><init>(Lcom/discord/stores/StoreStickers;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v2, p0, Lcom/discord/stores/StoreStickers;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x1

    new-array v3, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p0, v3, v0

    new-instance v7, Lcom/discord/stores/StoreStickers$observeStickerPack$2;

    invoke-direct {v7, p0, p1, p2}, Lcom/discord/stores/StoreStickers$observeStickerPack$2;-><init>(Lcom/discord/stores/StoreStickers;J)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final observeStickerPacks()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreStickers$observeStickerPacks$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreStickers$observeStickerPacks$1;-><init>(Lcom/discord/stores/StoreStickers;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeStickerStoreDirectoryLayout()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelStickerPack;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreStickers$observeStickerStoreDirectoryLayout$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreStickers$observeStickerStoreDirectoryLayout$1;-><init>(Lcom/discord/stores/StoreStickers;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeViewedPurchaseableStickerPacks()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->viewedPurchaseablePacksCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final onStickerUsed(Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 2

    const-string/jumbo v0, "sticker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreStickers$onStickerUsed$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreStickers$onStickerUsed$1;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/models/sticker/dto/ModelSticker;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final showStickerPackActivatedDialog(J)V
    .locals 18

    invoke-direct/range {p0 .. p2}, Lcom/discord/stores/StoreStickers;->getStickerPackBySkuId(J)Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v1

    new-instance v15, Lcom/discord/stores/StoreNotices$Notice;

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v10, 0x0

    new-instance v14, Lcom/discord/stores/StoreStickers$showStickerPackActivatedDialog$1;

    invoke-direct {v14, v0}, Lcom/discord/stores/StoreStickers$showStickerPackActivatedDialog$1;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    const/16 v0, 0x36

    const/16 v16, 0x0

    const-string v3, "STICKER_PACK_ACTIVATED_DIALOG"

    move-object v2, v15

    move-object/from16 v17, v15

    move v15, v0

    invoke-direct/range {v2 .. v16}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    :cond_0
    return-void
.end method

.method public snapshotData()V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStickers;->ownedStickerPackState:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    instance-of v1, v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    new-instance v2, Ljava/util/HashMap;

    check-cast v0, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;->getOwnedStickerPacks()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v2}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;-><init>(Ljava/util/Map;)V

    move-object v0, v1

    :cond_0
    iput-object v0, p0, Lcom/discord/stores/StoreStickers;->ownedStickerPackStateSnapshot:Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreStickers;->stickerPacks:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreStickers;->stickerPacksSnapshot:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    if-eqz v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v0}, Lf/h/a/f/f/n/g;->flatten(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_4

    const/16 v1, 0x10

    :cond_4
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    iput-object v2, p0, Lcom/discord/stores/StoreStickers;->stickersSnapshot:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayout:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/discord/stores/StoreStickers;->stickersStoreDirectoryLayoutSnapshot:Ljava/util/List;

    return-void
.end method

.method public final startPurchasingStickerPack(J)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreStickers;->getStickerPackBySkuId(J)Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/discord/stores/StoreStickers;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v0, Lcom/discord/stores/StoreStickers$startPurchasingStickerPack$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/stores/StoreStickers$startPurchasingStickerPack$1;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    invoke-virtual {p2, v0}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method
