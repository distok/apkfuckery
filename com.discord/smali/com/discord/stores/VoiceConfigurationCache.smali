.class public final Lcom/discord/stores/VoiceConfigurationCache;
.super Ljava/lang/Object;
.source "VoiceConfigurationCache.kt"


# instance fields
.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1

    const-string v0, "sharedPreferences"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method private final readNoiseProcessing()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->Companion:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_NOISE_SUPPRESSION()Z

    move-result v2

    const-string v3, "CACHE_KEY_VOICE_SETTINGS_NOISE_SUPPRESSION"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v2, p0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_NOISE_CANCELLATION()Z

    move-result v1

    const-string v3, "CACHE_KEY_VOICE_SETTINGS_NOISE_CANCELLATION"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Cancellation:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Suppression:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->None:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final read()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 23

    move-object/from16 v0, p0

    new-instance v16, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->Companion:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted()Z

    move-result v3

    const-string v4, "CACHE_KEY_VOICE_SETTINGS_IS_MUTED"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened()Z

    move-result v4

    const-string v5, "CACHE_KEY_VOICE_SETTINGS_IS_DEAFENED"

    invoke-interface {v1, v5, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getAutomaticVad()Z

    move-result v5

    const-string v6, "CACHE_KEY_VOICE_SETTINGS_AUTOMATIC_VAD"

    invoke-interface {v1, v6, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getVadUseKrisp()Z

    move-result v6

    const-string v7, "CACHE_KEY_VOICE_SETTINGS_VAD_USE_KRISP"

    invoke-interface {v1, v7, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getAutomaticGainControl()Z

    move-result v7

    const-string v8, "CACHE_KEY_VOICE_SETTINGS_AUTOMATIC_GAIN_CONTROL"

    invoke-interface {v1, v8, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v8

    invoke-virtual {v8}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getEchoCancellation()Z

    move-result v8

    const-string v9, "CACHE_KEY_VOICE_SETTINGS_ECHO_CANCELLATION"

    invoke-interface {v1, v9, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-direct/range {p0 .. p0}, Lcom/discord/stores/VoiceConfigurationCache;->readNoiseProcessing()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    move-result-object v9

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v10

    invoke-virtual {v10}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getSensitivity()F

    move-result v10

    const-string v11, "CACHE_KEY_VOICE_SETTINGS_SENSITIVITY"

    invoke-interface {v1, v11, v10}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v10

    sget-object v1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->Companion:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode$a;

    iget-object v11, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    sget-object v12, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->VOICE_ACTIVITY:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    invoke-virtual {v12}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->getNumeral()I

    move-result v13

    const-string v14, "CACHE_KEY_VOICE_SETTINGS_INPUT_MODE"

    invoke-interface {v11, v14, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    if-eq v11, v1, :cond_1

    const/4 v1, 0x2

    if-eq v11, v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->PUSH_TO_TALK:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    move-object v12, v1

    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v11

    invoke-virtual {v11}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getOutputVolume()F

    move-result v11

    const-string v13, "CACHE_KEY_VOICE_SETTINGS_OUTPUT_VOLUME"

    invoke-interface {v1, v13, v11}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v11

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    const/16 v19, 0x0

    sget-object v20, Lcom/discord/stores/VoiceConfigurationCache$read$1;->INSTANCE:Lcom/discord/stores/VoiceConfigurationCache$read$1;

    const/16 v21, 0x2

    const/16 v22, 0x0

    const-string v18, "MUTED_USERS_V2"

    move-object/from16 v17, v1

    invoke-static/range {v17 .. v22}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->getStringEntrySetAsMap$default(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v13

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    sget-object v20, Lcom/discord/stores/VoiceConfigurationCache$read$2;->INSTANCE:Lcom/discord/stores/VoiceConfigurationCache$read$2;

    const-string v18, "USER_OUTPUT_VOLUMES_V2"

    move-object/from16 v17, v1

    invoke-static/range {v17 .. v22}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->getStringEntrySetAsMap$default(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v14

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v15

    invoke-virtual {v15}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getEnableVideoHardwareScaling()Z

    move-result v15

    move-object/from16 v17, v14

    const-string v14, "VIDEO_ENABLE_HARDWARE_SCALING"

    invoke-interface {v1, v14, v15}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    iget-object v1, v0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;->getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getVoiceParticipantsHidden()Z

    move-result v2

    const-string v15, "CACHE_KEY_HIDE_VOICE_PARTICIPANTS"

    invoke-interface {v1, v15, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    move-object/from16 v1, v16

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move-object v8, v9

    move v9, v10

    move-object v10, v12

    move-object v12, v13

    move-object/from16 v13, v17

    invoke-direct/range {v1 .. v15}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;-><init>(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZ)V

    return-object v16
.end method

.method public final write(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V
    .locals 2

    const-string/jumbo v0, "voiceConfiguration"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/VoiceConfigurationCache;->sharedPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Lcom/discord/stores/VoiceConfigurationCache$write$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/VoiceConfigurationCache$write$1;-><init>(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V

    invoke-static {v0, v1}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
