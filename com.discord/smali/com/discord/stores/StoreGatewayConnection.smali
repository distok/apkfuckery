.class public final Lcom/discord/stores/StoreGatewayConnection;
.super Ljava/lang/Object;
.source "StoreGatewayConnection.kt"

# interfaces
.implements Lcom/discord/gateway/GatewayEventHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGatewayConnection$ClientState;
    }
.end annotation


# instance fields
.field private final callCreateOrUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelCall;",
            "Lcom/discord/models/domain/ModelCall;",
            ">;"
        }
    .end annotation
.end field

.field private final callDelete:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelCall;",
            "Lcom/discord/models/domain/ModelCall;",
            ">;"
        }
    .end annotation
.end field

.field private final channelCreateOrUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation
.end field

.field private final channelDeleted:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation
.end field

.field private final channelRecipientAdd:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            ">;"
        }
    .end annotation
.end field

.field private final channelRecipientRemove:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            ">;"
        }
    .end annotation
.end field

.field private final channelUnreadUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannelUnreadUpdate;",
            "Lcom/discord/models/domain/ModelChannelUnreadUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private clientState:Lcom/discord/stores/StoreGatewayConnection$ClientState;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final connected:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionReady:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final gatewaySocketLogger:Lcom/discord/utilities/logging/AppGatewaySocketLogger;

.field private final guildApplicationCommands:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;",
            "Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;",
            ">;"
        }
    .end annotation
.end field

.field private final guildBanAdd:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelBan;",
            "Lcom/discord/models/domain/ModelBan;",
            ">;"
        }
    .end annotation
.end field

.field private final guildBanRemove:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelBan;",
            "Lcom/discord/models/domain/ModelBan;",
            ">;"
        }
    .end annotation
.end field

.field private final guildCreateOrUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation
.end field

.field private final guildDeleted:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation
.end field

.field private final guildEmojisUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;",
            "Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final guildIntegrationsUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildIntegration$Update;",
            "Lcom/discord/models/domain/ModelGuildIntegration$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final guildMemberListUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMemberListUpdate;",
            "Lcom/discord/models/domain/ModelGuildMemberListUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private final guildMemberRemove:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMember;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;"
        }
    .end annotation
.end field

.field private final guildMembersAdd:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMember;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;"
        }
    .end annotation
.end field

.field private final guildMembersChunk:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMember$Chunk;",
            "Lcom/discord/models/domain/ModelGuildMember$Chunk;",
            ">;"
        }
    .end annotation
.end field

.field private final guildRoleCreateOrUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            ">;"
        }
    .end annotation
.end field

.field private final guildRoleDelete:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            ">;"
        }
    .end annotation
.end field

.field private final messageAck:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelReadState;",
            "Lcom/discord/models/domain/ModelReadState;",
            ">;"
        }
    .end annotation
.end field

.field private final messageCreate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final messageDelete:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageDelete;",
            "Lcom/discord/models/domain/ModelMessageDelete;",
            ">;"
        }
    .end annotation
.end field

.field private final messageReactionAdd:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final messageReactionRemove:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final messageReactionRemoveAll:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final messageReactionRemoveEmoji:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final messageUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final presenceReplace:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;>;"
        }
    .end annotation
.end field

.field private final presenceUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelPresence;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation
.end field

.field private final ready:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelPayload;",
            "Lcom/discord/models/domain/ModelPayload;",
            ">;"
        }
    .end annotation
.end field

.field private final relationshipAdd:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;"
        }
    .end annotation
.end field

.field private final relationshipRemove:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;"
        }
    .end annotation
.end field

.field private final scheduler:Lrx/Scheduler;

.field private final sessionsReplace:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSession;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSession;",
            ">;>;"
        }
    .end annotation
.end field

.field private socket:Lcom/discord/gateway/GatewaySocket;

.field private final stream:Lcom/discord/stores/StoreStream;

.field private final streamCreate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private final streamDelete:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamDelete;",
            "Lcom/discord/models/domain/StreamDelete;",
            ">;"
        }
    .end annotation
.end field

.field private final streamServerUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamServerUpdate;",
            "Lcom/discord/models/domain/StreamServerUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private final streamUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private final typingStart:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUser$Typing;",
            "Lcom/discord/models/domain/ModelUser$Typing;",
            ">;"
        }
    .end annotation
.end field

.field private final userConnectionUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final userGuildSettingsUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final userNoteUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserNote$Update;",
            "Lcom/discord/models/domain/ModelUserNote$Update;",
            ">;"
        }
    .end annotation
.end field

.field private final userPaymentSourcesUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final userRequiredActionUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;",
            "Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private final userSettingsUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final userStickerPackUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final userSubscriptionsUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final userUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation
.end field

.field private final voiceServerUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelVoice$Server;",
            "Lcom/discord/models/domain/ModelVoice$Server;",
            ">;"
        }
    .end annotation
.end field

.field private final voiceStateUpdate:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelVoice$State;",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/utilities/time/Clock;Lcom/discord/utilities/logging/AppGatewaySocketLogger;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/StoreGatewayConnection;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/logging/AppGatewaySocketLogger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/logging/AppGatewaySocketLogger;)V
    .locals 1

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gatewaySocketLogger"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->stream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreGatewayConnection;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p3, p0, Lcom/discord/stores/StoreGatewayConnection;->scheduler:Lrx/Scheduler;

    iput-object p4, p0, Lcom/discord/stores/StoreGatewayConnection;->gatewaySocketLogger:Lcom/discord/utilities/logging/AppGatewaySocketLogger;

    new-instance p1, Lrx/subjects/SerializedSubject;

    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p3

    invoke-direct {p1, p3}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->connected:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->connectionReady:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->callCreateOrUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->callDelete:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelCreateOrUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelDeleted:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelRecipientAdd:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelRecipientRemove:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelUnreadUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildApplicationCommands:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildBanAdd:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildBanRemove:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildCreateOrUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildEmojisUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildDeleted:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMembersAdd:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMembersChunk:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMemberRemove:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildRoleCreateOrUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildRoleDelete:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildIntegrationsUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageAck:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageCreate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageDelete:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionAdd:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemove:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemoveEmoji:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemoveAll:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->presenceUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->presenceReplace:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->ready:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->relationshipAdd:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->relationshipRemove:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->typingStart:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userConnectionUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userSettingsUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userGuildSettingsUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userNoteUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userRequiredActionUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->sessionsReplace:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->voiceStateUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->voiceServerUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMemberListUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userPaymentSourcesUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userSubscriptionsUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamCreate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamServerUpdate:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamDelete:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userStickerPackUpdate:Lrx/subjects/SerializedSubject;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/logging/AppGatewaySocketLogger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p3, 0x1

    invoke-static {p3}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object p3

    sget-object p5, Lg0/p/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance p5, Lg0/l/c/c;

    invoke-direct {p5, p3}, Lg0/l/c/c;-><init>(Ljava/util/concurrent/Executor;)V

    const-string p3, "Schedulers.from(Executors.newFixedThreadPool(1))"

    invoke-static {p5, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p3, p5

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreGatewayConnection;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/logging/AppGatewaySocketLogger;)V

    return-void
.end method

.method public static final synthetic access$getIdentifyData(Lcom/discord/stores/StoreGatewayConnection;)Lcom/discord/gateway/GatewaySocket$IdentifyData;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGatewayConnection;->getIdentifyData()Lcom/discord/gateway/GatewaySocket$IdentifyData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStream$p(Lcom/discord/stores/StoreGatewayConnection;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGatewayConnection;->stream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$handleClientStateUpdate(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/stores/StoreGatewayConnection$ClientState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGatewayConnection;->handleClientStateUpdate(Lcom/discord/stores/StoreGatewayConnection$ClientState;)V

    return-void
.end method

.method private final buildGatewaySocket(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)Lcom/discord/gateway/GatewaySocket;
    .locals 15

    move-object v13, p0

    const/4 v0, 0x2

    new-array v0, v0, [Lokhttp3/Interceptor;

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->buildAnalyticsInterceptor()Lokhttp3/Interceptor;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->buildLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/discord/app/App;->e:Lcom/discord/app/App;

    sget-boolean v1, Lcom/discord/app/App;->d:Z

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v10, v3

    goto :goto_0

    :cond_0
    invoke-static {v3, v2, v3}, Lcom/discord/utilities/ssl/SecureSocketsLayerUtils;->createSocketFactory$default(Ljavax/net/ssl/TrustManagerFactory;ILjava/lang/Object;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    move-object v10, v2

    :goto_0
    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreGatewayConnection$buildGatewaySocket$gatewayUrlTransform$1;->INSTANCE:Lcom/discord/stores/StoreGatewayConnection$buildGatewaySocket$gatewayUrlTransform$1;

    move-object v9, v1

    goto :goto_1

    :cond_1
    move-object v9, v3

    :goto_1
    new-instance v14, Lcom/discord/gateway/GatewaySocket;

    new-instance v1, Lcom/discord/stores/StoreGatewayConnection$buildGatewaySocket$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGatewayConnection$buildGatewaySocket$1;-><init>(Lcom/discord/stores/StoreGatewayConnection;)V

    sget-object v2, Lcom/discord/stores/StoreGatewayConnection$buildGatewaySocket$2;->INSTANCE:Lcom/discord/stores/StoreGatewayConnection$buildGatewaySocket$2;

    iget-object v4, v13, Lcom/discord/stores/StoreGatewayConnection;->scheduler:Lrx/Scheduler;

    sget-object v5, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v7, Lcom/discord/gateway/rest/RestConfig;

    sget-object v3, Lcom/discord/utilities/rest/RestAPI$AppHeadersProvider;->INSTANCE:Lcom/discord/utilities/rest/RestAPI$AppHeadersProvider;

    const-string v6, "https://discord.com/api/"

    invoke-direct {v7, v6, v3, v0}, Lcom/discord/gateway/rest/RestConfig;-><init>(Ljava/lang/String;Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;Ljava/util/List;)V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticSuperProperties;

    invoke-virtual {v0}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->getSuperProperties()Ljava/util/Map;

    move-result-object v11

    iget-object v12, v13, Lcom/discord/stores/StoreGatewayConnection;->gatewaySocketLogger:Lcom/discord/utilities/logging/AppGatewaySocketLogger;

    move-object v0, v14

    move-object v3, p0

    move-object/from16 v6, p2

    move-object/from16 v8, p1

    invoke-direct/range {v0 .. v12}, Lcom/discord/gateway/GatewaySocket;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/gateway/GatewayEventHandler;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/networking/NetworkMonitor;Lcom/discord/gateway/rest/RestConfig;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Ljavax/net/ssl/SSLSocketFactory;Ljava/util/Map;Lcom/discord/gateway/GatewaySocketLogger;)V

    return-object v14
.end method

.method private final getIdentifyData()Lcom/discord/gateway/GatewaySocket$IdentifyData;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->clientState:Lcom/discord/stores/StoreGatewayConnection$ClientState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection$ClientState;->getIdentifyData()Lcom/discord/gateway/GatewaySocket$IdentifyData;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final handleClientStateUpdate(Lcom/discord/stores/StoreGatewayConnection$ClientState;)V
    .locals 1

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->clientState:Lcom/discord/stores/StoreGatewayConnection$ClientState;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGatewayConnection$ClientState;->getTokenIfAvailable()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->socket:Lcom/discord/gateway/GatewaySocket;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/gateway/GatewaySocket;->connect()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->socket:Lcom/discord/gateway/GatewaySocket;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/stores/StoreGatewayConnection$ClientState;->getAuthed()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/discord/gateway/GatewaySocket;->close(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final onNext(Lrx/subjects/SerializedSubject;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/subjects/SerializedSubject<",
            "TT;TT;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic presenceUpdate$default(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;ILjava/lang/Object;)Z
    .locals 2

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    iget-object p2, p0, Lcom/discord/stores/StoreGatewayConnection;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {p2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    :cond_0
    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    move-object p4, v0

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreGatewayConnection;->presenceUpdate(Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;)Z

    move-result p0

    return p0
.end method

.method public static synthetic requestApplicationCommands$default(Lcom/discord/stores/StoreGatewayConnection;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;IILjava/lang/Object;)V
    .locals 10

    and-int/lit8 v0, p8, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object v7, p5

    :goto_0
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_1

    move-object v8, v1

    goto :goto_1

    :cond_1
    move-object/from16 v8, p6

    :goto_1
    move-object v2, p0

    move-wide v3, p1

    move-object v5, p3

    move v6, p4

    move/from16 v9, p7

    invoke-virtual/range {v2 .. v9}, Lcom/discord/stores/StoreGatewayConnection;->requestApplicationCommands(JLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;I)V

    return-void
.end method

.method public static synthetic requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;JLjava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z
    .locals 7

    and-int/lit8 p7, p6, 0x2

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x4

    if-eqz p3, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_2

    const/16 p3, 0x64

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    :cond_2
    move-object v6, p5

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers(JLjava/lang/String;Ljava/util/List;Ljava/lang/Integer;)Z

    move-result p0

    return p0
.end method

.method public static synthetic requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/16 p4, 0x64

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)Z

    move-result p0

    return p0
.end method

.method private final requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/gateway/GatewaySocket;",
            "Lkotlin/Unit;",
            ">;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->socket:Lcom/discord/gateway/GatewaySocket;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket;->isSessionEstablished()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private final voiceServerPing()V
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreGatewayConnection$voiceServerPing$1;->INSTANCE:Lcom/discord/stores/StoreGatewayConnection$voiceServerPing$1;

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    return-void
.end method

.method public static synthetic voiceStateUpdate$default(Lcom/discord/stores/StoreGatewayConnection;Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILjava/lang/Object;)Z
    .locals 7

    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_0

    const/4 p6, 0x0

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/discord/stores/StoreGatewayConnection;->voiceStateUpdate(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final callConnect(J)Z
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreGatewayConnection$callConnect$1;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StoreGatewayConnection$callConnect$1;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    move-result p1

    return p1
.end method

.method public final getCallCreateOrUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelCall;",
            "Lcom/discord/models/domain/ModelCall;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->callCreateOrUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getCallDelete()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelCall;",
            "Lcom/discord/models/domain/ModelCall;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->callDelete:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getChannelCreateOrUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->channelCreateOrUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getChannelDeleted()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->channelDeleted:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getChannelRecipientAdd()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->channelRecipientAdd:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getChannelRecipientRemove()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            "Lcom/discord/models/domain/ModelChannel$Recipient;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->channelRecipientRemove:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getChannelUnreadUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelChannelUnreadUpdate;",
            "Lcom/discord/models/domain/ModelChannelUnreadUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->channelUnreadUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getConnected()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->connected:Lrx/subjects/SerializedSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "connected.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getConnectionReady()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->connectionReady:Lrx/subjects/SerializedSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "connectionReady.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getGuildApplicationCommands()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;",
            "Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildApplicationCommands:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildBanAdd()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelBan;",
            "Lcom/discord/models/domain/ModelBan;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildBanAdd:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildBanRemove()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelBan;",
            "Lcom/discord/models/domain/ModelBan;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildBanRemove:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildCreateOrUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildCreateOrUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildDeleted()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildDeleted:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildEmojisUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;",
            "Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildEmojisUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildIntegrationsUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildIntegration$Update;",
            "Lcom/discord/models/domain/ModelGuildIntegration$Update;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildIntegrationsUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildMemberListUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMemberListUpdate;",
            "Lcom/discord/models/domain/ModelGuildMemberListUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMemberListUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildMemberRemove()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMember;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMemberRemove:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildMembersAdd()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMember;",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMembersAdd:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildMembersChunk()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildMember$Chunk;",
            "Lcom/discord/models/domain/ModelGuildMember$Chunk;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMembersChunk:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildRoleCreateOrUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildRoleCreateOrUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getGuildRoleDelete()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            "Lcom/discord/models/domain/ModelGuildRole$Payload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->guildRoleDelete:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageAck()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelReadState;",
            "Lcom/discord/models/domain/ModelReadState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageAck:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageCreate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageCreate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageDelete()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageDelete;",
            "Lcom/discord/models/domain/ModelMessageDelete;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageDelete:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageReactionAdd()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionAdd:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageReactionRemove()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemove:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageReactionRemoveAll()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemoveAll:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageReactionRemoveEmoji()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemoveEmoji:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getMessageUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->messageUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getPresenceReplace()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->presenceReplace:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getPresenceUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelPresence;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->presenceUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getReady()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelPayload;",
            "Lcom/discord/models/domain/ModelPayload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->ready:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getRelationshipAdd()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->relationshipAdd:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getRelationshipRemove()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            "Lcom/discord/models/domain/ModelUserRelationship;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->relationshipRemove:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getSessionsReplace()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSession;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSession;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->sessionsReplace:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getStreamCreate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->streamCreate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getStreamDelete()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamDelete;",
            "Lcom/discord/models/domain/StreamDelete;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->streamDelete:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getStreamServerUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamServerUpdate;",
            "Lcom/discord/models/domain/StreamServerUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->streamServerUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getStreamUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            "Lcom/discord/models/domain/StreamCreateOrUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->streamUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getTypingStart()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUser$Typing;",
            "Lcom/discord/models/domain/ModelUser$Typing;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->typingStart:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserConnectionUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userConnectionUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserGuildSettingsUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userGuildSettingsUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserNoteUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserNote$Update;",
            "Lcom/discord/models/domain/ModelUserNote$Update;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userNoteUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserPaymentSourcesUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userPaymentSourcesUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserRequiredActionUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;",
            "Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userRequiredActionUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserSettingsUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userSettingsUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserStickerPackUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userStickerPackUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserSubscriptionsUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userSubscriptionsUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getUserUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->userUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getVoiceServerUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelVoice$Server;",
            "Lcom/discord/models/domain/ModelVoice$Server;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->voiceServerUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getVoiceStateUpdate()Lrx/subjects/SerializedSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/models/domain/ModelVoice$State;",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->voiceStateUpdate:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public handleConnected(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->connected:Lrx/subjects/SerializedSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public handleConnectionReady(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->connectionReady:Lrx/subjects/SerializedSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public handleDisconnect(Z)V
    .locals 0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getAuthentication$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAuthentication;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreAuthentication;->logout()V

    :cond_0
    return-void
.end method

.method public handleDispatch(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    const-string v1, "data"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    goto/16 :goto_6

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_6

    :sswitch_0
    const-string v0, "STREAM_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_1
    const-string v0, "GUILD_ROLE_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_0

    :sswitch_2
    const-string v0, "VOICE_SERVER_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->voiceServerUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_3
    const-string v0, "VOICE_STATE_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->voiceStateUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_4
    const-string v0, "PRESENCES_REPLACE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->presenceReplace:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_5
    const-string v0, "STREAM_DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamDelete:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_6
    const-string v0, "GUILD_BAN_REMOVE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildBanRemove:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_7
    const-string v0, "STREAM_CREATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamCreate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_8
    const-string v0, "MESSAGE_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_9
    const-string v0, "GUILD_ROLE_DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildRoleDelete:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_a
    const-string p2, "USER_CONNECTIONS_UPDATE"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userConnectionUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, v0}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_b
    const-string v0, "GUILD_ROLE_CREATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_0
    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildRoleCreateOrUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_c
    const-string v0, "USER_GUILD_SETTINGS_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userGuildSettingsUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_d
    const-string v0, "MESSAGE_ACK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageAck:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_e
    const-string v0, "GUILD_EMOJIS_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildEmojisUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_f
    const-string v0, "MESSAGE_REACTION_REMOVE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemove:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_10
    const-string v0, "CHANNEL_RECIPIENT_ADD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelRecipientAdd:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_11
    const-string v0, "MESSAGE_DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :sswitch_12
    const-string v0, "MESSAGE_CREATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageCreate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_13
    const-string v0, "USER_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_14
    const-string v0, "CALL_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_2

    :sswitch_15
    const-string v0, "GUILD_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_3

    :sswitch_16
    const-string v0, "STREAM_SERVER_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->streamServerUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_17
    const-string v0, "READY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->ready:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_18
    const-string v0, "GUILD_MEMBER_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_5

    :sswitch_19
    const-string v0, "MESSAGE_DELETE_BULK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_1
    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageDelete:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_1a
    const-string v0, "GUILD_MEMBER_REMOVE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMemberRemove:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_1b
    const-string v0, "CHANNEL_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_4

    :sswitch_1c
    const-string v0, "CHANNEL_UNREAD_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelUnreadUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_1d
    const-string v0, "CALL_DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->callDelete:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_1e
    const-string v0, "CALL_CREATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_2
    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->callCreateOrUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_1f
    const-string v0, "GUILD_DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildDeleted:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_20
    const-string v0, "GUILD_CREATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_3
    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildCreateOrUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_21
    const-string v0, "USER_PAYMENT_SOURCES_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userPaymentSourcesUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_22
    const-string v0, "TYPING_START"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->typingStart:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_23
    const-string v0, "CHANNEL_RECIPIENT_REMOVE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelRecipientRemove:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_24
    const-string v0, "CHANNEL_DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelDeleted:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_25
    const-string v0, "CHANNEL_CREATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_4
    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->channelCreateOrUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_26
    const-string v0, "RELATIONSHIP_REMOVE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->relationshipRemove:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_27
    const-string v0, "GUILD_APPLICATION_COMMANDS_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildApplicationCommands:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_28
    const-string v0, "USER_SETTINGS_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userSettingsUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_29
    const-string v0, "GUILD_INTEGRATIONS_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildIntegrationsUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_2a
    const-string v0, "PRESENCE_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->presenceUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_2b
    const-string v0, "USER_NOTE_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userNoteUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_2c
    const-string v0, "USER_SUBSCRIPTIONS_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userSubscriptionsUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_2d
    const-string v0, "GUILD_MEMBER_LIST_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMemberListUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_2e
    const-string v0, "MESSAGE_REACTION_ADD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionAdd:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_2f
    const-string v0, "GUILD_MEMBER_ADD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_5
    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMembersAdd:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_30
    const-string v0, "GUILD_BAN_ADD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildBanAdd:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_6

    :sswitch_31
    const-string v0, "RELATIONSHIP_ADD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->relationshipAdd:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto :goto_6

    :sswitch_32
    const-string p2, "USER_STICKER_PACK_UPDATE"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userStickerPackUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, v0}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto :goto_6

    :sswitch_33
    const-string v0, "USER_REQUIRED_ACTION_UPDATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->userRequiredActionUpdate:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto :goto_6

    :sswitch_34
    const-string v0, "MESSAGE_REACTION_REMOVE_ALL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemoveAll:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto :goto_6

    :sswitch_35
    const-string v0, "GUILD_MEMBERS_CHUNK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->guildMembersChunk:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto :goto_6

    :sswitch_36
    const-string v0, "SESSIONS_REPLACE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->sessionsReplace:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    goto :goto_6

    :sswitch_37
    const-string v0, "MESSAGE_REACTION_REMOVE_EMOJI"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->messageReactionRemoveEmoji:Lrx/subjects/SerializedSubject;

    iget-object p1, p1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {p1, p2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    :cond_1
    :goto_6
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7f659b57 -> :sswitch_37
        -0x78ca470e -> :sswitch_36
        -0x6cd113e5 -> :sswitch_35
        -0x5c91e13c -> :sswitch_34
        -0x57767eba -> :sswitch_33
        -0x569f40e7 -> :sswitch_32
        -0x4f1a5206 -> :sswitch_31
        -0x4b2dfc3b -> :sswitch_30
        -0x4a71b2b8 -> :sswitch_2f
        -0x49d2993d -> :sswitch_2e
        -0x47a432af -> :sswitch_2d
        -0x3a1461ba -> :sswitch_2c
        -0x394108be -> :sswitch_2b
        -0x35d8e373 -> :sswitch_2a
        -0x32448a83 -> :sswitch_29
        -0x2e5f9c0f -> :sswitch_28
        -0x2dbcd51b -> :sswitch_27
        -0x2ad96395 -> :sswitch_26
        -0x283cac28 -> :sswitch_25
        -0x273bc779 -> :sswitch_24
        -0x20ab369a -> :sswitch_23
        -0x1e714c86 -> :sswitch_22
        -0x11eeea43 -> :sswitch_21
        -0x10511518 -> :sswitch_20
        -0xf503069 -> :sswitch_1f
        -0xdeca603 -> :sswitch_1e
        -0xcebc154 -> :sswitch_1d
        -0xc585f03 -> :sswitch_1c
        -0x9a2145b -> :sswitch_1b
        -0x816eda3 -> :sswitch_1a
        -0x669dd32 -> :sswitch_19
        -0x261ad5e -> :sswitch_18
        0x4a3e183 -> :sswitch_17
        0x5195ac6 -> :sswitch_16
        0xe4982b5 -> :sswitch_15
        0x10adf1ca -> :sswitch_14
        0x17547bbd -> :sswitch_13
        0x3b7f2454 -> :sswitch_12
        0x3c800903 -> :sswitch_11
        0x46f81adf -> :sswitch_10
        0x4a7f4302 -> :sswitch_f
        0x4c1b20ef -> :sswitch_e
        0x4c5c6c11 -> :sswitch_d
        0x52d81e85 -> :sswitch_c
        0x58044679 -> :sswitch_b
        0x581fe407 -> :sswitch_a
        0x59052b28 -> :sswitch_9
        0x5a19bc21 -> :sswitch_8
        0x60555e9b -> :sswitch_7
        0x60ba6ec0 -> :sswitch_6
        0x6156434a -> :sswitch_5
        0x64b9c00d -> :sswitch_4
        0x654afa64 -> :sswitch_3
        0x702fd8d8 -> :sswitch_2
        0x769ede46 -> :sswitch_1
        0x7eeff668 -> :sswitch_0
    .end sparse-switch
.end method

.method public final handlePreLogout()V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v8}, Lcom/discord/stores/StoreGatewayConnection;->voiceStateUpdate$default(Lcom/discord/stores/StoreGatewayConnection;Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;ILjava/lang/Object;)Z

    return-void
.end method

.method public final handleRtcConnectionStateChanged(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 1

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/rtcconnection/RtcConnection$State$d;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/rtcconnection/RtcConnection$State$d;

    iget-boolean p1, p1, Lcom/discord/rtcconnection/RtcConnection$State$d;->a:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/stores/StoreGatewayConnection;->voiceServerPing()V

    :cond_0
    return-void
.end method

.method public final init(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "networkMonitor"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGatewayConnection;->buildGatewaySocket(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)Lcom/discord/gateway/GatewaySocket;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection;->socket:Lcom/discord/gateway/GatewaySocket;

    sget-object p1, Lcom/discord/stores/StoreGatewayConnection$ClientState;->Companion:Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;

    iget-object p2, p0, Lcom/discord/stores/StoreGatewayConnection;->stream:Lcom/discord/stores/StoreStream;

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->scheduler:Lrx/Scheduler;

    new-instance v1, Lcom/discord/stores/StoreGatewayConnection$init$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGatewayConnection$init$1;-><init>(Lcom/discord/stores/StoreGatewayConnection;)V

    invoke-virtual {p1, p2, v0, v1}, Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;->initialize(Lcom/discord/stores/StoreStream;Lrx/Scheduler;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final presenceUpdate(Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelPresence$Status;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ">;",
            "Ljava/lang/Boolean;",
            ")Z"
        }
    .end annotation

    new-instance v0, Lcom/discord/stores/StoreGatewayConnection$presenceUpdate$1;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/stores/StoreGatewayConnection$presenceUpdate$1;-><init>(Lcom/discord/models/domain/ModelPresence$Status;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    move-result p1

    return p1
.end method

.method public final requestApplicationCommands(JLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;I)V
    .locals 9

    const-string v0, "nonce"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;

    move-object v1, v0

    move-wide v2, p1

    move v5, p4

    move-object v6, p6

    move-object v7, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/discord/stores/StoreGatewayConnection$requestApplicationCommands$1;-><init>(JLjava/lang/String;ZLjava/lang/Integer;Ljava/lang/String;I)V

    move-object v1, p0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    return-void
.end method

.method public final requestGuildMembers(J)Z
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v1, p1

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;JLjava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final requestGuildMembers(JLjava/lang/String;)Z
    .locals 8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;JLjava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final requestGuildMembers(JLjava/lang/String;Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;JLjava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final requestGuildMembers(JLjava/lang/String;Ljava/util/List;Ljava/lang/Integer;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1, p3, p4, p5}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)Z

    move-result p1

    return p1
.end method

.method public final requestGuildMembers(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final requestGuildMembers(Ljava/util/List;Ljava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers$default(Lcom/discord/stores/StoreGatewayConnection;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;ILjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Integer;",
            ")Z"
        }
    .end annotation

    const-string v0, "guildIds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    move-result p1

    return p1
.end method

.method public final resetOnError()Lkotlin/Unit;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection;->socket:Lcom/discord/gateway/GatewaySocket;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/gateway/GatewaySocket;->resetOnError()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final streamCreate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;-><init>(Lcom/discord/stores/StoreGatewayConnection;Ljava/lang/String;Lcom/discord/models/domain/ModelApplicationStream;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    return-void
.end method

.method public final streamDelete(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreGatewayConnection$streamDelete$1;

    invoke-direct {v0, p1}, Lcom/discord/stores/StoreGatewayConnection$streamDelete$1;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    return-void
.end method

.method public final streamWatch(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreGatewayConnection$streamWatch$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/stores/StoreGatewayConnection$streamWatch$1;-><init>(Lcom/discord/stores/StoreGatewayConnection;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    return-void
.end method

.method public final updateGuildSubscriptions(JLcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;)Z
    .locals 4

    const-string v0, "guildSubscriptions"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;

    invoke-virtual {p3}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->getSerializedRanges()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p3}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->getTyping()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p3}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->getActivities()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p3}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;->getMembers()Ljava/util/Set;

    move-result-object p3

    if-eqz p3, :cond_0

    invoke-static {p3}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p3}, Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;-><init>(Ljava/util/Map;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;)V

    new-instance p3, Lcom/discord/stores/StoreGatewayConnection$updateGuildSubscriptions$1;

    invoke-direct {p3, p1, p2, v0}, Lcom/discord/stores/StoreGatewayConnection$updateGuildSubscriptions$1;-><init>(JLcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;)V

    invoke-direct {p0, p3}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    move-result p1

    return p1
.end method

.method public final voiceStateUpdate(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)Z
    .locals 8

    new-instance v7, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;-><init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/discord/stores/StoreGatewayConnection;->requestIfSessionEstablished(Lkotlin/jvm/functions/Function1;)Z

    move-result p1

    return p1
.end method
