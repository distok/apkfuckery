.class public final Lcom/discord/stores/StoreMessages;
.super Lcom/discord/stores/Store;
.source "StoreMessages.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreMessages$Companion;
    }
.end annotation


# static fields
.field private static final BACKGROUND_SENDING_DELAY_MS:J = 0x1d4c0L

.field public static final Companion:Lcom/discord/stores/StoreMessages$Companion;


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private context:Landroid/content/Context;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final holder:Lcom/discord/stores/StoreMessagesHolder;

.field private final initResendFinished:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

.field private final messageQueues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/utilities/messagesend/MessageQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final queueExecutor:Ljava/util/concurrent/ExecutorService;

.field private final stream:Lcom/discord/stores/StoreStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreMessages$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreMessages$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreMessages;->Companion:Lcom/discord/stores/StoreMessages$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V
    .locals 1

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->stream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreMessages;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    new-instance p1, Lcom/discord/stores/StoreMessagesHolder;

    invoke-direct {p1}, Lcom/discord/stores/StoreMessagesHolder;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    new-instance p1, Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-direct {p1}, Lcom/discord/stores/StoreLocalMessagesHolder;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->messageQueues:Ljava/util/HashMap;

    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->initResendFinished:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getContext$p(Lcom/discord/stores/StoreMessages;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "context"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreMessages;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessages;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getInitResendFinished$p(Lcom/discord/stores/StoreMessages;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessages;->initResendFinished:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getMessageQueue(Lcom/discord/stores/StoreMessages;J)Lcom/discord/utilities/messagesend/MessageQueue;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->getMessageQueue(J)Lcom/discord/utilities/messagesend/MessageQueue;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStream$p(Lcom/discord/stores/StoreMessages;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMessages;->stream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$handleLocalMessageCreate(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessages;->handleLocalMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public static final synthetic access$handleLocalMessageDelete(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessages;->handleLocalMessageDelete(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public static final synthetic access$handleSendMessageFailure(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessages;->handleSendMessageFailure(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public static final synthetic access$handleSendMessageValidationError(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->handleSendMessageValidationError(Lcom/discord/models/domain/ModelMessage;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setContext$p(Lcom/discord/stores/StoreMessages;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    return-void
.end method

.method public static final synthetic access$trackFailedLocalMessageResolved(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;Lcom/discord/stores/FailedMessageResolutionType;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->trackFailedLocalMessageResolved(Lcom/discord/models/domain/ModelMessage;Lcom/discord/stores/FailedMessageResolutionType;)V

    return-void
.end method

.method private final declared-synchronized getMessageQueue(J)Lcom/discord/utilities/messagesend/MessageQueue;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->messageQueues:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/messagesend/MessageQueue;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/utilities/messagesend/MessageQueue;

    iget-object v1, p0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "context.contentResolver"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/stores/StoreMessages;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    const-string v3, "queueExecutor"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v0, v1, v2, v3}, Lcom/discord/utilities/messagesend/MessageQueue;-><init>(Landroid/content/ContentResolver;Ljava/util/concurrent/ExecutorService;Lcom/discord/utilities/time/Clock;)V

    iget-object v1, p0, Lcom/discord/stores/StoreMessages;->messageQueues:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    const-string p1, "context"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x0

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final handleLocalMessageCreate(Lcom/discord/models/domain/ModelMessage;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreLocalMessagesHolder;->addMessage(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method private final handleLocalMessageDelete(Lcom/discord/models/domain/ModelMessage;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreLocalMessagesHolder;->deleteMessage(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method private final handleMessageDelete(JLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreMessagesHolder;->deleteMessages(JLjava/util/List;)V

    return-void
.end method

.method private final handleSendMessageFailure(Lcom/discord/models/domain/ModelMessage;)V
    .locals 21
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/discord/stores/StoreMessages;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream;->getMessageUploads$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageUploads;

    move-result-object v2

    const-string v3, "nonce"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/discord/stores/StoreMessageUploads;->handleMessageCreateFailure(Ljava/lang/String;)V

    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/discord/stores/StoreMessages;->deleteMessage(Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->isHasLocalUploads()Z

    move-result v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v12

    iget-object v13, v0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    move-object/from16 v1, p1

    iget-object v14, v1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getLastManualAttemptTimestamp()Ljava/lang/Long;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getInitialAttemptTimestamp()Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getStickers()Ljava/util/List;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;

    move-result-object v20

    invoke-static/range {v4 .. v20}, Lcom/discord/models/domain/ModelMessage;->createLocalMessage(Ljava/lang/String;JLcom/discord/models/domain/ModelUser;Ljava/util/List;ZZLcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/utilities/time/Clock;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;)Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    const-string v2, "failedMessage"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreMessages;->handleLocalMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method private final handleSendMessageValidationError(Lcom/discord/models/domain/ModelMessage;Ljava/lang/String;)V
    .locals 7

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "ValidationError"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMessages;->deleteMessage(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method private final observeLocalMessagesForChannel(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-virtual {v0}, Lcom/discord/stores/StoreLocalMessagesHolder;->getMessagesPublisher()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreMessages$observeLocalMessagesForChannel$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreMessages$observeLocalMessagesForChannel$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "localMessagesHolder\n    \u2026annelId] ?: emptyList() }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreMessages$observeLocalMessagesForChannel$2;->INSTANCE:Lcom/discord/stores/StoreMessages$observeLocalMessagesForChannel$2;

    new-instance v0, Lg0/l/a/x0;

    invoke-direct {v0, p2}, Lg0/l/a/x0;-><init>(Lrx/functions/Func2;)V

    new-instance p2, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {p2, p1, v0}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {p2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    const-string p2, "localMessagesHolder\n    \u2026messages1 === messages2 }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final observeSyncedMessagesForChannel(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessagesHolder;->getMessagesPublisher()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreMessages$observeSyncedMessagesForChannel$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreMessages$observeSyncedMessagesForChannel$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "holder\n          .messag\u2026annelId] ?: emptyList() }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreMessages$observeSyncedMessagesForChannel$2;->INSTANCE:Lcom/discord/stores/StoreMessages$observeSyncedMessagesForChannel$2;

    new-instance v0, Lg0/l/a/x0;

    invoke-direct {v0, p2}, Lg0/l/a/x0;-><init>(Lrx/functions/Func2;)V

    new-instance p2, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {p2, p1, v0}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {p2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    const-string p2, "holder\n          .messag\u2026messages1 === messages2 }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final resendAllLocalMessages()V
    .locals 12
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-virtual {v0}, Lcom/discord/stores/StoreLocalMessagesHolder;->getFlattenedMessages()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {p0, v2, v3}, Lcom/discord/stores/StoreMessages;->resendMessage(Lcom/discord/models/domain/ModelMessage;Z)Lrx/Observable;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    new-instance v1, Lg0/l/a/t;

    invoke-direct {v1, v0}, Lg0/l/a/t;-><init>(Ljava/lang/Iterable;)V

    invoke-static {v1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lg0/l/a/a1$a;->a:Lg0/l/a/a1;

    new-instance v2, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v2, v0, v1}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v3

    const-string v0, "Observable\n        .mergeDelayError(observables)"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v4, Lcom/discord/stores/StoreMessages;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/stores/StoreMessages$resendAllLocalMessages$1;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreMessages$resendAllLocalMessages$1;-><init>(Lcom/discord/stores/StoreMessages;)V

    sget-object v9, Lcom/discord/stores/StoreMessages$resendAllLocalMessages$2;->INSTANCE:Lcom/discord/stores/StoreMessages$resendAllLocalMessages$2;

    const/16 v10, 0xe

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static synthetic resendMessage$default(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;ZILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->resendMessage(Lcom/discord/models/domain/ModelMessage;Z)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic sendMessage$default(Lcom/discord/stores/StoreMessages;JLcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelMessage$Activity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;
    .locals 19

    move/from16 v0, p16

    and-int/lit8 v1, v0, 0x20

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v10, v2

    goto :goto_0

    :cond_0
    move-object/from16 v10, p7

    :goto_0
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_1

    move-object v11, v2

    goto :goto_1

    :cond_1
    move-object/from16 v11, p8

    :goto_1
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_2

    move-object v12, v2

    goto :goto_2

    :cond_2
    move-object/from16 v12, p9

    :goto_2
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_3

    move-object v13, v2

    goto :goto_3

    :cond_3
    move-object/from16 v13, p10

    :goto_3
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_4

    move-object v14, v2

    goto :goto_4

    :cond_4
    move-object/from16 v14, p11

    :goto_4
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_5

    move-object v15, v2

    goto :goto_5

    :cond_5
    move-object/from16 v15, p12

    :goto_5
    and-int/lit16 v1, v0, 0x800

    if-eqz v1, :cond_6

    move-object/from16 v16, v2

    goto :goto_6

    :cond_6
    move-object/from16 v16, p13

    :goto_6
    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_7

    move-object/from16 v17, v2

    goto :goto_7

    :cond_7
    move-object/from16 v17, p14

    :goto_7
    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_8

    move-object/from16 v18, v2

    goto :goto_8

    :cond_8
    move-object/from16 v18, p15

    :goto_8
    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-virtual/range {v3 .. v18}, Lcom/discord/stores/StoreMessages;->sendMessage(JLcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelMessage$Activity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final trackFailedLocalMessageResolved(Lcom/discord/models/domain/ModelMessage;Lcom/discord/stores/FailedMessageResolutionType;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    const-string v3, "localAttachment"

    const-string v4, "context"

    const-string v5, "context.contentResolver"

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/models/messages/LocalAttachment;

    invoke-static {v9, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v10, v0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    if-eqz v10, :cond_3

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {v10, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v9, v10}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isImageAttachment(Lcom/discord/models/messages/LocalAttachment;Landroid/content/ContentResolver;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :goto_0
    if-ne v2, v7, :cond_4

    const/4 v13, 0x1

    goto :goto_1

    :cond_4
    const/4 v13, 0x0

    :goto_1
    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    if-eqz v2, :cond_9

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    :cond_6
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/models/messages/LocalAttachment;

    invoke-static {v9, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v10, v0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    if-eqz v10, :cond_8

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {v10, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v9, v10}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isVideoAttachment(Lcom/discord/models/messages/LocalAttachment;Landroid/content/ContentResolver;)Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v2, 0x1

    goto :goto_2

    :cond_8
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :goto_2
    if-ne v2, v7, :cond_9

    const/4 v14, 0x1

    goto :goto_3

    :cond_9
    const/4 v14, 0x0

    :goto_3
    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    if-eqz v2, :cond_c

    new-instance v3, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v2, v7}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/models/messages/LocalAttachment;

    invoke-virtual {v7}, Lcom/discord/models/messages/LocalAttachment;->getUriString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const-string v9, "Uri.parse(localAttachment.uriString)"

    invoke-static {v7, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v9, v0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    if-eqz v9, :cond_a

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v9, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7, v9}, Lcom/discord/utilities/rest/SendUtilsKt;->computeFileSizeBytes(Landroid/net/Uri;Landroid/content/ContentResolver;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_a
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :cond_b
    move-object v6, v3

    :cond_c
    if-eqz v6, :cond_d

    invoke-static {v6}, Lx/h/f;->maxOrNull(Ljava/lang/Iterable;)Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v3, v2

    move v11, v3

    goto :goto_5

    :cond_d
    const/4 v11, 0x0

    :goto_5
    if-eqz v6, :cond_e

    invoke-static {v6}, Lx/h/f;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v2

    long-to-int v3, v2

    move v12, v3

    goto :goto_6

    :cond_e
    const/4 v12, 0x0

    :goto_6
    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v9

    iget-object v2, v1, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    if-eqz v2, :cond_f

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move v10, v2

    goto :goto_7

    :cond_f
    const/4 v10, 0x0

    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getInitialAttemptTimestamp()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_10

    goto :goto_8

    :cond_10
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_8
    const-string v3, "localMessage.initialAttemptTimestamp ?: 0L"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_11

    goto :goto_9

    :cond_11
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_9
    const-string v3, "localMessage.numRetries ?: 0"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v18

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v19

    move-object/from16 v15, p2

    invoke-virtual/range {v9 .. v20}, Lcom/discord/stores/StoreAnalytics;->trackFailedMessageResolved(IIIZZLcom/discord/stores/FailedMessageResolutionType;JIJ)V

    return-void
.end method


# virtual methods
.method public final cancelMessageSend(JLjava/lang/String;)V
    .locals 1

    const-string v0, "requestId"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->getMessageQueue(J)Lcom/discord/utilities/messagesend/MessageQueue;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/discord/utilities/messagesend/MessageQueue;->cancel(Ljava/lang/String;)V

    return-void
.end method

.method public final deleteMessage(Lcom/discord/models/domain/ModelMessage;)V
    .locals 10

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->isLocal()Z

    move-result v4

    if-nez v4, :cond_1

    sget-object p1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p1

    invoke-virtual {p1, v2, v3, v0, v1}, Lcom/discord/utilities/rest/RestAPI;->deleteMessage(JJ)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lf/a/b/r;->f(ZI)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    const-string p1, "RestAPI\n          .api\n \u2026ormers.restSubscribeOn())"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/discord/stores/StoreMessages$deleteMessage$1;->INSTANCE:Lcom/discord/stores/StoreMessages$deleteMessage$1;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x35

    const/4 v9, 0x0

    const-string v3, "deleteMessage"

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreMessages$deleteMessage$2;

    invoke-direct {v1, p0, p1, v2, v3}, Lcom/discord/stores/StoreMessages$deleteMessage$2;-><init>(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public final editMessage(JJLjava/lang/String;)V
    .locals 10

    const-string v0, "content"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p3, p4}, Lcom/discord/stores/StoreMessages;->getMessageQueue(J)Lcom/discord/utilities/messagesend/MessageQueue;

    move-result-object v0

    new-instance v9, Lcom/discord/utilities/messagesend/MessageRequest$Edit;

    iget-object v1, p0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v7

    move-object v1, v9

    move-wide v2, p3

    move-object v4, p5

    move-wide v5, p1

    invoke-direct/range {v1 .. v8}, Lcom/discord/utilities/messagesend/MessageRequest$Edit;-><init>(JLjava/lang/String;JJ)V

    invoke-virtual {v0, v9}, Lcom/discord/utilities/messagesend/MessageQueue;->enqueue(Lcom/discord/utilities/messagesend/MessageRequest;)V

    return-void
.end method

.method public final getAllDetached()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessagesHolder;->getDetachedChannelSubject()Lrx/Observable;

    move-result-object v0

    const-string v1, "holder\n          .detachedChannelSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getMessage(JJ)Lcom/discord/models/domain/ModelMessage;
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesHolder;->getMessagesForChannel(Ljava/lang/Long;)Ljava/util/TreeMap;

    move-result-object p1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelMessage;

    return-object p1
.end method

.method public final handleChannelSelected(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMessagesHolder;->setSelectedChannelId(J)V

    return-void
.end method

.method public final handleConnected(Z)V
    .locals 1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {p1}, Lcom/discord/stores/StoreMessagesHolder;->invalidate()V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreMessages;->messageQueues:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p1

    const-string v0, "messageQueues.values"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/messagesend/MessageQueue;

    invoke-virtual {v0}, Lcom/discord/utilities/messagesend/MessageQueue;->handleConnected()V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 3

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    const-string v1, "payload.me"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreMessagesHolder;->setMyUserId(J)V

    return-void
.end method

.method public final handleMessageCreate(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "messagesList"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5, v2}, Lcom/discord/stores/StoreLocalMessagesHolder;->deleteMessage(JLjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesHolder;->addMessages(Ljava/util/List;)V

    return-void
.end method

.method public final handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V
    .locals 3

    const-string v0, "messageDelete"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getChannelId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getMessageIds()Ljava/util/List;

    move-result-object p1

    const-string v2, "messageDelete.messageIds"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/stores/StoreMessages;->handleMessageDelete(JLjava/util/List;)V

    return-void
.end method

.method public final handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesHolder;->updateMessages(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public final handleMessagesLoaded(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V
    .locals 1

    const-string v0, "chunk"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesHolder;->loadMessageChunks(Ljava/util/List;)V

    return-void
.end method

.method public final handlePreLogout()V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    invoke-virtual {v0}, Lcom/discord/stores/StoreLocalMessagesHolder;->clearCache()V

    return-void
.end method

.method public final handleReactionUpdate(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelMessageReaction$Update;",
            ">;Z)V"
        }
    .end annotation

    const-string/jumbo v0, "updates"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMessagesHolder;->updateReactions(Ljava/util/List;Z)V

    return-void
.end method

.method public final handleReactionsRemoveAll(Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 1

    const-string/jumbo v0, "update"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesHolder;->removeAllReactions(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method public final handleReactionsRemoveEmoji(Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 1

    const-string/jumbo v0, "update"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesHolder;->removeEmojiReactions(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    iget-object p1, p0, Lcom/discord/stores/StoreMessages;->holder:Lcom/discord/stores/StoreMessagesHolder;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreMessagesHolder;->init(Z)V

    iget-object p1, p0, Lcom/discord/stores/StoreMessages;->localMessagesHolder:Lcom/discord/stores/StoreLocalMessagesHolder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2}, Lcom/discord/stores/StoreLocalMessagesHolder;->init$default(Lcom/discord/stores/StoreLocalMessagesHolder;ZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreMessages;->resendAllLocalMessages()V

    return-void
.end method

.method public final observeInitResendFinished()Lrx/subjects/BehaviorSubject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessages;->initResendFinished:Lrx/subjects/BehaviorSubject;

    const-string v1, "initResendFinished"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeIsDetached(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreMessages;->getAllDetached()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreMessages$observeIsDetached$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreMessages$observeIsDetached$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "allDetached\n          .m\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeMessagesForChannel(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;>;"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->observeSyncedMessagesForChannel(J)Lrx/Observable;

    move-result-object v0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->observeLocalMessagesForChannel(J)Lrx/Observable;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->observeIsDetached(J)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreMessages$observeMessagesForChannel$1;->INSTANCE:Lcom/discord/stores/StoreMessages$observeMessagesForChannel$1;

    invoke-static {v0, v1, p1, p2}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ges + localMessages\n    }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeMessagesForChannel(JJ)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelMessage;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreMessages;->observeMessagesForChannel(J)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreMessages$observeMessagesForChannel$2;

    invoke-direct {p2, p3, p4}, Lcom/discord/stores/StoreMessages$observeMessagesForChannel$2;-><init>(J)V

    invoke-virtual {p1, p2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observeMessagesForChanne\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final resendMessage(Lcom/discord/models/domain/ModelMessage;Z)Lrx/Observable;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelMessage;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v13, p0

    move-object/from16 v0, p1

    move/from16 v1, p2

    const-string v2, "message"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    :cond_0
    if-nez v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v2

    const/4 v3, -0x2

    if-ne v2, v3, :cond_1

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Incorrect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " auto attempt and message type "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    :goto_0
    iget-object v2, v13, Lcom/discord/stores/StoreMessages;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v3, Lcom/discord/stores/StoreMessages$resendMessage$1;

    invoke-direct {v3, v13, v0}, Lcom/discord/stores/StoreMessages$resendMessage$1;-><init>(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual {v2, v3}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_1
    const-string v3, "message.numRetries ?: 0"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    const-string v6, "message.author"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    goto :goto_2

    :cond_4
    const-string v6, ""

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v7

    iget-object v8, v0, Lcom/discord/models/domain/ModelMessage;->localAttachments:Ljava/util/List;

    const/4 v9, 0x0

    if-eqz v8, :cond_5

    new-instance v10, Ljava/util/ArrayList;

    const/16 v11, 0xa

    invoke-static {v8, v11}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v11

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/discord/models/messages/LocalAttachment;

    invoke-virtual {v11}, Lcom/discord/models/messages/LocalAttachment;->getUriString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    new-instance v15, Lcom/lytefast/flexinput/model/Attachment;

    invoke-virtual {v11}, Lcom/discord/models/messages/LocalAttachment;->getId()J

    move-result-wide v16

    const-string v14, "contentUri"

    invoke-static {v12, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/discord/models/messages/LocalAttachment;->getDisplayName()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    move-object v14, v15

    move-object v11, v15

    move-wide/from16 v15, v16

    move-object/from16 v17, v12

    invoke-direct/range {v14 .. v19}, Lcom/lytefast/flexinput/model/Attachment;-><init>(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v10, v11}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    move-object v10, v9

    :cond_6
    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getAllowedMentions()Lcom/discord/models/domain/ModelAllowedMentions;

    move-result-object v12

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    if-eqz v1, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getLastManualAttemptTimestamp()Ljava/lang/Long;

    move-result-object v1

    move-object/from16 v21, v1

    goto :goto_4

    :cond_7
    move-object/from16 v21, v9

    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelMessage;->getInitialAttemptTimestamp()Ljava/lang/Long;

    move-result-object v14

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    const/16 v16, 0x720

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-wide v1, v3

    move-object v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, v10

    move-object v7, v8

    move-object v8, v11

    move-object v9, v12

    move-object/from16 v10, v18

    move-object/from16 v11, v19

    move-object/from16 v12, v20

    move-object/from16 v13, v21

    invoke-static/range {v0 .. v17}, Lcom/discord/stores/StoreMessages;->sendMessage$default(Lcom/discord/stores/StoreMessages;JLcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelMessage$Activity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final sendMessage(JLcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelMessage$Activity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Lrx/Observable;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;",
            "Lcom/discord/models/domain/ModelMessage$MessageReference;",
            "Lcom/discord/models/domain/ModelAllowedMentions;",
            "Lcom/discord/models/domain/ModelApplication;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            "Lcom/discord/models/domain/ModelMessage$Activity;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-wide/from16 v12, p1

    move-object/from16 v4, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p6

    sget-object v15, Lrx/Emitter$BackpressureMode;->e:Lrx/Emitter$BackpressureMode;

    const-string v3, "author"

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "content"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v11, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v11}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v2, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v5, 0x1

    if-eqz v2, :cond_1

    invoke-interface/range {p6 .. p6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v6, 0x1

    :goto_1
    const-string v10, "Observable.create({ emit\u2026r.BackpressureMode.ERROR)"

    const/16 v7, 0xa

    const/4 v8, 0x0

    if-nez v6, :cond_8

    sget-object v6, Lcom/discord/utilities/rest/ProcessedMessageContent;->Companion:Lcom/discord/utilities/rest/ProcessedMessageContent$Companion;

    iget-object v9, v0, Lcom/discord/stores/StoreMessages;->context:Landroid/content/Context;

    if-eqz v9, :cond_7

    invoke-virtual {v6, v2, v1, v9}, Lcom/discord/utilities/rest/ProcessedMessageContent$Companion;->fromAttachments(Ljava/util/List;Ljava/lang/String;Landroid/content/Context;)Lcom/discord/utilities/rest/ProcessedMessageContent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/rest/ProcessedMessageContent;->getInvalidAttachments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    xor-int/2addr v6, v5

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    new-instance v9, Ljava/util/ArrayList;

    invoke-static {v2, v7}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v14

    invoke-direct {v9, v14}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/lytefast/flexinput/model/Attachment;

    invoke-static {v14}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->toLocalAttachment(Lcom/lytefast/flexinput/model/Attachment;)Lcom/discord/models/messages/LocalAttachment;

    move-result-object v14

    invoke-interface {v9, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-static {v12, v13, v4, v6, v9}, Lcom/discord/models/domain/ModelMessage;->createInvalidAttachmentsMessage(JLcom/discord/models/domain/ModelUser;Lcom/discord/utilities/time/Clock;Ljava/util/List;)Lcom/discord/models/domain/ModelMessage;

    move-result-object v2

    iget-object v6, v0, Lcom/discord/stores/StoreMessages;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v9, Lcom/discord/stores/StoreMessages$sendMessage$1;

    invoke-direct {v9, v0, v2}, Lcom/discord/stores/StoreMessages$sendMessage$1;-><init>(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual {v6, v9}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    :cond_3
    invoke-virtual {v1}, Lcom/discord/utilities/rest/ProcessedMessageContent;->getValidAttachments()Ljava/util/List;

    move-result-object v2

    iput-object v2, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/ProcessedMessageContent;->getContent()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_8

    sget-object v1, Lcom/discord/stores/StoreMessages$sendMessage$2;->INSTANCE:Lcom/discord/stores/StoreMessages$sendMessage$2;

    invoke-static {v1, v15}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object v1

    invoke-static {v1, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1

    :cond_7
    const-string v1, "context"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v8

    :cond_8
    if-eqz p13, :cond_9

    invoke-virtual/range {p13 .. p13}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    goto :goto_6

    :cond_9
    iget-object v2, v0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v16

    :goto_6
    move-wide/from16 v18, v16

    const/4 v6, 0x0

    iget-object v2, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_b

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    goto :goto_7

    :cond_a
    const/4 v2, 0x0

    goto :goto_8

    :cond_b
    :goto_7
    const/4 v2, 0x1

    :goto_8
    xor-int/lit8 v9, v2, 0x1

    iget-object v5, v0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    iget-object v2, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_c

    new-instance v8, Ljava/util/ArrayList;

    invoke-static {v2, v7}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/lytefast/flexinput/model/Attachment;

    invoke-static {v7}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->toLocalAttachment(Lcom/lytefast/flexinput/model/Attachment;)Lcom/discord/models/messages/LocalAttachment;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_c
    move-object/from16 v16, v8

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    if-eqz p14, :cond_d

    invoke-virtual/range {p14 .. p14}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    goto :goto_a

    :cond_d
    iget-object v2, v0, Lcom/discord/stores/StoreMessages;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v7

    :goto_a
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    if-eqz p15, :cond_e

    invoke-virtual/range {p15 .. p15}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-wide/from16 v2, p1

    move-object/from16 v4, p3

    move-object/from16 v21, v5

    move-object/from16 v5, p5

    move v7, v9

    move-object/from16 v8, p10

    move-object/from16 v9, p12

    move-object/from16 v22, v10

    move-object/from16 v10, v21

    move-object/from16 v21, v11

    move-object/from16 v11, v16

    move-object/from16 v12, v17

    move-object/from16 v13, v20

    move-object/from16 v23, v15

    move-object/from16 v15, p7

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    invoke-static/range {v1 .. v17}, Lcom/discord/models/domain/ModelMessage;->createLocalMessage(Ljava/lang/String;JLcom/discord/models/domain/ModelUser;Ljava/util/List;ZZLcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/ModelMessage$Activity;Lcom/discord/utilities/time/Clock;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;)Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    if-nez p12, :cond_f

    iget-object v2, v0, Lcom/discord/stores/StoreMessages;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v3, Lcom/discord/stores/StoreMessages$sendMessage$3;

    invoke-direct {v3, v0, v1}, Lcom/discord/stores/StoreMessages$sendMessage$3;-><init>(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    invoke-virtual {v2, v3}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    :cond_f
    iget-object v2, v0, Lcom/discord/stores/StoreMessages;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v3, Lcom/discord/stores/StoreMessages$sendMessage$4;

    invoke-direct {v3, v0}, Lcom/discord/stores/StoreMessages$sendMessage$4;-><init>(Lcom/discord/stores/StoreMessages;)V

    invoke-virtual {v2, v3}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    new-instance v2, Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    move-object/from16 p3, v2

    move-object/from16 p4, p0

    move-object/from16 p5, v1

    move-object/from16 p6, v21

    move-object/from16 p7, p11

    move-wide/from16 p8, v18

    invoke-direct/range {p3 .. p9}, Lcom/discord/stores/StoreMessages$sendMessage$request$1;-><init>(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/discord/models/domain/activity/ModelActivity;J)V

    new-instance v1, Lcom/discord/stores/StoreMessages$sendMessage$5;

    move-wide/from16 v3, p1

    invoke-direct {v1, v0, v3, v4, v2}, Lcom/discord/stores/StoreMessages$sendMessage$5;-><init>(Lcom/discord/stores/StoreMessages;JLkotlin/jvm/functions/Function1;)V

    move-object/from16 v2, v23

    invoke-static {v1, v2}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object v1

    move-object/from16 v2, v22

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method
