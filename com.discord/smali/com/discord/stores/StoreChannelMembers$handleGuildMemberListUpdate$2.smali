.class public final Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;
.super Lx/m/c/k;
.source "StoreChannelMembers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreChannelMembers;->handleGuildMemberListUpdate(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;",
        "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreChannelMembers;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreChannelMembers;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;->this$0:Lcom/discord/stores/StoreChannelMembers;

    iput-wide p2, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;->$guildId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;->this$0:Lcom/discord/stores/StoreChannelMembers;

    iget-wide v1, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;->$guildId:J

    invoke-static {v0, v1, v2, p1}, Lcom/discord/stores/StoreChannelMembers;->access$makeGroup(Lcom/discord/stores/StoreChannelMembers;JLcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreChannelMembers$handleGuildMemberListUpdate$2;->invoke(Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p1

    return-object p1
.end method
