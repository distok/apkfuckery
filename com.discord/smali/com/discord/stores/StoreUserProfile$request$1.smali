.class public final Lcom/discord/stores/StoreUserProfile$request$1;
.super Lx/m/c/k;
.source "StoreUserProfile.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUserProfile;->request(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelUserProfile;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $userId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreUserProfile;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreUserProfile;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreUserProfile$request$1;->this$0:Lcom/discord/stores/StoreUserProfile;

    iput-wide p2, p0, Lcom/discord/stores/StoreUserProfile$request$1;->$userId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUserProfile;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreUserProfile$request$1;->invoke(Lcom/discord/models/domain/ModelUserProfile;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelUserProfile;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreUserProfile$request$1;->this$0:Lcom/discord/stores/StoreUserProfile;

    iget-wide v1, p0, Lcom/discord/stores/StoreUserProfile$request$1;->$userId:J

    invoke-static {v0, p1, v1, v2}, Lcom/discord/stores/StoreUserProfile;->access$handleUserProfile(Lcom/discord/stores/StoreUserProfile;Lcom/discord/models/domain/ModelUserProfile;J)V

    :cond_0
    return-void
.end method
