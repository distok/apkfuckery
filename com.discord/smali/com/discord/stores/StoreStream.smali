.class public final Lcom/discord/stores/StoreStream;
.super Ljava/lang/Object;
.source "StoreStream.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreStream$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreStream$Companion;

.field private static final collector:Lcom/discord/stores/StoreStream;

.field private static isInitialized:Z


# instance fields
.field private final accessibility:Lcom/discord/stores/StoreAccessibility;

.field private final analytics:Lcom/discord/stores/StoreAnalytics;

.field private final androidPackages:Lcom/discord/stores/StoreAndroidPackages;

.field private final application:Lcom/discord/stores/StoreApplication;

.field private final applicationCommands:Lcom/discord/stores/StoreApplicationCommands;

.field private final applicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

.field private final applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

.field private final audioDevices:Lcom/discord/stores/StoreAudioDevices;

.field private final audioManager:Lcom/discord/stores/StoreAudioManager;

.field private final auditLog:Lcom/discord/stores/StoreAuditLog;

.field private final authentication:Lcom/discord/stores/StoreAuthentication;

.field private final bans:Lcom/discord/stores/StoreBans;

.field private final calls:Lcom/discord/stores/StoreCalls;

.field private final callsIncoming:Lcom/discord/stores/StoreCallsIncoming;

.field private final changeLogStore:Lcom/discord/stores/StoreChangeLog;

.field private final channelConversions:Lcom/discord/stores/StoreChannelConversions;

.field private final channelFollowerStats:Lcom/discord/stores/StoreChannelFollowerStats;

.field private final channels:Lcom/discord/stores/StoreChannels;

.field private final channelsSelected:Lcom/discord/stores/StoreChannelsSelected;

.field private final chat:Lcom/discord/stores/StoreChat;

.field private final clientDataState:Lcom/discord/stores/StoreClientDataState;

.field private final clientVersion:Lcom/discord/stores/StoreClientVersion;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final connectionOpen:Lcom/discord/stores/StoreConnectionOpen;

.field private final connectivity:Lcom/discord/stores/StoreConnectivity;

.field private final customEmojis:Lcom/discord/stores/StoreEmojiCustom;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final emojis:Lcom/discord/stores/StoreEmoji;

.field private final entitlements:Lcom/discord/stores/StoreEntitlements;

.field private final expandedGuildFolders:Lcom/discord/stores/StoreExpandedGuildFolders;

.field private final experiments:Lcom/discord/stores/StoreExperiments;

.field private final expressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

.field private final gameParty:Lcom/discord/stores/StoreGameParty;

.field private final gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

.field private final gifPicker:Lcom/discord/stores/StoreGifPicker;

.field private final gifting:Lcom/discord/stores/StoreGifting;

.field private final googlePlayPurchases:Lcom/discord/stores/StoreGooglePlayPurchases;

.field private final googlePlaySkuDetails:Lcom/discord/stores/StoreGooglePlaySkuDetails;

.field private final guildEmojis:Lcom/discord/stores/StoreEmojiGuild;

.field private final guildGating:Lcom/discord/stores/StoreGuildGating;

.field private final guildInvite:Lcom/discord/stores/StoreInviteSettings;

.field private final guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

.field private final guildMemberRequesterStore:Lcom/discord/stores/StoreGuildMemberRequester;

.field private final guildProfiles:Lcom/discord/stores/StoreGuildProfiles;

.field private final guildSelected:Lcom/discord/stores/StoreGuildSelected;

.field private final guildSettings:Lcom/discord/stores/StoreUserGuildSettings;

.field private final guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

.field private final guildTemplates:Lcom/discord/stores/StoreGuildTemplates;

.field private final guildWelcomeScreens:Lcom/discord/stores/StoreGuildWelcomeScreens;

.field private final guilds:Lcom/discord/stores/StoreGuilds;

.field private final guildsNsfw:Lcom/discord/stores/StoreGuildsNsfw;

.field private final guildsSorted:Lcom/discord/stores/StoreGuildsSorted;

.field private final initialized:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final instantInvites:Lcom/discord/stores/StoreInstantInvites;

.field private final integrations:Lcom/discord/stores/StoreGuildIntegrations;

.field private final lazyChannelMembersStore:Lcom/discord/stores/StoreChannelMembers;

.field private final library:Lcom/discord/stores/StoreLibrary;

.field private final lurking:Lcom/discord/stores/StoreLurking;

.field private final maskedLinks:Lcom/discord/stores/StoreMaskedLinks;

.field private final mediaEngine:Lcom/discord/stores/StoreMediaEngine;

.field private final mediaSettings:Lcom/discord/stores/StoreMediaSettings;

.field private final mentions:Lcom/discord/stores/StoreMentions;

.field private final messageAck:Lcom/discord/stores/StoreMessageAck;

.field private final messageReactions:Lcom/discord/stores/StoreMessageReactions;

.field private final messageReplies:Lcom/discord/stores/StoreMessageReplies;

.field private final messageStates:Lcom/discord/stores/StoreMessageState;

.field private final messageUploads:Lcom/discord/stores/StoreMessageUploads;

.field private final messages:Lcom/discord/stores/StoreMessages;

.field private final messagesLoader:Lcom/discord/stores/StoreMessagesLoader;

.field private final messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

.field private final mfa:Lcom/discord/stores/StoreMFA;

.field private final navigation:Lcom/discord/stores/StoreNavigation;

.field private final notices:Lcom/discord/stores/StoreNotices;

.field private final notifications:Lcom/discord/stores/StoreNotifications;

.field private final nux:Lcom/discord/stores/StoreNux;

.field private final paymentSources:Lcom/discord/stores/StorePaymentSources;

.field private final pendingReplies:Lcom/discord/stores/StorePendingReplies;

.field private final permissions:Lcom/discord/stores/StorePermissions;

.field private final pinnedMessages:Lcom/discord/stores/StorePinnedMessages;

.field private final premiumGuildSubscriptions:Lcom/discord/stores/StorePremiumGuildSubscription;

.field private final presences:Lcom/discord/stores/StoreUserPresence;

.field private final readStates:Lcom/discord/stores/StoreReadStates;

.field private final reviewRequestStore:Lcom/discord/stores/StoreReviewRequest;

.field private final rtcConnection:Lcom/discord/stores/StoreRtcConnection;

.field private final rtcRegion:Lcom/discord/stores/StoreRtcRegion;

.field private final runningGame:Lcom/discord/stores/StoreRunningGame;

.field private final search:Lcom/discord/stores/StoreSearch;

.field private final slowMode:Lcom/discord/stores/StoreSlowMode;

.field private final spotify:Lcom/discord/stores/StoreSpotify;

.field private final stickers:Lcom/discord/stores/StoreStickers;

.field private final storeChannelCategories:Lcom/discord/stores/StoreChannelCategories;

.field private final storeDynamicLink:Lcom/discord/stores/StoreDynamicLink;

.field private final storeThreadScheduler:Lrx/Scheduler;

.field private final storesV2:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/StoreV2;",
            ">;"
        }
    .end annotation
.end field

.field private final streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

.field private final subscriptions:Lcom/discord/stores/StoreSubscriptions;

.field private final tabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

.field private final userAffinities:Lcom/discord/stores/StoreUserAffinities;

.field private final userConnections:Lcom/discord/stores/StoreUserConnections;

.field private final userNotes:Lcom/discord/stores/StoreUserNotes;

.field private final userProfile:Lcom/discord/stores/StoreUserProfile;

.field private final userRelationships:Lcom/discord/stores/StoreUserRelationships;

.field private final userRequiredAction:Lcom/discord/stores/StoreUserRequiredActions;

.field private final userSettings:Lcom/discord/stores/StoreUserSettings;

.field private final users:Lcom/discord/stores/StoreUser;

.field private final usersMutualGuilds:Lcom/discord/stores/StoreUsersMutualGuilds;

.field private final usersTyping:Lcom/discord/stores/StoreUserTyping;

.field private final videoStreams:Lcom/discord/stores/StoreVideoStreams;

.field private final videoSupport:Lcom/discord/stores/StoreVideoSupport;

.field private final voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

.field private final voiceParticipants:Lcom/discord/stores/StoreVoiceParticipants;

.field private final voiceSpeaking:Lcom/discord/stores/StoreVoiceSpeaking;

.field private final voiceStates:Lcom/discord/stores/StoreVoiceStates;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreStream$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreStream$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    new-instance v0, Lcom/discord/stores/StoreStream;

    invoke-direct {v0}, Lcom/discord/stores/StoreStream;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreStream;->collector:Lcom/discord/stores/StoreStream;

    return-void
.end method

.method public constructor <init>()V
    .locals 36

    move-object/from16 v8, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->initialized:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/discord/stores/StoreStream$storeThreadScheduler$1;->INSTANCE:Lcom/discord/stores/StoreStream$storeThreadScheduler$1;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sget-object v1, Lg0/p/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Lg0/l/c/c;

    invoke-direct {v1, v0}, Lg0/l/c/c;-><init>(Ljava/util/concurrent/Executor;)V

    const-string v0, "Schedulers.from(Executor\u2026       newThread\n      })"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->storeThreadScheduler:Lrx/Scheduler;

    new-instance v15, Lcom/discord/stores/Dispatcher;

    const/4 v14, 0x0

    invoke-direct {v15, v1, v14}, Lcom/discord/stores/Dispatcher;-><init>(Lrx/Scheduler;Z)V

    iput-object v15, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v13

    iput-object v13, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    new-instance v9, Lcom/discord/stores/StoreAnalytics;

    invoke-direct {v9, v8, v15, v13}, Lcom/discord/stores/StoreAnalytics;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V

    iput-object v9, v8, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    new-instance v12, Lcom/discord/stores/StoreAuthentication;

    invoke-direct {v12, v8, v15}, Lcom/discord/stores/StoreAuthentication;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v12, v8, Lcom/discord/stores/StoreStream;->authentication:Lcom/discord/stores/StoreAuthentication;

    new-instance v11, Lcom/discord/stores/StoreChannels;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v0

    invoke-direct {v11, v8, v15, v0}, Lcom/discord/stores/StoreChannels;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    iput-object v11, v8, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    new-instance v0, Lcom/discord/stores/StoreChannelsSelected;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v1

    invoke-direct {v0, v8, v15, v1}, Lcom/discord/stores/StoreChannelsSelected;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->channelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    new-instance v0, Lcom/discord/stores/StoreChannelConversions;

    invoke-direct {v0}, Lcom/discord/stores/StoreChannelConversions;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->channelConversions:Lcom/discord/stores/StoreChannelConversions;

    new-instance v0, Lcom/discord/stores/StoreClientVersion;

    invoke-direct {v0}, Lcom/discord/stores/StoreClientVersion;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->clientVersion:Lcom/discord/stores/StoreClientVersion;

    new-instance v10, Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-direct {v10}, Lcom/discord/stores/StoreGuildMemberCounts;-><init>()V

    iput-object v10, v8, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    new-instance v0, Lcom/discord/stores/StoreGuildsNsfw;

    invoke-direct {v0, v8}, Lcom/discord/stores/StoreGuildsNsfw;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->guildsNsfw:Lcom/discord/stores/StoreGuildsNsfw;

    new-instance v0, Lcom/discord/stores/StoreBans;

    invoke-direct {v0, v15}, Lcom/discord/stores/StoreBans;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->bans:Lcom/discord/stores/StoreBans;

    new-instance v0, Lcom/discord/stores/StoreEmojiGuild;

    invoke-direct {v0, v15}, Lcom/discord/stores/StoreEmojiGuild;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->guildEmojis:Lcom/discord/stores/StoreEmojiGuild;

    new-instance v0, Lcom/discord/stores/StoreGuildIntegrations;

    invoke-direct {v0, v15}, Lcom/discord/stores/StoreGuildIntegrations;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->integrations:Lcom/discord/stores/StoreGuildIntegrations;

    new-instance v0, Lcom/discord/stores/StoreInstantInvites;

    invoke-direct {v0}, Lcom/discord/stores/StoreInstantInvites;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->instantInvites:Lcom/discord/stores/StoreInstantInvites;

    new-instance v0, Lcom/discord/stores/StoreGuildTemplates;

    invoke-direct {v0, v15}, Lcom/discord/stores/StoreGuildTemplates;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->guildTemplates:Lcom/discord/stores/StoreGuildTemplates;

    new-instance v0, Lcom/discord/stores/StoreInviteSettings;

    invoke-direct {v0}, Lcom/discord/stores/StoreInviteSettings;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->guildInvite:Lcom/discord/stores/StoreInviteSettings;

    new-instance v4, Lcom/discord/stores/StoreMessages;

    invoke-direct {v4, v8, v15, v13}, Lcom/discord/stores/StoreMessages;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V

    iput-object v4, v8, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    new-instance v0, Lcom/discord/stores/StoreMessagesLoader;

    invoke-direct {v0, v8}, Lcom/discord/stores/StoreMessagesLoader;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->messagesLoader:Lcom/discord/stores/StoreMessagesLoader;

    new-instance v0, Lcom/discord/stores/StoreMessagesMostRecent;

    invoke-direct {v0}, Lcom/discord/stores/StoreMessagesMostRecent;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    new-instance v1, Lcom/discord/stores/StoreMessageAck;

    invoke-direct {v1, v8, v15}, Lcom/discord/stores/StoreMessageAck;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->messageAck:Lcom/discord/stores/StoreMessageAck;

    new-instance v0, Lcom/discord/stores/StoreMessageReplies;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v0

    move-object v3, v15

    invoke-direct/range {v2 .. v7}, Lcom/discord/stores/StoreMessageReplies;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->messageReplies:Lcom/discord/stores/StoreMessageReplies;

    new-instance v0, Lcom/discord/stores/StoreMessageState;

    invoke-direct {v0, v15}, Lcom/discord/stores/StoreMessageState;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->messageStates:Lcom/discord/stores/StoreMessageState;

    new-instance v0, Lcom/discord/stores/StoreNotifications;

    invoke-direct {v0, v13, v8}, Lcom/discord/stores/StoreNotifications;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->notifications:Lcom/discord/stores/StoreNotifications;

    new-instance v7, Lcom/discord/stores/StoreUserGuildSettings;

    invoke-direct {v7, v15, v13, v9, v11}, Lcom/discord/stores/StoreUserGuildSettings;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreChannels;)V

    iput-object v7, v8, Lcom/discord/stores/StoreStream;->guildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    new-instance v6, Lcom/discord/stores/StoreUser;

    const/4 v3, 0x0

    const/4 v4, 0x4

    move-object v0, v6

    move-object/from16 v19, v1

    move-object/from16 v1, p0

    move-object v2, v15

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreUser;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v6, v8, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    new-instance v5, Lcom/discord/stores/StoreGuilds;

    const/4 v4, 0x0

    const/4 v3, 0x2

    invoke-direct {v5, v6, v4, v3, v4}, Lcom/discord/stores/StoreGuilds;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v5, v8, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    new-instance v2, Lcom/discord/stores/StoreGuildSelected;

    const/16 v16, 0x0

    const/16 v17, 0x10

    const/16 v18, 0x0

    move-object v0, v2

    move-object v14, v2

    move-object v2, v15

    move-object v3, v5

    move-object v4, v9

    move-object v9, v5

    move-object/from16 v5, v16

    move-object/from16 v24, v6

    move/from16 v6, v17

    move-object/from16 v21, v7

    move-object/from16 v7, v18

    invoke-direct/range {v0 .. v7}, Lcom/discord/stores/StoreGuildSelected;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v14, v8, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    new-instance v7, Lcom/discord/stores/StoreExperiments;

    const/16 v17, 0x40

    move-object v0, v9

    move-object v9, v7

    move-object v1, v10

    move-object v10, v13

    move-object v6, v11

    move-object v11, v15

    move-object v2, v12

    move-object/from16 v12, v24

    move-object v5, v13

    move-object v13, v0

    const/16 v33, 0x0

    move-object v14, v2

    move-object v4, v15

    move-object v15, v1

    invoke-direct/range {v9 .. v18}, Lcom/discord/stores/StoreExperiments;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreGuildMemberCounts;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v7, v8, Lcom/discord/stores/StoreStream;->experiments:Lcom/discord/stores/StoreExperiments;

    new-instance v1, Lcom/discord/stores/StoreLurking;

    invoke-direct {v1, v8, v0, v4}, Lcom/discord/stores/StoreLurking;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->lurking:Lcom/discord/stores/StoreLurking;

    new-instance v1, Lcom/discord/stores/StoreUserConnections;

    invoke-direct {v1, v8, v4}, Lcom/discord/stores/StoreUserConnections;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->userConnections:Lcom/discord/stores/StoreUserConnections;

    new-instance v1, Lcom/discord/stores/StoreUsersMutualGuilds;

    invoke-direct {v1, v8}, Lcom/discord/stores/StoreUsersMutualGuilds;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->usersMutualGuilds:Lcom/discord/stores/StoreUsersMutualGuilds;

    new-instance v1, Lcom/discord/stores/StoreUserPresence;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v2

    invoke-direct {v1, v5, v8, v2}, Lcom/discord/stores/StoreUserPresence;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;Lcom/discord/stores/updates/ObservationDeck;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    new-instance v1, Lcom/discord/stores/StoreUserProfile;

    invoke-direct {v1}, Lcom/discord/stores/StoreUserProfile;-><init>()V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->userProfile:Lcom/discord/stores/StoreUserProfile;

    new-instance v1, Lcom/discord/stores/StoreUserNotes;

    invoke-direct {v1, v4}, Lcom/discord/stores/StoreUserNotes;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->userNotes:Lcom/discord/stores/StoreUserNotes;

    new-instance v1, Lcom/discord/stores/StoreUserTyping;

    invoke-direct {v1, v8, v4}, Lcom/discord/stores/StoreUserTyping;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->usersTyping:Lcom/discord/stores/StoreUserTyping;

    new-instance v11, Lcom/discord/stores/StoreUserSettings;

    invoke-direct {v11, v8, v4}, Lcom/discord/stores/StoreUserSettings;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v11, v8, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    new-instance v1, Lcom/discord/stores/StoreUserRequiredActions;

    invoke-direct {v1}, Lcom/discord/stores/StoreUserRequiredActions;-><init>()V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->userRequiredAction:Lcom/discord/stores/StoreUserRequiredActions;

    new-instance v3, Lcom/discord/stores/StoreUserRelationships;

    invoke-direct {v3}, Lcom/discord/stores/StoreUserRelationships;-><init>()V

    iput-object v3, v8, Lcom/discord/stores/StoreStream;->userRelationships:Lcom/discord/stores/StoreUserRelationships;

    new-instance v2, Lcom/discord/stores/StoreVoiceStates;

    invoke-direct {v2, v8}, Lcom/discord/stores/StoreVoiceStates;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v2, v8, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    new-instance v1, Lcom/discord/stores/StoreVoiceSpeaking;

    invoke-direct {v1}, Lcom/discord/stores/StoreVoiceSpeaking;-><init>()V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->voiceSpeaking:Lcom/discord/stores/StoreVoiceSpeaking;

    new-instance v1, Lcom/discord/stores/StorePermissions;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v9

    move-object/from16 v15, v24

    invoke-direct {v1, v15, v6, v0, v9}, Lcom/discord/stores/StorePermissions;-><init>(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/updates/ObservationDeck;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    new-instance v0, Lcom/discord/stores/StoreTabsNavigation;

    invoke-direct {v0, v4, v8}, Lcom/discord/stores/StoreTabsNavigation;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->tabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    new-instance v9, Lcom/discord/stores/StoreMaskedLinks;

    invoke-direct {v9, v4, v8}, Lcom/discord/stores/StoreMaskedLinks;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreStream;)V

    iput-object v9, v8, Lcom/discord/stores/StoreStream;->maskedLinks:Lcom/discord/stores/StoreMaskedLinks;

    new-instance v9, Lcom/discord/stores/StoreNavigation;

    invoke-direct {v9, v8, v0}, Lcom/discord/stores/StoreNavigation;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/StoreTabsNavigation;)V

    iput-object v9, v8, Lcom/discord/stores/StoreStream;->navigation:Lcom/discord/stores/StoreNavigation;

    new-instance v0, Lcom/discord/stores/StoreEmojiCustom;

    invoke-direct {v0, v8}, Lcom/discord/stores/StoreEmojiCustom;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    new-instance v0, Lcom/discord/stores/StoreAccessibility;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xc

    move-object v9, v0

    move-object v10, v4

    move-object/from16 v34, v15

    move-object/from16 v15, v16

    invoke-direct/range {v9 .. v15}, Lcom/discord/stores/StoreAccessibility;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/accessibility/AccessibilityMonitor;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->accessibility:Lcom/discord/stores/StoreAccessibility;

    new-instance v9, Lcom/discord/stores/StoreVoiceChannelSelected;

    const/4 v10, 0x0

    const/16 v11, 0x8

    move-object v0, v9

    move-object v13, v1

    move-object/from16 v1, p0

    move-object v14, v2

    move-object v2, v4

    move-object v15, v3

    move-object v3, v5

    move-object/from16 v35, v4

    move-object v4, v10

    move-object v10, v5

    move v5, v11

    move-object v11, v6

    move-object v6, v12

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/StoreVoiceChannelSelected;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v9, v8, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    new-instance v12, Lcom/discord/stores/StoreAudioDevices;

    sget-object v0, Lcom/discord/utilities/media/AudioOutputMonitor;->Companion:Lcom/discord/utilities/media/AudioOutputMonitor$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/media/AudioOutputMonitor$Companion;->getINSTANCE()Lcom/discord/utilities/media/AudioOutputMonitor;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;

    move-object/from16 v6, v34

    invoke-direct {v1, v11, v14, v6}, Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;-><init>(Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreUser;)V

    move-object/from16 v2, v35

    invoke-direct {v12, v2, v0, v1, v9}, Lcom/discord/stores/StoreAudioDevices;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/media/AudioOutputMonitor;Lcom/discord/stores/StoreAudioDevices$VideoUseDetector;Lcom/discord/stores/StoreVoiceChannelSelected;)V

    iput-object v12, v8, Lcom/discord/stores/StoreStream;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    new-instance v11, Lcom/discord/stores/StoreGatewayConnection;

    sget-object v0, Lcom/discord/utilities/logging/AppGatewaySocketLogger;->Companion:Lcom/discord/utilities/logging/AppGatewaySocketLogger$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/logging/AppGatewaySocketLogger$Companion;->getINSTANCE()Lcom/discord/utilities/logging/AppGatewaySocketLogger;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v14, 0x0

    move-object v0, v11

    move-object/from16 v1, p0

    move-object v2, v10

    move-object v10, v6

    move-object v6, v14

    invoke-direct/range {v0 .. v6}, Lcom/discord/stores/StoreGatewayConnection;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/logging/AppGatewaySocketLogger;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v11, v8, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    new-instance v0, Lcom/discord/stores/StoreConnectivity;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v0, v8, v1, v2}, Lcom/discord/stores/StoreConnectivity;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->connectivity:Lcom/discord/stores/StoreConnectivity;

    new-instance v0, Lcom/discord/stores/StoreConnectionOpen;

    invoke-direct {v0}, Lcom/discord/stores/StoreConnectionOpen;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->connectionOpen:Lcom/discord/stores/StoreConnectionOpen;

    new-instance v0, Lcom/discord/stores/StoreCalls;

    invoke-direct {v0, v8}, Lcom/discord/stores/StoreCalls;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    new-instance v0, Lcom/discord/stores/StoreCallsIncoming;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v2

    invoke-direct {v0, v1, v2, v10}, Lcom/discord/stores/StoreCallsIncoming;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreUser;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->callsIncoming:Lcom/discord/stores/StoreCallsIncoming;

    new-instance v0, Lcom/discord/stores/StoreChat;

    invoke-direct {v0}, Lcom/discord/stores/StoreChat;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->chat:Lcom/discord/stores/StoreChat;

    new-instance v0, Lcom/discord/stores/StoreMentions;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    move-object/from16 v16, v0

    move-object/from16 v17, v15

    move-object/from16 v18, v13

    move-object/from16 v20, v21

    move-object/from16 v21, v1

    invoke-direct/range {v16 .. v21}, Lcom/discord/stores/StoreMentions;-><init>(Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreMessageAck;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreChannels;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    new-instance v0, Lcom/discord/stores/StorePinnedMessages;

    invoke-direct {v0}, Lcom/discord/stores/StorePinnedMessages;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->pinnedMessages:Lcom/discord/stores/StorePinnedMessages;

    new-instance v0, Lcom/discord/stores/StoreReadStates;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreReadStates;-><init>(Lcom/discord/utilities/time/Clock;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->readStates:Lcom/discord/stores/StoreReadStates;

    new-instance v0, Lcom/discord/stores/StoreVoiceParticipants;

    invoke-direct {v0, v8}, Lcom/discord/stores/StoreVoiceParticipants;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->voiceParticipants:Lcom/discord/stores/StoreVoiceParticipants;

    new-instance v0, Lcom/discord/stores/StoreSearch;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->guildsNsfw:Lcom/discord/stores/StoreGuildsNsfw;

    invoke-direct {v0, v1, v10}, Lcom/discord/stores/StoreSearch;-><init>(Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->search:Lcom/discord/stores/StoreSearch;

    new-instance v0, Lcom/discord/stores/StoreMediaSettings;

    invoke-direct {v0, v8}, Lcom/discord/stores/StoreMediaSettings;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->mediaSettings:Lcom/discord/stores/StoreMediaSettings;

    new-instance v1, Lcom/discord/stores/StoreDynamicLink;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v1, v8, v2}, Lcom/discord/stores/StoreDynamicLink;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->storeDynamicLink:Lcom/discord/stores/StoreDynamicLink;

    new-instance v1, Lcom/discord/stores/StoreChannelCategories;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/StoreChannelCategories;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreChannels;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->storeChannelCategories:Lcom/discord/stores/StoreChannelCategories;

    new-instance v1, Lcom/discord/stores/StoreVideoSupport;

    invoke-direct {v1}, Lcom/discord/stores/StoreVideoSupport;-><init>()V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->videoSupport:Lcom/discord/stores/StoreVideoSupport;

    new-instance v6, Lcom/discord/stores/StoreMediaEngine;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v6, v0, v8, v1}, Lcom/discord/stores/StoreMediaEngine;-><init>(Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v6, v8, Lcom/discord/stores/StoreStream;->mediaEngine:Lcom/discord/stores/StoreMediaEngine;

    new-instance v4, Lcom/discord/stores/StoreRtcRegion;

    iget-object v0, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    const/16 v30, 0x0

    const/16 v31, 0x10

    const/16 v32, 0x0

    move-object/from16 v25, v4

    move-object/from16 v26, v0

    move-object/from16 v27, v1

    move-object/from16 v28, v6

    move-object/from16 v29, v7

    invoke-direct/range {v25 .. v32}, Lcom/discord/stores/StoreRtcRegion;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v4, v8, Lcom/discord/stores/StoreStream;->rtcRegion:Lcom/discord/stores/StoreRtcRegion;

    new-instance v10, Lcom/discord/stores/StoreRtcConnection;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    iget-object v5, v8, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    move-object v0, v10

    move-object/from16 v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreRtcConnection;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreRtcRegion;Lcom/discord/stores/StoreAnalytics;)V

    iput-object v10, v8, Lcom/discord/stores/StoreStream;->rtcConnection:Lcom/discord/stores/StoreRtcConnection;

    new-instance v11, Lcom/discord/stores/StoreVideoStreams;

    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-direct {v11, v14, v13, v14}, Lcom/discord/stores/StoreVideoStreams;-><init>(Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v11, v8, Lcom/discord/stores/StoreStream;->videoStreams:Lcom/discord/stores/StoreVideoStreams;

    new-instance v0, Lcom/discord/stores/StoreGameParty;

    invoke-direct {v0}, Lcom/discord/stores/StoreGameParty;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->gameParty:Lcom/discord/stores/StoreGameParty;

    new-instance v0, Lcom/discord/stores/StoreNotices;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v0, v1, v8}, Lcom/discord/stores/StoreNotices;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->notices:Lcom/discord/stores/StoreNotices;

    new-instance v1, Lcom/discord/stores/StoreGuildSubscriptions;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v1, v8, v2}, Lcom/discord/stores/StoreGuildSubscriptions;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

    new-instance v1, Lcom/discord/stores/StoreChannelMembers;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v3, Lcom/discord/stores/StoreStream$lazyChannelMembersStore$1;

    iget-object v4, v8, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    invoke-direct {v3, v4}, Lcom/discord/stores/StoreStream$lazyChannelMembersStore$1;-><init>(Lcom/discord/stores/StoreChannels;)V

    new-instance v4, Lcom/discord/stores/StoreStream$lazyChannelMembersStore$2;

    iget-object v5, v8, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-direct {v4, v5}, Lcom/discord/stores/StoreStream$lazyChannelMembersStore$2;-><init>(Lcom/discord/stores/StoreGuildMemberCounts;)V

    invoke-direct {v1, v8, v2, v3, v4}, Lcom/discord/stores/StoreChannelMembers;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->lazyChannelMembersStore:Lcom/discord/stores/StoreChannelMembers;

    new-instance v1, Lcom/discord/stores/StoreGuildMemberRequester;

    invoke-direct {v1, v8}, Lcom/discord/stores/StoreGuildMemberRequester;-><init>(Lcom/discord/stores/StoreStream;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->guildMemberRequesterStore:Lcom/discord/stores/StoreGuildMemberRequester;

    new-instance v1, Lcom/discord/stores/StoreReviewRequest;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v1, v2, v8}, Lcom/discord/stores/StoreReviewRequest;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->reviewRequestStore:Lcom/discord/stores/StoreReviewRequest;

    new-instance v1, Lcom/discord/stores/StoreChangeLog;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-direct {v1, v2, v0, v3}, Lcom/discord/stores/StoreChangeLog;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreNotices;Lcom/discord/stores/StoreUser;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->changeLogStore:Lcom/discord/stores/StoreChangeLog;

    new-instance v0, Lcom/discord/stores/StoreRunningGame;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v0, v8, v1, v2}, Lcom/discord/stores/StoreRunningGame;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->runningGame:Lcom/discord/stores/StoreRunningGame;

    new-instance v0, Lcom/discord/stores/StoreSlowMode;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v0, v1, v8}, Lcom/discord/stores/StoreSlowMode;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->slowMode:Lcom/discord/stores/StoreSlowMode;

    new-instance v0, Lcom/discord/stores/StoreAuditLog;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v8, v1}, Lcom/discord/stores/StoreAuditLog;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->auditLog:Lcom/discord/stores/StoreAuditLog;

    new-instance v0, Lcom/discord/stores/StoreMessageUploads;

    invoke-direct {v0}, Lcom/discord/stores/StoreMessageUploads;-><init>()V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->messageUploads:Lcom/discord/stores/StoreMessageUploads;

    new-instance v0, Lcom/discord/stores/StoreNux;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreNux;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->nux:Lcom/discord/stores/StoreNux;

    new-instance v0, Lcom/discord/stores/StoreLibrary;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreLibrary;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->library:Lcom/discord/stores/StoreLibrary;

    new-instance v0, Lcom/discord/stores/StoreGifting;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreGifting;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->gifting:Lcom/discord/stores/StoreGifting;

    new-instance v0, Lcom/discord/stores/StoreSpotify;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v0, v8, v1, v2}, Lcom/discord/stores/StoreSpotify;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->spotify:Lcom/discord/stores/StoreSpotify;

    new-instance v0, Lcom/discord/stores/StoreMessageReactions;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-direct {v0, v1, v2}, Lcom/discord/stores/StoreMessageReactions;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    new-instance v0, Lcom/discord/stores/StoreApplication;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreApplication;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->application:Lcom/discord/stores/StoreApplication;

    new-instance v0, Lcom/discord/stores/StorePaymentSources;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StorePaymentSources;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->paymentSources:Lcom/discord/stores/StorePaymentSources;

    new-instance v0, Lcom/discord/stores/StoreSubscriptions;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreSubscriptions;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->subscriptions:Lcom/discord/stores/StoreSubscriptions;

    new-instance v0, Lcom/discord/stores/StoreMFA;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v8, v1}, Lcom/discord/stores/StoreMFA;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->mfa:Lcom/discord/stores/StoreMFA;

    new-instance v7, Lcom/discord/stores/StoreApplicationStreaming;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    move-object v0, v7

    move-object/from16 v1, p0

    move-object v4, v9

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreApplicationStreaming;-><init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreRtcConnection;)V

    iput-object v7, v8, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    new-instance v9, Lcom/discord/stores/StoreStreamRtcConnection;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    iget-object v4, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v5, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    iget-object v7, v8, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    move-object v0, v9

    move-object v1, v6

    move-object/from16 v3, p0

    move-object v6, v7

    move-object v7, v10

    invoke-direct/range {v0 .. v7}, Lcom/discord/stores/StoreStreamRtcConnection;-><init>(Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreRtcConnection;)V

    iput-object v9, v8, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    new-instance v0, Lcom/discord/stores/StoreAudioManager;

    invoke-direct {v0, v12, v10}, Lcom/discord/stores/StoreAudioManager;-><init>(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/stores/StoreRtcConnection;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->audioManager:Lcom/discord/stores/StoreAudioManager;

    new-instance v0, Lcom/discord/stores/StoreApplicationStreamPreviews;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreApplicationStreamPreviews;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->applicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

    new-instance v0, Lcom/discord/stores/StoreGuildsSorted;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->lurking:Lcom/discord/stores/StoreLurking;

    invoke-direct {v0, v1, v2, v3}, Lcom/discord/stores/StoreGuildsSorted;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreLurking;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->guildsSorted:Lcom/discord/stores/StoreGuildsSorted;

    new-instance v1, Lcom/discord/stores/StoreExpandedGuildFolders;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v1, v2}, Lcom/discord/stores/StoreExpandedGuildFolders;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->expandedGuildFolders:Lcom/discord/stores/StoreExpandedGuildFolders;

    new-instance v1, Lcom/discord/stores/StoreEmoji;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    iget-object v4, v8, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/discord/stores/StoreEmoji;-><init>(Lcom/discord/stores/StoreEmojiCustom;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuildsSorted;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->emojis:Lcom/discord/stores/StoreEmoji;

    new-instance v0, Lcom/discord/stores/StorePremiumGuildSubscription;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StorePremiumGuildSubscription;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->premiumGuildSubscriptions:Lcom/discord/stores/StorePremiumGuildSubscription;

    new-instance v0, Lcom/discord/stores/StoreEntitlements;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreEntitlements;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->entitlements:Lcom/discord/stores/StoreEntitlements;

    new-instance v0, Lcom/discord/stores/StoreAndroidPackages;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreAndroidPackages;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->androidPackages:Lcom/discord/stores/StoreAndroidPackages;

    new-instance v0, Lcom/discord/stores/StoreGuildProfiles;

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/discord/stores/StoreGuildProfiles;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->guildProfiles:Lcom/discord/stores/StoreGuildProfiles;

    new-instance v1, Lcom/discord/stores/StoreGuildWelcomeScreens;

    iget-object v2, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    const/4 v3, 0x2

    invoke-direct {v1, v2, v14, v3, v14}, Lcom/discord/stores/StoreGuildWelcomeScreens;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v1, v8, Lcom/discord/stores/StoreStream;->guildWelcomeScreens:Lcom/discord/stores/StoreGuildWelcomeScreens;

    new-instance v2, Lcom/discord/stores/StoreUserAffinities;

    iget-object v4, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v2, v4}, Lcom/discord/stores/StoreUserAffinities;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v2, v8, Lcom/discord/stores/StoreStream;->userAffinities:Lcom/discord/stores/StoreUserAffinities;

    new-instance v2, Lcom/discord/stores/StoreClientDataState;

    invoke-direct {v2, v14, v13, v14}, Lcom/discord/stores/StoreClientDataState;-><init>(Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v8, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    new-instance v4, Lcom/discord/stores/StoreGifPicker;

    iget-object v5, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v6, v8, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0xc

    const/16 v21, 0x0

    move-object v15, v4

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    invoke-direct/range {v15 .. v21}, Lcom/discord/stores/StoreGifPicker;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v4, v8, Lcom/discord/stores/StoreStream;->gifPicker:Lcom/discord/stores/StoreGifPicker;

    new-instance v5, Lcom/discord/stores/StoreStickers;

    iget-object v6, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0xe

    const/16 v28, 0x0

    move-object/from16 v22, v5

    move-object/from16 v23, v6

    invoke-direct/range {v22 .. v28}, Lcom/discord/stores/StoreStickers;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/time/Clock;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v5, v8, Lcom/discord/stores/StoreStream;->stickers:Lcom/discord/stores/StoreStickers;

    new-instance v6, Lcom/discord/stores/StoreGooglePlayPurchases;

    iget-object v7, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object v9, v8, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    invoke-direct {v6, v7, v9}, Lcom/discord/stores/StoreGooglePlayPurchases;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V

    iput-object v6, v8, Lcom/discord/stores/StoreStream;->googlePlayPurchases:Lcom/discord/stores/StoreGooglePlayPurchases;

    new-instance v6, Lcom/discord/stores/StoreGooglePlaySkuDetails;

    iget-object v7, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v6, v7}, Lcom/discord/stores/StoreGooglePlaySkuDetails;-><init>(Lcom/discord/stores/Dispatcher;)V

    iput-object v6, v8, Lcom/discord/stores/StoreStream;->googlePlaySkuDetails:Lcom/discord/stores/StoreGooglePlaySkuDetails;

    new-instance v6, Lcom/discord/stores/StoreExpressionPickerNavigation;

    invoke-direct {v6}, Lcom/discord/stores/StoreExpressionPickerNavigation;-><init>()V

    iput-object v6, v8, Lcom/discord/stores/StoreStream;->expressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    new-instance v6, Lcom/discord/stores/StoreChannelFollowerStats;

    iget-object v7, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Lcom/discord/stores/StoreChannelFollowerStats;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    iput-object v6, v8, Lcom/discord/stores/StoreStream;->channelFollowerStats:Lcom/discord/stores/StoreChannelFollowerStats;

    new-instance v7, Lcom/discord/stores/StorePendingReplies;

    iget-object v9, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v7, v9, v14, v3, v14}, Lcom/discord/stores/StorePendingReplies;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v7, v8, Lcom/discord/stores/StoreStream;->pendingReplies:Lcom/discord/stores/StorePendingReplies;

    new-instance v9, Lcom/discord/stores/StoreApplicationCommands;

    iget-object v10, v8, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    iget-object v12, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v18

    const/16 v20, 0x8

    move-object v15, v9

    move-object/from16 v16, v10

    move-object/from16 v17, v12

    invoke-direct/range {v15 .. v21}, Lcom/discord/stores/StoreApplicationCommands;-><init>(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/BuiltInCommandsProvider;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v9, v8, Lcom/discord/stores/StoreStream;->applicationCommands:Lcom/discord/stores/StoreApplicationCommands;

    new-instance v10, Lcom/discord/stores/StoreGuildGating;

    iget-object v12, v8, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-direct {v10, v12, v14, v3, v14}, Lcom/discord/stores/StoreGuildGating;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v10, v8, Lcom/discord/stores/StoreStream;->guildGating:Lcom/discord/stores/StoreGuildGating;

    const/16 v10, 0x18

    new-array v10, v10, [Lcom/discord/stores/StoreV2;

    iget-object v12, v8, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    aput-object v12, v10, v33

    iget-object v12, v8, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    aput-object v12, v10, v13

    iget-object v12, v8, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    aput-object v12, v10, v3

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    const/4 v12, 0x3

    aput-object v3, v10, v12

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    const/4 v12, 0x4

    aput-object v3, v10, v12

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    const/4 v12, 0x5

    aput-object v3, v10, v12

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->accessibility:Lcom/discord/stores/StoreAccessibility;

    const/4 v12, 0x6

    aput-object v3, v10, v12

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->callsIncoming:Lcom/discord/stores/StoreCallsIncoming;

    const/4 v12, 0x7

    aput-object v3, v10, v12

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->connectivity:Lcom/discord/stores/StoreConnectivity;

    const/16 v12, 0x8

    aput-object v3, v10, v12

    iget-object v3, v8, Lcom/discord/stores/StoreStream;->connectionOpen:Lcom/discord/stores/StoreConnectionOpen;

    const/16 v12, 0x9

    aput-object v3, v10, v12

    const/16 v3, 0xa

    aput-object v1, v10, v3

    const/16 v1, 0xb

    aput-object v11, v10, v1

    const/16 v1, 0xc

    aput-object v2, v10, v1

    const/16 v1, 0xd

    aput-object v4, v10, v1

    const/16 v1, 0xe

    aput-object v5, v10, v1

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    const/16 v2, 0xf

    aput-object v1, v10, v2

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->experiments:Lcom/discord/stores/StoreExperiments;

    const/16 v2, 0x10

    aput-object v1, v10, v2

    iget-object v1, v8, Lcom/discord/stores/StoreStream;->messageReplies:Lcom/discord/stores/StoreMessageReplies;

    const/16 v2, 0x11

    aput-object v1, v10, v2

    const/16 v1, 0x12

    aput-object v6, v10, v1

    const/16 v1, 0x13

    aput-object v7, v10, v1

    const/16 v1, 0x14

    aput-object v0, v10, v1

    const/16 v0, 0x15

    aput-object v9, v10, v0

    iget-object v0, v8, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    const/16 v1, 0x16

    aput-object v0, v10, v1

    iget-object v0, v8, Lcom/discord/stores/StoreStream;->channelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    const/16 v1, 0x17

    aput-object v0, v10, v1

    invoke-static {v10}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v8, Lcom/discord/stores/StoreStream;->storesV2:Ljava/util/List;

    invoke-direct/range {p0 .. p0}, Lcom/discord/stores/StoreStream;->registerDispatchHandlers()V

    return-void
.end method

.method public static final synthetic access$deferredInit(Lcom/discord/stores/StoreStream;Landroid/app/Application;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->deferredInit(Landroid/app/Application;)V

    return-void
.end method

.method public static final synthetic access$dispatchSubscribe(Lcom/discord/stores/StoreStream;Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$getAudioManager$p(Lcom/discord/stores/StoreStream;)Lcom/discord/stores/StoreAudioManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStream;->audioManager:Lcom/discord/stores/StoreAudioManager;

    return-object p0
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/stores/StoreStream;)Lcom/discord/utilities/time/Clock;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method public static final synthetic access$getCollector$cp()Lcom/discord/stores/StoreStream;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->collector:Lcom/discord/stores/StoreStream;

    return-object v0
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreStream;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getInitialized$p(Lcom/discord/stores/StoreStream;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStream;->initialized:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getStreamRtcConnection$p(Lcom/discord/stores/StoreStream;)Lcom/discord/stores/StoreStreamRtcConnection;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    return-object p0
.end method

.method public static final synthetic access$handleAuthToken(Lcom/discord/stores/StoreStream;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleAuthToken(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleBackgrounded(Lcom/discord/stores/StoreStream;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleBackgrounded(Z)V

    return-void
.end method

.method public static final synthetic access$handleBanAdd(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelBan;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleBanAdd(Lcom/discord/models/domain/ModelBan;)V

    return-void
.end method

.method public static final synthetic access$handleBanRemove(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelBan;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleBanRemove(Lcom/discord/models/domain/ModelBan;)V

    return-void
.end method

.method public static final synthetic access$handleCallCreateOrUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelCall;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleCallCreateOrUpdate(Lcom/discord/models/domain/ModelCall;)V

    return-void
.end method

.method public static final synthetic access$handleCallDelete(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelCall;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleCallDelete(Lcom/discord/models/domain/ModelCall;)V

    return-void
.end method

.method public static final synthetic access$handleChannelCreated(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method public static final synthetic access$handleChannelDeleted(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleChannelDeleted(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method public static final synthetic access$handleChannelSelected(Lcom/discord/stores/StoreStream;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreStream;->handleChannelSelected(J)V

    return-void
.end method

.method public static final synthetic access$handleChannelUnreadUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelChannelUnreadUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleChannelUnreadUpdate(Lcom/discord/models/domain/ModelChannelUnreadUpdate;)V

    return-void
.end method

.method public static final synthetic access$handleConnected(Lcom/discord/stores/StoreStream;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleConnected(Z)V

    return-void
.end method

.method public static final synthetic access$handleConnectionOpen(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelPayload;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    return-void
.end method

.method public static final synthetic access$handleConnectionReady(Lcom/discord/stores/StoreStream;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleConnectionReady(Z)V

    return-void
.end method

.method public static final synthetic access$handleEmojiUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleEmojiUpdate(Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;)V

    return-void
.end method

.method public static final synthetic access$handleFingerprint(Lcom/discord/stores/StoreStream;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleFingerprint(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleGroupDMRecipientAdd(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelChannel$Recipient;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGroupDMRecipientAdd(Lcom/discord/models/domain/ModelChannel$Recipient;)V

    return-void
.end method

.method public static final synthetic access$handleGroupDMRecipientRemove(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelChannel$Recipient;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGroupDMRecipientRemove(Lcom/discord/models/domain/ModelChannel$Recipient;)V

    return-void
.end method

.method public static final synthetic access$handleGuildAdd(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuild;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    return-void
.end method

.method public static final synthetic access$handleGuildApplicationCommands(Lcom/discord/stores/StoreStream;Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildApplicationCommands(Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;)V

    return-void
.end method

.method public static final synthetic access$handleGuildIntegrationUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuildIntegration$Update;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildIntegrationUpdate(Lcom/discord/models/domain/ModelGuildIntegration$Update;)V

    return-void
.end method

.method public static final synthetic access$handleGuildMemberAdd(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuildMember;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    return-void
.end method

.method public static final synthetic access$handleGuildMemberListUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildMemberListUpdate(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V

    return-void
.end method

.method public static final synthetic access$handleGuildMemberRemove(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuildMember;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildMemberRemove(Lcom/discord/models/domain/ModelGuildMember;)V

    return-void
.end method

.method public static final synthetic access$handleGuildMembersChunk(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuildMember$Chunk;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildMembersChunk(Lcom/discord/models/domain/ModelGuildMember$Chunk;)V

    return-void
.end method

.method public static final synthetic access$handleGuildRemove(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuild;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    return-void
.end method

.method public static final synthetic access$handleGuildRoleAdd(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuildRole$Payload;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildRoleAdd(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    return-void
.end method

.method public static final synthetic access$handleGuildRoleRemove(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelGuildRole$Payload;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildRoleRemove(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    return-void
.end method

.method public static final synthetic access$handleGuildSettingUpdated(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelNotificationSettings;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleGuildSettingUpdated(Lcom/discord/models/domain/ModelNotificationSettings;)V

    return-void
.end method

.method public static final synthetic access$handleMessageAck(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelReadState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleMessageAck(Lcom/discord/models/domain/ModelReadState;)V

    return-void
.end method

.method public static final synthetic access$handleMessageCreate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelMessage;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public static final synthetic access$handleMessageDelete(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelMessageDelete;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V

    return-void
.end method

.method public static final synthetic access$handleMessageUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelMessage;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public static final synthetic access$handleMessagesLoaded(Lcom/discord/stores/StoreStream;Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleMessagesLoaded(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V

    return-void
.end method

.method public static final synthetic access$handlePreLogout(Lcom/discord/stores/StoreStream;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreStream;->handlePreLogout()V

    return-void
.end method

.method public static final synthetic access$handlePresenceReplace(Lcom/discord/stores/StoreStream;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handlePresenceReplace(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$handlePresenceUpdate(Lcom/discord/stores/StoreStream;JLcom/discord/models/domain/ModelPresence;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreStream;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    return-void
.end method

.method public static final synthetic access$handleReactionAdd(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleReactionAdd(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method public static final synthetic access$handleReactionRemove(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleReactionRemove(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method public static final synthetic access$handleReactionRemoveAll(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleReactionRemoveAll(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method public static final synthetic access$handleReactionRemoveEmoji(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleReactionRemoveEmoji(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method public static final synthetic access$handleRelationshipRemove(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelUserRelationship;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleRelationshipRemove(Lcom/discord/models/domain/ModelUserRelationship;)V

    return-void
.end method

.method public static final synthetic access$handleRequiredActionUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleRequiredActionUpdate(Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;)V

    return-void
.end method

.method public static final synthetic access$handleRtcConnectionStateChanged(Lcom/discord/stores/StoreStream;Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleRtcConnectionStateChanged(Lcom/discord/rtcconnection/RtcConnection$State;)V

    return-void
.end method

.method public static final synthetic access$handleSessionsReplace(Lcom/discord/stores/StoreStream;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleSessionsReplace(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$handleSpeakingUsers(Lcom/discord/stores/StoreStream;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleSpeakingUsers(Ljava/util/Set;)V

    return-void
.end method

.method public static final synthetic access$handleStreamCreate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/StreamCreateOrUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleStreamCreate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V

    return-void
.end method

.method public static final synthetic access$handleStreamServerUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/StreamServerUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleStreamServerUpdate(Lcom/discord/models/domain/StreamServerUpdate;)V

    return-void
.end method

.method public static final synthetic access$handleStreamUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/StreamCreateOrUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleStreamUpdate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V

    return-void
.end method

.method public static final synthetic access$handleTypingStart(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelUser$Typing;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleTypingStart(Lcom/discord/models/domain/ModelUser$Typing;)V

    return-void
.end method

.method public static final synthetic access$handleUserNoteUpdated(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelUserNote$Update;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleUserNoteUpdated(Lcom/discord/models/domain/ModelUserNote$Update;)V

    return-void
.end method

.method public static final synthetic access$handleUserPaymentSourcesUpdate(Lcom/discord/stores/StoreStream;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreStream;->handleUserPaymentSourcesUpdate()V

    return-void
.end method

.method public static final synthetic access$handleUserSettingsUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleUserSettingsUpdate(Lcom/discord/models/domain/ModelUserSettings;)V

    return-void
.end method

.method public static final synthetic access$handleUserSubscriptionsUpdate(Lcom/discord/stores/StoreStream;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreStream;->handleUserSubscriptionsUpdate()V

    return-void
.end method

.method public static final synthetic access$handleVoiceChannelSelected(Lcom/discord/stores/StoreStream;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreStream;->handleVoiceChannelSelected(J)V

    return-void
.end method

.method public static final synthetic access$handleVoiceServerUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelVoice$Server;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleVoiceServerUpdate(Lcom/discord/models/domain/ModelVoice$Server;)V

    return-void
.end method

.method public static final synthetic access$handleVoiceStateUpdate(Lcom/discord/stores/StoreStream;Lcom/discord/models/domain/ModelVoice$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;)V

    return-void
.end method

.method public static final synthetic access$init(Lcom/discord/stores/StoreStream;Landroid/app/Application;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->init(Landroid/app/Application;)V

    return-void
.end method

.method public static final synthetic access$initGatewaySocketListeners(Lcom/discord/stores/StoreStream;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreStream;->initGatewaySocketListeners()V

    return-void
.end method

.method public static final synthetic access$isInitialized$cp()Z
    .locals 1

    sget-boolean v0, Lcom/discord/stores/StoreStream;->isInitialized:Z

    return v0
.end method

.method public static final synthetic access$setInitialized$cp(Z)V
    .locals 0

    sput-boolean p0, Lcom/discord/stores/StoreStream;->isInitialized:Z

    return-void
.end method

.method private final deferredInit(Landroid/app/Application;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreStream$deferredInit$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreStream$deferredInit$1;-><init>(Lcom/discord/stores/StoreStream;Landroid/app/Application;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lrx/Observable;->G()Lrx/Observable;

    move-result-object v0

    const-string p1, "onBackpressureBuffer()"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/discord/stores/StoreStream$dispatchSubscribe$1;

    invoke-direct {v4, p0, p3}, Lcom/discord/stores/StoreStream$dispatchSubscribe$1;-><init>(Lcom/discord/stores/StoreStream;Lkotlin/jvm/functions/Function1;)V

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x35

    const/4 v8, 0x0

    move-object v2, p2

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method public static final getAnalytics()Lcom/discord/stores/StoreAnalytics;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    return-object v0
.end method

.method public static final getAndroidPackages()Lcom/discord/stores/StoreAndroidPackages;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAndroidPackages()Lcom/discord/stores/StoreAndroidPackages;

    move-result-object v0

    return-object v0
.end method

.method public static final getApplication()Lcom/discord/stores/StoreApplication;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplication()Lcom/discord/stores/StoreApplication;

    move-result-object v0

    return-object v0
.end method

.method public static final getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationCommands()Lcom/discord/stores/StoreApplicationCommands;

    move-result-object v0

    return-object v0
.end method

.method public static final getApplicationStreamPreviews()Lcom/discord/stores/StoreApplicationStreamPreviews;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreamPreviews()Lcom/discord/stores/StoreApplicationStreamPreviews;

    move-result-object v0

    return-object v0
.end method

.method public static final getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v0

    return-object v0
.end method

.method public static final getAuditLog()Lcom/discord/stores/StoreAuditLog;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAuditLog()Lcom/discord/stores/StoreAuditLog;

    move-result-object v0

    return-object v0
.end method

.method public static final getAuthentication()Lcom/discord/stores/StoreAuthentication;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object v0

    return-object v0
.end method

.method public static final getBans()Lcom/discord/stores/StoreBans;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getBans()Lcom/discord/stores/StoreBans;

    move-result-object v0

    return-object v0
.end method

.method public static final getCalls()Lcom/discord/stores/StoreCalls;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getCalls()Lcom/discord/stores/StoreCalls;

    move-result-object v0

    return-object v0
.end method

.method public static final getCallsIncoming()Lcom/discord/stores/StoreCallsIncoming;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getCallsIncoming()Lcom/discord/stores/StoreCallsIncoming;

    move-result-object v0

    return-object v0
.end method

.method public static final getChangeLog()Lcom/discord/stores/StoreChangeLog;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChangeLog()Lcom/discord/stores/StoreChangeLog;

    move-result-object v0

    return-object v0
.end method

.method public static final getChannelFollowerStats()Lcom/discord/stores/StoreChannelFollowerStats;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelFollowerStats()Lcom/discord/stores/StoreChannelFollowerStats;

    move-result-object v0

    return-object v0
.end method

.method public static final getChannelMembers()Lcom/discord/stores/StoreChannelMembers;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelMembers()Lcom/discord/stores/StoreChannelMembers;

    move-result-object v0

    return-object v0
.end method

.method public static final getChannels()Lcom/discord/stores/StoreChannels;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    return-object v0
.end method

.method public static final getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v0

    return-object v0
.end method

.method public static final getChat()Lcom/discord/stores/StoreChat;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChat()Lcom/discord/stores/StoreChat;

    move-result-object v0

    return-object v0
.end method

.method public static final getConnectivity()Lcom/discord/stores/StoreConnectivity;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getConnectivity()Lcom/discord/stores/StoreConnectivity;

    move-result-object v0

    return-object v0
.end method

.method public static final getDynamicLinkCache()Lcom/discord/stores/StoreDynamicLink;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getDynamicLinkCache()Lcom/discord/stores/StoreDynamicLink;

    move-result-object v0

    return-object v0
.end method

.method public static final getEmojis()Lcom/discord/stores/StoreEmoji;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getEmojis()Lcom/discord/stores/StoreEmoji;

    move-result-object v0

    return-object v0
.end method

.method public static final getEntitlements()Lcom/discord/stores/StoreEntitlements;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getEntitlements()Lcom/discord/stores/StoreEntitlements;

    move-result-object v0

    return-object v0
.end method

.method public static final getExpandedGuildFolders()Lcom/discord/stores/StoreExpandedGuildFolders;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExpandedGuildFolders()Lcom/discord/stores/StoreExpandedGuildFolders;

    move-result-object v0

    return-object v0
.end method

.method public static final getExperiments()Lcom/discord/stores/StoreExperiments;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    return-object v0
.end method

.method public static final getGameParty()Lcom/discord/stores/StoreGameParty;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGameParty()Lcom/discord/stores/StoreGameParty;

    move-result-object v0

    return-object v0
.end method

.method public static final getGatewaySocket()Lcom/discord/stores/StoreGatewayConnection;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGatewaySocket()Lcom/discord/stores/StoreGatewayConnection;

    move-result-object v0

    return-object v0
.end method

.method public static final getGifting()Lcom/discord/stores/StoreGifting;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGifting()Lcom/discord/stores/StoreGifting;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildEmojis()Lcom/discord/stores/StoreEmojiGuild;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildEmojis()Lcom/discord/stores/StoreEmojiGuild;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildGating()Lcom/discord/stores/StoreGuildGating;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildGating()Lcom/discord/stores/StoreGuildGating;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildIntegrations()Lcom/discord/stores/StoreGuildIntegrations;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildIntegrations()Lcom/discord/stores/StoreGuildIntegrations;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildProfiles()Lcom/discord/stores/StoreGuildProfiles;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildProfiles()Lcom/discord/stores/StoreGuildProfiles;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildSelected()Lcom/discord/stores/StoreGuildSelected;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildSubscriptions()Lcom/discord/stores/StoreGuildSubscriptions;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSubscriptions()Lcom/discord/stores/StoreGuildSubscriptions;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildTemplates()Lcom/discord/stores/StoreGuildTemplates;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildTemplates()Lcom/discord/stores/StoreGuildTemplates;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildWelcomeScreens()Lcom/discord/stores/StoreGuildWelcomeScreens;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildWelcomeScreens()Lcom/discord/stores/StoreGuildWelcomeScreens;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuilds()Lcom/discord/stores/StoreGuilds;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildsNsfw()Lcom/discord/stores/StoreGuildsNsfw;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildsNsfw()Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object v0

    return-object v0
.end method

.method public static final getGuildsSorted()Lcom/discord/stores/StoreGuildsSorted;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildsSorted()Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstantInvites()Lcom/discord/stores/StoreInstantInvites;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getInstantInvites()Lcom/discord/stores/StoreInstantInvites;

    move-result-object v0

    return-object v0
.end method

.method public static final getInviteSettings()Lcom/discord/stores/StoreInviteSettings;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getInviteSettings()Lcom/discord/stores/StoreInviteSettings;

    move-result-object v0

    return-object v0
.end method

.method public static final getLibrary()Lcom/discord/stores/StoreLibrary;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getLibrary()Lcom/discord/stores/StoreLibrary;

    move-result-object v0

    return-object v0
.end method

.method public static final getLurking()Lcom/discord/stores/StoreLurking;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getLurking()Lcom/discord/stores/StoreLurking;

    move-result-object v0

    return-object v0
.end method

.method public static final getMFA()Lcom/discord/stores/StoreMFA;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMFA()Lcom/discord/stores/StoreMFA;

    move-result-object v0

    return-object v0
.end method

.method public static final getMediaEngine()Lcom/discord/stores/StoreMediaEngine;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v0

    return-object v0
.end method

.method public static final getMediaSettings()Lcom/discord/stores/StoreMediaSettings;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v0

    return-object v0
.end method

.method public static final getMentions()Lcom/discord/stores/StoreMentions;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMentions()Lcom/discord/stores/StoreMentions;

    move-result-object v0

    return-object v0
.end method

.method public static final getMessageAck()Lcom/discord/stores/StoreMessageAck;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageAck()Lcom/discord/stores/StoreMessageAck;

    move-result-object v0

    return-object v0
.end method

.method public static final getMessageReactions()Lcom/discord/stores/StoreMessageReactions;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageReactions()Lcom/discord/stores/StoreMessageReactions;

    move-result-object v0

    return-object v0
.end method

.method public static final getMessageState()Lcom/discord/stores/StoreMessageState;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageState()Lcom/discord/stores/StoreMessageState;

    move-result-object v0

    return-object v0
.end method

.method public static final getMessageUploads()Lcom/discord/stores/StoreMessageUploads;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageUploads()Lcom/discord/stores/StoreMessageUploads;

    move-result-object v0

    return-object v0
.end method

.method public static final getMessages()Lcom/discord/stores/StoreMessages;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessages()Lcom/discord/stores/StoreMessages;

    move-result-object v0

    return-object v0
.end method

.method public static final getMessagesLoader()Lcom/discord/stores/StoreMessagesLoader;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessagesLoader()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object v0

    return-object v0
.end method

.method public static final getMessagesMostRecent()Lcom/discord/stores/StoreMessagesMostRecent;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessagesMostRecent()Lcom/discord/stores/StoreMessagesMostRecent;

    move-result-object v0

    return-object v0
.end method

.method public static final getNavigation()Lcom/discord/stores/StoreNavigation;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object v0

    return-object v0
.end method

.method public static final getNotices()Lcom/discord/stores/StoreNotices;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    return-object v0
.end method

.method public static final getNotifications()Lcom/discord/stores/StoreNotifications;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotifications()Lcom/discord/stores/StoreNotifications;

    move-result-object v0

    return-object v0
.end method

.method public static final getNux()Lcom/discord/stores/StoreNux;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNux()Lcom/discord/stores/StoreNux;

    move-result-object v0

    return-object v0
.end method

.method public static final getPaymentSources()Lcom/discord/stores/StorePaymentSources;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPaymentSources()Lcom/discord/stores/StorePaymentSources;

    move-result-object v0

    return-object v0
.end method

.method public static final getPermissions()Lcom/discord/stores/StorePermissions;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    return-object v0
.end method

.method public static final getPinnedMessages()Lcom/discord/stores/StorePinnedMessages;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPinnedMessages()Lcom/discord/stores/StorePinnedMessages;

    move-result-object v0

    return-object v0
.end method

.method public static final getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v0

    return-object v0
.end method

.method public static final getPresences()Lcom/discord/stores/StoreUserPresence;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v0

    return-object v0
.end method

.method public static final getReadStates()Lcom/discord/stores/StoreReadStates;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object v0

    return-object v0
.end method

.method public static final getRepliedMessages()Lcom/discord/stores/StoreMessageReplies;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getRepliedMessages()Lcom/discord/stores/StoreMessageReplies;

    move-result-object v0

    return-object v0
.end method

.method public static final getReviewRequest()Lcom/discord/stores/StoreReviewRequest;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getReviewRequest()Lcom/discord/stores/StoreReviewRequest;

    move-result-object v0

    return-object v0
.end method

.method public static final getRtcConnection()Lcom/discord/stores/StoreRtcConnection;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v0

    return-object v0
.end method

.method public static final getRunningGame()Lcom/discord/stores/StoreRunningGame;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getRunningGame()Lcom/discord/stores/StoreRunningGame;

    move-result-object v0

    return-object v0
.end method

.method public static final getSearch()Lcom/discord/stores/StoreSearch;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object v0

    return-object v0
.end method

.method public static final getSlowMode()Lcom/discord/stores/StoreSlowMode;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSlowMode()Lcom/discord/stores/StoreSlowMode;

    move-result-object v0

    return-object v0
.end method

.method public static final getSpotify()Lcom/discord/stores/StoreSpotify;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSpotify()Lcom/discord/stores/StoreSpotify;

    move-result-object v0

    return-object v0
.end method

.method public static final getStoreChannelCategories()Lcom/discord/stores/StoreChannelCategories;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getStoreChannelCategories()Lcom/discord/stores/StoreChannelCategories;

    move-result-object v0

    return-object v0
.end method

.method public static final getStreamRtcConnection()Lcom/discord/stores/StoreStreamRtcConnection;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getStreamRtcConnection()Lcom/discord/stores/StoreStreamRtcConnection;

    move-result-object v0

    return-object v0
.end method

.method public static final getSubscriptions()Lcom/discord/stores/StoreSubscriptions;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v0

    return-object v0
.end method

.method public static final getUserAffinities()Lcom/discord/stores/StoreUserAffinities;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserAffinities()Lcom/discord/stores/StoreUserAffinities;

    move-result-object v0

    return-object v0
.end method

.method public static final getUserConnections()Lcom/discord/stores/StoreUserConnections;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserConnections()Lcom/discord/stores/StoreUserConnections;

    move-result-object v0

    return-object v0
.end method

.method public static final getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v0

    return-object v0
.end method

.method public static final getUserProfile()Lcom/discord/stores/StoreUserProfile;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserProfile()Lcom/discord/stores/StoreUserProfile;

    move-result-object v0

    return-object v0
.end method

.method public static final getUserRelationships()Lcom/discord/stores/StoreUserRelationships;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v0

    return-object v0
.end method

.method public static final getUserRequiredActions()Lcom/discord/stores/StoreUserRequiredActions;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserRequiredActions()Lcom/discord/stores/StoreUserRequiredActions;

    move-result-object v0

    return-object v0
.end method

.method public static final getUserSettings()Lcom/discord/stores/StoreUserSettings;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    return-object v0
.end method

.method public static final getUsers()Lcom/discord/stores/StoreUser;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    return-object v0
.end method

.method public static final getUsersMutualGuilds()Lcom/discord/stores/StoreUsersMutualGuilds;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsersMutualGuilds()Lcom/discord/stores/StoreUsersMutualGuilds;

    move-result-object v0

    return-object v0
.end method

.method public static final getUsersNotes()Lcom/discord/stores/StoreUserNotes;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsersNotes()Lcom/discord/stores/StoreUserNotes;

    move-result-object v0

    return-object v0
.end method

.method public static final getUsersTyping()Lcom/discord/stores/StoreUserTyping;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsersTyping()Lcom/discord/stores/StoreUserTyping;

    move-result-object v0

    return-object v0
.end method

.method public static final getVideoSupport()Lcom/discord/stores/StoreVideoSupport;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVideoSupport()Lcom/discord/stores/StoreVideoSupport;

    move-result-object v0

    return-object v0
.end method

.method public static final getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v0

    return-object v0
.end method

.method public static final getVoiceParticipants()Lcom/discord/stores/StoreVoiceParticipants;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceParticipants()Lcom/discord/stores/StoreVoiceParticipants;

    move-result-object v0

    return-object v0
.end method

.method public static final getVoiceStates()Lcom/discord/stores/StoreVoiceStates;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceStates()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v0

    return-object v0
.end method

.method private final handleAuthToken(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->authentication:Lcom/discord/stores/StoreAuthentication;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAuthentication;->handleAuthToken$app_productionDiscordExternalRelease(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleAuthToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesLoader:Lcom/discord/stores/StoreMessagesLoader;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesLoader;->handleAuthToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->notifications:Lcom/discord/stores/StoreNotifications;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreNotifications;->handleAuthToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->experiments:Lcom/discord/stores/StoreExperiments;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreExperiments;->handleAuthToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleAuthToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleAuthToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceStates;->handleAuthToken(Ljava/lang/String;)V

    return-void
.end method

.method private final handleBackgrounded(Z)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->connectivity:Lcom/discord/stores/StoreConnectivity;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreConnectivity;->handleBackgrounded(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesLoader:Lcom/discord/stores/StoreMessagesLoader;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesLoader;->handleBackgrounded(Z)V

    return-void
.end method

.method private final handleBanAdd(Lcom/discord/models/domain/ModelBan;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->bans:Lcom/discord/stores/StoreBans;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreBans;->handleBanAdd(Lcom/discord/models/domain/ModelBan;)V

    return-void
.end method

.method private final handleBanRemove(Lcom/discord/models/domain/ModelBan;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->bans:Lcom/discord/stores/StoreBans;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreBans;->handleBanRemove(Lcom/discord/models/domain/ModelBan;)V

    return-void
.end method

.method private final handleCallCreateOrUpdate(Lcom/discord/models/domain/ModelCall;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreCalls;->handleCallCreateOrUpdate(Lcom/discord/models/domain/ModelCall;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->callsIncoming:Lcom/discord/stores/StoreCallsIncoming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreCallsIncoming;->handleCallCreateOrUpdate(Lcom/discord/models/domain/ModelCall;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCall;->getVoiceStates()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelVoice$State;

    const-string/jumbo v1, "voiceState"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreStream;->handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final handleCallDelete(Lcom/discord/models/domain/ModelCall;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->callsIncoming:Lcom/discord/stores/StoreCallsIncoming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreCallsIncoming;->handleCallDelete(Lcom/discord/models/domain/ModelCall;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreCalls;->handleCallDelete(Lcom/discord/models/domain/ModelCall;)V

    return-void
.end method

.method private final handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannels;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channelConversions:Lcom/discord/stores/StoreChannelConversions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannelConversions;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePermissions;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleChannelCreated()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesMostRecent;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleChannelCreateOrDelete(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method private final handleChannelDeleted(Lcom/discord/models/domain/ModelChannel;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannels;->handleChannelDeleted(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePermissions;->handleChannelDeleted(Lcom/discord/models/domain/ModelChannel;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleChannelDeleted()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleChannelCreateOrDelete(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method

.method private final handleChannelSelected(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channelConversions:Lcom/discord/stores/StoreChannelConversions;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreChannelConversions;->handleChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreCalls;->handleChannelSelect(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMentions;->handleChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMessages;->handleChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesLoader:Lcom/discord/stores/StoreMessagesLoader;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMessagesLoader;->handleChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageStates:Lcom/discord/stores/StoreMessageState;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessageState;->handleChannelSelected()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageAck:Lcom/discord/stores/StoreMessageAck;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessageAck;->handleChannelSelected()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->notifications:Lcom/discord/stores/StoreNotifications;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreNotifications;->handleChannelSelected(J)V

    return-void
.end method

.method private final handleChannelUnreadUpdate(Lcom/discord/models/domain/ModelChannelUnreadUpdate;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesMostRecent;->handleChannelUnreadUpdate(Lcom/discord/models/domain/ModelChannelUnreadUpdate;)V

    return-void
.end method

.method private final handleConnected(Z)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessages;->handleConnected(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesLoader:Lcom/discord/stores/StoreMessagesLoader;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesLoader;->handleConnected(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleConnected(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->connectivity:Lcom/discord/stores/StoreConnectivity;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreConnectivity;->handleConnected(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->connectionOpen:Lcom/discord/stores/StoreConnectionOpen;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreConnectionOpen;->handleConnected(Z)V

    return-void
.end method

.method private final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    new-instance v6, Lcom/discord/utilities/time/TimeElapsed;

    iget-object v1, p0, Lcom/discord/stores/StoreStream;->clock:Lcom/discord/utilities/time/Clock;

    const-wide/16 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/utilities/time/TimeElapsed;-><init>(Lcom/discord/utilities/time/Clock;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sget-object v0, Lcom/discord/stores/ReadyPayloadUtils;->INSTANCE:Lcom/discord/stores/ReadyPayloadUtils;

    iget-object v1, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    iget-object v3, p0, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/discord/stores/ReadyPayloadUtils;->hydrateReadyPayload(Lcom/discord/models/domain/ModelPayload;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreEmojiCustom;)Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;

    move-result-object p1

    instance-of v0, p1, Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;

    invoke-virtual {p1}, Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;->getPayload()Lcom/discord/models/domain/ModelPayload;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userConnections:Lcom/discord/stores/StoreUserConnections;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserConnections;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserSettings;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRelationships:Lcom/discord/stores/StoreUserRelationships;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserRelationships;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRequiredAction:Lcom/discord/stores/StoreUserRequiredActions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserRequiredActions;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildMemberCounts;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildSelected;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserGuildSettings;->handleConnectionOpen$app_productionDiscordExternalRelease(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lurking:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreLurking;->handleConnectionOpen$app_productionDiscordExternalRelease(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannels;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannelsSelected;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->storeChannelCategories:Lcom/discord/stores/StoreChannelCategories;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannelCategories;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceStates;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0}, Lcom/discord/stores/StorePermissions;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmojiCustom;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserPresence;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userNotes:Lcom/discord/stores/StoreUserNotes;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserNotes;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->rtcConnection:Lcom/discord/stores/StoreRtcConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->experiments:Lcom/discord/stores/StoreExperiments;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreExperiments;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessages;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesMostRecent;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageAck:Lcom/discord/stores/StoreMessageAck;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageAck;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    invoke-virtual {v0}, Lcom/discord/stores/StoreCalls;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mediaEngine:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMediaEngine;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gameParty:Lcom/discord/stores/StoreGameParty;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGameParty;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberRequesterStore:Lcom/discord/stores/StoreGuildMemberRequester;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildMemberRequester;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->reviewRequestStore:Lcom/discord/stores/StoreReviewRequest;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreReviewRequest;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->connectionOpen:Lcom/discord/stores/StoreConnectionOpen;

    invoke-virtual {v0}, Lcom/discord/stores/StoreConnectionOpen;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->library:Lcom/discord/stores/StoreLibrary;

    invoke-virtual {v0}, Lcom/discord/stores/StoreLibrary;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessageReactions;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->spotify:Lcom/discord/stores/StoreSpotify;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreSpotify;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->changeLogStore:Lcom/discord/stores/StoreChangeLog;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChangeLog;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->rtcRegion:Lcom/discord/stores/StoreRtcRegion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreRtcRegion;->handleConnectionOpen$app_productionDiscordExternalRelease()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userAffinities:Lcom/discord/stores/StoreUserAffinities;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserAffinities;->handleConnectionOpen()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Processed ready payload in "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lcom/discord/utilities/time/TimeElapsed;->getSeconds()F

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, " seconds"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/discord/stores/StoreStream;->handleHydrateError()V

    return-void
.end method

.method private final handleConnectionReady(Z)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->rtcConnection:Lcom/discord/stores/StoreRtcConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleConnectionReady(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreCalls;->handleConnectionReady(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->connectivity:Lcom/discord/stores/StoreConnectivity;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreConnectivity;->handleConnectionReady(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildSubscriptions;->handleConnectionReady(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberRequesterStore:Lcom/discord/stores/StoreGuildMemberRequester;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildMemberRequester;->handleConnectionReady(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->spotify:Lcom/discord/stores/StoreSpotify;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreSpotify;->handleConnectionReady(Z)V

    return-void
.end method

.method private final handleEmojiUpdate(Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmojiCustom;->handleEmojiUpdate(Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildEmojis:Lcom/discord/stores/StoreEmojiGuild;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmojiGuild;->handleEmojiUpdate(Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleEmojiUpdate(Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;)V

    return-void
.end method

.method private final handleFingerprint(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->experiments:Lcom/discord/stores/StoreExperiments;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreExperiments;->handleFingerprint(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleFingerprint(Ljava/lang/String;)V

    return-void
.end method

.method private final handleGroupDMRecipientAdd(Lcom/discord/models/domain/ModelChannel$Recipient;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/discord/stores/StoreChannels;->handleGroupDMRecipient(Lcom/discord/models/domain/ModelChannel$Recipient;Z)V

    return-void
.end method

.method private final handleGroupDMRecipientRemove(Lcom/discord/models/domain/ModelChannel$Recipient;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/discord/stores/StoreChannels;->handleGroupDMRecipient(Lcom/discord/models/domain/ModelChannel$Recipient;Z)V

    return-void
.end method

.method private final handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/discord/stores/ReadyPayloadUtils;->INSTANCE:Lcom/discord/stores/ReadyPayloadUtils;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/stores/ReadyPayloadUtils;->hydrateGuild$default(Lcom/discord/stores/ReadyPayloadUtils;Lcom/discord/models/domain/ModelGuild;Ljava/util/List;Ljava/util/Map;ILjava/lang/Object;)Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;

    move-result-object p1

    instance-of v0, p1, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;

    invoke-virtual {p1}, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleGuildAddOrSync(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildMemberCounts;->handleGuildCreate(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserPresence;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannels;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePermissions;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmojiCustom;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)Lkotlin/Unit;

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesMostRecent;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceStates;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gameParty:Lcom/discord/stores/StoreGameParty;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGameParty;->handleGuildCreateOrSync(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lurking:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreLurking;->handleGuildAdd$app_productionDiscordExternalRelease(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/discord/stores/StoreStream;->handleHydrateError()V

    return-void
.end method

.method private final handleGuildApplicationCommands(Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationCommands:Lcom/discord/stores/StoreApplicationCommands;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationCommands;->handleApplicationCommandsUpdate(Lcom/discord/models/slashcommands/ModelGatewayGuildApplicationCommands;)V

    return-void
.end method

.method private final handleGuildIntegrationUpdate(Lcom/discord/models/domain/ModelGuildIntegration$Update;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->integrations:Lcom/discord/stores/StoreGuildIntegrations;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildIntegrations;->handleUpdate(Lcom/discord/models/domain/ModelGuildIntegration$Update;)V

    return-void
.end method

.method private final handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildMemberCounts;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePermissions;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmojiCustom;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    return-void
.end method

.method private final handleGuildMemberListUpdate(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getOperations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;->getItem()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getGuildId()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;->getItem()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/discord/stores/StoreStream;->handleItem(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)V

    :cond_1
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;->getItems()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMemberListUpdate;->getGuildId()J

    move-result-wide v3

    invoke-direct {p0, v3, v4, v2}, Lcom/discord/stores/StoreStream;->handleItem(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lazyChannelMembersStore:Lcom/discord/stores/StoreChannelMembers;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannelMembers;->handleGuildMemberListUpdate(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V

    return-void
.end method

.method private final handleGuildMemberRemove(Lcom/discord/models/domain/ModelGuildMember;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMember;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleGuildMemberRemove(Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMember;->getGuildId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildMemberCounts;->handleGuildMemberRemove(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserPresence;->handleGuildMemberRemove(Lcom/discord/models/domain/ModelGuildMember;)V

    :cond_0
    return-void
.end method

.method private final handleGuildMembersChunk(Lcom/discord/models/domain/ModelGuildMember$Chunk;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleGuildMembersChunk(Lcom/discord/models/domain/ModelGuildMember$Chunk;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleGuildMembersChunk(Lcom/discord/models/domain/ModelGuildMember$Chunk;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMember$Chunk;->getPresences()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMember$Chunk;->getGuildId()J

    move-result-wide v2

    const-string v4, "presence"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3, v1}, Lcom/discord/stores/StoreStream;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberRequesterStore:Lcom/discord/stores/StoreGuildMemberRequester;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildMemberRequester;->handleGuildMembersChunk(Lcom/discord/models/domain/ModelGuildMember$Chunk;)V

    return-void
.end method

.method private final handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildMemberCounts;->handleGuildDelete(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildSubscriptions;->handleGuildRemove(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lazyChannelMembersStore:Lcom/discord/stores/StoreChannelMembers;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreChannelMembers;->handleGuildRemove(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserPresence;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuildSelected;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannels;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePermissions;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmojiCustom;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleGuildRemove()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceStates;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lurking:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreLurking;->handleGuildRemove$app_productionDiscordExternalRelease(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V

    return-void
.end method

.method private final handleGuildRoleAdd(Lcom/discord/models/domain/ModelGuildRole$Payload;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole$Payload;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleGuildRoleAdd(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePermissions;->handleGuildRoleAdd(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleGuildRoleAdd()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lazyChannelMembersStore:Lcom/discord/stores/StoreChannelMembers;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreChannelMembers;->handleGuildRoleUpdate(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleRoleAddOrRemove(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleGuildRoleRemove(Lcom/discord/models/domain/ModelGuildRole$Payload;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole$Payload;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGuilds;->handleGuildRoleRemove(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePermissions;->handleGuildRoleRemove(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleGuildRoleRemove()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleRoleAddOrRemove(Lcom/discord/models/domain/ModelGuildRole$Payload;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleGuildSettingUpdated(Lcom/discord/models/domain/ModelNotificationSettings;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStream;->guildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreUserGuildSettings;->handleGuildSettingUpdated$app_productionDiscordExternalRelease(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleGuildSettingUpdated(Lcom/discord/models/domain/ModelNotificationSettings;)V

    return-void
.end method

.method private final handleHydrateError()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0}, Lcom/discord/stores/StoreClientDataState;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->resetOnError()Lkotlin/Unit;

    return-void
.end method

.method private final handleItem(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    instance-of v0, p3, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;

    if-eqz v0, :cond_0

    check-cast p3, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object p3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lcom/discord/stores/StoreStream;->synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember;)V

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object p3

    if-eqz p3, :cond_0

    const-string v0, "presence"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreStream;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    :cond_0
    return-void
.end method

.method private final handleMessageAck(Lcom/discord/models/domain/ModelReadState;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleMessageAck(Lcom/discord/models/domain/ModelReadState;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageAck:Lcom/discord/stores/StoreMessageAck;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageAck;->handleMessageAck(Lcom/discord/models/domain/ModelReadState;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleMessageAck(Lcom/discord/models/domain/ModelReadState;)V

    return-void
.end method

.method private final handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->processMessageUsers(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageUploads:Lcom/discord/stores/StoreMessageUploads;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageUploads;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleMessageCreateOrUpdate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleMessageCreateOrUpdate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->usersTyping:Lcom/discord/stores/StoreUserTyping;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserTyping;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreMessages;->handleMessageCreate(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessagesMostRecent;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageAck:Lcom/discord/stores/StoreMessageAck;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageAck;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReplies:Lcom/discord/stores/StoreMessageReplies;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReplies;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->notifications:Lcom/discord/stores/StoreNotifications;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreNotifications;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreClientDataState;->handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method private final handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessages;->handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleMessageDeleted(Lcom/discord/models/domain/ModelMessageDelete;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->pinnedMessages:Lcom/discord/stores/StorePinnedMessages;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getChannelId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getMessageIds()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/stores/StorePinnedMessages;->handleMessageDeleteBulk(JLjava/util/Collection;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageStates:Lcom/discord/stores/StoreMessageState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageState;->handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReplies:Lcom/discord/stores/StoreMessageReplies;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReplies;->handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->pendingReplies:Lcom/discord/stores/StorePendingReplies;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePendingReplies;->handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V

    return-void
.end method

.method private final handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStream;->processMessageUsers(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMentions;->handleMessageCreateOrUpdate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleMessageCreateOrUpdate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessages;->handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReplies:Lcom/discord/stores/StoreMessageReplies;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReplies;->handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->pinnedMessages:Lcom/discord/stores/StorePinnedMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StorePinnedMessages;->handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageStates:Lcom/discord/stores/StoreMessageState;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageState;->handleMessageUpdate(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method private final handleMessagesLoaded(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleMessagesLoaded(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessages;->handleMessagesLoaded(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberRequesterStore:Lcom/discord/stores/StoreGuildMemberRequester;

    invoke-virtual {p1}, Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;->getChannelId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;->getMessages()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/stores/StoreGuildMemberRequester;->handleLoadMessages(JLjava/util/Collection;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReplies:Lcom/discord/stores/StoreMessageReplies;

    invoke-virtual {p1}, Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;->getMessages()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReplies;->handleLoadMessages(Ljava/util/Collection;)V

    return-void
.end method

.method private final handlePreLogout()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildSelected;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildSubscriptions;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->authentication:Lcom/discord/stores/StoreAuthentication;

    invoke-virtual {v0}, Lcom/discord/stores/StoreAuthentication;->handlePreLogout$app_productionDiscordExternalRelease()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gifting:Lcom/discord/stores/StoreGifting;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGifting;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->spotify:Lcom/discord/stores/StoreSpotify;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSpotify;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->paymentSources:Lcom/discord/stores/StorePaymentSources;

    invoke-virtual {v0}, Lcom/discord/stores/StorePaymentSources;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->subscriptions:Lcom/discord/stores/StoreSubscriptions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->notifications:Lcom/discord/stores/StoreNotifications;

    invoke-virtual {v0}, Lcom/discord/stores/StoreNotifications;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0}, Lcom/discord/stores/StoreAnalytics;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->tabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreTabsNavigation;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRelationships:Lcom/discord/stores/StoreUserRelationships;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMessages;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->emojis:Lcom/discord/stores/StoreEmoji;

    invoke-virtual {v0}, Lcom/discord/stores/StoreEmoji;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->stickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->experiments:Lcom/discord/stores/StoreExperiments;

    invoke-virtual {v0}, Lcom/discord/stores/StoreExperiments;->handlePreLogout()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->pendingReplies:Lcom/discord/stores/StorePendingReplies;

    invoke-virtual {v0}, Lcom/discord/stores/StorePendingReplies;->handlePreLogout()V

    return-void
.end method

.method private final handlePresenceReplace(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserPresence;->handlePresenceReplace(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gameParty:Lcom/discord/stores/StoreGameParty;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGameParty;->handlePresenceReplace(Ljava/util/List;)V

    return-void
.end method

.method private final handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p3}, Lcom/discord/stores/StoreUser;->handlePresenceUpdate(Lcom/discord/models/domain/ModelPresence;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreUserPresence;->handlePresenceUpdate(JLcom/discord/models/domain/ModelPresence;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gameParty:Lcom/discord/stores/StoreGameParty;

    invoke-virtual {v0, p3, p1, p2}, Lcom/discord/stores/StoreGameParty;->handlePresenceUpdate(Lcom/discord/models/domain/ModelPresence;J)V

    return-void
.end method

.method private final handleReactionAdd(Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreMessages;->handleReactionUpdate(Ljava/util/List;Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReactions;->handleReactionAdd(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method private final handleReactionRemove(Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreMessages;->handleReactionUpdate(Ljava/util/List;Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReactions;->handleReactionRemove(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method private final handleReactionRemoveAll(Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessages;->handleReactionsRemoveAll(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReactions;->handleReactionRemoveAll(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method private final handleReactionRemoveEmoji(Lcom/discord/models/domain/ModelMessageReaction$Update;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessages;->handleReactionsRemoveEmoji(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMessageReactions;->handleReactionRemoveEmoji(Lcom/discord/models/domain/ModelMessageReaction$Update;)V

    return-void
.end method

.method private final handleRelationshipRemove(Lcom/discord/models/domain/ModelUserRelationship;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRelationships:Lcom/discord/stores/StoreUserRelationships;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserRelationships;->handleRelationshipRemove(Lcom/discord/models/domain/ModelUserRelationship;)V

    return-void
.end method

.method private final handleRequiredActionUpdate(Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRequiredAction:Lcom/discord/stores/StoreUserRequiredActions;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserRequiredActions;->handleUserRequiredActionUpdate(Lcom/discord/models/domain/ModelUser$RequiredActionUpdate;)V

    return-void
.end method

.method private final handleRtcConnectionStateChanged(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGatewayConnection;->handleRtcConnectionStateChanged(Lcom/discord/rtcconnection/RtcConnection$State;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleRtcConnectionStateChanged(Lcom/discord/rtcconnection/RtcConnection$State;)V

    return-void
.end method

.method private final handleSessionsReplace(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelSession;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserPresence;->handleSessionsReplace(Ljava/util/List;)V

    return-void
.end method

.method private final handleSpeakingUsers(Ljava/util/Set;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleUserSpeaking(Ljava/util/Set;)V

    return-void
.end method

.method private final handleStreamCreate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->handleStreamCreate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamCreate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V

    return-void
.end method

.method private final handleStreamServerUpdate(Lcom/discord/models/domain/StreamServerUpdate;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->handleStreamServerUpdate(Lcom/discord/models/domain/StreamServerUpdate;)V

    return-void
.end method

.method private final handleStreamUpdate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamUpdate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V

    return-void
.end method

.method private final handleTypingStart(Lcom/discord/models/domain/ModelUser$Typing;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser$Typing;->getGuildId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser$Typing;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMember;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser$Typing;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/stores/StoreStream;->synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->usersTyping:Lcom/discord/stores/StoreUserTyping;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserTyping;->handleTypingStart(Lcom/discord/models/domain/ModelUser$Typing;)V

    return-void
.end method

.method private final handleUserNoteUpdated(Lcom/discord/models/domain/ModelUserNote$Update;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userNotes:Lcom/discord/stores/StoreUserNotes;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserNotes;->handleNoteUpdate(Lcom/discord/models/domain/ModelUserNote$Update;)V

    return-void
.end method

.method private final handleUserPaymentSourcesUpdate()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->paymentSources:Lcom/discord/stores/StorePaymentSources;

    invoke-virtual {v0}, Lcom/discord/stores/StorePaymentSources;->handleUserPaymentSourcesUpdate()V

    return-void
.end method

.method private final handleUserSettingsUpdate(Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserSettings;->handleUserSettingsUpdate(Lcom/discord/models/domain/ModelUserSettings;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserPresence;->handleUserSettingsUpdate(Lcom/discord/models/domain/ModelUserSettings;)V

    return-void
.end method

.method private final handleUserSubscriptionsUpdate()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->subscriptions:Lcom/discord/stores/StoreSubscriptions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions;->handleUserSubscriptionsUpdate()V

    return-void
.end method

.method private final handleVoiceChannelSelected(J)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices;->handleVoiceChannelSelected()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->rtcConnection:Lcom/discord/stores/StoreRtcConnection;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreRtcConnection;->handleVoiceChannelSelected(Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreApplicationStreaming;->handleVoiceChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->videoStreams:Lcom/discord/stores/StoreVideoStreams;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreVideoStreams;->handleVoiceChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceSpeaking:Lcom/discord/stores/StoreVoiceSpeaking;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreVoiceSpeaking;->handleVoiceChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->callsIncoming:Lcom/discord/stores/StoreCallsIncoming;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreCallsIncoming;->handleVoiceChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mediaEngine:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMediaEngine;->handleVoiceChannelSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mediaSettings:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMediaSettings;->handleVoiceChannelSelected(J)V

    return-void
.end method

.method private final handleVoiceServerUpdate(Lcom/discord/models/domain/ModelVoice$Server;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->rtcConnection:Lcom/discord/stores/StoreRtcConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreRtcConnection;->handleVoiceServerUpdate(Lcom/discord/models/domain/ModelVoice$Server;)V

    return-void
.end method

.method private final handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelVoice$State;->getGuildId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelVoice$State;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMember;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelVoice$State;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/stores/StoreStream;->synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceStates;->handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    const-wide/16 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v6}, Lcom/discord/stores/StoreApplicationStreaming;->handleVoiceStateUpdate$default(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/models/domain/ModelVoice$State;JILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleVoiceStateUpdates(Lcom/discord/models/domain/ModelVoice$State;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->videoStreams:Lcom/discord/stores/StoreVideoStreams;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVideoStreams;->handleVoiceStateUpdates(Lcom/discord/models/domain/ModelVoice$State;)V

    return-void
.end method

.method private final init(Landroid/app/Application;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->authentication:Lcom/discord/stores/StoreAuthentication;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAuthentication;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserSettings;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->emojis:Lcom/discord/stores/StoreEmoji;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreEmoji;->initBlocking(Landroid/content/Context;)V

    sget-object v0, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    iget-object v1, p0, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->init(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    new-instance v0, Lcom/discord/utilities/StoreUIEventHandler;

    iget-object v1, p0, Lcom/discord/stores/StoreStream;->mediaEngine:Lcom/discord/stores/StoreMediaEngine;

    invoke-direct {v0, p1, v1}, Lcom/discord/utilities/StoreUIEventHandler;-><init>(Landroid/content/Context;Lcom/discord/stores/StoreMediaEngine;)V

    return-void
.end method

.method private final initGatewaySocketListeners()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getReady()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$1;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamConnectionOpen"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getConnected()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$2;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$2;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamConnected"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getConnectionReady()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$3;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$3;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamReady"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildApplicationCommands()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$4;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$4;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string v2, "guildApplicationCommands"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildCreateOrUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$5;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$5;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildAdd"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildRoleCreateOrUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$6;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$6;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildRoleAdd"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildDeleted()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$7;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$7;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildRemove"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildRoleDelete()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$8;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$8;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildRoleRemove"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildBanAdd()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$9;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$9;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamBanAdd"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildBanRemove()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$10;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$10;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamBanRemove"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildIntegrationsUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$11;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$11;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildIntegrationUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildMembersAdd()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$12;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$12;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildMemberAdd"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildMembersChunk()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$13;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$13;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildMemberChunk"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildMemberRemove()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$14;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$14;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildMemberRemove"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getChannelCreateOrUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$15;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$15;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamChannelCreated"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getChannelDeleted()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$16;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$16;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamChannelDeleted"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getChannelUnreadUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$17;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$17;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamChannelUnreadUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$18;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$18;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamUserUpdated"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserNoteUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$19;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$19;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string v2, "handleUserNoteUpdated"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getRelationshipAdd()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$20;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$20;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamRelationshipAdd"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getRelationshipRemove()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$21;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$21;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamRelationshipRemove"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$22;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$22;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamMessageUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageCreate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$23;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$23;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamMessageCreate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageReactionAdd()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$24;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$24;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamReactionAdd"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageReactionRemove()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$25;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$25;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamReactionRemove"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageReactionRemoveEmoji()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$26;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$26;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamMessageRemoveEmoji"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageReactionRemoveAll()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$27;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$27;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamMessageRemoveAll"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageDelete()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$28;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$28;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamMessageDelete"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getMessageAck()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$29;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$29;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamMessageAck"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getVoiceStateUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$30;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$30;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamVoiceStateUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getVoiceServerUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$31;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$31;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamVoiceServerUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserGuildSettingsUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$32;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$32;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGuildSettingUpdated"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserSettingsUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$33;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$33;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamUserSettingsUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getTypingStart()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$34;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$34;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamTypingStart"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getPresenceUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$35;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$35;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamPresenceUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getPresenceReplace()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$36;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$36;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamPresenceReplace"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getChannelRecipientAdd()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$37;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$37;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGroupDMRecipientAdd"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getChannelRecipientRemove()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$38;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$38;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamGroupDMRecipientRemove"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getCallDelete()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$39;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$39;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamCallDelete"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getCallCreateOrUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$40;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$40;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamCallCreateOrUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildEmojisUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$41;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$41;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamEmojisUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserRequiredActionUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$42;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$42;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamUserRequiredActionUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getGuildMemberListUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$43;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$43;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string v2, "guildMemberListUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getSessionsReplace()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$44;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$44;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamSessionsReplace"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserPaymentSourcesUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$45;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$45;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamUserPaymentSourcesUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserSubscriptionsUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$46;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$46;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamUserSubscriptionsUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getStreamCreate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$47;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$47;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamStreamCreate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getStreamUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$48;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$48;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamStreamUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getStreamDelete()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$49;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$49;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamStreamDelete"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getStreamServerUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$50;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$50;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "streamStreamServerUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGatewayConnection;->getUserStickerPackUpdate()Lrx/subjects/SerializedSubject;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$51;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$51;-><init>(Lcom/discord/stores/StoreStream;)V

    const-string/jumbo v2, "userStickerPackUpdate"

    invoke-direct {p0, v0, v2, v1}, Lcom/discord/stores/StoreStream;->dispatchSubscribe(Lrx/Observable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final processMessageUsers(Lcom/discord/models/domain/ModelMessage;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/stores/StoreStream;->synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getMentions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    const-string v3, "mentionedUser"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getMember()Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v3

    invoke-direct {p0, v2, v1, v3}, Lcom/discord/stores/StoreStream;->synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final registerDispatchHandlers()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->dispatcher:Lcom/discord/stores/Dispatcher;

    const/16 v1, 0x27

    new-array v1, v1, [Lcom/discord/stores/DispatchHandler;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->userRelationships:Lcom/discord/stores/StoreUserRelationships;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->lazyChannelMembersStore:Lcom/discord/stores/StoreChannelMembers;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->customEmojis:Lcom/discord/stores/StoreEmojiCustom;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->gameParty:Lcom/discord/stores/StoreGameParty;

    const/4 v3, 0x7

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->bans:Lcom/discord/stores/StoreBans;

    const/16 v3, 0x8

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->guildEmojis:Lcom/discord/stores/StoreEmojiGuild;

    const/16 v3, 0x9

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->auditLog:Lcom/discord/stores/StoreAuditLog;

    const/16 v3, 0xa

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->messageAck:Lcom/discord/stores/StoreMessageAck;

    const/16 v3, 0xb

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    const/16 v3, 0xc

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->messageUploads:Lcom/discord/stores/StoreMessageUploads;

    const/16 v3, 0xd

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    const/16 v3, 0xe

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->usersTyping:Lcom/discord/stores/StoreUserTyping;

    const/16 v3, 0xf

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->application:Lcom/discord/stores/StoreApplication;

    const/16 v3, 0x10

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->paymentSources:Lcom/discord/stores/StorePaymentSources;

    const/16 v3, 0x11

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->subscriptions:Lcom/discord/stores/StoreSubscriptions;

    const/16 v3, 0x12

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->mfa:Lcom/discord/stores/StoreMFA;

    const/16 v3, 0x13

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->guildsSorted:Lcom/discord/stores/StoreGuildsSorted;

    const/16 v3, 0x14

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->applicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

    const/16 v3, 0x15

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->premiumGuildSubscriptions:Lcom/discord/stores/StorePremiumGuildSubscription;

    const/16 v3, 0x16

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->expandedGuildFolders:Lcom/discord/stores/StoreExpandedGuildFolders;

    const/16 v3, 0x17

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    const/16 v3, 0x18

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->guildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    const/16 v3, 0x19

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->entitlements:Lcom/discord/stores/StoreEntitlements;

    const/16 v3, 0x1a

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    const/16 v3, 0x1b

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->userAffinities:Lcom/discord/stores/StoreUserAffinities;

    const/16 v3, 0x1c

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    const/16 v3, 0x1d

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->storeChannelCategories:Lcom/discord/stores/StoreChannelCategories;

    const/16 v3, 0x1e

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->guildTemplates:Lcom/discord/stores/StoreGuildTemplates;

    const/16 v3, 0x1f

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->tabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    const/16 v3, 0x20

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->userNotes:Lcom/discord/stores/StoreUserNotes;

    const/16 v3, 0x21

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->maskedLinks:Lcom/discord/stores/StoreMaskedLinks;

    const/16 v3, 0x22

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->stickers:Lcom/discord/stores/StoreStickers;

    const/16 v3, 0x23

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->googlePlayPurchases:Lcom/discord/stores/StoreGooglePlayPurchases;

    const/16 v3, 0x24

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->googlePlaySkuDetails:Lcom/discord/stores/StoreGooglePlaySkuDetails;

    const/16 v3, 0x25

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/discord/stores/StoreStream;->guildGating:Lcom/discord/stores/StoreGuildGating;

    const/16 v3, 0x26

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->registerDispatchHandlers([Lcom/discord/stores/DispatchHandler;)V

    return-void
.end method

.method public static synthetic streamCreate$default(Lcom/discord/stores/StoreStream;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreStream;->streamCreate(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember;->getRoles()Ljava/util/List;

    move-result-object v1

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object v5, v0

    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember;->getNick()Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    goto :goto_1

    :cond_1
    move-object v6, v0

    :goto_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember;->getPremiumSince()Ljava/lang/String;

    move-result-object v1

    move-object v7, v1

    goto :goto_2

    :cond_2
    move-object v7, v0

    :goto_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelGuildMember;->isPending()Z

    move-result p3

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_3
    move-object v8, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v8}, Lcom/discord/stores/StoreStream;->synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method private final synthesizeGuildMemberAdd(Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-nez p3, :cond_0

    goto :goto_1

    :cond_0
    new-instance v8, Lcom/discord/models/domain/ModelGuildMember;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    if-eqz p6, :cond_1

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    move v7, p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    const/4 v7, 0x0

    :goto_0
    move-object v0, v8

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/discord/models/domain/ModelGuildMember;-><init>(JLcom/discord/models/domain/ModelUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {p0, v8}, Lcom/discord/stores/StoreStream;->handleGuildMemberAdd(Lcom/discord/models/domain/ModelGuildMember;)V

    :cond_2
    :goto_1
    return-void
.end method


# virtual methods
.method public final getAccessibility$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAccessibility;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->accessibility:Lcom/discord/stores/StoreAccessibility;

    return-object v0
.end method

.method public final getAnalytics$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAnalytics;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    return-object v0
.end method

.method public final getAndroidPackages$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAndroidPackages;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->androidPackages:Lcom/discord/stores/StoreAndroidPackages;

    return-object v0
.end method

.method public final getApplication$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->application:Lcom/discord/stores/StoreApplication;

    return-object v0
.end method

.method public final getApplicationCommands$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationCommands;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationCommands:Lcom/discord/stores/StoreApplicationCommands;

    return-object v0
.end method

.method public final getApplicationStreamPreviews$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreamPreviews;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

    return-object v0
.end method

.method public final getApplicationStreaming$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    return-object v0
.end method

.method public final getAudioDevices$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAudioDevices;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    return-object v0
.end method

.method public final getAuditLog$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAuditLog;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->auditLog:Lcom/discord/stores/StoreAuditLog;

    return-object v0
.end method

.method public final getAuthentication$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAuthentication;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->authentication:Lcom/discord/stores/StoreAuthentication;

    return-object v0
.end method

.method public final getBans$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreBans;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->bans:Lcom/discord/stores/StoreBans;

    return-object v0
.end method

.method public final getCalls$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreCalls;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    return-object v0
.end method

.method public final getCallsIncoming$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreCallsIncoming;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->callsIncoming:Lcom/discord/stores/StoreCallsIncoming;

    return-object v0
.end method

.method public final getChangeLogStore$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChangeLog;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->changeLogStore:Lcom/discord/stores/StoreChangeLog;

    return-object v0
.end method

.method public final getChannelFollowerStats$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelFollowerStats;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channelFollowerStats:Lcom/discord/stores/StoreChannelFollowerStats;

    return-object v0
.end method

.method public final getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channels:Lcom/discord/stores/StoreChannels;

    return-object v0
.end method

.method public final getChannelsSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelsSelected;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->channelsSelected:Lcom/discord/stores/StoreChannelsSelected;

    return-object v0
.end method

.method public final getChat$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChat;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->chat:Lcom/discord/stores/StoreChat;

    return-object v0
.end method

.method public final getClientDataState$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreClientDataState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientDataState:Lcom/discord/stores/StoreClientDataState;

    return-object v0
.end method

.method public final getClientVersion$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreClientVersion;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->clientVersion:Lcom/discord/stores/StoreClientVersion;

    return-object v0
.end method

.method public final getConnectionOpen$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreConnectionOpen;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->connectionOpen:Lcom/discord/stores/StoreConnectionOpen;

    return-object v0
.end method

.method public final getConnectivity$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreConnectivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->connectivity:Lcom/discord/stores/StoreConnectivity;

    return-object v0
.end method

.method public final getEmojis$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreEmoji;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->emojis:Lcom/discord/stores/StoreEmoji;

    return-object v0
.end method

.method public final getEntitlements$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreEntitlements;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->entitlements:Lcom/discord/stores/StoreEntitlements;

    return-object v0
.end method

.method public final getExpandedGuildFolders$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreExpandedGuildFolders;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->expandedGuildFolders:Lcom/discord/stores/StoreExpandedGuildFolders;

    return-object v0
.end method

.method public final getExperiments$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreExperiments;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->experiments:Lcom/discord/stores/StoreExperiments;

    return-object v0
.end method

.method public final getExpressionPickerNavigation$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreExpressionPickerNavigation;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->expressionPickerNavigation:Lcom/discord/stores/StoreExpressionPickerNavigation;

    return-object v0
.end method

.method public final getGameParty$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGameParty;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gameParty:Lcom/discord/stores/StoreGameParty;

    return-object v0
.end method

.method public final getGatewaySocket$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGatewayConnection;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    return-object v0
.end method

.method public final getGifPicker$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGifPicker;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gifPicker:Lcom/discord/stores/StoreGifPicker;

    return-object v0
.end method

.method public final getGifting$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGifting;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gifting:Lcom/discord/stores/StoreGifting;

    return-object v0
.end method

.method public final getGooglePlayPurchases$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGooglePlayPurchases;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->googlePlayPurchases:Lcom/discord/stores/StoreGooglePlayPurchases;

    return-object v0
.end method

.method public final getGooglePlaySkuDetails$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGooglePlaySkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->googlePlaySkuDetails:Lcom/discord/stores/StoreGooglePlaySkuDetails;

    return-object v0
.end method

.method public final getGuildEmojis$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreEmojiGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildEmojis:Lcom/discord/stores/StoreEmojiGuild;

    return-object v0
.end method

.method public final getGuildGating$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildGating;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildGating:Lcom/discord/stores/StoreGuildGating;

    return-object v0
.end method

.method public final getGuildInvite$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreInviteSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildInvite:Lcom/discord/stores/StoreInviteSettings;

    return-object v0
.end method

.method public final getGuildMemberCounts$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildMemberCounts;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildMemberCounts:Lcom/discord/stores/StoreGuildMemberCounts;

    return-object v0
.end method

.method public final getGuildProfiles$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildProfiles;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildProfiles:Lcom/discord/stores/StoreGuildProfiles;

    return-object v0
.end method

.method public final getGuildSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSelected;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    return-object v0
.end method

.method public final getGuildSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserGuildSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    return-object v0
.end method

.method public final getGuildSubscriptions$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSubscriptions;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

    return-object v0
.end method

.method public final getGuildTemplates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildTemplates;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildTemplates:Lcom/discord/stores/StoreGuildTemplates;

    return-object v0
.end method

.method public final getGuildWelcomeScreens$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildWelcomeScreens;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildWelcomeScreens:Lcom/discord/stores/StoreGuildWelcomeScreens;

    return-object v0
.end method

.method public final getGuilds$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuilds;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guilds:Lcom/discord/stores/StoreGuilds;

    return-object v0
.end method

.method public final getGuildsNsfw$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildsNsfw;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildsNsfw:Lcom/discord/stores/StoreGuildsNsfw;

    return-object v0
.end method

.method public final getGuildsSorted$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildsSorted;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildsSorted:Lcom/discord/stores/StoreGuildsSorted;

    return-object v0
.end method

.method public final getInstantInvites$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreInstantInvites;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->instantInvites:Lcom/discord/stores/StoreInstantInvites;

    return-object v0
.end method

.method public final getIntegrations$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildIntegrations;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->integrations:Lcom/discord/stores/StoreGuildIntegrations;

    return-object v0
.end method

.method public final getLazyChannelMembersStore$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelMembers;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lazyChannelMembersStore:Lcom/discord/stores/StoreChannelMembers;

    return-object v0
.end method

.method public final getLibrary$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreLibrary;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->library:Lcom/discord/stores/StoreLibrary;

    return-object v0
.end method

.method public final getLurking$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreLurking;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lurking:Lcom/discord/stores/StoreLurking;

    return-object v0
.end method

.method public final getMaskedLinks$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMaskedLinks;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->maskedLinks:Lcom/discord/stores/StoreMaskedLinks;

    return-object v0
.end method

.method public final getMediaEngine$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaEngine;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mediaEngine:Lcom/discord/stores/StoreMediaEngine;

    return-object v0
.end method

.method public final getMediaSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMediaSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mediaSettings:Lcom/discord/stores/StoreMediaSettings;

    return-object v0
.end method

.method public final getMentions$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMentions;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mentions:Lcom/discord/stores/StoreMentions;

    return-object v0
.end method

.method public final getMessageAck$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageAck;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageAck:Lcom/discord/stores/StoreMessageAck;

    return-object v0
.end method

.method public final getMessageReactions$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageReactions;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReactions:Lcom/discord/stores/StoreMessageReactions;

    return-object v0
.end method

.method public final getMessageReplies$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageReplies;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageReplies:Lcom/discord/stores/StoreMessageReplies;

    return-object v0
.end method

.method public final getMessageStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageStates:Lcom/discord/stores/StoreMessageState;

    return-object v0
.end method

.method public final getMessageUploads$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessageUploads;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messageUploads:Lcom/discord/stores/StoreMessageUploads;

    return-object v0
.end method

.method public final getMessages$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessages;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messages:Lcom/discord/stores/StoreMessages;

    return-object v0
.end method

.method public final getMessagesLoader$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessagesLoader;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesLoader:Lcom/discord/stores/StoreMessagesLoader;

    return-object v0
.end method

.method public final getMessagesMostRecent$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessagesMostRecent;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->messagesMostRecent:Lcom/discord/stores/StoreMessagesMostRecent;

    return-object v0
.end method

.method public final getMfa$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMFA;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mfa:Lcom/discord/stores/StoreMFA;

    return-object v0
.end method

.method public final getNavigation$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNavigation;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->navigation:Lcom/discord/stores/StoreNavigation;

    return-object v0
.end method

.method public final getNotices$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNotices;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->notices:Lcom/discord/stores/StoreNotices;

    return-object v0
.end method

.method public final getNotifications$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNotifications;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->notifications:Lcom/discord/stores/StoreNotifications;

    return-object v0
.end method

.method public final getNux$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreNux;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->nux:Lcom/discord/stores/StoreNux;

    return-object v0
.end method

.method public final getPaymentSources$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePaymentSources;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->paymentSources:Lcom/discord/stores/StorePaymentSources;

    return-object v0
.end method

.method public final getPendingReplies$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePendingReplies;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->pendingReplies:Lcom/discord/stores/StorePendingReplies;

    return-object v0
.end method

.method public final getPermissions$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePermissions;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->permissions:Lcom/discord/stores/StorePermissions;

    return-object v0
.end method

.method public final getPinnedMessages$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePinnedMessages;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->pinnedMessages:Lcom/discord/stores/StorePinnedMessages;

    return-object v0
.end method

.method public final getPremiumGuildSubscriptions$app_productionDiscordExternalRelease()Lcom/discord/stores/StorePremiumGuildSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->premiumGuildSubscriptions:Lcom/discord/stores/StorePremiumGuildSubscription;

    return-object v0
.end method

.method public final getPresences$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserPresence;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->presences:Lcom/discord/stores/StoreUserPresence;

    return-object v0
.end method

.method public final getReadStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreReadStates;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->readStates:Lcom/discord/stores/StoreReadStates;

    return-object v0
.end method

.method public final getReviewRequestStore$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreReviewRequest;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->reviewRequestStore:Lcom/discord/stores/StoreReviewRequest;

    return-object v0
.end method

.method public final getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcConnection;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->rtcConnection:Lcom/discord/stores/StoreRtcConnection;

    return-object v0
.end method

.method public final getRtcRegion$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcRegion;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->rtcRegion:Lcom/discord/stores/StoreRtcRegion;

    return-object v0
.end method

.method public final getRunningGame$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRunningGame;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->runningGame:Lcom/discord/stores/StoreRunningGame;

    return-object v0
.end method

.method public final getSearch$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreSearch;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->search:Lcom/discord/stores/StoreSearch;

    return-object v0
.end method

.method public final getSlowMode$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreSlowMode;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->slowMode:Lcom/discord/stores/StoreSlowMode;

    return-object v0
.end method

.method public final getSpotify$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreSpotify;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->spotify:Lcom/discord/stores/StoreSpotify;

    return-object v0
.end method

.method public final getStickers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreStickers;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->stickers:Lcom/discord/stores/StoreStickers;

    return-object v0
.end method

.method public final getStoreChannelCategories$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelCategories;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->storeChannelCategories:Lcom/discord/stores/StoreChannelCategories;

    return-object v0
.end method

.method public final getStoreDynamicLink$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreDynamicLink;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->storeDynamicLink:Lcom/discord/stores/StoreDynamicLink;

    return-object v0
.end method

.method public final getSubscriptions$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreSubscriptions;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->subscriptions:Lcom/discord/stores/StoreSubscriptions;

    return-object v0
.end method

.method public final getTabsNavigation$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreTabsNavigation;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->tabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    return-object v0
.end method

.method public final getUserAffinities$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserAffinities;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userAffinities:Lcom/discord/stores/StoreUserAffinities;

    return-object v0
.end method

.method public final getUserConnections$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserConnections;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userConnections:Lcom/discord/stores/StoreUserConnections;

    return-object v0
.end method

.method public final getUserNotes$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserNotes;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userNotes:Lcom/discord/stores/StoreUserNotes;

    return-object v0
.end method

.method public final getUserProfile$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserProfile;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userProfile:Lcom/discord/stores/StoreUserProfile;

    return-object v0
.end method

.method public final getUserRelationships$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserRelationships;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRelationships:Lcom/discord/stores/StoreUserRelationships;

    return-object v0
.end method

.method public final getUserRequiredAction$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserRequiredActions;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRequiredAction:Lcom/discord/stores/StoreUserRequiredActions;

    return-object v0
.end method

.method public final getUserSettings$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    return-object v0
.end method

.method public final getUsers$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    return-object v0
.end method

.method public final getUsersMutualGuilds$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUsersMutualGuilds;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->usersMutualGuilds:Lcom/discord/stores/StoreUsersMutualGuilds;

    return-object v0
.end method

.method public final getUsersTyping$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserTyping;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->usersTyping:Lcom/discord/stores/StoreUserTyping;

    return-object v0
.end method

.method public final getVideoStreams$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVideoStreams;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->videoStreams:Lcom/discord/stores/StoreVideoStreams;

    return-object v0
.end method

.method public final getVideoSupport$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVideoSupport;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->videoSupport:Lcom/discord/stores/StoreVideoSupport;

    return-object v0
.end method

.method public final getVoiceChannelSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceChannelSelected;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    return-object v0
.end method

.method public final getVoiceParticipants$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceParticipants;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceParticipants:Lcom/discord/stores/StoreVoiceParticipants;

    return-object v0
.end method

.method public final getVoiceSpeaking$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceSpeaking;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceSpeaking:Lcom/discord/stores/StoreVoiceSpeaking;

    return-object v0
.end method

.method public final getVoiceStates$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceStates;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceStates:Lcom/discord/stores/StoreVoiceStates;

    return-object v0
.end method

.method public final handleAccessibilitySettingsUpdated(ZI)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->accessibility:Lcom/discord/stores/StoreAccessibility;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreAccessibility;->handleAccessibilitySettingsUpdated(ZI)V

    return-void
.end method

.method public final handleApplicationStreamUpdate(JLjava/lang/Integer;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->videoStreams:Lcom/discord/stores/StoreVideoStreams;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreVideoStreams;->handleApplicationStreamUpdate(JLjava/lang/Integer;)V

    return-void
.end method

.method public final handleGuildJoined(JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildWelcomeScreens:Lcom/discord/stores/StoreGuildWelcomeScreens;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreGuildWelcomeScreens;->handleGuildJoined(JLcom/discord/models/domain/ModelGuildWelcomeScreen;)V

    return-void
.end method

.method public final handleGuildSelected(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuildSelected;->handleGuildSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->calls:Lcom/discord/stores/StoreCalls;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreCalls;->handleGuildSelect(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSubscriptions:Lcom/discord/stores/StoreGuildSubscriptions;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuildSubscriptions;->handleGuildSelect(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->lurking:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreLurking;->handleGuildSelected$app_productionDiscordExternalRelease(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->nux:Lcom/discord/stores/StoreNux;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreNux;->handleGuildSelected(J)V

    return-void
.end method

.method public final handleHomeTabSelected(Lcom/discord/stores/StoreNavigation$PanelAction;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "panelAction"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->navigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreNavigation;->handleHomeTabSelected(Lcom/discord/stores/StoreNavigation$PanelAction;)V

    return-void
.end method

.method public final handleIsScreenSharingChanged(Z)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleIsScreenSharingChanged(Z)V

    return-void
.end method

.method public final handleLoginResult(Lcom/discord/models/domain/auth/ModelLoginResult;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "loginResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->authentication:Lcom/discord/stores/StoreAuthentication;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAuthentication;->handleLoginResult(Lcom/discord/models/domain/auth/ModelLoginResult;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserSettings;->handleLoginResult(Lcom/discord/models/domain/auth/ModelLoginResult;)V

    return-void
.end method

.method public final handleRelationshipAdd(Lcom/discord/models/domain/ModelUserRelationship;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "relationship"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleUserRelationshipAdd(Lcom/discord/models/domain/ModelUserRelationship;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userRelationships:Lcom/discord/stores/StoreUserRelationships;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserRelationships;->handleRelationshipAdd(Lcom/discord/models/domain/ModelUserRelationship;)V

    return-void
.end method

.method public final handleSamplePremiumGuildSelected(J)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->guildSelected:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuildSelected;->handleGuildSelected(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->nux:Lcom/discord/stores/StoreNux;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreNux;->handleSamplePremiumGuildSelected(J)V

    return-void
.end method

.method public final handleStreamDelete(Lcom/discord/models/domain/StreamDelete;Z)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamDelete"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStreamRtcConnection;->handleStreamDelete()V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamDelete(Lcom/discord/models/domain/StreamDelete;)V

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamDelete;->getStreamKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/discord/stores/StoreGatewayConnection;->streamDelete(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final handleStreamRtcConnectionStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->streamRtcConnection:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->handleStreamRtcConnectionStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAudioDevices;->handleStreamRtcConnectionStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V

    return-void
.end method

.method public final handleStreamTargeted(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamTargeted(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->voiceChannelSelected:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->handleStreamTargeted(Ljava/lang/String;)V

    return-void
.end method

.method public final handleUserConnections(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelConnectedAccount;",
            ">;)V"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->userConnections:Lcom/discord/stores/StoreUserConnections;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUserConnections;->handleUserConnections(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->spotify:Lcom/discord/stores/StoreSpotify;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreSpotify;->handleUserConnections(Ljava/util/List;)V

    return-void
.end method

.method public final handleUserStickerPackUpdate()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->stickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->handleUserStickerPackUpdate()V

    return-void
.end method

.method public final handleUserUpdated(Lcom/discord/models/domain/ModelUser;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "user"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->users:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreUser;->handleUserUpdated(Lcom/discord/models/domain/ModelUser;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->mfa:Lcom/discord/stores/StoreMFA;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMFA;->handleUserUpdated(Lcom/discord/models/domain/ModelUser;)V

    return-void
.end method

.method public final handleVideoInputDeviceSelected(Lco/discord/media_engine/VideoInputDeviceDescription;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->analytics:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->handleVideoInputDeviceSelected(Lco/discord/media_engine/VideoInputDeviceDescription;)V

    return-void
.end method

.method public final handleVideoStreamUpdate(JLjava/lang/Integer;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->videoStreams:Lcom/discord/stores/StoreVideoStreams;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreVideoStreams;->handleVideoStreamUpdate(JLjava/lang/Integer;)V

    return-void
.end method

.method public final handleVoiceStatesUpdated()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices;->handleVoiceStatesUpdated()V

    return-void
.end method

.method public onDispatchEnded()V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/discord/stores/StoreStream;->storesV2:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/StoreV2;

    invoke-virtual {v2}, Lcom/discord/stores/StoreV2;->getUpdateSources()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v2}, Lcom/discord/stores/StoreV2;->snapshotData()V

    invoke-virtual {v2}, Lcom/discord/stores/StoreV2;->onDispatchEnded()V

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/stores/updates/ObservationDeck;->notify(Ljava/util/Set;)V

    :cond_2
    return-void
.end method

.method public final streamCreate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGatewayConnection;->streamCreate(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {p2, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamCreateRequest(Ljava/lang/String;)V

    return-void
.end method

.method public final streamWatch(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->gatewaySocket:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGatewayConnection;->streamWatch(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStream;->applicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamWatch(Ljava/lang/String;)V

    return-void
.end method
