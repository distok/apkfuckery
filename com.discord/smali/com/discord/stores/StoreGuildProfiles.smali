.class public final Lcom/discord/stores/StoreGuildProfiles;
.super Lcom/discord/stores/StoreV2;
.source "StoreGuildProfiles.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGuildProfiles$FetchStates;,
        Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final guildProfilesState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;",
            ">;"
        }
    .end annotation
.end field

.field private guildProfilesStateSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;",
            ">;"
        }
    .end annotation
.end field

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildProfiles;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreGuildProfiles;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesState:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesStateSnapshot:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$fetchIfNonexisting(Lcom/discord/stores/StoreGuildProfiles;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildProfiles;->fetchIfNonexisting(J)V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreGuildProfiles;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildProfiles;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getGuildProfilesState$p(Lcom/discord/stores/StoreGuildProfiles;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesState:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getGuildProfilesStateSnapshot$p(Lcom/discord/stores/StoreGuildProfiles;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesStateSnapshot:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$handleGuildProfileFetchFailed(Lcom/discord/stores/StoreGuildProfiles;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildProfiles;->handleGuildProfileFetchFailed(J)V

    return-void
.end method

.method public static final synthetic access$handleGuildProfileFetchStart(Lcom/discord/stores/StoreGuildProfiles;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildProfiles;->handleGuildProfileFetchStart(J)V

    return-void
.end method

.method public static final synthetic access$handleGuildProfileFetchSuccess(Lcom/discord/stores/StoreGuildProfiles;Lcom/discord/models/domain/ModelGuildPreview;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGuildProfiles;->handleGuildProfileFetchSuccess(Lcom/discord/models/domain/ModelGuildPreview;)V

    return-void
.end method

.method public static final synthetic access$setGuildProfilesStateSnapshot$p(Lcom/discord/stores/StoreGuildProfiles;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesStateSnapshot:Ljava/util/Map;

    return-void
.end method

.method private final fetchGuildProfile(J)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;-><init>(Lcom/discord/stores/StoreGuildProfiles;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final fetchIfNonexisting(J)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getFetchState()Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreGuildProfiles$FetchStates;->FAILED:Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildProfiles;->fetchGuildProfile(J)V

    return-void
.end method

.method private final handleGuildProfileFetchFailed(J)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;

    sget-object v1, Lcom/discord/stores/StoreGuildProfiles$FetchStates;->FAILED:Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    const/4 v2, 0x0

    invoke-direct {p2, v1, v2}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;-><init>(Lcom/discord/stores/StoreGuildProfiles$FetchStates;Lcom/discord/models/domain/ModelGuildPreview;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleGuildProfileFetchStart(J)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;

    sget-object v1, Lcom/discord/stores/StoreGuildProfiles$FetchStates;->FETCHING:Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    const/4 v2, 0x0

    invoke-direct {p2, v1, v2}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;-><init>(Lcom/discord/stores/StoreGuildProfiles$FetchStates;Lcom/discord/models/domain/ModelGuildPreview;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleGuildProfileFetchSuccess(Lcom/discord/models/domain/ModelGuildPreview;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesState:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildPreview;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;

    sget-object v3, Lcom/discord/stores/StoreGuildProfiles$FetchStates;->SUCCEEDED:Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    invoke-direct {v2, v3, p1}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;-><init>(Lcom/discord/stores/StoreGuildProfiles$FetchStates;Lcom/discord/models/domain/ModelGuildPreview;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method


# virtual methods
.method public final observeGuildProfile(J)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildProfiles$observeGuildProfile$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreGuildProfiles$observeGuildProfile$1;-><init>(Lcom/discord/stores/StoreGuildProfiles;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v2, p0, Lcom/discord/stores/StoreGuildProfiles;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v0, 0x1

    new-array v3, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v0, 0x0

    aput-object p0, v3, v0

    new-instance v7, Lcom/discord/stores/StoreGuildProfiles$observeGuildProfile$2;

    invoke-direct {v7, p0, p1, p2}, Lcom/discord/stores/StoreGuildProfiles$observeGuildProfile$2;-><init>(Lcom/discord/stores/StoreGuildProfiles;J)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observationDeck\n        \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public snapshotData()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesState:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGuildProfiles;->guildProfilesStateSnapshot:Ljava/util/Map;

    return-void
.end method
