.class public final Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$2;
.super Lx/m/c/k;
.source "StoreEmoji.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreEmoji;->buildUsableEmojiSet(Ljava/util/Map;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/List;ZZZZ)Lcom/discord/models/domain/emoji/EmojiSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

.field public final synthetic $hasExternalEmojiPermission:Z


# direct methods
.method public constructor <init>(ZLcom/discord/stores/StoreEmoji$EmojiContext;)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$2;->$hasExternalEmojiPermission:Z

    iput-object p2, p0, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$2;->$emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$2;->invoke(Z)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Z)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$2;->$emojiContext:Lcom/discord/stores/StoreEmoji$EmojiContext;

    instance-of v0, v0, Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/discord/stores/StoreEmoji$buildUsableEmojiSet$2;->$hasExternalEmojiPermission:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
