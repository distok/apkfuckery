.class public final Lcom/discord/stores/StoreUserAffinities;
.super Lcom/discord/stores/Store;
.source "StoreUserAffinities.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# instance fields
.field private affinities:Lcom/discord/models/domain/ModelUserAffinities;

.field private final affinitiesSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/models/domain/ModelUserAffinities;",
            ">;"
        }
    .end annotation
.end field

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreUserAffinities;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p1, Lcom/discord/models/domain/ModelUserAffinities;

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-direct {p1, v0, v0}, Lcom/discord/models/domain/ModelUserAffinities;-><init>(Ljava/util/List;Ljava/util/List;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserAffinities;->affinities:Lcom/discord/models/domain/ModelUserAffinities;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    const-string v0, "BehaviorSubject.create(affinities)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserAffinities;->affinitiesSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreUserAffinities;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreUserAffinities;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$handleUserAffinitiesFetchSuccess(Lcom/discord/stores/StoreUserAffinities;Lcom/discord/models/domain/ModelUserAffinities;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserAffinities;->handleUserAffinitiesFetchSuccess(Lcom/discord/models/domain/ModelUserAffinities;)V

    return-void
.end method

.method private final fetchUserAffinities()V
    .locals 13
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getUserAffinities()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreUserAffinities;

    new-instance v10, Lcom/discord/stores/StoreUserAffinities$fetchUserAffinities$1;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreUserAffinities$fetchUserAffinities$1;-><init>(Lcom/discord/stores/StoreUserAffinities;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final handleUserAffinitiesFetchSuccess(Lcom/discord/models/domain/ModelUserAffinities;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreUserAffinities;->affinities:Lcom/discord/models/domain/ModelUserAffinities;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreUserAffinities;->isDirty:Z

    return-void
.end method


# virtual methods
.method public final get()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserAffinities;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserAffinities;->affinitiesSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "affinitiesSubject\n      \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getAffinityUserIds()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserAffinities;->get()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreUserAffinities$getAffinityUserIds$1;->INSTANCE:Lcom/discord/stores/StoreUserAffinities$getAffinityUserIds$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "get()\n          .map { a\u2026ty -> affinity.userId } }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "get()\n          .map { a\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final handleConnectionOpen()V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/stores/StoreUserAffinities;->fetchUserAffinities()V

    return-void
.end method

.method public onDispatchEnded()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreUserAffinities;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreUserAffinities;->affinitiesSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreUserAffinities;->affinities:Lcom/discord/models/domain/ModelUserAffinities;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreUserAffinities;->isDirty:Z

    :cond_0
    return-void
.end method
