.class public final Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;
.super Lx/m/c/k;
.source "StoreGatewayConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGatewayConnection;->streamCreate(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/gateway/GatewaySocket;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $newStream:Lcom/discord/models/domain/ModelApplicationStream;

.field public final synthetic $preferredRegion:Ljava/lang/String;

.field public final synthetic $streamKey:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreGatewayConnection;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGatewayConnection;Ljava/lang/String;Lcom/discord/models/domain/ModelApplicationStream;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->this$0:Lcom/discord/stores/StoreGatewayConnection;

    iput-object p2, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$streamKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$newStream:Lcom/discord/models/domain/ModelApplicationStream;

    iput-object p4, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$preferredRegion:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/gateway/GatewaySocket;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->invoke(Lcom/discord/gateway/GatewaySocket;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/gateway/GatewaySocket;)V
    .locals 8

    const-string v0, "gatewaySocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->this$0:Lcom/discord/stores/StoreGatewayConnection;

    invoke-static {v0}, Lcom/discord/stores/StoreGatewayConnection;->access$getStream$p(Lcom/discord/stores/StoreGatewayConnection;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getApplicationStreaming$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming;->getActiveApplicationStream$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$streamKey:Ljava/lang/String;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->this$0:Lcom/discord/stores/StoreGatewayConnection;

    invoke-virtual {v2, v0}, Lcom/discord/stores/StoreGatewayConnection;->streamDelete(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$newStream:Lcom/discord/models/domain/ModelApplicationStream;

    instance-of v2, v0, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;->getGuildId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :cond_2
    move-object v6, v1

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$newStream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getType()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$newStream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v4

    iget-object v7, p0, Lcom/discord/stores/StoreGatewayConnection$streamCreate$1;->$preferredRegion:Ljava/lang/String;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/discord/gateway/GatewaySocket;->streamCreate(Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;)V

    return-void
.end method
