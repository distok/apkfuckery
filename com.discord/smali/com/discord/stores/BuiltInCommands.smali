.class public final Lcom/discord/stores/BuiltInCommands;
.super Ljava/lang/Object;
.source "StoreApplicationCommands.kt"

# interfaces
.implements Lcom/discord/stores/BuiltInCommandsProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/BuiltInCommands$Companion;
    }
.end annotation


# static fields
.field public static final BUILT_IN_APP_ID:J = -0x1L

.field public static final Companion:Lcom/discord/stores/BuiltInCommands$Companion;


# instance fields
.field private final builtInApplication:Lcom/discord/models/slashcommands/ModelApplication;

.field private final builtInCommands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/BuiltInCommands$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/BuiltInCommands$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/BuiltInCommands;->Companion:Lcom/discord/stores/BuiltInCommands$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/stores/ModelApplicationCommand;

    const v1, 0x7f1204bf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-wide/16 v3, -0x1

    const-string v5, "shrug"

    const-string/jumbo v7, "\u00af\\\\_(\u30c4)\\_/\u00af"

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/discord/stores/BuiltInCommands;->createAppendToEndSlashCommand(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f1204c3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-wide/16 v3, -0x2

    const-string/jumbo v5, "tableflip"

    const-string v7, "(\u256f\u00b0\u25a1\u00b0\uff09\u256f\ufe35 \u253b\u2501\u253b"

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/discord/stores/BuiltInCommands;->createAppendToEndSlashCommand(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const v1, 0x7f1204c5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-wide/16 v3, -0x3

    const-string/jumbo v5, "unflip"

    const-string/jumbo v7, "\u252c\u2500\u252c \u30ce( \u309c-\u309c\u30ce)"

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/discord/stores/BuiltInCommands;->createAppendToEndSlashCommand(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/stores/ModelApplicationCommand;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/BuiltInCommands;->builtInCommands:Ljava/util/List;

    new-instance v8, Lcom/discord/models/slashcommands/ModelApplication;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    const-wide/16 v2, -0x1

    const-string v4, "Built-In"

    const/4 v5, 0x0

    const/4 v7, 0x1

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/discord/models/slashcommands/ModelApplication;-><init>(JLjava/lang/String;Ljava/lang/String;IZ)V

    iput-object v8, p0, Lcom/discord/stores/BuiltInCommands;->builtInApplication:Lcom/discord/models/slashcommands/ModelApplication;

    return-void
.end method

.method private final createAppendToEndSlashCommand(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/stores/ModelApplicationCommand;
    .locals 13

    new-instance v11, Lcom/discord/stores/ModelApplicationCommand;

    new-instance v12, Lcom/discord/stores/ModelApplicationCommandOption;

    sget-object v1, Lcom/discord/models/slashcommands/ApplicationCommandType;->STRING:Lcom/discord/models/slashcommands/ApplicationCommandType;

    const-string v2, "message"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xec

    const/4 v10, 0x0

    move-object v0, v12

    invoke-direct/range {v0 .. v10}, Lcom/discord/stores/ModelApplicationCommandOption;-><init>(Lcom/discord/models/slashcommands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ZLjava/lang/String;Ljava/util/List;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v12}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    new-instance v10, Lcom/discord/stores/BuiltInCommands$createAppendToEndSlashCommand$1;

    move-object/from16 v0, p5

    invoke-direct {v10, v0}, Lcom/discord/stores/BuiltInCommands$createAppendToEndSlashCommand$1;-><init>(Ljava/lang/String;)V

    const-wide/16 v3, -0x1

    const-string v6, ""

    const/4 v9, 0x1

    move-object v0, v11

    move-wide v1, p1

    move-object/from16 v5, p3

    move-object/from16 v7, p4

    invoke-direct/range {v0 .. v10}, Lcom/discord/stores/ModelApplicationCommand;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;ZLkotlin/jvm/functions/Function1;)V

    return-object v11
.end method


# virtual methods
.method public getBuiltInApplication()Lcom/discord/models/slashcommands/ModelApplication;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/BuiltInCommands;->builtInApplication:Lcom/discord/models/slashcommands/ModelApplication;

    return-object v0
.end method

.method public getBuiltInCommands()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/BuiltInCommands;->builtInCommands:Ljava/util/List;

    return-object v0
.end method
