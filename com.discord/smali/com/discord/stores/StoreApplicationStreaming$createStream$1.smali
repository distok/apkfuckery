.class public final Lcom/discord/stores/StoreApplicationStreaming$createStream$1;
.super Lx/m/c/k;
.source "StoreApplicationStreaming.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreApplicationStreaming;->createStream(JLjava/lang/Long;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $guildId:Ljava/lang/Long;

.field public final synthetic $preferredRegion:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreApplicationStreaming;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreApplicationStreaming;JLjava/lang/Long;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    iput-wide p2, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$channelId:J

    iput-object p4, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$guildId:Ljava/lang/Long;

    iput-object p5, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$preferredRegion:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 12

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationStreaming;->access$getRtcConnectionStore$p(Lcom/discord/stores/StoreApplicationStreaming;)Lcom/discord/stores/StoreRtcConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreRtcConnection;->getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/rtcconnection/RtcConnection;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_8

    iget-wide v4, v0, Lcom/discord/rtcconnection/RtcConnection;->x:J

    iget-wide v6, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$channelId:J

    cmp-long v8, v4, v6

    if-nez v8, :cond_8

    iget-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    iget-object v5, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$guildId:Ljava/lang/Long;

    invoke-static {v4, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v3

    if-eqz v4, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationStreaming;->access$getUserStore$p(Lcom/discord/stores/StoreApplicationStreaming;)Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v9

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$guildId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    iget-object v4, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$guildId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$channelId:J

    move-object v4, v0

    invoke-direct/range {v4 .. v10}, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;-><init>(JJJ)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/discord/models/domain/ModelApplicationStream$CallStream;

    iget-wide v4, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$channelId:J

    invoke-direct {v0, v4, v5, v9, v10}, Lcom/discord/models/domain/ModelApplicationStream$CallStream;-><init>(JJ)V

    :goto_0
    iget-object v4, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreaming;->getActiveApplicationStream$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v5

    if-ne v1, v5, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->isStreamActive()Z

    move-result v1

    if-ne v1, v3, :cond_4

    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_5

    return-void

    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->isStreamActive()Z

    move-result v1

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/discord/stores/StoreApplicationStreaming;->access$stopStreamInternal(Lcom/discord/stores/StoreApplicationStreaming;Ljava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->this$0:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-static {v1}, Lcom/discord/stores/StoreApplicationStreaming;->access$getStoreStream$p(Lcom/discord/stores/StoreApplicationStreaming;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$preferredRegion:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/discord/stores/StoreStream;->streamCreate(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    return-void

    :cond_8
    :goto_2
    sget-object v4, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "\n                Failed to start stream.\n                rtcConnection == null: "

    invoke-static {v6}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v0, :cond_9

    const/4 v7, 0x1

    goto :goto_3

    :cond_9
    const/4 v7, 0x0

    :goto_3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v7, "\n                rtcConnection.channelId != channelId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_a

    iget-wide v7, v0, Lcom/discord/rtcconnection/RtcConnection;->x:J

    iget-wide v9, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$channelId:J

    cmp-long v11, v7, v9

    if-eqz v11, :cond_b

    :cond_a
    const/4 v2, 0x1

    :cond_b
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, "\n                rtcConnection.guildId != guildId: "

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_c

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->w:Ljava/lang/Long;

    :cond_c
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;->$guildId:Ljava/lang/Long;

    invoke-static {v1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, "\n              "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lx/s/i;->trimIndent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    const-string v0, "Failed to start stream."

    move-object v3, v4

    move-object v4, v0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    return-void
.end method
