.class public final Lcom/discord/stores/StoreGooglePlaySkuDetails;
.super Ljava/lang/Object;
.source "StoreGooglePlaySkuDetails.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGooglePlaySkuDetails$State;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private skuDetails:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;"
        }
    .end annotation
.end field

.field private state:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->skuDetails:Ljava/util/HashMap;

    sget-object p1, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Uninitialized;->INSTANCE:Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Uninitialized;

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->state:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->stateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method


# virtual methods
.method public final getState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->stateSubject:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v1, "this.stateSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final handleError()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlaySkuDetails$handleError$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGooglePlaySkuDetails$handleError$1;-><init>(Lcom/discord/stores/StoreGooglePlaySkuDetails;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final handleFetchError()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Failure;->INSTANCE:Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Failure;

    iput-object v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->state:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->isDirty:Z

    return-void
.end method

.method public final handleFetchSuccess(Ljava/util/List;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newSkuDetails"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->skuDetails:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    const/16 v1, 0x10

    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v3}, Lcom/android/billingclient/api/SkuDetails;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->skuDetails:Ljava/util/HashMap;

    sget-object p1, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayInAppSkus;

    invoke-virtual {p1, v0}, Lcom/discord/utilities/billing/GooglePlayInAppSkus;->populateSkuDetails(Ljava/util/HashMap;)V

    new-instance p1, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;

    invoke-direct {p1, v0}, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->state:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->isDirty:Z

    return-void
.end method

.method public onDispatchEnded()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->stateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->state:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final updateSkuDetails(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)V"
        }
    .end annotation

    const-string v0, "skuDetails"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlaySkuDetails;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlaySkuDetails$updateSkuDetails$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGooglePlaySkuDetails$updateSkuDetails$1;-><init>(Lcom/discord/stores/StoreGooglePlaySkuDetails;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
