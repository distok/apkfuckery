.class public final Lcom/discord/stores/StoreUserRelationships;
.super Ljava/lang/Object;
.source "StoreUserRelationships.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;
    }
.end annotation


# instance fields
.field private isDirty:Z

.field private final relationshipsCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

.field private final userRelationshipsSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;->INSTANCE:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;

    iput-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    new-instance v1, Lcom/discord/utilities/persister/Persister;

    invoke-static {}, Lcom/discord/stores/StoreUserRelationshipsKt;->access$getUNLOADED_RELATIONSHIPS_SENTINEL$p()Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "STORE_USER_RELATIONSHIPS_V9"

    invoke-direct {v1, v3, v2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsCache:Lcom/discord/utilities/persister/Persister;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->userRelationshipsSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method private final ensureRelationshipLoaded()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    instance-of v1, v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    sget-object v1, Lx/h/m;->d:Lx/h/m;

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;-><init>(Ljava/util/Map;)V

    :goto_0
    iput-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    return-object v0
.end method


# virtual methods
.method public final getRelationships()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    instance-of v1, v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;->getRelationships()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    return-object v0
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getRelationships()Ljava/util/List;

    move-result-object p1

    const-string v0, "payload\n        .relationships"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v0

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    const/16 v0, 0x10

    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelUserRelationship;

    const-string v2, "it"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUserRelationship;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUserRelationship;->getType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    invoke-direct {p1, v1}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreUserRelationships;->isDirty:Z

    return-void
.end method

.method public final handlePreLogout()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;->INSTANCE:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;

    iput-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreUserRelationships;->isDirty:Z

    return-void
.end method

.method public final handleRelationshipAdd(Lcom/discord/models/domain/ModelUserRelationship;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "relationship"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreUserRelationships;->ensureRelationshipLoaded()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;->getRelationships()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserRelationship;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v2, v0

    check-cast v2, Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserRelationship;->getType()I

    move-result v2

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v2, :cond_1

    :goto_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserRelationship;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserRelationship;->getType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    invoke-direct {p1, v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreUserRelationships;->isDirty:Z

    :cond_1
    return-void
.end method

.method public final handleRelationshipRemove(Lcom/discord/models/domain/ModelUserRelationship;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "relationship"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreUserRelationships;->ensureRelationshipLoaded()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;->getRelationships()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserRelationship;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    new-instance p1, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    invoke-direct {p1, v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    iput-boolean v1, p0, Lcom/discord/stores/StoreUserRelationships;->isDirty:Z

    :cond_1
    return-void
.end method

.method public final init()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {}, Lcom/discord/stores/StoreUserRelationshipsKt;->access$getUNLOADED_RELATIONSHIPS_SENTINEL$p()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    new-instance v1, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    invoke-direct {v1, v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    iput-boolean v2, p0, Lcom/discord/stores/StoreUserRelationships;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final observe()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->userRelationshipsSubject:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v1, "userRelationshipsSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreUserRelationships$observe$1;->INSTANCE:Lcom/discord/stores/StoreUserRelationships$observe$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "userRelationshipsSubject\u2026            }\n          }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observe(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserRelationships;->observe()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreUserRelationships$observe$3;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreUserRelationships$observe$3;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "observe()\n          .map\u2026> relationships[userId] }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observe(Ljava/util/Collection;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "userIds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserRelationships;->observe()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreUserRelationships$observe$2;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreUserRelationships$observe$2;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "observe()\n          .map\u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeForType(I)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserRelationships;->observe()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreUserRelationships$observeForType$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreUserRelationships$observeForType$1;-><init>(I)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "observe()\n          .map\u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final observeUserRelationshipsState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->userRelationshipsSubject:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v1, "userRelationshipsSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDispatchEnded()V
    .locals 5

    iget-boolean v0, p0, Lcom/discord/stores/StoreUserRelationships;->isDirty:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsState:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;

    instance-of v2, v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    new-instance v2, Ljava/util/HashMap;

    check-cast v0, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;->getRelationships()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsCache:Lcom/discord/utilities/persister/Persister;

    const/4 v4, 0x2

    invoke-static {v0, v2, v1, v4, v3}, Lcom/discord/utilities/persister/Persister;->set$default(Lcom/discord/utilities/persister/Persister;Ljava/lang/Object;ZILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->userRelationshipsSubject:Lrx/subjects/BehaviorSubject;

    new-instance v3, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;

    invoke-direct {v3, v2}, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v3}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->relationshipsCache:Lcom/discord/utilities/persister/Persister;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/persister/Persister;->clear$default(Lcom/discord/utilities/persister/Persister;ZILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreUserRelationships;->userRelationshipsSubject:Lrx/subjects/BehaviorSubject;

    sget-object v2, Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;->INSTANCE:Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;

    invoke-virtual {v0, v2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/discord/stores/StoreUserRelationships;->isDirty:Z

    return-void
.end method
