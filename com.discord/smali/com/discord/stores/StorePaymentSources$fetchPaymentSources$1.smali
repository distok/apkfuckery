.class public final Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;
.super Lx/m/c/k;
.source "StorePaymentSources.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePaymentSources;->fetchPaymentSources()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StorePaymentSources;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePaymentSources;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;->this$0:Lcom/discord/stores/StorePaymentSources;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;->this$0:Lcom/discord/stores/StorePaymentSources;

    invoke-static {v0}, Lcom/discord/stores/StorePaymentSources;->access$getPaymentSourcesState$p(Lcom/discord/stores/StorePaymentSources;)Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Loading;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;->this$0:Lcom/discord/stores/StorePaymentSources;

    invoke-static {v0}, Lcom/discord/stores/StorePaymentSources;->access$handlePaymentSourcesFetchStart(Lcom/discord/stores/StorePaymentSources;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getPaymentSources()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1$1;-><init>(Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    const-string v0, "RestAPI\n          .api\n \u2026efaultPaymentSource(it) }"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;->this$0:Lcom/discord/stores/StorePaymentSources;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v8, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1$2;-><init>(Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;)V

    const/4 v7, 0x0

    new-instance v6, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1$3;

    invoke-direct {v6, p0}, Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1$3;-><init>(Lcom/discord/stores/StorePaymentSources$fetchPaymentSources$1;)V

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
