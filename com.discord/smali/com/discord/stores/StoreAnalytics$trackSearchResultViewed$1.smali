.class public final Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;
.super Lx/m/c/k;
.source "StoreAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAnalytics;->trackSearchResultViewed(Lcom/discord/utilities/analytics/SearchType;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $searchType:Lcom/discord/utilities/analytics/SearchType;

.field public final synthetic $totalResultsCount:I

.field public final synthetic this$0:Lcom/discord/stores/StoreAnalytics;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/utilities/analytics/SearchType;I)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    iput-object p2, p0, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;->$searchType:Lcom/discord/utilities/analytics/SearchType;

    iput p3, p0, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;->$totalResultsCount:I

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v0}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChannelsSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelsSelected;->getId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreAnalytics;->access$getSnapshotProperties(Lcom/discord/stores/StoreAnalytics;J)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;->$searchType:Lcom/discord/utilities/analytics/SearchType;

    iget v3, p0, Lcom/discord/stores/StoreAnalytics$trackSearchResultViewed$1;->$totalResultsCount:I

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->searchResultViewed(Lcom/discord/utilities/analytics/SearchType;ILjava/util/Map;)V

    return-void
.end method
