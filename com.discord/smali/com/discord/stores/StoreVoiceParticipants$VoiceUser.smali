.class public final Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;
.super Ljava/lang/Object;
.source "StoreVoiceParticipants.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreVoiceParticipants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceUser"
.end annotation


# instance fields
.field private final _isSpeaking:Z

.field private final applicationStream:Lcom/discord/models/domain/ModelApplicationStream;

.field private final applicationStreamId:Ljava/lang/Integer;

.field private final callStreamId:Ljava/lang/Integer;

.field private final isDeafened:Z

.field private final isMe:Z

.field private final isMuted:Z

.field private final isRinging:Z

.field private final isSelfDeafened:Z

.field private final isSelfMuted:Z

.field private final isServerDeafened:Z

.field private final isServerMuted:Z

.field private final isSpeaking:Z

.field private final nickname:Ljava/lang/String;

.field private final streamContext:Lcom/discord/utilities/streams/StreamContext;

.field private final streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

.field private final user:Lcom/discord/models/domain/ModelUser;

.field private final voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

.field private final voiceState:Lcom/discord/models/domain/ModelVoice$State;

.field private final watchingStream:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelVoice$State;ZLcom/discord/stores/StoreVideoStreams$UserStreams;ZLjava/lang/String;Ljava/lang/String;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Z)V
    .locals 1

    const-string/jumbo v0, "user"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    iput-object p2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    iput-boolean p3, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    iput-object p4, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    iput-boolean p5, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    iput-object p6, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    iput-object p8, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    iput-object p9, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iput-boolean p10, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->_isSpeaking:Z

    const/4 p1, 0x1

    const/4 p3, 0x0

    if-nez p2, :cond_1

    :cond_0
    const/4 p6, 0x0

    goto :goto_0

    :cond_1
    if-eqz p10, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoice$State;->isMute()Z

    move-result p6

    if-nez p6, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoice$State;->isDeaf()Z

    move-result p6

    if-nez p6, :cond_0

    const/4 p6, 0x1

    :goto_0
    iput-boolean p6, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isSpeaking:Z

    const/4 p6, 0x0

    if-eqz p4, :cond_2

    invoke-virtual {p4}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->getCallStreamId()Ljava/lang/Integer;

    move-result-object p7

    goto :goto_1

    :cond_2
    move-object p7, p6

    :goto_1
    iput-object p7, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->callStreamId:Ljava/lang/Integer;

    if-eqz p4, :cond_3

    invoke-virtual {p4}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->getApplicationStreamId()Ljava/lang/Integer;

    move-result-object p4

    goto :goto_2

    :cond_3
    move-object p4, p6

    :goto_2
    iput-object p4, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->applicationStreamId:Ljava/lang/Integer;

    if-eqz p8, :cond_4

    invoke-virtual {p8}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p6

    :cond_4
    iput-object p6, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->applicationStream:Lcom/discord/models/domain/ModelApplicationStream;

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoice$State;->isMute()Z

    move-result p4

    goto :goto_3

    :cond_5
    const/4 p4, 0x0

    :goto_3
    iput-boolean p4, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isServerMuted:Z

    if-eqz p5, :cond_6

    if-eqz p9, :cond_7

    invoke-virtual {p9}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted()Z

    move-result p6

    goto :goto_4

    :cond_6
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoice$State;->isSelfMute()Z

    move-result p6

    goto :goto_4

    :cond_7
    const/4 p6, 0x0

    :goto_4
    iput-boolean p6, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isSelfMuted:Z

    if-nez p4, :cond_9

    if-eqz p6, :cond_8

    goto :goto_5

    :cond_8
    const/4 p4, 0x0

    goto :goto_6

    :cond_9
    :goto_5
    const/4 p4, 0x1

    :goto_6
    iput-boolean p4, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMuted:Z

    if-eqz p2, :cond_a

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoice$State;->isDeaf()Z

    move-result p4

    goto :goto_7

    :cond_a
    const/4 p4, 0x0

    :goto_7
    iput-boolean p4, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isServerDeafened:Z

    if-eqz p5, :cond_b

    if-eqz p9, :cond_c

    invoke-virtual {p9}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened()Z

    move-result p2

    goto :goto_8

    :cond_b
    if-eqz p2, :cond_c

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoice$State;->isSelfDeaf()Z

    move-result p2

    goto :goto_8

    :cond_c
    const/4 p2, 0x0

    :goto_8
    iput-boolean p2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isSelfDeafened:Z

    if-nez p4, :cond_e

    if-eqz p2, :cond_d

    goto :goto_9

    :cond_d
    const/4 p1, 0x0

    :cond_e
    :goto_9
    iput-boolean p1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isDeafened:Z

    return-void
.end method

.method private final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->_isSpeaking:Z

    return v0
.end method

.method private final component9()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelVoice$State;ZLcom/discord/stores/StoreVideoStreams$UserStreams;ZLjava/lang/String;Ljava/lang/String;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;ZILjava/lang/Object;)Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-boolean v1, v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->_isSpeaking:Z

    goto :goto_9

    :cond_9
    move/from16 v1, p10

    :goto_9
    move-object p1, v2

    move-object p2, v3

    move p3, v4

    move-object p4, v5

    move/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->copy(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelVoice$State;ZLcom/discord/stores/StoreVideoStreams$UserStreams;ZLjava/lang/String;Ljava/lang/String;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Z)Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelVoice$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    return v0
.end method

.method public final component4()Lcom/discord/stores/StoreVideoStreams$UserStreams;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    return v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Lcom/discord/utilities/streams/StreamContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelVoice$State;ZLcom/discord/stores/StoreVideoStreams$UserStreams;ZLjava/lang/String;Ljava/lang/String;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Z)Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;
    .locals 12

    const-string/jumbo v0, "user"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    move-object v1, v0

    move-object v3, p2

    move v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelVoice$State;ZLcom/discord/stores/StoreVideoStreams$UserStreams;ZLjava/lang/String;Ljava/lang/String;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    iget-object v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    iget-object v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    iget-object v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iget-object v1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->_isSpeaking:Z

    iget-boolean p1, p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->_isSpeaking:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplicationStream()Lcom/discord/models/domain/ModelApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->applicationStream:Lcom/discord/models/domain/ModelApplicationStream;

    return-object v0
.end method

.method public final getApplicationStreamId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->applicationStreamId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getCallStreamId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->callStreamId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "user.username"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public final getNickname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    return-object v0
.end method

.method public final getStreamContext()Lcom/discord/utilities/streams/StreamContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    return-object v0
.end method

.method public final getStreams()Lcom/discord/stores/StoreVideoStreams$UserStreams;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    return-object v0
.end method

.method public final getUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getVoiceState()Lcom/discord/models/domain/ModelVoice$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    return-object v0
.end method

.method public final getWatchingStream()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelVoice$State;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/discord/utilities/streams/StreamContext;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->_isSpeaking:Z

    if-eqz v1, :cond_9

    goto :goto_6

    :cond_9
    move v3, v1

    :goto_6
    add-int/2addr v0, v3

    return v0
.end method

.method public final isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isDeafened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isDeafened:Z

    return v0
.end method

.method public final isMe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    return v0
.end method

.method public final isMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMuted:Z

    return v0
.end method

.method public final isRinging()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    return v0
.end method

.method public final isSpeaking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isSpeaking:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "VoiceUser(user="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voiceState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceState:Lcom/discord/models/domain/ModelVoice$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isRinging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", streams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streams:Lcom/discord/stores/StoreVideoStreams$UserStreams;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isMe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", nickname="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->nickname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", watchingStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->watchingStream:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", streamContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voiceConfiguration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->voiceConfiguration:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", _isSpeaking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->_isSpeaking:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
