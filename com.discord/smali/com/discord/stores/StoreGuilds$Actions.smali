.class public final Lcom/discord/stores/StoreGuilds$Actions;
.super Ljava/lang/Object;
.source "StoreGuilds.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreGuilds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Actions"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreGuilds$Actions;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreGuilds$Actions;

    invoke-direct {v0}, Lcom/discord/stores/StoreGuilds$Actions;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreGuilds$Actions;->INSTANCE:Lcom/discord/stores/StoreGuilds$Actions;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final requestMembers(Lcom/discord/app/AppComponent;Lrx/Observable;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppComponent;",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "fragment"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "partialUserNameTokenEmitted"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v1, v2, v0}, Lrx/Observable;->o(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/StoreGuilds$Actions$requestMembers$1;

    invoke-direct {v0, p2}, Lcom/discord/stores/StoreGuilds$Actions$requestMembers$1;-><init>(Z)V

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreGuilds$Actions$requestMembers$2;->INSTANCE:Lcom/discord/stores/StoreGuilds$Actions$requestMembers$2;

    invoke-virtual {p1, p2}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreGuilds$Actions$requestMembers$3;->INSTANCE:Lcom/discord/stores/StoreGuilds$Actions$requestMembers$3;

    invoke-virtual {p1, p2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "partialUserNameTokenEmit\u2026            }\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x2

    invoke-static {p1, p0, p2, v0, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    sget-object v5, Lcom/discord/stores/StoreGuilds$Actions$requestMembers$4;->INSTANCE:Lcom/discord/stores/StoreGuilds$Actions$requestMembers$4;

    const/4 v2, 0x0

    const-string v3, "requestGuildMembers"

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x35

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method
