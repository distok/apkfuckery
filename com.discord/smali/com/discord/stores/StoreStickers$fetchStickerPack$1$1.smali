.class public final Lcom/discord/stores/StoreStickers$fetchStickerPack$1$1;
.super Lx/m/c/k;
.source "StoreStickers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStickers$fetchStickerPack$1;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/sticker/dto/ModelStickerPack;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreStickers$fetchStickerPack$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers$fetchStickerPack$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers$fetchStickerPack$1$1;->this$0:Lcom/discord/stores/StoreStickers$fetchStickerPack$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreStickers$fetchStickerPack$1$1;->invoke(Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/sticker/dto/ModelStickerPack;)V
    .locals 2

    const-string v0, "newPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchStickerPack$1$1;->this$0:Lcom/discord/stores/StoreStickers$fetchStickerPack$1;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchStickerPack$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v0}, Lcom/discord/stores/StoreStickers;->access$getDispatcher$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreStickers$fetchStickerPack$1$1$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreStickers$fetchStickerPack$1$1$1;-><init>(Lcom/discord/stores/StoreStickers$fetchStickerPack$1$1;Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
