.class public final Lcom/discord/stores/StoreClientDataState;
.super Lcom/discord/stores/StoreV2;
.source "StoreClientDataState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreClientDataState$ClientDataState;
    }
.end annotation


# instance fields
.field private clientDataStateSnapshot:Lcom/discord/stores/StoreClientDataState$ClientDataState;

.field private guildHashes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildHash;",
            ">;"
        }
    .end annotation
.end field

.field private highestLastMessageId:J

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private readStateVersion:I

.field private userGuildSettingsVersion:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/discord/stores/StoreClientDataState;-><init>(Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 8

    const-string v0, "observationDeck"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreClientDataState;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    const/4 p1, -0x1

    iput p1, p0, Lcom/discord/stores/StoreClientDataState;->readStateVersion:I

    iput p1, p0, Lcom/discord/stores/StoreClientDataState;->userGuildSettingsVersion:I

    new-instance p1, Lcom/discord/stores/StoreClientDataState$ClientDataState;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v7}, Lcom/discord/stores/StoreClientDataState$ClientDataState;-><init>(Ljava/util/Map;JIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreClientDataState;->clientDataStateSnapshot:Lcom/discord/stores/StoreClientDataState$ClientDataState;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/stores/StoreClientDataState;-><init>(Lcom/discord/stores/updates/ObservationDeck;)V

    return-void
.end method

.method public static final synthetic access$getClientDataStateSnapshot$p(Lcom/discord/stores/StoreClientDataState;)Lcom/discord/stores/StoreClientDataState$ClientDataState;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreClientDataState;->clientDataStateSnapshot:Lcom/discord/stores/StoreClientDataState$ClientDataState;

    return-object p0
.end method

.method public static final synthetic access$setClientDataStateSnapshot$p(Lcom/discord/stores/StoreClientDataState;Lcom/discord/stores/StoreClientDataState$ClientDataState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreClientDataState;->clientDataStateSnapshot:Lcom/discord/stores/StoreClientDataState$ClientDataState;

    return-void
.end method

.method private final updateGuildHash(Lcom/discord/models/domain/ModelGuild;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isUnavailable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object p1

    invoke-static {p1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v2, "guild.guildHashes!!"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method private final updateHighestLastMessageId(J)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-wide v0, p0, Lcom/discord/stores/StoreClientDataState;->highestLastMessageId:J

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iput-wide p1, p0, Lcom/discord/stores/StoreClientDataState;->highestLastMessageId:J

    :cond_0
    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/discord/stores/StoreClientDataState;->highestLastMessageId:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/discord/stores/StoreClientDataState;->readStateVersion:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/discord/stores/StoreClientDataState;->userGuildSettingsVersion:I

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleChannelCreateOrDelete(Lcom/discord/models/domain/ModelChannel;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "channel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object p1

    invoke-static {p1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v2, "channel.guildHashes!!"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreClientDataState;->clear()V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuilds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "channel"

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuild;

    const-string v3, "guild"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/discord/stores/StoreClientDataState;->updateGuildHash(Lcom/discord/models/domain/ModelGuild;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getChannels()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelChannel;

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getLastMessageId()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lcom/discord/stores/StoreClientDataState;->updateHighestLastMessageId(J)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getPrivateChannels()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelChannel;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getLastMessageId()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lcom/discord/stores/StoreClientDataState;->updateHighestLastMessageId(J)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getReadState()Lcom/discord/models/domain/ModelPayload$VersionedReadStates;

    move-result-object v0

    const-string v1, "payload.readState"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPayload$VersionedModel;->getVersion()I

    move-result v0

    iput v0, p0, Lcom/discord/stores/StoreClientDataState;->readStateVersion:I

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getUserGuildSettings()Lcom/discord/models/domain/ModelPayload$VersionedUserGuildSettings;

    move-result-object p1

    const-string v0, "payload.userGuildSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload$VersionedModel;->getVersion()I

    move-result p1

    iput p1, p0, Lcom/discord/stores/StoreClientDataState;->userGuildSettingsVersion:I

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleEmojiUpdate(Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;->getGuildId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/emoji/ModelEmojiCustom$Update;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object p1

    invoke-static {p1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final handleGuildAdd(Lcom/discord/models/domain/ModelGuild;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreClientDataState;->updateGuildHash(Lcom/discord/models/domain/ModelGuild;)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleGuildRemove(Lcom/discord/models/domain/ModelGuild;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleGuildSettingUpdated(Lcom/discord/models/domain/ModelNotificationSettings;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "userGuildSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->getVersion()I

    move-result v0

    iget v1, p0, Lcom/discord/stores/StoreClientDataState;->userGuildSettingsVersion:I

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->getVersion()I

    move-result p1

    iput p1, p0, Lcom/discord/stores/StoreClientDataState;->userGuildSettingsVersion:I

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final handleMessageAck(Lcom/discord/models/domain/ModelReadState;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "readState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelReadState;->getVersion()I

    move-result v0

    iget v1, p0, Lcom/discord/stores/StoreClientDataState;->readStateVersion:I

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelReadState;->getVersion()I

    move-result p1

    iput p1, p0, Lcom/discord/stores/StoreClientDataState;->readStateVersion:I

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final handleMessageCreate(Lcom/discord/models/domain/ModelMessage;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/stores/StoreClientDataState;->updateHighestLastMessageId(J)V

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final handleRoleAddOrRemove(Lcom/discord/models/domain/ModelGuildRole$Payload;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole$Payload;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole$Payload;->getGuildId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole$Payload;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object p1

    invoke-static {p1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final observeClientState()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreClientDataState$ClientDataState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreClientDataState;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreClientDataState$observeClientState$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreClientDataState$observeClientState$1;-><init>(Lcom/discord/stores/StoreClientDataState;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public snapshotData()V
    .locals 7

    invoke-super {p0}, Lcom/discord/stores/StoreV2;->snapshotData()V

    new-instance v6, Lcom/discord/stores/StoreClientDataState$ClientDataState;

    iget-object v1, p0, Lcom/discord/stores/StoreClientDataState;->guildHashes:Ljava/util/Map;

    iget-wide v2, p0, Lcom/discord/stores/StoreClientDataState;->highestLastMessageId:J

    iget v4, p0, Lcom/discord/stores/StoreClientDataState;->readStateVersion:I

    iget v5, p0, Lcom/discord/stores/StoreClientDataState;->userGuildSettingsVersion:I

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreClientDataState$ClientDataState;-><init>(Ljava/util/Map;JII)V

    iput-object v6, p0, Lcom/discord/stores/StoreClientDataState;->clientDataStateSnapshot:Lcom/discord/stores/StoreClientDataState$ClientDataState;

    return-void
.end method
