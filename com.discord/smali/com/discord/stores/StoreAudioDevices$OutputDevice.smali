.class public abstract Lcom/discord/stores/StoreAudioDevices$OutputDevice;
.super Ljava/lang/Object;
.source "StoreAudioDevices.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreAudioDevices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OutputDevice"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;,
        Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;,
        Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;,
        Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices$OutputDevice;-><init>()V

    return-void
.end method
