.class public final Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;
.super Ljava/lang/Object;
.source "StoreAudioManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AudioManagerSettings"
.end annotation


# instance fields
.field private final settingBluetoothScoOn:Z

.field private final settingMode:I

.field private final settingSpeakerPhoneOn:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;-><init>(ZIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZIZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    iput p2, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    iput-boolean p3, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    return-void
.end method

.method public synthetic constructor <init>(ZIZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;-><init>(ZIZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;ZIZILjava/lang/Object;)Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->copy(ZIZ)Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    return v0
.end method

.method public final copy(ZIZ)Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;-><init>(ZIZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    iget v1, p1, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    iget-boolean p1, p1, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSettingBluetoothScoOn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    return v0
.end method

.method public final getSettingMode()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    return v0
.end method

.method public final getSettingSpeakerPhoneOn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "AudioManagerSettings(settingSpeakerPhoneOn="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingSpeakerPhoneOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", settingMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", settingBluetoothScoOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$AudioManagerSettings;->settingBluetoothScoOn:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
