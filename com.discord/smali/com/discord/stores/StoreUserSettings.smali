.class public Lcom/discord/stores/StoreUserSettings;
.super Lcom/discord/stores/Store;
.source "StoreUserSettings.java"


# instance fields
.field private final allowAccessibilityDetectionPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private applicationContext:Landroid/content/Context;

.field private final collector:Lcom/discord/stores/StoreStream;

.field private final customStatusSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Lcom/discord/models/domain/ModelCustomStatusSetting;",
            "Lcom/discord/models/domain/ModelCustomStatusSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultGuildsRestrictedSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private expireCustomStatusSubscription:Lrx/Subscription;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final explicitContentFilterSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final fontScalingPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final friendSourceFlagsSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;",
            "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;",
            ">;"
        }
    .end annotation
.end field

.field private final guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildFolder;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localePublisher:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final showCurrentGame:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final stickerAnimationSettingsPublisher:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final themePublisher:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->explicitContentFilterSubject:Lrx/subjects/Subject;

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->defaultGuildsRestrictedSubject:Lrx/subjects/Subject;

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->friendSourceFlagsSubject:Lrx/subjects/Subject;

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->customStatusSubject:Lrx/subjects/Subject;

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "RESTRICTED_GUILD_IDS"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "STORE_SETTINGS_FOLDERS_V1"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "STORE_SETTINGS_ALLOW_ANIMATED_EMOJIS"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "CACHE_KEY_STICKER_ANIMATION_SETTINGS_V1"

    invoke-direct {v0, v3, v2}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->stickerAnimationSettingsPublisher:Lcom/discord/utilities/persister/Persister;

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    const-string v2, "STORE_SETTINGS_ALLOW_GAME_STATUS"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CACHE_KEY_FONT_SCALE"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    new-instance v0, Lcom/discord/utilities/persister/Persister;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v2, "STORE_SETTINGS_ALLOW_ACCESSIBILITY_DETECTION"

    invoke-direct {v0, v2, v1}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAccessibilityDetectionPublisher:Lcom/discord/utilities/persister/Persister;

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/stores/StoreUserSettings;->expireCustomStatusSubscription:Lrx/Subscription;

    iput-object p1, p0, Lcom/discord/stores/StoreUserSettings;->collector:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreUserSettings;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-void
.end method

.method public static synthetic a(Lcom/discord/stores/StoreUserSettings;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->getAdjustedTheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static cacheAndPublishString(Landroid/content/SharedPreferences;Lrx/subjects/Subject;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, p3, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-interface {p1, p2}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private expireCustomStatus()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreUserSettings;->updateCustomStatus(Lcom/discord/models/domain/ModelCustomStatusSetting;)Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lf/a/k/i;->d:Lf/a/k/i;

    invoke-static {v2, v0}, Lf/a/b/r;->j(Lrx/functions/Action1;Landroid/content/Context;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method private getAdjustedTheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "dark"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemePureEvil()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "pureEvil"

    :cond_0
    return-object p1
.end method

.method private getOsTheme()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->applicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, 0x30

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const-string v0, "dark"

    return-object v0

    :cond_0
    const-string v0, "light"

    return-object v0
.end method

.method private handleUserSettings(Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getGuildFolders()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getRestrictedGuilds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineEmbedMedia()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineEmbedMedia()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setInlineEmbedMedia(Lcom/discord/app/AppActivity;Z)V

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineAttachmentMedia()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getInlineAttachmentMedia()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setShowAttachmentMediaInline(Lcom/discord/app/AppActivity;Z)V

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getRenderEmbeds()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getRenderEmbeds()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setRenderEmbeds(Lcom/discord/app/AppActivity;Z)V

    :cond_2
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getAnimateEmoji()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getAnimateEmoji()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setAllowAnimatedEmojis(Lcom/discord/app/AppActivity;Z)V

    :cond_3
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getAnimateStickers()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getAnimateStickers()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/discord/stores/StoreUserSettings;->setStickerAnimationSettings(Lcom/discord/app/AppActivity;I)V

    :cond_4
    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/discord/stores/StoreUserSettings;->guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v2, v0}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDeveloperMode()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDeveloperMode()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setDeveloperModeInternal(Z)V

    :cond_7
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getShowCurrentGame()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getShowCurrentGame()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getTheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getTheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setTheme(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setLocale(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getExplicitContentFilter()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->explicitContentFilterSubject:Lrx/subjects/Subject;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getExplicitContentFilter()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_b
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getFriendSourceFlags()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->friendSourceFlagsSubject:Lrx/subjects/Subject;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getFriendSourceFlags()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_c
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDefaultGuildsRestricted()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->defaultGuildsRestrictedSubject:Lrx/subjects/Subject;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getDefaultGuildsRestricted()Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_d
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getCustomStatus()Lcom/discord/models/domain/ModelCustomStatusSetting;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings;->getCustomStatus()Lcom/discord/models/domain/ModelCustomStatusSetting;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->updateLocalCustomStatus(Lcom/discord/models/domain/ModelCustomStatusSetting;)V

    :cond_e
    return-void
.end method

.method private notifyAccessibilitySettingsUpdated()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAccessibilityDetectionPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/discord/stores/StoreUserSettings;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v3, Lf/a/k/g;

    invoke-direct {v3, p0, v0, v1}, Lf/a/k/g;-><init>(Lcom/discord/stores/StoreUserSettings;ZI)V

    invoke-virtual {v2, v3}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private setDeveloperModeInternal(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_DEVELOPER_MODE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private setLocale(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    const-string v2, "CACHE_KEY_LOCALE"

    invoke-static {v0, v1, p1, v2}, Lcom/discord/stores/StoreUserSettings;->cacheAndPublishString(Landroid/content/SharedPreferences;Lrx/subjects/Subject;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setTheme(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    const-string v2, "CACHE_KEY_THEME"

    invoke-static {v0, v1, p1, v2}, Lcom/discord/stores/StoreUserSettings;->cacheAndPublishString(Landroid/content/SharedPreferences;Lrx/subjects/Subject;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_LAST_THEME"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private updateAllowAccessibilityDetectionInternal(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAccessibilityDetectionPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/discord/stores/StoreUserSettings;->notifyAccessibilitySettingsUpdated()V

    return-void
.end method

.method private declared-synchronized updateLocalCustomStatus(Lcom/discord/models/domain/ModelCustomStatusSetting;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->expireCustomStatusSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->customStatusSubject:Lrx/subjects/Subject;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    sget-object v0, Lcom/discord/models/domain/ModelCustomStatusSetting;->Companion:Lcom/discord/models/domain/ModelCustomStatusSetting$Companion;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelCustomStatusSetting$Companion;->getCLEAR()Lcom/discord/models/domain/ModelCustomStatusSetting;

    move-result-object v0

    if-eq p1, v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getExpiresAt()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelCustomStatusSetting;->getExpiresAt()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/time/TimeUtils;->parseUTCDate(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p1}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lf/a/k/m;

    invoke-direct {v0, p0}, Lf/a/k/m;-><init>(Lcom/discord/stores/StoreUserSettings;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->Q(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreUserSettings;->expireCustomStatusSubscription:Lrx/Subscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private static updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V
    .locals 1

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/utilities/rest/RestAPI;->updateUserSettings(Lcom/discord/restapi/RestAPIParams$UserSettings;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lf/a/k/h;

    invoke-direct {v0, p2, p0}, Lf/a/k/h;-><init>(Ljava/lang/Integer;Lcom/discord/app/AppActivity;)V

    invoke-static {v0, p0}, Lf/a/b/r;->j(Lrx/functions/Action1;Landroid/content/Context;)Lrx/Observable$c;

    move-result-object p0

    invoke-virtual {p1, p0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method


# virtual methods
.method public synthetic b(ZI)Lkotlin/Unit;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->collector:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreStream;->handleAccessibilitySettingsUpdated(ZI)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic c(ZLcom/discord/models/domain/ModelUserSettings;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->updateAllowAccessibilityDetectionInternal(Z)V

    return-void
.end method

.method public synthetic d(Ljava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreUserSettings;->expireCustomStatus()V

    return-void
.end method

.method public getAllowAnimatedEmojisObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getAutoImageCompressionEnabled()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_IMAGE_COMPRESSION"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getConsents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/Consents;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->api:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getConsents()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/k/j;->d:Lf/a/k/j;

    invoke-virtual {v0, v1}, Lrx/Observable;->I(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getCustomStatus()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelCustomStatusSetting;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->customStatusSubject:Lrx/subjects/Subject;

    sget-object v1, Lf/a/b/q;->d:Lf/a/b/q;

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultGuildsRestricted()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->defaultGuildsRestrictedSubject:Lrx/subjects/Subject;

    sget-object v1, Lf/a/b/q;->d:Lf/a/b/q;

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getDeveloperMode()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_DEVELOPER_MODE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getExplicitContentFilter()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->explicitContentFilterSubject:Lrx/subjects/Subject;

    sget-object v1, Lf/a/b/q;->d:Lf/a/b/q;

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getFontScale()I
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getFontScaleObs()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getFriendSourceFlags()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->friendSourceFlagsSubject:Lrx/subjects/Subject;

    sget-object v1, Lf/a/b/q;->d:Lf/a/b/q;

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getGuildFolders()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildFolder;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->guildFoldersPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/b/q;->d:Lf/a/b/q;

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getInlineEmbedMedia()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_INLINE_EMBED_MEDIA"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/discord/models/domain/ModelUserSettings;->LOCALE_DEFAULT:Ljava/lang/String;

    const-string v2, "CACHE_KEY_LOCALE"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocaleObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getLocaleSync()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_LOCALE_SYNC"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getMobileOverlay()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_MOBILE_OVERLAY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRenderEmbeds()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_RENDER_EMBEDS"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRestrictedGuildIds()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/b/q;->d:Lf/a/b/q;

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getShiftEnterToSend()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_SHIFT_ENTER_TO_SEND"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowAttachmentMediaInline()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_INLINE_ATTACHMENT_MEDIA"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowCurrentGame()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getStickerAnimationSettings()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->stickerAnimationSettingsPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getSyncTextAndImages()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_SYNC_TEXT_AND_IMAGES"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getTheme()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_THEME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/discord/stores/StoreUserSettings;->getOsTheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CACHE_KEY_LAST_THEME"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setTheme(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->getAdjustedTheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThemeObservable()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    new-instance v1, Lf/a/k/k;

    invoke-direct {v1, p0}, Lf/a/k/k;-><init>(Lcom/discord/stores/StoreUserSettings;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getThemeObservable(Z)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeObservable()Lrx/Observable;

    move-result-object p1

    const-wide/16 v0, 0x5dc

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Lrx/Observable;->L(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeObservable()Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getThemePureEvil()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_THEME_PURE_EVIL"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getThemeSync()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_THEME_SYNC"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getUseChromeCustomTabs()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "CACHE_KEY_USE_CHROME_CUSTOM_TABS"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getUserSettings()Lcom/discord/models/domain/ModelUserSettings;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->handleUserSettings(Lcom/discord/models/domain/ModelUserSettings;)V

    return-void
.end method

.method public handleLoginResult(Lcom/discord/models/domain/auth/ModelLoginResult;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/auth/ModelLoginResult;->getUserSettings()Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;->getTheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;->getTheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreUserSettings;->setTheme(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;->getLocale()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;->getLocale()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->setLocale(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public handlePreLogout()V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->clear()Ljava/lang/Object;

    return-void
.end method

.method public handleUserSettingsUpdate(Lcom/discord/models/domain/ModelUserSettings;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreUserSettings;->handleUserSettings(Lcom/discord/models/domain/ModelUserSettings;)V

    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/discord/stores/StoreUserSettings;->applicationContext:Landroid/content/Context;

    iget-object p1, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getTheme()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public observeAllowAccessibilityDetection()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAccessibilityDetectionPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public observeStickerAnimationSettings()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->stickerAnimationSettingsPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->getObservable()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->themePublisher:Lrx/subjects/Subject;

    const-string v1, "dark"

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->localePublisher:Lrx/subjects/Subject;

    sget-object v1, Lcom/discord/models/domain/ModelUserSettings;->LOCALE_DEFAULT:Ljava/lang/String;

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setAllowAnimatedEmojis(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->allowAnimatedEmojisPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithAllowAnimatedEmojis(Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method public setAutoImageCompressionEnabled(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getAutoImageCompressionEnabled()Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_IMAGE_COMPRESSION"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setDefaultGuildsRestricted(Lcom/discord/app/AppActivity;ZLjava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppActivity;",
            "Z",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p2, p3}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithRestrictedGuilds(Ljava/lang/Boolean;Ljava/util/Collection;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setDeveloperMode(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getDeveloperMode()Z

    move-result v0

    if-eq v0, p2, :cond_0

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithDeveloperMode(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object v0

    const v1, 0x7f12182d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    invoke-direct {p0, p2}, Lcom/discord/stores/StoreUserSettings;->setDeveloperModeInternal(Z)V

    :cond_0
    return-void
.end method

.method public setExplicitContentFilter(Lcom/discord/app/AppActivity;I)V
    .locals 1

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithExplicitContentFilter(I)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setFontScale(I)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->fontScalingPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/discord/stores/StoreUserSettings;->notifyAccessibilitySettingsUpdated()V

    return-void
.end method

.method public setFriendSourceFlags(Lcom/discord/app/AppActivity;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    invoke-static {p2, p3, p4}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithFriendSourceFlags(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setInlineEmbedMedia(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getInlineEmbedMedia()Z

    move-result v0

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_INLINE_EMBED_MEDIA"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithInlineEmbedMedia(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_1
    return-void
.end method

.method public setLocale(Lcom/discord/app/AppActivity;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithLocale(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const v0, 0x7f120f03

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2}, Lcom/discord/stores/StoreUserSettings;->setLocale(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setLocaleSync(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_LOCALE_SYNC"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setMobileOverlay(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getMobileOverlay()Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_MOBILE_OVERLAY"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-static {p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->overlayToggled(Z)V

    :cond_0
    return-void
.end method

.method public setRenderEmbeds(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getRenderEmbeds()Z

    move-result v0

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_RENDER_EMBEDS"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithRenderEmbeds(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_1
    return-void
.end method

.method public setRestrictedGuildId(Lcom/discord/app/AppActivity;JZ)V
    .locals 7

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->restrictedGuildIdsPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/discord/stores/StoreUserSettings;->setRestrictedGuildIds(Lcom/discord/app/AppActivity;Ljava/util/Collection;JZ)V

    return-void
.end method

.method public setRestrictedGuildIds(Lcom/discord/app/AppActivity;Ljava/util/Collection;JZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppActivity;",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;JZ)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-eqz p5, :cond_0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-nez p5, :cond_1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    const/4 p2, 0x0

    invoke-static {p2, v0}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithRestrictedGuilds(Ljava/lang/Boolean;Ljava/util/Collection;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p3

    invoke-static {p1, p3, p2}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setShiftEnterToSend(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getShiftEnterToSend()Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_SHIFT_ENTER_TO_SEND"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setShowAttachmentMediaInline(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getShowAttachmentMediaInline()Z

    move-result v0

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_INLINE_ATTACHMENT_MEDIA"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithInlineAttachmentMedia(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_1
    return-void
.end method

.method public setShowCurrentGame(Lcom/discord/app/AppActivity;Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->showCurrentGame:Lcom/discord/utilities/persister/Persister;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithShowCurrentGame(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    return-void
.end method

.method public setStickerAnimationSettings(Lcom/discord/app/AppActivity;I)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreUserSettings;->stickerAnimationSettingsPublisher:Lcom/discord/utilities/persister/Persister;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithStickerAnimationSettings(Ljava/lang/Integer;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method public setSyncTextAndImages(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_SYNC_TEXT_AND_IMAGES"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setSyncTheme(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_THEME_SYNC"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public setTheme(Lcom/discord/app/AppActivity;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    const-string v0, "pureEvil"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "CACHE_KEY_THEME_PURE_EVIL"

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-static {p2}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithTheme(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object v0

    const v1, 0x7f12182d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v0, "dark"

    invoke-static {v0}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithTheme(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object v0

    const v1, 0x7f12182c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/discord/stores/StoreUserSettings;->updateUserSettings(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V

    :cond_1
    :goto_0
    invoke-direct {p0, p2}, Lcom/discord/stores/StoreUserSettings;->setTheme(Ljava/lang/String;)V

    return-void
.end method

.method public setUseChromeCustomTabs(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/Store;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_USE_CHROME_CUSTOM_TABS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public updateAllowAccessibilityDetection(Z)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/discord/restapi/RestAPIParams$UserSettings;->createWithAllowAccessibilityDetection(Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UserSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI;->updateUserSettings(Lcom/discord/restapi/RestAPIParams$UserSettings;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/k/l;

    invoke-direct {v1, p0, p1}, Lf/a/k/l;-><init>(Lcom/discord/stores/StoreUserSettings;Z)V

    invoke-virtual {v0, v1}, Lrx/Observable;->s(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateCustomStatus(Lcom/discord/models/domain/ModelCustomStatusSetting;)Lrx/Observable;
    .locals 2
    .param p1    # Lcom/discord/models/domain/ModelCustomStatusSetting;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelCustomStatusSetting;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelUserSettings;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApiSerializeNulls()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    new-instance v1, Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;

    invoke-direct {v1, p1}, Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;-><init>(Lcom/discord/models/domain/ModelCustomStatusSetting;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI;->updateUserSettingsCustomStatus(Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
