.class public final Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$2;
.super Ljava/lang/Object;
.source "StoreRtcRegion.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreRtcRegion;->fetchRtcLatencyTestRegionsIps()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Boolean;",
        "Lrx/Observable<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/domain/ModelRtcLatencyRegion;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreRtcRegion;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreRtcRegion;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$2;->this$0:Lcom/discord/stores/StoreRtcRegion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$2;->call(Ljava/lang/Boolean;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Boolean;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "+",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelRtcLatencyRegion;",
            ">;>;"
        }
    .end annotation

    const-string v0, "isRtcRegionExperimentEnabled"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreRtcRegion$fetchRtcLatencyTestRegionsIps$2;->this$0:Lcom/discord/stores/StoreRtcRegion;

    invoke-static {p1}, Lcom/discord/stores/StoreRtcRegion;->access$getRestAPI$p(Lcom/discord/stores/StoreRtcRegion;)Lcom/discord/utilities/rest/RestAPI;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/utilities/rest/RestAPI;->getRtcLatencyTestRegionsIps()Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lg0/l/a/f;->e:Lrx/Observable;

    :goto_0
    return-object p1
.end method
