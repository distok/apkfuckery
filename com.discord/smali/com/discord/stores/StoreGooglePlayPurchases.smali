.class public final Lcom/discord/stores/StoreGooglePlayPurchases;
.super Lcom/discord/stores/Store;
.source "StoreGooglePlayPurchases.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGooglePlayPurchases$State;,
        Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;,
        Lcom/discord/stores/StoreGooglePlayPurchases$Event;,
        Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;,
        Lcom/discord/stores/StoreGooglePlayPurchases$Companion;
    }
.end annotation


# static fields
.field private static final CACHED_ANALYTICS_TTL:J = 0xf731400L

.field private static final CACHE_KEY_PAYMENT_FLOW_ANALYTICS:Ljava/lang/String; = "CACHE_KEY_PAYMENT_FLOW_ANALYTICS"

.field public static final Companion:Lcom/discord/stores/StoreGooglePlayPurchases$Companion;


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/stores/StoreGooglePlayPurchases$Event;",
            ">;"
        }
    .end annotation
.end field

.field private isDirty:Z

.field private pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

.field private purchases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation
.end field

.field private final queryStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;",
            ">;"
        }
    .end annotation
.end field

.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreGooglePlayPurchases$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreGooglePlayPurchases$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreGooglePlayPurchases$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreGooglePlayPurchases;->Companion:Lcom/discord/stores/StoreGooglePlayPurchases$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->clock:Lcom/discord/utilities/time/Clock;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->purchases:Ljava/util/List;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object p1, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;->INSTANCE:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->queryStateSubject:Lrx/subjects/BehaviorSubject;

    sget-object p1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Uninitialized;->INSTANCE:Lcom/discord/stores/StoreGooglePlayPurchases$State$Uninitialized;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->stateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getEventSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->eventSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$getPendingDowngrade$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lcom/discord/stores/PendingDowngrade;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    return-object p0
.end method

.method public static final synthetic access$getQueryStateSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->queryStateSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getStateSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->stateSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$handleDowngradeFailure(Lcom/discord/stores/StoreGooglePlayPurchases;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->handleDowngradeFailure(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handleDowngradeSuccess(Lcom/discord/stores/StoreGooglePlayPurchases;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->handleDowngradeSuccess(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$handlePurchases(Lcom/discord/stores/StoreGooglePlayPurchases;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->handlePurchases(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$isDirty$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->isDirty:Z

    return p0
.end method

.method public static final synthetic access$setDirty$p(Lcom/discord/stores/StoreGooglePlayPurchases;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->isDirty:Z

    return-void
.end method

.method public static final synthetic access$setPendingDowngrade$p(Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/stores/PendingDowngrade;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    return-void
.end method

.method public static final synthetic access$shouldRetryDowngrade(Lcom/discord/stores/StoreGooglePlayPurchases;Ljava/lang/Throwable;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->shouldRetryDowngrade(Ljava/lang/Throwable;)Z

    move-result p0

    return p0
.end method

.method private final cacheAnalyticsTraits(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/gson/Gson;->k(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CACHE_KEY_PAYMENT_FLOW_ANALYTICS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private final clearAnalyticsTraits(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/discord/stores/StoreGooglePlayPurchases;->getCachedAnalyticsTraitsMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGooglePlayPurchases;->cacheAnalyticsTraits(Ljava/util/Map;)V

    return-void
.end method

.method private final getCachedAnalyticsTraitsMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CACHE_KEY_PAYMENT_FLOW_ANALYTICS"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$getCachedAnalyticsTraitsMap$1$typeToken$1;

    invoke-direct {v1}, Lcom/discord/stores/StoreGooglePlayPurchases$getCachedAnalyticsTraitsMap$1$typeToken$1;-><init>()V

    invoke-virtual {v1}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v2, v0, v1}, Lcom/google/gson/Gson;->f(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    :goto_0
    return-object v0
.end method

.method private final getOrClearAnalyticsTraits(Ljava/lang/String;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;
    .locals 7

    invoke-direct {p0}, Lcom/discord/stores/StoreGooglePlayPurchases;->getCachedAnalyticsTraitsMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getTimestamp()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xf731400

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->clearAnalyticsTraits(Ljava/lang/String;)V

    return-object v1

    :cond_1
    return-object v0

    :cond_2
    return-object v1
.end method

.method private final handleDowngradeFailure(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->updatePendingDowngrade(Lcom/discord/stores/PendingDowngrade;)V

    iget-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->queryStateSubject:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;->INSTANCE:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleDowngradeSuccess(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->updatePendingDowngrade(Lcom/discord/stores/PendingDowngrade;)V

    iget-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->queryStateSubject:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;->INSTANCE:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handlePurchases(Ljava/util/List;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->purchases:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->purchases:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->purchases:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v0

    if-ne v2, v0, :cond_1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    sget-object p1, Lx/h/l;->d:Lx/h/l;

    :goto_1
    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->purchases:Ljava/util/List;

    iput-boolean v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->isDirty:Z

    return-void
.end method

.method private final shouldRetryDowngrade(Ljava/lang/Throwable;)Z
    .locals 3

    instance-of v0, p1, Ljava/util/concurrent/TimeoutException;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lretrofit2/HttpException;

    if-eqz v0, :cond_2

    const/16 v0, 0x257

    const/16 v2, 0x1f4

    check-cast p1, Lretrofit2/HttpException;

    invoke-virtual {p1}, Lretrofit2/HttpException;->a()I

    move-result p1

    if-le v2, p1, :cond_1

    goto :goto_0

    :cond_1
    if-lt v0, p1, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    instance-of v1, p1, Ljava/io/IOException;

    :cond_3
    :goto_0
    return v1
.end method


# virtual methods
.method public final downgradePurchase()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final getQueryState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->queryStateSubject:Lrx/subjects/BehaviorSubject;

    const-string v1, "queryStateSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGooglePlayPurchases$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->stateSubject:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v1, "stateSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGooglePlayPurchases$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDispatchEnded()V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->stateSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->purchases:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v3, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final onVerificationFailure(Lcom/android/billingclient/api/Purchase;)V
    .locals 2

    const-string v0, "purchase"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationFailure$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationFailure$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/android/billingclient/api/Purchase;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onVerificationStart()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationStart$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationStart$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onVerificationSuccess(Lcom/android/billingclient/api/Purchase;)V
    .locals 2

    const-string v0, "purchase"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/android/billingclient/api/Purchase;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final processPurchases(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final trackPaymentFlowCompleted(Ljava/lang/String;)V
    .locals 9

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->getOrClearAnalyticsTraits(Ljava/lang/String;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getLocationTrait()Lcom/discord/utilities/analytics/Traits$Location;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getStoreSkuTrait()Lcom/discord/utilities/analytics/Traits$StoreSku;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getPaymentTrait()Lcom/discord/utilities/analytics/Traits$Payment;

    move-result-object v4

    const/4 v6, 0x0

    const/16 v7, 0x12

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowCompleted$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$Payment;Lcom/discord/utilities/analytics/Traits$StoreSku;Ljava/lang/String;ILjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->clearAnalyticsTraits(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final trackPaymentFlowFailed(Ljava/lang/String;)V
    .locals 8

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->getOrClearAnalyticsTraits(Ljava/lang/String;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getLocationTrait()Lcom/discord/utilities/analytics/Traits$Location;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getStoreSkuTrait()Lcom/discord/utilities/analytics/Traits$StoreSku;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getPaymentTrait()Lcom/discord/utilities/analytics/Traits$Payment;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowFailed$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->clearAnalyticsTraits(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final trackPaymentFlowStarted(Ljava/lang/String;JLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V
    .locals 9

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locationTrait"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeSkuTrait"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTrait"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v4

    move-object v1, v0

    move-wide v2, p2

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;-><init>(JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreGooglePlayPurchases;->getCachedAnalyticsTraitsMap()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2}, Lcom/discord/stores/StoreGooglePlayPurchases;->cacheAnalyticsTraits(Ljava/util/Map;)V

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getLocationTrait()Lcom/discord/utilities/analytics/Traits$Location;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getStoreSkuTrait()Lcom/discord/utilities/analytics/Traits$StoreSku;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getPaymentTrait()Lcom/discord/utilities/analytics/Traits$Payment;

    move-result-object v5

    const/4 v3, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowStarted$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    return-void
.end method

.method public final trackPaymentFlowStep(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const-string v0, "paymentGatewaySkuId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fromStep"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toStep"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->getOrClearAnalyticsTraits(Ljava/lang/String;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    move-result-object p1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getLocationTrait()Lcom/discord/utilities/analytics/Traits$Location;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getStoreSkuTrait()Lcom/discord/utilities/analytics/Traits$StoreSku;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->getPaymentTrait()Lcom/discord/utilities/analytics/Traits$Payment;

    move-result-object v6

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    move-object v4, p2

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowStep$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final updatePendingDowngrade(Lcom/discord/stores/PendingDowngrade;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGooglePlayPurchases$updatePendingDowngrade$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGooglePlayPurchases$updatePendingDowngrade$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/stores/PendingDowngrade;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
