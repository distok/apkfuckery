.class public final Lcom/discord/stores/StoreStickers$observeFrequentlyUsedStickers$1;
.super Ljava/lang/Object;
.source "StoreStickers.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStickers;->observeFrequentlyUsedStickers()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/utilities/media/MediaFrecencyTracker;",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/sticker/dto/ModelSticker;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreStickers;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers$observeFrequentlyUsedStickers$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/media/MediaFrecencyTracker;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreStickers$observeFrequentlyUsedStickers$1;->call(Lcom/discord/utilities/media/MediaFrecencyTracker;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/utilities/media/MediaFrecencyTracker;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/media/MediaFrecencyTracker;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            ">;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/discord/utilities/frecency/FrecencyTracker;->getSortedKeys$default(Lcom/discord/utilities/frecency/FrecencyTracker;JILjava/lang/Object;)Ljava/util/Collection;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/discord/stores/StoreStickers$observeFrequentlyUsedStickers$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v2}, Lcom/discord/stores/StoreStickers;->access$getStickersSnapshot$p(Lcom/discord/stores/StoreStickers;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/sticker/dto/ModelSticker;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/16 p1, 0x14

    invoke-static {v0, p1}, Lx/h/f;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
