.class public final Lcom/discord/stores/StorePremiumGuildSubscription;
.super Ljava/lang/Object;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StorePremiumGuildSubscription$State;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->dispatcher:Lcom/discord/stores/Dispatcher;

    sget-object p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getState$p(Lcom/discord/stores/StorePremiumGuildSubscription;)Lcom/discord/stores/StorePremiumGuildSubscription$State;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    return-object p0
.end method

.method public static final synthetic access$isDirty$p(Lcom/discord/stores/StorePremiumGuildSubscription;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return p0
.end method

.method public static final synthetic access$setDirty$p(Lcom/discord/stores/StorePremiumGuildSubscription;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return-void
.end method

.method public static final synthetic access$setState$p(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StorePremiumGuildSubscription$State;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    return-void
.end method

.method public static synthetic getPremiumGuildSubscriptionsState$default(Lcom/discord/stores/StorePremiumGuildSubscription;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState(Ljava/lang/Long;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final fetchUserGuildPremiumState()V
    .locals 13

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$fetchUserGuildPremiumState$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StorePremiumGuildSubscription$fetchUserGuildPremiumState$1;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getSubscriptionSlots()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StorePremiumGuildSubscription;

    new-instance v8, Lcom/discord/stores/StorePremiumGuildSubscription$fetchUserGuildPremiumState$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StorePremiumGuildSubscription$fetchUserGuildPremiumState$2;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription;)V

    new-instance v10, Lcom/discord/stores/StorePremiumGuildSubscription$fetchUserGuildPremiumState$3;

    invoke-direct {v10, p0}, Lcom/discord/stores/StorePremiumGuildSubscription$fetchUserGuildPremiumState$3;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getDispatcher()Lcom/discord/stores/Dispatcher;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object v0
.end method

.method public final getPremiumGuildSubscriptionsState(Ljava/lang/Long;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stateSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo v0, "stateSubject.map { state\u2026  state\n        }\n      }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final handleFetchError()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    iput-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return-void
.end method

.method public final handleFetchStateSuccess(Ljava/util/List;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;)V"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionSlots"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v0

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    const/16 v0, 0x10

    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-direct {p1, v1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return-void
.end method

.method public final handleFetchingState()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;->INSTANCE:Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    iput-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    return-void
.end method

.method public onDispatchEnded()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->stateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->state:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final updateUserPremiumGuildSubscriptionSlot(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V
    .locals 2

    const-string v0, "newSlot"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$updateUserPremiumGuildSubscriptionSlot$1;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
