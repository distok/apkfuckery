.class public final Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;
.super Ljava/lang/Object;
.source "StoreVoiceParticipants.kt"

# interfaces
.implements Lrx/functions/Func8;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->call(Lkotlin/Pair;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "T8:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func8<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelVoice$State;",
        ">;",
        "Ljava/util/Set<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/stores/StoreVideoStreams$UserStreams;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Lcom/discord/stores/StoreApplicationStreaming$State;",
        "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/utilities/streams/StreamContext;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $meUser:Lcom/discord/models/domain/ModelUser;

.field public final synthetic $otherUsers:Ljava/util/Collection;

.field public final synthetic this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1$2;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreVoiceParticipants$get$1$2;Lcom/discord/models/domain/ModelUser;Ljava/util/Collection;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1$2;

    iput-object p2, p0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    iput-object p3, p0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;->$otherUsers:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    check-cast p2, Ljava/util/Set;

    check-cast p3, Ljava/util/List;

    check-cast p4, Ljava/util/Map;

    check-cast p5, Ljava/util/Map;

    check-cast p6, Lcom/discord/stores/StoreApplicationStreaming$State;

    check-cast p7, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    check-cast p8, Ljava/util/Map;

    invoke-virtual/range {p0 .. p8}, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;->call(Ljava/util/Map;Ljava/util/Set;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreApplicationStreaming$State;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/Map;Ljava/util/Set;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreApplicationStreaming$State;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Ljava/util/Map;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVideoStreams$UserStreams;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Lcom/discord/stores/StoreApplicationStreaming$State;",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/utilities/streams/StreamContext;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    iget-object v1, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1$2;

    iget-object v1, v1, Lcom/discord/stores/StoreVoiceParticipants$get$1$2;->this$0:Lcom/discord/stores/StoreVoiceParticipants$get$1;

    iget-object v2, v1, Lcom/discord/stores/StoreVoiceParticipants$get$1;->this$0:Lcom/discord/stores/StoreVoiceParticipants;

    iget-object v3, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;->$meUser:Lcom/discord/models/domain/ModelUser;

    const-string v1, "meUser"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/discord/stores/StoreVoiceParticipants$get$1$2$2;->$otherUsers:Ljava/util/Collection;

    const-string v1, "otherUsers"

    invoke-static {v4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "voiceStates"

    move-object v5, p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "speakingUsers"

    move-object v6, p2

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "isRinging"

    move-object/from16 v7, p3

    invoke-static {v7, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "videoStreams"

    move-object/from16 v8, p4

    invoke-static {v8, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "guildMembers"

    move-object/from16 v9, p5

    invoke-static {v9, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "applicationStreamingState"

    move-object/from16 v10, p6

    invoke-static {v10, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "voiceConfig"

    move-object/from16 v11, p7

    invoke-static {v11, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "streamContexts"

    move-object/from16 v12, p8

    invoke-static {v12, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static/range {v2 .. v12}, Lcom/discord/stores/StoreVoiceParticipants;->access$create(Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/models/domain/ModelUser;Ljava/util/Collection;Ljava/util/Map;Ljava/util/Set;Ljava/util/Collection;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreApplicationStreaming$State;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method
