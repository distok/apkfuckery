.class public final Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;
.super Lx/m/c/k;
.source "StoreStreamRtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->onStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $state:Lcom/discord/rtcconnection/RtcConnection$State;

.field public final synthetic this$0:Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;->this$0:Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;

    iput-object p2, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;->$state:Lcom/discord/rtcconnection/RtcConnection$State;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;->this$0:Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;

    iget-object v0, v0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;->this$0:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-static {v0}, Lcom/discord/stores/StoreStreamRtcConnection;->access$getStoreStream$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1;->$state:Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream;->handleStreamRtcConnectionStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V

    return-void
.end method
