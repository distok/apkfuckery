.class public final Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;
.super Lx/m/c/k;
.source "StoreLurking.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreLurking;->startLurkingInternal(JLjava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:Ljava/lang/Long;

.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreLurking;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreLurking;Ljava/lang/Long;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->this$0:Lcom/discord/stores/StoreLurking;

    iput-object p2, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->$channelId:Ljava/lang/Long;

    iput-wide p3, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->$guildId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 9

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->$channelId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->$guildId:J

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->$channelId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel$default(Lcom/discord/utilities/channel/ChannelSelector;JJIILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->this$0:Lcom/discord/stores/StoreLurking;

    invoke-static {v0}, Lcom/discord/stores/StoreLurking;->access$getStream$p(Lcom/discord/stores/StoreLurking;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getGuildSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreLurking$startLurkingInternal$jumpToDestination$1;->$guildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildSelected;->handleGuildSelected(J)V

    :goto_0
    return-void
.end method
