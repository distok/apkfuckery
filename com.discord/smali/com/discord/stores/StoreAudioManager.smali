.class public final Lcom/discord/stores/StoreAudioManager;
.super Lcom/discord/stores/Store;
.source "StoreAudioManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;,
        Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;
    }
.end annotation


# instance fields
.field private final audioDevices:Lcom/discord/stores/StoreAudioDevices;

.field private final rtcConnectionStore:Lcom/discord/stores/StoreRtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAudioDevices;Lcom/discord/stores/StoreRtcConnection;)V
    .locals 1

    const-string v0, "audioDevices"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtcConnectionStore"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreAudioManager;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    iput-object p2, p0, Lcom/discord/stores/StoreAudioManager;->rtcConnectionStore:Lcom/discord/stores/StoreRtcConnection;

    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 10

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    new-instance v0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;

    sget-object v1, Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;->INSTANCE:Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;

    invoke-virtual {v1, p1}, Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;->get(Landroid/content/Context;)Landroid/media/AudioManager;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;-><init>(Landroid/media/AudioManager;)V

    iget-object p1, p0, Lcom/discord/stores/StoreAudioManager;->rtcConnectionStore:Lcom/discord/stores/StoreRtcConnection;

    invoke-virtual {p1}, Lcom/discord/stores/StoreRtcConnection;->getConnectionState()Lrx/Observable;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/stores/StoreAudioManager;->audioDevices:Lcom/discord/stores/StoreAudioDevices;

    invoke-virtual {v1}, Lcom/discord/stores/StoreAudioDevices;->getAudioDevicesState()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreAudioManager$init$1;->INSTANCE:Lcom/discord/stores/StoreAudioManager$init$1;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/discord/stores/StoreAudioManager$sam$rx_functions_Func2$0;

    invoke-direct {v3, v2}, Lcom/discord/stores/StoreAudioManager$sam$rx_functions_Func2$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lrx/functions/Func2;

    invoke-static {p1, v1, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    const-string v1, "Observable\n        .comb\u2026::Configuration\n        )"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v1, 0x1f4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->leadingEdgeThrottle(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    const-string p1, "Observable\n        .comb\u2026  .distinctUntilChanged()"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v2, Lcom/discord/stores/StoreAudioManager;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/stores/StoreAudioManager$init$2;

    invoke-direct {v7, v0}, Lcom/discord/stores/StoreAudioManager$init$2;-><init>(Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
