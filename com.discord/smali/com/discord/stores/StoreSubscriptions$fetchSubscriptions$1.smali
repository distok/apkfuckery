.class public final Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;
.super Lx/m/c/k;
.source "StoreSubscriptions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreSubscriptions;->fetchSubscriptions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreSubscriptions;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreSubscriptions;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;->this$0:Lcom/discord/stores/StoreSubscriptions;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 13

    iget-object v0, p0, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;->this$0:Lcom/discord/stores/StoreSubscriptions;

    invoke-static {v0}, Lcom/discord/stores/StoreSubscriptions;->access$getSubscriptionsState$p(Lcom/discord/stores/StoreSubscriptions;)Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;->this$0:Lcom/discord/stores/StoreSubscriptions;

    invoke-static {v0}, Lcom/discord/stores/StoreSubscriptions;->access$handleSubscriptionsFetchStart(Lcom/discord/stores/StoreSubscriptions;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getSubscriptions()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;->this$0:Lcom/discord/stores/StoreSubscriptions;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v10, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1$1;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1$1;-><init>(Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;)V

    const/4 v9, 0x0

    new-instance v8, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1$2;-><init>(Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
