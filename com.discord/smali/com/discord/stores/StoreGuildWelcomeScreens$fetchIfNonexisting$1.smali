.class public final Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;
.super Lx/m/c/k;
.source "StoreGuildWelcomeScreens.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGuildWelcomeScreens;->fetchIfNonexisting(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;

    iput-wide p2, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->$guildId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;

    invoke-static {v0}, Lcom/discord/stores/StoreGuildWelcomeScreens;->access$getGuildWelcomeScreensState$p(Lcom/discord/stores/StoreGuildWelcomeScreens;)Ljava/util/HashMap;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->$guildId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    instance-of v1, v0, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Loaded;

    if-nez v1, :cond_1

    instance-of v0, v0, Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen$Fetching;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;

    iget-wide v1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->$guildId:J

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreGuildWelcomeScreens;->access$handleGuildWelcomeScreenFetchStart(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->$guildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/rest/RestAPI;->getGuildWelcomeScreen(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object v2

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;->this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v8, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1$1;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1$1;-><init>(Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;)V

    const/4 v7, 0x0

    new-instance v6, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1$2;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1$2;-><init>(Lcom/discord/stores/StoreGuildWelcomeScreens$fetchIfNonexisting$1;)V

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
