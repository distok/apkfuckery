.class public final Lcom/discord/stores/StorePendingReplies$PendingReply;
.super Ljava/lang/Object;
.source "StorePendingReplies.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StorePendingReplies;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PendingReply"
.end annotation


# instance fields
.field private final messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

.field private final originalMessage:Lcom/discord/models/domain/ModelMessage;

.field private shouldMention:Z

.field private final showMentionToggle:Z


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelMessage;ZZ)V
    .locals 1

    const-string v0, "messageReference"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalMessage"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    iput-object p2, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    iput-boolean p3, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    iput-boolean p4, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StorePendingReplies$PendingReply;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelMessage;ZZILjava/lang/Object;)Lcom/discord/stores/StorePendingReplies$PendingReply;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StorePendingReplies$PendingReply;->copy(Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelMessage;ZZ)Lcom/discord/stores/StorePendingReplies$PendingReply;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelMessage$MessageReference;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelMessage;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelMessage;ZZ)Lcom/discord/stores/StorePendingReplies$PendingReply;
    .locals 1

    const-string v0, "messageReference"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalMessage"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StorePendingReplies$PendingReply;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/stores/StorePendingReplies$PendingReply;-><init>(Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelMessage;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StorePendingReplies$PendingReply;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StorePendingReplies$PendingReply;

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    iget-object v1, p1, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    iget-object v1, p1, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    iget-boolean v1, p1, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    iget-boolean p1, p1, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    return-object v0
.end method

.method public final getOriginalMessage()Lcom/discord/models/domain/ModelMessage;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    return-object v0
.end method

.method public final getShouldMention()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    return v0
.end method

.method public final getShowMentionToggle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage$MessageReference;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMessage;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    return v0
.end method

.method public final setShouldMention(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "PendingReply(messageReference="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->messageReference:Lcom/discord/models/domain/ModelMessage$MessageReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", originalMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->originalMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldMention="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->shouldMention:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showMentionToggle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StorePendingReplies$PendingReply;->showMentionToggle:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
