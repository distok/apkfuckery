.class public final Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;
.super Ljava/lang/Object;
.source "StoreAudioManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreAudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AudioManagerProvider"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;

    invoke-direct {v0}, Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;->INSTANCE:Lcom/discord/stores/StoreAudioManager$AudioManagerProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(Landroid/content/Context;)Landroid/media/AudioManager;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Landroid/media/AudioManager;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Landroid/media/AudioManager;

    return-object p1
.end method
