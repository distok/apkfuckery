.class public final Lcom/discord/stores/StoreMessages$deleteMessage$2;
.super Lx/m/c/k;
.source "StoreMessages.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessages;->deleteMessage(Lcom/discord/models/domain/ModelMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $message:Lcom/discord/models/domain/ModelMessage;

.field public final synthetic this$0:Lcom/discord/stores/StoreMessages;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->this$0:Lcom/discord/stores/StoreMessages;

    iput-object p2, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->$message:Lcom/discord/models/domain/ModelMessage;

    iput-wide p3, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->$channelId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreMessages$deleteMessage$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->$message:Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getNonce()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->this$0:Lcom/discord/stores/StoreMessages;

    iget-wide v2, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->$channelId:J

    invoke-static {v1, v2, v3}, Lcom/discord/stores/StoreMessages;->access$getMessageQueue(Lcom/discord/stores/StoreMessages;J)Lcom/discord/utilities/messagesend/MessageQueue;

    move-result-object v1

    const-string v2, "nonce"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/discord/utilities/messagesend/MessageQueue;->cancel(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->$message:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v1}, Lcom/discord/stores/StoreMessages;->access$handleLocalMessageDelete(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->$message:Lcom/discord/models/domain/ModelMessage;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getType()I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$deleteMessage$2;->$message:Lcom/discord/models/domain/ModelMessage;

    sget-object v2, Lcom/discord/stores/FailedMessageResolutionType;->DELETED:Lcom/discord/stores/FailedMessageResolutionType;

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreMessages;->access$trackFailedLocalMessageResolved(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;Lcom/discord/stores/FailedMessageResolutionType;)V

    :cond_1
    return-void
.end method
