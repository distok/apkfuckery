.class public final Lcom/discord/stores/StoreAuthentication$getShouldShowAgeGate$1;
.super Ljava/lang/Object;
.source "StoreAuthentication.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAuthentication;->getShouldShowAgeGate()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/models/domain/ModelUser$Me;",
        "Ljava/lang/String;",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lcom/discord/stores/StoreNavigation$AgeGate;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreAuthentication$getShouldShowAgeGate$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreAuthentication$getShouldShowAgeGate$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreAuthentication$getShouldShowAgeGate$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreAuthentication$getShouldShowAgeGate$1;->INSTANCE:Lcom/discord/stores/StoreAuthentication$getShouldShowAgeGate$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser$Me;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;)Lcom/discord/stores/StoreNavigation$AgeGate;
    .locals 1

    const/4 v0, 0x0

    if-nez p2, :cond_1

    const/4 p2, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser$Me;->hasBirthday()Z

    move-result p1

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelChannel;->isNsfw()Z

    move-result p1

    if-ne p1, p2, :cond_1

    sget-object v0, Lcom/discord/stores/StoreNavigation$AgeGate;->NSFW_CHANNEL_AGE_GATE:Lcom/discord/stores/StoreNavigation$AgeGate;

    :cond_1
    :goto_0
    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser$Me;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/StoreAuthentication$getShouldShowAgeGate$1;->call(Lcom/discord/models/domain/ModelUser$Me;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;)Lcom/discord/stores/StoreNavigation$AgeGate;

    move-result-object p1

    return-object p1
.end method
