.class public final Lcom/discord/stores/StoreTabsNavigation$selectTab$1;
.super Lx/m/c/k;
.source "StoreTabsNavigation.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreTabsNavigation;->selectTab(Lcom/discord/widgets/tabs/NavigationTab;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $dismissTabsDialogs:Z

.field public final synthetic $navigationTab:Lcom/discord/widgets/tabs/NavigationTab;

.field public final synthetic this$0:Lcom/discord/stores/StoreTabsNavigation;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/widgets/tabs/NavigationTab;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->this$0:Lcom/discord/stores/StoreTabsNavigation;

    iput-object p2, p0, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->$navigationTab:Lcom/discord/widgets/tabs/NavigationTab;

    iput-boolean p3, p0, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->$dismissTabsDialogs:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->this$0:Lcom/discord/stores/StoreTabsNavigation;

    iget-object v1, p0, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->$navigationTab:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-static {v0, v1}, Lcom/discord/stores/StoreTabsNavigation;->access$handleTabSelection(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/widgets/tabs/NavigationTab;)V

    iget-boolean v0, p0, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->$dismissTabsDialogs:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;->this$0:Lcom/discord/stores/StoreTabsNavigation;

    invoke-static {v0}, Lcom/discord/stores/StoreTabsNavigation;->access$dismissTabsDialogs(Lcom/discord/stores/StoreTabsNavigation;)V

    :cond_0
    return-void
.end method
