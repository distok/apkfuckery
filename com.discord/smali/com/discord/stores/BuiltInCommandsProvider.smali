.class public interface abstract Lcom/discord/stores/BuiltInCommandsProvider;
.super Ljava/lang/Object;
.source "StoreApplicationCommands.kt"


# virtual methods
.method public abstract getBuiltInApplication()Lcom/discord/models/slashcommands/ModelApplication;
.end method

.method public abstract getBuiltInCommands()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/stores/ModelApplicationCommand;",
            ">;"
        }
    .end annotation
.end method
