.class public final Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;
.super Lx/m/c/k;
.source "StoreAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAnalytics;->trackFileUploadAlertViewed(Lcom/discord/utilities/rest/FileUploadAlertType;IIIZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $alertType:Lcom/discord/utilities/rest/FileUploadAlertType;

.field public final synthetic $hasImage:Z

.field public final synthetic $hasVideo:Z

.field public final synthetic $isPremium:Z

.field public final synthetic $maxAttachmentSize:I

.field public final synthetic $numAttachments:I

.field public final synthetic $totalAttachmentSize:I

.field public final synthetic this$0:Lcom/discord/stores/StoreAnalytics;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAnalytics;Lcom/discord/utilities/rest/FileUploadAlertType;IIIZZZ)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    iput-object p2, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$alertType:Lcom/discord/utilities/rest/FileUploadAlertType;

    iput p3, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$numAttachments:I

    iput p4, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$maxAttachmentSize:I

    iput p5, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$totalAttachmentSize:I

    iput-boolean p6, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$hasImage:Z

    iput-boolean p7, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$hasVideo:Z

    iput-boolean p8, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$isPremium:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 10

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v0}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getChannelsSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreChannelsSelected;->getId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreAnalytics;->access$getSnapshotProperties(Lcom/discord/stores/StoreAnalytics;J)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$alertType:Lcom/discord/utilities/rest/FileUploadAlertType;

    iget v3, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$numAttachments:I

    iget v4, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$maxAttachmentSize:I

    iget v5, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$totalAttachmentSize:I

    iget-boolean v6, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$hasImage:Z

    iget-boolean v7, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$hasVideo:Z

    iget-boolean v8, p0, Lcom/discord/stores/StoreAnalytics$trackFileUploadAlertViewed$1;->$isPremium:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    move-object v9, v0

    invoke-virtual/range {v1 .. v9}, Lcom/discord/utilities/analytics/AnalyticsTracker;->fileUploadAlertViewed(Lcom/discord/utilities/rest/FileUploadAlertType;IIIZZZLjava/util/Map;)V

    return-void
.end method
