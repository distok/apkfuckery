.class public final Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;
.super Ljava/lang/Object;
.source "StoreRtcConnection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreRtcConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RtcConnectionMetadata"
.end annotation


# instance fields
.field private final channelId:Ljava/lang/Long;

.field private final guildId:Ljava/lang/Long;

.field private final mediaSessionId:Ljava/lang/String;

.field private final rtcConnectionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    const-string v0, "rtcConnectionId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    iput-object p4, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;ILjava/lang/Object;)Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component4()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;
    .locals 1

    const-string v0, "rtcConnectionId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    iget-object p1, p1, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getMediaSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getRtcConnectionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "RtcConnectionMetadata(rtcConnectionId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->rtcConnectionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mediaSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->mediaSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->channelId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->guildId:Ljava/lang/Long;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->y(Ljava/lang/StringBuilder;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
