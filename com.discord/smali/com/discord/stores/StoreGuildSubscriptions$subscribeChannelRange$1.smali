.class public final Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;
.super Lx/m/c/k;
.source "StoreGuildSubscriptions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGuildSubscriptions;->subscribeChannelRange(JJLkotlin/ranges/IntRange;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $guildId:J

.field public final synthetic $range:Lkotlin/ranges/IntRange;

.field public final synthetic this$0:Lcom/discord/stores/StoreGuildSubscriptions;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildSubscriptions;Lkotlin/ranges/IntRange;JJ)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->this$0:Lcom/discord/stores/StoreGuildSubscriptions;

    iput-object p2, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->$range:Lkotlin/ranges/IntRange;

    iput-wide p3, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->$guildId:J

    iput-wide p5, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->$channelId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 11

    sget-object v0, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;->INSTANCE:Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;

    iget-object v1, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->$range:Lkotlin/ranges/IntRange;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;->computeRanges$default(Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;Lkotlin/ranges/IntRange;IILjava/lang/Object;)Ljava/util/List;

    move-result-object v10

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->this$0:Lcom/discord/stores/StoreGuildSubscriptions;

    invoke-static {v0}, Lcom/discord/stores/StoreGuildSubscriptions;->access$getSubscriptionsManager$p(Lcom/discord/stores/StoreGuildSubscriptions;)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;

    move-result-object v5

    iget-wide v6, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->$guildId:J

    iget-wide v8, p0, Lcom/discord/stores/StoreGuildSubscriptions$subscribeChannelRange$1;->$channelId:J

    invoke-virtual/range {v5 .. v10}, Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;->subscribeChannel(JJLjava/util/List;)V

    return-void
.end method
