.class public final Lcom/discord/stores/ReadyPayloadUtils;
.super Ljava/lang/Object;
.source "ReadyPayloadUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;,
        Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;,
        Lcom/discord/stores/ReadyPayloadUtils$GuildCache;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/ReadyPayloadUtils;

.field private static final cache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/ReadyPayloadUtils$GuildCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/ReadyPayloadUtils;

    invoke-direct {v0}, Lcom/discord/stores/ReadyPayloadUtils;-><init>()V

    sput-object v0, Lcom/discord/stores/ReadyPayloadUtils;->INSTANCE:Lcom/discord/stores/ReadyPayloadUtils;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/discord/stores/ReadyPayloadUtils;->cache:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic hydrateGuild$default(Lcom/discord/stores/ReadyPayloadUtils;Lcom/discord/models/domain/ModelGuild;Ljava/util/List;Ljava/util/Map;ILjava/lang/Object;)Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    move-object p3, v0

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/ReadyPayloadUtils;->hydrateGuild(Lcom/discord/models/domain/ModelGuild;Ljava/util/List;Ljava/util/Map;)Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getCache()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/ReadyPayloadUtils$GuildCache;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/ReadyPayloadUtils;->cache:Ljava/util/HashMap;

    return-object v0
.end method

.method public final hydrateGuild(Lcom/discord/models/domain/ModelGuild;Ljava/util/List;Ljava/util/Map;)Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember;",
            ">;)",
            "Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;"
        }
    .end annotation

    const-string v1, "guild"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getGuildHashes()Lcom/discord/models/domain/ModelGuildHash;

    move-result-object v1

    instance-of v2, v1, Lcom/discord/models/domain/ModelGuildHash;

    if-eqz v2, :cond_3

    new-instance v2, Lkotlin/Triple;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildHash;->getMetadataHash()Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->getOmitted()Z

    move-result v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildHash;->getChannelsHash()Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->getOmitted()Z

    move-result v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildHash;->getRolesHash()Lcom/discord/models/domain/ModelGuildHash$GuildHash;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildHash$GuildHash;->getOmitted()Z

    move-result v4

    :cond_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v2, v3, v5, v1}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    new-instance v2, Lkotlin/Triple;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-direct {v2, v1, v1, v1}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    invoke-virtual {v2}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->isUnavailable()Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v1, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;

    invoke-direct {v1, p1}, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    return-object v1

    :cond_4
    sget-object v4, Lcom/discord/stores/ReadyPayloadUtils;->cache:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/stores/ReadyPayloadUtils$GuildCache;

    if-nez v1, :cond_5

    if-nez v3, :cond_5

    if-eqz v2, :cond_6

    :cond_5
    if-nez v4, :cond_6

    sget-object v0, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Error;->INSTANCE:Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Error;

    return-object v0

    :cond_6
    const/4 v5, 0x0

    if-eqz v4, :cond_8

    if-eqz v1, :cond_7

    move-object v6, v4

    goto :goto_3

    :cond_7
    move-object v6, v5

    :goto_3
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/discord/stores/ReadyPayloadUtils$GuildCache;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v6

    goto :goto_4

    :cond_8
    move-object v6, v5

    :goto_4
    if-eqz v4, :cond_a

    if-eqz v1, :cond_9

    move-object v1, v4

    goto :goto_5

    :cond_9
    move-object v1, v5

    :goto_5
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/discord/stores/ReadyPayloadUtils$GuildCache;->getEmojis()Ljava/util/Collection;

    move-result-object v1

    move-object v7, v1

    goto :goto_6

    :cond_a
    move-object v7, v5

    :goto_6
    if-eqz v4, :cond_c

    if-eqz v3, :cond_b

    move-object v1, v4

    goto :goto_7

    :cond_b
    move-object v1, v5

    :goto_7
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/discord/stores/ReadyPayloadUtils$GuildCache;->getChannels()Ljava/util/Collection;

    move-result-object v1

    move-object v3, v1

    goto :goto_8

    :cond_c
    move-object v3, v5

    :goto_8
    if-eqz v4, :cond_e

    if-eqz v2, :cond_d

    goto :goto_9

    :cond_d
    move-object v4, v5

    :goto_9
    if-eqz v4, :cond_e

    invoke-virtual {v4}, Lcom/discord/stores/ReadyPayloadUtils$GuildCache;->getRoles()Ljava/util/Collection;

    move-result-object v1

    move-object v4, v1

    goto :goto_a

    :cond_e
    move-object v4, v5

    :goto_a
    move-object v0, p1

    move-object v1, v6

    move-object v2, v7

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/discord/models/domain/ModelGuild;->hydrate(Lcom/discord/models/domain/ModelGuild;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;Ljava/util/Map;)Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    const-string v1, "guild.hydrate(\n         \u2026        members\n        )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;

    invoke-direct {v1, v0}, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    return-object v1
.end method

.method public final hydrateReadyPayload(Lcom/discord/models/domain/ModelPayload;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreEmojiCustom;)Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload_"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuilds"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeChannels"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeEmojiCustom"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/discord/stores/ReadyPayloadUtils;->hydrateUsers(Lcom/discord/models/domain/ModelPayload;)Lcom/discord/models/domain/ModelPayload;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/stores/StoreGuilds;->getRoles()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/stores/StoreGuilds;->getGuilds()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    goto :goto_1

    :cond_0
    move-object v3, v2

    :goto_1
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v4

    invoke-virtual {p3, v4, v5}, Lcom/discord/stores/StoreChannels;->getChannelsForGuild$app_productionDiscordExternalRelease(J)Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    goto :goto_2

    :cond_1
    move-object v4, v2

    :goto_2
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v5

    invoke-virtual {p4, v5, v6}, Lcom/discord/stores/StoreEmojiCustom;->getForGuildBlocking(J)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    :cond_2
    sget-object v5, Lcom/discord/stores/ReadyPayloadUtils;->cache:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    new-instance v7, Lcom/discord/stores/ReadyPayloadUtils$GuildCache;

    invoke-direct {v7, v1, v2, v4, v3}, Lcom/discord/stores/ReadyPayloadUtils$GuildCache;-><init>(Lcom/discord/models/domain/ModelGuild;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuilds()Ljava/util/List;

    move-result-object p3

    const-string p4, "payload.guilds"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p4, 0x0

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_3
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v1, p4, 0x1

    if-ltz p4, :cond_9

    check-cast v0, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuildPresences()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-interface {v3, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    goto :goto_4

    :cond_4
    move-object v3, v2

    :goto_4
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuildMembers()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-interface {v4, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/util/List;

    if-eqz p4, :cond_6

    const/16 v4, 0xa

    invoke-static {p4, v4}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-static {v4}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v4

    const/16 v5, 0x10

    if-ge v4, v5, :cond_5

    const/16 v4, 0x10

    :cond_5
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5, v4}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_5
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/discord/models/domain/ModelGuildMember;

    const-string v7, "member"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelGuildMember;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_6
    move-object v5, v2

    :cond_7
    sget-object p4, Lcom/discord/stores/ReadyPayloadUtils;->INSTANCE:Lcom/discord/stores/ReadyPayloadUtils;

    const-string v4, "guild"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4, v0, v3, v5}, Lcom/discord/stores/ReadyPayloadUtils;->hydrateGuild(Lcom/discord/models/domain/ModelGuild;Ljava/util/List;Ljava/util/Map;)Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;

    move-result-object p4

    instance-of v0, p4, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;

    if-eqz v0, :cond_8

    check-cast p4, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;

    invoke-virtual {p4}, Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move p4, v1

    goto :goto_3

    :cond_8
    sget-object p1, Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Error;->INSTANCE:Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Error;

    return-object p1

    :cond_9
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    throw v2

    :cond_a
    new-instance p3, Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;

    invoke-virtual {p1, p2}, Lcom/discord/models/domain/ModelPayload;->withGuilds(Ljava/util/List;)Lcom/discord/models/domain/ModelPayload;

    move-result-object p1

    const-string p2, "payload.withGuilds(guilds)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p3, p1}, Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;-><init>(Lcom/discord/models/domain/ModelPayload;)V

    return-object p3
.end method

.method public final hydrateUsers(Lcom/discord/models/domain/ModelPayload;)Lcom/discord/models/domain/ModelPayload;
    .locals 12
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getUsers()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_d

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v2

    const/16 v3, 0x10

    if-ge v2, v3, :cond_0

    const/16 v2, 0x10

    :cond_0
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/discord/models/domain/ModelUser;

    const-string/jumbo v5, "user"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getRelationships()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelUserRelationship;

    invoke-virtual {v5, v3}, Lcom/discord/models/domain/ModelUserRelationship;->hydrate(Ljava/util/Map;)Lcom/discord/models/domain/ModelUserRelationship;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v7, v4

    goto :goto_2

    :cond_3
    move-object v7, v2

    :goto_2
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getPrivateChannels()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v5, v3}, Lcom/discord/models/domain/ModelChannel;->hydrateUsers(Ljava/util/Map;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    move-object v8, v4

    goto :goto_4

    :cond_5
    move-object v8, v2

    :goto_4
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuildPresences()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_8

    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    const-string v6, "guildPresences"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-static {v5, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v9, v3}, Lcom/discord/models/domain/ModelPresence;->hydrate(Ljava/util/Map;)Lcom/discord/models/domain/ModelPresence;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_6
    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_7
    move-object v9, v4

    goto :goto_7

    :cond_8
    move-object v9, v2

    :goto_7
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getPresences()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_a

    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v5, v3}, Lcom/discord/models/domain/ModelPresence;->hydrate(Ljava/util/Map;)Lcom/discord/models/domain/ModelPresence;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_9
    move-object v11, v4

    goto :goto_9

    :cond_a
    move-object v11, v2

    :goto_9
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuildMembers()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_c

    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    const-string v5, "members"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v4, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelGuildMember;

    invoke-virtual {v6, v3}, Lcom/discord/models/domain/ModelGuildMember;->hydrate(Ljava/util/Map;)Lcom/discord/models/domain/ModelGuildMember;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_b
    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_c
    move-object v10, v2

    move-object v6, p1

    invoke-virtual/range {v6 .. v11}, Lcom/discord/models/domain/ModelPayload;->withHydratedUserData(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/discord/models/domain/ModelPayload;

    move-result-object p1

    const-string v0, "payload.withHydratedUser\u2026Members, friendPresences)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_d
    return-object p1
.end method
