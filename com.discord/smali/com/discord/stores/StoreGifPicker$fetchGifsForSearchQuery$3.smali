.class public final Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;
.super Lx/m/c/k;
.source "StoreGifPicker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGifPicker;->fetchGifsForSearchQuery(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/gifpicker/dto/ModelGif;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $query:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreGifPicker;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGifPicker;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;->this$0:Lcom/discord/stores/StoreGifPicker;

    iput-object p2, p0, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;->$query:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;->invoke(Ljava/util/List;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;->this$0:Lcom/discord/stores/StoreGifPicker;

    invoke-static {v0}, Lcom/discord/stores/StoreGifPicker;->access$getDispatcher$p(Lcom/discord/stores/StoreGifPicker;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3$1;-><init>(Lcom/discord/stores/StoreGifPicker$fetchGifsForSearchQuery$3;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
