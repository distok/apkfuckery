.class public final Lcom/discord/stores/StoreNotices$firstUseTimestamp$2;
.super Lx/m/c/k;
.source "StoreNotices.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreNotices;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreNotices;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreNotices;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreNotices$firstUseTimestamp$2;->this$0:Lcom/discord/stores/StoreNotices;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()J
    .locals 6

    new-instance v0, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    iget-object v1, p0, Lcom/discord/stores/StoreNotices$firstUseTimestamp$2;->this$0:Lcom/discord/stores/StoreNotices;

    invoke-virtual {v1}, Lcom/discord/stores/Store;->getPrefsSessionDurable()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "CACHE_KEY_FIRST_USE"

    const-wide/16 v3, -0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreNotices$firstUseTimestamp$2;->this$0:Lcom/discord/stores/StoreNotices;

    invoke-static {v1}, Lcom/discord/stores/StoreNotices;->access$getClock$p(Lcom/discord/stores/StoreNotices;)Lcom/discord/utilities/time/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    iget-object v1, p0, Lcom/discord/stores/StoreNotices$firstUseTimestamp$2;->this$0:Lcom/discord/stores/StoreNotices;

    invoke-virtual {v1}, Lcom/discord/stores/Store;->getPrefsSessionDurable()Landroid/content/SharedPreferences;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreNotices$firstUseTimestamp$2$1;

    invoke-direct {v2, v0}, Lcom/discord/stores/StoreNotices$firstUseTimestamp$2$1;-><init>(Lkotlin/jvm/internal/Ref$LongRef;)V

    invoke-static {v1, v2}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V

    :cond_0
    iget-wide v0, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    return-wide v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreNotices$firstUseTimestamp$2;->invoke()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
