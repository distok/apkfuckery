.class public final Lcom/discord/stores/StoreStreamRtcConnection$Companion;
.super Ljava/lang/Object;
.source "StoreStreamRtcConnection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreStreamRtcConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreStreamRtcConnection$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$recordBreadcrumb(Lcom/discord/stores/StoreStreamRtcConnection$Companion;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStreamRtcConnection$Companion;->recordBreadcrumb(Ljava/lang/String;)V

    return-void
.end method

.method private final recordBreadcrumb(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StoreStreamRtcConnection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "rtcconnection"

    invoke-virtual {v0, p1, v1}, Lcom/discord/app/AppLog;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
