.class public final Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;
.super Ljava/lang/Object;
.source "SlowTtiExperimentManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/SlowTtiExperimentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TtiExperiment"
.end annotation


# instance fields
.field private final bucket:I

.field private final population:I

.field private final revision:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    iput p2, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    iput p3, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;IIIILjava/lang/Object;)Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->copy(III)Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    return v0
.end method

.method public final copy(III)Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;
    .locals 1

    new-instance v0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;-><init>(III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    iget v1, p1, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    iget v1, p1, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    iget p1, p1, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBucket()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    return v0
.end method

.method public final getPopulation()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    return v0
.end method

.method public final getRevision()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "TtiExperiment(bucket="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->bucket:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", population="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->population:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->revision:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
