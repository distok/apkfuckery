.class public final Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1$1;
.super Ljava/lang/Object;
.source "StoreMaskedLinks.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1$1;->this$0:Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/discord/models/domain/ModelUserRelationship;->isType(Ljava/lang/Integer;I)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1$1;->this$0:Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;

    iget-boolean p1, p1, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;->$isLinkUnmasked:Z

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1$1;->call(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
