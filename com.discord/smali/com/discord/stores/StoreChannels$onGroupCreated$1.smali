.class public final Lcom/discord/stores/StoreChannels$onGroupCreated$1;
.super Lx/m/c/k;
.source "StoreChannels.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreChannels;->onGroupCreated(Lcom/discord/models/domain/ModelChannel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic this$0:Lcom/discord/stores/StoreChannels;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreChannels;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannels$onGroupCreated$1;->this$0:Lcom/discord/stores/StoreChannels;

    iput-object p2, p0, Lcom/discord/stores/StoreChannels$onGroupCreated$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreChannels$onGroupCreated$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreChannels$onGroupCreated$1;->this$0:Lcom/discord/stores/StoreChannels;

    iget-object v1, p0, Lcom/discord/stores/StoreChannels$onGroupCreated$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreChannels;->handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V

    return-void
.end method
