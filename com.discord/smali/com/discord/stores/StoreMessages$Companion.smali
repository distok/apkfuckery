.class public final Lcom/discord/stores/StoreMessages$Companion;
.super Ljava/lang/Object;
.source "StoreMessages.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreMessages$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$cancelBackgroundSendingWork(Lcom/discord/stores/StoreMessages$Companion;Landroid/content/Context;)Landroidx/work/Operation;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMessages$Companion;->cancelBackgroundSendingWork(Landroid/content/Context;)Landroidx/work/Operation;

    move-result-object p0

    return-object p0
.end method

.method private final cancelBackgroundSendingWork(Landroid/content/Context;)Landroidx/work/Operation;
    .locals 1

    invoke-static {p1}, Landroidx/work/WorkManager;->getInstance(Landroid/content/Context;)Landroidx/work/WorkManager;

    move-result-object p1

    const-string v0, "BACKGROUND_MESSAGE_SENDING"

    invoke-virtual {p1, v0}, Landroidx/work/WorkManager;->cancelUniqueWork(Ljava/lang/String;)Landroidx/work/Operation;

    move-result-object p1

    const-string v0, "WorkManager.getInstance(\u2026dWorker.UNIQUE_WORK_NAME)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
