.class public final Lcom/discord/stores/StoreApplicationStreaming;
.super Ljava/lang/Object;
.source "StoreApplicationStreaming.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreApplicationStreaming$State;,
        Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;,
        Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;
    }
.end annotation


# instance fields
.field private activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private final rtcConnectionStore:Lcom/discord/stores/StoreRtcConnection;

.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreApplicationStreaming$State;",
            ">;"
        }
    .end annotation
.end field

.field private final storeStream:Lcom/discord/stores/StoreStream;

.field private final streamSpectators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final streamViewerTracker:Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;

.field private final streamsByUser:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ">;"
        }
    .end annotation
.end field

.field private targetStream:Lcom/discord/models/domain/ModelApplicationStream;

.field private final userStore:Lcom/discord/stores/StoreUser;

.field private final voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreRtcConnection;)V
    .locals 1

    const-string/jumbo v0, "storeStream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voiceChannelSelectedStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtcConnectionStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->storeStream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationStreaming;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreApplicationStreaming;->userStore:Lcom/discord/stores/StoreUser;

    iput-object p4, p0, Lcom/discord/stores/StoreApplicationStreaming;->voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iput-object p5, p0, Lcom/discord/stores/StoreApplicationStreaming;->rtcConnectionStore:Lcom/discord/stores/StoreRtcConnection;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamSpectators:Ljava/util/HashMap;

    new-instance p1, Lcom/discord/stores/StoreApplicationStreaming$State;

    sget-object p2, Lx/h/m;->d:Lx/h/m;

    const/4 p3, 0x0

    invoke-direct {p1, p2, p2, p3}, Lcom/discord/stores/StoreApplicationStreaming$State;-><init>(Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->stateSubject:Lrx/subjects/BehaviorSubject;

    new-instance p1, Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;

    invoke-direct {p1}, Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamViewerTracker:Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;

    return-void
.end method

.method public static final synthetic access$getRtcConnectionStore$p(Lcom/discord/stores/StoreApplicationStreaming;)Lcom/discord/stores/StoreRtcConnection;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationStreaming;->rtcConnectionStore:Lcom/discord/stores/StoreRtcConnection;

    return-object p0
.end method

.method public static final synthetic access$getStoreStream$p(Lcom/discord/stores/StoreApplicationStreaming;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationStreaming;->storeStream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$getUserStore$p(Lcom/discord/stores/StoreApplicationStreaming;)Lcom/discord/stores/StoreUser;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationStreaming;->userStore:Lcom/discord/stores/StoreUser;

    return-object p0
.end method

.method public static final synthetic access$stopStreamInternal(Lcom/discord/stores/StoreApplicationStreaming;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->stopStreamInternal(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic createStream$default(Lcom/discord/stores/StoreApplicationStreaming;JLjava/lang/Long;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreApplicationStreaming;->createStream(JLjava/lang/Long;Ljava/lang/String;)V

    return-void
.end method

.method private final handleStreamCreateOrUpdate(Ljava/lang/String;ZLjava/util/List;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz p2, :cond_0

    sget-object p2, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->PAUSED:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->ACTIVE:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    :goto_0
    sget-object v1, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v1, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;-><init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationStreaming;->updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    iget-object p2, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamSpectators:Ljava/util/HashMap;

    invoke-interface {p2, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamViewerTracker:Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    invoke-virtual {p2, p1, p3}, Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;->onStreamUpdated(Ljava/lang/String;I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    return-void
.end method

.method public static synthetic handleVoiceStateUpdate$default(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/models/domain/ModelVoice$State;JILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelVoice$State;->getGuildId()J

    move-result-wide p2

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/stores/StoreApplicationStreaming;->handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;J)V

    return-void
.end method

.method private final isScreenSharing(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Z
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v0

    iget-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->userStore:Lcom/discord/stores/StoreUser;

    invoke-virtual {p1}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final stopStreamInternal(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->storeStream:Lcom/discord/stores/StoreStream;

    new-instance v1, Lcom/discord/models/domain/StreamDelete;

    sget-object v2, Lcom/discord/models/domain/StreamDelete$Reason;->USER_REQUESTED:Lcom/discord/models/domain/StreamDelete$Reason;

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lcom/discord/models/domain/StreamDelete;-><init>(Ljava/lang/String;Lcom/discord/models/domain/StreamDelete$Reason;Z)V

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/discord/stores/StoreStream;->handleStreamDelete(Lcom/discord/models/domain/StreamDelete;Z)V

    return-void
.end method

.method private final updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationStreaming;->isScreenSharing(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Z

    move-result v0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->isScreenSharing(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream;->handleIsScreenSharingChanged(Z)V

    :cond_0
    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    return-void
.end method


# virtual methods
.method public final createStream(JLjava/lang/Long;Ljava/lang/String;)V
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v7, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;

    move-object v1, v7

    move-object v2, p0

    move-wide v3, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StoreApplicationStreaming$createStream$1;-><init>(Lcom/discord/stores/StoreApplicationStreaming;JLjava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final getActiveApplicationStream$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    return-object v0
.end method

.method public final getActiveStream()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationStreaming;->getState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreApplicationStreaming$getActiveStream$1;->INSTANCE:Lcom/discord/stores/StoreApplicationStreaming$getActiveStream$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "getState()\n      .map { \u2026activeApplicationStream }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getMaxViewersForStream(JJLjava/lang/Long;)Ljava/lang/Integer;
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p5, :cond_0

    new-instance v7, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    move-object v0, v7

    move-wide v3, p3

    move-wide v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;-><init>(JJJ)V

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/discord/models/domain/ModelApplicationStream$CallStream;

    invoke-direct {v7, p3, p4, p1, p2}, Lcom/discord/models/domain/ModelApplicationStream$CallStream;-><init>(JJ)V

    :goto_0
    iget-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamViewerTracker:Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;->getMaxViewers(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public final getState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreApplicationStreaming$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->stateSubject:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v1, "stateSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getStreamsByUser()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationStreaming;->getState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreApplicationStreaming$getStreamsByUser$1;->INSTANCE:Lcom/discord/stores/StoreApplicationStreaming$getStreamsByUser$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "getState()\n          .ma\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getStreamsForGuild(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationStreaming;->getStreamsByUser()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreApplicationStreaming$getStreamsForGuild$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreApplicationStreaming$getStreamsForGuild$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "getStreamsByUser()\n     \u2026 }.distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getStreamsForUser(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationStreaming;->getState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreApplicationStreaming$getStreamsForUser$1;->INSTANCE:Lcom/discord/stores/StoreApplicationStreaming$getStreamsForUser$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreApplicationStreaming$getStreamsForUser$2;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreApplicationStreaming$getStreamsForUser$2;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "getState()\n        .map \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamViewerTracker:Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;->clear()V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getGuilds()Ljava/util/List;

    move-result-object p1

    const-string v0, "payload.guilds"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuild;

    const-string v1, "guild"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getVoiceStates()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelVoice$State;

    const-string/jumbo v3, "voiceState"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    invoke-virtual {p0, v2, v3, v4}, Lcom/discord/stores/StoreApplicationStreaming;->handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;J)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    return-void
.end method

.method public final handleStreamCreate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamCreate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamViewerTracker:Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;->remove(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getStreamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getPaused()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getViewerIds()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamCreateOrUpdate(Ljava/lang/String;ZLjava/util/List;)V

    return-void
.end method

.method public final handleStreamCreateRequest(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    sget-object v1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->CONNECTING:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    sget-object v2, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v2, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;-><init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationStreaming;->updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    return-void
.end method

.method public final handleStreamDelete(Lcom/discord/models/domain/StreamDelete;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamDelete"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamSpectators:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamDelete;->getStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamDelete;->getReason()Lcom/discord/models/domain/StreamDelete$Reason;

    move-result-object v0

    sget-object v1, Lcom/discord/models/domain/StreamDelete$Reason;->STREAM_FULL:Lcom/discord/models/domain/StreamDelete$Reason;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    sget-object v1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->DENIED_FULL:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    sget-object v3, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamDelete;->getStreamKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;-><init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationStreaming;->updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    iput-boolean v2, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->targetStream:Lcom/discord/models/domain/ModelApplicationStream;

    :goto_0
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    :goto_1
    invoke-virtual {p1}, Lcom/discord/models/domain/StreamDelete;->getStreamKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamDelete;->getUnavailable()Z

    move-result v0

    const/4 v3, 0x2

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz p1, :cond_5

    sget-object v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->RECONNECTING:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    invoke-static {p1, v0, v1, v3, v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->copy$default(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;ILjava/lang/Object;)Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/discord/models/domain/StreamDelete;->getReason()Lcom/discord/models/domain/StreamDelete$Reason;

    move-result-object p1

    sget-object v0, Lcom/discord/models/domain/StreamDelete$Reason;->USER_REQUESTED:Lcom/discord/models/domain/StreamDelete$Reason;

    if-ne p1, v0, :cond_4

    goto :goto_2

    :cond_4
    iget-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz p1, :cond_5

    sget-object v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->ENDED:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    invoke-static {p1, v0, v1, v3, v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->copy$default(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;ILjava/lang/Object;)Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-direct {p0, v1}, Lcom/discord/stores/StoreApplicationStreaming;->updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    iput-boolean v2, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    :cond_6
    return-void
.end method

.method public final handleStreamTargeted(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-static {p1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->isStreamActive()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->voiceChannelSelectedStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->getSelectedVoiceChannelId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStream;->streamWatch(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming;->targetStream:Lcom/discord/models/domain/ModelApplicationStream;

    goto :goto_1

    :cond_2
    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->targetStream:Lcom/discord/models/domain/ModelApplicationStream;

    :goto_1
    return-void
.end method

.method public final handleStreamUpdate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamUpdate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getStreamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getPaused()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getViewerIds()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamCreateOrUpdate(Ljava/lang/String;ZLjava/util/List;)V

    return-void
.end method

.method public final handleStreamWatch(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    sget-object v1, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->CONNECTING:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    sget-object v2, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v2, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;-><init>(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationStreaming;->updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    return-void
.end method

.method public final handleVoiceChannelSelected(J)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->targetStream:Lcom/discord/models/domain/ModelApplicationStream;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->storeStream:Lcom/discord/stores/StoreStream;

    iget-object p2, p0, Lcom/discord/stores/StoreApplicationStreaming;->targetStream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-static {p2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreStream;->streamWatch(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming;->targetStream:Lcom/discord/models/domain/ModelApplicationStream;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0, v1}, Lcom/discord/stores/StoreApplicationStreaming;->updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    iput-object v1, p0, Lcom/discord/stores/StoreApplicationStreaming;->targetStream:Lcom/discord/models/domain/ModelApplicationStream;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    :cond_2
    :goto_0
    return-void
.end method

.method public final handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;)V
    .locals 6
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-wide/16 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/stores/StoreApplicationStreaming;->handleVoiceStateUpdate$default(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/models/domain/ModelVoice$State;JILjava/lang/Object;)V

    return-void
.end method

.method public final handleVoiceStateUpdate(Lcom/discord/models/domain/ModelVoice$State;J)V
    .locals 16
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    move-object/from16 v0, p0

    const-string/jumbo v1, "voiceState"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelVoice$State;->getChannelId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelVoice$State;->getUserId()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelVoice$State;->isSelfStream()Z

    move-result v12

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v13, 0x1

    cmp-long v6, p2, v3

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v9, v7, v3

    if-eqz v9, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v12, :cond_2

    if-eqz v6, :cond_2

    if-eqz v3, :cond_2

    iget-object v14, v0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    new-instance v8, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v3, v8

    move-wide/from16 v4, p2

    move-object v1, v8

    move-wide v8, v10

    invoke-direct/range {v3 .. v9}, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;-><init>(JJJ)V

    invoke-interface {v14, v15, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v13, v0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    goto :goto_3

    :cond_2
    if-eqz v12, :cond_3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lcom/discord/models/domain/ModelApplicationStream$CallStream;

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v5, v6, v7, v10, v11}, Lcom/discord/models/domain/ModelApplicationStream$CallStream;-><init>(JJ)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v13, v0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    goto :goto_3

    :cond_3
    iget-object v1, v0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    iget-boolean v3, v0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    if-nez v3, :cond_5

    if-eqz v1, :cond_6

    :cond_5
    const/4 v5, 0x1

    :cond_6
    iput-boolean v5, v0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    :goto_3
    iget-object v1, v0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz v1, :cond_8

    iget-object v3, v0, Lcom/discord/stores/StoreApplicationStreaming;->userStore:Lcom/discord/stores/StoreUser;

    invoke-virtual {v3}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    cmp-long v5, v10, v3

    if-nez v5, :cond_8

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelVoice$State;->getChannelId()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_7

    goto :goto_4

    :cond_7
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v2, v3, v5

    if-eqz v2, :cond_8

    :goto_4
    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/discord/stores/StoreApplicationStreaming;->updateActiveApplicationStream(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    iput-boolean v13, v0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    :cond_8
    iget-object v2, v0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelApplicationStream;

    if-eqz v12, :cond_9

    if-eqz v2, :cond_9

    if-eqz v1, :cond_9

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v1

    sget-object v3, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->ENDED:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    if-ne v1, v3, :cond_9

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreApplicationStreaming;->handleStreamTargeted(Ljava/lang/String;)V

    :cond_9
    return-void
.end method

.method public final isScreenSharing()Z
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreApplicationStreaming;->isScreenSharing(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Z

    move-result v0

    return v0
.end method

.method public final isUserStreaming(J)Z
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onDispatchEnded()V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->stateSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreApplicationStreaming$State;

    new-instance v2, Ljava/util/HashMap;

    iget-object v3, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamsByUser:Ljava/util/HashMap;

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    new-instance v3, Ljava/util/HashMap;

    iget-object v4, p0, Lcom/discord/stores/StoreApplicationStreaming;->streamSpectators:Ljava/util/HashMap;

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object v4, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/stores/StoreApplicationStreaming$State;-><init>(Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->isDirty:Z

    return-void
.end method

.method public final setActiveApplicationStream$app_productionDiscordExternalRelease(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreaming;->activeApplicationStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    return-void
.end method

.method public final stopStream(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreApplicationStreaming$stopStream$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreApplicationStreaming$stopStream$1;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final targetStream(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreaming;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreApplicationStreaming$targetStream$1;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
