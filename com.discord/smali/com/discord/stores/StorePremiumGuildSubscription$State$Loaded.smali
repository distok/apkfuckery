.class public final Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;
.super Lcom/discord/stores/StorePremiumGuildSubscription$State;
.source "StorePremiumGuildSubscription.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StorePremiumGuildSubscription$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final premiumGuildSubscriptionSlotMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;)V"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionSlotMap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Ljava/util/Map;ILjava/lang/Object;)Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->copy(Ljava/util/Map;)Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Ljava/util/Map;)Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;)",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionSlotMap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-direct {v0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    iget-object p1, p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(premiumGuildSubscriptionSlotMap="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->premiumGuildSubscriptionSlotMap:Ljava/util/Map;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->B(Ljava/lang/StringBuilder;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
