.class public final Lcom/discord/stores/StoreEntitlements;
.super Ljava/lang/Object;
.source "StoreEntitlements.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreEntitlements$State;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private entitlementMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;>;"
        }
    .end annotation
.end field

.field private giftEntitlementMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;>;"
        }
    .end annotation
.end field

.field private isDirty:Z

.field private state:Lcom/discord/stores/StoreEntitlements$State;

.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreEntitlements$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreEntitlements;->dispatcher:Lcom/discord/stores/Dispatcher;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/stores/StoreEntitlements;->giftEntitlementMap:Ljava/util/Map;

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreEntitlements;->entitlementMap:Ljava/util/Map;

    sget-object p1, Lcom/discord/stores/StoreEntitlements$State$Loading;->INSTANCE:Lcom/discord/stores/StoreEntitlements$State$Loading;

    iput-object p1, p0, Lcom/discord/stores/StoreEntitlements;->state:Lcom/discord/stores/StoreEntitlements$State;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreEntitlements;->stateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method


# virtual methods
.method public final fetchMyEntitlementsForApplication(J)V
    .locals 13

    iget-object v0, p0, Lcom/discord/stores/StoreEntitlements;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreEntitlements$fetchMyEntitlementsForApplication$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreEntitlements$fetchMyEntitlementsForApplication$1;-><init>(Lcom/discord/stores/StoreEntitlements;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->getMyEntitlements(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreEntitlements;

    new-instance v8, Lcom/discord/stores/StoreEntitlements$fetchMyEntitlementsForApplication$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreEntitlements$fetchMyEntitlementsForApplication$2;-><init>(Lcom/discord/stores/StoreEntitlements;)V

    new-instance v10, Lcom/discord/stores/StoreEntitlements$fetchMyEntitlementsForApplication$3;

    invoke-direct {v10, p0, p1, p2}, Lcom/discord/stores/StoreEntitlements$fetchMyEntitlementsForApplication$3;-><init>(Lcom/discord/stores/StoreEntitlements;J)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final fetchMyGiftEntitlements()V
    .locals 13

    iget-object v0, p0, Lcom/discord/stores/StoreEntitlements;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreEntitlements$fetchMyGiftEntitlements$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreEntitlements$fetchMyGiftEntitlements$1;-><init>(Lcom/discord/stores/StoreEntitlements;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getGifts()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/stores/StoreEntitlements;

    new-instance v8, Lcom/discord/stores/StoreEntitlements$fetchMyGiftEntitlements$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreEntitlements$fetchMyGiftEntitlements$2;-><init>(Lcom/discord/stores/StoreEntitlements;)V

    new-instance v10, Lcom/discord/stores/StoreEntitlements$fetchMyGiftEntitlements$3;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreEntitlements$fetchMyGiftEntitlements$3;-><init>(Lcom/discord/stores/StoreEntitlements;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getDispatcher()Lcom/discord/stores/Dispatcher;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreEntitlements;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object v0
.end method

.method public final getEntitlementState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreEntitlements$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreEntitlements;->stateSubject:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v1, "stateSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final handleFetchEntitlementsSuccess(JLjava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;)V"
        }
    .end annotation

    const-string v0, "entitlements"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreEntitlements;->entitlementMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    iget-object p2, p0, Lcom/discord/stores/StoreEntitlements;->giftEntitlementMap:Ljava/util/Map;

    iget-object p3, p0, Lcom/discord/stores/StoreEntitlements;->entitlementMap:Ljava/util/Map;

    invoke-direct {p1, p2, p3}, Lcom/discord/stores/StoreEntitlements$State$Loaded;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    iput-object p1, p0, Lcom/discord/stores/StoreEntitlements;->state:Lcom/discord/stores/StoreEntitlements$State;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreEntitlements;->isDirty:Z

    return-void
.end method

.method public final handleFetchError()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreEntitlements$State$Failure;->INSTANCE:Lcom/discord/stores/StoreEntitlements$State$Failure;

    iput-object v0, p0, Lcom/discord/stores/StoreEntitlements;->state:Lcom/discord/stores/StoreEntitlements$State;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreEntitlements;->isDirty:Z

    return-void
.end method

.method public final handleFetchGiftsSuccess(Ljava/util/List;)V
    .locals 5
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;)V"
        }
    .end annotation

    const-string v0, "giftEntitlements"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelEntitlement;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelEntitlement;->getSkuId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelEntitlement;->getSkuId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iput-object v0, p0, Lcom/discord/stores/StoreEntitlements;->giftEntitlementMap:Ljava/util/Map;

    new-instance p1, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    iget-object v1, p0, Lcom/discord/stores/StoreEntitlements;->entitlementMap:Ljava/util/Map;

    invoke-direct {p1, v0, v1}, Lcom/discord/stores/StoreEntitlements$State$Loaded;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    iput-object p1, p0, Lcom/discord/stores/StoreEntitlements;->state:Lcom/discord/stores/StoreEntitlements$State;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreEntitlements;->isDirty:Z

    return-void
.end method

.method public final handleFetchingState()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreEntitlements$State$Loading;->INSTANCE:Lcom/discord/stores/StoreEntitlements$State$Loading;

    iput-object v0, p0, Lcom/discord/stores/StoreEntitlements;->state:Lcom/discord/stores/StoreEntitlements$State;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreEntitlements;->isDirty:Z

    return-void
.end method

.method public onDispatchEnded()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreEntitlements;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreEntitlements;->stateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreEntitlements;->state:Lcom/discord/stores/StoreEntitlements$State;

    invoke-virtual {v1}, Lcom/discord/stores/StoreEntitlements$State;->deepCopy()Lcom/discord/stores/StoreEntitlements$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreEntitlements;->isDirty:Z

    :cond_0
    return-void
.end method
