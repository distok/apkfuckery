.class public final Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;
.super Lx/m/c/k;
.source "StoreStickers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->invoke(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $ownedPacks:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iput-object p2, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->$ownedPacks:Ljava/util/List;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->$ownedPacks:Ljava/util/List;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-static {v3}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v3

    const/16 v4, 0x10

    if-ge v3, v4, :cond_0

    const/16 v3, 0x10

    :cond_0
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getPackId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    invoke-direct {v1, v4}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;-><init>(Ljava/util/Map;)V

    invoke-static {v0, v1}, Lcom/discord/stores/StoreStickers;->access$setOwnedStickerPackState$p(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->$ownedPacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    iget-object v3, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v3, v3, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v3, v3, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v3}, Lcom/discord/stores/StoreStickers;->access$getPurchasingStickerPacks$p(Lcom/discord/stores/StoreStickers;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getPackId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->$ownedPacks:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getPackId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-static {v1}, Lx/h/f;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v1, v1, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v1, v1, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v1}, Lcom/discord/stores/StoreStickers;->access$getFrecencyCache$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/frecency/FrecencyTracker;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v5, v4}, Lcom/discord/utilities/frecency/FrecencyTracker;->getSortedKeys$default(Lcom/discord/utilities/frecency/FrecencyTracker;JILjava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v4, v4, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v4, v4, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v4}, Lcom/discord/stores/StoreStickers;->access$getStickersSnapshot$p(Lcom/discord/stores/StoreStickers;)Ljava/util/Map;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/sticker/dto/ModelSticker;

    if-eqz v3, :cond_4

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v3, v3, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v3, v3, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v3}, Lcom/discord/stores/StoreStickers;->access$getFrecency$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/media/MediaFrecencyTracker;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelSticker;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/discord/utilities/frecency/FrecencyTracker;->removeEntry(Ljava/lang/Object;)V

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v0}, Lcom/discord/stores/StoreStickers;->access$getFrecencyCache$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v1, v1, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v1, v1, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v1}, Lcom/discord/stores/StoreStickers;->access$getFrecency$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/media/MediaFrecencyTracker;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreV2;->markChanged()V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;->this$0:Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;

    iget-object v0, v0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2$1;->$ownedPacks:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/sticker/dto/ModelUserStickerPack;

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelUserStickerPack;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreStickers;->handleNewLoadedStickerPacks(Ljava/util/List;)V

    return-void
.end method
