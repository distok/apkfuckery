.class public final Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;
.super Lx/m/c/k;
.source "StoreGooglePlayPurchases.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGooglePlayPurchases;->processPurchases(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $purchases:Ljava/util/List;

.field public final synthetic this$0:Lcom/discord/stores/StoreGooglePlayPurchases;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGooglePlayPurchases;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    iput-object p2, p0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->$purchases:Ljava/util/List;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    iget-object v2, v0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->$purchases:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/discord/stores/StoreGooglePlayPurchases;->access$handlePurchases(Lcom/discord/stores/StoreGooglePlayPurchases;Ljava/util/List;)V

    iget-object v1, v0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-static {v1}, Lcom/discord/stores/StoreGooglePlayPurchases;->access$getQueryStateSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/BehaviorSubject;

    move-result-object v2

    const-string v1, "queryStateSubject"

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v8

    iget-object v1, v0, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    new-instance v14, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1$1;

    invoke-direct {v14, v0}, Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases$processPurchases$1;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
