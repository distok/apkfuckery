.class public final Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;
.super Ljava/lang/Object;
.source "StoreChannelFollowerStats.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreChannelFollowerStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChannelFollowerStatData"
.end annotation


# instance fields
.field private final data:Lcom/discord/models/domain/ModelChannelFollowerStats;

.field private final fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)V
    .locals 1

    const-string v0, "fetchState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    iput-object p2, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 11

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    new-instance p2, Lcom/discord/models/domain/ModelChannelFollowerStats;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7f

    const/4 v10, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v10}, Lcom/discord/models/domain/ModelChannelFollowerStats;-><init>(JLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;-><init>(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;ILjava/lang/Object;)Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->copy(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreChannelFollowerStats$FetchState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelChannelFollowerStats;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;
    .locals 1

    const-string v0, "fetchState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;-><init>(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    iget-object v1, p1, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    iget-object p1, p1, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getData()Lcom/discord/models/domain/ModelChannelFollowerStats;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    return-object v0
.end method

.method public final getFetchState()Lcom/discord/stores/StoreChannelFollowerStats$FetchState;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannelFollowerStats;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ChannelFollowerStatData(fetchState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->fetchState:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->data:Lcom/discord/models/domain/ModelChannelFollowerStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
