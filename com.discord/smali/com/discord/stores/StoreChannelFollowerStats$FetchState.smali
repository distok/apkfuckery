.class public final enum Lcom/discord/stores/StoreChannelFollowerStats$FetchState;
.super Ljava/lang/Enum;
.source "StoreChannelFollowerStats.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreChannelFollowerStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FetchState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/stores/StoreChannelFollowerStats$FetchState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

.field public static final enum FAILED:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

.field public static final enum FETCHING:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

.field public static final enum SUCCEEDED:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    new-instance v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    const-string v2, "FETCHING"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->FETCHING:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    const-string v2, "FAILED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->FAILED:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    const-string v2, "SUCCEEDED"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->SUCCEEDED:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->$VALUES:[Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/stores/StoreChannelFollowerStats$FetchState;
    .locals 1

    const-class v0, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    return-object p0
.end method

.method public static values()[Lcom/discord/stores/StoreChannelFollowerStats$FetchState;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->$VALUES:[Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    invoke-virtual {v0}, [Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    return-object v0
.end method
