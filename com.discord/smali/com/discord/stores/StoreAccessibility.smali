.class public final Lcom/discord/stores/StoreAccessibility;
.super Lcom/discord/stores/StoreV2;
.source "StoreAccessibility.kt"


# instance fields
.field private accessibilityFeatures:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;",
            ">;"
        }
    .end annotation
.end field

.field private accessibilityFeaturesSnapshot:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;",
            ">;"
        }
    .end annotation
.end field

.field private final accessibilityMonitor:Lcom/discord/utilities/accessibility/AccessibilityMonitor;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private fontScaledDown:Z

.field private fontScaledUp:Z

.field private isDetectionAllowed:Z

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private final userSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/accessibility/AccessibilityMonitor;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userSettings"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilityMonitor"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreAccessibility;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreAccessibility;->userSettings:Lcom/discord/stores/StoreUserSettings;

    iput-object p3, p0, Lcom/discord/stores/StoreAccessibility;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    iput-object p4, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityMonitor:Lcom/discord/utilities/accessibility/AccessibilityMonitor;

    sget-object p1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->NONE:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-static {p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityFeatures:Ljava/util/EnumSet;

    invoke-static {p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p1

    const-string p2, "EnumSet.of(AccessibilityFeatureFlags.NONE)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityFeaturesSnapshot:Ljava/util/EnumSet;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/accessibility/AccessibilityMonitor;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p3

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    sget-object p4, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->Companion:Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;

    invoke-virtual {p4}, Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;->getINSTANCE()Lcom/discord/utilities/accessibility/AccessibilityMonitor;

    move-result-object p4

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreAccessibility;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/accessibility/AccessibilityMonitor;)V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreAccessibility;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreAccessibility;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$updateAccessibilityState(Lcom/discord/stores/StoreAccessibility;Lcom/discord/utilities/accessibility/AccessibilityState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAccessibility;->updateAccessibilityState(Lcom/discord/utilities/accessibility/AccessibilityState;)V

    return-void
.end method

.method private final updateAccessibilityState(Lcom/discord/utilities/accessibility/AccessibilityState;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/utilities/accessibility/AccessibilityState;->getFeatures()Ljava/util/EnumSet;

    move-result-object p1

    invoke-static {p1}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityFeatures:Ljava/util/EnumSet;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final updateDetectionAllowed(Z)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-boolean p1, p0, Lcom/discord/stores/StoreAccessibility;->isDetectionAllowed:Z

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final updateFontScale(I)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const/16 v0, 0x64

    const/4 v1, 0x0

    if-eq p1, v0, :cond_2

    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    if-le p1, v0, :cond_1

    iput-boolean v2, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledUp:Z

    iput-boolean v1, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledDown:Z

    goto :goto_1

    :cond_1
    if-ge p1, v0, :cond_3

    iput-boolean v1, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledUp:Z

    iput-boolean v2, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledDown:Z

    goto :goto_1

    :cond_2
    :goto_0
    iput-boolean v1, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledUp:Z

    iput-boolean v1, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledDown:Z

    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public final getAccessibilityFeatures()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityFeaturesSnapshot:Ljava/util/EnumSet;

    return-object v0
.end method

.method public final handleAccessibilitySettingsUpdated(ZI)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreAccessibility;->updateDetectionAllowed(Z)V

    invoke-direct {p0, p2}, Lcom/discord/stores/StoreAccessibility;->updateFontScale(I)V

    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityMonitor:Lcom/discord/utilities/accessibility/AccessibilityMonitor;

    invoke-virtual {p1}, Lcom/discord/utilities/accessibility/AccessibilityMonitor;->observeAccessibilityState()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreAccessibility;

    new-instance v6, Lcom/discord/stores/StoreAccessibility$init$1;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreAccessibility$init$1;-><init>(Lcom/discord/stores/StoreAccessibility;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final isScreenreaderEnabled()Z
    .locals 2

    invoke-virtual {p0}, Lcom/discord/stores/StoreAccessibility;->getAccessibilityFeatures()Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->SCREENREADER:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final observeAccessibilityFeatures()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/EnumSet<",
            "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAccessibility;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreAccessibility$observeAccessibilityFeatures$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreAccessibility$observeAccessibilityFeatures$1;-><init>(Lcom/discord/stores/StoreAccessibility;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final observeScreenreaderEnabled()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreAccessibility;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreAccessibility$observeScreenreaderEnabled$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreAccessibility$observeScreenreaderEnabled$1;-><init>(Lcom/discord/stores/StoreAccessibility;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public snapshotData()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-super {p0}, Lcom/discord/stores/StoreV2;->snapshotData()V

    iget-object v0, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityFeatures:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/stores/StoreAccessibility;->isDetectionAllowed:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->SCREENREADER:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-boolean v1, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledUp:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->CHAT_FONT_SCALE_INCREASED:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-boolean v1, p0, Lcom/discord/stores/StoreAccessibility;->fontScaledDown:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;->CHAT_FONT_SCALE_DECREASED:Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_2
    const-string v1, "features"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/stores/StoreAccessibility;->accessibilityFeaturesSnapshot:Ljava/util/EnumSet;

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticSuperProperties;

    invoke-virtual {p0}, Lcom/discord/stores/StoreAccessibility;->isScreenreaderEnabled()Z

    move-result v1

    invoke-virtual {p0}, Lcom/discord/stores/StoreAccessibility;->getAccessibilityFeatures()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/analytics/AnalyticSuperProperties;->setAccessibilityProperties(ZLjava/util/EnumSet;)V

    return-void
.end method
