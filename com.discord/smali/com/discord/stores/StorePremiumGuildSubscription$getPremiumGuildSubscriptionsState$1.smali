.class public final Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;
.super Ljava/lang/Object;
.source "StorePremiumGuildSubscription.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState(Ljava/lang/Long;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
        "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->$guildId:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StorePremiumGuildSubscription$State;)Lcom/discord/stores/StorePremiumGuildSubscription$State;
    .locals 4

    instance-of v0, p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->$guildId:Ljava/lang/Long;

    if-nez v0, :cond_0

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object p1

    goto :goto_2

    :cond_0
    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object p1

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->$guildId:Ljava/lang/Long;

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move-object p1, v0

    :goto_2
    new-instance v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-direct {v0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;-><init>(Ljava/util/Map;)V

    move-object p1, v0

    :cond_4
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StorePremiumGuildSubscription$getPremiumGuildSubscriptionsState$1;->call(Lcom/discord/stores/StorePremiumGuildSubscription$State;)Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object p1

    return-object p1
.end method
