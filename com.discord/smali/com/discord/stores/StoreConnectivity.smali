.class public final Lcom/discord/stores/StoreConnectivity;
.super Lcom/discord/stores/StoreV2;
.source "StoreConnectivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreConnectivity$State;,
        Lcom/discord/stores/StoreConnectivity$DelayedState;,
        Lcom/discord/stores/StoreConnectivity$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final Companion:Lcom/discord/stores/StoreConnectivity$Companion;

.field public static final DELAY_EXTENDED:J = 0x2710L

.field public static final DELAY_INTERVAL:J = 0x64L

.field public static final DELAY_NOMINAL:J = 0x3e8L


# instance fields
.field private channelMessagesLoading:Z

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private deviceNetworkState:Lcom/discord/utilities/networking/NetworkMonitor$State;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isReadyPayloadReceived:Z

.field private final state$delegate:Lcom/discord/stores/StoreV2$MarkChangedDelegate;

.field private stateTriggeredDelay:J

.field private stateTriggeredTimeMillis:Ljava/lang/Long;

.field private final stream:Lcom/discord/stores/StoreStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/o;

    const-class v2, Lcom/discord/stores/StoreConnectivity;

    const-string/jumbo v3, "state"

    const-string v4, "getState()Lcom/discord/stores/StoreConnectivity$DelayedState;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/o;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/stores/StoreConnectivity;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/stores/StoreConnectivity$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreConnectivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreConnectivity;->Companion:Lcom/discord/stores/StoreConnectivity$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V
    .locals 3

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreConnectivity;->stream:Lcom/discord/stores/StoreStream;

    iput-object p2, p0, Lcom/discord/stores/StoreConnectivity;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p3, p0, Lcom/discord/stores/StoreConnectivity;->clock:Lcom/discord/utilities/time/Clock;

    new-instance p1, Lcom/discord/stores/StoreV2$MarkChangedDelegate;

    new-instance p2, Lcom/discord/stores/StoreConnectivity$DelayedState;

    sget-object p3, Lcom/discord/stores/StoreConnectivity$State;->CONNECTING:Lcom/discord/stores/StoreConnectivity$State;

    const-wide/16 v0, 0x2710

    invoke-direct {p2, p3, v0, v1}, Lcom/discord/stores/StoreConnectivity$DelayedState;-><init>(Lcom/discord/stores/StoreConnectivity$State;J)V

    const/4 p3, 0x0

    const/4 v2, 0x2

    invoke-direct {p1, p2, p3, v2, p3}, Lcom/discord/stores/StoreV2$MarkChangedDelegate;-><init>(Ljava/lang/Object;Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreConnectivity;->state$delegate:Lcom/discord/stores/StoreV2$MarkChangedDelegate;

    iput-wide v0, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredDelay:J

    sget-object p1, Lcom/discord/utilities/networking/NetworkMonitor$State;->OFFLINE:Lcom/discord/utilities/networking/NetworkMonitor$State;

    iput-object p1, p0, Lcom/discord/stores/StoreConnectivity;->deviceNetworkState:Lcom/discord/utilities/networking/NetworkMonitor$State;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/discord/stores/StoreConnectivity$Companion;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreConnectivity;->Companion:Lcom/discord/stores/StoreConnectivity$Companion;

    return-object v0
.end method

.method public static final synthetic access$getChannelMessagesLoading$p(Lcom/discord/stores/StoreConnectivity;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreConnectivity;->channelMessagesLoading:Z

    return p0
.end method

.method public static final synthetic access$getDeviceNetworkState$p(Lcom/discord/stores/StoreConnectivity;)Lcom/discord/utilities/networking/NetworkMonitor$State;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreConnectivity;->deviceNetworkState:Lcom/discord/utilities/networking/NetworkMonitor$State;

    return-object p0
.end method

.method public static final synthetic access$getState$p(Lcom/discord/stores/StoreConnectivity;)Lcom/discord/stores/StoreConnectivity$DelayedState;
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->getState()Lcom/discord/stores/StoreConnectivity$DelayedState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleChannelMessagesLoading(Lcom/discord/stores/StoreConnectivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreConnectivity;->handleChannelMessagesLoading(Z)V

    return-void
.end method

.method public static final synthetic access$handleDeviceNetworkStateUpdated(Lcom/discord/stores/StoreConnectivity;Lcom/discord/utilities/networking/NetworkMonitor$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreConnectivity;->handleDeviceNetworkStateUpdated(Lcom/discord/utilities/networking/NetworkMonitor$State;)V

    return-void
.end method

.method public static final synthetic access$setChannelMessagesLoading$p(Lcom/discord/stores/StoreConnectivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreConnectivity;->channelMessagesLoading:Z

    return-void
.end method

.method public static final synthetic access$setDeviceNetworkState$p(Lcom/discord/stores/StoreConnectivity;Lcom/discord/utilities/networking/NetworkMonitor$State;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreConnectivity;->deviceNetworkState:Lcom/discord/utilities/networking/NetworkMonitor$State;

    return-void
.end method

.method public static final synthetic access$setState$p(Lcom/discord/stores/StoreConnectivity;Lcom/discord/stores/StoreConnectivity$DelayedState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreConnectivity;->setState(Lcom/discord/stores/StoreConnectivity$DelayedState;)V

    return-void
.end method

.method public static final synthetic access$updateConnectivityState(Lcom/discord/stores/StoreConnectivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->updateConnectivityState()V

    return-void
.end method

.method private final getNextStateDelayInitial(Lcom/discord/stores/StoreConnectivity$DelayedState;Lcom/discord/stores/StoreConnectivity$State;)J
    .locals 4

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    const-wide/16 v0, 0x3e8

    if-eqz p2, :cond_4

    const/4 v2, 0x1

    if-eq p2, v2, :cond_4

    const/4 v3, 0x2

    if-eq p2, v3, :cond_4

    const/4 v3, 0x3

    if-ne p2, v3, :cond_3

    invoke-virtual {p1}, Lcom/discord/stores/StoreConnectivity$DelayedState;->getState()Lcom/discord/stores/StoreConnectivity$State;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreConnectivity$State;->OFFLINE:Lcom/discord/stores/StoreConnectivity$State;

    if-eq p1, p2, :cond_1

    sget-object p2, Lcom/discord/stores/StoreConnectivity$State;->OFFLINE_AIRPLANE_MODE:Lcom/discord/stores/StoreConnectivity$State;

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->getStateActiveMillis()J

    move-result-wide p1

    cmp-long v2, p1, v0

    if-lez v2, :cond_2

    goto :goto_1

    :cond_2
    const-wide/16 v0, 0x2710

    goto :goto_1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_4
    :goto_1
    return-wide v0
.end method

.method private final getState()Lcom/discord/stores/StoreConnectivity$DelayedState;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity;->state$delegate:Lcom/discord/stores/StoreV2$MarkChangedDelegate;

    sget-object v1, Lcom/discord/stores/StoreConnectivity;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/discord/stores/StoreV2$MarkChangedDelegate;->getValue(Lcom/discord/stores/StoreV2;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreConnectivity$DelayedState;

    return-object v0
.end method

.method private final getStateActiveMillis()J
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredTimeMillis:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/stores/StoreConnectivity;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2
.end method

.method private final getStateDelay(J)J
    .locals 3

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->getStateActiveMillis()J

    move-result-wide v0

    sub-long/2addr p1, v0

    long-to-float p1, p1

    const-wide/16 v0, 0x64

    long-to-float p2, v0

    div-float/2addr p1, p2

    invoke-static {p1}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result p1

    int-to-long p1, p1

    mul-long p1, p1, v0

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    :cond_0
    return-wide p1
.end method

.method private final handleChannelMessagesLoading(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreConnectivity$handleChannelMessagesLoading$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreConnectivity$handleChannelMessagesLoading$1;-><init>(Lcom/discord/stores/StoreConnectivity;Z)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleDeviceNetworkStateUpdated(Lcom/discord/utilities/networking/NetworkMonitor$State;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreConnectivity$handleDeviceNetworkStateUpdated$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreConnectivity$handleDeviceNetworkStateUpdated$1;-><init>(Lcom/discord/stores/StoreConnectivity;Lcom/discord/utilities/networking/NetworkMonitor$State;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final setState(Lcom/discord/stores/StoreConnectivity$DelayedState;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity;->state$delegate:Lcom/discord/stores/StoreV2$MarkChangedDelegate;

    sget-object v1, Lcom/discord/stores/StoreConnectivity;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/discord/stores/StoreV2$MarkChangedDelegate;->setValue(Lcom/discord/stores/StoreV2;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method private final updateConnectivityState()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreConnectivity;->deviceNetworkState:Lcom/discord/utilities/networking/NetworkMonitor$State;

    sget-object v1, Lcom/discord/utilities/networking/NetworkMonitor$State;->OFFLINE:Lcom/discord/utilities/networking/NetworkMonitor$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/discord/stores/StoreConnectivity$State;->OFFLINE:Lcom/discord/stores/StoreConnectivity$State;

    goto :goto_1

    :cond_0
    sget-object v1, Lcom/discord/utilities/networking/NetworkMonitor$State;->OFFLINE_AIRPLANE_MODE:Lcom/discord/utilities/networking/NetworkMonitor$State;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/discord/stores/StoreConnectivity$State;->OFFLINE_AIRPLANE_MODE:Lcom/discord/stores/StoreConnectivity$State;

    goto :goto_1

    :cond_1
    iget-boolean v0, p0, Lcom/discord/stores/StoreConnectivity;->isReadyPayloadReceived:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/discord/stores/StoreConnectivity;->channelMessagesLoading:Z

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/discord/stores/StoreConnectivity$State;->ONLINE:Lcom/discord/stores/StoreConnectivity$State;

    goto :goto_1

    :cond_3
    :goto_0
    sget-object v0, Lcom/discord/stores/StoreConnectivity$State;->CONNECTING:Lcom/discord/stores/StoreConnectivity$State;

    :goto_1
    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->getState()Lcom/discord/stores/StoreConnectivity$DelayedState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreConnectivity$DelayedState;->getState()Lcom/discord/stores/StoreConnectivity$State;

    move-result-object v1

    if-ne v1, v0, :cond_4

    iget-object v1, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredTimeMillis:Ljava/lang/Long;

    if-nez v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/discord/stores/StoreConnectivity;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredTimeMillis:Ljava/lang/Long;

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->getState()Lcom/discord/stores/StoreConnectivity$DelayedState;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/discord/stores/StoreConnectivity;->getNextStateDelayInitial(Lcom/discord/stores/StoreConnectivity$DelayedState;Lcom/discord/stores/StoreConnectivity$State;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredDelay:J

    :cond_5
    new-instance v1, Lcom/discord/stores/StoreConnectivity$DelayedState;

    iget-wide v2, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredDelay:J

    invoke-direct {p0, v2, v3}, Lcom/discord/stores/StoreConnectivity;->getStateDelay(J)J

    move-result-wide v2

    invoke-direct {v1, v0, v2, v3}, Lcom/discord/stores/StoreConnectivity$DelayedState;-><init>(Lcom/discord/stores/StoreConnectivity$State;J)V

    invoke-direct {p0, v1}, Lcom/discord/stores/StoreConnectivity;->setState(Lcom/discord/stores/StoreConnectivity$DelayedState;)V

    return-void
.end method


# virtual methods
.method public final handleBackgrounded(Z)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredDelay:J

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->getStateActiveMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredDelay:J

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredTimeMillis:Ljava/lang/Long;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/stores/StoreConnectivity;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreConnectivity;->stateTriggeredTimeMillis:Ljava/lang/Long;

    :cond_1
    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->updateConnectivityState()V

    return-void
.end method

.method public final handleConnected(Z)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/discord/stores/StoreConnectivity;->isReadyPayloadReceived:Z

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->updateConnectivityState()V

    return-void
.end method

.method public final handleConnectionReady(Z)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-boolean p1, p0, Lcom/discord/stores/StoreConnectivity;->isReadyPayloadReceived:Z

    invoke-direct {p0}, Lcom/discord/stores/StoreConnectivity;->updateConnectivityState()V

    return-void
.end method

.method public final init(Lcom/discord/utilities/networking/NetworkMonitor;)V
    .locals 9
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "networkMonitor"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/networking/NetworkMonitor;->observeState()Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreConnectivity;

    new-instance v6, Lcom/discord/stores/StoreConnectivity$init$1;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreConnectivity$init$1;-><init>(Lcom/discord/stores/StoreConnectivity;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreConnectivity;->stream:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getMessagesLoader$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreMessagesLoader;->observeChannelMessagesLoading()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/stores/StoreConnectivity;

    new-instance v6, Lcom/discord/stores/StoreConnectivity$init$2;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreConnectivity$init$2;-><init>(Lcom/discord/stores/StoreConnectivity;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final observeState()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreConnectivity$DelayedState;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreConnectivity$observeState$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreConnectivity$observeState$1;-><init>(Lcom/discord/stores/StoreConnectivity;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "ObservationDeckProvider\n\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
