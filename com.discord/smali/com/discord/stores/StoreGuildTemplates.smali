.class public final Lcom/discord/stores/StoreGuildTemplates;
.super Ljava/lang/Object;
.source "StoreGuildTemplates.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private dynamicLinkGuildTemplateCode:Ljava/lang/String;

.field private final dynamicLinkGuildTemplateCodePublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final guildTemplatesByCode:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
            ">;"
        }
    .end annotation
.end field

.field private final guildTemplatesByCodePublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildTemplates;->dispatcher:Lcom/discord/stores/Dispatcher;

    iget-object p1, p0, Lcom/discord/stores/StoreGuildTemplates;->dynamicLinkGuildTemplateCode:Ljava/lang/String;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGuildTemplates;->dynamicLinkGuildTemplateCodePublisher:Lrx/subjects/BehaviorSubject;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCodePublisher:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreGuildTemplates;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildTemplates;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getDynamicLinkGuildTemplateCode$p(Lcom/discord/stores/StoreGuildTemplates;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildTemplates;->dynamicLinkGuildTemplateCode:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$handleRequestGuildTemplateError(Lcom/discord/stores/StoreGuildTemplates;Ljava/lang/String;Lcom/discord/utilities/error/Error$Type;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildTemplates;->handleRequestGuildTemplateError(Ljava/lang/String;Lcom/discord/utilities/error/Error$Type;)V

    return-void
.end method

.method public static final synthetic access$handleRequestGuildTemplateSuccess(Lcom/discord/stores/StoreGuildTemplates;Lcom/discord/models/domain/ModelGuildTemplate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGuildTemplates;->handleRequestGuildTemplateSuccess(Lcom/discord/models/domain/ModelGuildTemplate;)V

    return-void
.end method

.method public static final synthetic access$initializeTemplateState(Lcom/discord/stores/StoreGuildTemplates;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGuildTemplates;->initializeTemplateState(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setDynamicLinkGuildTemplateCode$p(Lcom/discord/stores/StoreGuildTemplates;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGuildTemplates;->dynamicLinkGuildTemplateCode:Ljava/lang/String;

    return-void
.end method

.method private final handleRequestGuildTemplateError(Ljava/lang/String;Lcom/discord/utilities/error/Error$Type;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/16 v0, 0xb

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    sget-object v0, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    sget-object v0, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Invalid;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Invalid;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->guildTemplateResolveFailed(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private final handleRequestGuildTemplateSuccess(Lcom/discord/models/domain/ModelGuildTemplate;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildTemplate;->getCode()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;

    invoke-direct {v2, p1}, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;-><init>(Lcom/discord/models/domain/ModelGuildTemplate;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->guildTemplateResolved(Lcom/discord/models/domain/ModelGuildTemplate;)V

    return-void
.end method

.method private final initializeTemplateState(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    sget-object v1, Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;->INSTANCE:Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreGuildTemplates;->requestGuildTemplate(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private final requestGuildTemplate(Ljava/lang/String;)V
    .locals 11

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/utilities/rest/RestAPI;->getGuildTemplateCode(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lf/a/b/r;->f(ZI)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v2

    const-string v0, "RestAPI\n        .api\n   \u2026ormers.restSubscribeOn())"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v3, Lcom/discord/stores/StoreGuildTemplates;

    new-instance v8, Lcom/discord/stores/StoreGuildTemplates$requestGuildTemplate$1;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreGuildTemplates$requestGuildTemplate$1;-><init>(Lcom/discord/stores/StoreGuildTemplates;)V

    new-instance v6, Lcom/discord/stores/StoreGuildTemplates$requestGuildTemplate$2;

    invoke-direct {v6, p0, p1}, Lcom/discord/stores/StoreGuildTemplates$requestGuildTemplate$2;-><init>(Lcom/discord/stores/StoreGuildTemplates;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final clearDynamicLinkGuildTemplateCode()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildTemplates$clearDynamicLinkGuildTemplateCode$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreGuildTemplates$clearDynamicLinkGuildTemplateCode$1;-><init>(Lcom/discord/stores/StoreGuildTemplates;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final getDynamicLinkGuildTemplateCode()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->dynamicLinkGuildTemplateCodePublisher:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "dynamicLinkGuildTemplate\u2026er.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeGuildTemplate(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;",
            ">;"
        }
    .end annotation

    const-string v0, "guildTemplateCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildTemplates$observeGuildTemplate$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGuildTemplates$observeGuildTemplate$1;-><init>(Lcom/discord/stores/StoreGuildTemplates;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCodePublisher:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreGuildTemplates$observeGuildTemplate$2;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreGuildTemplates$observeGuildTemplate$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "guildTemplatesByCodePubl\u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onDispatchEnded()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCode:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/discord/stores/StoreGuildTemplates;->guildTemplatesByCodePublisher:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->dynamicLinkGuildTemplateCodePublisher:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreGuildTemplates;->dynamicLinkGuildTemplateCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final setDynamicLinkGuildTemplateCode(Ljava/lang/String;)V
    .locals 2

    const-string v0, "guildTemplateCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGuildTemplates;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildTemplates$setDynamicLinkGuildTemplateCode$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreGuildTemplates$setDynamicLinkGuildTemplateCode$1;-><init>(Lcom/discord/stores/StoreGuildTemplates;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
