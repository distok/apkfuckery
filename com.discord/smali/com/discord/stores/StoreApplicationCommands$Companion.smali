.class public final Lcom/discord/stores/StoreApplicationCommands$Companion;
.super Ljava/lang/Object;
.source "StoreApplicationCommands.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreApplicationCommands;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreApplicationCommands$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDiscoverCommandsUpdate()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreApplicationCommands;->access$getDiscoverCommandsUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    move-result-object v0

    return-object v0
.end method

.method public final getGuildApplicationsUpdate()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreApplicationCommands;->access$getGuildApplicationsUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    move-result-object v0

    return-object v0
.end method

.method public final getQueryCommandsUpdate()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreApplicationCommands;->access$getQueryCommandsUpdate$cp()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    move-result-object v0

    return-object v0
.end method
