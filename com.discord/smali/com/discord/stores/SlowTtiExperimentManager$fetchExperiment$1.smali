.class public final Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$1;
.super Lx/m/c/k;
.source "SlowTtiExperimentManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/SlowTtiExperimentManager;->fetchExperiment(Lcom/discord/stores/StoreExperiments;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/models/experiments/domain/Experiment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $storeExperiments:Lcom/discord/stores/StoreExperiments;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreExperiments;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$1;->$storeExperiments:Lcom/discord/stores/StoreExperiments;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/models/experiments/domain/Experiment;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$1;->$storeExperiments:Lcom/discord/stores/StoreExperiments;

    const-string v1, "2020-08_android_tti_delay"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$1;->invoke()Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    return-object v0
.end method
