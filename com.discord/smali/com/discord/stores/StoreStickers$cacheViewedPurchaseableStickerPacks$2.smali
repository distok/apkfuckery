.class public final Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;
.super Lx/m/c/k;
.source "StoreStickers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStickers;->cacheViewedPurchaseableStickerPacks(Ljava/util/Set;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $fromChatInput:Z

.field public final synthetic $newStickerPackIdsSeen:Ljava/util/Set;

.field public final synthetic this$0:Lcom/discord/stores/StoreStickers;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers;Ljava/util/Set;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->this$0:Lcom/discord/stores/StoreStickers;

    iput-object p2, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->$newStickerPackIdsSeen:Ljava/util/Set;

    iput-boolean p3, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->$fromChatInput:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 7

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v0}, Lcom/discord/stores/StoreStickers;->access$getViewedPurchaseablePacksCache$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v0}, Lx/h/f;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->$newStickerPackIdsSeen:Ljava/util/Set;

    move-object v2, v0

    check-cast v2, Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->$newStickerPackIdsSeen:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v1}, Lcom/discord/stores/StoreStickers;->access$getClock$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/time/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    sub-long/2addr v1, v3

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v1}, Lcom/discord/stores/StoreStickers;->access$getClock$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/time/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    :goto_1
    iget-object v3, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->$newStickerPackIdsSeen:Ljava/util/Set;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    iget-boolean v1, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->$fromChatInput:Z

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v1}, Lcom/discord/stores/StoreStickers;->access$getViewedPurchaseablePacksCacheChatInput$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;

    move-result-object v1

    invoke-static {v0}, Lx/h/f;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v1, v0, v4, v3, v2}, Lcom/discord/utilities/persister/Persister;->set$default(Lcom/discord/utilities/persister/Persister;Ljava/lang/Object;ZILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lcom/discord/stores/StoreStickers$cacheViewedPurchaseableStickerPacks$2;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v1}, Lcom/discord/stores/StoreStickers;->access$getViewedPurchaseablePacksCache$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/persister/Persister;

    move-result-object v1

    invoke-static {v0}, Lx/h/f;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v1, v0, v4, v3, v2}, Lcom/discord/utilities/persister/Persister;->set$default(Lcom/discord/utilities/persister/Persister;Ljava/lang/Object;ZILjava/lang/Object;)Ljava/lang/Object;

    :goto_3
    return-void
.end method
