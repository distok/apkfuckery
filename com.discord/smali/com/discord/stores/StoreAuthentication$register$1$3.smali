.class public final Lcom/discord/stores/StoreAuthentication$register$1$3;
.super Ljava/lang/Object;
.source "StoreAuthentication.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAuthentication$register$1;->call(Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/models/domain/ModelUser$Token;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreAuthentication$register$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAuthentication$register$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAuthentication$register$1$3;->this$0:Lcom/discord/stores/StoreAuthentication$register$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser$Token;)V
    .locals 5

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreUserSettings;->setSyncTheme(Z)V

    iget-object v1, p0, Lcom/discord/stores/StoreAuthentication$register$1$3;->this$0:Lcom/discord/stores/StoreAuthentication$register$1;

    iget-object v1, v1, Lcom/discord/stores/StoreAuthentication$register$1;->this$0:Lcom/discord/stores/StoreAuthentication;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/discord/stores/StoreAuthentication;->setFingerprint(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/discord/stores/StoreAuthentication$register$1$3;->this$0:Lcom/discord/stores/StoreAuthentication$register$1;

    iget-object v1, v1, Lcom/discord/stores/StoreAuthentication$register$1;->this$0:Lcom/discord/stores/StoreAuthentication;

    const-string/jumbo v3, "token"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser$Token;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/discord/stores/StoreAuthentication;->setAuthed(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotifications()Lcom/discord/stores/StoreNotifications;

    move-result-object p1

    invoke-virtual {p1, v4, v2}, Lcom/discord/stores/StoreNotifications;->setEnabledInApp(ZZ)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNux()Lcom/discord/stores/StoreNux;

    move-result-object p1

    invoke-virtual {p1, v4}, Lcom/discord/stores/StoreNux;->setFirstOpen(Z)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser$Token;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreAuthentication$register$1$3;->call(Lcom/discord/models/domain/ModelUser$Token;)V

    return-void
.end method
