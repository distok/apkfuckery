.class public final Lcom/discord/stores/StoreSearch;
.super Ljava/lang/Object;
.source "StoreSearch.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreSearch$DisplayState;,
        Lcom/discord/stores/StoreSearch$SearchTarget;
    }
.end annotation


# instance fields
.field private final displayStateSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/stores/StoreSearch$DisplayState;",
            "Lcom/discord/stores/StoreSearch$DisplayState;",
            ">;"
        }
    .end annotation
.end field

.field private final historyCache:Lcom/discord/utilities/search/history/SearchHistoryCache;

.field private searchTarget:Lcom/discord/stores/StoreSearch$SearchTarget;

.field private final searchTargetSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/stores/StoreSearch$SearchTarget;",
            "Lcom/discord/stores/StoreSearch$SearchTarget;",
            ">;"
        }
    .end annotation
.end field

.field private final storeGuildsNsfw:Lcom/discord/stores/StoreGuildsNsfw;

.field private final storeSearchData:Lcom/discord/stores/StoreSearchData;

.field private final storeSearchInput:Lcom/discord/stores/StoreSearchInput;

.field private final storeSearchQuery:Lcom/discord/stores/StoreSearchQuery;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private subscription:Lrx/Subscription;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;)V
    .locals 8

    const-string/jumbo v0, "storeGuildsNsfw"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/stores/StoreSearchData;

    invoke-direct {v2}, Lcom/discord/stores/StoreSearchData;-><init>()V

    new-instance v3, Lcom/discord/stores/StoreSearchInput;

    invoke-direct {v3}, Lcom/discord/stores/StoreSearchInput;-><init>()V

    new-instance v4, Lcom/discord/stores/StoreSearchQuery;

    new-instance v0, Lcom/discord/utilities/search/network/SearchFetcher;

    invoke-direct {v0}, Lcom/discord/utilities/search/network/SearchFetcher;-><init>()V

    invoke-direct {v4, v0}, Lcom/discord/stores/StoreSearchQuery;-><init>(Lcom/discord/utilities/search/network/SearchFetcher;)V

    new-instance v7, Lcom/discord/utilities/search/history/MGPreferenceSearchHistoryCache;

    invoke-direct {v7}, Lcom/discord/utilities/search/history/MGPreferenceSearchHistoryCache;-><init>()V

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/discord/stores/StoreSearch;-><init>(Lcom/discord/stores/StoreSearchData;Lcom/discord/stores/StoreSearchInput;Lcom/discord/stores/StoreSearchQuery;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/search/history/SearchHistoryCache;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreSearchData;Lcom/discord/stores/StoreSearchInput;Lcom/discord/stores/StoreSearchQuery;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/search/history/SearchHistoryCache;)V
    .locals 1

    const-string/jumbo v0, "storeSearchData"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeSearchInput"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeSearchQuery"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeGuildsNsfw"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeUser"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "historyCache"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreSearch;->storeSearchData:Lcom/discord/stores/StoreSearchData;

    iput-object p2, p0, Lcom/discord/stores/StoreSearch;->storeSearchInput:Lcom/discord/stores/StoreSearchInput;

    iput-object p3, p0, Lcom/discord/stores/StoreSearch;->storeSearchQuery:Lcom/discord/stores/StoreSearchQuery;

    iput-object p4, p0, Lcom/discord/stores/StoreSearch;->storeGuildsNsfw:Lcom/discord/stores/StoreGuildsNsfw;

    iput-object p5, p0, Lcom/discord/stores/StoreSearch;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p6, p0, Lcom/discord/stores/StoreSearch;->historyCache:Lcom/discord/utilities/search/history/SearchHistoryCache;

    new-instance p1, Lrx/subjects/SerializedSubject;

    sget-object p2, Lcom/discord/stores/StoreSearch$DisplayState;->SUGGESTIONS:Lcom/discord/stores/StoreSearch$DisplayState;

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreSearch;->displayStateSubject:Lrx/subjects/SerializedSubject;

    new-instance p1, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreSearch;->searchTargetSubject:Lrx/subjects/SerializedSubject;

    return-void
.end method

.method public static final synthetic access$getHistoryCache$p(Lcom/discord/stores/StoreSearch;)Lcom/discord/utilities/search/history/SearchHistoryCache;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreSearch;->historyCache:Lcom/discord/utilities/search/history/SearchHistoryCache;

    return-object p0
.end method

.method public static final synthetic access$handleSubscription(Lcom/discord/stores/StoreSearch;Lrx/Subscription;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreSearch;->handleSubscription(Lrx/Subscription;)V

    return-void
.end method

.method public static final synthetic access$onStateChanged(Lcom/discord/stores/StoreSearch;Lcom/discord/stores/StoreSearch$DisplayState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreSearch;->onStateChanged(Lcom/discord/stores/StoreSearch$DisplayState;)V

    return-void
.end method

.method private final declared-synchronized handleSubscription(Lrx/Subscription;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->subscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iput-object p1, p0, Lcom/discord/stores/StoreSearch;->subscription:Lrx/Subscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final includeNsfw(Lcom/discord/stores/StoreSearch$SearchTarget;)Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->getMe()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser$Me;->getNsfwAllowed()Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->DISALLOWED:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    if-ne v0, v1, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    invoke-virtual {p1}, Lcom/discord/stores/StoreSearch$SearchTarget;->getType()Lcom/discord/stores/StoreSearch$SearchTarget$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_3
    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeGuildsNsfw:Lcom/discord/stores/StoreGuildsNsfw;

    invoke-virtual {p1}, Lcom/discord/stores/StoreSearch$SearchTarget;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildsNsfw;->isGuildNsfwGateAgreed(J)Z

    move-result v1

    :goto_1
    return v1
.end method

.method private final declared-synchronized init(Lcom/discord/stores/StoreSearch$SearchTarget;Lcom/discord/utilities/search/strings/SearchStringProvider;)V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->searchTarget:Lcom/discord/stores/StoreSearch$SearchTarget;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/discord/stores/StoreSearch;->updateTarget(Lcom/discord/stores/StoreSearch$SearchTarget;)V

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeSearchData:Lcom/discord/stores/StoreSearchData;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreSearchData;->init(Lcom/discord/stores/StoreSearch$SearchTarget;)V

    iget-object p1, p0, Lcom/discord/stores/StoreSearch;->storeSearchInput:Lcom/discord/stores/StoreSearchInput;

    invoke-virtual {p1, p2}, Lcom/discord/stores/StoreSearchInput;->init(Lcom/discord/utilities/search/strings/SearchStringProvider;)V

    iget-object p1, p0, Lcom/discord/stores/StoreSearch;->storeSearchQuery:Lcom/discord/stores/StoreSearchQuery;

    invoke-virtual {p1}, Lcom/discord/stores/StoreSearchQuery;->getState()Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreSearch$init$1;->INSTANCE:Lcom/discord/stores/StoreSearch$init$1;

    invoke-virtual {p1, p2}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreSearch$init$2;->INSTANCE:Lcom/discord/stores/StoreSearch$init$2;

    invoke-virtual {p1, p2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/stores/StoreSearch;->storeSearchInput:Lcom/discord/stores/StoreSearchInput;

    invoke-virtual {p2}, Lcom/discord/stores/StoreSearchInput;->getCurrentParsedInput()Lrx/Observable;

    move-result-object p2

    sget-object v0, Lcom/discord/stores/StoreSearch$init$3;->INSTANCE:Lcom/discord/stores/StoreSearch$init$3;

    invoke-virtual {p2, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    invoke-static {p1, p2}, Lrx/Observable;->E(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-string p1, "Observable\n        .merg\u2026splayState.SUGGESTIONS })"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/discord/stores/StoreSearch$init$4;

    invoke-direct {v3, p0}, Lcom/discord/stores/StoreSearch$init$4;-><init>(Lcom/discord/stores/StoreSearch;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/stores/StoreSearch$init$5;

    invoke-direct {v6, p0}, Lcom/discord/stores/StoreSearch$init$5;-><init>(Lcom/discord/stores/StoreSearch;)V

    const/16 v7, 0x1a

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final onStateChanged(Lcom/discord/stores/StoreSearch$DisplayState;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->displayStateSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final declared-synchronized updateTarget(Lcom/discord/stores/StoreSearch$SearchTarget;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/discord/stores/StoreSearch;->searchTarget:Lcom/discord/stores/StoreSearch$SearchTarget;

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->searchTargetSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final declared-synchronized clear()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/discord/stores/StoreSearch;->updateTarget(Lcom/discord/stores/StoreSearch$SearchTarget;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreSearch;->handleSubscription(Lrx/Subscription;)V

    sget-object v0, Lcom/discord/stores/StoreSearch$DisplayState;->SUGGESTIONS:Lcom/discord/stores/StoreSearch$DisplayState;

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreSearch;->onStateChanged(Lcom/discord/stores/StoreSearch$DisplayState;)V

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeSearchQuery:Lcom/discord/stores/StoreSearchQuery;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearchQuery;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeSearchInput:Lcom/discord/stores/StoreSearchInput;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearchInput;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeSearchData:Lcom/discord/stores/StoreSearchData;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearchData;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearHistory()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->historyCache:Lcom/discord/utilities/search/history/SearchHistoryCache;

    iget-object v1, p0, Lcom/discord/stores/StoreSearch;->searchTarget:Lcom/discord/stores/StoreSearch$SearchTarget;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/discord/utilities/search/history/SearchHistoryCache;->clear(Lcom/discord/stores/StoreSearch$SearchTarget;)V

    :cond_0
    return-void
.end method

.method public final getDisplayState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreSearch$DisplayState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->displayStateSubject:Lrx/subjects/SerializedSubject;

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "displayStateSubject\n    \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getHistory()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Collection<",
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->searchTargetSubject:Lrx/subjects/SerializedSubject;

    new-instance v1, Lcom/discord/stores/StoreSearch$getHistory$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreSearch$getHistory$1;-><init>(Lcom/discord/stores/StoreSearch;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "searchTargetSubject\n    \u2026())\n          }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getStoreSearchData()Lcom/discord/stores/StoreSearchData;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeSearchData:Lcom/discord/stores/StoreSearchData;

    return-object v0
.end method

.method public final getStoreSearchInput()Lcom/discord/stores/StoreSearchInput;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeSearchInput:Lcom/discord/stores/StoreSearchInput;

    return-object v0
.end method

.method public final getStoreSearchQuery()Lcom/discord/stores/StoreSearchQuery;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->storeSearchQuery:Lcom/discord/stores/StoreSearchQuery;

    return-object v0
.end method

.method public final initForChannel(JLcom/discord/utilities/search/strings/SearchStringProvider;)V
    .locals 2

    const-string v0, "searchStringProvider"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreSearch$SearchTarget;

    sget-object v1, Lcom/discord/stores/StoreSearch$SearchTarget$Type;->CHANNEL:Lcom/discord/stores/StoreSearch$SearchTarget$Type;

    invoke-direct {v0, v1, p1, p2}, Lcom/discord/stores/StoreSearch$SearchTarget;-><init>(Lcom/discord/stores/StoreSearch$SearchTarget$Type;J)V

    invoke-direct {p0, v0, p3}, Lcom/discord/stores/StoreSearch;->init(Lcom/discord/stores/StoreSearch$SearchTarget;Lcom/discord/utilities/search/strings/SearchStringProvider;)V

    sget-object p1, Lcom/discord/utilities/search/suggestion/SearchSuggestionEngine;->INSTANCE:Lcom/discord/utilities/search/suggestion/SearchSuggestionEngine;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/discord/utilities/search/suggestion/SearchSuggestionEngine;->setTargetGuildId(Ljava/lang/Long;)V

    return-void
.end method

.method public final initForGuild(JLcom/discord/utilities/search/strings/SearchStringProvider;)V
    .locals 2

    const-string v0, "searchStringProvider"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreSearch$SearchTarget;

    sget-object v1, Lcom/discord/stores/StoreSearch$SearchTarget$Type;->GUILD:Lcom/discord/stores/StoreSearch$SearchTarget$Type;

    invoke-direct {v0, v1, p1, p2}, Lcom/discord/stores/StoreSearch$SearchTarget;-><init>(Lcom/discord/stores/StoreSearch$SearchTarget$Type;J)V

    invoke-direct {p0, v0, p3}, Lcom/discord/stores/StoreSearch;->init(Lcom/discord/stores/StoreSearch$SearchTarget;Lcom/discord/utilities/search/strings/SearchStringProvider;)V

    sget-object p3, Lcom/discord/utilities/search/suggestion/SearchSuggestionEngine;->INSTANCE:Lcom/discord/utilities/search/suggestion/SearchSuggestionEngine;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/discord/utilities/search/suggestion/SearchSuggestionEngine;->setTargetGuildId(Ljava/lang/Long;)V

    return-void
.end method

.method public final loadInitial(Ljava/lang/String;Lcom/discord/utilities/search/strings/SearchStringProvider;)V
    .locals 7

    const-string v0, "queryString"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchStringProvider"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/stores/StoreSearch;->searchTarget:Lcom/discord/stores/StoreSearch$SearchTarget;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreSearch;->storeSearchQuery:Lcom/discord/stores/StoreSearchQuery;

    invoke-direct {p0, v3}, Lcom/discord/stores/StoreSearch;->includeNsfw(Lcom/discord/stores/StoreSearch$SearchTarget;)Z

    move-result v6

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/discord/stores/StoreSearchQuery;->parseAndQuery(Lcom/discord/stores/StoreSearch;Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/lang/String;Lcom/discord/utilities/search/strings/SearchStringProvider;Z)V

    :cond_0
    return-void
.end method

.method public final loadMore(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->searchTarget:Lcom/discord/stores/StoreSearch$SearchTarget;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreSearch;->storeSearchQuery:Lcom/discord/stores/StoreSearchQuery;

    invoke-virtual {v1, v0, p1, p2}, Lcom/discord/stores/StoreSearchQuery;->loadMore(Lcom/discord/stores/StoreSearch$SearchTarget;J)V

    :cond_0
    return-void
.end method

.method public final persistQuery$app_productionDiscordExternalRelease(Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreSearch$SearchTarget;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;)V"
        }
    .end annotation

    const-string v0, "searchTarget"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "query"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreSearch;->historyCache:Lcom/discord/utilities/search/history/SearchHistoryCache;

    invoke-interface {v0, p1, p2}, Lcom/discord/utilities/search/history/SearchHistoryCache;->persistQuery(Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/util/List;)V

    return-void
.end method
