.class public final Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;
.super Lx/m/c/k;
.source "StoreMessages.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->invoke(Lcom/discord/utilities/messagesend/MessageResult;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isLastMessage:Z

.field public final synthetic $result:Lcom/discord/utilities/messagesend/MessageResult;

.field public final synthetic this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;ZLcom/discord/utilities/messagesend/MessageResult;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iput-boolean p2, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$isLastMessage:Z

    iput-object p3, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$result:Lcom/discord/utilities/messagesend/MessageResult;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    iget-boolean v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$isLastMessage:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreMessages;->Companion:Lcom/discord/stores/StoreMessages$Companion;

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    invoke-static {v1}, Lcom/discord/stores/StoreMessages;->access$getContext$p(Lcom/discord/stores/StoreMessages;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/stores/StoreMessages$Companion;->access$cancelBackgroundSendingWork(Lcom/discord/stores/StoreMessages$Companion;Landroid/content/Context;)Landroidx/work/Operation;

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$result:Lcom/discord/utilities/messagesend/MessageResult;

    instance-of v1, v0, Lcom/discord/utilities/messagesend/MessageResult$Success;

    const-string v2, "localMessage"

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    invoke-static {v0}, Lcom/discord/stores/StoreMessages;->access$getStream$p(Lcom/discord/stores/StoreMessages;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getSlowMode$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreSlowMode;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$result:Lcom/discord/utilities/messagesend/MessageResult;

    check-cast v1, Lcom/discord/utilities/messagesend/MessageResult$Success;

    invoke-virtual {v1}, Lcom/discord/utilities/messagesend/MessageResult$Success;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/discord/stores/StoreSlowMode;->onMessageSent(J)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$result:Lcom/discord/utilities/messagesend/MessageResult;

    check-cast v1, Lcom/discord/utilities/messagesend/MessageResult$Success;

    invoke-virtual {v1}, Lcom/discord/utilities/messagesend/MessageResult$Success;->getMessage()Lcom/discord/models/domain/ModelMessage;

    move-result-object v1

    invoke-static {v1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreMessages;->handleMessageCreate(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage;->getNumRetries()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    const-string v1, "localMessage.numRetries ?: 0"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/stores/FailedMessageResolutionType;->RESENT:Lcom/discord/stores/FailedMessageResolutionType;

    invoke-static {v1, v0, v2}, Lcom/discord/stores/StoreMessages;->access$trackFailedLocalMessageResolved(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;Lcom/discord/stores/FailedMessageResolutionType;)V

    goto/16 :goto_1

    :cond_2
    instance-of v1, v0, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/discord/stores/StoreMessages;->access$handleSendMessageFailure(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    invoke-static {v0}, Lcom/discord/stores/StoreMessages;->access$getStream$p(Lcom/discord/stores/StoreMessages;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getSlowMode$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreSlowMode;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$result:Lcom/discord/utilities/messagesend/MessageResult;

    check-cast v3, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;

    invoke-virtual {v3}, Lcom/discord/utilities/messagesend/MessageResult$Slowmode;->getCooldownMs()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/discord/stores/StoreSlowMode;->onCooldown(JJ)V

    goto :goto_1

    :cond_3
    instance-of v1, v0, Lcom/discord/utilities/messagesend/MessageResult$RateLimited;

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    instance-of v1, v0, Lcom/discord/utilities/messagesend/MessageResult$UserCancelled;

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/discord/stores/StoreMessages;->access$handleLocalMessageDelete(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    goto :goto_1

    :cond_5
    instance-of v1, v0, Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/discord/stores/StoreMessages;->access$handleSendMessageFailure(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    goto :goto_1

    :cond_6
    instance-of v1, v0, Lcom/discord/utilities/messagesend/MessageResult$ValidationError;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v2, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v1, v1, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    check-cast v0, Lcom/discord/utilities/messagesend/MessageResult$ValidationError;

    invoke-virtual {v0}, Lcom/discord/utilities/messagesend/MessageResult$ValidationError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/discord/stores/StoreMessages;->access$handleSendMessageValidationError(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    instance-of v1, v0, Lcom/discord/utilities/messagesend/MessageResult$NetworkFailure;

    if-eqz v1, :cond_8

    goto :goto_1

    :cond_8
    instance-of v0, v0, Lcom/discord/utilities/messagesend/MessageResult$Timeout;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1;

    iget-object v1, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->this$0:Lcom/discord/stores/StoreMessages;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1;->$localMessage:Lcom/discord/models/domain/ModelMessage;

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/discord/stores/StoreMessages;->access$handleSendMessageFailure(Lcom/discord/stores/StoreMessages;Lcom/discord/models/domain/ModelMessage;)V

    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->$emitter:Lrx/Emitter;

    iget-object v1, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->$result:Lcom/discord/utilities/messagesend/MessageResult;

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1$1;->this$0:Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;

    iget-object v0, v0, Lcom/discord/stores/StoreMessages$sendMessage$request$1$1;->$emitter:Lrx/Emitter;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method
