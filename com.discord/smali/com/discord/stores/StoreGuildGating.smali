.class public final Lcom/discord/stores/StoreGuildGating;
.super Ljava/lang/Object;
.source "StoreGuildGating.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final gatingState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/guild/ModelGatingData;",
            ">;"
        }
    .end annotation
.end field

.field private final gatingStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/guild/ModelGatingData;",
            ">;>;"
        }
    .end annotation
.end field

.field private isDirty:Z

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildGating;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreGuildGating;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreGuildGating;->gatingState:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreGuildGating;->gatingStateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    sget-object p2, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p2}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildGating;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreGuildGating;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildGating;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getGatingState$p(Lcom/discord/stores/StoreGuildGating;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildGating;->gatingState:Ljava/util/HashMap;

    return-object p0
.end method

.method public static final synthetic access$getRestAPI$p(Lcom/discord/stores/StoreGuildGating;)Lcom/discord/utilities/rest/RestAPI;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreGuildGating;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object p0
.end method

.method public static final synthetic access$handleGatingFetchFailed(Lcom/discord/stores/StoreGuildGating;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildGating;->handleGatingFetchFailed(J)V

    return-void
.end method

.method public static final synthetic access$handleGatingFetchStart(Lcom/discord/stores/StoreGuildGating;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGuildGating;->handleGatingFetchStart(J)V

    return-void
.end method

.method public static final synthetic access$handleGatingFetchSuccess(Lcom/discord/stores/StoreGuildGating;JLcom/discord/models/domain/ModelMemberVerificationForm;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreGuildGating;->handleGatingFetchSuccess(JLcom/discord/models/domain/ModelMemberVerificationForm;)V

    return-void
.end method

.method private final handleGatingFetchFailed(J)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildGating;->gatingState:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/models/domain/guild/ModelGatingData;

    sget-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->FAILED:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const/4 v2, 0x0

    invoke-direct {p2, v1, v2}, Lcom/discord/models/domain/guild/ModelGatingData;-><init>(Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreGuildGating;->isDirty:Z

    return-void
.end method

.method private final handleGatingFetchStart(J)V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreGuildGating;->gatingState:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/models/domain/guild/ModelGatingData;

    sget-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->FETCHING:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const/4 v2, 0x0

    invoke-direct {p2, v1, v2}, Lcom/discord/models/domain/guild/ModelGatingData;-><init>(Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreGuildGating;->isDirty:Z

    return-void
.end method

.method private final handleGatingFetchSuccess(JLcom/discord/models/domain/ModelMemberVerificationForm;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildGating;->gatingState:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/models/domain/guild/ModelGatingData;

    sget-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->SUCCEEDED:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    invoke-direct {p2, v1, p3}, Lcom/discord/models/domain/guild/ModelGatingData;-><init>(Lcom/discord/models/domain/guild/CommunityGatingFetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreGuildGating;->isDirty:Z

    return-void
.end method


# virtual methods
.method public final fetchGating(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreGuildGating;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreGuildGating$fetchGating$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreGuildGating$fetchGating$1;-><init>(Lcom/discord/stores/StoreGuildGating;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final observeMemberVerificationForm(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/guild/ModelGatingData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGuildGating;->gatingStateSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreGuildGating$observeMemberVerificationForm$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/stores/StoreGuildGating$observeMemberVerificationForm$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "gatingStateSubject.map {\u2026 }.distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onDispatchEnded()V
    .locals 2

    iget-boolean v0, p0, Lcom/discord/stores/StoreGuildGating;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGuildGating;->gatingStateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreGuildGating;->gatingState:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreGuildGating;->isDirty:Z

    :cond_0
    return-void
.end method
