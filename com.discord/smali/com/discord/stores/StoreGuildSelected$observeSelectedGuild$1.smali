.class public final Lcom/discord/stores/StoreGuildSelected$observeSelectedGuild$1;
.super Lx/m/c/k;
.source "StoreGuildSelected.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGuildSelected;->observeSelectedGuild()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/models/domain/ModelGuild;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreGuildSelected;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildSelected;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGuildSelected$observeSelectedGuild$1;->this$0:Lcom/discord/stores/StoreGuildSelected;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/models/domain/ModelGuild;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreGuildSelected$observeSelectedGuild$1;->this$0:Lcom/discord/stores/StoreGuildSelected;

    invoke-static {v0}, Lcom/discord/stores/StoreGuildSelected;->access$getGuildStore$p(Lcom/discord/stores/StoreGuildSelected;)Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuilds;->getGuilds()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreGuildSelected$observeSelectedGuild$1;->this$0:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildSelected;->getSelectedGuildId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGuildSelected$observeSelectedGuild$1;->invoke()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    return-object v0
.end method
