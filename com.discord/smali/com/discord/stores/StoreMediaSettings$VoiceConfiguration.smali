.class public final Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
.super Ljava/lang/Object;
.source "StoreMediaSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreMediaSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceConfiguration"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;

.field private static final DEFAULT_NOISE_CANCELLATION:Z

.field private static final DEFAULT_NOISE_SUPPRESSION:Z

.field public static final DEFAULT_OUTPUT_VOLUME:F = 100.0f

.field public static final DEFAULT_SENSITIVITY:F = -50.0f

.field private static final DEFAULT_VOICE_CONFIG:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;


# instance fields
.field private final automaticGainControl:Z

.field private final automaticVad:Z

.field private final echoCancellation:Z

.field private final enableVideoHardwareScaling:Z

.field private final inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

.field private final isSelfDeafened:Z

.field private final isSelfMuted:Z

.field private final mutedUsers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

.field private final outputVolume:F

.field private final sensitivity:F

.field private final userOutputVolumes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final vadUseKrisp:Z

.field private final voiceParticipantsHidden:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 19

    new-instance v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->Companion:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;

    new-instance v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-object v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x3fff

    const/16 v18, 0x0

    invoke-direct/range {v2 .. v18}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;-><init>(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->DEFAULT_VOICE_CONFIG:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iget-object v0, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    sget-object v1, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Suppression:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    sput-boolean v1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->DEFAULT_NOISE_SUPPRESSION:Z

    sget-object v1, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Cancellation:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    sput-boolean v2, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->DEFAULT_NOISE_CANCELLATION:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 17

    move-object/from16 v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x3fff

    const/16 v16, 0x0

    invoke-direct/range {v0 .. v16}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;-><init>(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZZZ",
            "Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;",
            "F",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;",
            "F",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "noiseProcessing"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputMode"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mutedUsers"

    invoke-static {p11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userOutputVolumes"

    invoke-static {p12, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    iput-boolean p2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    iput-boolean p3, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    iput-boolean p4, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    iput-boolean p5, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    iput-boolean p6, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    iput-object p7, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    iput p8, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    iput-object p9, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    iput p10, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    iput-object p11, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    iput-object p12, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    iput-boolean p13, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    iput-boolean p14, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 15

    move/from16 v0, p15

    sget-object v1, Lx/h/m;->d:Lx/h/m;

    and-int/lit8 v2, v0, 0x1

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    move/from16 v2, p1

    :goto_0
    and-int/lit8 v4, v0, 0x2

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    move/from16 v4, p2

    :goto_1
    and-int/lit8 v5, v0, 0x4

    const/4 v6, 0x1

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :cond_2
    move/from16 v5, p3

    :goto_2
    and-int/lit8 v7, v0, 0x8

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    goto :goto_3

    :cond_3
    move/from16 v7, p4

    :goto_3
    and-int/lit8 v8, v0, 0x10

    if-eqz v8, :cond_4

    const/4 v8, 0x1

    goto :goto_4

    :cond_4
    move/from16 v8, p5

    :goto_4
    and-int/lit8 v9, v0, 0x20

    if-eqz v9, :cond_5

    goto :goto_5

    :cond_5
    move/from16 v6, p6

    :goto_5
    and-int/lit8 v9, v0, 0x40

    if-eqz v9, :cond_6

    sget-object v9, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Suppression:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    goto :goto_6

    :cond_6
    move-object/from16 v9, p7

    :goto_6
    and-int/lit16 v10, v0, 0x80

    if-eqz v10, :cond_7

    const/high16 v10, -0x3db80000    # -50.0f

    goto :goto_7

    :cond_7
    move/from16 v10, p8

    :goto_7
    and-int/lit16 v11, v0, 0x100

    if-eqz v11, :cond_8

    sget-object v11, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->VOICE_ACTIVITY:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    goto :goto_8

    :cond_8
    move-object/from16 v11, p9

    :goto_8
    and-int/lit16 v12, v0, 0x200

    if-eqz v12, :cond_9

    const/high16 v12, 0x42c80000    # 100.0f

    goto :goto_9

    :cond_9
    move/from16 v12, p10

    :goto_9
    and-int/lit16 v13, v0, 0x400

    if-eqz v13, :cond_a

    move-object v13, v1

    goto :goto_a

    :cond_a
    move-object/from16 v13, p11

    :goto_a
    and-int/lit16 v14, v0, 0x800

    if-eqz v14, :cond_b

    goto :goto_b

    :cond_b
    move-object/from16 v1, p12

    :goto_b
    and-int/lit16 v14, v0, 0x1000

    if-eqz v14, :cond_c

    const/4 v14, 0x0

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_d

    goto :goto_d

    :cond_d
    move/from16 v3, p14

    :goto_d
    move-object/from16 p1, p0

    move/from16 p2, v2

    move/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v7

    move/from16 p6, v8

    move/from16 p7, v6

    move-object/from16 p8, v9

    move/from16 p9, v10

    move-object/from16 p10, v11

    move/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v1

    move/from16 p14, v14

    move/from16 p15, v3

    invoke-direct/range {p1 .. p15}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;-><init>(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZ)V

    return-void
.end method

.method public static final synthetic access$getDEFAULT_NOISE_CANCELLATION$cp()Z
    .locals 1

    sget-boolean v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->DEFAULT_NOISE_CANCELLATION:Z

    return v0
.end method

.method public static final synthetic access$getDEFAULT_NOISE_SUPPRESSION$cp()Z
    .locals 1

    sget-boolean v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->DEFAULT_NOISE_SUPPRESSION:Z

    return v0
.end method

.method public static final synthetic access$getDEFAULT_VOICE_CONFIG$cp()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->DEFAULT_VOICE_CONFIG:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    return-object v0
.end method

.method private final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    return v0
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZILjava/lang/Object;)Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 15

    move-object v0, p0

    move/from16 v1, p15

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    goto :goto_0

    :cond_0
    move/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget v11, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-boolean v14, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    iget-boolean v1, v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    goto :goto_d

    :cond_d
    move/from16 v1, p14

    :goto_d
    move/from16 p1, v2

    move/from16 p2, v3

    move/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move/from16 p13, v14

    move/from16 p14, v1

    invoke-virtual/range {p0 .. p14}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->copy(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZ)Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component10()F
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    return v0
.end method

.method public final component11()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    return-object v0
.end method

.method public final component12()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    return-object v0
.end method

.method public final component13()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    return v0
.end method

.method public final component14()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    return v0
.end method

.method public final component7()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    return-object v0
.end method

.method public final component8()F
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    return v0
.end method

.method public final component9()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    return-object v0
.end method

.method public final copy(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZ)Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZZZ",
            "Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;",
            "F",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;",
            "F",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;ZZ)",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;"
        }
    .end annotation

    const-string v0, "noiseProcessing"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputMode"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mutedUsers"

    move-object/from16 v12, p11

    invoke-static {v12, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userOutputVolumes"

    move-object/from16 v13, p12

    invoke-static {v13, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-object v1, v0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v9, p8

    move/from16 v11, p10

    move/from16 v14, p13

    move/from16 v15, p14

    invoke-direct/range {v1 .. v15}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;-><init>(ZZZZZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    iget-object v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    iget v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    iget-object v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    iget v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    iget-boolean v1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    iget-boolean p1, p1, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutomaticGainControl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    return v0
.end method

.method public final getAutomaticVad()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    return v0
.end method

.method public final getEchoCancellation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    return v0
.end method

.method public final getEnableVideoHardwareScaling()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    return v0
.end method

.method public final getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    return-object v0
.end method

.method public final getMutedUsers()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    return-object v0
.end method

.method public final getNoiseProcessing()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    return-object v0
.end method

.method public final getOutputVolume()F
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    return v0
.end method

.method public final getSensitivity()F
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    return v0
.end method

.method public final getUserOutputVolumes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    return-object v0
.end method

.method public final getVadUseKrisp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    return v0
.end method

.method public final getVoiceParticipantsHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    const/4 v3, 0x0

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    const/16 v4, 0x1f

    invoke-static {v2, v0, v4}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget-object v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    const/16 v4, 0x1f

    invoke-static {v2, v0, v4}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget-object v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_8
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_9
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    if-eqz v2, :cond_b

    goto :goto_3

    :cond_b
    move v1, v2

    :goto_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isSelfDeafened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    return v0
.end method

.method public final isSelfMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "VoiceConfiguration(isSelfMuted="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSelfDeafened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", automaticVad="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticVad:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", vadUseKrisp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->vadUseKrisp:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", automaticGainControl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->automaticGainControl:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", echoCancellation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->echoCancellation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", noiseProcessing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->noiseProcessing:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", sensitivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->sensitivity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", inputMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", outputVolume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->outputVolume:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mutedUsers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->mutedUsers:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userOutputVolumes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->userOutputVolumes:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", enableVideoHardwareScaling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->enableVideoHardwareScaling:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", voiceParticipantsHidden="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->voiceParticipantsHidden:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
