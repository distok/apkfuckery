.class public final Lcom/discord/stores/StoreNux;
.super Lcom/discord/stores/Store;
.source "StoreNux.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreNux$NuxState;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private nuxState:Lcom/discord/stores/StoreNux$NuxState;

.field private final nuxStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreNux$NuxState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 8

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreNux;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p1, Lcom/discord/stores/StoreNux$NuxState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/discord/stores/StoreNux$NuxState;-><init>(ZZZLjava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreNux;->nuxStateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getNuxState$p(Lcom/discord/stores/StoreNux;)Lcom/discord/stores/StoreNux$NuxState;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    return-object p0
.end method

.method public static final synthetic access$publishNuxUpdated(Lcom/discord/stores/StoreNux;Lcom/discord/stores/StoreNux$NuxState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreNux;->publishNuxUpdated(Lcom/discord/stores/StoreNux$NuxState;)V

    return-void
.end method

.method public static final synthetic access$setNuxState$p(Lcom/discord/stores/StoreNux;Lcom/discord/stores/StoreNux$NuxState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    return-void
.end method

.method private final publishNuxUpdated(Lcom/discord/stores/StoreNux$NuxState;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreNux;->nuxStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getNuxState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreNux$NuxState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreNux;->nuxStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "nuxStateSubject.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final handleGuildSelected(J)V
    .locals 10
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    invoke-virtual {v0}, Lcom/discord/stores/StoreNux$NuxState;->getPremiumGuildHintGuildId()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, v0, p1

    if-eqz v2, :cond_1

    :goto_0
    iget-object v3, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x7

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/discord/stores/StoreNux$NuxState;->copy$default(Lcom/discord/stores/StoreNux$NuxState;ZZZLjava/lang/Long;ILjava/lang/Object;)Lcom/discord/stores/StoreNux$NuxState;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreNux;->publishNuxUpdated(Lcom/discord/stores/StoreNux$NuxState;)V

    :cond_1
    return-void
.end method

.method public final handleSamplePremiumGuildSelected(J)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/discord/stores/StoreNux$NuxState;->copy$default(Lcom/discord/stores/StoreNux$NuxState;ZZZLjava/lang/Long;ILjava/lang/Object;)Lcom/discord/stores/StoreNux$NuxState;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreNux;->nuxState:Lcom/discord/stores/StoreNux$NuxState;

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreNux;->publishNuxUpdated(Lcom/discord/stores/StoreNux$NuxState;)V

    return-void
.end method

.method public final setFirstOpen(Z)V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreNux$setFirstOpen$1;

    invoke-direct {v0, p1}, Lcom/discord/stores/StoreNux$setFirstOpen$1;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreNux;->updateNux(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final setPostRegister(Z)V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreNux$setPostRegister$1;

    invoke-direct {v0, p1}, Lcom/discord/stores/StoreNux$setPostRegister$1;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreNux;->updateNux(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final setPremiumGuildHintGuildId(Ljava/lang/Long;)V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreNux$setPremiumGuildHintGuildId$1;

    invoke-direct {v0, p1}, Lcom/discord/stores/StoreNux$setPremiumGuildHintGuildId$1;-><init>(Ljava/lang/Long;)V

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreNux;->updateNux(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateNux(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/StoreNux$NuxState;",
            "Lcom/discord/stores/StoreNux$NuxState;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "updateFunction"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreNux;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreNux$updateNux$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreNux$updateNux$1;-><init>(Lcom/discord/stores/StoreNux;Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
