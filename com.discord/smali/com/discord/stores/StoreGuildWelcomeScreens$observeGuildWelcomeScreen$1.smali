.class public final Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;
.super Lx/m/c/k;
.source "StoreGuildWelcomeScreens.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGuildWelcomeScreens;->observeGuildWelcomeScreen(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildWelcomeScreens;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;->this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;

    iput-wide p2, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;->$guildId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;->this$0:Lcom/discord/stores/StoreGuildWelcomeScreens;

    iget-wide v1, p0, Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;->$guildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildWelcomeScreens;->getGuildWelcomeScreen(J)Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1;->invoke()Lcom/discord/stores/StoreGuildWelcomeScreens$GuildWelcomeScreen;

    move-result-object v0

    return-object v0
.end method
