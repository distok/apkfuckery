.class public Lcom/discord/stores/StoreNotices$Notice;
.super Ljava/lang/Object;
.source "StoreNotices.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreNotices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Notice"
.end annotation


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final delayPeriodMs:J

.field private hasShown:Z

.field private final name:Ljava/lang/String;

.field private final persist:Z

.field private final priority:I

.field private final requestedShowTimestamp:J

.field private final show:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final sinceLastPeriodMs:J

.field private final validScreens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx/q/b<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/utilities/time/Clock;",
            "JIZ",
            "Ljava/util/List<",
            "+",
            "Lx/q/b<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;>;JJ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "show"

    invoke-static {p12, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreNotices$Notice;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/stores/StoreNotices$Notice;->clock:Lcom/discord/utilities/time/Clock;

    iput-wide p3, p0, Lcom/discord/stores/StoreNotices$Notice;->requestedShowTimestamp:J

    iput p5, p0, Lcom/discord/stores/StoreNotices$Notice;->priority:I

    iput-boolean p6, p0, Lcom/discord/stores/StoreNotices$Notice;->persist:Z

    iput-object p7, p0, Lcom/discord/stores/StoreNotices$Notice;->validScreens:Ljava/util/List;

    iput-wide p8, p0, Lcom/discord/stores/StoreNotices$Notice;->delayPeriodMs:J

    iput-wide p10, p0, Lcom/discord/stores/StoreNotices$Notice;->sinceLastPeriodMs:J

    iput-object p12, p0, Lcom/discord/stores/StoreNotices$Notice;->show:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 15

    move/from16 v0, p13

    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v1

    move-object v4, v1

    goto :goto_0

    :cond_0
    move-object/from16 v4, p2

    :goto_0
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_1

    invoke-interface {v4}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    move-wide v5, v1

    goto :goto_1

    :cond_1
    move-wide/from16 v5, p3

    :goto_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    const/16 v1, 0xa

    const/16 v7, 0xa

    goto :goto_2

    :cond_2
    move/from16 v7, p5

    :goto_2
    and-int/lit8 v1, v0, 0x10

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    move/from16 v8, p6

    :goto_3
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_4

    const/4 v1, 0x2

    new-array v1, v1, [Lx/q/b;

    const-class v3, Lcom/discord/widgets/home/WidgetHome;

    invoke-static {v3}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    invoke-static {v3}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    move-object v9, v1

    goto :goto_4

    :cond_4
    move-object/from16 v9, p7

    :goto_4
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_5

    const-wide/16 v1, 0x3a98

    move-wide v10, v1

    goto :goto_5

    :cond_5
    move-wide/from16 v10, p8

    :goto_5
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    const-wide v0, 0x757b12c00L

    move-wide v12, v0

    goto :goto_6

    :cond_6
    move-wide/from16 v12, p10

    :goto_6
    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v14, p12

    invoke-direct/range {v2 .. v14}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static synthetic getHasShown$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public canShow(Ljava/lang/Long;)Z
    .locals 5

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/discord/stores/StoreNotices$Notice;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-wide v3, p0, Lcom/discord/stores/StoreNotices$Notice;->sinceLastPeriodMs:J

    cmp-long p1, v1, v3

    if-lez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getClock()Lcom/discord/utilities/time/Clock;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreNotices$Notice;->clock:Lcom/discord/utilities/time/Clock;

    return-object v0
.end method

.method public final getDelayPeriodMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreNotices$Notice;->delayPeriodMs:J

    return-wide v0
.end method

.method public final getHasShown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNotices$Notice;->hasShown:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreNotices$Notice;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getPersist()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreNotices$Notice;->persist:Z

    return v0
.end method

.method public final getPriority()I
    .locals 1

    iget v0, p0, Lcom/discord/stores/StoreNotices$Notice;->priority:I

    return v0
.end method

.method public final getRequestedShowTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreNotices$Notice;->requestedShowTimestamp:J

    return-wide v0
.end method

.method public final getShow()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreNotices$Notice;->show:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getSinceLastPeriodMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreNotices$Notice;->sinceLastPeriodMs:J

    return-wide v0
.end method

.method public final getValidScreens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lx/q/b<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreNotices$Notice;->validScreens:Ljava/util/List;

    return-object v0
.end method

.method public final isInAppNotification()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreNotices$Notice;->name:Ljava/lang/String;

    const-string v1, "InAppNotif"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lx/s/r;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v0

    return v0
.end method

.method public final isPopup()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreNotices$Notice;->name:Ljava/lang/String;

    const-string v1, "Popup"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lx/s/r;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v0

    return v0
.end method

.method public final setHasShown(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreNotices$Notice;->hasShown:Z

    return-void
.end method

.method public shouldShow(Ljava/util/Map;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "lastShownTimes"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/discord/stores/StoreNotices$Notice;->priority:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    iget-object p1, p0, Lcom/discord/stores/StoreNotices$Notice;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    iget-wide v0, p0, Lcom/discord/stores/StoreNotices$Notice;->delayPeriodMs:J

    cmp-long p1, v0, v2

    if-gez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public final show(Landroidx/fragment/app/FragmentActivity;)Z
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "activity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/discord/stores/StoreNotices$Notice;->hasShown:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreNotices$Notice;->show:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/discord/stores/StoreNotices$Notice;->hasShown:Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Notice<"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreNotices$Notice;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ">(pri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/stores/StoreNotices$Notice;->priority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", ts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/stores/StoreNotices$Notice;->requestedShowTimestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
