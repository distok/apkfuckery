.class public final Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;
.super Lx/m/c/k;
.source "StoreVoiceChannelSelected.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreVoiceChannelSelected;->selectVoiceChannel(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $resultSubject:Lrx/subjects/PublishSubject;

.field public final synthetic this$0:Lcom/discord/stores/StoreVoiceChannelSelected;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreVoiceChannelSelected;JLrx/subjects/PublishSubject;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->this$0:Lcom/discord/stores/StoreVoiceChannelSelected;

    iput-wide p2, p0, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->$channelId:J

    iput-object p4, p0, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->$resultSubject:Lrx/subjects/PublishSubject;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->this$0:Lcom/discord/stores/StoreVoiceChannelSelected;

    iget-wide v1, p0, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->$channelId:J

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/stores/StoreVoiceChannelSelected;->access$selectVoiceChannelInternal(Lcom/discord/stores/StoreVoiceChannelSelected;JZ)Lcom/discord/stores/StoreVoiceChannelSelected$JoinVoiceChannelResult;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->$resultSubject:Lrx/subjects/PublishSubject;

    iget-object v1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceChannelSelected$selectVoiceChannel$1;->$resultSubject:Lrx/subjects/PublishSubject;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject$b;->onCompleted()V

    return-void
.end method
