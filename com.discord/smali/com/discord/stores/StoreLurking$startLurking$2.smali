.class public final Lcom/discord/stores/StoreLurking$startLurking$2;
.super Lx/m/c/k;
.source "StoreLurking.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreLurking;->startLurking(JLjava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:Ljava/lang/Long;

.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreLurking;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreLurking;JLjava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->this$0:Lcom/discord/stores/StoreLurking;

    iput-wide p2, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$guildId:J

    iput-object p4, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$channelId:Ljava/lang/Long;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreLurking$startLurking$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->this$0:Lcom/discord/stores/StoreLurking;

    invoke-static {v0}, Lcom/discord/stores/StoreLurking;->access$getSessionId$p(Lcom/discord/stores/StoreLurking;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->this$0:Lcom/discord/stores/StoreLurking;

    iget-wide v1, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$guildId:J

    iget-object v3, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$channelId:Ljava/lang/Long;

    invoke-static {v0, v1, v2, v3}, Lcom/discord/stores/StoreLurking;->access$startLurkingInternal(Lcom/discord/stores/StoreLurking;JLjava/lang/Long;)V

    goto :goto_0

    :cond_0
    const-string v0, "Queue lurk request: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$channelId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->this$0:Lcom/discord/stores/StoreLurking;

    new-instance v1, Lcom/discord/stores/StoreLurking$LurkRequest;

    iget-wide v2, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$guildId:J

    iget-object v4, p0, Lcom/discord/stores/StoreLurking$startLurking$2;->$channelId:Ljava/lang/Long;

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/stores/StoreLurking$LurkRequest;-><init>(JLjava/lang/Long;)V

    invoke-static {v0, v1}, Lcom/discord/stores/StoreLurking;->access$setLurkRequest$p(Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreLurking$LurkRequest;)V

    :goto_0
    return-void
.end method
