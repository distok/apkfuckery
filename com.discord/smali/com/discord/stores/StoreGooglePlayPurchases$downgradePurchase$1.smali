.class public final Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;
.super Lx/m/c/k;
.source "StoreGooglePlayPurchases.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGooglePlayPurchases;->downgradePurchase()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreGooglePlayPurchases;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGooglePlayPurchases;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 23

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-static {v1}, Lcom/discord/stores/StoreGooglePlayPurchases;->access$getStateSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    const-string/jumbo v2, "stateSubject"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lrx/subjects/BehaviorSubject;->i0()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/StoreGooglePlayPurchases$State;

    instance-of v2, v1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->getPendingDowngrade()Lcom/discord/stores/PendingDowngrade;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->getPendingDowngrade()Lcom/discord/stores/PendingDowngrade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/PendingDowngrade;->component1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/stores/PendingDowngrade;->component2()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/stores/PendingDowngrade;->component3()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;

    invoke-direct {v4, v3, v2, v1}, Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-static {v2}, Lcom/discord/stores/StoreGooglePlayPurchases;->access$getQueryStateSubject$p(Lcom/discord/stores/StoreGooglePlayPurchases;)Lrx/subjects/BehaviorSubject;

    move-result-object v2

    sget-object v3, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$InProgress;->INSTANCE:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$InProgress;

    invoke-virtual {v2, v3}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    sget-object v5, Lcom/discord/restapi/utils/RetryWithDelay;->INSTANCE:Lcom/discord/restapi/utils/RetryWithDelay;

    sget-object v2, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/discord/utilities/rest/RestAPI;->downgradeSubscription(Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;)Lrx/Observable;

    move-result-object v2

    invoke-static {}, Lg0/p/a;->c()Lrx/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrx/Observable;->S(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v6

    const-string v2, "RestAPI\n            .api\u2026scribeOn(Schedulers.io())"

    invoke-static {v6, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v7, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    new-instance v11, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1$1;

    iget-object v2, v0, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-direct {v11, v2}, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1$1;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases;)V

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/restapi/utils/RetryWithDelay;->restRetry$default(Lcom/discord/restapi/utils/RetryWithDelay;Lrx/Observable;JLjava/lang/Integer;Ljava/lang/Integer;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v14

    iget-object v2, v0, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;->this$0:Lcom/discord/stores/StoreGooglePlayPurchases;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v2, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1$2;

    invoke-direct {v2, v0, v1}, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1$2;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;Ljava/lang/String;)V

    const/16 v19, 0x0

    new-instance v3, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1$3;

    invoke-direct {v3, v0, v1}, Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1$3;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases$downgradePurchase$1;Ljava/lang/String;)V

    const/16 v21, 0x16

    const/16 v22, 0x0

    move-object/from16 v18, v3

    move-object/from16 v20, v2

    invoke-static/range {v14 .. v22}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method
