.class public final Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;
.super Ljava/lang/Object;
.source "StoreGatewayConnection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreGatewayConnection$ClientState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$create(Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;ZLjava/lang/String;JZLcom/discord/stores/StoreClientDataState$ClientDataState;)Lcom/discord/stores/StoreGatewayConnection$ClientState;
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;->create(ZLjava/lang/String;JZLcom/discord/stores/StoreClientDataState$ClientDataState;)Lcom/discord/stores/StoreGatewayConnection$ClientState;

    move-result-object p0

    return-object p0
.end method

.method private final create(ZLjava/lang/String;JZLcom/discord/stores/StoreClientDataState$ClientDataState;)Lcom/discord/stores/StoreGatewayConnection$ClientState;
    .locals 3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p1, :cond_2

    const-wide/16 v1, 0x0

    cmp-long p1, p3, v1

    if-gtz p1, :cond_2

    if-eqz p5, :cond_1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :cond_2
    :goto_1
    new-instance p1, Lcom/discord/stores/StoreGatewayConnection$ClientState;

    invoke-direct {p1, p2, v0, p6}, Lcom/discord/stores/StoreGatewayConnection$ClientState;-><init>(Ljava/lang/String;ZLcom/discord/stores/StoreClientDataState$ClientDataState;)V

    return-object p1
.end method


# virtual methods
.method public final initialize(Lcom/discord/stores/StoreStream;Lrx/Scheduler;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreStream;",
            "Lrx/Scheduler;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/StoreGatewayConnection$ClientState;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/miguelgaeta/backgrounded/Backgrounded;->get()Lrx/Observable;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getAuthentication$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAuthentication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreAuthentication;->getAuthedToken$app_productionDiscordExternalRelease()Lrx/Observable;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getVoiceChannelSelected$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedVoiceChannelId()Lrx/Observable;

    move-result-object v3

    sget-object v0, Lf/a/b/o;->c:Lf/a/b/o;

    sget-object v0, Lf/a/b/o;->b:Lrx/subjects/BehaviorSubject;

    sget-object v4, Lf/a/b/n;->d:Lf/a/b/n;

    invoke-virtual {v0, v4}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v4

    const-string v0, "numGatewayConnectionCons\u2026  .distinctUntilChanged()"

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream;->getClientDataState$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreClientDataState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreClientDataState;->observeClientState()Lrx/Observable;

    move-result-object v5

    new-instance p1, Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion$initialize$1;

    invoke-direct {p1, p0}, Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion$initialize$1;-><init>(Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;)V

    new-instance v6, Lcom/discord/stores/StoreGatewayConnection$sam$rx_functions_Func5$0;

    invoke-direct {v6, p1}, Lcom/discord/stores/StoreGatewayConnection$sam$rx_functions_Func5$0;-><init>(Lkotlin/jvm/functions/Function5;)V

    invoke-static/range {v1 .. v6}, Lrx/Observable;->g(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n            .\u2026   ::create\n            )"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1, p2}, Lrx/Observable;->F(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    const-string p1, "Observable\n            .\u2026    .observeOn(scheduler)"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const-string v2, "clientState"

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x35

    const/4 v8, 0x0

    move-object v4, p3

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method
