.class public final Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1;
.super Ljava/lang/Object;
.source "StoreUserGuildSettings.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreUserGuildSettings;->getMutedGuildIds()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelNotificationSettings;",
        ">;",
        "Lrx/Observable<",
        "+",
        "Ljava/util/List<",
        "Ljava/lang/Long;",
        ">;>;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1;->INSTANCE:Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1;->call(Ljava/util/Map;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/Map;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelNotificationSettings;",
            ">;)",
            "Lrx/Observable<",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->y(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1$1;->INSTANCE:Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1$2;->INSTANCE:Lcom/discord/stores/StoreUserGuildSettings$mutedGuildIds$1$2;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->a0()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
