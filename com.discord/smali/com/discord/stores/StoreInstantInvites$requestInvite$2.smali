.class public final Lcom/discord/stores/StoreInstantInvites$requestInvite$2;
.super Lx/m/c/k;
.source "StoreInstantInvites.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreInstantInvites;->requestInvite(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/error/Error;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $inviteCode:Ljava/lang/String;

.field public final synthetic $source:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreInstantInvites;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreInstantInvites;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->this$0:Lcom/discord/stores/StoreInstantInvites;

    iput-object p2, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->$inviteCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->$source:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/error/Error;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->invoke(Lcom/discord/utilities/error/Error;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/error/Error;)V
    .locals 4

    const-string v0, "error"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getType()Lcom/discord/utilities/error/Error$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/16 p1, 0xb

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->this$0:Lcom/discord/stores/StoreInstantInvites;

    iget-object v0, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->$inviteCode:Ljava/lang/String;

    sget-object v1, Lcom/discord/stores/StoreInstantInvites$InviteState$LoadFailed;->INSTANCE:Lcom/discord/stores/StoreInstantInvites$InviteState$LoadFailed;

    invoke-static {p1, v0, v1}, Lcom/discord/stores/StoreInstantInvites;->access$setChatInvites(Lcom/discord/stores/StoreInstantInvites;Ljava/lang/String;Lcom/discord/stores/StoreInstantInvites$InviteState;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    iget-object v0, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->this$0:Lcom/discord/stores/StoreInstantInvites;

    iget-object v1, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->$inviteCode:Ljava/lang/String;

    sget-object v2, Lcom/discord/stores/StoreInstantInvites$InviteState$Invalid;->INSTANCE:Lcom/discord/stores/StoreInstantInvites$InviteState$Invalid;

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreInstantInvites;->access$setChatInvites(Lcom/discord/stores/StoreInstantInvites;Ljava/lang/String;Lcom/discord/stores/StoreInstantInvites$InviteState;)V

    iget-object v0, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->$source:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/discord/stores/StoreInstantInvites$requestInvite$2;->$inviteCode:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v2

    const-string v3, "error.response"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/utilities/error/Error$Response;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error$Response;->getCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v1, v0, v2, p1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->inviteResolveFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    :goto_0
    return-void
.end method
