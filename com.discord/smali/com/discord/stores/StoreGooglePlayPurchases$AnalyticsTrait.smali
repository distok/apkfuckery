.class public final Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;
.super Ljava/lang/Object;
.source "StoreGooglePlayPurchases.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreGooglePlayPurchases;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnalyticsTrait"
.end annotation


# instance fields
.field private final locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

.field private final paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

.field private final skuId:J

.field private final storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

.field private final timestamp:J


# direct methods
.method public constructor <init>(JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V
    .locals 1

    const-string v0, "locationTrait"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeSkuTrait"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTrait"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    iput-wide p3, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    iput-object p5, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    iput-object p6, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    iput-object p7, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;
    .locals 8

    move-object v0, p0

    and-int/lit8 v1, p8, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p8, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    goto :goto_1

    :cond_1
    move-wide v3, p3

    :goto_1
    and-int/lit8 v5, p8, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    goto :goto_2

    :cond_2
    move-object v5, p5

    :goto_2
    and-int/lit8 v6, p8, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    goto :goto_3

    :cond_3
    move-object v6, p6

    :goto_3
    and-int/lit8 v7, p8, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    goto :goto_4

    :cond_4
    move-object v7, p7

    :goto_4
    move-wide p1, v1

    move-wide p3, v3

    move-object p5, v5

    move-object p6, v6

    move-object p7, v7

    invoke-virtual/range {p0 .. p7}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->copy(JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    return-wide v0
.end method

.method public final component3()Lcom/discord/utilities/analytics/Traits$Location;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    return-object v0
.end method

.method public final component4()Lcom/discord/utilities/analytics/Traits$StoreSku;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    return-object v0
.end method

.method public final component5()Lcom/discord/utilities/analytics/Traits$Payment;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    return-object v0
.end method

.method public final copy(JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;
    .locals 9

    const-string v0, "locationTrait"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeSkuTrait"

    move-object v7, p6

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTrait"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    move-object v1, v0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v8}, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;-><init>(JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;

    iget-wide v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    iget-wide v2, p1, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    iget-wide v2, p1, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    iget-object v1, p1, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    iget-object v1, p1, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    iget-object p1, p1, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLocationTrait()Lcom/discord/utilities/analytics/Traits$Location;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    return-object v0
.end method

.method public final getPaymentTrait()Lcom/discord/utilities/analytics/Traits$Payment;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    return-object v0
.end method

.method public final getSkuId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    return-wide v0
.end method

.method public final getStoreSkuTrait()Lcom/discord/utilities/analytics/Traits$StoreSku;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    return-object v0
.end method

.method public final getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/utilities/analytics/Traits$Location;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/utilities/analytics/Traits$StoreSku;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/utilities/analytics/Traits$Payment;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "AnalyticsTrait(skuId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->skuId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->timestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", locationTrait="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", storeSkuTrait="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->storeSkuTrait:Lcom/discord/utilities/analytics/Traits$StoreSku;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentTrait="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;->paymentTrait:Lcom/discord/utilities/analytics/Traits$Payment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
