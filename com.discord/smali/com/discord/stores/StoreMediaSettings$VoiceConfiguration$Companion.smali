.class public final Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;
.super Ljava/lang/Object;
.source "StoreMediaSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDEFAULT_NOISE_CANCELLATION()Z
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->access$getDEFAULT_NOISE_CANCELLATION$cp()Z

    move-result v0

    return v0
.end method

.method public final getDEFAULT_NOISE_SUPPRESSION()Z
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->access$getDEFAULT_NOISE_SUPPRESSION$cp()Z

    move-result v0

    return v0
.end method

.method public final getDEFAULT_VOICE_CONFIG()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->access$getDEFAULT_VOICE_CONFIG$cp()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v0

    return-object v0
.end method
