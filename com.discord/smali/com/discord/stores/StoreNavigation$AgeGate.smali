.class public final enum Lcom/discord/stores/StoreNavigation$AgeGate;
.super Ljava/lang/Enum;
.source "StoreNavigation.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AgeGate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/stores/StoreNavigation$AgeGate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/stores/StoreNavigation$AgeGate;

.field public static final enum NSFW_CHANNEL_AGE_GATE:Lcom/discord/stores/StoreNavigation$AgeGate;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/discord/stores/StoreNavigation$AgeGate;

    new-instance v1, Lcom/discord/stores/StoreNavigation$AgeGate;

    const-string v2, "NSFW_CHANNEL_AGE_GATE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/stores/StoreNavigation$AgeGate;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/stores/StoreNavigation$AgeGate;->NSFW_CHANNEL_AGE_GATE:Lcom/discord/stores/StoreNavigation$AgeGate;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/stores/StoreNavigation$AgeGate;->$VALUES:[Lcom/discord/stores/StoreNavigation$AgeGate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/stores/StoreNavigation$AgeGate;
    .locals 1

    const-class v0, Lcom/discord/stores/StoreNavigation$AgeGate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/stores/StoreNavigation$AgeGate;

    return-object p0
.end method

.method public static values()[Lcom/discord/stores/StoreNavigation$AgeGate;
    .locals 1

    sget-object v0, Lcom/discord/stores/StoreNavigation$AgeGate;->$VALUES:[Lcom/discord/stores/StoreNavigation$AgeGate;

    invoke-virtual {v0}, [Lcom/discord/stores/StoreNavigation$AgeGate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/stores/StoreNavigation$AgeGate;

    return-object v0
.end method
