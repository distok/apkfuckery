.class public final Lcom/discord/stores/StoreTabsNavigation;
.super Ljava/lang/Object;
.source "StoreTabsNavigation.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# instance fields
.field private final dismissTabsDialogEventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

.field private final selectedTabSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation
.end field

.field private final storeStream:Lcom/discord/stores/StoreStream;


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreStream;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStream"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreTabsNavigation;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreTabsNavigation;->storeStream:Lcom/discord/stores/StoreStream;

    sget-object p1, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    iput-object p1, p0, Lcom/discord/stores/StoreTabsNavigation;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreTabsNavigation;->selectedTabSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreTabsNavigation;->dismissTabsDialogEventSubject:Lrx/subjects/PublishSubject;

    return-void
.end method

.method public static final synthetic access$dismissTabsDialogs(Lcom/discord/stores/StoreTabsNavigation;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreTabsNavigation;->dismissTabsDialogs()V

    return-void
.end method

.method public static final synthetic access$handleTabSelection(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/widgets/tabs/NavigationTab;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreTabsNavigation;->handleTabSelection(Lcom/discord/widgets/tabs/NavigationTab;)V

    return-void
.end method

.method public static final synthetic access$notifyHomeTabSelected(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreTabsNavigation;->notifyHomeTabSelected(Lcom/discord/stores/StoreNavigation$PanelAction;)V

    return-void
.end method

.method private final dismissTabsDialogs()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->dismissTabsDialogEventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lkotlin/Unit;->a:Lkotlin/Unit;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleTabSelection(Lcom/discord/widgets/tabs/NavigationTab;)V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreTabsNavigation;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreTabsNavigation;->isDirty:Z

    return-void
.end method

.method private final notifyHomeTabSelected(Lcom/discord/stores/StoreNavigation$PanelAction;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStream;->handleHomeTabSelected(Lcom/discord/stores/StoreNavigation$PanelAction;)V

    return-void
.end method

.method public static synthetic selectHomeTab$default(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreTabsNavigation;->selectHomeTab(Lcom/discord/stores/StoreNavigation$PanelAction;Z)V

    return-void
.end method

.method public static synthetic selectTab$default(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/widgets/tabs/NavigationTab;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreTabsNavigation;->selectTab(Lcom/discord/widgets/tabs/NavigationTab;Z)V

    return-void
.end method


# virtual methods
.method public final getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    return-object v0
.end method

.method public final handlePreLogout()V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lcom/discord/stores/StoreTabsNavigation;->selectTab$default(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/widgets/tabs/NavigationTab;ZILjava/lang/Object;)V

    return-void
.end method

.method public final observeDismissTabsDialogEvent()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->dismissTabsDialogEventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "dismissTabsDialogEventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeSelectedTab()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->selectedTabSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "selectedTabSubject.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDispatchEnded()V
    .locals 2

    iget-boolean v0, p0, Lcom/discord/stores/StoreTabsNavigation;->isDirty:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->selectedTabSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreTabsNavigation;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreTabsNavigation;->isDirty:Z

    return-void
.end method

.method public final selectHomeTab(Lcom/discord/stores/StoreNavigation$PanelAction;Z)V
    .locals 2

    const-string v0, "panelAction"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreTabsNavigation$selectHomeTab$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreTabsNavigation$selectHomeTab$1;-><init>(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;Z)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final selectTab(Lcom/discord/widgets/tabs/NavigationTab;Z)V
    .locals 2

    const-string v0, "navigationTab"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreTabsNavigation;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreTabsNavigation$selectTab$1;-><init>(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/widgets/tabs/NavigationTab;Z)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
