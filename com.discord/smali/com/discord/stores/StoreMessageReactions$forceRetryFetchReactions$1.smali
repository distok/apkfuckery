.class public final Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;
.super Lx/m/c/k;
.source "StoreMessageReactions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessageReactions;->forceRetryFetchReactions(JJLcom/discord/models/domain/ModelMessageReaction$Emoji;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

.field public final synthetic $messageId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreMessageReactions;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessageReactions;JLcom/discord/models/domain/ModelMessageReaction$Emoji;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->this$0:Lcom/discord/stores/StoreMessageReactions;

    iput-wide p2, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$messageId:J

    iput-object p4, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    iput-wide p5, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$channelId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 7

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->this$0:Lcom/discord/stores/StoreMessageReactions;

    invoke-static {v0}, Lcom/discord/stores/StoreMessageReactions;->access$getReactions$p(Lcom/discord/stores/StoreMessageReactions;)Ljava/util/Map;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$messageId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreMessageReactions$EmojiResults;

    :cond_0
    iget-object v1, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->this$0:Lcom/discord/stores/StoreMessageReactions;

    iget-wide v2, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$channelId:J

    iget-wide v4, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$messageId:J

    iget-object v6, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->$emoji:Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    invoke-static/range {v1 .. v6}, Lcom/discord/stores/StoreMessageReactions;->access$fetchReactions(Lcom/discord/stores/StoreMessageReactions;JJLcom/discord/models/domain/ModelMessageReaction$Emoji;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReactions$forceRetryFetchReactions$1;->this$0:Lcom/discord/stores/StoreMessageReactions;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/discord/stores/StoreMessageReactions;->access$setDirty$p(Lcom/discord/stores/StoreMessageReactions;Z)V

    return-void
.end method
