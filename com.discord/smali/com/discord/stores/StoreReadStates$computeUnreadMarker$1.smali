.class public final Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;
.super Lx/m/c/k;
.source "StoreReadStates.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreReadStates;->computeUnreadMarker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lrx/Observable<",
        "Lcom/discord/models/application/Unread$Marker;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;->INSTANCE:Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1;->invoke(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(J)Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/application/Unread$Marker;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageAck()Lcom/discord/stores/StoreMessageAck;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreMessageAck;->get()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1$1;

    invoke-direct {v2, p1, p2}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1$1;-><init>(J)V

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageAck()Lcom/discord/stores/StoreMessageAck;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreMessageAck;->get()Lrx/Observable;

    move-result-object v3

    new-instance v4, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1$2;

    invoke-direct {v4, p1, p2}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1$2;-><init>(J)V

    invoke-virtual {v3, v4}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessagesMostRecent()Lcom/discord/stores/StoreMessagesMostRecent;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreMessagesMostRecent;->get(J)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0, v2}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1$3;

    invoke-direct {v2, p1, p2}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$1$3;-><init>(J)V

    invoke-static {v1, v3, v0, v2}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
