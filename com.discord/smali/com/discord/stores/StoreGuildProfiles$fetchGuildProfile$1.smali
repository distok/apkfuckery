.class public final Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;
.super Lx/m/c/k;
.source "StoreGuildProfiles.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGuildProfiles;->fetchGuildProfile(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreGuildProfiles;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildProfiles;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->this$0:Lcom/discord/stores/StoreGuildProfiles;

    iput-wide p2, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->$guildId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 13

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->this$0:Lcom/discord/stores/StoreGuildProfiles;

    invoke-static {v0}, Lcom/discord/stores/StoreGuildProfiles;->access$getGuildProfilesState$p(Lcom/discord/stores/StoreGuildProfiles;)Ljava/util/Map;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->$guildId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getFetchState()Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    sget-object v2, Lcom/discord/stores/StoreGuildProfiles$FetchStates;->FETCHING:Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    if-ne v0, v2, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->this$0:Lcom/discord/stores/StoreGuildProfiles;

    iget-wide v2, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->$guildId:J

    invoke-static {v0, v2, v3}, Lcom/discord/stores/StoreGuildProfiles;->access$handleGuildProfileFetchStart(Lcom/discord/stores/StoreGuildProfiles;J)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    iget-wide v2, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->$guildId:J

    invoke-virtual {v0, v2, v3}, Lcom/discord/utilities/rest/RestAPI;->getGuildPreview(J)Lrx/Observable;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;->this$0:Lcom/discord/stores/StoreGuildProfiles;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v10, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1$1;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1$1;-><init>(Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;)V

    const/4 v9, 0x0

    new-instance v8, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1$2;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1$2;-><init>(Lcom/discord/stores/StoreGuildProfiles$fetchGuildProfile$1;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
