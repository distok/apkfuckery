.class public final Lcom/discord/stores/StoreNotices$noticeQueue$2;
.super Lx/m/c/k;
.source "StoreNotices.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreNotices;-><init>(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreNotices$Notice;",
        "Ljava/lang/Comparable<",
        "*>;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreNotices$noticeQueue$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreNotices$noticeQueue$2;

    invoke-direct {v0}, Lcom/discord/stores/StoreNotices$noticeQueue$2;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreNotices$noticeQueue$2;->INSTANCE:Lcom/discord/stores/StoreNotices$noticeQueue$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/stores/StoreNotices$Notice;)Ljava/lang/Comparable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreNotices$Notice;",
            ")",
            "Ljava/lang/Comparable<",
            "*>;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/stores/StoreNotices$Notice;->getPriority()I

    move-result p1

    neg-int p1, p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreNotices$Notice;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreNotices$noticeQueue$2;->invoke(Lcom/discord/stores/StoreNotices$Notice;)Ljava/lang/Comparable;

    move-result-object p1

    return-object p1
.end method
