.class public final Lcom/discord/stores/StoreAudioDevices$Companion;
.super Ljava/lang/Object;
.source "StoreAudioDevices.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreAudioDevices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreAudioDevices$Companion;-><init>()V

    return-void
.end method

.method public static synthetic getDEFAULT_AUDIO_DEVICES_STATE$app_productionDiscordExternalRelease$annotations()V
    .locals 0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    return-void
.end method

.method public static synthetic getDEFAULT_OUTPUT_STATE$app_productionDiscordExternalRelease$annotations()V
    .locals 0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    return-void
.end method


# virtual methods
.method public final getDEFAULT_AUDIO_DEVICES_STATE$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreAudioDevices;->access$getDEFAULT_AUDIO_DEVICES_STATE$cp()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object v0

    return-object v0
.end method

.method public final getDEFAULT_OUTPUT_STATE$app_productionDiscordExternalRelease()Lcom/discord/utilities/media/AudioOutputState;
    .locals 1

    invoke-static {}, Lcom/discord/stores/StoreAudioDevices;->access$getDEFAULT_OUTPUT_STATE$cp()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v0

    return-object v0
.end method
