.class public final Lcom/discord/stores/StoreAuthentication$dispatchLogin$1;
.super Lx/m/c/k;
.source "StoreAuthentication.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAuthentication;->dispatchLogin(Lcom/discord/models/domain/auth/ModelLoginResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $loginResult:Lcom/discord/models/domain/auth/ModelLoginResult;

.field public final synthetic this$0:Lcom/discord/stores/StoreAuthentication;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAuthentication;Lcom/discord/models/domain/auth/ModelLoginResult;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAuthentication$dispatchLogin$1;->this$0:Lcom/discord/stores/StoreAuthentication;

    iput-object p2, p0, Lcom/discord/stores/StoreAuthentication$dispatchLogin$1;->$loginResult:Lcom/discord/models/domain/auth/ModelLoginResult;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreAuthentication$dispatchLogin$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreAuthentication$dispatchLogin$1;->this$0:Lcom/discord/stores/StoreAuthentication;

    invoke-static {v0}, Lcom/discord/stores/StoreAuthentication;->access$getStoreStream$p(Lcom/discord/stores/StoreAuthentication;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreAuthentication$dispatchLogin$1;->$loginResult:Lcom/discord/models/domain/auth/ModelLoginResult;

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStream;->handleLoginResult(Lcom/discord/models/domain/auth/ModelLoginResult;)V

    return-void
.end method
