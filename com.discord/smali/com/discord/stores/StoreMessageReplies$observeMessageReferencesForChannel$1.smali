.class public final Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;
.super Lx/m/c/k;
.source "StoreMessageReplies.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMessageReplies;->observeMessageReferencesForChannel(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/stores/StoreMessageReplies$MessageState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreMessageReplies;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMessageReplies;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;->this$0:Lcom/discord/stores/StoreMessageReplies;

    iput-wide p2, p0, Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;->$channelId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;->invoke()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMessageReplies$MessageState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;->this$0:Lcom/discord/stores/StoreMessageReplies;

    iget-wide v1, p0, Lcom/discord/stores/StoreMessageReplies$observeMessageReferencesForChannel$1;->$channelId:J

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreMessageReplies;->access$getCachedChannelMessages(Lcom/discord/stores/StoreMessageReplies;J)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
