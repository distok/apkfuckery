.class public final Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;
.super Lcom/discord/stores/StoreStickers$StickerPackState;
.source "StoreStickers.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreStickers$StickerPackState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;


# direct methods
.method public constructor <init>(Lcom/discord/models/sticker/dto/ModelStickerPack;)V
    .locals 1

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreStickers$StickerPackState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;Lcom/discord/models/sticker/dto/ModelStickerPack;ILjava/lang/Object;)Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->copy(Lcom/discord/models/sticker/dto/ModelStickerPack;)Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/sticker/dto/ModelStickerPack;)Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;
    .locals 1

    const-string/jumbo v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    invoke-direct {v0, p1}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iget-object p1, p1, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Loaded(stickerPack="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
