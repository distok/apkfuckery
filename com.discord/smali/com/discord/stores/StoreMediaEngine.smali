.class public final Lcom/discord/stores/StoreMediaEngine;
.super Lcom/discord/stores/Store;
.source "StoreMediaEngine.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreMediaEngine$EngineListener;,
        Lcom/discord/stores/StoreMediaEngine$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreMediaEngine$Companion;

.field private static final DEFAULT_OPENSLES_CONFIG:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

.field private static final DEFAULT_VIDEO_DEVICE_GUID:Ljava/lang/String; = ""

.field private static final LOCAL_VOICE_STATUS_DEFAULT:Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private hasNativeEngineEverInitialized:Z

.field private hasNativeEngineEverInitializedCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isNativeEngineInitializedSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final localVoiceStatus:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final localVoiceStatusSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

.field private mediaEngineSettingsSubscription:Lrx/Subscription;

.field private final mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

.field private final onKrispStatusSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/rtcconnection/KrispOveruseDetector$Status;",
            ">;"
        }
    .end annotation
.end field

.field private final openSLESConfigSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;",
            ">;"
        }
    .end annotation
.end field

.field private preferredVideoInputDeviceGUID:Ljava/lang/String;

.field private final preferredVideoInputDeviceGuidCache:Lcom/discord/utilities/persister/Persister;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/persister/Persister<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private previousVoiceChannelId:Ljava/lang/Long;

.field private final pttActiveSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

.field private final selectedVideoInputDeviceSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation
.end field

.field private final storeStream:Lcom/discord/stores/StoreStream;

.field private userId:J

.field private videoInputDevices:[Lco/discord/media_engine/VideoInputDeviceDescription;

.field private final videoInputDevicesSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/discord/stores/StoreMediaEngine$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreMediaEngine$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreMediaEngine;->Companion:Lcom/discord/stores/StoreMediaEngine$Companion;

    new-instance v0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;

    const/high16 v1, -0x3d380000    # -100.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;-><init>(FZ)V

    sput-object v0, Lcom/discord/stores/StoreMediaEngine;->LOCAL_VOICE_STATUS_DEFAULT:Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;

    sget-object v0, Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;->DEFAULT:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

    sput-object v0, Lcom/discord/stores/StoreMediaEngine;->DEFAULT_OPENSLES_CONFIG:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V
    .locals 2

    const-string v0, "mediaSettingsStore"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStream"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->storeStream:Lcom/discord/stores/StoreStream;

    iput-object p3, p0, Lcom/discord/stores/StoreMediaEngine;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance p1, Lrx/subjects/SerializedSubject;

    sget-object p2, Lcom/discord/stores/StoreMediaEngine;->LOCAL_VOICE_STATUS_DEFAULT:Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    invoke-direct {p1, p2}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->localVoiceStatusSubject:Lrx/subjects/SerializedSubject;

    new-instance p2, Lrx/subjects/SerializedSubject;

    sget-object p3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p3}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    invoke-direct {p2, v0}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->pttActiveSubject:Lrx/subjects/SerializedSubject;

    const-string p2, ""

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGUID:Ljava/lang/String;

    new-instance p2, Lcom/discord/utilities/persister/Persister;

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGUID:Ljava/lang/String;

    const-string v1, "PREFERRED_VIDEO_INPUT_DEVICE_GUID"

    invoke-direct {p2, v1, v0}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGuidCache:Lcom/discord/utilities/persister/Persister;

    iget-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDeviceSubject:Lrx/subjects/BehaviorSubject;

    const/4 p2, 0x0

    new-array p2, p2, [Lco/discord/media_engine/VideoInputDeviceDescription;

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevices:[Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-static {p2}, Lx/h/f;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevicesSubject:Lrx/subjects/BehaviorSubject;

    new-instance p2, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    invoke-direct {p2, v0}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->openSLESConfigSubject:Lrx/subjects/SerializedSubject;

    new-instance p2, Lrx/subjects/SerializedSubject;

    invoke-static {p3}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p3

    invoke-direct {p2, p3}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->isNativeEngineInitializedSubject:Lrx/subjects/SerializedSubject;

    const-wide/16 p2, -0x1

    iput-wide p2, p0, Lcom/discord/stores/StoreMediaEngine;->userId:J

    new-instance p2, Lcom/discord/utilities/persister/Persister;

    iget-boolean p3, p0, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitialized:Z

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    const-string v0, "CACHE_KEY_NATIVE_ENGINE_EVER_INITIALIZED"

    invoke-direct {p2, v0, p3}, Lcom/discord/utilities/persister/Persister;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitializedCache:Lcom/discord/utilities/persister/Persister;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->onKrispStatusSubject:Lrx/subjects/PublishSubject;

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreMediaEngine$localVoiceStatus$1;

    invoke-direct {p2, p0}, Lcom/discord/stores/StoreMediaEngine$localVoiceStatus$1;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    new-instance p3, Lcom/discord/stores/StoreMediaEngine$sam$rx_functions_Action0$0;

    invoke-direct {p3, p2}, Lcom/discord/stores/StoreMediaEngine$sam$rx_functions_Action0$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    new-instance p2, Lg0/l/a/y0;

    invoke-direct {p2, p3}, Lg0/l/a/y0;-><init>(Lrx/functions/Action0;)V

    new-instance p3, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {p3, p1, p2}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {p3}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreMediaEngine$localVoiceStatus$2;

    invoke-direct {p2, p0}, Lcom/discord/stores/StoreMediaEngine$localVoiceStatus$2;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    new-instance p3, Lcom/discord/stores/StoreMediaEngine$sam$rx_functions_Action0$0;

    invoke-direct {p3, p2}, Lcom/discord/stores/StoreMediaEngine$sam$rx_functions_Action0$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Lrx/Observable;->t(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->M()Lrx/Observable;

    move-result-object p1

    const-string p2, "localVoiceStatusSubject\n\u2026ening)\n          .share()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->localVoiceStatus:Lrx/Observable;

    return-void
.end method

.method public static final synthetic access$disableLocalVoiceStatusListening(Lcom/discord/stores/StoreMediaEngine;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreMediaEngine;->disableLocalVoiceStatusListening()V

    return-void
.end method

.method public static final synthetic access$enableLocalVoiceStatusListening(Lcom/discord/stores/StoreMediaEngine;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreMediaEngine;->enableLocalVoiceStatusListening()V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreMediaEngine;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMediaEngine;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getMediaEngineSettingsSubscription$p(Lcom/discord/stores/StoreMediaEngine;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngineSettingsSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getMediaSettingsStore$p(Lcom/discord/stores/StoreMediaEngine;)Lcom/discord/stores/StoreMediaSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    return-object p0
.end method

.method public static final synthetic access$getOnKrispStatusSubject$p(Lcom/discord/stores/StoreMediaEngine;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMediaEngine;->onKrispStatusSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$getPreferredVideoInputDeviceGUID$p(Lcom/discord/stores/StoreMediaEngine;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGUID:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getSelectedVideoInputDevice$p(Lcom/discord/stores/StoreMediaEngine;)Lco/discord/media_engine/VideoInputDeviceDescription;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    return-object p0
.end method

.method public static final synthetic access$handleNativeEngineInitialized(Lcom/discord/stores/StoreMediaEngine;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreMediaEngine;->handleNativeEngineInitialized()V

    return-void
.end method

.method public static final synthetic access$handleNewConnection(Lcom/discord/stores/StoreMediaEngine;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMediaEngine;->handleNewConnection(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V

    return-void
.end method

.method public static final synthetic access$handleVideoInputDevices(Lcom/discord/stores/StoreMediaEngine;[Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreMediaEngine;->handleVideoInputDevices([Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$handleVoiceConfigChanged(Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMediaEngine;->handleVoiceConfigChanged(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V

    return-void
.end method

.method public static final synthetic access$setMediaEngineSettingsSubscription$p(Lcom/discord/stores/StoreMediaEngine;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngineSettingsSubscription:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setPreferredVideoInputDeviceGUID$p(Lcom/discord/stores/StoreMediaEngine;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGUID:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$setSelectedVideoInputDevice$p(Lcom/discord/stores/StoreMediaEngine;Lco/discord/media_engine/VideoInputDeviceDescription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    return-void
.end method

.method private final declared-synchronized disableLocalVoiceStatusListening()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->n(Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "mediaEngine"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final declared-synchronized enableLocalVoiceStatusListening()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/stores/StoreMediaEngine$enableLocalVoiceStatusListening$1;

    iget-object v2, p0, Lcom/discord/stores/StoreMediaEngine;->localVoiceStatusSubject:Lrx/subjects/SerializedSubject;

    invoke-direct {v1, v2}, Lcom/discord/stores/StoreMediaEngine$enableLocalVoiceStatusListening$1;-><init>(Lrx/subjects/SerializedSubject;)V

    invoke-interface {v0, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->n(Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "mediaEngine"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final declared-synchronized getVideoInputDevicesNative(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-[",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/stores/StoreMediaEngine$getVideoInputDevicesNative$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreMediaEngine$getVideoInputDevicesNative$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-interface {v0, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->k(Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string p1, "mediaEngine"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x0

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleNativeEngineInitialized()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitialized:Z

    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitializedCache:Lcom/discord/utilities/persister/Persister;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v0}, Lcom/discord/utilities/persister/Persister;->set(Ljava/lang/Object;Z)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->isNativeEngineInitializedSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, v2}, Lg0/n/c;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final declared-synchronized handleNewConnection(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/discord/stores/StoreMediaEngine;->setupMediaEngineSettingsSubscription()V

    new-instance v0, Lcom/discord/stores/StoreMediaEngine$handleNewConnection$1;

    invoke-direct {v0, p0}, Lcom/discord/stores/StoreMediaEngine$handleNewConnection$1;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    invoke-interface {p1, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->j(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;)V

    new-instance p1, Lcom/discord/stores/StoreMediaEngine$handleNewConnection$2;

    invoke-direct {p1, p0}, Lcom/discord/stores/StoreMediaEngine$handleNewConnection$2;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreMediaEngine;->getVideoInputDevicesNative(Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final declared-synchronized handleVideoInputDevices([Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, -0x1

    if-ge v2, v0, :cond_1

    aget-object v4, p1, v2

    invoke-virtual {v4}, Lco/discord/media_engine/VideoInputDeviceDescription;->getGuid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    if-ltz v2, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v4, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    const/4 v5, 0x0

    if-eqz v4, :cond_c

    invoke-interface {v4, v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->f(I)V

    iget-object v3, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v3, :cond_b

    invoke-interface {v3, v2}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->f(I)V

    iget-object v3, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v3, :cond_a

    invoke-interface {v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->getConnections()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    invoke-interface {v4}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->getType()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_3

    :cond_3
    invoke-interface {v4, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->g(Z)V

    goto :goto_3

    :cond_4
    if-eqz p3, :cond_6

    if-eqz v0, :cond_5

    goto :goto_4

    :cond_5
    move-object p2, v5

    :goto_4
    invoke-interface {p3, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/Unit;

    :cond_6
    if-eqz v0, :cond_7

    aget-object p2, p1, v2

    goto :goto_5

    :cond_7
    move-object p2, v5

    :goto_5
    invoke-direct {p0, p2}, Lcom/discord/stores/StoreMediaEngine;->updateSelectedVideoInputDevice(Lco/discord/media_engine/VideoInputDeviceDescription;)V

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevices:[Lco/discord/media_engine/VideoInputDeviceDescription;

    iget-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevicesSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {p1}, Lx/h/f;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    if-eqz p1, :cond_9

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lco/discord/media_engine/VideoInputDeviceDescription;->getGuid()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_8

    goto :goto_6

    :cond_8
    const-string p1, ""

    :goto_6
    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGUID:Ljava/lang/String;

    iget-object p2, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGuidCache:Lcom/discord/utilities/persister/Persister;

    const/4 p3, 0x2

    invoke-static {p2, p1, v1, p3, v5}, Lcom/discord/utilities/persister/Persister;->set$default(Lcom/discord/utilities/persister/Persister;Ljava/lang/Object;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_9
    monitor-exit p0

    return-void

    :cond_a
    :try_start_1
    const-string p1, "mediaEngine"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    :cond_b
    :try_start_2
    const-string p1, "mediaEngine"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    :cond_c
    :try_start_3
    const-string p1, "mediaEngine"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static synthetic handleVideoInputDevices$default(Lcom/discord/stores/StoreMediaEngine;[Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreMediaEngine;->handleVideoInputDevices([Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final declared-synchronized handleVoiceConfigChanged(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    sget-object v0, Lcom/discord/stores/StoreMediaEngine;->Companion:Lcom/discord/stores/StoreMediaEngine$Companion;

    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v1, :cond_0

    invoke-static {v0, v1, p1}, Lcom/discord/stores/StoreMediaEngine$Companion;->access$setVoiceConfig(Lcom/discord/stores/StoreMediaEngine$Companion;Lcom/discord/rtcconnection/mediaengine/MediaEngine;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V

    goto :goto_0

    :cond_0
    const-string p1, "mediaEngine"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x0

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_1
    :goto_0
    monitor-exit p0

    return-void
.end method

.method public static synthetic selectDefaultVideoDevice$default(Lcom/discord/stores/StoreMediaEngine;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMediaEngine;->selectDefaultVideoDevice(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final declared-synchronized setupMediaEngineSettingsSubscription()V
    .locals 10

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngineSettingsSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings;->getVoiceConfig()Lrx/Observable;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v7, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$1;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    sget-object v5, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$2;->INSTANCE:Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$2;

    const/4 v6, 0x0

    new-instance v4, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$3;

    invoke-direct {v4, p0}, Lcom/discord/stores/StoreMediaEngine$setupMediaEngineSettingsSubscription$3;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    const/16 v8, 0x12

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private final declared-synchronized updateSelectedVideoInputDevice(Lco/discord/media_engine/VideoInputDeviceDescription;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDeviceSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->storeStream:Lcom/discord/stores/StoreStream;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreStream;->handleVideoInputDeviceSelected(Lco/discord/media_engine/VideoInputDeviceDescription;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final declared-synchronized cycleVideoInputDevice()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevices:[Lco/discord/media_engine/VideoInputDeviceDescription;

    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevices:[Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-static {v1}, Lf/h/a/f/f/n/g;->getLastIndex([Ljava/lang/Object;)I

    move-result v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevices:[Lco/discord/media_engine/VideoInputDeviceDescription;

    aget-object v0, v1, v0

    invoke-virtual {v0}, Lco/discord/media_engine/VideoInputDeviceDescription;->getGuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/stores/StoreMediaEngine;->selectVideoInputDevice(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getIsNativeEngineInitialized()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->isNativeEngineInitializedSubject:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final getLocalVoiceStatus()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->localVoiceStatus:Lrx/Observable;

    return-object v0
.end method

.method public final getMediaEngine()Lcom/discord/rtcconnection/mediaengine/MediaEngine;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "mediaEngine"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final getOpenSLESConfig()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->openSLESConfigSubject:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method public final declared-synchronized getRankedRtcRegions(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelRtcLatencyRegion;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "regionsWithIps"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelRtcLatencyRegion;

    new-instance v3, Lco/discord/media_engine/RtcRegion;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelRtcLatencyRegion;->getRegion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelRtcLatencyRegion;->getIps()Ljava/util/List;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, [Ljava/lang/String;

    invoke-direct {v3, v4, v1}, Lco/discord/media_engine/RtcRegion;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-array p1, v2, [Lco/discord/media_engine/RtcRegion;

    invoke-interface {v0, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, [Lco/discord/media_engine/RtcRegion;

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v0, :cond_2

    new-instance v1, Lcom/discord/stores/StoreMediaEngine$getRankedRtcRegions$1;

    invoke-direct {v1, p2}, Lcom/discord/stores/StoreMediaEngine$getRankedRtcRegions$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-interface {v0, p1, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->b([Lco/discord/media_engine/RtcRegion;Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    const-string p1, "mediaEngine"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x0

    throw p1

    :cond_3
    :try_start_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final getSelectedVideoInputDevice()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDeviceSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "selectedVideoInputDevice\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getSelectedVideoInputDeviceBlocking()Lco/discord/media_engine/VideoInputDeviceDescription;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->selectedVideoInputDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    return-object v0
.end method

.method public final getVideoInputDevices()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->videoInputDevicesSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "videoInputDevicesSubject\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final declared-synchronized getVoiceEngineNative()Lcom/hammerandchisel/libdiscord/Discord;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->j()Lcom/hammerandchisel/libdiscord/Discord;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "mediaEngine"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    const-string v0, "payload.me"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/stores/StoreMediaEngine;->userId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized handleVoiceChannelSelected(J)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->previousVoiceChannelId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_1

    :goto_0
    cmp-long v0, p1, v1

    if-nez v0, :cond_1

    new-instance v0, Lcom/discord/stores/StoreMediaEngine$handleVoiceChannelSelected$1;

    invoke-direct {v0, p0}, Lcom/discord/stores/StoreMediaEngine$handleVoiceChannelSelected$1;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreMediaEngine;->getVideoInputDevicesNative(Lkotlin/jvm/functions/Function1;)V

    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreMediaEngine;->previousVoiceChannelId:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized hasNativeEngineEverInitialized()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 10

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGuidCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->preferredVideoInputDeviceGUID:Ljava/lang/String;

    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitializedCache:Lcom/discord/utilities/persister/Persister;

    invoke-virtual {v1}, Lcom/discord/utilities/persister/Persister;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/discord/stores/StoreMediaEngine;->hasNativeEngineEverInitialized:Z

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefsSessionDurable()Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Lcom/discord/stores/StoreMediaEngine;->DEFAULT_OPENSLES_CONFIG:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "OPEN_SLES"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const-string v2, "prefsSessionDurable\n    \u2026AULT_OPENSLES_CONFIG.name"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;->valueOf(Ljava/lang/String;)Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

    move-result-object v6

    iget-object v1, p0, Lcom/discord/stores/StoreMediaEngine;->openSLESConfigSubject:Lrx/subjects/SerializedSubject;

    iget-object v1, v1, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v1, v6}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    const-string v1, "Executors.newSingleThreadExecutor()"

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "singleThreadExecutorService"

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openSLESConfig"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logger"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/h/t/c/m;

    const/4 v8, 0x0

    const/16 v9, 0x10

    move-object v3, v0

    move-object v4, p1

    invoke-direct/range {v3 .. v9}, Lf/a/h/t/c/m;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;Lcom/discord/utilities/logging/Logger;Ljava/util/HashSet;I)V

    new-instance p1, Lcom/discord/stores/StoreMediaEngine$EngineListener;

    invoke-direct {p1, p0}, Lcom/discord/stores/StoreMediaEngine$EngineListener;-><init>(Lcom/discord/stores/StoreMediaEngine;)V

    const-string v1, "listener"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lf/a/h/t/c/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    return-void
.end method

.method public final onKrispStatusEvent()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/KrispOveruseDetector$Status;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->onKrispStatusSubject:Lrx/subjects/PublishSubject;

    const-string v1, "onKrispStatusSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final selectDefaultVideoDevice(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/discord/stores/StoreMediaEngine$selectDefaultVideoDevice$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/stores/StoreMediaEngine$selectDefaultVideoDevice$1;-><init>(Lcom/discord/stores/StoreMediaEngine;Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreMediaEngine;->getVideoInputDevicesNative(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final selectVideoInputDevice(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreMediaEngine$selectVideoInputDevice$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/stores/StoreMediaEngine$selectVideoInputDevice$1;-><init>(Lcom/discord/stores/StoreMediaEngine;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreMediaEngine;->getVideoInputDevicesNative(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final declared-synchronized setOpenSLESConfig(Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ApplySharedPref"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "openSLESConfig"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->openSLESConfigSubject:Lrx/subjects/SerializedSubject;

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefsSessionDurable()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "OPEN_SLES"

    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized setPttActive(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->mediaEngine:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    invoke-interface {v1, p1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->n(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreMediaEngine;->pttActiveSubject:Lrx/subjects/SerializedSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, p1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string p1, "mediaEngine"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x0

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
