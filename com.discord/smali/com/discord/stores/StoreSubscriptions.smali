.class public final Lcom/discord/stores/StoreSubscriptions;
.super Ljava/lang/Object;
.source "StoreSubscriptions.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

.field private final subscriptionsStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreSubscriptions;->dispatcher:Lcom/discord/stores/Dispatcher;

    sget-object p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Unfetched;->INSTANCE:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Unfetched;

    iput-object p1, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsStateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreSubscriptions;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreSubscriptions;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getSubscriptionsState$p(Lcom/discord/stores/StoreSubscriptions;)Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-object p0
.end method

.method public static final synthetic access$handleSubscriptionsFetchFailure(Lcom/discord/stores/StoreSubscriptions;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreSubscriptions;->handleSubscriptionsFetchFailure()V

    return-void
.end method

.method public static final synthetic access$handleSubscriptionsFetchStart(Lcom/discord/stores/StoreSubscriptions;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreSubscriptions;->handleSubscriptionsFetchStart()V

    return-void
.end method

.method public static final synthetic access$handleSubscriptionsFetchSuccess(Lcom/discord/stores/StoreSubscriptions;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreSubscriptions;->handleSubscriptionsFetchSuccess(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$setSubscriptionsState$p(Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-void
.end method

.method private final handleSubscriptionsFetchFailure()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;->INSTANCE:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    iput-object v0, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    return-void
.end method

.method private final handleSubscriptionsFetchStart()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;->INSTANCE:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    iput-object v0, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    return-void
.end method

.method private final handleSubscriptionsFetchSuccess(Ljava/util/List;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-direct {v0, p1}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    return-void
.end method


# virtual methods
.method public final fetchSubscriptions()V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreSubscriptions;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;

    invoke-direct {v1, p0}, Lcom/discord/stores/StoreSubscriptions$fetchSubscriptions$1;-><init>(Lcom/discord/stores/StoreSubscriptions;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final getSubscriptions()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "subscriptionsStateSubjec\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final handlePreLogout()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Unfetched;->INSTANCE:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Unfetched;

    iput-object v0, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    return-void
.end method

.method public final handleUserSubscriptionsUpdate()V
    .locals 0
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    invoke-virtual {p0}, Lcom/discord/stores/StoreSubscriptions;->fetchSubscriptions()V

    return-void
.end method

.method public final isDirty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    return v0
.end method

.method public onDispatchEnded()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsStateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreSubscriptions;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final setDirty(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreSubscriptions;->isDirty:Z

    return-void
.end method
