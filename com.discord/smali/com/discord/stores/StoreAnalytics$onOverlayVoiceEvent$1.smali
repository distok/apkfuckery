.class public final Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;
.super Lx/m/c/k;
.source "StoreAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAnalytics;->onOverlayVoiceEvent(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isActive:Z

.field public final synthetic this$0:Lcom/discord/stores/StoreAnalytics;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAnalytics;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    iput-boolean p2, p0, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->$isActive:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 6

    sget-object v0, Lx/h/m;->d:Lx/h/m;

    iget-object v1, p0, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v1}, Lcom/discord/stores/StoreAnalytics;->access$getStores$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/stores/StoreStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream;->getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection;->getRtcConnection$app_productionDiscordExternalRelease()Lcom/discord/rtcconnection/RtcConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/discord/utilities/analytics/AnalyticsUtils;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsUtils;

    invoke-virtual {v2, v1}, Lcom/discord/utilities/analytics/AnalyticsUtils;->getProperties$app_productionDiscordExternalRelease(Lcom/discord/rtcconnection/RtcConnection;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iget-boolean v2, p0, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->$isActive:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v2}, Lcom/discord/stores/StoreAnalytics;->access$getSelectedVoiceChannel$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    const-string v4, "it.guildId"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/discord/stores/StoreAnalytics;->access$getGuildProperties(Lcom/discord/stores/StoreAnalytics;J)Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v2, v0

    :goto_1
    invoke-static {v2, v1}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/stores/StoreAnalytics$onOverlayVoiceEvent$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    invoke-static {v2}, Lcom/discord/stores/StoreAnalytics;->access$getSelectedVoiceChannel$p(Lcom/discord/stores/StoreAnalytics;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    if-eqz v2, :cond_2

    sget-object v3, Lcom/discord/utilities/analytics/AnalyticsUtils;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsUtils;

    invoke-virtual {v3, v2}, Lcom/discord/utilities/analytics/AnalyticsUtils;->getProperties$app_productionDiscordExternalRelease(Lcom/discord/models/domain/ModelChannel;)Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object v0, v2

    :cond_2
    invoke-static {v1, v0}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v1, v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->overlayVoiceOpened(Ljava/util/Map;)V

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->overlayVoiceClosed(Ljava/util/Map;)V

    :goto_2
    return-void
.end method
