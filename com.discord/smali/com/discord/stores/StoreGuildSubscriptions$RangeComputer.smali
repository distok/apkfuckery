.class public final Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;
.super Ljava/lang/Object;
.source "StoreGuildSubscriptions.kt"


# annotations
.annotation build Landroidx/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreGuildSubscriptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RangeComputer"
.end annotation


# static fields
.field private static final DEFAULT_CHUNK_SIZE:I = 0x64

.field public static final INSTANCE:Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;

    invoke-direct {v0}, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;->INSTANCE:Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic computeRanges$default(Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;Lkotlin/ranges/IntRange;IILjava/lang/Object;)Ljava/util/List;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/16 p2, 0x64

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;->computeRanges(Lkotlin/ranges/IntRange;I)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final computeRanges(Lkotlin/ranges/IntRange;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/ranges/IntRange;",
            "I)",
            "Ljava/util/List<",
            "Lkotlin/ranges/IntRange;",
            ">;"
        }
    .end annotation

    const-string v0, "range"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v1, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer$computeRanges$1;->INSTANCE:Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer$computeRanges$1;

    iget v2, p1, Lkotlin/ranges/IntProgression;->d:I

    invoke-virtual {v1, v2, p2}, Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer$computeRanges$1;->invoke(II)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v2, 0x0

    invoke-static {v2, p2}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget p1, p1, Lkotlin/ranges/IntProgression;->e:I

    invoke-static {v1, p1}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object p1

    invoke-static {p1, p2}, Lx/p/e;->step(Lkotlin/ranges/IntProgression;I)Lkotlin/ranges/IntProgression;

    move-result-object p1

    iget v1, p1, Lkotlin/ranges/IntProgression;->d:I

    iget v2, p1, Lkotlin/ranges/IntProgression;->e:I

    iget p1, p1, Lkotlin/ranges/IntProgression;->f:I

    if-ltz p1, :cond_1

    if-gt v1, v2, :cond_2

    goto :goto_0

    :cond_1
    if-lt v1, v2, :cond_2

    :goto_0
    add-int v3, v1, p2

    invoke-static {v1, v3}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eq v1, v2, :cond_2

    add-int/2addr v1, p1

    goto :goto_0

    :cond_2
    return-object v0
.end method
