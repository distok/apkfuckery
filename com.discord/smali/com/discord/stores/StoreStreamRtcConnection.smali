.class public final Lcom/discord/stores/StoreStreamRtcConnection;
.super Ljava/lang/Object;
.source "StoreStreamRtcConnection.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreStreamRtcConnection$State;,
        Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;,
        Lcom/discord/stores/StoreStreamRtcConnection$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreStreamRtcConnection$Companion;

.field public static final MAX_STREAM_VOLUME:F = 300.0f


# instance fields
.field private final analyticsStore:Lcom/discord/stores/StoreAnalytics;

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private isDirty:Z

.field private final mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

.field private networkMonitor:Lcom/discord/utilities/networking/NetworkMonitor;

.field private rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

.field private sessionId:Ljava/lang/String;

.field private state:Lcom/discord/stores/StoreStreamRtcConnection$State;

.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/stores/StoreStreamRtcConnection$State;",
            ">;"
        }
    .end annotation
.end field

.field private final storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

.field private final storeStream:Lcom/discord/stores/StoreStream;

.field private streamOwner:Ljava/lang/Long;

.field private streamVolume:F

.field private final streamVolumeSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final userStore:Lcom/discord/stores/StoreUser;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreStreamRtcConnection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreStreamRtcConnection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreStreamRtcConnection;->Companion:Lcom/discord/stores/StoreStreamRtcConnection$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreRtcConnection;)V
    .locals 1

    const-string v0, "mediaEngineStore"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userStore"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStream"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsStore"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeRtcConnection"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    iput-object p2, p0, Lcom/discord/stores/StoreStreamRtcConnection;->userStore:Lcom/discord/stores/StoreUser;

    iput-object p3, p0, Lcom/discord/stores/StoreStreamRtcConnection;->storeStream:Lcom/discord/stores/StoreStream;

    iput-object p4, p0, Lcom/discord/stores/StoreStreamRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p5, p0, Lcom/discord/stores/StoreStreamRtcConnection;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p6, p0, Lcom/discord/stores/StoreStreamRtcConnection;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    iput-object p7, p0, Lcom/discord/stores/StoreStreamRtcConnection;->storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

    new-instance p1, Lcom/discord/stores/StoreStreamRtcConnection$State;

    new-instance p2, Lcom/discord/rtcconnection/RtcConnection$State$d;

    const/4 p3, 0x0

    invoke-direct {p2, p3}, Lcom/discord/rtcconnection/RtcConnection$State$d;-><init>(Z)V

    iget-object p3, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    const/4 p4, 0x0

    invoke-direct {p1, p2, p4, p4, p3}, Lcom/discord/stores/StoreStreamRtcConnection$State;-><init>(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;)V

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->stateSubject:Lrx/subjects/BehaviorSubject;

    const/high16 p1, 0x43960000    # 300.0f

    iput p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolume:F

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolumeSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getAnalyticsStore$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/StoreAnalytics;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getRtcConnection$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/rtcconnection/RtcConnection;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    return-object p0
.end method

.method public static final synthetic access$getStoreStream$p(Lcom/discord/stores/StoreStreamRtcConnection;)Lcom/discord/stores/StoreStream;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->storeStream:Lcom/discord/stores/StoreStream;

    return-object p0
.end method

.method public static final synthetic access$getStreamOwner$p(Lcom/discord/stores/StoreStreamRtcConnection;)Ljava/lang/Long;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamOwner:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getStreamVolume$p(Lcom/discord/stores/StoreStreamRtcConnection;)F
    .locals 0

    iget p0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolume:F

    return p0
.end method

.method public static final synthetic access$handleMediaSessionIdReceived(Lcom/discord/stores/StoreStreamRtcConnection;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreStreamRtcConnection;->handleMediaSessionIdReceived()V

    return-void
.end method

.method public static final synthetic access$handleQualityUpdate(Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/rtcconnection/RtcConnection$Quality;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->handleQualityUpdate(Lcom/discord/rtcconnection/RtcConnection$Quality;)V

    return-void
.end method

.method public static final synthetic access$handleVideoStreamEndedAnalyticsEvent(Lcom/discord/stores/StoreStreamRtcConnection;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->handleVideoStreamEndedAnalyticsEvent(Ljava/util/Map;)V

    return-void
.end method

.method public static final synthetic access$isDirty$p(Lcom/discord/stores/StoreStreamRtcConnection;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    return p0
.end method

.method public static final synthetic access$setDirty$p(Lcom/discord/stores/StoreStreamRtcConnection;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    return-void
.end method

.method public static final synthetic access$setRtcConnection$p(Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/rtcconnection/RtcConnection;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    return-void
.end method

.method public static final synthetic access$setStreamOwner$p(Lcom/discord/stores/StoreStreamRtcConnection;Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamOwner:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$setStreamVolume$p(Lcom/discord/stores/StoreStreamRtcConnection;F)V
    .locals 0

    iput p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolume:F

    return-void
.end method

.method private final createRtcConnection(JLjava/lang/Long;JLjava/lang/String;Ljava/lang/String;J)Lcom/discord/rtcconnection/RtcConnection;
    .locals 22
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Lcom/discord/stores/StoreStreamRtcConnection;->destroyRtcConnection()V

    iget-object v1, v0, Lcom/discord/stores/StoreStreamRtcConnection;->storeRtcConnection:Lcom/discord/stores/StoreRtcConnection;

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection;->getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getChannelId()Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v5, v3, p4

    if-nez v5, :cond_2

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getMediaSessionId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v20, v1

    goto :goto_2

    :cond_2
    :goto_1
    move-object/from16 v20, v2

    :goto_2
    new-instance v1, Lcom/discord/rtcconnection/RtcConnection;

    const/4 v10, 0x1

    iget-object v3, v0, Lcom/discord/stores/StoreStreamRtcConnection;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v3}, Lcom/discord/stores/StoreMediaEngine;->getMediaEngine()Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    move-result-object v14

    sget-object v15, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    iget-object v3, v0, Lcom/discord/stores/StoreStreamRtcConnection;->clock:Lcom/discord/utilities/time/Clock;

    new-instance v4, Lcom/discord/rtcconnection/RtcConnection$c$b;

    move-wide/from16 v5, p8

    invoke-direct {v4, v5, v6}, Lcom/discord/rtcconnection/RtcConnection$c$b;-><init>(J)V

    iget-object v12, v0, Lcom/discord/stores/StoreStreamRtcConnection;->networkMonitor:Lcom/discord/utilities/networking/NetworkMonitor;

    if-eqz v12, :cond_3

    const/16 v19, 0x0

    const/16 v21, 0x800

    move-object v5, v1

    move-object/from16 v6, p3

    move-wide/from16 v7, p4

    move-object/from16 v9, p6

    move-object/from16 v11, p7

    move-object v2, v12

    move-wide/from16 v12, p1

    move-object/from16 v16, v3

    move-object/from16 v17, v4

    move-object/from16 v18, v2

    invoke-direct/range {v5 .. v21}, Lcom/discord/rtcconnection/RtcConnection;-><init>(Ljava/lang/Long;JLjava/lang/String;ZLjava/lang/String;JLcom/discord/rtcconnection/mediaengine/MediaEngine;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/time/Clock;Lcom/discord/rtcconnection/RtcConnection$c;Lcom/discord/utilities/networking/NetworkMonitor;Ljava/util/Map;Ljava/lang/String;I)V

    new-instance v2, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;

    invoke-direct {v2, v0}, Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;-><init>(Lcom/discord/stores/StoreStreamRtcConnection;)V

    invoke-virtual {v1, v2}, Lcom/discord/rtcconnection/RtcConnection;->b(Lcom/discord/rtcconnection/RtcConnection$b;)V

    return-object v1

    :cond_3
    const-string v1, "networkMonitor"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method private final destroyRtcConnection()V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/stores/StoreStreamRtcConnection;->Companion:Lcom/discord/stores/StoreStreamRtcConnection$Companion;

    const-string v1, "destroying stream rtc connection"

    invoke-static {v0, v1}, Lcom/discord/stores/StoreStreamRtcConnection$Companion;->access$recordBreadcrumb(Lcom/discord/stores/StoreStreamRtcConnection$Companion;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_0

    new-instance v1, Lf/a/h/i;

    invoke-direct {v1, v0}, Lf/a/h/i;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreStreamRtcConnection;->updateRtcConnection(Lcom/discord/rtcconnection/RtcConnection;)V

    iget-object v1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x9

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/stores/StoreStreamRtcConnection$State;->copy$default(Lcom/discord/stores/StoreStreamRtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;ILjava/lang/Object;)Lcom/discord/stores/StoreStreamRtcConnection$State;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    :cond_1
    return-void
.end method

.method private final handleMediaSessionIdReceived()V
    .locals 8
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/discord/rtcconnection/RtcConnection;->t:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v4, v0

    iget-object v1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/stores/StoreStreamRtcConnection$State;->copy$default(Lcom/discord/stores/StoreStreamRtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;ILjava/lang/Object;)Lcom/discord/stores/StoreStreamRtcConnection$State;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    return-void
.end method

.method private final handleQualityUpdate(Lcom/discord/rtcconnection/RtcConnection$Quality;)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xd

    const/4 v6, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/discord/stores/StoreStreamRtcConnection$State;->copy$default(Lcom/discord/stores/StoreStreamRtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;ILjava/lang/Object;)Lcom/discord/stores/StoreStreamRtcConnection$State;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    return-void
.end method

.method private final handleVideoStreamEndedAnalyticsEvent(Ljava/util/Map;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->trackVideoStreamEnded(Ljava/util/Map;)V

    return-void
.end method

.method private final updateRtcConnection(Lcom/discord/rtcconnection/RtcConnection;)V
    .locals 7
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v4, p1

    invoke-static/range {v0 .. v6}, Lcom/discord/stores/StoreStreamRtcConnection$State;->copy$default(Lcom/discord/stores/StoreStreamRtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;ILjava/lang/Object;)Lcom/discord/stores/StoreStreamRtcConnection$State;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    return-void
.end method


# virtual methods
.method public final handleConnectionOpen(Lcom/discord/models/domain/ModelPayload;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPayload;->getSessionId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->sessionId:Ljava/lang/String;

    return-void
.end method

.method public final handleStreamCreate(Lcom/discord/models/domain/StreamCreateOrUpdate;)V
    .locals 13
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamCreate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->userStore:Lcom/discord/stores/StoreUser;

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    iget-object v9, p0, Lcom/discord/stores/StoreStreamRtcConnection;->sessionId:Ljava/lang/String;

    if-eqz v1, :cond_6

    if-nez v9, :cond_1

    goto :goto_4

    :cond_1
    iget-object v3, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v3, :cond_3

    iget-wide v3, v3, Lcom/discord/rtcconnection/RtcConnection;->x:J

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-nez v7, :cond_3

    iget-object v3, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v3, :cond_2

    iget-object v3, v3, Lcom/discord/rtcconnection/RtcConnection;->y:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v3, v2

    :goto_1
    invoke-static {v3, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    return-void

    :cond_3
    instance-of v3, v0, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    if-eqz v3, :cond_4

    move-object v2, v0

    check-cast v2, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream$GuildStream;->getGuildId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_2
    move-object v6, v2

    goto :goto_3

    :cond_4
    instance-of v3, v0, Lcom/discord/models/domain/ModelApplicationStream$CallStream;

    if-eqz v3, :cond_5

    goto :goto_2

    :goto_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v7

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamCreateOrUpdate;->getRtcServerId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v11

    move-object v3, p0

    invoke-direct/range {v3 .. v12}, Lcom/discord/stores/StoreStreamRtcConnection;->createRtcConnection(JLjava/lang/Long;JLjava/lang/String;Ljava/lang/String;J)Lcom/discord/rtcconnection/RtcConnection;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->updateRtcConnection(Lcom/discord/rtcconnection/RtcConnection;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamOwner:Ljava/lang/Long;

    return-void

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_6
    :goto_4
    return-void
.end method

.method public final handleStreamDelete()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1, v1}, Lcom/discord/rtcconnection/RtcConnection;->m(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/stores/StoreStreamRtcConnection;->destroyRtcConnection()V

    iput-object v1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamOwner:Ljava/lang/Long;

    return-void
.end method

.method public final handleStreamRtcConnectionStateChange(Lcom/discord/rtcconnection/RtcConnection$State;)V
    .locals 11
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamOwner:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolume:F

    invoke-virtual {v2, v0, v1, v3}, Lcom/discord/rtcconnection/RtcConnection;->o(JF)V

    :cond_0
    iget-object v4, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    move-object v5, p1

    invoke-static/range {v4 .. v10}, Lcom/discord/stores/StoreStreamRtcConnection$State;->copy$default(Lcom/discord/stores/StoreStreamRtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;ILjava/lang/Object;)Lcom/discord/stores/StoreStreamRtcConnection$State;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    return-void
.end method

.method public final handleStreamServerUpdate(Lcom/discord/models/domain/StreamServerUpdate;)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamServerUpdate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/app/App;->e:Lcom/discord/app/App;

    sget-boolean v0, Lcom/discord/app/App;->d:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v1, v0, v1}, Lcom/discord/utilities/ssl/SecureSocketsLayerUtils;->createSocketFactory$default(Ljavax/net/ssl/TrustManagerFactory;ILjava/lang/Object;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    :goto_0
    sget-object v0, Lcom/discord/stores/StoreStreamRtcConnection;->Companion:Lcom/discord/stores/StoreStreamRtcConnection$Companion;

    const-string v2, "Voice stream update, connect to server w/ endpoint: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamServerUpdate;->getEndpoint()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/discord/stores/StoreStreamRtcConnection$Companion;->access$recordBreadcrumb(Lcom/discord/stores/StoreStreamRtcConnection$Companion;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->rtcConnection:Lcom/discord/rtcconnection/RtcConnection;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamServerUpdate;->getEndpoint()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/StreamServerUpdate;->getToken()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Lf/a/h/h;

    invoke-direct {v3, v0, v2, p1, v1}, Lf/a/h/h;-><init>(Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;)V

    invoke-virtual {v0, v3}, Lcom/discord/rtcconnection/RtcConnection;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    :cond_1
    return-void
.end method

.method public final init(Lcom/discord/utilities/networking/NetworkMonitor;)V
    .locals 1

    const-string v0, "networkMonitor"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->networkMonitor:Lcom/discord/utilities/networking/NetworkMonitor;

    return-void
.end method

.method public final observeRtcConnection()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/RtcConnection;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->stateSubject:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/discord/stores/StoreStreamRtcConnection$observeRtcConnection$1;->INSTANCE:Lcom/discord/stores/StoreStreamRtcConnection$observeRtcConnection$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string/jumbo v1, "stateSubject.map { state -> state.rtcConnection }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final observeStreamVolume()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolumeSubject:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v1, "streamVolumeSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDispatchEnded()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->stateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->state:Lcom/discord/stores/StoreStreamRtcConnection$State;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolumeSubject:Lrx/subjects/BehaviorSubject;

    iget v1, p0, Lcom/discord/stores/StoreStreamRtcConnection;->streamVolume:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->isDirty:Z

    :cond_0
    return-void
.end method

.method public final updateStreamVolume(F)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StoreStreamRtcConnection;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreStreamRtcConnection$updateStreamVolume$1;-><init>(Lcom/discord/stores/StoreStreamRtcConnection;F)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
