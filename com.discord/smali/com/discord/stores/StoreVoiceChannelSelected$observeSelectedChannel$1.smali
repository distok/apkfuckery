.class public final Lcom/discord/stores/StoreVoiceChannelSelected$observeSelectedChannel$1;
.super Lx/m/c/k;
.source "StoreVoiceChannelSelected.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedChannel()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreVoiceChannelSelected;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreVoiceChannelSelected;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreVoiceChannelSelected$observeSelectedChannel$1;->this$0:Lcom/discord/stores/StoreVoiceChannelSelected;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/models/domain/ModelChannel;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreVoiceChannelSelected$observeSelectedChannel$1;->this$0:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-static {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->access$getStream$p(Lcom/discord/stores/StoreVoiceChannelSelected;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getChannels$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreVoiceChannelSelected$observeSelectedChannel$1;->this$0:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-static {v1}, Lcom/discord/stores/StoreVoiceChannelSelected;->access$getSelectedVoiceChannelId$p(Lcom/discord/stores/StoreVoiceChannelSelected;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreChannels;->getChannel$app_productionDiscordExternalRelease(J)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreVoiceChannelSelected$observeSelectedChannel$1;->invoke()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    return-object v0
.end method
