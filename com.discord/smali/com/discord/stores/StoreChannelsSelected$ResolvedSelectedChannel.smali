.class public abstract Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;
.super Ljava/lang/Object;
.source "StoreChannelsSelected.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreChannelsSelected;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ResolvedSelectedChannel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;,
        Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;,
        Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unavailable;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;-><init>()V

    return-void
.end method


# virtual methods
.method public final getId()J
    .locals 2

    instance-of v0, p0, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;->INSTANCE:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;

    invoke-static {p0, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unavailable;->INSTANCE:Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unavailable;

    invoke-static {p0, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
