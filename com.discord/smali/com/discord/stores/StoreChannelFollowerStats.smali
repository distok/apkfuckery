.class public final Lcom/discord/stores/StoreChannelFollowerStats;
.super Lcom/discord/stores/StoreV2;
.source "StoreChannelFollowerStats.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreChannelFollowerStats$FetchState;,
        Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;,
        Lcom/discord/stores/StoreChannelFollowerStats$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/StoreChannelFollowerStats$Companion;

.field private static final STALE_TIME_DIFF_MS:I = 0x36ee80


# instance fields
.field private final channelFollowerStatsState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;",
            ">;"
        }
    .end annotation
.end field

.field private channelFollowerStatsStateSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;",
            ">;"
        }
    .end annotation
.end field

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreChannelFollowerStats$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreChannelFollowerStats$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreChannelFollowerStats;->Companion:Lcom/discord/stores/StoreChannelFollowerStats$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelFollowerStats;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreChannelFollowerStats;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsStateSnapshot:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$getChannelFollowerStatsState$p(Lcom/discord/stores/StoreChannelFollowerStats;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getChannelFollowerStatsStateSnapshot$p(Lcom/discord/stores/StoreChannelFollowerStats;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsStateSnapshot:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreChannelFollowerStats;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$handleChannelFollowerStatsFetchFailed(Lcom/discord/stores/StoreChannelFollowerStats;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats;->handleChannelFollowerStatsFetchFailed(J)V

    return-void
.end method

.method public static final synthetic access$handleChannelFollowerStatsFetchStart(Lcom/discord/stores/StoreChannelFollowerStats;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats;->handleChannelFollowerStatsFetchStart(J)V

    return-void
.end method

.method public static final synthetic access$handleChannelFollowerStatsFetchSuccess(Lcom/discord/stores/StoreChannelFollowerStats;JLcom/discord/models/domain/ModelChannelFollowerStats;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreChannelFollowerStats;->handleChannelFollowerStatsFetchSuccess(JLcom/discord/models/domain/ModelChannelFollowerStats;)V

    return-void
.end method

.method public static final synthetic access$isExisting(Lcom/discord/stores/StoreChannelFollowerStats;J)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats;->isExisting(J)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isStale(Lcom/discord/stores/StoreChannelFollowerStats;J)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats;->isStale(J)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setChannelFollowerStatsStateSnapshot$p(Lcom/discord/stores/StoreChannelFollowerStats;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsStateSnapshot:Ljava/util/Map;

    return-void
.end method

.method private final fetchIfNonexistingOrStale(J)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreChannelFollowerStats$fetchIfNonexistingOrStale$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats$fetchIfNonexistingOrStale$1;-><init>(Lcom/discord/stores/StoreChannelFollowerStats;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleChannelFollowerStatsFetchFailed(J)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    sget-object v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->FAILED:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {p2, v1, v2, v3, v2}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;-><init>(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleChannelFollowerStatsFetchStart(J)V
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    sget-object v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->FETCHING:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {p2, v1, v2, v3, v2}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;-><init>(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final handleChannelFollowerStatsFetchSuccess(JLcom/discord/models/domain/ModelChannelFollowerStats;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    sget-object v1, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->SUCCEEDED:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    invoke-direct {p2, v1, p3}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;-><init>(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method private final isExisting(J)Z
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->getFetchState()Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    move-result-object p1

    sget-object p2, Lcom/discord/stores/StoreChannelFollowerStats$FetchState;->FAILED:Lcom/discord/stores/StoreChannelFollowerStats$FetchState;

    if-eq p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isStale(J)Z
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;->getData()Lcom/discord/models/domain/ModelChannelFollowerStats;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannelFollowerStats;->getLastFetched()J

    move-result-wide p1

    const v0, 0x36ee80

    int-to-long v0, v0

    add-long/2addr p1, v0

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method


# virtual methods
.method public final fetchChannelFollowerStats(J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats;->fetchIfNonexistingOrStale(J)V

    return-void
.end method

.method public final observeChannelFollowerStats(J)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/models/domain/ModelChannelFollowerStats;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreChannelFollowerStats$observeChannelFollowerStats$1;

    invoke-direct {v5, p0, p1, p2}, Lcom/discord/stores/StoreChannelFollowerStats$observeChannelFollowerStats$1;-><init>(Lcom/discord/stores/StoreChannelFollowerStats;J)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "observationDeck\n        \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public snapshotData()V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsState:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreChannelFollowerStats;->channelFollowerStatsStateSnapshot:Ljava/util/Map;

    return-void
.end method
