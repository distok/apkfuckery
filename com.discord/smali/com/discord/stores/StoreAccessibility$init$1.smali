.class public final Lcom/discord/stores/StoreAccessibility$init$1;
.super Lx/m/c/k;
.source "StoreAccessibility.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAccessibility;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/accessibility/AccessibilityState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreAccessibility;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAccessibility;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAccessibility$init$1;->this$0:Lcom/discord/stores/StoreAccessibility;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/accessibility/AccessibilityState;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreAccessibility$init$1;->invoke(Lcom/discord/utilities/accessibility/AccessibilityState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/accessibility/AccessibilityState;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreAccessibility$init$1;->this$0:Lcom/discord/stores/StoreAccessibility;

    invoke-static {v0}, Lcom/discord/stores/StoreAccessibility;->access$getDispatcher$p(Lcom/discord/stores/StoreAccessibility;)Lcom/discord/stores/Dispatcher;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreAccessibility$init$1$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreAccessibility$init$1$1;-><init>(Lcom/discord/stores/StoreAccessibility$init$1;Lcom/discord/utilities/accessibility/AccessibilityState;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
