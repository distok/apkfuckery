.class public final enum Lcom/discord/stores/FailedMessageResolutionType;
.super Ljava/lang/Enum;
.source "StoreMessages.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/stores/FailedMessageResolutionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/stores/FailedMessageResolutionType;

.field public static final enum DELETED:Lcom/discord/stores/FailedMessageResolutionType;

.field public static final enum RESENT:Lcom/discord/stores/FailedMessageResolutionType;


# instance fields
.field private final analyticsValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/stores/FailedMessageResolutionType;

    new-instance v1, Lcom/discord/stores/FailedMessageResolutionType;

    const-string v2, "RESENT"

    const/4 v3, 0x0

    const-string v4, "Resent"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/stores/FailedMessageResolutionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/stores/FailedMessageResolutionType;->RESENT:Lcom/discord/stores/FailedMessageResolutionType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/stores/FailedMessageResolutionType;

    const-string v2, "DELETED"

    const/4 v3, 0x1

    const-string v4, "Deleted"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/stores/FailedMessageResolutionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/stores/FailedMessageResolutionType;->DELETED:Lcom/discord/stores/FailedMessageResolutionType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/stores/FailedMessageResolutionType;->$VALUES:[Lcom/discord/stores/FailedMessageResolutionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/stores/FailedMessageResolutionType;->analyticsValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/stores/FailedMessageResolutionType;
    .locals 1

    const-class v0, Lcom/discord/stores/FailedMessageResolutionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/stores/FailedMessageResolutionType;

    return-object p0
.end method

.method public static values()[Lcom/discord/stores/FailedMessageResolutionType;
    .locals 1

    sget-object v0, Lcom/discord/stores/FailedMessageResolutionType;->$VALUES:[Lcom/discord/stores/FailedMessageResolutionType;

    invoke-virtual {v0}, [Lcom/discord/stores/FailedMessageResolutionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/stores/FailedMessageResolutionType;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/FailedMessageResolutionType;->analyticsValue:Ljava/lang/String;

    return-object v0
.end method
