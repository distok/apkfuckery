.class public final Lcom/discord/stores/StorePermissions$observePermissionsForGuild$1;
.super Lx/m/c/k;
.source "StorePermissions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StorePermissions;->observePermissionsForGuild(J)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/stores/StorePermissions;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePermissions;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StorePermissions$observePermissionsForGuild$1;->this$0:Lcom/discord/stores/StorePermissions;

    iput-wide p2, p0, Lcom/discord/stores/StorePermissions$observePermissionsForGuild$1;->$guildId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Long;
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StorePermissions$observePermissionsForGuild$1;->this$0:Lcom/discord/stores/StorePermissions;

    invoke-static {v0}, Lcom/discord/stores/StorePermissions;->access$getPermissionsForGuildsSnapshot$p(Lcom/discord/stores/StorePermissions;)Ljava/util/Map;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/stores/StorePermissions$observePermissionsForGuild$1;->$guildId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StorePermissions$observePermissionsForGuild$1;->invoke()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
