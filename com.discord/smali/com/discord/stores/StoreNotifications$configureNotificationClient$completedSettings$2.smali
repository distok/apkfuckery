.class public final Lcom/discord/stores/StoreNotifications$configureNotificationClient$completedSettings$2;
.super Ljava/lang/Object;
.source "StoreNotifications.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreNotifications;->configureNotificationClient()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/util/HashSet<",
        "Ljava/lang/Long;",
        ">;",
        "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreNotifications$configureNotificationClient$completedSettings$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreNotifications$configureNotificationClient$completedSettings$2;

    invoke-direct {v0}, Lcom/discord/stores/StoreNotifications$configureNotificationClient$completedSettings$2;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreNotifications$configureNotificationClient$completedSettings$2;->INSTANCE:Lcom/discord/stores/StoreNotifications$configureNotificationClient$completedSettings$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;"
        }
    .end annotation

    const-string v0, "locale"

    move-object/from16 v9, p3

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nonSendableChannelIds"

    move-object/from16 v10, p4

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v11, 0x3f

    const/4 v12, 0x0

    move-object v1, p1

    move-object v8, p2

    invoke-static/range {v1 .. v12}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->copy$default(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;ZZZZZZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;ILjava/lang/Object;)Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Ljava/lang/String;

    check-cast p4, Ljava/util/HashSet;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StoreNotifications$configureNotificationClient$completedSettings$2;->call(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;)Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;

    move-result-object p1

    return-object p1
.end method
