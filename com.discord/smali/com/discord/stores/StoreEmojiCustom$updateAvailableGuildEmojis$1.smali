.class public final Lcom/discord/stores/StoreEmojiCustom$updateAvailableGuildEmojis$1;
.super Lx/m/c/k;
.source "StoreEmojiCustom.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreEmojiCustom;->updateAvailableGuildEmojis(JLjava/util/List;Ljava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/emoji/ModelEmojiCustom;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $available:Ljava/util/Map;

.field public final synthetic this$0:Lcom/discord/stores/StoreEmojiCustom;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreEmojiCustom;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreEmojiCustom$updateAvailableGuildEmojis$1;->this$0:Lcom/discord/stores/StoreEmojiCustom;

    iput-object p2, p0, Lcom/discord/stores/StoreEmojiCustom$updateAvailableGuildEmojis$1;->$available:Ljava/util/Map;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    check-cast p2, Lcom/discord/models/domain/emoji/ModelEmojiCustom;

    invoke-virtual {p0, v0, v1, p2}, Lcom/discord/stores/StoreEmojiCustom$updateAvailableGuildEmojis$1;->invoke(JLcom/discord/models/domain/emoji/ModelEmojiCustom;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(JLcom/discord/models/domain/emoji/ModelEmojiCustom;)V
    .locals 3

    const-string v0, "emoji"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreEmojiCustom$updateAvailableGuildEmojis$1;->$available:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_0

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    check-cast p2, Ljava/util/Map;

    invoke-virtual {p3}, Lcom/discord/models/domain/emoji/ModelEmojiCustom;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/emoji/ModelEmojiCustom;

    invoke-static {p3, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v2, 0x1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p2, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/stores/StoreEmojiCustom$updateAvailableGuildEmojis$1;->this$0:Lcom/discord/stores/StoreEmojiCustom;

    invoke-static {p1, v2}, Lcom/discord/stores/StoreEmojiCustom;->access$setDirty$p(Lcom/discord/stores/StoreEmojiCustom;Z)V

    :cond_1
    return-void
.end method
