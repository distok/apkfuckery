.class public final Lcom/discord/stores/StoreGifPicker$observeGifCategories$2;
.super Lx/m/c/k;
.source "StoreGifPicker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGifPicker;->observeGifCategories()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreGifPicker;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGifPicker;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGifPicker$observeGifCategories$2;->this$0:Lcom/discord/stores/StoreGifPicker;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreGifPicker$observeGifCategories$2;->invoke()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/domain/ModelGifCategory;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGifPicker$observeGifCategories$2;->this$0:Lcom/discord/stores/StoreGifPicker;

    invoke-static {v0}, Lcom/discord/stores/StoreGifPicker;->access$getGifCategories(Lcom/discord/stores/StoreGifPicker;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
