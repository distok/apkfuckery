.class public final synthetic Lcom/discord/stores/StoreAudioManager$init$1;
.super Lx/m/c/i;
.source "StoreAudioManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAudioManager;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/discord/rtcconnection/RtcConnection$State;",
        "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
        "Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreAudioManager$init$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreAudioManager$init$1;

    invoke-direct {v0}, Lcom/discord/stores/StoreAudioManager$init$1;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreAudioManager$init$1;->INSTANCE:Lcom/discord/stores/StoreAudioManager$init$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;

    const/4 v1, 0x2

    const-string v3, "<init>"

    const-string v4, "<init>(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;-><init>(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/rtcconnection/RtcConnection$State;

    check-cast p2, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreAudioManager$init$1;->invoke(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)Lcom/discord/stores/StoreAudioManager$MediaEngineAudioManager$Configuration;

    move-result-object p1

    return-object p1
.end method
