.class public final Lcom/discord/stores/StoreStream$deferredInit$1$11;
.super Ljava/lang/Object;
.source "StoreStream.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStream$deferredInit$1;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Boolean;",
        "Lrx/Observable<",
        "+",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/stores/StoreStream$deferredInit$1$11;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/stores/StoreStream$deferredInit$1$11;

    invoke-direct {v0}, Lcom/discord/stores/StoreStream$deferredInit$1$11;-><init>()V

    sput-object v0, Lcom/discord/stores/StoreStream$deferredInit$1$11;->INSTANCE:Lcom/discord/stores/StoreStream$deferredInit$1$11;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreStream$deferredInit$1$11;->call(Ljava/lang/Boolean;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Boolean;)Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lrx/Observable<",
            "+",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/SlowTtiExperimentManager;->Companion:Lcom/discord/stores/SlowTtiExperimentManager$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$Companion;->getINSTANCE()Lcom/discord/stores/SlowTtiExperimentManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager;->getExperimentStatus()Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus;

    move-result-object v0

    const-string v1, "isInitialized"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, v0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->getBucket()I

    move-result v1

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->getRevision()I

    move-result v2

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->getPopulation()I

    move-result v3

    const-string v4, "2020-08_android_tti_delay"

    invoke-static {v4, v2, v3, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->userExperimentTriggered(Ljava/lang/String;III)V

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;->getDelayMs()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    new-instance v2, Lg0/l/e/j;

    invoke-direct {v2, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, p1}, Lrx/Observable;->p(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    goto :goto_1

    :cond_0
    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_0
    move-object p1, v0

    :goto_1
    return-object p1
.end method
