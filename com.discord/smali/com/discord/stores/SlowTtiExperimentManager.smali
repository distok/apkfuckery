.class public final Lcom/discord/stores/SlowTtiExperimentManager;
.super Ljava/lang/Object;
.source "SlowTtiExperimentManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus;,
        Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;,
        Lcom/discord/stores/SlowTtiExperimentManager$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/stores/SlowTtiExperimentManager$Companion;

.field private static final INSTANCE$delegate:Lkotlin/Lazy;


# instance fields
.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private ttiExperiment:Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/SlowTtiExperimentManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/SlowTtiExperimentManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/SlowTtiExperimentManager;->Companion:Lcom/discord/stores/SlowTtiExperimentManager$Companion;

    sget-object v0, Lcom/discord/stores/SlowTtiExperimentManager$Companion$INSTANCE$2;->INSTANCE:Lcom/discord/stores/SlowTtiExperimentManager$Companion$INSTANCE$2;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/discord/stores/SlowTtiExperimentManager;->INSTANCE$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/discord/utilities/cache/SharedPreferencesProvider;->INSTANCE:Lcom/discord/utilities/cache/SharedPreferencesProvider;

    invoke-virtual {v0}, Lcom/discord/utilities/cache/SharedPreferencesProvider;->get()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/SlowTtiExperimentManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "SLOW_TTI_EXPERIMENT_V2"

    invoke-static {v0, v1}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->getString(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    const-class v2, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/Gson;->f(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2}, Lf/h/a/f/f/n/g;->k0(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    iput-object v0, p0, Lcom/discord/stores/SlowTtiExperimentManager;->ttiExperiment:Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    :cond_0
    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lkotlin/Lazy;
    .locals 1

    sget-object v0, Lcom/discord/stores/SlowTtiExperimentManager;->INSTANCE$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final synthetic access$writeExperimentToCache(Lcom/discord/stores/SlowTtiExperimentManager;Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/SlowTtiExperimentManager;->writeExperimentToCache(Lcom/discord/models/experiments/domain/Experiment;)V

    return-void
.end method

.method private final writeExperimentToCache(Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 5

    iget-object v0, p0, Lcom/discord/stores/SlowTtiExperimentManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    new-instance v2, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    invoke-virtual {p1}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v3

    invoke-virtual {p1}, Lcom/discord/models/experiments/domain/Experiment;->getPopulation()I

    move-result v4

    invoke-virtual {p1}, Lcom/discord/models/experiments/domain/Experiment;->getRevision()I

    move-result p1

    invoke-direct {v2, v3, v4, p1}, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;-><init>(III)V

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->k(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "SLOW_TTI_EXPERIMENT_V2"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public final fetchExperiment(Lcom/discord/stores/StoreExperiments;)V
    .locals 10

    const-string/jumbo v0, "storeExperiments"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    new-instance v6, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$1;

    invoke-direct {v6, p1}, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$1;-><init>(Lcom/discord/stores/StoreExperiments;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {p1, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {p1, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v1, "filter { it != null }.map { it!! }"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$2;

    invoke-direct {v0, p0}, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$2;-><init>(Lcom/discord/stores/SlowTtiExperimentManager;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->s(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    const-string v0, "ObservationDeckProvider.\u2026che(experiment)\n        }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/stores/SlowTtiExperimentManager;

    sget-object v7, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$3;->INSTANCE:Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$3;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final getExperimentStatus()Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus;
    .locals 8

    iget-object v0, p0, Lcom/discord/stores/SlowTtiExperimentManager;->ttiExperiment:Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->getBucket()I

    move-result v1

    invoke-static {}, Lcom/discord/stores/SlowTtiExperimentManagerKt;->access$getExperimentBucketToDelayMs$p()Ljava/util/Map;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    if-nez v1, :cond_0

    sget-object v0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserNotInExperiment;->INSTANCE:Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserNotInExperiment;

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->getBucket()I

    move-result v4

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->getRevision()I

    move-result v6

    invoke-virtual {v0}, Lcom/discord/stores/SlowTtiExperimentManager$TtiExperiment;->getPopulation()I

    move-result v5

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserInExperiment;-><init>(JIII)V

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserNotInExperiment;->INSTANCE:Lcom/discord/stores/SlowTtiExperimentManager$ExperimentStatus$UserNotInExperiment;

    return-object v0
.end method
