.class public final Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;
.super Lx/m/c/k;
.source "StoreStickers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStickers;->fetchOwnedStickerPacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreStickers;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 13

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    sget-object v1, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loading;->INSTANCE:Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loading;

    invoke-static {v0, v1}, Lcom/discord/stores/StoreStickers;->access$setOwnedStickerPackState$p(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;)V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreV2;->markChanged()V

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-static {v0}, Lcom/discord/stores/StoreStickers;->access$getApi$p(Lcom/discord/stores/StoreStickers;)Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI;->getMyStickerPacks()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;->this$0:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    new-instance v8, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$1;

    invoke-direct {v8, p0}, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$1;-><init>(Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;)V

    new-instance v10, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;

    invoke-direct {v10, p0}, Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1$2;-><init>(Lcom/discord/stores/StoreStickers$fetchOwnedStickerPacks$1;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
