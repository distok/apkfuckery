.class public final Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;
.super Lx/m/c/k;
.source "StoreChannelMembers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreChannelMembers;->handleGuildRoleUpdate(Lcom/discord/models/domain/ModelGuildRole$Payload;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $allowOwnerIndicator$inlined:Z

.field public final synthetic $guildId$inlined:J

.field public final synthetic this$0:Lcom/discord/stores/StoreChannelMembers;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreChannelMembers;JZ)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;->this$0:Lcom/discord/stores/StoreChannelMembers;

    iput-wide p2, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;->$guildId$inlined:J

    iput-boolean p4, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;->$allowOwnerIndicator$inlined:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(J)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;
    .locals 6

    iget-object v0, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;->this$0:Lcom/discord/stores/StoreChannelMembers;

    iget-wide v1, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;->$guildId$inlined:J

    iget-boolean v5, p0, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;->$allowOwnerIndicator$inlined:Z

    move-wide v3, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/stores/StoreChannelMembers;->access$makeMember(Lcom/discord/stores/StoreChannelMembers;JJZ)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/stores/StoreChannelMembers$handleGuildRoleUpdate$$inlined$forEach$lambda$1;->invoke(J)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList$Row;

    move-result-object p1

    return-object p1
.end method
