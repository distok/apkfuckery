.class public final Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;
.super Lcom/discord/stores/StoreGooglePlayPurchases$State;
.source "StoreGooglePlayPurchases.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreGooglePlayPurchases$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

.field private final purchases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;",
            "Lcom/discord/stores/PendingDowngrade;",
            ")V"
        }
    .end annotation

    const-string v0, "purchases"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/stores/StoreGooglePlayPurchases$State;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lcom/discord/stores/PendingDowngrade;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;Ljava/util/List;Lcom/discord/stores/PendingDowngrade;ILjava/lang/Object;)Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->copy(Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/PendingDowngrade;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;",
            "Lcom/discord/stores/PendingDowngrade;",
            ")",
            "Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;"
        }
    .end annotation

    const-string v0, "purchases"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    invoke-direct {v0, p1, p2}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;-><init>(Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    iget-object p1, p1, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPendingDowngrade()Lcom/discord/stores/PendingDowngrade;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    return-object v0
.end method

.method public final getPurchases()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/stores/PendingDowngrade;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Loaded(purchases="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->purchases:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pendingDowngrade="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->pendingDowngrade:Lcom/discord/stores/PendingDowngrade;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
