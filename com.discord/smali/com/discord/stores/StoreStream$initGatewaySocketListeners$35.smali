.class public final Lcom/discord/stores/StoreStream$initGatewaySocketListeners$35;
.super Lx/m/c/k;
.source "StoreStream.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreStream;->initGatewaySocketListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelPresence;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreStream;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStream;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$35;->this$0:Lcom/discord/stores/StoreStream;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$35;->invoke(Lcom/discord/models/domain/ModelPresence;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelPresence;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/stores/StoreStream$initGatewaySocketListeners$35;->this$0:Lcom/discord/stores/StoreStream;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence;->getGuildId()J

    move-result-wide v1

    const-string v3, "it"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, v2, p1}, Lcom/discord/stores/StoreStream;->access$handlePresenceUpdate(Lcom/discord/stores/StoreStream;JLcom/discord/models/domain/ModelPresence;)V

    return-void
.end method
