.class public final Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$2;
.super Ljava/lang/Object;
.source "SlowTtiExperimentManager.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/SlowTtiExperimentManager;->fetchExperiment(Lcom/discord/stores/StoreExperiments;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/models/experiments/domain/Experiment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/SlowTtiExperimentManager;


# direct methods
.method public constructor <init>(Lcom/discord/stores/SlowTtiExperimentManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$2;->this$0:Lcom/discord/stores/SlowTtiExperimentManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/experiments/domain/Experiment;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$2;->this$0:Lcom/discord/stores/SlowTtiExperimentManager;

    const-string v1, "experiment"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/discord/stores/SlowTtiExperimentManager;->access$writeExperimentToCache(Lcom/discord/stores/SlowTtiExperimentManager;Lcom/discord/models/experiments/domain/Experiment;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/models/experiments/domain/Experiment;

    invoke-virtual {p0, p1}, Lcom/discord/stores/SlowTtiExperimentManager$fetchExperiment$2;->call(Lcom/discord/models/experiments/domain/Experiment;)V

    return-void
.end method
