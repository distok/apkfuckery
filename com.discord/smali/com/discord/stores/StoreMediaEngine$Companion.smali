.class public final Lcom/discord/stores/StoreMediaEngine$Companion;
.super Ljava/lang/Object;
.source "StoreMediaEngine.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreMediaEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreMediaEngine$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$setVoiceConfig(Lcom/discord/stores/StoreMediaEngine$Companion;Lcom/discord/rtcconnection/mediaengine/MediaEngine;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StoreMediaEngine$Companion;->setVoiceConfig(Lcom/discord/rtcconnection/mediaengine/MediaEngine;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V

    return-void
.end method

.method private final setVoiceConfig(Lcom/discord/rtcconnection/mediaengine/MediaEngine;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V
    .locals 13

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getOutputVolume()F

    move-result v0

    invoke-interface {p1, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->h(F)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getEchoCancellation()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->c(Z)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getNoiseProcessing()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Suppression:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->i(Z)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getNoiseProcessing()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Cancellation:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->d(Z)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getAutomaticGainControl()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->m(Z)V

    invoke-interface {p1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->getConnections()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    move-result-object v1

    const/16 v4, 0x3f

    const/16 v5, 0x3f

    and-int/2addr v5, v2

    and-int/lit8 v5, v4, 0x2

    if-eqz v5, :cond_2

    const/16 v5, 0xa

    const/16 v8, 0xa

    goto :goto_3

    :cond_2
    const/4 v8, 0x0

    :goto_3
    and-int/lit8 v5, v4, 0x4

    if-eqz v5, :cond_3

    const/16 v5, 0x28

    const/16 v9, 0x28

    goto :goto_4

    :cond_3
    const/4 v9, 0x0

    :goto_4
    and-int/lit8 v5, v4, 0x8

    and-int/lit8 v5, v4, 0x10

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    const/4 v12, 0x5

    goto :goto_5

    :cond_4
    const/4 v12, 0x0

    :goto_5
    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getSensitivity()F

    move-result v4

    float-to-int v7, v4

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getAutomaticVad()Z

    move-result v10

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getVadUseKrisp()Z

    move-result v11

    new-instance v4, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    move-object v6, v4

    invoke-direct/range {v6 .. v12}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;-><init>(IIIZZI)V

    invoke-interface {v0, v1, v4}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->h(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->q(Z)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfMuted()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-interface {v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->getType()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    move-result-object v1

    sget-object v4, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;->STREAM:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    if-ne v1, v4, :cond_5

    goto :goto_6

    :cond_5
    const/4 v1, 0x0

    goto :goto_7

    :cond_6
    :goto_6
    const/4 v1, 0x1

    :goto_7
    invoke-interface {v0, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->c(Z)V

    goto :goto_2

    :cond_7
    return-void
.end method
