.class public final Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;
.super Lx/m/c/k;
.source "StoreGatewayConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGatewayConnection;->voiceStateUpdate(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/gateway/GatewaySocket;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:Ljava/lang/Long;

.field public final synthetic $guildId:Ljava/lang/Long;

.field public final synthetic $preferredRegion:Ljava/lang/String;

.field public final synthetic $selfDeaf:Z

.field public final synthetic $selfMute:Z

.field public final synthetic $selfVideo:Z


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$guildId:Ljava/lang/Long;

    iput-object p2, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$channelId:Ljava/lang/Long;

    iput-boolean p3, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$selfMute:Z

    iput-boolean p4, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$selfDeaf:Z

    iput-boolean p5, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$selfVideo:Z

    iput-object p6, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$preferredRegion:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/gateway/GatewaySocket;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->invoke(Lcom/discord/gateway/GatewaySocket;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/gateway/GatewaySocket;)V
    .locals 8

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$guildId:Ljava/lang/Long;

    iget-object v3, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$channelId:Ljava/lang/Long;

    iget-boolean v4, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$selfMute:Z

    iget-boolean v5, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$selfDeaf:Z

    iget-boolean v6, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$selfVideo:Z

    iget-object v7, p0, Lcom/discord/stores/StoreGatewayConnection$voiceStateUpdate$1;->$preferredRegion:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v1 .. v7}, Lcom/discord/gateway/GatewaySocket;->voiceStateUpdate(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;)V

    return-void
.end method
