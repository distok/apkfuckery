.class public final Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;
.super Lx/m/c/k;
.source "StoreGatewayConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/gateway/GatewaySocket;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildIds:Ljava/util/List;

.field public final synthetic $limit:Ljava/lang/Integer;

.field public final synthetic $query:Ljava/lang/String;

.field public final synthetic $userIds:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$guildIds:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$query:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$userIds:Ljava/util/List;

    iput-object p4, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$limit:Ljava/lang/Integer;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/gateway/GatewaySocket;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->invoke(Lcom/discord/gateway/GatewaySocket;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/gateway/GatewaySocket;)V
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$guildIds:Ljava/util/List;

    iget-object v1, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$query:Ljava/lang/String;

    iget-object v2, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$userIds:Ljava/util/List;

    iget-object v3, p0, Lcom/discord/stores/StoreGatewayConnection$requestGuildMembers$1;->$limit:Ljava/lang/Integer;

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/discord/gateway/GatewaySocket;->requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)V

    return-void
.end method
