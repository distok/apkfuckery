.class public final Lcom/discord/stores/StoreGifPicker$Companion;
.super Ljava/lang/Object;
.source "StoreGifPicker.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/stores/StoreGifPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/stores/StoreGifPicker$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getSearchResultsLoadingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/gifpicker/dto/ModelGif;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreGifPicker;->access$getSearchResultsLoadingList$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getSearchTermsLoadingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreGifPicker;->access$getSearchTermsLoadingList$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
