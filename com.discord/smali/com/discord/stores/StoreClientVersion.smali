.class public final Lcom/discord/stores/StoreClientVersion;
.super Lcom/discord/stores/Store;
.source "StoreClientVersion.kt"


# instance fields
.field private clientMinVersion:I

.field private final clientMinVersionKey:Ljava/lang/String;

.field private final clientOutdatedSubject:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final clientVersion:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/stores/Store;-><init>()V

    const/16 v0, 0x547

    iput v0, p0, Lcom/discord/stores/StoreClientVersion;->clientVersion:I

    const-string v0, "CLIENT_OUTDATED_KEY"

    iput-object v0, p0, Lcom/discord/stores/StoreClientVersion;->clientMinVersionKey:Ljava/lang/String;

    new-instance v0, Lrx/subjects/SerializedSubject;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    iput-object v0, p0, Lcom/discord/stores/StoreClientVersion;->clientOutdatedSubject:Lrx/subjects/SerializedSubject;

    return-void
.end method

.method public static final synthetic access$getClientMinVersionKey$p(Lcom/discord/stores/StoreClientVersion;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreClientVersion;->clientMinVersionKey:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$setClientMinVersion(Lcom/discord/stores/StoreClientVersion;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreClientVersion;->setClientMinVersion(I)V

    return-void
.end method

.method private final declared-synchronized setClientMinVersion(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/discord/stores/StoreClientVersion;->clientMinVersion:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/discord/stores/StoreClientVersion;->clientMinVersion:I

    iget-object v0, p0, Lcom/discord/stores/StoreClientVersion;->clientOutdatedSubject:Lrx/subjects/SerializedSubject;

    iget v1, p0, Lcom/discord/stores/StoreClientVersion;->clientVersion:I

    if-ge v1, p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v0, v0, Lrx/subjects/SerializedSubject;->e:Lg0/n/c;

    invoke-virtual {v0, v1}, Lg0/n/c;->onNext(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreClientVersion$setClientMinVersion$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreClientVersion$setClientMinVersion$1;-><init>(Lcom/discord/stores/StoreClientVersion;I)V

    invoke-static {v0, v1}, Lcom/discord/utilities/cache/SharedPreferenceExtensionsKt;->edit(Landroid/content/SharedPreferences;Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final getClientOutdated()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreClientVersion;->clientOutdatedSubject:Lrx/subjects/SerializedSubject;

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "clientOutdatedSubject\n  \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public declared-synchronized init(Landroid/content/Context;)V
    .locals 10

    monitor-enter p0

    :try_start_0
    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/stores/Store;->init(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/discord/stores/Store;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/stores/StoreClientVersion;->clientMinVersionKey:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/discord/stores/StoreClientVersion;->clientMinVersion:I

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreClientVersion;->setClientMinVersion(I)V

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x1

    sget-object p1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2, v3, p1}, Lrx/Observable;->A(JJLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreClientVersion$init$1;->INSTANCE:Lcom/discord/stores/StoreClientVersion$init$1;

    invoke-virtual {p1, v0}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n        .inte\u2026ClientVersion()\n        }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/stores/StoreClientVersion$init$2;

    invoke-direct {v7, p0}, Lcom/discord/stores/StoreClientVersion$init$2;-><init>(Lcom/discord/stores/StoreClientVersion;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
