.class public final Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;
.super Lx/m/c/k;
.source "StoreApplicationCommands.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreApplicationCommands;->requestApplicationCommandsQuery(JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic $query:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/stores/StoreApplicationCommands;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreApplicationCommands;JLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    iput-wide p2, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$guildId:J

    iput-object p4, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$query:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationCommands;->access$generateNonce(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    invoke-static {v0, v4}, Lcom/discord/stores/StoreApplicationCommands;->access$setQueryNonce$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationCommands;->access$getQueryGuildId$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v5, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$guildId:J

    cmp-long v0, v2, v5

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationCommands;->access$getQuery$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$query:Ljava/lang/String;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationCommands;->access$getQueryCommands$p(Lcom/discord/stores/StoreApplicationCommands;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    sget-object v3, Lcom/discord/stores/StoreApplicationCommands;->Companion:Lcom/discord/stores/StoreApplicationCommands$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreApplicationCommands$Companion;->getQueryCommandsUpdate()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreV2;->markChanged([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;)V

    :cond_2
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    iget-wide v1, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$guildId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/stores/StoreApplicationCommands;->access$setQueryGuildId$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$query:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/discord/stores/StoreApplicationCommands;->access$setQuery$p(Lcom/discord/stores/StoreApplicationCommands;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->this$0:Lcom/discord/stores/StoreApplicationCommands;

    invoke-static {v0}, Lcom/discord/stores/StoreApplicationCommands;->access$getStoreGatewayConnection$p(Lcom/discord/stores/StoreApplicationCommands;)Lcom/discord/stores/StoreGatewayConnection;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$guildId:J

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/discord/stores/StoreApplicationCommands$requestApplicationCommandsQuery$1;->$query:Ljava/lang/String;

    const/4 v7, 0x0

    const/16 v8, 0x14

    const/16 v9, 0x10

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/discord/stores/StoreGatewayConnection;->requestApplicationCommands$default(Lcom/discord/stores/StoreGatewayConnection;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;IILjava/lang/Object;)V

    return-void
.end method
