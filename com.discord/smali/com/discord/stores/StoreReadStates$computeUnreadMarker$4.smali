.class public final Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4;
.super Ljava/lang/Object;
.source "StoreReadStates.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreReadStates;->computeUnreadMarker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/application/Unread$Marker;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/models/application/Unread;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/stores/StoreReadStates;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreReadStates;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4;->this$0:Lcom/discord/stores/StoreReadStates;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/application/Unread$Marker;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4;->call(Lcom/discord/models/application/Unread$Marker;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/application/Unread$Marker;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/application/Unread$Marker;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/models/application/Unread;",
            ">;"
        }
    .end annotation

    const-string v0, "marker"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessages()Lcom/discord/stores/StoreMessages;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/application/Unread$Marker;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreMessages;->observeMessagesForChannel(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$1;-><init>(Lcom/discord/models/application/Unread$Marker;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4;->this$0:Lcom/discord/stores/StoreReadStates;

    invoke-static {v0}, Lcom/discord/stores/StoreReadStates;->access$getMarkAsRead$p(Lcom/discord/stores/StoreReadStates;)Lrx/subjects/SerializedSubject;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$2;->INSTANCE:Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$2;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->V(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$3;

    invoke-direct {v0, p0}, Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4$3;-><init>(Lcom/discord/stores/StoreReadStates$computeUnreadMarker$4;)V

    sget-object v1, Lg0/k/a;->a:Lg0/k/a$b;

    new-instance v2, Lg0/l/e/a;

    invoke-direct {v2, v1, v1, v0}, Lg0/l/e/a;-><init>(Lrx/functions/Action1;Lrx/functions/Action1;Lrx/functions/Action0;)V

    new-instance v0, Lg0/l/a/n;

    invoke-direct {v0, p1, v2}, Lg0/l/a/n;-><init>(Lrx/Observable;Lg0/g;)V

    invoke-static {v0}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
