.class public final Lcom/discord/stores/StoreExpandedGuildFolders$closeFolder$1;
.super Lx/m/c/k;
.source "StoreExpandedGuildFolders.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreExpandedGuildFolders;->closeFolder(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $folderId:J

.field public final synthetic this$0:Lcom/discord/stores/StoreExpandedGuildFolders;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreExpandedGuildFolders;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreExpandedGuildFolders$closeFolder$1;->this$0:Lcom/discord/stores/StoreExpandedGuildFolders;

    iput-wide p2, p0, Lcom/discord/stores/StoreExpandedGuildFolders$closeFolder$1;->$folderId:J

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreExpandedGuildFolders$closeFolder$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    iget-object v0, p0, Lcom/discord/stores/StoreExpandedGuildFolders$closeFolder$1;->this$0:Lcom/discord/stores/StoreExpandedGuildFolders;

    iget-wide v1, p0, Lcom/discord/stores/StoreExpandedGuildFolders$closeFolder$1;->$folderId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExpandedGuildFolders;->handleFolderClosed(J)V

    return-void
.end method
