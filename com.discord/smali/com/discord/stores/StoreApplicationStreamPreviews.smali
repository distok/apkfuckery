.class public final Lcom/discord/stores/StoreApplicationStreamPreviews;
.super Ljava/lang/Object;
.source "StoreApplicationStreamPreviews.kt"

# interfaces
.implements Lcom/discord/stores/DispatchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;,
        Lcom/discord/stores/StoreApplicationStreamPreviews$Companion;
    }
.end annotation


# static fields
.field private static final Companion:Lcom/discord/stores/StoreApplicationStreamPreviews$Companion;

.field private static final READ_PREVIEW_DEFAULT_RETRY_DELAY_MS:J = 0x2710L


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final fetchAttempts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final fetchStreamPreviewSubscriptions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lrx/Subscription;",
            ">;"
        }
    .end annotation
.end field

.field private isDirty:Z

.field private final previewsPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;",
            ">;>;"
        }
    .end annotation
.end field

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final streamKeyToPreviewMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/stores/StoreApplicationStreamPreviews$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/stores/StoreApplicationStreamPreviews$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/stores/StoreApplicationStreamPreviews;->Companion:Lcom/discord/stores/StoreApplicationStreamPreviews$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->clock:Lcom/discord/utilities/time/Clock;

    iput-object p3, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->streamKeyToPreviewMap:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    const-string p2, "BehaviorSubject.create(HashMap())"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->previewsPublisher:Lrx/subjects/BehaviorSubject;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchAttempts:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewSubscriptions:Ljava/util/HashMap;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/utilities/rest/RestAPI;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p3}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreApplicationStreamPreviews;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/utilities/rest/RestAPI;)V

    return-void
.end method

.method public static final synthetic access$fetchStreamPreviewIfNotFetching(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewIfNotFetching(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getDispatcher$p(Lcom/discord/stores/StoreApplicationStreamPreviews;)Lcom/discord/stores/Dispatcher;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->dispatcher:Lcom/discord/stores/Dispatcher;

    return-object p0
.end method

.method public static final synthetic access$getFetchStreamPreviewSubscriptions$p(Lcom/discord/stores/StoreApplicationStreamPreviews;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewSubscriptions:Ljava/util/HashMap;

    return-object p0
.end method

.method private final fetchStreamPreview(Ljava/lang/String;)V
    .locals 11

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$1;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewSubscriptions:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-object v1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/utilities/rest/RestAPI;->getStreamPreview(Ljava/lang/String;J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn(Lrx/Observable;Z)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/stores/StoreApplicationStreamPreviews;

    const/4 v4, 0x0

    new-instance v5, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$2;

    invoke-direct {v5, p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$2;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    new-instance v8, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$3;

    invoke-direct {v8, p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$3;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    new-instance v6, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$4;

    invoke-direct {v6, p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$4;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    new-instance v7, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$5;

    invoke-direct {v7, p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$fetchStreamPreview$5;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    const/4 v9, 0x2

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final fetchStreamPreviewIfNotFetching(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Ljava/lang/String;)V
    .locals 0

    if-eqz p1, :cond_0

    instance-of p1, p1, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Fetching;

    if-nez p1, :cond_1

    :cond_0
    invoke-direct {p0, p2}, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreview(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private final fetchStreamPreviewIfNotFetching(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->streamKeyToPreviewMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    invoke-direct {p0, v0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewIfNotFetching(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final fetchStreamPreviewIfNotFetching(Lcom/discord/utilities/streams/StreamContext;)V
    .locals 1

    const-string/jumbo v0, "streamContext"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->getPreview()Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewIfNotFetching(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Ljava/lang/String;)V

    return-void
.end method

.method public final get(Lcom/discord/models/domain/ModelApplicationStream;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelApplicationStream;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;",
            ">;"
        }
    .end annotation

    const-string v0, "applicationStream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final get(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->previewsPublisher:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/stores/StoreApplicationStreamPreviews$get$1;

    invoke-direct {v1, p1}, Lcom/discord/stores/StoreApplicationStreamPreviews$get$1;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string v0, "previewsPublisher\n      \u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final handleFetchFailed(Ljava/lang/String;Lcom/discord/utilities/error/Error;)V
    .locals 16
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string/jumbo v2, "streamKey"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "error"

    move-object/from16 v3, p2

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/stores/StoreApplicationStreamPreviews;->streamKeyToPreviewMap:Ljava/util/HashMap;

    new-instance v4, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p2 .. p2}, Lcom/discord/utilities/error/Error;->getThrowable()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v4, v2, Lretrofit2/HttpException;

    if-nez v4, :cond_0

    move-object v2, v5

    :cond_0
    check-cast v2, Lretrofit2/HttpException;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lretrofit2/HttpException;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v2, v5

    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v3

    const-string v4, "error.response"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/discord/utilities/error/Error$Response;->getRetryAfterMs()Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchAttempts:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_1
    const-string v6, "fetchAttempts[streamKey] ?: 0"

    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x1

    add-int/2addr v4, v6

    const-wide/16 v7, 0x2710

    int-to-long v9, v4

    mul-long v9, v9, v7

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/16 v8, 0x1ad

    if-ne v7, v8, :cond_4

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    :cond_3
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v7, 0x191

    if-ne v3, v7, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0x193

    if-ne v2, v3, :cond_6

    goto :goto_2

    :cond_6
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_2

    :cond_7
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    :goto_2
    if-eqz v5, :cond_9

    iget-object v2, v0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewSubscriptions:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrx/Subscription;

    if-eqz v2, :cond_8

    invoke-interface {v2}, Lrx/Subscription;->unsubscribe()V

    :cond_8
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v5}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v7

    const-string v2, "Observable.timer(retryAf\u2026s, TimeUnit.MILLISECONDS)"

    invoke-static {v7, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v8, Lcom/discord/stores/StoreApplicationStreamPreviews;

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;

    invoke-direct {v10, v0, v1}, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$1;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    const/4 v11, 0x0

    new-instance v12, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$2;

    invoke-direct {v12, v0, v1}, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$2;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    new-instance v13, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$3;

    invoke-direct {v13, v0, v1}, Lcom/discord/stores/StoreApplicationStreamPreviews$handleFetchFailed$3;-><init>(Lcom/discord/stores/StoreApplicationStreamPreviews;Ljava/lang/String;)V

    const/16 v14, 0xa

    const/4 v15, 0x0

    invoke-static/range {v7 .. v15}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_9
    iget-object v2, v0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchAttempts:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v6, v0, Lcom/discord/stores/StoreApplicationStreamPreviews;->isDirty:Z

    return-void
.end method

.method public final handleFetchStart(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->streamKeyToPreviewMap:Ljava/util/HashMap;

    sget-object v1, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Fetching;->INSTANCE:Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Fetching;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->isDirty:Z

    return-void
.end method

.method public final handleFetchSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "url"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->streamKeyToPreviewMap:Ljava/util/HashMap;

    new-instance v1, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;

    invoke-direct {v1, p2}, Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchAttempts:Ljava/util/HashMap;

    invoke-virtual {p2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->isDirty:Z

    return-void
.end method

.method public onDispatchEnded()V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-boolean v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->isDirty:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->previewsPublisher:Lrx/subjects/BehaviorSubject;

    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->streamKeyToPreviewMap:Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/stores/StoreApplicationStreamPreviews;->isDirty:Z

    return-void
.end method
