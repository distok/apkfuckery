.class public final Lcom/discord/stores/StorePendingReplies;
.super Lcom/discord/stores/StoreV2;
.source "StorePendingReplies.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StorePendingReplies$PendingReply;
    }
.end annotation


# instance fields
.field private final dispatcher:Lcom/discord/stores/Dispatcher;

.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private final pendingReplies:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap<",
            "Lcom/discord/stores/StorePendingReplies$PendingReply;",
            ">;"
        }
    .end annotation
.end field

.field private pendingRepliesSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StorePendingReplies$PendingReply;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 2

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observationDeck"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StorePendingReplies;->dispatcher:Lcom/discord/stores/Dispatcher;

    iput-object p2, p0, Lcom/discord/stores/StorePendingReplies;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    new-instance p1, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, p2, v0, v1}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/stores/StorePendingReplies;->pendingReplies:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/stores/StorePendingReplies;->pendingRepliesSnapshot:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/stores/StorePendingReplies;-><init>(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V

    return-void
.end method

.method public static final synthetic access$getPendingReplies$p(Lcom/discord/stores/StorePendingReplies;)Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StorePendingReplies;->pendingReplies:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    return-object p0
.end method

.method public static synthetic onCreatePendingReply$default(Lcom/discord/stores/StorePendingReplies;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelMessage;ZZILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x1

    if-eqz p6, :cond_0

    const/4 p3, 0x1

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x1

    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/stores/StorePendingReplies;->onCreatePendingReply(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelMessage;ZZ)V

    return-void
.end method


# virtual methods
.method public final getPendingReply(J)Lcom/discord/stores/StorePendingReplies$PendingReply;
    .locals 1

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->pendingRepliesSnapshot:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StorePendingReplies$PendingReply;

    return-object p1
.end method

.method public final handleMessageDelete(Lcom/discord/models/domain/ModelMessageDelete;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string v0, "messageDeleteBulk"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->pendingReplies:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getChannelId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StorePendingReplies$PendingReply;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getMessageIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StorePendingReplies$PendingReply;->getMessageReference()Lcom/discord/models/domain/ModelMessage$MessageReference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessage$MessageReference;->getMessageId()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->pendingReplies:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageDelete;->getChannelId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final handlePreLogout()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->pendingReplies:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-virtual {v0}, Lcom/discord/utilities/collections/ShallowPartitionMap$CopiablePartitionMap;->clear()V

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method

.method public final observePendingReply(J)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/stores/StorePendingReplies$PendingReply;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StorePendingReplies$observePendingReply$1;

    invoke-direct {v5, p0, p1, p2}, Lcom/discord/stores/StorePendingReplies$observePendingReply$1;-><init>(Lcom/discord/stores/StorePendingReplies;J)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final onCreatePendingReply(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelMessage;ZZ)V
    .locals 8

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v7, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/discord/stores/StorePendingReplies$onCreatePendingReply$1;-><init>(Lcom/discord/stores/StorePendingReplies;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelMessage;ZZ)V

    invoke-virtual {v0, v7}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onDeletePendingReply(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/stores/StorePendingReplies$onDeletePendingReply$1;-><init>(Lcom/discord/stores/StorePendingReplies;J)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onSetPendingReplyShouldMention(JZ)V
    .locals 2

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->dispatcher:Lcom/discord/stores/Dispatcher;

    new-instance v1, Lcom/discord/stores/StorePendingReplies$onSetPendingReplyShouldMention$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/discord/stores/StorePendingReplies$onSetPendingReplyShouldMention$1;-><init>(Lcom/discord/stores/StorePendingReplies;JZ)V

    invoke-virtual {v0, v1}, Lcom/discord/stores/Dispatcher;->schedule(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public snapshotData()V
    .locals 1
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StorePendingReplies;->pendingReplies:Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;

    invoke-virtual {v0}, Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;->fastCopy()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/stores/StorePendingReplies;->pendingRepliesSnapshot:Ljava/util/Map;

    return-void
.end method
