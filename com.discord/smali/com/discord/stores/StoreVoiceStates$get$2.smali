.class public final Lcom/discord/stores/StoreVoiceStates$get$2;
.super Ljava/lang/Object;
.source "StoreVoiceStates.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreVoiceStates;->get(JJ)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelVoice$State;",
        ">;",
        "Lrx/Observable<",
        "+",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelVoice$State;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/stores/StoreVoiceStates$get$2;->$channelId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreVoiceStates$get$2;->call(Ljava/util/Map;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/Map;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;)",
            "Lrx/Observable<",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelVoice$State;",
            ">;>;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->y(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/StoreVoiceStates$get$2$1;

    invoke-direct {v0, p0}, Lcom/discord/stores/StoreVoiceStates$get$2$1;-><init>(Lcom/discord/stores/StoreVoiceStates$get$2;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreVoiceStates$get$2$2;->INSTANCE:Lcom/discord/stores/StoreVoiceStates$get$2$2;

    new-instance v1, Lg0/l/a/o0;

    sget-object v2, Lg0/l/e/l;->d:Lg0/l/e/l;

    invoke-direct {v1, p1, v0, v2}, Lg0/l/a/o0;-><init>(Lrx/Observable;Lg0/k/b;Lg0/k/b;)V

    invoke-static {v1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
