.class public final Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;
.super Lx/m/c/k;
.source "StoreAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreAnalytics;->trackFailedMessageResolved(IIIZZLcom/discord/stores/FailedMessageResolutionType;JIJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $hasImage:Z

.field public final synthetic $hasVideo:Z

.field public final synthetic $initialAttemptTimestamp:J

.field public final synthetic $maxAttachmentSize:I

.field public final synthetic $numAttachments:I

.field public final synthetic $numRetries:I

.field public final synthetic $resolutionType:Lcom/discord/stores/FailedMessageResolutionType;

.field public final synthetic $totalAttachmentSize:I

.field public final synthetic this$0:Lcom/discord/stores/StoreAnalytics;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAnalytics;JIIIZZLcom/discord/stores/FailedMessageResolutionType;JI)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    iput-wide p2, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$channelId:J

    iput p4, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$numAttachments:I

    iput p5, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$maxAttachmentSize:I

    iput p6, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$totalAttachmentSize:I

    iput-boolean p7, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$hasImage:Z

    iput-boolean p8, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$hasVideo:Z

    iput-object p9, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$resolutionType:Lcom/discord/stores/FailedMessageResolutionType;

    iput-wide p10, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$initialAttemptTimestamp:J

    iput p12, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$numRetries:I

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 12

    iget-object v0, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->this$0:Lcom/discord/stores/StoreAnalytics;

    iget-wide v1, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$channelId:J

    invoke-static {v0, v1, v2}, Lcom/discord/stores/StoreAnalytics;->access$getSnapshotProperties(Lcom/discord/stores/StoreAnalytics;J)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget v2, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$numAttachments:I

    iget v3, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$maxAttachmentSize:I

    iget v4, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$totalAttachmentSize:I

    iget-boolean v5, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$hasImage:Z

    iget-boolean v6, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$hasVideo:Z

    iget-object v7, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$resolutionType:Lcom/discord/stores/FailedMessageResolutionType;

    iget-wide v8, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$initialAttemptTimestamp:J

    iget v10, p0, Lcom/discord/stores/StoreAnalytics$trackFailedMessageResolved$1;->$numRetries:I

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/m;->d:Lx/h/m;

    :goto_0
    move-object v11, v0

    invoke-virtual/range {v1 .. v11}, Lcom/discord/utilities/analytics/AnalyticsTracker;->failedMessageResolved(IIIZZLcom/discord/stores/FailedMessageResolutionType;JILjava/util/Map;)V

    return-void
.end method
