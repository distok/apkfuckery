.class public final Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;
.super Lx/m/c/k;
.source "StoreExperiments.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreExperiments;->getGuildExperiment(Ljava/lang/String;JZ)Lcom/discord/models/experiments/domain/Experiment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $bucket:I

.field public final synthetic $experimentName:Ljava/lang/String;

.field public final synthetic $guildId:J

.field public final synthetic $revision:I

.field public final synthetic this$0:Lcom/discord/stores/StoreExperiments;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;JII)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->this$0:Lcom/discord/stores/StoreExperiments;

    iput-object p2, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$experimentName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$guildId:J

    iput p5, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$bucket:I

    iput p6, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$revision:I

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 6

    iget-object v0, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->this$0:Lcom/discord/stores/StoreExperiments;

    iget-object v1, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$experimentName:Ljava/lang/String;

    iget-wide v2, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$guildId:J

    iget v4, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$bucket:I

    iget v5, p0, Lcom/discord/stores/StoreExperiments$getGuildExperiment$experiment$1;->$revision:I

    invoke-static/range {v0 .. v5}, Lcom/discord/stores/StoreExperiments;->access$triggerGuildExperiment(Lcom/discord/stores/StoreExperiments;Ljava/lang/String;JII)V

    return-void
.end method
