.class public final Lcom/discord/stores/StoreVideoStreams;
.super Lcom/discord/stores/StoreV2;
.source "StoreVideoStreams.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/stores/StoreVideoStreams$UserStreams;
    }
.end annotation


# instance fields
.field private final observationDeck:Lcom/discord/stores/updates/ObservationDeck;

.field private final userStreamsByUser:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVideoStreams$UserStreams;",
            ">;"
        }
    .end annotation
.end field

.field private userStreamsByUserSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVideoStreams$UserStreams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/discord/stores/StoreVideoStreams;-><init>(Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/updates/ObservationDeck;)V
    .locals 1

    const-string v0, "observationDeck"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/stores/StoreV2;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreVideoStreams;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    sget-object p1, Lx/h/m;->d:Lx/h/m;

    iput-object p1, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUserSnapshot:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/updates/ObservationDeck;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/discord/stores/updates/ObservationDeckProvider;->get()Lcom/discord/stores/updates/ObservationDeck;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/stores/StoreVideoStreams;-><init>(Lcom/discord/stores/updates/ObservationDeck;)V

    return-void
.end method

.method public static final synthetic access$getUserStreamsByUserSnapshot$p(Lcom/discord/stores/StoreVideoStreams;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUserSnapshot:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$setUserStreamsByUserSnapshot$p(Lcom/discord/stores/StoreVideoStreams;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUserSnapshot:Ljava/util/Map;

    return-void
.end method

.method private final setUserStreams(JLcom/discord/stores/StoreVideoStreams$UserStreams;)V
    .locals 1

    invoke-virtual {p3}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    return-void
.end method


# virtual methods
.method public final findUserForApplicationStream(I)Ljava/lang/Long;
    .locals 4
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    invoke-virtual {v1}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->getApplicationStreamId()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p1, :cond_0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public final handleApplicationStreamUpdate(JLjava/lang/Integer;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    invoke-direct {v0, v1, v1}, Lcom/discord/stores/StoreVideoStreams$UserStreams;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    :goto_0
    const-string/jumbo v2, "userStreamsByUser[userId\u2026plicationStreamId = null)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-static {v0, v1, p3, v2, v1}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->copy$default(Lcom/discord/stores/StoreVideoStreams$UserStreams;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/discord/stores/StoreVideoStreams$UserStreams;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreVideoStreams;->setUserStreams(JLcom/discord/stores/StoreVideoStreams$UserStreams;)V

    return-void
.end method

.method public final handleVideoStreamUpdate(JLjava/lang/Integer;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/stores/StoreVideoStreams$UserStreams;

    invoke-direct {v0, v1, v1}, Lcom/discord/stores/StoreVideoStreams$UserStreams;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    :goto_0
    const-string/jumbo v2, "userStreamsByUser[userId\u2026plicationStreamId = null)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-static {v0, p3, v1, v2, v1}, Lcom/discord/stores/StoreVideoStreams$UserStreams;->copy$default(Lcom/discord/stores/StoreVideoStreams$UserStreams;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/discord/stores/StoreVideoStreams$UserStreams;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/stores/StoreVideoStreams;->setUserStreams(JLcom/discord/stores/StoreVideoStreams$UserStreams;)V

    return-void
.end method

.method public final handleVoiceChannelSelected(J)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_0
    return-void
.end method

.method public final handleVoiceStateUpdates(Lcom/discord/models/domain/ModelVoice$State;)V
    .locals 3
    .annotation runtime Lcom/discord/stores/StoreThread;
    .end annotation

    const-string/jumbo v0, "voiceState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelVoice$State;->isRemoveSignal()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelVoice$State;->getUserId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/stores/StoreV2;->markChanged()V

    :cond_1
    return-void
.end method

.method public final observeUserStreams()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVideoStreams$UserStreams;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/stores/StoreVideoStreams;->observationDeck:Lcom/discord/stores/updates/ObservationDeck;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/discord/stores/updates/ObservationDeck$UpdateSource;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    new-instance v5, Lcom/discord/stores/StoreVideoStreams$observeUserStreams$1;

    invoke-direct {v5, p0}, Lcom/discord/stores/StoreVideoStreams$observeUserStreams$1;-><init>(Lcom/discord/stores/StoreVideoStreams;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/stores/updates/ObservationDeck;->connectRx$default(Lcom/discord/stores/updates/ObservationDeck;[Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public snapshotData()V
    .locals 2

    invoke-super {p0}, Lcom/discord/stores/StoreV2;->snapshotData()V

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUser:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/stores/StoreVideoStreams;->userStreamsByUserSnapshot:Ljava/util/Map;

    return-void
.end method
