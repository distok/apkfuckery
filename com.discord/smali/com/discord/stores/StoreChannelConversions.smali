.class public Lcom/discord/stores/StoreChannelConversions;
.super Ljava/lang/Object;
.source "StoreChannelConversions.java"


# instance fields
.field private selectedChannelId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleChannelCreated(Lcom/discord/models/domain/ModelChannel;)V
    .locals 7

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getOriginChannelId()J

    move-result-wide v0

    const-wide/16 v5, 0x0

    cmp-long p1, v0, v5

    if-nez p1, :cond_1

    return-void

    :cond_1
    iget-wide v5, p0, Lcom/discord/stores/StoreChannelConversions;->selectedChannelId:J

    cmp-long p1, v5, v0

    if-nez p1, :cond_2

    invoke-static {}, Lcom/discord/utilities/channel/ChannelSelector;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v0

    const-wide/16 v1, 0x0

    const/4 v5, 0x3

    invoke-virtual/range {v0 .. v5}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(JJI)V

    :cond_2
    return-void
.end method

.method public handleChannelSelected(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/stores/StoreChannelConversions;->selectedChannelId:J

    return-void
.end method
