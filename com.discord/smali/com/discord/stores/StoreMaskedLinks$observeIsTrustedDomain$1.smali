.class public final Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;
.super Ljava/lang/Object;
.source "StoreMaskedLinks.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/stores/StoreMaskedLinks;->observeIsTrustedDomain(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lrx/Observable<",
        "+",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $isLinkUnmasked:Z

.field public final synthetic this$0:Lcom/discord/stores/StoreMaskedLinks;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreMaskedLinks;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;->this$0:Lcom/discord/stores/StoreMaskedLinks;

    iput-boolean p2, p0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;->$isLinkUnmasked:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "+",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isDM()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;->this$0:Lcom/discord/stores/StoreMaskedLinks;

    invoke-static {v0}, Lcom/discord/stores/StoreMaskedLinks;->access$getStoreStream$p(Lcom/discord/stores/StoreMaskedLinks;)Lcom/discord/stores/StoreStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream;->getUserRelationships$app_productionDiscordExternalRelease()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    const-string v1, "selectedChannel.dmRecipient"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreUserRelationships;->observe(J)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1$1;

    invoke-direct {v0, p0}, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1$1;-><init>(Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-boolean p1, p0, Lcom/discord/stores/StoreMaskedLinks$observeIsTrustedDomain$1;->$isLinkUnmasked:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method
