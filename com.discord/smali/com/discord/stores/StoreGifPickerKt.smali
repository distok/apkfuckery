.class public final Lcom/discord/stores/StoreGifPickerKt;
.super Ljava/lang/Object;
.source "StoreGifPicker.kt"


# static fields
.field private static final DEFAULT_GIF_SEARCH_PAGE_SIZE:I = 0x32

.field private static final DEFAULT_TRENDING_SEARCH_TERM_LIMIT:I = 0x5

.field private static final MAX_CACHE_ENTRIES:I = 0x14

.field private static final TENOR_PROVIDER:Ljava/lang/String; = "tenor"

.field private static final TINYGIF_MEDIA_FORMAT:Ljava/lang/String; = "tinygif"
