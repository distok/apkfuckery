.class public Lcom/discord/panels/OverlappingPanelsLayout;
.super Landroid/widget/FrameLayout;
.source "OverlappingPanelsLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;,
        Lcom/discord/panels/OverlappingPanelsLayout$Panel;,
        Lcom/discord/panels/OverlappingPanelsLayout$LockState;,
        Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;,
        Lcom/discord/panels/OverlappingPanelsLayout$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/panels/OverlappingPanelsLayout$Companion;

.field private static final SIDE_PANEL_CLOSE_DURATION_MS:J = 0xc8L

.field private static final SIDE_PANEL_OPEN_DURATION_MS:J = 0xfaL


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private centerPanel:Landroid/view/View;

.field private centerPanelAnimationEndX:F

.field private centerPanelDiffX:F

.field private centerPanelXAnimator:Landroid/animation/ValueAnimator;

.field private childGestureRegions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private endPanel:Landroid/view/View;

.field private endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

.field private endPanelOpenedCenterPanelX:F

.field private endPanelState:Lcom/discord/panels/PanelState;

.field private final endPanelStateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private homeGestureFromBottomThreshold:F

.field private isHomeSystemGesture:Z

.field private isLeftToRight:Z

.field private isScrollingHorizontally:Z

.field private final isSystemGestureNavigationPossible:Z

.field private minFlingPxPerSecond:F

.field private nonFullScreenSidePanelWidth:I

.field private pendingUpdate:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private scrollingSlopPx:F

.field private selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

.field private startPanel:Landroid/view/View;

.field private startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

.field private startPanelOpenedCenterPanelX:F

.field private startPanelState:Lcom/discord/panels/PanelState;

.field private final startPanelStateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private swipeDirection:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

.field private useFullWidthForStartPanel:Z

.field private velocityTracker:Landroid/view/VelocityTracker;

.field private wasActionDownOnClosedCenterPanel:Z

.field private xFromInterceptActionDown:F

.field private yFromInterceptActionDown:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/panels/OverlappingPanelsLayout$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/panels/OverlappingPanelsLayout$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/panels/OverlappingPanelsLayout;->Companion:Lcom/discord/panels/OverlappingPanelsLayout$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelStateListeners:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelStateListeners:Ljava/util/ArrayList;

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->UNLOCKED:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    sget-object v0, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelState:Lcom/discord/panels/PanelState;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelState:Lcom/discord/panels/PanelState;

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->childGestureRegions:Ljava/util/List;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isSystemGestureNavigationPossible:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelStateListeners:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelStateListeners:Ljava/util/ArrayList;

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->UNLOCKED:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    sget-object v0, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelState:Lcom/discord/panels/PanelState;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelState:Lcom/discord/panels/PanelState;

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->childGestureRegions:Ljava/util/List;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isSystemGestureNavigationPossible:Z

    invoke-direct {p0, p2}, Lcom/discord/panels/OverlappingPanelsLayout;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    const/4 p3, 0x1

    iput p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    const p3, 0x7f7fffff    # Float.MAX_VALUE

    iput p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelStateListeners:Ljava/util/ArrayList;

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelStateListeners:Ljava/util/ArrayList;

    sget-object p3, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    sget-object p3, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->UNLOCKED:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    sget-object p3, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelState:Lcom/discord/panels/PanelState;

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelState:Lcom/discord/panels/PanelState;

    sget-object p3, Lx/h/l;->d:Lx/h/l;

    iput-object p3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->childGestureRegions:Ljava/util/List;

    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1d

    if-lt p3, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isSystemGestureNavigationPossible:Z

    invoke-direct {p0, p2}, Lcom/discord/panels/OverlappingPanelsLayout;->initialize(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/panels/OverlappingPanelsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/panels/OverlappingPanelsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$closePanels(Lcom/discord/panels/OverlappingPanelsLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels(Z)V

    return-void
.end method

.method public static final synthetic access$getCenterPanel$p(Lcom/discord/panels/OverlappingPanelsLayout;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "centerPanel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getStartPanel$p(Lcom/discord/panels/OverlappingPanelsLayout;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "startPanel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEndPanelWidthUpdate(Lcom/discord/panels/OverlappingPanelsLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->handleEndPanelWidthUpdate()V

    return-void
.end method

.method public static final synthetic access$handleStartPanelWidthUpdate(Lcom/discord/panels/OverlappingPanelsLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->handleStartPanelWidthUpdate()V

    return-void
.end method

.method public static final synthetic access$isLeftToRight$p(Lcom/discord/panels/OverlappingPanelsLayout;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    return p0
.end method

.method public static final synthetic access$openEndPanel(Lcom/discord/panels/OverlappingPanelsLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel(Z)V

    return-void
.end method

.method public static final synthetic access$openStartPanel(Lcom/discord/panels/OverlappingPanelsLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel(Z)V

    return-void
.end method

.method public static final synthetic access$setCenterPanel$p(Lcom/discord/panels/OverlappingPanelsLayout;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$setLeftToRight$p(Lcom/discord/panels/OverlappingPanelsLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    return-void
.end method

.method public static final synthetic access$setStartPanel$p(Lcom/discord/panels/OverlappingPanelsLayout;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$updateCenterPanelX(Lcom/discord/panels/OverlappingPanelsLayout;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->updateCenterPanelX(F)V

    return-void
.end method

.method private final calculateDistanceX(FLandroid/view/MotionEvent;)F
    .locals 0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    sub-float/2addr p2, p1

    return p2
.end method

.method private final calculateDistanceY(FLandroid/view/MotionEvent;)F
    .locals 0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p2

    sub-float/2addr p2, p1

    return p2
.end method

.method private final closePanels(Z)V
    .locals 3

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Lcom/discord/panels/OverlappingPanelsLayout$c;

    invoke-direct {v0, p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout$c;-><init>(Lcom/discord/panels/OverlappingPanelsLayout;Z)V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->pendingUpdate:Lkotlin/jvm/functions/Function0;

    return-void

    :cond_0
    const/4 v0, 0x0

    const-wide/16 v1, 0xc8

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/discord/panels/OverlappingPanelsLayout;->updateCenterPanelXWithAnimation(FZJ)V

    return-void
.end method

.method public static synthetic closePanels$default(Lcom/discord/panels/OverlappingPanelsLayout;ZILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels(Z)V

    return-void

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: closePanels"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final getEndPanelState(FF)Lcom/discord/panels/PanelState;
    .locals 5

    sget-object v0, Lcom/discord/panels/PanelState$d;->a:Lcom/discord/panels/PanelState$d;

    sget-object v1, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    cmpl-float v4, p2, v3

    if-ltz v4, :cond_0

    :goto_0
    move-object v0, v1

    goto :goto_1

    :cond_0
    if-nez v2, :cond_1

    cmpg-float v3, p2, v3

    if-gtz v3, :cond_1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    cmpg-float v1, p2, v1

    if-nez v1, :cond_2

    sget-object v0, Lcom/discord/panels/PanelState$c;->a:Lcom/discord/panels/PanelState$c;

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    cmpg-float v1, p2, p1

    if-gez v1, :cond_3

    goto :goto_1

    :cond_3
    if-nez v2, :cond_4

    cmpl-float p1, p2, p1

    if-lez p1, :cond_4

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/discord/panels/PanelState$b;->a:Lcom/discord/panels/PanelState$b;

    :goto_1
    return-object v0
.end method

.method private final getLeftPanel()Lcom/discord/panels/OverlappingPanelsLayout$Panel;
    .locals 1

    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->START:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->END:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    :goto_0
    return-object v0
.end method

.method private final getLeftPanelLockState()Lcom/discord/panels/OverlappingPanelsLayout$LockState;
    .locals 1

    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    :goto_0
    return-object v0
.end method

.method private final getNormalizedX(F)F
    .locals 4

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->OPEN:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    if-ne v0, v1, :cond_0

    iget p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    return p1

    :cond_0
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    if-ne v0, v1, :cond_1

    iget p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    return p1

    :cond_1
    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->getLeftPanelLockState()Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    move-result-object v0

    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->CLOSE:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    sget-object v3, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->swipeDirection:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    sget-object v3, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->LEFT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    if-ne v0, v3, :cond_2

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->getRightPanelLockState()Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    move-result-object v3

    if-eq v3, v1, :cond_5

    iget-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    sget-object v3, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->swipeDirection:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    sget-object v3, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->RIGHT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    if-ne v1, v3, :cond_4

    goto :goto_2

    :cond_4
    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    :cond_5
    :goto_2
    cmpl-float v1, p1, v0

    if-lez v1, :cond_6

    move p1, v0

    goto :goto_3

    :cond_6
    cmpg-float v0, p1, v2

    if-gez v0, :cond_7

    move p1, v2

    :cond_7
    :goto_3
    return p1
.end method

.method private final getRightPanel()Lcom/discord/panels/OverlappingPanelsLayout$Panel;
    .locals 1

    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->END:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->START:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    :goto_0
    return-object v0
.end method

.method private final getRightPanelLockState()Lcom/discord/panels/OverlappingPanelsLayout$LockState;
    .locals 1

    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    :goto_0
    return-object v0
.end method

.method private final getStartPanelState(FF)Lcom/discord/panels/PanelState;
    .locals 5

    sget-object v0, Lcom/discord/panels/PanelState$d;->a:Lcom/discord/panels/PanelState$d;

    sget-object v1, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    cmpg-float v4, p2, v3

    if-gtz v4, :cond_0

    :goto_0
    move-object v0, v1

    goto :goto_1

    :cond_0
    if-nez v2, :cond_1

    cmpl-float v3, p2, v3

    if-ltz v3, :cond_1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    cmpg-float v1, p2, v1

    if-nez v1, :cond_2

    sget-object v0, Lcom/discord/panels/PanelState$c;->a:Lcom/discord/panels/PanelState$c;

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    cmpl-float v1, p2, p1

    if-lez v1, :cond_3

    goto :goto_1

    :cond_3
    if-nez v2, :cond_4

    cmpg-float p1, p2, p1

    if-gez p1, :cond_4

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/discord/panels/PanelState$b;->a:Lcom/discord/panels/PanelState$b;

    :goto_1
    return-object v0
.end method

.method private final getTargetedX(Landroid/view/MotionEvent;)F
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result p1

    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelDiffX:F

    add-float/2addr p1, v0

    return p1
.end method

.method private final handleCenterPanelX(FF)V
    .locals 12

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_1a

    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    const/4 v3, 0x4

    const-string v4, "centerPanel"

    const/4 v5, 0x0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v2

    int-to-float v6, v5

    cmpl-float v2, v2, v6

    if-gtz v2, :cond_2

    goto :goto_0

    :cond_0
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v2

    int-to-float v6, v5

    cmpg-float v2, v2, v6

    if-gez v2, :cond_4

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_4
    const/4 v2, 0x4

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    if-eqz v0, :cond_19

    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v2

    int-to-float v6, v5

    cmpg-float v2, v2, v6

    if-ltz v2, :cond_7

    goto :goto_2

    :cond_5
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_6
    :goto_2
    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v2

    int-to-float v6, v5

    cmpl-float v2, v2, v6

    if-lez v2, :cond_9

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_9
    const/4 v2, 0x4

    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    cmpg-float v2, p2, v0

    if-nez v2, :cond_a

    sget-object v6, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    iput-object v6, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    goto :goto_4

    :cond_a
    iget v6, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    cmpg-float v6, p2, v6

    if-nez v6, :cond_b

    sget-object v6, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->START:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    iput-object v6, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    goto :goto_4

    :cond_b
    iget v6, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    cmpg-float v6, p2, v6

    if-nez v6, :cond_c

    sget-object v6, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->END:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    iput-object v6, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    :cond_c
    :goto_4
    iget v6, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    const/4 v7, 0x1

    cmpg-float v6, p2, v6

    if-eqz v6, :cond_e

    iget v6, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    cmpg-float v6, p2, v6

    if-nez v6, :cond_d

    goto :goto_5

    :cond_d
    const/4 v6, 0x0

    goto :goto_6

    :cond_e
    :goto_5
    const/4 v6, 0x1

    :goto_6
    iget-object v8, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v8, :cond_18

    xor-int/lit8 v9, v6, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    const-string v11, "$this$setEnabledAlpha"

    invoke-static {v8, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v9, :cond_f

    const/high16 v10, 0x3f800000    # 1.0f

    :cond_f
    invoke-virtual {v8, v10}, Landroid/view/View;->setAlpha(F)V

    iget-object v8, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v8, :cond_17

    if-eqz v6, :cond_10

    goto :goto_7

    :cond_10
    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v8, v3}, Landroid/view/View;->setImportantForAccessibility(I)V

    if-eqz v2, :cond_11

    if-eqz v6, :cond_12

    :cond_11
    const/4 v5, 0x1

    :cond_12
    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v2, :cond_16

    if-eqz v5, :cond_13

    goto :goto_8

    :cond_13
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/discord/panels/R$a;->overlapping_panels_center_panel_non_resting_elevation:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    :goto_8
    invoke-virtual {v2, v0}, Landroid/view/View;->setElevation(F)V

    invoke-direct {p0, p1, p2}, Lcom/discord/panels/OverlappingPanelsLayout;->getStartPanelState(FF)Lcom/discord/panels/PanelState;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelState:Lcom/discord/panels/PanelState;

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelState:Lcom/discord/panels/PanelState;

    invoke-interface {v1, v2}, Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;->onPanelStateChange(Lcom/discord/panels/PanelState;)V

    goto :goto_9

    :cond_14
    invoke-direct {p0, p1, p2}, Lcom/discord/panels/OverlappingPanelsLayout;->getEndPanelState(FF)Lcom/discord/panels/PanelState;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelState:Lcom/discord/panels/PanelState;

    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelStateListeners:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_a
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_15

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelState:Lcom/discord/panels/PanelState;

    invoke-interface {p2, v0}, Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;->onPanelStateChange(Lcom/discord/panels/PanelState;)V

    goto :goto_a

    :cond_15
    return-void

    :cond_16
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_17
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_18
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_19
    const-string p1, "endPanel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1a
    const-string/jumbo p1, "startPanel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final handleEndPanelWidthUpdate()V
    .locals 4

    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/discord/panels/R$a;->overlapping_panels_margin_between_panels:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v1

    neg-float v1, v2

    iput v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    neg-float v1, v1

    :goto_0
    iput v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    iget-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    cmpg-float v1, v1, v0

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelAnimationEndX:F

    cmpg-float v0, v1, v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel()V

    :cond_2
    return-void

    :cond_3
    const-string v0, "centerPanel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_4
    const-string v0, "endPanel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method

.method private final handleStartPanelWidthUpdate()V
    .locals 4

    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/discord/panels/R$a;->overlapping_panels_margin_between_panels:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v1

    iput v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget-boolean v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    neg-float v2, v2

    :goto_0
    iput v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    cmpg-float v1, v1, v0

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelAnimationEndX:F

    cmpg-float v0, v1, v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel()V

    :cond_2
    return-void

    :cond_3
    const-string v0, "centerPanel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_4
    const-string/jumbo v0, "startPanel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method

.method private final initPanels()V
    .locals 8

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "getChildAt(0)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "getChildAt(1)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "getChildAt(2)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    const/4 v3, 0x0

    const-string/jumbo v4, "startPanel"

    if-eqz v2, :cond_8

    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    if-eqz v2, :cond_7

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setElevation(F)V

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    const-string v7, "centerPanel"

    if-eqz v2, :cond_6

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v2, :cond_5

    invoke-virtual {v2, v6}, Landroid/view/View;->setElevation(F)V

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    const-string v7, "endPanel"

    if-eqz v2, :cond_4

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    if-eqz v2, :cond_3

    invoke-virtual {v2, v6}, Landroid/view/View;->setElevation(F)V

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->resetStartPanelWidth()V

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->resetEndPanelWidth()V

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->handleStartPanelWidthUpdate()V

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->handleEndPanelWidthUpdate()V

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->pendingUpdate:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/Unit;

    :cond_0
    iput-object v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->pendingUpdate:Lkotlin/jvm/functions/Function0;

    iget-object v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    if-eqz v2, :cond_2

    new-instance v4, Lcom/discord/panels/OverlappingPanelsLayout$b;

    invoke-direct {v4, v0, p0}, Lcom/discord/panels/OverlappingPanelsLayout$b;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    if-eqz v0, :cond_1

    new-instance v2, Lcom/discord/panels/OverlappingPanelsLayout$b;

    invoke-direct {v2, v1, p0}, Lcom/discord/panels/OverlappingPanelsLayout$b;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void

    :cond_1
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_3
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_4
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_5
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_6
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_7
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_8
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method

.method private final initialize(Landroid/util/AttributeSet;)V
    .locals 5

    sget-object v0, Lf/a/f/a;->b:Lf/a/f/a;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lf/a/f/a;->a:Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/discord/panels/R$a;->overlapping_panels_scroll_slop:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->scrollingSlopPx:F

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/discord/panels/R$a;->overlapping_panels_home_gesture_from_bottom_threshold:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->homeGestureFromBottomThreshold:F

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/discord/panels/R$a;->overlapping_panels_min_fling_dp_per_second:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->minFlingPxPerSecond:F

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v3, "resources"

    invoke-static {v0, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/discord/panels/R$a;->overlapping_panels_margin_between_panels:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/discord/panels/R$a;->overlapping_panels_closed_center_panel_visible_width:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    int-to-float v0, v0

    sub-float/2addr v0, v1

    sub-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->nonFullScreenSidePanelWidth:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/discord/panels/R$b;->OverlappingPanelsLayout:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    sget v0, Lcom/discord/panels/R$b;->OverlappingPanelsLayout_maxSidePanelNonFullScreenWidth:I

    const v1, 0x7fffffff

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->nonFullScreenSidePanelWidth:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->nonFullScreenSidePanelWidth:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private final isTouchingCenterPanelWhileSidePanelOpen(Landroid/view/MotionEvent;)Z
    .locals 7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result p1

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    const/4 v1, 0x0

    const-string v2, "centerPanel"

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget v4, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iget v4, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget v5, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget-object v5, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v4

    const/4 v2, 0x1

    const/4 v5, 0x0

    cmpl-float v6, p1, v3

    if-lez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    cmpg-float p1, p1, v1

    if-gez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    cmpg-float v1, v0, v3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    cmpg-float v0, v0, v4

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    if-eqz v1, :cond_4

    if-nez v6, :cond_6

    :cond_4
    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :cond_6
    :goto_4
    return v2

    :cond_7
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_8
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final isTouchingChildGestureRegion(Landroid/view/MotionEvent;)Z
    .locals 7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iget-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->childGestureRegions:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    const/4 v5, 0x1

    cmpl-float v4, v0, v4

    if-ltz v4, :cond_1

    iget v4, v2, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    cmpg-float v6, p1, v6

    if-gtz v6, :cond_2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v4, :cond_3

    if-eqz v2, :cond_3

    const/4 v3, 0x1

    :cond_3
    if-eqz v3, :cond_0

    return v5

    :cond_4
    return v3
.end method

.method private final openEndPanel(Z)V
    .locals 3

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Lcom/discord/panels/OverlappingPanelsLayout$d;

    invoke-direct {v0, p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout$d;-><init>(Lcom/discord/panels/OverlappingPanelsLayout;Z)V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->pendingUpdate:Lkotlin/jvm/functions/Function0;

    return-void

    :cond_0
    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    const-wide/16 v1, 0xfa

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/discord/panels/OverlappingPanelsLayout;->updateCenterPanelXWithAnimation(FZJ)V

    return-void
.end method

.method public static synthetic openEndPanel$default(Lcom/discord/panels/OverlappingPanelsLayout;ZILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel(Z)V

    return-void

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: openEndPanel"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final openPanel(Lcom/discord/panels/OverlappingPanelsLayout$Panel;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel(Z)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel(Z)V

    :goto_0
    return-void
.end method

.method private final openStartPanel(Z)V
    .locals 3

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Lcom/discord/panels/OverlappingPanelsLayout$e;

    invoke-direct {v0, p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout$e;-><init>(Lcom/discord/panels/OverlappingPanelsLayout;Z)V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->pendingUpdate:Lkotlin/jvm/functions/Function0;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->OPEN:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    if-ne v0, v1, :cond_1

    iget p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->updateCenterPanelX(F)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    const-wide/16 v1, 0xfa

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/discord/panels/OverlappingPanelsLayout;->updateCenterPanelXWithAnimation(FZJ)V

    :goto_0
    return-void
.end method

.method public static synthetic openStartPanel$default(Lcom/discord/panels/OverlappingPanelsLayout;ZILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel(Z)V

    return-void

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: openStartPanel"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final resetEndPanelWidth()V
    .locals 4

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    const/4 v1, 0x0

    const-string v2, "endPanel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->nonFullScreenSidePanelWidth:I

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanel:Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final resetStartPanelWidth()V
    .locals 4

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    const-string/jumbo v2, "startPanel"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-boolean v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->useFullWidthForStartPanel:Z

    if-eqz v3, :cond_0

    const/4 v3, -0x1

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->nonFullScreenSidePanelWidth:I

    :goto_0
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanel:Landroid/view/View;

    if-eqz v3, :cond_1

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    :goto_1
    return-void
.end method

.method private final shouldHandleActionMoveEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->getTargetedX(Landroid/view/MotionEvent;)F

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->getNormalizedX(F)F

    move-result p1

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/4 v2, 0x1

    const/4 v3, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    cmpg-float v1, p1, v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    cmpg-float p1, p1, v1

    if-eqz p1, :cond_2

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_1
    return v2

    :cond_3
    const-string p1, "centerPanel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final snapOpenOrClose(Landroid/view/MotionEvent;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->getTargetedX(Landroid/view/MotionEvent;)F

    move-result p1

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    :cond_0
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->minFlingPxPerSecond:F

    const/4 v3, 0x0

    const/4 v4, 0x1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isLeftToRight:Z

    if-eqz v2, :cond_3

    int-to-float v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    goto :goto_2

    :cond_3
    int-to-float v2, v3

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    :goto_2
    const/4 v3, 0x1

    :cond_4
    if-eqz v1, :cond_8

    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->END:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    if-ne v0, v1, :cond_5

    invoke-direct {p0, v4}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels(Z)V

    return-void

    :cond_5
    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    if-ne v0, v1, :cond_8

    invoke-direct {p0, v4}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel(Z)V

    return-void

    :cond_6
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->START:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    if-ne v0, v1, :cond_7

    invoke-direct {p0, v4}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels(Z)V

    return-void

    :cond_7
    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    if-ne v0, v1, :cond_8

    invoke-direct {p0, v4}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel(Z)V

    return-void

    :cond_8
    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelOpenedCenterPanelX:F

    iget v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelOpenedCenterPanelX:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/4 v2, 0x2

    int-to-float v2, v2

    div-float/2addr v0, v2

    cmpl-float v0, p1, v0

    if-lez v0, :cond_9

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->getLeftPanel()Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->openPanel(Lcom/discord/panels/OverlappingPanelsLayout$Panel;)V

    goto :goto_3

    :cond_9
    div-float/2addr v1, v2

    cmpg-float p1, p1, v1

    if-gez p1, :cond_a

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->getRightPanel()Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->openPanel(Lcom/discord/panels/OverlappingPanelsLayout$Panel;)V

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels()V

    :goto_3
    return-void
.end method

.method private final translateCenterPanel(Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->getTargetedX(Landroid/view/MotionEvent;)F

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->getNormalizedX(F)F

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->updateCenterPanelX(F)V

    return-void
.end method

.method private final updateCenterPanelX(F)V
    .locals 4

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    const/4 v1, 0x0

    const-string v2, "centerPanel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {v3, p1}, Landroid/view/View;->setX(F)V

    invoke-direct {p0, v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->handleCenterPanelX(FF)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final updateCenterPanelXWithAnimation(FZJ)V
    .locals 4

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelXAnimator:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->getNormalizedX(F)F

    move-result p1

    iput p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelAnimationEndX:F

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p2, :cond_1

    new-array p2, v1, [F

    aput v0, p2, v2

    aput p1, p2, v3

    invoke-static {p2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    new-instance p2, Landroidx/interpolator/view/animation/LinearOutSlowInInterpolator;

    invoke-direct {p2}, Landroidx/interpolator/view/animation/LinearOutSlowInInterpolator;-><init>()V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {p1, p3, p4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelXAnimator:Landroid/animation/ValueAnimator;

    new-instance p2, Lcom/discord/panels/OverlappingPanelsLayout$a;

    invoke-direct {p2, v2, p0}, Lcom/discord/panels/OverlappingPanelsLayout$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_0

    :cond_1
    new-array p2, v1, [F

    aput v0, p2, v2

    aput p1, p2, v3

    invoke-static {p2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    new-instance p2, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;

    invoke-direct {p2}, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {p1, p3, p4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelXAnimator:Landroid/animation/ValueAnimator;

    new-instance p2, Lcom/discord/panels/OverlappingPanelsLayout$a;

    invoke-direct {p2, v3, p0}, Lcom/discord/panels/OverlappingPanelsLayout$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    :goto_0
    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelXAnimator:Landroid/animation/ValueAnimator;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    :cond_2
    return-void

    :cond_3
    const-string p1, "centerPanel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public static synthetic updateCenterPanelXWithAnimation$default(Lcom/discord/panels/OverlappingPanelsLayout;FZJILjava/lang/Object;)V
    .locals 0

    if-nez p6, :cond_2

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    const-wide/16 p3, 0xfa

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/panels/OverlappingPanelsLayout;->updateCenterPanelXWithAnimation(FZJ)V

    return-void

    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: updateCenterPanelXWithAnimation"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final closePanels()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels(Z)V

    return-void
.end method

.method public final getSelectedPanel()Lcom/discord/panels/OverlappingPanelsLayout$Panel;
    .locals 1

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->selectedPanel:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    return-object v0
.end method

.method public final handleEndPanelState(Lcom/discord/panels/PanelState;)V
    .locals 3

    const-string v0, "endPanelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelState:Lcom/discord/panels/PanelState;

    sget-object v1, Lcom/discord/panels/PanelState$c;->a:Lcom/discord/panels/PanelState$c;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel()V

    goto :goto_0

    :cond_0
    instance-of v2, p1, Lcom/discord/panels/PanelState$a;

    if-eqz v2, :cond_1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels()V

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelState:Lcom/discord/panels/PanelState;

    return-void
.end method

.method public final handleStartPanelState(Lcom/discord/panels/PanelState;)V
    .locals 3

    const-string/jumbo v0, "startPanelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelState:Lcom/discord/panels/PanelState;

    sget-object v1, Lcom/discord/panels/PanelState$c;->a:Lcom/discord/panels/PanelState$c;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel()V

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    invoke-static {p1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels()V

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelState:Lcom/discord/panels/PanelState;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_3

    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    const/4 p1, 0x3

    if-eq v0, p1, :cond_3

    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->wasActionDownOnClosedCenterPanel:Z

    goto/16 :goto_2

    :cond_0
    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isScrollingHorizontally:Z

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    const/4 v2, 0x1

    goto/16 :goto_2

    :cond_2
    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->xFromInterceptActionDown:F

    invoke-direct {p0, v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->calculateDistanceX(FLandroid/view/MotionEvent;)F

    move-result v0

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->yFromInterceptActionDown:F

    invoke-direct {p0, v1, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->calculateDistanceY(FLandroid/view/MotionEvent;)F

    move-result v1

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->isTouchingChildGestureRegion(Landroid/view/MotionEvent;)Z

    move-result p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/discord/panels/OverlappingPanelsLayout;->scrollingSlopPx:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_9

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9

    if-nez p1, :cond_9

    iput-boolean v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isScrollingHorizontally:Z

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    :cond_4
    iput-object v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    iget-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isScrollingHorizontally:Z

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->wasActionDownOnClosedCenterPanel:Z

    if-eqz p1, :cond_9

    goto :goto_0

    :cond_5
    iput-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isScrollingHorizontally:Z

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->isTouchingCenterPanelWhileSidePanelOpen(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->wasActionDownOnClosedCenterPanel:Z

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanelDiffX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->xFromInterceptActionDown:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->yFromInterceptActionDown:F

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v4, "resources"

    invoke-static {v1, v4}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->homeGestureFromBottomThreshold:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isSystemGestureNavigationPossible:Z

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    :cond_6
    iput-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isHomeSystemGesture:Z

    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_7

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_8

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_7
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_8
    :goto_1
    iget-boolean v2, p0, Lcom/discord/panels/OverlappingPanelsLayout;->wasActionDownOnClosedCenterPanel:Z

    :cond_9
    :goto_2
    return v2

    :cond_a
    const-string p1, "centerPanel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onLayout(ZIIII)V
    .locals 0

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result p1

    const/4 p2, 0x3

    if-ne p1, p2, :cond_0

    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->centerPanel:Landroid/view/View;

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->initPanels()V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isHomeSystemGesture:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_a

    if-eq v0, v2, :cond_6

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    const/4 v3, 0x3

    if-eq v0, v3, :cond_6

    goto/16 :goto_4

    :cond_1
    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->isTouchingChildGestureRegion(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    iget v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->xFromInterceptActionDown:F

    invoke-direct {p0, v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->calculateDistanceX(FLandroid/view/MotionEvent;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/discord/panels/OverlappingPanelsLayout;->scrollingSlopPx:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->swipeDirection:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    if-nez v3, :cond_4

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->RIGHT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->LEFT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    :goto_0
    iput-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->swipeDirection:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    :cond_4
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_5

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_5
    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->shouldHandleActionMoveEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->translateCenterPanel(Landroid/view/MotionEvent;)V

    goto :goto_3

    :cond_6
    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->wasActionDownOnClosedCenterPanel:Z

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->xFromInterceptActionDown:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->scrollingSlopPx:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_7

    iget-boolean v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isScrollingHorizontally:Z

    if-nez v0, :cond_7

    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels()V

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/discord/panels/OverlappingPanelsLayout;->velocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_9

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_9
    invoke-direct {p0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->snapOpenOrClose(Landroid/view/MotionEvent;)V

    :goto_2
    iput-boolean v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->wasActionDownOnClosedCenterPanel:Z

    iput-boolean v1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->isScrollingHorizontally:Z

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->swipeDirection:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    :cond_a
    :goto_3
    const/4 v1, 0x1

    :goto_4
    return v1
.end method

.method public final openEndPanel()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel(Z)V

    return-void
.end method

.method public final openStartPanel()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel(Z)V

    return-void
.end method

.method public final varargs registerEndPanelStateListeners([Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;)V
    .locals 4

    const-string v0, "panelStateListenerArgs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final varargs registerStartPanelStateListeners([Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;)V
    .locals 4

    const-string v0, "panelStateListenerArgs"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final setChildGestureRegions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    const-string v0, "childGestureRegions"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->childGestureRegions:Ljava/util/List;

    return-void
.end method

.method public final setEndPanelLockState(Lcom/discord/panels/OverlappingPanelsLayout$LockState;)V
    .locals 1

    const-string v0, "lockState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->endPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->OPEN:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->openEndPanel()V

    :cond_0
    return-void
.end method

.method public final setStartPanelLockState(Lcom/discord/panels/OverlappingPanelsLayout$LockState;)V
    .locals 1

    const-string v0, "lockState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->startPanelLockState:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->OPEN:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel()V

    :cond_0
    return-void
.end method

.method public final setStartPanelUseFullPortraitWidth(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/panels/OverlappingPanelsLayout;->useFullWidthForStartPanel:Z

    invoke-direct {p0}, Lcom/discord/panels/OverlappingPanelsLayout;->resetStartPanelWidth()V

    return-void
.end method
