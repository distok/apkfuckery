.class public final enum Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;
.super Ljava/lang/Enum;
.source "OverlappingPanelsLayout.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/panels/OverlappingPanelsLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SwipeDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

.field public static final enum LEFT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

.field public static final enum RIGHT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    new-instance v1, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    const-string v2, "LEFT"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->LEFT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    const-string v2, "RIGHT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->RIGHT:Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->$VALUES:[Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;
    .locals 1

    const-class v0, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    return-object p0
.end method

.method public static values()[Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;
    .locals 1

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->$VALUES:[Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    invoke-virtual {v0}, [Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/panels/OverlappingPanelsLayout$SwipeDirection;

    return-object v0
.end method
