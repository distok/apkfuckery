.class public final Lcom/discord/panels/OverlappingPanelsLayout$b;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/panels/OverlappingPanelsLayout;->initPanels()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->d:I

    iput-object p2, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    iget p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->d:I

    if-eqz p1, :cond_3

    const/4 p3, 0x1

    if-ne p1, p3, :cond_2

    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$isLeftToRight$p(Lcom/discord/panels/OverlappingPanelsLayout;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eq p2, p6, :cond_0

    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$handleEndPanelWidthUpdate(Lcom/discord/panels/OverlappingPanelsLayout;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$isLeftToRight$p(Lcom/discord/panels/OverlappingPanelsLayout;)Z

    move-result p1

    if-nez p1, :cond_1

    if-eq p4, p8, :cond_1

    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$handleEndPanelWidthUpdate(Lcom/discord/panels/OverlappingPanelsLayout;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 p1, 0x0

    throw p1

    :cond_3
    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$isLeftToRight$p(Lcom/discord/panels/OverlappingPanelsLayout;)Z

    move-result p1

    if-eqz p1, :cond_4

    if-eq p4, p8, :cond_4

    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$handleStartPanelWidthUpdate(Lcom/discord/panels/OverlappingPanelsLayout;)V

    goto :goto_1

    :cond_4
    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$isLeftToRight$p(Lcom/discord/panels/OverlappingPanelsLayout;)Z

    move-result p1

    if-nez p1, :cond_5

    if-eq p2, p6, :cond_5

    iget-object p1, p0, Lcom/discord/panels/OverlappingPanelsLayout$b;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/panels/OverlappingPanelsLayout;

    invoke-static {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->access$handleStartPanelWidthUpdate(Lcom/discord/panels/OverlappingPanelsLayout;)V

    :cond_5
    :goto_1
    return-void
.end method
