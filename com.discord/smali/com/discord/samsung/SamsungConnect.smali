.class public final Lcom/discord/samsung/SamsungConnect;
.super Ljava/lang/Object;
.source "SamsungConnect.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/samsung/SamsungConnect$SamsungCallbackException;
    }
.end annotation


# static fields
.field public static final a:Lcom/discord/samsung/SamsungConnect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/samsung/SamsungConnect;

    invoke-direct {v0}, Lcom/discord/samsung/SamsungConnect;-><init>()V

    sput-object v0, Lcom/discord/samsung/SamsungConnect;->a:Lcom/discord/samsung/SamsungConnect;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Lcom/discord/samsung/SamsungConnect;Lokhttp3/Response;)Landroid/net/Uri;
    .locals 5

    const-string p0, "Location"

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p1, p0, v0, v1}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0

    :cond_0
    move-object p0, v0

    :goto_0
    const-string v2, "error"

    const/4 v3, 0x0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {v4, v2, v3, v1}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_4

    invoke-virtual {p1}, Lokhttp3/Response;->b()Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p1, Lokhttp3/Response;->g:I

    const/16 v4, 0x133

    if-eq v1, v4, :cond_2

    const/16 v4, 0x134

    if-eq v1, v4, :cond_2

    packed-switch v1, :pswitch_data_0

    goto :goto_2

    :cond_2
    :pswitch_0
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_4

    :cond_3
    return-object p0

    :cond_4
    new-instance v1, Lcom/discord/samsung/SamsungConnect$SamsungCallbackException;

    if-eqz p0, :cond_5

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    goto :goto_3

    :cond_5
    const-string/jumbo v2, "unknown"

    :goto_3
    const-string v3, "location?.getQueryParameter(\"error\") ?: \"unknown\""

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_6

    const-string v0, "error_description"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-direct {v1, v2, v0}, Lcom/discord/samsung/SamsungConnect$SamsungCallbackException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object p0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Samsung handleSamsungCallback error: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v1}, Lcom/discord/app/AppLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
