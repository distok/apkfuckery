.class public final Lcom/discord/samsung/SamsungConnectActivity$b;
.super Ljava/lang/Object;
.source "SamsungConnectActivity.kt"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/samsung/SamsungConnectActivity;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/samsung/SamsungConnectActivity;


# direct methods
.method public constructor <init>(Lcom/discord/samsung/SamsungConnectActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/samsung/SamsungConnectActivity$b;->d:Lcom/discord/samsung/SamsungConnectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    iget-object p1, p0, Lcom/discord/samsung/SamsungConnectActivity$b;->d:Lcom/discord/samsung/SamsungConnectActivity;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/discord/samsung/SamsungConnectActivity;->e:Z

    sget p1, Lf/k/a/a/b$a;->a:I

    const/4 p1, 0x0

    if-nez p2, :cond_0

    move-object v1, p1

    goto :goto_0

    :cond_0
    const-string v1, "com.msc.sa.aidl.ISAService"

    invoke-interface {p2, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    instance-of v2, v1, Lf/k/a/a/b;

    if-eqz v2, :cond_1

    check-cast v1, Lf/k/a/a/b;

    goto :goto_0

    :cond_1
    new-instance v1, Lf/k/a/a/b$a$a;

    invoke-direct {v1, p2}, Lf/k/a/a/b$a$a;-><init>(Landroid/os/IBinder;)V

    :goto_0
    const-string p2, "Samsung Account service connection established"

    invoke-static {p2}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    :try_start_0
    const-string p2, "97t47j218f"

    const-string v2, "dummy"

    const-string v3, "com.discord"

    iget-object v4, p0, Lcom/discord/samsung/SamsungConnectActivity$b;->d:Lcom/discord/samsung/SamsungConnectActivity;

    iget-object v4, v4, Lcom/discord/samsung/SamsungConnectActivity;->d:Lf/k/a/a/a;

    if-eqz v4, :cond_4

    invoke-interface {v1, p2, v2, v3, v4}, Lf/k/a/a/b;->H(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/k/a/a/a;)Ljava/lang/String;

    move-result-object p2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Samsung Account service connection established: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    if-nez p2, :cond_2

    iget-object p2, p0, Lcom/discord/samsung/SamsungConnectActivity$b;->d:Lcom/discord/samsung/SamsungConnectActivity;

    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.discord.samsung.intent.extra.ATTEMPT_COUNT"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    add-int/2addr v1, v0

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {p2, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p2}, Landroid/app/Activity;->finish()V

    return-void

    :cond_2
    const/16 v0, 0x4c5

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "additional"

    const-string v4, "api_server_url"

    const-string v5, "auth_server_url"

    filled-new-array {v4, v5}, [Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {v1, v0, p2, v2}, Lf/k/a/a/b;->Z(ILjava/lang/String;Landroid/os/Bundle;)Z

    move-result p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Samsung Account service connection established: isReqSucc? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    new-instance p2, Ljava/lang/Exception;

    const-string v0, "Call Samsung.requestAuthCode failed"

    invoke-direct {p2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_4
    const-string p2, "samsungAccountServiceCallback"

    invoke-static {p2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :catchall_0
    move-exception p2

    move-object v2, p2

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "Unable to connect to Samsung"

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    iget-object p2, p0, Lcom/discord/samsung/SamsungConnectActivity$b;->d:Lcom/discord/samsung/SamsungConnectActivity;

    invoke-static {p2, p1, p1}, Lcom/discord/samsung/SamsungConnectActivity;->a(Lcom/discord/samsung/SamsungConnectActivity;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "Samsung Account service connection unbound"

    invoke-static {p1}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/samsung/SamsungConnectActivity$b;->d:Lcom/discord/samsung/SamsungConnectActivity;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/discord/samsung/SamsungConnectActivity;->e:Z

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method
