.class public final Lcom/discord/samsung/SamsungConnectActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "SamsungConnectActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/samsung/SamsungConnectActivity$a;
    }
.end annotation


# static fields
.field public static final g:Lcom/discord/samsung/SamsungConnectActivity$a;


# instance fields
.field public d:Lf/k/a/a/a;

.field public e:Z

.field public final f:Lcom/discord/samsung/SamsungConnectActivity$b;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/samsung/SamsungConnectActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/samsung/SamsungConnectActivity$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/samsung/SamsungConnectActivity;->g:Lcom/discord/samsung/SamsungConnectActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    new-instance v0, Lcom/discord/samsung/SamsungConnectActivity$b;

    invoke-direct {v0, p0}, Lcom/discord/samsung/SamsungConnectActivity$b;-><init>(Lcom/discord/samsung/SamsungConnectActivity;)V

    iput-object v0, p0, Lcom/discord/samsung/SamsungConnectActivity;->f:Lcom/discord/samsung/SamsungConnectActivity$b;

    return-void
.end method

.method public static final a(Lcom/discord/samsung/SamsungConnectActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-static {p1}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_5

    if-eqz p2, :cond_3

    invoke-static {p2}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "SAMSUNG_REQ_AUTH_PARAM_AUTHCODE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "SAMSUNG_REQ_AUTH_PARAM_AUTH_SERVER_URL"

    invoke-virtual {v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_4

    :cond_5
    :goto_3
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    :goto_4
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Lf/a/i/c;

    invoke-direct {p1, p0}, Lf/a/i/c;-><init>(Lcom/discord/samsung/SamsungConnectActivity;)V

    iput-object p1, p0, Lcom/discord/samsung/SamsungConnectActivity;->d:Lf/k/a/a/a;

    new-instance p1, Landroid/content/Intent;

    const-string v0, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.osp.app.signin"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const-string v0, "Intent(SAMSUNG_REQ_SA_SE\u2026(SAMSUNG_ACCOUNT_SERVICE)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Samsung starting SA Service"

    invoke-static {v0}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    const-string v2, "Samsung Account service could not be started"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/samsung/SamsungConnectActivity;->f:Lcom/discord/samsung/SamsungConnectActivity$b;

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result p1

    if-nez p1, :cond_1

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    const-string v1, "Samsung Account service could not be bound"

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onStop()V
    .locals 1

    iget-boolean v0, p0, Lcom/discord/samsung/SamsungConnectActivity;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/samsung/SamsungConnectActivity;->f:Lcom/discord/samsung/SamsungConnectActivity$b;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/discord/samsung/SamsungConnectActivity;->e:Z

    :cond_0
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStop()V

    return-void
.end method
