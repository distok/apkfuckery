.class public final Lcom/discord/app/AppActivity$f;
.super Lx/m/c/k;
.source "AppActivity.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/app/AppActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/app/AppActivity$a;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/app/AppActivity;


# direct methods
.method public constructor <init>(Lcom/discord/app/AppActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/app/AppActivity$f;->this$0:Lcom/discord/app/AppActivity;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    check-cast p1, Lcom/discord/app/AppActivity$a;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/app/AppActivity$f;->this$0:Lcom/discord/app/AppActivity;

    sget-boolean v1, Lcom/discord/app/AppActivity;->p:Z

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lcom/discord/app/AppActivity$a;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/discord/models/domain/ModelUserSettings;->getLocaleObject(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    const-string v2, "ModelUserSettings.getLoc\u2026bject(model.localeString)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/app/AppActivity;->h(Ljava/util/Locale;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object p1, p1, Lcom/discord/app/AppActivity$a;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v2}, Lcom/discord/app/AppActivity;->b(Ljava/lang/String;Z)V

    goto :goto_2

    :cond_0
    iget-object v1, p1, Lcom/discord/app/AppActivity$a;->a:Ljava/lang/String;

    sget-object v4, Lf/a/b/m;->g:Lf/a/b/m;

    sget-object v4, Lf/a/b/m;->e:Ljava/util/List;

    invoke-virtual {v0, v4}, Lcom/discord/app/AppActivity;->i(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v4, Lf/a/b/e;

    invoke-direct {v4, v0}, Lf/a/b/e;-><init>(Lcom/discord/app/AppActivity;)V

    const v5, 0x7f0405c0

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    iget-object v4, v4, Lf/a/b/e;->this$0:Lcom/discord/app/AppActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v4, v5, v6, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget-object v4, v6, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-static {v4, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    :goto_0
    if-nez v1, :cond_6

    iget p1, p1, Lcom/discord/app/AppActivity$a;->c:I

    sget-object v1, Lcom/discord/utilities/font/FontUtils;->INSTANCE:Lcom/discord/utilities/font/FontUtils;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "contentResolver"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/discord/utilities/font/FontUtils;->getSystemFontScaleInt(Landroid/content/ContentResolver;)I

    move-result v1

    const/4 v4, -0x1

    if-ne p1, v4, :cond_2

    iget v5, v0, Lcom/discord/app/AppActivity;->h:I

    if-ne v5, v1, :cond_3

    :cond_2
    if-eq p1, v4, :cond_4

    iget v0, v0, Lcom/discord/app/AppActivity;->h:I

    if-eq v0, p1, :cond_4

    :cond_3
    const/4 p1, 0x1

    goto :goto_1

    :cond_4
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_5

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    goto :goto_3

    :cond_6
    :goto_2
    const/4 p1, 0x1

    :goto_3
    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/discord/app/AppActivity$f;->this$0:Lcom/discord/app/AppActivity;

    const/4 v0, 0x0

    invoke-static {p1, v3, v2, v0}, Lcom/discord/app/AppActivity;->k(Lcom/discord/app/AppActivity;ZILjava/lang/Object;)V

    :cond_7
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
