.class public final Lcom/discord/app/AppScrollingViewBehavior;
.super Lcom/google/android/material/appbar/AppBarLayout$ScrollingViewBehavior;
.source "AppScrollingViewBehavior.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/app/AppScrollingViewBehavior$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/discord/app/AppScrollingViewBehavior$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/material/appbar/AppBarLayout$ScrollingViewBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Lcom/discord/app/AppScrollingViewBehavior$a;

    invoke-direct {p1}, Lcom/discord/app/AppScrollingViewBehavior$a;-><init>()V

    iput-object p1, p0, Lcom/discord/app/AppScrollingViewBehavior;->a:Lcom/discord/app/AppScrollingViewBehavior$a;

    return-void
.end method


# virtual methods
.method public onApplyWindowInsets(Landroidx/coordinatorlayout/widget/CoordinatorLayout;Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 1

    const-string v0, "coordinatorLayout"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "child"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "insets"

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/utilities/display/DisplayUtils;->getNO_OP_WINDOW_INSETS_LISTENER()Landroidx/core/view/OnApplyWindowInsetsListener;

    move-result-object p1

    invoke-static {p2, p1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    iget-object p1, p0, Lcom/discord/app/AppScrollingViewBehavior;->a:Lcom/discord/app/AppScrollingViewBehavior$a;

    invoke-virtual {p1, p2, p3}, Lcom/discord/app/AppScrollingViewBehavior$a;->onApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    return-object p3
.end method
