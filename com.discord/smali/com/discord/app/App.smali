.class public Lcom/discord/app/App;
.super Landroid/app/Application;
.source "App.kt"


# static fields
.field public static final d:Z

.field public static final e:Lcom/discord/app/App;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const-string v0, "productionDiscordExternal"

    const-string v1, "local"

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v0

    sput-boolean v0, Lcom/discord/app/App;->d:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 19

    move-object/from16 v1, p0

    invoke-super/range {p0 .. p0}, Landroid/app/Application;->onCreate()V

    sget-object v0, Lcom/discord/utilities/cache/SharedPreferencesProvider;->INSTANCE:Lcom/discord/utilities/cache/SharedPreferencesProvider;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/cache/SharedPreferencesProvider;->init(Landroid/content/Context;)V

    sget-object v0, Lcom/discord/utilities/app/ApplicationProvider;->INSTANCE:Lcom/discord/utilities/app/ApplicationProvider;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/app/ApplicationProvider;->init(Landroid/app/Application;)V

    sget-object v0, Lcom/discord/utilities/time/ClockFactory;->INSTANCE:Lcom/discord/utilities/time/ClockFactory;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/time/ClockFactory;->init(Landroid/app/Application;)V

    sget-object v0, Lcom/discord/app/AppLog;->a:Landroid/content/SharedPreferences;

    const-string v0, "application"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static/range {p0 .. p0}, Landroidx/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/discord/app/AppLog;->a:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/discord/utilities/logging/LoggingProvider;->INSTANCE:Lcom/discord/utilities/logging/LoggingProvider;

    sget-object v9, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-virtual {v0, v9}, Lcom/discord/utilities/logging/LoggingProvider;->init(Lcom/discord/utilities/logging/Logger;)V

    invoke-static/range {p0 .. p0}, Lf/h/c/c;->e(Landroid/content/Context;)Lf/h/c/c;

    sget-object v0, Lcom/discord/utilities/buildutils/BuildUtils;->INSTANCE:Lcom/discord/utilities/buildutils/BuildUtils;

    const-string v2, "52.1"

    invoke-virtual {v0, v2}, Lcom/discord/utilities/buildutils/BuildUtils;->isValidBuildVersionName(Ljava/lang/String;)Z

    move-result v0

    invoke-static {}, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->getInstance()Lcom/google/firebase/crashlytics/FirebaseCrashlytics;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->setCrashlyticsCollectionEnabled(Z)V

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "libdiscord_version"

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const-string v0, "Unknown"

    :goto_1
    invoke-virtual {v9, v0, v2}, Lcom/discord/app/AppLog;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/analytics/AdjustConfig;->INSTANCE:Lcom/discord/utilities/analytics/AdjustConfig;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/analytics/AdjustConfig;->init(Landroid/app/Application;Z)V

    invoke-static/range {p0 .. p0}, Lcom/miguelgaeta/backgrounded/Backgrounded;->init(Landroid/app/Application;)V

    sget-object v0, Lcom/discord/utilities/persister/PersisterConfig;->INSTANCE:Lcom/discord/utilities/persister/PersisterConfig;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/persister/PersisterConfig;->init(Landroid/content/Context;Lcom/discord/utilities/time/Clock;)V

    invoke-static {}, Lcom/miguelgaeta/backgrounded/Backgrounded;->get()Lrx/Observable;

    move-result-object v0

    const-string v2, "Backgrounded\n        .get()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v10

    const-string v0, "Backgrounded\n        .ge\u2026  .distinctUntilChanged()"

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    sget-object v16, Lcom/discord/app/App$a;->d:Lcom/discord/app/App$a;

    const/16 v17, 0x1e

    const/16 v18, 0x0

    invoke-static/range {v10 .. v18}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->init(Landroid/content/Context;)V

    sget-object v0, Lcom/discord/utilities/fcm/NotificationClient;->INSTANCE:Lcom/discord/utilities/fcm/NotificationClient;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/fcm/NotificationClient;->init(Landroid/app/Application;)V

    sget-object v0, Lcom/discord/utilities/images/MGImagesConfig;->INSTANCE:Lcom/discord/utilities/images/MGImagesConfig;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/images/MGImagesConfig;->init(Landroid/app/Application;)V

    new-instance v0, Lcom/discord/app/App$b;

    invoke-direct {v0, v9}, Lcom/discord/app/App$b;-><init>(Lcom/discord/app/AppLog;)V

    new-instance v2, Lf/a/b/a;

    invoke-direct {v2, v0}, Lf/a/b/a;-><init>(Lkotlin/jvm/functions/Function3;)V

    invoke-static {v2}, Lcom/discord/utilities/error/Error;->init(Lrx/functions/Action3;)V

    sget-object v0, Lcom/discord/utilities/view/text/LinkifiedTextView;->Companion:Lcom/discord/utilities/view/text/LinkifiedTextView$Companion;

    sget-object v2, Lcom/discord/app/App$c;->d:Lcom/discord/app/App$c;

    invoke-virtual {v0, v2}, Lcom/discord/utilities/view/text/LinkifiedTextView$Companion;->init(Lkotlin/jvm/functions/Function2;)Lkotlin/Unit;

    const-string v0, "https://cdn.discordapp.com"

    invoke-static {v0}, Lcom/discord/models/domain/emoji/ModelEmojiCustom;->setCdnUri(Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/surveys/SurveyUtils;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/surveys/SurveyUtils;->init(Landroid/app/Application;)V

    const/4 v10, 0x1

    invoke-static {v10}, Landroidx/appcompat/app/AppCompatDelegate;->setDefaultNightMode(I)V

    const-string v0, "context"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v11, Lf/a/b/p0;->d:Lf/a/b/p0;

    monitor-enter v11

    :try_start_0
    const-string v0, "logger"

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-boolean v0, Lf/a/b/p0;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    monitor-exit v11

    goto/16 :goto_4

    :cond_2
    :try_start_1
    sget-object v0, Lf/a/b/p0;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v8}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    sget-object v0, Lf/a/b/p0;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v3, "NativeLoader"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loaded lib: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v9

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->d$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v5, v0

    :try_start_3
    const-string v3, "NativeLoader"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load lib: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v9

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_2

    :cond_3
    sget-object v0, Lf/a/b/p0;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget-object v2, Lf/a/b/p0;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_4

    const-string v3, "NativeLoader"

    const-string v4, "Failed to load all native libs"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    move-object v2, v9

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_3

    :cond_4
    sput-boolean v10, Lf/a/b/p0;->c:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_3
    monitor-exit v11

    :goto_4
    const-string v0, "Application initialized."

    invoke-static {v0}, Lcom/discord/app/AppLog;->i(Ljava/lang/String;)V

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->getInstance()Lcom/google/firebase/crashlytics/FirebaseCrashlytics;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->didCrashOnPreviousExecution()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->appCrashed()V

    :cond_5
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0
.end method

.method public onTrimMemory(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    sget-object v0, Lcom/discord/utilities/images/MGImagesConfig;->INSTANCE:Lcom/discord/utilities/images/MGImagesConfig;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/images/MGImagesConfig;->onTrimMemory(I)V

    return-void
.end method
