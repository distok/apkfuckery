.class public final enum Lcom/discord/app/AppTransitionActivity$Transition;
.super Ljava/lang/Enum;
.source "AppTransitionActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/app/AppTransitionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Transition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/app/AppTransitionActivity$Transition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/app/AppTransitionActivity$Transition;

.field public static final enum TYPE_FADE:Lcom/discord/app/AppTransitionActivity$Transition;

.field public static final enum TYPE_NONE:Lcom/discord/app/AppTransitionActivity$Transition;

.field public static final enum TYPE_SLIDE_HORIZONTAL:Lcom/discord/app/AppTransitionActivity$Transition;

.field public static final enum TYPE_SLIDE_POP_HORIZONTAL:Lcom/discord/app/AppTransitionActivity$Transition;

.field public static final enum TYPE_SLIDE_POP_VERTICAL:Lcom/discord/app/AppTransitionActivity$Transition;

.field public static final enum TYPE_SLIDE_VERTICAL:Lcom/discord/app/AppTransitionActivity$Transition;

.field public static final enum TYPE_STANDARD:Lcom/discord/app/AppTransitionActivity$Transition;


# instance fields
.field private final animations:Lcom/discord/app/AppTransitionActivity$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v1, Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v2, Lcom/discord/app/AppTransitionActivity$a;

    const v3, 0x7f01000e

    const v4, 0x7f01000f

    const v5, 0x7f01000c

    const v6, 0x7f01000d

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/discord/app/AppTransitionActivity$a;-><init>(IIII)V

    const-string v3, "TYPE_FADE"

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4, v2}, Lcom/discord/app/AppTransitionActivity$Transition;-><init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V

    sput-object v1, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_FADE:Lcom/discord/app/AppTransitionActivity$Transition;

    aput-object v1, v0, v4

    new-instance v1, Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v2, Lcom/discord/app/AppTransitionActivity$a;

    const v3, 0x7f010022

    const v5, 0x7f010023

    const v6, 0x7f010020

    const v7, 0x7f010021

    invoke-direct {v2, v3, v5, v6, v7}, Lcom/discord/app/AppTransitionActivity$a;-><init>(IIII)V

    const-string v3, "TYPE_STANDARD"

    const/4 v5, 0x1

    invoke-direct {v1, v3, v5, v2}, Lcom/discord/app/AppTransitionActivity$Transition;-><init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V

    sput-object v1, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_STANDARD:Lcom/discord/app/AppTransitionActivity$Transition;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v2, Lcom/discord/app/AppTransitionActivity$a;

    const v3, 0x7f010012

    const v5, 0x7f010013

    const v6, 0x7f010010

    const v7, 0x7f010011

    invoke-direct {v2, v3, v5, v6, v7}, Lcom/discord/app/AppTransitionActivity$a;-><init>(IIII)V

    const-string v3, "TYPE_SLIDE_HORIZONTAL"

    const/4 v5, 0x2

    invoke-direct {v1, v3, v5, v2}, Lcom/discord/app/AppTransitionActivity$Transition;-><init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V

    sput-object v1, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_SLIDE_HORIZONTAL:Lcom/discord/app/AppTransitionActivity$Transition;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v2, Lcom/discord/app/AppTransitionActivity$a;

    const v3, 0x7f01001e

    const v5, 0x7f01001f

    const v6, 0x7f01001c

    const v7, 0x7f01001d

    invoke-direct {v2, v3, v5, v6, v7}, Lcom/discord/app/AppTransitionActivity$a;-><init>(IIII)V

    const-string v3, "TYPE_SLIDE_VERTICAL"

    const/4 v5, 0x3

    invoke-direct {v1, v3, v5, v2}, Lcom/discord/app/AppTransitionActivity$Transition;-><init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V

    sput-object v1, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_SLIDE_VERTICAL:Lcom/discord/app/AppTransitionActivity$Transition;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v2, Lcom/discord/app/AppTransitionActivity$a;

    const v3, 0x7f01001a

    const v5, 0x7f01001b

    const v6, 0x7f010018

    const v7, 0x7f010019

    invoke-direct {v2, v3, v5, v6, v7}, Lcom/discord/app/AppTransitionActivity$a;-><init>(IIII)V

    const-string v3, "TYPE_SLIDE_POP_VERTICAL"

    const/4 v5, 0x4

    invoke-direct {v1, v3, v5, v2}, Lcom/discord/app/AppTransitionActivity$Transition;-><init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V

    sput-object v1, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_SLIDE_POP_VERTICAL:Lcom/discord/app/AppTransitionActivity$Transition;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v2, Lcom/discord/app/AppTransitionActivity$a;

    const v3, 0x7f010016

    const v5, 0x7f010017

    const v6, 0x7f010014

    const v7, 0x7f010015

    invoke-direct {v2, v3, v5, v6, v7}, Lcom/discord/app/AppTransitionActivity$a;-><init>(IIII)V

    const-string v3, "TYPE_SLIDE_POP_HORIZONTAL"

    const/4 v5, 0x5

    invoke-direct {v1, v3, v5, v2}, Lcom/discord/app/AppTransitionActivity$Transition;-><init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V

    sput-object v1, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_SLIDE_POP_HORIZONTAL:Lcom/discord/app/AppTransitionActivity$Transition;

    aput-object v1, v0, v5

    new-instance v1, Lcom/discord/app/AppTransitionActivity$Transition;

    new-instance v2, Lcom/discord/app/AppTransitionActivity$a;

    invoke-direct {v2, v4, v4, v4, v4}, Lcom/discord/app/AppTransitionActivity$a;-><init>(IIII)V

    const-string v3, "TYPE_NONE"

    const/4 v4, 0x6

    invoke-direct {v1, v3, v4, v2}, Lcom/discord/app/AppTransitionActivity$Transition;-><init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V

    sput-object v1, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_NONE:Lcom/discord/app/AppTransitionActivity$Transition;

    aput-object v1, v0, v4

    sput-object v0, Lcom/discord/app/AppTransitionActivity$Transition;->$VALUES:[Lcom/discord/app/AppTransitionActivity$Transition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/discord/app/AppTransitionActivity$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/app/AppTransitionActivity$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/app/AppTransitionActivity$Transition;->animations:Lcom/discord/app/AppTransitionActivity$a;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/app/AppTransitionActivity$Transition;
    .locals 1

    const-class v0, Lcom/discord/app/AppTransitionActivity$Transition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/app/AppTransitionActivity$Transition;

    return-object p0
.end method

.method public static values()[Lcom/discord/app/AppTransitionActivity$Transition;
    .locals 1

    sget-object v0, Lcom/discord/app/AppTransitionActivity$Transition;->$VALUES:[Lcom/discord/app/AppTransitionActivity$Transition;

    invoke-virtual {v0}, [Lcom/discord/app/AppTransitionActivity$Transition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/app/AppTransitionActivity$Transition;

    return-object v0
.end method


# virtual methods
.method public final getAnimations()Lcom/discord/app/AppTransitionActivity$a;
    .locals 1

    iget-object v0, p0, Lcom/discord/app/AppTransitionActivity$Transition;->animations:Lcom/discord/app/AppTransitionActivity$a;

    return-object v0
.end method
