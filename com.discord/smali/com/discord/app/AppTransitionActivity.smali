.class public abstract Lcom/discord/app/AppTransitionActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "AppTransitionActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/app/AppTransitionActivity$Transition;,
        Lcom/discord/app/AppTransitionActivity$a;
    }
.end annotation


# static fields
.field public static e:Z


# instance fields
.field public d:Lcom/discord/app/AppTransitionActivity$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    sget-object v0, Lcom/discord/app/AppTransitionActivity$Transition;->TYPE_SLIDE_POP_VERTICAL:Lcom/discord/app/AppTransitionActivity$Transition;

    invoke-virtual {v0}, Lcom/discord/app/AppTransitionActivity$Transition;->getAnimations()Lcom/discord/app/AppTransitionActivity$a;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/app/AppTransitionActivity;->d:Lcom/discord/app/AppTransitionActivity$a;

    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 1

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object p1, p0, Lcom/discord/app/AppTransitionActivity;->d:Lcom/discord/app/AppTransitionActivity$a;

    if-eqz p1, :cond_5

    if-eqz p2, :cond_3

    iget v0, p1, Lcom/discord/app/AppTransitionActivity$a;->a:I

    goto :goto_0

    :cond_3
    iget v0, p1, Lcom/discord/app/AppTransitionActivity$a;->c:I

    :goto_0
    if-eqz p2, :cond_4

    iget p1, p1, Lcom/discord/app/AppTransitionActivity$a;->b:I

    goto :goto_1

    :cond_4
    iget p1, p1, Lcom/discord/app/AppTransitionActivity$a;->d:I

    :goto_1
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->overridePendingTransition(II)V

    :cond_5
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/discord/app/AppTransitionActivity;->e:Z

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    sget-boolean v0, Lcom/discord/app/AppTransitionActivity;->e:Z

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/discord/app/AppTransitionActivity;->a(ZZ)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    sget-boolean v0, Lcom/discord/app/AppTransitionActivity;->e:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/discord/app/AppTransitionActivity;->a(ZZ)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/discord/app/AppTransitionActivity;->e:Z

    return-void
.end method
