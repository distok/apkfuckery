.class public final Lcom/discord/app/DiscordConnectService$d;
.super Lx/m/c/k;
.source "DiscordConnectService.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/app/DiscordConnectService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/error/Error;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $startId:I

.field public final synthetic this$0:Lcom/discord/app/DiscordConnectService;


# direct methods
.method public constructor <init>(Lcom/discord/app/DiscordConnectService;I)V
    .locals 0

    iput-object p1, p0, Lcom/discord/app/DiscordConnectService$d;->this$0:Lcom/discord/app/DiscordConnectService;

    iput p2, p0, Lcom/discord/app/DiscordConnectService$d;->$startId:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lcom/discord/utilities/error/Error;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/error/Error;->setShouldLog(Z)V

    sget-object v0, Lcom/discord/app/DiscordConnectService;->d:Lcom/discord/app/DiscordConnectService$a;

    const-string v1, "Request timeout["

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/discord/app/DiscordConnectService$d;->$startId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/discord/app/DiscordConnectService$a;->a(Lcom/discord/app/DiscordConnectService$a;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/app/DiscordConnectService$d;->this$0:Lcom/discord/app/DiscordConnectService;

    iget v0, p0, Lcom/discord/app/DiscordConnectService$d;->$startId:I

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/app/Service;->stopForeground(Z)V

    invoke-virtual {p1, v0}, Landroid/app/Service;->stopSelf(I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
