.class public final Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog$Companion;
.super Ljava/lang/Object;
.source "PremiumStickerPackUpsellTier2Dialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "fragmentManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onUpgradeClickListener"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;

    invoke-direct {v0}, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;-><init>()V

    iput-object p2, v0, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;->d:Lkotlin/jvm/functions/Function0;

    iput-object p3, v0, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;->e:Lkotlin/jvm/functions/Function0;

    const-class p2, Lcom/discord/dialogs/premium/PremiumStickerPackUpsellTier2Dialog;

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
