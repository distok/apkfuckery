.class public final Lcom/discord/dialogs/ImageUploadDialog$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/dialogs/ImageUploadDialog;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lcom/discord/dialogs/ImageUploadDialog$a;->d:I

    iput-object p2, p0, Lcom/discord/dialogs/ImageUploadDialog$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget p1, p0, Lcom/discord/dialogs/ImageUploadDialog$a;->d:I

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$a;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void

    :cond_0
    throw v0

    :cond_1
    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$a;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/dialogs/ImageUploadDialog$a;->e:Ljava/lang/Object;

    check-cast v1, Lcom/discord/dialogs/ImageUploadDialog;

    iget-object v2, v1, Lcom/discord/dialogs/ImageUploadDialog;->i:Lcom/miguelgaeta/media_picker/MediaPicker$Provider;

    if-eqz v2, :cond_3

    iget-object v1, v1, Lcom/discord/dialogs/ImageUploadDialog;->h:Landroid/net/Uri;

    if-eqz v1, :cond_2

    invoke-static {p1, v2, v1}, Lcom/discord/utilities/images/MGImages;->requestAvatarCrop(Landroid/content/Context;Lcom/miguelgaeta/media_picker/MediaPicker$Provider;Landroid/net/Uri;)V

    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$a;->e:Ljava/lang/Object;

    check-cast p1, Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void

    :cond_2
    const-string/jumbo p1, "uri"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_3
    const-string p1, "provider"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method
