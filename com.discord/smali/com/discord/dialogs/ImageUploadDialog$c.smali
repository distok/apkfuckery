.class public final Lcom/discord/dialogs/ImageUploadDialog$c;
.super Ljava/lang/Object;
.source "ImageUploadDialog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/dialogs/ImageUploadDialog;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/dialogs/ImageUploadDialog;

.field public final synthetic e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/discord/dialogs/ImageUploadDialog;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$c;->d:Lcom/discord/dialogs/ImageUploadDialog;

    iput-object p2, p0, Lcom/discord/dialogs/ImageUploadDialog$c;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$c;->d:Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog$c;->d:Lcom/discord/dialogs/ImageUploadDialog;

    iget-object v1, v0, Lcom/discord/dialogs/ImageUploadDialog;->h:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/discord/dialogs/ImageUploadDialog$c;->e:Ljava/lang/String;

    iget-object v0, v0, Lcom/discord/dialogs/ImageUploadDialog;->k:Lrx/functions/Action1;

    invoke-static {p1, v1, v2, v0}, Lcom/discord/utilities/images/MGImages;->requestDataUrl(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lrx/functions/Action1;)V

    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog$c;->d:Lcom/discord/dialogs/ImageUploadDialog;

    invoke-virtual {p1}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void

    :cond_0
    const-string/jumbo p1, "uri"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method
