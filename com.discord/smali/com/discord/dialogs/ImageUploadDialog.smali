.class public final Lcom/discord/dialogs/ImageUploadDialog;
.super Lcom/discord/app/AppDialog;
.source "ImageUploadDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/dialogs/ImageUploadDialog$PreviewType;,
        Lcom/discord/dialogs/ImageUploadDialog$b;
    }
.end annotation


# static fields
.field public static final synthetic m:[Lkotlin/reflect/KProperty;

.field public static final n:Lcom/discord/dialogs/ImageUploadDialog$b;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lkotlin/properties/ReadOnlyProperty;

.field public final g:Lkotlin/properties/ReadOnlyProperty;

.field public h:Landroid/net/Uri;

.field public i:Lcom/miguelgaeta/media_picker/MediaPicker$Provider;

.field public j:Ljava/lang/String;

.field public k:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/discord/dialogs/ImageUploadDialog$PreviewType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/dialogs/ImageUploadDialog;

    const-string/jumbo v3, "upload"

    const-string v4, "getUpload()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/dialogs/ImageUploadDialog;

    const-string v6, "crop"

    const-string v7, "getCrop()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/dialogs/ImageUploadDialog;

    const-string v6, "cancel"

    const-string v7, "getCancel()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/dialogs/ImageUploadDialog;

    const-string v6, "image"

    const-string v7, "getImage()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/dialogs/ImageUploadDialog;->m:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/dialogs/ImageUploadDialog$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/dialogs/ImageUploadDialog$b;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/dialogs/ImageUploadDialog;->n:Lcom/discord/dialogs/ImageUploadDialog$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a06fd

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->d:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06f8

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->e:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06f7

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->f:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06fb

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->g:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method


# virtual methods
.method public final f()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->g:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/dialogs/ImageUploadDialog;->m:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d006b

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 14

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    iget-object v0, p0, Lcom/discord/dialogs/ImageUploadDialog;->l:Lcom/discord/dialogs/ImageUploadDialog$PreviewType;

    iget-object v1, p0, Lcom/discord/dialogs/ImageUploadDialog;->j:Ljava/lang/String;

    if-eqz v0, :cond_5

    if-nez v1, :cond_0

    goto/16 :goto_2

    :cond_0
    const-string v2, "image/gif"

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    iget-object v3, p0, Lcom/discord/dialogs/ImageUploadDialog;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v4, Lcom/discord/dialogs/ImageUploadDialog;->m:[Lkotlin/reflect/KProperty;

    const/4 v5, 0x0

    aget-object v6, v4, v5

    invoke-interface {v3, p0, v6}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    new-instance v6, Lcom/discord/dialogs/ImageUploadDialog$c;

    invoke-direct {v6, p0, v1}, Lcom/discord/dialogs/ImageUploadDialog$c;-><init>(Lcom/discord/dialogs/ImageUploadDialog;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/discord/dialogs/ImageUploadDialog;->e:Lkotlin/properties/ReadOnlyProperty;

    const/4 v3, 0x1

    aget-object v6, v4, v3

    invoke-interface {v1, p0, v6}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v2, :cond_1

    const/4 v6, 0x4

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/discord/dialogs/ImageUploadDialog;->e:Lkotlin/properties/ReadOnlyProperty;

    aget-object v2, v4, v3

    invoke-interface {v1, p0, v2}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v2, Lcom/discord/dialogs/ImageUploadDialog$a;

    invoke-direct {v2, v5, p0}, Lcom/discord/dialogs/ImageUploadDialog$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v1, p0, Lcom/discord/dialogs/ImageUploadDialog;->f:Lkotlin/properties/ReadOnlyProperty;

    const/4 v2, 0x2

    aget-object v4, v4, v2

    invoke-interface {v1, p0, v4}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v4, Lcom/discord/dialogs/ImageUploadDialog$a;

    invoke-direct {v4, v3, p0}, Lcom/discord/dialogs/ImageUploadDialog$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/dialogs/ImageUploadDialog$PreviewType;->getPreviewSizeDimenId()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {p0}, Lcom/discord/dialogs/ImageUploadDialog;->f()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v8, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v8, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/discord/dialogs/ImageUploadDialog;->f()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eq v0, v3, :cond_3

    if-eq v0, v2, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/discord/dialogs/ImageUploadDialog;->f()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    int-to-float v1, v8

    const v2, 0x7f0404a0

    invoke-static {p1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0, v1, v3, p1}, Lcom/discord/utilities/images/MGImages;->setCornerRadius(Landroid/widget/ImageView;FZLjava/lang/Integer;)V

    :goto_1
    invoke-virtual {p0}, Lcom/discord/dialogs/ImageUploadDialog;->f()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v5

    iget-object p1, p0, Lcom/discord/dialogs/ImageUploadDialog;->h:Landroid/net/Uri;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x70

    const/4 v13, 0x0

    move v7, v8

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void

    :cond_4
    const-string/jumbo p1, "uri"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void
.end method
