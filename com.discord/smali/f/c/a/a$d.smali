.class public Lf/c/a/a$d;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->j(Lf/c/a/a0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/a0;

.field public final synthetic e:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;Lf/c/a/a0;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$d;->e:Lf/c/a/a;

    iput-object p2, p0, Lf/c/a/a$d;->d:Lf/c/a/a0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/c/a/a$d;->e:Lf/c/a/a;

    iget-object v1, p0, Lf/c/a/a$d;->d:Lf/c/a/a0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lf/c/a/y0;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lf/c/a/a;->J(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Handler;

    iget-object v3, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v3, v1, Lf/c/a/y0;->h:Lcom/adjust/sdk/AdjustAttribution;

    invoke-virtual {v0, v3}, Lf/c/a/a;->K(Lcom/adjust/sdk/AdjustAttribution;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Lf/c/a/a;->v(Landroid/os/Handler;)V

    :cond_0
    iget-object v1, v1, Lf/c/a/a0;->i:Landroid/net/Uri;

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const-string v5, "Deferred deeplink received (%s)"

    invoke-interface {v3, v5, v4}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v4, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v4, v4, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v4, Lf/c/a/i;

    invoke-direct {v4, v0, v1, v3}, Lf/c/a/i;-><init>(Lf/c/a/a;Landroid/net/Uri;Landroid/content/Intent;)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method
