.class public final enum Lf/c/a/q0;
.super Ljava/lang/Enum;
.source "LogLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/c/a/q0;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/c/a/q0;

.field public static final enum e:Lf/c/a/q0;

.field public static final enum f:Lf/c/a/q0;

.field public static final enum g:Lf/c/a/q0;

.field public static final enum h:Lf/c/a/q0;

.field public static final enum i:Lf/c/a/q0;

.field public static final enum j:Lf/c/a/q0;

.field public static final synthetic k:[Lf/c/a/q0;


# instance fields
.field public final androidLogLevel:I


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    new-instance v0, Lf/c/a/q0;

    const-string v1, "VERBOSE"

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lf/c/a/q0;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/c/a/q0;->d:Lf/c/a/q0;

    new-instance v1, Lf/c/a/q0;

    const-string v4, "DEBUG"

    const/4 v5, 0x1

    const/4 v6, 0x3

    invoke-direct {v1, v4, v5, v6}, Lf/c/a/q0;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/c/a/q0;->e:Lf/c/a/q0;

    new-instance v4, Lf/c/a/q0;

    const-string v7, "INFO"

    const/4 v8, 0x4

    invoke-direct {v4, v7, v3, v8}, Lf/c/a/q0;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lf/c/a/q0;->f:Lf/c/a/q0;

    new-instance v7, Lf/c/a/q0;

    const-string v9, "WARN"

    const/4 v10, 0x5

    invoke-direct {v7, v9, v6, v10}, Lf/c/a/q0;-><init>(Ljava/lang/String;II)V

    sput-object v7, Lf/c/a/q0;->g:Lf/c/a/q0;

    new-instance v9, Lf/c/a/q0;

    const-string v11, "ERROR"

    const/4 v12, 0x6

    invoke-direct {v9, v11, v8, v12}, Lf/c/a/q0;-><init>(Ljava/lang/String;II)V

    sput-object v9, Lf/c/a/q0;->h:Lf/c/a/q0;

    new-instance v11, Lf/c/a/q0;

    const-string v13, "ASSERT"

    const/4 v14, 0x7

    invoke-direct {v11, v13, v10, v14}, Lf/c/a/q0;-><init>(Ljava/lang/String;II)V

    sput-object v11, Lf/c/a/q0;->i:Lf/c/a/q0;

    new-instance v13, Lf/c/a/q0;

    const-string v15, "SUPRESS"

    const/16 v10, 0x8

    invoke-direct {v13, v15, v12, v10}, Lf/c/a/q0;-><init>(Ljava/lang/String;II)V

    sput-object v13, Lf/c/a/q0;->j:Lf/c/a/q0;

    new-array v10, v14, [Lf/c/a/q0;

    aput-object v0, v10, v2

    aput-object v1, v10, v5

    aput-object v4, v10, v3

    aput-object v7, v10, v6

    aput-object v9, v10, v8

    const/4 v0, 0x5

    aput-object v11, v10, v0

    aput-object v13, v10, v12

    sput-object v10, Lf/c/a/q0;->k:[Lf/c/a/q0;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/c/a/q0;->androidLogLevel:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/c/a/q0;
    .locals 1

    const-class v0, Lf/c/a/q0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/c/a/q0;

    return-object p0
.end method

.method public static values()[Lf/c/a/q0;
    .locals 1

    sget-object v0, Lf/c/a/q0;->k:[Lf/c/a/q0;

    invoke-virtual {v0}, [Lf/c/a/q0;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/c/a/q0;

    return-object v0
.end method
