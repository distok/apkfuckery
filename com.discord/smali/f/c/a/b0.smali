.class public final enum Lf/c/a/b0;
.super Ljava/lang/Enum;
.source "BackoffStrategy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/c/a/b0;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/c/a/b0;

.field public static final enum e:Lf/c/a/b0;

.field public static final enum f:Lf/c/a/b0;

.field public static final enum g:Lf/c/a/b0;

.field public static final synthetic h:[Lf/c/a/b0;


# instance fields
.field public maxRange:D

.field public maxWait:J

.field public milliSecondMultiplier:J

.field public minRange:D

.field public minRetries:I


# direct methods
.method public static constructor <clinit>()V
    .locals 37

    new-instance v12, Lf/c/a/b0;

    const-string v1, "LONG_WAIT"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-wide/32 v4, 0x1d4c0

    const-wide/32 v6, 0x5265c00

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lf/c/a/b0;-><init>(Ljava/lang/String;IIJJDD)V

    sput-object v12, Lf/c/a/b0;->d:Lf/c/a/b0;

    new-instance v0, Lf/c/a/b0;

    const-string v14, "SHORT_WAIT"

    const/4 v15, 0x1

    const/16 v16, 0x1

    const-wide/16 v17, 0xc8

    const-wide/32 v19, 0x36ee80

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v23, 0x3ff0000000000000L    # 1.0

    move-object v13, v0

    invoke-direct/range {v13 .. v24}, Lf/c/a/b0;-><init>(Ljava/lang/String;IIJJDD)V

    sput-object v0, Lf/c/a/b0;->e:Lf/c/a/b0;

    new-instance v1, Lf/c/a/b0;

    const-string v26, "TEST_WAIT"

    const/16 v27, 0x2

    const/16 v28, 0x1

    const-wide/16 v29, 0xc8

    const-wide/16 v31, 0x3e8

    const-wide/high16 v33, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v35, 0x3ff0000000000000L    # 1.0

    move-object/from16 v25, v1

    invoke-direct/range {v25 .. v36}, Lf/c/a/b0;-><init>(Ljava/lang/String;IIJJDD)V

    sput-object v1, Lf/c/a/b0;->f:Lf/c/a/b0;

    new-instance v2, Lf/c/a/b0;

    const-string v14, "NO_WAIT"

    const/4 v15, 0x3

    const/16 v16, 0x64

    const-wide/16 v17, 0x1

    const-wide/16 v19, 0x3e8

    const-wide/high16 v21, 0x3ff0000000000000L    # 1.0

    move-object v13, v2

    invoke-direct/range {v13 .. v24}, Lf/c/a/b0;-><init>(Ljava/lang/String;IIJJDD)V

    sput-object v2, Lf/c/a/b0;->g:Lf/c/a/b0;

    const/4 v3, 0x4

    new-array v3, v3, [Lf/c/a/b0;

    const/4 v4, 0x0

    aput-object v12, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x2

    aput-object v1, v3, v0

    const/4 v0, 0x3

    aput-object v2, v3, v0

    sput-object v3, Lf/c/a/b0;->h:[Lf/c/a/b0;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIJJDD)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJJDD)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/c/a/b0;->minRetries:I

    iput-wide p4, p0, Lf/c/a/b0;->milliSecondMultiplier:J

    iput-wide p6, p0, Lf/c/a/b0;->maxWait:J

    iput-wide p8, p0, Lf/c/a/b0;->minRange:D

    iput-wide p10, p0, Lf/c/a/b0;->maxRange:D

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/c/a/b0;
    .locals 1

    const-class v0, Lf/c/a/b0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/c/a/b0;

    return-object p0
.end method

.method public static values()[Lf/c/a/b0;
    .locals 1

    sget-object v0, Lf/c/a/b0;->h:[Lf/c/a/b0;

    invoke-virtual {v0}, [Lf/c/a/b0;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/c/a/b0;

    return-object v0
.end method
