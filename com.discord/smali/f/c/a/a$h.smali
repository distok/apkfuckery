.class public Lf/c/a/a$h;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$h;->d:Lf/c/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    iget-object v0, p0, Lf/c/a/a$h;->d:Lf/c/a/a;

    iget-object v0, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/a$h;->d:Lf/c/a/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/a$h;->d:Lf/c/a/a;

    invoke-virtual {v0}, Lf/c/a/a;->u()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lf/c/a/a;->e:Lf/c/a/n1/j;

    iget-boolean v1, v0, Lf/c/a/n1/j;->g:Z

    const/4 v3, 0x1

    if-nez v1, :cond_1

    iget-object v1, v0, Lf/c/a/n1/j;->h:Lf/c/a/j0;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, v0, Lf/c/a/n1/j;->c:Ljava/lang/String;

    aput-object v0, v3, v2

    const-string v0, "%s is already started"

    invoke-interface {v1, v0, v3}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lf/c/a/n1/j;->h:Lf/c/a/j0;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Lf/c/a/n1/j;->c:Ljava/lang/String;

    aput-object v4, v3, v2

    const-string v4, "%s starting"

    invoke-interface {v1, v4, v3}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, v0, Lf/c/a/n1/j;->a:Lf/c/a/n1/a;

    new-instance v3, Lf/c/a/n1/i;

    invoke-direct {v3, v0}, Lf/c/a/n1/i;-><init>(Lf/c/a/n1/j;)V

    iget-wide v6, v0, Lf/c/a/n1/j;->e:J

    iget-wide v8, v0, Lf/c/a/n1/j;->f:J

    check-cast v1, Lf/c/a/n1/e;

    iget-object v4, v1, Lf/c/a/n1/e;->a:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v5, Lf/c/a/n1/b;

    invoke-direct {v5, v3}, Lf/c/a/n1/b;-><init>(Ljava/lang/Runnable;)V

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v4 .. v10}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, v0, Lf/c/a/n1/j;->b:Ljava/util/concurrent/ScheduledFuture;

    iput-boolean v2, v0, Lf/c/a/n1/j;->g:Z

    :goto_0
    iget-object v0, p0, Lf/c/a/a$h;->d:Lf/c/a/a;

    iget-object v0, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "Subsession start"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/c/a/a$h;->d:Lf/c/a/a;

    invoke-static {v0}, Lf/c/a/a;->p(Lf/c/a/a;)V

    return-void
.end method
