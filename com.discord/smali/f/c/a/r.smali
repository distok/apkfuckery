.class public Lf/c/a/r;
.super Ljava/lang/Object;
.source "AdjustInstance.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:J


# direct methods
.method public constructor <init>(Lf/c/a/s;Landroid/content/Context;Ljava/lang/String;J)V
    .locals 0

    iput-object p2, p0, Lf/c/a/r;->d:Landroid/content/Context;

    iput-object p3, p0, Lf/c/a/r;->e:Ljava/lang/String;

    iput-wide p4, p0, Lf/c/a/r;->f:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    new-instance v0, Lf/c/a/g1;

    iget-object v1, p0, Lf/c/a/r;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lf/c/a/r;->e:Ljava/lang/String;

    iget-wide v2, p0, Lf/c/a/r;->f:J

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0, v1, v2, v3}, Lf/c/a/g1;->e(Ljava/lang/String;J)Lorg/json/JSONArray;

    move-result-object v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_0

    monitor-exit v0

    goto :goto_1

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lf/c/a/g1;->f()Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v6, 0xa

    if-ne v5, v6, :cond_1

    monitor-exit v0

    goto :goto_1

    :cond_1
    :try_start_2
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    const/4 v1, 0x1

    invoke-virtual {v5, v1, v2, v3}, Lorg/json/JSONArray;->put(IJ)Lorg/json/JSONArray;

    const/4 v1, 0x2

    invoke-virtual {v5, v1, v6}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;

    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    invoke-virtual {v0, v4}, Lf/c/a/g1;->l(Lorg/json/JSONArray;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :catch_0
    :goto_0
    monitor-exit v0

    :goto_1
    return-void
.end method
