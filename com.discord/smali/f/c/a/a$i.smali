.class public Lf/c/a/a$i;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->onPause()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$i;->d:Lf/c/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lf/c/a/a$i;->d:Lf/c/a/a;

    invoke-virtual {v0}, Lf/c/a/a;->D()V

    iget-object v0, p0, Lf/c/a/a$i;->d:Lf/c/a/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/a$i;->d:Lf/c/a/a;

    iget-object v0, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Subsession end"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/c/a/a$i;->d:Lf/c/a/a;

    invoke-virtual {v0}, Lf/c/a/a;->E()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lf/c/a/a;->w()V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/c/a/a;->I(J)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lf/c/a/a;->O()V

    :cond_1
    return-void
.end method
