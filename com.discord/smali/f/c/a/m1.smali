.class public final Lf/c/a/m1;
.super Landroid/os/AsyncTask;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Landroid/content/Context;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/c/a/t0;


# direct methods
.method public constructor <init>(Lf/c/a/t0;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/m1;->a:Lf/c/a/t0;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, [Landroid/content/Context;

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-static {p1}, Lf/c/a/l1;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "GoogleAdId read "

    invoke-static {v2, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1
.end method

.method public onPostExecute(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Ljava/lang/String;

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    iget-object v0, p0, Lf/c/a/m1;->a:Lf/c/a/t0;

    invoke-interface {v0, p1}, Lf/c/a/t0;->onGoogleAdIdRead(Ljava/lang/String;)V

    return-void
.end method
