.class public Lf/c/a/n1/k;
.super Ljava/lang/Object;
.source "TimerOnce.java"


# instance fields
.field public a:Lf/c/a/n1/a;

.field public b:Ljava/util/concurrent/ScheduledFuture;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Runnable;

.field public e:Lf/c/a/j0;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/c/a/n1/k;->c:Ljava/lang/String;

    new-instance v0, Lf/c/a/n1/e;

    const/4 v1, 0x1

    invoke-direct {v0, p2, v1}, Lf/c/a/n1/e;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lf/c/a/n1/k;->a:Lf/c/a/n1/a;

    iput-object p1, p0, Lf/c/a/n1/k;->d:Ljava/lang/Runnable;

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/n1/k;->e:Lf/c/a/j0;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    iget-object v0, p0, Lf/c/a/n1/k;->b:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lf/c/a/n1/k;->b:Ljava/util/concurrent/ScheduledFuture;

    iget-object p1, p0, Lf/c/a/n1/k;->e:Lf/c/a/j0;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lf/c/a/n1/k;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "%s canceled"

    invoke-interface {p1, v1, v0}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public b()J
    .locals 2

    iget-object v0, p0, Lf/c/a/n1/k;->b:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public c(J)V
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lf/c/a/n1/k;->a(Z)V

    sget-object v1, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    long-to-double v2, p1

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lf/c/a/n1/k;->e:Lf/c/a/j0;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lf/c/a/n1/k;->c:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const-string v0, "%s starting. Launching in %s seconds"

    invoke-interface {v2, v0, v3}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/c/a/n1/k;->a:Lf/c/a/n1/a;

    new-instance v1, Lf/c/a/n1/k$a;

    invoke-direct {v1, p0}, Lf/c/a/n1/k$a;-><init>(Lf/c/a/n1/k;)V

    check-cast v0, Lf/c/a/n1/e;

    iget-object v0, v0, Lf/c/a/n1/e;->a:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v2, Lf/c/a/n1/b;

    invoke-direct {v2, v1}, Lf/c/a/n1/b;-><init>(Ljava/lang/Runnable;)V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, p1, p2, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/n1/k;->b:Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method
