.class public Lf/c/a/n1/e;
.super Ljava/lang/Object;
.source "SingleThreadFutureScheduler.java"

# interfaces
.implements Lf/c/a/n1/a;


# instance fields
.field public a:Ljava/util/concurrent/ScheduledThreadPoolExecutor;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lf/c/a/n1/g;

    invoke-direct {v1, p1}, Lf/c/a/n1/g;-><init>(Ljava/lang/String;)V

    new-instance v2, Lf/c/a/n1/e$a;

    invoke-direct {v2, p0, p1}, Lf/c/a/n1/e$a;-><init>(Lf/c/a/n1/e;Ljava/lang/String;)V

    const/4 p1, 0x1

    invoke-direct {v0, p1, v1, v2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    iput-object v0, p0, Lf/c/a/n1/e;->a:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    if-nez p2, :cond_0

    const-wide/16 v1, 0xa

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, p2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->setKeepAliveTime(JLjava/util/concurrent/TimeUnit;)V

    iget-object p2, p0, Lf/c/a/n1/e;->a:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    :cond_0
    return-void
.end method
