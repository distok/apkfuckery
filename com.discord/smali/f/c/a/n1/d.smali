.class public Lf/c/a/n1/d;
.super Ljava/lang/Object;
.source "SingleThreadCachedScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/lang/Runnable;

.field public final synthetic e:Lf/c/a/n1/c;


# direct methods
.method public constructor <init>(Lf/c/a/n1/c;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    iput-object p2, p0, Lf/c/a/n1/d;->d:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    iget-object v1, p0, Lf/c/a/n1/d;->d:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lf/c/a/n1/c;->a(Lf/c/a/n1/c;Ljava/lang/Runnable;)V

    :goto_0
    iget-object v0, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    iget-object v0, v0, Lf/c/a/n1/c;->a:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    iget-object v1, v1, Lf/c/a/n1/c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    iput-boolean v2, v1, Lf/c/a/n1/c;->b:Z

    monitor-exit v0

    return-void

    :cond_0
    iget-object v1, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    iget-object v1, v1, Lf/c/a/n1/c;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    iget-object v3, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    iget-object v3, v3, Lf/c/a/n1/c;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lf/c/a/n1/d;->e:Lf/c/a/n1/c;

    invoke-static {v0, v1}, Lf/c/a/n1/c;->a(Lf/c/a/n1/c;Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
