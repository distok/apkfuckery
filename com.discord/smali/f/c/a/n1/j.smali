.class public Lf/c/a/n1/j;
.super Ljava/lang/Object;
.source "TimerCycle.java"


# instance fields
.field public a:Lf/c/a/n1/a;

.field public b:Ljava/util/concurrent/ScheduledFuture;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Runnable;

.field public e:J

.field public f:J

.field public g:Z

.field public h:Lf/c/a/j0;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;JJLjava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/c/a/n1/e;

    const/4 v1, 0x1

    invoke-direct {v0, p6, v1}, Lf/c/a/n1/e;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lf/c/a/n1/j;->a:Lf/c/a/n1/a;

    iput-object p6, p0, Lf/c/a/n1/j;->c:Ljava/lang/String;

    iput-object p1, p0, Lf/c/a/n1/j;->d:Ljava/lang/Runnable;

    iput-wide p2, p0, Lf/c/a/n1/j;->e:J

    iput-wide p4, p0, Lf/c/a/n1/j;->f:J

    iput-boolean v1, p0, Lf/c/a/n1/j;->g:Z

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/n1/j;->h:Lf/c/a/j0;

    sget-object p1, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    long-to-double p4, p4

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr p4, v2

    invoke-virtual {p1, p4, p5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p4

    long-to-double p2, p2

    div-double/2addr p2, v2

    invoke-virtual {p1, p2, p3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lf/c/a/n1/j;->h:Lf/c/a/j0;

    const/4 p3, 0x3

    new-array p3, p3, [Ljava/lang/Object;

    const/4 p5, 0x0

    aput-object p6, p3, p5

    aput-object p1, p3, v1

    const/4 p1, 0x2

    aput-object p4, p3, p1

    const-string p1, "%s configured to fire after %s seconds of starting and cycles every %s seconds"

    invoke-interface {p2, p1, p3}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
