.class public Lf/c/a/a$b;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->l(Lf/c/a/d1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/d1;

.field public final synthetic e:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;Lf/c/a/d1;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$b;->e:Lf/c/a/a;

    iput-object p2, p0, Lf/c/a/a$b;->d:Lf/c/a/d1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lf/c/a/a$b;->e:Lf/c/a/a;

    iget-object v1, p0, Lf/c/a/a$b;->d:Lf/c/a/d1;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lf/c/a/y0;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lf/c/a/a;->J(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Handler;

    iget-object v3, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v1, v1, Lf/c/a/y0;->h:Lcom/adjust/sdk/AdjustAttribution;

    invoke-virtual {v0, v1}, Lf/c/a/a;->K(Lcom/adjust/sdk/AdjustAttribution;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lf/c/a/a;->v(Landroid/os/Handler;)V

    :cond_0
    return-void
.end method
