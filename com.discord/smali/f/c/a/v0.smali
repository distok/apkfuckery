.class public Lf/c/a/v0;
.super Ljava/lang/Object;
.source "PackageHandler.java"

# interfaces
.implements Lf/c/a/k0;


# instance fields
.field public a:Lf/c/a/n1/h;

.field public b:Lf/c/a/l0;

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/c/a/h0;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/c/a/l;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public f:Z

.field public g:Landroid/content/Context;

.field public h:Lf/c/a/j0;

.field public i:Lf/c/a/b0;

.field public j:Lf/c/a/b0;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lf/c/a/h0;Landroid/content/Context;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/c/a/n1/c;

    const-string v1, "PackageHandler"

    invoke-direct {v0, v1}, Lf/c/a/n1/c;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/v0;->h:Lf/c/a/j0;

    sget-object v0, Lf/c/a/b0;->d:Lf/c/a/b0;

    iput-object v0, p0, Lf/c/a/v0;->i:Lf/c/a/b0;

    sget-object v0, Lf/c/a/b0;->e:Lf/c/a/b0;

    iput-object v0, p0, Lf/c/a/v0;->j:Lf/c/a/b0;

    invoke-virtual {p0, p1, p2, p3}, Lf/c/a/v0;->k(Lf/c/a/h0;Landroid/content/Context;Z)V

    iget-object p1, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    new-instance p2, Lf/c/a/v0$a;

    invoke-direct {p2, p0}, Lf/c/a/v0$a;-><init>(Lf/c/a/v0;)V

    check-cast p1, Lf/c/a/n1/c;

    invoke-virtual {p1, p2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/c/a/v0;->k:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/c/a/v0;->l:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/c/a/v0;->m:Ljava/lang/String;

    return-object v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/c/a/v0;->f:Z

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/v0$c;

    invoke-direct {v1, p0}, Lf/c/a/v0$c;-><init>(Lf/c/a/v0;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public f(Lf/c/a/y0;)V
    .locals 2

    iget-object v0, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/v0$d;

    invoke-direct {v1, p0}, Lf/c/a/v0$d;-><init>(Lf/c/a/v0;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lf/c/a/v0;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/c/a/h0;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lf/c/a/h0;->e(Lf/c/a/y0;)V

    :cond_0
    return-void
.end method

.method public flush()V
    .locals 2

    iget-object v0, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/v0$g;

    invoke-direct {v1, p0}, Lf/c/a/v0$g;-><init>(Lf/c/a/v0;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public g(Lf/c/a/l;)V
    .locals 2

    iget-object v0, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/v0$b;

    invoke-direct {v1, p0, p1}, Lf/c/a/v0$b;-><init>(Lf/c/a/v0;Lf/c/a/l;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public h(Lf/c/a/e1;)V
    .locals 3

    if-eqz p1, :cond_1

    new-instance v0, Lf/c/a/e1;

    invoke-direct {v0}, Lf/c/a/e1;-><init>()V

    iget-object v1, p1, Lf/c/a/e1;->a:Ljava/util/Map;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p1, Lf/c/a/e1;->a:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v1, v0, Lf/c/a/e1;->a:Ljava/util/Map;

    :cond_0
    iget-object v1, p1, Lf/c/a/e1;->b:Ljava/util/Map;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/util/HashMap;

    iget-object p1, p1, Lf/c/a/e1;->b:Ljava/util/Map;

    invoke-direct {v1, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v1, v0, Lf/c/a/e1;->b:Ljava/util/Map;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    iget-object p1, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/v0$f;

    invoke-direct {v1, p0, v0}, Lf/c/a/v0$f;-><init>(Lf/c/a/v0;Lf/c/a/e1;)V

    check-cast p1, Lf/c/a/n1/c;

    invoke-virtual {p1, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public i(Lf/c/a/y0;Lf/c/a/l;)V
    .locals 9

    const/4 v0, 0x1

    iput-boolean v0, p1, Lf/c/a/y0;->b:Z

    iget-object v1, p0, Lf/c/a/v0;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/c/a/h0;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Lf/c/a/h0;->e(Lf/c/a/y0;)V

    :cond_0
    new-instance p1, Lf/c/a/v0$e;

    invoke-direct {p1, p0}, Lf/c/a/v0$e;-><init>(Lf/c/a/v0;)V

    invoke-virtual {p2}, Lf/c/a/l;->m()I

    move-result v1

    new-instance v2, Lf/c/a/g1;

    iget-object v3, p0, Lf/c/a/v0;->g:Landroid/content/Context;

    invoke-direct {v2, v3}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Lf/c/a/l;->a()Lf/c/a/k;

    move-result-object p2

    sget-object v3, Lf/c/a/k;->e:Lf/c/a/k;

    const/4 v4, 0x0

    if-ne p2, v3, :cond_1

    monitor-enter v2

    :try_start_0
    const-string p2, "install_tracked"

    invoke-virtual {v2, p2, v4}, Lf/c/a/g1;->a(Ljava/lang/String;Z)Z

    move-result p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    if-nez p2, :cond_1

    iget-object p2, p0, Lf/c/a/v0;->j:Lf/c/a/b0;

    invoke-static {v1, p2}, Lf/c/a/l1;->m(ILf/c/a/b0;)J

    move-result-wide v2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1

    :cond_1
    iget-object p2, p0, Lf/c/a/v0;->i:Lf/c/a/b0;

    invoke-static {v1, p2}, Lf/c/a/l1;->m(ILf/c/a/b0;)J

    move-result-wide v2

    :goto_0
    long-to-double v5, v2

    const-wide v7, 0x408f400000000000L    # 1000.0

    div-double/2addr v5, v7

    sget-object p2, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    invoke-virtual {p2, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p2

    iget-object v5, p0, Lf/c/a/v0;->h:Lf/c/a/j0;

    const-string v6, "Waiting for %s seconds before retrying the %d time"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p2, v7, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v7, v0

    invoke-interface {v5, v6, v7}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p2, p0, Lf/c/a/v0;->a:Lf/c/a/n1/h;

    check-cast p2, Lf/c/a/n1/c;

    invoke-virtual {p2, p1, v2, v3}, Lf/c/a/n1/c;->b(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public j()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/c/a/v0;->f:Z

    return-void
.end method

.method public k(Lf/c/a/h0;Landroid/content/Context;Z)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lf/c/a/v0;->c:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lf/c/a/v0;->g:Landroid/content/Context;

    xor-int/lit8 p2, p3, 0x1

    iput-boolean p2, p0, Lf/c/a/v0;->f:Z

    invoke-interface {p1}, Lf/c/a/h0;->a()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/c/a/v0;->k:Ljava/lang/String;

    invoke-interface {p1}, Lf/c/a/h0;->b()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/c/a/v0;->l:Ljava/lang/String;

    invoke-interface {p1}, Lf/c/a/h0;->c()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/v0;->m:Ljava/lang/String;

    return-void
.end method

.method public final l()V
    .locals 5

    iget-object v0, p0, Lf/c/a/v0;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lf/c/a/v0;->f:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/c/a/v0;->h:Lf/c/a/j0;

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Package handler is paused"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/c/a/v0;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/c/a/v0;->h:Lf/c/a/j0;

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Package handler is already sending"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_2
    iget-object v0, p0, Lf/c/a/v0;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/c/a/l;

    iget-object v1, p0, Lf/c/a/v0;->b:Lf/c/a/l0;

    iget-object v3, p0, Lf/c/a/v0;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, v2

    check-cast v1, Lf/c/a/x0;

    iget-object v2, v1, Lf/c/a/x0;->a:Lf/c/a/n1/h;

    new-instance v4, Lf/c/a/w0;

    invoke-direct {v4, v1, v0, v3}, Lf/c/a/w0;-><init>(Lf/c/a/x0;Lf/c/a/l;I)V

    check-cast v2, Lf/c/a/n1/c;

    invoke-virtual {v2, v4}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final m()V
    .locals 4

    iget-object v0, p0, Lf/c/a/v0;->d:Ljava/util/List;

    iget-object v1, p0, Lf/c/a/v0;->g:Landroid/content/Context;

    const-string v2, "AdjustIoPackageQueue"

    const-string v3, "Package queue"

    invoke-static {v0, v1, v2, v3}, Lf/c/a/l1;->C(Ljava/lang/Object;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/v0;->h:Lf/c/a/j0;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lf/c/a/v0;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Package handler wrote %d packages"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
