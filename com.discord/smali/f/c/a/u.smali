.class public Lf/c/a/u;
.super Ljava/lang/Object;
.source "AttributionHandler.java"

# interfaces
.implements Lf/c/a/i0;


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lf/c/a/j0;

.field public f:Lf/c/a/n1/k;

.field public g:Lf/c/a/n1/h;

.field public h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/c/a/h0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/c/a/h0;Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/u;->e:Lf/c/a/j0;

    new-instance v0, Lf/c/a/n1/c;

    const-string v1, "AttributionHandler"

    invoke-direct {v0, v1}, Lf/c/a/n1/c;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lf/c/a/u;->g:Lf/c/a/n1/h;

    new-instance v0, Lf/c/a/n1/k;

    new-instance v1, Lf/c/a/u$a;

    invoke-direct {v1, p0}, Lf/c/a/u$a;-><init>(Lf/c/a/u;)V

    const-string v2, "Attribution timer"

    invoke-direct {v0, v1, v2}, Lf/c/a/n1/k;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lf/c/a/u;->f:Lf/c/a/n1/k;

    invoke-interface {p1}, Lf/c/a/h0;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/u;->b:Ljava/lang/String;

    invoke-interface {p1}, Lf/c/a/h0;->f()Lf/c/a/c0;

    move-result-object v0

    iget-object v0, v0, Lf/c/a/c0;->j:Ljava/lang/String;

    iput-object v0, p0, Lf/c/a/u;->c:Ljava/lang/String;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lf/c/a/u;->h:Ljava/lang/ref/WeakReference;

    xor-int/lit8 p1, p2, 0x1

    iput-boolean p1, p0, Lf/c/a/u;->a:Z

    return-void
.end method


# virtual methods
.method public final a(Lf/c/a/h0;Lf/c/a/y0;)V
    .locals 11

    iget-object v0, p2, Lf/c/a/y0;->f:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-wide/16 v1, -0x1

    const-string v3, "ask_in"

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    const/4 p2, 0x1

    invoke-interface {p1, p2}, Lf/c/a/h0;->m(Z)V

    const-string p1, "backend"

    iput-object p1, p0, Lf/c/a/u;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lf/c/a/u;->b(J)V

    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lf/c/a/h0;->m(Z)V

    iget-object p1, p2, Lf/c/a/y0;->f:Lorg/json/JSONObject;

    const-string v0, "attribution"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    iget-object v0, p2, Lf/c/a/y0;->c:Ljava/lang/String;

    iget-object v1, p0, Lf/c/a/u;->c:Ljava/lang/String;

    invoke-static {v1}, Lf/c/a/l1;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/adjust/sdk/AdjustAttribution;->d:I

    const/4 v2, 0x0

    if-nez p1, :cond_2

    goto/16 :goto_2

    :cond_2
    new-instance v3, Lcom/adjust/sdk/AdjustAttribution;

    invoke-direct {v3}, Lcom/adjust/sdk/AdjustAttribution;-><init>()V

    const-string/jumbo v4, "unity"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v4, "click_label"

    const-string v5, "creative"

    const-string v6, "adgroup"

    const-string v7, "campaign"

    const-string v8, "network"

    const-string/jumbo v9, "tracker_name"

    const-string/jumbo v10, "tracker_token"

    if-eqz v1, :cond_4

    const-string v1, ""

    invoke-virtual {p1, v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/adjust/sdk/AdjustAttribution;->trackerToken:Ljava/lang/String;

    invoke-virtual {p1, v9, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/adjust/sdk/AdjustAttribution;->trackerName:Ljava/lang/String;

    invoke-virtual {p1, v8, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/adjust/sdk/AdjustAttribution;->network:Ljava/lang/String;

    invoke-virtual {p1, v7, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/adjust/sdk/AdjustAttribution;->campaign:Ljava/lang/String;

    invoke-virtual {p1, v6, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/adjust/sdk/AdjustAttribution;->adgroup:Ljava/lang/String;

    invoke-virtual {p1, v5, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/adjust/sdk/AdjustAttribution;->creative:Ljava/lang/String;

    invoke-virtual {p1, v4, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/adjust/sdk/AdjustAttribution;->clickLabel:Ljava/lang/String;

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    move-object v0, v1

    :goto_0
    iput-object v0, v3, Lcom/adjust/sdk/AdjustAttribution;->adid:Ljava/lang/String;

    goto :goto_1

    :cond_4
    invoke-virtual {p1, v10, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/adjust/sdk/AdjustAttribution;->trackerToken:Ljava/lang/String;

    invoke-virtual {p1, v9, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/adjust/sdk/AdjustAttribution;->trackerName:Ljava/lang/String;

    invoke-virtual {p1, v8, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/adjust/sdk/AdjustAttribution;->network:Ljava/lang/String;

    invoke-virtual {p1, v7, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/adjust/sdk/AdjustAttribution;->campaign:Ljava/lang/String;

    invoke-virtual {p1, v6, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/adjust/sdk/AdjustAttribution;->adgroup:Ljava/lang/String;

    invoke-virtual {p1, v5, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/adjust/sdk/AdjustAttribution;->creative:Ljava/lang/String;

    invoke-virtual {p1, v4, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v3, Lcom/adjust/sdk/AdjustAttribution;->clickLabel:Ljava/lang/String;

    iput-object v0, v3, Lcom/adjust/sdk/AdjustAttribution;->adid:Ljava/lang/String;

    :goto_1
    move-object v2, v3

    :goto_2
    iput-object v2, p2, Lf/c/a/y0;->h:Lcom/adjust/sdk/AdjustAttribution;

    return-void
.end method

.method public final b(J)V
    .locals 4

    iget-object v0, p0, Lf/c/a/u;->f:Lf/c/a/n1/k;

    invoke-virtual {v0}, Lf/c/a/n1/k;->b()J

    move-result-wide v0

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    return-void

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-eqz v2, :cond_1

    long-to-double v0, p1

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    sget-object v2, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lf/c/a/u;->e:Lf/c/a/j0;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v0, "Waiting to query attribution in %s seconds"

    invoke-interface {v1, v0, v2}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lf/c/a/u;->f:Lf/c/a/n1/k;

    invoke-virtual {v0, p1, p2}, Lf/c/a/n1/k;->c(J)V

    return-void
.end method
