.class public Lf/c/a/v0$f;
.super Ljava/lang/Object;
.source "PackageHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/v0;->h(Lf/c/a/e1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/e1;

.field public final synthetic e:Lf/c/a/v0;


# direct methods
.method public constructor <init>(Lf/c/a/v0;Lf/c/a/e1;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/v0$f;->e:Lf/c/a/v0;

    iput-object p2, p0, Lf/c/a/v0$f;->d:Lf/c/a/e1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lf/c/a/v0$f;->e:Lf/c/a/v0;

    iget-object v1, p0, Lf/c/a/v0$f;->d:Lf/c/a/e1;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v2, v0, Lf/c/a/v0;->h:Lf/c/a/j0;

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "Updating package handler queue"

    invoke-interface {v2, v5, v4}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, v0, Lf/c/a/v0;->h:Lf/c/a/j0;

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    iget-object v6, v1, Lf/c/a/e1;->a:Ljava/util/Map;

    aput-object v6, v5, v3

    const-string v6, "Session callback parameters: %s"

    invoke-interface {v2, v6, v5}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, v0, Lf/c/a/v0;->h:Lf/c/a/j0;

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lf/c/a/e1;->b:Ljava/util/Map;

    aput-object v5, v4, v3

    const-string v3, "Session partner parameters: %s"

    invoke-interface {v2, v3, v4}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, v0, Lf/c/a/v0;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/c/a/l;

    invoke-virtual {v3}, Lf/c/a/l;->i()Ljava/util/Map;

    move-result-object v4

    iget-object v5, v1, Lf/c/a/e1;->a:Ljava/util/Map;

    invoke-virtual {v3}, Lf/c/a/l;->b()Ljava/util/Map;

    move-result-object v6

    const-string v7, "Callback"

    invoke-static {v5, v6, v7}, Lf/c/a/l1;->t(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    const-string v6, "callback_params"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v5, v1, Lf/c/a/e1;->b:Ljava/util/Map;

    invoke-virtual {v3}, Lf/c/a/l;->j()Ljava/util/Map;

    move-result-object v3

    const-string v6, "Partner"

    invoke-static {v5, v3, v6}, Lf/c/a/l1;->t(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    const-string v5, "partner_params"

    invoke-static {v4, v5, v3}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lf/c/a/v0;->m()V

    :goto_1
    return-void
.end method
