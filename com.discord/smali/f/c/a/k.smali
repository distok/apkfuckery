.class public final enum Lf/c/a/k;
.super Ljava/lang/Enum;
.source "ActivityKind.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/c/a/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/c/a/k;

.field public static final enum e:Lf/c/a/k;

.field public static final enum f:Lf/c/a/k;

.field public static final enum g:Lf/c/a/k;

.field public static final enum h:Lf/c/a/k;

.field public static final enum i:Lf/c/a/k;

.field public static final enum j:Lf/c/a/k;

.field public static final enum k:Lf/c/a/k;

.field public static final enum l:Lf/c/a/k;

.field public static final enum m:Lf/c/a/k;

.field public static final enum n:Lf/c/a/k;

.field public static final enum o:Lf/c/a/k;

.field public static final synthetic p:[Lf/c/a/k;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    new-instance v0, Lf/c/a/k;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/c/a/k;->d:Lf/c/a/k;

    new-instance v1, Lf/c/a/k;

    const-string v3, "SESSION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/c/a/k;->e:Lf/c/a/k;

    new-instance v3, Lf/c/a/k;

    const-string v5, "EVENT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/c/a/k;->f:Lf/c/a/k;

    new-instance v5, Lf/c/a/k;

    const-string v7, "CLICK"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/c/a/k;->g:Lf/c/a/k;

    new-instance v7, Lf/c/a/k;

    const-string v9, "ATTRIBUTION"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lf/c/a/k;->h:Lf/c/a/k;

    new-instance v9, Lf/c/a/k;

    const-string v11, "REVENUE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lf/c/a/k;->i:Lf/c/a/k;

    new-instance v11, Lf/c/a/k;

    const-string v13, "REATTRIBUTION"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lf/c/a/k;->j:Lf/c/a/k;

    new-instance v13, Lf/c/a/k;

    const-string v15, "INFO"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lf/c/a/k;->k:Lf/c/a/k;

    new-instance v15, Lf/c/a/k;

    const-string v14, "GDPR"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lf/c/a/k;->l:Lf/c/a/k;

    new-instance v14, Lf/c/a/k;

    const-string v12, "AD_REVENUE"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lf/c/a/k;->m:Lf/c/a/k;

    new-instance v12, Lf/c/a/k;

    const-string v10, "DISABLE_THIRD_PARTY_SHARING"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lf/c/a/k;->n:Lf/c/a/k;

    new-instance v10, Lf/c/a/k;

    const-string v8, "SUBSCRIPTION"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lf/c/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lf/c/a/k;->o:Lf/c/a/k;

    const/16 v8, 0xc

    new-array v8, v8, [Lf/c/a/k;

    aput-object v0, v8, v2

    aput-object v1, v8, v4

    const/4 v0, 0x2

    aput-object v3, v8, v0

    const/4 v0, 0x3

    aput-object v5, v8, v0

    const/4 v0, 0x4

    aput-object v7, v8, v0

    const/4 v0, 0x5

    aput-object v9, v8, v0

    const/4 v0, 0x6

    aput-object v11, v8, v0

    const/4 v0, 0x7

    aput-object v13, v8, v0

    const/16 v0, 0x8

    aput-object v15, v8, v0

    const/16 v0, 0x9

    aput-object v14, v8, v0

    const/16 v0, 0xa

    aput-object v12, v8, v0

    aput-object v10, v8, v6

    sput-object v8, Lf/c/a/k;->p:[Lf/c/a/k;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/c/a/k;
    .locals 1

    const-class v0, Lf/c/a/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/c/a/k;

    return-object p0
.end method

.method public static values()[Lf/c/a/k;
    .locals 1

    sget-object v0, Lf/c/a/k;->p:[Lf/c/a/k;

    invoke-virtual {v0}, [Lf/c/a/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/c/a/k;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v0, "unknown"

    return-object v0

    :pswitch_1
    const-string/jumbo v0, "subscription"

    return-object v0

    :pswitch_2
    const-string v0, "ad_revenue"

    return-object v0

    :pswitch_3
    const-string v0, "disable_third_party_sharing"

    return-object v0

    :pswitch_4
    const-string v0, "gdpr"

    return-object v0

    :pswitch_5
    const-string v0, "info"

    return-object v0

    :pswitch_6
    const-string v0, "attribution"

    return-object v0

    :pswitch_7
    const-string v0, "click"

    return-object v0

    :pswitch_8
    const-string v0, "event"

    return-object v0

    :pswitch_9
    const-string v0, "session"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
