.class public Lf/c/a/a;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Lf/c/a/h0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/c/a/a$n;
    }
.end annotation


# static fields
.field public static o:J

.field public static p:J

.field public static q:J

.field public static r:J

.field public static s:J


# instance fields
.field public a:Lf/c/a/n1/h;

.field public b:Lf/c/a/k0;

.field public c:Lf/c/a/m;

.field public d:Lf/c/a/j0;

.field public e:Lf/c/a/n1/j;

.field public f:Lf/c/a/a$n;

.field public g:Lf/c/a/c0;

.field public h:Lf/c/a/n;

.field public i:Lcom/adjust/sdk/AdjustAttribution;

.field public j:Lf/c/a/i0;

.field public k:Lf/c/a/m0;

.field public l:Lf/c/a/e1;

.field public m:Lf/c/a/n0;

.field public n:Lf/c/a/o0;


# direct methods
.method public constructor <init>(Lf/c/a/n;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    invoke-interface {p1}, Lf/c/a/j0;->a()V

    new-instance p1, Lf/c/a/n1/c;

    const-string v0, "ActivityHandler"

    invoke-direct {p1, v0}, Lf/c/a/n1/c;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v0, Lf/c/a/a$n;

    invoke-direct {v0, p0}, Lf/c/a/a$n;-><init>(Lf/c/a/a;)V

    iput-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/c/a/a$n;->a:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Lf/c/a/a$n;->b:Z

    iput-boolean v1, v0, Lf/c/a/a$n;->c:Z

    iput-boolean v2, v0, Lf/c/a/a$n;->d:Z

    iput-boolean v2, v0, Lf/c/a/a$n;->f:Z

    iput-boolean v2, v0, Lf/c/a/a$n;->g:Z

    new-instance v0, Lf/c/a/a$e;

    invoke-direct {v0, p0}, Lf/c/a/a$e;-><init>(Lf/c/a/a;)V

    check-cast p1, Lf/c/a/n1/c;

    invoke-virtual {p1, v0}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static p(Lf/c/a/a;)V
    .locals 11

    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v0, v0, Lf/c/a/a$n;->g:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-wide/16 v2, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v0}, Lf/c/a/t;->b(Lf/c/a/j0;)V

    new-instance v0, Lf/c/a/m;

    invoke-direct {v0}, Lf/c/a/m;-><init>()V

    iput-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iput-boolean v1, v0, Lf/c/a/a$n;->g:Z

    invoke-virtual {p0}, Lf/c/a/a;->L()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    new-instance v0, Lf/c/a/g1;

    iget-object v6, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v0, v6}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lf/c/a/a;->c:Lf/c/a/m;

    invoke-virtual {v0}, Lf/c/a/g1;->d()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lf/c/a/m;->pushToken:Ljava/lang/String;

    iget-object v6, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v6, v6, Lf/c/a/a$n;->a:Z

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lf/c/a/g1;->c()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lf/c/a/a;->t()V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lf/c/a/g1;->b()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lf/c/a/a;->s()V

    :cond_1
    iget-object v6, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iput v1, v6, Lf/c/a/m;->sessionCount:I

    invoke-virtual {p0, v4, v5}, Lf/c/a/a;->H(J)V

    invoke-virtual {p0, v0}, Lf/c/a/a;->r(Lf/c/a/g1;)V

    :cond_2
    :goto_0
    iget-object v6, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iput v1, v6, Lf/c/a/m;->subsessionCount:I

    iput-wide v2, v6, Lf/c/a/m;->sessionLength:J

    iput-wide v2, v6, Lf/c/a/m;->timeSpent:J

    iput-wide v4, v6, Lf/c/a/m;->lastActivity:J

    const-wide/16 v1, -0x1

    iput-wide v1, v6, Lf/c/a/m;->lastInterval:J

    iget-object v1, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v2, v1, Lf/c/a/a$n;->a:Z

    iput-boolean v2, v6, Lf/c/a/m;->enabled:Z

    iget-boolean v1, v1, Lf/c/a/a$n;->d:Z

    iput-boolean v1, v6, Lf/c/a/m;->updatePackages:Z

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    monitor-enter v0

    :try_start_0
    const-string v1, "push_token"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    monitor-exit v0

    monitor-enter v0

    :try_start_1
    const-string v1, "gdpr_forget_me"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->i(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v0

    monitor-enter v0

    :try_start_2
    const-string v1, "disable_third_party_sharing"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->i(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    invoke-virtual {p0}, Lf/c/a/a;->y()V

    goto/16 :goto_3

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0

    :catchall_2
    move-exception p0

    monitor-exit v0

    throw p0

    :cond_3
    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v0, v0, Lf/c/a/m;->enabled:Z

    if-nez v0, :cond_4

    goto/16 :goto_3

    :cond_4
    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v0}, Lf/c/a/t;->b(Lf/c/a/j0;)V

    invoke-virtual {p0}, Lf/c/a/a;->L()V

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v0, v0, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v0, :cond_5

    goto/16 :goto_1

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-wide v6, v0, Lf/c/a/m;->lastActivity:J

    sub-long v6, v4, v6

    const/4 v8, 0x0

    cmp-long v9, v6, v2

    if-gez v9, :cond_6

    iget-object v0, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "Time travel!"

    invoke-interface {v0, v3, v2}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iput-wide v4, v0, Lf/c/a/m;->lastActivity:J

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    goto :goto_1

    :cond_6
    sget-wide v2, Lf/c/a/a;->r:J

    cmp-long v9, v6, v2

    if-lez v9, :cond_7

    invoke-virtual {p0, v4, v5}, Lf/c/a/a;->G(J)V

    new-instance v0, Lf/c/a/g1;

    iget-object v2, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lf/c/a/a;->r(Lf/c/a/g1;)V

    goto :goto_1

    :cond_7
    sget-wide v2, Lf/c/a/a;->s:J

    cmp-long v9, v6, v2

    if-lez v9, :cond_8

    iget v2, v0, Lf/c/a/m;->subsessionCount:I

    add-int/2addr v2, v1

    iput v2, v0, Lf/c/a/m;->subsessionCount:I

    iget-wide v9, v0, Lf/c/a/m;->sessionLength:J

    add-long/2addr v9, v6

    iput-wide v9, v0, Lf/c/a/m;->sessionLength:J

    iput-wide v4, v0, Lf/c/a/m;->lastActivity:J

    iget-object v0, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    iget-object v2, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget v2, v2, Lf/c/a/m;->sessionCount:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const-string v2, "Started subsession %d of session %d"

    invoke-interface {v0, v2, v3}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    iget-object v0, p0, Lf/c/a/a;->m:Lf/c/a/n0;

    invoke-virtual {v0}, Lf/c/a/n0;->c()V

    iget-object v0, p0, Lf/c/a/a;->n:Lf/c/a/o0;

    invoke-virtual {v0}, Lf/c/a/o0;->a()V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "Time span since last activity too short for a new subsession"

    invoke-interface {v0, v3, v2}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    invoke-virtual {p0}, Lf/c/a/a;->q()Z

    move-result v0

    if-nez v0, :cond_9

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v2, v0, Lf/c/a/a$n;->e:Z

    if-eqz v2, :cond_a

    iget-boolean v0, v0, Lf/c/a/a$n;->f:Z

    xor-int/2addr v0, v1

    if-eqz v0, :cond_a

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lf/c/a/a;->i:Lcom/adjust/sdk/AdjustAttribution;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v0, v0, Lf/c/a/m;->askingAttribution:Z

    if-nez v0, :cond_b

    goto :goto_2

    :cond_b
    iget-object v0, p0, Lf/c/a/a;->j:Lf/c/a/i0;

    check-cast v0, Lf/c/a/u;

    iget-object v1, v0, Lf/c/a/u;->g:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/v;

    invoke-direct {v2, v0}, Lf/c/a/v;-><init>(Lf/c/a/u;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    :goto_2
    invoke-virtual {p0}, Lf/c/a/a;->y()V

    :goto_3
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 3

    invoke-virtual {p0}, Lf/c/a/a;->u()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v0, v0, Lf/c/a/a$n;->g:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lf/c/a/a;->k:Lf/c/a/m0;

    check-cast v0, Lf/c/a/z0;

    iget-object v1, v0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/a1;

    invoke-direct {v2, v0}, Lf/c/a/a1;-><init>(Lf/c/a/z0;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final B(Z)V
    .locals 5

    invoke-virtual {p0}, Lf/c/a/a;->u()Z

    move-result v0

    const-string v1, "Adjust already enabled"

    const-string v2, "Adjust already disabled"

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_2

    return-void

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    if-eqz v0, :cond_3

    iget-boolean v0, v0, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v0, :cond_3

    iget-object p1, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    const-string v0, "Re-enabling SDK not possible for forgotten user"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-interface {p1, v0, v1}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_3
    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iput-boolean p1, v0, Lf/c/a/a$n;->a:Z

    iget-boolean v0, v0, Lf/c/a/a$n;->g:Z

    xor-int/2addr v0, v3

    if-eqz v0, :cond_4

    xor-int/2addr p1, v3

    const-string v0, "Handlers will start as paused due to the SDK being disabled"

    const-string v1, "Handlers will still start as paused"

    const-string v2, "Handlers will start as active due to the SDK being enabled"

    invoke-virtual {p0, p1, v0, v1, v2}, Lf/c/a/a;->N(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_4
    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iput-boolean p1, v0, Lf/c/a/m;->enabled:Z

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    if-eqz p1, :cond_8

    new-instance v0, Lf/c/a/g1;

    iget-object v1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lf/c/a/g1;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lf/c/a/a;->t()V

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Lf/c/a/g1;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lf/c/a/a;->s()V

    :cond_6
    :goto_2
    monitor-enter v0

    :try_start_0
    const-string v1, "install_tracked"

    invoke-virtual {v0, v1, v4}, Lf/c/a/g1;->a(Ljava/lang/String;Z)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    if-nez v1, :cond_7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lf/c/a/a;->G(J)V

    :cond_7
    invoke-virtual {p0, v0}, Lf/c/a/a;->r(Lf/c/a/g1;)V

    goto :goto_3

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_8
    :goto_3
    xor-int/2addr p1, v3

    const-string v0, "Pausing handlers due to SDK being disabled"

    const-string v1, "Handlers remain paused"

    const-string v2, "Resuming handlers due to SDK being enabled"

    invoke-virtual {p0, p1, v0, v1, v2}, Lf/c/a/a;->N(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public C(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$f;

    invoke-direct {v1, p0, p2, p1}, Lf/c/a/a$f;-><init>(Lf/c/a/a;ZLjava/lang/String;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final D()V
    .locals 8

    iget-object v0, p0, Lf/c/a/a;->e:Lf/c/a/n1/j;

    iget-boolean v1, v0, Lf/c/a/n1/j;->g:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/c/a/n1/j;->h:Lf/c/a/j0;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v0, v0, Lf/c/a/n1/j;->c:Ljava/lang/String;

    aput-object v0, v2, v3

    const-string v0, "%s is already suspended"

    invoke-interface {v1, v0, v2}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lf/c/a/n1/j;->b:Ljava/util/concurrent/ScheduledFuture;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v4}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    iput-wide v4, v0, Lf/c/a/n1/j;->e:J

    iget-object v1, v0, Lf/c/a/n1/j;->b:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    sget-object v1, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    iget-wide v4, v0, Lf/c/a/n1/j;->e:J

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    iget-object v4, v0, Lf/c/a/n1/j;->h:Lf/c/a/j0;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, Lf/c/a/n1/j;->c:Ljava/lang/String;

    aput-object v6, v5, v3

    aput-object v1, v5, v2

    const-string v1, "%s suspended with %s seconds left"

    invoke-interface {v4, v1, v5}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v2, v0, Lf/c/a/n1/j;->g:Z

    :goto_0
    return-void
.end method

.method public final E()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lf/c/a/a;->F(Z)Z

    move-result v0

    return v0
.end method

.method public final F(Z)Z
    .locals 0

    invoke-virtual {p0, p1}, Lf/c/a/a;->x(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object p1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean p1, p1, Lf/c/a/a$n;->c:Z

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public final G(J)V
    .locals 5

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-wide v1, v0, Lf/c/a/m;->lastActivity:J

    sub-long v1, p1, v1

    iget v3, v0, Lf/c/a/m;->sessionCount:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, v0, Lf/c/a/m;->sessionCount:I

    iput-wide v1, v0, Lf/c/a/m;->lastInterval:J

    invoke-virtual {p0, p1, p2}, Lf/c/a/a;->H(J)V

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iput v4, v0, Lf/c/a/m;->subsessionCount:I

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lf/c/a/m;->sessionLength:J

    iput-wide v1, v0, Lf/c/a/m;->timeSpent:J

    iput-wide p1, v0, Lf/c/a/m;->lastActivity:J

    const-wide/16 p1, -0x1

    iput-wide p1, v0, Lf/c/a/m;->lastInterval:J

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    return-void
.end method

.method public final H(J)V
    .locals 9

    new-instance v7, Lf/c/a/u0;

    iget-object v8, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, p0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v3, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v4, p0, Lf/c/a/a;->l:Lf/c/a/e1;

    move-object v0, v7

    move-object v1, v8

    move-wide v5, p1

    invoke-direct/range {v0 .. v6}, Lf/c/a/u0;-><init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V

    iget-object p1, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, v8, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v1, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v0, v1}, Ls/a/b/b/a;->y(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_0
    iget-object v0, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v1, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v0, v1}, Ls/a/b/b/a;->z(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_1
    iget-object v0, v7, Lf/c/a/u0;->e:Lf/c/a/e1;

    iget-object v0, v0, Lf/c/a/e1;->a:Ljava/util/Map;

    const-string v1, "callback_params"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, v7, Lf/c/a/u0;->e:Lf/c/a/e1;

    iget-object v0, v0, Lf/c/a/e1;->b:Ljava/util/Map;

    const-string v1, "partner_params"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v0, v7, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v0, v0, Lf/c/a/u0$a;->g:Ljava/lang/String;

    const-string v1, "android_uuid"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const-string/jumbo v1, "tracking_enabled"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->a:Ljava/lang/String;

    const-string v1, "gps_adid"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->b:Ljava/lang/String;

    const-string v1, "gps_adid_src"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget v0, v0, Lf/c/a/c0;->c:I

    int-to-long v0, v0

    const-string v2, "gps_adid_attempt"

    invoke-static {p2, v2, v0, v1}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {v7, p2}, Lf/c/a/u0;->j(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lf/c/a/u0;->p:Lf/c/a/j0;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Google Advertising ID not detected, fallback to non Google Play identifiers will take place"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lf/c/a/c0;->a(Landroid/content/Context;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->f:Ljava/lang/String;

    const-string v1, "mac_sha1"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->g:Ljava/lang/String;

    const-string v1, "mac_md5"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->h:Ljava/lang/String;

    const-string v1, "android_id"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->r:Ljava/lang/String;

    const-string v1, "api_level"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v1, "app_secret"

    invoke-static {p2, v1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->b:Ljava/lang/String;

    const-string v2, "app_token"

    invoke-static {p2, v2, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->l:Ljava/lang/String;

    const-string v2, "app_version"

    invoke-static {p2, v2, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v2, "attribution_deeplink"

    invoke-static {p2, v2, v1}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v2}, Lf/c/a/l1;->d(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    const-string v4, "connectivity_type"

    invoke-static {p2, v4, v2, v3}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->t:Ljava/lang/String;

    const-string v3, "country"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->A:Ljava/lang/String;

    const-string v3, "cpu_type"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v2, v7, Lf/c/a/u0;->a:J

    const-string v4, "created_at"

    invoke-static {p2, v4, v2, v3}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v2, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->d:Ljava/lang/String;

    const-string v3, "default_tracker"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "device_known"

    invoke-static {p2, v2, v0}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->o:Ljava/lang/String;

    const-string v3, "device_manufacturer"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->n:Ljava/lang/String;

    const-string v3, "device_name"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->m:Ljava/lang/String;

    const-string v3, "device_type"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->y:Ljava/lang/String;

    const-string v3, "display_height"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->x:Ljava/lang/String;

    const-string v3, "display_width"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->c:Ljava/lang/String;

    const-string v3, "environment"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v3, "event_buffering_enabled"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "external_device_id"

    invoke-static {p2, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->i:Ljava/lang/String;

    const-string v3, "fb_id"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lf/c/a/l1;->e(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fire_adid"

    invoke-static {p2, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lf/c/a/l1;->f(Landroid/content/ContentResolver;)Ljava/lang/Boolean;

    move-result-object p1

    const-string v2, "fire_tracking_enabled"

    invoke-static {p2, v2, p1}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->z:Ljava/lang/String;

    const-string v2, "hardware_name"

    invoke-static {p2, v2, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->C:Ljava/lang/String;

    const-string v2, "installed_at"

    invoke-static {p2, v2, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->s:Ljava/lang/String;

    const-string v2, "language"

    invoke-static {p2, v2, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v2, p1, Lf/c/a/u0$a;->e:J

    const-string p1, "last_interval"

    invoke-static {p2, p1, v2, v3}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object p1, p1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {p1}, Lf/c/a/l1;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "mcc"

    invoke-static {p2, v2, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object p1, p1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {p1}, Lf/c/a/l1;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "mnc"

    invoke-static {p2, v2, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "needs_response_details"

    invoke-static {p2, p1, v1}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object p1, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object p1, p1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {p1}, Lf/c/a/l1;->j(Landroid/content/Context;)I

    move-result p1

    int-to-long v1, p1

    const-string p1, "network_type"

    invoke-static {p2, p1, v1, v2}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->B:Ljava/lang/String;

    const-string v1, "os_build"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->p:Ljava/lang/String;

    const-string v1, "os_name"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->q:Ljava/lang/String;

    const-string v1, "os_version"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->k:Ljava/lang/String;

    const-string v1, "package_name"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object p1, p1, Lf/c/a/u0$a;->h:Ljava/lang/String;

    const-string v1, "push_token"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->w:Ljava/lang/String;

    const-string v1, "screen_density"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->v:Ljava/lang/String;

    const-string v1, "screen_format"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->u:Ljava/lang/String;

    const-string v1, "screen_size"

    invoke-static {p2, v1, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "secret_id"

    invoke-static {p2, p1, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v7, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget p1, p1, Lf/c/a/u0$a;->b:I

    int-to-long v0, p1

    const-string p1, "session_count"

    invoke-static {p2, p1, v0, v1}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, v7, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v0, p1, Lf/c/a/u0$a;->f:J

    const-string p1, "session_length"

    invoke-static {p2, p1, v0, v1}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, v7, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget p1, p1, Lf/c/a/u0$a;->c:I

    int-to-long v0, p1

    const-string/jumbo p1, "subsession_count"

    invoke-static {p2, p1, v0, v1}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, v7, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v0, p1, Lf/c/a/u0$a;->d:J

    const-string/jumbo p1, "time_spent"

    invoke-static {p2, p1, v0, v1}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, v7, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->D:Ljava/lang/String;

    const-string/jumbo v0, "updated_at"

    invoke-static {p2, v0, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Lf/c/a/u0;->i(Ljava/util/Map;)V

    sget-object p1, Lf/c/a/k;->e:Lf/c/a/k;

    invoke-virtual {v7, p1}, Lf/c/a/u0;->k(Lf/c/a/k;)Lf/c/a/l;

    move-result-object v0

    const-string v1, "/session"

    invoke-virtual {v0, v1}, Lf/c/a/l;->s(Ljava/lang/String;)V

    const-string v1, ""

    invoke-virtual {v0, v1}, Lf/c/a/l;->t(Ljava/lang/String;)V

    invoke-virtual {p1}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lf/c/a/l;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v7, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v3, v2, Lf/c/a/n;->a:Landroid/content/Context;

    iget-object v2, v2, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {p2, p1, v1, v3, v2}, Lf/c/a/t;->c(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lf/c/a/j0;)V

    invoke-virtual {v0, p2}, Lf/c/a/l;->r(Ljava/util/Map;)V

    iget-object p1, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {p1, v0}, Lf/c/a/k0;->g(Lf/c/a/l;)V

    iget-object p1, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {p1}, Lf/c/a/k0;->e()V

    return-void
.end method

.method public final I(J)Z
    .locals 7

    invoke-virtual {p0}, Lf/c/a/a;->q()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-wide v2, v0, Lf/c/a/m;->lastActivity:J

    sub-long v2, p1, v2

    sget-wide v4, Lf/c/a/a;->r:J

    cmp-long v6, v2, v4

    if-lez v6, :cond_1

    return v1

    :cond_1
    iput-wide p1, v0, Lf/c/a/m;->lastActivity:J

    const-wide/16 p1, 0x0

    cmp-long v4, v2, p1

    if-gez v4, :cond_2

    iget-object p1, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array p2, v1, [Ljava/lang/Object;

    const-string v0, "Time travel!"

    invoke-interface {p1, v0, p2}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-wide p1, v0, Lf/c/a/m;->sessionLength:J

    add-long/2addr p1, v2

    iput-wide p1, v0, Lf/c/a/m;->sessionLength:J

    iget-wide p1, v0, Lf/c/a/m;->timeSpent:J

    add-long/2addr p1, v2

    iput-wide p1, v0, Lf/c/a/m;->timeSpent:J

    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final J(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v0, v0, Lf/c/a/m;->adid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iput-object p1, v0, Lf/c/a/m;->adid:Ljava/lang/String;

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    return-void
.end method

.method public K(Lcom/adjust/sdk/AdjustAttribution;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lf/c/a/a;->i:Lcom/adjust/sdk/AdjustAttribution;

    invoke-virtual {p1, v1}, Lcom/adjust/sdk/AdjustAttribution;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    :cond_1
    iput-object p1, p0, Lf/c/a/a;->i:Lcom/adjust/sdk/AdjustAttribution;

    const-class p1, Lcom/adjust/sdk/AdjustAttribution;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lf/c/a/a;->i:Lcom/adjust/sdk/AdjustAttribution;

    if-nez v0, :cond_2

    monitor-exit p1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    const-string v2, "AdjustAttribution"

    const-string v3, "Attribution"

    invoke-static {v0, v1, v2, v3}, Lf/c/a/l1;->C(Ljava/lang/Object;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit p1

    :goto_0
    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final L()V
    .locals 3

    invoke-virtual {p0}, Lf/c/a/a;->E()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lf/c/a/a;->w()V

    return-void

    :cond_0
    iget-object v0, p0, Lf/c/a/a;->j:Lf/c/a/i0;

    check-cast v0, Lf/c/a/u;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/c/a/u;->a:Z

    iget-object v0, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v0}, Lf/c/a/k0;->j()V

    iget-object v0, p0, Lf/c/a/a;->k:Lf/c/a/m0;

    check-cast v0, Lf/c/a/z0;

    iput-boolean v1, v0, Lf/c/a/z0;->a:Z

    iget-object v1, v0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/b1;

    invoke-direct {v2, v0}, Lf/c/a/b1;-><init>(Lf/c/a/z0;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v0}, Lf/c/a/k0;->e()V

    return-void
.end method

.method public final M()V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    iget-object v1, p0, Lf/c/a/a;->l:Lf/c/a/e1;

    invoke-interface {v0, v1}, Lf/c/a/k0;->h(Lf/c/a/e1;)V

    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/c/a/a$n;->d:Z

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    if-eqz v0, :cond_0

    iput-boolean v1, v0, Lf/c/a/m;->updatePackages:Z

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    :cond_0
    return-void
.end method

.method public final N(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    invoke-interface {p1, p2, p3}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lf/c/a/a;->x(Z)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lf/c/a/a;->x(Z)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array p2, v0, [Ljava/lang/Object;

    invoke-interface {p1, p3, p2}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    const-string p2, ", except the Sdk Click Handler"

    invoke-static {p3, p2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    new-array p3, v0, [Ljava/lang/Object;

    invoke-interface {p1, p2, p3}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array p2, v0, [Ljava/lang/Object;

    invoke-interface {p1, p4, p2}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0}, Lf/c/a/a;->L()V

    return-void
.end method

.method public final O()V
    .locals 5

    const-class v0, Lf/c/a/m;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/c/a/a;->c:Lf/c/a/m;

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    iget-object v2, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    const-string v3, "AdjustIoActivityState"

    const-string v4, "Activity state"

    invoke-static {v1, v2, v3, v4}, Lf/c/a/l1;->C(Ljava/lang/Object;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Lf/c/a/e1;
    .locals 1

    iget-object v0, p0, Lf/c/a/a;->l:Lf/c/a/e1;

    return-object v0
.end method

.method public e(Lf/c/a/y0;)V
    .locals 3

    instance-of v0, p1, Lf/c/a/f1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/c/a/a;->j:Lf/c/a/i0;

    check-cast p1, Lf/c/a/f1;

    check-cast v0, Lf/c/a/u;

    iget-object v1, v0, Lf/c/a/u;->g:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/w;

    invoke-direct {v2, v0, p1}, Lf/c/a/w;-><init>(Lf/c/a/u;Lf/c/a/f1;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    instance-of v0, p1, Lf/c/a/d1;

    if-eqz v0, :cond_4

    check-cast p1, Lf/c/a/d1;

    iget-boolean v0, p1, Lf/c/a/d1;->i:Z

    if-nez v0, :cond_1

    goto :goto_2

    :cond_1
    iget-object v0, p1, Lf/c/a/d1;->m:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v1, "huawei"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-wide v1, p1, Lf/c/a/d1;->j:J

    iput-wide v1, v0, Lf/c/a/m;->clickTime:J

    iget-wide v1, p1, Lf/c/a/d1;->k:J

    iput-wide v1, v0, Lf/c/a/m;->installBegin:J

    iget-object v1, p1, Lf/c/a/d1;->l:Ljava/lang/String;

    iput-object v1, v0, Lf/c/a/m;->installReferrer:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-wide v1, p1, Lf/c/a/d1;->j:J

    iput-wide v1, v0, Lf/c/a/m;->clickTimeHuawei:J

    iget-wide v1, p1, Lf/c/a/d1;->k:J

    iput-wide v1, v0, Lf/c/a/m;->installBeginHuawei:J

    iget-object v1, p1, Lf/c/a/d1;->l:Ljava/lang/String;

    iput-object v1, v0, Lf/c/a/m;->installReferrerHuawei:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0}, Lf/c/a/a;->O()V

    :goto_2
    iget-object v0, p0, Lf/c/a/a;->j:Lf/c/a/i0;

    check-cast v0, Lf/c/a/u;

    iget-object v1, v0, Lf/c/a/u;->g:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/x;

    invoke-direct {v2, v0, p1}, Lf/c/a/x;-><init>(Lf/c/a/u;Lf/c/a/d1;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void

    :cond_4
    instance-of v0, p1, Lf/c/a/d0;

    if-eqz v0, :cond_5

    check-cast p1, Lf/c/a/d0;

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/b;

    invoke-direct {v1, p0, p1}, Lf/c/a/b;-><init>(Lf/c/a/a;Lf/c/a/d0;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    :cond_5
    return-void
.end method

.method public f()Lf/c/a/c0;
    .locals 1

    iget-object v0, p0, Lf/c/a/a;->g:Lf/c/a/c0;

    return-object v0
.end method

.method public g()Lf/c/a/m;
    .locals 1

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    return-object v0
.end method

.method public h(Lf/c/a/f1;)V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$c;

    invoke-direct {v1, p0, p1}, Lf/c/a/a$c;-><init>(Lf/c/a/a;Lf/c/a/f1;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public i()Lf/c/a/n;
    .locals 1

    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    invoke-virtual {p0}, Lf/c/a/a;->u()Z

    move-result v0

    return v0
.end method

.method public j(Lf/c/a/a0;)V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$d;

    invoke-direct {v1, p0, p1}, Lf/c/a/a$d;-><init>(Lf/c/a/a;Lf/c/a/a0;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public k()V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$m;

    invoke-direct {v1, p0}, Lf/c/a/a$m;-><init>(Lf/c/a/a;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public l(Lf/c/a/d1;)V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$b;

    invoke-direct {v1, p0, p1}, Lf/c/a/a$b;-><init>(Lf/c/a/a;Lf/c/a/d1;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public m(Z)V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$l;

    invoke-direct {v1, p0, p1}, Lf/c/a/a$l;-><init>(Lf/c/a/a;Z)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public n()V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$g;

    invoke-direct {v1, p0}, Lf/c/a/a$g;-><init>(Lf/c/a/a;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public o(Lf/c/a/o;)V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$k;

    invoke-direct {v1, p0, p1}, Lf/c/a/a$k;-><init>(Lf/c/a/a;Lf/c/a/o;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/c/a/a$n;->c:Z

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$i;

    invoke-direct {v1, p0}, Lf/c/a/a$i;-><init>(Lf/c/a/a;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/c/a/a$n;->c:Z

    iget-object v0, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/a$h;

    invoke-direct {v1, p0}, Lf/c/a/a$h;-><init>(Lf/c/a/a;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final q()Z
    .locals 4

    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v0, v0, Lf/c/a/a$n;->g:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/c/a/a;->d:Lf/c/a/j0;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "Sdk did not yet start"

    invoke-interface {v0, v3, v2}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return v1
.end method

.method public final r(Lf/c/a/g1;)V
    .locals 4

    invoke-virtual {p1}, Lf/c/a/g1;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v1, v1, Lf/c/a/m;->pushToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v3, Lf/c/a/a$f;

    invoke-direct {v3, p0, v1, v0}, Lf/c/a/a$f;-><init>(Lf/c/a/a;ZLjava/lang/String;)V

    check-cast v2, Lf/c/a/n1/c;

    invoke-virtual {v2, v3}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    :cond_0
    invoke-virtual {p1}, Lf/c/a/g1;->f()Lorg/json/JSONArray;

    iget-object p1, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v0, Lf/c/a/a$m;

    invoke-direct {v0, p0}, Lf/c/a/a$m;-><init>(Lf/c/a/a;)V

    check-cast p1, Lf/c/a/n1/c;

    invoke-virtual {p1, v0}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    iget-object p1, p0, Lf/c/a/a;->m:Lf/c/a/n0;

    invoke-virtual {p1}, Lf/c/a/n0;->c()V

    iget-object p1, p0, Lf/c/a/a;->n:Lf/c/a/o0;

    invoke-virtual {p1}, Lf/c/a/o0;->a()V

    return-void
.end method

.method public final s()V
    .locals 11

    new-instance v0, Lf/c/a/g1;

    iget-object v1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    monitor-enter v0

    :try_start_0
    const-string v1, "disable_third_party_sharing"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lf/c/a/g1;->k(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v0

    invoke-virtual {p0}, Lf/c/a/a;->q()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lf/c/a/a;->u()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v3, v1, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v3, :cond_2

    return-void

    :cond_2
    iget-boolean v3, v1, Lf/c/a/m;->isThirdPartySharingDisabled:Z

    if-eqz v3, :cond_3

    return-void

    :cond_3
    iput-boolean v2, v1, Lf/c/a/m;->isThirdPartySharingDisabled:Z

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    new-instance v1, Lf/c/a/u0;

    iget-object v2, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v6, p0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v7, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v8, p0, Lf/c/a/a;->l:Lf/c/a/e1;

    move-object v4, v1

    move-object v5, v2

    invoke-direct/range {v4 .. v10}, Lf/c/a/u0;-><init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v4, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v4, v4, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v5, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v4, v5}, Ls/a/b/b/a;->y(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_4
    iget-object v4, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v4, v4, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v5, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v4, v5}, Ls/a/b/b/a;->z(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_5
    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v4, v1, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v4, v4, Lf/c/a/u0$a;->g:Ljava/lang/String;

    const-string v5, "android_uuid"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const-string/jumbo v5, "tracking_enabled"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->a:Ljava/lang/String;

    const-string v5, "gps_adid"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->b:Ljava/lang/String;

    const-string v5, "gps_adid_src"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget v4, v4, Lf/c/a/c0;->c:I

    int-to-long v4, v4

    const-string v6, "gps_adid_attempt"

    invoke-static {v3, v6, v4, v5}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {v1, v3}, Lf/c/a/u0;->j(Ljava/util/Map;)Z

    move-result v4

    if-nez v4, :cond_6

    sget-object v4, Lf/c/a/u0;->p:Lf/c/a/j0;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "Google Advertising ID not detected, fallback to non Google Play identifiers will take place"

    invoke-interface {v4, v6, v5}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lf/c/a/c0;->a(Landroid/content/Context;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->f:Ljava/lang/String;

    const-string v5, "mac_sha1"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->g:Ljava/lang/String;

    const-string v5, "mac_md5"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->h:Ljava/lang/String;

    const-string v5, "android_id"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v4, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->r:Ljava/lang/String;

    const-string v5, "api_level"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "app_secret"

    invoke-static {v3, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->b:Ljava/lang/String;

    const-string v6, "app_token"

    invoke-static {v3, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->l:Ljava/lang/String;

    const-string v6, "app_version"

    invoke-static {v3, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v6, "attribution_deeplink"

    invoke-static {v3, v6, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-wide v6, v1, Lf/c/a/u0;->a:J

    const-string v8, "created_at"

    invoke-static {v3, v8, v6, v7}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v6, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "device_known"

    invoke-static {v3, v6, v4}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v6, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v6, v6, Lf/c/a/c0;->n:Ljava/lang/String;

    const-string v7, "device_name"

    invoke-static {v3, v7, v6}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v6, v6, Lf/c/a/c0;->m:Ljava/lang/String;

    const-string v7, "device_type"

    invoke-static {v3, v7, v6}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->c:Ljava/lang/String;

    const-string v7, "environment"

    invoke-static {v3, v7, v6}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v6, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v7, "event_buffering_enabled"

    invoke-static {v3, v7, v6}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v6, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "external_device_id"

    invoke-static {v3, v6, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lf/c/a/l1;->e(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "fire_adid"

    invoke-static {v3, v7, v6}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lf/c/a/l1;->f(Landroid/content/ContentResolver;)Ljava/lang/Boolean;

    move-result-object v2

    const-string v6, "fire_tracking_enabled"

    invoke-static {v3, v6, v2}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "needs_response_details"

    invoke-static {v3, v2, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->p:Ljava/lang/String;

    const-string v5, "os_name"

    invoke-static {v3, v5, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->q:Ljava/lang/String;

    const-string v5, "os_version"

    invoke-static {v3, v5, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->k:Ljava/lang/String;

    const-string v5, "package_name"

    invoke-static {v3, v5, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v2, v2, Lf/c/a/u0$a;->h:Ljava/lang/String;

    const-string v5, "push_token"

    invoke-static {v3, v5, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "secret_id"

    invoke-static {v3, v2, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lf/c/a/u0;->i(Ljava/util/Map;)V

    sget-object v2, Lf/c/a/k;->n:Lf/c/a/k;

    invoke-virtual {v1, v2}, Lf/c/a/u0;->k(Lf/c/a/k;)Lf/c/a/l;

    move-result-object v4

    const-string v5, "/disable_third_party_sharing"

    invoke-virtual {v4, v5}, Lf/c/a/l;->s(Ljava/lang/String;)V

    const-string v5, ""

    invoke-virtual {v4, v5}, Lf/c/a/l;->t(Ljava/lang/String;)V

    invoke-virtual {v2}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lf/c/a/l;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v1, Lf/c/a/n;->a:Landroid/content/Context;

    iget-object v1, v1, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v3, v2, v5, v6, v1}, Lf/c/a/t;->c(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lf/c/a/j0;)V

    invoke-virtual {v4, v3}, Lf/c/a/l;->r(Ljava/util/Map;)V

    iget-object v1, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v1, v4}, Lf/c/a/k0;->g(Lf/c/a/l;)V

    monitor-enter v0

    :try_start_1
    const-string v1, "disable_third_party_sharing"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->i(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v0}, Lf/c/a/k0;->e()V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final t()V
    .locals 9

    invoke-virtual {p0}, Lf/c/a/a;->q()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lf/c/a/a;->u()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v1, v0, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v1, :cond_2

    return-void

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/c/a/m;->isGdprForgotten:Z

    invoke-virtual {p0}, Lf/c/a/a;->O()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    new-instance v0, Lf/c/a/u0;

    iget-object v1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v4, p0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v5, p0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v6, p0, Lf/c/a/a;->l:Lf/c/a/e1;

    move-object v2, v0

    move-object v3, v1

    invoke-direct/range {v2 .. v8}, Lf/c/a/u0;-><init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v3, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v4, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v3, v4}, Ls/a/b/b/a;->y(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_3
    iget-object v3, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v4, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v3, v4}, Ls/a/b/b/a;->z(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_4
    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v4, v4, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v3, v0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v3, v3, Lf/c/a/u0$a;->g:Ljava/lang/String;

    const-string v4, "android_uuid"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const-string/jumbo v4, "tracking_enabled"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->a:Ljava/lang/String;

    const-string v4, "gps_adid"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->b:Ljava/lang/String;

    const-string v4, "gps_adid_src"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget v3, v3, Lf/c/a/c0;->c:I

    int-to-long v3, v3

    const-string v5, "gps_adid_attempt"

    invoke-static {v2, v5, v3, v4}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {v0, v2}, Lf/c/a/u0;->j(Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_5

    sget-object v3, Lf/c/a/u0;->p:Lf/c/a/j0;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "Google Advertising ID not detected, fallback to non Google Play identifiers will take place"

    invoke-interface {v3, v5, v4}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v4, v4, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lf/c/a/c0;->a(Landroid/content/Context;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->f:Ljava/lang/String;

    const-string v4, "mac_sha1"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->g:Ljava/lang/String;

    const-string v4, "mac_md5"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->h:Ljava/lang/String;

    const-string v4, "android_id"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v3, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->r:Ljava/lang/String;

    const-string v4, "api_level"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "app_secret"

    invoke-static {v2, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v4, v4, Lf/c/a/n;->b:Ljava/lang/String;

    const-string v5, "app_token"

    invoke-static {v2, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v4, v4, Lf/c/a/c0;->l:Ljava/lang/String;

    const-string v5, "app_version"

    invoke-static {v2, v5, v4}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v5, "attribution_deeplink"

    invoke-static {v2, v5, v4}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-wide v5, v0, Lf/c/a/u0;->a:J

    const-string v7, "created_at"

    invoke-static {v2, v7, v5, v6}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v5, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "device_known"

    invoke-static {v2, v5, v3}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v5, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->n:Ljava/lang/String;

    const-string v6, "device_name"

    invoke-static {v2, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->m:Ljava/lang/String;

    const-string v6, "device_type"

    invoke-static {v2, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->c:Ljava/lang/String;

    const-string v6, "environment"

    invoke-static {v2, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v6, "event_buffering_enabled"

    invoke-static {v2, v6, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v5, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "external_device_id"

    invoke-static {v2, v5, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lf/c/a/l1;->e(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "fire_adid"

    invoke-static {v2, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lf/c/a/l1;->f(Landroid/content/ContentResolver;)Ljava/lang/Boolean;

    move-result-object v1

    const-string v5, "fire_tracking_enabled"

    invoke-static {v2, v5, v1}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "needs_response_details"

    invoke-static {v2, v1, v4}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v1, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->p:Ljava/lang/String;

    const-string v4, "os_name"

    invoke-static {v2, v4, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->q:Ljava/lang/String;

    const-string v4, "os_version"

    invoke-static {v2, v4, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->k:Ljava/lang/String;

    const-string v4, "package_name"

    invoke-static {v2, v4, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v1, v1, Lf/c/a/u0$a;->h:Ljava/lang/String;

    const-string v4, "push_token"

    invoke-static {v2, v4, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "secret_id"

    invoke-static {v2, v1, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lf/c/a/u0;->i(Ljava/util/Map;)V

    sget-object v1, Lf/c/a/k;->l:Lf/c/a/k;

    invoke-virtual {v0, v1}, Lf/c/a/u0;->k(Lf/c/a/k;)Lf/c/a/l;

    move-result-object v3

    const-string v4, "/gdpr_forget_device"

    invoke-virtual {v3, v4}, Lf/c/a/l;->s(Ljava/lang/String;)V

    const-string v4, ""

    invoke-virtual {v3, v4}, Lf/c/a/l;->t(Ljava/lang/String;)V

    invoke-virtual {v1}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lf/c/a/l;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v0, Lf/c/a/n;->a:Landroid/content/Context;

    iget-object v0, v0, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v2, v1, v4, v5, v0}, Lf/c/a/t;->c(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lf/c/a/j0;)V

    invoke-virtual {v3, v2}, Lf/c/a/l;->r(Ljava/util/Map;)V

    iget-object v0, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v0, v3}, Lf/c/a/k0;->g(Lf/c/a/l;)V

    new-instance v0, Lf/c/a/g1;

    iget-object v1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    monitor-enter v0

    :try_start_0
    const-string v1, "gdpr_forget_me"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v0}, Lf/c/a/k0;->e()V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final u()Z
    .locals 1

    iget-object v0, p0, Lf/c/a/a;->c:Lf/c/a/m;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lf/c/a/m;->enabled:Z

    return v0

    :cond_0
    iget-object v0, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v0, v0, Lf/c/a/a$n;->a:Z

    return v0
.end method

.method public final v(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->e:Lf/c/a/s0;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lf/c/a/a$j;

    invoke-direct {v0, p0}, Lf/c/a/a$j;-><init>(Lf/c/a/a;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final w()V
    .locals 3

    iget-object v0, p0, Lf/c/a/a;->j:Lf/c/a/i0;

    check-cast v0, Lf/c/a/u;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/c/a/u;->a:Z

    iget-object v0, p0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v0}, Lf/c/a/k0;->d()V

    invoke-virtual {p0, v1}, Lf/c/a/a;->F(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/c/a/a;->k:Lf/c/a/m0;

    check-cast v0, Lf/c/a/z0;

    iput-boolean v1, v0, Lf/c/a/z0;->a:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/c/a/a;->k:Lf/c/a/m0;

    check-cast v0, Lf/c/a/z0;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/c/a/z0;->a:Z

    iget-object v1, v0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/b1;

    invoke-direct {v2, v0}, Lf/c/a/b1;-><init>(Lf/c/a/z0;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public final x(Z)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    iget-object p1, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean p1, p1, Lf/c/a/a$n;->b:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lf/c/a/a;->u()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object p1, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean p1, p1, Lf/c/a/a$n;->b:Z

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lf/c/a/a;->u()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lf/c/a/a;->f:Lf/c/a/a$n;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    :cond_3
    return v0
.end method

.method public final y()V
    .locals 7

    invoke-virtual {p0}, Lf/c/a/a;->q()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lf/c/a/g1;

    iget-object v1, p0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    monitor-enter v0

    :try_start_0
    const-string v1, "deeplink_url"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    monitor-exit v0

    monitor-enter v0

    :try_start_1
    const-string v2, "deeplink_click_time"

    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const-wide/16 v3, -0x1

    :try_start_2
    iget-object v5, v0, Lf/c/a/g1;->a:Landroid/content/SharedPreferences;

    invoke-interface {v5, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :catch_0
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-wide v5, v3

    :goto_0
    monitor-exit v0

    if-nez v1, :cond_1

    return-void

    :cond_1
    cmp-long v2, v5, v3

    if-nez v2, :cond_2

    return-void

    :cond_2
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v3, Lf/c/a/j;

    invoke-direct {v3, p0, v1, v5, v6}, Lf/c/a/j;-><init>(Lf/c/a/a;Landroid/net/Uri;J)V

    check-cast v2, Lf/c/a/n1/c;

    invoke-virtual {v2, v3}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    monitor-enter v0

    :try_start_4
    const-string v1, "deeplink_url"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->i(Ljava/lang/String;)V

    const-string v1, "deeplink_click_time"

    invoke-virtual {v0, v1}, Lf/c/a/g1;->i(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit v0

    return-void

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1

    :catchall_2
    move-exception v1

    monitor-exit v0

    throw v1

    :catchall_3
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public z(Ljava/lang/String;JJLjava/lang/String;)V
    .locals 11

    move-object v8, p0

    iget-object v9, v8, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v10, Lf/c/a/a$a;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lf/c/a/a$a;-><init>(Lf/c/a/a;Ljava/lang/String;JJLjava/lang/String;)V

    check-cast v9, Lf/c/a/n1/c;

    invoke-virtual {v9, v10}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method
