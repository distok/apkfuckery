.class public Lf/c/a/z0;
.super Ljava/lang/Object;
.source "SdkClickHandler.java"

# interfaces
.implements Lf/c/a/m0;


# instance fields
.field public a:Z

.field public b:Lf/c/a/j0;

.field public c:Lf/c/a/b0;

.field public d:Ljava/lang/String;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/c/a/l;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lf/c/a/n1/h;

.field public g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/c/a/h0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/c/a/h0;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2}, Lf/c/a/z0;->a(Lf/c/a/h0;Z)V

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/z0;->b:Lf/c/a/j0;

    sget-object p1, Lf/c/a/b0;->e:Lf/c/a/b0;

    iput-object p1, p0, Lf/c/a/z0;->c:Lf/c/a/b0;

    new-instance p1, Lf/c/a/n1/c;

    const-string p2, "SdkClickHandler"

    invoke-direct {p1, p2}, Lf/c/a/n1/c;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    return-void
.end method


# virtual methods
.method public a(Lf/c/a/h0;Z)V
    .locals 0

    xor-int/lit8 p2, p2, 0x1

    iput-boolean p2, p0, Lf/c/a/z0;->a:Z

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lf/c/a/z0;->e:Ljava/util/List;

    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lf/c/a/z0;->g:Ljava/lang/ref/WeakReference;

    invoke-interface {p1}, Lf/c/a/h0;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/z0;->d:Ljava/lang/String;

    return-void
.end method

.method public final b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p1}, Lf/c/a/l;->g()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p3}, Lf/c/a/l1;->k(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p3, v0

    const/4 p1, 0x1

    aput-object p2, p3, p1

    const-string p1, "%s. (%s)"

    invoke-static {p1, p3}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lf/c/a/z0;->b:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    invoke-interface {p2, p1, p3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Lf/c/a/l;)V
    .locals 4

    invoke-virtual {p1}, Lf/c/a/l;->m()I

    move-result v0

    iget-object v1, p0, Lf/c/a/z0;->b:Lf/c/a/j0;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v0, "Retrying sdk_click package for the %d time"

    invoke-interface {v1, v0, v2}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/z0$a;

    invoke-direct {v1, p0, p1}, Lf/c/a/z0$a;-><init>(Lf/c/a/z0;Lf/c/a/l;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public d(Lf/c/a/l;)V
    .locals 2

    iget-object v0, p0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    new-instance v1, Lf/c/a/z0$a;

    invoke-direct {v1, p0, p1}, Lf/c/a/z0$a;-><init>(Lf/c/a/z0;Lf/c/a/l;)V

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v1}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method
