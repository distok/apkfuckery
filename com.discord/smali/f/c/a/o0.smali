.class public Lf/c/a/o0;
.super Ljava/lang/Object;
.source "InstallReferrerHuawei.java"


# instance fields
.field public a:Lf/c/a/j0;

.field public b:Landroid/content/Context;

.field public final c:Lf/c/a/p0;

.field public final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/c/a/p0;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/o0;->a:Lf/c/a/j0;

    iput-object p1, p0, Lf/c/a/o0;->b:Landroid/content/Context;

    iput-object p2, p0, Lf/c/a/o0;->c:Lf/c/a/p0;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lf/c/a/o0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    iget-object v0, p0, Lf/c/a/o0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/c/a/o0;->a:Lf/c/a/j0;

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Should not try to read Install referrer Huawei"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/c/a/o0;->b:Landroid/content/Context;

    const-string v2, "com.huawei.appmarket.commondata"

    sget-object v3, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    return-void

    :cond_2
    const/4 v0, 0x0

    const-string v2, "content://com.huawei.appmarket.commondata/item/5"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lf/c/a/o0;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-array v8, v3, [Ljava/lang/String;

    iget-object v5, p0, Lf/c/a/o0;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v5, v2

    :try_start_1
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v4, 0x2

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lf/c/a/o0;->a:Lf/c/a/j0;

    const-string v8, "InstallReferrerHuawei reads referrer[%s] clickTime[%s] installTime[%s]"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v7, v9, v1

    aput-object v2, v9, v3

    aput-object v5, v9, v4

    invoke-interface {v6, v8, v9}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    iget-object v6, p0, Lf/c/a/o0;->c:Lf/c/a/p0;

    invoke-interface/range {v6 .. v11}, Lf/c/a/p0;->a(Ljava/lang/String;JJ)V

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lf/c/a/o0;->a:Lf/c/a/j0;

    const-string v6, "InstallReferrerHuawei fail to read referrer for package [%s] and content uri [%s]"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v7, p0, Lf/c/a/o0;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v1

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v3

    invoke-interface {v5, v6, v4}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-eqz v0, :cond_4

    goto :goto_2

    :catchall_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v2

    :try_start_2
    iget-object v4, p0, Lf/c/a/o0;->a:Lf/c/a/j0;

    const-string v5, "InstallReferrerHuawei error [%s]"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-interface {v4, v5, v3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_4

    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    iget-object v0, p0, Lf/c/a/o0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    :goto_3
    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1
.end method
