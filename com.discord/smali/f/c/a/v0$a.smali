.class public Lf/c/a/v0$a;
.super Ljava/lang/Object;
.source "PackageHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/v0;-><init>(Lf/c/a/h0;Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/v0;


# direct methods
.method public constructor <init>(Lf/c/a/v0;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/v0$a;->d:Lf/c/a/v0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lf/c/a/v0$a;->d:Lf/c/a/v0;

    iget-object v1, v0, Lf/c/a/v0;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/c/a/h0;

    new-instance v2, Lf/c/a/x0;

    invoke-direct {v2, v1, v0}, Lf/c/a/x0;-><init>(Lf/c/a/h0;Lf/c/a/k0;)V

    iput-object v2, v0, Lf/c/a/v0;->b:Lf/c/a/l0;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v1, v0, Lf/c/a/v0;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v1, "Package queue"

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, v0, Lf/c/a/v0;->g:Landroid/content/Context;

    const-string v5, "AdjustIoPackageQueue"

    const-class v6, Ljava/util/List;

    invoke-static {v4, v5, v1, v6}, Lf/c/a/l1;->x(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    iput-object v4, v0, Lf/c/a/v0;->d:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    iget-object v5, v0, Lf/c/a/v0;->h:Lf/c/a/j0;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    const-string v1, "Failed to read %s file (%s)"

    invoke-interface {v5, v1, v6}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lf/c/a/v0;->d:Ljava/util/List;

    :goto_0
    iget-object v1, v0, Lf/c/a/v0;->d:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/c/a/v0;->h:Lf/c/a/j0;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const-string v1, "Package handler read %d packages"

    invoke-interface {v0, v1, v2}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lf/c/a/v0;->d:Ljava/util/List;

    :goto_1
    return-void
.end method
