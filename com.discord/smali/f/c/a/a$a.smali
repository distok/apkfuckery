.class public Lf/c/a/a$a;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->z(Ljava/lang/String;JJLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:J

.field public final synthetic f:J

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$a;->h:Lf/c/a/a;

    iput-object p2, p0, Lf/c/a/a$a;->d:Ljava/lang/String;

    iput-wide p3, p0, Lf/c/a/a$a;->e:J

    iput-wide p5, p0, Lf/c/a/a$a;->f:J

    iput-object p7, p0, Lf/c/a/a$a;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    iget-object v0, p0, Lf/c/a/a$a;->h:Lf/c/a/a;

    iget-object v1, p0, Lf/c/a/a$a;->d:Ljava/lang/String;

    iget-wide v2, p0, Lf/c/a/a$a;->e:J

    iget-wide v4, p0, Lf/c/a/a$a;->f:J

    iget-object v6, p0, Lf/c/a/a$a;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lf/c/a/a;->u()Z

    move-result v7

    if-nez v7, :cond_0

    goto/16 :goto_1

    :cond_0
    if-nez v1, :cond_1

    goto/16 :goto_1

    :cond_1
    const-string v7, "google"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-wide v8, v7, Lf/c/a/m;->clickTime:J

    cmp-long v10, v2, v8

    if-nez v10, :cond_3

    iget-wide v8, v7, Lf/c/a/m;->installBegin:J

    cmp-long v10, v4, v8

    if-nez v10, :cond_3

    iget-object v7, v7, Lf/c/a/m;->installReferrer:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_1

    :cond_2
    const-string v7, "huawei"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-wide v8, v7, Lf/c/a/m;->clickTimeHuawei:J

    cmp-long v10, v2, v8

    if-nez v10, :cond_3

    iget-wide v8, v7, Lf/c/a/m;->installBeginHuawei:J

    cmp-long v10, v4, v8

    if-nez v10, :cond_3

    iget-object v7, v7, Lf/c/a/m;->installReferrerHuawei:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_1

    :cond_3
    iget-object v11, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v9, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v10, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v12, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_4

    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    new-instance v7, Lf/c/a/u0;

    move-object v8, v7

    invoke-direct/range {v8 .. v14}, Lf/c/a/u0;-><init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V

    iput-object v1, v7, Lf/c/a/u0;->k:Ljava/lang/String;

    iput-wide v2, v7, Lf/c/a/u0;->f:J

    iput-wide v4, v7, Lf/c/a/u0;->h:J

    iput-object v6, v7, Lf/c/a/u0;->m:Ljava/lang/String;

    const-string v1, "install_referrer"

    invoke-virtual {v7, v1}, Lf/c/a/u0;->h(Ljava/lang/String;)Lf/c/a/l;

    move-result-object v1

    :goto_0
    iget-object v0, v0, Lf/c/a/a;->k:Lf/c/a/m0;

    check-cast v0, Lf/c/a/z0;

    invoke-virtual {v0, v1}, Lf/c/a/z0;->d(Lf/c/a/l;)V

    :goto_1
    return-void
.end method
