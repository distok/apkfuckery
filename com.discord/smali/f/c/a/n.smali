.class public Lf/c/a/n;
.super Ljava/lang/Object;
.source "AdjustConfig.java"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lf/c/a/s0;

.field public f:Lf/c/a/j0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    sget-object v1, Lf/c/a/q0;->f:Lf/c/a/q0;

    const-string v2, "production"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lf/c/a/j0;->b(Lf/c/a/q0;Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/n;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/c/a/n;->b:Ljava/lang/String;

    iput-object p3, p0, Lf/c/a/n;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 5

    iget-object v0, p0, Lf/c/a/n;->b:Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "Missing App Token"

    invoke-interface {v0, v4, v3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xc

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v0, v4, v2

    const-string v0, "Malformed App Token \'%s\'"

    invoke-interface {v3, v0, v4}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Lf/c/a/n;->c:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "Missing environment"

    invoke-interface {v0, v4, v3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    const/4 v0, 0x0

    goto :goto_4

    :cond_3
    const-string v3, "sandbox"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v0, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "SANDBOX: Adjust is running in Sandbox mode. Use this setting for testing. Don\'t forget to set the environment to `production` before publishing!"

    invoke-interface {v0, v4, v3}, Lf/c/a/j0;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    const/4 v0, 0x1

    goto :goto_4

    :cond_4
    const-string v3, "production"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v0, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "PRODUCTION: Adjust is running in Production mode. Use this setting only for the build that you want to publish. Set the environment to `sandbox` if you want to test your app!"

    invoke-interface {v0, v4, v3}, Lf/c/a/j0;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_5
    iget-object v3, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v0, v4, v2

    const-string v0, "Unknown environment \'%s\'"

    invoke-interface {v3, v0, v4}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :goto_4
    if-nez v0, :cond_6

    return v2

    :cond_6
    iget-object v0, p0, Lf/c/a/n;->a:Landroid/content/Context;

    if-nez v0, :cond_7

    iget-object v0, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "Missing context"

    invoke-interface {v0, v4, v3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_5
    const/4 v0, 0x0

    goto :goto_6

    :cond_7
    const-string v3, "android.permission.INTERNET"

    invoke-static {v0, v3}, Lf/c/a/l1;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lf/c/a/n;->f:Lf/c/a/j0;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "Missing permission: INTERNET"

    invoke-interface {v0, v4, v3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_8
    const/4 v0, 0x1

    :goto_6
    if-nez v0, :cond_9

    return v2

    :cond_9
    return v1
.end method
