.class public Lf/c/a/j;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Landroid/net/Uri;

.field public final synthetic e:J

.field public final synthetic f:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;Landroid/net/Uri;J)V
    .locals 0

    iput-object p1, p0, Lf/c/a/j;->f:Lf/c/a/a;

    iput-object p2, p0, Lf/c/a/j;->d:Landroid/net/Uri;

    iput-wide p3, p0, Lf/c/a/j;->e:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    iget-object v0, p0, Lf/c/a/j;->f:Lf/c/a/a;

    iget-object v1, p0, Lf/c/a/j;->d:Landroid/net/Uri;

    iget-wide v2, p0, Lf/c/a/j;->e:J

    invoke-virtual {v0}, Lf/c/a/a;->u()Z

    move-result v4

    if-nez v4, :cond_0

    goto/16 :goto_3

    :cond_0
    sget-object v4, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    goto :goto_0

    :cond_2
    const-string v7, "^(fb|vk)[0-9]{5,}[^:]*://authorize.*access_token=.*"

    invoke-virtual {v6, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v6, 0x1

    :goto_1
    if-eqz v6, :cond_5

    iget-object v0, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    const-string v2, "Deep link ("

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") processing skipped"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_5
    iget-object v6, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v7, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v8, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v9, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    const/4 v10, 0x0

    if-nez v1, :cond_6

    goto :goto_2

    :cond_6
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_9

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-nez v12, :cond_7

    goto :goto_2

    :cond_7
    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v12

    new-array v13, v4, [Ljava/lang/Object;

    aput-object v1, v13, v5

    const-string v5, "Url to parse (%s)"

    invoke-interface {v12, v5, v13}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v5, Landroid/net/UrlQuerySanitizer;

    invoke-direct {v5}, Landroid/net/UrlQuerySanitizer;-><init>()V

    invoke-static {}, Landroid/net/UrlQuerySanitizer;->getAllButNulLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    move-result-object v12

    invoke-virtual {v5, v12}, Landroid/net/UrlQuerySanitizer;->setUnregisteredParameterValueSanitizer(Landroid/net/UrlQuerySanitizer$ValueSanitizer;)V

    invoke-virtual {v5, v4}, Landroid/net/UrlQuerySanitizer;->setAllowUnregisteredParamaters(Z)V

    invoke-virtual {v5, v11}, Landroid/net/UrlQuerySanitizer;->parseUrl(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/net/UrlQuerySanitizer;->getParameterList()Ljava/util/List;

    move-result-object v4

    invoke-static {v4, v6, v7, v8, v9}, Ls/a/b/b/a;->U(Ljava/util/List;Lf/c/a/m;Lf/c/a/n;Lf/c/a/c0;Lf/c/a/e1;)Lf/c/a/u0;

    move-result-object v4

    if-nez v4, :cond_8

    goto :goto_2

    :cond_8
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lf/c/a/u0;->j:Ljava/lang/String;

    iput-wide v2, v4, Lf/c/a/u0;->g:J

    const-string v1, "deeplink"

    invoke-virtual {v4, v1}, Lf/c/a/u0;->h(Ljava/lang/String;)Lf/c/a/l;

    move-result-object v10

    :cond_9
    :goto_2
    if-nez v10, :cond_a

    goto :goto_3

    :cond_a
    iget-object v0, v0, Lf/c/a/a;->k:Lf/c/a/m0;

    check-cast v0, Lf/c/a/z0;

    iget-object v1, v0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/z0$a;

    invoke-direct {v2, v0, v10}, Lf/c/a/z0$a;-><init>(Lf/c/a/z0;Lf/c/a/l;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    :goto_3
    return-void
.end method
