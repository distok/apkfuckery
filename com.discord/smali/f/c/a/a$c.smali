.class public Lf/c/a/a$c;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->h(Lf/c/a/f1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/f1;

.field public final synthetic e:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;Lf/c/a/f1;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$c;->e:Lf/c/a/a;

    iput-object p2, p0, Lf/c/a/a$c;->d:Lf/c/a/f1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lf/c/a/a$c;->e:Lf/c/a/a;

    iget-object v1, p0, Lf/c/a/a$c;->d:Lf/c/a/f1;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lf/c/a/y0;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lf/c/a/a;->J(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Handler;

    iget-object v3, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v3, v1, Lf/c/a/y0;->h:Lcom/adjust/sdk/AdjustAttribution;

    invoke-virtual {v0, v3}, Lf/c/a/a;->K(Lcom/adjust/sdk/AdjustAttribution;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Lf/c/a/a;->v(Landroid/os/Handler;)V

    :cond_0
    iget-object v2, v0, Lf/c/a/a;->i:Lcom/adjust/sdk/AdjustAttribution;

    if-nez v2, :cond_1

    iget-object v2, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v2, v2, Lf/c/a/m;->askingAttribution:Z

    if-nez v2, :cond_1

    iget-object v2, v0, Lf/c/a/a;->j:Lf/c/a/i0;

    check-cast v2, Lf/c/a/u;

    iget-object v3, v2, Lf/c/a/u;->g:Lf/c/a/n1/h;

    new-instance v4, Lf/c/a/v;

    invoke-direct {v4, v2}, Lf/c/a/v;-><init>(Lf/c/a/u;)V

    check-cast v3, Lf/c/a/n1/c;

    invoke-virtual {v3, v4}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    :cond_1
    iget-boolean v2, v1, Lf/c/a/y0;->a:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    new-instance v2, Lf/c/a/g1;

    iget-object v4, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v4, v4, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v2, v4}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    monitor-enter v2

    :try_start_0
    const-string v4, "install_tracked"

    invoke-virtual {v2, v4, v3}, Lf/c/a/g1;->k(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :goto_0
    iget-boolean v2, v1, Lf/c/a/y0;->a:Z

    if-eqz v2, :cond_3

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-boolean v1, v1, Lf/c/a/y0;->a:Z

    if-nez v1, :cond_4

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    iget-object v0, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iput-boolean v3, v0, Lf/c/a/a$n;->f:Z

    return-void
.end method
