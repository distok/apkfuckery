.class public Lf/c/a/a$k;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->o(Lf/c/a/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/o;

.field public final synthetic e:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;Lf/c/a/o;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$k;->e:Lf/c/a/a;

    iput-object p2, p0, Lf/c/a/a$k;->d:Lf/c/a/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    iget-object v0, p0, Lf/c/a/a$k;->e:Lf/c/a/a;

    iget-object v1, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v1, v1, Lf/c/a/a$n;->g:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v1, v3, [Ljava/lang/Object;

    const-string v4, "Event tracked before first activity resumed.\nIf it was triggered in the Application class, it might timestamp or even send an install long before the user opens the app.\nPlease check https://github.com/adjust/android_sdk#can-i-trigger-an-event-at-application-launch for more information."

    invoke-interface {v0, v4, v1}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/c/a/a$k;->e:Lf/c/a/a;

    invoke-static {v0}, Lf/c/a/a;->p(Lf/c/a/a;)V

    :cond_0
    iget-object v0, p0, Lf/c/a/a$k;->e:Lf/c/a/a;

    iget-object v1, p0, Lf/c/a/a$k;->d:Lf/c/a/o;

    invoke-virtual {v0}, Lf/c/a/a;->q()Z

    move-result v4

    if-nez v4, :cond_1

    goto/16 :goto_3

    :cond_1
    invoke-virtual {v0}, Lf/c/a/a;->u()Z

    move-result v4

    if-nez v4, :cond_2

    goto/16 :goto_3

    :cond_2
    if-nez v1, :cond_3

    iget-object v4, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v5, v3, [Ljava/lang/Object;

    const-string v6, "Event missing"

    invoke-interface {v4, v6, v5}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    iget-object v4, v1, Lf/c/a/o;->a:Ljava/lang/String;

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    :goto_0
    if-nez v4, :cond_5

    iget-object v4, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v5, v3, [Ljava/lang/Object;

    const-string v6, "Event not initialized correctly"

    invoke-interface {v4, v6, v5}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const/4 v4, 0x1

    :goto_2
    if-nez v4, :cond_6

    goto/16 :goto_3

    :cond_6
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v4, v4, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v4, :cond_7

    goto/16 :goto_3

    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-object v4, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget v5, v4, Lf/c/a/m;->eventCount:I

    add-int/2addr v5, v2

    iput v5, v4, Lf/c/a/m;->eventCount:I

    invoke-virtual {v0, v10, v11}, Lf/c/a/a;->I(J)Z

    new-instance v4, Lf/c/a/u0;

    iget-object v6, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v7, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v8, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v9, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    move-object v5, v4

    invoke-direct/range {v5 .. v11}, Lf/c/a/u0;-><init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V

    iget-object v5, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iget-object v7, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v7, v7, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v8, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v7, v8}, Ls/a/b/b/a;->y(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_8
    iget-object v7, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v7, v7, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v8, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v7, v8}, Ls/a/b/b/a;->z(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v7

    if-eqz v7, :cond_9

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_9
    const/4 v7, 0x0

    iget-object v8, v4, Lf/c/a/u0;->e:Lf/c/a/e1;

    iget-object v8, v8, Lf/c/a/e1;->a:Ljava/util/Map;

    const-string v9, "Callback"

    invoke-static {v8, v7, v9}, Lf/c/a/l1;->t(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v8

    const-string v9, "callback_params"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v8, v4, Lf/c/a/u0;->e:Lf/c/a/e1;

    iget-object v8, v8, Lf/c/a/e1;->b:Ljava/util/Map;

    const-string v9, "Partner"

    invoke-static {v8, v7, v9}, Lf/c/a/l1;->t(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v8

    const-string v9, "partner_params"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v9, v9, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v8, v9}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v8, v4, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v8, v8, Lf/c/a/u0$a;->g:Ljava/lang/String;

    const-string v9, "android_uuid"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const-string/jumbo v9, "tracking_enabled"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->a:Ljava/lang/String;

    const-string v9, "gps_adid"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->b:Ljava/lang/String;

    const-string v9, "gps_adid_src"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget v8, v8, Lf/c/a/c0;->c:I

    int-to-long v8, v8

    const-string v10, "gps_adid_attempt"

    invoke-static {v6, v10, v8, v9}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {v4, v6}, Lf/c/a/u0;->j(Ljava/util/Map;)Z

    move-result v8

    if-nez v8, :cond_a

    sget-object v8, Lf/c/a/u0;->p:Lf/c/a/j0;

    new-array v9, v3, [Ljava/lang/Object;

    const-string v10, "Google Advertising ID not detected, fallback to non Google Play identifiers will take place"

    invoke-interface {v8, v10, v9}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v9, v9, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v8, v9}, Lf/c/a/c0;->a(Landroid/content/Context;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->f:Ljava/lang/String;

    const-string v9, "mac_sha1"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->g:Ljava/lang/String;

    const-string v9, "mac_md5"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->h:Ljava/lang/String;

    const-string v9, "android_id"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->r:Ljava/lang/String;

    const-string v9, "api_level"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "app_secret"

    invoke-static {v6, v8, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v8, v8, Lf/c/a/n;->b:Ljava/lang/String;

    const-string v9, "app_token"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v8, v8, Lf/c/a/c0;->l:Ljava/lang/String;

    const-string v9, "app_version"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v9, "attribution_deeplink"

    invoke-static {v6, v9, v8}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v9, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v9, v9, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v9}, Lf/c/a/l1;->d(Landroid/content/Context;)I

    move-result v9

    int-to-long v9, v9

    const-string v11, "connectivity_type"

    invoke-static {v6, v11, v9, v10}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->t:Ljava/lang/String;

    const-string v10, "country"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->A:Ljava/lang/String;

    const-string v10, "cpu_type"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v9, v4, Lf/c/a/u0;->a:J

    const-string v11, "created_at"

    invoke-static {v6, v11, v9, v10}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    const-string v9, "currency"

    invoke-static {v6, v9, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "device_known"

    invoke-static {v6, v9, v7}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->o:Ljava/lang/String;

    const-string v10, "device_manufacturer"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->n:Ljava/lang/String;

    const-string v10, "device_name"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->m:Ljava/lang/String;

    const-string v10, "device_type"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->y:Ljava/lang/String;

    const-string v10, "display_height"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->x:Ljava/lang/String;

    const-string v10, "display_width"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v9, v9, Lf/c/a/n;->c:Ljava/lang/String;

    const-string v10, "environment"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "event_callback_id"

    invoke-static {v6, v9, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget v9, v9, Lf/c/a/u0$a;->a:I

    int-to-long v9, v9

    const-string v11, "event_count"

    invoke-static {v6, v11, v9, v10}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v9, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v9, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v10, "event_buffering_enabled"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v9, v1, Lf/c/a/o;->a:Ljava/lang/String;

    const-string v10, "event_token"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "external_device_id"

    invoke-static {v6, v9, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v9, v9, Lf/c/a/c0;->i:Ljava/lang/String;

    const-string v10, "fb_id"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Lf/c/a/l1;->e(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "fire_adid"

    invoke-static {v6, v10, v9}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Lf/c/a/l1;->f(Landroid/content/ContentResolver;)Ljava/lang/Boolean;

    move-result-object v5

    const-string v9, "fire_tracking_enabled"

    invoke-static {v6, v9, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->z:Ljava/lang/String;

    const-string v9, "hardware_name"

    invoke-static {v6, v9, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->s:Ljava/lang/String;

    const-string v9, "language"

    invoke-static {v6, v9, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v5}, Lf/c/a/l1;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "mcc"

    invoke-static {v6, v9, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v5}, Lf/c/a/l1;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "mnc"

    invoke-static {v6, v9, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "needs_response_details"

    invoke-static {v6, v5, v8}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v5, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v5}, Lf/c/a/l1;->j(Landroid/content/Context;)I

    move-result v5

    int-to-long v8, v5

    const-string v5, "network_type"

    invoke-static {v6, v5, v8, v9}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->B:Ljava/lang/String;

    const-string v8, "os_build"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->p:Ljava/lang/String;

    const-string v8, "os_name"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->q:Ljava/lang/String;

    const-string v8, "os_version"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->k:Ljava/lang/String;

    const-string v8, "package_name"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v5, v5, Lf/c/a/u0$a;->h:Ljava/lang/String;

    const-string v8, "push_token"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->w:Ljava/lang/String;

    const-string v8, "screen_density"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->v:Ljava/lang/String;

    const-string v8, "screen_format"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->u:Ljava/lang/String;

    const-string v8, "screen_size"

    invoke-static {v6, v8, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "secret_id"

    invoke-static {v6, v5, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v4, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget v5, v5, Lf/c/a/u0$a;->b:I

    int-to-long v7, v5

    const-string v5, "session_count"

    invoke-static {v6, v5, v7, v8}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v5, v4, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v7, v5, Lf/c/a/u0$a;->f:J

    const-string v5, "session_length"

    invoke-static {v6, v5, v7, v8}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v5, v4, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget v5, v5, Lf/c/a/u0$a;->c:I

    int-to-long v7, v5

    const-string/jumbo v5, "subsession_count"

    invoke-static {v6, v5, v7, v8}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v5, v4, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v7, v5, Lf/c/a/u0$a;->d:J

    const-string/jumbo v5, "time_spent"

    invoke-static {v6, v5, v7, v8}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {v4, v6}, Lf/c/a/u0;->i(Ljava/util/Map;)V

    sget-object v5, Lf/c/a/k;->f:Lf/c/a/k;

    invoke-virtual {v4, v5}, Lf/c/a/u0;->k(Lf/c/a/k;)Lf/c/a/l;

    move-result-object v7

    const-string v8, "/event"

    invoke-virtual {v7, v8}, Lf/c/a/l;->s(Ljava/lang/String;)V

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v1, v1, Lf/c/a/o;->a:Ljava/lang/String;

    aput-object v1, v2, v3

    const-string v1, "\'%s\'"

    invoke-static {v1, v2}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lf/c/a/l;->t(Ljava/lang/String;)V

    invoke-virtual {v5}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lf/c/a/l;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v4, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v4, v3, Lf/c/a/n;->a:Landroid/content/Context;

    iget-object v3, v3, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v6, v1, v2, v4, v3}, Lf/c/a/t;->c(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lf/c/a/j0;)V

    invoke-virtual {v7, v6}, Lf/c/a/l;->r(Ljava/util/Map;)V

    iget-object v1, v0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v1, v7}, Lf/c/a/k0;->g(Lf/c/a/l;)V

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v1}, Lf/c/a/k0;->e()V

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/c/a/a;->O()V

    :goto_3
    return-void
.end method
