.class public Lf/c/a/x0;
.super Ljava/lang/Object;
.source "RequestHandler.java"

# interfaces
.implements Lf/c/a/l0;


# instance fields
.field public a:Lf/c/a/n1/h;

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/c/a/k0;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/c/a/h0;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lf/c/a/j0;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lf/c/a/h0;Lf/c/a/k0;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/x0;->d:Lf/c/a/j0;

    new-instance v0, Lf/c/a/n1/c;

    const-string v1, "RequestHandler"

    invoke-direct {v0, v1}, Lf/c/a/n1/c;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lf/c/a/x0;->a:Lf/c/a/n1/h;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lf/c/a/x0;->b:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lf/c/a/x0;->c:Ljava/lang/ref/WeakReference;

    invoke-interface {p2}, Lf/c/a/k0;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/x0;->e:Ljava/lang/String;

    invoke-interface {p2}, Lf/c/a/k0;->b()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/x0;->f:Ljava/lang/String;

    invoke-interface {p2}, Lf/c/a/k0;->c()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/x0;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    invoke-virtual {p1}, Lf/c/a/l;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Lf/c/a/l1;->k(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v0, p3, v1

    const/4 v0, 0x1

    aput-object p2, p3, v0

    const-string p2, "%s. (%s) Will retry later"

    invoke-static {p2, p3}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lf/c/a/x0;->d:Lf/c/a/j0;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-interface {p3, p2, v0}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lf/c/a/y0;->a(Lf/c/a/l;)Lf/c/a/y0;

    move-result-object p3

    iput-object p2, p3, Lf/c/a/y0;->d:Ljava/lang/String;

    iget-object p2, p0, Lf/c/a/x0;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/c/a/k0;

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-interface {p2, p3, p1}, Lf/c/a/k0;->i(Lf/c/a/y0;Lf/c/a/l;)V

    return-void
.end method

.method public final b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    invoke-virtual {p1}, Lf/c/a/l;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Lf/c/a/l1;->k(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v0, p3, v1

    const/4 v0, 0x1

    aput-object p2, p3, v0

    const-string p2, "%s. (%s)"

    invoke-static {p2, p3}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lf/c/a/x0;->d:Lf/c/a/j0;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-interface {p3, p2, v0}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lf/c/a/y0;->a(Lf/c/a/l;)Lf/c/a/y0;

    move-result-object p1

    iput-object p2, p1, Lf/c/a/y0;->d:Ljava/lang/String;

    iget-object p2, p0, Lf/c/a/x0;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/c/a/k0;

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-interface {p2, p1}, Lf/c/a/k0;->f(Lf/c/a/y0;)V

    return-void
.end method
