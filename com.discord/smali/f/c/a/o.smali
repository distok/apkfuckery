.class public Lf/c/a/o;
.super Ljava/lang/Object;
.source "AdjustEvent.java"


# static fields
.field public static b:Lf/c/a/j0;


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    sput-object v0, Lf/c/a/o;->b:Lf/c/a/j0;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/c/a/o;->b:Lf/c/a/j0;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    if-gtz v1, :cond_0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "Event Token can\'t be empty"

    invoke-interface {v0, v3, v1}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    return-void

    :cond_1
    iput-object p1, p0, Lf/c/a/o;->a:Ljava/lang/String;

    return-void
.end method
