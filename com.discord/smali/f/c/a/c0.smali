.class public Lf/c/a/c0;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/Boolean;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11

    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/c/a/c0;->e:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    sget-object v3, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    const/16 v3, 0x18

    const/4 v4, 0x0

    if-lt p2, v3, :cond_0

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/os/LocaleList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5, v0}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v3

    goto :goto_0

    :cond_0
    if-ge p2, v3, :cond_1

    iget-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_0

    :cond_1
    move-object v3, v4

    :goto_0
    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lf/c/a/c0;->k:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-object v5, v4

    :goto_1
    iput-object v5, p0, Lf/c/a/c0;->l:Ljava/lang/String;

    and-int/lit8 v5, v1, 0xf

    const/4 v6, 0x4

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-eq v5, v9, :cond_3

    if-eq v5, v8, :cond_3

    if-eq v5, v7, :cond_2

    if-eq v5, v6, :cond_2

    move-object v10, v4

    goto :goto_2

    :cond_2
    const-string/jumbo v10, "tablet"

    goto :goto_2

    :cond_3
    const-string v10, "phone"

    :goto_2
    iput-object v10, p0, Lf/c/a/c0;->m:Ljava/lang/String;

    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v10, p0, Lf/c/a/c0;->n:Ljava/lang/String;

    sget-object v10, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v10, p0, Lf/c/a/c0;->o:Ljava/lang/String;

    const-string v10, "android"

    iput-object v10, p0, Lf/c/a/c0;->p:Ljava/lang/String;

    sget-object v10, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v10, p0, Lf/c/a/c0;->q:Ljava/lang/String;

    const-string v10, ""

    invoke-static {v10, p2}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/c/a/c0;->r:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/c/a/c0;->s:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/c/a/c0;->t:Ljava/lang/String;

    const-string p2, "normal"

    if-eq v5, v9, :cond_7

    if-eq v5, v8, :cond_6

    if-eq v5, v7, :cond_5

    if-eq v5, v6, :cond_4

    move-object v3, v4

    goto :goto_3

    :cond_4
    const-string/jumbo v3, "xlarge"

    goto :goto_3

    :cond_5
    const-string v3, "large"

    goto :goto_3

    :cond_6
    move-object v3, p2

    goto :goto_3

    :cond_7
    const-string v3, "small"

    :goto_3
    iput-object v3, p0, Lf/c/a/c0;->u:Ljava/lang/String;

    and-int/lit8 v1, v1, 0x30

    const/16 v3, 0x10

    if-eq v1, v3, :cond_9

    const/16 p2, 0x20

    if-eq v1, p2, :cond_8

    move-object p2, v4

    goto :goto_4

    :cond_8
    const-string p2, "long"

    :cond_9
    :goto_4
    iput-object p2, p0, Lf/c/a/c0;->v:Ljava/lang/String;

    iget p2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    if-nez p2, :cond_a

    move-object p2, v4

    goto :goto_5

    :cond_a
    const/16 v1, 0x8c

    if-ge p2, v1, :cond_b

    const-string p2, "low"

    goto :goto_5

    :cond_b
    const/16 v1, 0xc8

    if-le p2, v1, :cond_c

    const-string p2, "high"

    goto :goto_5

    :cond_c
    const-string p2, "medium"

    :goto_5
    iput-object p2, p0, Lf/c/a/c0;->w:Ljava/lang/String;

    iget p2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/c/a/c0;->x:Ljava/lang/String;

    iget p2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/c/a/c0;->y:Ljava/lang/String;

    const-string p2, "android4.22.0"

    iput-object p2, p0, Lf/c/a/c0;->j:Ljava/lang/String;

    const-string p2, "aid"

    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.facebook.katana"

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v1, :cond_11

    array-length v2, v1

    if-eq v2, v9, :cond_d

    goto :goto_6

    :cond_d
    aget-object v1, v1, v0

    const-string v2, "30820268308201d102044a9c4610300d06092a864886f70d0101040500307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e3020170d3039303833313231353231365a180f32303530303932353231353231365a307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e30819f300d06092a864886f70d010101050003818d0030818902818100c207d51df8eb8c97d93ba0c8c1002c928fab00dc1b42fca5e66e99cc3023ed2d214d822bc59e8e35ddcf5f44c7ae8ade50d7e0c434f500e6c131f4a2834f987fc46406115de2018ebbb0d5a3c261bd97581ccfef76afc7135a6d59e8855ecd7eacc8f8737e794c60a761c536b72b11fac8e603f5da1a2d54aa103b8a13c0dbc10203010001300d06092a864886f70d0101040500038181005ee9be8bcbb250648d3b741290a82a1c9dc2e76a0af2f2228f1d9f9c4007529c446a70175c5a900d5141812866db46be6559e2141616483998211f4a673149fb2232a10d247663b26a9031e15f84bc1c74d141ff98a02d76f85b2c8ab2571b6469b232d8e768a7f7ca04f7abe4a775615916c07940656b58717457b42bd928a2"

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    goto :goto_6

    :cond_e
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v1, "content://com.facebook.katana.provider.AttributionIdProvider"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    filled-new-array {p2}, [Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_f

    goto :goto_6

    :cond_f
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_10

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_6

    :cond_10
    invoke-interface {v1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    invoke-interface {v1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_7

    :catch_1
    :cond_11
    :goto_6
    move-object p2, v4

    :goto_7
    iput-object p2, p0, Lf/c/a/c0;->i:Ljava/lang/String;

    sget-object p2, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    iput-object p2, p0, Lf/c/a/c0;->z:Ljava/lang/String;

    sget-object p2, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    sget-object p2, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    if-eqz p2, :cond_13

    array-length v1, p2

    if-nez v1, :cond_12

    goto :goto_8

    :cond_12
    aget-object p2, p2, v0

    goto :goto_9

    :cond_13
    :goto_8
    move-object p2, v4

    :goto_9
    iput-object p2, p0, Lf/c/a/c0;->A:Ljava/lang/String;

    sget-object p2, Landroid/os/Build;->ID:Ljava/lang/String;

    iput-object p2, p0, Lf/c/a/c0;->B:Ljava/lang/String;

    const/16 p2, 0x1000

    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    sget-object v1, Lf/c/a/l1;->b:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    iget-wide v5, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-direct {v2, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_a

    :catch_2
    move-object v0, v4

    :goto_a
    iput-object v0, p0, Lf/c/a/c0;->C:Ljava/lang/String;

    :try_start_3
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1

    sget-object p2, Lf/c/a/l1;->b:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/util/Date;

    iget-wide v1, p1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    iput-object v4, p0, Lf/c/a/c0;->D:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 4

    iget-boolean v0, p0, Lf/c/a/c0;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "android.permission.ACCESS_WIFI_STATE"

    invoke-static {p1, v0}, Lf/c/a/l1;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Missing permission: ACCESS_WIFI_STATE"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    const-string/jumbo v0, "wlan0"

    invoke-static {v0}, Ls/a/b/b/a;->J(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const-string v0, "eth0"

    invoke-static {v0}, Ls/a/b/b/a;->J(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    :try_start_0
    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_4

    goto :goto_0

    :catch_0
    :cond_4
    move-object v0, v1

    :goto_0
    const-string v2, ""

    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    goto :goto_1

    :cond_6
    const-string v3, "\\s"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    :goto_1
    move-object v0, v1

    :cond_7
    if-nez v0, :cond_8

    move-object v3, v1

    goto :goto_2

    :cond_8
    const-string v3, "SHA-1"

    invoke-static {v0, v3}, Lf/c/a/l1;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    iput-object v3, p0, Lf/c/a/c0;->f:Ljava/lang/String;

    if-nez v0, :cond_9

    goto :goto_3

    :cond_9
    const-string v1, ":"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MD5"

    invoke-static {v0, v1}, Lf/c/a/l1;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    iput-object v1, p0, Lf/c/a/c0;->g:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lf/c/a/c0;->h:Ljava/lang/String;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/c/a/c0;->e:Z

    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 8

    iget-object v0, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    iget-object v1, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const/4 v2, 0x0

    iput-object v2, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    iput-object v2, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    iput-object v2, p0, Lf/c/a/c0;->b:Ljava/lang/String;

    const/4 v2, -0x1

    iput v2, p0, Lf/c/a/c0;->c:I

    const/4 v2, 0x1

    const/4 v3, 0x1

    :goto_0
    const/4 v4, 0x3

    if-gt v3, v4, :cond_3

    mul-int/lit16 v4, v3, 0xbb8

    int-to-long v4, v4

    :try_start_0
    invoke-static {p1, v4, v5}, Ls/a/b/b/a;->x(Landroid/content/Context;J)Lf/c/a/f0;

    move-result-object v4

    iget-object v5, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    if-nez v5, :cond_0

    iget-object v5, v4, Lf/c/a/f0;->a:Ljava/lang/String;

    iput-object v5, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    :cond_0
    iget-object v5, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    if-nez v5, :cond_1

    iget-object v4, v4, Lf/c/a/f0;->b:Ljava/lang/Boolean;

    iput-object v4, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    :cond_1
    iget-object v4, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    const-string v4, "service"

    iput-object v4, p0, Lf/c/a/c0;->b:Ljava/lang/String;

    iput v3, p0, Lf/c/a/c0;->c:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    if-gt v2, v4, :cond_8

    const-wide/16 v5, 0x2af8

    new-instance v3, Lf/c/a/i1;

    invoke-direct {v3, p1}, Lf/c/a/i1;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v5, v6}, Lf/c/a/l1;->B(Ljava/util/concurrent/Callable;J)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_4

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    const-wide/16 v6, 0x3e8

    if-nez v5, :cond_5

    new-instance v5, Lf/c/a/j1;

    invoke-direct {v5, p1, v3}, Lf/c/a/j1;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    invoke-static {v5, v6, v7}, Lf/c/a/l1;->B(Ljava/util/concurrent/Callable;J)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    :cond_5
    iget-object v5, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    if-nez v5, :cond_6

    new-instance v5, Lf/c/a/k1;

    invoke-direct {v5, p1, v3}, Lf/c/a/k1;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    invoke-static {v5, v6, v7}, Lf/c/a/l1;->B(Ljava/util/concurrent/Callable;J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    iput-object v3, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    :cond_6
    iget-object v3, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    if-eqz v3, :cond_7

    const-string p1, "library"

    iput-object p1, p0, Lf/c/a/c0;->b:Ljava/lang/String;

    iput v2, p0, Lf/c/a/c0;->c:I

    return-void

    :cond_7
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    iget-object p1, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    if-nez p1, :cond_9

    iput-object v0, p0, Lf/c/a/c0;->a:Ljava/lang/String;

    :cond_9
    iget-object p1, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    if-nez p1, :cond_a

    iput-object v1, p0, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    :cond_a
    return-void
.end method
