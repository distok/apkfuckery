.class public Lf/c/a/y0;
.super Ljava/lang/Object;
.source "ResponseData.java"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lorg/json/JSONObject;

.field public g:Lf/c/a/h1;

.field public h:Lcom/adjust/sdk/AdjustAttribution;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lf/c/a/l;)Lf/c/a/y0;
    .locals 2

    invoke-virtual {p0}, Lf/c/a/l;->a()Lf/c/a/k;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 p0, 0x3

    if-eq v0, p0, :cond_1

    const/4 p0, 0x4

    if-eq v0, p0, :cond_0

    new-instance p0, Lf/c/a/y0;

    invoke-direct {p0}, Lf/c/a/y0;-><init>()V

    goto :goto_1

    :cond_0
    new-instance p0, Lf/c/a/a0;

    invoke-direct {p0}, Lf/c/a/a0;-><init>()V

    goto :goto_1

    :cond_1
    new-instance p0, Lf/c/a/d1;

    invoke-direct {p0}, Lf/c/a/d1;-><init>()V

    goto :goto_1

    :cond_2
    new-instance v0, Lf/c/a/d0;

    invoke-direct {v0, p0}, Lf/c/a/d0;-><init>(Lf/c/a/l;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lf/c/a/f1;

    invoke-direct {v0, p0}, Lf/c/a/f1;-><init>(Lf/c/a/l;)V

    :goto_0
    move-object p0, v0

    :goto_1
    return-object p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lf/c/a/y0;->d:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lf/c/a/y0;->e:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lf/c/a/y0;->f:Lorg/json/JSONObject;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "message:%s timestamp:%s json:%s"

    invoke-static {v1, v0}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
