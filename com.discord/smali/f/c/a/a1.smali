.class public Lf/c/a/a1;
.super Ljava/lang/Object;
.source "SdkClickHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/c/a/z0;


# direct methods
.method public constructor <init>(Lf/c/a/z0;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a1;->d:Lf/c/a/z0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    move-object/from16 v1, p0

    iget-object v0, v1, Lf/c/a/a1;->d:Lf/c/a/z0;

    iget-object v0, v0, Lf/c/a/z0;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/c/a/h0;

    new-instance v2, Lf/c/a/g1;

    invoke-interface {v0}, Lf/c/a/h0;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v2}, Lf/c/a/g1;->f()Lorg/json/JSONArray;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v6, v8, :cond_1

    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v8

    const/4 v9, -0x1

    const/4 v10, 0x2

    invoke-virtual {v8, v10, v9}, Lorg/json/JSONArray;->optInt(II)I

    move-result v9

    if-eqz v9, :cond_0

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    invoke-virtual {v8, v3, v7}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-wide/16 v12, -0x1

    invoke-virtual {v8, v4, v12, v13}, Lorg/json/JSONArray;->optLong(IJ)J

    move-result-wide v12

    invoke-virtual {v8, v10, v4}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;

    invoke-interface {v0}, Lf/c/a/h0;->g()Lf/c/a/m;

    move-result-object v14

    invoke-interface {v0}, Lf/c/a/h0;->i()Lf/c/a/n;

    move-result-object v15

    invoke-interface {v0}, Lf/c/a/h0;->f()Lf/c/a/c0;

    move-result-object v16

    invoke-interface {v0}, Lf/c/a/h0;->d()Lf/c/a/e1;

    move-result-object v17

    invoke-static/range {v11 .. v17}, Ls/a/b/b/a;->e(Ljava/lang/String;JLf/c/a/m;Lf/c/a/n;Lf/c/a/c0;Lf/c/a/e1;)Lf/c/a/l;

    move-result-object v7

    iget-object v8, v1, Lf/c/a/a1;->d:Lf/c/a/z0;

    invoke-virtual {v8, v7}, Lf/c/a/z0;->d(Lf/c/a/l;)V

    const/4 v7, 0x1

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    if-eqz v7, :cond_2

    invoke-virtual {v2, v5}, Lf/c/a/g1;->l(Lorg/json/JSONArray;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v2, v1, Lf/c/a/a1;->d:Lf/c/a/z0;

    iget-object v2, v2, Lf/c/a/z0;->b:Lf/c/a/j0;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    const-string v0, "Send saved raw referrers error (%s)"

    invoke-interface {v2, v0, v4}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_2
    return-void
.end method
