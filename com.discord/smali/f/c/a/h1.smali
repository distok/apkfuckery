.class public final enum Lf/c/a/h1;
.super Ljava/lang/Enum;
.source "TrackingState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/c/a/h1;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/c/a/h1;

.field public static final synthetic e:[Lf/c/a/h1;


# instance fields
.field private value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lf/c/a/h1;

    const-string v1, "OPTED_OUT"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lf/c/a/h1;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/c/a/h1;->d:Lf/c/a/h1;

    new-array v1, v3, [Lf/c/a/h1;

    aput-object v0, v1, v2

    sput-object v1, Lf/c/a/h1;->e:[Lf/c/a/h1;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/c/a/h1;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/c/a/h1;
    .locals 1

    const-class v0, Lf/c/a/h1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/c/a/h1;

    return-object p0
.end method

.method public static values()[Lf/c/a/h1;
    .locals 1

    sget-object v0, Lf/c/a/h1;->e:[Lf/c/a/h1;

    invoke-virtual {v0}, [Lf/c/a/h1;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/c/a/h1;

    return-object v0
.end method
