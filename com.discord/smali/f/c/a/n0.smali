.class public Lf/c/a/n0;
.super Ljava/lang/Object;
.source "InstallReferrer.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# instance fields
.field public a:I

.field public final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public c:Lf/c/a/j0;

.field public d:Ljava/lang/Object;

.field public e:Landroid/content/Context;

.field public f:Lf/c/a/n1/k;

.field public final g:Lf/c/a/p0;

.field public h:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/c/a/p0;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    const-string v1, "com.adjust.sdk.play.InstallReferrer"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Landroid/content/Context;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-class v4, Lf/c/a/p0;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, Lf/c/a/j0;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    aput-object p2, v2, v6

    aput-object v0, v2, v7

    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lf/c/a/n0;->h:Ljava/lang/Object;

    iput-object p1, p0, Lf/c/a/n0;->e:Landroid/content/Context;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lf/c/a/n0;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput v5, p0, Lf/c/a/n0;->a:I

    new-instance p1, Lf/c/a/n1/k;

    new-instance v0, Lf/c/a/n0$a;

    invoke-direct {v0, p0}, Lf/c/a/n0$a;-><init>(Lf/c/a/n0;)V

    const-string v1, "InstallReferrer"

    invoke-direct {p1, v0, v1}, Lf/c/a/n1/k;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object p1, p0, Lf/c/a/n0;->f:Lf/c/a/n1/k;

    iput-object p2, p0, Lf/c/a/n0;->g:Lf/c/a/p0;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    iget-object v0, p0, Lf/c/a/n0;->d:Ljava/lang/Object;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "endConnection"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v1, v4}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    const-string v3, "Install Referrer API connection closed"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-interface {v0, v3, v4}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v3, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    const-string v0, "closeReferrerClient error (%s) thrown by (%s)"

    invoke-interface {v3, v0, v4}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    iput-object v1, p0, Lf/c/a/n0;->d:Ljava/lang/Object;

    return-void
.end method

.method public final b()V
    .locals 7

    iget-object v0, p0, Lf/c/a/n0;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Should not try to read Install referrer"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/c/a/n0;->a()V

    return-void

    :cond_0
    iget v0, p0, Lf/c/a/n0;->a:I

    const/4 v2, 0x1

    add-int/2addr v0, v2

    const/4 v3, 0x2

    if-le v0, v3, :cond_1

    iget-object v0, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "Limit number of retry of %d for install referrer surpassed"

    invoke-interface {v0, v1, v2}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/c/a/n0;->f:Lf/c/a/n1/k;

    invoke-virtual {v0}, Lf/c/a/n1/k;->b()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    iget-object v0, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "Already waiting to retry to read install referrer in %d milliseconds"

    invoke-interface {v0, v1, v2}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_2
    iget v0, p0, Lf/c/a/n0;->a:I

    add-int/2addr v0, v2

    iput v0, p0, Lf/c/a/n0;->a:I

    iget-object v3, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v1

    const-string v0, "Retry number %d to connect to install referrer API"

    invoke-interface {v3, v0, v2}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/c/a/n0;->f:Lf/c/a/n1/k;

    const/16 v1, 0xbb8

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lf/c/a/n1/k;->c(J)V

    return-void
.end method

.method public c()V
    .locals 10

    iget-object v0, p0, Lf/c/a/n0;->h:Ljava/lang/Object;

    const-string/jumbo v1, "startConnection"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v5}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v5, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v4

    const-string v0, "Call to Play startConnection error: %s"

    invoke-interface {v5, v0, v6}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lf/c/a/n0;->a()V

    iget-object v0, p0, Lf/c/a/n0;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "Should not try to read Install referrer"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/c/a/n0;->e:Landroid/content/Context;

    if-nez v0, :cond_2

    return-void

    :cond_2
    const/4 v5, 0x2

    :try_start_1
    const-string v6, "com.android.installreferrer.api.InstallReferrerClient"

    const-string v7, "newBuilder"

    new-array v8, v3, [Ljava/lang/Class;

    const-class v9, Landroid/content/Context;

    aput-object v9, v8, v4

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v0, v9, v4

    invoke-static {v6, v7, v8, v9}, Ls/a/b/b/a;->I(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v6, "build"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v0, v6, v2, v7}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v6, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v3

    const-string v0, "createInstallReferrerClient error (%s) from (%s)"

    invoke-interface {v6, v0, v7}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v6, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v3

    const-string v0, "InstallReferrer not integrated in project (%s) thrown by (%s)"

    invoke-interface {v6, v0, v7}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    move-object v0, v2

    :goto_1
    iput-object v0, p0, Lf/c/a/n0;->d:Ljava/lang/Object;

    if-nez v0, :cond_3

    return-void

    :cond_3
    :try_start_2
    const-string v0, "f.e.b.a.b"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_2

    :catch_3
    move-exception v0

    iget-object v6, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v3

    const-string v0, "getInstallReferrerStateListenerClass error (%s) from (%s)"

    invoke-interface {v6, v0, v7}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v2

    :goto_2
    if-nez v0, :cond_4

    return-void

    :cond_4
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    new-array v7, v3, [Ljava/lang/Class;

    aput-object v0, v7, v4

    invoke-static {v6, v7, p0}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v6
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_4

    :catch_4
    iget-object v6, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v7, v4, [Ljava/lang/Object;

    const-string v8, "Null argument passed to InstallReferrer proxy"

    invoke-interface {v6, v8, v7}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :catch_5
    iget-object v6, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v7, v4, [Ljava/lang/Object;

    const-string v8, "InstallReferrer proxy violating parameter restrictions"

    invoke-interface {v6, v8, v7}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    move-object v6, v2

    :goto_4
    if-nez v6, :cond_5

    return-void

    :cond_5
    :try_start_4
    iget-object v7, p0, Lf/c/a/n0;->d:Ljava/lang/Object;

    new-array v8, v3, [Ljava/lang/Class;

    aput-object v0, v8, v4

    new-array v0, v3, [Ljava/lang/Object;

    aput-object v6, v0, v4

    invoke-static {v7, v1, v8, v0}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_6

    :catch_6
    move-exception v0

    iget-object v1, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const-string/jumbo v0, "startConnection error (%s) thrown by (%s)"

    invoke-interface {v1, v0, v2}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    :catch_7
    move-exception v0

    invoke-static {v0}, Lf/c/a/l1;->n(Ljava/lang/Exception;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Lf/c/a/l1;->n(Ljava/lang/Exception;)Z

    move-result v5

    if-nez v5, :cond_6

    goto :goto_5

    :cond_6
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    new-instance v5, Ljava/io/PrintWriter;

    invoke-direct {v5, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v5}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Caused by:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const-string v5, "\n"

    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_5
    aput-object v2, v3, v4

    const-string v0, "InstallReferrer encountered an InvocationTargetException %s"

    invoke-interface {v1, v0, v3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    :goto_6
    return-void
.end method

.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 p1, 0x0

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v0, "InstallReferrer invoke method null"

    invoke-interface {p2, v0, p3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1

    :cond_0
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_1

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v0, "InstallReferrer invoke method name null"

    invoke-interface {p2, v0, p3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1

    :cond_1
    iget-object v1, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    aput-object p2, v3, v0

    const-string v4, "InstallReferrer invoke method name: %s"

    invoke-interface {v1, v4, v3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez p3, :cond_2

    iget-object p3, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v1, v0, [Ljava/lang/Object;

    const-string v3, "InstallReferrer invoke args null"

    invoke-interface {p3, v3, v1}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array p3, v0, [Ljava/lang/Object;

    :cond_2
    array-length v1, p3

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, p3, v3

    iget-object v5, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v4, v6, v0

    const-string v4, "InstallReferrer invoke arg: %s"

    invoke-interface {v5, v4, v6}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    const-string v1, "onInstallReferrerSetupFinished"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    array-length p2, p3

    if-eq p2, v2, :cond_4

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v1, v2, [Ljava/lang/Object;

    array-length p3, p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v1, v0

    const-string p3, "InstallReferrer invoke onInstallReferrerSetupFinished args lenght not 1: %d"

    invoke-interface {p2, p3, v1}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1

    :cond_4
    aget-object p2, p3, v0

    instance-of p3, p2, Ljava/lang/Integer;

    if-nez p3, :cond_5

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v0, "InstallReferrer invoke onInstallReferrerSetupFinished arg not int"

    invoke-interface {p2, v0, p3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1

    :cond_5
    check-cast p2, Ljava/lang/Integer;

    if-nez p2, :cond_6

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v0, "InstallReferrer invoke onInstallReferrerSetupFinished responseCode arg is null"

    invoke-interface {p2, v0, p3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1

    :cond_6
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    const/4 p3, -0x1

    if-eq p2, p3, :cond_f

    const/4 p3, 0x3

    const/4 v1, 0x2

    if-eqz p2, :cond_a

    if-eq p2, v2, :cond_9

    if-eq p2, v1, :cond_8

    if-eq p2, p3, :cond_7

    iget-object p3, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v1, v0

    const-string p2, "Unexpected response code of install referrer response: %d. Closing connection"

    invoke-interface {p3, p2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_8

    :cond_7
    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v1, "Install Referrer API general errors caused by incorrect usage. Retrying..."

    invoke-interface {p2, v1, p3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_9

    :cond_8
    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v1, "Install Referrer API not supported by the installed Play Store app. Closing connection"

    invoke-interface {p2, v1, p3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_8

    :cond_9
    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v1, "Could not initiate connection to the Install Referrer service. Retrying..."

    invoke-interface {p2, v1, p3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_9

    :cond_a
    :try_start_0
    iget-object p2, p0, Lf/c/a/n0;->d:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    const-string v3, "getInstallReferrer"

    if-nez p2, :cond_b

    goto :goto_1

    :cond_b
    :try_start_1
    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p2, v3, p1, v4}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p2

    :try_start_2
    iget-object v4, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v5, v2

    const-string p2, "getInstallReferrer error (%s) thrown by (%s)"

    invoke-interface {v4, p2, v5}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    :goto_1
    move-object p2, p1

    :goto_2
    if-nez p2, :cond_c

    goto :goto_3

    :cond_c
    :try_start_3
    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p2, v3, p1, v4}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v7, v3

    goto :goto_4

    :catch_1
    move-exception v3

    :try_start_4
    iget-object v4, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    const-string v3, "getStringInstallReferrer error (%s) thrown by (%s)"

    invoke-interface {v4, v3, v5}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :goto_3
    move-object v7, p1

    :goto_4
    const-wide/16 v3, -0x1

    if-nez p2, :cond_d

    goto :goto_5

    :cond_d
    :try_start_5
    const-string v5, "getReferrerClickTimestampSeconds"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {p2, v5, p1, v6}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    move-wide v8, v5

    goto :goto_6

    :catch_2
    move-exception v5

    :try_start_6
    iget-object v6, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v8, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v0

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v2

    const-string v5, "getReferrerClickTimestampSeconds error (%s) thrown by (%s)"

    invoke-interface {v6, v5, v8}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    :goto_5
    move-wide v8, v3

    :goto_6
    if-nez p2, :cond_e

    goto :goto_7

    :cond_e
    :try_start_7
    const-string v5, "getInstallBeginTimestampSeconds"

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {p2, v5, p1, v6}, Ls/a/b/b/a;->H(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_7

    :catch_3
    move-exception p2

    :try_start_8
    iget-object v5, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v6, v2

    const-string p2, "getInstallBeginTimestampSeconds error (%s) thrown by (%s)"

    invoke-interface {v5, p2, v6}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_7
    move-wide v10, v3

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    const-string v3, "installReferrer: %s, clickTime: %d, installBeginTime: %d"

    new-array p3, p3, [Ljava/lang/Object;

    aput-object v7, p3, v0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, p3, v2

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, p3, v1

    invoke-interface {p2, v3, p3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    const-string p3, "Install Referrer read successfully. Closing connection"

    new-array v1, v0, [Ljava/lang/Object;

    invoke-interface {p2, p3, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v6, p0, Lf/c/a/n0;->g:Lf/c/a/p0;

    invoke-interface/range {v6 .. v11}, Lf/c/a/p0;->a(Ljava/lang/String;JJ)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :goto_8
    const/4 v2, 0x0

    goto :goto_9

    :catch_4
    move-exception p2

    iget-object p3, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, v0

    const-string p2, "Couldn\'t get install referrer from client (%s). Retrying..."

    invoke-interface {p3, p2, v1}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_9

    :cond_f
    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v1, "Play Store service is not connected now. Retrying..."

    invoke-interface {p2, v1, p3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_9
    if-eqz v2, :cond_10

    invoke-virtual {p0}, Lf/c/a/n0;->b()V

    goto :goto_a

    :cond_10
    iget-object p2, p0, Lf/c/a/n0;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {p0}, Lf/c/a/n0;->a()V

    goto :goto_a

    :cond_11
    const-string p3, "onInstallReferrerServiceDisconnected"

    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_12

    iget-object p2, p0, Lf/c/a/n0;->c:Lf/c/a/j0;

    new-array p3, v0, [Ljava/lang/Object;

    const-string v0, "Connection to install referrer service was lost. Retrying ..."

    invoke-interface {p2, v0, p3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/c/a/n0;->b()V

    :cond_12
    :goto_a
    return-object p1
.end method
