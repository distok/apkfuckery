.class public Lf/c/a/u0;
.super Ljava/lang/Object;
.source "PackageBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/c/a/u0$a;
    }
.end annotation


# static fields
.field public static p:Lf/c/a/j0;


# instance fields
.field public a:J

.field public b:Lf/c/a/c0;

.field public c:Lf/c/a/n;

.field public d:Lf/c/a/u0$a;

.field public e:Lf/c/a/e1;

.field public f:J

.field public g:J

.field public h:J

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Lcom/adjust/sdk/AdjustAttribution;

.field public o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/c/a/p;->a()Lf/c/a/j0;

    move-result-object v0

    sput-object v0, Lf/c/a/u0;->p:Lf/c/a/j0;

    return-void
.end method

.method public constructor <init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/c/a/u0;->f:J

    iput-wide v0, p0, Lf/c/a/u0;->g:J

    iput-wide v0, p0, Lf/c/a/u0;->h:J

    iput-wide p5, p0, Lf/c/a/u0;->a:J

    iput-object p2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iput-object p1, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    new-instance p1, Lf/c/a/u0$a;

    invoke-direct {p1, p0, p3}, Lf/c/a/u0$a;-><init>(Lf/c/a/u0;Lf/c/a/m;)V

    iput-object p1, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iput-object p4, p0, Lf/c/a/u0;->e:Lf/c/a/e1;

    return-void
.end method

.method public static a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    int-to-long v0, p2

    invoke-static {p0, p1, v0, v1}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    return-void
.end method

.method public static b(Ljava/util/Map;Ljava/lang/String;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p2, p3}, Ljava/util/Date;-><init>(J)V

    sget-object p2, Lf/c/a/l1;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static c(Ljava/util/Map;Ljava/lang/String;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/Date;

    const-wide/16 v1, 0x3e8

    mul-long p2, p2, v1

    invoke-direct {v0, p2, p3}, Ljava/util/Date;-><init>(J)V

    sget-object p2, Lf/c/a/l1;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static d(Ljava/util/Map;Ljava/lang/String;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-gez v2, :cond_0

    return-void

    :cond_0
    const-wide/16 v0, 0x1f4

    add-long/2addr p2, v0

    const-wide/16 v0, 0x3e8

    div-long/2addr p2, v0

    invoke-static {p0, p1, p2, p3}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    return-void
.end method

.method public static e(Ljava/util/Map;Ljava/lang/String;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-gez v2, :cond_0

    return-void

    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public h(Ljava/lang/String;)Lf/c/a/l;
    .locals 6

    iget-object v0, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v2, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v3, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v2, v3}, Ls/a/b/b/a;->y(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_0
    iget-object v2, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v3, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v2, v3}, Ls/a/b/b/a;->z(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_1
    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v2, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v2, v2, Lf/c/a/u0$a;->g:Ljava/lang/String;

    const-string v3, "android_uuid"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const-string/jumbo v3, "tracking_enabled"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->a:Ljava/lang/String;

    const-string v3, "gps_adid"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->b:Ljava/lang/String;

    const-string v3, "gps_adid_src"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget v2, v2, Lf/c/a/c0;->c:I

    int-to-long v2, v2

    const-string v4, "gps_adid_attempt"

    invoke-static {v1, v4, v2, v3}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {p0, v1}, Lf/c/a/u0;->j(Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lf/c/a/u0;->p:Lf/c/a/j0;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Google Advertising ID not detected, fallback to non Google Play identifiers will take place"

    invoke-interface {v2, v4, v3}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lf/c/a/c0;->a(Landroid/content/Context;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->f:Ljava/lang/String;

    const-string v3, "mac_sha1"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->g:Ljava/lang/String;

    const-string v3, "mac_md5"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->h:Ljava/lang/String;

    const-string v3, "android_id"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lf/c/a/u0;->n:Lcom/adjust/sdk/AdjustAttribution;

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/adjust/sdk/AdjustAttribution;->trackerName:Ljava/lang/String;

    const-string/jumbo v3, "tracker"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->n:Lcom/adjust/sdk/AdjustAttribution;

    iget-object v2, v2, Lcom/adjust/sdk/AdjustAttribution;->campaign:Ljava/lang/String;

    const-string v3, "campaign"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->n:Lcom/adjust/sdk/AdjustAttribution;

    iget-object v2, v2, Lcom/adjust/sdk/AdjustAttribution;->adgroup:Ljava/lang/String;

    const-string v3, "adgroup"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->n:Lcom/adjust/sdk/AdjustAttribution;

    iget-object v2, v2, Lcom/adjust/sdk/AdjustAttribution;->creative:Ljava/lang/String;

    const-string v3, "creative"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->r:Ljava/lang/String;

    const-string v3, "api_level"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "app_secret"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->b:Ljava/lang/String;

    const-string v3, "app_token"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v2, v2, Lf/c/a/c0;->l:Ljava/lang/String;

    const-string v3, "app_version"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v3, "attribution_deeplink"

    invoke-static {v1, v3, v2}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v3, p0, Lf/c/a/u0;->e:Lf/c/a/e1;

    iget-object v3, v3, Lf/c/a/e1;->a:Ljava/util/Map;

    const-string v4, "callback_params"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-wide v3, p0, Lf/c/a/u0;->g:J

    const-string v5, "click_time"

    invoke-static {v1, v5, v3, v4}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    iget-wide v3, p0, Lf/c/a/u0;->f:J

    invoke-static {v1, v5, v3, v4}, Lf/c/a/u0;->c(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v3}, Lf/c/a/l1;->d(Landroid/content/Context;)I

    move-result v3

    int-to-long v3, v3

    const-string v5, "connectivity_type"

    invoke-static {v1, v5, v3, v4}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->t:Ljava/lang/String;

    const-string v4, "country"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->A:Ljava/lang/String;

    const-string v4, "cpu_type"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v3, p0, Lf/c/a/u0;->a:J

    const-string v5, "created_at"

    invoke-static {v1, v5, v3, v4}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v3, p0, Lf/c/a/u0;->j:Ljava/lang/String;

    const-string v4, "deeplink"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "device_known"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->o:Ljava/lang/String;

    const-string v4, "device_manufacturer"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->n:Ljava/lang/String;

    const-string v4, "device_name"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->m:Ljava/lang/String;

    const-string v4, "device_type"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->y:Ljava/lang/String;

    const-string v4, "display_height"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->x:Ljava/lang/String;

    const-string v4, "display_width"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->c:Ljava/lang/String;

    const-string v4, "environment"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v4, "event_buffering_enabled"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "external_device_id"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v3, v3, Lf/c/a/c0;->i:Ljava/lang/String;

    const-string v4, "fb_id"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lf/c/a/l1;->e(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "fire_adid"

    invoke-static {v1, v4, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lf/c/a/l1;->f(Landroid/content/ContentResolver;)Ljava/lang/Boolean;

    move-result-object v0

    const-string v3, "fire_tracking_enabled"

    invoke-static {v1, v3, v0}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->z:Ljava/lang/String;

    const-string v3, "hardware_name"

    invoke-static {v1, v3, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v3, p0, Lf/c/a/u0;->h:J

    const-string v0, "install_begin_time"

    invoke-static {v1, v0, v3, v4}, Lf/c/a/u0;->c(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->C:Ljava/lang/String;

    const-string v3, "installed_at"

    invoke-static {v1, v3, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->s:Ljava/lang/String;

    const-string v3, "language"

    invoke-static {v1, v3, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v3, v0, Lf/c/a/u0$a;->e:J

    const-string v0, "last_interval"

    invoke-static {v1, v0, v3, v4}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v0, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/c/a/l1;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "mcc"

    invoke-static {v1, v3, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/c/a/l1;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "mnc"

    invoke-static {v1, v3, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "needs_response_details"

    invoke-static {v1, v0, v2}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/c/a/l1;->j(Landroid/content/Context;)I

    move-result v0

    int-to-long v2, v0

    const-string v0, "network_type"

    invoke-static {v1, v0, v2, v3}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->B:Ljava/lang/String;

    const-string v2, "os_build"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->p:Ljava/lang/String;

    const-string v2, "os_name"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->q:Ljava/lang/String;

    const-string v2, "os_version"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->k:Ljava/lang/String;

    const-string v2, "package_name"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->o:Ljava/util/Map;

    const-string v2, "params"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/c/a/u0;->e:Lf/c/a/e1;

    iget-object v0, v0, Lf/c/a/e1;->b:Ljava/util/Map;

    const-string v2, "partner_params"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->f(Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v0, v0, Lf/c/a/u0$a;->h:Ljava/lang/String;

    const-string v2, "push_token"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->l:Ljava/lang/String;

    const-string v2, "raw_referrer"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->k:Ljava/lang/String;

    const-string v2, "referrer"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->m:Ljava/lang/String;

    const-string v2, "referrer_api"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->i:Ljava/lang/String;

    const-string v2, "reftag"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->w:Ljava/lang/String;

    const-string v2, "screen_density"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->v:Ljava/lang/String;

    const-string v2, "screen_format"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v0, v0, Lf/c/a/c0;->u:Ljava/lang/String;

    const-string v2, "screen_size"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v2, "secret_id"

    invoke-static {v1, v2, v0}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget v0, v0, Lf/c/a/u0$a;->b:I

    int-to-long v2, v0

    const-string v0, "session_count"

    invoke-static {v1, v0, v2, v3}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v0, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v2, v0, Lf/c/a/u0$a;->f:J

    const-string v0, "session_length"

    invoke-static {v1, v0, v2, v3}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    const-string v0, "source"

    invoke-static {v1, v0, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget p1, p1, Lf/c/a/u0$a;->c:I

    int-to-long v2, p1

    const-string/jumbo p1, "subsession_count"

    invoke-static {v1, p1, v2, v3}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, p0, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-wide v2, p1, Lf/c/a/u0$a;->d:J

    const-string/jumbo p1, "time_spent"

    invoke-static {v1, p1, v2, v3}, Lf/c/a/u0;->d(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object p1, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->D:Ljava/lang/String;

    const-string/jumbo v0, "updated_at"

    invoke-static {v1, v0, p1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lf/c/a/u0;->i(Ljava/util/Map;)V

    sget-object p1, Lf/c/a/k;->g:Lf/c/a/k;

    invoke-virtual {p0, p1}, Lf/c/a/u0;->k(Lf/c/a/k;)Lf/c/a/l;

    move-result-object v0

    const-string v2, "/sdk_click"

    invoke-virtual {v0, v2}, Lf/c/a/l;->s(Ljava/lang/String;)V

    const-string v2, ""

    invoke-virtual {v0, v2}, Lf/c/a/l;->t(Ljava/lang/String;)V

    iget-wide v2, p0, Lf/c/a/u0;->g:J

    invoke-virtual {v0, v2, v3}, Lf/c/a/l;->n(J)V

    iget-wide v2, p0, Lf/c/a/u0;->f:J

    invoke-virtual {v0, v2, v3}, Lf/c/a/l;->o(J)V

    iget-wide v2, p0, Lf/c/a/u0;->h:J

    invoke-virtual {v0, v2, v3}, Lf/c/a/l;->q(J)V

    invoke-virtual {p1}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lf/c/a/l;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v4, v3, Lf/c/a/n;->a:Landroid/content/Context;

    iget-object v3, v3, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v1, p1, v2, v4, v3}, Lf/c/a/t;->c(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lf/c/a/j0;)V

    invoke-virtual {v0, v1}, Lf/c/a/l;->r(Ljava/util/Map;)V

    return-object v0
.end method

.method public final i(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "mac_sha1"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mac_md5"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android_id"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "gps_adid"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "oaid"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "imei"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "meid"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "device_id"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "imeis"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "meids"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "device_ids"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lf/c/a/u0;->p:Lf/c/a/j0;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Missing device id\'s. Please check if Proguard is correctly set with Adjust SDK"

    invoke-interface {p1, v1, v0}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final j(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "gps_adid"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final k(Lf/c/a/k;)Lf/c/a/l;
    .locals 1

    new-instance v0, Lf/c/a/l;

    invoke-direct {v0, p1}, Lf/c/a/l;-><init>(Lf/c/a/k;)V

    iget-object p1, p0, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object p1, p1, Lf/c/a/c0;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lf/c/a/l;->p(Ljava/lang/String;)V

    return-object v0
.end method
