.class public Lf/c/a/u0$a;
.super Ljava/lang/Object;
.source "PackageBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/c/a/u0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:J

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lf/c/a/u0;Lf/c/a/m;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, -0x1

    iput p1, p0, Lf/c/a/u0$a;->a:I

    iput p1, p0, Lf/c/a/u0$a;->b:I

    iput p1, p0, Lf/c/a/u0$a;->c:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/c/a/u0$a;->d:J

    iput-wide v0, p0, Lf/c/a/u0$a;->e:J

    iput-wide v0, p0, Lf/c/a/u0$a;->f:J

    const/4 p1, 0x0

    iput-object p1, p0, Lf/c/a/u0$a;->g:Ljava/lang/String;

    iput-object p1, p0, Lf/c/a/u0$a;->h:Ljava/lang/String;

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget p1, p2, Lf/c/a/m;->eventCount:I

    iput p1, p0, Lf/c/a/u0$a;->a:I

    iget p1, p2, Lf/c/a/m;->sessionCount:I

    iput p1, p0, Lf/c/a/u0$a;->b:I

    iget p1, p2, Lf/c/a/m;->subsessionCount:I

    iput p1, p0, Lf/c/a/u0$a;->c:I

    iget-wide v0, p2, Lf/c/a/m;->timeSpent:J

    iput-wide v0, p0, Lf/c/a/u0$a;->d:J

    iget-wide v0, p2, Lf/c/a/m;->lastInterval:J

    iput-wide v0, p0, Lf/c/a/u0$a;->e:J

    iget-wide v0, p2, Lf/c/a/m;->sessionLength:J

    iput-wide v0, p0, Lf/c/a/u0$a;->f:J

    iget-object p1, p2, Lf/c/a/m;->uuid:Ljava/lang/String;

    iput-object p1, p0, Lf/c/a/u0$a;->g:Ljava/lang/String;

    iget-object p1, p2, Lf/c/a/m;->pushToken:Ljava/lang/String;

    iput-object p1, p0, Lf/c/a/u0$a;->h:Ljava/lang/String;

    return-void
.end method
