.class public Lf/c/a/z;
.super Ljava/lang/Object;
.source "AttributionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/c/a/u;


# direct methods
.method public constructor <init>(Lf/c/a/u;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/z;->d:Lf/c/a/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    iget-object v0, p0, Lf/c/a/z;->d:Lf/c/a/u;

    iget-object v1, v0, Lf/c/a/u;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/c/a/h0;

    invoke-interface {v1}, Lf/c/a/h0;->g()Lf/c/a/m;

    move-result-object v1

    iget-boolean v1, v1, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :cond_0
    iget-boolean v1, v0, Lf/c/a/u;->a:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    iget-object v0, v0, Lf/c/a/u;->e:Lf/c/a/j0;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "Attribution handler is paused"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v1, v0, Lf/c/a/u;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/c/a/h0;

    new-instance v10, Lf/c/a/u0;

    invoke-interface {v1}, Lf/c/a/h0;->i()Lf/c/a/n;

    move-result-object v11

    invoke-interface {v1}, Lf/c/a/h0;->f()Lf/c/a/c0;

    move-result-object v5

    invoke-interface {v1}, Lf/c/a/h0;->g()Lf/c/a/m;

    move-result-object v6

    invoke-interface {v1}, Lf/c/a/h0;->d()Lf/c/a/e1;

    move-result-object v7

    move-object v3, v10

    move-object v4, v11

    invoke-direct/range {v3 .. v9}, Lf/c/a/u0;-><init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V

    iget-object v1, v0, Lf/c/a/u;->d:Ljava/lang/String;

    iget-object v3, v11, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v5, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v6, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v5, v6}, Ls/a/b/b/a;->y(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_2
    iget-object v5, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v6, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v5, v6}, Ls/a/b/b/a;->z(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_3
    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v6, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v5, v10, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v5, v5, Lf/c/a/u0$a;->g:Ljava/lang/String;

    const-string v6, "android_uuid"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const-string/jumbo v6, "tracking_enabled"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->a:Ljava/lang/String;

    const-string v6, "gps_adid"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->b:Ljava/lang/String;

    const-string v6, "gps_adid_src"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget v5, v5, Lf/c/a/c0;->c:I

    int-to-long v5, v5

    const-string v7, "gps_adid_attempt"

    invoke-static {v4, v7, v5, v6}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {v10, v4}, Lf/c/a/u0;->j(Ljava/util/Map;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v5, Lf/c/a/u0;->p:Lf/c/a/j0;

    new-array v6, v2, [Ljava/lang/Object;

    const-string v7, "Google Advertising ID not detected, fallback to non Google Play identifiers will take place"

    invoke-interface {v5, v7, v6}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v6, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lf/c/a/c0;->a(Landroid/content/Context;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->f:Ljava/lang/String;

    const-string v6, "mac_sha1"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->g:Ljava/lang/String;

    const-string v6, "mac_md5"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->h:Ljava/lang/String;

    const-string v6, "android_id"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v5, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->r:Ljava/lang/String;

    const-string v6, "api_level"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "app_secret"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->b:Ljava/lang/String;

    const-string v7, "app_token"

    invoke-static {v4, v7, v6}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v6, v6, Lf/c/a/c0;->l:Ljava/lang/String;

    const-string v7, "app_version"

    invoke-static {v4, v7, v6}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v7, "attribution_deeplink"

    invoke-static {v4, v7, v6}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-wide v7, v10, Lf/c/a/u0;->a:J

    const-string v9, "created_at"

    invoke-static {v4, v9, v7, v8}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v7, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "device_known"

    invoke-static {v4, v7, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v7, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v7, v7, Lf/c/a/c0;->n:Ljava/lang/String;

    const-string v8, "device_name"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v7, v7, Lf/c/a/c0;->m:Ljava/lang/String;

    const-string v8, "device_type"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v7, v7, Lf/c/a/n;->c:Ljava/lang/String;

    const-string v8, "environment"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v7, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v8, "event_buffering_enabled"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v7, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "external_device_id"

    invoke-static {v4, v7, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lf/c/a/l1;->e(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "fire_adid"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lf/c/a/l1;->f(Landroid/content/ContentResolver;)Ljava/lang/Boolean;

    move-result-object v3

    const-string v7, "fire_tracking_enabled"

    invoke-static {v4, v7, v3}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v3, "initiated_by"

    invoke-static {v4, v3, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "needs_response_details"

    invoke-static {v4, v1, v6}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v1, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->p:Ljava/lang/String;

    const-string v3, "os_name"

    invoke-static {v4, v3, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->q:Ljava/lang/String;

    const-string v3, "os_version"

    invoke-static {v4, v3, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v10, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->k:Ljava/lang/String;

    const-string v3, "package_name"

    invoke-static {v4, v3, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v10, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v1, v1, Lf/c/a/u0$a;->h:Ljava/lang/String;

    const-string v3, "push_token"

    invoke-static {v4, v3, v1}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "secret_id"

    invoke-static {v4, v1, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Lf/c/a/u0;->i(Ljava/util/Map;)V

    sget-object v1, Lf/c/a/k;->h:Lf/c/a/k;

    invoke-virtual {v10, v1}, Lf/c/a/u0;->k(Lf/c/a/k;)Lf/c/a/l;

    move-result-object v3

    const-string v6, "attribution"

    invoke-virtual {v3, v6}, Lf/c/a/l;->s(Ljava/lang/String;)V

    const-string v6, ""

    invoke-virtual {v3, v6}, Lf/c/a/l;->t(Ljava/lang/String;)V

    invoke-virtual {v1}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lf/c/a/l;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v10, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v8, v7, Lf/c/a/n;->a:Landroid/content/Context;

    iget-object v7, v7, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v4, v1, v6, v8, v7}, Lf/c/a/t;->c(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lf/c/a/j0;)V

    invoke-virtual {v3, v4}, Lf/c/a/l;->r(Ljava/util/Map;)V

    iput-object v5, v0, Lf/c/a/u;->d:Ljava/lang/String;

    iget-object v1, v0, Lf/c/a/u;->e:Lf/c/a/j0;

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {v3}, Lf/c/a/l;->f()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const-string v6, "%s"

    invoke-interface {v1, v6, v5}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    iget-object v1, v0, Lf/c/a/u;->b:Ljava/lang/String;

    invoke-static {v3, v1}, Ls/a/b/b/a;->m(Lf/c/a/l;Ljava/lang/String;)Lf/c/a/y0;

    move-result-object v1

    instance-of v3, v1, Lf/c/a/a0;

    if-nez v3, :cond_5

    goto :goto_0

    :cond_5
    iget-object v3, v1, Lf/c/a/y0;->g:Lf/c/a/h1;

    sget-object v5, Lf/c/a/h1;->d:Lf/c/a/h1;

    if-ne v3, v5, :cond_6

    iget-object v1, v0, Lf/c/a/u;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/c/a/h0;

    invoke-interface {v1}, Lf/c/a/h0;->n()V

    goto :goto_0

    :cond_6
    check-cast v1, Lf/c/a/a0;

    iget-object v3, v0, Lf/c/a/u;->g:Lf/c/a/n1/h;

    new-instance v5, Lf/c/a/y;

    invoke-direct {v5, v0, v1}, Lf/c/a/y;-><init>(Lf/c/a/u;Lf/c/a/a0;)V

    check-cast v3, Lf/c/a/n1/c;

    invoke-virtual {v3, v5}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v0, v0, Lf/c/a/u;->e:Lf/c/a/j0;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    const-string v1, "Failed to get attribution (%s)"

    invoke-interface {v0, v1, v3}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
