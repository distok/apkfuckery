.class public Lf/c/a/v0$d;
.super Ljava/lang/Object;
.source "PackageHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/v0;->f(Lf/c/a/y0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/v0;


# direct methods
.method public constructor <init>(Lf/c/a/v0;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/v0$d;->d:Lf/c/a/v0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lf/c/a/v0$d;->d:Lf/c/a/v0;

    iget-object v1, v0, Lf/c/a/v0;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lf/c/a/v0;->d:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/c/a/v0;->m()V

    iget-object v1, v0, Lf/c/a/v0;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, v0, Lf/c/a/v0;->h:Lf/c/a/j0;

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "Package handler can send"

    invoke-interface {v1, v3, v2}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lf/c/a/v0;->l()V

    :goto_0
    return-void
.end method
