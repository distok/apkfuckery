.class public Lf/c/a/a$f;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;->C(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Z

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;ZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$f;->f:Lf/c/a/a;

    iput-boolean p2, p0, Lf/c/a/a$f;->d:Z

    iput-object p3, p0, Lf/c/a/a$f;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    iget-boolean v0, p0, Lf/c/a/a$f;->d:Z

    if-nez v0, :cond_0

    new-instance v0, Lf/c/a/g1;

    iget-object v1, p0, Lf/c/a/a$f;->f:Lf/c/a/a;

    iget-object v1, v1, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lf/c/a/a$f;->e:Ljava/lang/String;

    monitor-enter v0

    :try_start_0
    const-string v2, "push_token"

    invoke-virtual {v0, v2, v1}, Lf/c/a/g1;->m(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/c/a/a$f;->f:Lf/c/a/a;

    iget-object v1, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v1, v1, Lf/c/a/a$n;->g:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Lf/c/a/a$f;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lf/c/a/a;->q()Z

    move-result v2

    if-nez v2, :cond_2

    goto/16 :goto_1

    :cond_2
    invoke-virtual {v0}, Lf/c/a/a;->u()Z

    move-result v2

    if-nez v2, :cond_3

    goto/16 :goto_1

    :cond_3
    iget-object v2, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v3, v2, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v3, :cond_4

    goto/16 :goto_1

    :cond_4
    if-nez v1, :cond_5

    goto/16 :goto_1

    :cond_5
    iget-object v2, v2, Lf/c/a/m;->pushToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    goto/16 :goto_1

    :cond_6
    iget-object v2, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iput-object v1, v2, Lf/c/a/m;->pushToken:Ljava/lang/String;

    invoke-virtual {v0}, Lf/c/a/a;->O()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    new-instance v1, Lf/c/a/u0;

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v5, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v6, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-object v7, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    move-object v3, v1

    move-object v4, v2

    invoke-direct/range {v3 .. v9}, Lf/c/a/u0;-><init>(Lf/c/a/n;Lf/c/a/c0;Lf/c/a/m;Lf/c/a/e1;J)V

    const-string v3, "push"

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v5, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v6, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v5, v6}, Ls/a/b/b/a;->y(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_7
    iget-object v5, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v5, v5, Lf/c/a/n;->a:Landroid/content/Context;

    sget-object v6, Lf/c/a/u0;->p:Lf/c/a/j0;

    invoke-static {v5, v6}, Ls/a/b/b/a;->z(Landroid/content/Context;Lf/c/a/j0;)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_8
    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v6, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v5, v1, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v5, v5, Lf/c/a/u0$a;->g:Ljava/lang/String;

    const-string v6, "android_uuid"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->d:Ljava/lang/Boolean;

    const-string/jumbo v6, "tracking_enabled"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->a:Ljava/lang/String;

    const-string v6, "gps_adid"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->b:Ljava/lang/String;

    const-string v6, "gps_adid_src"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget v5, v5, Lf/c/a/c0;->c:I

    int-to-long v5, v5

    const-string v7, "gps_adid_attempt"

    invoke-static {v4, v7, v5, v6}, Lf/c/a/u0;->e(Ljava/util/Map;Ljava/lang/String;J)V

    invoke-virtual {v1, v4}, Lf/c/a/u0;->j(Ljava/util/Map;)Z

    move-result v5

    if-nez v5, :cond_9

    sget-object v5, Lf/c/a/u0;->p:Lf/c/a/j0;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "Google Advertising ID not detected, fallback to non Google Play identifiers will take place"

    invoke-interface {v5, v7, v6}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v6, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lf/c/a/c0;->a(Landroid/content/Context;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->f:Ljava/lang/String;

    const-string v6, "mac_sha1"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->g:Ljava/lang/String;

    const-string v6, "mac_md5"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, Lf/c/a/u0;->b:Lf/c/a/c0;

    iget-object v5, v5, Lf/c/a/c0;->h:Ljava/lang/String;

    const-string v6, "android_id"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v5, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "app_secret"

    invoke-static {v4, v6, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v6, Lf/c/a/n;->b:Ljava/lang/String;

    const-string v7, "app_token"

    invoke-static {v4, v7, v6}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v7, "attribution_deeplink"

    invoke-static {v4, v7, v6}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-wide v7, v1, Lf/c/a/u0;->a:J

    const-string v9, "created_at"

    invoke-static {v4, v9, v7, v8}, Lf/c/a/u0;->b(Ljava/util/Map;Ljava/lang/String;J)V

    iget-object v7, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "device_known"

    invoke-static {v4, v7, v5}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v7, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v7, v7, Lf/c/a/n;->c:Ljava/lang/String;

    const-string v8, "environment"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v7, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v8, "event_buffering_enabled"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v7, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "external_device_id"

    invoke-static {v4, v7, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lf/c/a/l1;->e(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "fire_adid"

    invoke-static {v4, v8, v7}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lf/c/a/l1;->f(Landroid/content/ContentResolver;)Ljava/lang/Boolean;

    move-result-object v2

    const-string v7, "fire_tracking_enabled"

    invoke-static {v4, v7, v2}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "needs_response_details"

    invoke-static {v4, v2, v6}, Lf/c/a/u0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, v1, Lf/c/a/u0;->d:Lf/c/a/u0$a;

    iget-object v2, v2, Lf/c/a/u0$a;->h:Ljava/lang/String;

    const-string v6, "push_token"

    invoke-static {v4, v6, v2}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "secret_id"

    invoke-static {v4, v2, v5}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "source"

    invoke-static {v4, v2, v3}, Lf/c/a/u0;->g(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lf/c/a/u0;->i(Ljava/util/Map;)V

    sget-object v2, Lf/c/a/k;->k:Lf/c/a/k;

    invoke-virtual {v1, v2}, Lf/c/a/u0;->k(Lf/c/a/k;)Lf/c/a/l;

    move-result-object v3

    const-string v5, "/sdk_info"

    invoke-virtual {v3, v5}, Lf/c/a/l;->s(Ljava/lang/String;)V

    const-string v5, ""

    invoke-virtual {v3, v5}, Lf/c/a/l;->t(Ljava/lang/String;)V

    invoke-virtual {v2}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lf/c/a/l;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v1, Lf/c/a/u0;->c:Lf/c/a/n;

    iget-object v6, v1, Lf/c/a/n;->a:Landroid/content/Context;

    iget-object v1, v1, Lf/c/a/n;->f:Lf/c/a/j0;

    invoke-static {v4, v2, v5, v6, v1}, Lf/c/a/t;->c(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lf/c/a/j0;)V

    invoke-virtual {v3, v4}, Lf/c/a/l;->r(Ljava/util/Map;)V

    iget-object v1, v0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v1, v3}, Lf/c/a/k0;->g(Lf/c/a/l;)V

    new-instance v1, Lf/c/a/g1;

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    monitor-enter v1

    :try_start_1
    const-string v2, "push_token"

    invoke-virtual {v1, v2}, Lf/c/a/g1;->i(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v1

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-interface {v0}, Lf/c/a/k0;->e()V

    :goto_1
    return-void

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
