.class public Lf/c/a/a$e;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/c/a/a;-><init>(Lf/c/a/n;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/a$e;->d:Lf/c/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    iget-object v0, p0, Lf/c/a/a$e;->d:Lf/c/a/a;

    const-string v1, "Failed to read %s file (%s)"

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/32 v2, 0x1b7740

    sput-wide v2, Lf/c/a/a;->r:J

    const-wide/16 v2, 0x3e8

    sput-wide v2, Lf/c/a/a;->s:J

    const-wide/32 v2, 0xea60

    sput-wide v2, Lf/c/a/a;->o:J

    sput-wide v2, Lf/c/a/a;->p:J

    sput-wide v2, Lf/c/a/a;->q:J

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    const-string v3, "Attribution"

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    :try_start_0
    const-string v8, "AdjustAttribution"

    const-class v9, Lcom/adjust/sdk/AdjustAttribution;

    invoke-static {v2, v8, v3, v9}, Lf/c/a/l1;->x(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/adjust/sdk/AdjustAttribution;

    iput-object v2, v0, Lf/c/a/a;->i:Lcom/adjust/sdk/AdjustAttribution;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    iget-object v8, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v9, v5, [Ljava/lang/Object;

    aput-object v3, v9, v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v7

    invoke-interface {v8, v1, v9}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v6, v0, Lf/c/a/a;->i:Lcom/adjust/sdk/AdjustAttribution;

    :goto_0
    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    const-string v3, "Activity state"

    :try_start_1
    const-string v8, "AdjustIoActivityState"

    const-class v9, Lf/c/a/m;

    invoke-static {v2, v8, v3, v9}, Lf/c/a/l1;->x(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/c/a/m;

    iput-object v2, v0, Lf/c/a/a;->c:Lf/c/a/m;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v2

    iget-object v8, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v9, v5, [Ljava/lang/Object;

    aput-object v3, v9, v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v7

    invoke-interface {v8, v1, v9}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v6, v0, Lf/c/a/a;->c:Lf/c/a/m;

    :goto_1
    iget-object v2, v0, Lf/c/a/a;->c:Lf/c/a/m;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iput-boolean v7, v2, Lf/c/a/a$n;->g:Z

    :cond_0
    new-instance v2, Lf/c/a/e1;

    invoke-direct {v2}, Lf/c/a/e1;-><init>()V

    iput-object v2, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    iget-object v3, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    const-string v8, "Session Callback parameters"

    :try_start_2
    const-string v9, "AdjustSessionCallbackParameters"

    const-class v10, Ljava/util/Map;

    invoke-static {v3, v9, v8, v10}, Lf/c/a/l1;->x(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    iput-object v3, v2, Lf/c/a/e1;->a:Ljava/util/Map;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v2

    iget-object v3, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v9, v5, [Ljava/lang/Object;

    aput-object v8, v9, v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v7

    invoke-interface {v3, v1, v9}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    iput-object v6, v2, Lf/c/a/e1;->a:Ljava/util/Map;

    :goto_2
    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    const-string v3, "Session Partner parameters"

    :try_start_3
    iget-object v8, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    const-string v9, "AdjustSessionPartnerParameters"

    const-class v10, Ljava/util/Map;

    invoke-static {v2, v9, v3, v10}, Lf/c/a/l1;->x(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    iput-object v2, v8, Lf/c/a/e1;->b:Ljava/util/Map;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :catch_3
    move-exception v2

    iget-object v8, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-interface {v8, v1, v5}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, v0, Lf/c/a/a;->l:Lf/c/a/e1;

    iput-object v6, v1, Lf/c/a/e1;->b:Ljava/util/Map;

    :goto_3
    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v2, v1, Lf/c/a/a$n;->g:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lf/c/a/a;->c:Lf/c/a/m;

    iget-boolean v3, v2, Lf/c/a/m;->enabled:Z

    iput-boolean v3, v1, Lf/c/a/a$n;->a:Z

    iget-boolean v2, v2, Lf/c/a/m;->updatePackages:Z

    iput-boolean v2, v1, Lf/c/a/a$n;->d:Z

    iput-boolean v4, v1, Lf/c/a/a$n;->e:Z

    goto :goto_4

    :cond_1
    iput-boolean v7, v1, Lf/c/a/a$n;->e:Z

    :goto_4
    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    :try_start_4
    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "adjust_config.properties"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    iget-object v1, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v3, v4, [Ljava/lang/Object;

    const-string v5, "adjust_config.properties file read and loaded"

    invoke-interface {v1, v5, v3}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "defaultTracker"

    invoke-virtual {v2, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iput-object v1, v2, Lf/c/a/n;->d:Ljava/lang/String;

    goto :goto_5

    :catch_4
    move-exception v1

    iget-object v2, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const-string v1, "%s file not found in this app"

    invoke-interface {v2, v1, v3}, Lf/c/a/j0;->f(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_5
    new-instance v1, Lf/c/a/c0;

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v6}, Lf/c/a/c0;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lf/c/a/c0;->b(Landroid/content/Context;)V

    iget-object v1, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v1, v1, Lf/c/a/c0;->a:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "Unable to get Google Play Services Advertising ID at start time"

    invoke-interface {v1, v3, v2}, Lf/c/a/j0;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, v0, Lf/c/a/a;->g:Lf/c/a/c0;

    iget-object v2, v1, Lf/c/a/c0;->f:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, v1, Lf/c/a/c0;->g:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v1, v1, Lf/c/a/c0;->h:Ljava/lang/String;

    if-nez v1, :cond_4

    iget-object v1, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "Unable to get any device id\'s. Please check if Proguard is correctly set with Adjust SDK"

    invoke-interface {v1, v3, v2}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    :cond_3
    iget-object v1, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "Google Play Services Advertising ID read correctly at start time"

    invoke-interface {v1, v3, v2}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    :goto_6
    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v2, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v4

    const-string v1, "Default tracker: \'%s\'"

    invoke-interface {v2, v1, v3}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v1, v1, Lf/c/a/a$n;->g:Z

    if-eqz v1, :cond_6

    new-instance v1, Lf/c/a/g1;

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lf/c/a/g1;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lf/c/a/a;->C(Ljava/lang/String;Z)V

    :cond_6
    iget-object v1, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v1, v1, Lf/c/a/a$n;->g:Z

    if-eqz v1, :cond_8

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    const-string v2, "adjust_preferences"

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    :try_start_5
    const-string v2, "gdpr_forget_me"
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :try_start_6
    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2
    :try_end_6
    .catch Ljava/lang/ClassCastException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_7

    :catchall_0
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :catch_5
    const/4 v2, 0x0

    :goto_7
    if-eqz v2, :cond_7

    iget-object v1, v0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/c;

    invoke-direct {v2, v0}, Lf/c/a/c;-><init>(Lf/c/a/a;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    goto :goto_9

    :cond_7
    :try_start_8
    const-string v2, "disable_third_party_sharing"
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_8

    :catchall_1
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :catch_6
    const/4 v1, 0x0

    :goto_8
    if-eqz v1, :cond_8

    iget-object v1, v0, Lf/c/a/a;->a:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/d;

    invoke-direct {v2, v0}, Lf/c/a/d;-><init>(Lf/c/a/a;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    goto :goto_9

    :catchall_2
    move-exception v0

    throw v0

    :catchall_3
    move-exception v0

    throw v0

    :cond_8
    :goto_9
    new-instance v1, Lf/c/a/n1/j;

    new-instance v9, Lf/c/a/f;

    invoke-direct {v9, v0}, Lf/c/a/f;-><init>(Lf/c/a/a;)V

    sget-wide v10, Lf/c/a/a;->p:J

    sget-wide v12, Lf/c/a/a;->o:J

    const-string v14, "Foreground timer"

    move-object v8, v1

    invoke-direct/range {v8 .. v14}, Lf/c/a/n1/j;-><init>(Ljava/lang/Runnable;JJLjava/lang/String;)V

    iput-object v1, v0, Lf/c/a/a;->e:Lf/c/a/n1/j;

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v1, v1, Lf/c/a/a$n;->g:Z

    xor-int/2addr v1, v7

    if-eqz v1, :cond_9

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sput-object v6, Ls/a/b/b/a;->b:Ljava/lang/String;

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v1, v1, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v0, v4}, Lf/c/a/a;->F(Z)Z

    move-result v2

    new-instance v3, Lf/c/a/v0;

    invoke-direct {v3, v0, v1, v2}, Lf/c/a/v0;-><init>(Lf/c/a/h0;Landroid/content/Context;Z)V

    iput-object v3, v0, Lf/c/a/a;->b:Lf/c/a/k0;

    invoke-virtual {v0, v4}, Lf/c/a/a;->F(Z)Z

    move-result v1

    new-instance v2, Lf/c/a/u;

    invoke-direct {v2, v0, v1}, Lf/c/a/u;-><init>(Lf/c/a/h0;Z)V

    iput-object v2, v0, Lf/c/a/a;->j:Lf/c/a/i0;

    invoke-virtual {v0, v7}, Lf/c/a/a;->F(Z)Z

    move-result v1

    new-instance v2, Lf/c/a/z0;

    invoke-direct {v2, v0, v1}, Lf/c/a/z0;-><init>(Lf/c/a/h0;Z)V

    iput-object v2, v0, Lf/c/a/a;->k:Lf/c/a/m0;

    iget-object v1, v0, Lf/c/a/a;->c:Lf/c/a/m;

    if-eqz v1, :cond_a

    iget-boolean v1, v1, Lf/c/a/m;->updatePackages:Z

    goto :goto_a

    :cond_a
    iget-object v1, v0, Lf/c/a/a;->f:Lf/c/a/a$n;

    iget-boolean v1, v1, Lf/c/a/a$n;->d:Z

    :goto_a
    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lf/c/a/a;->M()V

    :cond_b
    new-instance v1, Lf/c/a/n0;

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    new-instance v3, Lf/c/a/g;

    invoke-direct {v3, v0}, Lf/c/a/g;-><init>(Lf/c/a/a;)V

    invoke-direct {v1, v2, v3}, Lf/c/a/n0;-><init>(Landroid/content/Context;Lf/c/a/p0;)V

    iput-object v1, v0, Lf/c/a/a;->m:Lf/c/a/n0;

    new-instance v1, Lf/c/a/o0;

    iget-object v2, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v2, v2, Lf/c/a/n;->a:Landroid/content/Context;

    new-instance v3, Lf/c/a/h;

    invoke-direct {v3, v0}, Lf/c/a/h;-><init>(Lf/c/a/a;)V

    invoke-direct {v1, v2, v3}, Lf/c/a/o0;-><init>(Landroid/content/Context;Lf/c/a/p0;)V

    iput-object v1, v0, Lf/c/a/a;->n:Lf/c/a/o0;

    iget-object v1, v0, Lf/c/a/a;->h:Lf/c/a/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/c/a/a;->A()V

    return-void
.end method
