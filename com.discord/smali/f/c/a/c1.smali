.class public Lf/c/a/c1;
.super Ljava/lang/Object;
.source "SdkClickHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/c/a/l;

.field public final synthetic e:Lf/c/a/z0;


# direct methods
.method public constructor <init>(Lf/c/a/z0;Lf/c/a/l;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/c1;->e:Lf/c/a/z0;

    iput-object p2, p0, Lf/c/a/c1;->d:Lf/c/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    iget-object v0, p0, Lf/c/a/c1;->e:Lf/c/a/z0;

    iget-object v1, p0, Lf/c/a/c1;->d:Lf/c/a/l;

    iget-object v2, v0, Lf/c/a/z0;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/c/a/h0;

    invoke-virtual {v1}, Lf/c/a/l;->i()Ljava/util/Map;

    move-result-object v3

    const-string v4, "source"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    const-string v5, "reftag"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v1}, Lf/c/a/l;->i()Ljava/util/Map;

    move-result-object v6

    const-string v7, "raw_referrer"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-eqz v5, :cond_1

    new-instance v7, Lf/c/a/g1;

    invoke-interface {v2}, Lf/c/a/h0;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lf/c/a/l;->c()J

    move-result-wide v8

    invoke-virtual {v7, v6, v8, v9}, Lf/c/a/g1;->e(Ljava/lang/String;J)Lorg/json/JSONArray;

    move-result-object v7

    if-nez v7, :cond_1

    goto/16 :goto_6

    :cond_1
    if-eqz v3, :cond_2

    const-string v7, "install_referrer"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    :cond_2
    const/4 v3, 0x0

    const-wide/16 v7, -0x1

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lf/c/a/l;->d()J

    move-result-wide v7

    invoke-virtual {v1}, Lf/c/a/l;->h()J

    move-result-wide v9

    invoke-virtual {v1}, Lf/c/a/l;->i()Ljava/util/Map;

    move-result-object v3

    const-string v11, "referrer"

    invoke-interface {v3, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1}, Lf/c/a/l;->i()Ljava/util/Map;

    move-result-object v11

    const-string v12, "referrer_api"

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object v11, v3

    move-wide v9, v7

    :goto_1
    const-string v12, "https://app.adjust.com"

    iget-object v13, v0, Lf/c/a/z0;->d:Ljava/lang/String;

    if-eqz v13, :cond_4

    invoke-static {v12}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v0, Lf/c/a/z0;->d:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    :cond_4
    invoke-static {v12}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Lf/c/a/l;->k()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    :try_start_0
    iget-object v13, v0, Lf/c/a/z0;->e:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-static {v12, v1, v13}, Ls/a/b/b/a;->o(Ljava/lang/String;Lf/c/a/l;I)Lf/c/a/y0;

    move-result-object v12

    check-cast v12, Lf/c/a/d1;

    iget-object v13, v12, Lf/c/a/y0;->f:Lorg/json/JSONObject;

    if-nez v13, :cond_5

    invoke-virtual {v0, v1}, Lf/c/a/z0;->c(Lf/c/a/l;)V

    goto :goto_6

    :cond_5
    if-nez v2, :cond_6

    goto :goto_6

    :cond_6
    iget-object v13, v12, Lf/c/a/y0;->g:Lf/c/a/h1;

    sget-object v14, Lf/c/a/h1;->d:Lf/c/a/h1;

    if-ne v13, v14, :cond_7

    invoke-interface {v2}, Lf/c/a/h0;->n()V

    goto :goto_6

    :cond_7
    if-eqz v5, :cond_8

    new-instance v5, Lf/c/a/g1;

    invoke-interface {v2}, Lf/c/a/h0;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-direct {v5, v13}, Lf/c/a/g1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lf/c/a/l;->c()J

    move-result-wide v13

    invoke-virtual {v5, v6, v13, v14}, Lf/c/a/g1;->j(Ljava/lang/String;J)V

    :cond_8
    if-eqz v4, :cond_9

    iput-wide v7, v12, Lf/c/a/d1;->j:J

    iput-wide v9, v12, Lf/c/a/d1;->k:J

    iput-object v3, v12, Lf/c/a/d1;->l:Ljava/lang/String;

    iput-object v11, v12, Lf/c/a/d1;->m:Ljava/lang/String;

    const/4 v3, 0x1

    iput-boolean v3, v12, Lf/c/a/d1;->i:Z

    :cond_9
    invoke-interface {v2, v12}, Lf/c/a/h0;->e(Lf/c/a/y0;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_6

    :catchall_0
    move-exception v2

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_3

    :catch_1
    move-exception v2

    goto :goto_4

    :catch_2
    move-exception v2

    goto :goto_5

    :goto_2
    const-string v3, "Sdk_click runtime exception"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/z0;->b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    :goto_3
    const-string v3, "Sdk_click request failed. Will retry later"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/z0;->b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lf/c/a/z0;->c(Lf/c/a/l;)V

    goto :goto_6

    :goto_4
    const-string v3, "Sdk_click request timed out. Will retry later"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/z0;->b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lf/c/a/z0;->c(Lf/c/a/l;)V

    goto :goto_6

    :goto_5
    const-string v3, "Sdk_click failed to encode parameters"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/z0;->b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_6
    iget-object v0, p0, Lf/c/a/c1;->e:Lf/c/a/z0;

    iget-object v1, v0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    new-instance v2, Lf/c/a/b1;

    invoke-direct {v2, v0}, Lf/c/a/b1;-><init>(Lf/c/a/z0;)V

    check-cast v1, Lf/c/a/n1/c;

    invoke-virtual {v1, v2}, Lf/c/a/n1/c;->c(Ljava/lang/Runnable;)V

    return-void
.end method
