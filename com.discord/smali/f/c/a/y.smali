.class public Lf/c/a/y;
.super Ljava/lang/Object;
.source "AttributionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/c/a/a0;

.field public final synthetic e:Lf/c/a/u;


# direct methods
.method public constructor <init>(Lf/c/a/u;Lf/c/a/a0;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/y;->e:Lf/c/a/u;

    iput-object p2, p0, Lf/c/a/y;->d:Lf/c/a/a0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lf/c/a/y;->e:Lf/c/a/u;

    iget-object v0, v0, Lf/c/a/u;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/c/a/h0;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lf/c/a/y;->e:Lf/c/a/u;

    iget-object v2, p0, Lf/c/a/y;->d:Lf/c/a/a0;

    invoke-virtual {v1, v0, v2}, Lf/c/a/u;->a(Lf/c/a/h0;Lf/c/a/y0;)V

    iget-object v1, v2, Lf/c/a/y0;->f:Lorg/json/JSONObject;

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v3, "attribution"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    const-string v4, "deeplink"

    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_3
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v2, Lf/c/a/a0;->i:Landroid/net/Uri;

    :goto_0
    invoke-interface {v0, v2}, Lf/c/a/h0;->j(Lf/c/a/a0;)V

    return-void
.end method
