.class public Lf/c/a/l;
.super Ljava/lang/Object;
.source "ActivityPackage.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialPersistentFields:[Ljava/io/ObjectStreamField;

.field private static final serialVersionUID:J = -0x7fab32c0b48621L


# instance fields
.field private activityKind:Lf/c/a/k;

.field private callbackParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private clickTimeInMilliseconds:J

.field private clickTimeInSeconds:J

.field private clientSdk:Ljava/lang/String;

.field public transient d:I

.field private installBeginTimeInSeconds:J

.field private parameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private partnerParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private path:Ljava/lang/String;

.field private retries:I

.field private suffix:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const-class v0, Ljava/lang/String;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/io/ObjectStreamField;

    new-instance v2, Ljava/io/ObjectStreamField;

    const-string v3, "path"

    invoke-direct {v2, v3, v0}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Ljava/io/ObjectStreamField;

    const-string v3, "clientSdk"

    invoke-direct {v2, v3, v0}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Ljava/io/ObjectStreamField;

    const-class v3, Ljava/util/Map;

    const-string v4, "parameters"

    invoke-direct {v2, v4, v3}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Ljava/io/ObjectStreamField;

    const-class v3, Lf/c/a/k;

    const-string v4, "activityKind"

    invoke-direct {v2, v4, v3}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Ljava/io/ObjectStreamField;

    const-string/jumbo v3, "suffix"

    invoke-direct {v2, v3, v0}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v0, 0x4

    aput-object v2, v1, v0

    new-instance v0, Ljava/io/ObjectStreamField;

    const-class v2, Ljava/util/Map;

    const-string v3, "callbackParameters"

    invoke-direct {v0, v3, v2}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v2, 0x5

    aput-object v0, v1, v2

    new-instance v0, Ljava/io/ObjectStreamField;

    const-class v2, Ljava/util/Map;

    const-string v3, "partnerParameters"

    invoke-direct {v0, v3, v2}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    const/4 v2, 0x6

    aput-object v0, v1, v2

    sput-object v1, Lf/c/a/l;->serialPersistentFields:[Ljava/io/ObjectStreamField;

    return-void
.end method

.method public constructor <init>(Lf/c/a/k;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/c/a/k;->d:Lf/c/a/k;

    iput-object v0, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    iput-object p1, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readFields()Ljava/io/ObjectInputStream$GetField;

    move-result-object p1

    const-string v0, "path"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lf/c/a/l1;->z(Ljava/io/ObjectInputStream$GetField;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/l;->path:Ljava/lang/String;

    const-string v0, "clientSdk"

    invoke-static {p1, v0, v1}, Lf/c/a/l1;->z(Ljava/io/ObjectInputStream$GetField;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/l;->clientSdk:Ljava/lang/String;

    const-string v0, "parameters"

    invoke-static {p1, v0, v1}, Lf/c/a/l1;->y(Ljava/io/ObjectInputStream$GetField;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lf/c/a/l;->parameters:Ljava/util/Map;

    sget-object v0, Lf/c/a/k;->d:Lf/c/a/k;

    const-string v2, "activityKind"

    invoke-static {p1, v2, v0}, Lf/c/a/l1;->y(Ljava/io/ObjectInputStream$GetField;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/c/a/k;

    iput-object v0, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    const-string/jumbo v0, "suffix"

    invoke-static {p1, v0, v1}, Lf/c/a/l1;->z(Ljava/io/ObjectInputStream$GetField;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/c/a/l;->suffix:Ljava/lang/String;

    const-string v0, "callbackParameters"

    invoke-static {p1, v0, v1}, Lf/c/a/l1;->y(Ljava/io/ObjectInputStream$GetField;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lf/c/a/l;->callbackParameters:Ljava/util/Map;

    const-string v0, "partnerParameters"

    invoke-static {p1, v0, v1}, Lf/c/a/l1;->y(Ljava/io/ObjectInputStream$GetField;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lf/c/a/l;->partnerParameters:Ljava/util/Map;

    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    return-void
.end method


# virtual methods
.method public a()Lf/c/a/k;
    .locals 1

    iget-object v0, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/c/a/l;->callbackParameters:Ljava/util/Map;

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lf/c/a/l;->clickTimeInMilliseconds:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lf/c/a/l;->clickTimeInSeconds:J

    return-wide v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/c/a/l;->clientSdk:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    check-cast p1, Lf/c/a/l;

    iget-object v2, p0, Lf/c/a/l;->path:Ljava/lang/String;

    iget-object v3, p1, Lf/c/a/l;->path:Ljava/lang/String;

    invoke-static {v2, v3}, Lf/c/a/l1;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    :cond_3
    iget-object v2, p0, Lf/c/a/l;->clientSdk:Ljava/lang/String;

    iget-object v3, p1, Lf/c/a/l;->clientSdk:Ljava/lang/String;

    invoke-static {v2, v3}, Lf/c/a/l1;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    :cond_4
    iget-object v2, p0, Lf/c/a/l;->parameters:Ljava/util/Map;

    iget-object v3, p1, Lf/c/a/l;->parameters:Ljava/util/Map;

    invoke-static {v2, v3}, Lf/c/a/l1;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    return v1

    :cond_5
    iget-object v2, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    iget-object v3, p1, Lf/c/a/l;->activityKind:Lf/c/a/k;

    invoke-static {v2, v3}, Lf/c/a/l1;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    return v1

    :cond_6
    iget-object v2, p0, Lf/c/a/l;->suffix:Ljava/lang/String;

    iget-object v3, p1, Lf/c/a/l;->suffix:Ljava/lang/String;

    invoke-static {v2, v3}, Lf/c/a/l1;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    return v1

    :cond_7
    iget-object v2, p0, Lf/c/a/l;->callbackParameters:Ljava/util/Map;

    iget-object v3, p1, Lf/c/a/l;->callbackParameters:Ljava/util/Map;

    invoke-static {v2, v3}, Lf/c/a/l1;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    return v1

    :cond_8
    iget-object v2, p0, Lf/c/a/l;->partnerParameters:Ljava/util/Map;

    iget-object p1, p1, Lf/c/a/l;->partnerParameters:Ljava/util/Map;

    invoke-static {v2, p1}, Lf/c/a/l1;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    return v1

    :cond_9
    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lf/c/a/l;->path:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "Path:      %s\n"

    invoke-static {v3, v2}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v2, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lf/c/a/l;->clientSdk:Ljava/lang/String;

    aput-object v3, v2, v4

    const-string v3, "ClientSdk: %s\n"

    invoke-static {v3, v2}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lf/c/a/l;->parameters:Ljava/util/Map;

    if-eqz v2, :cond_1

    const-string v2, "Parameters:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/util/TreeMap;

    iget-object v3, p0, Lf/c/a/l;->parameters:Ljava/util/Map;

    invoke-direct {v2, v3}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    const-string v3, "app_secret"

    const-string v5, "secret_id"

    const-string v6, "event_callback_id"

    filled-new-array {v3, v5, v6}, [Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_0

    :cond_0
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v6, v7, v4

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v7, v1

    const-string v5, "\n\t%-16s %s"

    invoke-static {v5, v7}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    invoke-virtual {v1}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lf/c/a/l;->suffix:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Failed to track %s%s"

    invoke-static {v1, v0}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()J
    .locals 2

    iget-wide v0, p0, Lf/c/a/l;->installBeginTimeInSeconds:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lf/c/a/l;->d:I

    if-nez v0, :cond_1

    const/16 v0, 0x11

    iput v0, p0, Lf/c/a/l;->d:I

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lf/c/a/l;->path:Ljava/lang/String;

    invoke-static {v1}, Lf/c/a/l1;->s(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, Lf/c/a/l;->d:I

    mul-int/lit8 v1, v1, 0x25

    iget-object v0, p0, Lf/c/a/l;->clientSdk:Ljava/lang/String;

    invoke-static {v0}, Lf/c/a/l1;->s(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lf/c/a/l;->d:I

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lf/c/a/l;->parameters:Ljava/util/Map;

    invoke-static {v1}, Lf/c/a/l1;->r(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, Lf/c/a/l;->d:I

    mul-int/lit8 v1, v1, 0x25

    iget-object v0, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Enum;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v1, v0

    iput v1, p0, Lf/c/a/l;->d:I

    mul-int/lit8 v1, v1, 0x25

    iget-object v0, p0, Lf/c/a/l;->suffix:Ljava/lang/String;

    invoke-static {v0}, Lf/c/a/l1;->s(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lf/c/a/l;->d:I

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lf/c/a/l;->callbackParameters:Ljava/util/Map;

    invoke-static {v1}, Lf/c/a/l1;->r(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, Lf/c/a/l;->d:I

    mul-int/lit8 v1, v1, 0x25

    iget-object v0, p0, Lf/c/a/l;->partnerParameters:Ljava/util/Map;

    invoke-static {v0}, Lf/c/a/l1;->r(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lf/c/a/l;->d:I

    :cond_1
    iget v0, p0, Lf/c/a/l;->d:I

    return v0
.end method

.method public i()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/c/a/l;->parameters:Ljava/util/Map;

    return-object v0
.end method

.method public j()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/c/a/l;->partnerParameters:Ljava/util/Map;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/c/a/l;->path:Ljava/lang/String;

    return-object v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, Lf/c/a/l;->retries:I

    return v0
.end method

.method public m()I
    .locals 1

    iget v0, p0, Lf/c/a/l;->retries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/c/a/l;->retries:I

    return v0
.end method

.method public n(J)V
    .locals 0

    iput-wide p1, p0, Lf/c/a/l;->clickTimeInMilliseconds:J

    return-void
.end method

.method public o(J)V
    .locals 0

    iput-wide p1, p0, Lf/c/a/l;->clickTimeInSeconds:J

    return-void
.end method

.method public p(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/l;->clientSdk:Ljava/lang/String;

    return-void
.end method

.method public q(J)V
    .locals 0

    iput-wide p1, p0, Lf/c/a/l;->installBeginTimeInSeconds:J

    return-void
.end method

.method public r(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lf/c/a/l;->parameters:Ljava/util/Map;

    return-void
.end method

.method public s(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/l;->path:Ljava/lang/String;

    return-void
.end method

.method public t(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/l;->suffix:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lf/c/a/l;->activityKind:Lf/c/a/k;

    invoke-virtual {v1}, Lf/c/a/k;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lf/c/a/l;->suffix:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "%s%s"

    invoke-static {v1, v0}, Lf/c/a/l1;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
