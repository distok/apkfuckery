.class public Lf/c/a/b1;
.super Ljava/lang/Object;
.source "SdkClickHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/c/a/z0;


# direct methods
.method public constructor <init>(Lf/c/a/z0;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/b1;->d:Lf/c/a/z0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    iget-object v0, p0, Lf/c/a/b1;->d:Lf/c/a/z0;

    iget-object v1, v0, Lf/c/a/z0;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/c/a/h0;

    invoke-interface {v1}, Lf/c/a/h0;->g()Lf/c/a/m;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Lf/c/a/h0;->g()Lf/c/a/m;

    move-result-object v1

    iget-boolean v1, v1, Lf/c/a/m;->isGdprForgotten:Z

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    iget-boolean v1, v0, Lf/c/a/z0;->a:Z

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lf/c/a/z0;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lf/c/a/z0;->e:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/c/a/l;

    invoke-virtual {v1}, Lf/c/a/l;->l()I

    move-result v3

    new-instance v4, Lf/c/a/c1;

    invoke-direct {v4, v0, v1}, Lf/c/a/c1;-><init>(Lf/c/a/z0;Lf/c/a/l;)V

    if-gtz v3, :cond_4

    invoke-virtual {v4}, Lf/c/a/c1;->run()V

    goto :goto_0

    :cond_4
    iget-object v1, v0, Lf/c/a/z0;->c:Lf/c/a/b0;

    invoke-static {v3, v1}, Lf/c/a/l1;->m(ILf/c/a/b0;)J

    move-result-wide v5

    long-to-double v7, v5

    const-wide v9, 0x408f400000000000L    # 1000.0

    div-double/2addr v7, v9

    sget-object v1, Lf/c/a/l1;->a:Ljava/text/DecimalFormat;

    invoke-virtual {v1, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    iget-object v7, v0, Lf/c/a/z0;->b:Lf/c/a/j0;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v1, v8, v2

    const/4 v1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const-string v1, "Waiting for %s seconds before retrying sdk_click for the %d time"

    invoke-interface {v7, v1, v8}, Lf/c/a/j0;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v0, Lf/c/a/z0;->f:Lf/c/a/n1/h;

    check-cast v0, Lf/c/a/n1/c;

    invoke-virtual {v0, v4, v5, v6}, Lf/c/a/n1/c;->b(Ljava/lang/Runnable;J)V

    :goto_0
    return-void
.end method
