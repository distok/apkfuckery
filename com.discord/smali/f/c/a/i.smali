.class public Lf/c/a/i;
.super Ljava/lang/Object;
.source "ActivityHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Landroid/net/Uri;

.field public final synthetic e:Landroid/content/Intent;

.field public final synthetic f:Lf/c/a/a;


# direct methods
.method public constructor <init>(Lf/c/a/a;Landroid/net/Uri;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lf/c/a/i;->f:Lf/c/a/a;

    iput-object p2, p0, Lf/c/a/i;->d:Landroid/net/Uri;

    iput-object p3, p0, Lf/c/a/i;->e:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/c/a/i;->f:Lf/c/a/a;

    iget-object v0, v0, Lf/c/a/a;->h:Lf/c/a/n;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/c/a/i;->f:Lf/c/a/a;

    iget-object v1, p0, Lf/c/a/i;->e:Landroid/content/Intent;

    iget-object v2, p0, Lf/c/a/i;->d:Landroid/net/Uri;

    iget-object v3, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v3, v3, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x1

    if-lez v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-nez v3, :cond_2

    iget-object v0, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v1, v5, [Ljava/lang/Object;

    aput-object v2, v1, v4

    const-string v2, "Unable to open deferred deep link (%s)"

    invoke-interface {v0, v2, v1}, Lf/c/a/j0;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object v3, v0, Lf/c/a/a;->d:Lf/c/a/j0;

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v4

    const-string v2, "Open deferred deep link (%s)"

    invoke-interface {v3, v2, v5}, Lf/c/a/j0;->h(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v0, Lf/c/a/a;->h:Lf/c/a/n;

    iget-object v0, v0, Lf/c/a/n;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_1
    return-void
.end method
