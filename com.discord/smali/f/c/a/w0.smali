.class public Lf/c/a/w0;
.super Ljava/lang/Object;
.source "RequestHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/c/a/l;

.field public final synthetic e:I

.field public final synthetic f:Lf/c/a/x0;


# direct methods
.method public constructor <init>(Lf/c/a/x0;Lf/c/a/l;I)V
    .locals 0

    iput-object p1, p0, Lf/c/a/w0;->f:Lf/c/a/x0;

    iput-object p2, p0, Lf/c/a/w0;->d:Lf/c/a/l;

    iput p3, p0, Lf/c/a/w0;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lf/c/a/w0;->f:Lf/c/a/x0;

    iget-object v1, p0, Lf/c/a/w0;->d:Lf/c/a/l;

    iget v2, p0, Lf/c/a/w0;->e:I

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lf/c/a/l;->a()Lf/c/a/k;

    move-result-object v3

    sget-object v4, Lf/c/a/k;->l:Lf/c/a/k;

    if-ne v3, v4, :cond_0

    const-string v3, "https://gdpr.adjust.com"

    iget-object v4, v0, Lf/c/a/x0;->f:Ljava/lang/String;

    if-eqz v4, :cond_2

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lf/c/a/x0;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lf/c/a/l;->a()Lf/c/a/k;

    move-result-object v3

    sget-object v4, Lf/c/a/k;->o:Lf/c/a/k;

    if-ne v3, v4, :cond_1

    const-string v3, "https://subscription.adjust.com"

    iget-object v4, v0, Lf/c/a/x0;->g:Ljava/lang/String;

    if-eqz v4, :cond_2

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lf/c/a/x0;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    const-string v3, "https://app.adjust.com"

    iget-object v4, v0, Lf/c/a/x0;->e:Ljava/lang/String;

    if-eqz v4, :cond_2

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lf/c/a/x0;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lf/c/a/l;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-static {v3, v1, v2}, Ls/a/b/b/a;->o(Ljava/lang/String;Lf/c/a/l;I)Lf/c/a/y0;

    move-result-object v2

    iget-object v3, v0, Lf/c/a/x0;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/c/a/k0;

    if-nez v3, :cond_3

    goto :goto_1

    :cond_3
    iget-object v4, v0, Lf/c/a/x0;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/c/a/h0;

    if-nez v4, :cond_4

    goto :goto_1

    :cond_4
    iget-object v5, v2, Lf/c/a/y0;->g:Lf/c/a/h1;

    sget-object v6, Lf/c/a/h1;->d:Lf/c/a/h1;

    if-ne v5, v6, :cond_5

    invoke-interface {v4}, Lf/c/a/h0;->n()V

    goto :goto_1

    :cond_5
    iget-object v4, v2, Lf/c/a/y0;->f:Lorg/json/JSONObject;

    if-nez v4, :cond_6

    invoke-interface {v3, v2, v1}, Lf/c/a/k0;->i(Lf/c/a/y0;Lf/c/a/l;)V

    goto :goto_1

    :cond_6
    invoke-interface {v3, v2}, Lf/c/a/k0;->f(Lf/c/a/y0;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    const-string v3, "Runtime exception"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/x0;->b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v3, "Request failed"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/x0;->a(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v2

    const-string v3, "Request timed out"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/x0;->a(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_2
    move-exception v2

    const-string v3, "Failed to encode parameters"

    invoke-virtual {v0, v1, v3, v2}, Lf/c/a/x0;->b(Lf/c/a/l;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method
