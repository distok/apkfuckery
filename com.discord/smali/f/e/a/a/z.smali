.class public final Lf/e/a/a/z;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/e/a/a/c;

.field public final synthetic e:Lf/e/a/a/d;

.field public final synthetic f:Lf/e/a/a/a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a;Lf/e/a/a/c;Lf/e/a/a/d;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/z;->f:Lf/e/a/a/a;

    iput-object p2, p0, Lf/e/a/a/z;->d:Lf/e/a/a/c;

    iput-object p3, p0, Lf/e/a/a/z;->e:Lf/e/a/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/e/a/a/z;->f:Lf/e/a/a/a;

    iget-object v1, p0, Lf/e/a/a/z;->d:Lf/e/a/a/c;

    iget-object v2, p0, Lf/e/a/a/z;->e:Lf/e/a/a/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "BillingClient"

    iget-object v1, v1, Lf/e/a/a/c;->a:Ljava/lang/String;

    :try_start_0
    const-string v4, "Consuming purchase with token: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v4, v5

    :goto_0
    invoke-static {v3, v4}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v4, v0, Lf/e/a/a/a;->k:Z

    if-eqz v4, :cond_2

    iget-object v4, v0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    const/16 v5, 0x9

    iget-object v6, v0, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, v0, Lf/e/a/a/a;->k:Z

    iget-object v8, v0, Lf/e/a/a/a;->b:Ljava/lang/String;

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    if-eqz v7, :cond_1

    const-string v7, "playBillingLibraryVersion"

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-interface {v4, v5, v6, v1, v9}, Lf/h/a/f/i/m/a;->h0(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "RESPONSE_CODE"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v4, v3}, Lf/h/a/f/i/m/b;->e(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    iget-object v3, v0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    const/4 v4, 0x3

    iget-object v5, v0, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5, v1}, Lf/h/a/f/i/m/a;->F(ILjava/lang/String;Ljava/lang/String;)I

    move-result v5

    const-string v3, ""

    :goto_1
    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v4

    iput v5, v4, Lcom/android/billingclient/api/BillingResult$a;->a:I

    iput-object v3, v4, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v3

    if-nez v5, :cond_3

    new-instance v4, Lf/e/a/a/c0;

    invoke-direct {v4, v2, v3, v1}, Lf/e/a/a/c0;-><init>(Lf/e/a/a/d;Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lf/e/a/a/a;->j(Ljava/lang/Runnable;)V

    goto :goto_2

    :cond_3
    new-instance v4, Lf/e/a/a/b0;

    invoke-direct {v4, v5, v2, v3, v1}, Lf/e/a/a/b0;-><init>(ILf/e/a/a/d;Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lf/e/a/a/a;->j(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v3

    new-instance v4, Lf/e/a/a/d0;

    invoke-direct {v4, v3, v2, v1}, Lf/e/a/a/d0;-><init>(Ljava/lang/Exception;Lf/e/a/a/d;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lf/e/a/a/a;->j(Ljava/lang/Runnable;)V

    :goto_2
    const/4 v0, 0x0

    return-object v0
.end method
