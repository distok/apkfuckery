.class public final Lf/e/a/a/t;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/e/a/a/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/e/a/a/e;)V
    .locals 1
    .param p2    # Lf/e/a/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/e/a/a/t;->a:Landroid/content/Context;

    new-instance p1, Lf/e/a/a/u;

    const/4 v0, 0x0

    invoke-direct {p1, p0, p2, v0}, Lf/e/a/a/u;-><init>(Lf/e/a/a/t;Lf/e/a/a/e;Lf/e/a/a/s;)V

    iput-object p1, p0, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    iget-object v1, p0, Lf/e/a/a/t;->a:Landroid/content/Context;

    iget-boolean v2, v0, Lf/e/a/a/u;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/e/a/a/u;->c:Lf/e/a/a/t;

    iget-object v2, v2, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/e/a/a/u;->b:Z

    goto :goto_0

    :cond_0
    const-string v0, "BillingBroadcastManager"

    const-string v1, "Receiver is not registered."

    invoke-static {v0, v1}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
