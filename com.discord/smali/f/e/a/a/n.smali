.class public final Lf/e/a/a/n;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/e/a/a/a$a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a$a;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v0, v0, Lf/e/a/a/a$a;->d:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-boolean v2, v1, Lf/e/a/a/a$a;->e:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    monitor-exit v0

    goto/16 :goto_c

    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x3

    const/4 v2, 0x0

    :try_start_1
    iget-object v1, v1, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    iget-object v1, v1, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v4, 0xd

    const/16 v5, 0xd

    const/4 v6, 0x3

    :goto_0
    if-lt v5, v0, :cond_2

    :try_start_2
    iget-object v7, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v7, v7, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    iget-object v7, v7, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    const-string/jumbo v8, "subs"

    invoke-interface {v7, v5, v1, v8}, Lf/h/a/f/i/m/a;->m(ILjava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v5, v5, -0x1

    goto :goto_0

    :catch_0
    move v0, v6

    goto/16 :goto_a

    :cond_2
    const/4 v5, 0x0

    :goto_1
    iget-object v7, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v7, v7, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    const/4 v8, 0x5

    const/4 v9, 0x1

    if-lt v5, v8, :cond_3

    const/4 v8, 0x1

    goto :goto_2

    :cond_3
    const/4 v8, 0x0

    :goto_2
    iput-boolean v8, v7, Lf/e/a/a/a;->i:Z

    if-lt v5, v0, :cond_4

    const/4 v8, 0x1

    goto :goto_3

    :cond_4
    const/4 v8, 0x0

    :goto_3
    iput-boolean v8, v7, Lf/e/a/a/a;->h:Z

    if-ge v5, v0, :cond_5

    const-string v5, "BillingClient"

    const-string v7, "In-app billing API does not support subscription on this device."

    invoke-static {v5, v7}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const/16 v5, 0xd

    :goto_4
    if-lt v5, v0, :cond_7

    iget-object v7, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v7, v7, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    iget-object v7, v7, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    const-string v8, "inapp"

    invoke-interface {v7, v5, v1, v8}, Lf/h/a/f/i/m/a;->m(ILjava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_6

    goto :goto_5

    :cond_6
    add-int/lit8 v5, v5, -0x1

    goto :goto_4

    :cond_7
    const/4 v5, 0x0

    :goto_5
    iget-object v1, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v1, v1, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    if-lt v5, v4, :cond_8

    const/4 v4, 0x1

    goto :goto_6

    :cond_8
    const/4 v4, 0x0

    :goto_6
    iput-boolean v4, v1, Lf/e/a/a/a;->m:Z

    const/16 v4, 0xa

    if-lt v5, v4, :cond_9

    const/4 v4, 0x1

    goto :goto_7

    :cond_9
    const/4 v4, 0x0

    :goto_7
    iput-boolean v4, v1, Lf/e/a/a/a;->l:Z

    const/16 v4, 0x9

    if-lt v5, v4, :cond_a

    const/4 v4, 0x1

    goto :goto_8

    :cond_a
    const/4 v4, 0x0

    :goto_8
    iput-boolean v4, v1, Lf/e/a/a/a;->k:Z

    const/4 v4, 0x6

    if-lt v5, v4, :cond_b

    goto :goto_9

    :cond_b
    const/4 v9, 0x0

    :goto_9
    iput-boolean v9, v1, Lf/e/a/a/a;->j:Z

    if-ge v5, v0, :cond_c

    const-string v0, "BillingClient"

    const-string v1, "In-app billing API version 3 is not supported on this device."

    invoke-static {v0, v1}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    if-nez v6, :cond_d

    iget-object v0, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v0, v0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    const/4 v1, 0x2

    iput v1, v0, Lf/e/a/a/a;->a:I

    goto :goto_b

    :cond_d
    iget-object v0, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v0, v0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    iput v2, v0, Lf/e/a/a/a;->a:I

    iput-object v3, v0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_b

    :catch_1
    :goto_a
    const-string v1, "BillingClient"

    const-string v4, "Exception while checking if billing is supported; try to reconnect"

    invoke-static {v1, v4}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    iget-object v1, v1, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    iput v2, v1, Lf/e/a/a/a;->a:I

    iput-object v3, v1, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    move v6, v0

    :goto_b
    if-nez v6, :cond_e

    iget-object v0, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    sget-object v1, Lf/e/a/a/q;->i:Lcom/android/billingclient/api/BillingResult;

    invoke-static {v0, v1}, Lf/e/a/a/a$a;->a(Lf/e/a/a/a$a;Lcom/android/billingclient/api/BillingResult;)V

    goto :goto_c

    :cond_e
    iget-object v0, p0, Lf/e/a/a/n;->d:Lf/e/a/a/a$a;

    sget-object v1, Lf/e/a/a/q;->a:Lcom/android/billingclient/api/BillingResult;

    invoke-static {v0, v1}, Lf/e/a/a/a$a;->a(Lf/e/a/a/a$a;Lcom/android/billingclient/api/BillingResult;)V

    :goto_c
    return-object v3

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method
