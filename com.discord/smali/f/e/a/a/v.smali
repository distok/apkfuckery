.class public final Lf/e/a/a/v;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/util/List;

.field public final synthetic f:Lf/e/a/a/g;

.field public final synthetic g:Lf/e/a/a/a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lf/e/a/a/g;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/v;->g:Lf/e/a/a/a;

    iput-object p2, p0, Lf/e/a/a/v;->d:Ljava/lang/String;

    iput-object p3, p0, Lf/e/a/a/v;->e:Ljava/util/List;

    iput-object p5, p0, Lf/e/a/a/v;->f:Lf/e/a/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v0, v1, Lf/e/a/a/v;->g:Lf/e/a/a/a;

    iget-object v8, v1, Lf/e/a/a/v;->d:Ljava/lang/String;

    iget-object v9, v1, Lf/e/a/a/v;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v10, "BillingClient"

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v12, :cond_9

    add-int/lit8 v15, v2, 0x14

    if-le v15, v12, :cond_0

    move v3, v12

    goto :goto_1

    :cond_0
    move v3, v15

    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v9, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ITEM_ID_LIST"

    invoke-virtual {v6, v2, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v2, v0, Lf/e/a/a/a;->b:Ljava/lang/String;

    const-string v3, "playBillingLibraryVersion"

    invoke-virtual {v6, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v2, v0, Lf/e/a/a/a;->l:Z

    if-eqz v2, :cond_3

    iget-object v2, v0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    iget-object v4, v0, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, v0, Lf/e/a/a/a;->k:Z

    iget-boolean v7, v0, Lf/e/a/a/a;->n:Z

    iget-object v13, v0, Lf/e/a/a/a;->b:Ljava/lang/String;

    sget v16, Lf/h/a/f/i/m/b;->a:I

    new-instance v14, Landroid/os/Bundle;

    invoke-direct {v14}, Landroid/os/Bundle;-><init>()V

    if-eqz v5, :cond_1

    invoke-virtual {v14, v3, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz v5, :cond_2

    if-eqz v7, :cond_2

    const-string v3, "enablePendingPurchases"

    const/4 v5, 0x1

    invoke-virtual {v14, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    const/16 v3, 0xa

    move-object v5, v8

    move-object v7, v14

    invoke-interface/range {v2 .. v7}, Lf/h/a/f/i/m/a;->d0(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_2

    :cond_3
    iget-object v2, v0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    const/4 v3, 0x3

    iget-object v4, v0, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4, v8, v6}, Lf/h/a/f/i/m/a;->Q(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    const/4 v3, 0x4

    if-nez v2, :cond_4

    const-string v0, "querySkuDetailsAsync got null sku details list"

    invoke-static {v10, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/billingclient/api/SkuDetails$a;

    const-string v2, "Null sku details list"

    const/4 v4, 0x0

    invoke-direct {v0, v3, v2, v4}, Lcom/android/billingclient/api/SkuDetails$a;-><init>(ILjava/lang/String;Ljava/util/List;)V

    goto/16 :goto_5

    :cond_4
    const-string v4, "DETAILS_LIST"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x6

    if-nez v5, :cond_6

    invoke-static {v2, v10}, Lf/h/a/f/i/m/b;->a(Landroid/os/Bundle;Ljava/lang/String;)I

    move-result v0

    invoke-static {v2, v10}, Lf/h/a/f/i/m/b;->e(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_5

    const/16 v3, 0x32

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "getSkuDetails() failed. Response code: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v10, v3}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/android/billingclient/api/SkuDetails$a;

    invoke-direct {v3, v0, v2, v11}, Lcom/android/billingclient/api/SkuDetails$a;-><init>(ILjava/lang/String;Ljava/util/List;)V

    move-object v0, v3

    goto :goto_3

    :cond_5
    const-string v0, "getSkuDetails() returned a bundle with neither an error nor a detail list."

    invoke-static {v10, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/billingclient/api/SkuDetails$a;

    invoke-direct {v0, v6, v2, v11}, Lcom/android/billingclient/api/SkuDetails$a;-><init>(ILjava/lang/String;Ljava/util/List;)V

    :goto_3
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_6
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_7

    const-string v0, "querySkuDetailsAsync got null response list"

    invoke-static {v10, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/android/billingclient/api/SkuDetails$a;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Lcom/android/billingclient/api/SkuDetails$a;-><init>(ILjava/lang/String;Ljava/util/List;)V

    move-object v0, v2

    goto/16 :goto_5

    :cond_7
    const/4 v3, 0x0

    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_8

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    :try_start_1
    new-instance v5, Lcom/android/billingclient/api/SkuDetails;

    invoke-direct {v5, v4}, Lcom/android/billingclient/api/SkuDetails;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x11

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Got sku details: "

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v10, v4}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :catch_0
    const-string v0, "Got a JSON exception trying to decode SkuDetails."

    invoke-static {v10, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/billingclient/api/SkuDetails$a;

    const-string v2, "Error trying to decode SkuDetails."

    const/4 v3, 0x0

    invoke-direct {v0, v6, v2, v3}, Lcom/android/billingclient/api/SkuDetails$a;-><init>(ILjava/lang/String;Ljava/util/List;)V

    move-object v4, v3

    goto :goto_5

    :cond_8
    move v2, v15

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "querySkuDetailsAsync got a remote exception (try to reconnect)."

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/billingclient/api/SkuDetails$a;

    const/4 v2, -0x1

    const-string v3, "Service connection is disconnected."

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/android/billingclient/api/SkuDetails$a;-><init>(ILjava/lang/String;Ljava/util/List;)V

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    new-instance v0, Lcom/android/billingclient/api/SkuDetails$a;

    const-string v2, ""

    const/4 v3, 0x0

    invoke-direct {v0, v3, v2, v11}, Lcom/android/billingclient/api/SkuDetails$a;-><init>(ILjava/lang/String;Ljava/util/List;)V

    :goto_5
    iget-object v2, v1, Lf/e/a/a/v;->g:Lf/e/a/a/a;

    new-instance v3, Lf/e/a/a/x;

    invoke-direct {v3, v1, v0}, Lf/e/a/a/x;-><init>(Lf/e/a/a/v;Lcom/android/billingclient/api/SkuDetails$a;)V

    invoke-virtual {v2, v3}, Lf/e/a/a/a;->j(Ljava/lang/Runnable;)V

    return-object v4
.end method
