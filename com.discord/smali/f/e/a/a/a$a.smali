.class public final Lf/e/a/a/a$a;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/e/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final d:Ljava/lang/Object;

.field public e:Z

.field public f:Lf/e/a/a/b;

.field public final synthetic g:Lf/e/a/a/a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a;Lf/e/a/a/b;Lcom/android/billingclient/api/zzh;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/e/a/a/a$a;->d:Ljava/lang/Object;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/e/a/a/a$a;->e:Z

    iput-object p2, p0, Lf/e/a/a/a$a;->f:Lf/e/a/a/b;

    return-void
.end method

.method public static a(Lf/e/a/a/a$a;Lcom/android/billingclient/api/BillingResult;)V
    .locals 2

    iget-object v0, p0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    new-instance v1, Lf/e/a/a/l;

    invoke-direct {v1, p0, p1}, Lf/e/a/a/l;-><init>(Lf/e/a/a/a$a;Lcom/android/billingclient/api/BillingResult;)V

    invoke-virtual {v0, v1}, Lf/e/a/a/a;->j(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    const-string p1, "BillingClient"

    const-string v0, "Billing service connected."

    invoke-static {p1, v0}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    sget v0, Lf/h/a/f/i/m/d;->a:I

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const-string v0, "com.android.vending.billing.IInAppBillingService"

    invoke-interface {p2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lf/h/a/f/i/m/a;

    if-eqz v1, :cond_1

    move-object p2, v0

    check-cast p2, Lf/h/a/f/i/m/a;

    goto :goto_0

    :cond_1
    new-instance v0, Lf/h/a/f/i/m/c;

    invoke-direct {v0, p2}, Lf/h/a/f/i/m/c;-><init>(Landroid/os/IBinder;)V

    move-object p2, v0

    :goto_0
    iput-object p2, p1, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    iget-object p1, p0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    new-instance p2, Lf/e/a/a/n;

    invoke-direct {p2, p0}, Lf/e/a/a/n;-><init>(Lf/e/a/a/a$a;)V

    new-instance v0, Lf/e/a/a/m;

    invoke-direct {v0, p0}, Lf/e/a/a/m;-><init>(Lf/e/a/a/a$a;)V

    const-wide/16 v1, 0x7530

    invoke-virtual {p1, p2, v1, v2, v0}, Lf/e/a/a/a;->i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    invoke-virtual {p1}, Lf/e/a/a/a;->k()Lcom/android/billingclient/api/BillingResult;

    move-result-object p1

    iget-object p2, p0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    new-instance v0, Lf/e/a/a/l;

    invoke-direct {v0, p0, p1}, Lf/e/a/a/l;-><init>(Lf/e/a/a/a$a;Lcom/android/billingclient/api/BillingResult;)V

    invoke-virtual {p2, v0}, Lf/e/a/a/a;->j(Ljava/lang/Runnable;)V

    :cond_2
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "BillingClient"

    const-string v0, "Billing service disconnected."

    invoke-static {p1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lf/e/a/a/a$a;->g:Lf/e/a/a/a;

    const/4 v0, 0x0

    iput-object v0, p1, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    const/4 v0, 0x0

    iput v0, p1, Lf/e/a/a/a;->a:I

    iget-object p1, p0, Lf/e/a/a/a$a;->d:Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lf/e/a/a/a$a;->f:Lf/e/a/a/b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/e/a/a/b;->onBillingServiceDisconnected()V

    :cond_0
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
