.class public Lf/e/a/a/a;
.super Lcom/android/billingclient/api/BillingClient;
.source "com.android.billingclient:billing@@3.0.0"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/e/a/a/a$a;
    }
.end annotation


# instance fields
.field public a:I

.field public final b:Ljava/lang/String;

.field public final c:Landroid/os/Handler;

.field public d:Lf/e/a/a/t;

.field public e:Landroid/content/Context;

.field public f:Lf/h/a/f/i/m/a;

.field public g:Lf/e/a/a/a$a;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Ljava/util/concurrent/ExecutorService;

.field public final p:Landroid/os/ResultReceiver;


# direct methods
.method public constructor <init>(ZLandroid/content/Context;Lf/e/a/a/e;)V
    .locals 3
    .param p2    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lf/e/a/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    :try_start_0
    const-string v0, "f.e.a.b.a"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "VERSION_NAME"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, "3.0.0"

    :goto_0
    invoke-direct {p0}, Lcom/android/billingclient/api/BillingClient;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, Lf/e/a/a/a;->a:I

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lf/e/a/a/a;->c:Landroid/os/Handler;

    new-instance v2, Lcom/android/billingclient/api/zzh;

    invoke-direct {v2, p0, v1}, Lcom/android/billingclient/api/zzh;-><init>(Lf/e/a/a/a;Landroid/os/Handler;)V

    iput-object v2, p0, Lf/e/a/a/a;->p:Landroid/os/ResultReceiver;

    iput-object v0, p0, Lf/e/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    iput-object p2, p0, Lf/e/a/a/a;->e:Landroid/content/Context;

    new-instance v0, Lf/e/a/a/t;

    invoke-direct {v0, p2, p3}, Lf/e/a/a/t;-><init>(Landroid/content/Context;Lf/e/a/a/e;)V

    iput-object v0, p0, Lf/e/a/a/a;->d:Lf/e/a/a/t;

    iput-boolean p1, p0, Lf/e/a/a/a;->n:Z

    return-void
.end method


# virtual methods
.method public a(Lf/e/a/a/c;Lf/e/a/a/d;)V
    .locals 4

    invoke-virtual {p0}, Lf/e/a/a/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    iget-object p1, p1, Lf/e/a/a/c;->a:Ljava/lang/String;

    invoke-interface {p2, v0, p1}, Lf/e/a/a/d;->onConsumeResponse(Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Lf/e/a/a/z;

    invoke-direct {v0, p0, p1, p2}, Lf/e/a/a/z;-><init>(Lf/e/a/a/a;Lf/e/a/a/c;Lf/e/a/a/d;)V

    const-wide/16 v1, 0x7530

    new-instance v3, Lf/e/a/a/y;

    invoke-direct {v3, p2, p1}, Lf/e/a/a/y;-><init>(Lf/e/a/a/d;Lf/e/a/a/c;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lf/e/a/a/a;->i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lf/e/a/a/a;->k()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    iget-object p1, p1, Lf/e/a/a/c;->a:Ljava/lang/String;

    invoke-interface {p2, v0, p1}, Lf/e/a/a/d;->onConsumeResponse(Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 5

    const/4 v0, 0x3

    :try_start_0
    iget-object v1, p0, Lf/e/a/a/a;->d:Lf/e/a/a/t;

    invoke-virtual {v1}, Lf/e/a/a/t;->a()V

    iget-object v1, p0, Lf/e/a/a/a;->g:Lf/e/a/a/a$a;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v3, v1, Lf/e/a/a/a$a;->d:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iput-object v2, v1, Lf/e/a/a/a$a;->f:Lf/e/a/a/b;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lf/e/a/a/a$a;->e:Z

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1

    :cond_0
    :goto_0
    iget-object v1, p0, Lf/e/a/a/a;->g:Lf/e/a/a/a$a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    if-eqz v1, :cond_1

    const-string v1, "BillingClient"

    const-string v3, "Unbinding from service."

    invoke-static {v1, v3}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lf/e/a/a/a;->e:Landroid/content/Context;

    iget-object v3, p0, Lf/e/a/a/a;->g:Lf/e/a/a/a$a;

    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v2, p0, Lf/e/a/a/a;->g:Lf/e/a/a/a$a;

    :cond_1
    iput-object v2, p0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    iget-object v1, p0, Lf/e/a/a/a;->o:Ljava/util/concurrent/ExecutorService;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v2, p0, Lf/e/a/a/a;->o:Ljava/util/concurrent/ExecutorService;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_2
    iput v0, p0, Lf/e/a/a/a;->a:I

    return-void

    :catchall_1
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_3
    const-string v2, "BillingClient"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "There was an exception while ending connection: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iput v0, p0, Lf/e/a/a/a;->a:I

    return-void

    :goto_1
    iput v0, p0, Lf/e/a/a/a;->a:I

    throw v1
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lf/e/a/a/a;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/e/a/a/a;->g:Lf/e/a/a/a$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)Lcom/android/billingclient/api/BillingResult;
    .locals 17
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v5, p2

    const-string v9, "BUY_INTENT"

    const-string v10, "; try to reconnect"

    invoke-virtual/range {p0 .. p0}, Lf/e/a/a/a;->c()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    invoke-virtual {v7, v0}, Lf/e/a/a/a;->h(Lcom/android/billingclient/api/BillingResult;)Lcom/android/billingclient/api/BillingResult;

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v5, Lcom/android/billingclient/api/BillingFlowParams;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v3}, Lcom/android/billingclient/api/SkuDetails;->e()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v1, "subs"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v11, "BillingClient"

    if-eqz v1, :cond_1

    iget-boolean v1, v7, Lf/e/a/a/a;->h:Z

    if-nez v1, :cond_1

    const-string v0, "Current client doesn\'t support subscriptions."

    invoke-static {v11, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->l:Lcom/android/billingclient/api/BillingResult;

    invoke-virtual {v7, v0}, Lf/e/a/a/a;->h(Lcom/android/billingclient/api/BillingResult;)Lcom/android/billingclient/api/BillingResult;

    return-object v0

    :cond_1
    iget-object v1, v5, Lcom/android/billingclient/api/BillingFlowParams;->b:Ljava/lang/String;

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    iget-boolean v6, v7, Lf/e/a/a/a;->i:Z

    if-nez v6, :cond_3

    const-string v0, "Current client doesn\'t support subscriptions update."

    invoke-static {v11, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->m:Lcom/android/billingclient/api/BillingResult;

    invoke-virtual {v7, v0}, Lf/e/a/a/a;->h(Lcom/android/billingclient/api/BillingResult;)Lcom/android/billingclient/api/BillingResult;

    return-object v0

    :cond_3
    iget-object v6, v5, Lcom/android/billingclient/api/BillingFlowParams;->f:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v13, 0x0

    :cond_4
    if-ge v13, v12, :cond_5

    invoke-virtual {v6, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    add-int/lit8 v13, v13, 0x1

    check-cast v14, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v14}, Lcom/android/billingclient/api/SkuDetails;->f()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_4

    const/4 v6, 0x0

    goto :goto_1

    :cond_5
    const/4 v6, 0x1

    :goto_1
    iget-boolean v12, v5, Lcom/android/billingclient/api/BillingFlowParams;->g:Z

    if-nez v12, :cond_7

    iget-object v12, v5, Lcom/android/billingclient/api/BillingFlowParams;->a:Ljava/lang/String;

    if-nez v12, :cond_7

    iget-object v12, v5, Lcom/android/billingclient/api/BillingFlowParams;->d:Ljava/lang/String;

    if-nez v12, :cond_7

    iget v12, v5, Lcom/android/billingclient/api/BillingFlowParams;->e:I

    if-nez v12, :cond_7

    if-eqz v6, :cond_6

    goto :goto_2

    :cond_6
    const/4 v6, 0x0

    goto :goto_3

    :cond_7
    :goto_2
    const/4 v6, 0x1

    :goto_3
    if-eqz v6, :cond_8

    iget-boolean v6, v7, Lf/e/a/a/a;->j:Z

    if-nez v6, :cond_8

    const-string v0, "Current client doesn\'t support extra params for buy intent."

    invoke-static {v11, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->g:Lcom/android/billingclient/api/BillingResult;

    invoke-virtual {v7, v0}, Lf/e/a/a/a;->h(Lcom/android/billingclient/api/BillingResult;)Lcom/android/billingclient/api/BillingResult;

    return-object v0

    :cond_8
    const-string v6, ""

    const/4 v12, 0x0

    move-object v12, v6

    const/4 v6, 0x0

    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v6, v13, :cond_a

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    add-int/2addr v15, v14

    invoke-static {v15, v12, v13}, Lf/e/c/a/a;->e(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v13

    sub-int/2addr v13, v2

    if-ge v6, v13, :cond_9

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_a
    const/16 v6, 0x29

    invoke-static {v12, v6}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v6

    const-string v6, "Constructing buy intent for "

    const-string v14, ", item type: "

    invoke-static {v13, v6, v12, v14, v4}, Lf/e/c/a/a;->g(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v11, v6}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v6, v7, Lf/e/a/a/a;->j:Z

    if-eqz v6, :cond_19

    iget-boolean v1, v7, Lf/e/a/a/a;->k:Z

    iget-boolean v6, v7, Lf/e/a/a/a;->n:Z

    iget-object v13, v7, Lf/e/a/a/a;->b:Ljava/lang/String;

    new-instance v14, Landroid/os/Bundle;

    invoke-direct {v14}, Landroid/os/Bundle;-><init>()V

    const-string v15, "playBillingLibraryVersion"

    invoke-virtual {v14, v15, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget v13, v5, Lcom/android/billingclient/api/BillingFlowParams;->e:I

    if-eqz v13, :cond_b

    const-string v15, "prorationMode"

    invoke-virtual {v14, v15, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_b
    iget-object v13, v5, Lcom/android/billingclient/api/BillingFlowParams;->a:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_c

    iget-object v13, v5, Lcom/android/billingclient/api/BillingFlowParams;->a:Ljava/lang/String;

    const-string v15, "accountId"

    invoke-virtual {v14, v15, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    iget-object v13, v5, Lcom/android/billingclient/api/BillingFlowParams;->d:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_d

    iget-object v13, v5, Lcom/android/billingclient/api/BillingFlowParams;->d:Ljava/lang/String;

    const-string v15, "obfuscatedProfileId"

    invoke-virtual {v14, v15, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    iget-boolean v13, v5, Lcom/android/billingclient/api/BillingFlowParams;->g:Z

    if-eqz v13, :cond_e

    const-string/jumbo v13, "vr"

    invoke-virtual {v14, v13, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_e
    iget-object v13, v5, Lcom/android/billingclient/api/BillingFlowParams;->b:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_f

    new-instance v13, Ljava/util/ArrayList;

    new-array v2, v2, [Ljava/lang/String;

    iget-object v15, v5, Lcom/android/billingclient/api/BillingFlowParams;->b:Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v15, v2, v16

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v13, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v2, "skusToReplace"

    invoke-virtual {v14, v2, v13}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_f
    iget-object v2, v5, Lcom/android/billingclient/api/BillingFlowParams;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    iget-object v2, v5, Lcom/android/billingclient/api/BillingFlowParams;->c:Ljava/lang/String;

    const-string v13, "oldSkuPurchaseToken"

    invoke-virtual {v14, v13, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    if-eqz v1, :cond_11

    if-eqz v6, :cond_11

    const-string v1, "enablePendingPurchases"

    const/4 v2, 0x1

    invoke-virtual {v14, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_11
    iget-object v1, v3, Lcom/android/billingclient/api/SkuDetails;->b:Lorg/json/JSONObject;

    const-string v2, "skuDetailsToken"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    iget-object v1, v3, Lcom/android/billingclient/api/SkuDetails;->b:Lorg/json/JSONObject;

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    invoke-virtual {v3}, Lcom/android/billingclient/api/SkuDetails;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    invoke-virtual {v3}, Lcom/android/billingclient/api/SkuDetails;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "skuPackageName"

    invoke-virtual {v14, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    const/4 v1, 0x0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14

    const-string v2, "accountName"

    invoke-virtual {v14, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_16

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    sub-int/2addr v6, v2

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x1

    :goto_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_15

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v6}, Lcom/android/billingclient/api/SkuDetails;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_15
    const-string v0, "additionalSkus"

    invoke-virtual {v14, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_16
    iget-boolean v0, v7, Lf/e/a/a/a;->k:Z

    if-eqz v0, :cond_17

    const/16 v0, 0x9

    const/16 v2, 0x9

    goto :goto_6

    :cond_17
    iget-boolean v0, v5, Lcom/android/billingclient/api/BillingFlowParams;->g:Z

    if-eqz v0, :cond_18

    const/4 v0, 0x7

    const/4 v2, 0x7

    goto :goto_6

    :cond_18
    const/4 v0, 0x6

    const/4 v2, 0x6

    :goto_6
    new-instance v13, Lf/e/a/a/i;

    move-object v0, v13

    move-object/from16 v1, p0

    move-object/from16 v5, p2

    move-object v6, v14

    invoke-direct/range {v0 .. v6}, Lf/e/a/a/i;-><init>(Lf/e/a/a/a;ILcom/android/billingclient/api/SkuDetails;Ljava/lang/String;Lcom/android/billingclient/api/BillingFlowParams;Landroid/os/Bundle;)V

    const-wide/16 v0, 0x1388

    const/4 v2, 0x0

    invoke-virtual {v7, v13, v0, v1, v2}, Lf/e/a/a/a;->i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    goto :goto_8

    :cond_19
    const-wide/16 v13, 0x1388

    const/4 v0, 0x0

    if-eqz v1, :cond_1a

    new-instance v1, Lf/e/a/a/h;

    invoke-direct {v1, v7, v5, v3}, Lf/e/a/a/h;-><init>(Lf/e/a/a/a;Lcom/android/billingclient/api/BillingFlowParams;Lcom/android/billingclient/api/SkuDetails;)V

    invoke-virtual {v7, v1, v13, v14, v0}, Lf/e/a/a/a;->i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    goto :goto_7

    :cond_1a
    new-instance v1, Lf/e/a/a/k;

    invoke-direct {v1, v7, v3, v4}, Lf/e/a/a/k;-><init>(Lf/e/a/a/a;Lcom/android/billingclient/api/SkuDetails;Ljava/lang/String;)V

    invoke-virtual {v7, v1, v13, v14, v0}, Lf/e/a/a/a;->i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    :goto_7
    move-wide v0, v13

    :goto_8
    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v0, v1, v3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0, v11}, Lf/h/a/f/i/m/b;->a(Landroid/os/Bundle;Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v11}, Lf/h/a/f/i/m/b;->e(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_1b

    const/16 v0, 0x34

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Unable to buy item, Error response code: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    iput-object v2, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    iget-object v1, v7, Lf/e/a/a/a;->d:Lf/e/a/a/t;

    iget-object v1, v1, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    iget-object v1, v1, Lf/e/a/a/u;->a:Lf/e/a/a/e;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lf/e/a/a/e;->onPurchasesUpdated(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-object v0

    :cond_1b
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/billingclient/api/ProxyBillingActivity;

    invoke-direct {v1, v8, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "result_receiver"

    iget-object v3, v7, Lf/e/a/a/a;->p:Landroid/os/ResultReceiver;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-virtual {v1, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v8, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v0, Lf/e/a/a/q;->i:Lcom/android/billingclient/api/BillingResult;

    return-object v0

    :catch_0
    const/16 v0, 0x45

    invoke-static {v12, v0}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Exception while launching billing flow: ; for sku: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    invoke-virtual {v7, v0}, Lf/e/a/a/a;->h(Lcom/android/billingclient/api/BillingResult;)Lcom/android/billingclient/api/BillingResult;

    return-object v0

    :catch_1
    const/16 v0, 0x44

    invoke-static {v12, v0}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Time out while launching billing flow: ; for sku: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->k:Lcom/android/billingclient/api/BillingResult;

    invoke-virtual {v7, v0}, Lf/e/a/a/a;->h(Lcom/android/billingclient/api/BillingResult;)Lcom/android/billingclient/api/BillingResult;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/android/billingclient/api/Purchase$a;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Lf/e/a/a/a;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-instance p1, Lcom/android/billingclient/api/Purchase$a;

    sget-object v0, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    invoke-direct {p1, v0, v1}, Lcom/android/billingclient/api/Purchase$a;-><init>(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-object p1

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "BillingClient"

    const-string v0, "Please provide a valid SKU type."

    invoke-static {p1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance p1, Lcom/android/billingclient/api/Purchase$a;

    sget-object v0, Lf/e/a/a/q;->f:Lcom/android/billingclient/api/BillingResult;

    invoke-direct {p1, v0, v1}, Lcom/android/billingclient/api/Purchase$a;-><init>(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-object p1

    :cond_1
    new-instance v0, Lf/e/a/a/j;

    invoke-direct {v0, p0, p1}, Lf/e/a/a/j;-><init>(Lf/e/a/a/a;Ljava/lang/String;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {p0, v0, v2, v3, v1}, Lf/e/a/a/a;->i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v2, v3, v0}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/billingclient/api/Purchase$a;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    new-instance p1, Lcom/android/billingclient/api/Purchase$a;

    sget-object v0, Lf/e/a/a/q;->h:Lcom/android/billingclient/api/BillingResult;

    invoke-direct {p1, v0, v1}, Lcom/android/billingclient/api/Purchase$a;-><init>(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-object p1

    :catch_1
    new-instance p1, Lcom/android/billingclient/api/Purchase$a;

    sget-object v0, Lf/e/a/a/q;->k:Lcom/android/billingclient/api/BillingResult;

    invoke-direct {p1, v0, v1}, Lcom/android/billingclient/api/Purchase$a;-><init>(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-object p1
.end method

.method public f(Lf/e/a/a/f;Lf/e/a/a/g;)V
    .locals 8

    invoke-virtual {p0}, Lf/e/a/a/a;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    sget-object p1, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {p2, p1, v1}, Lf/e/a/a/g;->onSkuDetailsResponse(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-void

    :cond_0
    iget-object v4, p1, Lf/e/a/a/f;->a:Ljava/lang/String;

    iget-object v5, p1, Lf/e/a/a/f;->b:Ljava/util/List;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const-string v0, "BillingClient"

    if-eqz p1, :cond_1

    const-string p1, "Please fix the input params. SKU type can\'t be empty."

    invoke-static {v0, p1}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object p1, Lf/e/a/a/q;->f:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {p2, p1, v1}, Lf/e/a/a/g;->onSkuDetailsResponse(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-void

    :cond_1
    if-nez v5, :cond_2

    const-string p1, "Please fix the input params. The list of SKUs can\'t be empty."

    invoke-static {v0, p1}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object p1, Lf/e/a/a/q;->e:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {p2, p1, v1}, Lf/e/a/a/g;->onSkuDetailsResponse(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-void

    :cond_2
    new-instance p1, Lf/e/a/a/v;

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p0

    move-object v7, p2

    invoke-direct/range {v2 .. v7}, Lf/e/a/a/v;-><init>(Lf/e/a/a/a;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lf/e/a/a/g;)V

    const-wide/16 v2, 0x7530

    new-instance v0, Lf/e/a/a/w;

    invoke-direct {v0, p2}, Lf/e/a/a/w;-><init>(Lf/e/a/a/g;)V

    invoke-virtual {p0, p1, v2, v3, v0}, Lf/e/a/a/a;->i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lf/e/a/a/a;->k()Lcom/android/billingclient/api/BillingResult;

    move-result-object p1

    invoke-interface {p2, p1, v1}, Lf/e/a/a/g;->onSkuDetailsResponse(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    :cond_3
    return-void
.end method

.method public g(Lf/e/a/a/b;)V
    .locals 7
    .param p1    # Lf/e/a/a/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/e/a/a/a;->c()Z

    move-result v0

    const-string v1, "BillingClient"

    if-eqz v0, :cond_0

    const-string v0, "Service connection is valid. No need to re-initialize."

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->i:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {p1, v0}, Lf/e/a/a/b;->onBillingSetupFinished(Lcom/android/billingclient/api/BillingResult;)V

    return-void

    :cond_0
    iget v0, p0, Lf/e/a/a/a;->a:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const-string v0, "Client is already in the process of connecting to billing service."

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->c:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {p1, v0}, Lf/e/a/a/b;->onBillingSetupFinished(Lcom/android/billingclient/api/BillingResult;)V

    return-void

    :cond_1
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    const-string v0, "Client was already closed and can\'t be reused. Please create another instance."

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {p1, v0}, Lf/e/a/a/b;->onBillingSetupFinished(Lcom/android/billingclient/api/BillingResult;)V

    return-void

    :cond_2
    iput v2, p0, Lf/e/a/a/a;->a:I

    iget-object v0, p0, Lf/e/a/a/a;->d:Lf/e/a/a/t;

    iget-object v3, v0, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    iget-object v0, v0, Lf/e/a/a/t;->a:Landroid/content/Context;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.android.vending.billing.PURCHASES_UPDATED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-boolean v5, v3, Lf/e/a/a/u;->b:Z

    if-nez v5, :cond_3

    iget-object v5, v3, Lf/e/a/a/u;->c:Lf/e/a/a/t;

    iget-object v5, v5, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    invoke-virtual {v0, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v2, v3, Lf/e/a/a/u;->b:Z

    :cond_3
    const-string v0, "Starting in-app billing setup."

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lf/e/a/a/a$a;

    const/4 v3, 0x0

    invoke-direct {v0, p0, p1, v3}, Lf/e/a/a/a$a;-><init>(Lf/e/a/a/a;Lf/e/a/a/b;Lcom/android/billingclient/api/zzh;)V

    iput-object v0, p0, Lf/e/a/a/a;->g:Lf/e/a/a/a$a;

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.android.vending"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v4, :cond_6

    iget-object v6, v4, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v4, :cond_5

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v6, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v0, p0, Lf/e/a/a/a;->b:Ljava/lang/String;

    const-string v3, "playBillingLibraryVersion"

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lf/e/a/a/a;->e:Landroid/content/Context;

    iget-object v3, p0, Lf/e/a/a/a;->g:Lf/e/a/a/a$a;

    invoke-virtual {v0, v4, v3, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string p1, "Service was bonded successfully."

    invoke-static {v1, p1}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_4
    const-string v0, "Connection to Billing service is blocked."

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v0, "The device doesn\'t have valid Play Store."

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_0
    iput v5, p0, Lf/e/a/a/a;->a:I

    const-string v0, "Billing service unavailable on device."

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/e/a/a/q;->b:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {p1, v0}, Lf/e/a/a/b;->onBillingSetupFinished(Lcom/android/billingclient/api/BillingResult;)V

    return-void
.end method

.method public final h(Lcom/android/billingclient/api/BillingResult;)Lcom/android/billingclient/api/BillingResult;
    .locals 2

    iget-object v0, p0, Lf/e/a/a/a;->d:Lf/e/a/a/t;

    iget-object v0, v0, Lf/e/a/a/t;->b:Lf/e/a/a/u;

    iget-object v0, v0, Lf/e/a/a/u;->a:Lf/e/a/a/e;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lf/e/a/a/e;->onPurchasesUpdated(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-object p1
.end method

.method public final i(Ljava/util/concurrent/Callable;JLjava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Ljava/util/concurrent/Callable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;J",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future<",
            "TT;>;"
        }
    .end annotation

    long-to-double p2, p2

    const-wide v0, 0x3fee666666666666L    # 0.95

    mul-double p2, p2, v0

    double-to-long p2, p2

    iget-object v0, p0, Lf/e/a/a/a;->o:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    sget v0, Lf/h/a/f/i/m/b;->a:I

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lf/e/a/a/a;->o:Ljava/util/concurrent/ExecutorService;

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/e/a/a/a;->o:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lf/e/a/a/a;->c:Landroid/os/Handler;

    new-instance v1, Lf/e/a/a/a0;

    invoke-direct {v1, p1, p4}, Lf/e/a/a/a0;-><init>(Ljava/util/concurrent/Future;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    add-int/lit8 p2, p2, 0x1c

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3, p2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p2, "Async task throws exception "

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "BillingClient"

    invoke-static {p2, p1}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public final j(Ljava/lang/Runnable;)V
    .locals 1

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/e/a/a/a;->c:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k()Lcom/android/billingclient/api/BillingResult;
    .locals 2

    iget v0, p0, Lf/e/a/a/a;->a:I

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lf/e/a/a/q;->h:Lcom/android/billingclient/api/BillingResult;

    return-object v0

    :cond_1
    :goto_0
    sget-object v0, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    return-object v0
.end method
