.class public final Lf/e/a/a/b0;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Lf/e/a/a/d;

.field public final synthetic f:Lcom/android/billingclient/api/BillingResult;

.field public final synthetic g:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILf/e/a/a/d;Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V
    .locals 0

    iput p1, p0, Lf/e/a/a/b0;->d:I

    iput-object p2, p0, Lf/e/a/a/b0;->e:Lf/e/a/a/d;

    iput-object p3, p0, Lf/e/a/a/b0;->f:Lcom/android/billingclient/api/BillingResult;

    iput-object p4, p0, Lf/e/a/a/b0;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget v0, p0, Lf/e/a/a/b0;->d:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Error consuming purchase with token. Response code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BillingClient"

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/e/a/a/b0;->e:Lf/e/a/a/d;

    iget-object v1, p0, Lf/e/a/a/b0;->f:Lcom/android/billingclient/api/BillingResult;

    iget-object v2, p0, Lf/e/a/a/b0;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lf/e/a/a/d;->onConsumeResponse(Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V

    return-void
.end method
