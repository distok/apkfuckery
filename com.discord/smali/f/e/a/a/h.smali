.class public final Lf/e/a/a/h;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/android/billingclient/api/BillingFlowParams;

.field public final synthetic e:Lcom/android/billingclient/api/SkuDetails;

.field public final synthetic f:Lf/e/a/a/a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a;Lcom/android/billingclient/api/BillingFlowParams;Lcom/android/billingclient/api/SkuDetails;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/h;->f:Lf/e/a/a/a;

    iput-object p2, p0, Lf/e/a/a/h;->d:Lcom/android/billingclient/api/BillingFlowParams;

    iput-object p3, p0, Lf/e/a/a/h;->e:Lcom/android/billingclient/api/SkuDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/e/a/a/h;->f:Lf/e/a/a/a;

    iget-object v1, v0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    const/4 v2, 0x5

    iget-object v0, v0, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lf/e/a/a/h;->d:Lcom/android/billingclient/api/BillingFlowParams;

    iget-object v5, v5, Lcom/android/billingclient/api/BillingFlowParams;->b:Ljava/lang/String;

    aput-object v5, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lf/e/a/a/h;->e:Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v0}, Lcom/android/billingclient/api/SkuDetails;->d()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const-string/jumbo v6, "subs"

    invoke-interface/range {v1 .. v7}, Lf/h/a/f/i/m/a;->A(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
