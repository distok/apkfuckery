.class public final Lf/e/a/a/i;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Lcom/android/billingclient/api/SkuDetails;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Lcom/android/billingclient/api/BillingFlowParams;

.field public final synthetic h:Landroid/os/Bundle;

.field public final synthetic i:Lf/e/a/a/a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a;ILcom/android/billingclient/api/SkuDetails;Ljava/lang/String;Lcom/android/billingclient/api/BillingFlowParams;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/i;->i:Lf/e/a/a/a;

    iput p2, p0, Lf/e/a/a/i;->d:I

    iput-object p3, p0, Lf/e/a/a/i;->e:Lcom/android/billingclient/api/SkuDetails;

    iput-object p4, p0, Lf/e/a/a/i;->f:Ljava/lang/String;

    iput-object p5, p0, Lf/e/a/a/i;->g:Lcom/android/billingclient/api/BillingFlowParams;

    iput-object p6, p0, Lf/e/a/a/i;->h:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/e/a/a/i;->i:Lf/e/a/a/a;

    iget-object v1, v0, Lf/e/a/a/a;->f:Lf/h/a/f/i/m/a;

    iget v2, p0, Lf/e/a/a/i;->d:I

    iget-object v0, v0, Lf/e/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lf/e/a/a/i;->e:Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v0}, Lcom/android/billingclient/api/SkuDetails;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lf/e/a/a/i;->f:Ljava/lang/String;

    iget-object v0, p0, Lf/e/a/a/i;->g:Lcom/android/billingclient/api/BillingFlowParams;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lf/e/a/a/i;->h:Landroid/os/Bundle;

    invoke-interface/range {v1 .. v7}, Lf/h/a/f/i/m/a;->n0(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
