.class public final Lf/e/a/a/l;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/android/billingclient/api/BillingResult;

.field public final synthetic e:Lf/e/a/a/a$a;


# direct methods
.method public constructor <init>(Lf/e/a/a/a$a;Lcom/android/billingclient/api/BillingResult;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/l;->e:Lf/e/a/a/a$a;

    iput-object p2, p0, Lf/e/a/a/l;->d:Lcom/android/billingclient/api/BillingResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/e/a/a/l;->e:Lf/e/a/a/a$a;

    iget-object v0, v0, Lf/e/a/a/a$a;->d:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/e/a/a/l;->e:Lf/e/a/a/a$a;

    iget-object v1, v1, Lf/e/a/a/a$a;->f:Lf/e/a/a/b;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lf/e/a/a/l;->d:Lcom/android/billingclient/api/BillingResult;

    invoke-interface {v1, v2}, Lf/e/a/a/b;->onBillingSetupFinished(Lcom/android/billingclient/api/BillingResult;)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
