.class public final Lf/e/a/a/c0;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/e/a/a/d;

.field public final synthetic e:Lcom/android/billingclient/api/BillingResult;

.field public final synthetic f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lf/e/a/a/d;Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/c0;->d:Lf/e/a/a/d;

    iput-object p2, p0, Lf/e/a/a/c0;->e:Lcom/android/billingclient/api/BillingResult;

    iput-object p3, p0, Lf/e/a/a/c0;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    const-string v0, "BillingClient"

    const-string v1, "Successfully consumed purchase."

    invoke-static {v0, v1}, Lf/h/a/f/i/m/b;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/e/a/a/c0;->d:Lf/e/a/a/d;

    iget-object v1, p0, Lf/e/a/a/c0;->e:Lcom/android/billingclient/api/BillingResult;

    iget-object v2, p0, Lf/e/a/a/c0;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lf/e/a/a/d;->onConsumeResponse(Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V

    return-void
.end method
