.class public final Lf/e/a/a/q;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"


# static fields
.field public static final a:Lcom/android/billingclient/api/BillingResult;

.field public static final b:Lcom/android/billingclient/api/BillingResult;

.field public static final c:Lcom/android/billingclient/api/BillingResult;

.field public static final d:Lcom/android/billingclient/api/BillingResult;

.field public static final e:Lcom/android/billingclient/api/BillingResult;

.field public static final f:Lcom/android/billingclient/api/BillingResult;

.field public static final g:Lcom/android/billingclient/api/BillingResult;

.field public static final h:Lcom/android/billingclient/api/BillingResult;

.field public static final i:Lcom/android/billingclient/api/BillingResult;

.field public static final j:Lcom/android/billingclient/api/BillingResult;

.field public static final k:Lcom/android/billingclient/api/BillingResult;

.field public static final l:Lcom/android/billingclient/api/BillingResult;

.field public static final m:Lcom/android/billingclient/api/BillingResult;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v2, "Google Play In-app Billing API version is less than 3"

    iput-object v2, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->a:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v2, "Google Play In-app Billing API version is less than 9"

    iput-object v2, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v2, "Billing service unavailable on device."

    iput-object v2, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->b:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v2, 0x5

    iput v2, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Client is already in the process of connecting to billing service."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->c:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v1, "Play Store version installed does not support cross selling products."

    iput-object v1, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->d:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v2, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v1, "The list of SKUs can\'t be empty."

    iput-object v1, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->e:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v2, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v1, "SKU type can\'t be empty."

    iput-object v1, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->f:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Client does not support extra params."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->g:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Client does not support the feature."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Client does not support get purchase history."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v2, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Invalid purchase token."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v3, 0x6

    iput v3, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "An internal error occurred."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->h:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v3, 0x4

    iput v3, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Item is unavailable for purchase."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v2, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "SKU can\'t be null."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v2, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "SKU type can\'t be null."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v3, 0x0

    iput v3, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->i:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v3, -0x1

    iput v3, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Service connection is disconnected."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    const/4 v3, -0x3

    iput v3, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Timeout communicating with service."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->k:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v3, "Client doesn\'t support subscriptions."

    iput-object v3, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->l:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v1, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v1, "Client doesn\'t support subscriptions update."

    iput-object v1, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v0

    sput-object v0, Lf/e/a/a/q;->m:Lcom/android/billingclient/api/BillingResult;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v0

    iput v2, v0, Lcom/android/billingclient/api/BillingResult$a;->a:I

    const-string v1, "Unknown feature"

    iput-object v1, v0, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    return-void
.end method
