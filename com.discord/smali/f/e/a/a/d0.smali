.class public final Lf/e/a/a/d0;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/lang/Exception;

.field public final synthetic e:Lf/e/a/a/d;

.field public final synthetic f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Exception;Lf/e/a/a/d;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/d0;->d:Ljava/lang/Exception;

    iput-object p2, p0, Lf/e/a/a/d0;->e:Lf/e/a/a/d;

    iput-object p3, p0, Lf/e/a/a/d0;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/e/a/a/d0;->d:Ljava/lang/Exception;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Error consuming purchase; ex: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BillingClient"

    invoke-static {v1, v0}, Lf/h/a/f/i/m/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/e/a/a/d0;->e:Lf/e/a/a/d;

    sget-object v1, Lf/e/a/a/q;->j:Lcom/android/billingclient/api/BillingResult;

    iget-object v2, p0, Lf/e/a/a/d0;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lf/e/a/a/d;->onConsumeResponse(Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V

    return-void
.end method
