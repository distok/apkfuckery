.class public final Lf/e/a/a/x;
.super Ljava/lang/Object;
.source "com.android.billingclient:billing@@3.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/android/billingclient/api/SkuDetails$a;

.field public final synthetic e:Lf/e/a/a/v;


# direct methods
.method public constructor <init>(Lf/e/a/a/v;Lcom/android/billingclient/api/SkuDetails$a;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/x;->e:Lf/e/a/a/v;

    iput-object p2, p0, Lf/e/a/a/x;->d:Lcom/android/billingclient/api/SkuDetails$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lf/e/a/a/x;->e:Lf/e/a/a/v;

    iget-object v0, v0, Lf/e/a/a/v;->f:Lf/e/a/a/g;

    invoke-static {}, Lcom/android/billingclient/api/BillingResult;->a()Lcom/android/billingclient/api/BillingResult$a;

    move-result-object v1

    iget-object v2, p0, Lf/e/a/a/x;->d:Lcom/android/billingclient/api/SkuDetails$a;

    iget v3, v2, Lcom/android/billingclient/api/SkuDetails$a;->b:I

    iput v3, v1, Lcom/android/billingclient/api/BillingResult$a;->a:I

    iget-object v2, v2, Lcom/android/billingclient/api/SkuDetails$a;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/android/billingclient/api/BillingResult$a;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/android/billingclient/api/BillingResult$a;->a()Lcom/android/billingclient/api/BillingResult;

    move-result-object v1

    iget-object v2, p0, Lf/e/a/a/x;->d:Lcom/android/billingclient/api/SkuDetails$a;

    iget-object v2, v2, Lcom/android/billingclient/api/SkuDetails$a;->a:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lf/e/a/a/g;->onSkuDetailsResponse(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-void
.end method
