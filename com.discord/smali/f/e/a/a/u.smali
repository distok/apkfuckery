.class public final Lf/e/a/a/u;
.super Landroid/content/BroadcastReceiver;
.source "com.android.billingclient:billing@@3.0.0"


# instance fields
.field public final a:Lf/e/a/a/e;

.field public b:Z

.field public final synthetic c:Lf/e/a/a/t;


# direct methods
.method public constructor <init>(Lf/e/a/a/t;Lf/e/a/a/e;Lf/e/a/a/s;)V
    .locals 0

    iput-object p1, p0, Lf/e/a/a/u;->c:Lf/e/a/a/t;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p2, p0, Lf/e/a/a/u;->a:Lf/e/a/a/e;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string p1, "BillingBroadcastManager"

    invoke-static {p2, p1}, Lf/h/a/f/i/m/b;->d(Landroid/content/Intent;Ljava/lang/String;)Lcom/android/billingclient/api/BillingResult;

    move-result-object p1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    invoke-static {p2}, Lf/h/a/f/i/m/b;->b(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object p2

    iget-object v0, p0, Lf/e/a/a/u;->a:Lf/e/a/a/e;

    invoke-interface {v0, p1, p2}, Lf/e/a/a/e;->onPurchasesUpdated(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V

    return-void
.end method
