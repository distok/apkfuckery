.class public final Lf/e/b/a/a$b;
.super Ljava/lang/Object;
.source "InstallReferrerClientImpl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/e/b/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field public final d:Lf/e/b/a/b;

.field public final synthetic e:Lf/e/b/a/a;


# direct methods
.method public constructor <init>(Lf/e/b/a/a;Lf/e/b/a/b;Lf/e/b/a/a$a;)V
    .locals 0

    iput-object p1, p0, Lf/e/b/a/a$b;->e:Lf/e/b/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_0

    iput-object p2, p0, Lf/e/b/a/a$b;->d:Lf/e/b/a/b;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Please specify a listener to know when setup is done."

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    const-string p1, "InstallReferrerClient"

    const-string v0, "Install Referrer service connected."

    invoke-static {p1, v0}, Ls/a/b/b/a;->K(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lf/e/b/a/a$b;->e:Lf/e/b/a/a;

    sget v0, Lf/h/a/d/a/a$a;->a:I

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const-string v0, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService"

    invoke-interface {p2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lf/h/a/d/a/a;

    if-eqz v1, :cond_1

    move-object p2, v0

    check-cast p2, Lf/h/a/d/a/a;

    goto :goto_0

    :cond_1
    new-instance v0, Lf/h/a/d/a/a$a$a;

    invoke-direct {v0, p2}, Lf/h/a/d/a/a$a$a;-><init>(Landroid/os/IBinder;)V

    move-object p2, v0

    :goto_0
    iput-object p2, p1, Lf/e/b/a/a;->c:Lf/h/a/d/a/a;

    iget-object p1, p0, Lf/e/b/a/a$b;->e:Lf/e/b/a/a;

    const/4 p2, 0x2

    iput p2, p1, Lf/e/b/a/a;->a:I

    iget-object p1, p0, Lf/e/b/a/a$b;->d:Lf/e/b/a/b;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Lf/e/b/a/b;->onInstallReferrerSetupFinished(I)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    const-string p1, "InstallReferrerClient"

    const-string v0, "Install Referrer service disconnected."

    invoke-static {p1, v0}, Ls/a/b/b/a;->L(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lf/e/b/a/a$b;->e:Lf/e/b/a/a;

    const/4 v0, 0x0

    iput-object v0, p1, Lf/e/b/a/a;->c:Lf/h/a/d/a/a;

    const/4 v0, 0x0

    iput v0, p1, Lf/e/b/a/a;->a:I

    iget-object p1, p0, Lf/e/b/a/a$b;->d:Lf/e/b/a/b;

    invoke-interface {p1}, Lf/e/b/a/b;->onInstallReferrerServiceDisconnected()V

    return-void
.end method
