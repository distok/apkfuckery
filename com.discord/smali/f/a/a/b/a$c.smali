.class public final Lf/a/a/b/a$c;
.super Lx/m/c/k;
.source "PremiumGuildSubscriptionCancelDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/b/a;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/b/d$d;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/b/a;


# direct methods
.method public constructor <init>(Lf/a/a/b/a;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/b/a$c;->this$0:Lf/a/a/b/a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 18

    move-object/from16 v0, p1

    check-cast v0, Lf/a/a/b/d$d;

    move-object/from16 v1, p0

    iget-object v2, v1, Lf/a/a/b/a$c;->this$0:Lf/a/a/b/a;

    const-string v3, "it"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v2, Lf/a/a/b/a;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v4, Lf/a/a/b/a;->k:[Lkotlin/reflect/KProperty;

    const/4 v5, 0x2

    aget-object v6, v4, v5

    invoke-interface {v3, v2, v6}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-boolean v6, v0, Lf/a/a/b/d$d;->a:Z

    const/16 v7, 0x8

    const/4 v8, 0x0

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    const/16 v6, 0x8

    :goto_0
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Lf/a/a/b/a;->f()Landroid/view/View;

    move-result-object v3

    iget-boolean v6, v0, Lf/a/a/b/d$d;->b:Z

    if-eqz v6, :cond_1

    const/4 v7, 0x0

    :cond_1
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Lf/a/a/b/a;->f()Landroid/view/View;

    move-result-object v3

    iget-boolean v6, v0, Lf/a/a/b/d$d;->c:Z

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static {v3, v6, v7, v5, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAndAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    iget-boolean v3, v0, Lf/a/a/b/d$d;->e:Z

    invoke-virtual {v2, v3}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v2}, Lf/a/a/b/a;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object v3

    iget-boolean v5, v0, Lf/a/a/b/d$d;->d:Z

    invoke-virtual {v3, v5}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    iget-object v3, v2, Lf/a/a/b/a;->i:Lkotlin/properties/ReadOnlyProperty;

    const/4 v5, 0x5

    aget-object v5, v4, v5

    invoke-interface {v3, v2, v5}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/app/AppViewFlipper;

    instance-of v5, v0, Lf/a/a/b/d$d$f;

    const/4 v6, 0x1

    xor-int/2addr v5, v6

    invoke-virtual {v3, v5}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    instance-of v3, v0, Lf/a/a/b/d$d$e;

    const-string v5, "getString(\n             \u2026eContext())\n            )"

    const-string v7, "requireContext()"

    if-eqz v3, :cond_3

    check-cast v0, Lf/a/a/b/d$d$e;

    iget-boolean v3, v0, Lf/a/a/b/d$d$e;->g:Z

    if-eqz v3, :cond_2

    const v3, 0x7f12135e

    goto :goto_1

    :cond_2
    const v3, 0x7f12135d

    :goto_1
    iget-object v9, v2, Lf/a/a/b/a;->e:Lkotlin/properties/ReadOnlyProperty;

    aget-object v4, v4, v6

    invoke-interface {v9, v2, v4}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v6, v6, [Ljava/lang/Object;

    sget-object v10, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    iget-object v0, v0, Lf/a/a/b/d$d$e;->f:Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodEnd()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v0, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1c

    const/16 v17, 0x0

    const/4 v14, 0x0

    invoke-static/range {v10 .. v17}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/text/DateFormat;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v3, v6}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v15, 0x3c

    const/16 v16, 0x0

    move-object v14, v0

    invoke-static/range {v9 .. v16}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/b/a;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const v3, 0x7f060238

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/discord/views/LoadingButton;->setBackgroundColor(I)V

    goto/16 :goto_2

    :cond_3
    instance-of v3, v0, Lf/a/a/b/d$d$b;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lf/a/a/b/a;->d:Lkotlin/properties/ReadOnlyProperty;

    aget-object v9, v4, v8

    invoke-interface {v3, v2, v9}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v9, 0x7f121369

    invoke-virtual {v2, v9}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lf/a/a/b/a;->e:Lkotlin/properties/ReadOnlyProperty;

    aget-object v4, v4, v6

    invoke-interface {v3, v2, v4}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f12136a

    new-array v6, v6, [Ljava/lang/Object;

    sget-object v10, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    check-cast v0, Lf/a/a/b/d$d$b;

    iget-object v0, v0, Lf/a/a/b/d$d$b;->f:Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodEnd()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v0, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1c

    const/16 v17, 0x0

    const/4 v14, 0x0

    invoke-static/range {v10 .. v17}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/text/DateFormat;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v4, v6}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v15, 0x3c

    const/16 v16, 0x0

    move-object v14, v0

    invoke-static/range {v9 .. v16}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/b/a;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const v3, 0x7f1211ee

    invoke-virtual {v2, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/b/a;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const v3, 0x7f060057

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/discord/views/LoadingButton;->setBackgroundColor(I)V

    goto :goto_2

    :cond_4
    instance-of v3, v0, Lf/a/a/b/d$d$c;

    if-eqz v3, :cond_6

    check-cast v0, Lf/a/a/b/d$d$c;

    iget-object v0, v0, Lf/a/a/b/d$d$c;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0xc

    invoke-static {v3, v0, v8, v9, v4}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    :cond_5
    invoke-virtual {v2}, Lcom/discord/app/AppDialog;->dismiss()V

    :cond_6
    :goto_2
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
