.class public final Lf/a/a/b/d$a;
.super Lx/m/c/k;
.source "PremiumGuildSubscriptionCancelViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/b/d;-><init>(JLcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/b/d$c;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/b/d;


# direct methods
.method public constructor <init>(Lf/a/a/b/d;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/b/d$a;->this$0:Lf/a/a/b/d;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    check-cast p1, Lf/a/a/b/d$c;

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/a/b/d$a;->this$0:Lf/a/a/b/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/a/a/b/d$c;->a:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iget-object p1, p1, Lf/a/a/b/d$c;->b:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lf/a/a/b/d$d$b;

    if-nez v2, :cond_a

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lf/a/a/b/d$d$c;

    if-eqz v2, :cond_0

    goto/16 :goto_5

    :cond_0
    instance-of v2, v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    if-nez v2, :cond_9

    instance-of v2, p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    if-eqz v2, :cond_1

    goto/16 :goto_3

    :cond_1
    instance-of v2, v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    const v3, 0x7f121363

    if-nez v2, :cond_8

    instance-of v2, p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    if-eqz v2, :cond_2

    goto/16 :goto_2

    :cond_2
    instance-of v2, v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v2, :cond_7

    instance-of v2, p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v2, :cond_7

    check-cast p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelSubscription;

    if-nez p1, :cond_3

    new-instance p1, Lf/a/a/b/d$d$c;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p1, v1}, Lf/a/a/b/d$d$c;-><init>(Ljava/lang/Integer;)V

    goto :goto_4

    :cond_3
    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lf/a/a/b/d$d$b;

    if-eqz v2, :cond_4

    new-instance v1, Lf/a/a/b/d$d$b;

    invoke-direct {v1, p1}, Lf/a/a/b/d$d$b;-><init>(Lcom/discord/models/domain/ModelSubscription;)V

    move-object p1, v1

    goto :goto_4

    :cond_4
    new-instance v2, Lf/a/a/b/d$d$e;

    check-cast v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object v1

    iget-wide v3, v0, Lf/a/a/b/d;->d:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_6

    const/4 v1, 0x1

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    :goto_1
    invoke-direct {v2, p1, v1}, Lf/a/a/b/d$d$e;-><init>(Lcom/discord/models/domain/ModelSubscription;Z)V

    move-object p1, v2

    goto :goto_4

    :cond_7
    new-instance p1, Lf/a/a/b/d$d$c;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p1, v1}, Lf/a/a/b/d$d$c;-><init>(Ljava/lang/Integer;)V

    goto :goto_4

    :cond_8
    :goto_2
    new-instance p1, Lf/a/a/b/d$d$c;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p1, v1}, Lf/a/a/b/d$d$c;-><init>(Ljava/lang/Integer;)V

    goto :goto_4

    :cond_9
    :goto_3
    sget-object p1, Lf/a/a/b/d$d$f;->f:Lf/a/a/b/d$d$f;

    :goto_4
    invoke-virtual {v0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_a
    :goto_5
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
