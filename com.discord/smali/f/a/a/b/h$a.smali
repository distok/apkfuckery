.class public final Lf/a/a/b/h$a;
.super Lx/m/c/k;
.source "PremiumGuildSubscriptionUncancelViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/b/h;-><init>(JLcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/b/h$c;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/b/h;


# direct methods
.method public constructor <init>(Lf/a/a/b/h;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/b/h$a;->this$0:Lf/a/a/b/h;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lf/a/a/b/h$c;

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/a/b/h$a;->this$0:Lf/a/a/b/h;

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lf/a/a/b/h$d$d;

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lf/a/a/b/h$d$a;

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object p1, p1, Lf/a/a/b/h$c;->a:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    instance-of v1, p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    if-eqz v1, :cond_1

    sget-object p1, Lf/a/a/b/h$d$c;->a:Lf/a/a/b/h$d$c;

    goto :goto_0

    :cond_1
    instance-of v1, p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    const v2, 0x7f121363

    if-eqz v1, :cond_2

    new-instance p1, Lf/a/a/b/h$d$a;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p1, v1}, Lf/a/a/b/h$d$a;-><init>(Ljava/lang/Integer;)V

    goto :goto_0

    :cond_2
    instance-of v1, p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v1, :cond_4

    check-cast p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelSubscription;

    if-nez p1, :cond_3

    new-instance p1, Lf/a/a/b/h$d$a;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p1, v1}, Lf/a/a/b/h$d$a;-><init>(Ljava/lang/Integer;)V

    goto :goto_0

    :cond_3
    new-instance v1, Lf/a/a/b/h$d$b;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, v2}, Lf/a/a/b/h$d$b;-><init>(Lcom/discord/models/domain/ModelSubscription;ZZ)V

    move-object p1, v1

    goto :goto_0

    :cond_4
    new-instance p1, Lf/a/a/b/h$d$a;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p1, v1, v2}, Lf/a/a/b/h$d$a;-><init>(Ljava/lang/Integer;I)V

    :goto_0
    invoke-virtual {v0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_5
    :goto_1
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
