.class public final Lf/a/a/b/b$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/b/b;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lf/a/a/b/b$a;->d:I

    iput-object p2, p0, Lf/a/a/b/b$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 13

    sget-object p1, Lf/a/a/b/h$d$c;->a:Lf/a/a/b/h$d$c;

    iget v0, p0, Lf/a/a/b/b$a;->d:I

    const-string/jumbo v1, "viewModel"

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_7

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lf/a/a/b/b$a;->e:Ljava/lang/Object;

    check-cast v0, Lf/a/a/b/b;

    iget-object v0, v0, Lf/a/a/b/b;->k:Lf/a/a/b/h;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/a/a/b/h$d;

    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    instance-of p1, v1, Lf/a/a/b/h$d$b;

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object p1

    const-string v1, "null cannot be cast to non-null type com.discord.dialogs.premiumguild.PremiumGuildSubscriptionUncancelViewModel.ViewState.Loaded"

    invoke-static {p1, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lf/a/a/b/h$d$b;

    iget-boolean p1, p1, Lf/a/a/b/h$d$b;->b:Z

    if-nez p1, :cond_4

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object p1

    instance-of v1, p1, Lf/a/a/b/h$d$b;

    if-nez v1, :cond_1

    move-object p1, v3

    :cond_1
    check-cast p1, Lf/a/a/b/h$d$b;

    if-eqz p1, :cond_4

    const/4 v1, 0x0

    const/4 v4, 0x5

    invoke-static {p1, v3, v2, v1, v4}, Lf/a/a/b/h$d$b;->a(Lf/a/a/b/h$d$b;Lcom/discord/models/domain/ModelSubscription;ZZI)Lf/a/a/b/h$d$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-object v8, p1, Lf/a/a/b/h$d$b;->a:Lcom/discord/models/domain/ModelSubscription;

    sget-object v4, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

    iget-object v5, v0, Lf/a/a/b/h;->g:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v6, v0, Lf/a/a/b/h;->d:J

    iget-object v9, v0, Lf/a/a/b/h;->f:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-virtual/range {v4 .. v9}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->uncancelSubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;Lcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v0, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lf/a/a/b/h;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lf/a/a/b/j;

    invoke-direct {v10, v0, p1}, Lf/a/a/b/j;-><init>(Lf/a/a/b/h;Lf/a/a/b/h$d$b;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    instance-of p1, v1, Lf/a/a/b/h$d$d;

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    instance-of p1, v1, Lf/a/a/b/h$d$a;

    if-eqz p1, :cond_4

    :goto_0
    new-instance p1, Lf/a/a/b/h$d$a;

    invoke-direct {p1, v3, v2}, Lf/a/a/b/h$d$a;-><init>(Ljava/lang/Integer;I)V

    invoke-virtual {v0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_6
    throw v3

    :cond_7
    iget-object v0, p0, Lf/a/a/b/b$a;->e:Ljava/lang/Object;

    check-cast v0, Lf/a/a/b/b;

    iget-object v0, v0, Lf/a/a/b/b;->k:Lf/a/a/b/h;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/a/a/b/h$d;

    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    goto :goto_3

    :cond_8
    instance-of p1, v1, Lf/a/a/b/h$d$b;

    if-eqz p1, :cond_9

    goto :goto_2

    :cond_9
    instance-of p1, v1, Lf/a/a/b/h$d$a;

    if-eqz p1, :cond_a

    :goto_2
    new-instance p1, Lf/a/a/b/h$d$a;

    invoke-direct {p1, v3, v2}, Lf/a/a/b/h$d$a;-><init>(Ljava/lang/Integer;I)V

    invoke-virtual {v0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_a
    :goto_3
    return-void

    :cond_b
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method
