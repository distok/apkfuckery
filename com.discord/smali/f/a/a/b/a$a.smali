.class public final Lf/a/a/b/a$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/b/a;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lf/a/a/b/a$a;->d:I

    iput-object p2, p0, Lf/a/a/b/a$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 13

    iget p1, p0, Lf/a/a/b/a$a;->d:I

    const-string/jumbo v0, "viewModel"

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_9

    if-ne p1, v1, :cond_8

    iget-object p1, p0, Lf/a/a/b/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/b/a;

    iget-object p1, p1, Lf/a/a/b/a;->j:Lf/a/a/b/d;

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/a/a/b/d$d;

    instance-of v3, v0, Lf/a/a/b/d$d$f;

    if-eqz v3, :cond_0

    goto :goto_2

    :cond_0
    instance-of v3, v0, Lf/a/a/b/d$d$a;

    if-eqz v3, :cond_1

    goto :goto_2

    :cond_1
    instance-of v3, v0, Lf/a/a/b/d$d$e;

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    instance-of v3, v0, Lf/a/a/b/d$d$d;

    if-eqz v3, :cond_5

    :goto_0
    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/a/a/b/d$d;

    instance-of v1, v0, Lf/a/a/b/d$d$e;

    if-eqz v1, :cond_3

    check-cast v0, Lf/a/a/b/d$d$e;

    iget-object v0, v0, Lf/a/a/b/d$d$e;->f:Lcom/discord/models/domain/ModelSubscription;

    goto :goto_1

    :cond_3
    instance-of v1, v0, Lf/a/a/b/d$d$d;

    if-eqz v1, :cond_4

    check-cast v0, Lf/a/a/b/d$d$d;

    iget-object v0, v0, Lf/a/a/b/d$d$d;->f:Lcom/discord/models/domain/ModelSubscription;

    goto :goto_1

    :cond_4
    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_6

    new-instance v1, Lf/a/a/b/d$d$a;

    invoke-direct {v1, v0}, Lf/a/a/b/d$d$a;-><init>(Lcom/discord/models/domain/ModelSubscription;)V

    invoke-virtual {p1, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    sget-object v3, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

    iget-object v4, p1, Lf/a/a/b/d;->g:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v5, p1, Lf/a/a/b/d;->d:J

    iget-object v8, p1, Lf/a/a/b/d;->e:Lcom/discord/stores/StorePremiumGuildSubscription;

    move-object v7, v0

    invoke-virtual/range {v3 .. v8}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->cancelSubscriptionSlot(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;Lcom/discord/stores/StorePremiumGuildSubscription;)Lrx/Observable;

    move-result-object v1

    const/4 v3, 0x2

    invoke-static {v1, p1, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lf/a/a/b/d;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lf/a/a/b/f;

    invoke-direct {v10, p1, v0}, Lf/a/a/b/f;-><init>(Lf/a/a/b/d;Lcom/discord/models/domain/ModelSubscription;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_2

    :cond_5
    instance-of v0, v0, Lf/a/a/b/d$d$b;

    if-eqz v0, :cond_6

    new-instance v0, Lf/a/a/b/d$d$c;

    invoke-direct {v0, v2, v1}, Lf/a/a/b/d$d$c;-><init>(Ljava/lang/Integer;I)V

    invoke-virtual {p1, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_6
    :goto_2
    return-void

    :cond_7
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_8
    throw v2

    :cond_9
    iget-object p1, p0, Lf/a/a/b/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/b/a;

    iget-object p1, p1, Lf/a/a/b/a;->j:Lf/a/a/b/d;

    if-eqz p1, :cond_f

    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/a/a/b/d$d;

    instance-of v3, v0, Lf/a/a/b/d$d$f;

    if-eqz v3, :cond_a

    goto :goto_4

    :cond_a
    instance-of v3, v0, Lf/a/a/b/d$d$a;

    if-eqz v3, :cond_b

    goto :goto_4

    :cond_b
    instance-of v3, v0, Lf/a/a/b/d$d$e;

    if-eqz v3, :cond_c

    goto :goto_3

    :cond_c
    instance-of v3, v0, Lf/a/a/b/d$d$d;

    if-eqz v3, :cond_d

    goto :goto_3

    :cond_d
    instance-of v0, v0, Lf/a/a/b/d$d$b;

    if-eqz v0, :cond_e

    :goto_3
    new-instance v0, Lf/a/a/b/d$d$c;

    invoke-direct {v0, v2, v1}, Lf/a/a/b/d$d$c;-><init>(Ljava/lang/Integer;I)V

    invoke-virtual {p1, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_e
    :goto_4
    return-void

    :cond_f
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
