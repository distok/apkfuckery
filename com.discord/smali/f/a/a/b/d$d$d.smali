.class public final Lf/a/a/b/d$d$d;
.super Lf/a/a/b/d$d;
.source "PremiumGuildSubscriptionCancelViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/a/b/d$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field public final f:Lcom/discord/models/domain/ModelSubscription;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelSubscription;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lf/a/a/b/d$d;-><init>(ZZZZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lf/a/a/b/d$d$d;->f:Lcom/discord/models/domain/ModelSubscription;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lf/a/a/b/d$d$d;

    if-eqz v0, :cond_0

    check-cast p1, Lf/a/a/b/d$d$d;

    iget-object v0, p0, Lf/a/a/b/d$d$d;->f:Lcom/discord/models/domain/ModelSubscription;

    iget-object p1, p1, Lf/a/a/b/d$d$d;->f:Lcom/discord/models/domain/ModelSubscription;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lf/a/a/b/d$d$d;->f:Lcom/discord/models/domain/ModelSubscription;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "FailureCancelling(subscription="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/a/a/b/d$d$d;->f:Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
