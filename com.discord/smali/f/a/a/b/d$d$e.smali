.class public final Lf/a/a/b/d$d$e;
.super Lf/a/a/b/d$d;
.source "PremiumGuildSubscriptionCancelViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/a/b/d$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# instance fields
.field public final f:Lcom/discord/models/domain/ModelSubscription;

.field public final g:Z


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelSubscription;Z)V
    .locals 8

    const-string/jumbo v0, "subscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lf/a/a/b/d$d;-><init>(ZZZZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lf/a/a/b/d$d$e;->f:Lcom/discord/models/domain/ModelSubscription;

    iput-boolean p2, p0, Lf/a/a/b/d$d$e;->g:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lf/a/a/b/d$d$e;

    if-eqz v0, :cond_0

    check-cast p1, Lf/a/a/b/d$d$e;

    iget-object v0, p0, Lf/a/a/b/d$d$e;->f:Lcom/discord/models/domain/ModelSubscription;

    iget-object v1, p1, Lf/a/a/b/d$d$e;->f:Lcom/discord/models/domain/ModelSubscription;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lf/a/a/b/d$d$e;->g:Z

    iget-boolean p1, p1, Lf/a/a/b/d$d$e;->g:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lf/a/a/b/d$d$e;->f:Lcom/discord/models/domain/ModelSubscription;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lf/a/a/b/d$d$e;->g:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(subscription="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/a/a/b/d$d$e;->f:Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isFromInventory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/a/b/d$d$e;->g:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
