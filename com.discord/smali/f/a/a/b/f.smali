.class public final Lf/a/a/b/f;
.super Lx/m/c/k;
.source "PremiumGuildSubscriptionCancelViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $subscription:Lcom/discord/models/domain/ModelSubscription;

.field public final synthetic this$0:Lf/a/a/b/d;


# direct methods
.method public constructor <init>(Lf/a/a/b/d;Lcom/discord/models/domain/ModelSubscription;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/b/f;->this$0:Lf/a/a/b/d;

    iput-object p2, p0, Lf/a/a/b/f;->$subscription:Lcom/discord/models/domain/ModelSubscription;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;

    const-string v0, "result"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/a/a/b/f;->this$0:Lf/a/a/b/d;

    new-instance v0, Lf/a/a/b/d$d$d;

    iget-object v1, p0, Lf/a/a/b/f;->$subscription:Lcom/discord/models/domain/ModelSubscription;

    invoke-direct {v0, v1}, Lf/a/a/b/d$d$d;-><init>(Lcom/discord/models/domain/ModelSubscription;)V

    invoke-virtual {p1, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/a/a/b/f;->this$0:Lf/a/a/b/d;

    new-instance v0, Lf/a/a/b/d$d$b;

    iget-object v1, p0, Lf/a/a/b/f;->$subscription:Lcom/discord/models/domain/ModelSubscription;

    invoke-direct {v0, v1}, Lf/a/a/b/d$d$b;-><init>(Lcom/discord/models/domain/ModelSubscription;)V

    invoke-virtual {p1, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
