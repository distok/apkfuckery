.class public final Lf/a/a/b/j;
.super Lx/m/c/k;
.source "PremiumGuildSubscriptionUncancelViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $loadedViewState:Lf/a/a/b/h$d$b;

.field public final synthetic this$0:Lf/a/a/b/h;


# direct methods
.method public constructor <init>(Lf/a/a/b/h;Lf/a/a/b/h$d$b;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/b/j;->this$0:Lf/a/a/b/h;

    iput-object p2, p0, Lf/a/a/b/j;->$loadedViewState:Lf/a/a/b/h$d$b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    check-cast p1, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils$ModifySubscriptionSlotResult;

    const-string v0, "result"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/a/a/b/j;->this$0:Lf/a/a/b/h;

    iget-object v1, p0, Lf/a/a/b/j;->$loadedViewState:Lf/a/a/b/h$d$b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v0, v4}, Lf/a/a/b/h$d$b;->a(Lf/a/a/b/h$d$b;Lcom/discord/models/domain/ModelSubscription;ZZI)Lf/a/a/b/h$d$b;

    move-result-object v0

    invoke-virtual {p1, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/a/a/b/j;->this$0:Lf/a/a/b/h;

    sget-object v0, Lf/a/a/b/h$d$d;->a:Lf/a/a/b/h$d$d;

    invoke-virtual {p1, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
