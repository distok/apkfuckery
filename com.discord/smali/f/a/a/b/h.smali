.class public final Lf/a/a/b/h;
.super Lf/a/b/l0;
.source "PremiumGuildSubscriptionUncancelViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/a/b/h$d;,
        Lf/a/a/b/h$c;,
        Lf/a/a/b/h$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lf/a/a/b/h$d;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:J

.field public final e:Lcom/discord/stores/StoreSubscriptions;

.field public final f:Lcom/discord/stores/StorePremiumGuildSubscription;

.field public final g:Lcom/discord/utilities/rest/RestAPI;


# direct methods
.method public constructor <init>(JLcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/stores/StoreSubscriptions;",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lrx/Observable<",
            "Lf/a/a/b/h$c;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "storeSubscriptions"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storePremiumGuildSubscription"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeObservable"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/a/b/h$d$c;->a:Lf/a/a/b/h$d$c;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-wide p1, p0, Lf/a/a/b/h;->d:J

    iput-object p3, p0, Lf/a/a/b/h;->e:Lcom/discord/stores/StoreSubscriptions;

    iput-object p4, p0, Lf/a/a/b/h;->f:Lcom/discord/stores/StorePremiumGuildSubscription;

    iput-object p5, p0, Lf/a/a/b/h;->g:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {p3}, Lcom/discord/stores/StoreSubscriptions;->fetchSubscriptions()V

    invoke-static {p6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lf/a/a/b/h;

    new-instance v6, Lf/a/a/b/h$a;

    invoke-direct {v6, p0}, Lf/a/a/b/h$a;-><init>(Lf/a/a/b/h;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
