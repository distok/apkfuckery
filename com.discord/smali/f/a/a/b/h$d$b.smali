.class public final Lf/a/a/b/h$d$b;
.super Lf/a/a/b/h$d;
.source "PremiumGuildSubscriptionUncancelViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/a/b/h$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Lcom/discord/models/domain/ModelSubscription;

.field public final b:Z

.field public final c:Z


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelSubscription;ZZ)V
    .locals 1

    const-string/jumbo v0, "subscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/a/b/h$d;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lf/a/a/b/h$d$b;->a:Lcom/discord/models/domain/ModelSubscription;

    iput-boolean p2, p0, Lf/a/a/b/h$d$b;->b:Z

    iput-boolean p3, p0, Lf/a/a/b/h$d$b;->c:Z

    return-void
.end method

.method public static a(Lf/a/a/b/h$d$b;Lcom/discord/models/domain/ModelSubscription;ZZI)Lf/a/a/b/h$d$b;
    .locals 1

    and-int/lit8 p1, p4, 0x1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/a/a/b/h$d$b;->a:Lcom/discord/models/domain/ModelSubscription;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    iget-boolean p2, p0, Lf/a/a/b/h$d$b;->b:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lf/a/a/b/h$d$b;->c:Z

    :cond_2
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo p0, "subscription"

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Lf/a/a/b/h$d$b;

    invoke-direct {p0, p1, p2, p3}, Lf/a/a/b/h$d$b;-><init>(Lcom/discord/models/domain/ModelSubscription;ZZ)V

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lf/a/a/b/h$d$b;

    if-eqz v0, :cond_0

    check-cast p1, Lf/a/a/b/h$d$b;

    iget-object v0, p0, Lf/a/a/b/h$d$b;->a:Lcom/discord/models/domain/ModelSubscription;

    iget-object v1, p1, Lf/a/a/b/h$d$b;->a:Lcom/discord/models/domain/ModelSubscription;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lf/a/a/b/h$d$b;->b:Z

    iget-boolean v1, p1, Lf/a/a/b/h$d$b;->b:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lf/a/a/b/h$d$b;->c:Z

    iget-boolean p1, p1, Lf/a/a/b/h$d$b;->c:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lf/a/a/b/h$d$b;->a:Lcom/discord/models/domain/ModelSubscription;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lf/a/a/b/h$d$b;->b:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lf/a/a/b/h$d$b;->c:Z

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(subscription="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/a/a/b/h$d$b;->a:Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", uncancelInProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/a/b/h$d$b;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/a/b/h$d$b;->c:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
