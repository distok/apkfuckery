.class public final Lf/a/a/b/b$c;
.super Lx/m/c/k;
.source "PremiumGuildSubscriptionUncancelDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/b/b;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/b/h$d;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/b/b;


# direct methods
.method public constructor <init>(Lf/a/a/b/b;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/b/b$c;->this$0:Lf/a/a/b/b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    check-cast p1, Lf/a/a/b/h$d;

    iget-object v0, p0, Lf/a/a/b/b$c;->this$0:Lf/a/a/b/b;

    const-string v1, "it"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lf/a/a/b/b;->j:Lkotlin/properties/ReadOnlyProperty;

    sget-object v2, Lf/a/a/b/b;->l:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x6

    aget-object v3, v2, v3

    invoke-interface {v1, v0, v3}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/app/AppViewFlipper;

    instance-of v3, p1, Lf/a/a/b/h$d$c;

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    invoke-virtual {v1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    iget-object v1, v0, Lf/a/a/b/b;->f:Lkotlin/properties/ReadOnlyProperty;

    const/4 v3, 0x2

    aget-object v5, v2, v3

    invoke-interface {v1, v0, v5}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    instance-of v5, p1, Lf/a/a/b/h$d$b;

    const/4 v6, 0x0

    if-eqz v5, :cond_0

    move-object v7, p1

    check-cast v7, Lf/a/a/b/h$d$b;

    iget-boolean v7, v7, Lf/a/a/b/h$d$b;->c:Z

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x8

    if-eqz v7, :cond_1

    const/4 v7, 0x0

    goto :goto_1

    :cond_1
    const/16 v7, 0x8

    :goto_1
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Lf/a/a/b/b;->f()Landroid/view/View;

    move-result-object v1

    if-eqz v5, :cond_2

    const/4 v8, 0x0

    :cond_2
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Lf/a/a/b/b;->f()Landroid/view/View;

    move-result-object v1

    if-eqz v5, :cond_3

    move-object v7, p1

    check-cast v7, Lf/a/a/b/h$d$b;

    iget-boolean v7, v7, Lf/a/a/b/h$d$b;->b:Z

    if-nez v7, :cond_3

    const/4 v7, 0x1

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    :goto_2
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v1, v7, v8, v3, v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAndAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    invoke-virtual {v0}, Lf/a/a/b/b;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object v1

    if-eqz v5, :cond_4

    move-object v3, p1

    check-cast v3, Lf/a/a/b/h$d$b;

    iget-boolean v3, v3, Lf/a/a/b/h$d$b;->b:Z

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v1, v3}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    sget-object v1, Lf/a/a/b/h$d$c;->a:Lf/a/a/b/h$d$c;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v4

    if-eqz v3, :cond_5

    if-eqz v5, :cond_5

    move-object v3, p1

    check-cast v3, Lf/a/a/b/h$d$b;

    iget-boolean v3, v3, Lf/a/a/b/h$d$b;->b:Z

    if-nez v3, :cond_5

    const/4 v3, 0x1

    goto :goto_4

    :cond_5
    const/4 v3, 0x0

    :goto_4
    invoke-virtual {v0, v3}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto/16 :goto_5

    :cond_6
    if-eqz v5, :cond_7

    iget-object p1, v0, Lf/a/a/b/b;->d:Lkotlin/properties/ReadOnlyProperty;

    aget-object v1, v2, v6

    invoke-interface {p1, v0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v1, 0x7f121381

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, v0, Lf/a/a/b/b;->e:Lkotlin/properties/ReadOnlyProperty;

    aget-object v1, v2, v4

    invoke-interface {p1, v0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v1, 0x7f12137e

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    :cond_7
    sget-object v1, Lf/a/a/b/h$d$d;->a:Lf/a/a/b/h$d$d;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object p1, v0, Lf/a/a/b/b;->d:Lkotlin/properties/ReadOnlyProperty;

    aget-object v1, v2, v6

    invoke-interface {p1, v0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v1, 0x7f12137d

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, v0, Lf/a/a/b/b;->i:Lkotlin/properties/ReadOnlyProperty;

    const/4 v1, 0x5

    aget-object v1, v2, v1

    invoke-interface {p1, v0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, v0, Lf/a/a/b/b;->e:Lkotlin/properties/ReadOnlyProperty;

    aget-object v1, v2, v4

    invoke-interface {p1, v0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v1, 0x7f12137c

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lf/a/a/b/b;->getConfirmBtn()Lcom/discord/views/LoadingButton;

    move-result-object p1

    const v1, 0x7f1211ee

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    :cond_8
    instance-of v1, p1, Lf/a/a/b/h$d$a;

    if-eqz v1, :cond_a

    check-cast p1, Lf/a/a/b/h$d$a;

    iget-object p1, p1, Lf/a/a/b/h$d$a;->a:Ljava/lang/Integer;

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v1, 0x4

    invoke-static {v0, p1, v6, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :cond_9
    invoke-virtual {v0}, Lcom/discord/app/AppDialog;->dismiss()V

    :cond_a
    :goto_5
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
