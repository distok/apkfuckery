.class public final synthetic Lf/a/a/b/e;
.super Lx/m/c/i;
.source "PremiumGuildSubscriptionCancelViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
        "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
        "Lf/a/a/b/d$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lf/a/a/b/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/a/b/e;

    invoke-direct {v0}, Lf/a/a/b/e;-><init>()V

    sput-object v0, Lf/a/a/b/e;->d:Lf/a/a/b/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Lf/a/a/b/d$c;

    const/4 v1, 0x2

    const-string v3, "<init>"

    const-string v4, "<init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    check-cast p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/a/b/d$c;

    invoke-direct {v0, p1, p2}, Lf/a/a/b/d$c;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V

    return-object v0
.end method
