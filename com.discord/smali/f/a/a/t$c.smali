.class public final Lf/a/a/t$c;
.super Ljava/lang/Object;
.source "WidgetEnableTwoFactorPasswordDialog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/t;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/a/a/t;

.field public final synthetic e:Landroid/view/View;


# direct methods
.method public constructor <init>(Lf/a/a/t;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/t$c;->d:Lf/a/a/t;

    iput-object p2, p0, Lf/a/a/t$c;->e:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 13

    iget-object p1, p0, Lf/a/a/t$c;->d:Lf/a/a/t;

    iget-object v0, p1, Lf/a/a/t;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lf/a/a/t;->h:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p1, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lf/a/a/t$c;->d:Lf/a/a/t;

    invoke-virtual {v0}, Lf/a/a/t;->f()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    new-instance v3, Lcom/discord/restapi/RestAPIParams$EnableMFA;

    const-string v4, "random code"

    const-string v5, "random secret"

    invoke-direct {v3, v4, v5, p1}, Lcom/discord/restapi/RestAPIParams$EnableMFA;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/discord/utilities/rest/RestAPI;->enableMFA(Lcom/discord/restapi/RestAPIParams$EnableMFA;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lf/a/a/t$c;->d:Lf/a/a/t;

    invoke-static {p1, v0, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    sget-object v10, Lf/a/a/t$c$a;->d:Lf/a/a/t$c$a;

    new-instance v8, Lf/a/a/t$c$b;

    invoke-direct {v8, p0}, Lf/a/a/t$c$b;-><init>(Lf/a/a/t$c;)V

    iget-object p1, p0, Lf/a/a/t$c;->d:Lf/a/a/t;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    iget-object p1, p0, Lf/a/a/t$c;->e:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x14

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
