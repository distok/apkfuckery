.class public final Lf/a/a/c/a/c$a;
.super Lx/m/c/k;
.source "AudioOutputSelectionDialogViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/c/a/c;-><init>(Lcom/discord/stores/StoreAudioDevices;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/c/a/c$c;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/c/a/c;


# direct methods
.method public constructor <init>(Lf/a/a/c/a/c;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/c/a/c$a;->this$0:Lf/a/a/c/a/c;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    check-cast p1, Lf/a/a/c/a/c$c;

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/a/c/a/c$a;->this$0:Lf/a/a/c/a/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p1, Lf/a/a/c/a/c$c;->a:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAvailableOutputDevices()Ljava/util/Set;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    instance-of v5, v4, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v5, :cond_0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    xor-int/lit8 v6, v3, 0x1

    if-eqz v6, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    move-object v7, v2

    new-instance v2, Lf/a/a/c/a/c$d$a;

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v5

    sget-object p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    sget-object p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    sget-object p1, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lf/a/a/c/a/c$d$a;-><init>(Lcom/discord/stores/StoreAudioDevices$OutputDevice;ZLcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;ZZZ)V

    invoke-virtual {v0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
