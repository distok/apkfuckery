.class public final Lf/a/a/c/a/c$c;
.super Ljava/lang/Object;
.source "AudioOutputSelectionDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/a/c/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field public final a:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V
    .locals 1

    const-string v0, "audioDevicesState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/a/c/a/c$c;->a:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lf/a/a/c/a/c$c;

    if-eqz v0, :cond_0

    check-cast p1, Lf/a/a/c/a/c$c;

    iget-object v0, p0, Lf/a/a/c/a/c$c;->a:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    iget-object p1, p1, Lf/a/a/c/a/c$c;->a:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lf/a/a/c/a/c$c;->a:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(audioDevicesState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/a/a/c/a/c$c;->a:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
