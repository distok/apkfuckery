.class public final Lf/a/a/c/a/a;
.super Lcom/discord/app/AppDialog;
.source "AudioOutputSelectionDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/a/c/a/a$b;
    }
.end annotation


# static fields
.field public static final synthetic j:[Lkotlin/reflect/KProperty;

.field public static final k:Lf/a/a/c/a/a$b;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lkotlin/properties/ReadOnlyProperty;

.field public final g:Lkotlin/properties/ReadOnlyProperty;

.field public final h:Lkotlin/properties/ReadOnlyProperty;

.field public i:Lf/a/a/c/a/c;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lf/a/a/c/a/a;

    const-string v3, "bluetoothRadioButton"

    const-string v4, "getBluetoothRadioButton()Landroid/widget/RadioButton;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lf/a/a/c/a/a;

    const-string/jumbo v6, "speakerRadioButton"

    const-string v7, "getSpeakerRadioButton()Landroid/widget/RadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lf/a/a/c/a/a;

    const-string/jumbo v6, "wiredRadioButton"

    const-string v7, "getWiredRadioButton()Landroid/widget/RadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lf/a/a/c/a/a;

    const-string v6, "earpieceRadioButton"

    const-string v7, "getEarpieceRadioButton()Landroid/widget/RadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lf/a/a/c/a/a;

    const-string v6, "radioGroup"

    const-string v7, "getRadioGroup()Landroid/widget/RadioGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lf/a/a/c/a/a;->j:[Lkotlin/reflect/KProperty;

    new-instance v0, Lf/a/a/c/a/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/a/a/c/a/a$b;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lf/a/a/c/a/a;->k:Lf/a/a/c/a/a$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a00a3

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/c/a/a;->d:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00a7

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/c/a/a;->e:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00a8

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/c/a/a;->f:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00a6

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/c/a/a;->g:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00a5

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/c/a/a;->h:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final f(Lf/a/a/c/a/a;)V
    .locals 12

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x12c

    invoke-static {v1, v2, v0}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n        .time\u2026S, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lf/a/a/c/a/a;

    new-instance v9, Lf/a/a/c/a/b;

    invoke-direct {v9, p0}, Lf/a/a/c/a/b;-><init>(Lf/a/a/c/a/a;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic g(Lf/a/a/c/a/a;)Lf/a/a/c/a/c;
    .locals 0

    iget-object p0, p0, Lf/a/a/c/a/a;->i:Lf/a/a/c/a/c;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string/jumbo p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0021

    return v0
.end method

.method public final h()Landroid/widget/RadioButton;
    .locals 3

    iget-object v0, p0, Lf/a/a/c/a/a;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lf/a/a/c/a/a;->j:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    return-object v0
.end method

.method public final i()Landroid/widget/RadioButton;
    .locals 3

    iget-object v0, p0, Lf/a/a/c/a/a;->g:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lf/a/a/c/a/a;->j:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    return-object v0
.end method

.method public final j()Landroid/widget/RadioButton;
    .locals 3

    iget-object v0, p0, Lf/a/a/c/a/a;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lf/a/a/c/a/a;->j:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    return-object v0
.end method

.method public final k()Landroid/widget/RadioButton;
    .locals 3

    iget-object v0, p0, Lf/a/a/c/a/a;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lf/a/a/c/a/a;->j:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lf/a/a/c/a/c$b;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lf/a/a/c/a/c$b;-><init>(Lcom/discord/stores/StoreAudioDevices;I)V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lf/a/a/c/a/c;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026logViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lf/a/a/c/a/c;

    iput-object p1, p0, Lf/a/a/c/a/a;->i:Lf/a/a/c/a/c;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f040158

    invoke-static {p1, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p1

    invoke-virtual {p0}, Lf/a/a/c/a/a;->h()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-virtual {p0}, Lf/a/a/c/a/a;->j()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-virtual {p0}, Lf/a/a/c/a/a;->k()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-virtual {p0}, Lf/a/a/c/a/a;->i()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-virtual {p0}, Lf/a/a/c/a/a;->h()Landroid/widget/RadioButton;

    move-result-object p1

    new-instance v0, Lf/a/a/c/a/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lf/a/a/c/a/a$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lf/a/a/c/a/a;->j()Landroid/widget/RadioButton;

    move-result-object p1

    new-instance v0, Lf/a/a/c/a/a$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lf/a/a/c/a/a$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lf/a/a/c/a/a;->k()Landroid/widget/RadioButton;

    move-result-object p1

    new-instance v0, Lf/a/a/c/a/a$a;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lf/a/a/c/a/a$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lf/a/a/c/a/a;->i()Landroid/widget/RadioButton;

    move-result-object p1

    new-instance v0, Lf/a/a/c/a/a$a;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lf/a/a/c/a/a$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppDialog;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lf/a/a/c/a/a;->i:Lf/a/a/c/a/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lf/a/a/c/a/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lf/a/a/c/a/a$c;

    invoke-direct {v7, p0}, Lf/a/a/c/a/a$c;-><init>(Lf/a/a/c/a/a;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string/jumbo v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
