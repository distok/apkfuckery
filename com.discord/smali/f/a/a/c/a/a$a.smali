.class public final Lf/a/a/c/a/a$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/c/a/a;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lf/a/a/c/a/a$a;->d:I

    iput-object p2, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget p1, p0, Lf/a/a/c/a/a$a;->d:I

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->g(Lf/a/a/c/a/a;)Lf/a/a/c/a/c;

    move-result-object p1

    iget-object p1, p1, Lf/a/a/c/a/c;->d:Lcom/discord/stores/StoreAudioDevices;

    sget-object v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAudioDevices;->selectAudioOutput(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->f(Lf/a/a/c/a/a;)V

    return-void

    :cond_0
    throw v0

    :cond_1
    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->g(Lf/a/a/c/a/a;)Lf/a/a/c/a/c;

    move-result-object p1

    iget-object p1, p1, Lf/a/a/c/a/c;->d:Lcom/discord/stores/StoreAudioDevices;

    sget-object v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAudioDevices;->selectAudioOutput(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->f(Lf/a/a/c/a/a;)V

    return-void

    :cond_2
    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->g(Lf/a/a/c/a/a;)Lf/a/a/c/a/c;

    move-result-object p1

    iget-object p1, p1, Lf/a/a/c/a/c;->d:Lcom/discord/stores/StoreAudioDevices;

    sget-object v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAudioDevices;->selectAudioOutput(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->f(Lf/a/a/c/a/a;)V

    return-void

    :cond_3
    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->g(Lf/a/a/c/a/a;)Lf/a/a/c/a/c;

    move-result-object p1

    invoke-virtual {p1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lf/a/a/c/a/c$d$a;

    if-nez v2, :cond_4

    goto :goto_0

    :cond_4
    move-object v0, v1

    :goto_0
    check-cast v0, Lf/a/a/c/a/c$d$a;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lf/a/a/c/a/c$d$a;->c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v0, :cond_5

    iget-object p1, p1, Lf/a/a/c/a/c;->d:Lcom/discord/stores/StoreAudioDevices;

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreAudioDevices;->selectAudioOutput(Lcom/discord/stores/StoreAudioDevices$OutputDevice;)V

    :cond_5
    iget-object p1, p0, Lf/a/a/c/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/c/a/a;

    invoke-static {p1}, Lf/a/a/c/a/a;->f(Lf/a/a/c/a/a;)V

    return-void
.end method
