.class public final Lf/a/a/c/a/c$b;
.super Ljava/lang/Object;
.source "AudioOutputSelectionDialogViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/a/c/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Lcom/discord/stores/StoreAudioDevices;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAudioDevices;I)V
    .locals 0

    and-int/lit8 p1, p2, 0x1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getAudioDevices()Lcom/discord/stores/StoreAudioDevices;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string/jumbo p2, "storeAudioDevices"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/a/c/a/c$b;->a:Lcom/discord/stores/StoreAudioDevices;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lf/a/a/c/a/c;

    iget-object v0, p0, Lf/a/a/c/a/c$b;->a:Lcom/discord/stores/StoreAudioDevices;

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices;->getAudioDevicesState()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lf/a/a/c/a/d;->d:Lf/a/a/c/a/d;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    const-string/jumbo v2, "storeAudioDevices.getAud\u2026tate(audioDevicesState) }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0, v1}, Lf/a/a/c/a/c;-><init>(Lcom/discord/stores/StoreAudioDevices;Lrx/Observable;)V

    return-object p1
.end method
