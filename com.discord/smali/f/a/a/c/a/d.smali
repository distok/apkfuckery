.class public final Lf/a/a/c/a/d;
.super Ljava/lang/Object;
.source "AudioOutputSelectionDialogViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
        "Lf/a/a/c/a/c$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lf/a/a/c/a/d;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/a/c/a/d;

    invoke-direct {v0}, Lf/a/a/c/a/d;-><init>()V

    sput-object v0, Lf/a/a/c/a/d;->d:Lf/a/a/c/a/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    new-instance v0, Lf/a/a/c/a/c$c;

    const-string v1, "audioDevicesState"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lf/a/a/c/a/c$c;-><init>(Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V

    return-object v0
.end method
