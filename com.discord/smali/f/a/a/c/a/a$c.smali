.class public final Lf/a/a/c/a/a$c;
.super Lx/m/c/k;
.source "AudioOutputSelectionDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/c/a/a;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/c/a/c$d;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/c/a/a;


# direct methods
.method public constructor <init>(Lf/a/a/c/a/a;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/c/a/a$c;->this$0:Lf/a/a/c/a/a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    check-cast p1, Lf/a/a/c/a/c$d;

    const-string/jumbo v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lf/a/a/c/a/a$c;->this$0:Lf/a/a/c/a/a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lf/a/a/c/a/c$d$b;

    if-eqz v0, :cond_0

    goto/16 :goto_6

    :cond_0
    instance-of v0, p1, Lf/a/a/c/a/c$d$a;

    if-eqz v0, :cond_a

    check-cast p1, Lf/a/a/c/a/c$d$a;

    iget-object v0, p1, Lf/a/a/c/a/c$d$a;->a:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    instance-of v2, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lf/a/a/c/a/a;->h()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    goto :goto_0

    :cond_1
    instance-of v2, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lf/a/a/c/a/a;->j()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    goto :goto_0

    :cond_2
    instance-of v2, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lf/a/a/c/a/a;->k()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    goto :goto_0

    :cond_3
    instance-of v0, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    if-eqz v0, :cond_9

    invoke-virtual {v1}, Lf/a/a/c/a/a;->i()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    :goto_0
    iget-object v2, v1, Lf/a/a/c/a/a;->h:Lkotlin/properties/ReadOnlyProperty;

    sget-object v3, Lf/a/a/c/a/a;->j:[Lkotlin/reflect/KProperty;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    invoke-interface {v2, v1, v3}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    invoke-virtual {v2, v0}, Landroid/widget/RadioGroup;->check(I)V

    invoke-virtual {v1}, Lf/a/a/c/a/a;->h()Landroid/widget/RadioButton;

    move-result-object v0

    iget-boolean v2, p1, Lf/a/a/c/a/c$d$a;->b:Z

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lf/a/a/c/a/a;->h()Landroid/widget/RadioButton;

    move-result-object v0

    iget-object v2, p1, Lf/a/a/c/a/c$d$a;->c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    goto :goto_2

    :cond_5
    const v2, 0x7f120267

    invoke-virtual {v1, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lf/a/a/c/a/a;->j()Landroid/widget/RadioButton;

    move-result-object v0

    iget-boolean v2, p1, Lf/a/a/c/a/c$d$a;->d:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    const/16 v2, 0x8

    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lf/a/a/c/a/a;->k()Landroid/widget/RadioButton;

    move-result-object v0

    iget-boolean v2, p1, Lf/a/a/c/a/c$d$a;->e:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    goto :goto_4

    :cond_7
    const/16 v2, 0x8

    :goto_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lf/a/a/c/a/a;->i()Landroid/widget/RadioButton;

    move-result-object v0

    iget-boolean p1, p1, Lf/a/a/c/a/c$d$a;->f:Z

    if-eqz p1, :cond_8

    goto :goto_5

    :cond_8
    const/16 v3, 0x8

    :goto_5
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_a
    :goto_6
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
