.class public final Lf/a/a/c/a/c;
.super Lf/a/b/l0;
.source "AudioOutputSelectionDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/a/c/a/c$d;,
        Lf/a/a/c/a/c$c;,
        Lf/a/a/c/a/c$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lf/a/a/c/a/c$d;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lcom/discord/stores/StoreAudioDevices;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAudioDevices;Lrx/Observable;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreAudioDevices;",
            "Lrx/Observable<",
            "Lf/a/a/c/a/c$c;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "storeAudioDevices"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeStateObservable"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/a/c/a/c$d$b;->a:Lf/a/a/c/a/c$d$b;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lf/a/a/c/a/c;->d:Lcom/discord/stores/StoreAudioDevices;

    const/4 p1, 0x0

    const/4 v0, 0x2

    invoke-static {p2, p0, p1, v0, p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lf/a/a/c/a/c;

    new-instance v7, Lf/a/a/c/a/c$a;

    invoke-direct {v7, p0}, Lf/a/a/c/a/c$a;-><init>(Lf/a/a/c/a/c;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
