.class public final Lf/a/a/c/a/c$d$a;
.super Lf/a/a/c/a/c$d;
.source "AudioOutputSelectionDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/a/c/a/c$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

.field public final b:Z

.field public final c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

.field public final d:Z

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreAudioDevices$OutputDevice;ZLcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;ZZZ)V
    .locals 1

    const-string v0, "selectedAudioOutput"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/a/a/c/a/c$d;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lf/a/a/c/a/c$d$a;->a:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    iput-boolean p2, p0, Lf/a/a/c/a/c$d$a;->b:Z

    iput-object p3, p0, Lf/a/a/c/a/c$d$a;->c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    iput-boolean p4, p0, Lf/a/a/c/a/c$d$a;->d:Z

    iput-boolean p5, p0, Lf/a/a/c/a/c$d$a;->e:Z

    iput-boolean p6, p0, Lf/a/a/c/a/c$d$a;->f:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lf/a/a/c/a/c$d$a;

    if-eqz v0, :cond_0

    check-cast p1, Lf/a/a/c/a/c$d$a;

    iget-object v0, p0, Lf/a/a/c/a/c$d$a;->a:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    iget-object v1, p1, Lf/a/a/c/a/c$d$a;->a:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lf/a/a/c/a/c$d$a;->b:Z

    iget-boolean v1, p1, Lf/a/a/c/a/c$d$a;->b:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lf/a/a/c/a/c$d$a;->c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    iget-object v1, p1, Lf/a/a/c/a/c$d$a;->c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lf/a/a/c/a/c$d$a;->d:Z

    iget-boolean v1, p1, Lf/a/a/c/a/c$d$a;->d:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lf/a/a/c/a/c$d$a;->e:Z

    iget-boolean v1, p1, Lf/a/a/c/a/c$d$a;->e:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lf/a/a/c/a/c$d$a;->f:Z

    iget-boolean p1, p1, Lf/a/a/c/a/c$d$a;->f:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lf/a/a/c/a/c$d$a;->a:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lf/a/a/c/a/c$d$a;->b:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lf/a/a/c/a/c$d$a;->c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lf/a/a/c/a/c$d$a;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lf/a/a/c/a/c$d$a;->e:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lf/a/a/c/a/c$d$a;->f:Z

    if-eqz v1, :cond_5

    goto :goto_1

    :cond_5
    move v3, v1

    :goto_1
    add-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(selectedAudioOutput="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/a/a/c/a/c$d$a;->a:Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showBluetoothItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/a/c/a/c$d$a;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", bluetoothDevice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/a/a/c/a/c$d$a;->c:Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSpeakerItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/a/c/a/c$d$a;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showWiredItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/a/c/a/c$d$a;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showEarpieceItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/a/c/a/c$d$a;->f:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
