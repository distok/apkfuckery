.class public final Lf/a/a/w$a;
.super Lx/m/c/k;
.source "WidgetUrgentMessageDialogViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/w;-><init>(Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/w$d;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/w;


# direct methods
.method public constructor <init>(Lf/a/a/w;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/w$a;->this$0:Lf/a/a/w;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lf/a/a/w$d;

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/a/w$a;->this$0:Lf/a/a/w;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p1, Lf/a/a/w$d;->a:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/a/a/w$e;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget-boolean v1, v1, Lf/a/a/w$e;->b:Z

    :goto_0
    new-instance v2, Lf/a/a/w$e;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getFlags()Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v2, p1, v1}, Lf/a/a/w$e;-><init>(Ljava/lang/Integer;Z)V

    invoke-virtual {v0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
