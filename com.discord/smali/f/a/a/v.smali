.class public final Lf/a/a/v;
.super Ljava/lang/Object;
.source "WidgetUrgentMessageDialog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lf/a/a/u;


# direct methods
.method public constructor <init>(Lf/a/a/u;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/v;->d:Lf/a/a/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 22

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/a/a/v;->d:Lf/a/a/u;

    iget-object v1, v1, Lf/a/a/u;->g:Lf/a/a/w;

    if-eqz v1, :cond_1

    const-string v2, "button"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "button.context"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v3}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v3

    const-wide v4, 0x8efc0ce7f420001L

    invoke-virtual {v3, v2, v4, v5}, Lcom/discord/utilities/channel/ChannelSelector;->findAndSetDirectMessage(Landroid/content/Context;J)V

    invoke-virtual {v1}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/a/a/w$e;

    if-eqz v2, :cond_0

    iget-object v3, v2, Lf/a/a/w$e;->a:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int/lit16 v3, v3, -0x2001

    new-instance v15, Lcom/discord/restapi/RestAPIParams$UserInfo;

    const/4 v5, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/4 v8, 0x0

    const/16 v19, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v20, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x0

    const/16 v3, 0x2ff

    const/16 v16, 0x0

    move-object v4, v15

    move-object/from16 v6, v17

    move-object/from16 v7, v18

    move-object/from16 v9, v19

    move-object/from16 v12, v20

    move-object/from16 v21, v15

    move v15, v3

    invoke-direct/range {v4 .. v16}, Lcom/discord/restapi/RestAPIParams$UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget-object v2, v2, Lf/a/a/w$e;->a:Ljava/lang/Integer;

    new-instance v3, Lf/a/a/w$e;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lf/a/a/w$e;-><init>(Ljava/lang/Integer;Z)V

    invoke-virtual {v1, v3}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-object v2, v1, Lf/a/a/w;->e:Lcom/discord/utilities/rest/RestAPI;

    move-object/from16 v3, v21

    invoke-virtual {v2, v3}, Lcom/discord/utilities/rest/RestAPI;->patchUser(Lcom/discord/restapi/RestAPIParams$UserInfo;)Lrx/Observable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v1, v5, v3, v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v6

    const-class v7, Lf/a/a/w;

    new-instance v12, Lf/a/a/y;

    invoke-direct {v12, v1}, Lf/a/a/y;-><init>(Lf/a/a/w;)V

    new-instance v10, Lf/a/a/z;

    invoke-direct {v10, v1}, Lf/a/a/z;-><init>(Lf/a/a/w;)V

    const/16 v13, 0x16

    move-object/from16 v8, v17

    move-object/from16 v9, v18

    move-object/from16 v11, v19

    move-object/from16 v14, v20

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v1, "viewModel"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    throw v1
.end method
