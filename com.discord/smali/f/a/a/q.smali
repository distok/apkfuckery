.class public final Lf/a/a/q;
.super Lf/a/b/l0;
.source "WidgetAccessibilityDetectionDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/a/q$b;,
        Lf/a/a/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lf/a/a/q$b;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lcom/discord/stores/StoreUserSettings;

.field public final e:Lcom/discord/stores/StoreNotices;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreNotices;)V
    .locals 1

    const-string/jumbo v0, "storeUserSettings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeNotices"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/a/q$b$a;->a:Lf/a/a/q$b$a;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lf/a/a/q;->d:Lcom/discord/stores/StoreUserSettings;

    iput-object p2, p0, Lf/a/a/q;->e:Lcom/discord/stores/StoreNotices;

    return-void
.end method


# virtual methods
.method public onCleared()V
    .locals 6

    iget-object v0, p0, Lf/a/a/q;->e:Lcom/discord/stores/StoreNotices;

    sget-object v1, Lf/a/a/g;->i:Lf/a/a/g$b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ALLOW_ACCESSIBILITY_DETECTION_DIALOG"

    const-wide/16 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/stores/StoreNotices;->markSeen$default(Lcom/discord/stores/StoreNotices;Ljava/lang/String;JILjava/lang/Object;)V

    return-void
.end method
