.class public final Lf/a/a/t$c$b;
.super Lx/m/c/k;
.source "WidgetEnableTwoFactorPasswordDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/t$c;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/error/Error;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/t$c;


# direct methods
.method public constructor <init>(Lf/a/a/t$c;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/t$c$b;->this$0:Lf/a/a/t$c;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lcom/discord/utilities/error/Error;

    const-string v0, "error"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/a/t$c$b;->this$0:Lf/a/a/t$c;

    iget-object v0, v0, Lf/a/a/t$c;->d:Lf/a/a/t;

    sget-object v1, Lf/a/a/t;->h:[Lkotlin/reflect/KProperty;

    invoke-virtual {v0}, Lf/a/a/t;->f()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v0

    const-string v2, "error.response"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/utilities/error/Error$Response;->getCode()I

    move-result v0

    const v2, 0xea65

    if-ne v0, v2, :cond_0

    invoke-virtual {p1, v1}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    iget-object p1, p0, Lf/a/a/t$c$b;->this$0:Lf/a/a/t$c;

    iget-object p1, p1, Lf/a/a/t$c;->d:Lf/a/a/t;

    invoke-virtual {p1}, Lcom/discord/app/AppDialog;->dismiss()V

    iget-object p1, p0, Lf/a/a/t$c$b;->this$0:Lf/a/a/t$c;

    iget-object v0, p1, Lf/a/a/t$c;->d:Lf/a/a/t;

    iget-object v0, v0, Lf/a/a/t;->g:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lf/a/a/t$c;->e:Landroid/view/View;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
