.class public final Lf/a/a/a0/a$c;
.super Lx/m/c/k;
.source "WidgetGiftAcceptDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/a0/a;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreGifting$GiftState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/a0/a;


# direct methods
.method public constructor <init>(Lf/a/a/a0/a;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    check-cast v1, Lcom/discord/stores/StoreGifting$GiftState;

    const-string v2, "giftState"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v2, v1, Lcom/discord/stores/StoreGifting$GiftState$Loading;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    iget-object v1, v0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    iget-object v2, v1, Lf/a/a/a0/a;->k:Lkotlin/properties/ReadOnlyProperty;

    sget-object v4, Lf/a/a/a0/a;->l:[Lkotlin/reflect/KProperty;

    const/4 v5, 0x7

    aget-object v4, v4, v5

    invoke-interface {v2, v1, v4}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/app/AppViewFlipper;

    invoke-virtual {v1, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    goto/16 :goto_15

    :cond_0
    instance-of v2, v1, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    const v4, 0x7f120926

    const v5, 0x7f12092d

    const-string/jumbo v6, "when {\n      gift.isAnyN\u2026body_confirm, name)\n    }"

    const/4 v7, 0x2

    const v8, 0x7f12090e

    const v9, 0x7f12090d

    const-string/jumbo v10, "when {\n      gift.isAnyN\u2026ion_header_confirm)\n    }"

    const v11, 0x7f12092c

    const v12, 0x7f12092b

    const/4 v13, 0x1

    const/16 v14, 0x8

    if-eqz v2, :cond_e

    check-cast v1, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGift;->getRedeemed()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v1

    sget-object v4, Lf/a/a/a0/a;->l:[Lkotlin/reflect/KProperty;

    invoke-virtual {v2, v1}, Lf/a/a/a0/a;->f(Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->getStoreListing()Lcom/discord/models/domain/ModelStoreListing;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelSku;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    const-string v4, ""

    :goto_0
    invoke-virtual {v2, v1}, Lf/a/a/a0/a;->i(Lcom/discord/models/domain/ModelGift;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v6

    if-eqz v6, :cond_2

    const v6, 0x7f12092f

    new-array v7, v13, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v2, v6, v7}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_2
    const v6, 0x7f12092e

    invoke-virtual {v2, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_1
    const-string/jumbo v7, "when {\n      gift.isAnyN\u2026ion_header_success)\n    }"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isNitroClassicGift()Z

    move-result v7

    if-eqz v7, :cond_3

    const v1, 0x7f12091c

    new-array v4, v13, [Ljava/lang/Object;

    aput-object v5, v4, v3

    invoke-virtual {v2, v1, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isNitroGift()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f12091d

    new-array v4, v13, [Ljava/lang/Object;

    aput-object v5, v4, v3

    invoke-virtual {v2, v1, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    const v1, 0x7f12091b

    new-array v5, v13, [Ljava/lang/Object;

    aput-object v4, v5, v3

    invoke-virtual {v2, v1, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    const-string/jumbo v4, "when {\n      gift.isNitr\u2026ccess_mobile, name)\n    }"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogHeader()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v4, v1, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->h()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v14}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_5

    const v4, 0x7f12092a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v15

    goto :goto_3

    :cond_5
    const/4 v15, 0x0

    :goto_3
    invoke-virtual {v1, v15}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v1

    new-instance v3, Lf/a/a/a0/b;

    invoke-direct {v3, v2}, Lf/a/a/a0/b;-><init>(Lf/a/a/a0/a;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_15

    :cond_6
    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGift;->getMaxUses()I

    move-result v2

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/discord/models/domain/ModelGift;->getUses()I

    move-result v15

    if-ne v2, v15, :cond_8

    iget-object v2, v0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v1

    sget-object v6, Lf/a/a/a0/a;->l:[Lkotlin/reflect/KProperty;

    invoke-virtual {v2, v1}, Lf/a/a/a0/a;->f(Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogHeader()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v2, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v1

    const v5, 0x7f12090c

    invoke-virtual {v2, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v5, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->h()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v14}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v15

    goto :goto_4

    :cond_7
    const/4 v15, 0x0

    :goto_4
    invoke-virtual {v1, v15}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v1

    new-instance v3, Lf/a/a/a0/c;

    invoke-direct {v3, v2}, Lf/a/a/a0/c;-><init>(Lf/a/a/a0/a;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_15

    :cond_8
    iget-object v2, v0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v1

    sget-object v4, Lf/a/a/a0/a;->l:[Lkotlin/reflect/KProperty;

    invoke-virtual {v2, v1}, Lf/a/a/a0/a;->f(Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->getStoreListing()Lcom/discord/models/domain/ModelStoreListing;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelSku;->getName()Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    :goto_5
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v5

    if-eqz v5, :cond_a

    new-array v5, v13, [Ljava/lang/Object;

    aput-object v4, v5, v3

    invoke-virtual {v2, v11, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_a
    invoke-virtual {v2, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_6
    invoke-static {v5, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v10

    if-eqz v10, :cond_b

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v2, v1}, Lf/a/a/a0/a;->i(Lcom/discord/models/domain/ModelGift;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v13

    invoke-virtual {v2, v8, v7}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_7

    :cond_b
    new-array v7, v13, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v2, v9, v7}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_7
    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogHeader()Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v5, v4, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->h()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v5

    if-eqz v5, :cond_c

    goto :goto_8

    :cond_c
    const/16 v3, 0x8

    :goto_8
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    if-eqz v4, :cond_d

    const v5, 0x7f120924

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v15

    goto :goto_9

    :cond_d
    const/4 v15, 0x0

    :goto_9
    invoke-virtual {v3, v15}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lf/a/a/a0/d;

    invoke-direct {v3, v1}, Lf/a/a/a0/d;-><init>(Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_15

    :cond_e
    instance-of v2, v1, Lcom/discord/stores/StoreGifting$GiftState$Redeeming;

    if-eqz v2, :cond_13

    iget-object v2, v0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    check-cast v1, Lcom/discord/stores/StoreGifting$GiftState$Redeeming;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$Redeeming;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v1

    sget-object v4, Lf/a/a/a0/a;->l:[Lkotlin/reflect/KProperty;

    invoke-virtual {v2, v1}, Lf/a/a/a0/a;->f(Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->getStoreListing()Lcom/discord/models/domain/ModelStoreListing;

    move-result-object v4

    if-eqz v4, :cond_f

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelStoreListing;->getSku()Lcom/discord/models/domain/ModelSku;

    move-result-object v4

    if-eqz v4, :cond_f

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelSku;->getName()Ljava/lang/String;

    move-result-object v15

    goto :goto_a

    :cond_f
    const/4 v15, 0x0

    :goto_a
    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v4, v2, Lf/a/a/a0/a;->j:Lkotlin/properties/ReadOnlyProperty;

    sget-object v5, Lf/a/a/a0/a;->l:[Lkotlin/reflect/KProperty;

    const/16 v16, 0x6

    aget-object v5, v5, v16

    invoke-interface {v4, v2, v5}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v4

    if-eqz v4, :cond_10

    new-array v4, v13, [Ljava/lang/Object;

    aput-object v15, v4, v3

    invoke-virtual {v2, v11, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_b

    :cond_10
    invoke-virtual {v2, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_b
    invoke-static {v4, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v5

    if-eqz v5, :cond_11

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v15, v5, v3

    invoke-virtual {v2, v1}, Lf/a/a/a0/a;->i(Lcom/discord/models/domain/ModelGift;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v13

    invoke-virtual {v2, v8, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_c

    :cond_11
    new-array v5, v13, [Ljava/lang/Object;

    aput-object v15, v5, v3

    invoke-virtual {v2, v9, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :goto_c
    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogHeader()Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v4

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->h()Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGift;->isAnyNitroGift()Z

    move-result v1

    if-eqz v1, :cond_12

    goto :goto_d

    :cond_12
    const/16 v3, 0x8

    :goto_d
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_15

    :cond_13
    instance-of v2, v1, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;

    if-eqz v2, :cond_1f

    iget-object v2, v0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    check-cast v1, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;

    sget-object v6, Lf/a/a/a0/a;->l:[Lkotlin/reflect/KProperty;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v6

    invoke-virtual {v2, v6}, Lf/a/a/a0/a;->f(Lcom/discord/models/domain/ModelGift;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogHeader()Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v2, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v5

    const v6, 0x7f120922

    invoke-virtual {v2, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v3, [Ljava/lang/Object;

    invoke-static {v5, v7, v8}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;->getCanRetry()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v2, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    if-eqz v5, :cond_14

    const v6, 0x7f121542

    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v15

    goto :goto_e

    :cond_14
    const/4 v15, 0x0

    :goto_e
    invoke-virtual {v4, v15}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v2

    new-instance v4, Lh;

    invoke-direct {v4, v3, v1}, Lh;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_15

    :cond_15
    invoke-virtual {v1}, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;->getErrorCode()Ljava/lang/Integer;

    move-result-object v1

    const v3, 0xc386

    if-nez v1, :cond_16

    goto :goto_f

    :cond_16
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v3, :cond_17

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v1

    const v3, 0x7f120915

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_12

    :cond_17
    :goto_f
    const v3, 0x186b8

    if-nez v1, :cond_18

    goto :goto_10

    :cond_18
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v3, :cond_19

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v1

    const v3, 0x7f120911

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_12

    :cond_19
    :goto_10
    const v3, 0x186b6

    if-nez v1, :cond_1a

    goto :goto_11

    :cond_1a
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v3, :cond_1b

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v1

    const v3, 0x7f120912

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_12

    :cond_1b
    :goto_11
    const v3, 0x186b9

    if-nez v1, :cond_1c

    goto :goto_12

    :cond_1c
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_1d

    invoke-virtual {v2}, Lf/a/a/a0/a;->getDialogText()Landroid/widget/TextView;

    move-result-object v1

    const v3, 0x7f120910

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_1d
    :goto_12
    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_1e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v15

    goto :goto_13

    :cond_1e
    const/4 v15, 0x0

    :goto_13
    invoke-virtual {v1, v15}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lf/a/a/a0/a;->g()Landroid/widget/Button;

    move-result-object v1

    new-instance v3, Lh;

    invoke-direct {v3, v13, v2}, Lh;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_15

    :cond_1f
    instance-of v2, v1, Lcom/discord/stores/StoreGifting$GiftState$Invalid;

    if-eqz v2, :cond_20

    goto :goto_14

    :cond_20
    instance-of v2, v1, Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;

    if-eqz v2, :cond_21

    goto :goto_14

    :cond_21
    instance-of v1, v1, Lcom/discord/stores/StoreGifting$GiftState$Revoking;

    if-eqz v1, :cond_22

    :goto_14
    iget-object v1, v0, Lf/a/a/a0/a$c;->this$0:Lf/a/a/a0/a;

    invoke-virtual {v1}, Lcom/discord/app/AppDialog;->dismiss()V

    :cond_22
    :goto_15
    sget-object v1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v1
.end method
