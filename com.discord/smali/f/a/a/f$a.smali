.class public final Lf/a/a/f$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/f;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lf/a/a/f$a;->d:I

    iput-object p2, p0, Lf/a/a/f$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 17

    move-object/from16 v0, p0

    iget v1, v0, Lf/a/a/f$a;->d:I

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lf/a/a/f$a;->e:Ljava/lang/Object;

    check-cast v1, Lf/a/a/f;

    sget-object v2, Lf/a/a/f;->n:[Lkotlin/reflect/KProperty;

    invoke-virtual {v1}, Lf/a/a/f;->f()Lcom/google/android/material/switchmaterial/SwitchMaterial;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/appcompat/widget/SwitchCompat;->toggle()V

    return-void

    :cond_0
    const/4 v1, 0x0

    throw v1

    :cond_1
    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    new-instance v10, Lcom/discord/utilities/analytics/Traits$Location;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1d

    const/4 v9, 0x0

    const-string v4, "File Upload Popout (w/ Compression)"

    move-object v2, v10

    invoke-direct/range {v2 .. v9}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v1, v10}, Lcom/discord/utilities/analytics/AnalyticsTracker;->premiumSettingsOpened(Lcom/discord/utilities/analytics/Traits$Location;)V

    sget-object v11, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->Companion:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;

    const-string v1, "it"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    const-string v1, "it.context"

    invoke-static {v12, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;->launch$default(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)V

    iget-object v1, v0, Lf/a/a/f$a;->e:Ljava/lang/Object;

    check-cast v1, Lf/a/a/f;

    invoke-virtual {v1}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void

    :cond_2
    iget-object v1, v0, Lf/a/a/f$a;->e:Ljava/lang/Object;

    check-cast v1, Lf/a/a/f;

    invoke-virtual {v1}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void

    :cond_3
    iget-object v1, v0, Lf/a/a/f$a;->e:Ljava/lang/Object;

    check-cast v1, Lf/a/a/f;

    iget-object v1, v1, Lf/a/a/f;->m:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_4

    invoke-interface {v1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Unit;

    :cond_4
    iget-object v1, v0, Lf/a/a/f$a;->e:Ljava/lang/Object;

    check-cast v1, Lf/a/a/f;

    invoke-virtual {v1}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void
.end method
