.class public final synthetic Lf/a/a/a/a$f;
.super Lx/m/c/i;
.source "UserActionsDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/a/a;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/a/b$b;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lf/a/a/a/a;)V
    .locals 7

    const-class v3, Lf/a/a/a/a;

    const/4 v1, 0x1

    const-string v4, "handleEvent"

    const-string v5, "handleEvent(Lcom/discord/dialogs/useractions/UserActionsDialogViewModel$Event;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Lf/a/a/a/b$b;

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lf/a/a/a/a;

    sget-object v1, Lf/a/a/a/a;->k:[Lkotlin/reflect/KProperty;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v1, p1, Lf/a/a/a/b$b$b;

    const/4 v2, 0x4

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    check-cast p1, Lf/a/a/a/b$b$b;

    iget p1, p1, Lf/a/a/a/b$b$b;->a:I

    invoke-static {v0, p1, v3, v2}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    invoke-virtual {v0}, Lcom/discord/app/AppDialog;->dismiss()V

    goto :goto_0

    :cond_0
    instance-of v1, p1, Lf/a/a/a/b$b$a;

    if-eqz v1, :cond_1

    check-cast p1, Lf/a/a/a/b$b$a;

    iget p1, p1, Lf/a/a/a/b$b$a;->a:I

    invoke-static {v0, p1, v3, v2}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    invoke-virtual {v0}, Lcom/discord/app/AppDialog;->dismiss()V

    :cond_1
    :goto_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
