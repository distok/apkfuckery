.class public final Lf/a/a/a/e;
.super Ljava/lang/Object;
.source "UserActionsDialogViewModel.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/Integer;",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        "Lf/a/a/a/b$d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/a/a/a/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/a/a/e;

    invoke-direct {v0}, Lf/a/a/a/e;-><init>()V

    sput-object v0, Lf/a/a/a/e;->a:Lf/a/a/a/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/models/domain/ModelGuildMember$Computed;

    new-instance v0, Lf/a/a/a/b$d;

    invoke-direct {v0, p1, p2, p3}, Lf/a/a/a/b$d;-><init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/Integer;Lcom/discord/models/domain/ModelGuildMember$Computed;)V

    return-object v0
.end method
