.class public final Lf/a/a/a/b$a;
.super Lx/m/c/k;
.source "UserActionsDialogViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/a/b;-><init>(JLcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/a/b$d;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/a/a/b;


# direct methods
.method public constructor <init>(Lf/a/a/a/b;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/a/b$a;->this$0:Lf/a/a/a/b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    check-cast p1, Lf/a/a/a/b$d;

    const-string/jumbo v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/a/a/b$a;->this$0:Lf/a/a/a/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lf/a/a/a/b$d;->a:Lcom/discord/models/domain/ModelUser;

    if-nez v2, :cond_0

    iget-object p1, v0, Lf/a/a/a/b;->d:Lrx/subjects/PublishSubject;

    new-instance v0, Lf/a/a/a/b$b$a;

    const v1, 0x7f1205df

    invoke-direct {v0, v1}, Lf/a/a/a/b$b$a;-><init>(I)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_4

    :cond_0
    iget-object v1, p1, Lf/a/a/a/b$d;->b:Ljava/lang/Integer;

    invoke-static {v1}, Lcom/discord/models/domain/ModelUserRelationship;->getType(Ljava/lang/Integer;)I

    move-result v1

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v1, v3, :cond_1

    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :goto_0
    if-ne v1, v3, :cond_2

    const/4 v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_1
    if-ne v1, v5, :cond_3

    const/4 v8, 0x1

    goto :goto_2

    :cond_3
    const/4 v8, 0x0

    :goto_2
    iget-object p1, p1, Lf/a/a/a/b$d;->c:Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    :cond_4
    const/4 p1, 0x0

    :goto_3
    move-object v3, p1

    new-instance p1, Lf/a/a/a/b$e$a;

    move-object v1, p1

    move v4, v6

    move v5, v7

    move v6, v8

    invoke-direct/range {v1 .. v6}, Lf/a/a/a/b$e$a;-><init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;ZZZ)V

    invoke-virtual {v0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_4
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
