.class public final Lf/a/a/a/b;
.super Lf/a/b/l0;
.source "UserActionsDialogViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/a/a/b$e;,
        Lf/a/a/a/b$d;,
        Lf/a/a/a/b$b;,
        Lf/a/a/a/b$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lf/a/a/a/b$e;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lf/a/a/a/b$b;",
            ">;"
        }
    .end annotation
.end field

.field public final e:J

.field public final f:Lcom/discord/utilities/rest/RestAPI;


# direct methods
.method public constructor <init>(JLcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lrx/Observable<",
            "Lf/a/a/a/b$d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "restAPI"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "storeObservable"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/a/a/b$e$b;->a:Lf/a/a/a/b$e$b;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-wide p1, p0, Lf/a/a/a/b;->e:J

    iput-object p3, p0, Lf/a/a/a/b;->f:Lcom/discord/utilities/rest/RestAPI;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lf/a/a/a/b;->d:Lrx/subjects/PublishSubject;

    invoke-static {p4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lf/a/a/a/b;

    new-instance v6, Lf/a/a/a/b$a;

    invoke-direct {v6, p0}, Lf/a/a/a/b$a;-><init>(Lf/a/a/a/b;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final removeRelationship(I)V
    .locals 13
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    iget-object v0, p0, Lf/a/a/a/b;->f:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v1, p0, Lf/a/a/a/b;->e:J

    const-string v3, "User Profile"

    invoke-virtual {v0, v3, v1, v2}, Lcom/discord/utilities/rest/RestAPI;->removeRelationship(Ljava/lang/String;J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v3, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lf/a/a/a/b;

    new-instance v10, Lf/a/a/a/b$f;

    invoke-direct {v10, p0, p1}, Lf/a/a/a/b$f;-><init>(Lf/a/a/a/b;I)V

    new-instance v8, Lf/a/a/a/b$g;

    invoke-direct {v8, p0}, Lf/a/a/a/b$g;-><init>(Lf/a/a/a/b;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
