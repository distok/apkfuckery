.class public final Lf/a/a/a/a$a;
.super Ljava/lang/Object;
.source "java-style lambda group"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/a/a;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 0

    iput p1, p0, Lf/a/a/a/a$a;->d:I

    iput-object p2, p0, Lf/a/a/a/a$a;->e:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 12

    iget p1, p0, Lf/a/a/a/a$a;->d:I

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz p1, :cond_2

    if-eq p1, v2, :cond_1

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lf/a/a/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/a/a;

    invoke-static {p1}, Lf/a/a/a/a;->f(Lf/a/a/a/a;)Lf/a/a/a/b;

    move-result-object p1

    const v0, 0x7f120848

    invoke-virtual {p1, v0}, Lf/a/a/a/b;->removeRelationship(I)V

    return-void

    :cond_0
    throw v0

    :cond_1
    iget-object p1, p0, Lf/a/a/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/a/a;

    invoke-static {p1}, Lf/a/a/a/a;->f(Lf/a/a/a/a;)Lf/a/a/a/b;

    move-result-object p1

    const v0, 0x7f12195e

    invoke-virtual {p1, v0}, Lf/a/a/a/b;->removeRelationship(I)V

    return-void

    :cond_2
    iget-object p1, p0, Lf/a/a/a/a$a;->e:Ljava/lang/Object;

    check-cast p1, Lf/a/a/a/a;

    invoke-static {p1}, Lf/a/a/a/a;->f(Lf/a/a/a/a;)Lf/a/a/a/b;

    move-result-object p1

    iget-object v3, p1, Lf/a/a/a/b;->f:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v5, p1, Lf/a/a/a/b;->e:J

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x8

    const/4 v10, 0x0

    const-string v4, "User Profile"

    invoke-static/range {v3 .. v10}, Lcom/discord/utilities/rest/RestAPI;->addRelationship$default(Lcom/discord/utilities/rest/RestAPI;Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/String;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4, v2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    invoke-static {v2, p1, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lf/a/a/a/b;

    new-instance v9, Lf/a/a/a/f;

    invoke-direct {v9, p1}, Lf/a/a/a/f;-><init>(Lf/a/a/a/b;)V

    new-instance v7, Lf/a/a/a/g;

    invoke-direct {v7, p1}, Lf/a/a/a/g;-><init>(Lf/a/a/a/b;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
