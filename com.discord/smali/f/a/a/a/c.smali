.class public final Lf/a/a/a/c;
.super Ljava/lang/Object;
.source "UserActionsDialogViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/a/a/a/d;


# direct methods
.method public constructor <init>(Lf/a/a/a/d;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/a/c;->d:Lf/a/a/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/util/Map;

    iget-object v0, p0, Lf/a/a/a/c;->d:Lf/a/a/a/d;

    iget-object v0, v0, Lf/a/a/a/d;->d:Lf/a/a/a/b$c;

    iget-wide v0, v0, Lf/a/a/a/b$c;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuildMember$Computed;

    return-object p1
.end method
