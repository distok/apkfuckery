.class public final Lf/a/a/a/b$c;
.super Ljava/lang/Object;
.source "UserActionsDialogViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/a/a/b$c;->a:J

    iput-wide p3, p0, Lf/a/a/a/b$c;->b:J

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lf/a/a/a/b;

    iget-wide v0, p0, Lf/a/a/a/b$c;->a:J

    sget-object v2, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v2

    iget-wide v3, p0, Lf/a/a/a/b$c;->a:J

    iget-wide v5, p0, Lf/a/a/a/b$c;->b:J

    sget-object v7, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v7}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v8

    invoke-virtual {v7}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v9

    invoke-virtual {v7}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v10

    invoke-virtual {v7}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v7

    invoke-virtual {v8, v3, v4}, Lcom/discord/stores/StoreUser;->observeUser(J)Lrx/Observable;

    move-result-object v8

    invoke-virtual {v9, v3, v4}, Lcom/discord/stores/StoreUserRelationships;->observe(J)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v7, v5, v6}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v4

    new-instance v5, Lf/a/a/a/d;

    invoke-direct {v5, p0, v10}, Lf/a/a/a/d;-><init>(Lf/a/a/a/b$c;Lcom/discord/stores/StoreGuilds;)V

    invoke-virtual {v4, v5}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v4

    invoke-virtual {v4}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v4

    const-string/jumbo v5, "storeChannels\n          \u2026  .distinctUntilChanged()"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lf/a/a/a/e;->a:Lf/a/a/a/e;

    invoke-static {v8, v3, v4, v5}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v3}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v3

    const-string v4, "Observable.combineLatest\u2026  .distinctUntilChanged()"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0, v1, v2, v3}, Lf/a/a/a/b;-><init>(JLcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V

    return-object p1
.end method
