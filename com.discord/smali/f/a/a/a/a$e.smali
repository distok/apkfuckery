.class public final synthetic Lf/a/a/a/a$e;
.super Lx/m/c/i;
.source "UserActionsDialog.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/a/a;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/a/a/b$e$a;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lf/a/a/a/a;)V
    .locals 7

    const-class v3, Lf/a/a/a/a;

    const/4 v1, 0x1

    const-string/jumbo v4, "updateView"

    const-string/jumbo v5, "updateView(Lcom/discord/dialogs/useractions/UserActionsDialogViewModel$ViewState$Loaded;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 24

    move-object/from16 v0, p1

    check-cast v0, Lf/a/a/a/b$e$a;

    const-string v1, "p1"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v1, p0

    iget-object v2, v1, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v2, Lf/a/a/a/a;

    iget-object v3, v2, Lf/a/a/a/a;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v4, Lf/a/a/a/a;->k:[Lkotlin/reflect/KProperty;

    const/4 v5, 0x0

    aget-object v6, v4, v5

    invoke-interface {v3, v2, v6}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Landroid/widget/ImageView;

    iget-object v3, v0, Lf/a/a/a/b$e$a;->a:Lcom/discord/models/domain/ModelUser;

    const/4 v13, 0x1

    const/4 v7, 0x0

    const/4 v14, 0x4

    invoke-static {v3, v13, v7, v14, v7}, Lcom/discord/utilities/icon/IconUtils;->getForUser$default(Lcom/discord/models/domain/ModelUser;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, v2, Lf/a/a/a/a;->i:Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;

    const/16 v11, 0xc

    const/4 v12, 0x0

    invoke-static/range {v6 .. v12}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iget-object v3, v2, Lf/a/a/a/a;->e:Lkotlin/properties/ReadOnlyProperty;

    aget-object v6, v4, v13

    invoke-interface {v3, v2, v6}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v15, v0, Lf/a/a/a/b$e$a;->a:Lcom/discord/models/domain/ModelUser;

    iget-object v6, v0, Lf/a/a/a/b$e$a;->b:Ljava/lang/String;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "requireContext()"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v18, 0x7f040153

    const v19, 0x7f090003

    const v20, 0x7f0b001e

    const v21, 0x7f040178

    const v22, 0x7f090002

    const v23, 0x7f0b001e

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    invoke-static/range {v15 .. v23}, Lcom/discord/widgets/user/UserNameFormatterKt;->getSpannableForUserNameWithDiscrim(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Landroid/content/Context;IIIIII)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lf/a/a/a/a;->f:Lkotlin/properties/ReadOnlyProperty;

    const/4 v6, 0x2

    aget-object v6, v4, v6

    invoke-interface {v3, v2, v6}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-boolean v6, v0, Lf/a/a/a/b$e$a;->c:Z

    const/16 v7, 0x8

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    const/16 v6, 0x8

    :goto_0
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v2, Lf/a/a/a/a;->g:Lkotlin/properties/ReadOnlyProperty;

    const/4 v6, 0x3

    aget-object v6, v4, v6

    invoke-interface {v3, v2, v6}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-boolean v6, v0, Lf/a/a/a/b$e$a;->d:Z

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    const/16 v6, 0x8

    :goto_1
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v2, Lf/a/a/a/a;->h:Lkotlin/properties/ReadOnlyProperty;

    aget-object v4, v4, v14

    invoke-interface {v3, v2, v4}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-boolean v0, v0, Lf/a/a/a/b$e$a;->e:Z

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/16 v5, 0x8

    :goto_2
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
