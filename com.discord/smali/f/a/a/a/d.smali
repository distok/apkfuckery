.class public final Lf/a/a/a/d;
.super Ljava/lang/Object;
.source "UserActionsDialogViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/a/a/a/b$c;

.field public final synthetic e:Lcom/discord/stores/StoreGuilds;


# direct methods
.method public constructor <init>(Lf/a/a/a/b$c;Lcom/discord/stores/StoreGuilds;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/a/d;->d:Lf/a/a/a/b$c;

    iput-object p2, p0, Lf/a/a/a/d;->e:Lcom/discord/stores/StoreGuilds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/a/a/a/d;->e:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "channel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lf/a/a/a/d;->d:Lf/a/a/a/b$c;

    iget-wide v3, v3, Lf/a/a/a/b$c;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeComputed(JLjava/util/Collection;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/a/a/c;

    invoke-direct {v1, p0, p1}, Lf/a/a/a/c;-><init>(Lf/a/a/a/d;Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method
