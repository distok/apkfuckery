.class public final Lf/a/a/j$b;
.super Ljava/lang/Object;
.source "DatePickerDialog.kt"

# interfaces
.implements Landroid/widget/DatePicker$OnDateChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/a/j;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/a/a/j;


# direct methods
.method public constructor <init>(Lf/a/a/j;)V
    .locals 0

    iput-object p1, p0, Lf/a/a/j$b;->d:Lf/a/a/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDateChanged(Landroid/widget/DatePicker;III)V
    .locals 0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    invoke-virtual {p1, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    iget-object p2, p0, Lf/a/a/j$b;->d:Lf/a/a/j;

    const-string p3, "cal"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p3

    iput-wide p3, p2, Lf/a/a/j;->e:J

    iget-object p1, p0, Lf/a/a/j$b;->d:Lf/a/a/j;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lf/a/a/j;->f:Z

    iget-object p2, p1, Lf/a/a/j;->d:Lkotlin/jvm/functions/Function1;

    if-eqz p2, :cond_0

    iget-wide p3, p1, Lf/a/a/j;->e:J

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method
