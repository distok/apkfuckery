.class public final Lf/a/a/t;
.super Lcom/discord/app/AppDialog;
.source "WidgetEnableTwoFactorPasswordDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/a/t$a;
    }
.end annotation


# static fields
.field public static final synthetic h:[Lkotlin/reflect/KProperty;

.field public static final i:Lf/a/a/t$a;


# instance fields
.field public final d:Lkotlin/properties/ReadOnlyProperty;

.field public final e:Lkotlin/properties/ReadOnlyProperty;

.field public final f:Lkotlin/properties/ReadOnlyProperty;

.field public g:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lf/a/a/t;

    const-string v3, "ok"

    const-string v4, "getOk()Lcom/discord/views/LoadingButton;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lf/a/a/t;

    const-string v6, "cancel"

    const-string v7, "getCancel()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lf/a/a/t;

    const-string/jumbo v6, "textInputLayout"

    const-string v7, "getTextInputLayout()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lf/a/a/t;->h:[Lkotlin/reflect/KProperty;

    new-instance v0, Lf/a/a/t$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/a/a/t$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lf/a/a/t;->i:Lf/a/a/t$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a03c5

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/t;->d:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03c2

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/t;->e:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a03c6

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lf/a/a/t;->f:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lf/a/a/t$b;->d:Lf/a/a/t$b;

    iput-object v0, p0, Lf/a/a/t;->g:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public final f()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lf/a/a/t;->d:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lf/a/a/t;->h:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01ec

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {p0}, Lf/a/a/t;->f()Lcom/discord/views/LoadingButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-virtual {p0}, Lf/a/a/t;->f()Lcom/discord/views/LoadingButton;

    move-result-object v0

    new-instance v1, Lf/a/a/t$c;

    invoke-direct {v1, p0, p1}, Lf/a/a/t$c;-><init>(Lf/a/a/t;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lf/a/a/t;->e:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lf/a/a/t;->h:[Lkotlin/reflect/KProperty;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-interface {p1, p0, v0}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    new-instance v0, Lf/a/a/t$d;

    invoke-direct {v0, p0}, Lf/a/a/t$d;-><init>(Lf/a/a/t;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
