.class public final synthetic Lf/a/o/a/s0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole;

.field public final synthetic e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole;Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/s0;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole;

    iput-object p2, p0, Lf/a/o/a/s0;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 9

    iget-object v0, p0, Lf/a/o/a/s0;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole;

    iget-object v1, p0, Lf/a/o/a/s0;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;

    check-cast p1, Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;->access$200(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;->access$200(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v7

    invoke-static/range {v2 .. v8}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->createForRole(Landroid/content/Context;JJJ)V

    return-void
.end method
