.class public final synthetic Lf/a/o/a/z0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

.field public final synthetic e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/z0;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

    iput-object p2, p0, Lf/a/o/a/z0;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lf/a/o/a/z0;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

    iget-object v0, p0, Lf/a/o/a/z0;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;->access$500(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;->access$100(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddRole;->create(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    const v0, 0x7f121241

    invoke-static {p1, v0}, Lf/a/b/p;->g(Landroidx/fragment/app/Fragment;I)V

    :goto_0
    return-void
.end method
