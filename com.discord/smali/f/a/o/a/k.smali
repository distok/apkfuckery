.class public final synthetic Lf/a/o/a/k;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelGroupDMSettings;

.field public final synthetic e:J


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelGroupDMSettings;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/k;->d:Lcom/discord/widgets/channels/WidgetChannelGroupDMSettings;

    iput-wide p2, p0, Lf/a/o/a/k;->e:J

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lf/a/o/a/k;->d:Lcom/discord/widgets/channels/WidgetChannelGroupDMSettings;

    iget-wide v1, p0, Lf/a/o/a/k;->e:J

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->showForChannel(JLandroidx/fragment/app/FragmentManager;)V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
