.class public final synthetic Lf/a/o/a/t;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func4;


# static fields
.field public static final synthetic a:Lf/a/o/a/t;


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/o/a/t;

    invoke-direct {v0}, Lf/a/o/a/t;-><init>()V

    sput-object v0, Lf/a/o/a/t;->a:Lf/a/o/a/t;

    return-void
.end method

.method public synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    move-object v3, p1

    check-cast v3, Lcom/discord/models/domain/ModelChannel;

    move-object v1, p2

    check-cast v1, Lcom/discord/models/domain/ModelUser;

    move-object v2, p3

    check-cast v2, Lcom/discord/models/domain/ModelGuild;

    check-cast p4, Ljava/lang/Long;

    invoke-static {v3, v1, v2, p4}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->isValid(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;J)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method
