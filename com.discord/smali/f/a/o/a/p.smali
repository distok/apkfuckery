.class public final synthetic Lf/a/o/a/p;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public synthetic constructor <init>(Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/p;->d:Landroid/content/Context;

    iput-object p2, p0, Lf/a/o/a/p;->e:Lcom/discord/models/domain/ModelChannel;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lf/a/o/a/p;->d:Landroid/content/Context;

    iget-object v1, p0, Lf/a/o/a/p;->e:Lcom/discord/models/domain/ModelChannel;

    check-cast p1, Ljava/lang/Void;

    sget p1, Lcom/discord/widgets/channels/WidgetChannelGroupDMSettings;->d:I

    invoke-static {}, Lcom/discord/utilities/channel/ChannelUtils;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object p1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/utilities/channel/ChannelUtils;->delete(Landroid/content/Context;J)V

    return-void
.end method
