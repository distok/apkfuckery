.class public final synthetic Lf/a/o/a/d1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

.field public final synthetic e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/d1;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

    iput-object p2, p0, Lf/a/o/a/d1;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lf/a/o/a/d1;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

    iget-object v0, p0, Lf/a/o/a/d1;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;->access$400(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-static {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;->access$100(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    invoke-static {p1, v1, v2, v3, v4}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;->create(Landroid/content/Context;JJ)V

    return-void
.end method
