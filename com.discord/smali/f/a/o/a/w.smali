.class public final synthetic Lf/a/o/a/w;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:J

.field public final synthetic e:J


# direct methods
.method public synthetic constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/o/a/w;->d:J

    iput-wide p3, p0, Lf/a/o/a/w;->e:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    iget-wide v0, p0, Lf/a/o/a/w;->d:J

    iget-wide v2, p0, Lf/a/o/a/w;->e:J

    check-cast p1, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/discord/stores/StoreGuilds;->observeRoles(J)Lrx/Observable;

    move-result-object v4

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->getMyId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v0, v1, v6}, Lcom/discord/stores/StoreGuilds;->observeComputed(JLjava/util/Collection;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/a/u;

    invoke-direct {v1, p1}, Lf/a/o/a/u;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/a/v;

    invoke-direct {v1, p1, v2, v3}, Lf/a/o/a/v;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;J)V

    invoke-static {v4, v0, v1}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
