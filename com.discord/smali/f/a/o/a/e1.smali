.class public final synthetic Lf/a/o/a/e1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

.field public final synthetic e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/e1;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

    iput-object p2, p0, Lf/a/o/a/e1;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 9

    iget-object v0, p0, Lf/a/o/a/e1;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

    iget-object v1, p0, Lf/a/o/a/e1;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;->access$400(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;->access$100(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    invoke-static/range {v2 .. v8}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->createForUser(Landroid/content/Context;JJJ)V

    return-void
.end method
