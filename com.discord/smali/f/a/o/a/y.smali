.class public final synthetic Lf/a/o/a/y;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action2;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;

.field public final synthetic e:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic f:J


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;Lcom/discord/models/domain/ModelChannel;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/y;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;

    iput-object p2, p0, Lf/a/o/a/y;->e:Lcom/discord/models/domain/ModelChannel;

    iput-wide p3, p0, Lf/a/o/a/y;->f:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Lf/a/o/a/y;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;

    iget-object v1, p0, Lf/a/o/a/y;->e:Lcom/discord/models/domain/ModelChannel;

    iget-wide v2, p0, Lf/a/o/a/y;->f:J

    check-cast p1, Landroid/view/MenuItem;

    check-cast p2, Landroid/content/Context;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const p2, 0x7f0a068e

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5, v2, v3}, Lcom/discord/utilities/rest/RestAPI;->deletePermissionOverwrites(JJ)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {v0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lf/a/o/a/b0;

    invoke-direct {p2, v0}, Lf/a/o/a/b0;-><init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;)V

    invoke-static {p2, v0}, Lf/a/b/r;->m(Lrx/functions/Action1;Lcom/discord/app/AppFragment;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    :goto_0
    return-void
.end method
