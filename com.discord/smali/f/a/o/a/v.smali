.class public final synthetic Lf/a/o/a/v;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func2;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

.field public final synthetic e:J


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/v;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    iput-wide p2, p0, Lf/a/o/a/v;->e:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lf/a/o/a/v;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    iget-wide v1, p0, Lf/a/o/a/v;->e:J

    check-cast p1, Ljava/util/Map;

    check-cast p2, Lcom/discord/models/domain/ModelGuildMember$Computed;

    invoke-static {v0, v1, v2, p1, p2}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;->a(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;JLjava/util/Map;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$ModelForRole;

    move-result-object p1

    return-object p1
.end method
