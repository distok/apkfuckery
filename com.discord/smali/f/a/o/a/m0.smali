.class public final synthetic Lf/a/o/a/m0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:Ljava/util/Map;

.field public final synthetic e:Ljava/util/Map;

.field public final synthetic f:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/m0;->d:Ljava/util/Map;

    iput-object p2, p0, Lf/a/o/a/m0;->e:Ljava/util/Map;

    iput-object p3, p0, Lf/a/o/a/m0;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lf/a/o/a/m0;->d:Ljava/util/Map;

    iget-object v1, p0, Lf/a/o/a/m0;->e:Ljava/util/Map;

    iget-object v2, p0, Lf/a/o/a/m0;->f:Ljava/lang/String;

    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->y(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object p1

    new-instance v3, Lf/a/o/a/n0;

    invoke-direct {v3, v0}, Lf/a/o/a/n0;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, v3}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lf/a/o/a/h0;

    invoke-direct {v0, v1}, Lf/a/o/a/h0;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lf/a/o/a/i0;

    invoke-direct {v0, v2}, Lf/a/o/a/i0;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lf/a/o/a/k0;

    invoke-direct {v0, v1}, Lf/a/o/a/k0;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->O(Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lf/a/o/a/p1;->d:Lf/a/o/a/p1;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->a0()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
