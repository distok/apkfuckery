.class public final synthetic Lf/a/o/a/l0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:Ljava/util/Map;

.field public final synthetic e:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/l0;->d:Ljava/util/Map;

    iput-object p2, p0, Lf/a/o/a/l0;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lf/a/o/a/l0;->d:Ljava/util/Map;

    iget-object v1, p0, Lf/a/o/a/l0;->e:Ljava/lang/String;

    check-cast p1, Ljava/util/Map;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/discord/stores/StoreUser;->observeUsers(Ljava/util/Collection;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lf/a/o/a/m0;

    invoke-direct {v3, v0, p1, v1}, Lf/a/o/a/m0;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
