.class public final synthetic Lf/a/o/a/u0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/u0;->d:Lcom/discord/models/domain/ModelChannel;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lf/a/o/a/u0;->d:Lcom/discord/models/domain/ModelChannel;

    check-cast p1, Ljava/util/List;

    new-instance v1, Lg0/l/a/t;

    invoke-direct {v1, p1}, Lg0/l/a/t;-><init>(Ljava/lang/Iterable;)V

    invoke-static {v1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lf/a/o/a/w0;

    invoke-direct {v1, v0}, Lf/a/o/a/w0;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-virtual {p1, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lf/a/o/a/b;->d:Lf/a/o/a/b;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->a0()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
