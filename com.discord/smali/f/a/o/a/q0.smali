.class public final synthetic Lf/a/o/a/q0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;

.field public final synthetic e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/q0;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;

    iput-object p2, p0, Lf/a/o/a/q0;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 9

    iget-object v0, p0, Lf/a/o/a/q0;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember;

    iget-object v1, p0, Lf/a/o/a/q0;->e:Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;->access$100(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v1}, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;->access$100(Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsAddMember$Model;)Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    invoke-static/range {v2 .. v8}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;->createForUser(Landroid/content/Context;JJJ)V

    return-void
.end method
