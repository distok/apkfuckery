.class public final synthetic Lf/a/o/a/o1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter$WidgetGroupInviteFriendsListItem;

.field public final synthetic e:Lcom/discord/models/domain/ModelUser;

.field public final synthetic f:Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter$WidgetGroupInviteFriendsListItem;Lcom/discord/models/domain/ModelUser;Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/o1;->d:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter$WidgetGroupInviteFriendsListItem;

    iput-object p2, p0, Lf/a/o/a/o1;->e:Lcom/discord/models/domain/ModelUser;

    iput-object p3, p0, Lf/a/o/a/o1;->f:Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lf/a/o/a/o1;->d:Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter$WidgetGroupInviteFriendsListItem;

    iget-object v0, p0, Lf/a/o/a/o1;->e:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p0, Lf/a/o/a/o1;->f:Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;

    iget-object v2, p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v2, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    invoke-virtual {v2}, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;->getListener()Lrx/functions/Action2;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object p1, p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p1, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;

    invoke-virtual {p1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriendsAdapter;->getListener()Lrx/functions/Action2;

    move-result-object p1

    invoke-virtual {v1}, Lcom/discord/widgets/channels/WidgetGroupInviteFriends$Model$FriendItem;->isSelected()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lrx/functions/Action2;->call(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
