.class public final synthetic Lf/a/o/a/u;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/a/u;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lf/a/o/a/u;->d:Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;

    check-cast p1, Ljava/util/Map;

    invoke-virtual {v0}, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions$BaseModel;->getMyId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuildMember$Computed;

    return-object p1
.end method
