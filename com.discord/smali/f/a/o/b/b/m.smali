.class public final synthetic Lf/a/o/b/b/m;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# static fields
.field public static final synthetic d:Lf/a/o/b/b/m;


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/o/b/b/m;

    invoke-direct {v0}, Lf/a/o/b/b/m;-><init>()V

    sput-object v0, Lf/a/o/b/b/m;->d:Lf/a/o/b/b/m;

    return-void
.end method

.method public synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lcom/discord/models/domain/ModelMessageReaction$Update;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessages()Lcom/discord/stores/StoreMessages;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/discord/stores/StoreMessages;->handleReactionUpdate(Ljava/util/List;Z)V

    return-void
.end method
