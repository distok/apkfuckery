.class public final synthetic Lf/a/o/b/b/a0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;

.field public final synthetic e:Lcom/discord/models/domain/ModelMessage;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;Lcom/discord/models/domain/ModelMessage;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/b/b/a0;->d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;

    iput-object p2, p0, Lf/a/o/b/b/a0;->e:Lcom/discord/models/domain/ModelMessage;

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lf/a/o/b/b/a0;->d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemMessage;

    iget-object v1, p0, Lf/a/o/b/b/a0;->e:Lcom/discord/models/domain/ModelMessage;

    check-cast p1, Lcom/discord/utilities/textprocessing/node/SpoilerNode;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessageState()Lcom/discord/stores/StoreMessageState;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/utilities/textprocessing/node/SpoilerNode;->getId()I

    move-result p1

    invoke-virtual {v2, v3, v4, p1}, Lcom/discord/stores/StoreMessageState;->revealSpoiler(JI)V

    iget-object p1, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
