.class public final synthetic Lf/a/o/b/b/b0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;

.field public final synthetic e:J


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/b/b/b0;->d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;

    iput-wide p2, p0, Lf/a/o/b/b/b0;->e:J

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lf/a/o/b/b/b0;->d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;

    iget-wide v0, p0, Lf/a/o/b/b/b0;->e:J

    iget-object p1, p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->onQuickAddReactionClicked(J)V

    return-void
.end method
