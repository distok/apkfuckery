.class public final synthetic Lf/a/o/b/b/j;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field public final synthetic d:Lcom/discord/models/domain/ModelMessageReaction;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelMessageReaction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/b/b/j;->d:Lcom/discord/models/domain/ModelMessageReaction;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lf/a/o/b/b/j;->d:Lcom/discord/models/domain/ModelMessageReaction;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->isMe()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getEmojis()Lcom/discord/stores/StoreEmoji;

    move-result-object p1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreEmoji;->onEmojiUsed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
