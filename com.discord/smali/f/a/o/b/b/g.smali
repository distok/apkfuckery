.class public final synthetic Lf/a/o/b/b/g;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Landroid/net/Uri;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Ljava/lang/ref/WeakReference;


# direct methods
.method public synthetic constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/ref/WeakReference;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/b/b/g;->d:Landroid/content/Context;

    iput-object p2, p0, Lf/a/o/b/b/g;->e:Landroid/net/Uri;

    iput-object p3, p0, Lf/a/o/b/b/g;->f:Ljava/lang/String;

    iput-object p4, p0, Lf/a/o/b/b/g;->g:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lf/a/o/b/b/g;->d:Landroid/content/Context;

    iget-object v1, p0, Lf/a/o/b/b/g;->e:Landroid/net/Uri;

    iget-object v2, p0, Lf/a/o/b/b/g;->f:Ljava/lang/String;

    iget-object v3, p0, Lf/a/o/b/b/g;->g:Ljava/lang/ref/WeakReference;

    new-instance v4, Lf/a/o/b/b/c;

    invoke-direct {v4, v3}, Lf/a/o/b/b/c;-><init>(Ljava/lang/ref/WeakReference;)V

    new-instance v5, Lf/a/o/b/b/d;

    invoke-direct {v5, v3}, Lf/a/o/b/b/d;-><init>(Ljava/lang/ref/WeakReference;)V

    const/4 v3, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/io/NetworkUtils;->downloadFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
