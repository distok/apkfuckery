.class public final synthetic Lf/a/o/b/b/d0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;

.field public final synthetic e:J

.field public final synthetic f:Lcom/discord/models/domain/ModelMessageReaction;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;JLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/b/b/d0;->d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;

    iput-wide p2, p0, Lf/a/o/b/b/d0;->e:J

    iput-object p4, p0, Lf/a/o/b/b/d0;->f:Lcom/discord/models/domain/ModelMessageReaction;

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 3

    iget-object p1, p0, Lf/a/o/b/b/d0;->d:Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemReactions;

    iget-wide v0, p0, Lf/a/o/b/b/d0;->e:J

    iget-object v2, p0, Lf/a/o/b/b/d0;->f:Lcom/discord/models/domain/ModelMessageReaction;

    iget-object p1, p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p1, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/widgets/chat/list/WidgetChatListAdapter;->onReactionLongClicked(JLcom/discord/models/domain/ModelMessageReaction;)V

    const/4 p1, 0x1

    return p1
.end method
