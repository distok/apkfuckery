.class public final synthetic Lf/a/o/b/b/a;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/chat/list/WidgetChatList$1;

.field public final synthetic e:Lcom/discord/models/domain/ModelApplication;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/chat/list/WidgetChatList$1;Lcom/discord/models/domain/ModelApplication;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/b/b/a;->d:Lcom/discord/widgets/chat/list/WidgetChatList$1;

    iput-object p2, p0, Lf/a/o/b/b/a;->e:Lcom/discord/models/domain/ModelApplication;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lf/a/o/b/b/a;->d:Lcom/discord/widgets/chat/list/WidgetChatList$1;

    iget-object v1, p0, Lf/a/o/b/b/a;->e:Lcom/discord/models/domain/ModelApplication;

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->getDeeplinkUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity$ActionConfirmation;->getSecret()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, v3, v4, p1}, Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$SDK;->join(Ljava/lang/String;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    :try_start_0
    iget-object v0, v0, Lcom/discord/widgets/chat/list/WidgetChatList$1;->this$0:Lcom/discord/widgets/chat/list/WidgetChatList;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    const/high16 v1, 0x10000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "SDK action unhandled."

    invoke-virtual {v0, v1, p1}, Lcom/discord/app/AppLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method
