.class public final synthetic Lf/a/o/b/a/e;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# instance fields
.field public final synthetic d:Lcom/discord/models/domain/emoji/Emoji;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/models/domain/emoji/Emoji;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/b/a/e;->d:Lcom/discord/models/domain/emoji/Emoji;

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf/a/o/b/a/e;->d:Lcom/discord/models/domain/emoji/Emoji;

    check-cast p1, Ljava/lang/Boolean;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Landroid/content/Context;

    sget p2, Lcom/discord/widgets/chat/input/WidgetChatInputCommandsAdapter$Item;->a:I

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/16 p2, 0x20

    invoke-interface {v0, p1, p2, p3}, Lcom/discord/models/domain/emoji/Emoji;->getImageUri(ZILandroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
