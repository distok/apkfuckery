.class public final Lf/a/h/u/i;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/h/u/a$c;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $ready:Lcom/discord/rtcconnection/socket/io/Payloads$Ready;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/socket/io/Payloads$Ready;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/i;->$ready:Lcom/discord/rtcconnection/socket/io/Payloads$Ready;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lf/a/h/u/a$c;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/u/i;->$ready:Lcom/discord/rtcconnection/socket/io/Payloads$Ready;

    invoke-virtual {v0}, Lcom/discord/rtcconnection/socket/io/Payloads$Ready;->getPort()I

    move-result v0

    iget-object v1, p0, Lf/a/h/u/i;->$ready:Lcom/discord/rtcconnection/socket/io/Payloads$Ready;

    invoke-virtual {v1}, Lcom/discord/rtcconnection/socket/io/Payloads$Ready;->getSsrc()I

    move-result v1

    iget-object v2, p0, Lf/a/h/u/i;->$ready:Lcom/discord/rtcconnection/socket/io/Payloads$Ready;

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Ready;->getIp()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lf/a/h/u/a$c;->k(IILjava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
