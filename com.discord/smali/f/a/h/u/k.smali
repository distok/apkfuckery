.class public final Lf/a/h/u/k;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/h/u/a$c;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $sessionUpdate:Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/k;->$sessionUpdate:Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lf/a/h/u/a$c;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/u/k;->$sessionUpdate:Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;

    invoke-virtual {v0}, Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;->getMediaSessionId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lf/a/h/u/a$c;->e(Ljava/lang/String;)V

    :cond_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
