.class public final Lf/a/h/u/a$l;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/u/a;->onMessage(Lokhttp3/WebSocket;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

.field public final synthetic $webSocket:Lokhttp3/WebSocket;

.field public final synthetic this$0:Lf/a/h/u/a;


# direct methods
.method public constructor <init>(Lf/a/h/u/a;Lokhttp3/WebSocket;Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iput-object p2, p0, Lf/a/h/u/a$l;->$webSocket:Lokhttp3/WebSocket;

    iput-object p3, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 13

    iget-object v0, p0, Lf/a/h/u/a$l;->$webSocket:Lokhttp3/WebSocket;

    iget-object v1, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v1, v1, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto/16 :goto_4

    :cond_0
    iget-object v0, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v0}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getOpcode()I

    move-result v0

    const-string v2, " ms"

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    goto/16 :goto_3

    :pswitch_1
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v1, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v2, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v2

    const-class v3, Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;

    invoke-virtual {v1, v2, v3}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "Gson.fromJson(message.da\u2026essionUpdate::class.java)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;

    new-instance v2, Lf/a/h/u/k;

    invoke-direct {v2, v1}, Lf/a/h/u/k;-><init>(Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;)V

    invoke-virtual {v0, v2}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_4

    :pswitch_2
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v1, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v2, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v2

    const-class v3, Lcom/discord/rtcconnection/socket/io/Payloads$ClientDisconnect;

    invoke-virtual {v1, v2, v3}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "Gson.fromJson(message.da\u2026ntDisconnect::class.java)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/discord/rtcconnection/socket/io/Payloads$ClientDisconnect;

    new-instance v2, Lf/a/h/u/d;

    invoke-direct {v2, v1}, Lf/a/h/u/d;-><init>(Lcom/discord/rtcconnection/socket/io/Payloads$ClientDisconnect;)V

    invoke-virtual {v0, v2}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_4

    :pswitch_3
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v1, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v2, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v2

    const-class v3, Lcom/discord/rtcconnection/socket/io/Payloads$Video;

    invoke-virtual {v1, v2, v3}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "Gson.fromJson(message.da\u2026yloads.Video::class.java)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/discord/rtcconnection/socket/io/Payloads$Video;

    invoke-virtual {v1}, Lcom/discord/rtcconnection/socket/io/Payloads$Video;->getUserId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_7

    new-instance v2, Lf/a/h/u/m;

    invoke-direct {v2, v1}, Lf/a/h/u/m;-><init>(Lcom/discord/rtcconnection/socket/io/Payloads$Video;)V

    invoke-virtual {v0, v2}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_4

    :pswitch_4
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v0, v0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    goto/16 :goto_4

    :pswitch_5
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v2, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v3, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v3

    const-class v4, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Gson.fromJson(message.da\u2026yloads.Hello::class.java)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;

    iget-boolean v3, v0, Lf/a/h/u/a;->q:Z

    if-eqz v3, :cond_1

    iget-object v4, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v5, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    const-string v6, "handleHello called on canceled instance of RtcControlSocket"

    invoke-static/range {v4 .. v9}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto/16 :goto_4

    :cond_1
    iget-object v3, v0, Lf/a/h/u/a;->o:Ljava/util/TimerTask;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/util/TimerTask;->cancel()Z

    :cond_2
    iget-object v4, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v5, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[HELLO] raw: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Hello;->getHeartbeatIntervalMs()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lf/a/h/u/a;->k:Ljava/lang/Long;

    iget-object v2, v0, Lf/a/h/u/a;->m:Ljava/util/TimerTask;

    invoke-virtual {v2}, Ljava/util/TimerTask;->cancel()Z

    iput-boolean v1, v0, Lf/a/h/u/a;->l:Z

    invoke-virtual {v0}, Lf/a/h/u/a;->j()V

    goto/16 :goto_4

    :pswitch_6
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v3, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v4, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v4}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v4

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "Gson.fromJson(message.data, Long::class.java)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    iget-boolean v5, v0, Lf/a/h/u/a;->q:Z

    if-eqz v5, :cond_3

    iget-object v6, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v7, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    const-string v8, "handleHeartbeatAck called on canceled instance of RtcControlSocket"

    invoke-static/range {v6 .. v11}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto/16 :goto_4

    :cond_3
    iget-object v5, v0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    invoke-interface {v5}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v5

    sub-long v3, v5, v3

    iget-object v7, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v8, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string v9, "got heartbeat ack after "

    invoke-static {v9, v3, v4, v2}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/logging/Logger;->d$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lf/a/h/u/a;->n:Ljava/lang/Long;

    iput-boolean v1, v0, Lf/a/h/u/a;->l:Z

    new-instance v1, Lf/a/h/u/f;

    invoke-direct {v1, v3, v4}, Lf/a/h/u/f;-><init>(J)V

    invoke-virtual {v0, v1}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_4

    :pswitch_7
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v2, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v3, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v3

    const-class v4, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Gson.fromJson(message.da\u2026ads.Speaking::class.java)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;->getUserId()Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;->getSpeaking()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;->getSpeaking()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v1, :cond_5

    goto :goto_1

    :cond_5
    :goto_0
    const/4 v1, 0x0

    :goto_1
    new-instance v3, Lf/a/h/u/l;

    invoke-direct {v3, v2, v1}, Lf/a/h/u/l;-><init>(Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;Z)V

    invoke-virtual {v0, v3}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_4

    :pswitch_8
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v2, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v3, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v3

    const-class v4, Lcom/discord/rtcconnection/socket/io/Payloads$Description;

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Gson.fromJson(message.da\u2026.Description::class.java)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/discord/rtcconnection/socket/io/Payloads$Description;

    new-instance v3, Lf/a/h/u/j;

    invoke-direct {v3, v2}, Lf/a/h/u/j;-><init>(Lcom/discord/rtcconnection/socket/io/Payloads$Description;)V

    invoke-virtual {v0, v3}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    iput-boolean v1, v0, Lf/a/h/u/a;->h:Z

    goto :goto_4

    :pswitch_9
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v1, v0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lf/a/h/u/a;->m(ILjava/lang/Object;)V

    goto :goto_4

    :pswitch_a
    iget-object v0, p0, Lf/a/h/u/a$l;->this$0:Lf/a/h/u/a;

    iget-object v1, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    iget-object v3, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getData()Lcom/google/gson/JsonElement;

    move-result-object v3

    const-class v4, Lcom/discord/rtcconnection/socket/io/Payloads$Ready;

    invoke-virtual {v1, v3, v4}, Lcom/google/gson/Gson;->c(Lcom/google/gson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    const-string v3, "Gson.fromJson(message.da\u2026yloads.Ready::class.java)"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/discord/rtcconnection/socket/io/Payloads$Ready;

    iget-object v3, v0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v3}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    iget-object v3, v0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    invoke-interface {v3}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v3

    iget-object v5, v0, Lf/a/h/u/a;->j:Ljava/lang/Long;

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_2

    :cond_6
    const-wide/16 v5, 0x0

    :goto_2
    sub-long/2addr v3, v5

    iget-object v5, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v6, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string v7, "[READY] took "

    invoke-static {v7, v3, v4, v2}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    new-instance v2, Lf/a/h/u/i;

    invoke-direct {v2, v1}, Lf/a/h/u/i;-><init>(Lcom/discord/rtcconnection/socket/io/Payloads$Ready;)V

    invoke-virtual {v0, v2}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto :goto_4

    :goto_3
    iget-object v1, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string/jumbo v0, "unknown opcode: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lf/a/h/u/a$l;->$message:Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;->getOpcode()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    :cond_7
    :goto_4
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
