.class public final Lf/a/h/u/a;
.super Lokhttp3/WebSocketListener;
.source "RtcControlSocket.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/h/u/a$c;,
        Lf/a/h/u/a$b;,
        Lf/a/h/u/a$a;
    }
.end annotation


# static fields
.field public static x:I

.field public static final y:Lf/a/h/u/a$a;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/gson/Gson;

.field public final c:Ljava/util/Timer;

.field public final d:Lcom/discord/utilities/networking/Backoff;

.field public e:Lokhttp3/WebSocket;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Lf/a/h/u/a$b;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/Long;

.field public l:Z

.field public m:Ljava/util/TimerTask;

.field public n:Ljava/lang/Long;

.field public o:Ljava/util/TimerTask;

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/a/h/u/a$c;",
            ">;"
        }
    .end annotation
.end field

.field public q:Z

.field public final r:Ljava/lang/String;

.field public final s:Ljava/lang/String;

.field public final t:Ljavax/net/ssl/SSLSocketFactory;

.field public final u:Lcom/discord/utilities/logging/Logger;

.field public final v:Ljava/util/concurrent/ExecutorService;

.field public final w:Lcom/discord/utilities/time/Clock;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/a/h/u/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/a/h/u/a$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lf/a/h/u/a;->y:Lf/a/h/u/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;Lcom/discord/utilities/logging/Logger;Ljava/util/concurrent/ExecutorService;Lcom/discord/utilities/time/Clock;)V
    .locals 12

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    const-string v6, "endpoint"

    invoke-static {p1, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v6, "token"

    invoke-static {p2, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "logger"

    invoke-static {v3, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "singleThreadExecutorService"

    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "clock"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lokhttp3/WebSocketListener;-><init>()V

    iput-object v1, v0, Lf/a/h/u/a;->r:Ljava/lang/String;

    iput-object v2, v0, Lf/a/h/u/a;->s:Ljava/lang/String;

    move-object v1, p3

    iput-object v1, v0, Lf/a/h/u/a;->t:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v3, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iput-object v4, v0, Lf/a/h/u/a;->v:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lf/a/h/u/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v2, Lf/a/h/u/a;->x:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lf/a/h/u/a;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance v1, Lf/h/d/k;

    invoke-direct {v1}, Lf/h/d/k;-><init>()V

    invoke-virtual {v1}, Lf/h/d/k;->a()Lcom/google/gson/Gson;

    move-result-object v1

    iput-object v1, v0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, v0, Lf/a/h/u/a;->c:Ljava/util/Timer;

    new-instance v1, Lcom/discord/utilities/networking/Backoff;

    const-wide/16 v3, 0x3e8

    const-wide/16 v5, 0x1388

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x18

    const/4 v11, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v11}, Lcom/discord/utilities/networking/Backoff;-><init>(JJIZLcom/discord/utilities/networking/Backoff$Scheduler;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v1, v0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    sget-object v1, Lf/a/h/u/a$b;->d:Lf/a/h/u/a$b;

    iput-object v1, v0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    new-instance v1, Lf/a/h/u/c;

    invoke-direct {v1, p0}, Lf/a/h/u/c;-><init>(Lf/a/h/u/a;)V

    iput-object v1, v0, Lf/a/h/u/a;->m:Ljava/util/TimerTask;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lf/a/h/u/a;->p:Ljava/util/List;

    return-void
.end method

.method public static final a(Lf/a/h/u/a;ZLjava/lang/Integer;Ljava/lang/String;)V
    .locals 8

    sget-object v0, Lf/a/h/u/a$b;->d:Lf/a/h/u/a$b;

    iput-object v0, p0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    const/4 v0, 0x1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0xfa4

    if-eq v1, v2, :cond_6

    :goto_0
    const/16 v1, 0xfaf

    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v1, :cond_6

    :goto_1
    const/16 v1, 0xfab

    if-nez p2, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v1, :cond_6

    :goto_2
    const/16 v1, 0xfa6

    if-nez p2, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_4

    goto :goto_4

    :cond_4
    :goto_3
    iget-object v1, p0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v1}, Lcom/discord/utilities/networking/Backoff;->hasReachedFailureThreshold()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v2, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v3, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v4, "[WS CLOSED] Backoff exceeded. Resetting."

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    invoke-virtual {p0, p1, p2, p3}, Lf/a/h/u/a;->g(ZLjava/lang/Integer;Ljava/lang/String;)V

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    iget-object v1, p0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    new-instance v2, Lf/a/h/u/e;

    invoke-direct {v2, p0, p1, p2, p3}, Lf/a/h/u/e;-><init>(Lf/a/h/u/a;ZLjava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/networking/Backoff;->fail(Lkotlin/jvm/functions/Function0;)J

    move-result-wide v1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    long-to-double v1, v1

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v1, v5

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v3, v4

    const-string v1, "%.2f"

    const-string v2, "java.lang.String.format(this, *args)"

    invoke-static {v3, v0, v1, v2}, Lf/e/c/a/a;->D([Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "`[WS CLOSED] ("

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, ", "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ") retrying in "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " seconds."

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto :goto_5

    :cond_6
    :goto_4
    invoke-virtual {p0, v0, p2, p3}, Lf/a/h/u/a;->g(ZLjava/lang/Integer;Ljava/lang/String;)V

    :goto_5
    return-void
.end method

.method public static synthetic c(Lf/a/h/u/a;Lkotlin/jvm/functions/Function1;I)V
    .locals 0

    and-int/lit8 p1, p2, 0x1

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public final b(Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lokhttp3/WebSocket;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lf/a/h/u/a;->q:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, "cleanupWebsocket called on canceled instance of RtcControlSocket"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->cancel()V

    iget-object v0, p0, Lf/a/h/u/a;->m:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    iget-object v0, p0, Lf/a/h/u/a;->o:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    :cond_1
    iget-object v0, p0, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_2
    const/4 p1, 0x0

    check-cast p1, Lokhttp3/WebSocket;

    iput-object p1, p0, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    return-void
.end method

.method public final d()V
    .locals 13

    iget-boolean v0, p0, Lf/a/h/u/a;->q:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, "close called on canceled instance of RtcControlSocket"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v7, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v8, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    const-string v9, "[CLOSE]"

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object v0, Lf/a/h/u/a$d;->d:Lf/a/h/u/a$d;

    invoke-virtual {p0, v0}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/a/h/u/a;->f:Ljava/lang/String;

    iput-object v0, p0, Lf/a/h/u/a;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/a/h/u/a;->h:Z

    sget-object v0, Lf/a/h/u/a$b;->d:Lf/a/h/u/a$b;

    iput-object v0, p0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    sget-object v0, Lf/a/h/u/a$e;->d:Lf/a/h/u/a$e;

    invoke-virtual {p0, v0}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final e()Z
    .locals 9

    iget-boolean v0, p0, Lf/a/h/u/a;->q:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v3, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v4, "Connect called on canceled instance of RtcControlSocket"

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return v1

    :cond_0
    iget-object v0, p0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    sget-object v2, Lf/a/h/u/a$b;->d:Lf/a/h/u/a$b;

    if-eq v0, v2, :cond_1

    iget-object v3, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v4, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    const-string v5, "Cannot start a new connection, connection state is not disconnected"

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return v1

    :cond_1
    sget-object v0, Lf/a/h/u/a$b;->e:Lf/a/h/u/a$b;

    iput-object v0, p0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    invoke-virtual {p0}, Lf/a/h/u/a;->f()V

    const/4 v0, 0x1

    return v0
.end method

.method public final f()V
    .locals 13

    iget-boolean v0, p0, Lf/a/h/u/a;->q:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, "connectInternal called on canceled instance of RtcControlSocket"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v7, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v8, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string v0, "[CONNECT] "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/a/h/u/a;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    iget-object v0, p0, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    const-string v3, "Connect called with already existing websocket"

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    sget-object v0, Lf/a/h/u/a$f;->d:Lf/a/h/u/a$f;

    invoke-virtual {p0, v0}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lf/a/h/u/a;->j:Ljava/lang/Long;

    iget-object v0, p0, Lf/a/h/u/a;->o:Ljava/util/TimerTask;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    :cond_2
    new-instance v0, Lf/a/h/u/a$g;

    invoke-direct {v0, p0}, Lf/a/h/u/a$g;-><init>(Lf/a/h/u/a;)V

    iput-object v0, p0, Lf/a/h/u/a;->o:Ljava/util/TimerTask;

    iget-object v1, p0, Lf/a/h/u/a;->c:Ljava/util/Timer;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    new-instance v0, Lb0/y$a;

    invoke-direct {v0}, Lb0/y$a;-><init>()V

    const-wide/16 v1, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lb0/y$a;->a(JLjava/util/concurrent/TimeUnit;)Lb0/y$a;

    iget-object v1, p0, Lf/a/h/u/a;->t:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_3

    sget-object v2, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object v2, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {v2}, Lb0/g0/k/h;->n()Ljavax/net/ssl/X509TrustManager;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lb0/y$a;->b(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/X509TrustManager;)Lb0/y$a;

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lf/a/h/u/a;->r:Ljava/lang/String;

    const-string v3, "?v=4"

    invoke-static {v1, v2, v3}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v3, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string v4, "attempting WSS connection with "

    invoke-static {v4, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    new-instance v2, Lb0/y;

    invoke-direct {v2, v0}, Lb0/y;-><init>(Lb0/y$a;)V

    new-instance v0, Lb0/a0$a;

    invoke-direct {v0}, Lb0/a0$a;-><init>()V

    invoke-virtual {v0, v1}, Lb0/a0$a;->f(Ljava/lang/String;)Lb0/a0$a;

    invoke-virtual {v0}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Lb0/y;->f(Lb0/a0;Lokhttp3/WebSocketListener;)Lokhttp3/WebSocket;

    move-result-object v0

    iput-object v0, p0, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    sget-object v0, Lf/a/h/u/a$h;->d:Lf/a/h/u/a$h;

    invoke-virtual {p0, v0}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final g(ZLjava/lang/Integer;Ljava/lang/String;)V
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    iget-boolean v4, v0, Lf/a/h/u/a;->q:Z

    if-eqz v4, :cond_0

    iget-object v5, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v6, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    const-string v7, "disconnect called on canceled instance of RtcControlSocket"

    invoke-static/range {v5 .. v10}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v11, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v12, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[DISCONNECT] ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x29

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    iput-object v4, v0, Lf/a/h/u/a;->f:Ljava/lang/String;

    iput-object v4, v0, Lf/a/h/u/a;->g:Ljava/lang/String;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lf/a/h/u/a;->h:Z

    sget-object v4, Lf/a/h/u/a$b;->d:Lf/a/h/u/a$b;

    iput-object v4, v0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    new-instance v4, Lf/a/h/u/a$i;

    invoke-direct {v4, v1, v2, v3}, Lf/a/h/u/a$i;-><init>(ZLjava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final h(JLjava/lang/String;Z)V
    .locals 7

    const-string v0, "reason"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lf/a/h/u/a;->q:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, "immediateHeartbeat called on canceled instance of RtcControlSocket"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v2, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string p4, "Performing an immediate heartbeat on existing socket: "

    invoke-static {p4, p3}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    iget-object p3, p0, Lf/a/h/u/a;->m:Ljava/util/TimerTask;

    invoke-virtual {p3}, Ljava/util/TimerTask;->cancel()Z

    new-instance p3, Lf/a/h/u/c;

    invoke-direct {p3, p0}, Lf/a/h/u/c;-><init>(Lf/a/h/u/a;)V

    iput-object p3, p0, Lf/a/h/u/a;->m:Ljava/util/TimerTask;

    iget-object p4, p0, Lf/a/h/u/a;->c:Ljava/util/Timer;

    invoke-virtual {p4, p3, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    iget-object p1, p0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {p1}, Lcom/discord/utilities/networking/Backoff;->isPending()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    if-nez p1, :cond_3

    iget-object v0, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v1, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string p1, "Connection backoff reset "

    const-string p2, "Immediate heartbeat when socket was disconnected."

    invoke-static {p1, p2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    iget-object p1, p0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {p1}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    const/4 p1, 0x0

    const/16 p2, 0x12c2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string p3, "Reset backoff."

    invoke-virtual {p0, p1, p2, p3}, Lf/a/h/u/a;->k(ZLjava/lang/Integer;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v1, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string p1, "Immediate heartbeat requested, but is disconnected and a reset was not requested: "

    invoke-static {p1, p3}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public final i(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lf/a/h/u/a$c;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/a/h/u/a;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/a/h/u/a$c;

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final j()V
    .locals 18

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lf/a/h/u/a;->q:Z

    if-eqz v1, :cond_0

    iget-object v2, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v3, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v4, "onHeartbeatInterval called on canceled instance of RtcControlSocket"

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-boolean v2, v0, Lf/a/h/u/a;->l:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    iput-boolean v3, v0, Lf/a/h/u/a;->l:Z

    iget-object v1, v0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lf/a/h/u/a;->m(ILjava/lang/Object;)V

    iget-object v1, v0, Lf/a/h/u/a;->k:Ljava/lang/Long;

    if-eqz v1, :cond_1

    new-instance v2, Lf/a/h/u/c;

    invoke-direct {v2, v0}, Lf/a/h/u/c;-><init>(Lf/a/h/u/a;)V

    iput-object v2, v0, Lf/a/h/u/a;->m:Ljava/util/TimerTask;

    iget-object v3, v0, Lf/a/h/u/a;->c:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    :cond_1
    iget-object v6, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v7, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    const-string v8, "onHeartbeatInterval called when heartbeatInterval was null"

    invoke-static/range {v6 .. v11}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    iget-object v12, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v13, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    const-string v14, "handleHeartbeatTimeout called on canceled instance of RtcControlSocket"

    invoke-static/range {v12 .. v17}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    sget-object v1, Lf/a/h/u/g;->d:Lf/a/h/u/g;

    invoke-virtual {v0, v1}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    iget-object v1, v0, Lf/a/h/u/a;->d:Lcom/discord/utilities/networking/Backoff;

    new-instance v2, Lf/a/h/u/h;

    invoke-direct {v2, v0}, Lf/a/h/u/h;-><init>(Lf/a/h/u/a;)V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/networking/Backoff;->fail(Lkotlin/jvm/functions/Function0;)J

    move-result-wide v1

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    long-to-double v1, v1

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v1, v6

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v5, v3

    const-string v1, "%.2f"

    const-string v2, "java.lang.String.format(this, *args)"

    invoke-static {v5, v4, v1, v2}, Lf/e/c/a/a;->D([Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v3, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string v4, "[ACK TIMEOUT] reconnecting in "

    const-string v5, " seconds."

    invoke-static {v4, v1, v5}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final k(ZLjava/lang/Integer;Ljava/lang/String;)V
    .locals 14

    move-object v0, p0

    iget-boolean v1, v0, Lf/a/h/u/a;->q:Z

    if-eqz v1, :cond_0

    iget-object v2, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v3, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v4, "reconnect called on canceled instance of RtcControlSocket"

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v8, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v9, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RECONNECT] wasFatal="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, " code="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " reason="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v2, p3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    invoke-static/range {v8 .. v13}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object v1, Lf/a/h/u/a$n;->d:Lf/a/h/u/a$n;

    invoke-virtual {p0, v1}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    sget-object v1, Lf/a/h/u/a$b;->i:Lf/a/h/u/a$b;

    iput-object v1, v0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    invoke-virtual {p0}, Lf/a/h/u/a;->f()V

    return-void
.end method

.method public final l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lf/a/h/u/a;->v:Ljava/util/concurrent/ExecutorService;

    if-eqz p1, :cond_0

    new-instance v1, Lf/a/h/u/r;

    invoke-direct {v1, p1}, Lf/a/h/u/r;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p1, v1

    :cond_0
    check-cast p1, Ljava/lang/Runnable;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public final m(ILjava/lang/Object;)V
    .locals 9

    iget-object v0, p0, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    new-instance v2, Lcom/discord/rtcconnection/socket/io/Payloads$Outgoing;

    invoke-direct {v2, p1, p2}, Lcom/discord/rtcconnection/socket/io/Payloads$Outgoing;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->k(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v3, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sending: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->d$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    const-string v2, "json"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lokhttp3/WebSocket;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object v3, p0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v4, p0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exception sending opcode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " and payload: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Lokhttp3/WebSocketListener;->onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V

    new-instance v0, Lf/a/h/u/a$j;

    invoke-direct {v0, p0, p1, p2, p3}, Lf/a/h/u/a$j;-><init>(Lf/a/h/u/a;Lokhttp3/WebSocket;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lf/a/h/u/a;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public onFailure(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "throwable"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Lokhttp3/WebSocketListener;->onFailure(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V

    new-instance p3, Lf/a/h/u/a$k;

    invoke-direct {p3, p0, p1, p2}, Lf/a/h/u/a$k;-><init>(Lf/a/h/u/a;Lokhttp3/WebSocket;Ljava/lang/Throwable;)V

    invoke-virtual {p0, p3}, Lf/a/h/u/a;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public onMessage(Lokhttp3/WebSocket;Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "text"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/u/a;->b:Lcom/google/gson/Gson;

    const-class v1, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-virtual {v0, p2, v1}, Lcom/google/gson/Gson;->f(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Lf/h/a/f/f/n/g;->k0(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;

    invoke-super {p0, p1, p2}, Lokhttp3/WebSocketListener;->onMessage(Lokhttp3/WebSocket;Ljava/lang/String;)V

    new-instance p2, Lf/a/h/u/a$l;

    invoke-direct {p2, p0, p1, v0}, Lf/a/h/u/a$l;-><init>(Lf/a/h/u/a;Lokhttp3/WebSocket;Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;)V

    invoke-virtual {p0, p2}, Lf/a/h/u/a;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public onOpen(Lokhttp3/WebSocket;Lokhttp3/Response;)V
    .locals 1

    const-string/jumbo v0, "webSocket"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lokhttp3/WebSocketListener;->onOpen(Lokhttp3/WebSocket;Lokhttp3/Response;)V

    new-instance p2, Lf/a/h/u/a$m;

    invoke-direct {p2, p0, p1}, Lf/a/h/u/a$m;-><init>(Lf/a/h/u/a;Lokhttp3/WebSocket;)V

    invoke-virtual {p0, p2}, Lf/a/h/u/a;->l(Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    return-void
.end method
