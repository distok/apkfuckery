.class public final Lf/a/h/u/l;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/h/u/a$c;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isSpeaking:Z

.field public final synthetic $speaking:Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;Z)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/l;->$speaking:Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;

    iput-boolean p2, p0, Lf/a/h/u/l;->$isSpeaking:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Lf/a/h/u/a$c;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/u/l;->$speaking:Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;

    invoke-virtual {v0}, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;->getUserId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lf/a/h/u/l;->$speaking:Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;->getSsrc()I

    move-result v2

    iget-boolean v3, p0, Lf/a/h/u/l;->$isSpeaking:Z

    invoke-interface {p1, v0, v1, v2, v3}, Lf/a/h/u/a$c;->onSpeaking(JIZ)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
