.class public final Lf/a/h/u/a$j;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/u/a;->onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $code:I

.field public final synthetic $reason:Ljava/lang/String;

.field public final synthetic $webSocket:Lokhttp3/WebSocket;

.field public final synthetic this$0:Lf/a/h/u/a;


# direct methods
.method public constructor <init>(Lf/a/h/u/a;Lokhttp3/WebSocket;ILjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/a$j;->this$0:Lf/a/h/u/a;

    iput-object p2, p0, Lf/a/h/u/a$j;->$webSocket:Lokhttp3/WebSocket;

    iput p3, p0, Lf/a/h/u/a$j;->$code:I

    iput-object p4, p0, Lf/a/h/u/a$j;->$reason:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lf/a/h/u/a$j;->$webSocket:Lokhttp3/WebSocket;

    iget-object v1, p0, Lf/a/h/u/a$j;->this$0:Lf/a/h/u/a;

    iget-object v1, v1, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/a/h/u/a$j;->this$0:Lf/a/h/u/a;

    iget v2, p0, Lf/a/h/u/a$j;->$code:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lf/a/h/u/a$j;->$reason:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lf/a/h/u/a;->a(Lf/a/h/u/a;ZLjava/lang/Integer;Ljava/lang/String;)V

    :goto_0
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
