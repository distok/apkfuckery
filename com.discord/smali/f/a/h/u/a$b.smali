.class public final enum Lf/a/h/u/a$b;
.super Ljava/lang/Enum;
.source "RtcControlSocket.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/h/u/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/a/h/u/a$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/a/h/u/a$b;

.field public static final enum e:Lf/a/h/u/a$b;

.field public static final enum f:Lf/a/h/u/a$b;

.field public static final enum g:Lf/a/h/u/a$b;

.field public static final enum h:Lf/a/h/u/a$b;

.field public static final enum i:Lf/a/h/u/a$b;

.field public static final synthetic j:[Lf/a/h/u/a$b;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lf/a/h/u/a$b;

    new-instance v1, Lf/a/h/u/a$b;

    const-string v2, "DISCONNECTED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lf/a/h/u/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/a/h/u/a$b;->d:Lf/a/h/u/a$b;

    aput-object v1, v0, v3

    new-instance v1, Lf/a/h/u/a$b;

    const-string v2, "CONNECTING"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lf/a/h/u/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/a/h/u/a$b;->e:Lf/a/h/u/a$b;

    aput-object v1, v0, v3

    new-instance v1, Lf/a/h/u/a$b;

    const-string v2, "IDENTIFYING"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lf/a/h/u/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/a/h/u/a$b;->f:Lf/a/h/u/a$b;

    aput-object v1, v0, v3

    new-instance v1, Lf/a/h/u/a$b;

    const-string v2, "RESUMING"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lf/a/h/u/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/a/h/u/a$b;->g:Lf/a/h/u/a$b;

    aput-object v1, v0, v3

    new-instance v1, Lf/a/h/u/a$b;

    const-string v2, "CONNECTED"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lf/a/h/u/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/a/h/u/a$b;->h:Lf/a/h/u/a$b;

    aput-object v1, v0, v3

    new-instance v1, Lf/a/h/u/a$b;

    const-string v2, "RECONNECTING"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lf/a/h/u/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/a/h/u/a$b;->i:Lf/a/h/u/a$b;

    aput-object v1, v0, v3

    sput-object v0, Lf/a/h/u/a$b;->j:[Lf/a/h/u/a$b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/a/h/u/a$b;
    .locals 1

    const-class v0, Lf/a/h/u/a$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/a/h/u/a$b;

    return-object p0
.end method

.method public static values()[Lf/a/h/u/a$b;
    .locals 1

    sget-object v0, Lf/a/h/u/a$b;->j:[Lf/a/h/u/a$b;

    invoke-virtual {v0}, [Lf/a/h/u/a$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/a/h/u/a$b;

    return-object v0
.end method
