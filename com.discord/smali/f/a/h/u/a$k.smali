.class public final Lf/a/h/u/a$k;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/u/a;->onFailure(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $throwable:Ljava/lang/Throwable;

.field public final synthetic $webSocket:Lokhttp3/WebSocket;

.field public final synthetic this$0:Lf/a/h/u/a;


# direct methods
.method public constructor <init>(Lf/a/h/u/a;Lokhttp3/WebSocket;Ljava/lang/Throwable;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/a$k;->this$0:Lf/a/h/u/a;

    iput-object p2, p0, Lf/a/h/u/a$k;->$webSocket:Lokhttp3/WebSocket;

    iput-object p3, p0, Lf/a/h/u/a$k;->$throwable:Ljava/lang/Throwable;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lf/a/h/u/a$k;->$webSocket:Lokhttp3/WebSocket;

    iget-object v1, p0, Lf/a/h/u/a$k;->this$0:Lf/a/h/u/a;

    iget-object v1, v1, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/a/h/u/a$k;->$throwable:Ljava/lang/Throwable;

    instance-of v0, v0, Ljavax/net/ssl/SSLException;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/a/h/u/a$k;->this$0:Lf/a/h/u/a;

    invoke-static {v0, v2, v1}, Lf/a/h/u/a;->c(Lf/a/h/u/a;Lkotlin/jvm/functions/Function1;I)V

    iget-object v0, p0, Lf/a/h/u/a$k;->this$0:Lf/a/h/u/a;

    sget-object v1, Lf/a/h/u/n;->d:Lf/a/h/u/n;

    invoke-virtual {v0, v1}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/a/h/u/a$k;->this$0:Lf/a/h/u/a;

    const-string v3, "An error with the websocket occurred: "

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lf/a/h/u/a$k;->$throwable:Ljava/lang/Throwable;

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lf/a/h/u/a;->a(Lf/a/h/u/a;ZLjava/lang/Integer;Ljava/lang/String;)V

    :goto_0
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
