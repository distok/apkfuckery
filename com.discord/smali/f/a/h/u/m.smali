.class public final Lf/a/h/u/m;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/a/h/u/a$c;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $video:Lcom/discord/rtcconnection/socket/io/Payloads$Video;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/socket/io/Payloads$Video;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/m;->$video:Lcom/discord/rtcconnection/socket/io/Payloads$Video;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Lf/a/h/u/a$c;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/u/m;->$video:Lcom/discord/rtcconnection/socket/io/Payloads$Video;

    invoke-virtual {v0}, Lcom/discord/rtcconnection/socket/io/Payloads$Video;->getUserId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lf/a/h/u/m;->$video:Lcom/discord/rtcconnection/socket/io/Payloads$Video;

    invoke-virtual {v2}, Lcom/discord/rtcconnection/socket/io/Payloads$Video;->getAudioSsrc()I

    move-result v2

    iget-object v3, p0, Lf/a/h/u/m;->$video:Lcom/discord/rtcconnection/socket/io/Payloads$Video;

    invoke-virtual {v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Video;->getVideoSsrc()I

    move-result v3

    invoke-interface {p1, v0, v1, v2, v3}, Lf/a/h/u/a$c;->c(JII)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
