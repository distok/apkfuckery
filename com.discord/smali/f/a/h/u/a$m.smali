.class public final Lf/a/h/u/a$m;
.super Lx/m/c/k;
.source "RtcControlSocket.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/u/a;->onOpen(Lokhttp3/WebSocket;Lokhttp3/Response;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $webSocket:Lokhttp3/WebSocket;

.field public final synthetic this$0:Lf/a/h/u/a;


# direct methods
.method public constructor <init>(Lf/a/h/u/a;Lokhttp3/WebSocket;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    iput-object p2, p0, Lf/a/h/u/a$m;->$webSocket:Lokhttp3/WebSocket;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 13

    iget-object v0, p0, Lf/a/h/u/a$m;->$webSocket:Lokhttp3/WebSocket;

    iget-object v1, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    iget-object v1, v1, Lf/a/h/u/a;->e:Lokhttp3/WebSocket;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto/16 :goto_3

    :cond_0
    iget-object v0, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    iget-object v2, v0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    sget-object v3, Lf/a/h/u/a$b;->e:Lf/a/h/u/a$b;

    if-ne v2, v3, :cond_1

    sget-object v1, Lf/a/h/u/o;->d:Lf/a/h/u/o;

    invoke-virtual {v0, v1}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_1

    :cond_1
    sget-object v3, Lf/a/h/u/a$b;->i:Lf/a/h/u/a$b;

    if-ne v2, v3, :cond_5

    iget-object v2, v0, Lf/a/h/u/a;->g:Ljava/lang/String;

    iget-object v3, v0, Lf/a/h/u/a;->f:Ljava/lang/String;

    iget-object v4, v0, Lf/a/h/u/a;->s:Ljava/lang/String;

    iget-object v5, v0, Lf/a/h/u/a;->n:Ljava/lang/Long;

    const/4 v6, 0x0

    if-eqz v5, :cond_3

    iget-object v7, v0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    invoke-interface {v7}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sub-long/2addr v7, v9

    const-wide/32 v9, 0xea60

    cmp-long v5, v7, v9

    if-gtz v5, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    iget-boolean v5, v0, Lf/a/h/u/a;->h:Z

    if-eqz v5, :cond_4

    if-eqz v1, :cond_4

    iget-object v7, v0, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v8, v0, Lf/a/h/u/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[RESUME] resuming session. serverId="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " sessionId="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object v1, Lf/a/h/u/q;->d:Lf/a/h/u/q;

    invoke-virtual {v0, v1}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    sget-object v1, Lf/a/h/u/a$b;->g:Lf/a/h/u/a$b;

    iput-object v1, v0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    const/4 v1, 0x7

    new-instance v5, Lcom/discord/rtcconnection/socket/io/Payloads$Resume;

    invoke-direct {v5, v4, v2, v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Resume;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v5}, Lf/a/h/u/a;->m(ILjava/lang/Object;)V

    goto :goto_3

    :cond_4
    const-string v2, "Cannot resume connection. resumable: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, v0, Lf/a/h/u/a;->h:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, " -- isHeartbeatRecentEnough: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x12c1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v6, v2, v1}, Lf/a/h/u/a;->g(ZLjava/lang/Integer;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    :goto_1
    iget-object v0, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    sget-object v1, Lf/a/h/u/a$b;->h:Lf/a/h/u/a$b;

    iput-object v1, v0, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    iget-object v0, v0, Lf/a/h/u/a;->w:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    iget-object v2, v2, Lf/a/h/u/a;->j:Ljava/lang/Long;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_2

    :cond_6
    const-wide/16 v2, 0x0

    :goto_2
    sub-long/2addr v0, v2

    iget-object v2, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    iget-object v3, v2, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v4, v2, Lf/a/h/u/a;->a:Ljava/lang/String;

    const-string v2, "[CONNECTED] to "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    iget-object v5, v5, Lf/a/h/u/a;->r:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    iget-object v2, p0, Lf/a/h/u/a$m;->this$0:Lf/a/h/u/a;

    new-instance v3, Lf/a/h/u/p;

    invoke-direct {v3, v0, v1}, Lf/a/h/u/p;-><init>(J)V

    invoke-virtual {v2, v3}, Lf/a/h/u/a;->i(Lkotlin/jvm/functions/Function1;)V

    :goto_3
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
