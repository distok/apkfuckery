.class public final Lf/a/h/h;
.super Lx/m/c/k;
.source "RtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $endpoint:Ljava/lang/String;

.field public final synthetic $sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

.field public final synthetic $token:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/rtcconnection/RtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/h;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    iput-object p2, p0, Lf/a/h/h;->$endpoint:Ljava/lang/String;

    iput-object p3, p0, Lf/a/h/h;->$token:Ljava/lang/String;

    iput-object p4, p0, Lf/a/h/h;->$sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lf/a/h/h;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->E:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->o:Ljava/lang/Long;

    iget-object v0, p0, Lf/a/h/h;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    iget v1, v0, Lcom/discord/rtcconnection/RtcConnection;->q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/discord/rtcconnection/RtcConnection;->q:I

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->C:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    invoke-interface {v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->a()Lrx/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Lrx/Observable;->X(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/h/e;

    invoke-direct {v2, p0}, Lf/a/h/e;-><init>(Lf/a/h/h;)V

    new-instance v3, Lf/a/h/g;

    invoke-direct {v3, p0}, Lf/a/h/g;-><init>(Lf/a/h/h;)V

    invoke-virtual {v1, v2, v3}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->r:Lrx/Subscription;

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
