.class public final Lf/a/h/m;
.super Lx/m/c/k;
.source "RtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/rtcconnection/RtcConnection$b;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isSpeaking:Z

.field public final synthetic $userId:J


# direct methods
.method public constructor <init>(JZ)V
    .locals 0

    iput-wide p1, p0, Lf/a/h/m;->$userId:J

    iput-boolean p3, p0, Lf/a/h/m;->$isSpeaking:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lcom/discord/rtcconnection/RtcConnection$b;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lf/a/h/m;->$userId:J

    iget-boolean v2, p0, Lf/a/h/m;->$isSpeaking:Z

    invoke-interface {p1, v0, v1, v2}, Lcom/discord/rtcconnection/RtcConnection$b;->onSpeaking(JZ)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
