.class public final Lf/a/h/d;
.super Lx/m/c/k;
.source "RtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lf/a/h/e;


# direct methods
.method public constructor <init>(Lf/a/h/e;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/d;->this$0:Lf/a/h/e;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 14

    iget-object v0, p0, Lf/a/h/d;->this$0:Lf/a/h/e;

    iget-object v0, v0, Lf/a/h/e;->d:Lf/a/h/h;

    iget-object v1, v0, Lf/a/h/h;->this$0:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v2, v0, Lf/a/h/h;->$endpoint:Ljava/lang/String;

    iget-object v5, v0, Lf/a/h/h;->$token:Ljava/lang/String;

    iget-object v6, v0, Lf/a/h/h;->$sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v0, v1, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v0}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    if-eqz v6, :cond_0

    const-string/jumbo v0, "wss"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "ws"

    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v7, 0x1

    :goto_2
    if-eqz v7, :cond_4

    iget-object v0, v1, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v0, :cond_3

    iget-object v2, v0, Lf/a/h/u/a;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    sget-object v2, Lf/a/h/u/b;->d:Lf/a/h/u/b;

    invoke-virtual {v0, v2}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    iput-boolean v3, v0, Lf/a/h/u/a;->q:Z

    :cond_3
    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$State$b;->a:Lcom/discord/rtcconnection/RtcConnection$State$b;

    invoke-virtual {v1, v0}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    goto/16 :goto_5

    :cond_4
    const/4 v7, 0x4

    const-string v8, ".gg"

    const-string v9, ".media"

    invoke-static {v2, v8, v9, v4, v7}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v2

    const-string v8, "://"

    invoke-static {v0, v8, v2}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v8, ":80"

    const-string v9, ":443"

    invoke-static {v0, v8, v9, v4, v7}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v4

    iget-object v7, v1, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    const/4 v8, 0x0

    if-eqz v7, :cond_5

    iget-object v7, v7, Lf/a/h/u/a;->r:Ljava/lang/String;

    goto :goto_3

    :cond_5
    move-object v7, v8

    :goto_3
    invoke-static {v7, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, v1, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v7, :cond_6

    iget-object v8, v7, Lf/a/h/u/a;->s:Ljava/lang/String;

    :cond_6
    invoke-static {v8, v5}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, v1, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    instance-of v7, v7, Lcom/discord/rtcconnection/RtcConnection$State$d;

    if-nez v7, :cond_7

    iget-object v8, v1, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object v9, v1, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    const-string v10, "Already connecting, no need to continue."

    invoke-static/range {v8 .. v13}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto/16 :goto_5

    :cond_7
    iget-object v7, v1, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v7, :cond_8

    iget-object v8, v7, Lf/a/h/u/a;->p:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    sget-object v8, Lf/a/h/u/b;->d:Lf/a/h/u/b;

    invoke-virtual {v7, v8}, Lf/a/h/u/a;->b(Lkotlin/jvm/functions/Function1;)V

    iput-boolean v3, v7, Lf/a/h/u/a;->q:Z

    :cond_8
    iget-boolean v3, v1, Lcom/discord/rtcconnection/RtcConnection;->h:Z

    if-eqz v3, :cond_9

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x4

    const-string v3, "Connect called on destroyed instance."

    invoke-static/range {v1 .. v6}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;ZLjava/lang/String;Ljava/lang/Throwable;ZI)V

    goto/16 :goto_5

    :cond_9
    if-nez v5, :cond_a

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const-string v3, "Connect called with no token."

    invoke-static/range {v1 .. v6}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;ZLjava/lang/String;Ljava/lang/Throwable;ZI)V

    goto :goto_5

    :cond_a
    iget-object v3, v1, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connecting via endpoint: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " token: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {v3, v7, v8}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v3, Ljava/net/URI;

    invoke-direct {v3, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/discord/rtcconnection/RtcConnection;->l:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/net/URI;->getPort()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/discord/rtcconnection/RtcConnection;->m:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    iget-object v3, v1, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    new-instance v7, Lkotlin/Pair;

    const-string v8, "endpoint"

    invoke-direct {v7, v8, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v7}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    const-string v7, "Failed to parse RTC endpoint"

    invoke-virtual {v3, v7, v0, v2}, Lcom/discord/utilities/logging/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    :goto_4
    new-instance v0, Lf/a/h/u/a;

    iget-object v7, v1, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object v2, v1, Lcom/discord/rtcconnection/RtcConnection;->C:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    invoke-interface {v2}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object v8

    iget-object v9, v1, Lcom/discord/rtcconnection/RtcConnection;->E:Lcom/discord/utilities/time/Clock;

    move-object v3, v0

    invoke-direct/range {v3 .. v9}, Lf/a/h/u/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;Lcom/discord/utilities/logging/Logger;Ljava/util/concurrent/ExecutorService;Lcom/discord/utilities/time/Clock;)V

    iget-object v2, v1, Lcom/discord/rtcconnection/RtcConnection;->u:Lf/a/h/r;

    const-string v3, "listener"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lf/a/h/u/a;->p:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lf/a/h/u/a;->e()Z

    iput-object v0, v1, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    :goto_5
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
