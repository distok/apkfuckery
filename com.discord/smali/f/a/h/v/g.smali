.class public final Lf/a/h/v/g;
.super Ljava/lang/Object;
.source "RtcStatsCollector.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/a/h/v/h;


# direct methods
.method public constructor <init>(Lf/a/h/v/h;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/v/g;->d:Lf/a/h/v/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 6

    move-object v2, p1

    check-cast v2, Ljava/lang/Throwable;

    iget-object p1, p0, Lf/a/h/v/g;->d:Lf/a/h/v/h;

    iget-object v0, p1, Lf/a/h/v/h;->d:Lcom/discord/utilities/logging/Logger;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "RtcStatsCollector: Error collecting stats"

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    return-void
.end method
