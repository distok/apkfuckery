.class public final Lf/a/h/v/h;
.super Ljava/lang/Object;
.source "RtcStatsCollector.kt"


# instance fields
.field public a:Lco/discord/media_engine/Stats;

.field public b:Lrx/Subscription;

.field public final c:J

.field public final d:Lcom/discord/utilities/logging/Logger;

.field public final e:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

.field public final f:Lco/discord/media_engine/VoiceQuality;

.field public final g:Lf/a/h/v/j;

.field public final h:Lcom/discord/rtcconnection/KrispOveruseDetector;


# direct methods
.method public constructor <init>(JLcom/discord/utilities/logging/Logger;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lco/discord/media_engine/VoiceQuality;Lf/a/h/v/j;Lcom/discord/rtcconnection/KrispOveruseDetector;)V
    .locals 1

    const-string v0, "logger"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connection"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voiceQuality"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "videoQuality"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "krispOveruseDetector"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/h/v/h;->c:J

    iput-object p3, p0, Lf/a/h/v/h;->d:Lcom/discord/utilities/logging/Logger;

    iput-object p4, p0, Lf/a/h/v/h;->e:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    iput-object p5, p0, Lf/a/h/v/h;->f:Lco/discord/media_engine/VoiceQuality;

    iput-object p6, p0, Lf/a/h/v/h;->g:Lf/a/h/v/j;

    iput-object p7, p0, Lf/a/h/v/h;->h:Lcom/discord/rtcconnection/KrispOveruseDetector;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lf/a/h/v/h;->b:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    return-void
.end method
