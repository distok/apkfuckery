.class public final Lf/a/h/v/i;
.super Lx/m/c/k;
.source "VideoQuality.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lf/a/h/v/i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/h/v/i;

    invoke-direct {v0}, Lf/a/h/v/i;-><init>()V

    sput-object v0, Lf/a/h/v/i;->d:Lf/a/h/v/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(JJ)J
    .locals 1

    cmp-long v0, p3, p1

    if-lez v0, :cond_0

    return-wide p1

    :cond_0
    sub-long/2addr p1, p3

    return-wide p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->longValue()J

    move-result-wide p1

    invoke-virtual {p0, v0, v1, p1, p2}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method
