.class public final Lf/a/h/v/d;
.super Ljava/lang/Object;
.source "VideoQuality.kt"


# instance fields
.field public a:J

.field public b:J

.field public final c:Lf/a/h/v/a;


# direct methods
.method public constructor <init>()V
    .locals 24

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    new-instance v23, Lf/a/h/v/a;

    move-object/from16 v5, v23

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    const/16 v22, 0xff

    invoke-direct/range {v5 .. v22}, Lf/a/h/v/a;-><init>(JJJJJJJJI)V

    move-object/from16 v0, p0

    move-object/from16 v5, v23

    invoke-direct/range {v0 .. v5}, Lf/a/h/v/d;-><init>(JJLf/a/h/v/a;)V

    return-void
.end method

.method public constructor <init>(JJLf/a/h/v/a;)V
    .locals 1

    const-string v0, "aggregatedProperties"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/h/v/d;->a:J

    iput-wide p3, p0, Lf/a/h/v/d;->b:J

    iput-object p5, p0, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lf/a/h/v/d;

    if-eqz v0, :cond_0

    check-cast p1, Lf/a/h/v/d;

    iget-wide v0, p0, Lf/a/h/v/d;->a:J

    iget-wide v2, p1, Lf/a/h/v/d;->a:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lf/a/h/v/d;->b:J

    iget-wide v2, p1, Lf/a/h/v/d;->b:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    iget-object p1, p1, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 7

    iget-wide v0, p0, Lf/a/h/v/d;->a:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lf/a/h/v/d;->b:J

    ushr-long v5, v3, v2

    xor-long v2, v3, v5

    long-to-int v0, v2

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/h/v/a;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "RawVideoStats(resolution="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lf/a/h/v/d;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lf/a/h/v/d;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", aggregatedProperties="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
