.class public final synthetic Lf/a/h/v/e;
.super Lx/m/c/i;
.source "RtcStatsCollector.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lco/discord/media_engine/Stats;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lf/a/h/v/h;)V
    .locals 7

    const-class v3, Lf/a/h/v/h;

    const/4 v1, 0x1

    const-string v4, "onStatsReceived"

    const-string v5, "onStatsReceived(Lco/discord/media_engine/Stats;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 34

    move-object/from16 v0, p1

    check-cast v0, Lco/discord/media_engine/Stats;

    const-string v1, "p1"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v1, p0

    iget-object v2, v1, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v2, Lf/a/h/v/h;

    iput-object v0, v2, Lf/a/h/v/h;->a:Lco/discord/media_engine/Stats;

    iget-object v3, v2, Lf/a/h/v/h;->f:Lco/discord/media_engine/VoiceQuality;

    invoke-virtual {v3, v0}, Lco/discord/media_engine/VoiceQuality;->update(Lco/discord/media_engine/Stats;)V

    iget-object v3, v2, Lf/a/h/v/h;->g:Lf/a/h/v/j;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v4, "stats"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v3, Lf/a/h/v/j;->h:Lcom/discord/utilities/time/Clock;

    invoke-interface {v5}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getTransport()Lco/discord/media_engine/Transport;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lco/discord/media_engine/Transport;->getReceiverReports()[Lco/discord/media_engine/ReceiverReport;

    move-result-object v5

    if-eqz v5, :cond_0

    array-length v5, v5

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    iget-object v6, v3, Lf/a/h/v/j;->c:Lf/a/h/v/b;

    if-nez v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v6, v5, v12, v13}, Lf/a/h/v/b;->b(ZJ)V

    :cond_2
    iget-object v5, v3, Lf/a/h/v/j;->b:Lf/a/h/v/b;

    invoke-virtual {v5}, Lf/a/h/v/b;->a()Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, v3, Lf/a/h/v/j;->c:Lf/a/h/v/b;

    invoke-virtual {v5}, Lf/a/h/v/b;->a()Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v5, 0x1

    :goto_3
    iget-object v6, v3, Lf/a/h/v/j;->d:Lf/a/h/v/b;

    invoke-virtual {v6}, Lf/a/h/v/b;->a()Z

    move-result v6

    if-eq v5, v6, :cond_5

    iget-object v6, v3, Lf/a/h/v/j;->d:Lf/a/h/v/b;

    invoke-virtual {v6, v5, v12, v13}, Lf/a/h/v/b;->b(ZJ)V

    iget-object v5, v3, Lf/a/h/v/j;->e:Lf/a/h/v/c;

    iget-object v5, v5, Lf/a/h/v/c;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    :cond_5
    iget-object v5, v3, Lf/a/h/v/j;->d:Lf/a/h/v/b;

    invoke-virtual {v5}, Lf/a/h/v/b;->a()Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getOutboundRtpVideo()Lco/discord/media_engine/OutboundRtpVideo;

    move-result-object v5

    if-eqz v5, :cond_7

    iget-object v11, v3, Lf/a/h/v/j;->e:Lf/a/h/v/c;

    new-instance v9, Lf/a/h/v/d;

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getResolution()Lco/discord/media_engine/Resolution;

    move-result-object v6

    invoke-virtual {v6}, Lco/discord/media_engine/Resolution;->getHeight()I

    move-result v6

    int-to-long v7, v6

    new-instance v33, Lf/a/h/v/a;

    move-object/from16 v16, v33

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getFramesEncoded()I

    move-result v6

    int-to-long v14, v6

    move-wide/from16 v17, v14

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getFramesSent()I

    move-result v6

    int-to-long v14, v6

    move-wide/from16 v19, v14

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getPacketsSent()I

    move-result v6

    int-to-long v14, v6

    move-wide/from16 v21, v14

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getPacketsLost()I

    move-result v6

    int-to-long v14, v6

    move-wide/from16 v23, v14

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getBytesSent()J

    move-result-wide v27

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getNackCount()I

    move-result v6

    int-to-long v14, v6

    move-wide/from16 v29, v14

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getPliCount()I

    move-result v6

    int-to-long v14, v6

    move-wide/from16 v31, v14

    const-wide/16 v25, 0x0

    invoke-direct/range {v16 .. v32}, Lf/a/h/v/a;-><init>(JJJJJJJJ)V

    move-object v6, v9

    move-object v14, v9

    move-wide v9, v12

    move-object v15, v11

    move-object/from16 v11, v33

    invoke-direct/range {v6 .. v11}, Lf/a/h/v/d;-><init>(JJLf/a/h/v/a;)V

    invoke-virtual {v3, v15, v14}, Lf/a/h/v/j;->a(Lf/a/h/v/c;Lf/a/h/v/d;)V

    iget-object v6, v3, Lf/a/h/v/j;->e:Lf/a/h/v/c;

    iget-object v6, v6, Lf/a/h/v/c;->b:Ljava/lang/Long;

    if-nez v6, :cond_6

    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getFramesEncoded()I

    move-result v6

    if-lez v6, :cond_6

    iget-object v6, v3, Lf/a/h/v/j;->e:Lf/a/h/v/c;

    iget-wide v7, v3, Lf/a/h/v/j;->a:J

    sub-long v7, v12, v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v6, Lf/a/h/v/c;->b:Ljava/lang/Long;

    :cond_6
    invoke-virtual {v5}, Lco/discord/media_engine/OutboundRtpVideo;->getBitrateTarget()I

    move-result v5

    iput v5, v3, Lf/a/h/v/j;->g:I

    :cond_7
    iget-object v5, v3, Lf/a/h/v/j;->b:Lf/a/h/v/b;

    invoke-virtual {v5}, Lf/a/h/v/b;->a()Z

    move-result v5

    if-nez v5, :cond_a

    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getInboundRtpVideo()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    move-object v14, v6

    check-cast v14, Lco/discord/media_engine/InboundRtpVideo;

    iget-object v6, v3, Lf/a/h/v/j;->f:Ljava/util/Map;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_8

    new-instance v8, Lf/a/h/v/c;

    invoke-direct {v8}, Lf/a/h/v/c;-><init>()V

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    move-object v15, v8

    check-cast v15, Lf/a/h/v/c;

    new-instance v11, Lf/a/h/v/d;

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getResolution()Lco/discord/media_engine/Resolution;

    move-result-object v6

    invoke-virtual {v6}, Lco/discord/media_engine/Resolution;->getHeight()I

    move-result v6

    int-to-long v7, v6

    new-instance v33, Lf/a/h/v/a;

    move-object/from16 v16, v33

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getFramesDecoded()I

    move-result v6

    int-to-long v9, v6

    move-wide/from16 v17, v9

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getFramesReceived()I

    move-result v6

    int-to-long v9, v6

    move-wide/from16 v19, v9

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getPacketsReceived()I

    move-result v6

    int-to-long v9, v6

    move-wide/from16 v21, v9

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getPacketsLost()I

    move-result v6

    int-to-long v9, v6

    move-wide/from16 v23, v9

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getFramesDropped()I

    move-result v6

    int-to-long v9, v6

    move-wide/from16 v25, v9

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getBytesReceived()J

    move-result-wide v27

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getNackCount()I

    move-result v6

    int-to-long v9, v6

    move-wide/from16 v29, v9

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getPliCount()I

    move-result v6

    int-to-long v9, v6

    move-wide/from16 v31, v9

    invoke-direct/range {v16 .. v32}, Lf/a/h/v/a;-><init>(JJJJJJJJ)V

    move-object v6, v11

    move-wide v9, v12

    move-object v1, v11

    move-object/from16 v11, v33

    invoke-direct/range {v6 .. v11}, Lf/a/h/v/d;-><init>(JJLf/a/h/v/a;)V

    invoke-virtual {v3, v15, v1}, Lf/a/h/v/j;->a(Lf/a/h/v/c;Lf/a/h/v/d;)V

    iget-object v1, v15, Lf/a/h/v/c;->b:Ljava/lang/Long;

    if-nez v1, :cond_9

    invoke-virtual {v14}, Lco/discord/media_engine/InboundRtpVideo;->getFramesDecoded()I

    move-result v1

    if-lez v1, :cond_9

    iget-wide v6, v3, Lf/a/h/v/j;->a:J

    sub-long v6, v12, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v15, Lf/a/h/v/c;->b:Ljava/lang/Long;

    :cond_9
    move-object/from16 v1, p0

    goto/16 :goto_4

    :cond_a
    iget-object v1, v2, Lf/a/h/v/h;->h:Lcom/discord/rtcconnection/KrispOveruseDetector;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->d:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    invoke-interface {v2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->getType()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    move-result-object v2

    sget-object v3, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;->DEFAULT:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    if-eq v2, v3, :cond_b

    goto/16 :goto_6

    :cond_b
    iget-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->d:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    invoke-interface {v2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->b()Z

    move-result v2

    if-nez v2, :cond_c

    goto/16 :goto_6

    :cond_c
    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object v2

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Lco/discord/media_engine/OutboundRtpAudio;->getNoiseCancellerIsEnabled()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_10

    iget-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->b:Lco/discord/media_engine/OutboundRtpAudio;

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object v3

    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/discord/rtcconnection/KrispOveruseDetector;->a(Lco/discord/media_engine/OutboundRtpAudio;Lco/discord/media_engine/OutboundRtpAudio;D)Lkotlin/Pair;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    if-eqz v3, :cond_d

    iget-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->d:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    sget-object v3, Lcom/discord/rtcconnection/KrispOveruseDetector$Status;->CPU_OVERUSE:Lcom/discord/rtcconnection/KrispOveruseDetector$Status;

    invoke-interface {v2, v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->e(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V

    goto :goto_5

    :cond_d
    const-wide/16 v2, 0x0

    cmp-long v6, v4, v2

    if-nez v6, :cond_e

    iget v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->c:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    iput v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->c:I

    const/4 v3, 0x2

    if-le v2, v3, :cond_f

    iget-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->d:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    sget-object v3, Lcom/discord/rtcconnection/KrispOveruseDetector$Status;->FAILED:Lcom/discord/rtcconnection/KrispOveruseDetector$Status;

    invoke-interface {v2, v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->e(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V

    goto :goto_5

    :cond_e
    const/4 v2, 0x0

    iput v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->c:I

    :cond_f
    :goto_5
    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object v2

    iput-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->b:Lco/discord/media_engine/OutboundRtpAudio;

    :cond_10
    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-virtual {v2}, Lco/discord/media_engine/OutboundRtpAudio;->getVoiceActivityDetectorIsEnabled()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_12

    iget-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->a:Lco/discord/media_engine/OutboundRtpAudio;

    if-eqz v2, :cond_11

    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object v3

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/discord/rtcconnection/KrispOveruseDetector;->a(Lco/discord/media_engine/OutboundRtpAudio;Lco/discord/media_engine/OutboundRtpAudio;D)Lkotlin/Pair;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->d:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    sget-object v3, Lcom/discord/rtcconnection/KrispOveruseDetector$Status;->VAD_CPU_OVERUSE:Lcom/discord/rtcconnection/KrispOveruseDetector$Status;

    invoke-interface {v2, v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->e(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V

    :cond_11
    invoke-virtual {v0}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object v0

    iput-object v0, v1, Lcom/discord/rtcconnection/KrispOveruseDetector;->a:Lco/discord/media_engine/OutboundRtpAudio;

    :cond_12
    :goto_6
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
