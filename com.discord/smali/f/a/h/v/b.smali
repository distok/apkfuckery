.class public final Lf/a/h/v/b;
.super Ljava/lang/Object;
.source "VideoQuality.kt"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:J

.field public final c:J


# direct methods
.method public constructor <init>(ZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lf/a/h/v/b;->c:J

    if-eqz p1, :cond_0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lf/a/h/v/b;->a:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lf/a/h/v/b;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b(ZJ)V
    .locals 4

    iget-object v0, p0, Lf/a/h/v/b;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lf/a/h/v/b;->a:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    if-nez p1, :cond_1

    iget-wide v2, p0, Lf/a/h/v/b;->b:J

    sub-long/2addr p2, v0

    add-long/2addr p2, v2

    iput-wide p2, p0, Lf/a/h/v/b;->b:J

    const/4 p1, 0x0

    iput-object p1, p0, Lf/a/h/v/b;->a:Ljava/lang/Long;

    :cond_1
    :goto_0
    return-void
.end method

.method public final c(J)J
    .locals 4

    iget-object v0, p0, Lf/a/h/v/b;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    iget-wide v2, p0, Lf/a/h/v/b;->b:J

    add-long/2addr v2, p1

    sub-long/2addr v2, v0

    goto :goto_0

    :cond_0
    iget-wide v2, p0, Lf/a/h/v/b;->b:J

    :goto_0
    return-wide v2
.end method
