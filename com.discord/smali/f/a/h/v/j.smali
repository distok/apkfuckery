.class public final Lf/a/h/v/j;
.super Ljava/lang/Object;
.source "VideoQuality.kt"


# instance fields
.field public final a:J

.field public final b:Lf/a/h/v/b;

.field public final c:Lf/a/h/v/b;

.field public final d:Lf/a/h/v/b;

.field public final e:Lf/a/h/v/c;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lf/a/h/v/c;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public final h:Lcom/discord/utilities/time/Clock;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/time/Clock;I)V
    .locals 2

    and-int/lit8 p1, p2, 0x1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string p2, "clock"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/h/v/j;->h:Lcom/discord/utilities/time/Clock;

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lf/a/h/v/j;->a:J

    new-instance v0, Lf/a/h/v/b;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, p2}, Lf/a/h/v/b;-><init>(ZJ)V

    iput-object v0, p0, Lf/a/h/v/j;->b:Lf/a/h/v/b;

    new-instance v0, Lf/a/h/v/b;

    invoke-direct {v0, v1, p1, p2}, Lf/a/h/v/b;-><init>(ZJ)V

    iput-object v0, p0, Lf/a/h/v/j;->c:Lf/a/h/v/b;

    new-instance v0, Lf/a/h/v/b;

    invoke-direct {v0, v1, p1, p2}, Lf/a/h/v/b;-><init>(ZJ)V

    iput-object v0, p0, Lf/a/h/v/j;->d:Lf/a/h/v/b;

    new-instance p1, Lf/a/h/v/c;

    invoke-direct {p1}, Lf/a/h/v/c;-><init>()V

    iput-object p1, p0, Lf/a/h/v/j;->e:Lf/a/h/v/c;

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lf/a/h/v/j;->f:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Lf/a/h/v/c;Lf/a/h/v/d;)V
    .locals 24

    move-object/from16 v0, p1

    iget-object v1, v0, Lf/a/h/v/c;->a:Ljava/util/List;

    move-object/from16 v2, p2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lf/a/h/v/c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    return-void

    :cond_0
    iget-object v1, v0, Lf/a/h/v/c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/a/h/v/d;

    iget-object v3, v0, Lf/a/h/v/c;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/a/h/v/d;

    sget-object v3, Lf/a/h/v/i;->d:Lf/a/h/v/i;

    iget-wide v4, v0, Lf/a/h/v/c;->d:J

    iget-wide v6, v1, Lf/a/h/v/d;->b:J

    iget-wide v8, v2, Lf/a/h/v/d;->b:J

    sub-long/2addr v6, v8

    add-long/2addr v6, v4

    iput-wide v6, v0, Lf/a/h/v/c;->d:J

    iget-object v4, v0, Lf/a/h/v/c;->c:Lf/a/h/v/a;

    iget-object v5, v1, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    iget-object v6, v2, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    iget-wide v7, v4, Lf/a/h/v/a;->a:J

    iget-wide v9, v5, Lf/a/h/v/a;->a:J

    iget-wide v11, v6, Lf/a/h/v/a;->a:J

    invoke-virtual {v3, v9, v10, v11, v12}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v9

    add-long/2addr v9, v7

    iput-wide v9, v4, Lf/a/h/v/a;->a:J

    iget-wide v7, v4, Lf/a/h/v/a;->b:J

    iget-wide v9, v5, Lf/a/h/v/a;->b:J

    iget-wide v11, v6, Lf/a/h/v/a;->b:J

    invoke-virtual {v3, v9, v10, v11, v12}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v9

    add-long/2addr v9, v7

    iput-wide v9, v4, Lf/a/h/v/a;->b:J

    iget-wide v7, v4, Lf/a/h/v/a;->c:J

    iget-wide v9, v5, Lf/a/h/v/a;->c:J

    iget-wide v11, v6, Lf/a/h/v/a;->c:J

    invoke-virtual {v3, v9, v10, v11, v12}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v9

    add-long/2addr v9, v7

    iput-wide v9, v4, Lf/a/h/v/a;->c:J

    iget-wide v7, v4, Lf/a/h/v/a;->d:J

    iget-wide v9, v5, Lf/a/h/v/a;->d:J

    iget-wide v11, v6, Lf/a/h/v/a;->d:J

    invoke-virtual {v3, v9, v10, v11, v12}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v9

    add-long/2addr v9, v7

    iput-wide v9, v4, Lf/a/h/v/a;->d:J

    iget-wide v7, v4, Lf/a/h/v/a;->e:J

    iget-wide v9, v5, Lf/a/h/v/a;->e:J

    iget-wide v11, v6, Lf/a/h/v/a;->e:J

    invoke-virtual {v3, v9, v10, v11, v12}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v9

    add-long/2addr v9, v7

    iput-wide v9, v4, Lf/a/h/v/a;->e:J

    iget-wide v7, v4, Lf/a/h/v/a;->f:J

    iget-wide v9, v5, Lf/a/h/v/a;->f:J

    iget-wide v11, v6, Lf/a/h/v/a;->f:J

    invoke-virtual {v3, v9, v10, v11, v12}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v9

    add-long/2addr v9, v7

    iput-wide v9, v4, Lf/a/h/v/a;->f:J

    iget-wide v7, v4, Lf/a/h/v/a;->g:J

    iget-wide v9, v5, Lf/a/h/v/a;->g:J

    iget-wide v11, v6, Lf/a/h/v/a;->g:J

    invoke-virtual {v3, v9, v10, v11, v12}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v9

    add-long/2addr v9, v7

    iput-wide v9, v4, Lf/a/h/v/a;->g:J

    iget-wide v7, v4, Lf/a/h/v/a;->h:J

    iget-wide v9, v5, Lf/a/h/v/a;->h:J

    iget-wide v5, v6, Lf/a/h/v/a;->h:J

    invoke-virtual {v3, v9, v10, v5, v6}, Lf/a/h/v/i;->a(JJ)J

    move-result-wide v5

    add-long/2addr v5, v7

    iput-wide v5, v4, Lf/a/h/v/a;->h:J

    iget-object v3, v1, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    iget-wide v4, v3, Lf/a/h/v/a;->f:J

    iget-wide v6, v3, Lf/a/h/v/a;->a:J

    iget-wide v8, v1, Lf/a/h/v/d;->b:J

    iget-wide v10, v1, Lf/a/h/v/d;->a:J

    iget-wide v1, v2, Lf/a/h/v/d;->b:J

    sub-long v1, v8, v1

    long-to-float v1, v1

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    iget v2, v0, Lf/a/h/v/c;->g:F

    long-to-float v3, v10

    mul-float v3, v3, v1

    add-float/2addr v3, v2

    iput v3, v0, Lf/a/h/v/c;->g:F

    iget-object v2, v0, Lf/a/h/v/c;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x6

    if-ge v2, v3, :cond_1

    return-void

    :cond_1
    iget-object v2, v0, Lf/a/h/v/c;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/a/h/v/d;

    iget-object v3, v2, Lf/a/h/v/d;->c:Lf/a/h/v/a;

    iget-wide v12, v3, Lf/a/h/v/a;->f:J

    iget-wide v14, v3, Lf/a/h/v/a;->a:J

    iget-wide v2, v2, Lf/a/h/v/d;->b:J

    sget-object v16, Lf/a/h/v/k;->a:[Ljava/lang/Integer;

    move-wide/from16 v17, v6

    sget-object v6, Lf/a/h/v/k;->c:[Ljava/lang/Integer;

    array-length v7, v6

    const/16 v16, 0x0

    move-wide/from16 v19, v14

    const/4 v14, 0x0

    :goto_0
    if-ge v14, v7, :cond_4

    aget-object v16, v6, v14

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Number;->intValue()I

    move-result v15

    move-object/from16 v16, v6

    move/from16 v21, v7

    int-to-long v6, v15

    cmp-long v22, v10, v6

    if-gtz v22, :cond_3

    iget-object v6, v0, Lf/a/h/v/c;->h:Ljava/util/Map;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-wide/from16 v22, v10

    iget-object v10, v0, Lf/a/h/v/c;->h:Ljava/util/Map;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    if-eqz v10, :cond_2

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v15

    goto :goto_1

    :cond_2
    const/4 v15, 0x0

    :goto_1
    add-float/2addr v15, v1

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-interface {v6, v7, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    move-wide/from16 v22, v10

    :goto_2
    add-int/lit8 v14, v14, 0x1

    move-object/from16 v6, v16

    move/from16 v7, v21

    move-wide/from16 v10, v22

    goto :goto_0

    :cond_4
    sub-long/2addr v8, v2

    long-to-float v2, v8

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    sub-long/2addr v4, v12

    const/16 v3, 0x8

    int-to-long v6, v3

    mul-long v4, v4, v6

    long-to-float v3, v4

    div-float/2addr v3, v2

    sub-long v6, v17, v19

    long-to-float v4, v6

    div-float/2addr v4, v2

    sget-object v2, Lf/a/h/v/k;->a:[Ljava/lang/Integer;

    sget-object v2, Lf/a/h/v/k;->a:[Ljava/lang/Integer;

    array-length v5, v2

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v5, :cond_7

    aget-object v7, v2, v6

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    int-to-float v8, v7

    cmpg-float v8, v3, v8

    if-gtz v8, :cond_6

    iget-object v8, v0, Lf/a/h/v/c;->e:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v10, v0, Lf/a/h/v/c;->e:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v10, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    goto :goto_4

    :cond_5
    const/4 v7, 0x0

    :goto_4
    add-float/2addr v7, v1

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v8, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_7
    sget-object v2, Lf/a/h/v/k;->a:[Ljava/lang/Integer;

    sget-object v2, Lf/a/h/v/k;->b:[Ljava/lang/Integer;

    array-length v3, v2

    const/4 v5, 0x0

    :goto_5
    if-ge v5, v3, :cond_a

    aget-object v6, v2, v5

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    int-to-float v7, v6

    cmpg-float v7, v4, v7

    if-gtz v7, :cond_9

    iget-object v7, v0, Lf/a/h/v/c;->f:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iget-object v9, v0, Lf/a/h/v/c;->f:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    goto :goto_6

    :cond_8
    const/4 v6, 0x0

    :goto_6
    add-float/2addr v6, v1

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_a
    iget-object v0, v0, Lf/a/h/v/c;->a:Ljava/util/List;

    const-string v1, "$this$removeFirst"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-void

    :cond_b
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "List is empty."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "userId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/v/j;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/a/h/v/c;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lf/a/h/v/j;->d(Lf/a/h/v/c;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final c()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/a/h/v/j;->e:Lf/a/h/v/c;

    iget-object v1, v0, Lf/a/h/v/c;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lf/a/h/v/j;->d(Lf/a/h/v/c;)Ljava/util/Map;

    move-result-object v0

    const/16 v1, 0x9

    new-array v1, v1, [Lkotlin/Pair;

    iget v2, p0, Lf/a/h/v/j;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string/jumbo v4, "target_bitrate"

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x0

    aput-object v3, v1, v2

    const/4 v3, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "duration_encoder_nvidia_cuda"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    const/4 v3, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "duration_encoder_nvidia_direct3d"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    const/4 v3, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "duration_encoder_openh264"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    const/4 v3, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "duration_encoder_videotoolbox"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    const/4 v3, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "duration_encoder_amd_direct3d"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    const/4 v3, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "duration_encoder_intel"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    const/4 v3, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "duration_encoder_intel_direct3d"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    const/16 v3, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v4, Lkotlin/Pair;

    const-string v5, "duration_encoder_unknown"

    invoke-direct {v4, v5, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v1, v3

    invoke-static {v1}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0, v1}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lf/a/h/v/c;)Ljava/util/Map;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/a/h/v/c;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lf/a/h/v/j;->h:Lcom/discord/utilities/time/Clock;

    invoke-interface {v2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, v0, Lf/a/h/v/j;->a:J

    sub-long v4, v2, v4

    long-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    iget-wide v6, v1, Lf/a/h/v/c;->d:J

    long-to-float v6, v6

    div-float/2addr v6, v5

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    int-to-float v9, v7

    cmpl-float v9, v6, v9

    if-lez v9, :cond_0

    iget v12, v1, Lf/a/h/v/c;->g:F

    div-float/2addr v12, v6

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v12}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v12

    goto :goto_0

    :cond_0
    const-wide/16 v12, 0x0

    :goto_0
    const/16 v14, 0x21

    new-array v14, v14, [Lkotlin/Pair;

    float-to-double v10, v4

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration"

    invoke-direct {v10, v11, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v7

    iget-object v4, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v10, 0x7a1200

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v4, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-static {v4}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_stream_under_8mbps"

    invoke-direct {v10, v11, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x1

    aput-object v10, v14, v4

    iget-object v10, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v11, 0x6acfc0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-static {v10}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v11, Lkotlin/Pair;

    const-string v15, "duration_stream_under_7mbps"

    invoke-direct {v11, v15, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v10, 0x2

    aput-object v11, v14, v10

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0x5b8d80

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v10, "duration_stream_under_6mbps"

    invoke-direct {v15, v10, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v10, 0x3

    aput-object v15, v14, v10

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0x4c4b40

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v17

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v10, "duration_stream_under_5mbps"

    invoke-direct {v15, v10, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v10, 0x4

    aput-object v15, v14, v10

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0x3d0900

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v10, "duration_stream_under_4mbps"

    invoke-direct {v15, v10, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v10, 0x5

    aput-object v15, v14, v10

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0x2dc6c0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v4, "duration_stream_under_3mbps"

    invoke-direct {v15, v4, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x6

    aput-object v15, v14, v4

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0x1e8480

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v4, "duration_stream_under_2mbps"

    invoke-direct {v15, v4, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x7

    aput-object v15, v14, v4

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0x16e360

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v4, "duration_stream_under_1_5mbps"

    invoke-direct {v15, v4, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v4, 0x8

    aput-object v15, v14, v4

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0xf4240

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v7, "duration_stream_under_1mbps"

    invoke-direct {v15, v7, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v7, 0x9

    aput-object v15, v14, v7

    iget-object v11, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    const v15, 0x7a120

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v7, "duration_stream_under_0_5mbps"

    invoke-direct {v15, v7, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v7, 0xa

    aput-object v15, v14, v7

    const/16 v11, 0xb

    iget-object v15, v1, Lf/a/h/v/c;->e:Ljava/util/Map;

    invoke-interface {v15, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Float;

    invoke-static {v15}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    new-instance v4, Lkotlin/Pair;

    const-string v5, "duration_stream_at_0mbps"

    invoke-direct {v4, v5, v15}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v4, v14, v11

    const/16 v4, 0xc

    iget-object v5, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v11, 0x3c

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v11, Lkotlin/Pair;

    const-string v15, "duration_fps_under_60"

    invoke-direct {v11, v15, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v14, v4

    const/16 v4, 0xd

    iget-object v5, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v11, 0x37

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v11, Lkotlin/Pair;

    const-string v15, "duration_fps_under_55"

    invoke-direct {v11, v15, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v14, v4

    const/16 v4, 0xe

    iget-object v5, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v11, 0x32

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v11, Lkotlin/Pair;

    const-string v15, "duration_fps_under_50"

    invoke-direct {v11, v15, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v14, v4

    iget-object v4, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v5, 0x2d

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-static {v4}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v11, "duration_fps_under_45"

    invoke-direct {v5, v11, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v4, 0xf

    aput-object v5, v14, v4

    const/16 v5, 0x10

    iget-object v11, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v15, 0x28

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-static {v11}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v15, Lkotlin/Pair;

    const-string v10, "duration_fps_under_40"

    invoke-direct {v15, v10, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v15, v14, v5

    const/16 v5, 0x11

    iget-object v10, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v11, 0x23

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-static {v10}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v11, Lkotlin/Pair;

    const-string v15, "duration_fps_under_35"

    invoke-direct {v11, v15, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v14, v5

    const/16 v5, 0x12

    iget-object v10, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v11, 0x1e

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v10, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-static {v10}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v15, Lkotlin/Pair;

    const-string v11, "duration_fps_under_30"

    invoke-direct {v15, v11, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v15, v14, v5

    const/16 v5, 0x13

    iget-object v10, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v11, 0x19

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v10, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-static {v10}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v15, Lkotlin/Pair;

    const-string v11, "duration_fps_under_25"

    invoke-direct {v15, v11, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v15, v14, v5

    iget-object v5, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/16 v10, 0x14

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v29

    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v11, Lkotlin/Pair;

    const-string v15, "duration_fps_under_20"

    invoke-direct {v11, v15, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v14, v10

    const/16 v5, 0x15

    iget-object v10, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v10, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-static {v4}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_fps_under_15"

    invoke-direct {v10, v11, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v5

    const/16 v4, 0x16

    iget-object v5, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_fps_under_10"

    invoke-direct {v10, v11, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v4

    const/16 v4, 0x17

    iget-object v5, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    const/4 v10, 0x5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_fps_under_5"

    invoke-direct {v10, v11, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v4

    const/16 v4, 0x18

    iget-object v5, v1, Lf/a/h/v/c;->f:Ljava/util/Map;

    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_fps_at_0"

    invoke-direct {v10, v11, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v4

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v10, "avg_resolution"

    invoke-direct {v5, v10, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v4, 0x19

    aput-object v5, v14, v4

    const/16 v4, 0x1a

    iget-object v5, v1, Lf/a/h/v/c;->h:Ljava/util/Map;

    const/16 v10, 0x2d0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_resolution_under_720"

    invoke-direct {v10, v11, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v4

    const/16 v4, 0x1b

    iget-object v5, v1, Lf/a/h/v/c;->h:Ljava/util/Map;

    const/16 v10, 0x1e0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_resolution_under_480"

    invoke-direct {v10, v11, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v4

    const/16 v4, 0x1c

    iget-object v5, v1, Lf/a/h/v/c;->h:Ljava/util/Map;

    const/16 v10, 0x168

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v10, Lkotlin/Pair;

    const-string v11, "duration_resolution_under_360"

    invoke-direct {v10, v11, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v14, v4

    const/16 v4, 0x1d

    new-instance v5, Lkotlin/Pair;

    const-string v10, "num_pauses"

    invoke-direct {v5, v10, v8}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v14, v4

    iget-object v4, v0, Lf/a/h/v/j;->b:Lf/a/h/v/b;

    invoke-virtual {v4, v2, v3}, Lf/a/h/v/b;->c(J)J

    move-result-wide v4

    long-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v4}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v8, "duration_paused"

    invoke-direct {v5, v8, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v4, 0x1e

    aput-object v5, v14, v4

    const/16 v4, 0x1f

    iget-object v5, v0, Lf/a/h/v/j;->c:Lf/a/h/v/b;

    invoke-virtual {v5, v2, v3}, Lf/a/h/v/b;->c(J)J

    move-result-wide v10

    long-to-float v5, v10

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v5, v8

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v5}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v8, Lkotlin/Pair;

    const-string v10, "duration_zero_receivers"

    invoke-direct {v8, v10, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v8, v14, v4

    const/16 v4, 0x20

    iget-object v5, v0, Lf/a/h/v/j;->d:Lf/a/h/v/b;

    invoke-virtual {v5, v2, v3}, Lf/a/h/v/b;->c(J)J

    move-result-wide v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v2}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    const-string v5, "duration_video_stopped"

    invoke-direct {v3, v5, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v14, v4

    invoke-static {v14}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, v1, Lf/a/h/v/c;->c:Lf/a/h/v/a;

    if-lez v9, :cond_1

    iget-wide v4, v3, Lf/a/h/v/a;->f:J

    const/16 v8, 0x8

    int-to-long v10, v8

    mul-long v4, v4, v10

    long-to-float v4, v4

    div-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v4}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v4

    goto :goto_1

    :cond_1
    const-wide/16 v4, 0x0

    :goto_1
    if-lez v9, :cond_2

    iget-wide v8, v3, Lf/a/h/v/a;->a:J

    long-to-float v8, v8

    div-float/2addr v8, v6

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-static {v6}, Lf/a/h/v/k;->a(Ljava/lang/Float;)J

    move-result-wide v8

    goto :goto_2

    :cond_2
    const-wide/16 v8, 0x0

    :goto_2
    new-array v6, v7, [Lkotlin/Pair;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v7, "avg_bitrate"

    invoke-direct {v5, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x0

    aput-object v5, v6, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v7, "avg_fps"

    invoke-direct {v5, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x1

    aput-object v5, v6, v4

    iget-wide v4, v3, Lf/a/h/v/a;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v7, "num_bytes"

    invoke-direct {v5, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x2

    aput-object v5, v6, v4

    iget-wide v4, v3, Lf/a/h/v/a;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v7, "num_packets_lost"

    invoke-direct {v5, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x3

    aput-object v5, v6, v4

    iget-wide v4, v3, Lf/a/h/v/a;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v7, "num_packets"

    invoke-direct {v5, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x4

    aput-object v5, v6, v4

    iget-wide v4, v3, Lf/a/h/v/a;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v7, "num_frames"

    invoke-direct {v5, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x5

    aput-object v5, v6, v4

    iget-object v1, v1, Lf/a/h/v/c;->b:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    goto :goto_3

    :cond_3
    const-wide/16 v10, 0x0

    :goto_3
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v4, Lkotlin/Pair;

    const-string/jumbo v5, "time_to_first_frame_ms"

    invoke-direct {v4, v5, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x6

    aput-object v4, v6, v1

    iget-wide v4, v3, Lf/a/h/v/a;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v4, Lkotlin/Pair;

    const-string v5, "num_frames_dropped"

    invoke-direct {v4, v5, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x7

    aput-object v4, v6, v1

    iget-wide v4, v3, Lf/a/h/v/a;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v4, Lkotlin/Pair;

    const-string v5, "num_nacks"

    invoke-direct {v4, v5, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0x8

    aput-object v4, v6, v1

    iget-wide v3, v3, Lf/a/h/v/a;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v3, Lkotlin/Pair;

    const-string v4, "num_plis"

    invoke-direct {v3, v4, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0x9

    aput-object v3, v6, v1

    invoke-static {v6}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v2, v1}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method
