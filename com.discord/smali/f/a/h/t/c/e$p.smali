.class public final Lf/a/h/t/c/e$p;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/e;->a(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lco/discord/media_engine/Connection;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $screenCapturer:Lf/a/h/t/b;


# direct methods
.method public constructor <init>(Lf/a/h/t/b;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/e$p;->$screenCapturer:Lf/a/h/t/b;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lco/discord/media_engine/Connection;

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/e$p;->$screenCapturer:Lf/a/h/t/b;

    iget-object v1, v0, Lf/a/h/t/b;->h:Lco/discord/media_engine/SoundshareAudioSource;

    invoke-virtual {v1}, Lco/discord/media_engine/SoundshareAudioSource;->getNativeInstance()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lco/discord/media_engine/Connection;->startScreenshareBroadcast(Lorg/webrtc/VideoCapturer;J)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
