.class public final Lf/a/h/t/c/c$a;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/c;->onUserSpeakingStatusChanged(JZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isUserSpeakingNow:Z

.field public final synthetic $userId:J

.field public final synthetic this$0:Lf/a/h/t/c/c;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/c;JZ)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/c$a;->this$0:Lf/a/h/t/c/c;

    iput-wide p2, p0, Lf/a/h/t/c/c$a;->$userId:J

    iput-boolean p4, p0, Lf/a/h/t/c/c$a;->$isUserSpeakingNow:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lf/a/h/t/c/c$a;->this$0:Lf/a/h/t/c/c;

    iget-object v0, v0, Lf/a/h/t/c/c;->a:Lf/a/h/t/c/e;

    iget-wide v1, p0, Lf/a/h/t/c/c$a;->$userId:J

    iget-boolean v3, p0, Lf/a/h/t/c/c$a;->$isUserSpeakingNow:Z

    iget-object v4, v0, Lf/a/h/t/c/e;->c:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_0
    const-string v5, "audioSsrcs[userId] ?: 0"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-instance v5, Lf/a/h/t/c/j;

    invoke-direct {v5, v1, v2, v4, v3}, Lf/a/h/t/c/j;-><init>(JIZ)V

    invoke-virtual {v0, v5}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
