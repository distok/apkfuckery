.class public final Lf/a/h/t/c/d$a;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/d;->onConnectToServer(Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $connectionInfo:Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;

.field public final synthetic $errorMessage:Ljava/lang/String;

.field public final synthetic this$0:Lf/a/h/t/c/d;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/d;Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/d$a;->this$0:Lf/a/h/t/c/d;

    iput-object p2, p0, Lf/a/h/t/c/d$a;->$connectionInfo:Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;

    iput-object p3, p0, Lf/a/h/t/c/d$a;->$errorMessage:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 10

    iget-object v0, p0, Lf/a/h/t/c/d$a;->this$0:Lf/a/h/t/c/d;

    iget-object v0, v0, Lf/a/h/t/c/d;->a:Lf/a/h/t/c/e;

    iget-object v1, p0, Lf/a/h/t/c/d$a;->$connectionInfo:Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;

    iget-object v2, p0, Lf/a/h/t/c/d$a;->$errorMessage:Ljava/lang/String;

    iget-object v3, v0, Lf/a/h/t/c/e;->h:Lcom/discord/utilities/logging/Logger;

    sget-object v4, Lf/a/h/t/c/e;->m:Ljava/lang/String;

    const-string v5, "TAG"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleConnection(). errorMessage: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    if-eqz v1, :cond_2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_2

    new-instance v2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;

    iget-object v3, v1, Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;->localAddress:Ljava/lang/String;

    const-string v4, "connectionInfo.localAddress"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget v4, v1, Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;->localPort:I

    iget-object v1, v1, Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;->protocol:Ljava/lang/String;

    const-string v5, "connectionInfo.protocol"

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v5, "(this as java.lang.String).toUpperCase()"

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo$Protocol;->valueOf(Ljava/lang/String;)Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo$Protocol;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;-><init>(Ljava/lang/String;ILcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo$Protocol;)V

    sget-object v1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;->CONNECTED:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

    iput-object v1, v0, Lf/a/h/t/c/e;->b:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

    new-instance v3, Lf/a/h/t/c/a;

    invoke-direct {v3, v0, v1}, Lf/a/h/t/c/a;-><init>(Lf/a/h/t/c/e;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;)V

    invoke-virtual {v0, v3}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    new-instance v1, Lf/a/h/t/a;

    const/4 v6, 0x1

    const/16 v8, 0x78

    const/4 v9, 0x0

    const-string v5, "opus"

    const-string v7, "audio"

    move-object v4, v1

    invoke-direct/range {v4 .. v9}, Lf/a/h/t/a;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/Integer;)V

    iget-object v3, v0, Lf/a/h/t/c/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iget-object v3, v0, Lf/a/h/t/c/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lf/a/h/t/c/e;->e:Ljava/util/ArrayList;

    iget-object v3, v0, Lf/a/h/t/c/e;->k:Ljava/util/List;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v1, Lf/a/h/t/c/g;

    invoke-direct {v1, v0, v2}, Lf/a/h/t/c/g;-><init>(Lf/a/h/t/c/e;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;)V

    invoke-virtual {v0, v1}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    goto :goto_2

    :cond_2
    if-nez v1, :cond_3

    new-instance v1, Lf/a/h/t/c/h;

    invoke-direct {v1, v0, v2}, Lf/a/h/t/c/h;-><init>(Lf/a/h/t/c/e;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    goto :goto_2

    :cond_3
    new-instance v1, Lf/a/h/t/c/i;

    invoke-direct {v1, v0, v2}, Lf/a/h/t/c/i;-><init>(Lf/a/h/t/c/e;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    :goto_2
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
