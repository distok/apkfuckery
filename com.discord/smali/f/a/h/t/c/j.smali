.class public final Lf/a/h/t/c/j;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $audioSsrc:I

.field public final synthetic $isSpeaking:Z

.field public final synthetic $userId:J


# direct methods
.method public constructor <init>(JIZ)V
    .locals 0

    iput-wide p1, p0, Lf/a/h/t/c/j;->$userId:J

    iput p3, p0, Lf/a/h/t/c/j;->$audioSsrc:I

    iput-boolean p4, p0, Lf/a/h/t/c/j;->$isSpeaking:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lf/a/h/t/c/j;->$userId:J

    iget v2, p0, Lf/a/h/t/c/j;->$audioSsrc:I

    iget-boolean v3, p0, Lf/a/h/t/c/j;->$isSpeaking:Z

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;->onSpeaking(JIZ)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
