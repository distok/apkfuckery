.class public final Lf/a/h/t/c/f;
.super Ljava/lang/Object;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lco/discord/media_engine/Connection$GetStatsCallback;


# instance fields
.field public final synthetic a:Lf/a/h/t/c/e$e;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/e$e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/a/h/t/c/f;->a:Lf/a/h/t/c/e$e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStats(Lco/discord/media_engine/Stats;)V
    .locals 1

    const-string/jumbo v0, "stats"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/f;->a:Lf/a/h/t/c/e$e;

    iget-object v0, v0, Lf/a/h/t/c/e$e;->$onStats:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onStatsError(Ljava/lang/Throwable;)V
    .locals 8

    iget-object v0, p0, Lf/a/h/t/c/f;->a:Lf/a/h/t/c/e$e;

    iget-object v0, v0, Lf/a/h/t/c/e$e;->this$0:Lf/a/h/t/c/e;

    iget-object v1, v0, Lf/a/h/t/c/e;->h:Lcom/discord/utilities/logging/Logger;

    sget-object v2, Lf/a/h/t/c/e;->m:Ljava/lang/String;

    const-string v0, "TAG"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "error collecting stats"

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v4, p1

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    return-void
.end method
