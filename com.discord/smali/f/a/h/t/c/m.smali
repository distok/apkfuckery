.class public final Lf/a/h/t/c/m;
.super Ljava/lang/Object;
.source "MediaEngineLegacy.kt"

# interfaces
.implements Lcom/discord/rtcconnection/mediaengine/MediaEngine;


# static fields
.field public static final k:Ljava/lang/String; = "m"


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/hammerandchisel/libdiscord/Discord;

.field public final c:Lf/a/h/t/c/u;

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$b;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/a/h/t/a;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/content/Context;

.field public final g:Ljava/util/concurrent/ExecutorService;

.field public final h:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

.field public final i:Lcom/discord/utilities/logging/Logger;

.field public final j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;Lcom/discord/utilities/logging/Logger;Ljava/util/HashSet;I)V
    .locals 6

    and-int/lit8 p5, p6, 0x10

    if-eqz p5, :cond_0

    const-string v0, "Pixel"

    const-string v1, "Pixel XL"

    const-string v2, "Pixel 3a"

    const-string v3, "Pixel 3a XL"

    const-string v4, "Pixel 4"

    const-string v5, "Pixel 4 XL"

    filled-new-array/range {v0 .. v5}, [Ljava/lang/String;

    move-result-object p5

    invoke-static {p5}, Lx/h/f;->hashSetOf([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object p5

    goto :goto_0

    :cond_0
    const/4 p5, 0x0

    :goto_0
    const-string p6, "context"

    invoke-static {p1, p6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p6, "singleThreadExecutorService"

    invoke-static {p2, p6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p6, "openSLESConfig"

    invoke-static {p3, p6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p6, "logger"

    invoke-static {p4, p6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p6, "defaultUseOpenSL"

    invoke-static {p5, p6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/h/t/c/m;->f:Landroid/content/Context;

    iput-object p2, p0, Lf/a/h/t/c/m;->g:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lf/a/h/t/c/m;->h:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

    iput-object p4, p0, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    iput-object p5, p0, Lf/a/h/t/c/m;->j:Ljava/util/HashSet;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lf/a/h/t/c/m;->a:Ljava/util/ArrayList;

    new-instance p1, Lf/a/h/t/c/u;

    invoke-direct {p1}, Lf/a/h/t/c/u;-><init>()V

    iput-object p1, p0, Lf/a/h/t/c/m;->c:Lf/a/h/t/c/u;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lf/a/h/t/c/m;->d:Ljava/util/ArrayList;

    return-void
.end method

.method public static final o(Lf/a/h/t/c/m;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/hammerandchisel/libdiscord/Discord;->setLocalVoiceLevelChangedCallback(Lcom/hammerandchisel/libdiscord/Discord$LocalVoiceLevelChangedCallback;)V

    :cond_0
    iget-object v0, p0, Lf/a/h/t/c/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static final p(Lf/a/h/t/c/m;Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;
    .locals 1

    iget-object p0, p0, Lf/a/h/t/c/m;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lf/a/h/t/c/t;

    invoke-direct {v0, p1}, Lf/a/h/t/c/t;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/a/h/t/c/m;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string v0, "Observable.just(Unit)"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1

    :cond_1
    invoke-virtual {p0}, Lf/a/h/t/c/m;->r()V

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to initialize native media engine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lrx/Observable;->u(Ljava/lang/Throwable;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.error(Illegal\u2026ze native media engine\"))"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    new-instance v0, Lf/a/h/t/c/q;

    invoke-direct {v0, p0}, Lf/a/h/t/c/q;-><init>(Lf/a/h/t/c/m;)V

    sget-object v1, Lrx/Emitter$BackpressureMode;->d:Lrx/Emitter$BackpressureMode;

    invoke-static {v0, v1}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.create({ emit\u2026er.BackpressureMode.NONE)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lf/a/h/t/c/m$d;

    invoke-direct {v1, p0}, Lf/a/h/t/c/m$d;-><init>(Lf/a/h/t/c/m;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->s(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/h/t/c/m$e;->d:Lf/a/h/t/c/m$e;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "getSupportedVideoCodecs(\u2026s }\n        .map { Unit }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b([Lco/discord/media_engine/RtcRegion;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lco/discord/media_engine/RtcRegion;",
            "Lkotlin/jvm/functions/Function1<",
            "-[",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "regionsWithIps"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/h/t/c/m;->r()V

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    new-instance v1, Lf/a/h/t/c/r;

    invoke-direct {v1, p2}, Lf/a/h/t/c/r;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, p1, v1}, Lcom/hammerandchisel/libdiscord/Discord;->getRankedRtcRegions([Lco/discord/media_engine/RtcRegion;Lcom/hammerandchisel/libdiscord/Discord$GetRankedRtcRegionsCallback;)V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/hammerandchisel/libdiscord/Discord;->setEchoCancellation(Z)V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/m;->c:Lf/a/h/t/c/u;

    iput-boolean p1, v0, Lf/a/h/t/c/u;->a:Z

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/hammerandchisel/libdiscord/Discord;->setNoiseCancellation(Z)V

    :cond_0
    return-void
.end method

.method public e()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/a/h/t/c/m$c;

    invoke-direct {v0, p0}, Lf/a/h/t/c/m$c;-><init>(Lf/a/h/t/c/m;)V

    sget-object v1, Lrx/Emitter$BackpressureMode;->h:Lrx/Emitter$BackpressureMode;

    invoke-static {v0, v1}, Lrx/Observable;->n(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.create({ emit\u2026.BackpressureMode.LATEST)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public f(I)V
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/hammerandchisel/libdiscord/Discord;->setVideoInputDevice(I)V

    :cond_0
    return-void
.end method

.method public declared-synchronized g(JLcom/discord/rtcconnection/mediaengine/MediaEngine$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;Lkotlin/jvm/functions/Function1;)Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Exception;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;"
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    monitor-enter p0

    :try_start_0
    const-string v2, "options"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "type"

    move-object/from16 v14, p4

    invoke-static {v14, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "onFailure"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v7, Lf/a/h/t/c/m;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v8, 0x0

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "connect called on unprepared media engine."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast v1, Lf/a/h/p;

    :try_start_1
    invoke-virtual {v1, v0}, Lf/a/h/p;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v8

    :cond_1
    :try_start_2
    iget-object v12, v7, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v12, :cond_3

    iget-object v1, v7, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    sget-object v2, Lf/a/h/t/c/m;->k:Ljava/lang/String;

    const-string v3, "TAG"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Connecting with options: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    new-instance v9, Lf/a/h/t/c/m$a;

    move-object v1, v9

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-wide/from16 v5, p1

    invoke-direct/range {v1 .. v6}, Lf/a/h/t/c/m$a;-><init>(Lf/a/h/t/c/m;Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;J)V

    new-instance v1, Lf/a/h/t/c/e;

    iget-object v2, v7, Lf/a/h/t/c/m;->g:Ljava/util/concurrent/ExecutorService;

    iget-object v10, v7, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    iget-object v11, v7, Lf/a/h/t/c/m;->c:Lf/a/h/t/c/u;

    iget-object v13, v7, Lf/a/h/t/c/m;->e:Ljava/util/List;

    if-eqz v13, :cond_2

    invoke-static {v9}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v18

    move-object v8, v1

    move-object v9, v2

    move-object/from16 v14, p4

    move-wide/from16 v15, p1

    move-object/from16 v17, p3

    invoke-direct/range {v8 .. v18}, Lf/a/h/t/c/e;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/discord/utilities/logging/Logger;Lf/a/h/t/c/u;Lcom/hammerandchisel/libdiscord/Discord;Ljava/util/List;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;JLcom/discord/rtcconnection/mediaengine/MediaEngine$a;Ljava/util/List;)V

    move-object v8, v1

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "supportedVideoCodecs"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v8

    :cond_3
    :goto_1
    if-eqz v8, :cond_4

    :try_start_3
    iget-object v0, v7, Lf/a/h/t/c/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lf/a/h/t/c/m$b;

    invoke-direct {v0, v8}, Lf/a/h/t/c/m$b;-><init>(Lf/a/h/t/c/e;)V

    invoke-virtual {v7, v0}, Lf/a/h/t/c/m;->q(Lkotlin/jvm/functions/Function1;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    monitor-exit p0

    return-object v8

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getConnections()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lf/a/h/t/c/m;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h(F)V
    .locals 3

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    const/high16 v1, 0x43960000    # 300.0f

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/hammerandchisel/libdiscord/Discord;->setSpeakerVolume(F)V

    :cond_0
    return-void
.end method

.method public i(Z)V
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/hammerandchisel/libdiscord/Discord;->setNoiseSuppression(Z)V

    :cond_0
    return-void
.end method

.method public j()Lcom/hammerandchisel/libdiscord/Discord;
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    return-object v0
.end method

.method public k(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-[",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "devicesCallback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/h/t/c/m;->r()V

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    new-instance v1, Lf/a/h/t/c/s;

    invoke-direct {v1, p1}, Lf/a/h/t/c/s;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, v1}, Lcom/hammerandchisel/libdiscord/Discord;->getVideoInputDevices(Lcom/hammerandchisel/libdiscord/Discord$GetVideoInputDevicesCallback;)V

    :cond_0
    return-void
.end method

.method public l()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/m;->g:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public m(Z)V
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/hammerandchisel/libdiscord/Discord;->setAutomaticGainControl(Z)V

    :cond_0
    return-void
.end method

.method public n(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lf/a/h/t/c/m;->r()V

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_1

    new-instance v1, Lf/a/h/t/c/m$f;

    invoke-direct {v1, p1}, Lf/a/h/t/c/m$f;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, v1}, Lcom/hammerandchisel/libdiscord/Discord;->setLocalVoiceLevelChangedCallback(Lcom/hammerandchisel/libdiscord/Discord$LocalVoiceLevelChangedCallback;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/hammerandchisel/libdiscord/Discord;->setLocalVoiceLevelChangedCallback(Lcom/hammerandchisel/libdiscord/Discord$LocalVoiceLevelChangedCallback;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final q(Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$b;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/a/h/t/c/m;->d:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/rtcconnection/mediaengine/MediaEngine$b;

    :try_start_0
    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    iget-object v2, p0, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    sget-object v3, Lf/a/h/t/c/m;->k:Ljava/lang/String;

    const-string v1, "TAG"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    const-string v4, "Error in listener"

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final r()V
    .locals 11

    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    sget-object v0, Lf/a/h/t/c/m;->k:Ljava/lang/String;

    const-string v8, "TAG"

    invoke-static {v0, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initializing voice engine. OpenSL ES: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lf/a/h/t/c/m;->h:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v2, v0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    iget-object v1, p0, Lf/a/h/t/c/m;->h:Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eqz v1, :cond_3

    if-eq v1, v10, :cond_2

    if-eq v1, v9, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {v10}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->setBlacklistDeviceForOpenSLESUsage(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->setBlacklistDeviceForOpenSLESUsage(Z)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lf/a/h/t/c/m;->j:Ljava/util/HashSet;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    iget-object v3, p0, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    invoke-static {v0, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OpenSL ES default. enableOpenSL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v5, ", model: \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v3

    move-object v3, v0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    xor-int/lit8 v0, v1, 0x1

    invoke-static {v0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->setBlacklistDeviceForOpenSLESUsage(Z)V

    :goto_0
    new-instance v0, Lf/a/h/t/c/m$g;

    invoke-direct {v0, p0}, Lf/a/h/t/c/m$g;-><init>(Lf/a/h/t/c/m;)V

    sput-object v0, Lorg/webrtc/Logging;->externalReporter:Lorg/webrtc/Logging$ExternalReporter;

    :try_start_0
    new-instance v0, Lcom/hammerandchisel/libdiscord/Discord;

    iget-object v1, p0, Lf/a/h/t/c/m;->f:Landroid/content/Context;

    invoke-direct {v0, v1, v9}, Lcom/hammerandchisel/libdiscord/Discord;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;
    :try_end_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v4, v0

    iget-object v1, p0, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    sget-object v2, Lf/a/h/t/c/m;->k:Ljava/lang/String;

    invoke-static {v2, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    const-string v3, "Unable to initialize voice engine, new error discovered"

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v4, v0

    iget-object v1, p0, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    sget-object v2, Lf/a/h/t/c/m;->k:Ljava/lang/String;

    invoke-static {v2, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    const-string v3, "Unable to initialize voice engine."

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v4, v0

    iget-object v1, p0, Lf/a/h/t/c/m;->i:Lcom/discord/utilities/logging/Logger;

    sget-object v2, Lf/a/h/t/c/m;->k:Ljava/lang/String;

    invoke-static {v2, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    const-string v3, "Unable to initialize voice engine."

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_4

    invoke-virtual {v0, v10}, Lcom/hammerandchisel/libdiscord/Discord;->enableBuiltInAEC(Z)V

    :cond_4
    iget-object v0, p0, Lf/a/h/t/c/m;->b:Lcom/hammerandchisel/libdiscord/Discord;

    if-eqz v0, :cond_5

    sget-object v0, Lf/a/h/t/c/m$h;->d:Lf/a/h/t/c/m$h;

    invoke-virtual {p0, v0}, Lf/a/h/t/c/m;->q(Lkotlin/jvm/functions/Function1;)V

    :cond_5
    return-void
.end method
