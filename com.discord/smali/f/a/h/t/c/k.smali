.class public final Lf/a/h/t/c/k;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $streamId:Ljava/lang/String;

.field public final synthetic $userId:J

.field public final synthetic $videoSsrc:I

.field public final synthetic this$0:Lf/a/h/t/c/e;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/e;JLjava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/k;->this$0:Lf/a/h/t/c/e;

    iput-wide p2, p0, Lf/a/h/t/c/k;->$userId:J

    iput-object p4, p0, Lf/a/h/t/c/k;->$streamId:Ljava/lang/String;

    iput p5, p0, Lf/a/h/t/c/k;->$videoSsrc:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    move-object v0, p1

    check-cast v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;

    const-string p1, "it"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v1, p0, Lf/a/h/t/c/k;->$userId:J

    iget-object p1, p0, Lf/a/h/t/c/k;->$streamId:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lx/s/l;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v3, p1

    iget-object p1, p0, Lf/a/h/t/c/k;->this$0:Lf/a/h/t/c/e;

    iget-object p1, p1, Lf/a/h/t/c/e;->c:Ljava/util/HashMap;

    iget-wide v4, p0, Lf/a/h/t/c/k;->$userId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_1
    const-string v5, "audioSsrcs[userId] ?: 0"

    invoke-static {p1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget v5, p0, Lf/a/h/t/c/k;->$videoSsrc:I

    iget-object v6, p0, Lf/a/h/t/c/k;->this$0:Lf/a/h/t/c/e;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-lez v5, :cond_2

    add-int/lit8 v4, v5, 0x1

    move v6, v4

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    move v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;->onVideo(JLjava/lang/Integer;III)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
