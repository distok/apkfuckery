.class public final Lf/a/h/t/c/e$i;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/e;->h(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lco/discord/media_engine/Connection;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

.field public final synthetic $inputModeOptions:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/e$i;->$inputModeOptions:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    iput-object p2, p0, Lf/a/h/t/c/e$i;->$inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lco/discord/media_engine/Connection;

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/e$i;->$inputModeOptions:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    iget v0, v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->b:I

    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->setVADLeadingFramesToBuffer(I)V

    iget-object v0, p0, Lf/a/h/t/c/e$i;->$inputModeOptions:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    iget v0, v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->c:I

    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->setVADTrailingFramesToSend(I)V

    iget-object v0, p0, Lf/a/h/t/c/e$i;->$inputModeOptions:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    iget v0, v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->a:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->setVADTriggerThreshold(F)V

    iget-object v0, p0, Lf/a/h/t/c/e$i;->$inputModeOptions:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    iget-boolean v0, v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->setVADAutoThreshold(I)V

    iget-object v0, p0, Lf/a/h/t/c/e$i;->$inputModeOptions:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;

    iget-boolean v0, v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->e:Z

    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->setVADUseKrisp(Z)V

    iget-object v0, p0, Lf/a/h/t/c/e$i;->$inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    invoke-virtual {v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->getNumeral()I

    move-result v0

    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->setAudioInputMode(I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->enableForwardErrorCorrection(Z)V

    const v0, 0x3e99999a    # 0.3f

    invoke-virtual {p1, v0}, Lco/discord/media_engine/Connection;->setExpectedPacketLossRate(F)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
