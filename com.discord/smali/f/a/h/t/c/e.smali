.class public final Lf/a/h/t/c/e;
.super Ljava/lang/Object;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;


# static fields
.field public static final m:Ljava/lang/String; = "e"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

.field public final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lf/a/h/t/a;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lco/discord/media_engine/Connection;

.field public final g:Ljava/util/concurrent/ExecutorService;

.field public final h:Lcom/discord/utilities/logging/Logger;

.field public final i:Lf/a/h/t/c/u;

.field public final j:Lcom/hammerandchisel/libdiscord/Discord;

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/a/h/t/a;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/discord/utilities/logging/Logger;Lf/a/h/t/c/u;Lcom/hammerandchisel/libdiscord/Discord;Ljava/util/List;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;JLcom/discord/rtcconnection/mediaengine/MediaEngine$a;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/discord/utilities/logging/Logger;",
            "Lf/a/h/t/c/u;",
            "Lcom/hammerandchisel/libdiscord/Discord;",
            "Ljava/util/List<",
            "Lf/a/h/t/a;",
            ">;",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;",
            "J",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p9

    move-object/from16 v9, p10

    const-string v8, "executorService"

    invoke-static {p1, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "logger"

    invoke-static {p2, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "noiseCancellationConfig"

    invoke-static {p3, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v8, "voiceEngineLegacy"

    invoke-static {v4, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v8, "supportedVideoCodecs"

    invoke-static {v5, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v8, "type"

    invoke-static {v6, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "connectionOptions"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "listeners"

    invoke-static {v9, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lf/a/h/t/c/e;->g:Ljava/util/concurrent/ExecutorService;

    iput-object v2, v0, Lf/a/h/t/c/e;->h:Lcom/discord/utilities/logging/Logger;

    iput-object v3, v0, Lf/a/h/t/c/e;->i:Lf/a/h/t/c/u;

    iput-object v4, v0, Lf/a/h/t/c/e;->j:Lcom/hammerandchisel/libdiscord/Discord;

    iput-object v5, v0, Lf/a/h/t/c/e;->k:Ljava/util/List;

    iput-object v6, v0, Lf/a/h/t/c/e;->l:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    sget-object v10, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;->CONNECTING:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

    iput-object v10, v0, Lf/a/h/t/c/e;->b:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

    const/4 v1, 0x1

    new-array v2, v1, [Lkotlin/Pair;

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget v5, v7, Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v8, Lkotlin/Pair;

    invoke-direct {v8, v3, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v3, 0x0

    aput-object v8, v2, v3

    const-string v5, "pairs"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Ljava/util/HashMap;

    invoke-static {v1}, Lf/h/a/f/f/n/g;->mapCapacity(I)I

    move-result v8

    invoke-direct {v5, v8}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v5, v2}, Lx/h/f;->putAll(Ljava/util/Map;[Lkotlin/Pair;)V

    iput-object v5, v0, Lf/a/h/t/c/e;->c:Ljava/util/HashMap;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v0, Lf/a/h/t/c/e;->d:Ljava/util/HashMap;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lf/a/h/t/c/e;->e:Ljava/util/ArrayList;

    sget-object v2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;->STREAM:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    if-ne v6, v2, :cond_0

    const/4 v8, 0x1

    goto :goto_0

    :cond_0
    const/4 v8, 0x0

    :goto_0
    iget v2, v7, Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;->a:I

    iget-object v5, v7, Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;->b:Ljava/lang/String;

    iget v6, v7, Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;->c:I

    new-instance v11, Lf/a/h/t/c/d;

    invoke-direct {v11, p0}, Lf/a/h/t/c/d;-><init>(Lf/a/h/t/c/e;)V

    move-object/from16 v1, p4

    move-wide/from16 v3, p7

    move v7, v8

    move-object v8, v11

    invoke-virtual/range {v1 .. v8}, Lcom/hammerandchisel/libdiscord/Discord;->connectToServer(IJLjava/lang/String;IZLcom/hammerandchisel/libdiscord/Discord$ConnectToServerCallback;)Lco/discord/media_engine/Connection;

    move-result-object v1

    const-string/jumbo v2, "voiceEngineLegacy.connec\u2026fo, errorMessage) }\n    }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/a/h/t/c/b;

    invoke-direct {v2, p0}, Lf/a/h/t/c/b;-><init>(Lf/a/h/t/c/e;)V

    invoke-virtual {v1, v2}, Lco/discord/media_engine/Connection;->setOnVideoCallback(Lco/discord/media_engine/Connection$OnVideoCallback;)V

    new-instance v2, Lf/a/h/t/c/c;

    invoke-direct {v2, p0}, Lf/a/h/t/c/c;-><init>(Lf/a/h/t/c/e;)V

    invoke-virtual {v1, v2}, Lco/discord/media_engine/Connection;->setUserSpeakingStatusChangedCallback(Lco/discord/media_engine/Connection$UserSpeakingStatusChangedCallback;)V

    iput-object v1, v0, Lf/a/h/t/c/e;->f:Lco/discord/media_engine/Connection;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lf/a/h/t/c/e;->a:Ljava/util/List;

    iput-object v10, v0, Lf/a/h/t/c/e;->b:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

    new-instance v1, Lf/a/h/t/c/a;

    invoke-direct {v1, p0, v10}, Lf/a/h/t/c/a;-><init>(Lf/a/h/t/c/e;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;)V

    invoke-virtual {p0, v1}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final s(Lf/a/h/t/c/e;Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;
    .locals 1

    iget-object p0, p0, Lf/a/h/t/c/e;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lf/a/h/t/c/l;

    invoke-direct {v0, p1}, Lf/a/h/t/c/l;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public declared-synchronized a(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "permission"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/h/t/c/e$q;

    invoke-direct {v0}, Lf/a/h/t/c/e$q;-><init>()V

    new-instance v1, Lf/a/h/t/b;

    invoke-direct {v1, p1, v0, p2}, Lf/a/h/t/b;-><init>(Landroid/content/Intent;Landroid/media/projection/MediaProjection$Callback;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V

    new-instance p1, Lf/a/h/t/c/e$p;

    invoke-direct {p1, v1}, Lf/a/h/t/c/e$p;-><init>(Lf/a/h/t/b;)V

    invoke-virtual {p0, p1}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/e;->i:Lf/a/h/t/c/u;

    iget-boolean v0, v0, Lf/a/h/t/c/u;->a:Z

    return v0
.end method

.method public c(Z)V
    .locals 1

    new-instance v0, Lf/a/h/t/c/e$n;

    invoke-direct {v0, p1}, Lf/a/h/t/c/e$n;-><init>(Z)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public d(JF)V
    .locals 1

    new-instance v0, Lf/a/h/t/c/e$k;

    invoke-direct {v0, p1, p2, p3}, Lf/a/h/t/c/e$k;-><init>(JF)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public e(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V
    .locals 1

    const-string/jumbo v0, "status"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/h/t/c/e$d;

    invoke-direct {v0, p0, p1}, Lf/a/h/t/c/e$d;-><init>(Lf/a/h/t/c/e;Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public declared-synchronized f()V
    .locals 1

    monitor-enter p0

    :try_start_0
    sget-object v0, Lf/a/h/t/c/e$r;->d:Lf/a/h/t/c/e$r;

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g(Z)V
    .locals 1

    new-instance v0, Lf/a/h/t/c/e$o;

    invoke-direct {v0, p1}, Lf/a/h/t/c/e$o;-><init>(Z)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public getType()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;
    .locals 1

    iget-object v0, p0, Lf/a/h/t/c/e;->l:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    return-object v0
.end method

.method public h(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;)V
    .locals 2

    const-string v0, "inputMode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputModeOptions"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/e;->i:Lf/a/h/t/c/u;

    sget-object v1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->VOICE_ACTIVITY:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    if-ne p1, v1, :cond_0

    iget-boolean v1, p2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;->d:Z

    :cond_0
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/a/h/t/c/e$i;

    invoke-direct {v0, p2, p1}, Lf/a/h/t/c/e$i;-><init>(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public i()V
    .locals 6

    iget-object v0, p0, Lf/a/h/t/c/e;->h:Lcom/discord/utilities/logging/Logger;

    sget-object v1, Lf/a/h/t/c/e;->m:Ljava/lang/String;

    const-string v2, "TAG"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "destroy(). Disconnecting from server"

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->i$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    sget-object v0, Lf/a/h/t/c/e$b;->d:Lf/a/h/t/c/e$b;

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    new-instance v0, Lf/a/h/t/c/e$c;

    invoke-direct {v0, p0}, Lf/a/h/t/c/e$c;-><init>(Lf/a/h/t/c/e;)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public j(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/e;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lf/a/h/t/c/e;->b:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

    invoke-interface {p1, p0, v0}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;->onConnectionStateChange(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;)V

    return-void
.end method

.method public k(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lco/discord/media_engine/Stats;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onStats"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/h/t/c/e$e;

    invoke-direct {v0, p0, p1}, Lf/a/h/t/c/e$e;-><init>(Lf/a/h/t/c/e;Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public l(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/e;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public m(JILjava/lang/Integer;Z)V
    .locals 8

    if-eqz p4, :cond_0

    goto :goto_0

    :cond_0
    iget-object p4, p0, Lf/a/h/t/c/e;->d:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Integer;

    :goto_0
    const/4 v0, 0x0

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p4

    goto :goto_1

    :cond_1
    const/4 p4, 0x0

    :goto_1
    iget-object v1, p0, Lf/a/h/t/c/e;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const/4 v2, 0x1

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p3, :cond_3

    :goto_2
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lf/a/h/t/c/e;->d:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-nez v3, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, p4, :cond_5

    :goto_4
    const/4 v0, 0x1

    :cond_5
    if-nez v1, :cond_6

    if-eqz v0, :cond_7

    :cond_6
    new-instance v0, Lf/a/h/t/c/e$a;

    move-object v1, v0

    move-object v2, p0

    move-wide v3, p1

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lf/a/h/t/c/e$a;-><init>(Lf/a/h/t/c/e;JIIZ)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    :cond_7
    iget-object p5, p0, Lf/a/h/t/c/e;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {p5, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p3, p0, Lf/a/h/t/c/e;->d:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public n(Z)V
    .locals 1

    new-instance v0, Lf/a/h/t/c/e$l;

    invoke-direct {v0, p1}, Lf/a/h/t/c/e$l;-><init>(Z)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public o(Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    move-object v1, p0

    move-object v0, p1

    move-object/from16 v9, p2

    const-string v2, "audioCodec"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "videoCodec"

    invoke-static {v9, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v1, Lf/a/h/t/c/e;->e:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lf/a/h/t/a;

    iget-object v5, v5, Lf/a/h/t/a;->a:Ljava/lang/String;

    invoke-static {v5, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_1
    move-object v3, v4

    :goto_0
    move-object v10, v3

    check-cast v10, Lf/a/h/t/a;

    iget-object v2, v1, Lf/a/h/t/c/e;->e:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lf/a/h/t/a;

    iget-object v5, v5, Lf/a/h/t/a;->a:Ljava/lang/String;

    invoke-static {v5, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v4, v3

    :cond_3
    move-object v11, v4

    check-cast v11, Lf/a/h/t/a;

    const/16 v2, 0x2e

    if-eqz v10, :cond_6

    if-eqz v11, :cond_5

    :try_start_0
    iget-object v2, v11, Lf/a/h/t/a;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    const v12, 0xbb80

    new-instance v13, Lco/discord/media_engine/AudioEncoder;

    iget v3, v10, Lf/a/h/t/a;->d:I

    const/16 v6, 0x3c0

    const/4 v7, 0x1

    const v8, 0xfa00

    const v5, 0xbb80

    move-object v2, v13

    move-object v4, p1

    invoke-direct/range {v2 .. v8}, Lco/discord/media_engine/AudioEncoder;-><init>(ILjava/lang/String;IIII)V

    new-instance v8, Lco/discord/media_engine/AudioDecoder;

    iget v3, v10, Lf/a/h/t/a;->d:I

    const/4 v6, 0x2

    new-instance v2, Lkotlin/Pair;

    const-string/jumbo v4, "stereo"

    const-string v10, "1"

    invoke-direct {v2, v4, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v7

    move-object v2, v8

    move-object v4, p1

    move v5, v12

    invoke-direct/range {v2 .. v7}, Lco/discord/media_engine/AudioDecoder;-><init>(ILjava/lang/String;IILjava/util/Map;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/Pair;

    const/4 v2, 0x0

    new-instance v3, Lkotlin/Pair;

    const-string v4, "level-asymmetry-allowed"

    invoke-direct {v3, v4, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v2

    const/4 v2, 0x1

    new-instance v3, Lkotlin/Pair;

    const-string v4, "packetization-mode"

    invoke-direct {v3, v4, v10}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v2

    const/4 v2, 0x2

    new-instance v3, Lkotlin/Pair;

    const-string v4, "profile-level-id"

    const-string v5, "42e01f"

    invoke-direct {v3, v4, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v2

    invoke-static {v0}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    new-instance v2, Lco/discord/media_engine/VideoEncoder;

    iget v3, v11, Lf/a/h/t/a;->d:I

    iget-object v4, v11, Lf/a/h/t/a;->e:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {v2, v9, v3, v4, v0}, Lco/discord/media_engine/VideoEncoder;-><init>(Ljava/lang/String;IILjava/util/Map;)V

    new-instance v3, Lco/discord/media_engine/VideoDecoder;

    iget v4, v11, Lf/a/h/t/a;->d:I

    iget-object v5, v11, Lf/a/h/t/a;->e:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v3, v9, v4, v5, v0}, Lco/discord/media_engine/VideoDecoder;-><init>(Ljava/lang/String;IILjava/util/Map;)V

    new-instance v0, Lf/a/h/t/c/e$g;

    invoke-direct {v0, v13, v2, v8, v3}, Lf/a/h/t/c/e$g;-><init>(Lco/discord/media_engine/AudioEncoder;Lco/discord/media_engine/VideoEncoder;Lco/discord/media_engine/AudioDecoder;Lco/discord/media_engine/VideoDecoder;)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_4
    :try_start_1
    new-instance v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Video codec "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " rtxPayloadType was null."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;->CODEC_NEGOTIATION_FAILED:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;

    invoke-direct {v0, v2, v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;-><init>(Ljava/lang/String;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;)V

    throw v0

    :cond_5
    new-instance v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Missing video codec: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;->CODEC_NEGOTIATION_FAILED:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;

    invoke-direct {v0, v2, v3}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;-><init>(Ljava/lang/String;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;)V

    throw v0

    :cond_6
    new-instance v3, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Missing audio codec: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;->CODEC_NEGOTIATION_FAILED:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;

    invoke-direct {v3, v0, v2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;-><init>(Ljava/lang/String;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;)V

    throw v3
    :try_end_1
    .catch Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lf/a/h/t/c/e$f;

    invoke-direct {v2, p0, v0}, Lf/a/h/t/c/e$f;-><init>(Lf/a/h/t/c/e;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;)V

    invoke-virtual {p0, v2}, Lf/a/h/t/c/e;->t(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public p(Ljava/lang/String;[I)V
    .locals 1

    const-string v0, "mode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secretKey"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/discord/media_engine/EncryptionSettings;

    invoke-direct {v0, p1, p2}, Lco/discord/media_engine/EncryptionSettings;-><init>(Ljava/lang/String;[I)V

    new-instance p1, Lf/a/h/t/c/e$h;

    invoke-direct {p1, v0}, Lf/a/h/t/c/e$h;-><init>(Lco/discord/media_engine/EncryptionSettings;)V

    invoke-virtual {p0, p1}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public q(Z)V
    .locals 1

    new-instance v0, Lf/a/h/t/c/e$m;

    invoke-direct {v0, p1}, Lf/a/h/t/c/e$m;-><init>(Z)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public r(JZ)V
    .locals 1

    new-instance v0, Lf/a/h/t/c/e$j;

    invoke-direct {v0, p1, p2, p3}, Lf/a/h/t/c/e$j;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lf/a/h/t/c/e;->u(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final t(Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/a/h/t/c/e;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;

    :try_start_0
    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    iget-object v2, p0, Lf/a/h/t/c/e;->h:Lcom/discord/utilities/logging/Logger;

    sget-object v3, Lf/a/h/t/c/e;->m:Ljava/lang/String;

    const-string v1, "TAG"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    const-string v4, "Error in listener"

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final declared-synchronized u(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lco/discord/media_engine/Connection;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/a/h/t/c/e;->f:Lco/discord/media_engine/Connection;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
