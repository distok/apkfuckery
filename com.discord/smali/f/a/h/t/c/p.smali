.class public final Lf/a/h/t/c/p;
.super Ljava/lang/Object;
.source "MediaEngineLegacy.kt"

# interfaces
.implements Lcom/hammerandchisel/libdiscord/Discord$GetSupportedVideoCodecsCallback;


# instance fields
.field public final synthetic a:Lf/a/h/t/c/q;

.field public final synthetic b:Lrx/Emitter;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/q;Lrx/Emitter;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/p;->a:Lf/a/h/t/c/q;

    iput-object p2, p0, Lf/a/h/t/c/p;->b:Lrx/Emitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSupportedVideoCodecs([Ljava/lang/String;)V
    .locals 10

    const-string/jumbo v0, "videoCodecsStringArray"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/p;->a:Lf/a/h/t/c/q;

    iget-object v0, v0, Lf/a/h/t/c/q;->d:Lf/a/h/t/c/m;

    sget-object v1, Lf/a/h/t/c/m;->k:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p1, v3

    sget-object v5, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v6, "Locale.ROOT"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "null cannot be cast to non-null type java.lang.String"

    invoke-static {v4, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lf/a/h/t/c/n;

    invoke-direct {p1}, Lf/a/h/t/c/n;-><init>()V

    invoke-static {v0, p1}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v9, v2, 0x1

    if-ltz v2, :cond_1

    move-object v4, v1

    check-cast v4, Ljava/lang/String;

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v7, v2, 0x65

    new-instance v1, Lf/a/h/t/a;

    add-int/lit8 v2, v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string/jumbo v6, "video"

    move-object v3, v1

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lf/a/h/t/a;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/Integer;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v9

    goto :goto_1

    :cond_1
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    const/4 p1, 0x0

    throw p1

    :cond_2
    iget-object p1, p0, Lf/a/h/t/c/p;->b:Lrx/Emitter;

    invoke-interface {p1, v0}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lf/a/h/t/c/p;->b:Lrx/Emitter;

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    return-void
.end method
