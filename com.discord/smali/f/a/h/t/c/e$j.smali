.class public final Lf/a/h/t/c/e$j;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/e;->r(JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lco/discord/media_engine/Connection;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $mute:Z

.field public final synthetic $userId:J


# direct methods
.method public constructor <init>(JZ)V
    .locals 0

    iput-wide p1, p0, Lf/a/h/t/c/e$j;->$userId:J

    iput-boolean p3, p0, Lf/a/h/t/c/e$j;->$mute:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lco/discord/media_engine/Connection;

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lf/a/h/t/c/e$j;->$userId:J

    iget-boolean v2, p0, Lf/a/h/t/c/e$j;->$mute:Z

    invoke-virtual {p1, v0, v1, v2}, Lco/discord/media_engine/Connection;->muteUser(JZ)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
