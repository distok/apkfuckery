.class public final Lf/a/h/t/c/d;
.super Ljava/lang/Object;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lcom/hammerandchisel/libdiscord/Discord$ConnectToServerCallback;


# instance fields
.field public final synthetic a:Lf/a/h/t/c/e;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/e;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/d;->a:Lf/a/h/t/c/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConnectToServer(Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;Ljava/lang/String;)V
    .locals 2

    const-string v0, "connectionInfo"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/d;->a:Lf/a/h/t/c/e;

    new-instance v1, Lf/a/h/t/c/d$a;

    invoke-direct {v1, p0, p1, p2}, Lf/a/h/t/c/d$a;-><init>(Lf/a/h/t/c/d;Lcom/hammerandchisel/libdiscord/Discord$ConnectionInfo;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lf/a/h/t/c/e;->s(Lf/a/h/t/c/e;Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    return-void
.end method
