.class public final Lf/a/h/t/c/e$a;
.super Lx/m/c/k;
.source "MediaEngineConnectionLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/e;->m(JILjava/lang/Integer;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lco/discord/media_engine/Connection;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $audioSsrc:I

.field public final synthetic $isMuted:Z

.field public final synthetic $resolvedVideoSsrc:I

.field public final synthetic $userId:J

.field public final synthetic this$0:Lf/a/h/t/c/e;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/e;JIIZ)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/e$a;->this$0:Lf/a/h/t/c/e;

    iput-wide p2, p0, Lf/a/h/t/c/e$a;->$userId:J

    iput p4, p0, Lf/a/h/t/c/e$a;->$audioSsrc:I

    iput p5, p0, Lf/a/h/t/c/e$a;->$resolvedVideoSsrc:I

    iput-boolean p6, p0, Lf/a/h/t/c/e$a;->$isMuted:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    move-object v0, p1

    check-cast v0, Lco/discord/media_engine/Connection;

    const-string p1, "$receiver"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v1, p0, Lf/a/h/t/c/e$a;->$userId:J

    iget v3, p0, Lf/a/h/t/c/e$a;->$audioSsrc:I

    iget v4, p0, Lf/a/h/t/c/e$a;->$resolvedVideoSsrc:I

    iget-object p1, p0, Lf/a/h/t/c/e$a;->this$0:Lf/a/h/t/c/e;

    sget-object v5, Lf/a/h/t/c/e;->m:Ljava/lang/String;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-lez v4, :cond_0

    add-int/lit8 p1, v4, 0x1

    move v5, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v5, 0x0

    :goto_0
    iget-boolean v6, p0, Lf/a/h/t/c/e$a;->$isMuted:Z

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v7}, Lco/discord/media_engine/Connection;->connectUser(JIIIZF)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
