.class public final synthetic Lf/a/h/t/c/m$h;
.super Lx/m/c/i;
.source "MediaEngineLegacy.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/m;->r()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/rtcconnection/mediaengine/MediaEngine$b;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lf/a/h/t/c/m$h;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/h/t/c/m$h;

    invoke-direct {v0}, Lf/a/h/t/c/m$h;-><init>()V

    sput-object v0, Lf/a/h/t/c/m$h;->d:Lf/a/h/t/c/m$h;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Lcom/discord/rtcconnection/mediaengine/MediaEngine$b;

    const/4 v1, 0x1

    const-string v3, "onNativeEngineInitialized"

    const-string v4, "onNativeEngineInitialized()V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/discord/rtcconnection/mediaengine/MediaEngine$b;

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine$b;->onNativeEngineInitialized()V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
