.class public final Lf/a/h/t/c/m$a;
.super Ljava/lang/Object;
.source "MediaEngineLegacy.kt"

# interfaces
.implements Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/h/t/c/m;->g(JLcom/discord/rtcconnection/mediaengine/MediaEngine$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;Lkotlin/jvm/functions/Function1;)Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/a/h/t/c/m;


# direct methods
.method public constructor <init>(Lf/a/h/t/c/m;Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;J)V
    .locals 0

    iput-object p1, p0, Lf/a/h/t/c/m$a;->a:Lf/a/h/t/c/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;",
            "Ljava/util/List<",
            "Lf/a/h/t/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "transportInfo"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "supportedVideoCodecs"

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onConnectionStateChange(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;->DISCONNECTED:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;

    if-ne p2, v0, :cond_0

    iget-object p2, p0, Lf/a/h/t/c/m$a;->a:Lf/a/h/t/c/m;

    new-instance v0, Lf/a/h/t/c/m$a$b;

    invoke-direct {v0, p0, p1}, Lf/a/h/t/c/m$a$b;-><init>(Lf/a/h/t/c/m$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V

    invoke-static {p2, v0}, Lf/a/h/t/c/m;->p(Lf/a/h/t/c/m;Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    :cond_0
    return-void
.end method

.method public onDestroy(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V
    .locals 2

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/t/c/m$a;->a:Lf/a/h/t/c/m;

    new-instance v1, Lf/a/h/t/c/m$a$a;

    invoke-direct {v1, p0, p1}, Lf/a/h/t/c/m$a$a;-><init>(Lf/a/h/t/c/m$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V

    invoke-static {v0, v1}, Lf/a/h/t/c/m;->p(Lf/a/h/t/c/m;Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public onError(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "exception"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lf/a/h/t/c/m$a;->a:Lf/a/h/t/c/m;

    new-instance v0, Lf/a/h/t/c/m$a$c;

    invoke-direct {v0, p0, p1}, Lf/a/h/t/c/m$a$c;-><init>(Lf/a/h/t/c/m$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V

    invoke-static {p2, v0}, Lf/a/h/t/c/m;->p(Lf/a/h/t/c/m;Lkotlin/jvm/functions/Function0;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public onKrispStatus(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "status"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onSpeaking(JIZ)V
    .locals 0

    return-void
.end method

.method public onVideo(JLjava/lang/Integer;III)V
    .locals 0

    return-void
.end method
