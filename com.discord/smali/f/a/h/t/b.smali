.class public final Lf/a/h/t/b;
.super Lorg/webrtc/ScreenCapturerAndroid;
.source "ScreenCapturer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/h/t/b$b;,
        Lf/a/h/t/b$a;
    }
.end annotation


# static fields
.field public static final q:Lf/a/h/t/b$a;


# instance fields
.field public final d:Lf/a/h/t/b$b;

.field public e:Lco/discord/media_engine/NativeCapturerObserver;

.field public f:Lorg/webrtc/SurfaceTextureHelper;

.field public g:Landroid/view/Display;

.field public final h:Lco/discord/media_engine/SoundshareAudioSource;

.field public i:I

.field public j:I

.field public k:Ljava/lang/Long;

.field public final l:Landroid/graphics/Point;

.field public final m:Landroid/graphics/Point;

.field public final n:Landroid/graphics/Point;

.field public o:I

.field public final p:Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lf/a/h/t/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/a/h/t/b$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lf/a/h/t/b;->q:Lf/a/h/t/b$a;

    const v0, 0xac44

    const/16 v1, 0x10

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Landroid/media/projection/MediaProjection$Callback;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V
    .locals 1

    const-string v0, "mediaProjectionPermissionResultData"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaProjectionCallback"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lorg/webrtc/ScreenCapturerAndroid;-><init>(Landroid/content/Intent;Landroid/media/projection/MediaProjection$Callback;)V

    iput-object p3, p0, Lf/a/h/t/b;->p:Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;

    new-instance p1, Lf/a/h/t/b$b;

    invoke-direct {p1, p0}, Lf/a/h/t/b$b;-><init>(Lf/a/h/t/b;)V

    iput-object p1, p0, Lf/a/h/t/b;->d:Lf/a/h/t/b$b;

    new-instance p1, Lco/discord/media_engine/SoundshareAudioSource;

    invoke-direct {p1}, Lco/discord/media_engine/SoundshareAudioSource;-><init>()V

    iput-object p1, p0, Lf/a/h/t/b;->h:Lco/discord/media_engine/SoundshareAudioSource;

    new-instance p1, Landroid/graphics/Point;

    invoke-direct {p1}, Landroid/graphics/Point;-><init>()V

    iput-object p1, p0, Lf/a/h/t/b;->l:Landroid/graphics/Point;

    new-instance p1, Landroid/graphics/Point;

    invoke-direct {p1}, Landroid/graphics/Point;-><init>()V

    iput-object p1, p0, Lf/a/h/t/b;->m:Landroid/graphics/Point;

    new-instance p1, Landroid/graphics/Point;

    invoke-direct {p1}, Landroid/graphics/Point;-><init>()V

    iput-object p1, p0, Lf/a/h/t/b;->n:Landroid/graphics/Point;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/graphics/Point;I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "measuredSize"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-lez v0, :cond_2

    int-to-float p2, p2

    int-to-float v0, v0

    div-float/2addr p2, v0

    iget-object v0, p0, Lf/a/h/t/b;->n:Landroid/graphics/Point;

    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    mul-float v1, v1, p2

    float-to-int v1, v1

    iget p1, p1, Landroid/graphics/Point;->y:I

    int-to-float p1, p1

    mul-float p1, p1, p2

    float-to-int p1, p1

    invoke-virtual {v0, v1, p1}, Landroid/graphics/Point;->set(II)V

    iget-object p1, p0, Lf/a/h/t/b;->n:Landroid/graphics/Point;

    iget p2, p1, Landroid/graphics/Point;->x:I

    rem-int/lit8 v0, p2, 0x10

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 p2, p2, 0x10

    :goto_0
    sub-int/2addr p2, v0

    iget v0, p1, Landroid/graphics/Point;->y:I

    rem-int/lit8 v2, v0, 0x10

    if-ge v2, v1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x10

    :goto_1
    sub-int/2addr v0, v2

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Point;->set(II)V

    iget-object p1, p0, Lf/a/h/t/b;->n:Landroid/graphics/Point;

    iget p2, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    iget v0, p0, Lf/a/h/t/b;->i:I

    invoke-super {p0, p2, p1, v0}, Lorg/webrtc/ScreenCapturerAndroid;->changeCaptureFormat(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b()Landroid/media/AudioRecord;
    .locals 4
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x1d
    .end annotation

    iget-object v0, p0, Lorg/webrtc/ScreenCapturerAndroid;->mediaProjection:Landroid/media/projection/MediaProjection;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/media/AudioPlaybackCaptureConfiguration$Builder;

    invoke-direct {v1, v0}, Landroid/media/AudioPlaybackCaptureConfiguration$Builder;-><init>(Landroid/media/projection/MediaProjection;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/media/AudioPlaybackCaptureConfiguration$Builder;->addMatchingUsage(I)Landroid/media/AudioPlaybackCaptureConfiguration$Builder;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/media/AudioPlaybackCaptureConfiguration$Builder;->addMatchingUsage(I)Landroid/media/AudioPlaybackCaptureConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioPlaybackCaptureConfiguration$Builder;->addMatchingUsage(I)Landroid/media/AudioPlaybackCaptureConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioPlaybackCaptureConfiguration$Builder;->build()Landroid/media/AudioPlaybackCaptureConfiguration;

    move-result-object v0

    const-string v1, "AudioPlaybackCaptureConf\u2026KNOWN)\n          .build()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Landroid/media/AudioRecord$Builder;

    invoke-direct {v1}, Landroid/media/AudioRecord$Builder;-><init>()V

    new-instance v2, Landroid/media/AudioFormat$Builder;

    invoke-direct {v2}, Landroid/media/AudioFormat$Builder;-><init>()V

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/media/AudioFormat$Builder;->setEncoding(I)Landroid/media/AudioFormat$Builder;

    move-result-object v2

    const v3, 0xac44

    invoke-virtual {v2, v3}, Landroid/media/AudioFormat$Builder;->setSampleRate(I)Landroid/media/AudioFormat$Builder;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/media/AudioFormat$Builder;->setChannelMask(I)Landroid/media/AudioFormat$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioFormat$Builder;->build()Landroid/media/AudioFormat;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/AudioRecord$Builder;->setAudioFormat(Landroid/media/AudioFormat;)Landroid/media/AudioRecord$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/media/AudioRecord$Builder;->setAudioPlaybackCaptureConfig(Landroid/media/AudioPlaybackCaptureConfiguration;)Landroid/media/AudioRecord$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord$Builder;->build()Landroid/media/AudioRecord;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()V
    .locals 6

    iget v0, p0, Lf/a/h/t/b;->i:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lf/a/h/t/b;->f:Lorg/webrtc/SurfaceTextureHelper;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/webrtc/SurfaceTextureHelper;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lf/a/h/t/b;->d:Lf/a/h/t/b$b;

    iget v2, p0, Lf/a/h/t/b;->j:I

    int-to-long v2, v2

    const v4, 0xf4240

    int-to-long v4, v4

    div-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "surfaceTextureHelper"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public declared-synchronized changeCaptureFormat(III)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/a/h/t/b;->l:Landroid/graphics/Point;

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lf/a/h/t/b;->a(Landroid/graphics/Point;I)V

    if-gtz p3, :cond_0

    const/4 p1, 0x0

    iput p1, p0, Lf/a/h/t/b;->i:I

    iput p1, p0, Lf/a/h/t/b;->j:I

    goto :goto_0

    :cond_0
    iput p3, p0, Lf/a/h/t/b;->i:I

    const p1, 0x3b9aca00

    div-int/2addr p1, p3

    iput p1, p0, Lf/a/h/t/b;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public dispose()V
    .locals 3

    invoke-virtual {p0}, Lf/a/h/t/b;->stopCapture()V

    iget-object v0, p0, Lf/a/h/t/b;->p:Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;

    if-eqz v0, :cond_0

    monitor-enter v0

    :try_start_0
    iget-object v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->e:Lorg/webrtc/VideoFrameDrawer;

    invoke-virtual {v1}, Lorg/webrtc/VideoFrameDrawer;->release()V

    iget-object v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->d:Lorg/webrtc/GlRectDrawer;

    invoke-virtual {v1}, Lorg/webrtc/GlRectDrawer;->release()V

    iget-object v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->b:Ljava/nio/ByteBuffer;

    invoke-static {v1}, Lorg/webrtc/JniCommon;->nativeFreeByteBuffer(Ljava/nio/ByteBuffer;)V

    const-wide v1, 0x7fffffffffffffffL

    iput-wide v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/a/h/t/b;->h:Lco/discord/media_engine/SoundshareAudioSource;

    invoke-virtual {v0}, Lco/discord/media_engine/SoundshareAudioSource;->release()V

    invoke-super {p0}, Lorg/webrtc/ScreenCapturerAndroid;->dispose()V

    return-void
.end method

.method public declared-synchronized initialize(Lorg/webrtc/SurfaceTextureHelper;Landroid/content/Context;Lorg/webrtc/CapturerObserver;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, "surfaceTextureHelper"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applicationContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capturerObserver"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lf/a/h/t/b;->f:Lorg/webrtc/SurfaceTextureHelper;

    const-string/jumbo v0, "window"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const-string/jumbo v1, "wm.defaultDisplay"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lf/a/h/t/b;->g:Landroid/view/Display;

    move-object v0, p3

    check-cast v0, Lco/discord/media_engine/NativeCapturerObserver;

    iput-object v0, p0, Lf/a/h/t/b;->e:Lco/discord/media_engine/NativeCapturerObserver;

    invoke-super {p0, p1, p2, p3}, Lorg/webrtc/ScreenCapturerAndroid;->initialize(Lorg/webrtc/SurfaceTextureHelper;Landroid/content/Context;Lorg/webrtc/CapturerObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "null cannot be cast to non-null type android.view.WindowManager"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public onFrame(Lorg/webrtc/VideoFrame;)V
    .locals 8

    iget-object v0, p0, Lf/a/h/t/b;->g:Landroid/view/Display;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lf/a/h/t/b;->m:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget-object v0, p0, Lf/a/h/t/b;->m:Landroid/graphics/Point;

    iget-object v1, p0, Lf/a/h/t/b;->l:Landroid/graphics/Point;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/a/h/t/b;->l:Landroid/graphics/Point;

    iget-object v1, p0, Lf/a/h/t/b;->m:Landroid/graphics/Point;

    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lf/a/h/t/b;->m:Landroid/graphics/Point;

    iget v1, p0, Lf/a/h/t/b;->o:I

    invoke-virtual {p0, v0, v1}, Lf/a/h/t/b;->a(Landroid/graphics/Point;I)V

    :cond_0
    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getTimestampNs()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lf/a/h/t/b;->k:Ljava/lang/Long;

    iget-object v0, p0, Lf/a/h/t/b;->p:Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;

    if-eqz v0, :cond_2

    monitor-enter v0

    :try_start_0
    const-string v1, "frame"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->l:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->f:J

    sub-long/2addr v1, v3

    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getTimestampNs()J

    move-result-wide v3

    iget-wide v5, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->a:J

    sub-long/2addr v3, v5

    iget-wide v5, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->j:J

    cmp-long v7, v1, v5

    if-lez v7, :cond_1

    iget-wide v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->i:J

    const/16 v5, 0x3e8

    int-to-long v5, v5

    mul-long v1, v1, v5

    mul-long v1, v1, v5

    cmp-long v5, v3, v1

    if-lez v5, :cond_1

    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->getTimestampNs()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->a:J

    iget-object v1, v0, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->k:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1}, Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;->a(Lorg/webrtc/VideoFrame;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-interface {v1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_2
    :goto_0
    invoke-super {p0, p1}, Lorg/webrtc/ScreenCapturerAndroid;->onFrame(Lorg/webrtc/VideoFrame;)V

    return-void

    :cond_3
    const-string p1, "defaultDisplay"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public declared-synchronized startCapture(III)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lorg/webrtc/ScreenCapturerAndroid;->startCapture(III)V

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lf/a/h/t/b;->o:I

    if-gtz p3, :cond_0

    const/4 p1, 0x0

    iput p1, p0, Lf/a/h/t/b;->i:I

    iput p1, p0, Lf/a/h/t/b;->j:I

    goto :goto_0

    :cond_0
    iput p3, p0, Lf/a/h/t/b;->i:I

    const p1, 0x3b9aca00

    div-int/2addr p1, p3

    iput p1, p0, Lf/a/h/t/b;->j:I

    :goto_0
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x1d

    if-lt p1, p2, :cond_1

    invoke-virtual {p0}, Lf/a/h/t/b;->b()Landroid/media/AudioRecord;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p2, p0, Lf/a/h/t/b;->h:Lco/discord/media_engine/SoundshareAudioSource;

    invoke-virtual {p2, p1}, Lco/discord/media_engine/SoundshareAudioSource;->startRecording(Landroid/media/AudioRecord;)Z

    :cond_1
    invoke-virtual {p0}, Lf/a/h/t/b;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized stopCapture()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lorg/webrtc/ScreenCapturerAndroid;->stopCapture()V

    iget-object v0, p0, Lf/a/h/t/b;->h:Lco/discord/media_engine/SoundshareAudioSource;

    invoke-virtual {v0}, Lco/discord/media_engine/SoundshareAudioSource;->stopRecording()Z

    const/4 v0, 0x0

    iput v0, p0, Lf/a/h/t/b;->i:I

    const/4 v0, 0x0

    iput-object v0, p0, Lf/a/h/t/b;->k:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
