.class public final Lf/a/h/t/b$b;
.super Ljava/lang/Object;
.source "ScreenCapturer.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/h/t/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field public final synthetic d:Lf/a/h/t/b;


# direct methods
.method public constructor <init>(Lf/a/h/t/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/a/h/t/b$b;->d:Lf/a/h/t/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lf/a/h/t/b$b;->d:Lf/a/h/t/b;

    iget v1, v0, Lf/a/h/t/b;->i:I

    if-lez v1, :cond_2

    iget-object v0, v0, Lf/a/h/t/b;->k:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {}, Lorg/webrtc/TimestampAligner;->getRtcTimeNanos()J

    move-result-wide v2

    sub-long v0, v2, v0

    iget-object v4, p0, Lf/a/h/t/b$b;->d:Lf/a/h/t/b;

    iget v5, v4, Lf/a/h/t/b;->j:I

    int-to-long v5, v5

    cmp-long v7, v0, v5

    if-lez v7, :cond_1

    iget-object v0, v4, Lf/a/h/t/b;->e:Lco/discord/media_engine/NativeCapturerObserver;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2, v3}, Lco/discord/media_engine/NativeCapturerObserver;->repeatLastFrame(J)V

    goto :goto_0

    :cond_0
    const-string v0, "nativeObserver"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lf/a/h/t/b$b;->d:Lf/a/h/t/b;

    invoke-virtual {v0}, Lf/a/h/t/b;->c()V

    :cond_2
    return-void
.end method
