.class public final Lf/a/h/l;
.super Ljava/lang/Object;
.source "RtcConnection.kt"

# interfaces
.implements Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;


# instance fields
.field public final synthetic a:Lcom/discord/rtcconnection/RtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/RtcConnection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/a/h/l;->a:Lcom/discord/rtcconnection/RtcConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;",
            "Ljava/util/List<",
            "Lf/a/h/t/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "transportInfo"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "supportedVideoCodecs"

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/l;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {p1}, Lcom/discord/utilities/networking/Backoff;->succeed()V

    iput-object p2, v0, Lcom/discord/rtcconnection/RtcConnection;->f:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;

    iget-object v5, v0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    const/4 p1, 0x1

    if-eqz v5, :cond_1

    new-instance v9, Lf/a/h/v/h;

    const-wide/16 v2, 0x3e8

    iget-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    new-instance v6, Lco/discord/media_engine/VoiceQuality;

    invoke-direct {v6}, Lco/discord/media_engine/VoiceQuality;-><init>()V

    new-instance v7, Lf/a/h/v/j;

    const/4 v10, 0x0

    invoke-direct {v7, v10, p1}, Lf/a/h/v/j;-><init>(Lcom/discord/utilities/time/Clock;I)V

    new-instance v8, Lcom/discord/rtcconnection/KrispOveruseDetector;

    invoke-direct {v8, v5}, Lcom/discord/rtcconnection/KrispOveruseDetector;-><init>(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lf/a/h/v/h;-><init>(JLcom/discord/utilities/logging/Logger;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lco/discord/media_engine/VoiceQuality;Lf/a/h/v/j;Lcom/discord/rtcconnection/KrispOveruseDetector;)V

    iput-object v9, v0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    iput-object v10, v9, Lf/a/h/v/h;->a:Lco/discord/media_engine/Stats;

    iget-object v1, v9, Lf/a/h/v/h;->b:Lrx/Subscription;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lrx/Subscription;->isUnsubscribed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v9}, Lf/a/h/v/h;->a()V

    :cond_0
    const-wide/16 v1, 0x0

    iget-wide v3, v9, Lf/a/h/v/h;->c:J

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v2, v3, v4, v5}, Lrx/Observable;->A(JJLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/h/v/f;

    invoke-direct {v2, v9}, Lf/a/h/v/f;-><init>(Lf/a/h/v/h;)V

    new-instance v3, Lf/a/h/v/g;

    invoke-direct {v3, v9}, Lf/a/h/v/g;-><init>(Lf/a/h/v/h;)V

    invoke-virtual {v1, v2, v3}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    iput-object v1, v9, Lf/a/h/v/h;->b:Lrx/Subscription;

    :cond_1
    iget-object v1, p2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;->c:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo$Protocol;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    const-string p1, "Unsupported protocol: "

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object p2, p2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;->c:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo$Protocol;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p2, 0x2e

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    invoke-static/range {v0 .. v5}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;ZLjava/lang/String;Ljava/lang/Throwable;ZI)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object v2, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    const-string v3, "Sending UDP info to RTC server."

    invoke-virtual {v1, v3, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v0, :cond_4

    const-string/jumbo v1, "udp"

    iget-object v2, p2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;->a:Ljava/lang/String;

    iget p2, p2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;->b:I

    const-string/jumbo v3, "xsalsa20_poly1305"

    const-string v4, "protocol"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "address"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "mode"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "codecs"

    invoke-static {p3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;

    invoke-direct {v4, v2, p2, v3}, Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    new-instance p2, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p3, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p2, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/a/h/t/a;

    new-instance v3, Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$CodecInfo;

    iget-object v6, v2, Lf/a/h/t/a;->a:Ljava/lang/String;

    iget v7, v2, Lf/a/h/t/a;->b:I

    iget-object v8, v2, Lf/a/h/t/a;->c:Ljava/lang/String;

    iget v9, v2, Lf/a/h/t/a;->d:I

    iget-object v10, v2, Lf/a/h/t/a;->e:Ljava/lang/Integer;

    move-object v5, v3

    invoke-direct/range {v5 .. v10}, Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$CodecInfo;-><init>(Ljava/lang/String;ILjava/lang/String;ILjava/lang/Integer;)V

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    new-instance p3, Lcom/discord/rtcconnection/socket/io/Payloads$Protocol;

    invoke-direct {p3, v1, v4, p2}, Lcom/discord/rtcconnection/socket/io/Payloads$Protocol;-><init>(Ljava/lang/String;Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;Ljava/util/List;)V

    invoke-virtual {v0, p1, p3}, Lf/a/h/u/a;->m(ILjava/lang/Object;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public onConnectionStateChange(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$ConnectionState;)V
    .locals 7

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "connectionState"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/a/h/l;->a:Lcom/discord/rtcconnection/RtcConnection;

    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    sget-object v1, Lcom/discord/rtcconnection/RtcConnection$State$g;->a:Lcom/discord/rtcconnection/RtcConnection$State$g;

    sget-object v2, Lcom/discord/rtcconnection/RtcConnection$State$h;->a:Lcom/discord/rtcconnection/RtcConnection$State$h;

    iget-object v3, p1, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    iget-object v4, p1, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Connection state change: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    const/4 v4, 0x1

    if-eqz p2, :cond_3

    if-eq p2, v4, :cond_2

    const/4 v5, 0x2

    if-eq p2, v5, :cond_1

    const/4 v5, 0x3

    if-ne p2, v5, :cond_0

    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$State$e;->a:Lcom/discord/rtcconnection/RtcConnection$State$e;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    move-object p2, v0

    goto :goto_0

    :cond_2
    move-object p2, v1

    goto :goto_0

    :cond_3
    move-object p2, v2

    :goto_0
    invoke-virtual {p1, p2}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    if-ne v3, v1, :cond_4

    iget-object p2, p1, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    if-ne p2, v2, :cond_4

    invoke-virtual {p1}, Lcom/discord/rtcconnection/RtcConnection;->k()V

    :cond_4
    iget-object p2, p1, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    if-ne p2, v0, :cond_8

    iget-object p2, p1, Lcom/discord/rtcconnection/RtcConnection;->E:Lcom/discord/utilities/time/Clock;

    invoke-interface {p2}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p1, Lcom/discord/rtcconnection/RtcConnection;->p:Ljava/lang/Long;

    iput-boolean v4, p1, Lcom/discord/rtcconnection/RtcConnection;->s:Z

    new-array p2, v4, [Lkotlin/Pair;

    const/4 v0, 0x0

    iget v1, p1, Lcom/discord/rtcconnection/RtcConnection;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    const-string v3, "connect_count"

    invoke-direct {v2, v3, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, p2, v0

    invoke-static {p2}, Lx/h/f;->mutableMapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p2

    iget-object v0, p1, Lcom/discord/rtcconnection/RtcConnection;->o:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/rtcconnection/RtcConnection;->p:Ljava/lang/Long;

    if-eqz v1, :cond_6

    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_6
    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "connect_time"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    invoke-virtual {p1, p2}, Lcom/discord/rtcconnection/RtcConnection;->a(Ljava/util/Map;)Ljava/util/Map;

    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;->VOICE_CONNECTION_SUCCESS:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

    invoke-virtual {p1, v0, p2}, Lcom/discord/rtcconnection/RtcConnection;->i(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V

    :cond_8
    return-void
.end method

.method public onDestroy(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onError(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;)V
    .locals 7

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "exception"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/l;->a:Lcom/discord/rtcconnection/RtcConnection;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connection error: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;->a()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException;->a()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$FailedConnectionException$FailureType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    if-eq p1, v1, :cond_0

    const/4 v4, 0x2

    if-eq p1, v4, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_1

    iget-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    const-string v1, " -- "

    invoke-static {v2, v1}, Lf/e/c/a/a;->L(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v3}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sget-object p1, Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;->VOICE_CONNECTION_FAILURE:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

    new-array v1, v1, [Lkotlin/Pair;

    iget v4, v0, Lcom/discord/rtcconnection/RtcConnection;->q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lkotlin/Pair;

    const-string v6, "connect_count"

    invoke-direct {v5, v6, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v1, v3

    invoke-static {v1}, Lx/h/f;->mutableMapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->a(Ljava/util/Map;)Ljava/util/Map;

    invoke-virtual {v0, p1, v1}, Lcom/discord/rtcconnection/RtcConnection;->i(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V

    :goto_1
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x8

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/rtcconnection/RtcConnection;->d(Lcom/discord/rtcconnection/RtcConnection;ZLjava/lang/String;Ljava/lang/Throwable;ZI)V

    return-void
.end method

.method public onKrispStatus(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "status"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onSpeaking(JIZ)V
    .locals 11

    iget-object v0, p0, Lf/a/h/l;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-wide v1, v0, Lcom/discord/rtcconnection/RtcConnection;->B:J

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v1, :cond_0

    const/4 v2, 0x5

    new-instance v10, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v10

    move v4, p3

    invoke-direct/range {v3 .. v9}, Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v1, v2, v10}, Lf/a/h/u/a;->m(ILjava/lang/Object;)V

    :cond_0
    new-instance p3, Lf/a/h/m;

    invoke-direct {p3, p1, p2, p4}, Lf/a/h/m;-><init>(JZ)V

    invoke-virtual {v0, p3}, Lcom/discord/rtcconnection/RtcConnection;->j(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public onVideo(JLjava/lang/Integer;III)V
    .locals 13

    move-wide v0, p1

    move-object v2, p0

    iget-object v3, v2, Lf/a/h/l;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-wide v4, v3, Lcom/discord/rtcconnection/RtcConnection;->B:J

    cmp-long v6, v0, v4

    if-nez v6, :cond_0

    iget-object v4, v3, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v4, :cond_0

    new-instance v12, Lcom/discord/rtcconnection/socket/io/Payloads$Video;

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object v5, v12

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v5 .. v11}, Lcom/discord/rtcconnection/socket/io/Payloads$Video;-><init>(IIILjava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v5, 0xc

    invoke-virtual {v4, v5, v12}, Lf/a/h/u/a;->m(ILjava/lang/Object;)V

    :cond_0
    new-instance v4, Lf/a/h/n;

    move-object/from16 v5, p3

    invoke-direct {v4, p1, p2, v5}, Lf/a/h/n;-><init>(JLjava/lang/Integer;)V

    invoke-virtual {v3, v4}, Lcom/discord/rtcconnection/RtcConnection;->j(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
