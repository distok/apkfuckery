.class public final Lf/a/h/a;
.super Lx/m/c/k;
.source "RtcConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $isConnected:Ljava/lang/Boolean;

.field public final synthetic this$0:Lf/a/h/b;


# direct methods
.method public constructor <init>(Lf/a/h/b;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lf/a/h/a;->this$0:Lf/a/h/b;

    iput-object p2, p0, Lf/a/h/a;->$isConnected:Ljava/lang/Boolean;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lf/a/h/a;->this$0:Lf/a/h/b;

    iget-object v0, v0, Lf/a/h/b;->d:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, p0, Lf/a/h/a;->$isConnected:Ljava/lang/Boolean;

    const-string v2, "isConnected"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, v0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    const-wide/16 v1, 0x1388

    const/4 v3, 0x1

    const-string v4, "network detected online"

    invoke-virtual {v0, v1, v2, v4, v3}, Lf/a/h/u/a;->h(JLjava/lang/String;Z)V

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x3a98

    const/4 v3, 0x0

    const-string v4, "network detected offline"

    invoke-virtual {v0, v1, v2, v4, v3}, Lf/a/h/u/a;->h(JLjava/lang/String;Z)V

    :cond_1
    :goto_0
    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
