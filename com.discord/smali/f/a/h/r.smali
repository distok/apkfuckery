.class public final Lf/a/h/r;
.super Ljava/lang/Object;
.source "RtcConnection.kt"

# interfaces
.implements Lf/a/h/u/a$c;


# instance fields
.field public final synthetic a:Lcom/discord/rtcconnection/RtcConnection;


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/RtcConnection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "audioCodec"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "videoCodec"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v0, v0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->o(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(ZLjava/lang/Integer;Ljava/lang/String;)V
    .locals 9

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Disconnected from RTC server. wasFatal: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, " -- code: "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " -- reason: "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v2, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->i()V

    :cond_0
    iget-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz p1, :cond_1

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->v:Lf/a/h/l;

    invoke-interface {p1, v1}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->l(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;)V

    :cond_1
    const/16 p1, 0x3e8

    const/4 v1, 0x0

    if-nez p2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_3

    :goto_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->g:Lcom/discord/rtcconnection/RtcConnection$State;

    instance-of v3, v3, Lcom/discord/rtcconnection/RtcConnection$State$d;

    const/4 v4, 0x0

    if-nez v3, :cond_5

    invoke-virtual {v0, v2, p3}, Lcom/discord/rtcconnection/RtcConnection;->h(ZLjava/lang/String;)V

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lf/a/h/v/h;->a()V

    :cond_4
    iput-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    :cond_5
    iput v1, v0, Lcom/discord/rtcconnection/RtcConnection;->n:I

    iput-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->p:Ljava/lang/Long;

    new-instance v1, Lcom/discord/rtcconnection/RtcConnection$State$d;

    invoke-direct {v1, v2}, Lcom/discord/rtcconnection/RtcConnection$State$d;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    if-eqz v2, :cond_6

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    new-instance v2, Lf/a/h/o;

    invoke-direct {v2, v0}, Lf/a/h/o;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/networking/Backoff;->fail(Lkotlin/jvm/functions/Function0;)J

    move-result-wide v1

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Disconnect was not clean! Reason: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", code: "

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ". Reconnecting in "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-long p1, p1

    div-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " seconds."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    :cond_6
    return-void
.end method

.method public c(JII)V
    .locals 10

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-wide v1, v0, Lcom/discord/rtcconnection/RtcConnection;->B:J

    cmp-long v3, p1, v1

    if-eqz v3, :cond_1

    iget-object v4, v0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v4, :cond_1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iget-object p4, v0, Lcom/discord/rtcconnection/RtcConnection;->G:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Boolean;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    move v9, p4

    goto :goto_0

    :cond_0
    const/4 p4, 0x0

    const/4 v9, 0x0

    :goto_0
    move-wide v5, p1

    move v7, p3

    invoke-interface/range {v4 .. v9}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->m(JILjava/lang/Integer;Z)V

    :cond_1
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "mode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secretKey"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v0, v0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v0, :cond_0

    invoke-static {p2}, Lx/h/f;->toIntArray(Ljava/util/Collection;)[I

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->p(Ljava/lang/String;[I)V

    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 3

    const-string v0, "mediaSessionId"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iput-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->t:Ljava/lang/String;

    iget-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/rtcconnection/RtcConnection$b;

    invoke-interface {v1}, Lcom/discord/rtcconnection/RtcConnection$b;->onMediaSessionIdReceived()V

    sget-object v1, Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;->MEDIA_SESSION_JOINED:Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;

    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/discord/rtcconnection/RtcConnection;->i(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public f(J)V
    .locals 5

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->i:Ljava/util/LinkedList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->i:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_0

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->i:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_0
    const/16 v1, 0x1f4

    int-to-long v2, v1

    cmp-long v4, p1, v2

    if-lez v4, :cond_1

    iget v2, v0, Lcom/discord/rtcconnection/RtcConnection;->n:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/discord/rtcconnection/RtcConnection;->n:I

    :cond_1
    sget-object v2, Lcom/discord/rtcconnection/RtcConnection$Quality;->Companion:Lcom/discord/rtcconnection/RtcConnection$Quality$a;

    long-to-double p1, p1

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object p1, Lcom/discord/rtcconnection/RtcConnection$Quality;->UNKNOWN:Lcom/discord/rtcconnection/RtcConnection$Quality;

    goto :goto_0

    :cond_2
    const/16 v2, 0xfa

    int-to-double v2, v2

    cmpg-double v4, p1, v2

    if-gez v4, :cond_3

    sget-object p1, Lcom/discord/rtcconnection/RtcConnection$Quality;->FINE:Lcom/discord/rtcconnection/RtcConnection$Quality;

    goto :goto_0

    :cond_3
    int-to-double v1, v1

    cmpg-double v3, p1, v1

    if-gez v3, :cond_4

    sget-object p1, Lcom/discord/rtcconnection/RtcConnection$Quality;->AVERAGE:Lcom/discord/rtcconnection/RtcConnection$Quality;

    goto :goto_0

    :cond_4
    sget-object p1, Lcom/discord/rtcconnection/RtcConnection$Quality;->BAD:Lcom/discord/rtcconnection/RtcConnection$Quality;

    :goto_0
    new-instance p2, Lf/a/h/q;

    invoke-direct {p2, p1}, Lf/a/h/q;-><init>(Lcom/discord/rtcconnection/RtcConnection$Quality;)V

    invoke-virtual {v0, p2}, Lcom/discord/rtcconnection/RtcConnection;->j(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v1}, Lcom/discord/utilities/networking/Backoff;->cancel()V

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    const-string v2, "Connecting to RTC server "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v3, :cond_0

    iget-object v3, v3, Lf/a/h/u/a;->r:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/rtcconnection/RtcConnection$State$c;->a:Lcom/discord/rtcconnection/RtcConnection$State$c;

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    return-void
.end method

.method public i()V
    .locals 15

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->e:Lcom/discord/utilities/networking/Backoff;

    invoke-virtual {v1}, Lcom/discord/utilities/networking/Backoff;->cancel()V

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    iget-object v2, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    const-string v3, "Connected to RTC server."

    invoke-virtual {v1, v3, v2}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->j:Lf/a/h/u/a;

    if-eqz v1, :cond_1

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->A:Ljava/lang/String;

    iget-wide v4, v0, Lcom/discord/rtcconnection/RtcConnection;->B:J

    iget-object v6, v0, Lcom/discord/rtcconnection/RtcConnection;->y:Ljava/lang/String;

    iget-boolean v8, v0, Lcom/discord/rtcconnection/RtcConnection;->z:Z

    const-string v2, "serverId"

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "sessionId"

    invoke-static {v6, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v2, v1, Lf/a/h/u/a;->q:Z

    if-eqz v2, :cond_0

    iget-object v9, v1, Lf/a/h/u/a;->u:Lcom/discord/utilities/logging/Logger;

    iget-object v10, v1, Lf/a/h/u/a;->a:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    const-string v11, "identify called on canceled instance of RtcControlSocket"

    invoke-static/range {v9 .. v14}, Lcom/discord/utilities/logging/Logger;->w$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iput-object v3, v1, Lf/a/h/u/a;->f:Ljava/lang/String;

    iput-object v6, v1, Lf/a/h/u/a;->g:Ljava/lang/String;

    sget-object v2, Lf/a/h/u/a$b;->f:Lf/a/h/u/a$b;

    iput-object v2, v1, Lf/a/h/u/a;->i:Lf/a/h/u/a$b;

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/rtcconnection/socket/io/Payloads$Identify;

    iget-object v7, v1, Lf/a/h/u/a;->s:Ljava/lang/String;

    move-object v2, v10

    invoke-direct/range {v2 .. v8}, Lcom/discord/rtcconnection/socket/io/Payloads$Identify;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v9, v10}, Lf/a/h/u/a;->m(ILjava/lang/Object;)V

    :cond_1
    :goto_0
    sget-object v1, Lcom/discord/rtcconnection/RtcConnection$State$a;->a:Lcom/discord/rtcconnection/RtcConnection$State$a;

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    return-void
.end method

.method public j(J)V
    .locals 3

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    instance-of v1, v1, Lcom/discord/rtcconnection/RtcConnection$c$a;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->d:Lf/a/h/v/h;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lf/a/h/v/h;->g:Lf/a/h/v/j;

    if-eqz v1, :cond_0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lf/a/h/v/j;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/rtcconnection/RtcConnection;->f(JLjava/util/Map;)V

    :cond_0
    return-void
.end method

.method public k(IILjava/lang/String;)V
    .locals 8

    const-string v0, "ip"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-object v1, v0, Lcom/discord/rtcconnection/RtcConnection;->D:Lcom/discord/utilities/logging/Logger;

    const-string v2, "Discovered dedicated UDP server on port "

    invoke-static {v2, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/discord/rtcconnection/RtcConnection;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/discord/utilities/logging/Logger;->recordBreadcrumb(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/rtcconnection/RtcConnection$State$g;->a:Lcom/discord/rtcconnection/RtcConnection$State$g;

    invoke-virtual {v0, v1}, Lcom/discord/rtcconnection/RtcConnection;->n(Lcom/discord/rtcconnection/RtcConnection$State;)V

    iget-object v2, v0, Lcom/discord/rtcconnection/RtcConnection;->C:Lcom/discord/rtcconnection/mediaengine/MediaEngine;

    iget-wide v3, v0, Lcom/discord/rtcconnection/RtcConnection;->B:J

    new-instance v5, Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;

    invoke-direct {v5, p2, p3, p1}, Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;-><init>(ILjava/lang/String;I)V

    iget-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->F:Lcom/discord/rtcconnection/RtcConnection$c;

    sget-object p2, Lcom/discord/rtcconnection/RtcConnection$c$a;->a:Lcom/discord/rtcconnection/RtcConnection$c$a;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;->DEFAULT:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    :goto_0
    move-object v6, p1

    goto :goto_1

    :cond_0
    instance-of p1, p1, Lcom/discord/rtcconnection/RtcConnection$c$b;

    if-eqz p1, :cond_2

    sget-object p1, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;->STREAM:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;

    goto :goto_0

    :goto_1
    new-instance v7, Lf/a/h/p;

    invoke-direct {v7, v0}, Lf/a/h/p;-><init>(Lcom/discord/rtcconnection/RtcConnection;)V

    invoke-interface/range {v2 .. v7}, Lcom/discord/rtcconnection/mediaengine/MediaEngine;->g(JLcom/discord/rtcconnection/mediaengine/MediaEngine$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;Lkotlin/jvm/functions/Function1;)Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p2, v0, Lcom/discord/rtcconnection/RtcConnection;->v:Lf/a/h/l;

    invoke-interface {p1, p2}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->j(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$b;)V

    iput-object p1, v0, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    :cond_1
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public onSpeaking(JIZ)V
    .locals 9

    iget-object p4, p0, Lf/a/h/r;->a:Lcom/discord/rtcconnection/RtcConnection;

    iget-wide v0, p4, Lcom/discord/rtcconnection/RtcConnection;->B:J

    cmp-long v2, p1, v0

    if-eqz v2, :cond_1

    iget-object v3, p4, Lcom/discord/rtcconnection/RtcConnection;->k:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;

    if-eqz v3, :cond_1

    iget-object p4, p4, Lcom/discord/rtcconnection/RtcConnection;->G:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Boolean;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    move v8, p4

    goto :goto_0

    :cond_0
    const/4 p4, 0x0

    const/4 v8, 0x0

    :goto_0
    const/4 v7, 0x0

    move-wide v4, p1

    move v6, p3

    invoke-interface/range {v3 .. v8}, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;->m(JILjava/lang/Integer;Z)V

    :cond_1
    return-void
.end method
