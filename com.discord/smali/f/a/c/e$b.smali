.class public abstract Lf/a/c/e$b;
.super Ljava/lang/Object;
.source "RemoteIntentService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/c/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/Looper;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callbackLooper"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/c/e$b;->b:Ljava/lang/String;

    new-instance p1, Landroid/os/Handler;

    new-instance v0, Lf/a/c/e$b$a;

    invoke-direct {v0, p0}, Lf/a/c/e$b$a;-><init>(Lf/a/c/e$b;)V

    invoke-direct {p1, p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lf/a/c/e$b;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Throwable;)V
.end method

.method public abstract b(Landroid/os/Bundle;)V
.end method
