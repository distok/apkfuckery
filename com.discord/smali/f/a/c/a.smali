.class public final Lf/a/c/a;
.super Lx/m/c/k;
.source "HardwareSurveyService.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lx/d<",
        "+",
        "Lcom/discord/hardware_analytics/DecoderCountInfo;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $appContext:Landroid/content/Context;

.field public final synthetic $callbackLooper:Landroid/os/Looper;

.field public final synthetic $onComplete:Lkotlin/jvm/functions/Function1;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lkotlin/jvm/functions/Function1;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lf/a/c/a;->$callbackLooper:Landroid/os/Looper;

    iput-object p2, p0, Lf/a/c/a;->$onComplete:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lf/a/c/a;->$appContext:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    check-cast p1, Lx/d;

    invoke-virtual {p1}, Lx/d;->unbox-impl()Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lf/a/c/a;->$callbackLooper:Landroid/os/Looper;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/discord/utilities/logging/LoggingProvider;->INSTANCE:Lcom/discord/utilities/logging/LoggingProvider;

    invoke-virtual {v0}, Lcom/discord/utilities/logging/LoggingProvider;->get()Lcom/discord/utilities/logging/Logger;

    move-result-object v1

    instance-of v0, p1, Lx/d$a;

    xor-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-static {p1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    check-cast p1, Lcom/discord/hardware_analytics/DecoderCountInfo;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Completed hardware survey: version=1, result="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v2, "HardwareSurveyService"

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->d$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    iget-object v0, p0, Lf/a/c/a;->$onComplete:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/discord/hardware_analytics/HardwareSurveyService;->f:Lcom/discord/hardware_analytics/HardwareSurveyService$a;

    iget-object v0, p0, Lf/a/c/a;->$appContext:Landroid/content/Context;

    const-string v2, "appContext"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;

    invoke-direct {v2, v0}, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;-><init>(Landroid/content/Context;)V

    iget-object v0, v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;->a:Landroid/content/SharedPreferences;

    const/4 v3, -0x1

    const-string v4, "HardwareSurveyService.VERSION_KEY"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v2, v2, Lcom/discord/hardware_analytics/HardwareSurveyVersionCache;->a:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    const-string v4, "HardwareSurveyService.ATTEMPTS"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "Error running hardware survey: version = 1, "

    const-string v4, "err="

    invoke-static {v3, v4}, Lf/e/c/a/a;->L(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lx/d;->exceptionOrNull-impl(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "attemptCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p1, 0x1

    if-lt v0, p1, :cond_2

    const-string p1, "previous completed version = "

    invoke-static {p1, v0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-string p1, "no previous completed version"

    :goto_0
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    const-string v2, "HardwareSurveyService"

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_1
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
