.class public final Lf/a/c/f/a$a;
.super Ljava/lang/Object;
.source "DecoderCountUtil.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/c/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

.field public final b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

.field public final c:F

.field public final d:Z


# direct methods
.method public constructor <init>(Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZ)V
    .locals 1

    const-string/jumbo v0, "videoRes"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/c/f/a$a;->a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    iput-object p2, p0, Lf/a/c/f/a$a;->b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    iput p3, p0, Lf/a/c/f/a$a;->c:F

    iput-boolean p4, p0, Lf/a/c/f/a$a;->d:Z

    return-void
.end method

.method public static a(Lf/a/c/f/a$a;Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZI)Lf/a/c/f/a$a;
    .locals 1

    and-int/lit8 p2, p5, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lf/a/c/f/a$a;->a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    :cond_0
    and-int/lit8 p2, p5, 0x2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lf/a/c/f/a$a;->b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_2

    iget p3, p0, Lf/a/c/f/a$a;->c:F

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lf/a/c/f/a$a;->d:Z

    :cond_3
    const-string/jumbo p0, "videoRes"

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "mimeType"

    invoke-static {p2, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Lf/a/c/f/a$a;

    invoke-direct {p0, p1, p2, p3, p4}, Lf/a/c/f/a$a;-><init>(Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZ)V

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lf/a/c/f/a$a;

    if-eqz v0, :cond_0

    check-cast p1, Lf/a/c/f/a$a;

    iget-object v0, p0, Lf/a/c/f/a$a;->a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    iget-object v1, p1, Lf/a/c/f/a$a;->a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/a/c/f/a$a;->b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    iget-object v1, p1, Lf/a/c/f/a$a;->b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lf/a/c/f/a$a;->c:F

    iget v1, p1, Lf/a/c/f/a$a;->c:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lf/a/c/f/a$a;->d:Z

    iget-boolean p1, p1, Lf/a/c/f/a$a;->d:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lf/a/c/f/a$a;->a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lf/a/c/f/a$a;->b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lf/a/c/f/a$a;->c:F

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget-boolean v1, p0, Lf/a/c/f/a$a;->d:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "TestDef(videoRes="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/a/c/f/a$a;->a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mimeType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/a/c/f/a$a;->b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", frameRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lf/a/c/f/a$a;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", requireHardwareAcceleration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/a/c/f/a$a;->d:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
