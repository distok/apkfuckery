.class public final Lf/a/c/f/a;
.super Ljava/lang/Object;
.source "DecoderCountUtil.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/c/f/a$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/a/c/f/a$a;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lf/a/c/f/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 20

    new-instance v6, Lf/a/c/f/a$a;

    sget-object v7, Lcom/discord/hardware_analytics/media_codec/VideoRes;->RESOLUTION_1080:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    sget-object v0, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->H264:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    const/high16 v1, 0x41f00000    # 30.0f

    const/4 v8, 0x1

    invoke-direct {v6, v7, v0, v1, v8}, Lf/a/c/f/a$a;-><init>(Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZ)V

    sget-object v9, Lcom/discord/hardware_analytics/media_codec/VideoRes;->RESOLUTION_720:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v5, 0xe

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v0, v6

    move-object v1, v9

    move-object v2, v13

    move v3, v14

    move v4, v15

    invoke-static/range {v0 .. v5}, Lf/a/c/f/a$a;->a(Lf/a/c/f/a$a;Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZI)Lf/a/c/f/a$a;

    move-result-object v16

    const/4 v5, 0x6

    move-object v1, v7

    invoke-static/range {v0 .. v5}, Lf/a/c/f/a$a;->a(Lf/a/c/f/a$a;Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZI)Lf/a/c/f/a$a;

    move-result-object v7

    sget-object v17, Lcom/discord/hardware_analytics/media_codec/VideoRes;->RESOLUTION_480:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    const/16 v5, 0xe

    move-object/from16 v1, v17

    invoke-static/range {v0 .. v5}, Lf/a/c/f/a$a;->a(Lf/a/c/f/a$a;Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZI)Lf/a/c/f/a$a;

    move-result-object v18

    const/16 v19, 0x6

    const/4 v5, 0x6

    move-object v1, v9

    invoke-static/range {v0 .. v5}, Lf/a/c/f/a$a;->a(Lf/a/c/f/a$a;Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZI)Lf/a/c/f/a$a;

    move-result-object v9

    move-object/from16 v1, v17

    move-object v2, v10

    move v3, v11

    move v4, v12

    move/from16 v5, v19

    invoke-static/range {v0 .. v5}, Lf/a/c/f/a$a;->a(Lf/a/c/f/a$a;Lcom/discord/hardware_analytics/media_codec/VideoRes;Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;FZI)Lf/a/c/f/a$a;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Lf/a/c/f/a$a;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    aput-object v16, v1, v8

    const/4 v2, 0x2

    aput-object v7, v1, v2

    const/4 v2, 0x3

    aput-object v18, v1, v2

    const/4 v2, 0x4

    aput-object v9, v1, v2

    const/4 v2, 0x5

    aput-object v0, v1, v2

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lf/a/c/f/a;->a:Ljava/util/List;

    return-void
.end method

.method public static final a(Lf/a/c/f/a$a;I)I
    .locals 9

    iget-object v0, p0, Lf/a/c/f/a$a;->b:Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;

    iget-object v1, p0, Lf/a/c/f/a$a;->a:Lcom/discord/hardware_analytics/media_codec/VideoRes;

    iget v2, p0, Lf/a/c/f/a$a;->c:F

    const-string/jumbo v3, "videoCodecMimeType"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "videoRes"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/hardware_analytics/media_codec/VideoCodecMimeType;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/discord/hardware_analytics/media_codec/VideoRes;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Lcom/discord/hardware_analytics/media_codec/VideoRes;->getHeight()I

    move-result v1

    invoke-static {v0, v3, v1}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    const-string v1, "frame-rate"

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setFloat(Ljava/lang/String;F)V

    const-string v1, "MediaFormat.createVideoF\u2026t.KEY_BIT_RATE, it) }\n  }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p0, p0, Lf/a/c/f/a$a;->d:Z

    const-string v1, "mediaFormat"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x1

    if-ge v3, p1, :cond_2

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    :try_start_0
    new-instance v6, Lf/a/c/f/e;

    invoke-direct {v6, v0}, Lf/a/c/f/e;-><init>(Landroid/media/MediaFormat;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to create VideoDecoder: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "VideoDecoder"

    invoke-static {v7, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    :goto_1
    if-eqz v6, :cond_0

    iget-object v7, v6, Lf/a/c/f/e;->c:Lf/a/c/f/b;

    iget-boolean v7, v7, Lf/a/c/f/b;->b:Z

    if-nez v7, :cond_0

    if-eqz p0, :cond_0

    iget-boolean v7, v6, Lf/a/c/f/e;->e:Z

    if-nez v7, :cond_1

    iput-boolean v4, v6, Lf/a/c/f/e;->e:Z

    iget-object v6, v6, Lf/a/c/f/e;->d:Landroid/media/MediaCodec;

    invoke-virtual {v6}, Landroid/media/MediaCodec;->release()V

    goto :goto_2

    :cond_0
    move-object v5, v6

    :cond_1
    :goto_2
    if-eqz v5, :cond_2

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/a/c/f/e;

    iget-boolean v0, p1, Lf/a/c/f/e;->e:Z

    if-nez v0, :cond_3

    iput-boolean v4, p1, Lf/a/c/f/e;->e:Z

    iget-object p1, p1, Lf/a/c/f/e;->d:Landroid/media/MediaCodec;

    invoke-virtual {p1}, Landroid/media/MediaCodec;->release()V

    goto :goto_3

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p0

    return p0
.end method
