.class public final Lf/a/n/j0/h;
.super Lx/m/c/k;
.source "StickerView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/discord/utilities/file/DownloadUtils$DownloadState;",
        "+",
        "Ljava/lang/Integer;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $localAnimationSettings:Ljava/lang/Integer;

.field public final synthetic $sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field public final synthetic this$0:Lcom/discord/views/sticker/StickerView;


# direct methods
.method public constructor <init>(Lcom/discord/views/sticker/StickerView;Ljava/lang/Integer;Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 0

    iput-object p1, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    iput-object p2, p0, Lf/a/n/j0/h;->$localAnimationSettings:Ljava/lang/Integer;

    iput-object p3, p0, Lf/a/n/j0/h;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/file/DownloadUtils$DownloadState;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    instance-of v1, v0, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;

    if-nez v1, :cond_0

    goto/16 :goto_4

    :cond_0
    iget-object v1, p0, Lf/a/n/j0/h;->$localAnimationSettings:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    move-object p1, v1

    :cond_1
    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->LOOP:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    goto :goto_1

    :cond_3
    :goto_0
    sget-object v1, Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;->FREEZE:Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;

    :goto_1
    iget-object v2, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {v2}, Lcom/discord/views/sticker/StickerView;->b(Lcom/discord/views/sticker/StickerView;)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {v2}, Lcom/discord/views/sticker/StickerView;->a(Lcom/discord/views/sticker/StickerView;)Lcom/discord/rlottie/RLottieImageView;

    move-result-object v2

    iget-object v3, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    iget-object v4, p0, Lf/a/n/j0/h;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v3, v4}, Lcom/discord/views/sticker/StickerView;->f(Lcom/discord/models/sticker/dto/ModelSticker;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {v2}, Lcom/discord/views/sticker/StickerView;->a(Lcom/discord/views/sticker/StickerView;)Lcom/discord/rlottie/RLottieImageView;

    move-result-object v2

    iget-object v3, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "context"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;

    invoke-virtual {v0}, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;->getFile()Ljava/io/File;

    move-result-object v6

    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {v0}, Lcom/discord/utilities/dsti/StickerUtils;->getDEFAULT_STICKER_SIZE_PX()I

    move-result v7

    invoke-virtual {v0}, Lcom/discord/utilities/dsti/StickerUtils;->getDEFAULT_STICKER_SIZE_PX()I

    move-result v8

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "file"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "playbackMode"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/rlottie/RLottieDrawable;

    invoke-static {v3}, Lcom/discord/utilities/display/DisplayUtils;->getScreenRefreshRate(Landroid/content/Context;)F

    move-result v11

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v12, 0x0

    const/16 v13, 0x40

    move-object v5, v0

    invoke-direct/range {v5 .. v13}, Lcom/discord/rlottie/RLottieDrawable;-><init>(Ljava/io/File;IIZZF[II)V

    iput-object v0, v2, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    invoke-virtual {v0, v1}, Lcom/discord/rlottie/RLottieDrawable;->f(Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;)V

    iget-object v0, v2, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    invoke-virtual {v0, v1}, Lcom/discord/rlottie/RLottieDrawable;->e(Z)V

    :cond_4
    iget-object v0, v2, Lcom/discord/rlottie/RLottieImageView;->d:Lcom/discord/rlottie/RLottieDrawable;

    invoke-virtual {v2, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-nez p1, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_6

    iget-object p1, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {p1}, Lcom/discord/views/sticker/StickerView;->a(Lcom/discord/views/sticker/StickerView;)Lcom/discord/rlottie/RLottieImageView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/rlottie/RLottieImageView;->a()V

    goto :goto_4

    :cond_6
    :goto_2
    if-nez p1, :cond_7

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_8

    iget-object p1, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {p1}, Lcom/discord/views/sticker/StickerView;->a(Lcom/discord/views/sticker/StickerView;)Lcom/discord/rlottie/RLottieImageView;

    move-result-object p1

    new-instance v0, Lf/a/n/j0/g;

    invoke-direct {v0, p0}, Lf/a/n/j0/g;-><init>(Lf/a/n/j0/h;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_4

    :cond_8
    :goto_3
    const/4 v0, 0x2

    if-nez p1, :cond_9

    goto :goto_4

    :cond_9
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v0, :cond_a

    iget-object p1, p0, Lf/a/n/j0/h;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {p1}, Lcom/discord/views/sticker/StickerView;->a(Lcom/discord/views/sticker/StickerView;)Lcom/discord/rlottie/RLottieImageView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_a
    :goto_4
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
