.class public final Lf/a/n/j0/d;
.super Lx/m/c/k;
.source "StickerView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/discord/utilities/file/DownloadUtils$DownloadState;",
        "+",
        "Ljava/lang/Integer;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $localAnimationSettings:Ljava/lang/Integer;

.field public final synthetic $sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field public final synthetic this$0:Lcom/discord/views/sticker/StickerView;


# direct methods
.method public constructor <init>(Lcom/discord/views/sticker/StickerView;Ljava/lang/Integer;Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 0

    iput-object p1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    iput-object p2, p0, Lf/a/n/j0/d;->$localAnimationSettings:Ljava/lang/Integer;

    iput-object p3, p0, Lf/a/n/j0/d;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/file/DownloadUtils$DownloadState;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    instance-of v1, v0, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;

    if-nez v1, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v1, p0, Lf/a/n/j0/d;->$localAnimationSettings:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    move-object p1, v1

    :cond_1
    iget-object v1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {v1}, Lcom/discord/views/sticker/StickerView;->b(Lcom/discord/views/sticker/StickerView;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {v1}, Lcom/discord/views/sticker/StickerView;->c(Lcom/discord/views/sticker/StickerView;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    iget-object v3, p0, Lf/a/n/j0/d;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v2, v3}, Lcom/discord/views/sticker/StickerView;->f(Lcom/discord/models/sticker/dto/ModelSticker;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_3

    iget-object p1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    check-cast v0, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;

    invoke-virtual {v0}, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/discord/views/sticker/StickerView;->d(Lcom/discord/views/sticker/StickerView;Ljava/io/File;Z)Lkotlinx/coroutines/Job;

    move-result-object v0

    iput-object v0, p1, Lcom/discord/views/sticker/StickerView;->i:Lkotlinx/coroutines/Job;

    goto/16 :goto_2

    :cond_3
    :goto_0
    if-nez p1, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_5

    iget-object p1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {p1}, Lcom/discord/views/sticker/StickerView;->c(Lcom/discord/views/sticker/StickerView;)Landroid/widget/ImageView;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    iget-object v3, p0, Lf/a/n/j0/d;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 v4, 0x0

    const/4 p1, 0x0

    const/4 v6, 0x2

    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/utilities/dsti/StickerUtils;->getCDNAssetUrl$default(Lcom/discord/utilities/dsti/StickerUtils;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v9, 0x7c

    const/4 v10, 0x0

    move v5, p1

    move-object v7, v8

    move v8, v9

    move-object v9, v10

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iget-object p1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {p1}, Lcom/discord/views/sticker/StickerView;->c(Lcom/discord/views/sticker/StickerView;)Landroid/widget/ImageView;

    move-result-object p1

    new-instance v1, Lf/a/n/j0/c;

    invoke-direct {v1, p0, v0}, Lf/a/n/j0/c;-><init>(Lf/a/n/j0/d;Lcom/discord/utilities/file/DownloadUtils$DownloadState;)V

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_2

    :cond_5
    :goto_1
    const/4 v0, 0x2

    if-nez p1, :cond_6

    goto :goto_2

    :cond_6
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v0, :cond_7

    iget-object p1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {p1}, Lcom/discord/views/sticker/StickerView;->c(Lcom/discord/views/sticker/StickerView;)Landroid/widget/ImageView;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    iget-object v2, p0, Lf/a/n/j0/d;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    const/4 v3, 0x0

    const/4 p1, 0x0

    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/dsti/StickerUtils;->getCDNAssetUrl$default(Lcom/discord/utilities/dsti/StickerUtils;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v8, 0x7c

    const/4 v9, 0x0

    move v4, p1

    move-object v6, v7

    move v7, v8

    move-object v8, v9

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iget-object p1, p0, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    invoke-static {p1}, Lcom/discord/views/sticker/StickerView;->c(Lcom/discord/views/sticker/StickerView;)Landroid/widget/ImageView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_7
    :goto_2
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
