.class public final Lf/a/n/j0/c;
.super Ljava/lang/Object;
.source "StickerView.kt"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic d:Lf/a/n/j0/d;

.field public final synthetic e:Lcom/discord/utilities/file/DownloadUtils$DownloadState;


# direct methods
.method public constructor <init>(Lf/a/n/j0/d;Lcom/discord/utilities/file/DownloadUtils$DownloadState;)V
    .locals 0

    iput-object p1, p0, Lf/a/n/j0/c;->d:Lf/a/n/j0/d;

    iput-object p2, p0, Lf/a/n/j0/c;->e:Lcom/discord/utilities/file/DownloadUtils$DownloadState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 2

    iget-object p1, p0, Lf/a/n/j0/c;->d:Lf/a/n/j0/d;

    iget-object p1, p1, Lf/a/n/j0/d;->this$0:Lcom/discord/views/sticker/StickerView;

    iget-object v0, p0, Lf/a/n/j0/c;->e:Lcom/discord/utilities/file/DownloadUtils$DownloadState;

    check-cast v0, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;

    invoke-virtual {v0}, Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;->getFile()Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/discord/views/sticker/StickerView;->d(Lcom/discord/views/sticker/StickerView;Ljava/io/File;Z)Lkotlinx/coroutines/Job;

    move-result-object v0

    iput-object v0, p1, Lcom/discord/views/sticker/StickerView;->i:Lkotlinx/coroutines/Job;

    return v1
.end method
