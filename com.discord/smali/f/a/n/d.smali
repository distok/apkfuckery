.class public final Lf/a/n/d;
.super Ljava/lang/Object;
.source "CodeVerificationView.kt"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field public final synthetic d:Lcom/discord/views/CodeVerificationView;


# direct methods
.method public constructor <init>(Lcom/discord/views/CodeVerificationView;)V
    .locals 0

    iput-object p1, p0, Lf/a/n/d;->d:Lcom/discord/views/CodeVerificationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    const-string p1, "event"

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 p2, 0x1

    if-nez p1, :cond_4

    iget-object p1, p0, Lf/a/n/d;->d:Lcom/discord/views/CodeVerificationView;

    sget-object v0, Lcom/discord/views/CodeVerificationView;->j:Lcom/discord/views/CodeVerificationView$a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x43

    if-ne v0, v1, :cond_2

    iget-object p3, p1, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p3

    const/4 v0, 0x0

    if-lez p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eqz p3, :cond_4

    iget-object p3, p1, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    const-string v1, "$this$dropLast"

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, p2

    if-gez v1, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    invoke-static {p3, v0}, Lx/s/u;->take(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p1, Lcom/discord/views/CodeVerificationView;->h:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/discord/views/CodeVerificationView;->d()V

    goto :goto_2

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    const/16 p3, 0x30

    invoke-virtual {p1, p3}, Lcom/discord/views/CodeVerificationView;->c(C)V

    goto :goto_2

    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result p3

    if-eqz p3, :cond_4

    int-to-char p3, p3

    invoke-virtual {p1, p3}, Lcom/discord/views/CodeVerificationView;->c(C)V

    :cond_4
    :goto_2
    return p2
.end method
