.class public final Lf/a/n/x;
.super Ljava/lang/Object;
.source "OverlayVoiceSelectorBubbleDialog.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple$OnUpdated;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple$OnUpdated<",
        "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/a/n/z;

.field public final synthetic b:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;


# direct methods
.method public constructor <init>(Lf/a/n/z;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;)V
    .locals 0

    iput-object p1, p0, Lf/a/n/x;->a:Lf/a/n/z;

    iput-object p2, p0, Lf/a/n/x;->b:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onUpdated(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "<anonymous parameter 1>"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/a/n/x;->a:Lf/a/n/z;

    iget-object p1, p1, Lf/a/n/z;->A:Ljava/lang/String;

    iget-object p2, p0, Lf/a/n/x;->b:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    invoke-virtual {p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;->getFilter()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/a/n/x;->a:Lf/a/n/z;

    invoke-static {p1}, Lf/a/n/z;->j(Lf/a/n/z;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    iget-object p1, p0, Lf/a/n/x;->a:Lf/a/n/z;

    iget-object p2, p0, Lf/a/n/x;->b:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    invoke-virtual {p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;->getFilter()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p1, Lf/a/n/z;->A:Ljava/lang/String;

    :cond_0
    return-void
.end method
