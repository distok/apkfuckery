.class public final Lf/a/n/m$a;
.super Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;
.source "OverlayMenuBubbleDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/n/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder<",
        "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/discord/views/VoiceUserView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    check-cast p1, Lcom/discord/views/VoiceUserView;

    iput-object p1, p0, Lf/a/n/m$a;->a:Lcom/discord/views/VoiceUserView;

    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/n/m$a;->a:Lcom/discord/views/VoiceUserView;

    const v1, 0x7f070064

    invoke-virtual {v0, p1, v1}, Lcom/discord/views/VoiceUserView;->a(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;I)V

    return-void
.end method
