.class public Lf/a/n/a0;
.super Landroid/widget/LinearLayout;
.source "ReactionView.java"


# instance fields
.field public d:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

.field public e:Landroid/widget/TextSwitcher;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;

.field public h:I

.field public i:Ljava/lang/Long;

.field public j:Lcom/discord/models/domain/ModelMessageReaction;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0d00dd

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    const v0, 0x7f0a0819

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iput-object v0, p0, Lf/a/n/a0;->d:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    const v0, 0x7f0a0818

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextSwitcher;

    iput-object v0, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    const v0, 0x7f0a0816

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lf/a/n/a0;->f:Landroid/widget/TextView;

    const v0, 0x7f0a0817

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lf/a/n/a0;->g:Landroid/widget/TextView;

    return-void
.end method

.method private setIsMe(Z)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setActivated(Z)V

    iget-object v0, p0, Lf/a/n/a0;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setActivated(Z)V

    iget-object v0, p0, Lf/a/n/a0;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setActivated(Z)V

    return-void
.end method


# virtual methods
.method public a(Lcom/discord/models/domain/ModelMessageReaction;JZ)V
    .locals 4

    iget-object v0, p0, Lf/a/n/a0;->i:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, v0, p2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction;->getCount()I

    move-result v1

    iget v2, p0, Lf/a/n/a0;->h:I

    if-ne v1, v2, :cond_2

    goto :goto_4

    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    invoke-virtual {v0}, Landroid/widget/TextSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lf/a/n/a0;->h:I

    if-le v1, v2, :cond_4

    iget-object v2, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    const v3, 0x7f010030

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/content/Context;I)V

    iget-object v2, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    const v3, 0x7f010032

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/content/Context;I)V

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    const v3, 0x7f01002f

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/content/Context;I)V

    iget-object v2, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    const v3, 0x7f010031

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/content/Context;I)V

    :goto_2
    iget-object v0, p0, Lf/a/n/a0;->e:Landroid/widget/TextSwitcher;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iput v1, p0, Lf/a/n/a0;->h:I

    :goto_4
    iget-object v0, p0, Lf/a/n/a0;->j:Lcom/discord/models/domain/ModelMessageReaction;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v0

    iget-object v1, p0, Lf/a/n/a0;->j:Lcom/discord/models/domain/ModelMessageReaction;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lf/a/n/a0;->d:Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    invoke-static {v0, v1, p4}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->renderEmoji(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/models/domain/ModelMessageReaction$Emoji;Z)V

    :cond_6
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction;->isMe()Z

    move-result p4

    invoke-direct {p0, p4}, Lf/a/n/a0;->setIsMe(Z)V

    iput-object p1, p0, Lf/a/n/a0;->j:Lcom/discord/models/domain/ModelMessageReaction;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lf/a/n/a0;->i:Ljava/lang/Long;

    return-void
.end method

.method public getReaction()Lcom/discord/models/domain/ModelMessageReaction;
    .locals 1

    iget-object v0, p0, Lf/a/n/a0;->j:Lcom/discord/models/domain/ModelMessageReaction;

    return-object v0
.end method
