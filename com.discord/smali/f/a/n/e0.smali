.class public final Lf/a/n/e0;
.super Lf/g/j/r/a;
.source "VoiceUserView.kt"


# instance fields
.field public final synthetic a:Lcom/discord/views/VoiceUserView$c;


# direct methods
.method public constructor <init>(Lcom/discord/views/VoiceUserView$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/a/n/e0;->a:Lcom/discord/views/VoiceUserView$c;

    invoke-direct {p0}, Lf/g/j/r/a;-><init>()V

    return-void
.end method


# virtual methods
.method public process(Landroid/graphics/Bitmap;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/a/n/e0;->a:Lcom/discord/views/VoiceUserView$c;

    iget-object v0, v0, Lcom/discord/views/VoiceUserView$c;->this$0:Lcom/discord/views/VoiceUserView;

    iget-object v0, v0, Lcom/discord/views/VoiceUserView;->i:Lkotlin/jvm/functions/Function1;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    const-string v1, "bitmap.copy(Bitmap.Config.ARGB_8888, false)"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
