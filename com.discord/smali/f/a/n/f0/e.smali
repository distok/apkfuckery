.class public final Lf/a/n/f0/e;
.super Ljava/lang/Object;
.source "RxRendererEvents.kt"

# interfaces
.implements Lorg/webrtc/RendererCommon$RendererEvents;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/n/f0/e$a;
    }
.end annotation


# instance fields
.field public final d:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lf/a/n/f0/e$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lf/a/n/f0/e;->d:Lrx/subjects/BehaviorSubject;

    return-void
.end method


# virtual methods
.method public onFirstFrameRendered()V
    .locals 0

    return-void
.end method

.method public onFrameResolutionChanged(III)V
    .locals 2

    iget-object v0, p0, Lf/a/n/f0/e;->d:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lf/a/n/f0/e$a;

    invoke-direct {v1, p1, p2, p3}, Lf/a/n/f0/e$a;-><init>(III)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
