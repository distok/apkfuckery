.class public final Lf/a/n/f0/h;
.super Lx/m/c/k;
.source "VideoCallParticipantView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/graphics/Bitmap;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $participant:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V
    .locals 0

    iput-object p1, p0, Lf/a/n/f0/h;->$participant:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Landroid/graphics/Bitmap;

    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    iget-object v1, p0, Lf/a/n/f0/h;->$participant:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->handleBitmap(JLandroid/graphics/Bitmap;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
