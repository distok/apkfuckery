.class public final Lf/a/n/f0/a;
.super Ljava/lang/Object;
.source "AppVideoStreamRenderer.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lkotlin/Unit;",
        "Lrx/Observable<",
        "+",
        "Landroid/graphics/Point;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/a/n/f0/e;


# direct methods
.method public constructor <init>(Lf/a/n/f0/e;)V
    .locals 0

    iput-object p1, p0, Lf/a/n/f0/a;->d:Lf/a/n/f0/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lkotlin/Unit;

    iget-object p1, p0, Lf/a/n/f0/a;->d:Lf/a/n/f0/e;

    const-wide/16 v0, 0x32

    iget-object p1, p1, Lf/a/n/f0/e;->d:Lrx/subjects/BehaviorSubject;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Lrx/Observable;->L(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lf/a/n/f0/f;->d:Lf/a/n/f0/f;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->H()Lrx/Observable;

    move-result-object p1

    const-string v0, "frameResolutionSubject\n \u2026  .onBackpressureLatest()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
