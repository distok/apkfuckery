.class public final Lf/a/i/a;
.super Lx/j/h/a/g;
.source "SamsungConnect.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Landroid/net/Uri;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.discord.samsung.SamsungConnect$getSamsungAuthorizeCallback$2"
    f = "SamsungConnect.kt"
    l = {}
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $okHttpClient:Lb0/y;

.field public final synthetic $url:Ljava/lang/String;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lb0/y;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lf/a/i/a;->$url:Ljava/lang/String;

    iput-object p2, p0, Lf/a/i/a;->$okHttpClient:Lb0/y;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/i/a;

    iget-object v1, p0, Lf/a/i/a;->$url:Ljava/lang/String;

    iget-object v2, p0, Lf/a/i/a;->$okHttpClient:Lb0/y;

    invoke-direct {v0, v1, v2, p2}, Lf/a/i/a;-><init>(Ljava/lang/String;Lb0/y;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lf/a/i/a;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p2, Lkotlin/coroutines/Continuation;

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/i/a;

    iget-object v1, p0, Lf/a/i/a;->$url:Ljava/lang/String;

    iget-object v2, p0, Lf/a/i/a;->$okHttpClient:Lb0/y;

    invoke-direct {v0, v1, v2, p2}, Lf/a/i/a;-><init>(Ljava/lang/String;Lb0/y;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lf/a/i/a;->p$:Lkotlinx/coroutines/CoroutineScope;

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {v0, p1}, Lf/a/i/a;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget v0, p0, Lf/a/i/a;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    new-instance p1, Lb0/a0$a;

    invoke-direct {p1}, Lb0/a0$a;-><init>()V

    const/4 v0, 0x0

    const-string v1, "GET"

    invoke-virtual {p1, v1, v0}, Lb0/a0$a;->c(Ljava/lang/String;Lokhttp3/RequestBody;)Lb0/a0$a;

    iget-object v0, p0, Lf/a/i/a;->$url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lb0/a0$a;->f(Ljava/lang/String;)Lb0/a0$a;

    invoke-virtual {p1}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object p1

    iget-object v0, p0, Lf/a/i/a;->$okHttpClient:Lb0/y;

    invoke-virtual {v0, p1}, Lb0/y;->b(Lb0/a0;)Lb0/e;

    move-result-object p1

    check-cast p1, Lb0/g0/g/e;

    invoke-virtual {p1}, Lb0/g0/g/e;->execute()Lokhttp3/Response;

    move-result-object p1

    sget-object v0, Lcom/discord/samsung/SamsungConnect;->a:Lcom/discord/samsung/SamsungConnect;

    invoke-static {v0, p1}, Lcom/discord/samsung/SamsungConnect;->a(Lcom/discord/samsung/SamsungConnect;Lokhttp3/Response;)Landroid/net/Uri;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
