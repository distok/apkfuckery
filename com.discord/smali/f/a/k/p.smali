.class public final synthetic Lf/a/k/p;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# static fields
.field public static final synthetic d:Lf/a/k/p;


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/k/p;

    invoke-direct {v0}, Lf/a/k/p;-><init>()V

    sput-object v0, Lf/a/k/p;->d:Lf/a/k/p;

    return-void
.end method

.method public synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lkotlin/Pair;

    new-instance v0, Lcom/discord/models/domain/ModelUser$Speaking;

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Lcom/discord/models/domain/ModelUser$Speaking;-><init>(JZ)V

    return-object v0
.end method
