.class public final synthetic Lf/a/k/q;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# static fields
.field public static final synthetic d:Lf/a/k/q;


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/k/q;

    invoke-direct {v0}, Lf/a/k/q;-><init>()V

    sput-object v0, Lf/a/k/q;->d:Lf/a/k/q;

    return-void
.end method

.method public synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lcom/discord/rtcconnection/RtcConnection$State;

    instance-of p1, p1, Lcom/discord/rtcconnection/RtcConnection$State$f;

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreRtcConnection;->getSpeakingUpdates()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lf/a/k/p;->d:Lf/a/k/p;

    invoke-virtual {p1, v0}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-wide/16 v0, 0x12c

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Lrx/Observable;->a(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lg0/l/a/g;->e:Lrx/Observable;

    :goto_0
    return-object p1
.end method
