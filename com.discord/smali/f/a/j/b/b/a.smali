.class public final Lf/a/j/b/b/a;
.super Ljava/lang/Object;
.source "SimpleMarkdownRules.kt"


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Ljava/util/regex/Pattern;

.field public static final c:Ljava/util/regex/Pattern;

.field public static final d:Ljava/util/regex/Pattern;

.field public static final e:Ljava/util/regex/Pattern;

.field public static final f:Ljava/util/regex/Pattern;

.field public static final g:Ljava/util/regex/Pattern;

.field public static final h:Lf/a/j/b/b/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/j/b/b/a;

    invoke-direct {v0}, Lf/a/j/b/b/a;-><init>()V

    sput-object v0, Lf/a/j/b/b/a;->h:Lf/a/j/b/b/a;

    const-string v0, "^\\*\\*([\\s\\S]+?)\\*\\*(?!\\*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/b/b/a;->a:Ljava/util/regex/Pattern;

    const-string v0, "^__([\\s\\S]+?)__(?!_)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/b/b/a;->b:Ljava/util/regex/Pattern;

    const-string v0, "^~~(?=\\S)([\\s\\S]*?\\S)~~"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/b/b/a;->c:Ljava/util/regex/Pattern;

    const-string v0, "^(?:\\n *)*\\n"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/b/b/a;->d:Ljava/util/regex/Pattern;

    const-string v0, "^[\\s\\S]+?(?=[^0-9A-Za-z\\s\\u00c0-\\uffff]|\\n| {2,}\\n|\\w+:\\S|$)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/b/b/a;->e:Ljava/util/regex/Pattern;

    const-string v0, "^\\\\([^0-9A-Za-z\\s])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/b/b/a;->f:Ljava/util/regex/Pattern;

    const-string v0, "^\\b_((?:__|\\\\[\\s\\S]|[^\\\\_])+?)_\\b|^\\*(?=\\S)((?:\\*\\*|\\s+(?:[^*\\s]|\\*\\*)|[^\\s*])+?)\\*(?!\\*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/b/b/a;->g:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final b(Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(Z)",
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TR;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lf/a/j/b/b/a;->h:Lf/a/j/b/b/a;

    new-instance v2, Lf/a/j/b/b/b;

    sget-object v3, Lf/a/j/b/b/a;->f:Ljava/util/regex/Pattern;

    const-string v4, "PATTERN_ESCAPE"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1, v3}, Lf/a/j/b/b/b;-><init>(Lf/a/j/b/b/a;Ljava/util/regex/Pattern;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/a/j/b/b/d;

    sget-object v3, Lf/a/j/b/b/a;->d:Ljava/util/regex/Pattern;

    const-string v4, "PATTERN_NEWLINE"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1, v3}, Lf/a/j/b/b/d;-><init>(Lf/a/j/b/b/a;Ljava/util/regex/Pattern;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lf/a/j/b/b/a;->a()Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lf/a/j/b/b/a;->b:Ljava/util/regex/Pattern;

    const-string v3, "PATTERN_UNDERLINE"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lf/a/j/b/b/f;->d:Lf/a/j/b/b/f;

    invoke-static {v2, v3}, Lf/a/j/b/b/a;->d(Ljava/util/regex/Pattern;Lkotlin/jvm/functions/Function0;)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/a/j/b/b/c;

    sget-object v3, Lf/a/j/b/b/a;->g:Ljava/util/regex/Pattern;

    const-string v4, "PATTERN_ITALICS"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1, v3}, Lf/a/j/b/b/c;-><init>(Lf/a/j/b/b/a;Ljava/util/regex/Pattern;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lf/a/j/b/b/a;->c:Ljava/util/regex/Pattern;

    const-string v3, "PATTERN_STRIKETHRU"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lf/a/j/b/b/e;->d:Lf/a/j/b/b/e;

    invoke-static {v2, v3}, Lf/a/j/b/b/a;->d(Ljava/util/regex/Pattern;Lkotlin/jvm/functions/Function0;)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lf/a/j/b/b/a;->e()Lcom/discord/simpleast/core/parser/Rule;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public static synthetic c(ZI)Ljava/util/List;
    .locals 1

    const/4 v0, 0x1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    const/4 p0, 0x1

    :cond_0
    invoke-static {p0}, Lf/a/j/b/b/a;->b(Z)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final d(Ljava/util/regex/Pattern;Lkotlin/jvm/functions/Function0;)Lcom/discord/simpleast/core/parser/Rule;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/regex/Pattern;",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Ljava/util/List<",
            "+",
            "Landroid/text/style/CharacterStyle;",
            ">;>;)",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TR;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;"
        }
    .end annotation

    const-string v0, "pattern"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "styleFactory"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/j/b/b/a$b;

    invoke-direct {v0, p1, p0, p0}, Lf/a/j/b/b/a$b;-><init>(Lkotlin/jvm/functions/Function0;Ljava/util/regex/Pattern;Ljava/util/regex/Pattern;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/discord/simpleast/core/parser/Rule;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TR;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;"
        }
    .end annotation

    sget-object v0, Lf/a/j/b/b/a;->a:Ljava/util/regex/Pattern;

    const-string v1, "PATTERN_BOLD"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lf/a/j/b/b/a$a;->d:Lf/a/j/b/b/a$a;

    invoke-static {v0, v1}, Lf/a/j/b/b/a;->d(Ljava/util/regex/Pattern;Lkotlin/jvm/functions/Function0;)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/discord/simpleast/core/parser/Rule;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TR;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;"
        }
    .end annotation

    new-instance v0, Lf/a/j/b/b/a$c;

    sget-object v1, Lf/a/j/b/b/a;->e:Ljava/util/regex/Pattern;

    const-string v2, "PATTERN_TEXT"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lf/a/j/b/b/a$c;-><init>(Lf/a/j/b/b/a;Ljava/util/regex/Pattern;)V

    return-object v0
.end method
