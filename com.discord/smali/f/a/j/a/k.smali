.class public final Lf/a/j/a/k;
.super Ljava/lang/Object;
.source "Xml.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/j/a/k$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Ljava/util/regex/Pattern;

.field public static final c:Lf/a/j/a/k;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lf/a/j/a/k;

    invoke-direct {v0}, Lf/a/j/a/k;-><init>()V

    sput-object v0, Lf/a/j/a/k;->c:Lf/a/j/a/k;

    const-string v0, "^<!--[\\s\\S]*?-->"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v2, "Pattern.compile(\"\"\"^<!--\u2026*?-->\"\"\", Pattern.DOTALL)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/a/j/a/k;->a:Ljava/util/regex/Pattern;

    const-string v0, "^<([\\s\\S]+?)(?:>(.*?)<\\/([\\s\\S]+?))?>"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "Pattern.compile(\n      \"\u2026?))?>\"\"\", Pattern.DOTALL)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/a/j/a/k;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
