.class public final Lf/a/j/a/i$b;
.super Lcom/discord/simpleast/core/node/Node$a;
.source "Kotlin.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/j/a/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/j/a/i$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RC:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/discord/simpleast/core/node/Node$a<",
        "TRC;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Lf/a/j/a/i$b$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lf/a/j/a/i$b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/a/j/a/i$b$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lf/a/j/a/i$b;->b:Lf/a/j/a/i$b$a;

    sget-object v0, Lx/s/f;->f:Lx/s/f;

    new-instance v1, Lkotlin/text/Regex;

    const-string v2, "^(fun)( *<.*>)?( \\w+)( *\\(.*?\\))"

    invoke-direct {v1, v2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    invoke-virtual {v1}, Lkotlin/text/Regex;->toPattern()Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/a/i$b;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/a/j/a/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lf/a/j/a/f<",
            "TRC;>;)V"
        }
    .end annotation

    const-string v0, "pre"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "params"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "codeStyleProviders"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/discord/simpleast/core/node/Node;

    new-instance v1, Lcom/discord/simpleast/core/node/StyleNode$b;

    iget-object v2, p5, Lf/a/j/a/f;->d:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {v1, p1, v2}, Lcom/discord/simpleast/core/node/StyleNode$b;-><init>(Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    const/4 p1, 0x0

    aput-object v1, v0, p1

    if-eqz p2, :cond_0

    new-instance p1, Lcom/discord/simpleast/core/node/StyleNode$b;

    iget-object v1, p5, Lf/a/j/a/f;->g:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {p1, p2, v1}, Lcom/discord/simpleast/core/node/StyleNode$b;-><init>(Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const/4 p2, 0x1

    aput-object p1, v0, p2

    const/4 p1, 0x2

    new-instance p2, Lcom/discord/simpleast/core/node/StyleNode$b;

    iget-object v1, p5, Lf/a/j/a/f;->e:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {p2, p3, v1}, Lcom/discord/simpleast/core/node/StyleNode$b;-><init>(Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    aput-object p2, v0, p1

    const/4 p1, 0x3

    new-instance p2, Lcom/discord/simpleast/core/node/StyleNode$b;

    iget-object p3, p5, Lf/a/j/a/f;->h:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {p2, p4, p3}, Lcom/discord/simpleast/core/node/StyleNode$b;-><init>(Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    aput-object p2, v0, p1

    invoke-direct {p0, v0}, Lcom/discord/simpleast/core/node/Node$a;-><init>([Lcom/discord/simpleast/core/node/Node;)V

    return-void
.end method
