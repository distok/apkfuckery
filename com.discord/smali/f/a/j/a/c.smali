.class public final Lf/a/j/a/c;
.super Lcom/discord/simpleast/core/parser/Rule;
.source "CodeRules.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/simpleast/core/parser/Rule<",
        "TR;",
        "Lcom/discord/simpleast/core/node/Node<",
        "TR;>;TS;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/discord/simpleast/core/node/StyleNode$a;

.field public final synthetic b:Lcom/discord/simpleast/core/node/StyleNode$a;


# direct methods
.method public constructor <init>(Lf/a/j/a/e;Lcom/discord/simpleast/core/node/StyleNode$a;Lcom/discord/simpleast/core/node/StyleNode$a;Ljava/util/regex/Pattern;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/simpleast/core/node/StyleNode$a;",
            "Lcom/discord/simpleast/core/node/StyleNode$a;",
            "Ljava/util/regex/Pattern;",
            ")V"
        }
    .end annotation

    iput-object p2, p0, Lf/a/j/a/c;->a:Lcom/discord/simpleast/core/node/StyleNode$a;

    iput-object p3, p0, Lf/a/j/a/c;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {p0, p4}, Lcom/discord/simpleast/core/parser/Rule;-><init>(Ljava/util/regex/Pattern;)V

    return-void
.end method


# virtual methods
.method public parse(Ljava/util/regex/Matcher;Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/Object;)Lcom/discord/simpleast/core/parser/ParseSpec;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/regex/Matcher;",
            "Lcom/discord/simpleast/core/parser/Parser<",
            "TR;-",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;TS;)",
            "Lcom/discord/simpleast/core/parser/ParseSpec<",
            "TR;TS;>;"
        }
    .end annotation

    const-string v0, "matcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parser"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    new-instance v0, Lcom/discord/simpleast/code/CodeNode$a$b;

    invoke-direct {v0, p1}, Lcom/discord/simpleast/code/CodeNode$a$b;-><init>(Ljava/lang/String;)V

    new-instance p1, Lcom/discord/simpleast/code/CodeNode;

    const/4 v1, 0x0

    iget-object v2, p0, Lf/a/j/a/c;->a:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {p1, v0, v1, v2}, Lcom/discord/simpleast/code/CodeNode;-><init>(Lcom/discord/simpleast/code/CodeNode$a;Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    new-instance v0, Lf/a/j/a/c$a;

    new-array p2, p2, [Lcom/discord/simpleast/core/node/Node;

    const/4 v1, 0x0

    aput-object p1, p2, v1

    invoke-direct {v0, p0, p1, p2}, Lf/a/j/a/c$a;-><init>(Lf/a/j/a/c;Lcom/discord/simpleast/code/CodeNode;[Lcom/discord/simpleast/core/node/Node;)V

    const-string p1, "node"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/simpleast/core/parser/ParseSpec;

    invoke-direct {p1, v0, p3}, Lcom/discord/simpleast/core/parser/ParseSpec;-><init>(Lcom/discord/simpleast/core/node/Node;Ljava/lang/Object;)V

    return-object p1
.end method
