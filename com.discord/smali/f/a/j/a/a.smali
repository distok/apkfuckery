.class public final Lf/a/j/a/a;
.super Lcom/discord/simpleast/core/parser/Rule;
.source "CodeRules.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/simpleast/core/parser/Rule<",
        "TR;",
        "Lcom/discord/simpleast/core/node/Node<",
        "TR;>;TS;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:Lcom/discord/simpleast/core/node/StyleNode$a;

.field public final synthetic c:Lkotlin/jvm/functions/Function3;


# direct methods
.method public constructor <init>(Lf/a/j/a/e;Ljava/util/Map;Lcom/discord/simpleast/core/node/StyleNode$a;Lkotlin/jvm/functions/Function3;Ljava/util/regex/Pattern;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map;",
            "Lcom/discord/simpleast/core/node/StyleNode$a;",
            "Lkotlin/jvm/functions/Function3;",
            "Ljava/util/regex/Pattern;",
            ")V"
        }
    .end annotation

    iput-object p2, p0, Lf/a/j/a/a;->a:Ljava/util/Map;

    iput-object p3, p0, Lf/a/j/a/a;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    iput-object p4, p0, Lf/a/j/a/a;->c:Lkotlin/jvm/functions/Function3;

    invoke-direct {p0, p5}, Lcom/discord/simpleast/core/parser/Rule;-><init>(Ljava/util/regex/Pattern;)V

    return-void
.end method


# virtual methods
.method public parse(Ljava/util/regex/Matcher;Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/Object;)Lcom/discord/simpleast/core/parser/ParseSpec;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/regex/Matcher;",
            "Lcom/discord/simpleast/core/parser/Parser<",
            "TR;-",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;TS;)",
            "Lcom/discord/simpleast/core/parser/ParseSpec<",
            "TR;TS;>;"
        }
    .end annotation

    const-string v0, "matcher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parser"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    const/16 v4, 0xa

    invoke-static {p1, v4, v3, v2}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;CZI)Z

    move-result v3

    :cond_1
    if-eqz v0, :cond_2

    iget-object p1, p0, Lf/a/j/a/a;->a:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    invoke-virtual {p2, v1, p3, p1}, Lcom/discord/simpleast/core/parser/Parser;->parse(Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    const-string p2, "null cannot be cast to non-null type kotlin.collections.List<com.discord.simpleast.core.node.Node<R>>"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance p2, Lcom/discord/simpleast/code/CodeNode$a$a;

    invoke-direct {p2, v1, p1}, Lcom/discord/simpleast/code/CodeNode$a$a;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_2

    :cond_3
    new-instance p2, Lcom/discord/simpleast/code/CodeNode$a$b;

    invoke-direct {p2, v1}, Lcom/discord/simpleast/code/CodeNode$a$b;-><init>(Ljava/lang/String;)V

    :goto_2
    new-instance p1, Lcom/discord/simpleast/code/CodeNode;

    iget-object v1, p0, Lf/a/j/a/a;->b:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {p1, p2, v0, v1}, Lcom/discord/simpleast/code/CodeNode;-><init>(Lcom/discord/simpleast/code/CodeNode$a;Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    iget-object p2, p0, Lf/a/j/a/a;->c:Lkotlin/jvm/functions/Function3;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, p1, v0, p3}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/simpleast/core/node/Node;

    const-string p2, "node"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lcom/discord/simpleast/core/parser/ParseSpec;

    invoke-direct {p2, p1, p3}, Lcom/discord/simpleast/core/parser/ParseSpec;-><init>(Lcom/discord/simpleast/core/node/Node;Ljava/lang/Object;)V

    return-object p2
.end method
