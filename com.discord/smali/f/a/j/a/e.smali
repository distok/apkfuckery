.class public final Lf/a/j/a/e;
.super Ljava/lang/Object;
.source "CodeRules.kt"


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Ljava/util/regex/Pattern;

.field public static final c:Ljava/util/regex/Pattern;

.field public static final d:Ljava/util/regex/Pattern;

.field public static final e:Ljava/util/regex/Pattern;

.field public static final f:Lf/a/j/a/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lf/a/j/a/e;

    invoke-direct {v0}, Lf/a/j/a/e;-><init>()V

    sput-object v0, Lf/a/j/a/e;->f:Lf/a/j/a/e;

    const-string v0, "^```(?:([\\w+\\-.]+?)?(\\s*\\n))?([^\\n].*?)\\n*```"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v2, "Pattern.compile(\"\"\"^```(\u2026n*```\"\"\", Pattern.DOTALL)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/a/j/a/e;->a:Ljava/util/regex/Pattern;

    const-string v0, "^`(?:\\s*)([^\\n].*?)\\n*`"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "Pattern.compile(\"\"\"^`(?:\u2026)\\n*`\"\"\", Pattern.DOTALL)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/a/j/a/e;->b:Ljava/util/regex/Pattern;

    const-string v0, "^(?:\\n\\s*)+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "Pattern.compile(\"\"\"^(?:\\n\\s*)+\"\"\")"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/a/j/a/e;->c:Ljava/util/regex/Pattern;

    const-string v0, "^[\\s\\S]+?(?=\\b|[^0-9A-Za-z\\s\\u00c0-\\uffff]|\\n| {2,}\\n|\\w+:\\S|$)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "Pattern.compile(\"\"\"^[\\s\\\u2026|\\n| {2,}\\n|\\w+:\\S|$)\"\"\")"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/a/j/a/e;->d:Ljava/util/regex/Pattern;

    const-string v0, "^\\b\\d+?\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v1, "Pattern.compile(\"\"\"^\\b\\d+?\\b\"\"\")"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lf/a/j/a/e;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;
    .locals 0

    and-int/lit8 p0, p4, 0x1

    if-eqz p0, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p0, p4, 0x2

    if-eqz p0, :cond_1

    const/4 p3, 0x0

    :cond_1
    const-string p0, "$this$toMatchGroupRule"

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Lf/a/j/a/d;

    invoke-direct {p0, p1, p2, p3, p1}, Lf/a/j/a/d;-><init>(Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;Ljava/util/regex/Pattern;)V

    return-object p0
.end method


# virtual methods
.method public final varargs a(Lf/a/j/a/f;Ljava/util/List;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/a/j/a/f<",
            "TR;>;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TR;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;>;[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/simpleast/core/parser/Rule<",
            "TR;",
            "Lcom/discord/simpleast/core/node/Node<",
            "TR;>;TS;>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    const/4 v5, 0x6

    new-array v5, v5, [Lcom/discord/simpleast/core/parser/Rule;

    array-length v6, v2

    invoke-static {v2, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const-string v6, "codeStyleProviders"

    invoke-static {v1, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "identifiers"

    invoke-static {v2, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v14, Lf/a/j/a/b;

    const-string v6, "^\\b("

    invoke-static {v6}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v7, "|"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x3e

    move-object v6, v2

    invoke-static/range {v6 .. v13}, Lf/h/a/f/f/n/g;->joinToString$default([Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ")(\\s+\\w+)"

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    const-string v7, "Pattern.compile(\"\"\"^\\b($\u2026String(\"|\")})(\\s+\\w+)\"\"\")"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v14, v1, v2, v6}, Lf/a/j/a/b;-><init>(Lf/a/j/a/f;[Ljava/lang/String;Ljava/util/regex/Pattern;)V

    const/4 v2, 0x0

    aput-object v14, v5, v2

    array-length v6, v3

    invoke-static {v3, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Lf/a/j/a/e;->c([Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    const-string v6, "createWordPattern(*builtIns)"

    invoke-static {v3, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v1, Lf/a/j/a/f;->g:Lcom/discord/simpleast/core/node/StyleNode$a;

    const/4 v7, 0x1

    invoke-static {v0, v3, v2, v6, v7}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v3

    aput-object v3, v5, v7

    array-length v3, v4

    invoke-static {v4, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Lf/a/j/a/e;->c([Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    const-string v4, "createWordPattern(*keywords)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v1, Lf/a/j/a/f;->d:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v0, v3, v2, v4, v7}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v5, v4

    sget-object v3, Lf/a/j/a/e;->e:Ljava/util/regex/Pattern;

    iget-object v1, v1, Lf/a/j/a/f;->c:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-static {v0, v3, v2, v1, v7}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    const/4 v3, 0x3

    aput-object v1, v5, v3

    sget-object v1, Lf/a/j/a/e;->c:Ljava/util/regex/Pattern;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v4, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    const/4 v6, 0x4

    aput-object v1, v5, v6

    sget-object v1, Lf/a/j/a/e;->d:Ljava/util/regex/Pattern;

    invoke-static {v0, v1, v2, v4, v3}, Lf/a/j/a/e;->d(Lf/a/j/a/e;Ljava/util/regex/Pattern;ILcom/discord/simpleast/core/node/StyleNode$a;I)Lcom/discord/simpleast/core/parser/Rule;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v5, v2

    invoke-static {v5}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    move-object/from16 v2, p2

    invoke-static {v2, v1}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public final b(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "^(?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".*?(?=\\n|$))"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    return-object p1
.end method

.method public final varargs c([Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 9

    const-string/jumbo v0, "words"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "^\\b(?:"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "|"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    move-object v1, p1

    invoke-static/range {v1 .. v8}, Lf/h/a/f/f/n/g;->joinToString$default([Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")\\b"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    return-object p1
.end method
