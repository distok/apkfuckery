.class public final Lf/a/j/a/i$a;
.super Lcom/discord/simpleast/core/node/Node$a;
.source "Kotlin.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/j/a/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/j/a/i$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RC:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/discord/simpleast/core/node/Node$a<",
        "TRC;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Lf/a/j/a/i$a$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/a/j/a/i$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/a/j/a/i$a$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lf/a/j/a/i$a;->b:Lf/a/j/a/i$a$a;

    const-string v0, "^(val|var)(\\s+\\w+)"

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/a/j/a/i$a;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lf/a/j/a/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lf/a/j/a/f<",
            "TRC;>;)V"
        }
    .end annotation

    const-string v0, "definition"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "codeStyleProviders"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/simpleast/core/node/Node;

    new-instance v1, Lcom/discord/simpleast/core/node/StyleNode$b;

    iget-object v2, p3, Lf/a/j/a/f;->d:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {v1, p1, v2}, Lcom/discord/simpleast/core/node/StyleNode$b;-><init>(Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    const/4 p1, 0x0

    aput-object v1, v0, p1

    new-instance p1, Lcom/discord/simpleast/core/node/StyleNode$b;

    iget-object p3, p3, Lf/a/j/a/f;->e:Lcom/discord/simpleast/core/node/StyleNode$a;

    invoke-direct {p1, p2, p3}, Lcom/discord/simpleast/core/node/StyleNode$b;-><init>(Ljava/lang/String;Lcom/discord/simpleast/core/node/StyleNode$a;)V

    const/4 p2, 0x1

    aput-object p1, v0, p2

    invoke-direct {p0, v0}, Lcom/discord/simpleast/core/node/Node$a;-><init>([Lcom/discord/simpleast/core/node/Node;)V

    return-void
.end method
