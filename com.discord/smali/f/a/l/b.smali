.class public final Lf/a/l/b;
.super Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;
.source "SparkleView.kt"


# instance fields
.field public final synthetic a:Lcom/discord/tooltips/SparkleView;

.field public final synthetic b:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;


# direct methods
.method public constructor <init>(Lcom/discord/tooltips/SparkleView;Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lf/a/l/b;->a:Lcom/discord/tooltips/SparkleView;

    iput-object p2, p0, Lf/a/l/b;->b:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    invoke-direct {p0}, Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/a/l/b;->a:Lcom/discord/tooltips/SparkleView;

    new-instance v0, Lf/a/l/b$a;

    invoke-direct {v0, p0}, Lf/a/l/b$a;-><init>(Lf/a/l/b;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
