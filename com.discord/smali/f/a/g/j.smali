.class public final Lf/a/g/j;
.super Ljava/lang/Object;
.source "CacheDataSourceFactory.kt"

# interfaces
.implements Lf/h/a/c/h1/j$a;


# instance fields
.field public final a:Lf/h/a/c/h1/o;

.field public final b:J


# direct methods
.method public constructor <init>(Lf/h/a/c/h1/o;J)V
    .locals 1

    const-string v0, "defaultDatasourceFactory"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/g/j;->a:Lf/h/a/c/h1/o;

    iput-wide p2, p0, Lf/a/g/j;->b:J

    return-void
.end method


# virtual methods
.method public a()Lf/h/a/c/h1/j;
    .locals 8

    sget-object v0, Lf/a/g/l;->b:Lf/a/g/l;

    sget-object v0, Lf/a/g/l;->a:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lf/h/a/c/h1/y/r;

    new-instance v0, Lf/h/a/c/h1/y/c;

    iget-object v1, p0, Lf/a/g/j;->a:Lf/h/a/c/h1/o;

    new-instance v3, Lf/h/a/c/h1/n;

    iget-object v4, v1, Lf/h/a/c/h1/o;->a:Landroid/content/Context;

    iget-object v1, v1, Lf/h/a/c/h1/o;->b:Lf/h/a/c/h1/j$a;

    invoke-interface {v1}, Lf/h/a/c/h1/j$a;->a()Lf/h/a/c/h1/j;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Lf/h/a/c/h1/n;-><init>(Landroid/content/Context;Lf/h/a/c/h1/j;)V

    new-instance v4, Lcom/google/android/exoplayer2/upstream/FileDataSource;

    invoke-direct {v4}, Lcom/google/android/exoplayer2/upstream/FileDataSource;-><init>()V

    new-instance v5, Lcom/google/android/exoplayer2/upstream/cache/CacheDataSink;

    iget-wide v6, p0, Lf/a/g/j;->b:J

    invoke-direct {v5, v2, v6, v7}, Lcom/google/android/exoplayer2/upstream/cache/CacheDataSink;-><init>(Lcom/google/android/exoplayer2/upstream/cache/Cache;J)V

    const/4 v7, 0x0

    const/4 v6, 0x3

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lf/h/a/c/h1/y/c;-><init>(Lcom/google/android/exoplayer2/upstream/cache/Cache;Lf/h/a/c/h1/j;Lf/h/a/c/h1/j;Lf/h/a/c/h1/h;ILf/h/a/c/h1/y/c$a;)V

    return-object v0
.end method
