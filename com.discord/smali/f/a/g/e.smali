.class public final Lf/a/g/e;
.super Ljava/lang/Object;
.source "AppMediaPlayer.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lf/a/g/k$c;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/player/AppMediaPlayer;


# direct methods
.method public constructor <init>(Lcom/discord/player/AppMediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lf/a/g/e;->d:Lcom/discord/player/AppMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 7

    check-cast p1, Lf/a/g/k$c;

    iget p1, p1, Lf/a/g/k$c;->b:I

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/a/g/e;->d:Lcom/discord/player/AppMediaPlayer;

    iget-object p1, p1, Lcom/discord/player/AppMediaPlayer;->a:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$e;->a:Lcom/discord/player/AppMediaPlayer$Event$e;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lf/a/g/e;->d:Lcom/discord/player/AppMediaPlayer;

    iget-object p1, p1, Lcom/discord/player/AppMediaPlayer;->b:Lrx/Subscription;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    iget-object p1, p0, Lf/a/g/e;->d:Lcom/discord/player/AppMediaPlayer;

    iget-object v0, p1, Lcom/discord/player/AppMediaPlayer;->a:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/player/AppMediaPlayer$Event$c;

    iget-object p1, p1, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {p1}, Lf/h/a/c/s0;->G()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/discord/player/AppMediaPlayer$Event$c;-><init>(J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lf/a/g/e;->d:Lcom/discord/player/AppMediaPlayer;

    iget-object p1, p1, Lcom/discord/player/AppMediaPlayer;->a:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$a;->a:Lcom/discord/player/AppMediaPlayer$Event$a;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lf/a/g/e;->d:Lcom/discord/player/AppMediaPlayer;

    iget-object v0, p1, Lcom/discord/player/AppMediaPlayer;->b:Lrx/Subscription;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_3
    const-wide/16 v3, 0x1f4

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p1, Lcom/discord/player/AppMediaPlayer;->i:Lrx/Scheduler;

    move-wide v1, v3

    invoke-static/range {v1 .. v6}, Lrx/Observable;->B(JJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    invoke-static {}, Lg0/j/b/a;->a()Lrx/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->F(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/g/g;

    invoke-direct {v1, p1}, Lf/a/g/g;-><init>(Lcom/discord/player/AppMediaPlayer;)V

    new-instance v2, Lf/a/g/h;

    invoke-direct {v2, p1}, Lf/a/g/h;-><init>(Lcom/discord/player/AppMediaPlayer;)V

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->R(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p1, Lcom/discord/player/AppMediaPlayer;->b:Lrx/Subscription;

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lf/a/g/e;->d:Lcom/discord/player/AppMediaPlayer;

    iget-object p1, p1, Lcom/discord/player/AppMediaPlayer;->a:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$b;->a:Lcom/discord/player/AppMediaPlayer$Event$b;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
