.class public final Lf/a/g/i;
.super Ljava/lang/Object;
.source "AppMediaPlayerFactory.kt"


# direct methods
.method public static final a(Landroid/content/Context;)Lcom/discord/player/AppMediaPlayer;
    .locals 21

    move-object/from16 v0, p0

    const-string v1, "context"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "com.discord"

    const-string v2, "applicationId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "logger"

    invoke-static {v7, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/h/a/c/h1/o;

    sget v3, Lf/h/a/c/i1/a0;->a:I

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v4, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v4, "?"

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " (Linux;Android "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "ExoPlayerLib/2.11.3"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lf/h/a/c/h1/o;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v5, Lf/a/g/j;

    const-wide/32 v8, 0x6400000

    invoke-direct {v5, v2, v8, v9}, Lf/a/g/j;-><init>(Lf/h/a/c/h1/o;J)V

    new-instance v4, Lf/a/g/k;

    invoke-direct {v4}, Lf/a/g/k;-><init>()V

    const/16 v1, 0x2710

    const/16 v2, 0x7530

    const/16 v6, 0x9c4

    const/16 v8, 0x1388

    const/4 v9, 0x1

    invoke-static {v9}, Lf/g/j/k/a;->s(Z)V

    const-string v10, "bufferForPlaybackMs"

    const-string v11, "0"

    invoke-static {v6, v3, v10, v11}, Lf/h/a/c/x;->j(IILjava/lang/String;Ljava/lang/String;)V

    const-string v12, "bufferForPlaybackAfterRebufferMs"

    invoke-static {v8, v3, v12, v11}, Lf/h/a/c/x;->j(IILjava/lang/String;Ljava/lang/String;)V

    const-string v3, "minBufferMs"

    invoke-static {v1, v6, v3, v10}, Lf/h/a/c/x;->j(IILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v8, v3, v12}, Lf/h/a/c/x;->j(IILjava/lang/String;Ljava/lang/String;)V

    const-string v6, "maxBufferMs"

    invoke-static {v2, v1, v6, v3}, Lf/h/a/c/x;->j(IILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v9}, Lf/g/j/k/a;->s(Z)V

    new-instance v11, Lf/h/a/c/h1/l;

    const/high16 v1, 0x10000

    invoke-direct {v11, v9, v1}, Lf/h/a/c/h1/l;-><init>(ZI)V

    new-instance v1, Lf/h/a/c/x;

    const/16 v17, -0x1

    const/16 v18, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v13, 0x2710

    const/16 v14, 0x7530

    const/16 v15, 0x9c4

    const/16 v16, 0x1388

    move-object v10, v1

    move v12, v13

    invoke-direct/range {v10 .. v20}, Lf/h/a/c/x;-><init>(Lf/h/a/c/h1/l;IIIIIIZIZ)V

    const-string v2, "DefaultLoadControl.Build\u2026reateDefaultLoadControl()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/h/a/c/s0$b;

    invoke-direct {v2, v0}, Lf/h/a/c/s0$b;-><init>(Landroid/content/Context;)V

    iget-boolean v0, v2, Lf/h/a/c/s0$b;->i:Z

    xor-int/2addr v0, v9

    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iput-object v1, v2, Lf/h/a/c/s0$b;->e:Lf/h/a/c/e0;

    iget-boolean v0, v2, Lf/h/a/c/s0$b;->i:Z

    xor-int/2addr v0, v9

    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iput-boolean v9, v2, Lf/h/a/c/s0$b;->i:Z

    new-instance v3, Lf/h/a/c/s0;

    iget-object v11, v2, Lf/h/a/c/s0$b;->a:Landroid/content/Context;

    iget-object v12, v2, Lf/h/a/c/s0$b;->b:Lf/h/a/c/z;

    iget-object v13, v2, Lf/h/a/c/s0$b;->d:Lf/h/a/c/f1/h;

    iget-object v14, v2, Lf/h/a/c/s0$b;->e:Lf/h/a/c/e0;

    iget-object v15, v2, Lf/h/a/c/s0$b;->f:Lf/h/a/c/h1/e;

    iget-object v0, v2, Lf/h/a/c/s0$b;->g:Lf/h/a/c/v0/a;

    iget-object v1, v2, Lf/h/a/c/s0$b;->c:Lf/h/a/c/i1/f;

    iget-object v2, v2, Lf/h/a/c/s0$b;->h:Landroid/os/Looper;

    move-object v10, v3

    move-object/from16 v16, v0

    move-object/from16 v17, v1

    move-object/from16 v18, v2

    invoke-direct/range {v10 .. v18}, Lf/h/a/c/s0;-><init>(Landroid/content/Context;Lf/h/a/c/z;Lf/h/a/c/f1/h;Lf/h/a/c/e0;Lf/h/a/c/h1/e;Lf/h/a/c/v0/a;Lf/h/a/c/i1/f;Landroid/os/Looper;)V

    const-string v0, "SimpleExoPlayer\n        \u2026ntrol)\n          .build()"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lf/h/a/c/s0;->S()V

    iget-object v0, v3, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/s$a;

    invoke-direct {v1, v4}, Lf/h/a/c/s$a;-><init>(Lf/h/a/c/m0$a;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    new-instance v0, Lcom/discord/player/AppMediaPlayer;

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v6

    const-string v1, "Schedulers.computation()"

    invoke-static {v6, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/discord/player/AppMediaPlayer;-><init>(Lf/h/a/c/s0;Lf/a/g/k;Lf/h/a/c/h1/j$a;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;)V

    return-object v0
.end method
