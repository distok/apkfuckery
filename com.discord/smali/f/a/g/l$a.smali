.class public final Lf/a/g/l$a;
.super Lx/m/c/k;
.source "SimpleCacheProvider.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/a/g/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lf/h/a/c/h1/y/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lf/a/g/l$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/g/l$a;

    invoke-direct {v0}, Lf/a/g/l$a;-><init>()V

    sput-object v0, Lf/a/g/l$a;->d:Lf/a/g/l$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 5

    sget-object v0, Lcom/discord/utilities/app/ApplicationProvider;->INSTANCE:Lcom/discord/utilities/app/ApplicationProvider;

    invoke-virtual {v0}, Lcom/discord/utilities/app/ApplicationProvider;->get()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "app_media_player"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Lf/h/a/c/h1/y/p;

    const-wide/32 v3, 0x6400000

    invoke-direct {v2, v3, v4}, Lf/h/a/c/h1/y/p;-><init>(J)V

    new-instance v3, Lf/h/a/c/x0/b;

    invoke-direct {v3, v0}, Lf/h/a/c/x0/b;-><init>(Landroid/content/Context;)V

    new-instance v0, Lf/h/a/c/h1/y/r;

    invoke-direct {v0, v1, v2, v3}, Lf/h/a/c/h1/y/r;-><init>(Ljava/io/File;Lf/h/a/c/h1/y/d;Lf/h/a/c/x0/a;)V

    return-object v0
.end method
