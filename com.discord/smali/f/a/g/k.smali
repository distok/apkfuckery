.class public final Lf/a/g/k;
.super Ljava/lang/Object;
.source "RxPlayerEventListener.kt"

# interfaces
.implements Lf/h/a/c/m0$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/a/g/k$c;,
        Lf/a/g/k$a;,
        Lf/a/g/k$b;
    }
.end annotation


# instance fields
.field public final d:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lf/a/g/k$c;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lf/a/g/k$a;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lf/a/g/k$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lf/a/g/k;->d:Lrx/subjects/PublishSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lf/a/g/k;->e:Lrx/subjects/PublishSubject;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lf/a/g/k;->f:Lrx/subjects/PublishSubject;

    return-void
.end method


# virtual methods
.method public synthetic C(Lf/h/a/c/j0;)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->c(Lf/h/a/c/m0$a;Lf/h/a/c/j0;)V

    return-void
.end method

.method public D(Z)V
    .locals 2

    iget-object v0, p0, Lf/a/g/k;->e:Lrx/subjects/PublishSubject;

    new-instance v1, Lf/a/g/k$a;

    invoke-direct {v1, p1}, Lf/a/g/k$a;-><init>(Z)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic a()V
    .locals 0

    invoke-static {p0}, Lf/h/a/c/l0;->h(Lf/h/a/c/m0$a;)V

    return-void
.end method

.method public synthetic e(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->d(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public synthetic f(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->b(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public synthetic g(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->f(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public k(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 2

    const-string v0, "error"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/g/k;->f:Lrx/subjects/PublishSubject;

    new-instance v1, Lf/a/g/k$b;

    invoke-direct {v1, p1}, Lf/a/g/k$b;-><init>(Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic l(Lf/h/a/c/t0;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lf/h/a/c/l0;->j(Lf/h/a/c/m0$a;Lf/h/a/c/t0;I)V

    return-void
.end method

.method public synthetic p(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->i(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public s(ZI)V
    .locals 2

    iget-object v0, p0, Lf/a/g/k;->d:Lrx/subjects/PublishSubject;

    new-instance v1, Lf/a/g/k$c;

    invoke-direct {v1, p1, p2}, Lf/a/g/k$c;-><init>(ZI)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic t(Lf/h/a/c/t0;Ljava/lang/Object;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lf/h/a/c/l0;->k(Lf/h/a/c/m0$a;Lf/h/a/c/t0;Ljava/lang/Object;I)V

    return-void
.end method

.method public synthetic u(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->g(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public synthetic z(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lf/h/a/c/l0;->l(Lf/h/a/c/m0$a;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V

    return-void
.end method
