.class public final Lf/a/e/c$a;
.super Ljava/lang/Object;
.source "OverlayManager.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/e/c;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/a/e/c;


# direct methods
.method public constructor <init>(Lf/a/e/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/a/e/c$a;->d:Lf/a/e/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "v"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/a/e/c$a;->d:Lf/a/e/c;

    iget-object v0, v0, Lf/a/e/c;->g:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "v.findViewWithTag(anchorViewTag)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/a/e/c$a;->d:Lf/a/e/c;

    iget-object v0, v0, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-virtual {v0, p1}, Lcom/discord/overlay/views/OverlayBubbleWrap;->b(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lf/a/e/c$a$a;

    invoke-direct {v0, p0, p1}, Lf/a/e/c$a$a;-><init>(Lf/a/e/c$a;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :goto_0
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lf/a/e/c$a;->d:Lf/a/e/c;

    iget-object p1, p1, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/a/e/c$a;->d:Lf/a/e/c;

    iget-object p1, p1, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/overlay/views/OverlayBubbleWrap;->setBubbleTouchable(Z)V

    iget-object p1, p0, Lf/a/e/c$a;->d:Lf/a/e/c;

    iget-object p1, p1, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-virtual {p1, v0}, Lcom/discord/overlay/views/OverlayBubbleWrap;->a(Z)V

    :cond_0
    return-void
.end method
