.class public final Lf/a/e/d;
.super Ljava/lang/Object;
.source "Animator.kt"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/discord/overlay/OverlayManager;

.field public final synthetic b:Lcom/discord/overlay/views/OverlayBubbleWrap;


# direct methods
.method public constructor <init>(Lcom/discord/overlay/OverlayManager;Lcom/discord/overlay/views/OverlayBubbleWrap;)V
    .locals 0

    iput-object p1, p0, Lf/a/e/d;->a:Lcom/discord/overlay/OverlayManager;

    iput-object p2, p0, Lf/a/e/d;->b:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animator"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animator"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/a/e/d;->a:Lcom/discord/overlay/OverlayManager;

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/a/e/d;->b:Lcom/discord/overlay/views/OverlayBubbleWrap;

    const-string v0, "bubble"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lf/a/e/d;->a:Lcom/discord/overlay/OverlayManager;

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->j:Lf/a/e/e;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lf/a/e/d;->b:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-interface {p1, v0}, Lf/a/e/e;->b(Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animator"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animator"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
