.class public final Lf/a/e/c$a$a;
.super Ljava/lang/Object;
.source "View.kt"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/a/e/c$a;->onViewAttachedToWindow(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/a/e/c$a;

.field public final synthetic e:Landroid/view/View;


# direct methods
.method public constructor <init>(Lf/a/e/c$a;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lf/a/e/c$a$a;->d:Lf/a/e/c$a;

    iput-object p2, p0, Lf/a/e/c$a$a;->e:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object p1, p0, Lf/a/e/c$a$a;->e:Landroid/view/View;

    iget-object p2, p0, Lf/a/e/c$a$a;->d:Lf/a/e/c$a;

    iget-object p2, p2, Lf/a/e/c$a;->d:Lf/a/e/c;

    iget-object p2, p2, Lf/a/e/c;->g:Ljava/lang/Object;

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "v.findViewWithTag(anchorViewTag)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lf/a/e/c$a$a;->d:Lf/a/e/c$a;

    iget-object p2, p2, Lf/a/e/c$a;->d:Lf/a/e/c;

    iget-object p2, p2, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-virtual {p2, p1}, Lcom/discord/overlay/views/OverlayBubbleWrap;->b(Landroid/view/View;)V

    return-void
.end method
