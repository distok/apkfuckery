.class public final Lf/a/e/b;
.super Lx/m/c/k;
.source "OverlayManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $bubble:Lcom/discord/overlay/views/OverlayBubbleWrap;

.field public final synthetic this$0:Lcom/discord/overlay/OverlayManager;


# direct methods
.method public constructor <init>(Lcom/discord/overlay/OverlayManager;Lcom/discord/overlay/views/OverlayBubbleWrap;)V
    .locals 0

    iput-object p1, p0, Lf/a/e/b;->this$0:Lcom/discord/overlay/OverlayManager;

    iput-object p2, p0, Lf/a/e/b;->$bubble:Lcom/discord/overlay/views/OverlayBubbleWrap;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/a/e/b;->this$0:Lcom/discord/overlay/OverlayManager;

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    if-eqz p1, :cond_2

    iget-object p1, p1, Lf/a/e/f/a;->h:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/a/e/b;->this$0:Lcom/discord/overlay/OverlayManager;

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->i:Lf/a/e/f/a;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lf/a/e/f/a;->i:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :cond_1
    iget-object p1, p0, Lf/a/e/b;->this$0:Lcom/discord/overlay/OverlayManager;

    iget-object v0, p0, Lf/a/e/b;->$bubble:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-virtual {p1, v0}, Lcom/discord/overlay/OverlayManager;->b(Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    :cond_2
    :goto_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
