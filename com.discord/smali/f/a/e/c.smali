.class public final Lf/a/e/c;
.super Ljava/lang/Object;
.source "OverlayManager.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/overlay/OverlayManager;

.field public final synthetic e:Lkotlin/jvm/functions/Function1;

.field public final synthetic f:Lcom/discord/overlay/views/OverlayBubbleWrap;

.field public final synthetic g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/discord/overlay/OverlayManager;Lkotlin/jvm/functions/Function1;Lcom/discord/overlay/views/OverlayBubbleWrap;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lf/a/e/c;->d:Lcom/discord/overlay/OverlayManager;

    iput-object p2, p0, Lf/a/e/c;->e:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    iput-object p4, p0, Lf/a/e/c;->g:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lf/a/e/c;->e:Lkotlin/jvm/functions/Function1;

    iget-object v0, p0, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/overlay/views/OverlayBubbleWrap;

    new-instance v0, Lf/a/e/c$a;

    invoke-direct {v0, p0}, Lf/a/e/c$a;-><init>(Lf/a/e/c;)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Lf/a/e/c;->d:Lcom/discord/overlay/OverlayManager;

    invoke-virtual {v0, p1}, Lcom/discord/overlay/OverlayManager;->a(Lcom/discord/overlay/views/OverlayBubbleWrap;)V

    iget-object p1, p0, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/overlay/views/OverlayBubbleWrap;->setBubbleTouchable(Z)V

    iget-object p1, p0, Lf/a/e/c;->d:Lcom/discord/overlay/OverlayManager;

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    iget-object v0, p0, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-interface {p1, v0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    iget-object p1, p0, Lf/a/e/c;->d:Lcom/discord/overlay/OverlayManager;

    iget-object p1, p1, Lcom/discord/overlay/OverlayManager;->m:Landroid/view/WindowManager;

    iget-object v0, p0, Lf/a/e/c;->f:Lcom/discord/overlay/views/OverlayBubbleWrap;

    invoke-virtual {v0}, Lcom/discord/overlay/views/OverlayBubbleWrap;->getWindowLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
