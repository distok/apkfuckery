.class public final Lf/a/b/w;
.super Ljava/lang/Object;
.source "AppTransformers.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "TT;",
        "Lrx/Observable<",
        "+TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/a/b/r$d;


# direct methods
.method public constructor <init>(Lf/a/b/r$d;)V
    .locals 0

    iput-object p1, p0, Lf/a/b/w;->d:Lf/a/b/r$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lf/a/b/w;->d:Lf/a/b/r$d;

    iget-object v0, v0, Lf/a/b/r$d;->d:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/a/b/w;->d:Lf/a/b/r$d;

    iget-object p1, p1, Lf/a/b/r$d;->e:Ljava/lang/Object;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    iget-object p1, p0, Lf/a/b/w;->d:Lf/a/b/r$d;

    iget-wide v1, p1, Lf/a/b/r$d;->f:J

    iget-object p1, p1, Lf/a/b/r$d;->g:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, p1}, Lrx/Observable;->p(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
