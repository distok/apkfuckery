.class public final Lf/a/b/a0;
.super Ljava/lang/Object;
.source "AppTransformers.kt"

# interfaces
.implements Lrx/Observable$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$c<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Ljava/lang/Class;

.field public final synthetic f:Lrx/functions/Action1;

.field public final synthetic g:Lrx/functions/Action1;

.field public final synthetic h:Lrx/functions/Action1;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;Lrx/functions/Action1;Lrx/functions/Action1;Lrx/functions/Action1;)V
    .locals 0

    iput-object p1, p0, Lf/a/b/a0;->d:Landroid/content/Context;

    iput-object p2, p0, Lf/a/b/a0;->e:Ljava/lang/Class;

    iput-object p3, p0, Lf/a/b/a0;->f:Lrx/functions/Action1;

    iput-object p4, p0, Lf/a/b/a0;->g:Lrx/functions/Action1;

    iput-object p5, p0, Lf/a/b/a0;->h:Lrx/functions/Action1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    move-object v0, p1

    check-cast v0, Lrx/Observable;

    const-string p1, "observable"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lf/a/b/a0;->d:Landroid/content/Context;

    iget-object p1, p0, Lf/a/b/a0;->e:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string p1, "errorClass.simpleName"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/a/b/a0;->f:Lrx/functions/Action1;

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    new-instance v3, Lf/a/b/k0;

    invoke-direct {v3, p1}, Lf/a/b/k0;-><init>(Lrx/functions/Action1;)V

    goto :goto_0

    :cond_0
    move-object v3, v7

    :goto_0
    new-instance v4, Lf/a/b/y;

    iget-object p1, p0, Lf/a/b/a0;->g:Lrx/functions/Action1;

    invoke-direct {v4, p1}, Lf/a/b/y;-><init>(Lrx/functions/Action1;)V

    iget-object p1, p0, Lf/a/b/a0;->h:Lrx/functions/Action1;

    if-eqz p1, :cond_1

    new-instance v5, Lf/a/b/k0;

    invoke-direct {v5, p1}, Lf/a/b/k0;-><init>(Lrx/functions/Action1;)V

    goto :goto_1

    :cond_1
    move-object v5, v7

    :goto_1
    sget-object v6, Lf/a/b/z;->d:Lf/a/b/z;

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v7
.end method
