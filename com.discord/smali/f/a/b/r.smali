.class public final Lf/a/b/r;
.super Ljava/lang/Object;
.source "AppTransformers.kt"


# static fields
.field public static final a:Lf/a/b/r;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/b/r;

    invoke-direct {v0}, Lf/a/b/r;-><init>()V

    sput-object v0, Lf/a/b/r;->a:Lf/a/b/r;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/util/Collection;)Lrx/Observable$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+TK;>;)",
            "Lrx/Observable$c<",
            "Ljava/util/Map<",
            "TK;TT;>;",
            "Ljava/util/Map<",
            "TK;TT;>;>;"
        }
    .end annotation

    const-string v0, "filterKeys"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/b/r$a;->d:Lf/a/b/r$a;

    invoke-static {p0, v0}, Lf/a/b/r;->b(Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)Lrx/Observable$c;

    move-result-object p0

    return-object p0
.end method

.method public static final b(Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)Lrx/Observable$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            "V1:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+TK;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TV;+TV1;>;)",
            "Lrx/Observable$c<",
            "Ljava/util/Map<",
            "TK;TV;>;",
            "Ljava/util/Map<",
            "TK;TV1;>;>;"
        }
    .end annotation

    const-string/jumbo v0, "valueMapper"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lf/a/b/r$b;->d:Lf/a/b/r$b;

    goto :goto_0

    :cond_0
    new-instance v0, Lf/a/b/r$c;

    invoke-direct {v0, p0, p1}, Lf/a/b/r$c;-><init>(Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static final c(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Lrx/Observable$c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;TT;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "predicate"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timeUnit"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/b/r$d;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lf/a/b/r$d;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method public static synthetic d(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;I)Lrx/Observable$c;
    .locals 0

    and-int/lit8 p4, p5, 0x4

    if-eqz p4, :cond_0

    const-wide/16 p2, 0x1388

    :cond_0
    and-int/lit8 p4, p5, 0x8

    if-eqz p4, :cond_1

    sget-object p4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_0

    :cond_1
    const/4 p4, 0x0

    :goto_0
    invoke-static {p0, p1, p2, p3, p4}, Lf/a/b/r;->c(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Lrx/Observable$c;

    move-result-object p0

    return-object p0
.end method

.method public static final e()Lrx/Observable$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lf/a/b/r;->f(ZI)Lrx/Observable$c;

    move-result-object v0

    return-object v0
.end method

.method public static f(ZI)Lrx/Observable$c;
    .locals 1

    const/4 v0, 0x1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    const/4 p0, 0x1

    :cond_0
    new-instance p1, Lf/a/b/x;

    invoke-direct {p1, p0}, Lf/a/b/x;-><init>(Z)V

    return-object p1
.end method

.method public static final g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action1<",
            "-TT;>;",
            "Ljava/lang/Class<",
            "*>;)",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "onNext"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/b/a0;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, v0

    move-object v3, p1

    move-object v5, p0

    invoke-direct/range {v1 .. v6}, Lf/a/b/a0;-><init>(Landroid/content/Context;Ljava/lang/Class;Lrx/functions/Action1;Lrx/functions/Action1;Lrx/functions/Action1;)V

    return-object v0
.end method

.method public static h(Lf/a/b/r;Lkotlin/jvm/functions/Function1;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Landroid/content/Context;Lkotlin/jvm/functions/Function0;I)Lrx/Observable$c;
    .locals 7

    and-int/lit8 p0, p7, 0x4

    const/4 p4, 0x0

    if-eqz p0, :cond_0

    move-object v5, p4

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 p0, p7, 0x8

    const/4 v3, 0x0

    and-int/lit8 p0, p7, 0x10

    if-eqz p0, :cond_1

    move-object v1, p4

    goto :goto_1

    :cond_1
    move-object v1, p5

    :goto_1
    and-int/lit8 p0, p7, 0x20

    const/4 v6, 0x0

    const-string p0, "onNext"

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "errorTag"

    invoke-static {p2, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Lf/a/b/c0;

    move-object v0, p0

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lf/a/b/c0;-><init>(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object p0
.end method

.method public static final j(Lrx/functions/Action1;Landroid/content/Context;)Lrx/Observable$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action1<",
            "-TT;>;",
            "Landroid/content/Context;",
            ")",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/r;->n(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;I)Lrx/Observable$c;

    move-result-object p0

    return-object p0
.end method

.method public static final k(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;)Lrx/Observable$c;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action1<",
            "-TT;>;",
            "Landroid/content/Context;",
            "Lrx/functions/Action1<",
            "Lcom/discord/utilities/error/Error;",
            ">;)",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "onNext"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Lf/a/b/r$e;

    invoke-direct {v5, p0}, Lf/a/b/r$e;-><init>(Lrx/functions/Action1;)V

    const/4 v7, 0x0

    if-eqz p2, :cond_0

    new-instance p0, Lf/a/b/k0;

    invoke-direct {p0, p2}, Lf/a/b/k0;-><init>(Lrx/functions/Action1;)V

    goto :goto_0

    :cond_0
    move-object p0, v7

    :goto_0
    const/16 p2, 0x28

    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_1

    move-object v6, v7

    goto :goto_1

    :cond_1
    move-object v6, p0

    :goto_1
    and-int/lit8 p0, p2, 0x8

    and-int/lit8 p0, p2, 0x10

    if-eqz p0, :cond_2

    move-object v2, v7

    goto :goto_2

    :cond_2
    move-object v2, p1

    :goto_2
    and-int/lit8 p0, p2, 0x20

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "restClient"

    const-string p0, "errorTag"

    invoke-static {v3, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Lf/a/b/c0;

    move-object v1, p0

    move-object v4, v7

    invoke-direct/range {v1 .. v7}, Lf/a/b/c0;-><init>(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object p0
.end method

.method public static final l(Lrx/functions/Action1;Lcom/discord/app/AppDialog;)Lrx/Observable$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action1<",
            "-TT;>;",
            "Lcom/discord/app/AppDialog;",
            ")",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "onNext"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/b/r;->a:Lf/a/b/r;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v1, Lf/a/b/r$g;

    invoke-direct {v1, p0}, Lf/a/b/r$g;-><init>(Lrx/functions/Action1;)V

    const/4 p0, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object p0

    return-object p0
.end method

.method public static final m(Lrx/functions/Action1;Lcom/discord/app/AppFragment;)Lrx/Observable$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action1<",
            "-TT;>;",
            "Lcom/discord/app/AppFragment;",
            ")",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "onNext"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf/a/b/r;->a:Lf/a/b/r;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v1, Lf/a/b/r$f;

    invoke-direct {v1, p0}, Lf/a/b/r$f;-><init>(Lrx/functions/Action1;)V

    const/4 p0, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic n(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;I)Lrx/Observable$c;
    .locals 0

    and-int/lit8 p2, p3, 0x4

    const/4 p2, 0x0

    invoke-static {p0, p1, p2}, Lf/a/b/r;->k(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object p0

    return-object p0
.end method

.method public static final o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lrx/Observable$c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;TR;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lrx/Observable<",
            "TR;>;>;)",
            "Lrx/Observable$c<",
            "TT;TR;>;"
        }
    .end annotation

    const-string v0, "observableCondition"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "defaultObservableFunc"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/a/b/r$h;

    invoke-direct {v2, p1}, Lf/a/b/r$h;-><init>(Ljava/lang/Object;)V

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "switchedObservableFunc"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lf/a/b/e0;

    invoke-direct {p1, p0, v2, p2}, Lf/a/b/e0;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object p1
.end method

.method public static final p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/discord/app/AppComponent;",
            ")",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "appComponent"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/b/h0;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lf/a/b/h0;-><init>(Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)V

    return-object v0
.end method

.method public static final q(Lcom/discord/utilities/dimmer/DimmerView;)Lrx/Observable$c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/discord/utilities/dimmer/DimmerView;",
            ")",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    const/4 v2, 0x2

    invoke-static {p0, v0, v1, v2}, Lf/a/b/r;->r(Lcom/discord/utilities/dimmer/DimmerView;JI)Lrx/Observable$c;

    move-result-object p0

    return-object p0
.end method

.method public static r(Lcom/discord/utilities/dimmer/DimmerView;JI)Lrx/Observable$c;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-wide/16 p1, 0x1c2

    :cond_0
    new-instance p3, Lf/a/b/j0;

    invoke-direct {p3, p0, p1, p2}, Lf/a/b/j0;-><init>(Lcom/discord/utilities/dimmer/DimmerView;J)V

    return-object p3
.end method


# virtual methods
.method public final i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;",
            "Lrx/functions/Action1<",
            "Lcom/discord/utilities/error/Error;",
            ">;)",
            "Lrx/Observable$c<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "onNext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    new-instance v0, Lf/a/b/k0;

    invoke-direct {v0, p3}, Lf/a/b/k0;-><init>(Lrx/functions/Action1;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v4, v0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x28

    const-string v3, "restClient"

    move-object v1, p0

    move-object v2, p2

    move-object v6, p1

    invoke-static/range {v1 .. v8}, Lf/a/b/r;->h(Lf/a/b/r;Lkotlin/jvm/functions/Function1;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Landroid/content/Context;Lkotlin/jvm/functions/Function0;I)Lrx/Observable$c;

    move-result-object p1

    return-object p1
.end method
