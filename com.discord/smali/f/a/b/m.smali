.class public final Lf/a/b/m;
.super Ljava/lang/Object;
.source "AppScreen.kt"


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx/q/b<",
            "+",
            "Lcom/discord/app/AppFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx/q/b<",
            "+",
            "Lcom/discord/widgets/auth/WidgetOauth2Authorize;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx/q/b<",
            "+",
            "Lcom/discord/app/AppFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx/q/b<",
            "+",
            "Lcom/discord/app/AppFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx/q/b<",
            "Lcom/discord/widgets/media/WidgetMedia;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx/q/b<",
            "Lcom/discord/widgets/tabs/WidgetTabsHost;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final g:Lf/a/b/m;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    new-instance v0, Lf/a/b/m;

    invoke-direct {v0}, Lf/a/b/m;-><init>()V

    sput-object v0, Lf/a/b/m;->g:Lf/a/b/m;

    const/16 v0, 0x8

    new-array v1, v0, [Lx/q/b;

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthLanding;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthLogin;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthRegister;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthUndeleteAccount;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthCaptcha;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthMfa;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v8, 0x5

    aput-object v2, v1, v8

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthBirthday;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v9, 0x6

    aput-object v2, v1, v9

    const-class v2, Lcom/discord/widgets/auth/WidgetAuthAgeGated;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    const/4 v10, 0x7

    aput-object v2, v1, v10

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lf/a/b/m;->a:Ljava/util/List;

    new-array v1, v5, [Lx/q/b;

    const-class v2, Lcom/discord/widgets/auth/WidgetOauth2Authorize;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v3

    const-class v2, Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lf/a/b/m;->b:Ljava/util/List;

    const/16 v1, 0x2c

    new-array v1, v1, [Lx/q/b;

    const-class v2, Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v3

    const-class v2, Lcom/discord/widgets/settings/account/WidgetSettingsAccountBackupCodes;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v4

    const-class v2, Lcom/discord/widgets/settings/account/WidgetSettingsAccountChangePassword;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v5

    const-class v2, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v6

    const-class v2, Lcom/discord/widgets/settings/account/WidgetSettingsAccountEmailEdit;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v7

    const-class v2, Lcom/discord/widgets/user/WidgetUserPasswordVerify;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v8

    const-class v2, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFASteps;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v9

    const-class v2, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v10

    const-class v2, Lcom/discord/widgets/settings/WidgetSettingsBehavior;

    invoke-static {v2}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v2

    aput-object v2, v1, v0

    const-class v0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x9

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0xa

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0xb

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0xc

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0xd

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/WidgetSettingsVoice;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0xe

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0xf

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/WidgetSettingsAuthorizedApps;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x10

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerNotifications;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x11

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x12

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x13

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x14

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x15

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsIntegrations;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x16

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x17

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x18

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsSecurity;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x19

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x1a

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsEmojis;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x1b

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsEmojisEdit;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x1c

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x1d

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x1e

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsBans;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x1f

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/channels/WidgetChannelSettingsEditPermissions;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x20

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/channels/WidgetChannelSettingsPermissionsOverview;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x21

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/auth/WidgetAuthRegister;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x22

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/auth/WidgetAuthBirthday;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x23

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/auth/WidgetAuthAgeGated;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x24

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/auth/WidgetAuthLogin;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x25

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x26

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x27

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x28

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x29

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x2a

    aput-object v0, v1, v2

    const-class v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    const/16 v2, 0x2b

    aput-object v0, v1, v2

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lf/a/b/m;->c:Ljava/util/List;

    new-array v0, v10, [Lx/q/b;

    const-class v1, Lcom/discord/widgets/user/account/WidgetUserAccountVerify;

    invoke-static {v1}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v1

    aput-object v1, v0, v3

    const-class v1, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

    invoke-static {v1}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v1

    aput-object v1, v0, v4

    const-class v1, Lcom/discord/widgets/user/email/WidgetUserEmailUpdate;

    invoke-static {v1}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v1

    aput-object v1, v0, v5

    const-class v1, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {v1}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v1

    aput-object v1, v0, v6

    const-class v1, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;

    invoke-static {v1}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v1

    aput-object v1, v0, v7

    const-class v1, Lcom/discord/widgets/user/captcha/WidgetUserCaptchaVerify;

    invoke-static {v1}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v1

    aput-object v1, v0, v8

    const-class v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify;

    invoke-static {v1}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lf/a/b/m;->d:Ljava/util/List;

    const-class v0, Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lf/a/b/m;->e:Ljava/util/List;

    const-class v0, Lcom/discord/widgets/tabs/WidgetTabsHost;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lf/a/b/m;->f:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-static {p0, v0, v1, v2}, Lf/a/b/m;->c(Landroid/content/Context;ZLandroid/content/Intent;I)V

    return-void
.end method

.method public static final b(Landroid/content/Context;ZLandroid/content/Intent;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    const-string v0, "com.discord.intent.extra.EXTRA_OPEN_PANEL"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/stores/StoreNavigation$PanelAction;->OPEN:Lcom/discord/stores/StoreNavigation$PanelAction;

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/discord/stores/StoreNavigation$PanelAction;->CLOSE:Lcom/discord/stores/StoreNavigation$PanelAction;

    :goto_1
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getTabsNavigation()Lcom/discord/stores/StoreTabsNavigation;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, v0, p1, v2, v3}, Lcom/discord/stores/StoreTabsNavigation;->selectHomeTab$default(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;ZILjava/lang/Object;)V

    const-class p1, Lcom/discord/widgets/tabs/WidgetTabsHost;

    goto :goto_2

    :cond_2
    const-class p1, Lcom/discord/widgets/auth/WidgetAuthLanding;

    :goto_2
    invoke-static {p0, p1, p2}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic c(Landroid/content/Context;ZLandroid/content/Intent;I)V
    .locals 1

    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    :cond_1
    invoke-static {p0, p1, p2}, Lf/a/b/m;->b(Landroid/content/Context;ZLandroid/content/Intent;)V

    return-void
.end method

.method public static final d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "javaClass.simpleName"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "screen.simpleName"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/app/AppLog;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    :goto_0
    const-class p2, Lcom/discord/app/AppActivity;

    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object p2

    const-string v0, "com.discord.intent.extra.EXTRA_SCREEN"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object p1

    const-string p2, "if (extras != null) {\n  \u2026nts.EXTRA_SCREEN, screen)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic e(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;I)V
    .locals 0

    and-int/lit8 p2, p3, 0x4

    const/4 p2, 0x0

    invoke-static {p0, p1, p2}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static final f(Landroidx/fragment/app/Fragment;Ljava/lang/Class;Landroid/content/Intent;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/Fragment;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/discord/app/AppComponent;",
            ">;",
            "Landroid/content/Intent;",
            "I)V"
        }
    .end annotation

    const-string v0, "fragment"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "javaClass.simpleName"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "screen.simpleName"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/discord/app/AppLog;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    :goto_0
    const-class p2, Lcom/discord/app/AppActivity;

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object p2

    const-string v0, "com.discord.intent.extra.EXTRA_SCREEN"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object p1

    const-string p2, "if (extras != null) {\n  \u2026nts.EXTRA_SCREEN, screen)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p3}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_1
    return-void
.end method
