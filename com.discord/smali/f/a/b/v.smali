.class public final Lf/a/b/v;
.super Ljava/lang/Object;
.source "AppTransformers.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/util/Map<",
        "TK;+TV;>;",
        "Lrx/Observable<",
        "+",
        "Ljava/util/Map<",
        "TK;TV1;>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/a/b/r$c;


# direct methods
.method public constructor <init>(Lf/a/b/r$c;)V
    .locals 0

    iput-object p1, p0, Lf/a/b/v;->d:Lf/a/b/r$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Ljava/util/Map;

    iget-object v0, p0, Lf/a/b/v;->d:Lf/a/b/r$c;

    iget-object v0, v0, Lf/a/b/r$c;->d:Ljava/util/Collection;

    invoke-static {v0}, Lrx/Observable;->y(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/b/s;

    invoke-direct {v1, p1}, Lf/a/b/s;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/b/t;->d:Lf/a/b/t;

    new-instance v2, Lf/a/b/u;

    invoke-direct {v2, p0, p1}, Lf/a/b/u;-><init>(Lf/a/b/v;Ljava/util/Map;)V

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->b0(Lg0/k/b;Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
