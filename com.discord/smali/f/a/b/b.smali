.class public final synthetic Lf/a/b/b;
.super Lx/m/c/i;
.source "AppActivity.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Lcom/discord/app/AppActivity$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lf/a/b/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/b/b;

    invoke-direct {v0}, Lf/a/b/b;-><init>()V

    sput-object v0, Lf/a/b/b;->d:Lf/a/b/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Lcom/discord/app/AppActivity$a;

    const/4 v1, 0x3

    const-string v3, "<init>"

    const-string v4, "<init>(Ljava/lang/String;Ljava/lang/String;I)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/app/AppActivity$a;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/app/AppActivity$a;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
