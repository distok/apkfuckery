.class public final Lf/a/b/q0/b;
.super Ljava/lang/Object;
.source "RoutingPatterns.kt"


# static fields
.field public static final A:Lkotlin/text/Regex;

.field public static final B:Lkotlin/text/Regex;

.field public static final C:Lkotlin/text/Regex;

.field public static final D:Lkotlin/text/Regex;

.field public static final E:Lf/a/b/q0/b;

.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static final h:Ljava/lang/String;

.field public static final i:Ljava/lang/String;

.field public static final j:Ljava/lang/String;

.field public static final k:Ljava/lang/String;

.field public static final l:Lkotlin/text/Regex;

.field public static final m:Lkotlin/text/Regex;

.field public static final n:Lkotlin/text/Regex;

.field public static final o:Lkotlin/text/Regex;

.field public static final p:Lkotlin/text/Regex;

.field public static final q:Lkotlin/text/Regex;

.field public static final r:Lkotlin/text/Regex;

.field public static final s:Lkotlin/text/Regex;

.field public static final t:Lkotlin/text/Regex;

.field public static final u:Lkotlin/text/Regex;

.field public static final v:Lkotlin/text/Regex;

.field public static final w:Lkotlin/text/Regex;

.field public static final x:Lkotlin/text/Regex;

.field public static final y:Lkotlin/text/Regex;

.field public static final z:Lkotlin/text/Regex;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    new-instance v0, Lf/a/b/q0/b;

    invoke-direct {v0}, Lf/a/b/q0/b;-><init>()V

    sput-object v0, Lf/a/b/q0/b;->E:Lf/a/b/q0/b;

    const-string v1, "https://discord.com"

    invoke-virtual {v0, v1}, Lf/a/b/q0/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lf/a/b/q0/b;->a:Ljava/lang/String;

    const-string v2, "https://discordapp.com"

    invoke-virtual {v0, v2}, Lf/a/b/q0/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lf/a/b/q0/b;->b:Ljava/lang/String;

    const-string v3, "https://discord.gift"

    invoke-virtual {v0, v3}, Lf/a/b/q0/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lf/a/b/q0/b;->c:Ljava/lang/String;

    const-string v4, "https://discord.gg"

    invoke-virtual {v0, v4}, Lf/a/b/q0/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lf/a/b/q0/b;->d:Ljava/lang/String;

    const-string v5, "https://discord.new"

    invoke-virtual {v0, v5}, Lf/a/b/q0/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf/a/b/q0/b;->e:Ljava/lang/String;

    const-string v5, "."

    const-string v6, "\\."

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-static {v1, v5, v6, v7, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lf/a/b/q0/b;->f:Ljava/lang/String;

    invoke-static {v2, v5, v6, v7, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lf/a/b/q0/b;->g:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "(?:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7c

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lf/a/b/q0/b;->h:Ljava/lang/String;

    invoke-static {v3, v5, v6, v7, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lf/a/b/q0/b;->i:Ljava/lang/String;

    invoke-static {v4, v5, v6, v7, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lf/a/b/q0/b;->j:Ljava/lang/String;

    invoke-static {v0, v5, v6, v7, v8}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf/a/b/q0/b;->k:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "^(?:ptb|canary|www)."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x24

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lx/s/f;->d:Lx/s/f;

    new-instance v6, Lkotlin/text/Regex;

    invoke-direct {v6, v4, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v6, Lf/a/b/q0/b;->l:Lkotlin/text/Regex;

    new-instance v4, Lkotlin/text/Regex;

    const-string v6, "^/(?:(invite|gift|template)/)?([\\w-]+)/?$"

    invoke-direct {v4, v6, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v4, Lf/a/b/q0/b;->m:Lkotlin/text/Regex;

    new-instance v4, Lkotlin/text/Regex;

    const-string v6, "^/(?:invite/)?([\\w-]+)/?$"

    invoke-direct {v4, v6, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v4, Lf/a/b/q0/b;->n:Lkotlin/text/Regex;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "(?:https?://(?:(?:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "/invite)|"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "))|"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "(?:^"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "))/([\\w-]+)/?"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "StringBuilder()\n        \u2026?\")\n          .toString()"

    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v9, Lkotlin/text/Regex;

    invoke-direct {v9, v4, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v9, Lf/a/b/q0/b;->o:Lkotlin/text/Regex;

    new-instance v4, Lkotlin/text/Regex;

    const-string v9, "^/(?:template/)?([\\w-]+)/?$"

    invoke-direct {v4, v9, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v4, Lf/a/b/q0/b;->p:Lkotlin/text/Regex;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "/template)|"

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lkotlin/text/Regex;

    invoke-direct {v4, v0, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v4, Lf/a/b/q0/b;->q:Lkotlin/text/Regex;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "(?:https?://)?(?:(?:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/gifts)|"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lkotlin/text/Regex;

    invoke-direct {v1, v0, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v1, Lf/a/b/q0/b;->r:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/connect(?:/(\\d+))?/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->s:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/channels/((?:@me)|(?:\\d+))/(\\d+)(?:/(\\d+))?/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->t:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/channels/@me/-1?/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->u:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/lurk/(\\d+)(?:/(\\d+))?/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->v:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/channels/@me/user/(\\d+)/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->w:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/users/(\\d+)/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->x:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/settings(/\\w+)*/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->y:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/feature(/\\w+)*/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->z:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/query(/\\w+)*/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->A:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/(?:oauth2|connect)/authorize/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->B:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/ra/([\\w-]+)$"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lf/a/b/q0/b;->C:Lkotlin/text/Regex;

    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^/guild/((?:@me)|(?:\\d+))/premiumguild/?$"

    invoke-direct {v0, v1, v5}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lx/s/f;)V

    sput-object v0, Lf/a/b/q0/b;->D:Lkotlin/text/Regex;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :cond_0
    sget-object v0, Lf/a/b/q0/b;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lf/a/b/q0/b;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    sget-object v0, Lf/a/b/q0/b;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    sget-object v0, Lf/a/b/q0/b;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_4
    sget-object v0, Lf/a/b/q0/b;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :goto_0
    const/4 p1, 0x1

    goto :goto_1

    :cond_5
    sget-object v0, Lf/a/b/q0/b;->l:Lkotlin/text/Regex;

    invoke-virtual {v0, p1}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result p1

    :goto_1
    return p1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v0, "Uri.parse(this)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1
.end method
