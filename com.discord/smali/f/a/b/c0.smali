.class public final Lf/a/b/c0;
.super Ljava/lang/Object;
.source "AppTransformers.kt"

# interfaces
.implements Lrx/Observable$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$c<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lkotlin/jvm/functions/Function1;

.field public final synthetic g:Lkotlin/jvm/functions/Function1;

.field public final synthetic h:Lkotlin/jvm/functions/Function1;

.field public final synthetic i:Lkotlin/jvm/functions/Function0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lf/a/b/c0;->d:Landroid/content/Context;

    iput-object p2, p0, Lf/a/b/c0;->e:Ljava/lang/String;

    iput-object p3, p0, Lf/a/b/c0;->f:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lf/a/b/c0;->g:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lf/a/b/c0;->h:Lkotlin/jvm/functions/Function1;

    iput-object p6, p0, Lf/a/b/c0;->i:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    move-object v0, p1

    check-cast v0, Lrx/Observable;

    const-string p1, "observable"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lf/a/b/c0;->d:Landroid/content/Context;

    iget-object v2, p0, Lf/a/b/c0;->e:Ljava/lang/String;

    iget-object v3, p0, Lf/a/b/c0;->f:Lkotlin/jvm/functions/Function1;

    iget-object v4, p0, Lf/a/b/c0;->g:Lkotlin/jvm/functions/Function1;

    iget-object v5, p0, Lf/a/b/c0;->h:Lkotlin/jvm/functions/Function1;

    iget-object p1, p0, Lf/a/b/c0;->i:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lf/a/b/b0;->d:Lf/a/b/b0;

    :goto_0
    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    const/4 p1, 0x0

    return-object p1
.end method
