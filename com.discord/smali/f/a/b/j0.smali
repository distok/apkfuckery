.class public final Lf/a/b/j0;
.super Ljava/lang/Object;
.source "AppTransformers.kt"

# interfaces
.implements Lrx/Observable$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$c<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/discord/utilities/dimmer/DimmerView;

.field public final synthetic e:J


# direct methods
.method public constructor <init>(Lcom/discord/utilities/dimmer/DimmerView;J)V
    .locals 0

    iput-object p1, p0, Lf/a/b/j0;->d:Lcom/discord/utilities/dimmer/DimmerView;

    iput-wide p2, p0, Lf/a/b/j0;->e:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    check-cast p1, Lrx/Observable;

    new-instance v8, Lcom/discord/utilities/rx/OnDelayedEmissionHandler;

    new-instance v1, Lf/a/b/i0;

    invoke-direct {v1, p0}, Lf/a/b/i0;-><init>(Lf/a/b/j0;)V

    iget-wide v2, p0, Lf/a/b/j0;->e:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/utilities/rx/OnDelayedEmissionHandler;-><init>(Lkotlin/jvm/functions/Function1;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    new-instance v0, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v0, p1, v8}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v0}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
