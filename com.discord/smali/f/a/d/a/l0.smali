.class public final synthetic Lf/a/d/a/l0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/discord/models/domain/Model$JsonReader$KeySelector;


# static fields
.field public static final synthetic a:Lf/a/d/a/l0;


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/d/a/l0;

    invoke-direct {v0}, Lf/a/d/a/l0;-><init>()V

    sput-object v0, Lf/a/d/a/l0;->a:Lf/a/d/a/l0;

    return-void
.end method

.method public synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/discord/models/domain/ModelMessageReaction;

    sget-object v0, Lcom/discord/models/domain/ModelMessage;->EVERYONE:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessageReaction$Emoji;->getKey()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
