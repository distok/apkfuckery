.class public Lf/d/a/j;
.super Landroid/graphics/drawable/Drawable;
.source "LottieDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/j$o;
    }
.end annotation


# instance fields
.field public final d:Landroid/graphics/Matrix;

.field public e:Lf/d/a/d;

.field public final f:Lf/d/a/b0/d;

.field public g:F

.field public h:Z

.field public i:Z

.field public final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lf/d/a/j$o;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field public l:Landroid/widget/ImageView$ScaleType;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public m:Lf/d/a/x/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public o:Lf/d/a/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public p:Lf/d/a/x/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Lf/d/a/y/l/c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lf/d/a/j;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/d/a/j;->d:Landroid/graphics/Matrix;

    new-instance v0, Lf/d/a/b0/d;

    invoke-direct {v0}, Lf/d/a/b0/d;-><init>()V

    iput-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lf/d/a/j;->g:F

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/d/a/j;->h:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lf/d/a/j;->i:Z

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v3, Lf/d/a/j$f;

    invoke-direct {v3, p0}, Lf/d/a/j$f;-><init>(Lf/d/a/j;)V

    iput-object v3, p0, Lf/d/a/j;->k:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    const/16 v4, 0xff

    iput v4, p0, Lf/d/a/j;->s:I

    iput-boolean v1, p0, Lf/d/a/j;->v:Z

    iput-boolean v2, p0, Lf/d/a/j;->w:Z

    iget-object v0, v0, Lf/d/a/b0/a;->d:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/y/e;Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/d/a/y/e;",
            "TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$e;

    invoke-direct {v1, p0, p1, p2, p3}, Lf/d/a/j$e;-><init>(Lf/d/a/j;Lf/d/a/y/e;Ljava/lang/Object;Lf/d/a/c0/c;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    sget-object v1, Lf/d/a/y/e;->c:Lf/d/a/y/e;

    const/4 v2, 0x1

    if-ne p1, v1, :cond_1

    invoke-virtual {v0, p2, p3}, Lf/d/a/y/l/c;->g(Ljava/lang/Object;Lf/d/a/c0/c;)V

    goto :goto_1

    :cond_1
    iget-object v1, p1, Lf/d/a/y/e;->b:Lf/d/a/y/f;

    if-eqz v1, :cond_2

    invoke-interface {v1, p2, p3}, Lf/d/a/y/f;->g(Ljava/lang/Object;Lf/d/a/c0/c;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    if-nez v0, :cond_3

    const-string p1, "Cannot resolve KeyPath. Composition is not set yet."

    invoke-static {p1}, Lf/d/a/b0/c;->b(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    new-instance v4, Lf/d/a/y/e;

    new-array v5, v1, [Ljava/lang/String;

    invoke-direct {v4, v5}, Lf/d/a/y/e;-><init>([Ljava/lang/String;)V

    invoke-virtual {v3, p1, v1, v0, v4}, Lf/d/a/y/l/b;->c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V

    move-object p1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/y/e;

    iget-object v0, v0, Lf/d/a/y/e;->b:Lf/d/a/y/f;

    invoke-interface {v0, p2, p3}, Lf/d/a/y/f;->g(Ljava/lang/Object;Lf/d/a/c0/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/2addr v2, p1

    :goto_1
    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lf/d/a/j;->invalidateSelf()V

    sget-object p1, Lf/d/a/o;->A:Ljava/lang/Float;

    if-ne p2, p1, :cond_5

    invoke-virtual {p0}, Lf/d/a/j;->g()F

    move-result p1

    invoke-virtual {p0, p1}, Lf/d/a/j;->u(F)V

    :cond_5
    return-void
.end method

.method public final b()V
    .locals 28

    move-object/from16 v0, p0

    new-instance v1, Lf/d/a/y/l/c;

    iget-object v2, v0, Lf/d/a/j;->e:Lf/d/a/d;

    move-object v4, v2

    sget-object v3, Lf/d/a/a0/r;->a:Lf/d/a/a0/h0/c$a;

    iget-object v5, v2, Lf/d/a/d;->j:Landroid/graphics/Rect;

    new-instance v15, Lf/d/a/y/l/e;

    move-object v2, v15

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    sget-object v8, Lf/d/a/y/l/e$a;->d:Lf/d/a/y/l/e$a;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v12

    new-instance v16, Lf/d/a/y/j/l;

    move-object/from16 v13, v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-direct/range {v16 .. v25}, Lf/d/a/y/j/l;-><init>(Lf/d/a/y/j/e;Lf/d/a/y/j/m;Lf/d/a/y/j/g;Lf/d/a/y/j/b;Lf/d/a/y/j/d;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v19

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v20

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v23

    sget-object v24, Lf/d/a/y/l/e$b;->d:Lf/d/a/y/l/e$b;

    const-string v5, "__container"

    const-wide/16 v6, -0x1

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v17, 0x0

    move/from16 v16, v17

    move-object/from16 v27, v15

    move/from16 v15, v17

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v26, 0x0

    invoke-direct/range {v2 .. v26}, Lf/d/a/y/l/e;-><init>(Ljava/util/List;Lf/d/a/d;Ljava/lang/String;JLf/d/a/y/l/e$a;JLjava/lang/String;Ljava/util/List;Lf/d/a/y/j/l;IIIFFIILf/d/a/y/j/j;Lf/d/a/y/j/k;Ljava/util/List;Lf/d/a/y/l/e$b;Lf/d/a/y/j/b;Z)V

    iget-object v2, v0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v3, v2, Lf/d/a/d;->i:Ljava/util/List;

    move-object/from16 v4, v27

    invoke-direct {v1, v0, v4, v3, v2}, Lf/d/a/y/l/c;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;Ljava/util/List;Lf/d/a/d;)V

    iput-object v1, v0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    iget-boolean v1, v0, Lf/d/a/b0/d;->n:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lf/d/a/b0/d;->cancel()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iput-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    iput-object v0, p0, Lf/d/a/j;->m:Lf/d/a/x/b;

    iget-object v1, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    iput-object v0, v1, Lf/d/a/b0/d;->m:Lf/d/a/d;

    const/high16 v0, -0x31000000

    iput v0, v1, Lf/d/a/b0/d;->k:F

    const/high16 v0, 0x4f000000

    iput v0, v1, Lf/d/a/b0/d;->l:F

    invoke-virtual {p0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public final d(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    iget-object v1, p0, Lf/d/a/j;->l:Landroid/widget/ImageView$ScaleType;

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v5, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v5, v5, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v1, v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v6, v6, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    iget-boolean v6, p0, Lf/d/a/j;->v:Z

    if-eqz v6, :cond_2

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v6

    cmpg-float v7, v6, v4

    if-gez v7, :cond_1

    div-float v7, v4, v6

    div-float/2addr v1, v7

    div-float/2addr v5, v7

    goto :goto_0

    :cond_1
    const/high16 v7, 0x3f800000    # 1.0f

    :goto_0
    cmpl-float v4, v7, v4

    if-lez v4, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    mul-float v2, v4, v6

    mul-float v6, v6, v0

    sub-float/2addr v4, v2

    sub-float/2addr v0, v6

    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, v7, v7, v2, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    :cond_2
    iget-object v0, p0, Lf/d/a/j;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lf/d/a/j;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v5}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    iget-object v1, p0, Lf/d/a/j;->d:Landroid/graphics/Matrix;

    iget v2, p0, Lf/d/a/j;->s:I

    invoke-virtual {v0, p1, v1, v2}, Lf/d/a/y/l/b;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    if-lez v3, :cond_7

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_2

    :cond_3
    iget-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    if-nez v0, :cond_4

    goto :goto_2

    :cond_4
    iget v0, p0, Lf/d/a/j;->g:F

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v5, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v5, v5, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v1, v5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v6, v6, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    cmpl-float v5, v0, v1

    if-lez v5, :cond_5

    iget v0, p0, Lf/d/a/j;->g:F

    div-float/2addr v0, v1

    goto :goto_1

    :cond_5
    move v1, v0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    cmpl-float v4, v0, v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    iget-object v4, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v4, v4, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v2

    iget-object v5, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v5, v5, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v2

    mul-float v2, v4, v1

    mul-float v6, v5, v1

    iget v7, p0, Lf/d/a/j;->g:F

    mul-float v4, v4, v7

    sub-float/2addr v4, v2

    mul-float v7, v7, v5

    sub-float/2addr v7, v6

    invoke-virtual {p1, v4, v7}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, v0, v0, v2, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    :cond_6
    iget-object v0, p0, Lf/d/a/j;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lf/d/a/j;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    iget-object v1, p0, Lf/d/a/j;->d:Landroid/graphics/Matrix;

    iget v2, p0, Lf/d/a/j;->s:I

    invoke-virtual {v0, p1, v1, v2}, Lf/d/a/y/l/b;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    if-lez v3, :cond_7

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_7
    :goto_2
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/d/a/j;->w:Z

    iget-boolean v0, p0, Lf/d/a/j;->i:Z

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0, p1}, Lf/d/a/j;->d(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    sget-object p1, Lf/d/a/b0/c;->a:Lf/d/a/m;

    check-cast p1, Lf/d/a/b0/b;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lf/d/a/j;->d(Landroid/graphics/Canvas;)V

    :goto_0
    const-string p1, "Drawable#draw"

    invoke-static {p1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    return-void
.end method

.method public e()F
    .locals 1

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    invoke-virtual {v0}, Lf/d/a/b0/d;->h()F

    move-result v0

    return v0
.end method

.method public f()F
    .locals 1

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    invoke-virtual {v0}, Lf/d/a/b0/d;->i()F

    move-result v0

    return v0
.end method

.method public g()F
    .locals 1
    .annotation build Landroidx/annotation/FloatRange;
        from = 0.0
        to = 1.0
    .end annotation

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    invoke-virtual {v0}, Lf/d/a/b0/d;->g()F

    move-result v0

    return v0
.end method

.method public getAlpha()I
    .locals 1

    iget v0, p0, Lf/d/a/j;->s:I

    return v0
.end method

.method public getIntrinsicHeight()I
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lf/d/a/j;->g:F

    mul-float v0, v0, v1

    float-to-int v0, v0

    :goto_0
    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lf/d/a/j;->g:F

    mul-float v0, v0, v1

    float-to-int v0, v0

    :goto_0
    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public h()I
    .locals 1

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getRepeatCount()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-boolean v0, v0, Lf/d/a/b0/d;->n:Z

    return v0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-interface {p1, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public invalidateSelf()V
    .locals 1

    iget-boolean v0, p0, Lf/d/a/j;->w:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/d/a/j;->w:Z

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method public isRunning()Z
    .locals 1

    invoke-virtual {p0}, Lf/d/a/j;->i()Z

    move-result v0

    return v0
.end method

.method public j()V
    .locals 6
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$g;

    invoke-direct {v1, p0}, Lf/d/a/j$g;-><init>(Lf/d/a/j;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-boolean v0, p0, Lf/d/a/j;->h:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lf/d/a/j;->h()I

    move-result v0

    if-nez v0, :cond_5

    :cond_1
    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/d/a/b0/d;->n:Z

    invoke-virtual {v0}, Lf/d/a/b0/d;->j()Z

    move-result v1

    iget-object v2, v0, Lf/d/a/b0/a;->e:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x1a

    if-lt v4, v5, :cond_2

    invoke-interface {v3, v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;Z)V

    goto :goto_0

    :cond_2
    invoke-interface {v3, v0}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lf/d/a/b0/d;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lf/d/a/b0/d;->h()F

    move-result v1

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lf/d/a/b0/d;->i()F

    move-result v1

    :goto_1
    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lf/d/a/b0/d;->m(F)V

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lf/d/a/b0/d;->h:J

    const/4 v1, 0x0

    iput v1, v0, Lf/d/a/b0/d;->j:I

    invoke-virtual {v0}, Lf/d/a/b0/d;->k()V

    :cond_5
    iget-boolean v0, p0, Lf/d/a/j;->h:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    iget v0, v0, Lf/d/a/b0/d;->f:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    invoke-virtual {p0}, Lf/d/a/j;->f()F

    move-result v0

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lf/d/a/j;->e()F

    move-result v0

    :goto_2
    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lf/d/a/j;->l(I)V

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    invoke-virtual {v0}, Lf/d/a/b0/d;->f()V

    :cond_7
    return-void
.end method

.method public k()V
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lf/d/a/j;->r:Lf/d/a/y/l/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$h;

    invoke-direct {v1, p0}, Lf/d/a/j$h;-><init>(Lf/d/a/j;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-boolean v0, p0, Lf/d/a/j;->h:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lf/d/a/j;->h()I

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/d/a/b0/d;->n:Z

    invoke-virtual {v0}, Lf/d/a/b0/d;->k()V

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lf/d/a/b0/d;->h:J

    invoke-virtual {v0}, Lf/d/a/b0/d;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, v0, Lf/d/a/b0/d;->i:F

    invoke-virtual {v0}, Lf/d/a/b0/d;->i()F

    move-result v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lf/d/a/b0/d;->h()F

    move-result v1

    iput v1, v0, Lf/d/a/b0/d;->i:F

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lf/d/a/b0/d;->j()Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, v0, Lf/d/a/b0/d;->i:F

    invoke-virtual {v0}, Lf/d/a/b0/d;->h()F

    move-result v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lf/d/a/b0/d;->i()F

    move-result v1

    iput v1, v0, Lf/d/a/b0/d;->i:F

    :cond_3
    :goto_0
    iget-boolean v0, p0, Lf/d/a/j;->h:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    iget v0, v0, Lf/d/a/b0/d;->f:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    invoke-virtual {p0}, Lf/d/a/j;->f()F

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lf/d/a/j;->e()F

    move-result v0

    :goto_1
    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lf/d/a/j;->l(I)V

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    invoke-virtual {v0}, Lf/d/a/b0/d;->f()V

    :cond_5
    return-void
.end method

.method public l(I)V
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$c;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$c;-><init>(Lf/d/a/j;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Lf/d/a/b0/d;->m(F)V

    return-void
.end method

.method public m(I)V
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$k;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$k;-><init>(Lf/d/a/j;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    int-to-float p1, p1

    const v1, 0x3f7d70a4    # 0.99f

    add-float/2addr p1, v1

    iget v1, v0, Lf/d/a/b0/d;->k:F

    invoke-virtual {v0, v1, p1}, Lf/d/a/b0/d;->n(FF)V

    return-void
.end method

.method public n(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$n;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$n;-><init>(Lf/d/a/j;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Lf/d/a/d;->d(Ljava/lang/String;)Lf/d/a/y/h;

    move-result-object v0

    if-eqz v0, :cond_1

    iget p1, v0, Lf/d/a/y/h;->b:F

    iget v0, v0, Lf/d/a/y/h;->c:F

    add-float/2addr p1, v0

    float-to-int p1, p1

    invoke-virtual {p0, p1}, Lf/d/a/j;->m(I)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot find marker with name "

    const-string v2, "."

    invoke-static {v1, p1, v2}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public o(F)V
    .locals 2
    .param p1    # F
        .annotation build Landroidx/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$l;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$l;-><init>(Lf/d/a/j;F)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget v1, v0, Lf/d/a/d;->k:F

    iget v0, v0, Lf/d/a/d;->l:F

    invoke-static {v1, v0, p1}, Lf/d/a/b0/f;->e(FFF)F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p0, p1}, Lf/d/a/j;->m(I)V

    return-void
.end method

.method public p(II)V
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$b;

    invoke-direct {v1, p0, p1, p2}, Lf/d/a/j$b;-><init>(Lf/d/a/j;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    int-to-float p1, p1

    int-to-float p2, p2

    const v1, 0x3f7d70a4    # 0.99f

    add-float/2addr p2, v1

    invoke-virtual {v0, p1, p2}, Lf/d/a/b0/d;->n(FF)V

    return-void
.end method

.method public q(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$a;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$a;-><init>(Lf/d/a/j;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Lf/d/a/d;->d(Ljava/lang/String;)Lf/d/a/y/h;

    move-result-object v0

    if-eqz v0, :cond_1

    iget p1, v0, Lf/d/a/y/h;->b:F

    float-to-int p1, p1

    iget v0, v0, Lf/d/a/y/h;->c:F

    float-to-int v0, v0

    add-int/2addr v0, p1

    invoke-virtual {p0, p1, v0}, Lf/d/a/j;->p(II)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot find marker with name "

    const-string v2, "."

    invoke-static {v1, p1, v2}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public r(I)V
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$i;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$i;-><init>(Lf/d/a/j;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    int-to-float p1, p1

    iget v1, v0, Lf/d/a/b0/d;->l:F

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, p1, v1}, Lf/d/a/b0/d;->n(FF)V

    return-void
.end method

.method public s(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$m;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$m;-><init>(Lf/d/a/j;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Lf/d/a/d;->d(Ljava/lang/String;)Lf/d/a/y/h;

    move-result-object v0

    if-eqz v0, :cond_1

    iget p1, v0, Lf/d/a/y/h;->b:F

    float-to-int p1, p1

    invoke-virtual {p0, p1}, Lf/d/a/j;->r(I)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot find marker with name "

    const-string v2, "."

    invoke-static {v1, p1, v2}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Runnable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-interface {p1, p0, p2, p3, p4}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    return-void
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/IntRange;
            from = 0x0L
            to = 0xffL
        .end annotation
    .end param

    iput p1, p0, Lf/d/a/j;->s:I

    invoke-virtual {p0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const-string p1, "Use addColorFilter instead."

    invoke-static {p1}, Lf/d/a/b0/c;->b(Ljava/lang/String;)V

    return-void
.end method

.method public start()V
    .locals 0
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    invoke-virtual {p0}, Lf/d/a/j;->j()V

    return-void
.end method

.method public stop()V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    invoke-virtual {v0}, Lf/d/a/b0/d;->f()V

    return-void
.end method

.method public t(F)V
    .locals 2

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$j;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$j;-><init>(Lf/d/a/j;F)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget v1, v0, Lf/d/a/d;->k:F

    iget v0, v0, Lf/d/a/d;->l:F

    invoke-static {v1, v0, p1}, Lf/d/a/b0/f;->e(FFF)F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p0, p1}, Lf/d/a/j;->r(I)V

    return-void
.end method

.method public u(F)V
    .locals 3
    .param p1    # F
        .annotation build Landroidx/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/d/a/j;->j:Ljava/util/ArrayList;

    new-instance v1, Lf/d/a/j$d;

    invoke-direct {v1, p0, p1}, Lf/d/a/j$d;-><init>(Lf/d/a/j;F)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    iget-object v1, p0, Lf/d/a/j;->f:Lf/d/a/b0/d;

    iget v2, v0, Lf/d/a/d;->k:F

    iget v0, v0, Lf/d/a/d;->l:F

    invoke-static {v2, v0, p1}, Lf/d/a/b0/f;->e(FFF)F

    move-result p1

    invoke-virtual {v1, p1}, Lf/d/a/b0/d;->m(F)V

    const-string p1, "Drawable#setProgress"

    invoke-static {p1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Runnable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-interface {p1, p0, p2}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final v()V
    .locals 3

    iget-object v0, p0, Lf/d/a/j;->e:Lf/d/a/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lf/d/a/j;->g:F

    iget-object v0, v0, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    mul-float v0, v0, v1

    float-to-int v0, v0

    iget-object v2, p0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v2, v2, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v1

    float-to-int v1, v2

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-void
.end method
