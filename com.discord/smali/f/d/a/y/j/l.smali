.class public Lf/d/a/y/j/l;
.super Ljava/lang/Object;
.source "AnimatableTransform.java"

# interfaces
.implements Lf/d/a/y/k/b;


# instance fields
.field public final a:Lf/d/a/y/j/e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lf/d/a/y/j/m;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/y/j/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/d/a/y/j/g;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lf/d/a/y/j/d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final g:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final h:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lf/d/a/y/j/l;-><init>(Lf/d/a/y/j/e;Lf/d/a/y/j/m;Lf/d/a/y/j/g;Lf/d/a/y/j/b;Lf/d/a/y/j/d;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;)V

    return-void
.end method

.method public constructor <init>(Lf/d/a/y/j/e;Lf/d/a/y/j/m;Lf/d/a/y/j/g;Lf/d/a/y/j/b;Lf/d/a/y/j/d;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;)V
    .locals 0
    .param p1    # Lf/d/a/y/j/e;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lf/d/a/y/j/m;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lf/d/a/y/j/g;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lf/d/a/y/j/d;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/j/e;",
            "Lf/d/a/y/j/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;",
            "Lf/d/a/y/j/g;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/d;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/b;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/j/l;->a:Lf/d/a/y/j/e;

    iput-object p2, p0, Lf/d/a/y/j/l;->b:Lf/d/a/y/j/m;

    iput-object p3, p0, Lf/d/a/y/j/l;->c:Lf/d/a/y/j/g;

    iput-object p4, p0, Lf/d/a/y/j/l;->d:Lf/d/a/y/j/b;

    iput-object p5, p0, Lf/d/a/y/j/l;->e:Lf/d/a/y/j/d;

    iput-object p6, p0, Lf/d/a/y/j/l;->h:Lf/d/a/y/j/b;

    iput-object p7, p0, Lf/d/a/y/j/l;->i:Lf/d/a/y/j/b;

    iput-object p8, p0, Lf/d/a/y/j/l;->f:Lf/d/a/y/j/b;

    iput-object p9, p0, Lf/d/a/y/j/l;->g:Lf/d/a/y/j/b;

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method
