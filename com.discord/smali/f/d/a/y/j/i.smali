.class public Lf/d/a/y/j/i;
.super Ljava/lang/Object;
.source "AnimatableSplitDimensionPathValue.java"

# interfaces
.implements Lf/d/a/y/j/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/d/a/y/j/m<",
        "Landroid/graphics/PointF;",
        "Landroid/graphics/PointF;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/d/a/y/j/b;

.field public final b:Lf/d/a/y/j/b;


# direct methods
.method public constructor <init>(Lf/d/a/y/j/b;Lf/d/a/y/j/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/j/i;->a:Lf/d/a/y/j/b;

    iput-object p2, p0, Lf/d/a/y/j/i;->b:Lf/d/a/y/j/b;

    return-void
.end method


# virtual methods
.method public a()Lf/d/a/w/c/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/d/a/w/c/m;

    iget-object v1, p0, Lf/d/a/y/j/i;->a:Lf/d/a/y/j/b;

    invoke-virtual {v1}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object v1

    iget-object v2, p0, Lf/d/a/y/j/i;->b:Lf/d/a/y/j/b;

    invoke-virtual {v2}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lf/d/a/w/c/m;-><init>(Lf/d/a/w/c/a;Lf/d/a/w/c/a;)V

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/d/a/c0/a<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot call getKeyframes on AnimatableSplitDimensionPathValue."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lf/d/a/y/j/i;->a:Lf/d/a/y/j/b;

    invoke-virtual {v0}, Lf/d/a/y/j/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d/a/y/j/i;->b:Lf/d/a/y/j/b;

    invoke-virtual {v0}, Lf/d/a/y/j/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
