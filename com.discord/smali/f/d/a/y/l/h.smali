.class public Lf/d/a/y/l/h;
.super Lf/d/a/y/l/b;
.source "SolidLayer.java"


# instance fields
.field public final A:Landroid/graphics/Path;

.field public final B:Lf/d/a/y/l/e;

.field public C:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/ColorFilter;",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field

.field public final x:Landroid/graphics/RectF;

.field public final y:Landroid/graphics/Paint;

.field public final z:[F


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/e;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lf/d/a/y/l/b;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lf/d/a/y/l/h;->x:Landroid/graphics/RectF;

    new-instance p1, Lf/d/a/w/a;

    invoke-direct {p1}, Lf/d/a/w/a;-><init>()V

    iput-object p1, p0, Lf/d/a/y/l/h;->y:Landroid/graphics/Paint;

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lf/d/a/y/l/h;->z:[F

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    iput-object p2, p0, Lf/d/a/y/l/h;->B:Lf/d/a/y/l/e;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget p2, p2, Lf/d/a/y/l/e;->l:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lf/d/a/y/l/b;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lf/d/a/y/l/h;->x:Landroid/graphics/RectF;

    iget-object p3, p0, Lf/d/a/y/l/h;->B:Lf/d/a/y/l/e;

    iget v0, p3, Lf/d/a/y/l/e;->j:I

    int-to-float v0, v0

    iget p3, p3, Lf/d/a/y/l/e;->k:I

    int-to-float p3, p3

    const/4 v1, 0x0

    invoke-virtual {p2, v1, v1, v0, p3}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object p2, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    iget-object p3, p0, Lf/d/a/y/l/h;->x:Landroid/graphics/RectF;

    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object p2, p0, Lf/d/a/y/l/h;->x:Landroid/graphics/RectF;

    invoke-virtual {p1, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v0, p1, p2}, Lf/d/a/w/c/o;->c(Ljava/lang/Object;Lf/d/a/c0/c;)Z

    sget-object v0, Lf/d/a/o;->C:Landroid/graphics/ColorFilter;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    if-nez p2, :cond_0

    iput-object p1, p0, Lf/d/a/y/l/h;->C:Lf/d/a/w/c/a;

    goto :goto_0

    :cond_0
    new-instance v0, Lf/d/a/w/c/p;

    invoke-direct {v0, p2, p1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/d/a/y/l/h;->C:Lf/d/a/w/c/a;

    :cond_1
    :goto_0
    return-void
.end method

.method public j(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 9

    iget-object v0, p0, Lf/d/a/y/l/h;->B:Lf/d/a/y/l/e;

    iget v0, v0, Lf/d/a/y/l/e;->l:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    iget-object v1, v1, Lf/d/a/w/c/o;->j:Lf/d/a/w/c/a;

    if-nez v1, :cond_1

    const/16 v1, 0x64

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    int-to-float p3, p3

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr p3, v2

    int-to-float v0, v0

    div-float/2addr v0, v2

    int-to-float v1, v1

    mul-float v0, v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    mul-float v0, v0, p3

    mul-float v0, v0, v2

    float-to-int p3, v0

    iget-object v0, p0, Lf/d/a/y/l/h;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lf/d/a/y/l/h;->C:Lf/d/a/w/c/a;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lf/d/a/y/l/h;->y:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :cond_2
    if-lez p3, :cond_3

    iget-object p3, p0, Lf/d/a/y/l/h;->z:[F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, p3, v0

    const/4 v2, 0x1

    aput v1, p3, v2

    iget-object v3, p0, Lf/d/a/y/l/h;->B:Lf/d/a/y/l/e;

    iget v4, v3, Lf/d/a/y/l/e;->j:I

    int-to-float v5, v4

    const/4 v6, 0x2

    aput v5, p3, v6

    const/4 v5, 0x3

    aput v1, p3, v5

    int-to-float v4, v4

    const/4 v7, 0x4

    aput v4, p3, v7

    iget v3, v3, Lf/d/a/y/l/e;->k:I

    int-to-float v4, v3

    const/4 v8, 0x5

    aput v4, p3, v8

    const/4 v4, 0x6

    aput v1, p3, v4

    int-to-float v1, v3

    const/4 v3, 0x7

    aput v1, p3, v3

    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->mapPoints([F)V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    iget-object p3, p0, Lf/d/a/y/l/h;->z:[F

    aget v1, p3, v0

    aget p3, p3, v2

    invoke-virtual {p2, v1, p3}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    iget-object p3, p0, Lf/d/a/y/l/h;->z:[F

    aget v1, p3, v6

    aget p3, p3, v5

    invoke-virtual {p2, v1, p3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    iget-object p3, p0, Lf/d/a/y/l/h;->z:[F

    aget v1, p3, v7

    aget p3, p3, v8

    invoke-virtual {p2, v1, p3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    iget-object p3, p0, Lf/d/a/y/l/h;->z:[F

    aget v1, p3, v4

    aget p3, p3, v3

    invoke-virtual {p2, v1, p3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    iget-object p3, p0, Lf/d/a/y/l/h;->z:[F

    aget v0, p3, v0

    aget p3, p3, v2

    invoke-virtual {p2, v0, p3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    iget-object p2, p0, Lf/d/a/y/l/h;->A:Landroid/graphics/Path;

    iget-object p3, p0, Lf/d/a/y/l/h;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_3
    return-void
.end method
