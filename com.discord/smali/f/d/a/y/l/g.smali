.class public Lf/d/a/y/l/g;
.super Lf/d/a/y/l/b;
.source "ShapeLayer.java"


# instance fields
.field public final x:Lf/d/a/w/b/d;


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/e;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lf/d/a/y/l/b;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    new-instance v0, Lf/d/a/y/k/n;

    iget-object p2, p2, Lf/d/a/y/l/e;->a:Ljava/util/List;

    const-string v1, "__container"

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lf/d/a/y/k/n;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    new-instance p2, Lf/d/a/w/b/d;

    invoke-direct {p2, p1, p0, v0}, Lf/d/a/w/b/d;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/n;)V

    iput-object p2, p0, Lf/d/a/y/l/g;->x:Lf/d/a/w/b/d;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lf/d/a/w/b/d;->b(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lf/d/a/y/l/b;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lf/d/a/y/l/g;->x:Lf/d/a/w/b/d;

    iget-object v0, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    invoke-virtual {p2, p1, v0, p3}, Lf/d/a/w/b/d;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    return-void
.end method

.method public j(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/d/a/y/l/g;->x:Lf/d/a/w/b/d;

    invoke-virtual {v0, p1, p2, p3}, Lf/d/a/w/b/d;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    return-void
.end method

.method public n(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/y/l/g;->x:Lf/d/a/w/b/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lf/d/a/w/b/d;->c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V

    return-void
.end method
