.class public abstract Lf/d/a/y/l/b;
.super Ljava/lang/Object;
.source "BaseLayer.java"

# interfaces
.implements Lf/d/a/w/b/e;
.implements Lf/d/a/w/c/a$b;
.implements Lf/d/a/y/f;


# instance fields
.field public final a:Landroid/graphics/Path;

.field public final b:Landroid/graphics/Matrix;

.field public final c:Landroid/graphics/Paint;

.field public final d:Landroid/graphics/Paint;

.field public final e:Landroid/graphics/Paint;

.field public final f:Landroid/graphics/Paint;

.field public final g:Landroid/graphics/Paint;

.field public final h:Landroid/graphics/RectF;

.field public final i:Landroid/graphics/RectF;

.field public final j:Landroid/graphics/RectF;

.field public final k:Landroid/graphics/RectF;

.field public final l:Ljava/lang/String;

.field public final m:Landroid/graphics/Matrix;

.field public final n:Lf/d/a/j;

.field public final o:Lf/d/a/y/l/e;

.field public p:Lf/d/a/w/c/g;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public q:Lf/d/a/w/c/c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public r:Lf/d/a/y/l/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public s:Lf/d/a/y/l/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/y/l/b;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/w/c/a<",
            "**>;>;"
        }
    .end annotation
.end field

.field public final v:Lf/d/a/w/c/o;

.field public w:Z


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/e;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    new-instance v0, Lf/d/a/w/a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lf/d/a/w/a;-><init>(I)V

    iput-object v0, p0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    new-instance v0, Lf/d/a/w/a;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Lf/d/a/w/a;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, Lf/d/a/y/l/b;->d:Landroid/graphics/Paint;

    new-instance v0, Lf/d/a/w/a;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Lf/d/a/w/a;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v0, p0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    new-instance v0, Lf/d/a/w/a;

    invoke-direct {v0, v1}, Lf/d/a/w/a;-><init>(I)V

    iput-object v0, p0, Lf/d/a/y/l/b;->f:Landroid/graphics/Paint;

    new-instance v2, Lf/d/a/w/a;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Lf/d/a/w/a;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    iput-object v2, p0, Lf/d/a/y/l/b;->g:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lf/d/a/y/l/b;->j:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lf/d/a/y/l/b;->k:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    iput-boolean v1, p0, Lf/d/a/y/l/b;->w:Z

    iput-object p1, p0, Lf/d/a/y/l/b;->n:Lf/d/a/j;

    iput-object p2, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p2, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    const-string v3, "#draw"

    invoke-static {p1, v2, v3}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/y/l/b;->l:Ljava/lang/String;

    iget-object p1, p2, Lf/d/a/y/l/e;->u:Lf/d/a/y/l/e$b;

    sget-object v2, Lf/d/a/y/l/e$b;->f:Lf/d/a/y/l/e$b;

    if-ne p1, v2, :cond_0

    new-instance p1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    :goto_0
    iget-object p1, p2, Lf/d/a/y/l/e;->i:Lf/d/a/y/j/l;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/d/a/w/c/o;

    invoke-direct {v0, p1}, Lf/d/a/w/c/o;-><init>(Lf/d/a/y/j/l;)V

    iput-object v0, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v0, p0}, Lf/d/a/w/c/o;->b(Lf/d/a/w/c/a$b;)V

    iget-object p1, p2, Lf/d/a/y/l/e;->h:Ljava/util/List;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    new-instance p1, Lf/d/a/w/c/g;

    iget-object p2, p2, Lf/d/a/y/l/e;->h:Ljava/util/List;

    invoke-direct {p1, p2}, Lf/d/a/w/c/g;-><init>(Ljava/util/List;)V

    iput-object p1, p0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object p1, p1, Lf/d/a/w/c/g;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/d/a/w/c/a;

    iget-object p2, p2, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object p1, p1, Lf/d/a/w/c/g;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/d/a/w/c/a;

    invoke-virtual {p0, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p2, p2, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object p1, p1, Lf/d/a/y/l/e;->t:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    new-instance p1, Lf/d/a/w/c/c;

    iget-object p2, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object p2, p2, Lf/d/a/y/l/e;->t:Ljava/util/List;

    invoke-direct {p1, p2}, Lf/d/a/w/c/c;-><init>(Ljava/util/List;)V

    iput-object p1, p0, Lf/d/a/y/l/b;->q:Lf/d/a/w/c/c;

    iput-boolean v1, p1, Lf/d/a/w/c/a;->b:Z

    new-instance p2, Lf/d/a/y/l/a;

    invoke-direct {p2, p0}, Lf/d/a/y/l/a;-><init>(Lf/d/a/y/l/b;)V

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/b;->q:Lf/d/a/w/c/c;

    invoke-virtual {p1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    const/high16 p2, 0x3f800000    # 1.0f

    cmpl-float p1, p1, p2

    if-nez p1, :cond_3

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {p0, v1}, Lf/d/a/y/l/b;->p(Z)V

    iget-object p1, p0, Lf/d/a/y/l/b;->q:Lf/d/a/w/c/c;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    goto :goto_4

    :cond_4
    invoke-virtual {p0, v1}, Lf/d/a/y/l/b;->p(Z)V

    :goto_4
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lf/d/a/y/l/b;->n:Lf/d/a/j;

    invoke-virtual {v0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->e(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    const-string v1, "__container"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {p4, v0}, Lf/d/a/y/e;->a(Ljava/lang/String;)Lf/d/a/y/e;

    move-result-object p4

    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->c(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4, p0}, Lf/d/a/y/e;->g(Lf/d/a/y/f;)Lf/d/a/y/e;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->f(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->d(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p0, p1, v0, p3, p4}, Lf/d/a/y/l/b;->n(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V

    :cond_2
    return-void
.end method

.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iget-object p1, p0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p0}, Lf/d/a/y/l/b;->h()V

    iget-object p1, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    if-eqz p3, :cond_1

    iget-object p1, p0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :goto_0
    if-ltz p1, :cond_1

    iget-object p2, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    iget-object p3, p0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/d/a/y/l/b;

    iget-object p3, p3, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {p3}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/d/a/y/l/b;->s:Lf/d/a/y/l/b;

    if-eqz p1, :cond_1

    iget-object p2, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    iget-object p1, p1, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {p1}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    :cond_1
    iget-object p1, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    iget-object p2, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {p2}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    return-void
.end method

.method public e(Lf/d/a/w/c/a;)V
    .locals 1
    .param p1    # Lf/d/a/w/c/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/w/c/a<",
            "**>;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    iget-object v3, v0, Lf/d/a/y/l/b;->l:Ljava/lang/String;

    iget-boolean v4, v0, Lf/d/a/y/l/b;->w:Z

    if-eqz v4, :cond_1f

    iget-object v4, v0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-boolean v4, v4, Lf/d/a/y/l/e;->v:Z

    if-eqz v4, :cond_0

    goto/16 :goto_d

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/y/l/b;->h()V

    iget-object v3, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    iget-object v3, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, v0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    :goto_0
    if-ltz v3, :cond_1

    iget-object v5, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    iget-object v6, v0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/d/a/y/l/b;

    iget-object v6, v6, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v6}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_1
    const-string v3, "Layer#parentMatrix"

    invoke-static {v3}, Lf/d/a/c;->a(Ljava/lang/String;)F

    iget-object v3, v0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    iget-object v3, v3, Lf/d/a/w/c/o;->j:Lf/d/a/w/c/a;

    if-nez v3, :cond_2

    const/16 v3, 0x64

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :goto_1
    move/from16 v5, p3

    int-to-float v5, v5

    const/high16 v6, 0x437f0000    # 255.0f

    div-float/2addr v5, v6

    int-to-float v3, v3

    mul-float v5, v5, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v5, v3

    mul-float v5, v5, v6

    float-to-int v3, v5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/y/l/b;->l()Z

    move-result v5

    const-string v6, "Layer#drawLayer"

    const/4 v7, 0x0

    if-nez v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/y/l/b;->k()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v2, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    iget-object v4, v0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v4}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    iget-object v2, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v2, v3}, Lf/d/a/y/l/b;->j(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    invoke-static {v6}, Lf/d/a/c;->a(Ljava/lang/String;)F

    iget-object v1, v0, Lf/d/a/y/l/b;->l:Ljava/lang/String;

    invoke-static {v1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    invoke-virtual {v0, v7}, Lf/d/a/y/l/b;->m(F)V

    return-void

    :cond_3
    iget-object v5, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v8, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    const/4 v9, 0x0

    invoke-virtual {v0, v5, v8, v9}, Lf/d/a/y/l/b;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v5, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    invoke-virtual/range {p0 .. p0}, Lf/d/a/y/l/b;->l()Z

    move-result v8

    if-nez v8, :cond_4

    goto :goto_2

    :cond_4
    iget-object v8, v0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v8, v8, Lf/d/a/y/l/e;->u:Lf/d/a/y/l/e$b;

    sget-object v10, Lf/d/a/y/l/e$b;->f:Lf/d/a/y/l/e$b;

    if-ne v8, v10, :cond_5

    goto :goto_2

    :cond_5
    iget-object v8, v0, Lf/d/a/y/l/b;->j:Landroid/graphics/RectF;

    invoke-virtual {v8, v7, v7, v7, v7}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v8, v0, Lf/d/a/y/l/b;->r:Lf/d/a/y/l/b;

    iget-object v10, v0, Lf/d/a/y/l/b;->j:Landroid/graphics/RectF;

    invoke-virtual {v8, v10, v2, v4}, Lf/d/a/y/l/b;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v8, v0, Lf/d/a/y/l/b;->j:Landroid/graphics/RectF;

    invoke-virtual {v5, v8}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-virtual {v5, v7, v7, v7, v7}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_6
    :goto_2
    iget-object v5, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    iget-object v8, v0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v8}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    iget-object v5, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v8, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    iget-object v10, v0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    invoke-virtual {v10, v7, v7, v7, v7}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/y/l/b;->k()Z

    move-result v10

    const/4 v11, 0x3

    const/4 v12, 0x2

    if-nez v10, :cond_7

    goto/16 :goto_7

    :cond_7
    iget-object v10, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v10, v10, Lf/d/a/w/c/g;->c:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    const/4 v13, 0x0

    :goto_3
    if-ge v13, v10, :cond_b

    iget-object v14, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v14, v14, Lf/d/a/w/c/g;->c:Ljava/util/List;

    invoke-interface {v14, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lf/d/a/y/k/g;

    iget-object v15, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v15, v15, Lf/d/a/w/c/g;->a:Ljava/util/List;

    invoke-interface {v15, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lf/d/a/w/c/a;

    invoke-virtual {v15}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/graphics/Path;

    iget-object v7, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v7, v15}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v7, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v7, v8}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-object v7, v14, Lf/d/a/y/k/g;->a:Lf/d/a/y/k/g$a;

    invoke-virtual {v7}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    if-eqz v7, :cond_8

    if-eq v7, v4, :cond_c

    if-eq v7, v12, :cond_8

    if-eq v7, v11, :cond_c

    goto :goto_4

    :cond_8
    iget-boolean v7, v14, Lf/d/a/y/k/g;->d:Z

    if-eqz v7, :cond_9

    goto :goto_6

    :cond_9
    :goto_4
    iget-object v7, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    iget-object v14, v0, Lf/d/a/y/l/b;->k:Landroid/graphics/RectF;

    invoke-virtual {v7, v14, v9}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    if-nez v13, :cond_a

    iget-object v7, v0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    iget-object v14, v0, Lf/d/a/y/l/b;->k:Landroid/graphics/RectF;

    invoke-virtual {v7, v14}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_5

    :cond_a
    iget-object v7, v0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    iget v14, v7, Landroid/graphics/RectF;->left:F

    iget-object v15, v0, Lf/d/a/y/l/b;->k:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->left:F

    invoke-static {v14, v15}, Ljava/lang/Math;->min(FF)F

    move-result v14

    iget-object v15, v0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->top:F

    iget-object v9, v0, Lf/d/a/y/l/b;->k:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    invoke-static {v15, v9}, Ljava/lang/Math;->min(FF)F

    move-result v9

    iget-object v15, v0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->right:F

    iget-object v11, v0, Lf/d/a/y/l/b;->k:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    invoke-static {v15, v11}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget-object v15, v0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->bottom:F

    iget-object v12, v0, Lf/d/a/y/l/b;->k:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->bottom:F

    invoke-static {v15, v12}, Ljava/lang/Math;->max(FF)F

    move-result v12

    invoke-virtual {v7, v14, v9, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_5
    add-int/lit8 v13, v13, 0x1

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x3

    const/4 v12, 0x2

    goto/16 :goto_3

    :cond_b
    iget-object v7, v0, Lf/d/a/y/l/b;->i:Landroid/graphics/RectF;

    invoke-virtual {v5, v7}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v7

    if-nez v7, :cond_c

    const/4 v7, 0x0

    invoke-virtual {v5, v7, v7, v7, v7}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_7

    :cond_c
    :goto_6
    const/4 v7, 0x0

    :goto_7
    iget-object v5, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v5, v7, v7, v8, v9}, Landroid/graphics/RectF;->intersect(FFFF)Z

    move-result v5

    if-nez v5, :cond_d

    iget-object v5, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    invoke-virtual {v5, v7, v7, v7, v7}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_d
    const-string v5, "Layer#computeBounds"

    invoke-static {v5}, Lf/d/a/c;->a(Ljava/lang/String;)F

    iget-object v5, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1e

    iget-object v5, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    const/16 v7, 0xff

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v5, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v8, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    const/16 v9, 0x1f

    invoke-static {v1, v5, v8, v9}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    const-string v5, "Layer#saveLayer"

    invoke-static {v5}, Lf/d/a/c;->a(Ljava/lang/String;)F

    invoke-virtual/range {p0 .. p1}, Lf/d/a/y/l/b;->i(Landroid/graphics/Canvas;)V

    iget-object v8, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v8, v3}, Lf/d/a/y/l/b;->j(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    invoke-static {v6}, Lf/d/a/c;->a(Ljava/lang/String;)F

    invoke-virtual/range {p0 .. p0}, Lf/d/a/y/l/b;->k()Z

    move-result v6

    const/16 v8, 0x13

    const-string v10, "Layer#restoreLayer"

    if-eqz v6, :cond_1c

    iget-object v6, v0, Lf/d/a/y/l/b;->b:Landroid/graphics/Matrix;

    iget-object v11, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v12, v0, Lf/d/a/y/l/b;->d:Landroid/graphics/Paint;

    invoke-static {v1, v11, v12, v8}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x1c

    if-ge v11, v12, :cond_e

    invoke-virtual/range {p0 .. p1}, Lf/d/a/y/l/b;->i(Landroid/graphics/Canvas;)V

    :cond_e
    invoke-static {v5}, Lf/d/a/c;->a(Ljava/lang/String;)F

    const/4 v11, 0x0

    :goto_8
    iget-object v12, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v12, v12, Lf/d/a/w/c/g;->c:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-ge v11, v12, :cond_1b

    iget-object v12, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v12, v12, Lf/d/a/w/c/g;->c:Ljava/util/List;

    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lf/d/a/y/k/g;

    iget-object v13, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v13, v13, Lf/d/a/w/c/g;->a:Ljava/util/List;

    invoke-interface {v13, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lf/d/a/w/c/a;

    iget-object v14, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v14, v14, Lf/d/a/w/c/g;->b:Ljava/util/List;

    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lf/d/a/w/c/a;

    iget-object v15, v12, Lf/d/a/y/k/g;->a:Lf/d/a/y/k/g$a;

    invoke-virtual {v15}, Ljava/lang/Enum;->ordinal()I

    move-result v15

    const v16, 0x40233333    # 2.55f

    if-eqz v15, :cond_18

    if-eq v15, v4, :cond_15

    const/4 v4, 0x2

    if-eq v15, v4, :cond_13

    const/4 v4, 0x3

    if-eq v15, v4, :cond_f

    goto/16 :goto_c

    :cond_f
    iget-object v12, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v12, v12, Lf/d/a/w/c/g;->a:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_10

    goto :goto_a

    :cond_10
    const/4 v12, 0x0

    :goto_9
    iget-object v13, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v13, v13, Lf/d/a/w/c/g;->c:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-ge v12, v13, :cond_12

    iget-object v13, v0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v13, v13, Lf/d/a/w/c/g;->c:Ljava/util/List;

    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lf/d/a/y/k/g;

    iget-object v13, v13, Lf/d/a/y/k/g;->a:Lf/d/a/y/k/g$a;

    sget-object v14, Lf/d/a/y/k/g$a;->g:Lf/d/a/y/k/g$a;

    if-eq v13, v14, :cond_11

    :goto_a
    const/4 v12, 0x0

    goto :goto_b

    :cond_11
    add-int/lit8 v12, v12, 0x1

    goto :goto_9

    :cond_12
    const/4 v12, 0x1

    :goto_b
    if-eqz v12, :cond_1a

    iget-object v12, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v12, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v12, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v13, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v12, v13}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_c

    :cond_13
    const/4 v4, 0x3

    iget-boolean v12, v12, Lf/d/a/y/k/g;->d:Z

    if-eqz v12, :cond_14

    iget-object v12, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v15, v0, Lf/d/a/y/l/b;->d:Landroid/graphics/Paint;

    invoke-static {v1, v12, v15, v9}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    iget-object v12, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v15, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v12, v15}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v12, v0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    invoke-virtual {v14}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    int-to-float v14, v14

    mul-float v14, v14, v16

    float-to-int v14, v14

    invoke-virtual {v12, v14}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {v13}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/Path;

    iget-object v13, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v13, v12}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v12, v6}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    iget-object v13, v0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v12, v13}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_c

    :cond_14
    iget-object v12, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v15, v0, Lf/d/a/y/l/b;->d:Landroid/graphics/Paint;

    invoke-static {v1, v12, v15, v9}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    invoke-virtual {v13}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/Path;

    iget-object v13, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v13, v12}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v12, v6}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-object v12, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v14}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    int-to-float v13, v13

    mul-float v13, v13, v16

    float-to-int v13, v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    iget-object v13, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v12, v13}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_c

    :cond_15
    const/4 v4, 0x3

    if-nez v11, :cond_16

    iget-object v15, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v4, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v4, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v15, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v15}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_16
    iget-boolean v4, v12, Lf/d/a/y/k/g;->d:Z

    if-eqz v4, :cond_17

    iget-object v4, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v12, v0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    invoke-static {v1, v4, v12, v9}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    iget-object v4, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v12, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v12}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    invoke-virtual {v14}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    int-to-float v12, v12

    mul-float v12, v12, v16

    float-to-int v12, v12

    invoke-virtual {v4, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {v13}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v12, v4}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v12}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_c

    :cond_17
    invoke-virtual {v13}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v12, v4}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v12}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_c

    :cond_18
    iget-boolean v4, v12, Lf/d/a/y/k/g;->d:Z

    if-eqz v4, :cond_19

    iget-object v4, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v12, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-static {v1, v4, v12, v9}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    iget-object v4, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v12, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v12}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {v13}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v12, v4}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v14}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    int-to-float v12, v12

    mul-float v12, v12, v16

    float-to-int v12, v12

    invoke-virtual {v4, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v12}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_c

    :cond_19
    invoke-virtual {v13}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v12, v4}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v14}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    int-to-float v12, v12

    mul-float v12, v12, v16

    float-to-int v12, v12

    invoke-virtual {v4, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v4, v0, Lf/d/a/y/l/b;->a:Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/y/l/b;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v12}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_1a
    :goto_c
    add-int/lit8 v11, v11, 0x1

    const/4 v4, 0x1

    goto/16 :goto_8

    :cond_1b
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    invoke-static {v10}, Lf/d/a/c;->a(Ljava/lang/String;)F

    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/y/l/b;->l()Z

    move-result v4

    if-eqz v4, :cond_1d

    iget-object v4, v0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget-object v6, v0, Lf/d/a/y/l/b;->f:Landroid/graphics/Paint;

    invoke-static {v1, v4, v6, v8}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    invoke-static {v5}, Lf/d/a/c;->a(Ljava/lang/String;)F

    invoke-virtual/range {p0 .. p1}, Lf/d/a/y/l/b;->i(Landroid/graphics/Canvas;)V

    iget-object v4, v0, Lf/d/a/y/l/b;->r:Lf/d/a/y/l/b;

    invoke-virtual {v4, v1, v2, v3}, Lf/d/a/y/l/b;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    invoke-static {v10}, Lf/d/a/c;->a(Ljava/lang/String;)F

    const-string v2, "Layer#drawMatte"

    invoke-static {v2}, Lf/d/a/c;->a(Ljava/lang/String;)F

    :cond_1d
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    invoke-static {v10}, Lf/d/a/c;->a(Ljava/lang/String;)F

    :cond_1e
    iget-object v1, v0, Lf/d/a/y/l/b;->l:Ljava/lang/String;

    invoke-static {v1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/d/a/y/l/b;->m(F)V

    return-void

    :cond_1f
    :goto_d
    invoke-static {v3}, Lf/d/a/c;->a(Ljava/lang/String;)F

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v0, p1, p2}, Lf/d/a/w/c/o;->c(Ljava/lang/Object;Lf/d/a/c0/c;)Z

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/y/l/b;->s:Lf/d/a/y/l/b;

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    iget-object v0, p0, Lf/d/a/y/l/b;->s:Lf/d/a/y/l/b;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lf/d/a/y/l/b;->t:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lf/d/a/y/l/b;->s:Lf/d/a/y/l/b;

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final i(Landroid/graphics/Canvas;)V
    .locals 9

    iget-object v0, p0, Lf/d/a/y/l/b;->h:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v4, v1, v2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    sub-float v5, v1, v2

    iget v1, v0, Landroid/graphics/RectF;->right:F

    add-float v6, v1, v2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    add-float v7, v0, v2

    iget-object v8, p0, Lf/d/a/y/l/b;->g:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    const-string p1, "Layer#clearLayer"

    invoke-static {p1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    return-void
.end method

.method public abstract j(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lf/d/a/w/c/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lf/d/a/y/l/b;->r:Lf/d/a/y/l/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final m(F)V
    .locals 6

    iget-object v0, p0, Lf/d/a/y/l/b;->n:Lf/d/a/j;

    iget-object v0, v0, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v0, v0, Lf/d/a/d;->a:Lf/d/a/s;

    iget-object v1, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v1, v1, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    iget-boolean v2, v0, Lf/d/a/s;->a:Z

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v2, v0, Lf/d/a/s;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/d/a/b0/e;

    if-nez v2, :cond_1

    new-instance v2, Lf/d/a/b0/e;

    invoke-direct {v2}, Lf/d/a/b0/e;-><init>()V

    iget-object v3, v0, Lf/d/a/s;->c:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget v3, v2, Lf/d/a/b0/e;->a:F

    add-float/2addr v3, p1

    iput v3, v2, Lf/d/a/b0/e;->a:F

    iget v4, v2, Lf/d/a/b0/e;->b:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lf/d/a/b0/e;->b:I

    const v5, 0x7fffffff

    if-ne v4, v5, :cond_2

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    iput v3, v2, Lf/d/a/b0/e;->a:F

    div-int/lit8 v4, v4, 0x2

    iput v4, v2, Lf/d/a/b0/e;->b:I

    :cond_2
    const-string v2, "__container"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, v0, Lf/d/a/s;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/s$a;

    invoke-interface {v1, p1}, Lf/d/a/s$a;->a(F)V

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method public n(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public o(F)V
    .locals 3
    .param p1    # F
        .annotation build Landroidx/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param

    iget-object v0, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    iget-object v1, v0, Lf/d/a/w/c/o;->j:Lf/d/a/w/c/a;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_0
    iget-object v1, v0, Lf/d/a/w/c/o;->m:Lf/d/a/w/c/a;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_1
    iget-object v1, v0, Lf/d/a/w/c/o;->n:Lf/d/a/w/c/a;

    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_2
    iget-object v1, v0, Lf/d/a/w/c/o;->f:Lf/d/a/w/c/a;

    if-eqz v1, :cond_3

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_3
    iget-object v1, v0, Lf/d/a/w/c/o;->g:Lf/d/a/w/c/a;

    if-eqz v1, :cond_4

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_4
    iget-object v1, v0, Lf/d/a/w/c/o;->h:Lf/d/a/w/c/a;

    if-eqz v1, :cond_5

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_5
    iget-object v1, v0, Lf/d/a/w/c/o;->i:Lf/d/a/w/c/a;

    if-eqz v1, :cond_6

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_6
    iget-object v1, v0, Lf/d/a/w/c/o;->k:Lf/d/a/w/c/c;

    if-eqz v1, :cond_7

    invoke-virtual {v1, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_7
    iget-object v0, v0, Lf/d/a/w/c/o;->l:Lf/d/a/w/c/c;

    if-eqz v0, :cond_8

    invoke-virtual {v0, p1}, Lf/d/a/w/c/a;->h(F)V

    :cond_8
    iget-object v0, p0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    const/4 v1, 0x0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v2, v2, Lf/d/a/w/c/g;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lf/d/a/y/l/b;->p:Lf/d/a/w/c/g;

    iget-object v2, v2, Lf/d/a/w/c/g;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/d/a/w/c/a;

    invoke-virtual {v2, p1}, Lf/d/a/w/c/a;->h(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget v0, v0, Lf/d/a/y/l/e;->m:F

    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_a

    div-float/2addr p1, v0

    :cond_a
    iget-object v2, p0, Lf/d/a/y/l/b;->q:Lf/d/a/w/c/c;

    if-eqz v2, :cond_b

    div-float v0, p1, v0

    invoke-virtual {v2, v0}, Lf/d/a/w/c/a;->h(F)V

    :cond_b
    iget-object v0, p0, Lf/d/a/y/l/b;->r:Lf/d/a/y/l/b;

    if-eqz v0, :cond_c

    iget-object v2, v0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget v2, v2, Lf/d/a/y/l/e;->m:F

    mul-float v2, v2, p1

    invoke-virtual {v0, v2}, Lf/d/a/y/l/b;->o(F)V

    :cond_c
    :goto_1
    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/c/a;

    invoke-virtual {v0, p1}, Lf/d/a/w/c/a;->h(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_d
    return-void
.end method

.method public final p(Z)V
    .locals 1

    iget-boolean v0, p0, Lf/d/a/y/l/b;->w:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lf/d/a/y/l/b;->w:Z

    iget-object p1, p0, Lf/d/a/y/l/b;->n:Lf/d/a/j;

    invoke-virtual {p1}, Lf/d/a/j;->invalidateSelf()V

    :cond_0
    return-void
.end method
