.class public Lf/d/a/y/l/e;
.super Ljava/lang/Object;
.source "Layer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/y/l/e$b;,
        Lf/d/a/y/l/e$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/y/k/b;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/d/a/d;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Lf/d/a/y/l/e$a;

.field public final f:J

.field public final g:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/y/k/g;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lf/d/a/y/j/l;

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:F

.field public final n:F

.field public final o:I

.field public final p:I

.field public final q:Lf/d/a/y/j/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final r:Lf/d/a/y/j/k;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final s:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/c0/a<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field public final u:Lf/d/a/y/l/e$b;

.field public final v:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Lf/d/a/d;Ljava/lang/String;JLf/d/a/y/l/e$a;JLjava/lang/String;Ljava/util/List;Lf/d/a/y/j/l;IIIFFIILf/d/a/y/j/j;Lf/d/a/y/j/k;Ljava/util/List;Lf/d/a/y/l/e$b;Lf/d/a/y/j/b;Z)V
    .locals 3
    .param p9    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p19    # Lf/d/a/y/j/j;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p20    # Lf/d/a/y/j/k;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p23    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/y/k/b;",
            ">;",
            "Lf/d/a/d;",
            "Ljava/lang/String;",
            "J",
            "Lf/d/a/y/l/e$a;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lf/d/a/y/k/g;",
            ">;",
            "Lf/d/a/y/j/l;",
            "IIIFFII",
            "Lf/d/a/y/j/j;",
            "Lf/d/a/y/j/k;",
            "Ljava/util/List<",
            "Lf/d/a/c0/a<",
            "Ljava/lang/Float;",
            ">;>;",
            "Lf/d/a/y/l/e$b;",
            "Lf/d/a/y/j/b;",
            "Z)V"
        }
    .end annotation

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    iput-object v1, v0, Lf/d/a/y/l/e;->a:Ljava/util/List;

    move-object v1, p2

    iput-object v1, v0, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    move-object v1, p3

    iput-object v1, v0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    move-wide v1, p4

    iput-wide v1, v0, Lf/d/a/y/l/e;->d:J

    move-object v1, p6

    iput-object v1, v0, Lf/d/a/y/l/e;->e:Lf/d/a/y/l/e$a;

    move-wide v1, p7

    iput-wide v1, v0, Lf/d/a/y/l/e;->f:J

    move-object v1, p9

    iput-object v1, v0, Lf/d/a/y/l/e;->g:Ljava/lang/String;

    move-object v1, p10

    iput-object v1, v0, Lf/d/a/y/l/e;->h:Ljava/util/List;

    move-object v1, p11

    iput-object v1, v0, Lf/d/a/y/l/e;->i:Lf/d/a/y/j/l;

    move v1, p12

    iput v1, v0, Lf/d/a/y/l/e;->j:I

    move/from16 v1, p13

    iput v1, v0, Lf/d/a/y/l/e;->k:I

    move/from16 v1, p14

    iput v1, v0, Lf/d/a/y/l/e;->l:I

    move/from16 v1, p15

    iput v1, v0, Lf/d/a/y/l/e;->m:F

    move/from16 v1, p16

    iput v1, v0, Lf/d/a/y/l/e;->n:F

    move/from16 v1, p17

    iput v1, v0, Lf/d/a/y/l/e;->o:I

    move/from16 v1, p18

    iput v1, v0, Lf/d/a/y/l/e;->p:I

    move-object/from16 v1, p19

    iput-object v1, v0, Lf/d/a/y/l/e;->q:Lf/d/a/y/j/j;

    move-object/from16 v1, p20

    iput-object v1, v0, Lf/d/a/y/l/e;->r:Lf/d/a/y/j/k;

    move-object/from16 v1, p21

    iput-object v1, v0, Lf/d/a/y/l/e;->t:Ljava/util/List;

    move-object/from16 v1, p22

    iput-object v1, v0, Lf/d/a/y/l/e;->u:Lf/d/a/y/l/e$b;

    move-object/from16 v1, p23

    iput-object v1, v0, Lf/d/a/y/l/e;->s:Lf/d/a/y/j/b;

    move/from16 v1, p24

    iput-boolean v1, v0, Lf/d/a/y/l/e;->v:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    iget-wide v3, p0, Lf/d/a/y/l/e;->f:J

    invoke-virtual {v2, v3, v4}, Lf/d/a/d;->e(J)Lf/d/a/y/l/e;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "\t\tParents: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    iget-wide v4, v2, Lf/d/a/y/l/e;->f:J

    invoke-virtual {v3, v4, v5}, Lf/d/a/d;->e(J)Lf/d/a/y/l/e;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_0

    const-string v3, "->"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lf/d/a/y/l/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    iget-wide v4, v2, Lf/d/a/y/l/e;->f:J

    invoke-virtual {v3, v4, v5}, Lf/d/a/d;->e(J)Lf/d/a/y/l/e;

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v2, p0, Lf/d/a/y/l/e;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tMasks: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lf/d/a/y/l/e;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget v2, p0, Lf/d/a/y/l/e;->j:I

    if-eqz v2, :cond_3

    iget v2, p0, Lf/d/a/y/l/e;->k:I

    if-eqz v2, :cond_3

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tBackground: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lf/d/a/y/l/e;->j:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lf/d/a/y/l/e;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lf/d/a/y/l/e;->l:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "%dx%d %X\n"

    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v2, p0, Lf/d/a/y/l/e;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\tShapes:\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lf/d/a/y/l/e;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\t\t"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    invoke-virtual {p0, v0}, Lf/d/a/y/l/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
