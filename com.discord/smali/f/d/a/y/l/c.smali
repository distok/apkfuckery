.class public Lf/d/a/y/l/c;
.super Lf/d/a/y/l/b;
.source "CompositionLayer.java"


# instance fields
.field public final A:Landroid/graphics/RectF;

.field public B:Landroid/graphics/Paint;

.field public x:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/y/l/b;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/e;Ljava/util/List;Lf/d/a/d;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/j;",
            "Lf/d/a/y/l/e;",
            "Ljava/util/List<",
            "Lf/d/a/y/l/e;",
            ">;",
            "Lf/d/a/d;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lf/d/a/y/l/b;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/c;->z:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/c;->A:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/c;->B:Landroid/graphics/Paint;

    iget-object p2, p2, Lf/d/a/y/l/e;->s:Lf/d/a/y/j/b;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object p2

    iput-object p2, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    invoke-virtual {p0, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p2, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    iget-object p2, p2, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v0, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    :goto_0
    new-instance p2, Landroidx/collection/LongSparseArray;

    iget-object v1, p4, Lf/d/a/d;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p2, v1}, Landroidx/collection/LongSparseArray;-><init>(I)V

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    move-object v3, v0

    :goto_1
    const/4 v4, 0x0

    if-ltz v1, :cond_a

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/d/a/y/l/e;

    iget-object v6, v5, Lf/d/a/y/l/e;->e:Lf/d/a/y/l/e$a;

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    const/4 v7, 0x2

    if-eqz v6, :cond_6

    if-eq v6, v2, :cond_5

    if-eq v6, v7, :cond_4

    const/4 v8, 0x3

    if-eq v6, v8, :cond_3

    const/4 v8, 0x4

    if-eq v6, v8, :cond_2

    const/4 v8, 0x5

    if-eq v6, v8, :cond_1

    const-string v6, "Unknown layer type "

    invoke-static {v6}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, v5, Lf/d/a/y/l/e;->e:Lf/d/a/y/l/e$a;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lf/d/a/b0/c;->b(Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_2

    :cond_1
    new-instance v6, Lf/d/a/y/l/i;

    invoke-direct {v6, p1, v5}, Lf/d/a/y/l/i;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    goto :goto_2

    :cond_2
    new-instance v6, Lf/d/a/y/l/g;

    invoke-direct {v6, p1, v5}, Lf/d/a/y/l/g;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    goto :goto_2

    :cond_3
    new-instance v6, Lf/d/a/y/l/f;

    invoke-direct {v6, p1, v5}, Lf/d/a/y/l/f;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    goto :goto_2

    :cond_4
    new-instance v6, Lf/d/a/y/l/d;

    invoke-direct {v6, p1, v5}, Lf/d/a/y/l/d;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    goto :goto_2

    :cond_5
    new-instance v6, Lf/d/a/y/l/h;

    invoke-direct {v6, p1, v5}, Lf/d/a/y/l/h;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    goto :goto_2

    :cond_6
    new-instance v6, Lf/d/a/y/l/c;

    iget-object v8, v5, Lf/d/a/y/l/e;->g:Ljava/lang/String;

    iget-object v9, p4, Lf/d/a/d;->c:Ljava/util/Map;

    invoke-interface {v9, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-direct {v6, p1, v5, v8, p4}, Lf/d/a/y/l/c;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;Ljava/util/List;Lf/d/a/d;)V

    :goto_2
    if-nez v6, :cond_7

    goto :goto_3

    :cond_7
    iget-object v8, v6, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-wide v8, v8, Lf/d/a/y/l/e;->d:J

    invoke-virtual {p2, v8, v9, v6}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    if-eqz v3, :cond_8

    iput-object v6, v3, Lf/d/a/y/l/b;->r:Lf/d/a/y/l/b;

    move-object v3, v0

    goto :goto_3

    :cond_8
    iget-object v8, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v8, v4, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v4, v5, Lf/d/a/y/l/e;->u:Lf/d/a/y/l/e$b;

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    if-eq v4, v2, :cond_9

    if-eq v4, v7, :cond_9

    goto :goto_3

    :cond_9
    move-object v3, v6

    :goto_3
    add-int/lit8 v1, v1, -0x1

    goto/16 :goto_1

    :cond_a
    :goto_4
    invoke-virtual {p2}, Landroidx/collection/LongSparseArray;->size()I

    move-result p1

    if-ge v4, p1, :cond_d

    invoke-virtual {p2, v4}, Landroidx/collection/LongSparseArray;->keyAt(I)J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/d/a/y/l/b;

    if-nez p1, :cond_b

    goto :goto_5

    :cond_b
    iget-object p3, p1, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-wide p3, p3, Lf/d/a/y/l/e;->f:J

    invoke-virtual {p2, p3, p4}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/d/a/y/l/b;

    if-eqz p3, :cond_c

    iput-object p3, p1, Lf/d/a/y/l/b;->s:Lf/d/a/y/l/b;

    :cond_c
    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_d
    return-void
.end method


# virtual methods
.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lf/d/a/y/l/b;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    sub-int/2addr p2, p3

    :goto_0
    if-ltz p2, :cond_0

    iget-object v0, p0, Lf/d/a/y/l/c;->z:Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/y/l/b;

    iget-object v1, p0, Lf/d/a/y/l/c;->z:Landroid/graphics/RectF;

    iget-object v2, p0, Lf/d/a/y/l/b;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v2, p3}, Lf/d/a/y/l/b;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v0, p0, Lf/d/a/y/l/c;->z:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v0, p1, p2}, Lf/d/a/w/c/o;->c(Ljava/lang/Object;Lf/d/a/c0/c;)Z

    sget-object v0, Lf/d/a/o;->A:Ljava/lang/Float;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    if-nez p2, :cond_0

    iget-object p2, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    if-eqz p2, :cond_1

    invoke-virtual {p2, p1}, Lf/d/a/w/c/a;->i(Lf/d/a/c0/c;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lf/d/a/w/c/p;

    invoke-direct {v0, p2, p1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    iget-object p1, v0, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public j(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 6

    iget-object v0, p0, Lf/d/a/y/l/c;->A:Landroid/graphics/RectF;

    iget-object v1, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget v2, v1, Lf/d/a/y/l/e;->o:I

    int-to-float v2, v2

    iget v1, v1, Lf/d/a/y/l/e;->p:I

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v2, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lf/d/a/y/l/c;->A:Landroid/graphics/RectF;

    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lf/d/a/y/l/b;->n:Lf/d/a/j;

    iget-boolean v0, v0, Lf/d/a/j;->u:Z

    const/16 v1, 0xff

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    if-eq p3, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v3, p0, Lf/d/a/y/l/c;->B:Landroid/graphics/Paint;

    invoke-virtual {v3, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v3, p0, Lf/d/a/y/l/c;->A:Landroid/graphics/RectF;

    iget-object v4, p0, Lf/d/a/y/l/c;->B:Landroid/graphics/Paint;

    const/16 v5, 0x1f

    invoke-static {p1, v3, v4, v5}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    :goto_1
    if-eqz v0, :cond_2

    const/16 p3, 0xff

    :cond_2
    iget-object v0, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v2

    :goto_2
    if-ltz v0, :cond_5

    iget-object v1, p0, Lf/d/a/y/l/c;->A:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lf/d/a/y/l/c;->A:Landroid/graphics/RectF;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_4

    iget-object v1, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/y/l/b;

    invoke-virtual {v1, p1, p2, p3}, Lf/d/a/y/l/b;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    const-string p1, "CompositionLayer#draw"

    invoke-static {p1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    return-void
.end method

.method public n(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/y/l/b;

    invoke-virtual {v1, p1, p2, p3, p4}, Lf/d/a/y/l/b;->c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public o(F)V
    .locals 3
    .param p1    # F
        .annotation build Landroidx/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param

    invoke-super {p0, p1}, Lf/d/a/y/l/b;->o(F)V

    iget-object v0, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/d/a/y/l/b;->n:Lf/d/a/j;

    iget-object p1, p1, Lf/d/a/j;->e:Lf/d/a/d;

    invoke-virtual {p1}, Lf/d/a/d;->c()F

    move-result p1

    const v0, 0x3c23d70a    # 0.01f

    add-float/2addr p1, v0

    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v0, v0, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    iget v0, v0, Lf/d/a/d;->k:F

    iget-object v1, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget-object v2, v2, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    iget v2, v2, Lf/d/a/d;->m:F

    mul-float v1, v1, v2

    sub-float/2addr v1, v0

    div-float p1, v1, p1

    :cond_0
    iget-object v0, p0, Lf/d/a/y/l/c;->x:Lf/d/a/w/c/a;

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget v1, v0, Lf/d/a/y/l/e;->n:F

    iget-object v0, v0, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    invoke-virtual {v0}, Lf/d/a/d;->c()F

    move-result v0

    div-float/2addr v1, v0

    sub-float/2addr p1, v1

    :cond_1
    iget-object v0, p0, Lf/d/a/y/l/b;->o:Lf/d/a/y/l/e;

    iget v0, v0, Lf/d/a/y/l/e;->m:F

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    div-float/2addr p1, v0

    :cond_2
    iget-object v0, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_3

    iget-object v1, p0, Lf/d/a/y/l/c;->y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/y/l/b;

    invoke-virtual {v1, p1}, Lf/d/a/y/l/b;->o(F)V

    goto :goto_0

    :cond_3
    return-void
.end method
