.class public Lf/d/a/y/l/i;
.super Lf/d/a/y/l/b;
.source "TextLayer.java"


# instance fields
.field public final A:Landroid/graphics/Paint;

.field public final B:Landroid/graphics/Paint;

.field public final C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lf/d/a/y/d;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/d;",
            ">;>;"
        }
    .end annotation
.end field

.field public final D:Landroidx/collection/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LongSparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final E:Lf/d/a/w/c/n;

.field public final F:Lf/d/a/j;

.field public final G:Lf/d/a/d;

.field public H:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public I:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public J:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public K:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public L:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public M:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public N:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public O:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public P:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final x:Ljava/lang/StringBuilder;

.field public final y:Landroid/graphics/RectF;

.field public final z:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/e;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lf/d/a/y/l/b;-><init>(Lf/d/a/j;Lf/d/a/y/l/e;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lf/d/a/y/l/i;->x:Ljava/lang/StringBuilder;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/i;->y:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/i;->z:Landroid/graphics/Matrix;

    new-instance v0, Lf/d/a/y/l/i$a;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lf/d/a/y/l/i$a;-><init>(Lf/d/a/y/l/i;I)V

    iput-object v0, p0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    new-instance v0, Lf/d/a/y/l/i$b;

    invoke-direct {v0, p0, v1}, Lf/d/a/y/l/i$b;-><init>(Lf/d/a/y/l/i;I)V

    iput-object v0, p0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/i;->C:Ljava/util/Map;

    new-instance v0, Landroidx/collection/LongSparseArray;

    invoke-direct {v0}, Landroidx/collection/LongSparseArray;-><init>()V

    iput-object v0, p0, Lf/d/a/y/l/i;->D:Landroidx/collection/LongSparseArray;

    iput-object p1, p0, Lf/d/a/y/l/i;->F:Lf/d/a/j;

    iget-object p1, p2, Lf/d/a/y/l/e;->b:Lf/d/a/d;

    iput-object p1, p0, Lf/d/a/y/l/i;->G:Lf/d/a/d;

    iget-object p1, p2, Lf/d/a/y/l/e;->q:Lf/d/a/y/j/j;

    new-instance v0, Lf/d/a/w/c/n;

    iget-object p1, p1, Lf/d/a/y/j/n;->a:Ljava/util/List;

    invoke-direct {v0, p1}, Lf/d/a/w/c/n;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lf/d/a/y/l/i;->E:Lf/d/a/w/c/n;

    iget-object p1, v0, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p2, Lf/d/a/y/l/e;->r:Lf/d/a/y/j/k;

    if-eqz p1, :cond_0

    iget-object p2, p1, Lf/d/a/y/j/k;->a:Lf/d/a/y/j/a;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lf/d/a/y/j/a;->a()Lf/d/a/w/c/a;

    move-result-object p2

    iput-object p2, p0, Lf/d/a/y/l/i;->H:Lf/d/a/w/c/a;

    iget-object p2, p2, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, p0, Lf/d/a/y/l/i;->H:Lf/d/a/w/c/a;

    invoke-virtual {p0, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object p2, p1, Lf/d/a/y/j/k;->b:Lf/d/a/y/j/a;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lf/d/a/y/j/a;->a()Lf/d/a/w/c/a;

    move-result-object p2

    iput-object p2, p0, Lf/d/a/y/l/i;->J:Lf/d/a/w/c/a;

    iget-object p2, p2, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, p0, Lf/d/a/y/l/i;->J:Lf/d/a/w/c/a;

    invoke-virtual {p0, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_1
    if-eqz p1, :cond_2

    iget-object p2, p1, Lf/d/a/y/j/k;->c:Lf/d/a/y/j/b;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object p2

    iput-object p2, p0, Lf/d/a/y/l/i;->L:Lf/d/a/w/c/a;

    iget-object p2, p2, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, p0, Lf/d/a/y/l/i;->L:Lf/d/a/w/c/a;

    invoke-virtual {p0, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_2
    if-eqz p1, :cond_3

    iget-object p1, p1, Lf/d/a/y/j/k;->d:Lf/d/a/y/j/b;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/y/l/i;->N:Lf/d/a/w/c/a;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/i;->N:Lf/d/a/w/c/a;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lf/d/a/y/l/b;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lf/d/a/y/l/i;->G:Lf/d/a/d;

    iget-object p2, p2, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p2

    int-to-float p2, p2

    iget-object p3, p0, Lf/d/a/y/l/i;->G:Lf/d/a/d;

    iget-object p3, p3, Lf/d/a/d;->j:Landroid/graphics/Rect;

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result p3

    int-to-float p3, p3

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, p2, p3}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 2
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    invoke-virtual {v0, p1, p2}, Lf/d/a/w/c/o;->c(Ljava/lang/Object;Lf/d/a/c0/c;)Z

    sget-object v0, Lf/d/a/o;->a:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lf/d/a/y/l/i;->I:Lf/d/a/w/c/a;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    if-nez p2, :cond_1

    iput-object v1, p0, Lf/d/a/y/l/i;->I:Lf/d/a/w/c/a;

    goto/16 :goto_0

    :cond_1
    new-instance p1, Lf/d/a/w/c/p;

    invoke-direct {p1, p2, v1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/d/a/y/l/i;->I:Lf/d/a/w/c/a;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/i;->I:Lf/d/a/w/c/a;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lf/d/a/o;->b:Ljava/lang/Integer;

    if-ne p1, v0, :cond_5

    iget-object p1, p0, Lf/d/a/y/l/i;->K:Lf/d/a/w/c/a;

    if-eqz p1, :cond_3

    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_3
    if-nez p2, :cond_4

    iput-object v1, p0, Lf/d/a/y/l/i;->K:Lf/d/a/w/c/a;

    goto/16 :goto_0

    :cond_4
    new-instance p1, Lf/d/a/w/c/p;

    invoke-direct {p1, p2, v1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/d/a/y/l/i;->K:Lf/d/a/w/c/a;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/i;->K:Lf/d/a/w/c/a;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lf/d/a/o;->o:Ljava/lang/Float;

    if-ne p1, v0, :cond_8

    iget-object p1, p0, Lf/d/a/y/l/i;->M:Lf/d/a/w/c/a;

    if-eqz p1, :cond_6

    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_6
    if-nez p2, :cond_7

    iput-object v1, p0, Lf/d/a/y/l/i;->M:Lf/d/a/w/c/a;

    goto :goto_0

    :cond_7
    new-instance p1, Lf/d/a/w/c/p;

    invoke-direct {p1, p2, v1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/d/a/y/l/i;->M:Lf/d/a/w/c/a;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/i;->M:Lf/d/a/w/c/a;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    goto :goto_0

    :cond_8
    sget-object v0, Lf/d/a/o;->p:Ljava/lang/Float;

    if-ne p1, v0, :cond_b

    iget-object p1, p0, Lf/d/a/y/l/i;->O:Lf/d/a/w/c/a;

    if-eqz p1, :cond_9

    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_9
    if-nez p2, :cond_a

    iput-object v1, p0, Lf/d/a/y/l/i;->O:Lf/d/a/w/c/a;

    goto :goto_0

    :cond_a
    new-instance p1, Lf/d/a/w/c/p;

    invoke-direct {p1, p2, v1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/d/a/y/l/i;->O:Lf/d/a/w/c/a;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/i;->O:Lf/d/a/w/c/a;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    goto :goto_0

    :cond_b
    sget-object v0, Lf/d/a/o;->B:Ljava/lang/Float;

    if-ne p1, v0, :cond_e

    iget-object p1, p0, Lf/d/a/y/l/i;->P:Lf/d/a/w/c/a;

    if-eqz p1, :cond_c

    iget-object v0, p0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_c
    if-nez p2, :cond_d

    iput-object v1, p0, Lf/d/a/y/l/i;->P:Lf/d/a/w/c/a;

    goto :goto_0

    :cond_d
    new-instance p1, Lf/d/a/w/c/p;

    invoke-direct {p1, p2, v1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/d/a/y/l/i;->P:Lf/d/a/w/c/a;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/y/l/i;->P:Lf/d/a/w/c/a;

    invoke-virtual {p0, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_e
    :goto_0
    return-void
.end method

.method public j(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    iget-object v2, v0, Lf/d/a/y/l/i;->F:Lf/d/a/j;

    iget-object v2, v2, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v2, v2, Lf/d/a/d;->g:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v2}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    invoke-virtual/range {p1 .. p2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    iget-object v2, v0, Lf/d/a/y/l/i;->E:Lf/d/a/w/c/n;

    invoke-virtual {v2}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/d/a/y/b;

    iget-object v3, v0, Lf/d/a/y/l/i;->G:Lf/d/a/d;

    iget-object v3, v3, Lf/d/a/d;->e:Ljava/util/Map;

    iget-object v4, v2, Lf/d/a/y/b;->b:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/d/a/y/c;

    if-nez v3, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_2
    iget-object v4, v0, Lf/d/a/y/l/i;->I:Lf/d/a/w/c/a;

    if-eqz v4, :cond_3

    iget-object v5, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_3
    iget-object v4, v0, Lf/d/a/y/l/i;->H:Lf/d/a/w/c/a;

    if-eqz v4, :cond_4

    iget-object v5, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_4
    iget-object v4, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    iget v5, v2, Lf/d/a/y/b;->h:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    iget-object v4, v0, Lf/d/a/y/l/i;->K:Lf/d/a/w/c/a;

    if-eqz v4, :cond_5

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2

    :cond_5
    iget-object v4, v0, Lf/d/a/y/l/i;->J:Lf/d/a/w/c/a;

    if-eqz v4, :cond_6

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2

    :cond_6
    iget-object v4, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    iget v5, v2, Lf/d/a/y/b;->i:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    :goto_2
    iget-object v4, v0, Lf/d/a/y/l/b;->v:Lf/d/a/w/c/o;

    iget-object v4, v4, Lf/d/a/w/c/o;->j:Lf/d/a/w/c/a;

    if-nez v4, :cond_7

    const/16 v4, 0x64

    goto :goto_3

    :cond_7
    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :goto_3
    mul-int/lit16 v4, v4, 0xff

    div-int/lit8 v4, v4, 0x64

    iget-object v5, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v4, v0, Lf/d/a/y/l/i;->M:Lf/d/a/w/c/a;

    if-eqz v4, :cond_8

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_4

    :cond_8
    iget-object v4, v0, Lf/d/a/y/l/i;->L:Lf/d/a/w/c/a;

    if-eqz v4, :cond_9

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_4

    :cond_9
    invoke-static/range {p2 .. p2}, Lf/d/a/b0/g;->d(Landroid/graphics/Matrix;)F

    move-result v4

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    iget v6, v2, Lf/d/a/y/b;->j:F

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v7

    mul-float v7, v7, v6

    mul-float v7, v7, v4

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :goto_4
    iget-object v4, v0, Lf/d/a/y/l/i;->F:Lf/d/a/j;

    iget-object v4, v4, Lf/d/a/j;->e:Lf/d/a/d;

    iget-object v4, v4, Lf/d/a/d;->g:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v4}, Landroidx/collection/SparseArrayCompat;->size()I

    move-result v4

    if-lez v4, :cond_a

    const/4 v4, 0x1

    goto :goto_5

    :cond_a
    const/4 v4, 0x0

    :goto_5
    if-eqz v4, :cond_16

    iget-object v4, v0, Lf/d/a/y/l/i;->P:Lf/d/a/w/c/a;

    if-eqz v4, :cond_b

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    goto :goto_6

    :cond_b
    iget v4, v2, Lf/d/a/y/b;->c:F

    :goto_6
    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    invoke-static/range {p2 .. p2}, Lf/d/a/b0/g;->d(Landroid/graphics/Matrix;)F

    move-result v5

    iget-object v6, v2, Lf/d/a/y/b;->a:Ljava/lang/String;

    iget v7, v2, Lf/d/a/y/b;->f:F

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v8

    mul-float v8, v8, v7

    invoke-virtual {v0, v6}, Lf/d/a/y/l/i;->t(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v9, 0x0

    :goto_7
    if-ge v9, v7, :cond_2c

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_8
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    if-ge v12, v13, :cond_d

    invoke-virtual {v10, v12}, Ljava/lang/String;->charAt(I)C

    move-result v13

    iget-object v14, v3, Lf/d/a/y/c;->a:Ljava/lang/String;

    iget-object v15, v3, Lf/d/a/y/c;->c:Ljava/lang/String;

    invoke-static {v13, v14, v15}, Lf/d/a/y/d;->a(CLjava/lang/String;Ljava/lang/String;)I

    move-result v13

    iget-object v14, v0, Lf/d/a/y/l/i;->G:Lf/d/a/d;

    iget-object v14, v14, Lf/d/a/d;->g:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v14, v13}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lf/d/a/y/d;

    if-nez v13, :cond_c

    move v13, v8

    move/from16 v16, v9

    move-object/from16 p3, v10

    goto :goto_9

    :cond_c
    float-to-double v14, v11

    move-object/from16 p3, v10

    iget-wide v10, v13, Lf/d/a/y/d;->c:D

    move v13, v8

    move/from16 v16, v9

    float-to-double v8, v4

    mul-double v10, v10, v8

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v8

    float-to-double v8, v8

    mul-double v10, v10, v8

    float-to-double v8, v5

    mul-double v10, v10, v8

    add-double/2addr v10, v14

    double-to-float v8, v10

    move v11, v8

    :goto_9
    add-int/lit8 v12, v12, 0x1

    move-object/from16 v10, p3

    move v8, v13

    move/from16 v9, v16

    goto :goto_8

    :cond_d
    move v13, v8

    move/from16 v16, v9

    move-object/from16 p3, v10

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    iget-object v8, v2, Lf/d/a/y/b;->d:Lf/d/a/y/b$a;

    invoke-virtual {v0, v8, v1, v11}, Lf/d/a/y/l/i;->q(Lf/d/a/y/b$a;Landroid/graphics/Canvas;F)V

    add-int/lit8 v8, v7, -0x1

    int-to-float v8, v8

    mul-float v8, v8, v13

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    move/from16 v9, v16

    int-to-float v10, v9

    mul-float v10, v10, v13

    sub-float/2addr v10, v8

    const/4 v8, 0x0

    invoke-virtual {v1, v8, v10}, Landroid/graphics/Canvas;->translate(FF)V

    const/4 v8, 0x0

    :goto_a
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v8, v10, :cond_15

    move-object/from16 v10, p3

    invoke-virtual {v10, v8}, Ljava/lang/String;->charAt(I)C

    move-result v11

    iget-object v12, v3, Lf/d/a/y/c;->a:Ljava/lang/String;

    iget-object v14, v3, Lf/d/a/y/c;->c:Ljava/lang/String;

    invoke-static {v11, v12, v14}, Lf/d/a/y/d;->a(CLjava/lang/String;Ljava/lang/String;)I

    move-result v11

    iget-object v12, v0, Lf/d/a/y/l/i;->G:Lf/d/a/d;

    iget-object v12, v12, Lf/d/a/d;->g:Landroidx/collection/SparseArrayCompat;

    invoke-virtual {v12, v11}, Landroidx/collection/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lf/d/a/y/d;

    if-nez v11, :cond_e

    move-object/from16 v14, p2

    move-object/from16 p3, v6

    move/from16 v17, v7

    move-object/from16 v16, v10

    goto/16 :goto_10

    :cond_e
    iget-object v12, v0, Lf/d/a/y/l/i;->C:Ljava/util/Map;

    invoke-interface {v12, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_f

    iget-object v12, v0, Lf/d/a/y/l/i;->C:Ljava/util/Map;

    invoke-interface {v12, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    move-object/from16 p3, v6

    move/from16 v17, v7

    move-object/from16 v16, v10

    goto :goto_c

    :cond_f
    iget-object v12, v11, Lf/d/a/y/d;->a:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v14

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v14}, Ljava/util/ArrayList;-><init>(I)V

    const/16 v16, 0x0

    move-object/from16 p3, v6

    const/4 v6, 0x0

    :goto_b
    if-ge v6, v14, :cond_10

    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    move/from16 v17, v7

    move-object/from16 v7, v16

    check-cast v7, Lf/d/a/y/k/n;

    move-object/from16 v16, v10

    new-instance v10, Lf/d/a/w/b/d;

    move-object/from16 v18, v12

    iget-object v12, v0, Lf/d/a/y/l/i;->F:Lf/d/a/j;

    invoke-direct {v10, v12, v0, v7}, Lf/d/a/w/b/d;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/n;)V

    invoke-virtual {v15, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v10, v16

    move/from16 v7, v17

    move-object/from16 v12, v18

    goto :goto_b

    :cond_10
    move/from16 v17, v7

    move-object/from16 v16, v10

    iget-object v6, v0, Lf/d/a/y/l/i;->C:Ljava/util/Map;

    invoke-interface {v6, v11, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v12, v15

    :goto_c
    const/4 v6, 0x0

    :goto_d
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_12

    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/d/a/w/b/d;

    invoke-virtual {v7}, Lf/d/a/w/b/d;->getPath()Landroid/graphics/Path;

    move-result-object v7

    iget-object v10, v0, Lf/d/a/y/l/i;->y:Landroid/graphics/RectF;

    const/4 v14, 0x0

    invoke-virtual {v7, v10, v14}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    iget-object v10, v0, Lf/d/a/y/l/i;->z:Landroid/graphics/Matrix;

    move-object/from16 v14, p2

    invoke-virtual {v10, v14}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v10, v0, Lf/d/a/y/l/i;->z:Landroid/graphics/Matrix;

    iget v15, v2, Lf/d/a/y/b;->g:F

    neg-float v15, v15

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v18

    mul-float v15, v15, v18

    move-object/from16 v18, v12

    const/4 v12, 0x0

    invoke-virtual {v10, v12, v15}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object v10, v0, Lf/d/a/y/l/i;->z:Landroid/graphics/Matrix;

    invoke-virtual {v10, v4, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v10, v0, Lf/d/a/y/l/i;->z:Landroid/graphics/Matrix;

    invoke-virtual {v7, v10}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-boolean v10, v2, Lf/d/a/y/b;->k:Z

    if-eqz v10, :cond_11

    iget-object v10, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v0, v7, v10, v1}, Lf/d/a/y/l/i;->s(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v10, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v0, v7, v10, v1}, Lf/d/a/y/l/i;->s(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    goto :goto_e

    :cond_11
    iget-object v10, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v0, v7, v10, v1}, Lf/d/a/y/l/i;->s(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v10, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v0, v7, v10, v1}, Lf/d/a/y/l/i;->s(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    :goto_e
    add-int/lit8 v6, v6, 0x1

    move-object/from16 v12, v18

    goto :goto_d

    :cond_12
    move-object/from16 v14, p2

    iget-wide v6, v11, Lf/d/a/y/d;->c:D

    double-to-float v6, v6

    mul-float v6, v6, v4

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v7

    mul-float v7, v7, v6

    mul-float v7, v7, v5

    iget v6, v2, Lf/d/a/y/b;->e:I

    int-to-float v6, v6

    const/high16 v10, 0x41200000    # 10.0f

    div-float/2addr v6, v10

    iget-object v10, v0, Lf/d/a/y/l/i;->O:Lf/d/a/w/c/a;

    if-eqz v10, :cond_13

    invoke-virtual {v10}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    goto :goto_f

    :cond_13
    iget-object v10, v0, Lf/d/a/y/l/i;->N:Lf/d/a/w/c/a;

    if-eqz v10, :cond_14

    invoke-virtual {v10}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    :goto_f
    add-float/2addr v6, v10

    :cond_14
    mul-float v6, v6, v5

    add-float/2addr v6, v7

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_10
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v6, p3

    move-object/from16 p3, v16

    move/from16 v7, v17

    goto/16 :goto_a

    :cond_15
    move-object/from16 v14, p2

    move-object/from16 p3, v6

    move/from16 v17, v7

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v9, v9, 0x1

    move v8, v13

    goto/16 :goto_7

    :cond_16
    move-object/from16 v14, p2

    invoke-static/range {p2 .. p2}, Lf/d/a/b0/g;->d(Landroid/graphics/Matrix;)F

    move-result v4

    iget-object v5, v0, Lf/d/a/y/l/i;->F:Lf/d/a/j;

    iget-object v6, v3, Lf/d/a/y/c;->a:Ljava/lang/String;

    iget-object v3, v3, Lf/d/a/y/c;->c:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v7

    const/4 v8, 0x0

    if-nez v7, :cond_17

    move-object v5, v8

    goto :goto_11

    :cond_17
    iget-object v7, v5, Lf/d/a/j;->p:Lf/d/a/x/a;

    if-nez v7, :cond_18

    new-instance v7, Lf/d/a/x/a;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v9

    invoke-direct {v7, v9}, Lf/d/a/x/a;-><init>(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object v7, v5, Lf/d/a/j;->p:Lf/d/a/x/a;

    :cond_18
    iget-object v5, v5, Lf/d/a/j;->p:Lf/d/a/x/a;

    :goto_11
    if-eqz v5, :cond_1f

    iget-object v7, v5, Lf/d/a/x/a;->a:Lf/d/a/y/i;

    iput-object v6, v7, Lf/d/a/y/i;->a:Ljava/lang/Object;

    iput-object v3, v7, Lf/d/a/y/i;->b:Ljava/lang/Object;

    iget-object v8, v5, Lf/d/a/x/a;->b:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Landroid/graphics/Typeface;

    if-eqz v8, :cond_19

    goto :goto_15

    :cond_19
    iget-object v7, v5, Lf/d/a/x/a;->c:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Typeface;

    if-eqz v7, :cond_1a

    goto :goto_12

    :cond_1a
    const-string v7, "fonts/"

    invoke-static {v7, v6}, Lf/e/c/a/a;->L(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lf/d/a/x/a;->e:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v5, Lf/d/a/x/a;->d:Landroid/content/res/AssetManager;

    invoke-static {v8, v7}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v7

    iget-object v8, v5, Lf/d/a/x/a;->c:Ljava/util/Map;

    invoke-interface {v8, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_12
    const-string v6, "Italic"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    const-string v8, "Bold"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v6, :cond_1b

    if-eqz v3, :cond_1b

    const/4 v3, 0x3

    goto :goto_13

    :cond_1b
    if-eqz v6, :cond_1c

    const/4 v3, 0x2

    goto :goto_13

    :cond_1c
    if-eqz v3, :cond_1d

    const/4 v3, 0x1

    goto :goto_13

    :cond_1d
    const/4 v3, 0x0

    :goto_13
    invoke-virtual {v7}, Landroid/graphics/Typeface;->getStyle()I

    move-result v6

    if-ne v6, v3, :cond_1e

    move-object v8, v7

    goto :goto_14

    :cond_1e
    invoke-static {v7, v3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v3

    move-object v8, v3

    :goto_14
    iget-object v3, v5, Lf/d/a/x/a;->b:Ljava/util/Map;

    iget-object v5, v5, Lf/d/a/x/a;->a:Lf/d/a/y/i;

    invoke-interface {v3, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1f
    :goto_15
    if-nez v8, :cond_20

    goto/16 :goto_21

    :cond_20
    iget-object v3, v2, Lf/d/a/y/b;->a:Ljava/lang/String;

    iget-object v5, v0, Lf/d/a/y/l/i;->F:Lf/d/a/j;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v5, v0, Lf/d/a/y/l/i;->P:Lf/d/a/w/c/a;

    if-eqz v5, :cond_21

    invoke-virtual {v5}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    goto :goto_16

    :cond_21
    iget v5, v2, Lf/d/a/y/b;->c:F

    :goto_16
    iget-object v6, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v7

    mul-float v7, v7, v5

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    iget-object v6, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v5, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    iget-object v6, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getTextSize()F

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v5, v2, Lf/d/a/y/b;->f:F

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v6

    mul-float v6, v6, v5

    invoke-virtual {v0, v3}, Lf/d/a/y/l/i;->t(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    const/4 v7, 0x0

    :goto_17
    if-ge v7, v5, :cond_2c

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iget-object v9, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v9, v8}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, v2, Lf/d/a/y/b;->d:Lf/d/a/y/b$a;

    invoke-virtual {v0, v10, v1, v9}, Lf/d/a/y/l/i;->q(Lf/d/a/y/b$a;Landroid/graphics/Canvas;F)V

    add-int/lit8 v9, v5, -0x1

    int-to-float v9, v9

    mul-float v9, v9, v6

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    int-to-float v10, v7

    mul-float v10, v10, v6

    sub-float/2addr v10, v9

    const/4 v9, 0x0

    invoke-virtual {v1, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    const/4 v9, 0x0

    :goto_18
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v9, v10, :cond_2b

    invoke-virtual {v8, v9}, Ljava/lang/String;->codePointAt(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Character;->charCount(I)I

    move-result v11

    add-int/2addr v11, v9

    :goto_19
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v11, v12, :cond_25

    invoke-virtual {v8, v11}, Ljava/lang/String;->codePointAt(I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Character;->getType(I)I

    move-result v13

    const/16 v15, 0x10

    if-eq v13, v15, :cond_23

    invoke-static {v12}, Ljava/lang/Character;->getType(I)I

    move-result v13

    const/16 v15, 0x1b

    if-eq v13, v15, :cond_23

    invoke-static {v12}, Ljava/lang/Character;->getType(I)I

    move-result v13

    const/4 v15, 0x6

    if-eq v13, v15, :cond_23

    invoke-static {v12}, Ljava/lang/Character;->getType(I)I

    move-result v13

    const/16 v15, 0x1c

    if-eq v13, v15, :cond_23

    invoke-static {v12}, Ljava/lang/Character;->getType(I)I

    move-result v13

    const/16 v15, 0x13

    if-ne v13, v15, :cond_22

    goto :goto_1a

    :cond_22
    const/4 v13, 0x0

    goto :goto_1b

    :cond_23
    :goto_1a
    const/4 v13, 0x1

    :goto_1b
    if-nez v13, :cond_24

    goto :goto_1c

    :cond_24
    invoke-static {v12}, Ljava/lang/Character;->charCount(I)I

    move-result v13

    add-int/2addr v11, v13

    mul-int/lit8 v10, v10, 0x1f

    add-int/2addr v10, v12

    goto :goto_19

    :cond_25
    :goto_1c
    iget-object v12, v0, Lf/d/a/y/l/i;->D:Landroidx/collection/LongSparseArray;

    move v13, v5

    move/from16 p3, v6

    int-to-long v5, v10

    invoke-virtual {v12, v5, v6}, Landroidx/collection/LongSparseArray;->containsKey(J)Z

    move-result v10

    if-eqz v10, :cond_26

    iget-object v10, v0, Lf/d/a/y/l/i;->D:Landroidx/collection/LongSparseArray;

    invoke-virtual {v10, v5, v6}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto :goto_1e

    :cond_26
    iget-object v10, v0, Lf/d/a/y/l/i;->x:Ljava/lang/StringBuilder;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    move v10, v9

    :goto_1d
    if-ge v10, v11, :cond_27

    invoke-virtual {v8, v10}, Ljava/lang/String;->codePointAt(I)I

    move-result v12

    iget-object v15, v0, Lf/d/a/y/l/i;->x:Ljava/lang/StringBuilder;

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/Character;->charCount(I)I

    move-result v12

    add-int/2addr v10, v12

    goto :goto_1d

    :cond_27
    iget-object v10, v0, Lf/d/a/y/l/i;->x:Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lf/d/a/y/l/i;->D:Landroidx/collection/LongSparseArray;

    invoke-virtual {v11, v5, v6, v10}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    move-object v5, v10

    :goto_1e
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v9, v6

    iget-boolean v6, v2, Lf/d/a/y/b;->k:Z

    if-eqz v6, :cond_28

    iget-object v6, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v6, v1}, Lf/d/a/y/l/i;->r(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v6, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v6, v1}, Lf/d/a/y/l/i;->r(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    goto :goto_1f

    :cond_28
    iget-object v6, v0, Lf/d/a/y/l/i;->B:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v6, v1}, Lf/d/a/y/l/i;->r(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v6, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v6, v1}, Lf/d/a/y/l/i;->r(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    :goto_1f
    iget-object v6, v0, Lf/d/a/y/l/i;->A:Landroid/graphics/Paint;

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v6, v5, v11, v10}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v5

    iget v6, v2, Lf/d/a/y/b;->e:I

    int-to-float v6, v6

    const/high16 v10, 0x41200000    # 10.0f

    div-float/2addr v6, v10

    iget-object v10, v0, Lf/d/a/y/l/i;->O:Lf/d/a/w/c/a;

    if-eqz v10, :cond_29

    invoke-virtual {v10}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    goto :goto_20

    :cond_29
    iget-object v10, v0, Lf/d/a/y/l/i;->N:Lf/d/a/w/c/a;

    if-eqz v10, :cond_2a

    invoke-virtual {v10}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    :goto_20
    add-float/2addr v6, v10

    :cond_2a
    mul-float v6, v6, v4

    add-float/2addr v6, v5

    const/4 v5, 0x0

    invoke-virtual {v1, v6, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move/from16 v6, p3

    move v5, v13

    goto/16 :goto_18

    :cond_2b
    move v13, v5

    move/from16 p3, v6

    invoke-virtual/range {p1 .. p2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_17

    :cond_2c
    :goto_21
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public final q(Lf/d/a/y/b$a;Landroid/graphics/Canvas;F)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    neg-float p1, p3

    const/high16 p3, 0x40000000    # 2.0f

    div-float/2addr p1, p3

    invoke-virtual {p2, p1, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    :cond_1
    neg-float p1, p3

    invoke-virtual {p2, p1, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_0
    return-void
.end method

.method public final r(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 8

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p3

    move-object v2, p1

    move-object v7, p2

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final s(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 2

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p3, p1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method public final t(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "\r\n"

    const-string v1, "\r"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\n"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
