.class public Lf/d/a/y/b;
.super Ljava/lang/Object;
.source "DocumentData.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/y/b$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:F

.field public final d:Lf/d/a/y/b$a;

.field public final e:I

.field public final f:F

.field public final g:F

.field public final h:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public final i:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public final j:F

.field public final k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FLf/d/a/y/b$a;IFFIIFZ)V
    .locals 0
    .param p8    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/d/a/y/b;->b:Ljava/lang/String;

    iput p3, p0, Lf/d/a/y/b;->c:F

    iput-object p4, p0, Lf/d/a/y/b;->d:Lf/d/a/y/b$a;

    iput p5, p0, Lf/d/a/y/b;->e:I

    iput p6, p0, Lf/d/a/y/b;->f:F

    iput p7, p0, Lf/d/a/y/b;->g:F

    iput p8, p0, Lf/d/a/y/b;->h:I

    iput p9, p0, Lf/d/a/y/b;->i:I

    iput p10, p0, Lf/d/a/y/b;->j:F

    iput-boolean p11, p0, Lf/d/a/y/b;->k:Z

    return-void
.end method


# virtual methods
.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lf/d/a/y/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lf/d/a/y/b;->b:Ljava/lang/String;

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->T(Ljava/lang/String;II)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lf/d/a/y/b;->c:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lf/d/a/y/b;->d:Lf/d/a/y/b$a;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lf/d/a/y/b;->e:I

    add-int/2addr v1, v0

    iget v0, p0, Lf/d/a/y/b;->f:F

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    int-to-long v2, v0

    mul-int/lit8 v1, v1, 0x1f

    const/16 v0, 0x20

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lf/d/a/y/b;->h:I

    add-int/2addr v1, v0

    return v1
.end method
