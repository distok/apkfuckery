.class public Lf/d/a/y/k/q;
.super Ljava/lang/Object;
.source "ShapeTrimPath.java"

# interfaces
.implements Lf/d/a/y/k/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/y/k/q$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/d/a/y/k/q$a;

.field public final c:Lf/d/a/y/j/b;

.field public final d:Lf/d/a/y/j/b;

.field public final e:Lf/d/a/y/j/b;

.field public final f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/d/a/y/k/q$a;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/q;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/d/a/y/k/q;->b:Lf/d/a/y/k/q$a;

    iput-object p3, p0, Lf/d/a/y/k/q;->c:Lf/d/a/y/j/b;

    iput-object p4, p0, Lf/d/a/y/k/q;->d:Lf/d/a/y/j/b;

    iput-object p5, p0, Lf/d/a/y/k/q;->e:Lf/d/a/y/j/b;

    iput-boolean p6, p0, Lf/d/a/y/k/q;->f:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 0

    new-instance p1, Lf/d/a/w/b/s;

    invoke-direct {p1, p2, p0}, Lf/d/a/w/b/s;-><init>(Lf/d/a/y/l/b;Lf/d/a/y/k/q;)V

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Trim Path: {start: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/d/a/y/k/q;->c:Lf/d/a/y/j/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", end: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/d/a/y/k/q;->d:Lf/d/a/y/j/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", offset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/d/a/y/k/q;->e:Lf/d/a/y/j/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
