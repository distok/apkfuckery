.class public Lf/d/a/y/k/o;
.super Ljava/lang/Object;
.source "ShapePath.java"

# interfaces
.implements Lf/d/a/y/k/b;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Lf/d/a/y/j/h;

.field public final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ILf/d/a/y/j/h;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/o;->a:Ljava/lang/String;

    iput p2, p0, Lf/d/a/y/k/o;->b:I

    iput-object p3, p0, Lf/d/a/y/k/o;->c:Lf/d/a/y/j/h;

    iput-boolean p4, p0, Lf/d/a/y/k/o;->d:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 1

    new-instance v0, Lf/d/a/w/b/q;

    invoke-direct {v0, p1, p2, p0}, Lf/d/a/w/b/q;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/o;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ShapePath{name="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/d/a/y/k/o;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lf/d/a/y/k/o;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
