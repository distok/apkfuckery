.class public Lf/d/a/y/k/e;
.super Ljava/lang/Object;
.source "GradientStroke.java"

# interfaces
.implements Lf/d/a/y/k/b;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/d/a/y/k/f;

.field public final c:Lf/d/a/y/j/c;

.field public final d:Lf/d/a/y/j/d;

.field public final e:Lf/d/a/y/j/f;

.field public final f:Lf/d/a/y/j/f;

.field public final g:Lf/d/a/y/j/b;

.field public final h:Lf/d/a/y/k/p$a;

.field public final i:Lf/d/a/y/k/p$b;

.field public final j:F

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/y/j/b;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final m:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/d/a/y/k/f;Lf/d/a/y/j/c;Lf/d/a/y/j/d;Lf/d/a/y/j/f;Lf/d/a/y/j/f;Lf/d/a/y/j/b;Lf/d/a/y/k/p$a;Lf/d/a/y/k/p$b;FLjava/util/List;Lf/d/a/y/j/b;Z)V
    .locals 0
    .param p12    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/d/a/y/k/f;",
            "Lf/d/a/y/j/c;",
            "Lf/d/a/y/j/d;",
            "Lf/d/a/y/j/f;",
            "Lf/d/a/y/j/f;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/k/p$a;",
            "Lf/d/a/y/k/p$b;",
            "F",
            "Ljava/util/List<",
            "Lf/d/a/y/j/b;",
            ">;",
            "Lf/d/a/y/j/b;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/e;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/d/a/y/k/e;->b:Lf/d/a/y/k/f;

    iput-object p3, p0, Lf/d/a/y/k/e;->c:Lf/d/a/y/j/c;

    iput-object p4, p0, Lf/d/a/y/k/e;->d:Lf/d/a/y/j/d;

    iput-object p5, p0, Lf/d/a/y/k/e;->e:Lf/d/a/y/j/f;

    iput-object p6, p0, Lf/d/a/y/k/e;->f:Lf/d/a/y/j/f;

    iput-object p7, p0, Lf/d/a/y/k/e;->g:Lf/d/a/y/j/b;

    iput-object p8, p0, Lf/d/a/y/k/e;->h:Lf/d/a/y/k/p$a;

    iput-object p9, p0, Lf/d/a/y/k/e;->i:Lf/d/a/y/k/p$b;

    iput p10, p0, Lf/d/a/y/k/e;->j:F

    iput-object p11, p0, Lf/d/a/y/k/e;->k:Ljava/util/List;

    iput-object p12, p0, Lf/d/a/y/k/e;->l:Lf/d/a/y/j/b;

    iput-boolean p13, p0, Lf/d/a/y/k/e;->m:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 1

    new-instance v0, Lf/d/a/w/b/i;

    invoke-direct {v0, p1, p2, p0}, Lf/d/a/w/b/i;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/e;)V

    return-object v0
.end method
