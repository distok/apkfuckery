.class public final enum Lf/d/a/y/k/f;
.super Ljava/lang/Enum;
.source "GradientType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/d/a/y/k/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/d/a/y/k/f;

.field public static final enum e:Lf/d/a/y/k/f;

.field public static final synthetic f:[Lf/d/a/y/k/f;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/d/a/y/k/f;

    const-string v1, "LINEAR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/d/a/y/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/d/a/y/k/f;->d:Lf/d/a/y/k/f;

    new-instance v1, Lf/d/a/y/k/f;

    const-string v3, "RADIAL"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/d/a/y/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/d/a/y/k/f;->e:Lf/d/a/y/k/f;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/d/a/y/k/f;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/d/a/y/k/f;->f:[Lf/d/a/y/k/f;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/d/a/y/k/f;
    .locals 1

    const-class v0, Lf/d/a/y/k/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/d/a/y/k/f;

    return-object p0
.end method

.method public static values()[Lf/d/a/y/k/f;
    .locals 1

    sget-object v0, Lf/d/a/y/k/f;->f:[Lf/d/a/y/k/f;

    invoke-virtual {v0}, [Lf/d/a/y/k/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/d/a/y/k/f;

    return-object v0
.end method
