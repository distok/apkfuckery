.class public Lf/d/a/y/k/m;
.super Ljava/lang/Object;
.source "ShapeFill.java"

# interfaces
.implements Lf/d/a/y/k/b;


# instance fields
.field public final a:Z

.field public final b:Landroid/graphics/Path$FillType;

.field public final c:Ljava/lang/String;

.field public final d:Lf/d/a/y/j/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lf/d/a/y/j/d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLandroid/graphics/Path$FillType;Lf/d/a/y/j/a;Lf/d/a/y/j/d;Z)V
    .locals 0
    .param p4    # Lf/d/a/y/j/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lf/d/a/y/j/d;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/m;->c:Ljava/lang/String;

    iput-boolean p2, p0, Lf/d/a/y/k/m;->a:Z

    iput-object p3, p0, Lf/d/a/y/k/m;->b:Landroid/graphics/Path$FillType;

    iput-object p4, p0, Lf/d/a/y/k/m;->d:Lf/d/a/y/j/a;

    iput-object p5, p0, Lf/d/a/y/k/m;->e:Lf/d/a/y/j/d;

    iput-boolean p6, p0, Lf/d/a/y/k/m;->f:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 1

    new-instance v0, Lf/d/a/w/b/g;

    invoke-direct {v0, p1, p2, p0}, Lf/d/a/w/b/g;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/m;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ShapeFill{color=, fillEnabled="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lf/d/a/y/k/m;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
