.class public final enum Lf/d/a/y/k/i$a;
.super Ljava/lang/Enum;
.source "PolystarShape.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/d/a/y/k/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/d/a/y/k/i$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/d/a/y/k/i$a;

.field public static final enum e:Lf/d/a/y/k/i$a;

.field public static final synthetic f:[Lf/d/a/y/k/i$a;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    new-instance v0, Lf/d/a/y/k/i$a;

    const-string v1, "STAR"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lf/d/a/y/k/i$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/d/a/y/k/i$a;->d:Lf/d/a/y/k/i$a;

    new-instance v1, Lf/d/a/y/k/i$a;

    const-string v4, "POLYGON"

    const/4 v5, 0x2

    invoke-direct {v1, v4, v3, v5}, Lf/d/a/y/k/i$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/d/a/y/k/i$a;->e:Lf/d/a/y/k/i$a;

    new-array v4, v5, [Lf/d/a/y/k/i$a;

    aput-object v0, v4, v2

    aput-object v1, v4, v3

    sput-object v4, Lf/d/a/y/k/i$a;->f:[Lf/d/a/y/k/i$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/d/a/y/k/i$a;->value:I

    return-void
.end method

.method public static f(I)Lf/d/a/y/k/i$a;
    .locals 4

    invoke-static {}, Lf/d/a/y/k/i$a;->values()[Lf/d/a/y/k/i$a;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    iget v3, v2, Lf/d/a/y/k/i$a;->value:I

    if-ne v3, p0, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lf/d/a/y/k/i$a;
    .locals 1

    const-class v0, Lf/d/a/y/k/i$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/d/a/y/k/i$a;

    return-object p0
.end method

.method public static values()[Lf/d/a/y/k/i$a;
    .locals 1

    sget-object v0, Lf/d/a/y/k/i$a;->f:[Lf/d/a/y/k/i$a;

    invoke-virtual {v0}, [Lf/d/a/y/k/i$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/d/a/y/k/i$a;

    return-object v0
.end method
