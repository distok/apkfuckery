.class public Lf/d/a/y/k/p;
.super Ljava/lang/Object;
.source "ShapeStroke.java"

# interfaces
.implements Lf/d/a/y/k/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/y/k/p$b;,
        Lf/d/a/y/k/p$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/d/a/y/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/y/j/b;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lf/d/a/y/j/a;

.field public final e:Lf/d/a/y/j/d;

.field public final f:Lf/d/a/y/j/b;

.field public final g:Lf/d/a/y/k/p$a;

.field public final h:Lf/d/a/y/k/p$b;

.field public final i:F

.field public final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/d/a/y/j/b;Ljava/util/List;Lf/d/a/y/j/a;Lf/d/a/y/j/d;Lf/d/a/y/j/b;Lf/d/a/y/k/p$a;Lf/d/a/y/k/p$b;FZ)V
    .locals 0
    .param p2    # Lf/d/a/y/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/d/a/y/j/b;",
            "Ljava/util/List<",
            "Lf/d/a/y/j/b;",
            ">;",
            "Lf/d/a/y/j/a;",
            "Lf/d/a/y/j/d;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/k/p$a;",
            "Lf/d/a/y/k/p$b;",
            "FZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/p;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/d/a/y/k/p;->b:Lf/d/a/y/j/b;

    iput-object p3, p0, Lf/d/a/y/k/p;->c:Ljava/util/List;

    iput-object p4, p0, Lf/d/a/y/k/p;->d:Lf/d/a/y/j/a;

    iput-object p5, p0, Lf/d/a/y/k/p;->e:Lf/d/a/y/j/d;

    iput-object p6, p0, Lf/d/a/y/k/p;->f:Lf/d/a/y/j/b;

    iput-object p7, p0, Lf/d/a/y/k/p;->g:Lf/d/a/y/k/p$a;

    iput-object p8, p0, Lf/d/a/y/k/p;->h:Lf/d/a/y/k/p$b;

    iput p9, p0, Lf/d/a/y/k/p;->i:F

    iput-boolean p10, p0, Lf/d/a/y/k/p;->j:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 1

    new-instance v0, Lf/d/a/w/b/r;

    invoke-direct {v0, p1, p2, p0}, Lf/d/a/w/b/r;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/p;)V

    return-object v0
.end method
