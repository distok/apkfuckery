.class public Lf/d/a/y/k/h;
.super Ljava/lang/Object;
.source "MergePaths.java"

# interfaces
.implements Lf/d/a/y/k/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/y/k/h$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/d/a/y/k/h$a;

.field public final c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/d/a/y/k/h$a;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/h;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/d/a/y/k/h;->b:Lf/d/a/y/k/h$a;

    iput-boolean p3, p0, Lf/d/a/y/k/h;->c:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-boolean p1, p1, Lf/d/a/j;->q:Z

    if-nez p1, :cond_0

    const-string p1, "Animation contains merge paths but they are disabled."

    invoke-static {p1}, Lf/d/a/b0/c;->b(Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance p1, Lf/d/a/w/b/l;

    invoke-direct {p1, p0}, Lf/d/a/w/b/l;-><init>(Lf/d/a/y/k/h;)V

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "MergePaths{mode="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/d/a/y/k/h;->b:Lf/d/a/y/k/h$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
