.class public Lf/d/a/y/k/d;
.super Ljava/lang/Object;
.source "GradientFill.java"

# interfaces
.implements Lf/d/a/y/k/b;


# instance fields
.field public final a:Lf/d/a/y/k/f;

.field public final b:Landroid/graphics/Path$FillType;

.field public final c:Lf/d/a/y/j/c;

.field public final d:Lf/d/a/y/j/d;

.field public final e:Lf/d/a/y/j/f;

.field public final f:Lf/d/a/y/j/f;

.field public final g:Ljava/lang/String;

.field public final h:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/d/a/y/k/f;Landroid/graphics/Path$FillType;Lf/d/a/y/j/c;Lf/d/a/y/j/d;Lf/d/a/y/j/f;Lf/d/a/y/j/f;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/d/a/y/k/d;->a:Lf/d/a/y/k/f;

    iput-object p3, p0, Lf/d/a/y/k/d;->b:Landroid/graphics/Path$FillType;

    iput-object p4, p0, Lf/d/a/y/k/d;->c:Lf/d/a/y/j/c;

    iput-object p5, p0, Lf/d/a/y/k/d;->d:Lf/d/a/y/j/d;

    iput-object p6, p0, Lf/d/a/y/k/d;->e:Lf/d/a/y/j/f;

    iput-object p7, p0, Lf/d/a/y/k/d;->f:Lf/d/a/y/j/f;

    iput-object p1, p0, Lf/d/a/y/k/d;->g:Ljava/lang/String;

    iput-boolean p10, p0, Lf/d/a/y/k/d;->h:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 1

    new-instance v0, Lf/d/a/w/b/h;

    invoke-direct {v0, p1, p2, p0}, Lf/d/a/w/b/h;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/d;)V

    return-object v0
.end method
