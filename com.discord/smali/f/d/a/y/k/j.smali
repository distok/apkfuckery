.class public Lf/d/a/y/k/j;
.super Ljava/lang/Object;
.source "RectangleShape.java"

# interfaces
.implements Lf/d/a/y/k/b;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/d/a/y/j/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/y/j/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/d/a/y/j/f;

.field public final d:Lf/d/a/y/j/b;

.field public final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/d/a/y/j/m;Lf/d/a/y/j/f;Lf/d/a/y/j/b;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/d/a/y/j/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;",
            "Lf/d/a/y/j/f;",
            "Lf/d/a/y/j/b;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/j;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/d/a/y/k/j;->b:Lf/d/a/y/j/m;

    iput-object p3, p0, Lf/d/a/y/k/j;->c:Lf/d/a/y/j/f;

    iput-object p4, p0, Lf/d/a/y/k/j;->d:Lf/d/a/y/j/b;

    iput-boolean p5, p0, Lf/d/a/y/k/j;->e:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 1

    new-instance v0, Lf/d/a/w/b/o;

    invoke-direct {v0, p1, p2, p0}, Lf/d/a/w/b/o;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/j;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "RectangleShape{position="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/d/a/y/k/j;->b:Lf/d/a/y/j/m;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/d/a/y/k/j;->c:Lf/d/a/y/j/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
