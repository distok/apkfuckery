.class public final enum Lf/d/a/y/k/p$b;
.super Ljava/lang/Enum;
.source "ShapeStroke.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/d/a/y/k/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/d/a/y/k/p$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/d/a/y/k/p$b;

.field public static final enum e:Lf/d/a/y/k/p$b;

.field public static final enum f:Lf/d/a/y/k/p$b;

.field public static final synthetic g:[Lf/d/a/y/k/p$b;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lf/d/a/y/k/p$b;

    const-string v1, "MITER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/d/a/y/k/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/d/a/y/k/p$b;->d:Lf/d/a/y/k/p$b;

    new-instance v1, Lf/d/a/y/k/p$b;

    const-string v3, "ROUND"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/d/a/y/k/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/d/a/y/k/p$b;->e:Lf/d/a/y/k/p$b;

    new-instance v3, Lf/d/a/y/k/p$b;

    const-string v5, "BEVEL"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/d/a/y/k/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/d/a/y/k/p$b;->f:Lf/d/a/y/k/p$b;

    const/4 v5, 0x3

    new-array v5, v5, [Lf/d/a/y/k/p$b;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lf/d/a/y/k/p$b;->g:[Lf/d/a/y/k/p$b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/d/a/y/k/p$b;
    .locals 1

    const-class v0, Lf/d/a/y/k/p$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/d/a/y/k/p$b;

    return-object p0
.end method

.method public static values()[Lf/d/a/y/k/p$b;
    .locals 1

    sget-object v0, Lf/d/a/y/k/p$b;->g:[Lf/d/a/y/k/p$b;

    invoke-virtual {v0}, [Lf/d/a/y/k/p$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/d/a/y/k/p$b;

    return-object v0
.end method


# virtual methods
.method public f()Landroid/graphics/Paint$Join;
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    return-object v0

    :cond_1
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    return-object v0

    :cond_2
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    return-object v0
.end method
