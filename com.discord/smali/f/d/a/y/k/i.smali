.class public Lf/d/a/y/k/i;
.super Ljava/lang/Object;
.source "PolystarShape.java"

# interfaces
.implements Lf/d/a/y/k/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/y/k/i$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/d/a/y/k/i$a;

.field public final c:Lf/d/a/y/j/b;

.field public final d:Lf/d/a/y/j/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/y/j/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lf/d/a/y/j/b;

.field public final f:Lf/d/a/y/j/b;

.field public final g:Lf/d/a/y/j/b;

.field public final h:Lf/d/a/y/j/b;

.field public final i:Lf/d/a/y/j/b;

.field public final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/d/a/y/k/i$a;Lf/d/a/y/j/b;Lf/d/a/y/j/m;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/d/a/y/k/i$a;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/b;",
            "Lf/d/a/y/j/b;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/d/a/y/k/i;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/d/a/y/k/i;->b:Lf/d/a/y/k/i$a;

    iput-object p3, p0, Lf/d/a/y/k/i;->c:Lf/d/a/y/j/b;

    iput-object p4, p0, Lf/d/a/y/k/i;->d:Lf/d/a/y/j/m;

    iput-object p5, p0, Lf/d/a/y/k/i;->e:Lf/d/a/y/j/b;

    iput-object p6, p0, Lf/d/a/y/k/i;->f:Lf/d/a/y/j/b;

    iput-object p7, p0, Lf/d/a/y/k/i;->g:Lf/d/a/y/j/b;

    iput-object p8, p0, Lf/d/a/y/k/i;->h:Lf/d/a/y/j/b;

    iput-object p9, p0, Lf/d/a/y/k/i;->i:Lf/d/a/y/j/b;

    iput-boolean p10, p0, Lf/d/a/y/k/i;->j:Z

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;
    .locals 1

    new-instance v0, Lf/d/a/w/b/n;

    invoke-direct {v0, p1, p2, p0}, Lf/d/a/w/b/n;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/i;)V

    return-object v0
.end method
