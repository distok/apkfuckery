.class public Lf/d/a/a0/s;
.super Ljava/lang/Object;
.source "LottieCompositionMoshiParser.java"


# static fields
.field public static final a:Lf/d/a/a0/h0/c$a;

.field public static b:Lf/d/a/a0/h0/c$a;

.field public static final c:Lf/d/a/a0/h0/c$a;

.field public static final d:Lf/d/a/a0/h0/c$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    const-string/jumbo v0, "w"

    const-string v1, "h"

    const-string v2, "ip"

    const-string v3, "op"

    const-string v4, "fr"

    const-string/jumbo v5, "v"

    const-string v6, "layers"

    const-string v7, "assets"

    const-string v8, "fonts"

    const-string v9, "chars"

    const-string v10, "markers"

    filled-new-array/range {v0 .. v10}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/s;->a:Lf/d/a/a0/h0/c$a;

    const-string v1, "id"

    const-string v2, "layers"

    const-string/jumbo v3, "w"

    const-string v4, "h"

    const-string v5, "p"

    const-string/jumbo v6, "u"

    filled-new-array/range {v1 .. v6}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/s;->b:Lf/d/a/a0/h0/c$a;

    const-string v0, "list"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/s;->c:Lf/d/a/a0/h0/c$a;

    const-string v0, "cm"

    const-string/jumbo v1, "tm"

    const-string v2, "dr"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/s;->d:Lf/d/a/a0/h0/c$a;

    return-void
.end method

.method public static a(Lf/d/a/a0/h0/c;)Lf/d/a/d;
    .locals 32
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v1

    new-instance v2, Landroidx/collection/LongSparseArray;

    invoke-direct {v2}, Landroidx/collection/LongSparseArray;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Landroidx/collection/SparseArrayCompat;

    invoke-direct {v8}, Landroidx/collection/SparseArrayCompat;-><init>()V

    new-instance v9, Lf/d/a/d;

    invoke-direct {v9}, Lf/d/a/d;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v16

    if-eqz v16, :cond_2b

    sget-object v11, Lf/d/a/a0/s;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v11}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v11

    move/from16 v17, v10

    const/16 v19, 0x0

    packed-switch v11, :pswitch_data_0

    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto/16 :goto_17

    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    move-object/from16 v11, v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v18

    if-eqz v18, :cond_3

    sget-object v10, Lf/d/a/a0/s;->d:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v10}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v10

    if-eqz v10, :cond_2

    move/from16 v22, v15

    const/4 v15, 0x1

    if-eq v10, v15, :cond_1

    const/4 v15, 0x2

    if-eq v10, v15, :cond_0

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    move/from16 v15, v22

    goto :goto_2

    :cond_0
    move v10, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v14

    double-to-float v14, v14

    move/from16 v21, v14

    goto :goto_3

    :cond_1
    move v10, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v14

    double-to-float v14, v14

    move/from16 v20, v14

    :goto_3
    move/from16 v15, v22

    move v14, v10

    goto :goto_2

    :cond_2
    move v10, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v11

    goto :goto_2

    :cond_3
    move v10, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    new-instance v14, Lf/d/a/y/h;

    move/from16 v15, v20

    move/from16 v20, v10

    move/from16 v10, v21

    invoke-direct {v14, v11, v15, v10}, Lf/d/a/y/h;-><init>(Ljava/lang/String;FF)V

    invoke-virtual {v7, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v14, v20

    move/from16 v15, v22

    goto :goto_1

    :cond_4
    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto/16 :goto_8

    :pswitch_1
    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v10

    if-eqz v10, :cond_f

    sget-object v10, Lf/d/a/a0/i;->a:Lf/d/a/a0/h0/c$a;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const-wide/16 v14, 0x0

    move-wide/from16 v26, v14

    move-wide/from16 v28, v26

    move-object/from16 v30, v19

    move-object/from16 v31, v30

    const/16 v25, 0x0

    :goto_5
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v11

    if-eqz v11, :cond_e

    sget-object v11, Lf/d/a/a0/i;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v11}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v11

    if-eqz v11, :cond_d

    const/4 v14, 0x1

    if-eq v11, v14, :cond_c

    const/4 v14, 0x2

    if-eq v11, v14, :cond_b

    const/4 v14, 0x3

    if-eq v11, v14, :cond_a

    const/4 v14, 0x4

    if-eq v11, v14, :cond_9

    const/4 v14, 0x5

    if-eq v11, v14, :cond_5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_5

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v11

    if-eqz v11, :cond_8

    sget-object v11, Lf/d/a/a0/i;->b:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v11}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v11

    if-eqz v11, :cond_6

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_6

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-static {v0, v9}, Lf/d/a/a0/f;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/k/b;

    move-result-object v11

    check-cast v11, Lf/d/a/y/k/n;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto :goto_6

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    goto :goto_5

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v31

    goto :goto_5

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v30

    goto :goto_5

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v28

    goto :goto_5

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v26

    goto :goto_5

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v11

    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Ljava/lang/String;->charAt(I)C

    move-result v25

    goto :goto_5

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    new-instance v11, Lf/d/a/y/d;

    move-object/from16 v23, v11

    move-object/from16 v24, v10

    invoke-direct/range {v23 .. v31}, Lf/d/a/y/d;-><init>(Ljava/util/List;CDDLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11}, Lf/d/a/y/d;->hashCode()I

    move-result v10

    invoke-virtual {v8, v10, v11}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto/16 :goto_4

    :cond_f
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    :goto_8
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    goto/16 :goto_17

    :pswitch_2
    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    :goto_9
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v10

    if-eqz v10, :cond_17

    sget-object v10, Lf/d/a/a0/s;->c:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v10}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v10

    if-eqz v10, :cond_10

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_9

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v10

    if-eqz v10, :cond_16

    sget-object v10, Lf/d/a/a0/j;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    move-object/from16 v10, v19

    move-object v11, v10

    move-object v14, v11

    const/4 v15, 0x0

    :goto_b
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v18

    if-eqz v18, :cond_15

    move-object/from16 v21, v7

    sget-object v7, Lf/d/a/a0/j;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v7}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v7

    if-eqz v7, :cond_14

    move-object/from16 v23, v8

    const/4 v8, 0x1

    if-eq v7, v8, :cond_13

    const/4 v8, 0x2

    if-eq v7, v8, :cond_12

    const/4 v8, 0x3

    if-eq v7, v8, :cond_11

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_c

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v7

    double-to-float v15, v7

    goto :goto_c

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v14

    goto :goto_c

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v11

    :goto_c
    move-object/from16 v7, v21

    move-object/from16 v8, v23

    goto :goto_b

    :cond_14
    move-object/from16 v23, v8

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v7, v21

    goto :goto_b

    :cond_15
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    new-instance v7, Lf/d/a/y/c;

    invoke-direct {v7, v10, v11, v14, v15}, Lf/d/a/y/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V

    invoke-virtual {v6, v11, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v7, v21

    goto :goto_a

    :cond_16
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto/16 :goto_9

    :cond_17
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    goto/16 :goto_17

    :pswitch_3
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v7

    if-eqz v7, :cond_21

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Landroidx/collection/LongSparseArray;

    invoke-direct {v8}, Landroidx/collection/LongSparseArray;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    move-object/from16 v10, v19

    move-object/from16 v28, v10

    move-object/from16 v29, v28

    const/16 v25, 0x0

    const/16 v26, 0x0

    :goto_e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v11

    if-eqz v11, :cond_1f

    sget-object v11, Lf/d/a/a0/s;->b:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v11}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v11

    if-eqz v11, :cond_1e

    const/4 v14, 0x1

    if-eq v11, v14, :cond_1c

    const/4 v14, 0x2

    if-eq v11, v14, :cond_1b

    const/4 v14, 0x3

    if-eq v11, v14, :cond_1a

    const/4 v15, 0x4

    if-eq v11, v15, :cond_19

    const/4 v15, 0x5

    if-eq v11, v15, :cond_18

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_e

    :cond_18
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v29

    goto :goto_e

    :cond_19
    const/4 v15, 0x5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v28

    goto :goto_e

    :cond_1a
    const/4 v15, 0x5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v26

    goto :goto_e

    :cond_1b
    const/4 v14, 0x3

    const/4 v15, 0x5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v25

    goto :goto_e

    :cond_1c
    const/4 v14, 0x3

    const/4 v15, 0x5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_f
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v11

    if-eqz v11, :cond_1d

    invoke-static {v0, v9}, Lf/d/a/a0/r;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/l/e;

    move-result-object v11

    iget-wide v14, v11, Lf/d/a/y/l/e;->d:J

    invoke-virtual {v8, v14, v15, v11}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v14, 0x3

    const/4 v15, 0x5

    goto :goto_f

    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto :goto_e

    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v10

    goto :goto_e

    :cond_1f
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    if-eqz v28, :cond_20

    new-instance v7, Lf/d/a/k;

    move-object/from16 v24, v7

    move-object/from16 v27, v10

    invoke-direct/range {v24 .. v29}, Lf/d/a/k;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_d

    :cond_20
    invoke-virtual {v4, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_d

    :cond_21
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto/16 :goto_17

    :pswitch_4
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    const/4 v7, 0x0

    :cond_22
    :goto_10
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v8

    if-eqz v8, :cond_24

    invoke-static {v0, v9}, Lf/d/a/a0/r;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/l/e;

    move-result-object v8

    iget-object v10, v8, Lf/d/a/y/l/e;->e:Lf/d/a/y/l/e$a;

    sget-object v11, Lf/d/a/y/l/e$a;->f:Lf/d/a/y/l/e$a;

    if-ne v10, v11, :cond_23

    add-int/lit8 v7, v7, 0x1

    :cond_23
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v10, v8, Lf/d/a/y/l/e;->d:J

    invoke-virtual {v2, v10, v11, v8}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    const/4 v8, 0x4

    if-le v7, v8, :cond_22

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "You have "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, " images. Lottie should primarily be used with shapes. If you are using Adobe Illustrator, convert the Illustrator layers to shape layers."

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lf/d/a/b0/c;->b(Ljava/lang/String;)V

    goto :goto_10

    :cond_24
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto/16 :goto_17

    :pswitch_5
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v7

    const-string v8, "\\."

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v10, v7, v8

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    const/4 v10, 0x1

    aget-object v11, v7, v10

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    const/4 v14, 0x2

    aget-object v7, v7, v14

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/4 v14, 0x4

    if-ge v8, v14, :cond_25

    goto :goto_11

    :cond_25
    if-le v8, v14, :cond_26

    goto :goto_12

    :cond_26
    if-ge v11, v14, :cond_27

    goto :goto_11

    :cond_27
    if-le v11, v14, :cond_28

    goto :goto_12

    :cond_28
    if-ltz v7, :cond_29

    goto :goto_12

    :cond_29
    :goto_11
    const/4 v10, 0x0

    :goto_12
    if-nez v10, :cond_2a

    const-string v7, "Lottie only supports bodymovin >= 4.4.0"

    invoke-virtual {v9, v7}, Lf/d/a/d;->a(Ljava/lang/String;)V

    goto :goto_17

    :pswitch_6
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v7

    double-to-float v10, v7

    goto :goto_14

    :pswitch_7
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v7

    double-to-float v7, v7

    const v8, 0x3c23d70a    # 0.01f

    sub-float v15, v7, v8

    goto :goto_13

    :pswitch_8
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v7

    double-to-float v14, v7

    :goto_13
    move/from16 v10, v17

    :goto_14
    move-object/from16 v7, v21

    :goto_15
    move-object/from16 v8, v23

    goto/16 :goto_0

    :pswitch_9
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v13

    goto :goto_16

    :pswitch_a
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v20, v14

    move/from16 v22, v15

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v12

    :goto_16
    move/from16 v10, v17

    goto/16 :goto_0

    :cond_2a
    :goto_17
    move/from16 v10, v17

    move/from16 v14, v20

    move-object/from16 v7, v21

    move/from16 v15, v22

    goto :goto_15

    :cond_2b
    move-object/from16 v21, v7

    move-object/from16 v23, v8

    move/from16 v17, v10

    move/from16 v20, v14

    move/from16 v22, v15

    int-to-float v0, v12

    mul-float v0, v0, v1

    float-to-int v0, v0

    int-to-float v7, v13

    mul-float v7, v7, v1

    float-to-int v1, v7

    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    invoke-direct {v7, v8, v8, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v7, v9, Lf/d/a/d;->j:Landroid/graphics/Rect;

    move/from16 v10, v20

    iput v10, v9, Lf/d/a/d;->k:F

    move/from16 v15, v22

    iput v15, v9, Lf/d/a/d;->l:F

    move/from16 v10, v17

    iput v10, v9, Lf/d/a/d;->m:F

    iput-object v3, v9, Lf/d/a/d;->i:Ljava/util/List;

    iput-object v2, v9, Lf/d/a/d;->h:Landroidx/collection/LongSparseArray;

    iput-object v4, v9, Lf/d/a/d;->c:Ljava/util/Map;

    iput-object v5, v9, Lf/d/a/d;->d:Ljava/util/Map;

    move-object/from16 v0, v23

    iput-object v0, v9, Lf/d/a/d;->g:Landroidx/collection/SparseArrayCompat;

    iput-object v6, v9, Lf/d/a/d;->e:Ljava/util/Map;

    move-object/from16 v0, v21

    iput-object v0, v9, Lf/d/a/d;->f:Ljava/util/List;

    return-object v9

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
