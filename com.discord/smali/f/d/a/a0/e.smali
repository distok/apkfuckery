.class public Lf/d/a/a0/e;
.super Ljava/lang/Object;
.source "ColorParser.java"

# interfaces
.implements Lf/d/a/a0/g0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/d/a/a0/g0<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/d/a/a0/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/d/a/a0/e;

    invoke-direct {v0}, Lf/d/a/a0/e;-><init>()V

    sput-object v0, Lf/d/a/a0/e;->a:Lf/d/a/a0/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/a0/h0/c;F)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object p2

    sget-object v0, Lf/d/a/a0/h0/c$b;->d:Lf/d/a/a0/h0/c$b;

    if-ne p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->a()V

    :cond_1
    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v0

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v2

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v4

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v6

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->c()V

    :cond_2
    const-wide/high16 p1, 0x3ff0000000000000L    # 1.0

    cmpg-double v8, v0, p1

    if-gtz v8, :cond_3

    cmpg-double v8, v2, p1

    if-gtz v8, :cond_3

    cmpg-double v8, v4, p1

    if-gtz v8, :cond_3

    const-wide v8, 0x406fe00000000000L    # 255.0

    mul-double v0, v0, v8

    mul-double v2, v2, v8

    mul-double v4, v4, v8

    cmpg-double v10, v6, p1

    if-gtz v10, :cond_3

    mul-double v6, v6, v8

    :cond_3
    double-to-int p1, v6

    double-to-int p2, v0

    double-to-int v0, v2

    double-to-int v1, v4

    invoke-static {p1, p2, v0, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
