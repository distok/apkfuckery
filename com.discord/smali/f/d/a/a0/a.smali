.class public Lf/d/a/a0/a;
.super Ljava/lang/Object;
.source "AnimatablePathValueParser.java"


# static fields
.field public static a:Lf/d/a/a0/h0/c$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const-string v0, "k"

    const-string/jumbo v1, "x"

    const-string/jumbo v2, "y"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/a;->a:Lf/d/a/a0/h0/c$a;

    return-void
.end method

.method public static a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/e;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object v1

    sget-object v2, Lf/d/a/a0/h0/c$b;->d:Lf/d/a/a0/h0/c$b;

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_0
    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object v1

    sget-object v2, Lf/d/a/a0/h0/c$b;->f:Lf/d/a/a0/h0/c$b;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :goto_1
    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v2

    sget-object v3, Lf/d/a/a0/u;->a:Lf/d/a/a0/u;

    invoke-static {p0, p1, v2, v3, v1}, Lf/d/a/a0/p;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;FLf/d/a/a0/g0;Z)Lf/d/a/c0/a;

    move-result-object v1

    new-instance v2, Lf/d/a/w/c/h;

    invoke-direct {v2, p1, v1}, Lf/d/a/w/c/h;-><init>(Lf/d/a/d;Lf/d/a/c0/a;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->c()V

    invoke-static {v0}, Lf/d/a/a0/q;->b(Ljava/util/List;)V

    goto :goto_2

    :cond_2
    new-instance p1, Lf/d/a/c0/a;

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v1

    invoke-static {p0, v1}, Lf/d/a/a0/o;->b(Lf/d/a/a0/h0/c;F)Landroid/graphics/PointF;

    move-result-object p0

    invoke-direct {p1, p0}, Lf/d/a/c0/a;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    new-instance p0, Lf/d/a/y/j/e;

    invoke-direct {p0, v0}, Lf/d/a/y/j/e;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method public static b(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/m;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/a0/h0/c;",
            "Lf/d/a/d;",
            ")",
            "Lf/d/a/y/j/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/d/a/a0/h0/c$b;->i:Lf/d/a/a0/h0/c$b;

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v3, v2

    move-object v4, v3

    const/4 v5, 0x0

    :goto_0
    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object v6

    sget-object v7, Lf/d/a/a0/h0/c$b;->g:Lf/d/a/a0/h0/c$b;

    if-eq v6, v7, :cond_5

    sget-object v6, Lf/d/a/a0/a;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {p0, v6}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v6

    if-eqz v6, :cond_4

    if-eq v6, v1, :cond_2

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object v6

    if-ne v6, v0, :cond_1

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_1

    :cond_1
    invoke-static {p0, p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v4

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object v6

    if-ne v6, v0, :cond_3

    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->w()V

    :goto_1
    const/4 v5, 0x1

    goto :goto_0

    :cond_3
    invoke-static {p0, p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v3

    goto :goto_0

    :cond_4
    invoke-static {p0, p1}, Lf/d/a/a0/a;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/e;

    move-result-object v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lf/d/a/a0/h0/c;->d()V

    if-eqz v5, :cond_6

    const-string p0, "Lottie doesn\'t support expressions."

    invoke-virtual {p1, p0}, Lf/d/a/d;->a(Ljava/lang/String;)V

    :cond_6
    if-eqz v2, :cond_7

    return-object v2

    :cond_7
    new-instance p0, Lf/d/a/y/j/i;

    invoke-direct {p0, v3, v4}, Lf/d/a/y/j/i;-><init>(Lf/d/a/y/j/b;Lf/d/a/y/j/b;)V

    return-object p0
.end method
