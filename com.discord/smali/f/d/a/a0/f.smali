.class public Lf/d/a/a0/f;
.super Ljava/lang/Object;
.source "ContentModelParser.java"


# static fields
.field public static a:Lf/d/a/a0/h0/c$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "ty"

    const-string v1, "d"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/f;->a:Lf/d/a/a0/h0/c$a;

    return-void
.end method

.method public static a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/k/b;
    .locals 30
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    sget-object v2, Lf/d/a/y/k/f;->d:Lf/d/a/y/k/f;

    sget-object v3, Lf/d/a/y/k/f;->e:Lf/d/a/y/k/f;

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v4, 0x2

    const/4 v5, 0x2

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v6, :cond_2

    sget-object v6, Lf/d/a/a0/f;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v6}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v6

    if-eqz v6, :cond_1

    if-eq v6, v7, :cond_0

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_0

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v5

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_2
    move-object v6, v8

    :goto_1
    if-nez v6, :cond_3

    return-object v8

    :cond_3
    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v8

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x0

    sparse-switch v8, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    const-string/jumbo v8, "tr"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    goto/16 :goto_2

    :cond_4
    const/16 v8, 0xc

    goto/16 :goto_3

    :sswitch_1
    const-string/jumbo v8, "tm"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    goto/16 :goto_2

    :cond_5
    const/16 v8, 0xb

    goto/16 :goto_3

    :sswitch_2
    const-string/jumbo v8, "st"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    goto/16 :goto_2

    :cond_6
    const/16 v8, 0xa

    goto/16 :goto_3

    :sswitch_3
    const-string/jumbo v8, "sr"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    goto/16 :goto_2

    :cond_7
    const/16 v8, 0x9

    goto/16 :goto_3

    :sswitch_4
    const-string v8, "sh"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    goto/16 :goto_2

    :cond_8
    const/16 v8, 0x8

    goto/16 :goto_3

    :sswitch_5
    const-string v8, "rp"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    goto :goto_2

    :cond_9
    const/4 v8, 0x7

    goto :goto_3

    :sswitch_6
    const-string v8, "rc"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    goto :goto_2

    :cond_a
    const/4 v8, 0x6

    goto :goto_3

    :sswitch_7
    const-string v8, "mm"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    goto :goto_2

    :cond_b
    const/4 v8, 0x5

    goto :goto_3

    :sswitch_8
    const-string v8, "gs"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_c

    goto :goto_2

    :cond_c
    const/4 v8, 0x4

    goto :goto_3

    :sswitch_9
    const-string v8, "gr"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    goto :goto_2

    :cond_d
    const/4 v8, 0x3

    goto :goto_3

    :sswitch_a
    const-string v8, "gf"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_e

    goto :goto_2

    :cond_e
    const/4 v8, 0x2

    goto :goto_3

    :sswitch_b
    const-string v8, "fl"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_f

    goto :goto_2

    :cond_f
    const/4 v8, 0x1

    goto :goto_3

    :sswitch_c
    const-string v8, "el"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_10

    goto :goto_2

    :cond_10
    const/4 v8, 0x0

    goto :goto_3

    :goto_2
    const/4 v8, -0x1

    :goto_3
    const-string v13, "o"

    const-string v14, "g"

    const-string v15, "d"

    packed-switch v8, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown shape type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf/d/a/b0/c;->b(Ljava/lang/String;)V

    const/4 v1, 0x0

    goto/16 :goto_1e

    :pswitch_0
    invoke-static/range {p0 .. p1}, Lf/d/a/a0/c;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/l;

    move-result-object v1

    goto/16 :goto_1e

    :pswitch_1
    sget-object v2, Lf/d/a/a0/f0;->a:Lf/d/a/a0/h0/c$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v13, 0x0

    move-object v15, v2

    move-object/from16 v16, v3

    move-object/from16 v17, v5

    move-object/from16 v18, v6

    move-object/from16 v19, v8

    const/16 v20, 0x0

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v2

    if-eqz v2, :cond_19

    sget-object v2, Lf/d/a/a0/f0;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v2}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v2

    if-eqz v2, :cond_18

    if-eq v2, v7, :cond_17

    if-eq v2, v4, :cond_16

    if-eq v2, v9, :cond_15

    if-eq v2, v10, :cond_12

    if-eq v2, v11, :cond_11

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_4

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v20

    goto :goto_4

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v2

    if-eq v2, v7, :cond_14

    if-ne v2, v4, :cond_13

    sget-object v2, Lf/d/a/y/k/q$a;->e:Lf/d/a/y/k/q$a;

    goto :goto_5

    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown trim path type "

    invoke-static {v1, v2}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    sget-object v2, Lf/d/a/y/k/q$a;->d:Lf/d/a/y/k/q$a;

    :goto_5
    move-object/from16 v16, v2

    goto :goto_4

    :cond_15
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v15

    goto :goto_4

    :cond_16
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v19

    goto :goto_4

    :cond_17
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v18

    goto :goto_4

    :cond_18
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v17

    goto :goto_4

    :cond_19
    new-instance v1, Lf/d/a/y/k/q;

    move-object v14, v1

    invoke-direct/range {v14 .. v20}, Lf/d/a/y/k/q;-><init>(Ljava/lang/String;Lf/d/a/y/k/q$a;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Z)V

    goto/16 :goto_1e

    :pswitch_2
    sget-object v2, Lf/d/a/a0/e0;->a:Lf/d/a/a0/h0/c$a;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v17, v3

    move-object/from16 v18, v5

    move-object/from16 v20, v6

    move-object/from16 v21, v8

    move-object/from16 v22, v9

    move-object/from16 v23, v10

    move-object/from16 v24, v11

    const/16 v25, 0x0

    const/16 v26, 0x0

    :cond_1a
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v3

    if-eqz v3, :cond_27

    sget-object v3, Lf/d/a/a0/e0;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v3}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_6

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v3

    if-eqz v3, :cond_26

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v3, 0x0

    const/4 v5, 0x0

    :goto_8
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v6

    if-eqz v6, :cond_1d

    sget-object v6, Lf/d/a/a0/e0;->b:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v6}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v6

    if-eqz v6, :cond_1c

    if-eq v6, v7, :cond_1b

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_8

    :cond_1b
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v3

    goto :goto_8

    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/16 v8, 0x64

    if-eq v6, v8, :cond_22

    const/16 v8, 0x67

    if-eq v6, v8, :cond_20

    const/16 v8, 0x6f

    if-eq v6, v8, :cond_1e

    goto :goto_9

    :cond_1e
    invoke-virtual {v5, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1f

    goto :goto_9

    :cond_1f
    const/4 v5, 0x2

    goto :goto_a

    :cond_20
    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_21

    goto :goto_9

    :cond_21
    const/4 v5, 0x1

    goto :goto_a

    :cond_22
    invoke-virtual {v5, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_23

    :goto_9
    const/4 v5, -0x1

    goto :goto_a

    :cond_23
    const/4 v5, 0x0

    :goto_a
    if-eqz v5, :cond_25

    if-eq v5, v7, :cond_25

    if-eq v5, v4, :cond_24

    goto :goto_7

    :cond_24
    move-object/from16 v18, v3

    goto :goto_7

    :cond_25
    iput-boolean v7, v1, Lf/d/a/d;->n:Z

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_26
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v7, :cond_1a

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v26

    goto/16 :goto_6

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v5

    double-to-float v3, v5

    move/from16 v25, v3

    goto/16 :goto_6

    :pswitch_6
    invoke-static {}, Lf/d/a/y/k/p$b;->values()[Lf/d/a/y/k/p$b;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v5

    sub-int/2addr v5, v7

    aget-object v24, v3, v5

    goto/16 :goto_6

    :pswitch_7
    invoke-static {}, Lf/d/a/y/k/p$a;->values()[Lf/d/a/y/k/p$a;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v5

    sub-int/2addr v5, v7

    aget-object v23, v3, v5

    goto/16 :goto_6

    :pswitch_8
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->S(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/d;

    move-result-object v21

    goto/16 :goto_6

    :pswitch_9
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v22

    goto/16 :goto_6

    :pswitch_a
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->P(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/a;

    move-result-object v20

    goto/16 :goto_6

    :pswitch_b
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_6

    :cond_27
    new-instance v1, Lf/d/a/y/k/p;

    move-object/from16 v16, v1

    move-object/from16 v19, v2

    invoke-direct/range {v16 .. v26}, Lf/d/a/y/k/p;-><init>(Ljava/lang/String;Lf/d/a/y/j/b;Ljava/util/List;Lf/d/a/y/j/a;Lf/d/a/y/j/d;Lf/d/a/y/j/b;Lf/d/a/y/k/p$a;Lf/d/a/y/k/p$b;FZ)V

    goto/16 :goto_1e

    :pswitch_c
    sget-object v2, Lf/d/a/a0/w;->a:Lf/d/a/a0/h0/c$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v14, v2

    move-object v15, v3

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    move-object/from16 v18, v6

    move-object/from16 v19, v7

    move-object/from16 v20, v8

    move-object/from16 v21, v9

    move-object/from16 v22, v10

    const/16 v23, 0x0

    :goto_b
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v2

    if-eqz v2, :cond_28

    sget-object v2, Lf/d/a/a0/w;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v2}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v2

    packed-switch v2, :pswitch_data_2

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_b

    :pswitch_d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v23

    goto :goto_b

    :pswitch_e
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v21

    goto :goto_b

    :pswitch_f
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v19

    goto :goto_b

    :pswitch_10
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v22

    goto :goto_b

    :pswitch_11
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v20

    goto :goto_b

    :pswitch_12
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v18

    goto :goto_b

    :pswitch_13
    invoke-static/range {p0 .. p1}, Lf/d/a/a0/a;->b(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/m;

    move-result-object v17

    goto :goto_b

    :pswitch_14
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v16

    goto :goto_b

    :pswitch_15
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v2

    invoke-static {v2}, Lf/d/a/y/k/i$a;->f(I)Lf/d/a/y/k/i$a;

    move-result-object v15

    goto :goto_b

    :pswitch_16
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v14

    goto :goto_b

    :cond_28
    new-instance v1, Lf/d/a/y/k/i;

    move-object v13, v1

    invoke-direct/range {v13 .. v23}, Lf/d/a/y/k/i;-><init>(Ljava/lang/String;Lf/d/a/y/k/i$a;Lf/d/a/y/j/b;Lf/d/a/y/j/m;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Z)V

    goto/16 :goto_1e

    :pswitch_17
    sget-object v2, Lf/d/a/a0/d0;->a:Lf/d/a/a0/h0/c$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    :goto_c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v6

    if-eqz v6, :cond_2d

    sget-object v6, Lf/d/a/a0/d0;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v6}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v6

    if-eqz v6, :cond_2c

    if-eq v6, v7, :cond_2b

    if-eq v6, v4, :cond_2a

    if-eq v6, v9, :cond_29

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_c

    :cond_29
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v3

    goto :goto_c

    :cond_2a
    new-instance v2, Lf/d/a/y/j/h;

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v6

    sget-object v8, Lf/d/a/a0/a0;->a:Lf/d/a/a0/a0;

    invoke-static {v0, v1, v6, v8}, Lf/d/a/a0/q;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;FLf/d/a/a0/g0;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v2, v6}, Lf/d/a/y/j/h;-><init>(Ljava/util/List;)V

    goto :goto_c

    :cond_2b
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v12

    goto :goto_c

    :cond_2c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v5

    goto :goto_c

    :cond_2d
    new-instance v1, Lf/d/a/y/k/o;

    invoke-direct {v1, v5, v12, v2, v3}, Lf/d/a/y/k/o;-><init>(Ljava/lang/String;ILf/d/a/y/j/h;Z)V

    goto/16 :goto_1e

    :pswitch_18
    sget-object v2, Lf/d/a/a0/y;->a:Lf/d/a/a0/h0/c$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v14, v2

    move-object v15, v3

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    const/16 v18, 0x0

    :goto_d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v2

    if-eqz v2, :cond_33

    sget-object v2, Lf/d/a/a0/y;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v2}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v2

    if-eqz v2, :cond_32

    if-eq v2, v7, :cond_31

    if-eq v2, v4, :cond_30

    if-eq v2, v9, :cond_2f

    if-eq v2, v10, :cond_2e

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_d

    :cond_2e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v18

    goto :goto_d

    :cond_2f
    invoke-static/range {p0 .. p1}, Lf/d/a/a0/c;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/l;

    move-result-object v17

    goto :goto_d

    :cond_30
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v16

    goto :goto_d

    :cond_31
    invoke-static {v0, v1, v12}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v15

    goto :goto_d

    :cond_32
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v14

    goto :goto_d

    :cond_33
    new-instance v1, Lf/d/a/y/k/k;

    move-object v13, v1

    invoke-direct/range {v13 .. v18}, Lf/d/a/y/k/k;-><init>(Ljava/lang/String;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Lf/d/a/y/j/l;Z)V

    goto/16 :goto_1e

    :pswitch_19
    sget-object v2, Lf/d/a/a0/x;->a:Lf/d/a/a0/h0/c$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v12, v2

    move-object v13, v3

    move-object v14, v5

    move-object v15, v6

    const/16 v16, 0x0

    :goto_e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v2

    if-eqz v2, :cond_39

    sget-object v2, Lf/d/a/a0/x;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v2}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v2

    if-eqz v2, :cond_38

    if-eq v2, v7, :cond_37

    if-eq v2, v4, :cond_36

    if-eq v2, v9, :cond_35

    if-eq v2, v10, :cond_34

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_e

    :cond_34
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v16

    goto :goto_e

    :cond_35
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v15

    goto :goto_e

    :cond_36
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->T(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/f;

    move-result-object v14

    goto :goto_e

    :cond_37
    invoke-static/range {p0 .. p1}, Lf/d/a/a0/a;->b(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/m;

    move-result-object v13

    goto :goto_e

    :cond_38
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v12

    goto :goto_e

    :cond_39
    new-instance v1, Lf/d/a/y/k/j;

    move-object v11, v1

    invoke-direct/range {v11 .. v16}, Lf/d/a/y/k/j;-><init>(Ljava/lang/String;Lf/d/a/y/j/m;Lf/d/a/y/j/f;Lf/d/a/y/j/b;Z)V

    goto/16 :goto_1e

    :pswitch_1a
    sget-object v2, Lf/d/a/a0/t;->a:Lf/d/a/a0/h0/c$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_f
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_42

    sget-object v5, Lf/d/a/a0/t;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v5}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v5

    if-eqz v5, :cond_41

    if-eq v5, v7, :cond_3b

    if-eq v5, v4, :cond_3a

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_f

    :cond_3a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v12

    goto :goto_f

    :cond_3b
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v2

    sget-object v5, Lf/d/a/y/k/h$a;->d:Lf/d/a/y/k/h$a;

    if-eq v2, v7, :cond_40

    if-eq v2, v4, :cond_3f

    if-eq v2, v9, :cond_3e

    if-eq v2, v10, :cond_3d

    if-eq v2, v11, :cond_3c

    goto :goto_10

    :cond_3c
    sget-object v2, Lf/d/a/y/k/h$a;->h:Lf/d/a/y/k/h$a;

    goto :goto_f

    :cond_3d
    sget-object v2, Lf/d/a/y/k/h$a;->g:Lf/d/a/y/k/h$a;

    goto :goto_f

    :cond_3e
    sget-object v2, Lf/d/a/y/k/h$a;->f:Lf/d/a/y/k/h$a;

    goto :goto_f

    :cond_3f
    sget-object v2, Lf/d/a/y/k/h$a;->e:Lf/d/a/y/k/h$a;

    goto :goto_f

    :cond_40
    :goto_10
    move-object v2, v5

    goto :goto_f

    :cond_41
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v3

    goto :goto_f

    :cond_42
    new-instance v4, Lf/d/a/y/k/h;

    invoke-direct {v4, v3, v2, v12}, Lf/d/a/y/k/h;-><init>(Ljava/lang/String;Lf/d/a/y/k/h$a;Z)V

    const-string v2, "Animation contains merge paths. Merge paths are only supported on KitKat+ and must be manually enabled by calling enableMergePathsForKitKatAndAbove()."

    invoke-virtual {v1, v2}, Lf/d/a/d;->a(Ljava/lang/String;)V

    move-object v1, v4

    goto/16 :goto_1e

    :pswitch_1b
    sget-object v4, Lf/d/a/a0/m;->a:Lf/d/a/a0/h0/c$a;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v19, v8

    move-object/from16 v21, v10

    move-object/from16 v22, v11

    move-object/from16 v23, v16

    move-object/from16 v24, v17

    move-object/from16 v25, v18

    move-object/from16 v28, v20

    const/16 v26, 0x0

    const/16 v29, 0x0

    move-object/from16 v17, v5

    move-object/from16 v18, v6

    move-object/from16 v20, v9

    :cond_43
    :goto_11
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_4f

    sget-object v5, Lf/d/a/a0/m;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v5}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v5

    packed-switch v5, :pswitch_data_3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_11

    :pswitch_1c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :cond_44
    :goto_12
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_4a

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_13
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v8

    if-eqz v8, :cond_47

    sget-object v8, Lf/d/a/a0/m;->c:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v8}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v8

    if-eqz v8, :cond_46

    if-eq v8, v7, :cond_45

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_13

    :cond_45
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v5

    goto :goto_13

    :cond_46
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v6

    goto :goto_13

    :cond_47
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    invoke-virtual {v6, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_48

    move-object/from16 v28, v5

    goto :goto_12

    :cond_48
    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_49

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_44

    :cond_49
    iput-boolean v7, v1, Lf/d/a/d;->n:Z

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_12

    :cond_4a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v5, v7, :cond_43

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11

    :pswitch_1d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v29

    goto :goto_11

    :pswitch_1e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v5

    double-to-float v5, v5

    move/from16 v26, v5

    goto/16 :goto_11

    :pswitch_1f
    invoke-static {}, Lf/d/a/y/k/p$b;->values()[Lf/d/a/y/k/p$b;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v6

    sub-int/2addr v6, v7

    aget-object v25, v5, v6

    goto/16 :goto_11

    :pswitch_20
    invoke-static {}, Lf/d/a/y/k/p$a;->values()[Lf/d/a/y/k/p$a;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v6

    sub-int/2addr v6, v7

    aget-object v24, v5, v6

    goto/16 :goto_11

    :pswitch_21
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v23

    goto/16 :goto_11

    :pswitch_22
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->T(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/f;

    move-result-object v22

    goto/16 :goto_11

    :pswitch_23
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->T(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/f;

    move-result-object v21

    goto/16 :goto_11

    :pswitch_24
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v5

    if-ne v5, v7, :cond_4b

    move-object/from16 v18, v2

    goto/16 :goto_11

    :cond_4b
    move-object/from16 v18, v3

    goto/16 :goto_11

    :pswitch_25
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->S(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/d;

    move-result-object v20

    goto/16 :goto_11

    :pswitch_26
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v5, -0x1

    :goto_14
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v6

    if-eqz v6, :cond_4e

    sget-object v6, Lf/d/a/a0/m;->b:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v6}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v6

    if-eqz v6, :cond_4d

    if-eq v6, v7, :cond_4c

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_14

    :cond_4c
    new-instance v6, Lf/d/a/y/j/c;

    new-instance v8, Lf/d/a/a0/k;

    invoke-direct {v8, v5}, Lf/d/a/a0/k;-><init>(I)V

    invoke-static {v0, v1, v8}, Ls/a/b/b/a;->O(Lf/d/a/a0/h0/c;Lf/d/a/d;Lf/d/a/a0/g0;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Lf/d/a/y/j/c;-><init>(Ljava/util/List;)V

    move-object/from16 v19, v6

    goto :goto_14

    :cond_4d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v5

    goto :goto_14

    :cond_4e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    goto/16 :goto_11

    :pswitch_27
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_11

    :cond_4f
    new-instance v1, Lf/d/a/y/k/e;

    move-object/from16 v16, v1

    move-object/from16 v27, v4

    invoke-direct/range {v16 .. v29}, Lf/d/a/y/k/e;-><init>(Ljava/lang/String;Lf/d/a/y/k/f;Lf/d/a/y/j/c;Lf/d/a/y/j/d;Lf/d/a/y/j/f;Lf/d/a/y/j/f;Lf/d/a/y/j/b;Lf/d/a/y/k/p$a;Lf/d/a/y/k/p$b;FLjava/util/List;Lf/d/a/y/j/b;Z)V

    goto/16 :goto_1e

    :pswitch_28
    sget-object v2, Lf/d/a/a0/c0;->a:Lf/d/a/a0/h0/c$a;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_15
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_55

    sget-object v5, Lf/d/a/a0/c0;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v5}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v5

    if-eqz v5, :cond_54

    if-eq v5, v7, :cond_53

    if-eq v5, v4, :cond_50

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_15

    :cond_50
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :cond_51
    :goto_16
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_52

    invoke-static/range {p0 .. p1}, Lf/d/a/a0/f;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/k/b;

    move-result-object v5

    if-eqz v5, :cond_51

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_16

    :cond_52
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto :goto_15

    :cond_53
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v12

    goto :goto_15

    :cond_54
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v3

    goto :goto_15

    :cond_55
    new-instance v1, Lf/d/a/y/k/n;

    invoke-direct {v1, v3, v2, v12}, Lf/d/a/y/k/n;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    goto/16 :goto_1e

    :pswitch_29
    sget-object v4, Lf/d/a/a0/l;->a:Lf/d/a/a0/h0/c$a;

    sget-object v4, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v16, v4

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v17, v8

    move-object/from16 v18, v9

    move-object/from16 v19, v10

    move-object/from16 v20, v11

    const/16 v23, 0x0

    :goto_17
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v4

    if-eqz v4, :cond_5b

    sget-object v4, Lf/d/a/a0/l;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v4}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v4

    packed-switch v4, :pswitch_data_4

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_17

    :pswitch_2a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v23

    goto :goto_17

    :pswitch_2b
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v4

    if-ne v4, v7, :cond_56

    sget-object v4, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    goto :goto_18

    :cond_56
    sget-object v4, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    :goto_18
    move-object/from16 v16, v4

    goto :goto_17

    :pswitch_2c
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->T(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/f;

    move-result-object v20

    goto :goto_17

    :pswitch_2d
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->T(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/f;

    move-result-object v19

    goto :goto_17

    :pswitch_2e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v4

    if-ne v4, v7, :cond_57

    move-object v15, v2

    goto :goto_17

    :cond_57
    move-object v15, v3

    goto :goto_17

    :pswitch_2f
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->S(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/d;

    move-result-object v18

    goto :goto_17

    :pswitch_30
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v4, -0x1

    :goto_19
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_5a

    sget-object v5, Lf/d/a/a0/l;->b:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v5}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v5

    if-eqz v5, :cond_59

    if-eq v5, v7, :cond_58

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_19

    :cond_58
    new-instance v5, Lf/d/a/y/j/c;

    new-instance v6, Lf/d/a/a0/k;

    invoke-direct {v6, v4}, Lf/d/a/a0/k;-><init>(I)V

    invoke-static {v0, v1, v6}, Ls/a/b/b/a;->O(Lf/d/a/a0/h0/c;Lf/d/a/d;Lf/d/a/a0/g0;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Lf/d/a/y/j/c;-><init>(Ljava/util/List;)V

    move-object/from16 v17, v5

    goto :goto_19

    :cond_59
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v4

    goto :goto_19

    :cond_5a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    goto :goto_17

    :pswitch_31
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v14

    goto :goto_17

    :cond_5b
    new-instance v1, Lf/d/a/y/k/d;

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object v13, v1

    invoke-direct/range {v13 .. v23}, Lf/d/a/y/k/d;-><init>(Ljava/lang/String;Lf/d/a/y/k/f;Landroid/graphics/Path$FillType;Lf/d/a/y/j/c;Lf/d/a/y/j/d;Lf/d/a/y/j/f;Lf/d/a/y/j/f;Lf/d/a/y/j/b;Lf/d/a/y/j/b;Z)V

    goto/16 :goto_1e

    :pswitch_32
    sget-object v2, Lf/d/a/a0/b0;->a:Lf/d/a/a0/h0/c$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v12, 0x0

    move-object v13, v3

    move-object/from16 v16, v6

    move-object/from16 v17, v8

    const/4 v14, 0x0

    const/16 v18, 0x0

    :goto_1a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v3

    if-eqz v3, :cond_62

    sget-object v3, Lf/d/a/a0/b0;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v3}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v3

    if-eqz v3, :cond_61

    if-eq v3, v7, :cond_60

    if-eq v3, v4, :cond_5f

    if-eq v3, v9, :cond_5e

    if-eq v3, v10, :cond_5d

    if-eq v3, v11, :cond_5c

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_1a

    :cond_5c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v18

    goto :goto_1a

    :cond_5d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v2

    goto :goto_1a

    :cond_5e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v14

    goto :goto_1a

    :cond_5f
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->S(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/d;

    move-result-object v17

    goto :goto_1a

    :cond_60
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->P(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/a;

    move-result-object v16

    goto :goto_1a

    :cond_61
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v13

    goto :goto_1a

    :cond_62
    if-ne v2, v7, :cond_63

    sget-object v1, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    goto :goto_1b

    :cond_63
    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    :goto_1b
    move-object v15, v1

    new-instance v1, Lf/d/a/y/k/m;

    move-object v12, v1

    invoke-direct/range {v12 .. v18}, Lf/d/a/y/k/m;-><init>(Ljava/lang/String;ZLandroid/graphics/Path$FillType;Lf/d/a/y/j/a;Lf/d/a/y/j/d;Z)V

    goto :goto_1e

    :pswitch_33
    sget-object v2, Lf/d/a/a0/d;->a:Lf/d/a/a0/h0/c$a;

    if-ne v5, v9, :cond_64

    const/4 v2, 0x1

    goto :goto_1c

    :cond_64
    const/4 v2, 0x0

    :goto_1c
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move v15, v2

    move-object v12, v3

    move-object v13, v5

    move-object v14, v6

    const/16 v16, 0x0

    :goto_1d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v2

    if-eqz v2, :cond_6b

    sget-object v2, Lf/d/a/a0/d;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v2}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v2

    if-eqz v2, :cond_6a

    if-eq v2, v7, :cond_69

    if-eq v2, v4, :cond_68

    if-eq v2, v9, :cond_67

    if-eq v2, v10, :cond_65

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_1d

    :cond_65
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v2

    if-ne v2, v9, :cond_66

    const/4 v15, 0x1

    goto :goto_1d

    :cond_66
    const/4 v15, 0x0

    goto :goto_1d

    :cond_67
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v16

    goto :goto_1d

    :cond_68
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->T(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/f;

    move-result-object v14

    goto :goto_1d

    :cond_69
    invoke-static/range {p0 .. p1}, Lf/d/a/a0/a;->b(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/m;

    move-result-object v13

    goto :goto_1d

    :cond_6a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v12

    goto :goto_1d

    :cond_6b
    new-instance v1, Lf/d/a/y/k/a;

    move-object v11, v1

    invoke-direct/range {v11 .. v16}, Lf/d/a/y/k/a;-><init>(Ljava/lang/String;Lf/d/a/y/j/m;Lf/d/a/y/j/f;ZZ)V

    :goto_1e
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v2

    if-eqz v2, :cond_6c

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_1e

    :cond_6c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        0xca7 -> :sswitch_c
        0xcc6 -> :sswitch_b
        0xcdf -> :sswitch_a
        0xceb -> :sswitch_9
        0xcec -> :sswitch_8
        0xda0 -> :sswitch_7
        0xe31 -> :sswitch_6
        0xe3e -> :sswitch_5
        0xe55 -> :sswitch_4
        0xe5f -> :sswitch_3
        0xe61 -> :sswitch_2
        0xe79 -> :sswitch_1
        0xe7e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_33
        :pswitch_32
        :pswitch_29
        :pswitch_28
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_c
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
    .end packed-switch
.end method
