.class public Lf/d/a/a0/z;
.super Ljava/lang/Object;
.source "ScaleXYParser.java"

# interfaces
.implements Lf/d/a/a0/g0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/d/a/a0/g0<",
        "Lf/d/a/c0/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/d/a/a0/z;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/d/a/a0/z;

    invoke-direct {v0}, Lf/d/a/a0/z;-><init>()V

    sput-object v0, Lf/d/a/a0/z;->a:Lf/d/a/a0/z;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/a0/h0/c;F)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object v0

    sget-object v1, Lf/d/a/a0/h0/c$b;->d:Lf/d/a/a0/h0/c$b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->a()V

    :cond_1
    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v2

    double-to-float v2, v2

    :goto_1
    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->c()V

    :cond_3
    new-instance p1, Lf/d/a/c0/d;

    const/high16 v0, 0x42c80000    # 100.0f

    div-float/2addr v1, v0

    mul-float v1, v1, p2

    div-float/2addr v2, v0

    mul-float v2, v2, p2

    invoke-direct {p1, v1, v2}, Lf/d/a/c0/d;-><init>(FF)V

    return-object p1
.end method
