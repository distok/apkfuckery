.class public Lf/d/a/a0/r;
.super Ljava/lang/Object;
.source "LayerParser.java"


# static fields
.field public static final a:Lf/d/a/a0/h0/c$a;

.field public static final b:Lf/d/a/a0/h0/c$a;

.field public static final c:Lf/d/a/a0/h0/c$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 23

    const-string v0, "nm"

    const-string v1, "ind"

    const-string v2, "refId"

    const-string/jumbo v3, "ty"

    const-string v4, "parent"

    const-string/jumbo v5, "sw"

    const-string v6, "sh"

    const-string v7, "sc"

    const-string v8, "ks"

    const-string/jumbo v9, "tt"

    const-string v10, "masksProperties"

    const-string v11, "shapes"

    const-string/jumbo v12, "t"

    const-string v13, "ef"

    const-string/jumbo v14, "sr"

    const-string/jumbo v15, "st"

    const-string/jumbo v16, "w"

    const-string v17, "h"

    const-string v18, "ip"

    const-string v19, "op"

    const-string/jumbo v20, "tm"

    const-string v21, "cl"

    const-string v22, "hd"

    filled-new-array/range {v0 .. v22}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/r;->a:Lf/d/a/a0/h0/c$a;

    const-string v0, "d"

    const-string v1, "a"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/r;->b:Lf/d/a/a0/h0/c$a;

    const-string v0, "nm"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/r;->c:Lf/d/a/a0/h0/c$a;

    return-void
.end method

.method public static a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/l/e;
    .locals 42
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    sget-object v1, Lf/d/a/y/l/e$b;->d:Lf/d/a/y/l/e$b;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    const/4 v3, 0x0

    const-string v5, "UNSET"

    const-wide/16 v13, 0x0

    const-wide/16 v15, -0x1

    move-object/from16 v30, v1

    move-wide/from16 v17, v15

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/16 v16, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/high16 v24, 0x3f800000    # 1.0f

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    move-wide v14, v13

    move-object v13, v5

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_2d

    sget-object v5, Lf/d/a/a0/r;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v5}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v5

    const/4 v11, 0x1

    packed-switch v5, :pswitch_data_0

    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto/16 :goto_14

    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v32

    goto/16 :goto_16

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_16

    :pswitch_2
    invoke-static {v0, v7, v3}, Ls/a/b/b/a;->R(Lf/d/a/a0/h0/c;Lf/d/a/d;Z)Lf/d/a/y/j/b;

    move-result-object v31

    goto/16 :goto_16

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v4

    double-to-float v2, v4

    goto/16 :goto_16

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v4

    double-to-float v1, v4

    goto :goto_1

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v4

    int-to-float v4, v4

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v5

    mul-float v5, v5, v4

    float-to-int v4, v5

    move/from16 v27, v4

    goto :goto_1

    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v4

    int-to-float v4, v4

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v5

    mul-float v5, v5, v4

    float-to-int v4, v5

    move/from16 v26, v4

    goto :goto_1

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v4

    double-to-float v4, v4

    move/from16 v25, v4

    goto :goto_1

    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v4

    double-to-float v4, v4

    move/from16 v24, v4

    :goto_1
    move-object/from16 v37, v6

    goto/16 :goto_15

    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    :goto_3
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lf/d/a/a0/r;->c:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v5}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_3

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    goto :goto_2

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Lottie doesn\'t support layer effects. If you are using them for  fills, strokes, trim paths etc. then try adding them directly as contents  in your shape. Found: "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Lf/d/a/d;->a(Ljava/lang/String;)V

    move-object/from16 v37, v6

    const/4 v4, 0x0

    goto/16 :goto_b

    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_f

    sget-object v5, Lf/d/a/a0/r;->b:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v5}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v5

    if-eqz v5, :cond_e

    if-eq v5, v11, :cond_3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_4

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v5

    if-eqz v5, :cond_c

    sget-object v5, Lf/d/a/a0/b;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v5, 0x0

    :goto_5
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v29

    if-eqz v29, :cond_a

    sget-object v3, Lf/d/a/a0/b;->a:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v3}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    const/4 v3, 0x0

    goto :goto_5

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v29

    if-eqz v29, :cond_9

    sget-object v4, Lf/d/a/a0/b;->b:Lf/d/a/a0/h0/c$a;

    invoke-virtual {v0, v4}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v4

    if-eqz v4, :cond_8

    if-eq v4, v11, :cond_7

    const/4 v11, 0x2

    if-eq v4, v11, :cond_6

    const/4 v11, 0x3

    if-eq v4, v11, :cond_5

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_7

    :cond_5
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v5

    goto :goto_7

    :cond_6
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->Q(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/b;

    move-result-object v3

    goto :goto_7

    :cond_7
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->P(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/a;

    move-result-object v38

    goto :goto_7

    :cond_8
    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->P(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/a;

    move-result-object v37

    :goto_7
    const/4 v11, 0x1

    goto :goto_6

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    new-instance v4, Lf/d/a/y/j/k;

    move-object/from16 v11, v37

    move-object/from16 v37, v6

    move-object/from16 v6, v38

    invoke-direct {v4, v11, v6, v3, v5}, Lf/d/a/y/j/k;-><init>(Lf/d/a/y/j/a;Lf/d/a/y/j/a;Lf/d/a/y/j/b;Lf/d/a/y/j/b;)V

    move-object v5, v4

    move-object/from16 v6, v37

    const/4 v3, 0x0

    const/4 v11, 0x1

    goto :goto_5

    :cond_a
    move-object/from16 v37, v6

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    if-nez v5, :cond_b

    new-instance v3, Lf/d/a/y/j/k;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v4, v4, v4}, Lf/d/a/y/j/k;-><init>(Lf/d/a/y/j/a;Lf/d/a/y/j/a;Lf/d/a/y/j/b;Lf/d/a/y/j/b;)V

    move-object/from16 v29, v3

    goto :goto_8

    :cond_b
    const/4 v4, 0x0

    move-object/from16 v29, v5

    goto :goto_8

    :cond_c
    move-object/from16 v37, v6

    const/4 v4, 0x0

    :goto_8
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_8

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto :goto_9

    :cond_e
    move-object/from16 v37, v6

    const/4 v4, 0x0

    new-instance v3, Lf/d/a/y/j/j;

    sget-object v5, Lf/d/a/a0/g;->a:Lf/d/a/a0/g;

    invoke-static {v0, v7, v5}, Ls/a/b/b/a;->O(Lf/d/a/a0/h0/c;Lf/d/a/d;Lf/d/a/a0/g0;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v5}, Lf/d/a/y/j/j;-><init>(Ljava/util/List;)V

    move-object/from16 v28, v3

    :goto_9
    move-object/from16 v6, v37

    const/4 v3, 0x0

    const/4 v11, 0x1

    goto/16 :goto_4

    :cond_f
    move-object/from16 v37, v6

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    goto/16 :goto_15

    :pswitch_b
    move-object/from16 v37, v6

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :cond_10
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-static/range {p0 .. p1}, Lf/d/a/a0/f;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/k/b;

    move-result-object v3

    if-eqz v3, :cond_10

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    :goto_b
    move-wide/from16 v39, v14

    goto/16 :goto_14

    :pswitch_c
    move-object/from16 v37, v6

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->a()V

    :goto_c
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v3

    if-eqz v3, :cond_2b

    sget-object v3, Lf/d/a/y/k/g$a;->d:Lf/d/a/y/k/g$a;

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->b()V

    move-object v5, v4

    move-object v6, v5

    move-object v11, v6

    const/4 v4, 0x0

    :goto_d
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->e()Z

    move-result v38

    if-eqz v38, :cond_2a

    move-object/from16 v38, v3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-wide/from16 v39, v14

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v14

    const/16 v15, 0x6f

    const/16 v41, -0x1

    if-eq v14, v15, :cond_18

    const/16 v15, 0xe04

    if-eq v14, v15, :cond_16

    const v15, 0x197f1

    if-eq v14, v15, :cond_14

    const v15, 0x3339a3

    if-eq v14, v15, :cond_12

    goto :goto_e

    :cond_12
    const-string v14, "mode"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_13

    goto :goto_e

    :cond_13
    const/4 v14, 0x3

    goto :goto_f

    :cond_14
    const-string v14, "inv"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_15

    goto :goto_e

    :cond_15
    const/4 v14, 0x2

    goto :goto_f

    :cond_16
    const-string v14, "pt"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_17

    goto :goto_e

    :cond_17
    const/4 v14, 0x1

    goto :goto_f

    :cond_18
    const-string v14, "o"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_19

    :goto_e
    const/4 v14, -0x1

    goto :goto_f

    :cond_19
    const/4 v14, 0x0

    :goto_f
    if-eqz v14, :cond_29

    const/4 v15, 0x1

    if-eq v14, v15, :cond_28

    const/4 v15, 0x2

    if-eq v14, v15, :cond_27

    const/4 v15, 0x3

    if-eq v14, v15, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->w()V

    const/4 v15, 0x3

    goto/16 :goto_13

    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v14

    const/16 v15, 0x61

    if-eq v14, v15, :cond_21

    const/16 v15, 0x69

    if-eq v14, v15, :cond_1f

    const/16 v15, 0x6e

    if-eq v14, v15, :cond_1d

    const/16 v15, 0x73

    if-eq v14, v15, :cond_1b

    goto :goto_10

    :cond_1b
    const-string v14, "s"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1c

    goto :goto_10

    :cond_1c
    const/4 v5, 0x3

    goto :goto_11

    :cond_1d
    const-string v14, "n"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1e

    goto :goto_10

    :cond_1e
    const/4 v5, 0x2

    goto :goto_11

    :cond_1f
    const-string v14, "i"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_20

    goto :goto_10

    :cond_20
    const/4 v5, 0x1

    goto :goto_11

    :cond_21
    const-string v14, "a"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_22

    :goto_10
    const/4 v5, -0x1

    goto :goto_11

    :cond_22
    const/4 v5, 0x0

    :goto_11
    if-eqz v5, :cond_26

    const/4 v14, 0x1

    if-eq v5, v14, :cond_25

    const/4 v14, 0x2

    if-eq v5, v14, :cond_24

    const/4 v15, 0x3

    if-eq v5, v15, :cond_23

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unknown mask mode "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ". Defaulting to Add."

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lf/d/a/b0/c;->b(Ljava/lang/String;)V

    goto :goto_12

    :cond_23
    sget-object v5, Lf/d/a/y/k/g$a;->e:Lf/d/a/y/k/g$a;

    goto :goto_13

    :cond_24
    const/4 v15, 0x3

    sget-object v5, Lf/d/a/y/k/g$a;->g:Lf/d/a/y/k/g$a;

    goto :goto_13

    :cond_25
    const/4 v15, 0x3

    const-string v3, "Animation contains intersect masks. They are not supported but will be treated like add masks."

    invoke-virtual {v7, v3}, Lf/d/a/d;->a(Ljava/lang/String;)V

    sget-object v5, Lf/d/a/y/k/g$a;->f:Lf/d/a/y/k/g$a;

    goto :goto_13

    :cond_26
    const/4 v15, 0x3

    :goto_12
    move-object/from16 v5, v38

    goto :goto_13

    :cond_27
    const/4 v15, 0x3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->f()Z

    move-result v4

    goto :goto_13

    :cond_28
    const/4 v15, 0x3

    new-instance v6, Lf/d/a/y/j/h;

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v3

    sget-object v14, Lf/d/a/a0/a0;->a:Lf/d/a/a0/a0;

    invoke-static {v0, v7, v3, v14}, Lf/d/a/a0/q;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;FLf/d/a/a0/g0;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v6, v3}, Lf/d/a/y/j/h;-><init>(Ljava/util/List;)V

    goto :goto_13

    :cond_29
    const/4 v15, 0x3

    invoke-static/range {p0 .. p1}, Ls/a/b/b/a;->S(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/d;

    move-result-object v11

    :goto_13
    move-object/from16 v3, v38

    move-wide/from16 v14, v39

    goto/16 :goto_d

    :cond_2a
    move-wide/from16 v39, v14

    const/4 v15, 0x3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    new-instance v3, Lf/d/a/y/k/g;

    invoke-direct {v3, v5, v6, v11, v4}, Lf/d/a/y/k/g;-><init>(Lf/d/a/y/k/g$a;Lf/d/a/y/j/h;Lf/d/a/y/j/d;Z)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide/from16 v14, v39

    const/4 v4, 0x0

    goto/16 :goto_c

    :cond_2b
    move-wide/from16 v39, v14

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget v4, v7, Lf/d/a/d;->o:I

    add-int/2addr v4, v3

    iput v4, v7, Lf/d/a/d;->o:I

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->c()V

    goto/16 :goto_14

    :pswitch_d
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-static {}, Lf/d/a/y/l/e$b;->values()[Lf/d/a/y/l/e$b;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v4

    aget-object v30, v3, v4

    iget v3, v7, Lf/d/a/d;->o:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    iput v3, v7, Lf/d/a/d;->o:I

    goto/16 :goto_14

    :pswitch_e
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-static/range {p0 .. p1}, Lf/d/a/a0/c;->a(Lf/d/a/a0/h0/c;Lf/d/a/d;)Lf/d/a/y/j/l;

    move-result-object v20

    goto/16 :goto_15

    :pswitch_f
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_15

    :pswitch_10
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v3

    int-to-float v3, v3

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v4

    mul-float v4, v4, v3

    float-to-int v3, v4

    move/from16 v22, v3

    goto :goto_15

    :pswitch_11
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v3

    int-to-float v3, v3

    invoke-static {}, Lf/d/a/b0/g;->c()F

    move-result v4

    mul-float v4, v4, v3

    float-to-int v3, v4

    move/from16 v21, v3

    goto :goto_15

    :pswitch_12
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v3

    int-to-long v3, v3

    move-wide/from16 v17, v3

    goto :goto_15

    :pswitch_13
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v3

    sget-object v4, Lf/d/a/y/l/e$a;->j:Lf/d/a/y/l/e$a;

    const/4 v5, 0x6

    if-ge v3, v5, :cond_2c

    invoke-static {}, Lf/d/a/y/l/e$a;->values()[Lf/d/a/y/l/e$a;

    move-result-object v4

    aget-object v3, v4, v3

    move-object/from16 v16, v3

    goto :goto_14

    :cond_2c
    move-object/from16 v16, v4

    goto :goto_14

    :pswitch_14
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v19

    goto :goto_15

    :pswitch_15
    move-object/from16 v37, v6

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->i()I

    move-result v3

    int-to-long v3, v3

    move-wide v14, v3

    goto :goto_15

    :pswitch_16
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v13

    goto :goto_15

    :goto_14
    move-wide/from16 v14, v39

    :goto_15
    move-object/from16 v6, v37

    :goto_16
    const/4 v3, 0x0

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_2d
    move-object/from16 v37, v6

    move-wide/from16 v39, v14

    invoke-virtual/range {p0 .. p0}, Lf/d/a/a0/h0/c;->d()V

    div-float v11, v1, v24

    div-float v14, v2, v24

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    cmpl-float v1, v11, v0

    if-lez v1, :cond_2e

    new-instance v6, Lf/d/a/c0/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v34

    move-object v0, v6

    move-object/from16 v1, p1

    move-object v2, v12

    move-object v3, v12

    move-object/from16 v36, v8

    move-object/from16 v35, v10

    move-object/from16 v10, v37

    move-object v8, v6

    move-object/from16 v6, v34

    invoke-direct/range {v0 .. v6}, Lf/d/a/c0/a;-><init>(Lf/d/a/d;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/animation/Interpolator;FLjava/lang/Float;)V

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_17

    :cond_2e
    move-object/from16 v36, v8

    move-object/from16 v35, v10

    move-object/from16 v10, v37

    :goto_17
    const/4 v0, 0x0

    cmpl-float v0, v14, v0

    if-lez v0, :cond_2f

    goto :goto_18

    :cond_2f
    iget v0, v7, Lf/d/a/d;->l:F

    move v14, v0

    :goto_18
    new-instance v8, Lf/d/a/c0/a;

    const/16 v33, 0x0

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    const/4 v4, 0x0

    move-object v0, v8

    move-object/from16 v1, p1

    move-object v2, v9

    move-object v3, v9

    move v5, v11

    invoke-direct/range {v0 .. v6}, Lf/d/a/c0/a;-><init>(Lf/d/a/d;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/animation/Interpolator;FLjava/lang/Float;)V

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Lf/d/a/c0/a;

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    move-object v0, v8

    move-object v2, v12

    move-object v3, v12

    move-object/from16 v4, v33

    move v5, v14

    invoke-direct/range {v0 .. v6}, Lf/d/a/c0/a;-><init>(Lf/d/a/d;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/animation/Interpolator;FLjava/lang/Float;)V

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v0, ".ai"

    invoke-virtual {v13, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_30

    const-string v0, "ai"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    :cond_30
    const-string v0, "Convert your Illustrator layers to shape layers."

    invoke-virtual {v7, v0}, Lf/d/a/d;->a(Ljava/lang/String;)V

    :cond_31
    new-instance v33, Lf/d/a/y/l/e;

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move-object v3, v13

    move-wide/from16 v4, v39

    move-object/from16 v6, v16

    move-wide/from16 v7, v17

    move-object/from16 v9, v19

    move-object/from16 v10, v35

    move-object/from16 v11, v20

    move/from16 v12, v21

    move/from16 v13, v22

    move/from16 v14, v23

    move-object/from16 v21, v15

    move/from16 v15, v24

    move/from16 v16, v25

    move/from16 v17, v26

    move/from16 v18, v27

    move-object/from16 v19, v28

    move-object/from16 v20, v29

    move-object/from16 v22, v30

    move-object/from16 v23, v31

    move/from16 v24, v32

    invoke-direct/range {v0 .. v24}, Lf/d/a/y/l/e;-><init>(Ljava/util/List;Lf/d/a/d;Ljava/lang/String;JLf/d/a/y/l/e$a;JLjava/lang/String;Ljava/util/List;Lf/d/a/y/j/l;IIIFFIILf/d/a/y/j/j;Lf/d/a/y/j/k;Ljava/util/List;Lf/d/a/y/l/e$b;Lf/d/a/y/j/b;Z)V

    return-object v33

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
