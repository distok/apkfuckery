.class public Lf/d/a/a0/n;
.super Ljava/lang/Object;
.source "IntegerParser.java"

# interfaces
.implements Lf/d/a/a0/g0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/d/a/a0/g0<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/d/a/a0/n;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/d/a/a0/n;

    invoke-direct {v0}, Lf/d/a/a0/n;-><init>()V

    sput-object v0, Lf/d/a/a0/n;->a:Lf/d/a/a0/n;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/a0/h0/c;F)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lf/d/a/a0/o;->d(Lf/d/a/a0/h0/c;)F

    move-result p1

    mul-float p1, p1, p2

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
