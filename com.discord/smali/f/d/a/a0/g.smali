.class public Lf/d/a/a0/g;
.super Ljava/lang/Object;
.source "DocumentDataParser.java"

# interfaces
.implements Lf/d/a/a0/g0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/d/a/a0/g0<",
        "Lf/d/a/y/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/d/a/a0/g;

.field public static final b:Lf/d/a/a0/h0/c$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    new-instance v0, Lf/d/a/a0/g;

    invoke-direct {v0}, Lf/d/a/a0/g;-><init>()V

    sput-object v0, Lf/d/a/a0/g;->a:Lf/d/a/a0/g;

    const-string/jumbo v1, "t"

    const-string v2, "f"

    const-string v3, "s"

    const-string v4, "j"

    const-string/jumbo v5, "tr"

    const-string v6, "lh"

    const-string v7, "ls"

    const-string v8, "fc"

    const-string v9, "sc"

    const-string/jumbo v10, "sw"

    const-string v11, "of"

    filled-new-array/range {v1 .. v11}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/a0/h0/c$a;->a([Ljava/lang/String;)Lf/d/a/a0/h0/c$a;

    move-result-object v0

    sput-object v0, Lf/d/a/a0/g;->b:Lf/d/a/a0/h0/c$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/a0/h0/c;F)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/d/a/y/b$a;->f:Lf/d/a/y/b$a;

    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->b()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v9, v0

    move-object v6, v1

    move-object v7, v6

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lf/d/a/a0/g;->b:Lf/d/a/a0/h0/c$a;

    move-object/from16 v2, p1

    invoke-virtual {v2, v1}, Lf/d/a/a0/h0/c;->t(Lf/d/a/a0/h0/c$a;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->v()V

    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_0

    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->f()Z

    move-result v16

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v3

    double-to-float v15, v3

    goto :goto_0

    :pswitch_2
    invoke-static/range {p1 .. p1}, Lf/d/a/a0/o;->a(Lf/d/a/a0/h0/c;)I

    move-result v14

    goto :goto_0

    :pswitch_3
    invoke-static/range {p1 .. p1}, Lf/d/a/a0/o;->a(Lf/d/a/a0/h0/c;)I

    move-result v13

    goto :goto_0

    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v3

    double-to-float v12, v3

    goto :goto_0

    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v3

    double-to-float v11, v3

    goto :goto_0

    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->i()I

    move-result v10

    goto :goto_0

    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->i()I

    move-result v1

    const/4 v3, 0x2

    if-gt v1, v3, :cond_1

    if-gez v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {}, Lf/d/a/y/b$a;->values()[Lf/d/a/y/b$a;

    move-result-object v3

    aget-object v9, v3, v1

    goto :goto_0

    :cond_1
    :goto_1
    move-object v9, v0

    goto :goto_0

    :pswitch_8
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v3

    double-to-float v8, v3

    goto :goto_0

    :pswitch_9
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :pswitch_a
    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->n()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_2
    move-object/from16 v2, p1

    invoke-virtual/range {p1 .. p1}, Lf/d/a/a0/h0/c;->d()V

    new-instance v0, Lf/d/a/y/b;

    move-object v5, v0

    invoke-direct/range {v5 .. v16}, Lf/d/a/y/b;-><init>(Ljava/lang/String;Ljava/lang/String;FLf/d/a/y/b$a;IFFIIFZ)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
