.class public Lf/d/a/a0/v;
.super Ljava/lang/Object;
.source "PointFParser.java"

# interfaces
.implements Lf/d/a/a0/g0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/d/a/a0/g0<",
        "Landroid/graphics/PointF;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/d/a/a0/v;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/d/a/a0/v;

    invoke-direct {v0}, Lf/d/a/a0/v;-><init>()V

    sput-object v0, Lf/d/a/a0/v;->a:Lf/d/a/a0/v;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/d/a/a0/h0/c;F)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->o()Lf/d/a/a0/h0/c$b;

    move-result-object v0

    sget-object v1, Lf/d/a/a0/h0/c$b;->d:Lf/d/a/a0/h0/c$b;

    if-ne v0, v1, :cond_0

    invoke-static {p1, p2}, Lf/d/a/a0/o;->b(Lf/d/a/a0/h0/c;F)Landroid/graphics/PointF;

    move-result-object p1

    goto :goto_1

    :cond_0
    sget-object v1, Lf/d/a/a0/h0/c$b;->f:Lf/d/a/a0/h0/c$b;

    if-ne v0, v1, :cond_1

    invoke-static {p1, p2}, Lf/d/a/a0/o;->b(Lf/d/a/a0/h0/c;F)Landroid/graphics/PointF;

    move-result-object p1

    goto :goto_1

    :cond_1
    sget-object v1, Lf/d/a/a0/h0/c$b;->j:Lf/d/a/a0/h0/c$b;

    if-ne v0, v1, :cond_3

    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float v1, v1, p2

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->g()D

    move-result-wide v2

    double-to-float v2, v2

    mul-float v2, v2, p2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    :goto_0
    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->e()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lf/d/a/a0/h0/c;->w()V

    goto :goto_0

    :cond_2
    move-object p1, v0

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot convert json to point. Next token is "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
