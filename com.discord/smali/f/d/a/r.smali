.class public Lf/d/a/r;
.super Ljava/lang/Object;
.source "LottieTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/d/a/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static e:Ljava/util/concurrent/Executor;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/d/a/l<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/d/a/l<",
            "Ljava/lang/Throwable;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/os/Handler;

.field public volatile d:Lf/d/a/p;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/p<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lf/d/a/r;->e:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 2
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable<",
            "Lf/d/a/p<",
            "TT;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lf/d/a/r;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lf/d/a/r;->b:Ljava/util/Set;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lf/d/a/r;->c:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    sget-object v0, Lf/d/a/r;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lf/d/a/r$a;

    invoke-direct {v1, p0, p1}, Lf/d/a/r$a;-><init>(Lf/d/a/r;Ljava/util/concurrent/Callable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lf/d/a/l;)Lf/d/a/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/l<",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lf/d/a/r<",
            "TT;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    iget-object v0, v0, Lf/d/a/p;->b:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    iget-object v0, v0, Lf/d/a/p;->b:Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lf/d/a/l;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lf/d/a/r;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized b(Lf/d/a/l;)Lf/d/a/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/l<",
            "TT;>;)",
            "Lf/d/a/r<",
            "TT;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    iget-object v0, v0, Lf/d/a/p;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    iget-object v0, v0, Lf/d/a/p;->a:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lf/d/a/l;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lf/d/a/r;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final c(Lf/d/a/p;)V
    .locals 1
    .param p1    # Lf/d/a/p;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/p<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/r;->d:Lf/d/a/p;

    if-nez v0, :cond_0

    iput-object p1, p0, Lf/d/a/r;->d:Lf/d/a/p;

    iget-object p1, p0, Lf/d/a/r;->c:Landroid/os/Handler;

    new-instance v0, Lf/d/a/q;

    invoke-direct {v0, p0}, Lf/d/a/q;-><init>(Lf/d/a/r;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "A task may only be set once."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
