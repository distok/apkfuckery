.class public Lf/d/a/b0/c;
.super Ljava/lang/Object;
.source "Logger.java"


# static fields
.field public static a:Lf/d/a/m;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/d/a/b0/b;

    invoke-direct {v0}, Lf/d/a/b0/b;-><init>()V

    sput-object v0, Lf/d/a/b0/c;->a:Lf/d/a/m;

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    sget-object p0, Lf/d/a/b0/c;->a:Lf/d/a/m;

    check-cast p0, Lf/d/a/b0/b;

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lf/d/a/b0/c;->a:Lf/d/a/m;

    check-cast v0, Lf/d/a/b0/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/d/a/b0/b;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const-string v2, "LOTTIE"

    invoke-static {v2, p0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    sget-object v0, Lf/d/a/b0/c;->a:Lf/d/a/m;

    check-cast v0, Lf/d/a/b0/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/d/a/b0/b;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "LOTTIE"

    invoke-static {v1, p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method
