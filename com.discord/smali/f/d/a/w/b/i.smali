.class public Lf/d/a/w/b/i;
.super Lf/d/a/w/b/a;
.source "GradientStrokeContent.java"


# instance fields
.field public final o:Ljava/lang/String;

.field public final p:Z

.field public final q:Landroidx/collection/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LongSparseArray<",
            "Landroid/graphics/LinearGradient;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Landroidx/collection/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LongSparseArray<",
            "Landroid/graphics/RadialGradient;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Landroid/graphics/RectF;

.field public final t:Lf/d/a/y/k/f;

.field public final u:I

.field public final v:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Lf/d/a/y/k/c;",
            "Lf/d/a/y/k/c;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final x:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lf/d/a/w/c/p;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/e;)V
    .locals 11

    iget-object v0, p3, Lf/d/a/y/k/e;->h:Lf/d/a/y/k/p$a;

    invoke-virtual {v0}, Lf/d/a/y/k/p$a;->f()Landroid/graphics/Paint$Cap;

    move-result-object v4

    iget-object v0, p3, Lf/d/a/y/k/e;->i:Lf/d/a/y/k/p$b;

    invoke-virtual {v0}, Lf/d/a/y/k/p$b;->f()Landroid/graphics/Paint$Join;

    move-result-object v5

    iget v6, p3, Lf/d/a/y/k/e;->j:F

    iget-object v7, p3, Lf/d/a/y/k/e;->d:Lf/d/a/y/j/d;

    iget-object v8, p3, Lf/d/a/y/k/e;->g:Lf/d/a/y/j/b;

    iget-object v9, p3, Lf/d/a/y/k/e;->k:Ljava/util/List;

    iget-object v10, p3, Lf/d/a/y/k/e;->l:Lf/d/a/y/j/b;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v10}, Lf/d/a/w/b/a;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLf/d/a/y/j/d;Lf/d/a/y/j/b;Ljava/util/List;Lf/d/a/y/j/b;)V

    new-instance v0, Landroidx/collection/LongSparseArray;

    invoke-direct {v0}, Landroidx/collection/LongSparseArray;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/i;->q:Landroidx/collection/LongSparseArray;

    new-instance v0, Landroidx/collection/LongSparseArray;

    invoke-direct {v0}, Landroidx/collection/LongSparseArray;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/i;->r:Landroidx/collection/LongSparseArray;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/i;->s:Landroid/graphics/RectF;

    iget-object v0, p3, Lf/d/a/y/k/e;->a:Ljava/lang/String;

    iput-object v0, p0, Lf/d/a/w/b/i;->o:Ljava/lang/String;

    iget-object v0, p3, Lf/d/a/y/k/e;->b:Lf/d/a/y/k/f;

    iput-object v0, p0, Lf/d/a/w/b/i;->t:Lf/d/a/y/k/f;

    iget-boolean v0, p3, Lf/d/a/y/k/e;->m:Z

    iput-boolean v0, p0, Lf/d/a/w/b/i;->p:Z

    iget-object p1, p1, Lf/d/a/j;->e:Lf/d/a/d;

    invoke-virtual {p1}, Lf/d/a/d;->b()F

    move-result p1

    const/high16 v0, 0x42000000    # 32.0f

    div-float/2addr p1, v0

    float-to-int p1, p1

    iput p1, p0, Lf/d/a/w/b/i;->u:I

    iget-object p1, p3, Lf/d/a/y/k/e;->c:Lf/d/a/y/j/c;

    invoke-virtual {p1}, Lf/d/a/y/j/c;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/i;->v:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p3, Lf/d/a/y/k/e;->e:Lf/d/a/y/j/f;

    invoke-virtual {p1}, Lf/d/a/y/j/f;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/i;->w:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p3, Lf/d/a/y/k/e;->f:Lf/d/a/y/j/f;

    invoke-virtual {p1}, Lf/d/a/y/j/f;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/i;->x:Lf/d/a/w/c/a;

    iget-object p3, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    return-void
.end method


# virtual methods
.method public final e([I)[I
    .locals 4

    iget-object v0, p0, Lf/d/a/w/b/i;->y:Lf/d/a/w/c/p;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/d/a/w/c/p;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    array-length v1, p1

    array-length v2, v0

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    :goto_0
    array-length v1, p1

    if-ge v3, v1, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    array-length p1, v0

    new-array p1, p1, [I

    :goto_1
    array-length v1, v0

    if-ge v3, v1, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    return-object p1
.end method

.method public f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    iget-boolean v2, v0, Lf/d/a/w/b/i;->p:Z

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget-object v2, v0, Lf/d/a/w/b/i;->s:Landroid/graphics/RectF;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lf/d/a/w/b/a;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v2, v0, Lf/d/a/w/b/i;->t:Lf/d/a/y/k/f;

    sget-object v3, Lf/d/a/y/k/f;->d:Lf/d/a/y/k/f;

    if-ne v2, v3, :cond_2

    invoke-virtual/range {p0 .. p0}, Lf/d/a/w/b/i;->h()I

    move-result v2

    iget-object v3, v0, Lf/d/a/w/b/i;->q:Landroidx/collection/LongSparseArray;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/LinearGradient;

    if-eqz v2, :cond_1

    goto/16 :goto_0

    :cond_1
    iget-object v2, v0, Lf/d/a/w/b/i;->w:Lf/d/a/w/c/a;

    invoke-virtual {v2}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget-object v3, v0, Lf/d/a/w/b/i;->x:Lf/d/a/w/c/a;

    invoke-virtual {v3}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget-object v6, v0, Lf/d/a/w/b/i;->v:Lf/d/a/w/c/a;

    invoke-virtual {v6}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/d/a/y/k/c;

    iget-object v7, v6, Lf/d/a/y/k/c;->b:[I

    invoke-virtual {v0, v7}, Lf/d/a/w/b/i;->e([I)[I

    move-result-object v13

    iget-object v14, v6, Lf/d/a/y/k/c;->a:[F

    iget v9, v2, Landroid/graphics/PointF;->x:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    iget v11, v3, Landroid/graphics/PointF;->x:F

    iget v12, v3, Landroid/graphics/PointF;->y:F

    new-instance v2, Landroid/graphics/LinearGradient;

    sget-object v15, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v8, v2

    invoke-direct/range {v8 .. v15}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iget-object v3, v0, Lf/d/a/w/b/i;->q:Landroidx/collection/LongSparseArray;

    invoke-virtual {v3, v4, v5, v2}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lf/d/a/w/b/i;->h()I

    move-result v2

    iget-object v3, v0, Lf/d/a/w/b/i;->r:Landroidx/collection/LongSparseArray;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RadialGradient;

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    iget-object v2, v0, Lf/d/a/w/b/i;->w:Lf/d/a/w/c/a;

    invoke-virtual {v2}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    iget-object v3, v0, Lf/d/a/w/b/i;->x:Lf/d/a/w/c/a;

    invoke-virtual {v3}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget-object v6, v0, Lf/d/a/w/b/i;->v:Lf/d/a/w/c/a;

    invoke-virtual {v6}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/d/a/y/k/c;

    iget-object v7, v6, Lf/d/a/y/k/c;->b:[I

    invoke-virtual {v0, v7}, Lf/d/a/w/b/i;->e([I)[I

    move-result-object v12

    iget-object v13, v6, Lf/d/a/y/k/c;->a:[F

    iget v9, v2, Landroid/graphics/PointF;->x:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    iget v2, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v9

    float-to-double v6, v2

    sub-float/2addr v3, v10

    float-to-double v2, v3

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v11, v2

    new-instance v2, Landroid/graphics/RadialGradient;

    sget-object v14, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v8, v2

    invoke-direct/range {v8 .. v14}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    iget-object v3, v0, Lf/d/a/w/b/i;->r:Landroidx/collection/LongSparseArray;

    invoke-virtual {v3, v4, v5, v2}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    :goto_0
    invoke-virtual {v2, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    iget-object v3, v0, Lf/d/a/w/b/a;->i:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    invoke-super/range {p0 .. p3}, Lf/d/a/w/b/a;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lf/d/a/w/b/a;->g(Ljava/lang/Object;Lf/d/a/c0/c;)V

    sget-object v0, Lf/d/a/o;->D:[Ljava/lang/Integer;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lf/d/a/w/b/i;->y:Lf/d/a/w/c/p;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/d/a/w/b/a;->f:Lf/d/a/y/l/b;

    iget-object v0, v0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    const/4 p1, 0x0

    if-nez p2, :cond_1

    iput-object p1, p0, Lf/d/a/w/b/i;->y:Lf/d/a/w/c/p;

    goto :goto_0

    :cond_1
    new-instance v0, Lf/d/a/w/c/p;

    invoke-direct {v0, p2, p1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/d/a/w/b/i;->y:Lf/d/a/w/c/p;

    iget-object p1, v0, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/w/b/a;->f:Lf/d/a/y/l/b;

    iget-object p2, p0, Lf/d/a/w/b/i;->y:Lf/d/a/w/c/p;

    invoke-virtual {p1, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/i;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final h()I
    .locals 4

    iget-object v0, p0, Lf/d/a/w/b/i;->w:Lf/d/a/w/c/a;

    iget v0, v0, Lf/d/a/w/c/a;->d:F

    iget v1, p0, Lf/d/a/w/b/i;->u:I

    int-to-float v1, v1

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lf/d/a/w/b/i;->x:Lf/d/a/w/c/a;

    iget v1, v1, Lf/d/a/w/c/a;->d:F

    iget v2, p0, Lf/d/a/w/b/i;->u:I

    int-to-float v2, v2

    mul-float v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lf/d/a/w/b/i;->v:Lf/d/a/w/c/a;

    iget v2, v2, Lf/d/a/w/c/a;->d:F

    iget v3, p0, Lf/d/a/w/b/i;->u:I

    int-to-float v3, v3

    mul-float v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/16 v3, 0x11

    if-eqz v0, :cond_0

    const/16 v3, 0x20f

    mul-int v3, v3, v0

    :cond_0
    if-eqz v1, :cond_1

    mul-int/lit8 v3, v3, 0x1f

    mul-int v3, v3, v1

    :cond_1
    if-eqz v2, :cond_2

    mul-int/lit8 v3, v3, 0x1f

    mul-int v3, v3, v2

    :cond_2
    return v3
.end method
