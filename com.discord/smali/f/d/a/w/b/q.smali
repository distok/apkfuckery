.class public Lf/d/a/w/b/q;
.super Ljava/lang/Object;
.source "ShapeContent.java"

# interfaces
.implements Lf/d/a/w/b/m;
.implements Lf/d/a/w/c/a$b;


# instance fields
.field public final a:Landroid/graphics/Path;

.field public final b:Z

.field public final c:Lf/d/a/j;

.field public final d:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "*",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:Lf/d/a/w/b/b;


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    new-instance v0, Lf/d/a/w/b/b;

    invoke-direct {v0}, Lf/d/a/w/b/b;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/q;->f:Lf/d/a/w/b/b;

    iget-boolean v0, p3, Lf/d/a/y/k/o;->d:Z

    iput-boolean v0, p0, Lf/d/a/w/b/q;->b:Z

    iput-object p1, p0, Lf/d/a/w/b/q;->c:Lf/d/a/j;

    iget-object p1, p3, Lf/d/a/y/k/o;->c:Lf/d/a/y/j/h;

    invoke-virtual {p1}, Lf/d/a/y/j/h;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/q;->d:Lf/d/a/w/c/a;

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/d/a/w/b/q;->e:Z

    iget-object v0, p0, Lf/d/a/w/b/q;->c:Lf/d/a/j;

    invoke-virtual {v0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/b/c;

    instance-of v1, v0, Lf/d/a/w/b/s;

    if-eqz v1, :cond_0

    check-cast v0, Lf/d/a/w/b/s;

    iget-object v1, v0, Lf/d/a/w/b/s;->c:Lf/d/a/y/k/q$a;

    sget-object v2, Lf/d/a/y/k/q$a;->d:Lf/d/a/y/k/q$a;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/q;->f:Lf/d/a/w/b/b;

    iget-object v1, v1, Lf/d/a/w/b/b;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lf/d/a/w/b/s;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 3

    iget-boolean v0, p0, Lf/d/a/w/b/q;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    return-object v0

    :cond_0
    iget-object v0, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-boolean v0, p0, Lf/d/a/w/b/q;->b:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lf/d/a/w/b/q;->e:Z

    iget-object v0, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    return-object v0

    :cond_1
    iget-object v0, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    iget-object v2, p0, Lf/d/a/w/b/q;->d:Lf/d/a/w/c/a;

    invoke-virtual {v2}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Path;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    iget-object v0, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-object v0, p0, Lf/d/a/w/b/q;->f:Lf/d/a/w/b/b;

    iget-object v2, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    invoke-virtual {v0, v2}, Lf/d/a/w/b/b;->a(Landroid/graphics/Path;)V

    iput-boolean v1, p0, Lf/d/a/w/b/q;->e:Z

    iget-object v0, p0, Lf/d/a/w/b/q;->a:Landroid/graphics/Path;

    return-object v0
.end method
