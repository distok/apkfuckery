.class public Lf/d/a/w/b/g;
.super Ljava/lang/Object;
.source "FillContent.java"

# interfaces
.implements Lf/d/a/w/b/e;
.implements Lf/d/a/w/c/a$b;
.implements Lf/d/a/w/b/k;


# instance fields
.field public final a:Landroid/graphics/Path;

.field public final b:Landroid/graphics/Paint;

.field public final c:Lf/d/a/y/l/b;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/w/b/m;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/ColorFilter;",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lf/d/a/j;


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/m;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/g;->a:Landroid/graphics/Path;

    new-instance v1, Lf/d/a/w/a;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lf/d/a/w/a;-><init>(I)V

    iput-object v1, p0, Lf/d/a/w/b/g;->b:Landroid/graphics/Paint;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lf/d/a/w/b/g;->f:Ljava/util/List;

    iput-object p2, p0, Lf/d/a/w/b/g;->c:Lf/d/a/y/l/b;

    iget-object v1, p3, Lf/d/a/y/k/m;->c:Ljava/lang/String;

    iput-object v1, p0, Lf/d/a/w/b/g;->d:Ljava/lang/String;

    iget-boolean v1, p3, Lf/d/a/y/k/m;->f:Z

    iput-boolean v1, p0, Lf/d/a/w/b/g;->e:Z

    iput-object p1, p0, Lf/d/a/w/b/g;->j:Lf/d/a/j;

    iget-object p1, p3, Lf/d/a/y/k/m;->d:Lf/d/a/y/j/a;

    if-eqz p1, :cond_1

    iget-object p1, p3, Lf/d/a/y/k/m;->e:Lf/d/a/y/j/d;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p3, Lf/d/a/y/k/m;->b:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, p1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-object p1, p3, Lf/d/a/y/k/m;->d:Lf/d/a/y/j/a;

    invoke-virtual {p1}, Lf/d/a/y/j/a;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/g;->g:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p3, Lf/d/a/y/k/m;->e:Lf/d/a/y/j/d;

    invoke-virtual {p1}, Lf/d/a/y/j/d;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/g;->h:Lf/d/a/w/c/a;

    iget-object p3, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    return-void

    :cond_1
    :goto_0
    const/4 p1, 0x0

    iput-object p1, p0, Lf/d/a/w/b/g;->g:Lf/d/a/w/c/a;

    iput-object p1, p0, Lf/d/a/w/b/g;->h:Lf/d/a/w/c/a;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/g;->j:Lf/d/a/j;

    invoke-virtual {v0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/b/c;

    instance-of v1, v0, Lf/d/a/w/b/m;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/g;->f:Ljava/util/List;

    check-cast v0, Lf/d/a/w/b/m;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    invoke-static {p1, p2, p3, p4, p0}, Lf/d/a/b0/f;->f(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;Lf/d/a/w/b/k;)V

    return-void
.end method

.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    iget-object p3, p0, Lf/d/a/w/b/g;->a:Landroid/graphics/Path;

    invoke-virtual {p3}, Landroid/graphics/Path;->reset()V

    const/4 p3, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/d/a/w/b/g;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/g;->a:Landroid/graphics/Path;

    iget-object v2, p0, Lf/d/a/w/b/g;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/d/a/w/b/m;

    invoke-interface {v2}, Lf/d/a/w/b/m;->getPath()Landroid/graphics/Path;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lf/d/a/w/b/g;->a:Landroid/graphics/Path;

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    iget p2, p1, Landroid/graphics/RectF;->left:F

    const/high16 p3, 0x3f800000    # 1.0f

    sub-float/2addr p2, p3

    iget v0, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p3

    iget v1, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, p3

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 4

    iget-boolean v0, p0, Lf/d/a/w/b/g;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/w/b/g;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lf/d/a/w/b/g;->g:Lf/d/a/w/c/a;

    check-cast v1, Lf/d/a/w/c/b;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->a()Lf/d/a/c0/a;

    move-result-object v2

    invoke-virtual {v1}, Lf/d/a/w/c/a;->c()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lf/d/a/w/c/b;->j(Lf/d/a/c0/a;F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float p3, p3

    const/high16 v0, 0x437f0000    # 255.0f

    div-float/2addr p3, v0

    iget-object v1, p0, Lf/d/a/w/b/g;->h:Lf/d/a/w/c/a;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    mul-float p3, p3, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr p3, v1

    mul-float p3, p3, v0

    float-to-int p3, p3

    iget-object v0, p0, Lf/d/a/w/b/g;->b:Landroid/graphics/Paint;

    const/16 v1, 0xff

    const/4 v2, 0x0

    invoke-static {p3, v2, v1}, Lf/d/a/b0/f;->c(III)I

    move-result p3

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object p3, p0, Lf/d/a/w/b/g;->i:Lf/d/a/w/c/a;

    if-eqz p3, :cond_1

    iget-object v0, p0, Lf/d/a/w/b/g;->b:Landroid/graphics/Paint;

    invoke-virtual {p3}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/graphics/ColorFilter;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :cond_1
    iget-object p3, p0, Lf/d/a/w/b/g;->a:Landroid/graphics/Path;

    invoke-virtual {p3}, Landroid/graphics/Path;->reset()V

    :goto_0
    iget-object p3, p0, Lf/d/a/w/b/g;->f:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    if-ge v2, p3, :cond_2

    iget-object p3, p0, Lf/d/a/w/b/g;->a:Landroid/graphics/Path;

    iget-object v0, p0, Lf/d/a/w/b/g;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/b/m;

    invoke-interface {v0}, Lf/d/a/w/b/m;->getPath()Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {p3, v0, p2}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object p2, p0, Lf/d/a/w/b/g;->a:Landroid/graphics/Path;

    iget-object p3, p0, Lf/d/a/w/b/g;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    const-string p1, "FillContent#draw"

    invoke-static {p1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    sget-object v0, Lf/d/a/o;->a:Ljava/lang/Integer;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lf/d/a/w/b/g;->g:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    goto :goto_0

    :cond_0
    sget-object v0, Lf/d/a/o;->d:Ljava/lang/Integer;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lf/d/a/w/b/g;->h:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    goto :goto_0

    :cond_1
    sget-object v0, Lf/d/a/o;->C:Landroid/graphics/ColorFilter;

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lf/d/a/w/b/g;->i:Lf/d/a/w/c/a;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lf/d/a/w/b/g;->c:Lf/d/a/y/l/b;

    iget-object v0, v0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_2
    const/4 p1, 0x0

    if-nez p2, :cond_3

    iput-object p1, p0, Lf/d/a/w/b/g;->i:Lf/d/a/w/c/a;

    goto :goto_0

    :cond_3
    new-instance v0, Lf/d/a/w/c/p;

    invoke-direct {v0, p2, p1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/d/a/w/b/g;->i:Lf/d/a/w/c/a;

    iget-object p1, v0, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/w/b/g;->c:Lf/d/a/y/l/b;

    iget-object p2, p0, Lf/d/a/w/b/g;->i:Lf/d/a/w/c/a;

    invoke-virtual {p1, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/g;->d:Ljava/lang/String;

    return-object v0
.end method
