.class public Lf/d/a/w/b/o;
.super Ljava/lang/Object;
.source "RectangleContent.java"

# interfaces
.implements Lf/d/a/w/c/a$b;
.implements Lf/d/a/w/b/k;
.implements Lf/d/a/w/b/m;


# instance fields
.field public final a:Landroid/graphics/Path;

.field public final b:Landroid/graphics/RectF;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Lf/d/a/j;

.field public final f:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "*",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "*",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lf/d/a/w/b/b;

.field public j:Z


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    new-instance v0, Lf/d/a/w/b/b;

    invoke-direct {v0}, Lf/d/a/w/b/b;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/o;->i:Lf/d/a/w/b/b;

    iget-object v0, p3, Lf/d/a/y/k/j;->a:Ljava/lang/String;

    iput-object v0, p0, Lf/d/a/w/b/o;->c:Ljava/lang/String;

    iget-boolean v0, p3, Lf/d/a/y/k/j;->e:Z

    iput-boolean v0, p0, Lf/d/a/w/b/o;->d:Z

    iput-object p1, p0, Lf/d/a/w/b/o;->e:Lf/d/a/j;

    iget-object p1, p3, Lf/d/a/y/k/j;->b:Lf/d/a/y/j/m;

    invoke-interface {p1}, Lf/d/a/y/j/m;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/o;->f:Lf/d/a/w/c/a;

    iget-object v0, p3, Lf/d/a/y/k/j;->c:Lf/d/a/y/j/f;

    invoke-virtual {v0}, Lf/d/a/y/j/f;->a()Lf/d/a/w/c/a;

    move-result-object v0

    iput-object v0, p0, Lf/d/a/w/b/o;->g:Lf/d/a/w/c/a;

    iget-object p3, p3, Lf/d/a/y/k/j;->d:Lf/d/a/y/j/b;

    invoke-virtual {p3}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object p3

    iput-object p3, p0, Lf/d/a/w/b/o;->h:Lf/d/a/w/c/a;

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    invoke-virtual {p2, v0}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    invoke-virtual {p2, p3}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, v0, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p3, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/d/a/w/b/o;->j:Z

    iget-object v0, p0, Lf/d/a/w/b/o;->e:Lf/d/a/j;

    invoke-virtual {v0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/b/c;

    instance-of v1, v0, Lf/d/a/w/b/s;

    if-eqz v1, :cond_0

    check-cast v0, Lf/d/a/w/b/s;

    iget-object v1, v0, Lf/d/a/w/b/s;->c:Lf/d/a/y/k/q$a;

    sget-object v2, Lf/d/a/y/k/q$a;->d:Lf/d/a/y/k/q$a;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/o;->i:Lf/d/a/w/b/b;

    iget-object v1, v1, Lf/d/a/w/b/b;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lf/d/a/w/b/s;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    invoke-static {p1, p2, p3, p4, p0}, Lf/d/a/b0/f;->f(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;Lf/d/a/w/b/k;)V

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    sget-object v0, Lf/d/a/o;->h:Landroid/graphics/PointF;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lf/d/a/w/b/o;->g:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    goto :goto_0

    :cond_0
    sget-object v0, Lf/d/a/o;->j:Landroid/graphics/PointF;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lf/d/a/w/b/o;->f:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    goto :goto_0

    :cond_1
    sget-object v0, Lf/d/a/o;->i:Ljava/lang/Float;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lf/d/a/w/b/o;->h:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    :cond_2
    :goto_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/o;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 17

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lf/d/a/w/b/o;->j:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    return-object v1

    :cond_0
    iget-object v1, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    iget-boolean v1, v0, Lf/d/a/w/b/o;->d:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    iput-boolean v2, v0, Lf/d/a/w/b/o;->j:Z

    iget-object v1, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    return-object v1

    :cond_1
    iget-object v1, v0, Lf/d/a/w/b/o;->g:Lf/d/a/w/c/a;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget v1, v1, Landroid/graphics/PointF;->y:F

    div-float/2addr v1, v4

    iget-object v5, v0, Lf/d/a/w/b/o;->h:Lf/d/a/w/c/a;

    const/4 v6, 0x0

    if-nez v5, :cond_2

    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    check-cast v5, Lf/d/a/w/c/c;

    invoke-virtual {v5}, Lf/d/a/w/c/c;->j()F

    move-result v5

    :goto_0
    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v7

    cmpl-float v8, v5, v7

    if-lez v8, :cond_3

    move v5, v7

    :cond_3
    iget-object v7, v0, Lf/d/a/w/b/o;->f:Lf/d/a/w/c/a;

    invoke-virtual {v7}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    iget-object v8, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget v9, v7, Landroid/graphics/PointF;->x:F

    add-float/2addr v9, v3

    iget v10, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v1

    add-float/2addr v10, v5

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v8, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget v9, v7, Landroid/graphics/PointF;->x:F

    add-float/2addr v9, v3

    iget v10, v7, Landroid/graphics/PointF;->y:F

    add-float/2addr v10, v1

    sub-float/2addr v10, v5

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    const/4 v8, 0x0

    const/high16 v9, 0x42b40000    # 90.0f

    cmpl-float v10, v5, v6

    if-lez v10, :cond_4

    iget-object v11, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    iget v12, v7, Landroid/graphics/PointF;->x:F

    add-float v13, v12, v3

    mul-float v14, v5, v4

    sub-float/2addr v13, v14

    iget v15, v7, Landroid/graphics/PointF;->y:F

    add-float v16, v15, v1

    sub-float v14, v16, v14

    add-float/2addr v12, v3

    add-float/2addr v15, v1

    invoke-virtual {v11, v13, v14, v12, v15}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v11, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget-object v12, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    invoke-virtual {v11, v12, v6, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    :cond_4
    iget-object v6, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget v11, v7, Landroid/graphics/PointF;->x:F

    sub-float/2addr v11, v3

    add-float/2addr v11, v5

    iget v12, v7, Landroid/graphics/PointF;->y:F

    add-float/2addr v12, v1

    invoke-virtual {v6, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    if-lez v10, :cond_5

    iget-object v6, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    iget v11, v7, Landroid/graphics/PointF;->x:F

    sub-float v12, v11, v3

    iget v13, v7, Landroid/graphics/PointF;->y:F

    add-float v14, v13, v1

    mul-float v15, v5, v4

    sub-float/2addr v14, v15

    sub-float/2addr v11, v3

    add-float/2addr v11, v15

    add-float/2addr v13, v1

    invoke-virtual {v6, v12, v14, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v6, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget-object v11, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    invoke-virtual {v6, v11, v9, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    :cond_5
    iget-object v6, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget v11, v7, Landroid/graphics/PointF;->x:F

    sub-float/2addr v11, v3

    iget v12, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v12, v1

    add-float/2addr v12, v5

    invoke-virtual {v6, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    if-lez v10, :cond_6

    iget-object v6, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    iget v11, v7, Landroid/graphics/PointF;->x:F

    sub-float v12, v11, v3

    iget v13, v7, Landroid/graphics/PointF;->y:F

    sub-float v14, v13, v1

    sub-float/2addr v11, v3

    mul-float v15, v5, v4

    add-float/2addr v11, v15

    sub-float/2addr v13, v1

    add-float/2addr v13, v15

    invoke-virtual {v6, v12, v14, v11, v13}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v6, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget-object v11, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    const/high16 v12, 0x43340000    # 180.0f

    invoke-virtual {v6, v11, v12, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    :cond_6
    iget-object v6, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget v11, v7, Landroid/graphics/PointF;->x:F

    add-float/2addr v11, v3

    sub-float/2addr v11, v5

    iget v12, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v12, v1

    invoke-virtual {v6, v11, v12}, Landroid/graphics/Path;->lineTo(FF)V

    if-lez v10, :cond_7

    iget-object v6, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    iget v10, v7, Landroid/graphics/PointF;->x:F

    add-float v11, v10, v3

    mul-float v5, v5, v4

    sub-float/2addr v11, v5

    iget v4, v7, Landroid/graphics/PointF;->y:F

    sub-float v7, v4, v1

    add-float/2addr v10, v3

    sub-float/2addr v4, v1

    add-float/2addr v4, v5

    invoke-virtual {v6, v11, v7, v10, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    iget-object v3, v0, Lf/d/a/w/b/o;->b:Landroid/graphics/RectF;

    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {v1, v3, v4, v9, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    :cond_7
    iget-object v1, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    iget-object v1, v0, Lf/d/a/w/b/o;->i:Lf/d/a/w/b/b;

    iget-object v3, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    invoke-virtual {v1, v3}, Lf/d/a/w/b/b;->a(Landroid/graphics/Path;)V

    iput-boolean v2, v0, Lf/d/a/w/b/o;->j:Z

    iget-object v1, v0, Lf/d/a/w/b/o;->a:Landroid/graphics/Path;

    return-object v1
.end method
