.class public Lf/d/a/w/b/b;
.super Ljava/lang/Object;
.source "CompoundTrimPathContent.java"


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/w/b/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/b;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Path;)V
    .locals 5

    iget-object v0, p0, Lf/d/a/w/b/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    iget-object v1, p0, Lf/d/a/w/b/b;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/w/b/s;

    sget-object v2, Lf/d/a/b0/g;->a:Landroid/graphics/PathMeasure;

    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lf/d/a/w/b/s;->a:Z

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v2, v1, Lf/d/a/w/b/s;->d:Lf/d/a/w/c/a;

    check-cast v2, Lf/d/a/w/c/c;

    invoke-virtual {v2}, Lf/d/a/w/c/c;->j()F

    move-result v2

    iget-object v3, v1, Lf/d/a/w/b/s;->e:Lf/d/a/w/c/a;

    check-cast v3, Lf/d/a/w/c/c;

    invoke-virtual {v3}, Lf/d/a/w/c/c;->j()F

    move-result v3

    iget-object v1, v1, Lf/d/a/w/b/s;->f:Lf/d/a/w/c/a;

    check-cast v1, Lf/d/a/w/c/c;

    invoke-virtual {v1}, Lf/d/a/w/c/c;->j()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v2, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x43b40000    # 360.0f

    div-float/2addr v1, v4

    invoke-static {p1, v2, v3, v1}, Lf/d/a/b0/g;->a(Landroid/graphics/Path;FFF)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method
