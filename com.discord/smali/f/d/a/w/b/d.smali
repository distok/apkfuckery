.class public Lf/d/a/w/b/d;
.super Ljava/lang/Object;
.source "ContentGroup.java"

# interfaces
.implements Lf/d/a/w/b/e;
.implements Lf/d/a/w/b/m;
.implements Lf/d/a/w/c/a$b;
.implements Lf/d/a/y/f;


# instance fields
.field public a:Landroid/graphics/Paint;

.field public b:Landroid/graphics/RectF;

.field public final c:Landroid/graphics/Matrix;

.field public final d:Landroid/graphics/Path;

.field public final e:Landroid/graphics/RectF;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lf/d/a/j;

.field public j:Ljava/util/List;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/w/b/m;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lf/d/a/w/c/o;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/n;)V
    .locals 7

    iget-object v3, p3, Lf/d/a/y/k/n;->a:Ljava/lang/String;

    iget-boolean v4, p3, Lf/d/a/y/k/n;->c:Z

    iget-object v0, p3, Lf/d/a/y/k/n;->b:Ljava/util/List;

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/d/a/y/k/b;

    invoke-interface {v6, p1, p2}, Lf/d/a/y/k/b;->a(Lf/d/a/j;Lf/d/a/y/l/b;)Lf/d/a/w/b/c;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object p3, p3, Lf/d/a/y/k/n;->b:Ljava/util/List;

    :goto_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/y/k/b;

    instance-of v2, v0, Lf/d/a/y/j/l;

    if-eqz v2, :cond_2

    check-cast v0, Lf/d/a/y/j/l;

    move-object v6, v0

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/4 p3, 0x0

    move-object v6, p3

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lf/d/a/w/b/d;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Ljava/lang/String;ZLjava/util/List;Lf/d/a/y/j/l;)V

    return-void
.end method

.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Ljava/lang/String;ZLjava/util/List;Lf/d/a/y/j/l;)V
    .locals 1
    .param p6    # Lf/d/a/y/j/l;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/j;",
            "Lf/d/a/y/l/b;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Lf/d/a/y/j/l;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/d/a/w/a;

    invoke-direct {v0}, Lf/d/a/w/a;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/d;->a:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/d;->b:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/d;->d:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/d;->e:Landroid/graphics/RectF;

    iput-object p3, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    iput-object p1, p0, Lf/d/a/w/b/d;->i:Lf/d/a/j;

    iput-boolean p4, p0, Lf/d/a/w/b/d;->g:Z

    iput-object p5, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    if-eqz p6, :cond_0

    new-instance p1, Lf/d/a/w/c/o;

    invoke-direct {p1, p6}, Lf/d/a/w/c/o;-><init>(Lf/d/a/y/j/l;)V

    iput-object p1, p0, Lf/d/a/w/b/d;->k:Lf/d/a/w/c/o;

    invoke-virtual {p1, p2}, Lf/d/a/w/c/o;->a(Lf/d/a/y/l/b;)V

    iget-object p1, p0, Lf/d/a/w/b/d;->k:Lf/d/a/w/c/o;

    invoke-virtual {p1, p0}, Lf/d/a/w/c/o;->b(Lf/d/a/w/c/a$b;)V

    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p2

    :cond_1
    :goto_0
    add-int/lit8 p2, p2, -0x1

    if-ltz p2, :cond_2

    invoke-interface {p5, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/d/a/w/b/c;

    instance-of p4, p3, Lf/d/a/w/b/j;

    if-eqz p4, :cond_1

    check-cast p3, Lf/d/a/w/b/j;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p2

    :goto_1
    add-int/lit8 p2, p2, -0x1

    if-ltz p2, :cond_3

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/d/a/w/b/j;

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p4

    invoke-interface {p5, p4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object p4

    invoke-interface {p3, p4}, Lf/d/a/w/b/j;->e(Ljava/util/ListIterator;)V

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/d;->i:Lf/d/a/j;

    invoke-virtual {v0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v0

    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :goto_0
    if-ltz p1, :cond_0

    iget-object v0, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/b/c;

    iget-object v1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Lf/d/a/w/b/c;->b(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->e(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    const-string v1, "__container"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    invoke-virtual {p4, v0}, Lf/d/a/y/e;->a(Ljava/lang/String;)Lf/d/a/y/e;

    move-result-object p4

    iget-object v0, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->c(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4, p0}, Lf/d/a/y/e;->g(Lf/d/a/y/f;)Lf/d/a/y/e;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->f(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/d/a/y/e;->d(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr v0, p2

    const/4 p2, 0x0

    :goto_0
    iget-object v1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_3

    iget-object v1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/w/b/c;

    instance-of v2, v1, Lf/d/a/y/f;

    if-eqz v2, :cond_2

    check-cast v1, Lf/d/a/y/f;

    invoke-interface {v1, p1, v0, p3, p4}, Lf/d/a/y/f;->c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    iget-object v0, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object p2, p0, Lf/d/a/w/b/d;->k:Lf/d/a/w/c/o;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    :cond_0
    iget-object p2, p0, Lf/d/a/w/b/d;->e:Landroid/graphics/RectF;

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object p2, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-ltz p2, :cond_2

    iget-object v0, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/b/c;

    instance-of v1, v0, Lf/d/a/w/b/e;

    if-eqz v1, :cond_1

    check-cast v0, Lf/d/a/w/b/e;

    iget-object v1, p0, Lf/d/a/w/b/d;->e:Landroid/graphics/RectF;

    iget-object v2, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-interface {v0, v1, v2, p3}, Lf/d/a/w/b/e;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v0, p0, Lf/d/a/w/b/d;->e:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    :cond_1
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public e()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/d/a/w/b/m;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/w/b/d;->j:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/d;->j:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/w/b/c;

    instance-of v2, v1, Lf/d/a/w/b/m;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lf/d/a/w/b/d;->j:Ljava/util/List;

    check-cast v1, Lf/d/a/w/b/m;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/d/a/w/b/d;->j:Ljava/util/List;

    return-object v0
.end method

.method public f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 5

    iget-boolean v0, p0, Lf/d/a/w/b/d;->g:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object p2, p0, Lf/d/a/w/b/d;->k:Lf/d/a/w/c/o;

    if-eqz p2, :cond_2

    iget-object v0, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    iget-object p2, p0, Lf/d/a/w/b/d;->k:Lf/d/a/w/c/o;

    iget-object p2, p2, Lf/d/a/w/c/o;->j:Lf/d/a/w/c/a;

    if-nez p2, :cond_1

    const/16 p2, 0x64

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :goto_0
    int-to-float p2, p2

    const/high16 v0, 0x42c80000    # 100.0f

    div-float/2addr p2, v0

    int-to-float p3, p3

    mul-float p2, p2, p3

    const/high16 p3, 0x437f0000    # 255.0f

    div-float/2addr p2, p3

    mul-float p2, p2, p3

    float-to-int p3, p2

    :cond_2
    iget-object p2, p0, Lf/d/a/w/b/d;->i:Lf/d/a/j;

    iget-boolean p2, p2, Lf/d/a/j;->u:Z

    const/16 v0, 0xff

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_5

    const/4 p2, 0x0

    const/4 v3, 0x0

    :goto_1
    iget-object v4, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge p2, v4, :cond_4

    iget-object v4, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lf/d/a/w/b/e;

    if-eqz v4, :cond_3

    add-int/lit8 v3, v3, 0x1

    const/4 v4, 0x2

    if-lt v3, v4, :cond_3

    const/4 p2, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_4
    const/4 p2, 0x0

    :goto_2
    if-eqz p2, :cond_5

    if-eq p3, v0, :cond_5

    const/4 v1, 0x1

    :cond_5
    if-eqz v1, :cond_6

    iget-object p2, p0, Lf/d/a/w/b/d;->b:Landroid/graphics/RectF;

    const/4 v3, 0x0

    invoke-virtual {p2, v3, v3, v3, v3}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object p2, p0, Lf/d/a/w/b/d;->b:Landroid/graphics/RectF;

    iget-object v3, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {p0, p2, v3, v2}, Lf/d/a/w/b/d;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Lf/d/a/w/b/d;->a:Landroid/graphics/Paint;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object p2, p0, Lf/d/a/w/b/d;->b:Landroid/graphics/RectF;

    iget-object v3, p0, Lf/d/a/w/b/d;->a:Landroid/graphics/Paint;

    const/16 v4, 0x1f

    invoke-static {p1, p2, v3, v4}, Lf/d/a/b0/g;->f(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;I)V

    :cond_6
    if-eqz v1, :cond_7

    const/16 p3, 0xff

    :cond_7
    iget-object p2, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v2

    :goto_3
    if-ltz p2, :cond_9

    iget-object v0, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lf/d/a/w/b/e;

    if-eqz v2, :cond_8

    check-cast v0, Lf/d/a/w/b/e;

    iget-object v2, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-interface {v0, p1, v2, p3}, Lf/d/a/w/b/e;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    :cond_8
    add-int/lit8 p2, p2, -0x1

    goto :goto_3

    :cond_9
    if-eqz v1, :cond_a

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_a
    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/w/b/d;->k:Lf/d/a/w/c/o;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lf/d/a/w/c/o;->c(Ljava/lang/Object;Lf/d/a/c0/c;)Z

    :cond_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 4

    iget-object v0, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lf/d/a/w/b/d;->k:Lf/d/a/w/c/o;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Lf/d/a/w/c/o;->e()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    :cond_0
    iget-object v0, p0, Lf/d/a/w/b/d;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-boolean v0, p0, Lf/d/a/w/b/d;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/d/a/w/b/d;->d:Landroid/graphics/Path;

    return-object v0

    :cond_1
    iget-object v0, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    iget-object v1, p0, Lf/d/a/w/b/d;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/w/b/c;

    instance-of v2, v1, Lf/d/a/w/b/m;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf/d/a/w/b/d;->d:Landroid/graphics/Path;

    check-cast v1, Lf/d/a/w/b/m;

    invoke-interface {v1}, Lf/d/a/w/b/m;->getPath()Landroid/graphics/Path;

    move-result-object v1

    iget-object v3, p0, Lf/d/a/w/b/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lf/d/a/w/b/d;->d:Landroid/graphics/Path;

    return-object v0
.end method
