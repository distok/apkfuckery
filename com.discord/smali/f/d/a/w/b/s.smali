.class public Lf/d/a/w/b/s;
.super Ljava/lang/Object;
.source "TrimPathContent.java"

# interfaces
.implements Lf/d/a/w/b/c;
.implements Lf/d/a/w/c/a$b;


# instance fields
.field public final a:Z

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/w/c/a$b;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/d/a/y/k/q$a;

.field public final d:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/d/a/y/l/b;Lf/d/a/y/k/q;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/s;->b:Ljava/util/List;

    iget-boolean v0, p2, Lf/d/a/y/k/q;->f:Z

    iput-boolean v0, p0, Lf/d/a/w/b/s;->a:Z

    iget-object v0, p2, Lf/d/a/y/k/q;->b:Lf/d/a/y/k/q$a;

    iput-object v0, p0, Lf/d/a/w/b/s;->c:Lf/d/a/y/k/q$a;

    iget-object v0, p2, Lf/d/a/y/k/q;->c:Lf/d/a/y/j/b;

    invoke-virtual {v0}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object v0

    iput-object v0, p0, Lf/d/a/w/b/s;->d:Lf/d/a/w/c/a;

    iget-object v1, p2, Lf/d/a/y/k/q;->d:Lf/d/a/y/j/b;

    invoke-virtual {v1}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object v1

    iput-object v1, p0, Lf/d/a/w/b/s;->e:Lf/d/a/w/c/a;

    iget-object p2, p2, Lf/d/a/y/k/q;->e:Lf/d/a/y/j/b;

    invoke-virtual {p2}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object p2

    iput-object p2, p0, Lf/d/a/w/b/s;->f:Lf/d/a/w/c/a;

    invoke-virtual {p1, v0}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    invoke-virtual {p1, v1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    invoke-virtual {p1, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, v0, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, v1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p2, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/d/a/w/b/s;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/s;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/d/a/w/c/a$b;

    invoke-interface {v1}, Lf/d/a/w/c/a$b;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
