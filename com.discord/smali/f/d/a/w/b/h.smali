.class public Lf/d/a/w/b/h;
.super Ljava/lang/Object;
.source "GradientFillContent.java"

# interfaces
.implements Lf/d/a/w/b/e;
.implements Lf/d/a/w/c/a$b;
.implements Lf/d/a/w/b/k;


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final b:Z

.field public final c:Lf/d/a/y/l/b;

.field public final d:Landroidx/collection/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LongSparseArray<",
            "Landroid/graphics/LinearGradient;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroidx/collection/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LongSparseArray<",
            "Landroid/graphics/RadialGradient;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/graphics/Path;

.field public final g:Landroid/graphics/Paint;

.field public final h:Landroid/graphics/RectF;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/d/a/w/b/m;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lf/d/a/y/k/f;

.field public final k:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Lf/d/a/y/k/c;",
            "Lf/d/a/y/k/c;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/ColorFilter;",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lf/d/a/w/c/p;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final q:Lf/d/a/j;

.field public final r:I


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/d;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroidx/collection/LongSparseArray;

    invoke-direct {v0}, Landroidx/collection/LongSparseArray;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/h;->d:Landroidx/collection/LongSparseArray;

    new-instance v0, Landroidx/collection/LongSparseArray;

    invoke-direct {v0}, Landroidx/collection/LongSparseArray;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/h;->e:Landroidx/collection/LongSparseArray;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    new-instance v1, Lf/d/a/w/a;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lf/d/a/w/a;-><init>(I)V

    iput-object v1, p0, Lf/d/a/w/b/h;->g:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lf/d/a/w/b/h;->h:Landroid/graphics/RectF;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lf/d/a/w/b/h;->i:Ljava/util/List;

    iput-object p2, p0, Lf/d/a/w/b/h;->c:Lf/d/a/y/l/b;

    iget-object v1, p3, Lf/d/a/y/k/d;->g:Ljava/lang/String;

    iput-object v1, p0, Lf/d/a/w/b/h;->a:Ljava/lang/String;

    iget-boolean v1, p3, Lf/d/a/y/k/d;->h:Z

    iput-boolean v1, p0, Lf/d/a/w/b/h;->b:Z

    iput-object p1, p0, Lf/d/a/w/b/h;->q:Lf/d/a/j;

    iget-object v1, p3, Lf/d/a/y/k/d;->a:Lf/d/a/y/k/f;

    iput-object v1, p0, Lf/d/a/w/b/h;->j:Lf/d/a/y/k/f;

    iget-object v1, p3, Lf/d/a/y/k/d;->b:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-object p1, p1, Lf/d/a/j;->e:Lf/d/a/d;

    invoke-virtual {p1}, Lf/d/a/d;->b()F

    move-result p1

    const/high16 v0, 0x42000000    # 32.0f

    div-float/2addr p1, v0

    float-to-int p1, p1

    iput p1, p0, Lf/d/a/w/b/h;->r:I

    iget-object p1, p3, Lf/d/a/y/k/d;->c:Lf/d/a/y/j/c;

    invoke-virtual {p1}, Lf/d/a/y/j/c;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/h;->k:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p3, Lf/d/a/y/k/d;->d:Lf/d/a/y/j/d;

    invoke-virtual {p1}, Lf/d/a/y/j/d;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/h;->l:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p3, Lf/d/a/y/k/d;->e:Lf/d/a/y/j/f;

    invoke-virtual {p1}, Lf/d/a/y/j/f;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/h;->m:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p3, Lf/d/a/y/k/d;->f:Lf/d/a/y/j/f;

    invoke-virtual {p1}, Lf/d/a/y/j/f;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/h;->n:Lf/d/a/w/c/a;

    iget-object p3, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/h;->q:Lf/d/a/j;

    invoke-virtual {v0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/d/a/w/b/c;

    instance-of v1, v0, Lf/d/a/w/b/m;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/h;->i:Ljava/util/List;

    check-cast v0, Lf/d/a/w/b/m;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    invoke-static {p1, p2, p3, p4, p0}, Lf/d/a/b0/f;->f(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;Lf/d/a/w/b/k;)V

    return-void
.end method

.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    iget-object p3, p0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    invoke-virtual {p3}, Landroid/graphics/Path;->reset()V

    const/4 p3, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/d/a/w/b/h;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    iget-object v2, p0, Lf/d/a/w/b/h;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/d/a/w/b/m;

    invoke-interface {v2}, Lf/d/a/w/b/m;->getPath()Landroid/graphics/Path;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    iget p2, p1, Landroid/graphics/RectF;->left:F

    const/high16 p3, 0x3f800000    # 1.0f

    sub-float/2addr p2, p3

    iget v0, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p3

    iget v1, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, p3

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public final e([I)[I
    .locals 4

    iget-object v0, p0, Lf/d/a/w/b/h;->p:Lf/d/a/w/c/p;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/d/a/w/c/p;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    array-length v1, p1

    array-length v2, v0

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    :goto_0
    array-length v1, p1

    if-ge v3, v1, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    array-length p1, v0

    new-array p1, p1, [I

    :goto_1
    array-length v1, v0

    if-ge v3, v1, :cond_1

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    return-object p1
.end method

.method public f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    iget-boolean v2, v0, Lf/d/a/w/b/h;->b:Z

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget-object v2, v0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    iget-object v4, v0, Lf/d/a/w/b/h;->i:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    iget-object v4, v0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    iget-object v5, v0, Lf/d/a/w/b/h;->i:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/d/a/w/b/m;

    invoke-interface {v5}, Lf/d/a/w/b/m;->getPath()Landroid/graphics/Path;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    iget-object v4, v0, Lf/d/a/w/b/h;->h:Landroid/graphics/RectF;

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    iget-object v3, v0, Lf/d/a/w/b/h;->j:Lf/d/a/y/k/f;

    sget-object v4, Lf/d/a/y/k/f;->d:Lf/d/a/y/k/f;

    if-ne v3, v4, :cond_3

    invoke-virtual/range {p0 .. p0}, Lf/d/a/w/b/h;->h()I

    move-result v3

    iget-object v4, v0, Lf/d/a/w/b/h;->d:Landroidx/collection/LongSparseArray;

    int-to-long v5, v3

    invoke-virtual {v4, v5, v6}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/LinearGradient;

    if-eqz v3, :cond_2

    goto/16 :goto_2

    :cond_2
    iget-object v3, v0, Lf/d/a/w/b/h;->m:Lf/d/a/w/c/a;

    invoke-virtual {v3}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget-object v4, v0, Lf/d/a/w/b/h;->n:Lf/d/a/w/c/a;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget-object v7, v0, Lf/d/a/w/b/h;->k:Lf/d/a/w/c/a;

    invoke-virtual {v7}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/d/a/y/k/c;

    iget-object v8, v7, Lf/d/a/y/k/c;->b:[I

    invoke-virtual {v0, v8}, Lf/d/a/w/b/h;->e([I)[I

    move-result-object v14

    iget-object v15, v7, Lf/d/a/y/k/c;->a:[F

    new-instance v7, Landroid/graphics/LinearGradient;

    iget v10, v3, Landroid/graphics/PointF;->x:F

    iget v11, v3, Landroid/graphics/PointF;->y:F

    iget v12, v4, Landroid/graphics/PointF;->x:F

    iget v13, v4, Landroid/graphics/PointF;->y:F

    sget-object v16, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v9, v7

    invoke-direct/range {v9 .. v16}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iget-object v3, v0, Lf/d/a/w/b/h;->d:Landroidx/collection/LongSparseArray;

    invoke-virtual {v3, v5, v6, v7}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    move-object v3, v7

    goto :goto_2

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lf/d/a/w/b/h;->h()I

    move-result v3

    iget-object v4, v0, Lf/d/a/w/b/h;->e:Landroidx/collection/LongSparseArray;

    int-to-long v5, v3

    invoke-virtual {v4, v5, v6}, Landroidx/collection/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RadialGradient;

    if-eqz v3, :cond_4

    goto :goto_2

    :cond_4
    iget-object v3, v0, Lf/d/a/w/b/h;->m:Lf/d/a/w/c/a;

    invoke-virtual {v3}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    iget-object v4, v0, Lf/d/a/w/b/h;->n:Lf/d/a/w/c/a;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    iget-object v7, v0, Lf/d/a/w/b/h;->k:Lf/d/a/w/c/a;

    invoke-virtual {v7}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/d/a/y/k/c;

    iget-object v8, v7, Lf/d/a/y/k/c;->b:[I

    invoke-virtual {v0, v8}, Lf/d/a/w/b/h;->e([I)[I

    move-result-object v13

    iget-object v14, v7, Lf/d/a/y/k/c;->a:[F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    iget v11, v3, Landroid/graphics/PointF;->y:F

    iget v3, v4, Landroid/graphics/PointF;->x:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v10

    float-to-double v7, v3

    sub-float/2addr v4, v11

    float-to-double v3, v4

    invoke-static {v7, v8, v3, v4}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v3

    double-to-float v3, v3

    const/4 v4, 0x0

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_5

    const v3, 0x3a83126f    # 0.001f

    const v12, 0x3a83126f    # 0.001f

    goto :goto_1

    :cond_5
    move v12, v3

    :goto_1
    new-instance v3, Landroid/graphics/RadialGradient;

    sget-object v15, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v9, v3

    invoke-direct/range {v9 .. v15}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    iget-object v4, v0, Lf/d/a/w/b/h;->e:Landroidx/collection/LongSparseArray;

    invoke-virtual {v4, v5, v6, v3}, Landroidx/collection/LongSparseArray;->put(JLjava/lang/Object;)V

    :goto_2
    invoke-virtual {v3, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    iget-object v1, v0, Lf/d/a/w/b/h;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, v0, Lf/d/a/w/b/h;->o:Lf/d/a/w/c/a;

    if-eqz v1, :cond_6

    iget-object v3, v0, Lf/d/a/w/b/h;->g:Landroid/graphics/Paint;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/ColorFilter;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :cond_6
    move/from16 v1, p3

    int-to-float v1, v1

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v1, v3

    iget-object v4, v0, Lf/d/a/w/b/h;->l:Lf/d/a/w/c/a;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    mul-float v1, v1, v4

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v1, v4

    mul-float v1, v1, v3

    float-to-int v1, v1

    iget-object v3, v0, Lf/d/a/w/b/h;->g:Landroid/graphics/Paint;

    const/16 v4, 0xff

    invoke-static {v1, v2, v4}, Lf/d/a/b0/f;->c(III)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v1, v0, Lf/d/a/w/b/h;->f:Landroid/graphics/Path;

    iget-object v2, v0, Lf/d/a/w/b/h;->g:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    const-string v1, "GradientFillContent#draw"

    invoke-static {v1}, Lf/d/a/c;->a(Ljava/lang/String;)F

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 2
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    sget-object v0, Lf/d/a/o;->d:Ljava/lang/Integer;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lf/d/a/w/b/h;->l:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    goto :goto_0

    :cond_0
    sget-object v0, Lf/d/a/o;->C:Landroid/graphics/ColorFilter;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lf/d/a/w/b/h;->o:Lf/d/a/w/c/a;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lf/d/a/w/b/h;->c:Lf/d/a/y/l/b;

    iget-object v0, v0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    if-nez p2, :cond_2

    iput-object v1, p0, Lf/d/a/w/b/h;->o:Lf/d/a/w/c/a;

    goto :goto_0

    :cond_2
    new-instance p1, Lf/d/a/w/c/p;

    invoke-direct {p1, p2, v1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/d/a/w/b/h;->o:Lf/d/a/w/c/a;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/w/b/h;->c:Lf/d/a/y/l/b;

    iget-object p2, p0, Lf/d/a/w/b/h;->o:Lf/d/a/w/c/a;

    invoke-virtual {p1, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lf/d/a/o;->D:[Ljava/lang/Integer;

    if-ne p1, v0, :cond_6

    iget-object p1, p0, Lf/d/a/w/b/h;->p:Lf/d/a/w/c/p;

    if-eqz p1, :cond_4

    iget-object v0, p0, Lf/d/a/w/b/h;->c:Lf/d/a/y/l/b;

    iget-object v0, v0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_4
    if-nez p2, :cond_5

    iput-object v1, p0, Lf/d/a/w/b/h;->p:Lf/d/a/w/c/p;

    goto :goto_0

    :cond_5
    new-instance p1, Lf/d/a/w/c/p;

    invoke-direct {p1, p2, v1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/d/a/w/b/h;->p:Lf/d/a/w/c/p;

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/w/b/h;->c:Lf/d/a/y/l/b;

    iget-object p2, p0, Lf/d/a/w/b/h;->p:Lf/d/a/w/c/p;

    invoke-virtual {p1, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_6
    :goto_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final h()I
    .locals 4

    iget-object v0, p0, Lf/d/a/w/b/h;->m:Lf/d/a/w/c/a;

    iget v0, v0, Lf/d/a/w/c/a;->d:F

    iget v1, p0, Lf/d/a/w/b/h;->r:I

    int-to-float v1, v1

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lf/d/a/w/b/h;->n:Lf/d/a/w/c/a;

    iget v1, v1, Lf/d/a/w/c/a;->d:F

    iget v2, p0, Lf/d/a/w/b/h;->r:I

    int-to-float v2, v2

    mul-float v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lf/d/a/w/b/h;->k:Lf/d/a/w/c/a;

    iget v2, v2, Lf/d/a/w/c/a;->d:F

    iget v3, p0, Lf/d/a/w/b/h;->r:I

    int-to-float v3, v3

    mul-float v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/16 v3, 0x11

    if-eqz v0, :cond_0

    const/16 v3, 0x20f

    mul-int v3, v3, v0

    :cond_0
    if-eqz v1, :cond_1

    mul-int/lit8 v3, v3, 0x1f

    mul-int v3, v3, v1

    :cond_1
    if-eqz v2, :cond_2

    mul-int/lit8 v3, v3, 0x1f

    mul-int v3, v3, v2

    :cond_2
    return v3
.end method
