.class public Lf/d/a/w/b/p;
.super Ljava/lang/Object;
.source "RepeaterContent.java"

# interfaces
.implements Lf/d/a/w/b/e;
.implements Lf/d/a/w/b/m;
.implements Lf/d/a/w/b/j;
.implements Lf/d/a/w/c/a$b;
.implements Lf/d/a/w/b/k;


# instance fields
.field public final a:Landroid/graphics/Matrix;

.field public final b:Landroid/graphics/Path;

.field public final c:Lf/d/a/j;

.field public final d:Lf/d/a/y/l/b;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lf/d/a/w/c/o;

.field public j:Lf/d/a/w/b/d;


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/k;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/p;->a:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lf/d/a/w/b/p;->b:Landroid/graphics/Path;

    iput-object p1, p0, Lf/d/a/w/b/p;->c:Lf/d/a/j;

    iput-object p2, p0, Lf/d/a/w/b/p;->d:Lf/d/a/y/l/b;

    iget-object p1, p3, Lf/d/a/y/k/k;->a:Ljava/lang/String;

    iput-object p1, p0, Lf/d/a/w/b/p;->e:Ljava/lang/String;

    iget-boolean p1, p3, Lf/d/a/y/k/k;->e:Z

    iput-boolean p1, p0, Lf/d/a/w/b/p;->f:Z

    iget-object p1, p3, Lf/d/a/y/k/k;->b:Lf/d/a/y/j/b;

    invoke-virtual {p1}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/p;->g:Lf/d/a/w/c/a;

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p3, Lf/d/a/y/k/k;->c:Lf/d/a/y/j/b;

    invoke-virtual {p1}, Lf/d/a/y/j/b;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/p;->h:Lf/d/a/w/c/a;

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    iget-object p1, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p3, Lf/d/a/y/k/k;->d:Lf/d/a/y/j/l;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p3, Lf/d/a/w/c/o;

    invoke-direct {p3, p1}, Lf/d/a/w/c/o;-><init>(Lf/d/a/y/j/l;)V

    iput-object p3, p0, Lf/d/a/w/b/p;->i:Lf/d/a/w/c/o;

    invoke-virtual {p3, p2}, Lf/d/a/w/c/o;->a(Lf/d/a/y/l/b;)V

    invoke-virtual {p3, p0}, Lf/d/a/w/c/o;->b(Lf/d/a/w/c/a$b;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/p;->c:Lf/d/a/j;

    invoke-virtual {v0}, Lf/d/a/j;->invalidateSelf()V

    return-void
.end method

.method public b(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;",
            "Ljava/util/List<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/w/b/p;->j:Lf/d/a/w/b/d;

    invoke-virtual {v0, p1, p2}, Lf/d/a/w/b/d;->b(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public c(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/y/e;",
            "I",
            "Ljava/util/List<",
            "Lf/d/a/y/e;",
            ">;",
            "Lf/d/a/y/e;",
            ")V"
        }
    .end annotation

    invoke-static {p1, p2, p3, p4, p0}, Lf/d/a/b0/f;->f(Lf/d/a/y/e;ILjava/util/List;Lf/d/a/y/e;Lf/d/a/w/b/k;)V

    return-void
.end method

.method public d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/p;->j:Lf/d/a/w/b/d;

    invoke-virtual {v0, p1, p2, p3}, Lf/d/a/w/b/d;->d(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    return-void
.end method

.method public e(Ljava/util/ListIterator;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ListIterator<",
            "Lf/d/a/w/b/c;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/w/b/p;->j:Lf/d/a/w/b/d;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_1

    goto :goto_0

    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    invoke-interface {p1}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Ljava/util/ListIterator;->remove()V

    goto :goto_1

    :cond_2
    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    new-instance p1, Lf/d/a/w/b/d;

    iget-object v2, p0, Lf/d/a/w/b/p;->c:Lf/d/a/j;

    iget-object v3, p0, Lf/d/a/w/b/p;->d:Lf/d/a/y/l/b;

    iget-boolean v5, p0, Lf/d/a/w/b/p;->f:Z

    const/4 v7, 0x0

    const-string v4, "Repeater"

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lf/d/a/w/b/d;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Ljava/lang/String;ZLjava/util/List;Lf/d/a/y/j/l;)V

    iput-object p1, p0, Lf/d/a/w/b/p;->j:Lf/d/a/w/b/d;

    return-void
.end method

.method public f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 9

    iget-object v0, p0, Lf/d/a/w/b/p;->g:Lf/d/a/w/c/a;

    invoke-virtual {v0}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lf/d/a/w/b/p;->h:Lf/d/a/w/c/a;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lf/d/a/w/b/p;->i:Lf/d/a/w/c/o;

    iget-object v2, v2, Lf/d/a/w/c/o;->m:Lf/d/a/w/c/a;

    invoke-virtual {v2}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    iget-object v4, p0, Lf/d/a/w/b/p;->i:Lf/d/a/w/c/o;

    iget-object v4, v4, Lf/d/a/w/c/o;->n:Lf/d/a/w/c/a;

    invoke-virtual {v4}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    div-float/2addr v4, v3

    float-to-int v3, v0

    add-int/lit8 v3, v3, -0x1

    :goto_0
    if-ltz v3, :cond_0

    iget-object v5, p0, Lf/d/a/w/b/p;->a:Landroid/graphics/Matrix;

    invoke-virtual {v5, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v5, p0, Lf/d/a/w/b/p;->a:Landroid/graphics/Matrix;

    iget-object v6, p0, Lf/d/a/w/b/p;->i:Lf/d/a/w/c/o;

    int-to-float v7, v3

    add-float v8, v7, v1

    invoke-virtual {v6, v8}, Lf/d/a/w/c/o;->f(F)Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    int-to-float v5, p3

    div-float/2addr v7, v0

    invoke-static {v2, v4, v7}, Lf/d/a/b0/f;->e(FFF)F

    move-result v6

    mul-float v6, v6, v5

    iget-object v5, p0, Lf/d/a/w/b/p;->j:Lf/d/a/w/b/d;

    iget-object v7, p0, Lf/d/a/w/b/p;->a:Landroid/graphics/Matrix;

    float-to-int v6, v6

    invoke-virtual {v5, p1, v7, v6}, Lf/d/a/w/b/d;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/w/b/p;->i:Lf/d/a/w/c/o;

    invoke-virtual {v0, p1, p2}, Lf/d/a/w/c/o;->c(Ljava/lang/Object;Lf/d/a/c0/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lf/d/a/o;->q:Ljava/lang/Float;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lf/d/a/w/b/p;->g:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    goto :goto_0

    :cond_1
    sget-object v0, Lf/d/a/o;->r:Ljava/lang/Float;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lf/d/a/w/b/p;->h:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    :cond_2
    :goto_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/p;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 6

    iget-object v0, p0, Lf/d/a/w/b/p;->j:Lf/d/a/w/b/d;

    invoke-virtual {v0}, Lf/d/a/w/b/d;->getPath()Landroid/graphics/Path;

    move-result-object v0

    iget-object v1, p0, Lf/d/a/w/b/p;->b:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    iget-object v1, p0, Lf/d/a/w/b/p;->g:Lf/d/a/w/c/a;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lf/d/a/w/b/p;->h:Lf/d/a/w/c/a;

    invoke-virtual {v2}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    iget-object v3, p0, Lf/d/a/w/b/p;->a:Landroid/graphics/Matrix;

    iget-object v4, p0, Lf/d/a/w/b/p;->i:Lf/d/a/w/c/o;

    int-to-float v5, v1

    add-float/2addr v5, v2

    invoke-virtual {v4, v5}, Lf/d/a/w/c/o;->f(F)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lf/d/a/w/b/p;->b:Landroid/graphics/Path;

    iget-object v4, p0, Lf/d/a/w/b/p;->a:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/d/a/w/b/p;->b:Landroid/graphics/Path;

    return-object v0
.end method
