.class public Lf/d/a/w/b/r;
.super Lf/d/a/w/b/a;
.source "StrokeContent.java"


# instance fields
.field public final o:Lf/d/a/y/l/b;

.field public final p:Ljava/lang/String;

.field public final q:Z

.field public final r:Lf/d/a/w/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lf/d/a/w/c/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/d/a/w/c/a<",
            "Landroid/graphics/ColorFilter;",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/d/a/j;Lf/d/a/y/l/b;Lf/d/a/y/k/p;)V
    .locals 11

    iget-object v0, p3, Lf/d/a/y/k/p;->g:Lf/d/a/y/k/p$a;

    invoke-virtual {v0}, Lf/d/a/y/k/p$a;->f()Landroid/graphics/Paint$Cap;

    move-result-object v4

    iget-object v0, p3, Lf/d/a/y/k/p;->h:Lf/d/a/y/k/p$b;

    invoke-virtual {v0}, Lf/d/a/y/k/p$b;->f()Landroid/graphics/Paint$Join;

    move-result-object v5

    iget v6, p3, Lf/d/a/y/k/p;->i:F

    iget-object v7, p3, Lf/d/a/y/k/p;->e:Lf/d/a/y/j/d;

    iget-object v8, p3, Lf/d/a/y/k/p;->f:Lf/d/a/y/j/b;

    iget-object v9, p3, Lf/d/a/y/k/p;->c:Ljava/util/List;

    iget-object v10, p3, Lf/d/a/y/k/p;->b:Lf/d/a/y/j/b;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v10}, Lf/d/a/w/b/a;-><init>(Lf/d/a/j;Lf/d/a/y/l/b;Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLf/d/a/y/j/d;Lf/d/a/y/j/b;Ljava/util/List;Lf/d/a/y/j/b;)V

    iput-object p2, p0, Lf/d/a/w/b/r;->o:Lf/d/a/y/l/b;

    iget-object p1, p3, Lf/d/a/y/k/p;->a:Ljava/lang/String;

    iput-object p1, p0, Lf/d/a/w/b/r;->p:Ljava/lang/String;

    iget-boolean p1, p3, Lf/d/a/y/k/p;->j:Z

    iput-boolean p1, p0, Lf/d/a/w/b/r;->q:Z

    iget-object p1, p3, Lf/d/a/y/k/p;->d:Lf/d/a/y/j/a;

    invoke-virtual {p1}, Lf/d/a/y/j/a;->a()Lf/d/a/w/c/a;

    move-result-object p1

    iput-object p1, p0, Lf/d/a/w/b/r;->r:Lf/d/a/w/c/a;

    iget-object p3, p1, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p1}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    return-void
.end method


# virtual methods
.method public f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 4

    iget-boolean v0, p0, Lf/d/a/w/b/r;->q:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/d/a/w/b/a;->i:Landroid/graphics/Paint;

    iget-object v1, p0, Lf/d/a/w/b/r;->r:Lf/d/a/w/c/a;

    check-cast v1, Lf/d/a/w/c/b;

    invoke-virtual {v1}, Lf/d/a/w/c/a;->a()Lf/d/a/c0/a;

    move-result-object v2

    invoke-virtual {v1}, Lf/d/a/w/c/a;->c()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lf/d/a/w/c/b;->j(Lf/d/a/c0/a;F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lf/d/a/w/b/r;->s:Lf/d/a/w/c/a;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/d/a/w/b/a;->i:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lf/d/a/w/c/a;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lf/d/a/w/b/a;->f(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/d/a/c0/c;)V
    .locals 1
    .param p2    # Lf/d/a/c0/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lf/d/a/c0/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lf/d/a/w/b/a;->g(Ljava/lang/Object;Lf/d/a/c0/c;)V

    sget-object v0, Lf/d/a/o;->b:Ljava/lang/Integer;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lf/d/a/w/b/r;->r:Lf/d/a/w/c/a;

    iget-object v0, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    iput-object p2, p1, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    goto :goto_0

    :cond_0
    sget-object v0, Lf/d/a/o;->C:Landroid/graphics/ColorFilter;

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lf/d/a/w/b/r;->s:Lf/d/a/w/c/a;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lf/d/a/w/b/r;->o:Lf/d/a/y/l/b;

    iget-object v0, v0, Lf/d/a/y/l/b;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    const/4 p1, 0x0

    if-nez p2, :cond_2

    iput-object p1, p0, Lf/d/a/w/b/r;->s:Lf/d/a/w/c/a;

    goto :goto_0

    :cond_2
    new-instance v0, Lf/d/a/w/c/p;

    invoke-direct {v0, p2, p1}, Lf/d/a/w/c/p;-><init>(Lf/d/a/c0/c;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/d/a/w/b/r;->s:Lf/d/a/w/c/a;

    iget-object p1, v0, Lf/d/a/w/c/a;->a:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/d/a/w/b/r;->o:Lf/d/a/y/l/b;

    iget-object p2, p0, Lf/d/a/w/b/r;->r:Lf/d/a/w/c/a;

    invoke-virtual {p1, p2}, Lf/d/a/y/l/b;->e(Lf/d/a/w/c/a;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/d/a/w/b/r;->p:Ljava/lang/String;

    return-object v0
.end method
