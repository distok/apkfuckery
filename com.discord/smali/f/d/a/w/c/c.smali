.class public Lf/d/a/w/c/c;
.super Lf/d/a/w/c/f;
.source "FloatKeyframeAnimation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/d/a/w/c/f<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/c0/a<",
            "Ljava/lang/Float;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/d/a/w/c/f;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public f(Lf/d/a/c0/a;F)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lf/d/a/w/c/c;->k(Lf/d/a/c0/a;F)F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    return-object p1
.end method

.method public j()F
    .locals 2

    invoke-virtual {p0}, Lf/d/a/w/c/a;->a()Lf/d/a/c0/a;

    move-result-object v0

    invoke-virtual {p0}, Lf/d/a/w/c/a;->c()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lf/d/a/w/c/c;->k(Lf/d/a/c0/a;F)F

    move-result v0

    return v0
.end method

.method public k(Lf/d/a/c0/a;F)F
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/d/a/c0/a<",
            "Ljava/lang/Float;",
            ">;F)F"
        }
    .end annotation

    iget-object v0, p1, Lf/d/a/c0/a;->b:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lf/d/a/c0/a;->c:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    if-eqz v1, :cond_0

    iget v2, p1, Lf/d/a/c0/a;->e:F

    iget-object v0, p1, Lf/d/a/c0/a;->f:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v4, p1, Lf/d/a/c0/a;->b:Ljava/lang/Object;

    iget-object v5, p1, Lf/d/a/c0/a;->c:Ljava/lang/Object;

    invoke-virtual {p0}, Lf/d/a/w/c/a;->d()F

    move-result v7

    iget v8, p0, Lf/d/a/w/c/a;->d:F

    move v6, p2

    invoke-virtual/range {v1 .. v8}, Lf/d/a/c0/c;->a(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p1

    return p1

    :cond_0
    iget v0, p1, Lf/d/a/c0/a;->g:F

    const v1, -0x358c9d09

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p1, Lf/d/a/c0/a;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p1, Lf/d/a/c0/a;->g:F

    :cond_1
    iget v0, p1, Lf/d/a/c0/a;->g:F

    iget v2, p1, Lf/d/a/c0/a;->h:F

    cmpl-float v1, v2, v1

    if-nez v1, :cond_2

    iget-object v1, p1, Lf/d/a/c0/a;->c:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p1, Lf/d/a/c0/a;->h:F

    :cond_2
    iget p1, p1, Lf/d/a/c0/a;->h:F

    invoke-static {v0, p1, p2}, Lf/d/a/b0/f;->e(FFF)F

    move-result p1

    return p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Missing values for keyframe."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
