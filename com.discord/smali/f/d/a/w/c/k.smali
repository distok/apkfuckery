.class public Lf/d/a/w/c/k;
.super Lf/d/a/w/c/f;
.source "ScaleKeyframeAnimation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/d/a/w/c/f<",
        "Lf/d/a/c0/d;",
        ">;"
    }
.end annotation


# instance fields
.field public final i:Lf/d/a/c0/d;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/d/a/c0/a<",
            "Lf/d/a/c0/d;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/d/a/w/c/f;-><init>(Ljava/util/List;)V

    new-instance p1, Lf/d/a/c0/d;

    invoke-direct {p1}, Lf/d/a/c0/d;-><init>()V

    iput-object p1, p0, Lf/d/a/w/c/k;->i:Lf/d/a/c0/d;

    return-void
.end method


# virtual methods
.method public f(Lf/d/a/c0/a;F)Ljava/lang/Object;
    .locals 10

    iget-object v0, p1, Lf/d/a/c0/a;->b:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v1, p1, Lf/d/a/c0/a;->c:Ljava/lang/Object;

    if-eqz v1, :cond_1

    check-cast v0, Lf/d/a/c0/d;

    check-cast v1, Lf/d/a/c0/d;

    iget-object v2, p0, Lf/d/a/w/c/a;->e:Lf/d/a/c0/c;

    if-eqz v2, :cond_0

    iget v3, p1, Lf/d/a/c0/a;->e:F

    iget-object p1, p1, Lf/d/a/c0/a;->f:Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {p0}, Lf/d/a/w/c/a;->d()F

    move-result v8

    iget v9, p0, Lf/d/a/w/c/a;->d:F

    move-object v5, v0

    move-object v6, v1

    move v7, p2

    invoke-virtual/range {v2 .. v9}, Lf/d/a/c0/c;->a(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/d/a/c0/d;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/d/a/w/c/k;->i:Lf/d/a/c0/d;

    iget v2, v0, Lf/d/a/c0/d;->a:F

    iget v3, v1, Lf/d/a/c0/d;->a:F

    invoke-static {v2, v3, p2}, Lf/d/a/b0/f;->e(FFF)F

    move-result v2

    iget v0, v0, Lf/d/a/c0/d;->b:F

    iget v1, v1, Lf/d/a/c0/d;->b:F

    invoke-static {v0, v1, p2}, Lf/d/a/b0/f;->e(FFF)F

    move-result p2

    iput v2, p1, Lf/d/a/c0/d;->a:F

    iput p2, p1, Lf/d/a/c0/d;->b:F

    iget-object p1, p0, Lf/d/a/w/c/k;->i:Lf/d/a/c0/d;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Missing values for keyframe."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
