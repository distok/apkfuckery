.class public Lf/d/a/f;
.super Ljava/lang/Object;
.source "LottieCompositionFactory.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lf/d/a/p<",
        "Lf/d/a/d;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/d/a/f;->d:Landroid/content/Context;

    iput-object p2, p0, Lf/d/a/f;->e:Ljava/lang/String;

    iput-object p3, p0, Lf/d/a/f;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/d/a/f;->d:Landroid/content/Context;

    iget-object v1, p0, Lf/d/a/f;->e:Ljava/lang/String;

    iget-object v2, p0, Lf/d/a/f;->f:Ljava/lang/String;

    new-instance v3, Lf/d/a/z/c;

    invoke-direct {v3, v0, v1, v2}, Lf/d/a/z/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lf/d/a/z/a;->e:Lf/d/a/z/a;

    iget-object v1, v3, Lf/d/a/z/c;->c:Lf/d/a/z/b;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    goto/16 :goto_4

    :cond_0
    iget-object v4, v3, Lf/d/a/z/c;->b:Ljava/lang/String;

    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-virtual {v1}, Lf/d/a/z/b;->b()Ljava/io/File;

    move-result-object v6

    sget-object v7, Lf/d/a/z/a;->d:Lf/d/a/z/a;

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lf/d/a/z/b;->a(Ljava/lang/String;Lf/d/a/z/a;Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v6, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-virtual {v1}, Lf/d/a/z/b;->b()Ljava/io/File;

    move-result-object v1

    invoke-static {v4, v0, v8}, Lf/d/a/z/b;->a(Ljava/lang/String;Lf/d/a/z/a;Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    move-object v5, v2

    :goto_0
    if-nez v5, :cond_3

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-string v8, ".zip"

    invoke-virtual {v6, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v7, v0

    :cond_4
    const-string v6, "Cache hit for "

    const-string v8, " at "

    invoke-static {v6, v4, v8}, Lf/e/c/a/a;->M(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lf/d/a/b0/c;->a(Ljava/lang/String;)V

    new-instance v4, Landroidx/core/util/Pair;

    invoke-direct {v4, v7, v1}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :catch_0
    :goto_1
    move-object v4, v2

    :goto_2
    if-nez v4, :cond_5

    goto :goto_4

    :cond_5
    iget-object v1, v4, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lf/d/a/z/a;

    iget-object v4, v4, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/io/InputStream;

    if-ne v1, v0, :cond_6

    new-instance v0, Ljava/util/zip/ZipInputStream;

    invoke-direct {v0, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    iget-object v1, v3, Lf/d/a/z/c;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lf/d/a/e;->d(Ljava/util/zip/ZipInputStream;Ljava/lang/String;)Lf/d/a/p;

    move-result-object v0

    goto :goto_3

    :cond_6
    iget-object v0, v3, Lf/d/a/z/c;->b:Ljava/lang/String;

    invoke-static {v4, v0}, Lf/d/a/e;->b(Ljava/io/InputStream;Ljava/lang/String;)Lf/d/a/p;

    move-result-object v0

    :goto_3
    iget-object v0, v0, Lf/d/a/p;->a:Ljava/lang/Object;

    if-eqz v0, :cond_7

    move-object v2, v0

    check-cast v2, Lf/d/a/d;

    :cond_7
    :goto_4
    if-eqz v2, :cond_8

    new-instance v0, Lf/d/a/p;

    invoke-direct {v0, v2}, Lf/d/a/p;-><init>(Ljava/lang/Object;)V

    goto :goto_5

    :cond_8
    const-string v0, "Animation for "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v3, Lf/d/a/z/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not found in cache. Fetching from network."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/d/a/b0/c;->a(Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {v3}, Lf/d/a/z/c;->a()Lf/d/a/p;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    new-instance v1, Lf/d/a/p;

    invoke-direct {v1, v0}, Lf/d/a/p;-><init>(Ljava/lang/Throwable;)V

    move-object v0, v1

    :goto_5
    return-object v0
.end method
