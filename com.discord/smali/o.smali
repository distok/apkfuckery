.class public final Lo;
.super Ljava/lang/Object;
.source "WidgetRemoteAuthViewModel.kt"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:LWidgetRemoteAuthViewModel$a;

.field public final synthetic e:Lcom/discord/models/domain/ModelRemoteAuthHandshake;


# direct methods
.method public constructor <init>(LWidgetRemoteAuthViewModel$a;Lcom/discord/models/domain/ModelRemoteAuthHandshake;)V
    .locals 0

    iput-object p1, p0, Lo;->d:LWidgetRemoteAuthViewModel$a;

    iput-object p2, p0, Lo;->e:Lcom/discord/models/domain/ModelRemoteAuthHandshake;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lo;->d:LWidgetRemoteAuthViewModel$a;

    iget-object v0, v0, LWidgetRemoteAuthViewModel$a;->this$0:LWidgetRemoteAuthViewModel;

    new-instance v1, LWidgetRemoteAuthViewModel$ViewState$b;

    iget-object v2, p0, Lo;->e:Lcom/discord/models/domain/ModelRemoteAuthHandshake;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelRemoteAuthHandshake;->getHandshakeToken()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lo;->d:LWidgetRemoteAuthViewModel$a;

    iget-object v3, v3, LWidgetRemoteAuthViewModel$a;->this$0:LWidgetRemoteAuthViewModel;

    iget-object v3, v3, LWidgetRemoteAuthViewModel;->d:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v4, "temporaryBehaviorSubject"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lrx/subjects/BehaviorSubject;->i0()Ljava/lang/Object;

    move-result-object v3

    const-string/jumbo v4, "temporaryBehaviorSubject.value"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, LWidgetRemoteAuthViewModel$ViewState$b;-><init>(Ljava/lang/String;ZZ)V

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method
