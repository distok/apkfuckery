.class public final synthetic Lm;
.super Lx/m/c/i;
.source "WidgetFriendsAddUserRequestsModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "LWidgetFriendsAddUserRequestsModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lm;

    invoke-direct {v0}, Lm;-><init>()V

    sput-object v0, Lm;->d:Lm;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, LWidgetFriendsAddUserRequestsModel;

    const/4 v1, 0x2

    const-string v3, "<init>"

    const-string v4, "<init>(Ljava/util/Set;Ljava/util/Set;)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/util/Set;

    check-cast p2, Ljava/util/Set;

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, LWidgetFriendsAddUserRequestsModel;

    invoke-direct {v0, p1, p2}, LWidgetFriendsAddUserRequestsModel;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    return-object v0
.end method
