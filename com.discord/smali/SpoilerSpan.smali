.class public final LSpoilerSpan;
.super Landroid/text/style/BackgroundColorSpan;
.source "SpoilerSpan.kt"


# instance fields
.field public d:I

.field public e:I

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    iput v0, p0, LSpoilerSpan;->d:I

    iput v0, p0, LSpoilerSpan;->e:I

    iput-boolean v0, p0, LSpoilerSpan;->f:Z

    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    iput p1, p0, LSpoilerSpan;->d:I

    iput p2, p0, LSpoilerSpan;->e:I

    iput-boolean p3, p0, LSpoilerSpan;->f:Z

    return-void
.end method


# virtual methods
.method public getBackgroundColor()I
    .locals 1

    iget-boolean v0, p0, LSpoilerSpan;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, LSpoilerSpan;->e:I

    goto :goto_0

    :cond_0
    iget v0, p0, LSpoilerSpan;->d:I

    :goto_0
    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    const-string/jumbo v0, "textPaint"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, LSpoilerSpan;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, LSpoilerSpan;->e:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    goto :goto_0

    :cond_0
    iget v0, p0, LSpoilerSpan;->d:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    :goto_0
    return-void
.end method
