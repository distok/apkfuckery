.class public final LWidgetRemoteAuthViewModel$a;
.super Lx/m/c/k;
.source "WidgetRemoteAuthViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = LWidgetRemoteAuthViewModel;-><init>(Ljava/lang/String;Lcom/discord/utilities/rest/RestAPI;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelRemoteAuthHandshake;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LWidgetRemoteAuthViewModel;


# direct methods
.method public constructor <init>(LWidgetRemoteAuthViewModel;)V
    .locals 0

    iput-object p1, p0, LWidgetRemoteAuthViewModel$a;->this$0:LWidgetRemoteAuthViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    check-cast p1, Lcom/discord/models/domain/ModelRemoteAuthHandshake;

    const-string v0, "handshakeData"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, LWidgetRemoteAuthViewModel$a;->this$0:LWidgetRemoteAuthViewModel;

    new-instance v1, LWidgetRemoteAuthViewModel$ViewState$b;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelRemoteAuthHandshake;->getHandshakeToken()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LWidgetRemoteAuthViewModel$a;->this$0:LWidgetRemoteAuthViewModel;

    iget-object v3, v3, LWidgetRemoteAuthViewModel;->d:Lrx/subjects/BehaviorSubject;

    const-string/jumbo v4, "temporaryBehaviorSubject"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lrx/subjects/BehaviorSubject;->i0()Ljava/lang/Object;

    move-result-object v3

    const-string/jumbo v4, "temporaryBehaviorSubject.value"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LWidgetRemoteAuthViewModel$ViewState$b;-><init>(Ljava/lang/String;ZZ)V

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lo;

    invoke-direct {v1, p0, p1}, Lo;-><init>(LWidgetRemoteAuthViewModel$a;Lcom/discord/models/domain/ModelRemoteAuthHandshake;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
