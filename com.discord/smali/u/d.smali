.class public Lu/d;
.super Ljava/lang/Object;
.source "Task.java"

# interfaces
.implements Lu/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu/c<",
        "TTResult;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lu/h;

.field public final synthetic b:Lu/c;

.field public final synthetic c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lu/g;Lu/h;Lu/c;Ljava/util/concurrent/Executor;)V
    .locals 0

    iput-object p2, p0, Lu/d;->a:Lu/h;

    iput-object p3, p0, Lu/d;->b:Lu/c;

    iput-object p4, p0, Lu/d;->c:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lu/g;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lu/d;->a:Lu/h;

    iget-object v1, p0, Lu/d;->b:Lu/c;

    iget-object v2, p0, Lu/d;->c:Ljava/util/concurrent/Executor;

    :try_start_0
    new-instance v3, Lu/e;

    invoke-direct {v3, v0, v1, p1}, Lu/e;-><init>(Lu/h;Lu/c;Lu/g;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v1, Lbolts/ExecutorException;

    invoke-direct {v1, p1}, Lbolts/ExecutorException;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Lu/h;->b(Ljava/lang/Exception;)V

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method
