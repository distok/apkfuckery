.class public final Lco/discord/media_engine/PlayoutMetric;
.super Ljava/lang/Object;
.source "Statistics.kt"


# instance fields
.field private final last:F

.field private final max:F

.field private final mean:F

.field private final p75:F

.field private final p95:F

.field private final p99:F


# direct methods
.method public constructor <init>(FFFFFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/PlayoutMetric;->last:F

    iput p2, p0, Lco/discord/media_engine/PlayoutMetric;->mean:F

    iput p3, p0, Lco/discord/media_engine/PlayoutMetric;->p75:F

    iput p4, p0, Lco/discord/media_engine/PlayoutMetric;->p95:F

    iput p5, p0, Lco/discord/media_engine/PlayoutMetric;->p99:F

    iput p6, p0, Lco/discord/media_engine/PlayoutMetric;->max:F

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/PlayoutMetric;FFFFFFILjava/lang/Object;)Lco/discord/media_engine/PlayoutMetric;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget p1, p0, Lco/discord/media_engine/PlayoutMetric;->last:F

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget p2, p0, Lco/discord/media_engine/PlayoutMetric;->mean:F

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lco/discord/media_engine/PlayoutMetric;->p75:F

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lco/discord/media_engine/PlayoutMetric;->p95:F

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lco/discord/media_engine/PlayoutMetric;->p99:F

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lco/discord/media_engine/PlayoutMetric;->max:F

    :cond_5
    move v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lco/discord/media_engine/PlayoutMetric;->copy(FFFFFF)Lco/discord/media_engine/PlayoutMetric;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->last:F

    return v0
.end method

.method public final component2()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->mean:F

    return v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p75:F

    return v0
.end method

.method public final component4()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p95:F

    return v0
.end method

.method public final component5()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p99:F

    return v0
.end method

.method public final component6()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->max:F

    return v0
.end method

.method public final copy(FFFFFF)Lco/discord/media_engine/PlayoutMetric;
    .locals 8

    new-instance v7, Lco/discord/media_engine/PlayoutMetric;

    move-object v0, v7

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lco/discord/media_engine/PlayoutMetric;-><init>(FFFFFF)V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lco/discord/media_engine/PlayoutMetric;

    if-eqz v0, :cond_0

    check-cast p1, Lco/discord/media_engine/PlayoutMetric;

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->last:F

    iget v1, p1, Lco/discord/media_engine/PlayoutMetric;->last:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->mean:F

    iget v1, p1, Lco/discord/media_engine/PlayoutMetric;->mean:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p75:F

    iget v1, p1, Lco/discord/media_engine/PlayoutMetric;->p75:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p95:F

    iget v1, p1, Lco/discord/media_engine/PlayoutMetric;->p95:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p99:F

    iget v1, p1, Lco/discord/media_engine/PlayoutMetric;->p99:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->max:F

    iget p1, p1, Lco/discord/media_engine/PlayoutMetric;->max:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLast()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->last:F

    return v0
.end method

.method public final getMax()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->max:F

    return v0
.end method

.method public final getMean()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->mean:F

    return v0
.end method

.method public final getP75()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p75:F

    return v0
.end method

.method public final getP95()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p95:F

    return v0
.end method

.method public final getP99()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->p99:F

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lco/discord/media_engine/PlayoutMetric;->last:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->mean:F

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->p75:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->p95:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->p99:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->max:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "PlayoutMetric(last="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->last:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", mean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->mean:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", p75="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->p75:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", p95="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->p95:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", p99="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->p99:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/PlayoutMetric;->max:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
