.class public final Lco/discord/media_engine/internal/InboundVideo;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final codecName:Ljava/lang/String;

.field private final codecPayloadType:I

.field private final currentDelay:F

.field private final currentPayloadType:I

.field private final decode:I

.field private final decodeFrameRate:I

.field private final decoderImplementationName:Ljava/lang/String;

.field private final discardedPackets:I

.field private final frameCounts:Lco/discord/media_engine/internal/FrameCounts;

.field private final framesDecoded:I

.field private final framesDropped:I

.field private final framesRendered:I

.field private final height:I

.field private final jitterBuffer:F

.field private final maxDecode:F

.field private final minPlayoutDelay:F

.field private final networkFrameRate:I

.field private final qpSum:I

.field private final renderDelay:F

.field private final renderFrameRate:I

.field private final rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

.field private final rtpStats:Lco/discord/media_engine/internal/RtpStats;

.field private final ssrc:I

.field private final syncOffset:F

.field private final targetDelay:F

.field private final totalBitrate:I

.field private final width:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IFIIILjava/lang/String;ILco/discord/media_engine/internal/FrameCounts;IIIIFFFIIFILco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;IFFII)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p7

    move-object/from16 v3, p9

    move-object/from16 v4, p21

    move-object/from16 v5, p22

    const-string v6, "codecName"

    invoke-static {p1, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "decoderImplementationName"

    invoke-static {p7, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "frameCounts"

    invoke-static {v3, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "rtcpStats"

    invoke-static {v4, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "rtpStats"

    invoke-static {v5, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    move v1, p2

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    move v1, p3

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    move v1, p4

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    move v1, p5

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    move v1, p6

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    iput-object v2, v0, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    move v1, p8

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    iput-object v3, v0, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    move/from16 v1, p10

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    move/from16 v1, p11

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    move/from16 v1, p12

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    move/from16 v1, p13

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->height:I

    move/from16 v1, p14

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    move/from16 v1, p15

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    move/from16 v1, p16

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    move/from16 v1, p17

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    move/from16 v1, p18

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    move/from16 v1, p19

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    move/from16 v1, p20

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    iput-object v4, v0, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    iput-object v5, v0, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    move/from16 v1, p23

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    move/from16 v1, p24

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    move/from16 v1, p25

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    move/from16 v1, p26

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    move/from16 v1, p27

    iput v1, v0, Lco/discord/media_engine/internal/InboundVideo;->width:I

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/InboundVideo;Ljava/lang/String;IFIIILjava/lang/String;ILco/discord/media_engine/internal/FrameCounts;IIIIFFFIIFILco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;IFFIIILjava/lang/Object;)Lco/discord/media_engine/internal/InboundVideo;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p28

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget v6, v0, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget v11, v0, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget v12, v0, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget v13, v0, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    goto :goto_b

    :cond_b
    move/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget v14, v0, Lco/discord/media_engine/internal/InboundVideo;->height:I

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    goto :goto_d

    :cond_d
    move/from16 v15, p14

    :goto_d
    move/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    goto :goto_e

    :cond_e
    move/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move/from16 p15, v15

    if-eqz v16, :cond_f

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    goto :goto_f

    :cond_f
    move/from16 v15, p16

    :goto_f
    const/high16 v16, 0x10000

    and-int v16, v1, v16

    move/from16 p16, v15

    if-eqz v16, :cond_10

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    goto :goto_10

    :cond_10
    move/from16 v15, p17

    :goto_10
    const/high16 v16, 0x20000

    and-int v16, v1, v16

    move/from16 p17, v15

    if-eqz v16, :cond_11

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    goto :goto_11

    :cond_11
    move/from16 v15, p18

    :goto_11
    const/high16 v16, 0x40000

    and-int v16, v1, v16

    move/from16 p18, v15

    if-eqz v16, :cond_12

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    goto :goto_12

    :cond_12
    move/from16 v15, p19

    :goto_12
    const/high16 v16, 0x80000

    and-int v16, v1, v16

    move/from16 p19, v15

    if-eqz v16, :cond_13

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    goto :goto_13

    :cond_13
    move/from16 v15, p20

    :goto_13
    const/high16 v16, 0x100000

    and-int v16, v1, v16

    move/from16 p20, v15

    if-eqz v16, :cond_14

    iget-object v15, v0, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    goto :goto_14

    :cond_14
    move-object/from16 v15, p21

    :goto_14
    const/high16 v16, 0x200000

    and-int v16, v1, v16

    move-object/from16 p21, v15

    if-eqz v16, :cond_15

    iget-object v15, v0, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    goto :goto_15

    :cond_15
    move-object/from16 v15, p22

    :goto_15
    const/high16 v16, 0x400000

    and-int v16, v1, v16

    move-object/from16 p22, v15

    if-eqz v16, :cond_16

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    goto :goto_16

    :cond_16
    move/from16 v15, p23

    :goto_16
    const/high16 v16, 0x800000

    and-int v16, v1, v16

    move/from16 p23, v15

    if-eqz v16, :cond_17

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    goto :goto_17

    :cond_17
    move/from16 v15, p24

    :goto_17
    const/high16 v16, 0x1000000

    and-int v16, v1, v16

    move/from16 p24, v15

    if-eqz v16, :cond_18

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    goto :goto_18

    :cond_18
    move/from16 v15, p25

    :goto_18
    const/high16 v16, 0x2000000

    and-int v16, v1, v16

    move/from16 p25, v15

    if-eqz v16, :cond_19

    iget v15, v0, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    goto :goto_19

    :cond_19
    move/from16 v15, p26

    :goto_19
    const/high16 v16, 0x4000000

    and-int v1, v1, v16

    if-eqz v1, :cond_1a

    iget v1, v0, Lco/discord/media_engine/internal/InboundVideo;->width:I

    goto :goto_1a

    :cond_1a
    move/from16 v1, p27

    :goto_1a
    move-object/from16 p1, v2

    move/from16 p2, v3

    move/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v13

    move/from16 p13, v14

    move/from16 p26, v15

    move/from16 p27, v1

    invoke-virtual/range {p0 .. p27}, Lco/discord/media_engine/internal/InboundVideo;->copy(Ljava/lang/String;IFIIILjava/lang/String;ILco/discord/media_engine/internal/FrameCounts;IIIIFFFIIFILco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;IFFII)Lco/discord/media_engine/internal/InboundVideo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    return v0
.end method

.method public final component11()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    return v0
.end method

.method public final component12()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    return v0
.end method

.method public final component13()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->height:I

    return v0
.end method

.method public final component14()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    return v0
.end method

.method public final component15()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    return v0
.end method

.method public final component16()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    return v0
.end method

.method public final component17()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    return v0
.end method

.method public final component18()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    return v0
.end method

.method public final component19()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    return v0
.end method

.method public final component20()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    return v0
.end method

.method public final component21()Lco/discord/media_engine/internal/RtcpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    return-object v0
.end method

.method public final component22()Lco/discord/media_engine/internal/RtpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    return-object v0
.end method

.method public final component23()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    return v0
.end method

.method public final component24()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    return v0
.end method

.method public final component25()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    return v0
.end method

.method public final component26()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    return v0
.end method

.method public final component27()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->width:I

    return v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    return v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    return v0
.end method

.method public final component9()Lco/discord/media_engine/internal/FrameCounts;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;IFIIILjava/lang/String;ILco/discord/media_engine/internal/FrameCounts;IIIIFFFIIFILco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;IFFII)Lco/discord/media_engine/internal/InboundVideo;
    .locals 30

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    move/from16 v15, p15

    move/from16 v16, p16

    move/from16 v17, p17

    move/from16 v18, p18

    move/from16 v19, p19

    move/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    move/from16 v23, p23

    move/from16 v24, p24

    move/from16 v25, p25

    move/from16 v26, p26

    move/from16 v27, p27

    const-string v0, "codecName"

    move-object/from16 v28, v1

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "decoderImplementationName"

    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "frameCounts"

    move-object/from16 v1, p9

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtcpStats"

    move-object/from16 v1, p21

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtpStats"

    move-object/from16 v1, p22

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v29, Lco/discord/media_engine/internal/InboundVideo;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-direct/range {v0 .. v27}, Lco/discord/media_engine/internal/InboundVideo;-><init>(Ljava/lang/String;IFIIILjava/lang/String;ILco/discord/media_engine/internal/FrameCounts;IIIIFFFIIFILco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;IFFII)V

    return-object v29
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_10

    instance-of v1, p1, Lco/discord/media_engine/internal/InboundVideo;

    const/4 v2, 0x0

    if-eqz v1, :cond_f

    check-cast p1, Lco/discord/media_engine/internal/InboundVideo;

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    iget-object v3, p1, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_f

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    iget-object v3, p1, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_f

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    iget-object v3, p1, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    if-ne v1, v3, :cond_6

    const/4 v1, 0x1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    if-ne v1, v3, :cond_7

    const/4 v1, 0x1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->height:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->height:I

    if-ne v1, v3, :cond_8

    const/4 v1, 0x1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    if-ne v1, v3, :cond_9

    const/4 v1, 0x1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    if-ne v1, v3, :cond_a

    const/4 v1, 0x1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    if-ne v1, v3, :cond_b

    const/4 v1, 0x1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    if-eqz v1, :cond_f

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    iget-object v3, p1, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    iget-object v3, p1, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    if-ne v1, v3, :cond_c

    const/4 v1, 0x1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    iget v3, p1, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    if-ne v1, v3, :cond_d

    const/4 v1, 0x1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    if-eqz v1, :cond_f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->width:I

    iget p1, p1, Lco/discord/media_engine/internal/InboundVideo;->width:I

    if-ne v1, p1, :cond_e

    const/4 p1, 0x1

    goto :goto_e

    :cond_e
    const/4 p1, 0x0

    :goto_e
    if-eqz p1, :cond_f

    goto :goto_f

    :cond_f
    return v2

    :cond_10
    :goto_f
    return v0
.end method

.method public final getCodecName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCodecPayloadType()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    return v0
.end method

.method public final getCurrentDelay()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    return v0
.end method

.method public final getCurrentPayloadType()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    return v0
.end method

.method public final getDecode()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    return v0
.end method

.method public final getDecodeFrameRate()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    return v0
.end method

.method public final getDecoderImplementationName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    return-object v0
.end method

.method public final getDiscardedPackets()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    return v0
.end method

.method public final getFrameCounts()Lco/discord/media_engine/internal/FrameCounts;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    return-object v0
.end method

.method public final getFramesDecoded()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    return v0
.end method

.method public final getFramesDropped()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    return v0
.end method

.method public final getFramesRendered()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    return v0
.end method

.method public final getHeight()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->height:I

    return v0
.end method

.method public final getJitterBuffer()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    return v0
.end method

.method public final getMaxDecode()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    return v0
.end method

.method public final getMinPlayoutDelay()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    return v0
.end method

.method public final getNetworkFrameRate()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    return v0
.end method

.method public final getQpSum()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    return v0
.end method

.method public final getRenderDelay()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    return v0
.end method

.method public final getRenderFrameRate()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    return v0
.end method

.method public final getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    return-object v0
.end method

.method public final getRtpStats()Lco/discord/media_engine/internal/RtpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    return-object v0
.end method

.method public final getSsrc()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    return v0
.end method

.method public final getSyncOffset()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    return v0
.end method

.method public final getTargetDelay()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    return v0
.end method

.method public final getTotalBitrate()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/InboundVideo;->width:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    const/16 v3, 0x1f

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lco/discord/media_engine/internal/FrameCounts;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->height:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    const/16 v3, 0x1f

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v2, p0, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lco/discord/media_engine/internal/RtcpStats;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lco/discord/media_engine/internal/RtpStats;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->width:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "InboundVideo(codecName="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->codecName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", codecPayloadType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->codecPayloadType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", currentDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->currentDelay:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", currentPayloadType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->currentPayloadType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decodeFrameRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->decodeFrameRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->decode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decoderImplementationName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->decoderImplementationName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", discardedPackets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->discardedPackets:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", frameCounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", framesDecoded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDecoded:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", framesRendered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->framesRendered:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", framesDropped="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->framesDropped:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", jitterBuffer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->jitterBuffer:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", maxDecode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->maxDecode:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", minPlayoutDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->minPlayoutDelay:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", networkFrameRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->networkFrameRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", qpSum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->qpSum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", renderDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->renderDelay:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", renderFrameRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->renderFrameRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", rtcpStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rtpStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/InboundVideo;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ssrc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", syncOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->syncOffset:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", targetDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->targetDelay:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", totalBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->totalBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/InboundVideo;->width:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
