.class public final Lco/discord/media_engine/internal/TransformStats;
.super Ljava/lang/Object;
.source "TransformStats.kt"


# static fields
.field public static final INSTANCE:Lco/discord/media_engine/internal/TransformStats;

.field private static final gson:Lcom/google/gson/Gson;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/discord/media_engine/internal/TransformStats;

    invoke-direct {v0}, Lco/discord/media_engine/internal/TransformStats;-><init>()V

    sput-object v0, Lco/discord/media_engine/internal/TransformStats;->INSTANCE:Lco/discord/media_engine/internal/TransformStats;

    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    sput-object v0, Lco/discord/media_engine/internal/TransformStats;->gson:Lcom/google/gson/Gson;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;
    .locals 11

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getLast()F

    move-result v0

    const/16 v1, 0x3e8

    int-to-float v1, v1

    mul-float v0, v0, v1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    move-result-wide v2

    double-to-float v5, v2

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getMean()F

    move-result v0

    mul-float v0, v0, v1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    move-result-wide v2

    double-to-float v6, v2

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getP75()F

    move-result v0

    mul-float v0, v0, v1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    move-result-wide v2

    double-to-float v7, v2

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getP95()F

    move-result v0

    mul-float v0, v0, v1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    move-result-wide v2

    double-to-float v8, v2

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getP99()F

    move-result v0

    mul-float v0, v0, v1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    move-result-wide v2

    double-to-float v9, v2

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getMax()F

    move-result p1

    mul-float p1, p1, v1

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->rint(D)D

    move-result-wide v0

    double-to-float v10, v0

    new-instance p1, Lco/discord/media_engine/PlayoutMetric;

    move-object v4, p1

    invoke-direct/range {v4 .. v10}, Lco/discord/media_engine/PlayoutMetric;-><init>(FFFFFF)V

    return-object p1
.end method

.method private final sumBytes(Lco/discord/media_engine/internal/RtpStats;)J
    .locals 4

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getFec()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v0

    invoke-virtual {v0}, Lco/discord/media_engine/internal/PacketStats;->getHeaderBytes()J

    move-result-wide v0

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getFec()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v2

    invoke-virtual {v2}, Lco/discord/media_engine/internal/PacketStats;->getPayloadBytes()J

    move-result-wide v2

    add-long/2addr v2, v0

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getFec()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v0

    invoke-virtual {v0}, Lco/discord/media_engine/internal/PacketStats;->getPaddingBytes()J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getRetransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v2

    invoke-virtual {v2}, Lco/discord/media_engine/internal/PacketStats;->getHeaderBytes()J

    move-result-wide v2

    add-long/2addr v2, v0

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getRetransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v0

    invoke-virtual {v0}, Lco/discord/media_engine/internal/PacketStats;->getPayloadBytes()J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getRetransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v2

    invoke-virtual {v2}, Lco/discord/media_engine/internal/PacketStats;->getPaddingBytes()J

    move-result-wide v2

    add-long/2addr v2, v0

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getTransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v0

    invoke-virtual {v0}, Lco/discord/media_engine/internal/PacketStats;->getHeaderBytes()J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getTransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v2

    invoke-virtual {v2}, Lco/discord/media_engine/internal/PacketStats;->getPayloadBytes()J

    move-result-wide v2

    add-long/2addr v2, v0

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getTransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object p1

    invoke-virtual {p1}, Lco/discord/media_engine/internal/PacketStats;->getPaddingBytes()J

    move-result-wide v0

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private final sumPackets(Lco/discord/media_engine/internal/RtpStats;)I
    .locals 2

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getFec()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v0

    invoke-virtual {v0}, Lco/discord/media_engine/internal/PacketStats;->getPackets()I

    move-result v0

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getRetransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object v1

    invoke-virtual {v1}, Lco/discord/media_engine/internal/PacketStats;->getPackets()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p1}, Lco/discord/media_engine/internal/RtpStats;->getTransmitted()Lco/discord/media_engine/internal/PacketStats;

    move-result-object p1

    invoke-virtual {p1}, Lco/discord/media_engine/internal/PacketStats;->getPackets()I

    move-result p1

    add-int/2addr p1, v1

    return p1
.end method

.method public static final transform(Ljava/lang/String;)Lco/discord/media_engine/Stats;
    .locals 80

    move-object/from16 v0, p0

    const-string/jumbo v1, "stats"

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lco/discord/media_engine/internal/TransformStats;->gson:Lcom/google/gson/Gson;

    const-class v2, Lco/discord/media_engine/internal/NativeStats;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/Gson;->f(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2}, Lf/h/a/f/f/n/g;->k0(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/discord/media_engine/internal/NativeStats;

    invoke-virtual {v0}, Lco/discord/media_engine/internal/NativeStats;->getOutbound()Lco/discord/media_engine/internal/Outbound;

    move-result-object v1

    const/high16 v2, 0x47000000    # 32768.0f

    const-wide/16 v3, 0x0

    const/4 v6, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lco/discord/media_engine/internal/Outbound;->getAudio()Lco/discord/media_engine/internal/OutboundAudio;

    move-result-object v8

    invoke-virtual {v8}, Lco/discord/media_engine/internal/OutboundAudio;->getBytesSent()I

    move-result v8

    int-to-long v8, v8

    add-long/2addr v8, v3

    invoke-virtual {v1}, Lco/discord/media_engine/internal/Outbound;->getAudio()Lco/discord/media_engine/internal/OutboundAudio;

    move-result-object v1

    new-instance v31, Lco/discord/media_engine/OutboundRtpAudio;

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getSsrc()I

    move-result v12

    new-instance v13, Lco/discord/media_engine/StatsCodec;

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getCodecPayloadType()I

    move-result v10

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getCodecName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v13, v10, v11}, Lco/discord/media_engine/StatsCodec;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getPacketsSent()I

    move-result v16

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getPacketsLost()I

    move-result v10

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v17

    const/16 v10, 0x64

    int-to-float v10, v10

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getFractionLost()F

    move-result v11

    mul-float v18, v11, v10

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getAudioLevel()I

    move-result v10

    int-to-float v10, v10

    div-float v19, v10, v2

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getSpeaking()I

    move-result v10

    if-lez v10, :cond_0

    const/16 v20, 0x1

    goto :goto_0

    :cond_0
    const/16 v20, 0x0

    :goto_0
    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getFramesCaptured()J

    move-result-wide v21

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getFramesRendered()J

    move-result-wide v23

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getNoiseCancellerIsEnabled()Z

    move-result v25

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getNoiseCancellerProcessTime()J

    move-result-wide v26

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getVoiceActivityDetectorIsEnabled()Z

    move-result v28

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundAudio;->getVoiceActivityDetectorProcessTime()J

    move-result-wide v29

    const-string v11, "audio"

    move-object/from16 v10, v31

    move-wide v14, v8

    invoke-direct/range {v10 .. v30}, Lco/discord/media_engine/OutboundRtpAudio;-><init>(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFFZJJZJZJ)V

    move-object/from16 v12, v31

    goto :goto_1

    :cond_1
    move-wide v8, v3

    const/4 v12, 0x0

    :goto_1
    invoke-virtual {v0}, Lco/discord/media_engine/internal/NativeStats;->getOutbound()Lco/discord/media_engine/internal/Outbound;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lco/discord/media_engine/internal/Outbound;->getVideo()Lco/discord/media_engine/internal/OutboundVideo;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getSubstreams()[Lco/discord/media_engine/internal/Substream;

    move-result-object v1

    if-eqz v1, :cond_4

    array-length v10, v1

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v10, :cond_4

    aget-object v13, v1, v11

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->isFlexFEC()Z

    move-result v14

    if-nez v14, :cond_2

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->isRTX()Z

    move-result v14

    if-nez v14, :cond_2

    const/4 v14, 0x1

    goto :goto_3

    :cond_2
    const/4 v14, 0x0

    :goto_3
    if-eqz v14, :cond_3

    goto :goto_4

    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_4
    const/4 v13, 0x0

    :goto_4
    if-eqz v13, :cond_7

    invoke-virtual {v0}, Lco/discord/media_engine/internal/NativeStats;->getOutbound()Lco/discord/media_engine/internal/Outbound;

    move-result-object v1

    invoke-virtual {v1}, Lco/discord/media_engine/internal/Outbound;->getVideo()Lco/discord/media_engine/internal/OutboundVideo;

    move-result-object v1

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getSubstreams()[Lco/discord/media_engine/internal/Substream;

    move-result-object v1

    array-length v10, v1

    move-wide/from16 v18, v3

    const/4 v11, 0x0

    :goto_5
    if-ge v11, v10, :cond_5

    aget-object v14, v1, v11

    sget-object v15, Lco/discord/media_engine/internal/TransformStats;->INSTANCE:Lco/discord/media_engine/internal/TransformStats;

    invoke-virtual {v14}, Lco/discord/media_engine/internal/Substream;->getRtpStats()Lco/discord/media_engine/internal/RtpStats;

    move-result-object v14

    invoke-direct {v15, v14}, Lco/discord/media_engine/internal/TransformStats;->sumBytes(Lco/discord/media_engine/internal/RtpStats;)J

    move-result-wide v14

    add-long v18, v18, v14

    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :cond_5
    add-long v8, v8, v18

    invoke-virtual {v0}, Lco/discord/media_engine/internal/NativeStats;->getOutbound()Lco/discord/media_engine/internal/Outbound;

    move-result-object v1

    invoke-virtual {v1}, Lco/discord/media_engine/internal/Outbound;->getVideo()Lco/discord/media_engine/internal/OutboundVideo;

    move-result-object v1

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getSsrc()I

    move-result v16

    new-instance v10, Lco/discord/media_engine/StatsCodec;

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getCodecPayloadType()I

    move-result v11

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getCodecName()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v10, v11, v14}, Lco/discord/media_engine/StatsCodec;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getSubstreams()[Lco/discord/media_engine/internal/Substream;

    move-result-object v11

    array-length v14, v11

    const/4 v15, 0x0

    const/16 v20, 0x0

    :goto_6
    if-ge v15, v14, :cond_6

    aget-object v17, v11, v15

    sget-object v3, Lco/discord/media_engine/internal/TransformStats;->INSTANCE:Lco/discord/media_engine/internal/TransformStats;

    invoke-virtual/range {v17 .. v17}, Lco/discord/media_engine/internal/Substream;->getRtpStats()Lco/discord/media_engine/internal/RtpStats;

    move-result-object v4

    invoke-direct {v3, v4}, Lco/discord/media_engine/internal/TransformStats;->sumPackets(Lco/discord/media_engine/internal/RtpStats;)I

    move-result v3

    add-int v20, v20, v3

    add-int/lit8 v15, v15, 0x1

    const-wide/16 v3, 0x0

    goto :goto_6

    :cond_6
    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v3

    invoke-virtual {v3}, Lco/discord/media_engine/internal/RtcpStats;->getPacketsLost()I

    move-result v21

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v3

    invoke-virtual {v3}, Lco/discord/media_engine/internal/RtcpStats;->getFractionLost()F

    move-result v22

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getMediaBitrate()I

    move-result v23

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getTargetMediaBitrate()I

    move-result v24

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getEncodeUsage()I

    move-result v25

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getEncoderImplementationName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getAvgEncodeTime()I

    move-result v27

    new-instance v3, Lco/discord/media_engine/Resolution;

    move-object/from16 v28, v3

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getWidth()I

    move-result v4

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getHeight()I

    move-result v11

    invoke-direct {v3, v4, v11}, Lco/discord/media_engine/Resolution;-><init>(II)V

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getFrameCounts()Lco/discord/media_engine/internal/FrameCounts;

    move-result-object v3

    invoke-virtual {v3}, Lco/discord/media_engine/internal/FrameCounts;->getDeltaFrames()I

    move-result v3

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getFrameCounts()Lco/discord/media_engine/internal/FrameCounts;

    move-result-object v4

    invoke-virtual {v4}, Lco/discord/media_engine/internal/FrameCounts;->getKeyFrames()I

    move-result v4

    add-int v29, v4, v3

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getFramesEncoded()I

    move-result v30

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getInputFrameRate()I

    move-result v31

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getEncodeFrameRate()I

    move-result v32

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v3

    invoke-virtual {v3}, Lco/discord/media_engine/internal/RtcpStats;->getFirPackets()I

    move-result v33

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v3

    invoke-virtual {v3}, Lco/discord/media_engine/internal/RtcpStats;->getNackPackets()I

    move-result v34

    invoke-virtual {v13}, Lco/discord/media_engine/internal/Substream;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v3

    invoke-virtual {v3}, Lco/discord/media_engine/internal/RtcpStats;->getPliPackets()I

    move-result v35

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getQpSum()I

    move-result v36

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getBwLimitedResolution()Z

    move-result v37

    invoke-virtual {v1}, Lco/discord/media_engine/internal/OutboundVideo;->getCpuLimitedResolution()Z

    move-result v38

    new-instance v1, Lco/discord/media_engine/OutboundRtpVideo;

    move-object v14, v1

    const-string/jumbo v15, "video"

    move-object/from16 v17, v10

    invoke-direct/range {v14 .. v38}, Lco/discord/media_engine/OutboundRtpVideo;-><init>(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFIIILjava/lang/String;ILco/discord/media_engine/Resolution;IIIIIIIIZZ)V

    move-object v13, v1

    move-wide/from16 v45, v8

    goto :goto_7

    :cond_7
    move-wide/from16 v45, v8

    const/4 v13, 0x0

    :goto_7
    new-instance v14, Ljava/util/LinkedHashMap;

    invoke-direct {v14}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v15, Ljava/util/LinkedHashMap;

    invoke-direct {v15}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {v0}, Lco/discord/media_engine/internal/NativeStats;->getInbound()[Lco/discord/media_engine/internal/Inbound;

    move-result-object v1

    if-eqz v1, :cond_13

    array-length v3, v1

    const/4 v4, 0x0

    const-wide/16 v39, 0x0

    :goto_8
    if-ge v4, v3, :cond_12

    aget-object v8, v1, v4

    invoke-virtual {v8}, Lco/discord/media_engine/internal/Inbound;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Lco/discord/media_engine/internal/Inbound;->getAudio()Lco/discord/media_engine/internal/InboundAudio;

    move-result-object v10

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getBytesReceived()I

    move-result v10

    int-to-long v10, v10

    add-long v39, v39, v10

    invoke-virtual {v8}, Lco/discord/media_engine/internal/Inbound;->getAudio()Lco/discord/media_engine/internal/InboundAudio;

    move-result-object v10

    invoke-virtual {v8}, Lco/discord/media_engine/internal/Inbound;->getPlayout()Lco/discord/media_engine/internal/InboundPlayout;

    move-result-object v11

    if-eqz v11, :cond_8

    sget-object v11, Lco/discord/media_engine/internal/TransformStats;->INSTANCE:Lco/discord/media_engine/internal/TransformStats;

    invoke-virtual {v8}, Lco/discord/media_engine/internal/Inbound;->getPlayout()Lco/discord/media_engine/internal/InboundPlayout;

    move-result-object v5

    invoke-direct {v11, v5}, Lco/discord/media_engine/internal/TransformStats;->transformPlayoutStats(Lco/discord/media_engine/internal/InboundPlayout;)Lco/discord/media_engine/internal/InboundPlayout;

    move-result-object v5

    goto :goto_9

    :cond_8
    const/4 v5, 0x0

    :goto_9
    new-instance v11, Lco/discord/media_engine/InboundRtpAudio;

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getSsrc()I

    move-result v49

    new-instance v7, Lco/discord/media_engine/StatsCodec;

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getCodecPayloadType()I

    move-result v6

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getCodecName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v6, v2}, Lco/discord/media_engine/StatsCodec;-><init>(ILjava/lang/String;)V

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getPacketsReceived()I

    move-result v53

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getPacketsLost()I

    move-result v54

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getFractionLost()F

    move-result v55

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getAudioLevel()I

    move-result v2

    int-to-float v2, v2

    const/high16 v6, 0x47000000    # 32768.0f

    div-float v56, v2, v6

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getSpeaking()I

    move-result v2

    if-lez v2, :cond_9

    const/16 v57, 0x1

    goto :goto_a

    :cond_9
    const/16 v57, 0x0

    :goto_a
    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getJitter()I

    move-result v58

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getJitterBuffer()I

    move-result v59

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getJitterBufferPreferred()I

    move-result v60

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getDelayEstimate()I

    move-result v61

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getDecodingCNG()I

    move-result v62

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getDecodingMutedOutput()I

    move-result v63

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getDecodingNormal()I

    move-result v64

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getDecodingPLC()I

    move-result v65

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getDecodingPLCCNG()I

    move-result v66

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getOpSilence()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v67

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getOpNormal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v68

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getOpMerge()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v69

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getOpExpand()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v70

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getOpAccelerate()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v71

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getOpPreemptiveExpand()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v72

    invoke-virtual {v10}, Lco/discord/media_engine/internal/InboundAudio;->getOpCNG()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v73

    if-eqz v5, :cond_a

    invoke-virtual {v5}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioJitterBuffer()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v2

    move-object/from16 v74, v2

    goto :goto_b

    :cond_a
    const/16 v74, 0x0

    :goto_b
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioJitterDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v2

    move-object/from16 v75, v2

    goto :goto_c

    :cond_b
    const/16 v75, 0x0

    :goto_c
    if-eqz v5, :cond_c

    invoke-virtual {v5}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioJitterTarget()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v2

    move-object/from16 v76, v2

    goto :goto_d

    :cond_c
    const/16 v76, 0x0

    :goto_d
    if-eqz v5, :cond_d

    invoke-virtual {v5}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioPlayoutUnderruns()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v2

    move-object/from16 v77, v2

    goto :goto_e

    :cond_d
    const/16 v77, 0x0

    :goto_e
    if-eqz v5, :cond_e

    invoke-virtual {v5}, Lco/discord/media_engine/internal/InboundPlayout;->getRelativeReceptionDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v2

    move-object/from16 v78, v2

    goto :goto_f

    :cond_e
    const/16 v78, 0x0

    :goto_f
    if-eqz v5, :cond_f

    invoke-virtual {v5}, Lco/discord/media_engine/internal/InboundPlayout;->getRelativePlayoutDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v2

    move-object/from16 v79, v2

    goto :goto_10

    :cond_f
    const/16 v79, 0x0

    :goto_10
    const-string v48, "audio"

    move-object/from16 v47, v11

    move-object/from16 v50, v7

    move-wide/from16 v51, v39

    invoke-direct/range {v47 .. v79}, Lco/discord/media_engine/InboundRtpAudio;-><init>(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFFZIIIIIIIIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)V

    invoke-interface {v14, v9, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/discord/media_engine/InboundRtpAudio;

    invoke-virtual {v8}, Lco/discord/media_engine/internal/Inbound;->getVideo()Lco/discord/media_engine/internal/InboundVideo;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {v8}, Lco/discord/media_engine/internal/Inbound;->getVideo()Lco/discord/media_engine/internal/InboundVideo;

    move-result-object v2

    sget-object v5, Lco/discord/media_engine/internal/TransformStats;->INSTANCE:Lco/discord/media_engine/internal/TransformStats;

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRtpStats()Lco/discord/media_engine/internal/RtpStats;

    move-result-object v7

    invoke-direct {v5, v7}, Lco/discord/media_engine/internal/TransformStats;->sumBytes(Lco/discord/media_engine/internal/RtpStats;)J

    move-result-wide v51

    add-long v39, v39, v51

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getCodecPayloadType()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_10

    new-instance v7, Lco/discord/media_engine/StatsCodec;

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getCodecPayloadType()I

    move-result v8

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getCodecName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v10}, Lco/discord/media_engine/StatsCodec;-><init>(ILjava/lang/String;)V

    move-object/from16 v50, v7

    goto :goto_11

    :cond_10
    const/16 v50, 0x0

    :goto_11
    new-instance v7, Lco/discord/media_engine/InboundRtpVideo;

    move-object/from16 v47, v7

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getSsrc()I

    move-result v49

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRtpStats()Lco/discord/media_engine/internal/RtpStats;

    move-result-object v8

    invoke-direct {v5, v8}, Lco/discord/media_engine/internal/TransformStats;->sumPackets(Lco/discord/media_engine/internal/RtpStats;)I

    move-result v53

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v5

    invoke-virtual {v5}, Lco/discord/media_engine/internal/RtcpStats;->getPacketsLost()I

    move-result v54

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v5

    invoke-virtual {v5}, Lco/discord/media_engine/internal/RtcpStats;->getFractionLost()F

    move-result v55

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getTotalBitrate()I

    move-result v56

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getDecode()I

    move-result v57

    new-instance v5, Lco/discord/media_engine/Resolution;

    move-object/from16 v58, v5

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getWidth()I

    move-result v8

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getHeight()I

    move-result v10

    invoke-direct {v5, v8, v10}, Lco/discord/media_engine/Resolution;-><init>(II)V

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getDecoderImplementationName()Ljava/lang/String;

    move-result-object v59

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getFramesDecoded()I

    move-result v60

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getDiscardedPackets()I

    move-result v61

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getFrameCounts()Lco/discord/media_engine/internal/FrameCounts;

    move-result-object v5

    invoke-virtual {v5}, Lco/discord/media_engine/internal/FrameCounts;->getDeltaFrames()I

    move-result v5

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getFrameCounts()Lco/discord/media_engine/internal/FrameCounts;

    move-result-object v8

    invoke-virtual {v8}, Lco/discord/media_engine/internal/FrameCounts;->getKeyFrames()I

    move-result v8

    add-int v62, v8, v5

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getDecodeFrameRate()I

    move-result v63

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getNetworkFrameRate()I

    move-result v64

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRenderFrameRate()I

    move-result v65

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v5

    invoke-virtual {v5}, Lco/discord/media_engine/internal/RtcpStats;->getFirPackets()I

    move-result v66

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v5

    invoke-virtual {v5}, Lco/discord/media_engine/internal/RtcpStats;->getNackPackets()I

    move-result v67

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v5

    invoke-virtual {v5}, Lco/discord/media_engine/internal/RtcpStats;->getPliPackets()I

    move-result v68

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->getQpSum()I

    move-result v69

    const-string/jumbo v48, "video"

    invoke-direct/range {v47 .. v69}, Lco/discord/media_engine/InboundRtpVideo;-><init>(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFIILco/discord/media_engine/Resolution;Ljava/lang/String;IIIIIIIIII)V

    invoke-interface {v15, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/discord/media_engine/InboundRtpVideo;

    :cond_11
    add-int/lit8 v4, v4, 0x1

    const/high16 v2, 0x47000000    # 32768.0f

    const/4 v6, 0x0

    goto/16 :goto_8

    :cond_12
    move-wide/from16 v43, v39

    goto :goto_12

    :cond_13
    const-wide/16 v43, 0x0

    :goto_12
    invoke-virtual {v0}, Lco/discord/media_engine/internal/NativeStats;->getTransport()Lco/discord/media_engine/internal/Transport;

    move-result-object v0

    if-eqz v0, :cond_14

    new-instance v1, Lco/discord/media_engine/Transport;

    invoke-virtual {v0}, Lco/discord/media_engine/internal/Transport;->getSendBandwidth()I

    move-result v42

    invoke-virtual {v0}, Lco/discord/media_engine/internal/Transport;->getRtt()I

    move-result v47

    invoke-virtual {v0}, Lco/discord/media_engine/internal/Transport;->getDecryptionFailures()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    invoke-virtual {v0}, Lco/discord/media_engine/internal/Transport;->getLocalAddress()Ljava/lang/String;

    move-result-object v49

    invoke-virtual {v0}, Lco/discord/media_engine/internal/Transport;->getReceiverReports()[Lco/discord/media_engine/ReceiverReport;

    move-result-object v50

    move-object/from16 v41, v1

    invoke-direct/range {v41 .. v50}, Lco/discord/media_engine/Transport;-><init>(IJJILjava/lang/Integer;Ljava/lang/String;[Lco/discord/media_engine/ReceiverReport;)V

    move-object v11, v1

    goto :goto_13

    :cond_14
    new-instance v0, Lco/discord/media_engine/Transport;

    const/16 v42, 0x0

    const/16 v47, 0x0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    const/16 v50, 0x0

    const-string v49, ""

    move-object/from16 v41, v0

    invoke-direct/range {v41 .. v50}, Lco/discord/media_engine/Transport;-><init>(IJJILjava/lang/Integer;Ljava/lang/String;[Lco/discord/media_engine/ReceiverReport;)V

    move-object v11, v0

    :goto_13
    new-instance v0, Lco/discord/media_engine/Stats;

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Lco/discord/media_engine/Stats;-><init>(Lco/discord/media_engine/Transport;Lco/discord/media_engine/OutboundRtpAudio;Lco/discord/media_engine/OutboundRtpVideo;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method

.method private final transformPlayoutStats(Lco/discord/media_engine/internal/InboundPlayout;)Lco/discord/media_engine/internal/InboundPlayout;
    .locals 11

    new-instance v10, Lco/discord/media_engine/internal/InboundPlayout;

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioJitterBuffer()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v1

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioJitterDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v2

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioJitterTarget()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v3

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getAudioPlayoutUnderruns()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v4

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getVideoJitterBuffer()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v5

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getVideoJitterDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v6

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getVideoJitterTarget()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v7

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getRelativeReceptionDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v8

    invoke-virtual {p1}, Lco/discord/media_engine/internal/InboundPlayout;->getRelativePlayoutDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object p1

    invoke-direct {p0, p1}, Lco/discord/media_engine/internal/TransformStats;->convertPlayoutMetricToMs(Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/PlayoutMetric;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lco/discord/media_engine/internal/InboundPlayout;-><init>(Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)V

    return-object v10
.end method
