.class public final Lco/discord/media_engine/internal/Inbound;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final audio:Lco/discord/media_engine/internal/InboundAudio;

.field private final id:Ljava/lang/String;

.field private final playout:Lco/discord/media_engine/internal/InboundPlayout;

.field private final video:Lco/discord/media_engine/internal/InboundVideo;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lco/discord/media_engine/internal/InboundAudio;Lco/discord/media_engine/internal/InboundVideo;Lco/discord/media_engine/internal/InboundPlayout;)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audio"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    iput-object p2, p0, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    iput-object p3, p0, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    iput-object p4, p0, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/Inbound;Ljava/lang/String;Lco/discord/media_engine/internal/InboundAudio;Lco/discord/media_engine/internal/InboundVideo;Lco/discord/media_engine/internal/InboundPlayout;ILjava/lang/Object;)Lco/discord/media_engine/internal/Inbound;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lco/discord/media_engine/internal/Inbound;->copy(Ljava/lang/String;Lco/discord/media_engine/internal/InboundAudio;Lco/discord/media_engine/internal/InboundVideo;Lco/discord/media_engine/internal/InboundPlayout;)Lco/discord/media_engine/internal/Inbound;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lco/discord/media_engine/internal/InboundAudio;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    return-object v0
.end method

.method public final component3()Lco/discord/media_engine/internal/InboundVideo;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    return-object v0
.end method

.method public final component4()Lco/discord/media_engine/internal/InboundPlayout;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lco/discord/media_engine/internal/InboundAudio;Lco/discord/media_engine/internal/InboundVideo;Lco/discord/media_engine/internal/InboundPlayout;)Lco/discord/media_engine/internal/Inbound;
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audio"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/discord/media_engine/internal/Inbound;

    invoke-direct {v0, p1, p2, p3, p4}, Lco/discord/media_engine/internal/Inbound;-><init>(Ljava/lang/String;Lco/discord/media_engine/internal/InboundAudio;Lco/discord/media_engine/internal/InboundVideo;Lco/discord/media_engine/internal/InboundPlayout;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lco/discord/media_engine/internal/Inbound;

    if-eqz v0, :cond_0

    check-cast p1, Lco/discord/media_engine/internal/Inbound;

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    iget-object v1, p1, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    iget-object v1, p1, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    iget-object v1, p1, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    iget-object p1, p1, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAudio()Lco/discord/media_engine/internal/InboundAudio;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlayout()Lco/discord/media_engine/internal/InboundPlayout;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    return-object v0
.end method

.method public final getVideo()Lco/discord/media_engine/internal/InboundVideo;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundAudio;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundVideo;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lco/discord/media_engine/internal/InboundPlayout;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Inbound(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/discord/media_engine/internal/Inbound;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", audio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Inbound;->audio:Lco/discord/media_engine/internal/InboundAudio;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", video="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Inbound;->video:Lco/discord/media_engine/internal/InboundVideo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", playout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Inbound;->playout:Lco/discord/media_engine/internal/InboundPlayout;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
