.class public final Lco/discord/media_engine/internal/PacketStats;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final headerBytes:J

.field private final packets:I

.field private final paddingBytes:J

.field private final payloadBytes:J


# direct methods
.method public constructor <init>(JIJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    iput p3, p0, Lco/discord/media_engine/internal/PacketStats;->packets:I

    iput-wide p4, p0, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    iput-wide p6, p0, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/PacketStats;JIJJILjava/lang/Object;)Lco/discord/media_engine/internal/PacketStats;
    .locals 8

    move-object v0, p0

    and-int/lit8 v1, p8, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p8, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lco/discord/media_engine/internal/PacketStats;->packets:I

    goto :goto_1

    :cond_1
    move v3, p3

    :goto_1
    and-int/lit8 v4, p8, 0x4

    if-eqz v4, :cond_2

    iget-wide v4, v0, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    goto :goto_2

    :cond_2
    move-wide v4, p4

    :goto_2
    and-int/lit8 v6, p8, 0x8

    if-eqz v6, :cond_3

    iget-wide v6, v0, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    goto :goto_3

    :cond_3
    move-wide v6, p6

    :goto_3
    move-wide p1, v1

    move p3, v3

    move-wide p4, v4

    move-wide p6, v6

    invoke-virtual/range {p0 .. p7}, Lco/discord/media_engine/internal/PacketStats;->copy(JIJJ)Lco/discord/media_engine/internal/PacketStats;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    return-wide v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/PacketStats;->packets:I

    return v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    return-wide v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    return-wide v0
.end method

.method public final copy(JIJJ)Lco/discord/media_engine/internal/PacketStats;
    .locals 9

    new-instance v8, Lco/discord/media_engine/internal/PacketStats;

    move-object v0, v8

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    move-wide v6, p6

    invoke-direct/range {v0 .. v7}, Lco/discord/media_engine/internal/PacketStats;-><init>(JIJJ)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-eq p0, p1, :cond_5

    instance-of v1, p1, Lco/discord/media_engine/internal/PacketStats;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    check-cast p1, Lco/discord/media_engine/internal/PacketStats;

    iget-wide v3, p0, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    iget-wide v5, p1, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget v1, p0, Lco/discord/media_engine/internal/PacketStats;->packets:I

    iget v3, p1, Lco/discord/media_engine/internal/PacketStats;->packets:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    iget-wide v3, p0, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    iget-wide v5, p1, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_4

    iget-wide v3, p0, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    iget-wide v5, p1, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    cmp-long p1, v3, v5

    if-nez p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    return v2

    :cond_5
    :goto_4
    return v0
.end method

.method public final getHeaderBytes()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    return-wide v0
.end method

.method public final getPackets()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/PacketStats;->packets:I

    return v0
.end method

.method public final getPaddingBytes()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    return-wide v0
.end method

.method public final getPayloadBytes()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/PacketStats;->packets:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "PacketStats(headerBytes="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lco/discord/media_engine/internal/PacketStats;->headerBytes:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", packets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/PacketStats;->packets:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", paddingBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/internal/PacketStats;->paddingBytes:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", payloadBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/internal/PacketStats;->payloadBytes:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
