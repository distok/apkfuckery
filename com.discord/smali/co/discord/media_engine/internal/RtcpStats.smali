.class public final Lco/discord/media_engine/internal/RtcpStats;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final extendedHighestSequenceNumber:I

.field private final firPackets:I

.field private final fractionLost:F

.field private final nackPackets:I

.field private final nackRequests:I

.field private final packetsLost:I

.field private final pliPackets:I

.field private final uniqueNackRequests:I


# direct methods
.method public constructor <init>(FIIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    iput p2, p0, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    iput p3, p0, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    iput p4, p0, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    iput p5, p0, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    iput p6, p0, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    iput p7, p0, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    iput p8, p0, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/RtcpStats;FIIIIIIIILjava/lang/Object;)Lco/discord/media_engine/internal/RtcpStats;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget v6, v0, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget v1, v0, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    move p1, v2

    move p2, v3

    move p3, v4

    move p4, v5

    move p5, v6

    move p6, v7

    move/from16 p7, v8

    move/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lco/discord/media_engine/internal/RtcpStats;->copy(FIIIIIII)Lco/discord/media_engine/internal/RtcpStats;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    return v0
.end method

.method public final copy(FIIIIIII)Lco/discord/media_engine/internal/RtcpStats;
    .locals 10

    new-instance v9, Lco/discord/media_engine/internal/RtcpStats;

    move-object v0, v9

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lco/discord/media_engine/internal/RtcpStats;-><init>(FIIIIIII)V

    return-object v9
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_8

    instance-of v1, p1, Lco/discord/media_engine/internal/RtcpStats;

    const/4 v2, 0x0

    if-eqz v1, :cond_7

    check-cast p1, Lco/discord/media_engine/internal/RtcpStats;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    iget v3, p1, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_7

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    iget v3, p1, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_7

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    iget v3, p1, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_7

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    iget v3, p1, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_7

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    iget v3, p1, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_7

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    iget v3, p1, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_7

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    iget v3, p1, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_7

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    iget p1, p1, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    if-ne v1, p1, :cond_6

    const/4 p1, 0x1

    goto :goto_6

    :cond_6
    const/4 p1, 0x0

    :goto_6
    if-eqz p1, :cond_7

    goto :goto_7

    :cond_7
    return v2

    :cond_8
    :goto_7
    return v0
.end method

.method public final getExtendedHighestSequenceNumber()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    return v0
.end method

.method public final getFirPackets()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    return v0
.end method

.method public final getFractionLost()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    return v0
.end method

.method public final getNackPackets()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    return v0
.end method

.method public final getNackRequests()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    return v0
.end method

.method public final getPacketsLost()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    return v0
.end method

.method public final getPliPackets()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    return v0
.end method

.method public final getUniqueNackRequests()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "RtcpStats(fractionLost="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", packetsLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", extendedHighestSequenceNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->extendedHighestSequenceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", firPackets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->firPackets:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nackPackets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->nackPackets:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nackRequests="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->nackRequests:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pliPackets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->pliPackets:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", uniqueNackRequests="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/RtcpStats;->uniqueNackRequests:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
