.class public final Lco/discord/media_engine/internal/Substream;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final avgDelay:F

.field private final frameCounts:Lco/discord/media_engine/internal/FrameCounts;

.field private final height:I

.field private final isFlexFEC:Z

.field private final isRTX:Z

.field private final maxDelay:F

.field private final retransmitBitrate:F

.field private final rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

.field private final rtpStats:Lco/discord/media_engine/internal/RtpStats;

.field private final ssrc:I

.field private final totalBitrate:I

.field private final width:I


# direct methods
.method public constructor <init>(FLco/discord/media_engine/internal/FrameCounts;IZZFFLco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;III)V
    .locals 1

    const-string v0, "frameCounts"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtcpStats"

    invoke-static {p8, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtpStats"

    invoke-static {p9, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    iput-object p2, p0, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    iput p3, p0, Lco/discord/media_engine/internal/Substream;->height:I

    iput-boolean p4, p0, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    iput-boolean p5, p0, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    iput p6, p0, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    iput p7, p0, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    iput-object p8, p0, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    iput-object p9, p0, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    iput p10, p0, Lco/discord/media_engine/internal/Substream;->ssrc:I

    iput p11, p0, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    iput p12, p0, Lco/discord/media_engine/internal/Substream;->width:I

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/Substream;FLco/discord/media_engine/internal/FrameCounts;IZZFFLco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;IIIILjava/lang/Object;)Lco/discord/media_engine/internal/Substream;
    .locals 13

    move-object v0, p0

    move/from16 v1, p13

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lco/discord/media_engine/internal/Substream;->height:I

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget v11, v0, Lco/discord/media_engine/internal/Substream;->ssrc:I

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget v12, v0, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_b

    iget v1, v0, Lco/discord/media_engine/internal/Substream;->width:I

    goto :goto_b

    :cond_b
    move/from16 v1, p12

    :goto_b
    move p1, v2

    move-object p2, v3

    move/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v1

    invoke-virtual/range {p0 .. p12}, Lco/discord/media_engine/internal/Substream;->copy(FLco/discord/media_engine/internal/FrameCounts;IZZFFLco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;III)Lco/discord/media_engine/internal/Substream;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    return v0
.end method

.method public final component10()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->ssrc:I

    return v0
.end method

.method public final component11()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    return v0
.end method

.method public final component12()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->width:I

    return v0
.end method

.method public final component2()Lco/discord/media_engine/internal/FrameCounts;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->height:I

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    return v0
.end method

.method public final component6()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    return v0
.end method

.method public final component7()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    return v0
.end method

.method public final component8()Lco/discord/media_engine/internal/RtcpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    return-object v0
.end method

.method public final component9()Lco/discord/media_engine/internal/RtpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    return-object v0
.end method

.method public final copy(FLco/discord/media_engine/internal/FrameCounts;IZZFFLco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;III)Lco/discord/media_engine/internal/Substream;
    .locals 14

    const-string v0, "frameCounts"

    move-object/from16 v3, p2

    invoke-static {v3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtcpStats"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rtpStats"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/discord/media_engine/internal/Substream;

    move-object v1, v0

    move v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    invoke-direct/range {v1 .. v13}, Lco/discord/media_engine/internal/Substream;-><init>(FLco/discord/media_engine/internal/FrameCounts;IZZFFLco/discord/media_engine/internal/RtcpStats;Lco/discord/media_engine/internal/RtpStats;III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_7

    instance-of v1, p1, Lco/discord/media_engine/internal/Substream;

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    check-cast p1, Lco/discord/media_engine/internal/Substream;

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    iget v3, p1, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    iget-object v3, p1, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->height:I

    iget v3, p1, Lco/discord/media_engine/internal/Substream;->height:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    iget-boolean v3, p1, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    iget-boolean v3, p1, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_6

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    iget v3, p1, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_6

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    iget v3, p1, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    iget-object v3, p1, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    iget-object v3, p1, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->ssrc:I

    iget v3, p1, Lco/discord/media_engine/internal/Substream;->ssrc:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_6

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    iget v3, p1, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_6

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->width:I

    iget p1, p1, Lco/discord/media_engine/internal/Substream;->width:I

    if-ne v1, p1, :cond_5

    const/4 p1, 0x1

    goto :goto_5

    :cond_5
    const/4 p1, 0x0

    :goto_5
    if-eqz p1, :cond_6

    goto :goto_6

    :cond_6
    return v2

    :cond_7
    :goto_6
    return v0
.end method

.method public final getAvgDelay()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    return v0
.end method

.method public final getFrameCounts()Lco/discord/media_engine/internal/FrameCounts;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    return-object v0
.end method

.method public final getHeight()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->height:I

    return v0
.end method

.method public final getMaxDelay()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    return v0
.end method

.method public final getRetransmitBitrate()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    return v0
.end method

.method public final getRtcpStats()Lco/discord/media_engine/internal/RtcpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    return-object v0
.end method

.method public final getRtpStats()Lco/discord/media_engine/internal/RtpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    return-object v0
.end method

.method public final getSsrc()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->ssrc:I

    return v0
.end method

.method public final getTotalBitrate()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->width:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget v0, p0, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/discord/media_engine/internal/FrameCounts;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->height:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    move v3, v1

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    const/16 v3, 0x1f

    invoke-static {v1, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    invoke-static {v1, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lco/discord/media_engine/internal/RtcpStats;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lco/discord/media_engine/internal/RtpStats;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->ssrc:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->width:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final isFlexFEC()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    return v0
.end method

.method public final isRTX()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Substream(avgDelay="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->avgDelay:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", frameCounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->frameCounts:Lco/discord/media_engine/internal/FrameCounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isFlexFEC="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lco/discord/media_engine/internal/Substream;->isFlexFEC:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isRTX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lco/discord/media_engine/internal/Substream;->isRTX:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", maxDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->maxDelay:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", retransmitBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->retransmitBitrate:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", rtcpStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->rtcpStats:Lco/discord/media_engine/internal/RtcpStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rtpStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Substream;->rtpStats:Lco/discord/media_engine/internal/RtpStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ssrc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", totalBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->totalBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Substream;->width:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
