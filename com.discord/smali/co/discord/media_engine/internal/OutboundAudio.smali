.class public final Lco/discord/media_engine/internal/OutboundAudio;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final audioLevel:I

.field private final bytesSent:I

.field private final codecName:Ljava/lang/String;

.field private final codecPayloadType:I

.field private final delayMedian:F

.field private final delayStd:F

.field private final echoReturnLoss:F

.field private final echoReturnLossEnchancement:F

.field private final extSeqNum:I

.field private final fractionLost:F

.field private final framesCaptured:J

.field private final framesRendered:J

.field private final jitter:F

.field private final noiseCancellerIsEnabled:Z

.field private final noiseCancellerProcessTime:J

.field private final packetsLost:I

.field private final packetsSent:I

.field private final residualEchoLikelihood:F

.field private final residualEchoLikelihoodRecentMax:F

.field private final rtt:I

.field private final speaking:I

.field private final ssrc:I

.field private final typingNoiseDetected:Z

.field private final voiceActivityDetectorIsEnabled:Z

.field private final voiceActivityDetectorProcessTime:J


# direct methods
.method public constructor <init>(IILjava/lang/String;IFFFFIFFIIFFIIIZJJZJZJ)V
    .locals 3

    move-object v0, p0

    move-object v1, p3

    const-string v2, "codecName"

    invoke-static {p3, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v2, p1

    iput v2, v0, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    move v2, p2

    iput v2, v0, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    iput-object v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    move v1, p4

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    move v1, p5

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    move v1, p6

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    move v1, p7

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    move v1, p8

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    move v1, p9

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    move v1, p10

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    move v1, p11

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    move v1, p12

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    move/from16 v1, p13

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    move/from16 v1, p14

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    move/from16 v1, p15

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    move/from16 v1, p16

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    move/from16 v1, p17

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    move/from16 v1, p18

    iput v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    move/from16 v1, p19

    iput-boolean v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    move-wide/from16 v1, p20

    iput-wide v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    move-wide/from16 v1, p22

    iput-wide v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    move/from16 v1, p24

    iput-boolean v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    move-wide/from16 v1, p25

    iput-wide v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    move/from16 v1, p27

    iput-boolean v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    move-wide/from16 v1, p28

    iput-wide v1, v0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/OutboundAudio;IILjava/lang/String;IFFFFIFFIIFFIIIZJJZJZJILjava/lang/Object;)Lco/discord/media_engine/internal/OutboundAudio;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p30

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    goto :goto_0

    :cond_0
    move/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget v6, v0, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget v10, v0, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget v11, v0, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget v12, v0, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget v13, v0, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    goto :goto_b

    :cond_b
    move/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget v14, v0, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget v15, v0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    goto :goto_d

    :cond_d
    move/from16 v15, p14

    :goto_d
    move/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget v15, v0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    goto :goto_e

    :cond_e
    move/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move/from16 p15, v15

    if-eqz v16, :cond_f

    iget v15, v0, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    goto :goto_f

    :cond_f
    move/from16 v15, p16

    :goto_f
    const/high16 v16, 0x10000

    and-int v16, v1, v16

    move/from16 p16, v15

    if-eqz v16, :cond_10

    iget v15, v0, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    goto :goto_10

    :cond_10
    move/from16 v15, p17

    :goto_10
    const/high16 v16, 0x20000

    and-int v16, v1, v16

    move/from16 p17, v15

    if-eqz v16, :cond_11

    iget v15, v0, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    goto :goto_11

    :cond_11
    move/from16 v15, p18

    :goto_11
    const/high16 v16, 0x40000

    and-int v16, v1, v16

    move/from16 p18, v15

    if-eqz v16, :cond_12

    iget-boolean v15, v0, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    goto :goto_12

    :cond_12
    move/from16 v15, p19

    :goto_12
    const/high16 v16, 0x80000

    and-int v16, v1, v16

    move/from16 p13, v14

    move/from16 p19, v15

    if-eqz v16, :cond_13

    iget-wide v14, v0, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    goto :goto_13

    :cond_13
    move-wide/from16 v14, p20

    :goto_13
    const/high16 v16, 0x100000

    and-int v16, v1, v16

    move-wide/from16 p20, v14

    if-eqz v16, :cond_14

    iget-wide v14, v0, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    goto :goto_14

    :cond_14
    move-wide/from16 v14, p22

    :goto_14
    const/high16 v16, 0x200000

    and-int v16, v1, v16

    move-wide/from16 p22, v14

    if-eqz v16, :cond_15

    iget-boolean v14, v0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    goto :goto_15

    :cond_15
    move/from16 v14, p24

    :goto_15
    const/high16 v15, 0x400000

    and-int/2addr v15, v1

    move/from16 p24, v14

    if-eqz v15, :cond_16

    iget-wide v14, v0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    goto :goto_16

    :cond_16
    move-wide/from16 v14, p25

    :goto_16
    const/high16 v16, 0x800000

    and-int v16, v1, v16

    move-wide/from16 p25, v14

    if-eqz v16, :cond_17

    iget-boolean v14, v0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    goto :goto_17

    :cond_17
    move/from16 v14, p27

    :goto_17
    const/high16 v15, 0x1000000

    and-int/2addr v1, v15

    move/from16 p27, v14

    if-eqz v1, :cond_18

    iget-wide v14, v0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    goto :goto_18

    :cond_18
    move-wide/from16 v14, p28

    :goto_18
    move/from16 p1, v2

    move/from16 p2, v3

    move-object/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v13

    move-wide/from16 p28, v14

    invoke-virtual/range {p0 .. p29}, Lco/discord/media_engine/internal/OutboundAudio;->copy(IILjava/lang/String;IFFFFIFFIIFFIIIZJJZJZJ)Lco/discord/media_engine/internal/OutboundAudio;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    return v0
.end method

.method public final component10()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    return v0
.end method

.method public final component11()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    return v0
.end method

.method public final component12()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    return v0
.end method

.method public final component13()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    return v0
.end method

.method public final component14()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    return v0
.end method

.method public final component15()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    return v0
.end method

.method public final component16()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    return v0
.end method

.method public final component17()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    return v0
.end method

.method public final component18()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    return v0
.end method

.method public final component19()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    return v0
.end method

.method public final component20()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    return-wide v0
.end method

.method public final component21()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    return-wide v0
.end method

.method public final component22()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    return v0
.end method

.method public final component23()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    return-wide v0
.end method

.method public final component24()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    return v0
.end method

.method public final component25()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    return-wide v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    return v0
.end method

.method public final component5()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    return v0
.end method

.method public final component6()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    return v0
.end method

.method public final component7()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    return v0
.end method

.method public final component8()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    return v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    return v0
.end method

.method public final copy(IILjava/lang/String;IFFFFIFFIIFFIIIZJJZJZJ)Lco/discord/media_engine/internal/OutboundAudio;
    .locals 31

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    move/from16 v15, p15

    move/from16 v16, p16

    move/from16 v17, p17

    move/from16 v18, p18

    move/from16 v19, p19

    move-wide/from16 v20, p20

    move-wide/from16 v22, p22

    move/from16 v24, p24

    move-wide/from16 v25, p25

    move/from16 v27, p27

    move-wide/from16 v28, p28

    const-string v0, "codecName"

    move-object/from16 v1, p3

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v30, Lco/discord/media_engine/internal/OutboundAudio;

    move-object/from16 v0, v30

    move/from16 v1, p1

    invoke-direct/range {v0 .. v29}, Lco/discord/media_engine/internal/OutboundAudio;-><init>(IILjava/lang/String;IFFFFIFFIIFFIIIZJJZJZJ)V

    return-object v30
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-eq p0, p1, :cond_11

    instance-of v1, p1, Lco/discord/media_engine/internal/OutboundAudio;

    const/4 v2, 0x0

    if-eqz v1, :cond_10

    check-cast p1, Lco/discord/media_engine/internal/OutboundAudio;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_10

    iget-object v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    iget-object v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    if-ne v1, v3, :cond_6

    const/4 v1, 0x1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    if-ne v1, v3, :cond_7

    const/4 v1, 0x1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    if-eqz v1, :cond_10

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    iget v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    if-ne v1, v3, :cond_8

    const/4 v1, 0x1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    if-eqz v1, :cond_10

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    iget-boolean v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    if-ne v1, v3, :cond_9

    const/4 v1, 0x1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    if-eqz v1, :cond_10

    iget-wide v3, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    iget-wide v5, p1, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_a

    const/4 v1, 0x1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    if-eqz v1, :cond_10

    iget-wide v3, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    iget-wide v5, p1, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_b

    const/4 v1, 0x1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    if-eqz v1, :cond_10

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    iget-boolean v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    if-ne v1, v3, :cond_c

    const/4 v1, 0x1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    if-eqz v1, :cond_10

    iget-wide v3, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    iget-wide v5, p1, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_d

    const/4 v1, 0x1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    if-eqz v1, :cond_10

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    iget-boolean v3, p1, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    if-ne v1, v3, :cond_e

    const/4 v1, 0x1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    if-eqz v1, :cond_10

    iget-wide v3, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    iget-wide v5, p1, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    cmp-long p1, v3, v5

    if-nez p1, :cond_f

    const/4 p1, 0x1

    goto :goto_f

    :cond_f
    const/4 p1, 0x0

    :goto_f
    if-eqz p1, :cond_10

    goto :goto_10

    :cond_10
    return v2

    :cond_11
    :goto_10
    return v0
.end method

.method public final getAudioLevel()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    return v0
.end method

.method public final getBytesSent()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    return v0
.end method

.method public final getCodecName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCodecPayloadType()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    return v0
.end method

.method public final getDelayMedian()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    return v0
.end method

.method public final getDelayStd()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    return v0
.end method

.method public final getEchoReturnLoss()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    return v0
.end method

.method public final getEchoReturnLossEnchancement()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    return v0
.end method

.method public final getExtSeqNum()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    return v0
.end method

.method public final getFractionLost()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    return v0
.end method

.method public final getFramesCaptured()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    return-wide v0
.end method

.method public final getFramesRendered()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    return-wide v0
.end method

.method public final getJitter()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    return v0
.end method

.method public final getNoiseCancellerIsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    return v0
.end method

.method public final getNoiseCancellerProcessTime()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    return-wide v0
.end method

.method public final getPacketsLost()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    return v0
.end method

.method public final getPacketsSent()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    return v0
.end method

.method public final getResidualEchoLikelihood()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    return v0
.end method

.method public final getResidualEchoLikelihoodRecentMax()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    return v0
.end method

.method public final getRtt()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    return v0
.end method

.method public final getSpeaking()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    return v0
.end method

.method public final getSsrc()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    return v0
.end method

.method public final getTypingNoiseDetected()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    return v0
.end method

.method public final getVoiceActivityDetectorIsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    return v0
.end method

.method public final getVoiceActivityDetectorProcessTime()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 5

    iget v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    invoke-static {v3, v4}, Ld;->a(J)I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    invoke-static {v3, v4}, Ld;->a(J)I

    move-result v0

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    invoke-static {v3, v4}, Ld;->a(J)I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v0, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    move v2, v0

    :goto_1
    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "OutboundAudio(audioLevel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->audioLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bytesSent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->bytesSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", codecName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", codecPayloadType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->codecPayloadType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", delayMedian="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayMedian:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", delayStd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->delayStd:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", echoReturnLoss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLoss:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", echoReturnLossEnchancement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->echoReturnLossEnchancement:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", extSeqNum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->extSeqNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", fractionLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", jitter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->jitter:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", packetsLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", packetsSent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->packetsSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", residualEchoLikelihood="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihood:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", residualEchoLikelihoodRecentMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->residualEchoLikelihoodRecentMax:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", rtt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->rtt:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", speaking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->speaking:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", ssrc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", typingNoiseDetected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->typingNoiseDetected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", framesCaptured="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesCaptured:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", framesRendered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->framesRendered:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", noiseCancellerIsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerIsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", noiseCancellerProcessTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->noiseCancellerProcessTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", voiceActivityDetectorIsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorIsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", voiceActivityDetectorProcessTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/internal/OutboundAudio;->voiceActivityDetectorProcessTime:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
