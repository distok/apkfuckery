.class public final Lco/discord/media_engine/internal/Transport;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final decryptionFailures:I

.field private final localAddress:Ljava/lang/String;

.field private final maxPaddingBitrate:F

.field private final pacerDelay:F

.field private final receiverReports:[Lco/discord/media_engine/ReceiverReport;

.field private final rtt:I

.field private final sendBandwidth:I


# direct methods
.method public constructor <init>(IFFIILjava/lang/String;[Lco/discord/media_engine/ReceiverReport;)V
    .locals 1

    const-string v0, "localAddress"

    invoke-static {p6, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    iput p2, p0, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    iput p3, p0, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    iput p4, p0, Lco/discord/media_engine/internal/Transport;->rtt:I

    iput p5, p0, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    iput-object p6, p0, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    iput-object p7, p0, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/Transport;IFFIILjava/lang/String;[Lco/discord/media_engine/ReceiverReport;ILjava/lang/Object;)Lco/discord/media_engine/internal/Transport;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget p1, p0, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget p2, p0, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lco/discord/media_engine/internal/Transport;->rtt:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lco/discord/media_engine/internal/Transport;->copy(IFFIILjava/lang/String;[Lco/discord/media_engine/ReceiverReport;)Lco/discord/media_engine/internal/Transport;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    return v0
.end method

.method public final component2()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    return v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->rtt:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    return v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()[Lco/discord/media_engine/ReceiverReport;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    return-object v0
.end method

.method public final copy(IFFIILjava/lang/String;[Lco/discord/media_engine/ReceiverReport;)Lco/discord/media_engine/internal/Transport;
    .locals 9

    const-string v0, "localAddress"

    move-object v7, p6

    invoke-static {p6, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/discord/media_engine/internal/Transport;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lco/discord/media_engine/internal/Transport;-><init>(IFFIILjava/lang/String;[Lco/discord/media_engine/ReceiverReport;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_4

    instance-of v1, p1, Lco/discord/media_engine/internal/Transport;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    check-cast p1, Lco/discord/media_engine/internal/Transport;

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    iget v3, p1, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    iget v3, p1, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    iget v3, p1, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->rtt:I

    iget v3, p1, Lco/discord/media_engine/internal/Transport;->rtt:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    iget v3, p1, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_3

    iget-object v1, p0, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    iget-object v3, p1, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    iget-object p1, p1, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    return v2

    :cond_4
    :goto_3
    return v0
.end method

.method public final getDecryptionFailures()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    return v0
.end method

.method public final getLocalAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final getMaxPaddingBitrate()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    return v0
.end method

.method public final getPacerDelay()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    return v0
.end method

.method public final getReceiverReports()[Lco/discord/media_engine/ReceiverReport;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    return-object v0
.end method

.method public final getRtt()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->rtt:I

    return v0
.end method

.method public final getSendBandwidth()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->rtt:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Transport(decryptionFailures="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->decryptionFailures:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", maxPaddingBitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->maxPaddingBitrate:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", pacerDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->pacerDelay:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", rtt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->rtt:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", sendBandwidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Transport;->sendBandwidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", localAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Transport;->localAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", receiverReports="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/internal/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
