.class public final Lco/discord/media_engine/internal/Screenshare;
.super Ljava/lang/Object;
.source "NativeStatistics.kt"


# instance fields
.field private final capturedFramesCount:I

.field private final capturedFramesDropped:I

.field private final capturedFramesMean:F

.field private final capturedFramesStdev:F

.field private final hybridDxgiFrames:I

.field private final hybridGdiFrames:I

.field private final hybridVideohookFrames:I

.field private final screenshareFrames:I

.field private final videohookBackend:I

.field private final videohookFrames:I


# direct methods
.method public constructor <init>(IIIIIIIIFF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    iput p2, p0, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    iput p3, p0, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    iput p4, p0, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    iput p5, p0, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    iput p6, p0, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    iput p7, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    iput p8, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    iput p9, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    iput p10, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/internal/Screenshare;IIIIIIIIFFILjava/lang/Object;)Lco/discord/media_engine/internal/Screenshare;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget v6, v0, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget v10, v0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget v1, v0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    goto :goto_9

    :cond_9
    move/from16 v1, p10

    :goto_9
    move p1, v2

    move p2, v3

    move p3, v4

    move p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lco/discord/media_engine/internal/Screenshare;->copy(IIIIIIIIFF)Lco/discord/media_engine/internal/Screenshare;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    return v0
.end method

.method public final component10()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    return v0
.end method

.method public final component9()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    return v0
.end method

.method public final copy(IIIIIIIIFF)Lco/discord/media_engine/internal/Screenshare;
    .locals 12

    new-instance v11, Lco/discord/media_engine/internal/Screenshare;

    move-object v0, v11

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lco/discord/media_engine/internal/Screenshare;-><init>(IIIIIIIIFF)V

    return-object v11
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_9

    instance-of v1, p1, Lco/discord/media_engine/internal/Screenshare;

    const/4 v2, 0x0

    if-eqz v1, :cond_8

    check-cast p1, Lco/discord/media_engine/internal/Screenshare;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    if-ne v1, v3, :cond_6

    const/4 v1, 0x1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    if-ne v1, v3, :cond_7

    const/4 v1, 0x1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    if-eqz v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    iget v3, p1, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_8

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    iget p1, p1, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    invoke-static {v1, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_8

    goto :goto_8

    :cond_8
    return v2

    :cond_9
    :goto_8
    return v0
.end method

.method public final getCapturedFramesCount()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    return v0
.end method

.method public final getCapturedFramesDropped()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    return v0
.end method

.method public final getCapturedFramesMean()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    return v0
.end method

.method public final getCapturedFramesStdev()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    return v0
.end method

.method public final getHybridDxgiFrames()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    return v0
.end method

.method public final getHybridGdiFrames()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    return v0
.end method

.method public final getHybridVideohookFrames()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    return v0
.end method

.method public final getScreenshareFrames()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    return v0
.end method

.method public final getVideohookBackend()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    return v0
.end method

.method public final getVideohookFrames()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Screenshare(videohookFrames="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->videohookFrames:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", screenshareFrames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->screenshareFrames:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", hybridDxgiFrames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridDxgiFrames:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", hybridGdiFrames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridGdiFrames:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", hybridVideohookFrames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->hybridVideohookFrames:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", videohookBackend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->videohookBackend:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", capturedFramesDropped="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesDropped:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", capturedFramesCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", capturedFramesMean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesMean:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", capturedFramesStdev="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/internal/Screenshare;->capturedFramesStdev:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
