.class public final Lco/discord/media_engine/Transport;
.super Ljava/lang/Object;
.source "Statistics.kt"


# instance fields
.field private final availableOutgoingBitrate:I

.field private final bytesReceived:J

.field private final bytesSent:J

.field private final decryptionFailures:Ljava/lang/Integer;

.field private final localAddress:Ljava/lang/String;

.field private final ping:I

.field private final receiverReports:[Lco/discord/media_engine/ReceiverReport;


# direct methods
.method public constructor <init>(IJJILjava/lang/Integer;Ljava/lang/String;[Lco/discord/media_engine/ReceiverReport;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    iput-wide p2, p0, Lco/discord/media_engine/Transport;->bytesReceived:J

    iput-wide p4, p0, Lco/discord/media_engine/Transport;->bytesSent:J

    iput p6, p0, Lco/discord/media_engine/Transport;->ping:I

    iput-object p7, p0, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    iput-object p8, p0, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    iput-object p9, p0, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/Transport;IJJILjava/lang/Integer;Ljava/lang/String;[Lco/discord/media_engine/ReceiverReport;ILjava/lang/Object;)Lco/discord/media_engine/Transport;
    .locals 10

    move-object v0, p0

    and-int/lit8 v1, p10, 0x1

    if-eqz v1, :cond_0

    iget v1, v0, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    goto :goto_0

    :cond_0
    move v1, p1

    :goto_0
    and-int/lit8 v2, p10, 0x2

    if-eqz v2, :cond_1

    iget-wide v2, v0, Lco/discord/media_engine/Transport;->bytesReceived:J

    goto :goto_1

    :cond_1
    move-wide v2, p2

    :goto_1
    and-int/lit8 v4, p10, 0x4

    if-eqz v4, :cond_2

    iget-wide v4, v0, Lco/discord/media_engine/Transport;->bytesSent:J

    goto :goto_2

    :cond_2
    move-wide v4, p4

    :goto_2
    and-int/lit8 v6, p10, 0x8

    if-eqz v6, :cond_3

    iget v6, v0, Lco/discord/media_engine/Transport;->ping:I

    goto :goto_3

    :cond_3
    move/from16 v6, p6

    :goto_3
    and-int/lit8 v7, p10, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p7

    :goto_4
    and-int/lit8 v8, p10, 0x20

    if-eqz v8, :cond_5

    iget-object v8, v0, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p8

    :goto_5
    and-int/lit8 v9, p10, 0x40

    if-eqz v9, :cond_6

    iget-object v9, v0, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    goto :goto_6

    :cond_6
    move-object/from16 v9, p9

    :goto_6
    move p1, v1

    move-wide p2, v2

    move-wide p4, v4

    move/from16 p6, v6

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    move-object/from16 p9, v9

    invoke-virtual/range {p0 .. p9}, Lco/discord/media_engine/Transport;->copy(IJJILjava/lang/Integer;Ljava/lang/String;[Lco/discord/media_engine/ReceiverReport;)Lco/discord/media_engine/Transport;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    return v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/Transport;->bytesReceived:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/Transport;->bytesSent:J

    return-wide v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Transport;->ping:I

    return v0
.end method

.method public final component5()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()[Lco/discord/media_engine/ReceiverReport;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    return-object v0
.end method

.method public final copy(IJJILjava/lang/Integer;Ljava/lang/String;[Lco/discord/media_engine/ReceiverReport;)Lco/discord/media_engine/Transport;
    .locals 11

    new-instance v10, Lco/discord/media_engine/Transport;

    move-object v0, v10

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lco/discord/media_engine/Transport;-><init>(IJJILjava/lang/Integer;Ljava/lang/String;[Lco/discord/media_engine/ReceiverReport;)V

    return-object v10
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-eq p0, p1, :cond_5

    instance-of v1, p1, Lco/discord/media_engine/Transport;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    check-cast p1, Lco/discord/media_engine/Transport;

    iget v1, p0, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    iget v3, p1, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget-wide v3, p0, Lco/discord/media_engine/Transport;->bytesReceived:J

    iget-wide v5, p1, Lco/discord/media_engine/Transport;->bytesReceived:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    iget-wide v3, p0, Lco/discord/media_engine/Transport;->bytesSent:J

    iget-wide v5, p1, Lco/discord/media_engine/Transport;->bytesSent:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_4

    iget v1, p0, Lco/discord/media_engine/Transport;->ping:I

    iget v3, p1, Lco/discord/media_engine/Transport;->ping:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_4

    iget-object v1, p0, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    iget-object v3, p1, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    iget-object p1, p1, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    return v2

    :cond_5
    :goto_4
    return v0
.end method

.method public final getAvailableOutgoingBitrate()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    return v0
.end method

.method public final getBytesReceived()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/Transport;->bytesReceived:J

    return-wide v0
.end method

.method public final getBytesSent()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/Transport;->bytesSent:J

    return-wide v0
.end method

.method public final getDecryptionFailures()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getLocalAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final getPing()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Transport;->ping:I

    return v0
.end method

.method public final getReceiverReports()[Lco/discord/media_engine/ReceiverReport;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lco/discord/media_engine/Transport;->bytesReceived:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lco/discord/media_engine/Transport;->bytesSent:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/Transport;->ping:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    if-eqz v1, :cond_2

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Transport(availableOutgoingBitrate="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/Transport;->availableOutgoingBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bytesReceived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/Transport;->bytesReceived:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", bytesSent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/Transport;->bytesSent:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", ping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/Transport;->ping:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decryptionFailures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/Transport;->decryptionFailures:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", localAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/Transport;->localAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", receiverReports="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/Transport;->receiverReports:[Lco/discord/media_engine/ReceiverReport;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
