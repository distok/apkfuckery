.class public final Lco/discord/media_engine/ReceiverReport;
.super Ljava/lang/Object;
.source "Statistics.kt"


# instance fields
.field private final bitrate:F

.field private final fractionLost:F

.field private final id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;FF)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    iput p2, p0, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    iput p3, p0, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/ReceiverReport;Ljava/lang/String;FFILjava/lang/Object;)Lco/discord/media_engine/ReceiverReport;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lco/discord/media_engine/ReceiverReport;->copy(Ljava/lang/String;FF)Lco/discord/media_engine/ReceiverReport;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    return v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    return v0
.end method

.method public final copy(Ljava/lang/String;FF)Lco/discord/media_engine/ReceiverReport;
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/discord/media_engine/ReceiverReport;

    invoke-direct {v0, p1, p2, p3}, Lco/discord/media_engine/ReceiverReport;-><init>(Ljava/lang/String;FF)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lco/discord/media_engine/ReceiverReport;

    if-eqz v0, :cond_0

    check-cast p1, Lco/discord/media_engine/ReceiverReport;

    iget-object v0, p0, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    iget-object v1, p1, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    iget v1, p1, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    iget p1, p1, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBitrate()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    return v0
.end method

.method public final getFractionLost()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    return v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v1, p0, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ReceiverReport(id="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/discord/media_engine/ReceiverReport;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", bitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/ReceiverReport;->bitrate:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", fractionLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/ReceiverReport;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
