.class public final Lco/discord/media_engine/VoiceQuality;
.super Ljava/lang/Object;
.source "VoiceQuality.kt"


# instance fields
.field private _duration:Lco/discord/media_engine/Duration;

.field private _inboundStats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lco/discord/media_engine/InboundAudio;",
            ">;"
        }
    .end annotation
.end field

.field private _outboundStats:Lco/discord/media_engine/OutboundAudio;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    new-instance v0, Lco/discord/media_engine/OutboundAudio;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lco/discord/media_engine/OutboundAudio;-><init>(II)V

    iput-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_outboundStats:Lco/discord/media_engine/OutboundAudio;

    new-instance v0, Lco/discord/media_engine/Duration;

    invoke-direct {v0, v1, v1, v1, v1}, Lco/discord/media_engine/Duration;-><init>(IIII)V

    iput-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    return-void
.end method


# virtual methods
.method public final getBufferStats(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/discord/media_engine/InboundAudio;

    if-nez v2, :cond_1

    invoke-virtual {v3}, Lco/discord/media_engine/InboundAudio;->getBufferStats()Lco/discord/media_engine/InboundBufferStats;

    move-result-object v2

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lco/discord/media_engine/InboundBufferStats;->getAudioJitterBuffer()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lco/discord/media_engine/InboundAudio;->getBufferStats()Lco/discord/media_engine/InboundBufferStats;

    move-result-object v5

    invoke-virtual {v5}, Lco/discord/media_engine/InboundBufferStats;->getAudioJitterBuffer()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lco/discord/media_engine/PlayoutMetric;->getP75()F

    move-result v5

    invoke-virtual {v4}, Lco/discord/media_engine/PlayoutMetric;->getP75()F

    move-result v4

    cmpl-float v4, v5, v4

    if-lez v4, :cond_0

    invoke-virtual {v3}, Lco/discord/media_engine/InboundAudio;->getBufferStats()Lco/discord/media_engine/InboundBufferStats;

    move-result-object v2

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lco/discord/media_engine/InboundBufferStats;->getAudioJitterBuffer()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    :goto_1
    const-string v3, "audio_jitter_buffer"

    invoke-static {v3, v0, p1}, Lco/discord/media_engine/VoiceQualityKt;->access$explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lco/discord/media_engine/InboundBufferStats;->getAudioJitterTarget()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    :goto_2
    const-string v3, "audio_jitter_target"

    invoke-static {v3, v0, p1}, Lco/discord/media_engine/VoiceQualityKt;->access$explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lco/discord/media_engine/InboundBufferStats;->getAudioJitterDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    goto :goto_3

    :cond_5
    move-object v0, v1

    :goto_3
    const-string v3, "audio_jitter_delay"

    invoke-static {v3, v0, p1}, Lco/discord/media_engine/VoiceQualityKt;->access$explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lco/discord/media_engine/InboundBufferStats;->getRelativeReceptionDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v0

    goto :goto_4

    :cond_6
    move-object v0, v1

    :goto_4
    const-string v3, "relative_reception_delay"

    invoke-static {v3, v0, p1}, Lco/discord/media_engine/VoiceQualityKt;->access$explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lco/discord/media_engine/InboundBufferStats;->getRelativePlayoutDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v1

    :cond_7
    const-string v0, "relative_playout_delay"

    invoke-static {v0, v1, p1}, Lco/discord/media_engine/VoiceQualityKt;->access$explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V

    return-void
.end method

.method public final getDurationStats(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v0}, Lco/discord/media_engine/Duration;->getListening()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "duration_listening"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v0}, Lco/discord/media_engine/Duration;->getSpeaking()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "duration_speaking"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v0}, Lco/discord/media_engine/Duration;->getParticipation()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "duration_participation"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v0}, Lco/discord/media_engine/Duration;->getConnected()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "duration_connected"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final getFrameOpStats(Ljava/util/Map;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, v1

    move-object v3, v2

    move-object v4, v3

    move-object v5, v4

    move-object v6, v5

    move-object v7, v6

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/discord/media_engine/InboundAudio;

    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object v9

    invoke-virtual {v9}, Lco/discord/media_engine/InboundFrameOpStats;->getSilent()Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x0

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v1, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object v9

    invoke-virtual {v9}, Lco/discord/media_engine/InboundFrameOpStats;->getNormal()Ljava/lang/Integer;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_4
    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object v9

    invoke-virtual {v9}, Lco/discord/media_engine/InboundFrameOpStats;->getMerged()Ljava/lang/Integer;

    move-result-object v9

    if-eqz v9, :cond_6

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    add-int/2addr v3, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_6
    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object v9

    invoke-virtual {v9}, Lco/discord/media_engine/InboundFrameOpStats;->getExpanded()Ljava/lang/Integer;

    move-result-object v9

    if-eqz v9, :cond_8

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_4

    :cond_7
    const/4 v4, 0x0

    :goto_4
    add-int/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :cond_8
    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object v9

    invoke-virtual {v9}, Lco/discord/media_engine/InboundFrameOpStats;->getAccelerated()Ljava/lang/Integer;

    move-result-object v9

    if-eqz v9, :cond_a

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    if-eqz v5, :cond_9

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_5

    :cond_9
    const/4 v5, 0x0

    :goto_5
    add-int/2addr v5, v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :cond_a
    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object v9

    invoke-virtual {v9}, Lco/discord/media_engine/InboundFrameOpStats;->getPreemptiveExpanded()Ljava/lang/Integer;

    move-result-object v9

    if-eqz v9, :cond_c

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    if-eqz v6, :cond_b

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_6

    :cond_b
    const/4 v6, 0x0

    :goto_6
    add-int/2addr v6, v9

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    :cond_c
    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object v8

    invoke-virtual {v8}, Lco/discord/media_engine/InboundFrameOpStats;->getCng()Ljava/lang/Integer;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v8

    if-eqz v7, :cond_d

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v10

    :cond_d
    add-int/2addr v10, v8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/16 :goto_0

    :cond_e
    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "frame_op_silent"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_f
    if-eqz v2, :cond_10

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "frame_op_normal"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_10
    if-eqz v3, :cond_11

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "frame_op_merged"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_11
    if-eqz v4, :cond_12

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "frame_op_expanded"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_12
    if-eqz v5, :cond_13

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "frame_op_accelerated"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_13
    if-eqz v6, :cond_14

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "frame_op_preemptive_expanded"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_14
    if-eqz v7, :cond_15

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "frame_op_cng"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_15
    return-void
.end method

.method public final getMosStats(Ljava/util/Map;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "result"

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const/4 v5, 0x2

    aput-object v3, v1, v5

    const/4 v6, 0x3

    aput-object v3, v1, v6

    const/4 v7, 0x4

    aput-object v3, v1, v7

    move-object/from16 v8, p0

    iget-object v9, v8, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lco/discord/media_engine/InboundAudio;

    invoke-virtual {v13}, Lco/discord/media_engine/InboundAudio;->getMosSum()D

    move-result-wide v14

    add-double/2addr v10, v14

    invoke-virtual {v13}, Lco/discord/media_engine/InboundAudio;->getMosCount()I

    move-result v14

    add-int/2addr v12, v14

    const/4 v14, 0x0

    :goto_0
    if-gt v14, v7, :cond_0

    aget-object v15, v1, v14

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-virtual {v13}, Lco/discord/media_engine/InboundAudio;->getMosBuckets()[Ljava/lang/Integer;

    move-result-object v16

    aget-object v16, v16, v14

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    add-int v16, v16, v15

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    if-lez v12, :cond_2

    int-to-double v2, v12

    div-double/2addr v10, v2

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    :cond_2
    const-string v2, "mos_mean"

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v2, v1, v4

    const-string v3, "mos_1"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v2, v1, v5

    const-string v3, "mos_2"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v2, v1, v6

    const-string v3, "mos_3"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v1, v1, v7

    const-string v2, "mos_4"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final getPacketStats(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/discord/media_engine/InboundAudio;

    invoke-virtual {v3}, Lco/discord/media_engine/InboundAudio;->getPacketsReceived()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {v3}, Lco/discord/media_engine/InboundAudio;->getPacketsLost()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_outboundStats:Lco/discord/media_engine/OutboundAudio;

    invoke-virtual {v0}, Lco/discord/media_engine/OutboundAudio;->getPacketsSent()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v3, "packets_sent"

    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lco/discord/media_engine/VoiceQuality;->_outboundStats:Lco/discord/media_engine/OutboundAudio;

    invoke-virtual {v0}, Lco/discord/media_engine/OutboundAudio;->getPacketsLost()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v3, "packets_sent_lost"

    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "packets_received"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "packets_received_lost"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final update(Lco/discord/media_engine/Stats;)V
    .locals 36

    move-object/from16 v0, p0

    const-string/jumbo v1, "stats"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v1}, Lco/discord/media_engine/Duration;->getConnected()I

    move-result v3

    const/4 v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {v1, v3}, Lco/discord/media_engine/Duration;->setConnected(I)V

    iget-object v1, v0, Lco/discord/media_engine/VoiceQuality;->_outboundStats:Lco/discord/media_engine/OutboundAudio;

    invoke-virtual {v1}, Lco/discord/media_engine/OutboundAudio;->getPacketsSent()I

    move-result v1

    iget-object v3, v0, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/discord/media_engine/InboundAudio;

    invoke-virtual {v8}, Lco/discord/media_engine/InboundAudio;->getPacketsReceived()I

    move-result v8

    add-int/2addr v7, v8

    goto :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lco/discord/media_engine/Stats;->getOutboundRtpAudio()Lco/discord/media_engine/OutboundRtpAudio;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v8, Lco/discord/media_engine/OutboundAudio;

    invoke-virtual {v3}, Lco/discord/media_engine/OutboundRtpAudio;->getPacketsSent()I

    move-result v9

    invoke-virtual {v3}, Lco/discord/media_engine/OutboundRtpAudio;->getPacketsLost()I

    move-result v3

    invoke-direct {v8, v9, v3}, Lco/discord/media_engine/OutboundAudio;-><init>(II)V

    goto :goto_1

    :cond_1
    new-instance v8, Lco/discord/media_engine/OutboundAudio;

    invoke-direct {v8, v5, v5}, Lco/discord/media_engine/OutboundAudio;-><init>(II)V

    :goto_1
    iput-object v8, v0, Lco/discord/media_engine/VoiceQuality;->_outboundStats:Lco/discord/media_engine/OutboundAudio;

    invoke-virtual/range {p1 .. p1}, Lco/discord/media_engine/Stats;->getInboundRtpAudio()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/discord/media_engine/InboundRtpAudio;

    invoke-virtual/range {p1 .. p1}, Lco/discord/media_engine/Stats;->getTransport()Lco/discord/media_engine/Transport;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-virtual {v10}, Lco/discord/media_engine/Transport;->getPing()I

    move-result v10

    goto :goto_3

    :cond_2
    const/4 v10, 0x0

    :goto_3
    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getPacketsReceived()I

    move-result v12

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getPacketsLost()I

    move-result v13

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getJitterBuffer()I

    move-result v11

    new-instance v20, Lco/discord/media_engine/InboundBufferStats;

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getAudioJitterBuffer()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v15

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getAudioJitterTarget()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v16

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getAudioJitterDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v17

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getRelativeReceptionDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v18

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getRelativePlayoutDelay()Lco/discord/media_engine/PlayoutMetric;

    move-result-object v19

    move-object/from16 v14, v20

    invoke-direct/range {v14 .. v19}, Lco/discord/media_engine/InboundBufferStats;-><init>(Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)V

    new-instance v29, Lco/discord/media_engine/InboundFrameOpStats;

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getOpSilence()Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getOpNormal()Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getOpMerge()Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getOpExpand()Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getOpAccelerate()Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getOpPreemptiveExpand()Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual {v8}, Lco/discord/media_engine/InboundRtpAudio;->getOpCNG()Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v21, v29

    invoke-direct/range {v21 .. v28}, Lco/discord/media_engine/InboundFrameOpStats;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    iget-object v8, v0, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lco/discord/media_engine/InboundAudio;

    if-eqz v14, :cond_5

    invoke-virtual {v14}, Lco/discord/media_engine/InboundAudio;->getPacketsReceived()I

    move-result v15

    sub-int v15, v12, v15

    invoke-virtual {v14}, Lco/discord/media_engine/InboundAudio;->getPacketsLost()I

    move-result v16

    sub-int v5, v13, v16

    const-wide/16 v16, 0x0

    invoke-virtual {v14}, Lco/discord/media_engine/InboundAudio;->getMosBuckets()[Ljava/lang/Integer;

    move-result-object v19

    if-lez v15, :cond_3

    if-ltz v5, :cond_3

    add-int/2addr v10, v11

    int-to-double v10, v10

    move-object/from16 v23, v3

    int-to-double v2, v5

    add-int/2addr v15, v5

    int-to-double v4, v15

    div-double v30, v2, v4

    const-wide/16 v32, 0x0

    const-wide/high16 v34, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v30 .. v35}, Lco/discord/media_engine/VoiceQualityKt;->access$clamp(DDD)D

    move-result-wide v2

    invoke-static {v10, v11, v2, v3}, Lco/discord/media_engine/VoiceQualityKt;->access$_calculateMos(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    aget-object v5, v19, v4

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v10, 0x1

    add-int/2addr v5, v10

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v19, v4

    move-wide/from16 v16, v2

    goto :goto_4

    :cond_3
    move-object/from16 v23, v3

    :goto_4
    new-instance v2, Lco/discord/media_engine/InboundAudio;

    invoke-virtual {v14}, Lco/discord/media_engine/InboundAudio;->getMosSum()D

    move-result-wide v3

    add-double v3, v3, v16

    invoke-virtual {v14}, Lco/discord/media_engine/InboundAudio;->getMosCount()I

    move-result v5

    const/4 v10, 0x0

    int-to-double v14, v10

    cmpl-double v10, v16, v14

    if-lez v10, :cond_4

    const/4 v10, 0x1

    goto :goto_5

    :cond_4
    const/4 v10, 0x0

    :goto_5
    add-int v18, v5, v10

    move-object v11, v2

    move-wide/from16 v14, v16

    move-wide/from16 v16, v3

    move-object/from16 v21, v29

    invoke-direct/range {v11 .. v21}, Lco/discord/media_engine/InboundAudio;-><init>(IIDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)V

    const/4 v10, 0x0

    goto :goto_6

    :cond_5
    move-object/from16 v23, v3

    new-instance v2, Lco/discord/media_engine/InboundAudio;

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const/16 v18, 0x0

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v10, 0x0

    aput-object v6, v3, v10

    const/4 v4, 0x1

    aput-object v6, v3, v4

    const/4 v4, 0x2

    aput-object v6, v3, v4

    const/4 v4, 0x3

    aput-object v6, v3, v4

    const/4 v4, 0x4

    aput-object v6, v3, v4

    move-object v11, v2

    move-object/from16 v19, v3

    move-object/from16 v21, v29

    invoke-direct/range {v11 .. v21}, Lco/discord/media_engine/InboundAudio;-><init>(IIDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)V

    :goto_6
    invoke-interface {v8, v9, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v2, p1

    move-object/from16 v3, v23

    const/4 v4, 0x1

    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v10, 0x0

    iget-object v2, v0, Lco/discord/media_engine/VoiceQuality;->_outboundStats:Lco/discord/media_engine/OutboundAudio;

    invoke-virtual {v2}, Lco/discord/media_engine/OutboundAudio;->getPacketsSent()I

    move-result v2

    if-le v2, v1, :cond_7

    iget-object v1, v0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v1}, Lco/discord/media_engine/Duration;->getSpeaking()I

    move-result v2

    const/4 v3, 0x1

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lco/discord/media_engine/Duration;->setSpeaking(I)V

    const/4 v1, 0x1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    iget-object v2, v0, Lco/discord/media_engine/VoiceQuality;->_inboundStats:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/discord/media_engine/InboundAudio;

    invoke-virtual {v4}, Lco/discord/media_engine/InboundAudio;->getPacketsReceived()I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_8

    :cond_8
    if-le v3, v7, :cond_9

    iget-object v2, v0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v2}, Lco/discord/media_engine/Duration;->getListening()I

    move-result v3

    const/4 v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Lco/discord/media_engine/Duration;->setListening(I)V

    const/4 v5, 0x1

    goto :goto_9

    :cond_9
    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_9
    if-nez v1, :cond_a

    if-eqz v5, :cond_b

    :cond_a
    iget-object v1, v0, Lco/discord/media_engine/VoiceQuality;->_duration:Lco/discord/media_engine/Duration;

    invoke-virtual {v1}, Lco/discord/media_engine/Duration;->getParticipation()I

    move-result v2

    add-int/2addr v2, v4

    invoke-virtual {v1, v2}, Lco/discord/media_engine/Duration;->setParticipation(I)V

    :cond_b
    return-void
.end method
