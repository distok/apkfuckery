.class public final Lco/discord/media_engine/OutboundAudio;
.super Ljava/lang/Object;
.source "VoiceQuality.kt"


# instance fields
.field private final packetsLost:I

.field private final packetsSent:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    iput p2, p0, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/OutboundAudio;IIILjava/lang/Object;)Lco/discord/media_engine/OutboundAudio;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lco/discord/media_engine/OutboundAudio;->copy(II)Lco/discord/media_engine/OutboundAudio;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    return v0
.end method

.method public final copy(II)Lco/discord/media_engine/OutboundAudio;
    .locals 1

    new-instance v0, Lco/discord/media_engine/OutboundAudio;

    invoke-direct {v0, p1, p2}, Lco/discord/media_engine/OutboundAudio;-><init>(II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_3

    instance-of v1, p1, Lco/discord/media_engine/OutboundAudio;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lco/discord/media_engine/OutboundAudio;

    iget v1, p0, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    iget v3, p1, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iget v1, p0, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    iget p1, p1, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    if-ne v1, p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    return v2

    :cond_3
    :goto_2
    return v0
.end method

.method public final getPacketsLost()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    return v0
.end method

.method public final getPacketsSent()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "OutboundAudio(packetsSent="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/OutboundAudio;->packetsSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", packetsLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/OutboundAudio;->packetsLost:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
