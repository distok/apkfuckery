.class public final Lco/discord/media_engine/Duration;
.super Ljava/lang/Object;
.source "VoiceQuality.kt"


# instance fields
.field private connected:I

.field private listening:I

.field private participation:I

.field private speaking:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/Duration;->listening:I

    iput p2, p0, Lco/discord/media_engine/Duration;->speaking:I

    iput p3, p0, Lco/discord/media_engine/Duration;->participation:I

    iput p4, p0, Lco/discord/media_engine/Duration;->connected:I

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/Duration;IIIIILjava/lang/Object;)Lco/discord/media_engine/Duration;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lco/discord/media_engine/Duration;->listening:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lco/discord/media_engine/Duration;->speaking:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lco/discord/media_engine/Duration;->participation:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lco/discord/media_engine/Duration;->connected:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lco/discord/media_engine/Duration;->copy(IIII)Lco/discord/media_engine/Duration;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->listening:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->speaking:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->participation:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->connected:I

    return v0
.end method

.method public final copy(IIII)Lco/discord/media_engine/Duration;
    .locals 1

    new-instance v0, Lco/discord/media_engine/Duration;

    invoke-direct {v0, p1, p2, p3, p4}, Lco/discord/media_engine/Duration;-><init>(IIII)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_5

    instance-of v1, p1, Lco/discord/media_engine/Duration;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    check-cast p1, Lco/discord/media_engine/Duration;

    iget v1, p0, Lco/discord/media_engine/Duration;->listening:I

    iget v3, p1, Lco/discord/media_engine/Duration;->listening:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget v1, p0, Lco/discord/media_engine/Duration;->speaking:I

    iget v3, p1, Lco/discord/media_engine/Duration;->speaking:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_4

    iget v1, p0, Lco/discord/media_engine/Duration;->participation:I

    iget v3, p1, Lco/discord/media_engine/Duration;->participation:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_4

    iget v1, p0, Lco/discord/media_engine/Duration;->connected:I

    iget p1, p1, Lco/discord/media_engine/Duration;->connected:I

    if-ne v1, p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    return v2

    :cond_5
    :goto_4
    return v0
.end method

.method public final getConnected()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->connected:I

    return v0
.end method

.method public final getListening()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->listening:I

    return v0
.end method

.method public final getParticipation()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->participation:I

    return v0
.end method

.method public final getSpeaking()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/Duration;->speaking:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lco/discord/media_engine/Duration;->listening:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/Duration;->speaking:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/Duration;->participation:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/Duration;->connected:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final setConnected(I)V
    .locals 0

    iput p1, p0, Lco/discord/media_engine/Duration;->connected:I

    return-void
.end method

.method public final setListening(I)V
    .locals 0

    iput p1, p0, Lco/discord/media_engine/Duration;->listening:I

    return-void
.end method

.method public final setParticipation(I)V
    .locals 0

    iput p1, p0, Lco/discord/media_engine/Duration;->participation:I

    return-void
.end method

.method public final setSpeaking(I)V
    .locals 0

    iput p1, p0, Lco/discord/media_engine/Duration;->speaking:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Duration(listening="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/Duration;->listening:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", speaking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/Duration;->speaking:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", participation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/Duration;->participation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", connected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/Duration;->connected:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
