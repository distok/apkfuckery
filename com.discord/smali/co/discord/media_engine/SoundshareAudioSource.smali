.class public final Lco/discord/media_engine/SoundshareAudioSource;
.super Ljava/lang/Object;
.source "SoundshareAudioSource.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;,
        Lco/discord/media_engine/SoundshareAudioSource$Companion;
    }
.end annotation


# static fields
.field private static final AUDIO_RECORD_THREAD_JOIN_TIMEOUT_MS:J = 0x7d0L

.field private static final BITS_PER_SAMPLE:I = 0x10

.field private static final BUFFERS_PER_SECOND:I = 0x64

.field private static final BUFFER_SIZE_FACTOR:I = 0x2

.field private static final CALLBACK_BUFFER_SIZE_MS:I = 0xa

.field public static final Companion:Lco/discord/media_engine/SoundshareAudioSource$Companion;

.field private static final TAG:Ljava/lang/String; = "SoundshareAudioSource"

.field private static volatile microphoneMute:Z


# instance fields
.field private audioRecord:Landroid/media/AudioRecord;

.field private audioThread:Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;

.field private byteBuffer:Ljava/nio/ByteBuffer;

.field private final nativeInstance:J

.field private released:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/discord/media_engine/SoundshareAudioSource$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/discord/media_engine/SoundshareAudioSource$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lco/discord/media_engine/SoundshareAudioSource;->Companion:Lco/discord/media_engine/SoundshareAudioSource$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lco/discord/media_engine/SoundshareAudioSource;->nativeCreateInstance()J

    move-result-wide v0

    iput-wide v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->nativeInstance:J

    return-void
.end method

.method public static final synthetic access$dataIsRecorded(Lco/discord/media_engine/SoundshareAudioSource;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lco/discord/media_engine/SoundshareAudioSource;->dataIsRecorded(IJ)V

    return-void
.end method

.method public static final synthetic access$getMicrophoneMute$cp()Z
    .locals 1

    sget-boolean v0, Lco/discord/media_engine/SoundshareAudioSource;->microphoneMute:Z

    return v0
.end method

.method public static final synthetic access$reportSoundshareAudioSourceError(Lco/discord/media_engine/SoundshareAudioSource;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lco/discord/media_engine/SoundshareAudioSource;->reportSoundshareAudioSourceError(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setMicrophoneMute$cp(Z)V
    .locals 0

    sput-boolean p0, Lco/discord/media_engine/SoundshareAudioSource;->microphoneMute:Z

    return-void
.end method

.method private final channelCountToConfiguration(I)I
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/16 p1, 0x10

    goto :goto_0

    :cond_0
    const/16 p1, 0xc

    :goto_0
    return p1
.end method

.method private final declared-synchronized dataIsRecorded(IJ)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->released:Z

    if-nez v0, :cond_0

    iget-wide v2, p0, Lco/discord/media_engine/SoundshareAudioSource;->nativeInstance:J

    move-object v1, p0

    move v4, p1

    move-wide v5, p2

    invoke-direct/range {v1 .. v6}, Lco/discord/media_engine/SoundshareAudioSource;->nativeDataIsRecorded(JIJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private final synchronized native nativeCacheDirectBufferAddress(JLjava/nio/ByteBuffer;)V
.end method

.method private final synchronized native nativeCreateInstance()J
.end method

.method private final native nativeDataIsRecorded(JIJ)V
.end method

.method private final synchronized native nativeDestroyInstance(J)V
.end method

.method private final native nativeSetSampleFormat(JIII)V
.end method

.method private final reportSoundshareAudioSourceError(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Run-time recording error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SoundshareAudioSource"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private final reportSoundshareAudioSourceInitError(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Init recording error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SoundshareAudioSource"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private final reportSoundshareAudioSourceStartError(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Start recording error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SoundshareAudioSource"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public final getNativeInstance()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->nativeInstance:J

    return-wide v0
.end method

.method public final declared-synchronized release()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->released:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioRecord:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioRecord:Landroid/media/AudioRecord;

    iget-wide v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->nativeInstance:J

    invoke-direct {p0, v0, v1}, Lco/discord/media_engine/SoundshareAudioSource;->nativeDestroyInstance(J)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->released:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setSampleFormat(III)V
    .locals 6

    iget-wide v1, p0, Lco/discord/media_engine/SoundshareAudioSource;->nativeInstance:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lco/discord/media_engine/SoundshareAudioSource;->nativeSetSampleFormat(JIII)V

    return-void
.end method

.method public final startRecording(Landroid/media/AudioRecord;)Z
    .locals 13

    const-string v0, "SoundshareAudioSource"

    const-string v1, "audioRecord"

    invoke-static {p1, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/media/AudioRecord;->getChannelCount()I

    move-result v1

    invoke-virtual {p1}, Landroid/media/AudioRecord;->getSampleRate()I

    move-result v2

    iget-object v3, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioRecord:Landroid/media/AudioRecord;

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    const-string p1, "StartRecording called twice without StopRecording."

    invoke-direct {p0, p1}, Lco/discord/media_engine/SoundshareAudioSource;->reportSoundshareAudioSourceInitError(Ljava/lang/String;)V

    return v4

    :cond_0
    iput-object p1, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioRecord:Landroid/media/AudioRecord;

    const/16 v3, 0x10

    invoke-virtual {p0, v2, v3, v1}, Lco/discord/media_engine/SoundshareAudioSource;->setSampleFormat(III)V

    mul-int/lit8 v3, v1, 0x2

    div-int/lit8 v5, v2, 0x64

    mul-int v5, v5, v3

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    iput-object v10, p0, Lco/discord/media_engine/SoundshareAudioSource;->byteBuffer:Ljava/nio/ByteBuffer;

    iget-wide v5, p0, Lco/discord/media_engine/SoundshareAudioSource;->nativeInstance:J

    invoke-direct {p0, v5, v6, v10}, Lco/discord/media_engine/SoundshareAudioSource;->nativeCacheDirectBufferAddress(JLjava/nio/ByteBuffer;)V

    invoke-direct {p0, v1}, Lco/discord/media_engine/SoundshareAudioSource;->channelCountToConfiguration(I)I

    move-result v1

    const/4 v3, 0x2

    invoke-static {v2, v1, v3}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    const/4 v2, -0x2

    if-ne v1, v2, :cond_1

    goto/16 :goto_1

    :cond_1
    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    invoke-virtual {p1}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const-string p1, "Failed to create a new AudioRecord instance"

    invoke-direct {p0, p1}, Lco/discord/media_engine/SoundshareAudioSource;->reportSoundshareAudioSourceInitError(Ljava/lang/String;)V

    invoke-virtual {p0}, Lco/discord/media_engine/SoundshareAudioSource;->release()V

    return v4

    :cond_2
    :try_start_0
    sget-object v1, Lco/discord/media_engine/SoundshareAudioSource;->Companion:Lco/discord/media_engine/SoundshareAudioSource$Companion;

    iget-object v3, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioThread:Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;

    if-nez v3, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    invoke-static {v1, v3}, Lco/discord/media_engine/SoundshareAudioSource$Companion;->access$assertTrue(Lco/discord/media_engine/SoundshareAudioSource$Companion;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {}, Lorg/webrtc/TimestampAligner;->getRtcTimeNanos()J

    move-result-wide v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {p1}, Landroid/media/AudioRecord;->startRecording()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {p1}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioRecord.startRecording failed - incorrect state :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lco/discord/media_engine/SoundshareAudioSource;->reportSoundshareAudioSourceStartError(Ljava/lang/String;)V

    return v4

    :cond_4
    new-instance v1, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;

    const-string v8, "SoundshareThread"

    const-string v3, "byteBuffer"

    invoke-static {v10, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v1

    move-object v7, p0

    move-object v9, p1

    invoke-direct/range {v6 .. v12}, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;-><init>(Lco/discord/media_engine/SoundshareAudioSource;Ljava/lang/String;Landroid/media/AudioRecord;Ljava/nio/ByteBuffer;J)V

    iput-object v1, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioThread:Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return v2

    :catch_0
    move-exception p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioRecord.startRecording failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lco/discord/media_engine/SoundshareAudioSource;->reportSoundshareAudioSourceStartError(Ljava/lang/String;)V

    return v4

    :catchall_0
    move-exception p1

    const-string v1, "WebrtcAudioRecord.startRecording: audioThread != null!"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    const-string v1, "SoundshareAudioSource.startRecording fail hard!"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw p1

    :cond_5
    :goto_1
    const-string p1, "AudioRecord.getMinBufferSize failed: "

    invoke-static {p1, v1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lco/discord/media_engine/SoundshareAudioSource;->reportSoundshareAudioSourceInitError(Ljava/lang/String;)V

    return v4
.end method

.method public final stopRecording()Z
    .locals 3

    iget-object v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioThread:Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->stopThread()V

    const-wide/16 v1, 0x7d0

    invoke-static {v0, v1, v2}, Lorg/webrtc/ThreadUtils;->joinUninterruptibly(Ljava/lang/Thread;J)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SoundshareAudioSource"

    const-string v1, "Join of SoundshareThread timed out"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lco/discord/media_engine/SoundshareAudioSource;->audioThread:Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
