.class public final Lco/discord/media_engine/InboundFrameOpStats;
.super Ljava/lang/Object;
.source "VoiceQuality.kt"


# instance fields
.field private final accelerated:Ljava/lang/Integer;

.field private final cng:Ljava/lang/Integer;

.field private final expanded:Ljava/lang/Integer;

.field private final merged:Ljava/lang/Integer;

.field private final normal:Ljava/lang/Integer;

.field private final preemptiveExpanded:Ljava/lang/Integer;

.field private final silent:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    iput-object p2, p0, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    iput-object p3, p0, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    iput-object p4, p0, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    iput-object p5, p0, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    iput-object p6, p0, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    iput-object p7, p0, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/InboundFrameOpStats;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lco/discord/media_engine/InboundFrameOpStats;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lco/discord/media_engine/InboundFrameOpStats;->copy(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lco/discord/media_engine/InboundFrameOpStats;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component3()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component4()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component5()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component6()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component7()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lco/discord/media_engine/InboundFrameOpStats;
    .locals 9

    new-instance v8, Lco/discord/media_engine/InboundFrameOpStats;

    move-object v0, v8

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lco/discord/media_engine/InboundFrameOpStats;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lco/discord/media_engine/InboundFrameOpStats;

    if-eqz v0, :cond_0

    check-cast p1, Lco/discord/media_engine/InboundFrameOpStats;

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    iget-object v1, p1, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    iget-object v1, p1, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    iget-object v1, p1, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    iget-object v1, p1, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    iget-object v1, p1, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    iget-object v1, p1, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    iget-object p1, p1, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAccelerated()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getCng()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getExpanded()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getMerged()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getNormal()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getPreemptiveExpanded()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getSilent()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "InboundFrameOpStats(silent="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/discord/media_engine/InboundFrameOpStats;->silent:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", normal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundFrameOpStats;->normal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", merged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundFrameOpStats;->merged:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", expanded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundFrameOpStats;->expanded:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", accelerated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundFrameOpStats;->accelerated:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preemptiveExpanded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundFrameOpStats;->preemptiveExpanded:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cng="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundFrameOpStats;->cng:Ljava/lang/Integer;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->w(Ljava/lang/StringBuilder;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
