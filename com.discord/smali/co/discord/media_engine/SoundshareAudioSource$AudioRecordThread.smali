.class public final Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;
.super Ljava/lang/Thread;
.source "SoundshareAudioSource.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/discord/media_engine/SoundshareAudioSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "AudioRecordThread"
.end annotation


# instance fields
.field private final audioRecord:Landroid/media/AudioRecord;

.field private final byteBuffer:Ljava/nio/ByteBuffer;

.field private final emptyBytes:[B

.field private volatile keepAlive:Z

.field public final synthetic this$0:Lco/discord/media_engine/SoundshareAudioSource;

.field private timestamp:J


# direct methods
.method public constructor <init>(Lco/discord/media_engine/SoundshareAudioSource;Ljava/lang/String;Landroid/media/AudioRecord;Ljava/nio/ByteBuffer;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/media/AudioRecord;",
            "Ljava/nio/ByteBuffer;",
            "J)V"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioRecord"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "byteBuffer"

    invoke-static {p4, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->this$0:Lco/discord/media_engine/SoundshareAudioSource;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    iput-object p3, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->audioRecord:Landroid/media/AudioRecord;

    iput-object p4, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->byteBuffer:Ljava/nio/ByteBuffer;

    iput-wide p5, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->timestamp:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->keepAlive:Z

    invoke-virtual {p4}, Ljava/nio/ByteBuffer;->capacity()I

    move-result p1

    new-array p1, p1, [B

    iput-object p1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->emptyBytes:[B

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    sget-object v0, Lco/discord/media_engine/SoundshareAudioSource;->Companion:Lco/discord/media_engine/SoundshareAudioSource$Companion;

    iget-object v1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lco/discord/media_engine/SoundshareAudioSource$Companion;->access$assertTrue(Lco/discord/media_engine/SoundshareAudioSource$Companion;Z)V

    :cond_1
    :goto_1
    iget-boolean v0, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->keepAlive:Z

    const-string v1, "SoundshareAudioSource"

    if-eqz v0, :cond_4

    iget-object v0, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->audioRecord:Landroid/media/AudioRecord;

    iget-object v3, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/media/AudioRecord;->read(Ljava/nio/ByteBuffer;I)I

    move-result v0

    invoke-static {}, Lorg/webrtc/TimestampAligner;->getRtcTimeNanos()J

    move-result-wide v3

    iput-wide v3, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->timestamp:J

    iget-object v3, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-ne v0, v3, :cond_3

    invoke-static {}, Lco/discord/media_engine/SoundshareAudioSource;->access$getMicrophoneMute$cp()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->byteBuffer:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->emptyBytes:[B

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    :cond_2
    iget-boolean v1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->keepAlive:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->this$0:Lco/discord/media_engine/SoundshareAudioSource;

    iget-wide v3, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->timestamp:J

    invoke-static {v1, v0, v3, v4}, Lco/discord/media_engine/SoundshareAudioSource;->access$dataIsRecorded(Lco/discord/media_engine/SoundshareAudioSource;IJ)V

    goto :goto_1

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AudioRecord.read failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_1

    iput-boolean v2, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->keepAlive:Z

    iget-object v0, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->this$0:Lco/discord/media_engine/SoundshareAudioSource;

    invoke-static {v0, v3}, Lco/discord/media_engine/SoundshareAudioSource;->access$reportSoundshareAudioSourceError(Lco/discord/media_engine/SoundshareAudioSource;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    :try_start_0
    iget-object v0, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v2, "AudioRecord.stop failed: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void
.end method

.method public final stopThread()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/discord/media_engine/SoundshareAudioSource$AudioRecordThread;->keepAlive:Z

    return-void
.end method
