.class public final Lco/discord/media_engine/InboundRtpAudio;
.super Ljava/lang/Object;
.source "Statistics.kt"


# instance fields
.field private final audioDetected:Z

.field private final audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

.field private final audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

.field private final audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

.field private final audioLevel:F

.field private final audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

.field private final bytesReceived:J

.field private final codec:Lco/discord/media_engine/StatsCodec;

.field private final decodingCNG:I

.field private final decodingMutedOutput:I

.field private final decodingNormal:I

.field private final decodingPLC:I

.field private final decodingPLCCNG:I

.field private final delayEstimate:I

.field private final fractionLost:F

.field private final jitter:I

.field private final jitterBuffer:I

.field private final jitterBufferPreferred:I

.field private final opAccelerate:Ljava/lang/Integer;

.field private final opCNG:Ljava/lang/Integer;

.field private final opExpand:Ljava/lang/Integer;

.field private final opMerge:Ljava/lang/Integer;

.field private final opNormal:Ljava/lang/Integer;

.field private final opPreemptiveExpand:Ljava/lang/Integer;

.field private final opSilence:Ljava/lang/Integer;

.field private final packetsLost:I

.field private final packetsReceived:I

.field private final relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

.field private final relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

.field private final ssrc:I

.field private final type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFFZIIIIIIIIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)V
    .locals 4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    const-string/jumbo v3, "type"

    invoke-static {p1, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "codec"

    invoke-static {p3, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    move v1, p2

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    iput-object v2, v0, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    move-wide v1, p4

    iput-wide v1, v0, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    move v1, p6

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    move v1, p7

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    move v1, p8

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    move v1, p9

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    move v1, p10

    iput-boolean v1, v0, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    move v1, p11

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    move/from16 v1, p12

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    move/from16 v1, p13

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    move/from16 v1, p14

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    move/from16 v1, p15

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    move/from16 v1, p16

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    move/from16 v1, p17

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    move/from16 v1, p18

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    move/from16 v1, p19

    iput v1, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    move-object/from16 v1, p20

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    move-object/from16 v1, p21

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    move-object/from16 v1, p22

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    move-object/from16 v1, p23

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    move-object/from16 v1, p24

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    move-object/from16 v1, p25

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    move-object/from16 v1, p26

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    move-object/from16 v1, p27

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    move-object/from16 v1, p28

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    move-object/from16 v1, p29

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    move-object/from16 v1, p30

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    move-object/from16 v1, p31

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    move-object/from16 v1, p32

    iput-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/InboundRtpAudio;Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFFZIIIIIIIIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;ILjava/lang/Object;)Lco/discord/media_engine/InboundRtpAudio;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p33

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-wide v5, v0, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    goto :goto_3

    :cond_3
    move-wide/from16 v5, p4

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget v7, v0, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    goto :goto_4

    :cond_4
    move/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget v8, v0, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    goto :goto_5

    :cond_5
    move/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget v9, v0, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    goto :goto_6

    :cond_6
    move/from16 v9, p8

    :goto_6
    and-int/lit16 v10, v1, 0x80

    if-eqz v10, :cond_7

    iget v10, v0, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    goto :goto_7

    :cond_7
    move/from16 v10, p9

    :goto_7
    and-int/lit16 v11, v1, 0x100

    if-eqz v11, :cond_8

    iget-boolean v11, v0, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    goto :goto_8

    :cond_8
    move/from16 v11, p10

    :goto_8
    and-int/lit16 v12, v1, 0x200

    if-eqz v12, :cond_9

    iget v12, v0, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    goto :goto_9

    :cond_9
    move/from16 v12, p11

    :goto_9
    and-int/lit16 v13, v1, 0x400

    if-eqz v13, :cond_a

    iget v13, v0, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    goto :goto_a

    :cond_a
    move/from16 v13, p12

    :goto_a
    and-int/lit16 v14, v1, 0x800

    if-eqz v14, :cond_b

    iget v14, v0, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    goto :goto_b

    :cond_b
    move/from16 v14, p13

    :goto_b
    and-int/lit16 v15, v1, 0x1000

    if-eqz v15, :cond_c

    iget v15, v0, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    goto :goto_c

    :cond_c
    move/from16 v15, p14

    :goto_c
    move/from16 p14, v15

    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget v15, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    goto :goto_d

    :cond_d
    move/from16 v15, p15

    :goto_d
    move/from16 p15, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget v15, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    goto :goto_e

    :cond_e
    move/from16 v15, p16

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move/from16 p16, v15

    if-eqz v16, :cond_f

    iget v15, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    goto :goto_f

    :cond_f
    move/from16 v15, p17

    :goto_f
    const/high16 v16, 0x10000

    and-int v16, v1, v16

    move/from16 p17, v15

    if-eqz v16, :cond_10

    iget v15, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    goto :goto_10

    :cond_10
    move/from16 v15, p18

    :goto_10
    const/high16 v16, 0x20000

    and-int v16, v1, v16

    move/from16 p18, v15

    if-eqz v16, :cond_11

    iget v15, v0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    goto :goto_11

    :cond_11
    move/from16 v15, p19

    :goto_11
    const/high16 v16, 0x40000

    and-int v16, v1, v16

    move/from16 p19, v15

    if-eqz v16, :cond_12

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    goto :goto_12

    :cond_12
    move-object/from16 v15, p20

    :goto_12
    const/high16 v16, 0x80000

    and-int v16, v1, v16

    move-object/from16 p20, v15

    if-eqz v16, :cond_13

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    goto :goto_13

    :cond_13
    move-object/from16 v15, p21

    :goto_13
    const/high16 v16, 0x100000

    and-int v16, v1, v16

    move-object/from16 p21, v15

    if-eqz v16, :cond_14

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    goto :goto_14

    :cond_14
    move-object/from16 v15, p22

    :goto_14
    const/high16 v16, 0x200000

    and-int v16, v1, v16

    move-object/from16 p22, v15

    if-eqz v16, :cond_15

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    goto :goto_15

    :cond_15
    move-object/from16 v15, p23

    :goto_15
    const/high16 v16, 0x400000

    and-int v16, v1, v16

    move-object/from16 p23, v15

    if-eqz v16, :cond_16

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    goto :goto_16

    :cond_16
    move-object/from16 v15, p24

    :goto_16
    const/high16 v16, 0x800000

    and-int v16, v1, v16

    move-object/from16 p24, v15

    if-eqz v16, :cond_17

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    goto :goto_17

    :cond_17
    move-object/from16 v15, p25

    :goto_17
    const/high16 v16, 0x1000000

    and-int v16, v1, v16

    move-object/from16 p25, v15

    if-eqz v16, :cond_18

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    goto :goto_18

    :cond_18
    move-object/from16 v15, p26

    :goto_18
    const/high16 v16, 0x2000000

    and-int v16, v1, v16

    move-object/from16 p26, v15

    if-eqz v16, :cond_19

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    goto :goto_19

    :cond_19
    move-object/from16 v15, p27

    :goto_19
    const/high16 v16, 0x4000000

    and-int v16, v1, v16

    move-object/from16 p27, v15

    if-eqz v16, :cond_1a

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    goto :goto_1a

    :cond_1a
    move-object/from16 v15, p28

    :goto_1a
    const/high16 v16, 0x8000000

    and-int v16, v1, v16

    move-object/from16 p28, v15

    if-eqz v16, :cond_1b

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    goto :goto_1b

    :cond_1b
    move-object/from16 v15, p29

    :goto_1b
    const/high16 v16, 0x10000000

    and-int v16, v1, v16

    move-object/from16 p29, v15

    if-eqz v16, :cond_1c

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    goto :goto_1c

    :cond_1c
    move-object/from16 v15, p30

    :goto_1c
    const/high16 v16, 0x20000000

    and-int v16, v1, v16

    move-object/from16 p30, v15

    if-eqz v16, :cond_1d

    iget-object v15, v0, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    goto :goto_1d

    :cond_1d
    move-object/from16 v15, p31

    :goto_1d
    const/high16 v16, 0x40000000    # 2.0f

    and-int v1, v1, v16

    if-eqz v1, :cond_1e

    iget-object v1, v0, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    goto :goto_1e

    :cond_1e
    move-object/from16 v1, p32

    :goto_1e
    move-object/from16 p1, v2

    move/from16 p2, v3

    move-object/from16 p3, v4

    move-wide/from16 p4, v5

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v13

    move/from16 p13, v14

    move-object/from16 p31, v15

    move-object/from16 p32, v1

    invoke-virtual/range {p0 .. p32}, Lco/discord/media_engine/InboundRtpAudio;->copy(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFFZIIIIIIIIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/InboundRtpAudio;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    return v0
.end method

.method public final component11()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    return v0
.end method

.method public final component12()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    return v0
.end method

.method public final component13()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    return v0
.end method

.method public final component14()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    return v0
.end method

.method public final component15()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    return v0
.end method

.method public final component16()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    return v0
.end method

.method public final component17()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    return v0
.end method

.method public final component18()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    return v0
.end method

.method public final component19()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    return v0
.end method

.method public final component20()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component21()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component22()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component23()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component24()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component25()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component26()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final component27()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final component28()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final component29()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final component3()Lco/discord/media_engine/StatsCodec;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    return-object v0
.end method

.method public final component30()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final component31()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    return-wide v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    return v0
.end method

.method public final component7()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    return v0
.end method

.method public final component8()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    return v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFFZIIIIIIIIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)Lco/discord/media_engine/InboundRtpAudio;
    .locals 35

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    move/from16 v15, p15

    move/from16 v16, p16

    move/from16 v17, p17

    move/from16 v18, p18

    move/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    move-object/from16 v23, p23

    move-object/from16 v24, p24

    move-object/from16 v25, p25

    move-object/from16 v26, p26

    move-object/from16 v27, p27

    move-object/from16 v28, p28

    move-object/from16 v29, p29

    move-object/from16 v30, p30

    move-object/from16 v31, p31

    move-object/from16 v32, p32

    const-string/jumbo v0, "type"

    move-object/from16 v33, v1

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "codec"

    move-object/from16 v1, p3

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v34, Lco/discord/media_engine/InboundRtpAudio;

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-direct/range {v0 .. v32}, Lco/discord/media_engine/InboundRtpAudio;-><init>(Ljava/lang/String;ILco/discord/media_engine/StatsCodec;JIIFFZIIIIIIIIILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;Lco/discord/media_engine/PlayoutMetric;)V

    return-object v34
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-eq p0, p1, :cond_f

    instance-of v1, p1, Lco/discord/media_engine/InboundRtpAudio;

    const/4 v2, 0x0

    if-eqz v1, :cond_e

    check-cast p1, Lco/discord/media_engine/InboundRtpAudio;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-wide v3, p0, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    iget-wide v5, p1, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    iget-boolean v3, p1, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    if-ne v1, v3, :cond_6

    const/4 v1, 0x1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    if-ne v1, v3, :cond_7

    const/4 v1, 0x1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    if-ne v1, v3, :cond_8

    const/4 v1, 0x1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    if-ne v1, v3, :cond_9

    const/4 v1, 0x1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    if-ne v1, v3, :cond_a

    const/4 v1, 0x1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    if-ne v1, v3, :cond_b

    const/4 v1, 0x1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    if-ne v1, v3, :cond_c

    const/4 v1, 0x1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    if-eqz v1, :cond_e

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    iget v3, p1, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    if-ne v1, v3, :cond_d

    const/4 v1, 0x1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    iget-object v3, p1, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    iget-object p1, p1, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_e

    goto :goto_e

    :cond_e
    return v2

    :cond_f
    :goto_e
    return v0
.end method

.method public final getAudioDetected()Z
    .locals 1

    iget-boolean v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    return v0
.end method

.method public final getAudioJitterBuffer()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final getAudioJitterDelay()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final getAudioJitterTarget()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final getAudioLevel()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    return v0
.end method

.method public final getAudioPlayoutUnderruns()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final getBytesReceived()J
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    return-wide v0
.end method

.method public final getCodec()Lco/discord/media_engine/StatsCodec;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    return-object v0
.end method

.method public final getDecodingCNG()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    return v0
.end method

.method public final getDecodingMutedOutput()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    return v0
.end method

.method public final getDecodingNormal()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    return v0
.end method

.method public final getDecodingPLC()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    return v0
.end method

.method public final getDecodingPLCCNG()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    return v0
.end method

.method public final getDelayEstimate()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    return v0
.end method

.method public final getFractionLost()F
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    return v0
.end method

.method public final getJitter()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    return v0
.end method

.method public final getJitterBuffer()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    return v0
.end method

.method public final getJitterBufferPreferred()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    return v0
.end method

.method public final getOpAccelerate()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getOpCNG()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getOpExpand()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getOpMerge()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getOpNormal()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getOpPreemptiveExpand()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getOpSilence()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getPacketsLost()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    return v0
.end method

.method public final getPacketsReceived()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    return v0
.end method

.method public final getRelativePlayoutDelay()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final getRelativeReceptionDelay()Lco/discord/media_engine/PlayoutMetric;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    return-object v0
.end method

.method public final getSsrc()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    return v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lco/discord/media_engine/StatsCodec;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x1f

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x1f

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x1f

    iget v0, p0, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    const/16 v3, 0x1f

    invoke-static {v0, v2, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->m(FII)I

    move-result v0

    iget-boolean v2, p0, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_9
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lco/discord/media_engine/PlayoutMetric;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_a
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lco/discord/media_engine/PlayoutMetric;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_b
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lco/discord/media_engine/PlayoutMetric;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_c
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Lco/discord/media_engine/PlayoutMetric;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_d
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lco/discord/media_engine/PlayoutMetric;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_e
    const/4 v2, 0x0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lco/discord/media_engine/PlayoutMetric;->hashCode()I

    move-result v1

    :cond_f
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "InboundRtpAudio(type="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", ssrc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", codec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->codec:Lco/discord/media_engine/StatsCodec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bytesReceived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/InboundRtpAudio;->bytesReceived:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", packetsReceived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsReceived:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", packetsLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", fractionLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", audioLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioLevel:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", audioDetected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioDetected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", jitter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->jitter:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", jitterBuffer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBuffer:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", jitterBufferPreferred="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->jitterBufferPreferred:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", delayEstimate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->delayEstimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decodingCNG="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingCNG:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decodingMutedOutput="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingMutedOutput:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decodingNormal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingNormal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decodingPLC="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLC:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", decodingPLCCNG="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundRtpAudio;->decodingPLCCNG:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", opSilence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opSilence:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", opNormal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opNormal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", opMerge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opMerge:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", opExpand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opExpand:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", opAccelerate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opAccelerate:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", opPreemptiveExpand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opPreemptiveExpand:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", opCNG="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->opCNG:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", audioJitterBuffer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterBuffer:Lco/discord/media_engine/PlayoutMetric;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", audioJitterDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterDelay:Lco/discord/media_engine/PlayoutMetric;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", audioJitterTarget="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioJitterTarget:Lco/discord/media_engine/PlayoutMetric;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", audioPlayoutUnderruns="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->audioPlayoutUnderruns:Lco/discord/media_engine/PlayoutMetric;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", relativeReceptionDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->relativeReceptionDelay:Lco/discord/media_engine/PlayoutMetric;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", relativePlayoutDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundRtpAudio;->relativePlayoutDelay:Lco/discord/media_engine/PlayoutMetric;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
