.class public final Lco/discord/media_engine/VideoStreamRenderer$Muxer$addSink$1$2;
.super Ljava/lang/Object;
.source "VideoStreamRenderer.kt"

# interfaces
.implements Lcom/hammerandchisel/libdiscord/Discord$VideoFrameCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/discord/media_engine/VideoStreamRenderer$Muxer;->addSink(Lcom/hammerandchisel/libdiscord/Discord;Lco/discord/media_engine/VideoStreamRenderer;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $sinks:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lco/discord/media_engine/VideoStreamRenderer$Muxer$addSink$1$2;->$sinks:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFrame(Lorg/webrtc/VideoFrame;)Z
    .locals 2

    const-string v0, "frame"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/discord/media_engine/VideoStreamRenderer$Muxer$addSink$1$2;->$sinks:Ljava/util/Set;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lco/discord/media_engine/VideoStreamRenderer$Muxer$addSink$1$2;->$sinks:Ljava/util/Set;

    invoke-static {v1}, Lx/h/f;->firstOrNull(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/discord/media_engine/VideoStreamRenderer;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lorg/webrtc/SurfaceViewRenderer;->onFrame(Lorg/webrtc/VideoFrame;)V

    :cond_0
    invoke-virtual {p1}, Lorg/webrtc/VideoFrame;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method
