.class public final Lco/discord/media_engine/VoiceQualityKt;
.super Ljava/lang/Object;
.source "VoiceQuality.kt"


# direct methods
.method private static final _calculateMos(DD)D
    .locals 6

    invoke-static {p0, p1, p2, p3}, Lco/discord/media_engine/VoiceQualityKt;->_calculateR(DD)D

    move-result-wide p0

    const/4 p2, 0x0

    int-to-double p2, p2

    cmpg-double v0, p0, p2

    if-gez v0, :cond_0

    const-wide/high16 p0, 0x3ff0000000000000L    # 1.0

    return-wide p0

    :cond_0
    const/16 p2, 0x64

    int-to-double p2, p2

    cmpl-double v0, p0, p2

    if-lez v0, :cond_1

    const-wide/high16 p0, 0x4012000000000000L    # 4.5

    return-wide p0

    :cond_1
    const/4 v0, 0x1

    int-to-double v0, v0

    const-wide v2, 0x3fa1eb851eb851ecL    # 0.035

    mul-double v2, v2, p0

    add-double/2addr v2, v0

    const-wide v0, 0x3eddc79123a95274L    # 7.1E-6

    mul-double v0, v0, p0

    const/16 v4, 0x3c

    int-to-double v4, v4

    sub-double v4, p0, v4

    mul-double v4, v4, v0

    sub-double/2addr p2, p0

    mul-double p2, p2, v4

    add-double/2addr p2, v2

    return-wide p2
.end method

.method private static final _calculateR(DD)D
    .locals 6

    const-wide v0, 0x3f989374bc6a7efaL    # 0.024

    mul-double v0, v0, p0

    const-wide v2, 0x406629999999999aL    # 177.3

    cmpl-double v4, p0, v2

    if-lez v4, :cond_0

    const-wide v4, 0x3fbc28f5c28f5c29L    # 0.11

    sub-double/2addr p0, v2

    mul-double p0, p0, v4

    goto :goto_0

    :cond_0
    const-wide/16 p0, 0x0

    :goto_0
    add-double/2addr v0, p0

    const/16 p0, 0xa

    int-to-double p0, p0

    const/16 v2, 0x7a

    int-to-double v2, v2

    mul-double v2, v2, p2

    add-double/2addr p2, p0

    div-double/2addr v2, p2

    add-double/2addr v2, p0

    const-wide p0, 0x405759999999999aL    # 93.4

    sub-double/2addr p0, v0

    sub-double/2addr p0, v2

    return-wide p0
.end method

.method public static final synthetic access$_calculateMos(DD)D
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lco/discord/media_engine/VoiceQualityKt;->_calculateMos(DD)D

    move-result-wide p0

    return-wide p0
.end method

.method public static final synthetic access$clamp(DDD)D
    .locals 0

    invoke-static/range {p0 .. p5}, Lco/discord/media_engine/VoiceQualityKt;->clamp(DDD)D

    move-result-wide p0

    return-wide p0
.end method

.method public static final synthetic access$explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lco/discord/media_engine/VoiceQualityKt;->explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V

    return-void
.end method

.method private static final clamp(DDD)D
    .locals 1

    cmpg-double v0, p0, p2

    if-gez v0, :cond_0

    move-wide p0, p2

    goto :goto_0

    :cond_0
    cmpg-double p2, p4, p0

    if-gez p2, :cond_1

    move-wide p0, p4

    :cond_1
    :goto_0
    return-wide p0
.end method

.method private static final explodePlayoutMetric(Ljava/lang/String;Lco/discord/media_engine/PlayoutMetric;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lco/discord/media_engine/PlayoutMetric;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "_mean"

    invoke-static {p0, v0}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getMean()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-interface {p2, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_p75"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getP75()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    invoke-interface {p2, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_p95"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getP95()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto :goto_2

    :cond_2
    move-object v2, v1

    :goto_2
    invoke-interface {p2, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_p99"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getP99()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto :goto_3

    :cond_3
    move-object v2, v1

    :goto_3
    invoke-interface {p2, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "_max"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lco/discord/media_engine/PlayoutMetric;->getMax()F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    :cond_4
    invoke-interface {p2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
