.class public final Lco/discord/media_engine/InboundAudio;
.super Ljava/lang/Object;
.source "VoiceQuality.kt"


# instance fields
.field private final bufferStats:Lco/discord/media_engine/InboundBufferStats;

.field private final frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

.field private final mos:D

.field private final mosBuckets:[Ljava/lang/Integer;

.field private final mosCount:I

.field private final mosSum:D

.field private final packetsLost:I

.field private final packetsReceived:I


# direct methods
.method public constructor <init>(IIDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)V
    .locals 1

    const-string v0, "mosBuckets"

    invoke-static {p8, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bufferStats"

    invoke-static {p9, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "frameOpStats"

    invoke-static {p10, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    iput p2, p0, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    iput-wide p3, p0, Lco/discord/media_engine/InboundAudio;->mos:D

    iput-wide p5, p0, Lco/discord/media_engine/InboundAudio;->mosSum:D

    iput p7, p0, Lco/discord/media_engine/InboundAudio;->mosCount:I

    iput-object p8, p0, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    iput-object p9, p0, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    iput-object p10, p0, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    return-void
.end method

.method public static synthetic copy$default(Lco/discord/media_engine/InboundAudio;IIDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;ILjava/lang/Object;)Lco/discord/media_engine/InboundAudio;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-wide v4, v0, Lco/discord/media_engine/InboundAudio;->mos:D

    goto :goto_2

    :cond_2
    move-wide v4, p3

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget-wide v6, v0, Lco/discord/media_engine/InboundAudio;->mosSum:D

    goto :goto_3

    :cond_3
    move-wide/from16 v6, p5

    :goto_3
    and-int/lit8 v8, v1, 0x10

    if-eqz v8, :cond_4

    iget v8, v0, Lco/discord/media_engine/InboundAudio;->mosCount:I

    goto :goto_4

    :cond_4
    move/from16 v8, p7

    :goto_4
    and-int/lit8 v9, v1, 0x20

    if-eqz v9, :cond_5

    iget-object v9, v0, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    goto :goto_5

    :cond_5
    move-object/from16 v9, p8

    :goto_5
    and-int/lit8 v10, v1, 0x40

    if-eqz v10, :cond_6

    iget-object v10, v0, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    goto :goto_6

    :cond_6
    move-object/from16 v10, p9

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p10

    :goto_7
    move p1, v2

    move p2, v3

    move-wide p3, v4

    move-wide/from16 p5, v6

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lco/discord/media_engine/InboundAudio;->copy(IIDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)Lco/discord/media_engine/InboundAudio;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    return v0
.end method

.method public final component3()D
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/InboundAudio;->mos:D

    return-wide v0
.end method

.method public final component4()D
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/InboundAudio;->mosSum:D

    return-wide v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundAudio;->mosCount:I

    return v0
.end method

.method public final component6()[Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    return-object v0
.end method

.method public final component7()Lco/discord/media_engine/InboundBufferStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    return-object v0
.end method

.method public final component8()Lco/discord/media_engine/InboundFrameOpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    return-object v0
.end method

.method public final copy(IIDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)Lco/discord/media_engine/InboundAudio;
    .locals 12

    const-string v0, "mosBuckets"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bufferStats"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "frameOpStats"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/discord/media_engine/InboundAudio;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v11}, Lco/discord/media_engine/InboundAudio;-><init>(IIDDI[Ljava/lang/Integer;Lco/discord/media_engine/InboundBufferStats;Lco/discord/media_engine/InboundFrameOpStats;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-eq p0, p1, :cond_4

    instance-of v1, p1, Lco/discord/media_engine/InboundAudio;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    check-cast p1, Lco/discord/media_engine/InboundAudio;

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    iget v3, p1, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    iget v3, p1, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    iget-wide v3, p0, Lco/discord/media_engine/InboundAudio;->mos:D

    iget-wide v5, p1, Lco/discord/media_engine/InboundAudio;->mos:D

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v1

    if-nez v1, :cond_3

    iget-wide v3, p0, Lco/discord/media_engine/InboundAudio;->mosSum:D

    iget-wide v5, p1, Lco/discord/media_engine/InboundAudio;->mosSum:D

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->mosCount:I

    iget v3, p1, Lco/discord/media_engine/InboundAudio;->mosCount:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_3

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    iget-object v3, p1, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    iget-object v3, p1, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    iget-object p1, p1, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    return v2

    :cond_4
    :goto_3
    return v0
.end method

.method public final getBufferStats()Lco/discord/media_engine/InboundBufferStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    return-object v0
.end method

.method public final getFrameOpStats()Lco/discord/media_engine/InboundFrameOpStats;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    return-object v0
.end method

.method public final getMos()D
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/InboundAudio;->mos:D

    return-wide v0
.end method

.method public final getMosBuckets()[Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    return-object v0
.end method

.method public final getMosCount()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundAudio;->mosCount:I

    return v0
.end method

.method public final getMosSum()D
    .locals 2

    iget-wide v0, p0, Lco/discord/media_engine/InboundAudio;->mosSum:D

    return-wide v0
.end method

.method public final getPacketsLost()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    return v0
.end method

.method public final getPacketsReceived()I
    .locals 1

    iget v0, p0, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lco/discord/media_engine/InboundAudio;->mos:D

    invoke-static {v1, v2}, Lc;->a(D)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lco/discord/media_engine/InboundAudio;->mosSum:D

    invoke-static {v1, v2}, Lc;->a(D)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->mosCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lco/discord/media_engine/InboundBufferStats;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lco/discord/media_engine/InboundFrameOpStats;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "InboundAudio(packetsReceived="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->packetsReceived:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", packetsLost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/InboundAudio;->mos:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", mosSum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lco/discord/media_engine/InboundAudio;->mosSum:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", mosCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lco/discord/media_engine/InboundAudio;->mosCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mosBuckets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->mosBuckets:[Ljava/lang/Integer;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", bufferStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->bufferStats:Lco/discord/media_engine/InboundBufferStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", frameOpStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lco/discord/media_engine/InboundAudio;->frameOpStats:Lco/discord/media_engine/InboundFrameOpStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
