.class public La0/a/a/a/b$a;
.super Ljava/lang/Object;
.source "CameraHandlerThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = La0/a/a/a/b;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Landroid/hardware/Camera;

.field public final synthetic e:La0/a/a/a/b;


# direct methods
.method public constructor <init>(La0/a/a/a/b;Landroid/hardware/Camera;)V
    .locals 0

    iput-object p1, p0, La0/a/a/a/b$a;->e:La0/a/a/a/b;

    iput-object p2, p0, La0/a/a/a/b$a;->d:Landroid/hardware/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, La0/a/a/a/b$a;->e:La0/a/a/a/b;

    iget-object v1, v0, La0/a/a/a/b;->e:La0/a/a/a/c;

    iget-object v1, v1, La0/a/a/a/c;->d:La0/a/a/a/a;

    iget-object v2, p0, La0/a/a/a/b$a;->d:Landroid/hardware/Camera;

    iget v0, v0, La0/a/a/a/b;->d:I

    if-nez v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v3, La0/a/a/a/e;

    invoke-direct {v3, v2, v0}, La0/a/a/a/e;-><init>(Landroid/hardware/Camera;I)V

    move-object v0, v3

    :goto_0
    invoke-virtual {v1, v0}, La0/a/a/a/a;->setupCameraPreview(La0/a/a/a/e;)V

    return-void
.end method
