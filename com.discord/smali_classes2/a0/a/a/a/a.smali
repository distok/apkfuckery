.class public abstract La0/a/a/a/a;
.super Landroid/widget/FrameLayout;
.source "BarcodeScannerView.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# instance fields
.field public d:La0/a/a/a/e;

.field public e:La0/a/a/a/d;

.field public f:La0/a/a/a/f;

.field public g:Landroid/graphics/Rect;

.field public h:La0/a/a/a/c;

.field public i:Ljava/lang/Boolean;

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public n:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public o:I

.field public p:I

.field public q:I

.field public r:Z

.field public s:I

.field public t:Z

.field public u:F

.field public v:I

.field public w:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, La0/a/a/a/a;->j:Z

    iput-boolean v0, p0, La0/a/a/a/a;->k:Z

    iput-boolean v0, p0, La0/a/a/a/a;->l:Z

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lme/dm7/barcodescanner/core/R$a;->viewfinder_laser:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, La0/a/a/a/a;->m:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lme/dm7/barcodescanner/core/R$a;->viewfinder_border:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, La0/a/a/a/a;->n:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lme/dm7/barcodescanner/core/R$a;->viewfinder_mask:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, La0/a/a/a/a;->o:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lme/dm7/barcodescanner/core/R$b;->viewfinder_border_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, La0/a/a/a/a;->p:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lme/dm7/barcodescanner/core/R$b;->viewfinder_border_length:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, La0/a/a/a/a;->q:I

    const/4 v1, 0x0

    iput-boolean v1, p0, La0/a/a/a/a;->r:Z

    iput v1, p0, La0/a/a/a/a;->s:I

    iput-boolean v1, p0, La0/a/a/a/a;->t:Z

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, La0/a/a/a/a;->u:F

    iput v1, p0, La0/a/a/a/a;->v:I

    const v2, 0x3dcccccd    # 0.1f

    iput v2, p0, La0/a/a/a/a;->w:F

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView:[I

    invoke-virtual {p1, p2, v2, v1, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_shouldScaleToFill:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    invoke-virtual {p0, p2}, La0/a/a/a/a;->setShouldScaleToFill(Z)V

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_laserEnabled:I

    iget-boolean v0, p0, La0/a/a/a/a;->l:Z

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, La0/a/a/a/a;->l:Z

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_laserColor:I

    iget v0, p0, La0/a/a/a/a;->m:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, La0/a/a/a/a;->m:I

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_borderColor:I

    iget v0, p0, La0/a/a/a/a;->n:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, La0/a/a/a/a;->n:I

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_maskColor:I

    iget v0, p0, La0/a/a/a/a;->o:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, La0/a/a/a/a;->o:I

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_borderWidth:I

    iget v0, p0, La0/a/a/a/a;->p:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, La0/a/a/a/a;->p:I

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_borderLength:I

    iget v0, p0, La0/a/a/a/a;->q:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, La0/a/a/a/a;->q:I

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_roundedCorner:I

    iget-boolean v0, p0, La0/a/a/a/a;->r:Z

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, La0/a/a/a/a;->r:Z

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_cornerRadius:I

    iget v0, p0, La0/a/a/a/a;->s:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, La0/a/a/a/a;->s:I

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_squaredFinder:I

    iget-boolean v0, p0, La0/a/a/a/a;->t:Z

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, La0/a/a/a/a;->t:Z

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_borderAlpha:I

    iget v0, p0, La0/a/a/a/a;->u:F

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, La0/a/a/a/a;->u:F

    sget p2, Lme/dm7/barcodescanner/core/R$c;->BarcodeScannerView_finderOffset:I

    iget v0, p0, La0/a/a/a/a;->v:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, La0/a/a/a/a;->v:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance p2, La0/a/a/a/g;

    invoke-direct {p2, p1}, La0/a/a/a/g;-><init>(Landroid/content/Context;)V

    iget p1, p0, La0/a/a/a/a;->n:I

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setBorderColor(I)V

    iget p1, p0, La0/a/a/a/a;->m:I

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setLaserColor(I)V

    iget-boolean p1, p0, La0/a/a/a/a;->l:Z

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setLaserEnabled(Z)V

    iget p1, p0, La0/a/a/a/a;->p:I

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setBorderStrokeWidth(I)V

    iget p1, p0, La0/a/a/a/a;->q:I

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setBorderLineLength(I)V

    iget p1, p0, La0/a/a/a/a;->o:I

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setMaskColor(I)V

    iget-boolean p1, p0, La0/a/a/a/a;->r:Z

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setBorderCornerRounded(Z)V

    iget p1, p0, La0/a/a/a/a;->s:I

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setBorderCornerRadius(I)V

    iget-boolean p1, p0, La0/a/a/a/a;->t:Z

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setSquareViewFinder(Z)V

    iget p1, p0, La0/a/a/a/a;->v:I

    invoke-virtual {p2, p1}, La0/a/a/a/g;->setViewFinderOffset(I)V

    iput-object p2, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    invoke-virtual {v0}, La0/a/a/a/d;->e()V

    iget-object v0, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    iput-object v1, v0, La0/a/a/a/d;->d:La0/a/a/a/e;

    iput-object v1, v0, La0/a/a/a/d;->j:Landroid/hardware/Camera$PreviewCallback;

    iget-object v0, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    iget-object v0, v0, La0/a/a/a/e;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    iput-object v1, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    :cond_0
    iget-object v0, p0, La0/a/a/a/a;->h:La0/a/a/a/c;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iput-object v1, p0, La0/a/a/a/a;->h:La0/a/a/a/c;

    :cond_1
    return-void
.end method

.method public getFlash()Z
    .locals 3

    iget-object v0, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, La0/a/a/a/e;->a:Landroid/hardware/Camera;

    invoke-static {v0}, Ly/a/g0;->u(Landroid/hardware/Camera;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    iget-object v0, v0, La0/a/a/a/e;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v0

    const-string v2, "torch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v1
.end method

.method public getRotationCount()I
    .locals 1

    iget-object v0, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    invoke-virtual {v0}, La0/a/a/a/d;->getDisplayOrientation()I

    move-result v0

    div-int/lit8 v0, v0, 0x5a

    return v0
.end method

.method public setAspectTolerance(F)V
    .locals 0

    iput p1, p0, La0/a/a/a/a;->w:F

    return-void
.end method

.method public setAutoFocus(Z)V
    .locals 1

    iput-boolean p1, p0, La0/a/a/a/a;->j:Z

    iget-object v0, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, La0/a/a/a/d;->setAutoFocus(Z)V

    :cond_0
    return-void
.end method

.method public setBorderAlpha(F)V
    .locals 1

    iput p1, p0, La0/a/a/a/a;->u:F

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setBorderAlpha(F)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setBorderColor(I)V
    .locals 1

    iput p1, p0, La0/a/a/a/a;->n:I

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setBorderColor(I)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setBorderCornerRadius(I)V
    .locals 1

    iput p1, p0, La0/a/a/a/a;->s:I

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setBorderCornerRadius(I)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setBorderLineLength(I)V
    .locals 1

    iput p1, p0, La0/a/a/a/a;->q:I

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setBorderLineLength(I)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setBorderStrokeWidth(I)V
    .locals 1

    iput p1, p0, La0/a/a/a/a;->p:I

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setBorderStrokeWidth(I)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setFlash(Z)V
    .locals 2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, La0/a/a/a/a;->i:Ljava/lang/Boolean;

    iget-object v0, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    if-eqz v0, :cond_3

    iget-object v0, v0, La0/a/a/a/e;->a:Landroid/hardware/Camera;

    invoke-static {v0}, Ly/a/g0;->u(Landroid/hardware/Camera;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    iget-object v0, v0, La0/a/a/a/e;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object p1

    const-string v1, "torch"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object p1

    const-string v1, "off"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    :cond_2
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    :goto_0
    iget-object p1, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    iget-object p1, p1, La0/a/a/a/e;->a:Landroid/hardware/Camera;

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    :cond_3
    return-void
.end method

.method public setIsBorderCornerRounded(Z)V
    .locals 1

    iput-boolean p1, p0, La0/a/a/a/a;->r:Z

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setBorderCornerRounded(Z)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setLaserColor(I)V
    .locals 1

    iput p1, p0, La0/a/a/a/a;->m:I

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setLaserColor(I)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setLaserEnabled(Z)V
    .locals 1

    iput-boolean p1, p0, La0/a/a/a/a;->l:Z

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setLaserEnabled(Z)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setMaskColor(I)V
    .locals 1

    iput p1, p0, La0/a/a/a/a;->o:I

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setMaskColor(I)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setShouldScaleToFill(Z)V
    .locals 0

    iput-boolean p1, p0, La0/a/a/a/a;->k:Z

    return-void
.end method

.method public setSquareViewFinder(Z)V
    .locals 1

    iput-boolean p1, p0, La0/a/a/a/a;->t:Z

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0, p1}, La0/a/a/a/f;->setSquareViewFinder(Z)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setupCameraPreview(La0/a/a/a/e;)V
    .locals 0

    iput-object p1, p0, La0/a/a/a/a;->d:La0/a/a/a/e;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, La0/a/a/a/a;->setupLayout(La0/a/a/a/e;)V

    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    check-cast p1, La0/a/a/a/g;

    invoke-virtual {p1}, La0/a/a/a/g;->a()V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    iget-object p1, p0, La0/a/a/a/a;->i:Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, La0/a/a/a/a;->setFlash(Z)V

    :cond_0
    iget-boolean p1, p0, La0/a/a/a/a;->j:Z

    invoke-virtual {p0, p1}, La0/a/a/a/a;->setAutoFocus(Z)V

    :cond_1
    return-void
.end method

.method public final setupLayout(La0/a/a/a/e;)V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->removeAllViews()V

    new-instance v0, La0/a/a/a/d;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, La0/a/a/a/d;-><init>(Landroid/content/Context;La0/a/a/a/e;Landroid/hardware/Camera$PreviewCallback;)V

    iput-object v0, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    iget p1, p0, La0/a/a/a/a;->w:F

    invoke-virtual {v0, p1}, La0/a/a/a/d;->setAspectTolerance(F)V

    iget-object p1, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    iget-boolean v0, p0, La0/a/a/a/a;->k:Z

    invoke-virtual {p1, v0}, La0/a/a/a/d;->setShouldScaleToFill(Z)V

    iget-boolean p1, p0, La0/a/a/a/a;->k:Z

    if-nez p1, :cond_0

    new-instance p1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setGravity(I)V

    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    iget-object v0, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, La0/a/a/a/a;->e:La0/a/a/a/d;

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :goto_0
    iget-object p1, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    instance-of v0, p1, Landroid/view/View;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "IViewFinder object returned by \'createViewFinderView()\' should be instance of android.view.View"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
