.class public final Lx/o/c$a;
.super Lx/o/c;
.source "Random.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lx/o/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lx/o/c;-><init>()V

    return-void
.end method


# virtual methods
.method public nextBits(I)I
    .locals 1

    sget-object v0, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v0, p1}, Lx/o/c;->nextBits(I)I

    move-result p1

    return p1
.end method

.method public nextBytes([B)[B
    .locals 1

    const-string v0, "array"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v0, p1}, Lx/o/c;->nextBytes([B)[B

    move-result-object p1

    return-object p1
.end method

.method public nextBytes([BII)[B
    .locals 1

    const-string v0, "array"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v0, p1, p2, p3}, Lx/o/c;->nextBytes([BII)[B

    move-result-object p1

    return-object p1
.end method

.method public nextInt()I
    .locals 1

    sget-object v0, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v0}, Lx/o/c;->nextInt()I

    move-result v0

    return v0
.end method

.method public nextInt(I)I
    .locals 1

    sget-object v0, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v0, p1}, Lx/o/c;->nextInt(I)I

    move-result p1

    return p1
.end method

.method public nextInt(II)I
    .locals 1

    sget-object v0, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v0, p1, p2}, Lx/o/c;->nextInt(II)I

    move-result p1

    return p1
.end method

.method public nextLong()J
    .locals 2

    sget-object v0, Lx/o/c;->a:Lx/o/c;

    invoke-virtual {v0}, Lx/o/c;->nextLong()J

    move-result-wide v0

    return-wide v0
.end method
