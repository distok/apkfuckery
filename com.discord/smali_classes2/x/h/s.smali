.class public final enum Lx/h/s;
.super Ljava/lang/Enum;
.source "AbstractIterator.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lx/h/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lx/h/s;

.field public static final enum e:Lx/h/s;

.field public static final enum f:Lx/h/s;

.field public static final enum g:Lx/h/s;

.field public static final synthetic h:[Lx/h/s;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lx/h/s;

    new-instance v1, Lx/h/s;

    const-string v2, "Ready"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lx/h/s;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx/h/s;->d:Lx/h/s;

    aput-object v1, v0, v3

    new-instance v1, Lx/h/s;

    const-string v2, "NotReady"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lx/h/s;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx/h/s;->e:Lx/h/s;

    aput-object v1, v0, v3

    new-instance v1, Lx/h/s;

    const-string v2, "Done"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lx/h/s;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx/h/s;->f:Lx/h/s;

    aput-object v1, v0, v3

    new-instance v1, Lx/h/s;

    const-string v2, "Failed"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lx/h/s;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx/h/s;->g:Lx/h/s;

    aput-object v1, v0, v3

    sput-object v0, Lx/h/s;->h:[Lx/h/s;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lx/h/s;
    .locals 1

    const-class v0, Lx/h/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lx/h/s;

    return-object p0
.end method

.method public static values()[Lx/h/s;
    .locals 1

    sget-object v0, Lx/h/s;->h:[Lx/h/s;

    invoke-virtual {v0}, [Lx/h/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lx/h/s;

    return-object v0
.end method
