.class public final Lx/r/t$a;
.super Ljava/lang/Object;
.source "Sequences.kt"

# interfaces
.implements Ljava/util/Iterator;
.implements Lx/m/c/x/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lx/r/t;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;",
        "Lx/m/c/x/a;"
    }
.end annotation


# instance fields
.field public final d:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation
.end field

.field public e:I

.field public final synthetic f:Lx/r/t;


# direct methods
.method public constructor <init>(Lx/r/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lx/r/t$a;->f:Lx/r/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object p1, p1, Lx/r/t;->a:Lkotlin/sequences/Sequence;

    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    iput-object p1, p0, Lx/r/t$a;->d:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    :goto_0
    iget v0, p0, Lx/r/t$a;->e:I

    iget-object v1, p0, Lx/r/t$a;->f:Lx/r/t;

    iget v1, v1, Lx/r/t;->b:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lx/r/t$a;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lx/r/t$a;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    iget v0, p0, Lx/r/t$a;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lx/r/t$a;->e:I

    goto :goto_0

    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 2

    invoke-virtual {p0}, Lx/r/t$a;->a()V

    iget v0, p0, Lx/r/t$a;->e:I

    iget-object v1, p0, Lx/r/t$a;->f:Lx/r/t;

    iget v1, v1, Lx/r/t;->c:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lx/r/t$a;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lx/r/t$a;->a()V

    iget v0, p0, Lx/r/t$a;->e:I

    iget-object v1, p0, Lx/r/t$a;->f:Lx/r/t;

    iget v1, v1, Lx/r/t;->c:I

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lx/r/t$a;->e:I

    iget-object v0, p0, Lx/r/t$a;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Operation is not supported for read-only collection"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
