.class public final Lx/s/b$a;
.super Ljava/lang/Object;
.source "Strings.kt"

# interfaces
.implements Ljava/util/Iterator;
.implements Lx/m/c/x/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lx/s/b;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Lkotlin/ranges/IntRange;",
        ">;",
        "Lx/m/c/x/a;"
    }
.end annotation


# instance fields
.field public d:I

.field public e:I

.field public f:I

.field public g:Lkotlin/ranges/IntRange;

.field public h:I

.field public final synthetic i:Lx/s/b;


# direct methods
.method public constructor <init>(Lx/s/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lx/s/b$a;->i:Lx/s/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lx/s/b$a;->d:I

    iget v0, p1, Lx/s/b;->b:I

    iget-object p1, p1, Lx/s/b;->a:Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lx/p/e;->coerceIn(III)I

    move-result p1

    iput p1, p0, Lx/s/b$a;->e:I

    iput p1, p0, Lx/s/b$a;->f:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    iget v0, p0, Lx/s/b$a;->f:I

    const/4 v1, 0x0

    if-gez v0, :cond_0

    iput v1, p0, Lx/s/b$a;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lx/s/b$a;->g:Lkotlin/ranges/IntRange;

    goto/16 :goto_1

    :cond_0
    iget-object v2, p0, Lx/s/b$a;->i:Lx/s/b;

    iget v3, v2, Lx/s/b;->c:I

    const/4 v4, -0x1

    const/4 v5, 0x1

    if-lez v3, :cond_1

    iget v6, p0, Lx/s/b$a;->h:I

    add-int/2addr v6, v5

    iput v6, p0, Lx/s/b$a;->h:I

    if-ge v6, v3, :cond_2

    :cond_1
    iget-object v2, v2, Lx/s/b;->a:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v0, v2, :cond_3

    :cond_2
    iget v0, p0, Lx/s/b$a;->e:I

    new-instance v1, Lkotlin/ranges/IntRange;

    iget-object v2, p0, Lx/s/b$a;->i:Lx/s/b;

    iget-object v2, v2, Lx/s/b;->a:Ljava/lang/CharSequence;

    invoke-static {v2}, Lx/s/r;->getLastIndex(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-direct {v1, v0, v2}, Lkotlin/ranges/IntRange;-><init>(II)V

    iput-object v1, p0, Lx/s/b$a;->g:Lkotlin/ranges/IntRange;

    iput v4, p0, Lx/s/b$a;->f:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lx/s/b$a;->i:Lx/s/b;

    iget-object v2, v0, Lx/s/b;->d:Lkotlin/jvm/functions/Function2;

    iget-object v0, v0, Lx/s/b;->a:Ljava/lang/CharSequence;

    iget v3, p0, Lx/s/b$a;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Pair;

    if-nez v0, :cond_4

    iget v0, p0, Lx/s/b$a;->e:I

    new-instance v1, Lkotlin/ranges/IntRange;

    iget-object v2, p0, Lx/s/b$a;->i:Lx/s/b;

    iget-object v2, v2, Lx/s/b;->a:Ljava/lang/CharSequence;

    invoke-static {v2}, Lx/s/r;->getLastIndex(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-direct {v1, v0, v2}, Lkotlin/ranges/IntRange;-><init>(II)V

    iput-object v1, p0, Lx/s/b$a;->g:Lkotlin/ranges/IntRange;

    iput v4, p0, Lx/s/b$a;->f:I

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iget v3, p0, Lx/s/b$a;->e:I

    invoke-static {v3, v2}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    iput-object v3, p0, Lx/s/b$a;->g:Lkotlin/ranges/IntRange;

    add-int/2addr v2, v0

    iput v2, p0, Lx/s/b$a;->e:I

    if-nez v0, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v2, v1

    iput v2, p0, Lx/s/b$a;->f:I

    :goto_0
    iput v5, p0, Lx/s/b$a;->d:I

    :goto_1
    return-void
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lx/s/b$a;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lx/s/b$a;->a()V

    :cond_0
    iget v0, p0, Lx/s/b$a;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public next()Ljava/lang/Object;
    .locals 3

    iget v0, p0, Lx/s/b$a;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lx/s/b$a;->a()V

    :cond_0
    iget v0, p0, Lx/s/b$a;->d:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lx/s/b$a;->g:Lkotlin/ranges/IntRange;

    const-string v2, "null cannot be cast to non-null type kotlin.ranges.IntRange"

    invoke-static {v0, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v2, 0x0

    iput-object v2, p0, Lx/s/b$a;->g:Lkotlin/ranges/IntRange;

    iput v1, p0, Lx/s/b$a;->d:I

    return-object v0

    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Operation is not supported for read-only collection"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
