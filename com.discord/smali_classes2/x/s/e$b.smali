.class public final Lx/s/e$b;
.super Lx/h/a;
.source "Regex.kt"

# interfaces
.implements Lx/s/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lx/s/e;-><init>(Ljava/util/regex/Matcher;Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/h/a<",
        "Lx/s/c;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lx/s/e;


# direct methods
.method public constructor <init>(Lx/s/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lx/s/e$b;->d:Lx/s/e;

    invoke-direct {p0}, Lx/h/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge contains(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_0

    instance-of v0, p1, Lx/s/c;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    check-cast p1, Lx/s/c;

    invoke-super {p0, p1}, Lx/h/a;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public getSize()I
    .locals 1

    iget-object v0, p0, Lx/s/e$b;->d:Lx/s/e;

    iget-object v0, v0, Lx/s/e;->c:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lx/s/c;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Lx/h/f;->getIndices(Ljava/util/Collection;)Lkotlin/ranges/IntRange;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    new-instance v1, Lx/s/e$b$a;

    invoke-direct {v1, p0}, Lx/s/e$b$a;-><init>(Lx/s/e$b;)V

    invoke-static {v0, v1}, Lx/r/q;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    check-cast v0, Lx/r/v;

    new-instance v1, Lx/r/v$a;

    invoke-direct {v1, v0}, Lx/r/v$a;-><init>(Lx/r/v;)V

    return-object v1
.end method
