.class public final enum Lx/s/f;
.super Ljava/lang/Enum;
.source "Regex.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lx/s/f;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final enum d:Lx/s/f;

.field public static final enum e:Lx/s/f;

.field public static final enum f:Lx/s/f;

.field public static final synthetic g:[Lx/s/f;


# instance fields
.field private final mask:I

.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x7

    new-array v0, v0, [Lx/s/f;

    new-instance v7, Lx/s/f;

    const-string v2, "IGNORE_CASE"

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x2

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lx/s/f;-><init>(Ljava/lang/String;IIII)V

    sput-object v7, Lx/s/f;->d:Lx/s/f;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lx/s/f;

    const-string v9, "MULTILINE"

    const/4 v10, 0x1

    const/16 v11, 0x8

    const/4 v12, 0x0

    const/4 v13, 0x2

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lx/s/f;-><init>(Ljava/lang/String;IIII)V

    sput-object v1, Lx/s/f;->e:Lx/s/f;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lx/s/f;

    const-string v4, "LITERAL"

    const/4 v5, 0x2

    const/16 v6, 0x10

    const/4 v7, 0x0

    const/4 v8, 0x2

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lx/s/f;-><init>(Ljava/lang/String;IIII)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lx/s/f;

    const-string v4, "UNIX_LINES"

    const/4 v5, 0x3

    const/4 v6, 0x1

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lx/s/f;-><init>(Ljava/lang/String;IIII)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lx/s/f;

    const-string v4, "COMMENTS"

    const/4 v5, 0x4

    const/4 v6, 0x4

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lx/s/f;-><init>(Ljava/lang/String;IIII)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lx/s/f;

    const-string v4, "DOT_MATCHES_ALL"

    const/4 v5, 0x5

    const/16 v6, 0x20

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lx/s/f;-><init>(Ljava/lang/String;IIII)V

    sput-object v1, Lx/s/f;->f:Lx/s/f;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lx/s/f;

    const-string v4, "CANON_EQ"

    const/4 v5, 0x6

    const/16 v6, 0x80

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lx/s/f;-><init>(Ljava/lang/String;IIII)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lx/s/f;->g:[Lx/s/f;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_0

    move p4, p3

    :cond_0
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lx/s/f;->value:I

    iput p4, p0, Lx/s/f;->mask:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lx/s/f;
    .locals 1

    const-class v0, Lx/s/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lx/s/f;

    return-object p0
.end method

.method public static values()[Lx/s/f;
    .locals 1

    sget-object v0, Lx/s/f;->g:[Lx/s/f;

    invoke-virtual {v0}, [Lx/s/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lx/s/f;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lx/s/f;->value:I

    return v0
.end method
