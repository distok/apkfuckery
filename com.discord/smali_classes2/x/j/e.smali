.class public final Lx/j/e;
.super Lx/m/c/k;
.source "CoroutineContext.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlin/coroutines/CoroutineContext;",
        "Lkotlin/coroutines/CoroutineContext$a;",
        "Lkotlin/coroutines/CoroutineContext;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lx/j/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lx/j/e;

    invoke-direct {v0}, Lx/j/e;-><init>()V

    sput-object v0, Lx/j/e;->d:Lx/j/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lkotlin/coroutines/CoroutineContext;

    check-cast p2, Lkotlin/coroutines/CoroutineContext$a;

    const-string v0, "acc"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "element"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Lkotlin/coroutines/CoroutineContext$a;->getKey()Lkotlin/coroutines/CoroutineContext$b;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/coroutines/CoroutineContext;->minusKey(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    sget-object v0, Lx/j/f;->d:Lx/j/f;

    if-ne p1, v0, :cond_0

    goto :goto_1

    :cond_0
    sget v1, Lx/j/d;->a:I

    sget-object v1, Lx/j/d$a;->a:Lx/j/d$a;

    invoke-interface {p1, v1}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object v2

    check-cast v2, Lx/j/d;

    if-nez v2, :cond_1

    new-instance v0, Lx/j/c;

    invoke-direct {v0, p1, p2}, Lx/j/c;-><init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/CoroutineContext$a;)V

    :goto_0
    move-object p2, v0

    goto :goto_1

    :cond_1
    invoke-interface {p1, v1}, Lkotlin/coroutines/CoroutineContext;->minusKey(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    if-ne p1, v0, :cond_2

    new-instance p1, Lx/j/c;

    invoke-direct {p1, p2, v2}, Lx/j/c;-><init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/CoroutineContext$a;)V

    move-object p2, p1

    goto :goto_1

    :cond_2
    new-instance v0, Lx/j/c;

    new-instance v1, Lx/j/c;

    invoke-direct {v1, p1, p2}, Lx/j/c;-><init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/CoroutineContext$a;)V

    invoke-direct {v0, v1, v2}, Lx/j/c;-><init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/CoroutineContext$a;)V

    goto :goto_0

    :goto_1
    return-object p2
.end method
