.class public final enum Lx/j/g/a;
.super Ljava/lang/Enum;
.source "Intrinsics.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lx/j/g/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lx/j/g/a;

.field public static final synthetic e:[Lx/j/g/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lx/j/g/a;

    new-instance v1, Lx/j/g/a;

    const-string v2, "COROUTINE_SUSPENDED"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lx/j/g/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx/j/g/a;->d:Lx/j/g/a;

    aput-object v1, v0, v3

    new-instance v1, Lx/j/g/a;

    const-string v2, "UNDECIDED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lx/j/g/a;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v3

    new-instance v1, Lx/j/g/a;

    const-string v2, "RESUMED"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lx/j/g/a;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v3

    sput-object v0, Lx/j/g/a;->e:[Lx/j/g/a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lx/j/g/a;
    .locals 1

    const-class v0, Lx/j/g/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lx/j/g/a;

    return-object p0
.end method

.method public static values()[Lx/j/g/a;
    .locals 1

    sget-object v0, Lx/j/g/a;->e:[Lx/j/g/a;

    invoke-virtual {v0}, [Lx/j/g/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lx/j/g/a;

    return-object v0
.end method
