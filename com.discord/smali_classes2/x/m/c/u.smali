.class public Lx/m/c/u;
.super Ljava/lang/Object;
.source "Reflection.java"


# static fields
.field public static final a:Lx/m/c/v;

.field public static final b:[Lx/q/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "kotlin.reflect.jvm.internal.ReflectionFactoryImpl"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lx/m/c/v;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Lx/m/c/v;

    invoke-direct {v0}, Lx/m/c/v;-><init>()V

    :goto_1
    sput-object v0, Lx/m/c/u;->a:Lx/m/c/v;

    const/4 v0, 0x0

    new-array v0, v0, [Lx/q/b;

    sput-object v0, Lx/m/c/u;->b:[Lx/q/b;

    return-void
.end method

.method public static getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;
    .locals 1

    sget-object v0, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lx/m/c/e;

    invoke-direct {v0, p0}, Lx/m/c/e;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method
