.class public abstract Lx/m/c/n;
.super Lx/m/c/p;
.source "MutablePropertyReference1.java"

# interfaces
.implements Lx/q/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lx/m/c/p;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lx/m/c/p;-><init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public computeReflected()Lkotlin/reflect/KCallable;
    .locals 1

    sget-object v0, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public getGetter()Lx/q/d$a;
    .locals 1

    invoke-virtual {p0}, Lx/m/c/t;->getReflected()Lkotlin/reflect/KProperty;

    move-result-object v0

    check-cast v0, Lx/q/c;

    invoke-interface {v0}, Lx/q/d;->getGetter()Lx/q/d$a;

    move-result-object v0

    return-object v0
.end method

.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    move-object v0, p0

    check-cast v0, Lx/m/c/o;

    invoke-virtual {v0}, Lx/m/c/n;->getGetter()Lx/q/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, v1}, Lkotlin/reflect/KCallable;->call([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
