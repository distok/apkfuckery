.class public Lx/l/d;
.super Lx/l/c;
.source "Utils.kt"


# direct methods
.method public static final deleteRecursively(Ljava/io/File;)Z
    .locals 3

    const-string v0, "$this$deleteRecursively"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$this$walkBottomUp"

    invoke-static {p0, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lx/l/b;->e:Lx/l/b;

    const-string v1, "$this$walk"

    invoke-static {p0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "direction"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lx/l/a;

    invoke-direct {v1, p0, v0}, Lx/l/a;-><init>(Ljava/io/File;Lx/l/b;)V

    new-instance p0, Lx/l/a$b;

    invoke-direct {p0, v1}, Lx/l/a$b;-><init>(Lx/l/a;)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0}, Lx/h/b;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lx/h/b;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    return v0
.end method
