.class public final enum Lx/l/b;
.super Ljava/lang/Enum;
.source "FileTreeWalk.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lx/l/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lx/l/b;

.field public static final enum e:Lx/l/b;

.field public static final synthetic f:[Lx/l/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lx/l/b;

    new-instance v1, Lx/l/b;

    const-string v2, "TOP_DOWN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lx/l/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx/l/b;->d:Lx/l/b;

    aput-object v1, v0, v3

    new-instance v1, Lx/l/b;

    const-string v2, "BOTTOM_UP"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lx/l/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx/l/b;->e:Lx/l/b;

    aput-object v1, v0, v3

    sput-object v0, Lx/l/b;->f:[Lx/l/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lx/l/b;
    .locals 1

    const-class v0, Lx/l/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lx/l/b;

    return-object p0
.end method

.method public static values()[Lx/l/b;
    .locals 1

    sget-object v0, Lx/l/b;->f:[Lx/l/b;

    invoke-virtual {v0}, [Lx/l/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lx/l/b;

    return-object v0
.end method
