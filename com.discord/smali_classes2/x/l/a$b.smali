.class public final Lx/l/a$b;
.super Lx/h/b;
.source "FileTreeWalk.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lx/l/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx/l/a$b$a;,
        Lx/l/a$b$c;,
        Lx/l/a$b$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/h/b<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field public final f:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lx/l/a$c;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic g:Lx/l/a;


# direct methods
.method public constructor <init>(Lx/l/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lx/l/a$b;->g:Lx/l/a;

    invoke-direct {p0}, Lx/h/b;-><init>()V

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lx/l/a$b;->f:Ljava/util/ArrayDeque;

    iget-object v1, p1, Lx/l/a;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p1, Lx/l/a;->a:Ljava/io/File;

    invoke-virtual {p0, p1}, Lx/l/a$b;->b(Ljava/io/File;)Lx/l/a$a;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lx/l/a;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lx/l/a$b$b;

    iget-object p1, p1, Lx/l/a;->a:Ljava/io/File;

    invoke-direct {v1, p0, p1}, Lx/l/a$b$b;-><init>(Lx/l/a$b;Ljava/io/File;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    sget-object p1, Lx/h/s;->f:Lx/h/s;

    iput-object p1, p0, Lx/h/b;->d:Lx/h/s;

    :goto_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    :goto_0
    iget-object v0, p0, Lx/l/a$b;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l/a$c;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lx/l/a$c;->step()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lx/l/a$b;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lx/l/a$c;->a:Ljava/io/File;

    invoke-static {v1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lx/l/a$b;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v0

    iget-object v2, p0, Lx/l/a$b;->g:Lx/l/a;

    iget v2, v2, Lx/l/a;->c:I

    if-lt v0, v2, :cond_1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lx/l/a$b;->f:Ljava/util/ArrayDeque;

    invoke-virtual {p0, v1}, Lx/l/a$b;->b(Ljava/io/File;)Lx/l/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    iput-object v1, p0, Lx/h/b;->e:Ljava/lang/Object;

    sget-object v0, Lx/h/s;->d:Lx/h/s;

    iput-object v0, p0, Lx/h/b;->d:Lx/h/s;

    goto :goto_2

    :cond_4
    sget-object v0, Lx/h/s;->f:Lx/h/s;

    iput-object v0, p0, Lx/h/b;->d:Lx/h/s;

    :goto_2
    return-void
.end method

.method public final b(Ljava/io/File;)Lx/l/a$a;
    .locals 2

    iget-object v0, p0, Lx/l/a$b;->g:Lx/l/a;

    iget-object v0, v0, Lx/l/a;->b:Lx/l/b;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lx/l/a$b$a;

    invoke-direct {v0, p0, p1}, Lx/l/a$b$a;-><init>(Lx/l/a$b;Ljava/io/File;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    new-instance v0, Lx/l/a$b$c;

    invoke-direct {v0, p0, p1}, Lx/l/a$b$c;-><init>(Lx/l/a$b;Ljava/io/File;)V

    :goto_0
    return-object v0
.end method
