.class public final Lx/l/a;
.super Ljava/lang/Object;
.source "FileTreeWalk.kt"

# interfaces
.implements Lkotlin/sequences/Sequence;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx/l/a$c;,
        Lx/l/a$a;,
        Lx/l/a$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/sequences/Sequence<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Lx/l/b;

.field public final c:I


# direct methods
.method public constructor <init>(Ljava/io/File;Lx/l/b;)V
    .locals 1

    const-string v0, "start"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx/l/a;->a:Ljava/io/File;

    iput-object p2, p0, Lx/l/a;->b:Lx/l/b;

    const p1, 0x7fffffff

    iput p1, p0, Lx/l/a;->c:I

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    new-instance v0, Lx/l/a$b;

    invoke-direct {v0, p0}, Lx/l/a$b;-><init>(Lx/l/a;)V

    return-object v0
.end method
