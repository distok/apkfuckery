.class public final Lc0/b$a;
.super Ljava/lang/Object;
.source "AsyncTimeout.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc0/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lc0/b;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-class v0, Lc0/b;

    sget-object v1, Lc0/b;->j:Lc0/b;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    iget-object v1, v1, Lc0/b;->f:Lc0/b;

    if-nez v1, :cond_2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    sget-wide v5, Lc0/b;->h:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/Object;->wait(J)V

    sget-object v0, Lc0/b;->j:Lc0/b;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lc0/b;->f:Lc0/b;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long/2addr v0, v3

    sget-wide v3, Lc0/b;->i:J

    cmp-long v5, v0, v3

    if-ltz v5, :cond_0

    sget-object v2, Lc0/b;->j:Lc0/b;

    :cond_0
    return-object v2

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iget-wide v5, v1, Lc0/b;->g:J

    sub-long/2addr v5, v3

    const-wide/16 v3, 0x0

    cmp-long v7, v5, v3

    if-lez v7, :cond_3

    const-wide/32 v3, 0xf4240

    div-long v7, v5, v3

    mul-long v3, v3, v7

    sub-long/2addr v5, v3

    long-to-int v1, v5

    invoke-virtual {v0, v7, v8, v1}, Ljava/lang/Object;->wait(JI)V

    return-object v2

    :cond_3
    sget-object v0, Lc0/b;->j:Lc0/b;

    if-eqz v0, :cond_4

    iget-object v3, v1, Lc0/b;->f:Lc0/b;

    iput-object v3, v0, Lc0/b;->f:Lc0/b;

    iput-object v2, v1, Lc0/b;->f:Lc0/b;

    return-object v1

    :cond_4
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_5
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2
.end method
