.class public Lc0/k;
.super Lc0/y;
.source "ForwardingTimeout.kt"


# instance fields
.field public e:Lc0/y;


# direct methods
.method public constructor <init>(Lc0/y;)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lc0/y;-><init>()V

    iput-object p1, p0, Lc0/k;->e:Lc0/y;

    return-void
.end method


# virtual methods
.method public a()Lc0/y;
    .locals 1

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0}, Lc0/y;->a()Lc0/y;

    move-result-object v0

    return-object v0
.end method

.method public b()Lc0/y;
    .locals 1

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0}, Lc0/y;->b()Lc0/y;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0}, Lc0/y;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public d(J)Lc0/y;
    .locals 1

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0, p1, p2}, Lc0/y;->d(J)Lc0/y;

    move-result-object p1

    return-object p1
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0}, Lc0/y;->e()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0}, Lc0/y;->f()V

    return-void
.end method

.method public g(JLjava/util/concurrent/TimeUnit;)Lc0/y;
    .locals 1

    const-string v0, "unit"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0, p1, p2, p3}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    move-result-object p1

    return-object p1
.end method

.method public h()J
    .locals 2

    iget-object v0, p0, Lc0/k;->e:Lc0/y;

    invoke-virtual {v0}, Lc0/y;->h()J

    move-result-wide v0

    return-wide v0
.end method
