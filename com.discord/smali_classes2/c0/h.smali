.class public final Lc0/h;
.super Ljava/lang/Object;
.source "DeflaterSink.kt"

# interfaces
.implements Lc0/v;


# instance fields
.field public d:Z

.field public final e:Lokio/BufferedSink;

.field public final f:Ljava/util/zip/Deflater;


# direct methods
.method public constructor <init>(Lc0/v;Ljava/util/zip/Deflater;)V
    .locals 3

    const-string v0, "sink"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "deflater"

    invoke-static {p2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "$this$buffer"

    invoke-static {p1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lc0/q;

    invoke-direct {v2, p1}, Lc0/q;-><init>(Lc0/v;)V

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lc0/h;->e:Lokio/BufferedSink;

    iput-object p2, p0, Lc0/h;->f:Ljava/util/zip/Deflater;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 7
    .annotation build Lorg/codehaus/mojo/animal_sniffer/IgnoreJRERequirement;
    .end annotation

    iget-object v0, p0, Lc0/h;->e:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->h()Lc0/e;

    move-result-object v0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lc0/e;->F(I)Lc0/s;

    move-result-object v1

    if-eqz p1, :cond_1

    iget-object v2, p0, Lc0/h;->f:Ljava/util/zip/Deflater;

    iget-object v3, v1, Lc0/s;->a:[B

    iget v4, v1, Lc0/s;->c:I

    rsub-int v5, v4, 0x2000

    const/4 v6, 0x2

    invoke-virtual {v2, v3, v4, v5, v6}, Ljava/util/zip/Deflater;->deflate([BIII)I

    move-result v2

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lc0/h;->f:Ljava/util/zip/Deflater;

    iget-object v3, v1, Lc0/s;->a:[B

    iget v4, v1, Lc0/s;->c:I

    rsub-int v5, v4, 0x2000

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/zip/Deflater;->deflate([BII)I

    move-result v2

    :goto_1
    if-lez v2, :cond_2

    iget v3, v1, Lc0/s;->c:I

    add-int/2addr v3, v2

    iput v3, v1, Lc0/s;->c:I

    iget-wide v3, v0, Lc0/e;->e:J

    int-to-long v1, v2

    add-long/2addr v3, v1

    iput-wide v3, v0, Lc0/e;->e:J

    iget-object v1, p0, Lc0/h;->e:Lokio/BufferedSink;

    invoke-interface {v1}, Lokio/BufferedSink;->Q()Lokio/BufferedSink;

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lc0/h;->f:Ljava/util/zip/Deflater;

    invoke-virtual {v2}, Ljava/util/zip/Deflater;->needsInput()Z

    move-result v2

    if-eqz v2, :cond_0

    iget p1, v1, Lc0/s;->b:I

    iget v2, v1, Lc0/s;->c:I

    if-ne p1, v2, :cond_3

    invoke-virtual {v1}, Lc0/s;->a()Lc0/s;

    move-result-object p1

    iput-object p1, v0, Lc0/e;->d:Lc0/s;

    invoke-static {v1}, Lc0/t;->a(Lc0/s;)V

    :cond_3
    return-void
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lc0/h;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lc0/h;->f:Ljava/util/zip/Deflater;

    invoke-virtual {v1}, Ljava/util/zip/Deflater;->finish()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lc0/h;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_0
    :try_start_1
    iget-object v1, p0, Lc0/h;->f:Ljava/util/zip/Deflater;

    invoke-virtual {v1}, Ljava/util/zip/Deflater;->end()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v1

    if-nez v0, :cond_1

    move-object v0, v1

    :cond_1
    :goto_1
    :try_start_2
    iget-object v1, p0, Lc0/h;->e:Lokio/BufferedSink;

    invoke-interface {v1}, Lc0/v;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v1

    if-nez v0, :cond_2

    move-object v0, v1

    :cond_2
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lc0/h;->d:Z

    if-nez v0, :cond_3

    return-void

    :cond_3
    throw v0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lc0/h;->a(Z)V

    iget-object v0, p0, Lc0/h;->e:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->flush()V

    return-void
.end method

.method public timeout()Lc0/y;
    .locals 1

    iget-object v0, p0, Lc0/h;->e:Lokio/BufferedSink;

    invoke-interface {v0}, Lc0/v;->timeout()Lc0/y;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "DeflaterSink("

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lc0/h;->e:Lokio/BufferedSink;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lc0/e;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v1, p1, Lc0/e;->e:J

    const-wide/16 v3, 0x0

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Ly/a/g0;->m(JJJ)V

    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-lez v2, :cond_2

    iget-object v0, p1, Lc0/e;->d:Lc0/s;

    if-eqz v0, :cond_1

    iget v1, v0, Lc0/s;->c:I

    iget v2, v0, Lc0/s;->b:I

    sub-int/2addr v1, v2

    int-to-long v1, v1

    invoke-static {p2, p3, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v2, v1

    iget-object v1, p0, Lc0/h;->f:Ljava/util/zip/Deflater;

    iget-object v3, v0, Lc0/s;->a:[B

    iget v4, v0, Lc0/s;->b:I

    invoke-virtual {v1, v3, v4, v2}, Ljava/util/zip/Deflater;->setInput([BII)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lc0/h;->a(Z)V

    iget-wide v3, p1, Lc0/e;->e:J

    int-to-long v5, v2

    sub-long/2addr v3, v5

    iput-wide v3, p1, Lc0/e;->e:J

    iget v1, v0, Lc0/s;->b:I

    add-int/2addr v1, v2

    iput v1, v0, Lc0/s;->b:I

    iget v2, v0, Lc0/s;->c:I

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lc0/s;->a()Lc0/s;

    move-result-object v1

    iput-object v1, p1, Lc0/e;->d:Lc0/s;

    invoke-static {v0}, Lc0/t;->a(Lc0/s;)V

    :cond_0
    sub-long/2addr p2, v5

    goto :goto_0

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_2
    return-void
.end method
