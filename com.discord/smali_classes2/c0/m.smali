.class public final Lc0/m;
.super Ljava/lang/Object;
.source "InflaterSource.kt"

# interfaces
.implements Lc0/x;


# instance fields
.field public d:I

.field public e:Z

.field public final f:Lc0/g;

.field public final g:Ljava/util/zip/Inflater;


# direct methods
.method public constructor <init>(Lc0/g;Ljava/util/zip/Inflater;)V
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc0/m;->f:Lc0/g;

    iput-object p2, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    return-void
.end method

.method public constructor <init>(Lc0/x;Ljava/util/zip/Inflater;)V
    .locals 3

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "inflater"

    invoke-static {p2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "$this$buffer"

    invoke-static {p1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lc0/r;

    invoke-direct {v2, p1}, Lc0/r;-><init>(Lc0/x;)V

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lc0/m;->f:Lc0/g;

    iput-object p2, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    return-void
.end method


# virtual methods
.method public final a(Lc0/e;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    cmp-long v3, p2, v1

    if-ltz v3, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_6

    iget-boolean v4, p0, Lc0/m;->e:Z

    xor-int/2addr v4, v0

    if-eqz v4, :cond_5

    if-nez v3, :cond_1

    return-wide v1

    :cond_1
    :try_start_0
    invoke-virtual {p1, v0}, Lc0/e;->F(I)Lc0/s;

    move-result-object v0

    iget v3, v0, Lc0/s;->c:I

    rsub-int v3, v3, 0x2000

    int-to-long v3, v3

    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    long-to-int p3, p2

    invoke-virtual {p0}, Lc0/m;->b()Z

    iget-object p2, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    iget-object v3, v0, Lc0/s;->a:[B

    iget v4, v0, Lc0/s;->c:I

    invoke-virtual {p2, v3, v4, p3}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result p2

    iget p3, p0, Lc0/m;->d:I

    if-nez p3, :cond_2

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    invoke-virtual {v3}, Ljava/util/zip/Inflater;->getRemaining()I

    move-result v3

    sub-int/2addr p3, v3

    iget v3, p0, Lc0/m;->d:I

    sub-int/2addr v3, p3

    iput v3, p0, Lc0/m;->d:I

    iget-object v3, p0, Lc0/m;->f:Lc0/g;

    int-to-long v4, p3

    invoke-interface {v3, v4, v5}, Lc0/g;->skip(J)V

    :goto_1
    if-lez p2, :cond_3

    iget p3, v0, Lc0/s;->c:I

    add-int/2addr p3, p2

    iput p3, v0, Lc0/s;->c:I

    iget-wide v0, p1, Lc0/e;->e:J

    int-to-long p2, p2

    add-long/2addr v0, p2

    iput-wide v0, p1, Lc0/e;->e:J

    return-wide p2

    :cond_3
    iget p2, v0, Lc0/s;->b:I

    iget p3, v0, Lc0/s;->c:I

    if-ne p2, p3, :cond_4

    invoke-virtual {v0}, Lc0/s;->a()Lc0/s;

    move-result-object p2

    iput-object p2, p1, Lc0/e;->d:Lc0/s;

    invoke-static {v0}, Lc0/t;->a(Lc0/s;)V
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-wide v1

    :catch_0
    move-exception p1

    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    const-string p1, "byteCount < 0: "

    invoke-static {p1, p2, p3}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final b()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->needsInput()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lc0/m;->f:Lc0/g;

    invoke-interface {v0}, Lc0/g;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, Lc0/m;->f:Lc0/g;

    invoke-interface {v0}, Lc0/g;->h()Lc0/e;

    move-result-object v0

    iget-object v0, v0, Lc0/e;->d:Lc0/s;

    if-eqz v0, :cond_2

    iget v2, v0, Lc0/s;->c:I

    iget v3, v0, Lc0/s;->b:I

    sub-int/2addr v2, v3

    iput v2, p0, Lc0/m;->d:I

    iget-object v4, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    iget-object v0, v0, Lc0/s;->a:[B

    invoke-virtual {v4, v0, v3, v2}, Ljava/util/zip/Inflater;->setInput([BII)V

    return v1

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lc0/m;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lc0/m;->e:Z

    iget-object v0, p0, Lc0/m;->f:Lc0/g;

    invoke-interface {v0}, Lc0/x;->close()V

    return-void
.end method

.method public timeout()Lc0/y;
    .locals 1

    iget-object v0, p0, Lc0/m;->f:Lc0/g;

    invoke-interface {v0}, Lc0/x;->timeout()Lc0/y;

    move-result-object v0

    return-object v0
.end method

.method public v0(Lc0/e;J)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lc0/m;->a(Lc0/e;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    return-wide v0

    :cond_0
    iget-object v0, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->finished()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lc0/m;->g:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->needsDictionary()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lc0/m;->f:Lc0/g;

    invoke-interface {v0}, Lc0/g;->H()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/io/EOFException;

    const-string p2, "source exhausted prematurely"

    invoke-direct {p1, p2}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    const-wide/16 p1, -0x1

    return-wide p1
.end method
