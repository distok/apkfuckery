.class public final Lc0/p;
.super Ljava/lang/Object;
.source "JvmOkio.kt"

# interfaces
.implements Lc0/v;


# instance fields
.field public final d:Ljava/io/OutputStream;

.field public final e:Lc0/y;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lc0/y;)V
    .locals 1

    const-string v0, "out"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc0/p;->d:Ljava/io/OutputStream;

    iput-object p2, p0, Lc0/p;->e:Lc0/y;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lc0/p;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method public flush()V
    .locals 1

    iget-object v0, p0, Lc0/p;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public timeout()Lc0/y;
    .locals 1

    iget-object v0, p0, Lc0/p;->e:Lc0/y;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "sink("

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lc0/p;->d:Ljava/io/OutputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lc0/e;J)V
    .locals 7

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v1, p1, Lc0/e;->e:J

    const-wide/16 v3, 0x0

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Ly/a/g0;->m(JJJ)V

    :cond_0
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-lez v2, :cond_2

    iget-object v0, p0, Lc0/p;->e:Lc0/y;

    invoke-virtual {v0}, Lc0/y;->f()V

    iget-object v0, p1, Lc0/e;->d:Lc0/s;

    if-eqz v0, :cond_1

    iget v1, v0, Lc0/s;->c:I

    iget v2, v0, Lc0/s;->b:I

    sub-int/2addr v1, v2

    int-to-long v1, v1

    invoke-static {p2, p3, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v2, v1

    iget-object v1, p0, Lc0/p;->d:Ljava/io/OutputStream;

    iget-object v3, v0, Lc0/s;->a:[B

    iget v4, v0, Lc0/s;->b:I

    invoke-virtual {v1, v3, v4, v2}, Ljava/io/OutputStream;->write([BII)V

    iget v1, v0, Lc0/s;->b:I

    add-int/2addr v1, v2

    iput v1, v0, Lc0/s;->b:I

    int-to-long v2, v2

    sub-long/2addr p2, v2

    iget-wide v4, p1, Lc0/e;->e:J

    sub-long/2addr v4, v2

    iput-wide v4, p1, Lc0/e;->e:J

    iget v2, v0, Lc0/s;->c:I

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lc0/s;->a()Lc0/s;

    move-result-object v1

    iput-object v1, p1, Lc0/e;->d:Lc0/s;

    invoke-static {v0}, Lc0/t;->a(Lc0/s;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_2
    return-void
.end method
