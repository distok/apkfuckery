.class public final Lc0/e$a;
.super Ljava/lang/Object;
.source "Buffer.kt"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc0/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public d:Lc0/e;

.field public e:Z

.field public f:Lc0/s;

.field public g:J

.field public h:[B

.field public i:I

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lc0/e$a;->g:J

    const/4 v0, -0x1

    iput v0, p0, Lc0/e$a;->i:I

    iput v0, p0, Lc0/e$a;->j:I

    return-void
.end method


# virtual methods
.method public final a(J)J
    .locals 13

    iget-object v0, p0, Lc0/e$a;->d:Lc0/e;

    if-eqz v0, :cond_a

    iget-boolean v1, p0, Lc0/e$a;->e:Z

    if-eqz v1, :cond_9

    iget-wide v1, v0, Lc0/e;->e:J

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    cmp-long v6, p1, v1

    if-gtz v6, :cond_6

    cmp-long v3, p1, v4

    if-ltz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_5

    sub-long v6, v1, p1

    :goto_1
    const/4 v3, 0x0

    cmp-long v8, v6, v4

    if-lez v8, :cond_4

    iget-object v8, v0, Lc0/e;->d:Lc0/s;

    if-eqz v8, :cond_3

    iget-object v8, v8, Lc0/s;->g:Lc0/s;

    if-eqz v8, :cond_2

    iget v9, v8, Lc0/s;->c:I

    iget v10, v8, Lc0/s;->b:I

    sub-int v10, v9, v10

    int-to-long v10, v10

    cmp-long v12, v10, v6

    if-gtz v12, :cond_1

    invoke-virtual {v8}, Lc0/s;->a()Lc0/s;

    move-result-object v3

    iput-object v3, v0, Lc0/e;->d:Lc0/s;

    invoke-static {v8}, Lc0/t;->a(Lc0/s;)V

    sub-long/2addr v6, v10

    goto :goto_1

    :cond_1
    long-to-int v4, v6

    sub-int/2addr v9, v4

    iput v9, v8, Lc0/s;->c:I

    goto :goto_2

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v3

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v3

    :cond_4
    :goto_2
    iput-object v3, p0, Lc0/e$a;->f:Lc0/s;

    iput-wide p1, p0, Lc0/e$a;->g:J

    iput-object v3, p0, Lc0/e$a;->h:[B

    const/4 v3, -0x1

    iput v3, p0, Lc0/e$a;->i:I

    iput v3, p0, Lc0/e$a;->j:I

    goto :goto_4

    :cond_5
    const-string v0, "newSize < 0: "

    invoke-static {v0, p1, p2}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_6
    if-lez v6, :cond_8

    sub-long v6, p1, v1

    const/4 v8, 0x1

    :goto_3
    cmp-long v9, v6, v4

    if-lez v9, :cond_8

    invoke-virtual {v0, v3}, Lc0/e;->F(I)Lc0/s;

    move-result-object v3

    iget v9, v3, Lc0/s;->c:I

    rsub-int v9, v9, 0x2000

    int-to-long v9, v9

    invoke-static {v6, v7, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v9

    long-to-int v10, v9

    iget v9, v3, Lc0/s;->c:I

    add-int/2addr v9, v10

    iput v9, v3, Lc0/s;->c:I

    int-to-long v11, v10

    sub-long/2addr v6, v11

    if-eqz v8, :cond_7

    iput-object v3, p0, Lc0/e$a;->f:Lc0/s;

    iput-wide v1, p0, Lc0/e$a;->g:J

    iget-object v3, v3, Lc0/s;->a:[B

    iput-object v3, p0, Lc0/e$a;->h:[B

    sub-int v3, v9, v10

    iput v3, p0, Lc0/e$a;->i:I

    iput v9, p0, Lc0/e$a;->j:I

    const/4 v3, 0x0

    const/4 v8, 0x0

    :cond_7
    const/4 v3, 0x1

    goto :goto_3

    :cond_8
    :goto_4
    iput-wide p1, v0, Lc0/e;->e:J

    return-wide v1

    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "resizeBuffer() only permitted for read/write buffers"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "not attached to a buffer"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b(J)I
    .locals 18

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    iget-object v3, v0, Lc0/e$a;->d:Lc0/e;

    if-eqz v3, :cond_11

    const/4 v4, -0x1

    int-to-long v5, v4

    cmp-long v7, v1, v5

    if-ltz v7, :cond_10

    iget-wide v5, v3, Lc0/e;->e:J

    cmp-long v7, v1, v5

    if-gtz v7, :cond_10

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    cmp-long v10, v1, v7

    if-eqz v10, :cond_f

    cmp-long v7, v1, v5

    if-nez v7, :cond_0

    goto/16 :goto_4

    :cond_0
    const-wide/16 v7, 0x0

    iget-object v4, v3, Lc0/e;->d:Lc0/s;

    iget-object v10, v0, Lc0/e$a;->f:Lc0/s;

    if-eqz v10, :cond_3

    iget-wide v11, v0, Lc0/e$a;->g:J

    iget v13, v0, Lc0/e$a;->i:I

    if-eqz v10, :cond_2

    iget v14, v10, Lc0/s;->b:I

    sub-int/2addr v13, v14

    int-to-long v13, v13

    sub-long/2addr v11, v13

    cmp-long v13, v11, v1

    if-lez v13, :cond_1

    move-wide v5, v11

    move-object/from16 v17, v10

    move-object v10, v4

    move-object/from16 v4, v17

    goto :goto_0

    :cond_1
    move-wide v7, v11

    goto :goto_0

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v9

    :cond_3
    move-object v10, v4

    :goto_0
    sub-long v11, v5, v1

    sub-long v13, v1, v7

    cmp-long v15, v11, v13

    if-lez v15, :cond_5

    :goto_1
    if-eqz v10, :cond_4

    iget v4, v10, Lc0/s;->c:I

    iget v5, v10, Lc0/s;->b:I

    sub-int v6, v4, v5

    int-to-long v11, v6

    add-long/2addr v11, v7

    cmp-long v6, v1, v11

    if-ltz v6, :cond_9

    sub-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v7, v4

    iget-object v10, v10, Lc0/s;->f:Lc0/s;

    goto :goto_1

    :cond_4
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v9

    :cond_5
    :goto_2
    cmp-long v7, v5, v1

    if-lez v7, :cond_8

    if-eqz v4, :cond_7

    iget-object v4, v4, Lc0/s;->g:Lc0/s;

    if-eqz v4, :cond_6

    iget v7, v4, Lc0/s;->c:I

    iget v8, v4, Lc0/s;->b:I

    sub-int/2addr v7, v8

    int-to-long v7, v7

    sub-long/2addr v5, v7

    goto :goto_2

    :cond_6
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v9

    :cond_7
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v9

    :cond_8
    move-object v10, v4

    move-wide v7, v5

    :cond_9
    iget-boolean v4, v0, Lc0/e$a;->e:Z

    if-eqz v4, :cond_d

    if-eqz v10, :cond_c

    iget-boolean v4, v10, Lc0/s;->d:Z

    if-eqz v4, :cond_d

    iget-object v4, v10, Lc0/s;->a:[B

    array-length v5, v4

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v12

    const-string v4, "java.util.Arrays.copyOf(this, size)"

    invoke-static {v12, v4}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v13, v10, Lc0/s;->b:I

    iget v14, v10, Lc0/s;->c:I

    new-instance v4, Lc0/s;

    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object v11, v4

    invoke-direct/range {v11 .. v16}, Lc0/s;-><init>([BIIZZ)V

    iget-object v5, v3, Lc0/e;->d:Lc0/s;

    if-ne v5, v10, :cond_a

    iput-object v4, v3, Lc0/e;->d:Lc0/s;

    :cond_a
    invoke-virtual {v10, v4}, Lc0/s;->b(Lc0/s;)Lc0/s;

    iget-object v3, v4, Lc0/s;->g:Lc0/s;

    if-eqz v3, :cond_b

    invoke-virtual {v3}, Lc0/s;->a()Lc0/s;

    move-object v10, v4

    goto :goto_3

    :cond_b
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v9

    :cond_c
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v9

    :cond_d
    :goto_3
    iput-object v10, v0, Lc0/e$a;->f:Lc0/s;

    iput-wide v1, v0, Lc0/e$a;->g:J

    if-eqz v10, :cond_e

    iget-object v3, v10, Lc0/s;->a:[B

    iput-object v3, v0, Lc0/e$a;->h:[B

    iget v3, v10, Lc0/s;->b:I

    sub-long/2addr v1, v7

    long-to-int v2, v1

    add-int/2addr v3, v2

    iput v3, v0, Lc0/e$a;->i:I

    iget v1, v10, Lc0/s;->c:I

    iput v1, v0, Lc0/e$a;->j:I

    sub-int/2addr v1, v3

    return v1

    :cond_e
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v9

    :cond_f
    :goto_4
    iput-object v9, v0, Lc0/e$a;->f:Lc0/s;

    iput-wide v1, v0, Lc0/e$a;->g:J

    iput-object v9, v0, Lc0/e$a;->h:[B

    iput v4, v0, Lc0/e$a;->i:I

    iput v4, v0, Lc0/e$a;->j:I

    return v4

    :cond_10
    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    iget-wide v2, v3, Lc0/e;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v5, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    const-string v2, "offset=%s > size=%s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "java.lang.String.format(format, *args)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "not attached to a buffer"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public close()V
    .locals 3

    iget-object v0, p0, Lc0/e$a;->d:Lc0/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lc0/e$a;->d:Lc0/e;

    iput-object v0, p0, Lc0/e$a;->f:Lc0/s;

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lc0/e$a;->g:J

    iput-object v0, p0, Lc0/e$a;->h:[B

    const/4 v0, -0x1

    iput v0, p0, Lc0/e$a;->i:I

    iput v0, p0, Lc0/e$a;->j:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not attached to a buffer"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
