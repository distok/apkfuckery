.class public abstract Lf/f/b/c;
.super Ljava/lang/Object;
.source "FieldAccess.java"


# instance fields
.field public a:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Class;)Lf/f/b/c;
    .locals 26

    sget-object v0, Ld0/a/a/w;->g:Ld0/a/a/w;

    sget-object v1, Ld0/a/a/w;->k:Ld0/a/a/w;

    sget-object v2, Ld0/a/a/w;->m:Ld0/a/a/w;

    sget-object v3, Ld0/a/a/w;->l:Ld0/a/a/w;

    sget-object v4, Ld0/a/a/w;->j:Ld0/a/a/w;

    sget-object v5, Ld0/a/a/w;->i:Ld0/a/a/w;

    sget-object v6, Ld0/a/a/w;->h:Ld0/a/a/w;

    sget-object v7, Ld0/a/a/w;->f:Ld0/a/a/w;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v9, p0

    :goto_0
    const-class v10, Ljava/lang/Object;

    const/4 v11, 0x0

    if-eq v9, v10, :cond_3

    invoke-virtual {v9}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v10

    array-length v12, v10

    :goto_1
    if-ge v11, v12, :cond_2

    aget-object v13, v10, v11

    invoke-virtual {v13}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v14

    invoke-static {v14}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v15

    if-eqz v15, :cond_0

    goto :goto_2

    :cond_0
    invoke-static {v14}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v14

    if-eqz v14, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v9}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v9

    goto :goto_0

    :cond_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v10, v9, [Ljava/lang/String;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v12, v12, [Ljava/lang/Class;

    const/4 v13, 0x0

    :goto_3
    if-ge v13, v9, :cond_4

    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/reflect/Field;

    invoke-virtual {v14}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v10, v13

    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/reflect/Field;

    invoke-virtual {v14}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v14

    aput-object v14, v12, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v12, "FieldAccess"

    invoke-static {v9, v12}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "java."

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    const-string v13, "reflectasm."

    invoke-static {v13, v12}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    :cond_5
    invoke-static/range {p0 .. p0}, Lf/f/b/a;->b(Ljava/lang/Class;)Lf/f/b/a;

    move-result-object v13

    :try_start_0
    invoke-virtual {v13, v12}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v16, v10

    goto/16 :goto_5

    :catch_0
    monitor-enter v13

    :try_start_1
    invoke-virtual {v13, v12}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v16, v10

    goto/16 :goto_4

    :catchall_0
    move-exception v0

    goto/16 :goto_6

    :catch_1
    const/16 v14, 0x2f

    const/16 v15, 0x2e

    :try_start_2
    invoke-virtual {v12, v15, v14}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v9, v15, v14}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v9

    new-instance v14, Ld0/a/a/f;

    invoke-direct {v14, v11}, Ld0/a/a/f;-><init>(I)V

    const v17, 0x3002d

    const/16 v18, 0x21

    const/16 v20, 0x0

    const-string v21, "com/esotericsoftware/reflectasm/FieldAccess"

    const/16 v22, 0x0

    move-object/from16 v16, v14

    invoke-virtual/range {v16 .. v22}, Ld0/a/a/f;->c(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    const/16 v21, 0x1

    const-string v22, "<init>"

    const-string v23, "()V"

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v20, v14

    invoke-virtual/range {v20 .. v25}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object v15

    move-object/from16 v16, v10

    const/16 v10, 0x19

    invoke-virtual {v15, v10, v11}, Ld0/a/a/q;->t(II)V

    const-string v10, "com/esotericsoftware/reflectasm/FieldAccess"

    const-string v11, "<init>"

    move-object/from16 v17, v12

    const-string v12, "()V"

    move-object/from16 v18, v0

    const/16 v0, 0xb7

    invoke-virtual {v15, v0, v10, v11, v12}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xb1

    invoke-virtual {v15, v0}, Ld0/a/a/q;->e(I)V

    const/4 v0, 0x1

    invoke-virtual {v15, v0, v0}, Ld0/a/a/q;->n(II)V

    invoke-static {v14, v9, v8}, Lf/f/b/c;->m(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-static {v14, v9, v8}, Lf/f/b/c;->p(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-static {v14, v9, v8, v7}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v7}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v6}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v6}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v5}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v5}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v4}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v4}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v3}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v3}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v2}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v2}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v1}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v1}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    move-object/from16 v0, v18

    invoke-static {v14, v9, v8, v0}, Lf/f/b/c;->n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8, v0}, Lf/f/b/c;->q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V

    invoke-static {v14, v9, v8}, Lf/f/b/c;->o(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v14}, Ld0/a/a/f;->b()[B

    move-result-object v0

    move-object/from16 v12, v17

    invoke-virtual {v13, v12, v0}, Lf/f/b/a;->a(Ljava/lang/String;[B)Ljava/lang/Class;

    move-result-object v0

    :goto_4
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_5
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/f/b/c;

    move-object/from16 v1, v16

    iput-object v1, v0, Lf/f/b/c;->a:[Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-object v0

    :catchall_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error constructing field access class: "

    invoke-static {v2, v12}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :goto_6
    :try_start_4
    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0
.end method

.method public static m(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld0/a/a/f;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/reflect/Field;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x1

    const-string v2, "get"

    const-string v3, "(Ljava/lang/Object;I)Ljava/lang/Object;"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object p0

    const/16 v0, 0x15

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Ld0/a/a/q;->t(II)V

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x5

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Ld0/a/a/p;

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    new-instance v5, Ld0/a/a/p;

    invoke-direct {v5}, Ld0/a/a/p;-><init>()V

    aput-object v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-instance v4, Ld0/a/a/p;

    invoke-direct {v4}, Ld0/a/a/p;-><init>()V

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {p0, v3, v5, v4, v2}, Ld0/a/a/q;->q(IILd0/a/a/p;[Ld0/a/a/p;)V

    :goto_1
    if-ge v3, v1, :cond_1

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/reflect/Field;

    aget-object v6, v2, v3

    invoke-virtual {p0, v6}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/16 v6, 0x19

    const/4 v7, 0x1

    invoke-virtual {p0, v6, v7}, Ld0/a/a/q;->t(II)V

    const/16 v6, 0xc0

    invoke-virtual {p0, v6, p1}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/16 v6, 0xb4

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v8

    invoke-static {v8}, Ld0/a/a/w;->e(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v6, p1, v7, v8}, Ld0/a/a/q;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Ld0/a/a/w;->j(Ljava/lang/Class;)Ld0/a/a/w;

    move-result-object v5

    invoke-virtual {v5}, Ld0/a/a/w;->i()I

    move-result v5

    const-string v6, "valueOf"

    const/16 v7, 0xb8

    packed-switch v5, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const-string v5, "java/lang/Double"

    const-string v8, "(D)Ljava/lang/Double;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_1
    const-string v5, "java/lang/Long"

    const-string v8, "(J)Ljava/lang/Long;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_2
    const-string v5, "java/lang/Float"

    const-string v8, "(F)Ljava/lang/Float;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_3
    const-string v5, "java/lang/Integer"

    const-string v8, "(I)Ljava/lang/Integer;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_4
    const-string v5, "java/lang/Short"

    const-string v8, "(S)Ljava/lang/Short;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_5
    const-string v5, "java/lang/Byte"

    const-string v8, "(B)Ljava/lang/Byte;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_6
    const-string v5, "java/lang/Character"

    const-string v8, "(C)Ljava/lang/Character;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_7
    const-string v5, "java/lang/Boolean"

    const-string v8, "(Z)Ljava/lang/Boolean;"

    invoke-virtual {p0, v7, v5, v6, v8}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const/16 v5, 0xb0

    invoke-virtual {p0, v5}, Ld0/a/a/q;->e(I)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    invoke-virtual {p0, v4}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    goto :goto_3

    :cond_2
    const/4 v0, 0x6

    :goto_3
    invoke-static {p0}, Lf/f/b/c;->r(Ld0/a/a/q;)Ld0/a/a/q;

    const/4 p1, 0x3

    invoke-virtual {p0, v0, p1}, Ld0/a/a/q;->n(II)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static n(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld0/a/a/f;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/reflect/Field;",
            ">;",
            "Ld0/a/a/w;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual/range {p3 .. p3}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Ld0/a/a/w;->i()I

    move-result v3

    const/16 v4, 0xac

    packed-switch v3, :pswitch_data_0

    const/16 v4, 0xb0

    const-string v3, "get"

    goto :goto_0

    :pswitch_0
    const/16 v4, 0xaf

    const-string v3, "getDouble"

    goto :goto_0

    :pswitch_1
    const/16 v4, 0xad

    const-string v3, "getLong"

    goto :goto_0

    :pswitch_2
    const/16 v4, 0xae

    const-string v3, "getFloat"

    goto :goto_0

    :pswitch_3
    const-string v3, "getInt"

    goto :goto_0

    :pswitch_4
    const-string v3, "getShort"

    goto :goto_0

    :pswitch_5
    const-string v3, "getByte"

    goto :goto_0

    :pswitch_6
    const-string v3, "getChar"

    goto :goto_0

    :pswitch_7
    const-string v3, "getBoolean"

    :goto_0
    move-object v7, v3

    const/4 v6, 0x1

    const-string v3, "(Ljava/lang/Object;I)"

    invoke-static {v3, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v10}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object v3

    const/16 v5, 0x15

    const/4 v6, 0x2

    invoke-virtual {v3, v5, v6}, Ld0/a/a/q;->t(II)V

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v6, v5, [Ld0/a/a/p;

    new-instance v7, Ld0/a/a/p;

    invoke-direct {v7}, Ld0/a/a/p;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_1
    if-ge v9, v5, :cond_1

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/reflect/Field;

    invoke-virtual {v11}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v11

    invoke-static {v11}, Ld0/a/a/w;->j(Ljava/lang/Class;)Ld0/a/a/w;

    move-result-object v11

    move-object/from16 v15, p3

    invoke-virtual {v11, v15}, Ld0/a/a/w;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    new-instance v11, Ld0/a/a/p;

    invoke-direct {v11}, Ld0/a/a/p;-><init>()V

    aput-object v11, v6, v9

    goto :goto_2

    :cond_0
    aput-object v7, v6, v9

    const/4 v10, 0x1

    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v15, p3

    new-instance v9, Ld0/a/a/p;

    invoke-direct {v9}, Ld0/a/a/p;-><init>()V

    add-int/lit8 v11, v5, -0x1

    invoke-virtual {v3, v8, v11, v9, v6}, Ld0/a/a/q;->q(IILd0/a/a/p;[Ld0/a/a/p;)V

    :goto_3
    if-ge v8, v5, :cond_3

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    move-object/from16 v17, v11

    check-cast v17, Ljava/lang/reflect/Field;

    aget-object v11, v6, v8

    invoke-virtual {v11, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    aget-object v11, v6, v8

    invoke-virtual {v3, v11}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v12, 0x3

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x0

    move-object v11, v3

    move/from16 v15, v16

    move-object/from16 v16, v18

    invoke-virtual/range {v11 .. v16}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/16 v11, 0x19

    const/4 v12, 0x1

    invoke-virtual {v3, v11, v12}, Ld0/a/a/q;->t(II)V

    const/16 v11, 0xc0

    invoke-virtual {v3, v11, v0}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/16 v11, 0xb4

    invoke-virtual/range {v17 .. v17}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v11, v0, v12, v2}, Ld0/a/a/q;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ld0/a/a/q;->e(I)V

    :cond_2
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v15, p3

    goto :goto_3

    :cond_3
    if-eqz v10, :cond_4

    invoke-virtual {v3, v7}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v12, 0x3

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v11, v3

    invoke-virtual/range {v11 .. v16}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    invoke-virtual/range {p3 .. p3}, Ld0/a/a/w;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lf/f/b/c;->s(Ld0/a/a/q;Ljava/lang/String;)Ld0/a/a/q;

    :cond_4
    invoke-virtual {v3, v9}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v12, 0x3

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v11, v3

    invoke-virtual/range {v11 .. v16}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/4 v0, 0x5

    goto :goto_4

    :cond_5
    const/4 v0, 0x6

    :goto_4
    invoke-static {v3}, Lf/f/b/c;->r(Ld0/a/a/q;)Ld0/a/a/q;

    const/4 v1, 0x3

    invoke-virtual {v3, v0, v1}, Ld0/a/a/q;->n(II)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static o(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld0/a/a/f;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/reflect/Field;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x1

    const-string v2, "getString"

    const-string v3, "(Ljava/lang/Object;I)Ljava/lang/String;"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object p0

    const/16 v0, 0x15

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Ld0/a/a/q;->t(II)V

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ld0/a/a/p;

    new-instance v2, Ld0/a/a/p;

    invoke-direct {v2}, Ld0/a/a/p;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v12, 0x1

    if-ge v4, v0, :cond_1

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/reflect/Field;

    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    const-class v7, Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ld0/a/a/p;

    invoke-direct {v6}, Ld0/a/a/p;-><init>()V

    aput-object v6, v1, v4

    goto :goto_1

    :cond_0
    aput-object v2, v1, v4

    const/4 v5, 0x1

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    new-instance v4, Ld0/a/a/p;

    invoke-direct {v4}, Ld0/a/a/p;-><init>()V

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v3, v6, v4, v1}, Ld0/a/a/q;->q(IILd0/a/a/p;[Ld0/a/a/p;)V

    :goto_2
    if-ge v3, v0, :cond_3

    aget-object v6, v1, v3

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    aget-object v6, v1, v3

    invoke-virtual {p0, v6}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/16 v6, 0x19

    invoke-virtual {p0, v6, v12}, Ld0/a/a/q;->t(II)V

    const/16 v6, 0xc0

    invoke-virtual {p0, v6, p1}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/16 v6, 0xb4

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/reflect/Field;

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Ljava/lang/String;"

    invoke-virtual {p0, v6, p1, v7, v8}, Ld0/a/a/q;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v6, 0xb0

    invoke-virtual {p0, v6}, Ld0/a/a/q;->e(I)V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {p0, v2}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const-string p1, "String"

    invoke-static {p0, p1}, Lf/f/b/c;->s(Ld0/a/a/q;Ljava/lang/String;)Ld0/a/a/q;

    :cond_4
    invoke-virtual {p0, v4}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/4 p1, 0x5

    goto :goto_3

    :cond_5
    const/4 p1, 0x6

    :goto_3
    invoke-static {p0}, Lf/f/b/c;->r(Ld0/a/a/q;)Ld0/a/a/q;

    const/4 p2, 0x3

    invoke-virtual {p0, p1, p2}, Ld0/a/a/q;->n(II)V

    return-void
.end method

.method public static p(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld0/a/a/f;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/reflect/Field;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x1

    const-string v2, "set"

    const-string v3, "(Ljava/lang/Object;ILjava/lang/Object;)V"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object p0

    const/16 v0, 0x15

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Ld0/a/a/q;->t(II)V

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x5

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Ld0/a/a/p;

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    new-instance v5, Ld0/a/a/p;

    invoke-direct {v5}, Ld0/a/a/p;-><init>()V

    aput-object v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-instance v4, Ld0/a/a/p;

    invoke-direct {v4}, Ld0/a/a/p;-><init>()V

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {p0, v3, v5, v4, v2}, Ld0/a/a/q;->q(IILd0/a/a/p;[Ld0/a/a/p;)V

    :goto_1
    if-ge v3, v1, :cond_1

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/reflect/Field;

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, Ld0/a/a/w;->j(Ljava/lang/Class;)Ld0/a/a/w;

    move-result-object v12

    aget-object v6, v2, v3

    invoke-virtual {p0, v6}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/16 v6, 0x19

    const/4 v7, 0x1

    invoke-virtual {p0, v6, v7}, Ld0/a/a/q;->t(II)V

    const/16 v7, 0xc0

    invoke-virtual {p0, v7, p1}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/4 v8, 0x3

    invoke-virtual {p0, v6, v8}, Ld0/a/a/q;->t(II)V

    invoke-virtual {v12}, Ld0/a/a/w;->i()I

    move-result v6

    const/16 v8, 0xb6

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_2

    :pswitch_0
    invoke-virtual {v12}, Ld0/a/a/w;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    goto/16 :goto_2

    :pswitch_1
    invoke-virtual {v12}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    goto :goto_2

    :pswitch_2
    const-string v6, "java/lang/Double"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "doubleValue"

    const-string v9, "()D"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_3
    const-string v6, "java/lang/Long"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "longValue"

    const-string v9, "()J"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_4
    const-string v6, "java/lang/Float"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "floatValue"

    const-string v9, "()F"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_5
    const-string v6, "java/lang/Integer"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "intValue"

    const-string v9, "()I"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_6
    const-string v6, "java/lang/Short"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "shortValue"

    const-string v9, "()S"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_7
    const-string v6, "java/lang/Byte"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "byteValue"

    const-string v9, "()B"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_8
    const-string v6, "java/lang/Character"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "charValue"

    const-string v9, "()C"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_9
    const-string v6, "java/lang/Boolean"

    invoke-virtual {p0, v7, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v7, "booleanValue"

    const-string v9, "()Z"

    invoke-virtual {p0, v8, v6, v7, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const/16 v6, 0xb5

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, p1, v5, v7}, Ld0/a/a/q;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v5, 0xb1

    invoke-virtual {p0, v5}, Ld0/a/a/q;->e(I)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    invoke-virtual {p0, v4}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    goto :goto_3

    :cond_2
    const/4 v0, 0x6

    :goto_3
    invoke-static {p0}, Lf/f/b/c;->r(Ld0/a/a/q;)Ld0/a/a/q;

    const/4 p1, 0x4

    invoke-virtual {p0, v0, p1}, Ld0/a/a/q;->n(II)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static q(Ld0/a/a/f;Ljava/lang/String;Ljava/util/ArrayList;Ld0/a/a/w;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld0/a/a/f;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/reflect/Field;",
            ">;",
            "Ld0/a/a/w;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual/range {p3 .. p3}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Ld0/a/a/w;->i()I

    move-result v3

    const/16 v4, 0x15

    packed-switch v3, :pswitch_data_0

    const-string v3, "set"

    const/16 v5, 0x19

    move-object v5, v3

    const/16 v3, 0x19

    goto :goto_2

    :pswitch_0
    const/16 v3, 0x18

    const-string v5, "setDouble"

    goto :goto_0

    :pswitch_1
    const/16 v3, 0x16

    const-string v5, "setLong"

    :goto_0
    const/4 v6, 0x5

    move-object v7, v5

    const/4 v11, 0x5

    goto :goto_3

    :pswitch_2
    const/16 v3, 0x17

    const-string v5, "setFloat"

    goto :goto_2

    :pswitch_3
    const-string v3, "setInt"

    goto :goto_1

    :pswitch_4
    const-string v3, "setShort"

    goto :goto_1

    :pswitch_5
    const-string v3, "setByte"

    goto :goto_1

    :pswitch_6
    const-string v3, "setChar"

    goto :goto_1

    :pswitch_7
    const-string v3, "setBoolean"

    :goto_1
    const/16 v5, 0x15

    move-object v5, v3

    const/16 v3, 0x15

    :goto_2
    const/4 v6, 0x4

    move-object v7, v5

    const/4 v11, 0x4

    :goto_3
    const/4 v6, 0x1

    const-string v5, "(Ljava/lang/Object;I"

    const-string v8, ")V"

    invoke-static {v5, v2, v8}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v10}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v4, v6}, Ld0/a/a/q;->t(II)V

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v6, v4, [Ld0/a/a/p;

    new-instance v7, Ld0/a/a/p;

    invoke-direct {v7}, Ld0/a/a/p;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_4
    if-ge v9, v4, :cond_1

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/reflect/Field;

    invoke-virtual {v12}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v12

    invoke-static {v12}, Ld0/a/a/w;->j(Ljava/lang/Class;)Ld0/a/a/w;

    move-result-object v12

    move-object/from16 v15, p3

    invoke-virtual {v12, v15}, Ld0/a/a/w;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    new-instance v12, Ld0/a/a/p;

    invoke-direct {v12}, Ld0/a/a/p;-><init>()V

    aput-object v12, v6, v9

    goto :goto_5

    :cond_0
    aput-object v7, v6, v9

    const/4 v10, 0x1

    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_1
    move-object/from16 v15, p3

    new-instance v9, Ld0/a/a/p;

    invoke-direct {v9}, Ld0/a/a/p;-><init>()V

    add-int/lit8 v12, v4, -0x1

    invoke-virtual {v5, v8, v12, v9, v6}, Ld0/a/a/q;->q(IILd0/a/a/p;[Ld0/a/a/p;)V

    :goto_6
    if-ge v8, v4, :cond_3

    aget-object v12, v6, v8

    invoke-virtual {v12, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    aget-object v12, v6, v8

    invoke-virtual {v5, v12}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v13, 0x3

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/4 v12, 0x1

    move-object v12, v5

    move-object/from16 v15, v16

    move/from16 v16, v17

    move-object/from16 v17, v18

    invoke-virtual/range {v12 .. v17}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/16 v12, 0x19

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Ld0/a/a/q;->t(II)V

    const/16 v12, 0xc0

    invoke-virtual {v5, v12, v0}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/4 v12, 0x3

    invoke-virtual {v5, v3, v12}, Ld0/a/a/q;->t(II)V

    const/16 v12, 0xb5

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/reflect/Field;

    invoke-virtual {v13}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v12, v0, v13, v2}, Ld0/a/a/q;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v12, 0xb1

    invoke-virtual {v5, v12}, Ld0/a/a/q;->e(I)V

    :cond_2
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v15, p3

    goto :goto_6

    :cond_3
    if-eqz v10, :cond_4

    invoke-virtual {v5, v7}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v13, 0x3

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v12, v5

    invoke-virtual/range {v12 .. v17}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    invoke-virtual/range {p3 .. p3}, Ld0/a/a/w;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lf/f/b/c;->s(Ld0/a/a/q;Ljava/lang/String;)Ld0/a/a/q;

    :cond_4
    invoke-virtual {v5, v9}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v13, 0x3

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v12, v5

    invoke-virtual/range {v12 .. v17}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    const/4 v0, 0x5

    goto :goto_7

    :cond_5
    const/4 v0, 0x6

    :goto_7
    invoke-static {v5}, Lf/f/b/c;->r(Ld0/a/a/q;)Ld0/a/a/q;

    invoke-virtual {v5, v0, v11}, Ld0/a/a/q;->n(II)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static r(Ld0/a/a/q;)Ld0/a/a/q;
    .locals 8

    const/16 v0, 0xbb

    const-string v1, "java/lang/IllegalArgumentException"

    invoke-virtual {p0, v0, v1}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/16 v2, 0x59

    invoke-virtual {p0, v2}, Ld0/a/a/q;->e(I)V

    const-string v3, "java/lang/StringBuilder"

    invoke-virtual {p0, v0, v3}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    invoke-virtual {p0, v2}, Ld0/a/a/q;->e(I)V

    const-string v0, "Field not found: "

    invoke-virtual {p0, v0}, Ld0/a/a/q;->j(Ljava/lang/Object;)V

    const/16 v0, 0xb7

    const-string v2, "<init>"

    const-string v4, "(Ljava/lang/String;)V"

    invoke-virtual {p0, v0, v3, v2, v4}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v5, 0x15

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Ld0/a/a/q;->t(II)V

    const/16 v5, 0xb6

    const-string v6, "append"

    const-string v7, "(I)Ljava/lang/StringBuilder;"

    invoke-virtual {p0, v5, v3, v6, v7}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "toString"

    const-string v7, "()Ljava/lang/String;"

    invoke-virtual {p0, v5, v3, v6, v7}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1, v2, v4}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xbf

    invoke-virtual {p0, v0}, Ld0/a/a/q;->e(I)V

    return-object p0
.end method

.method public static s(Ld0/a/a/q;Ljava/lang/String;)Ld0/a/a/q;
    .locals 7

    const/16 v0, 0xbb

    const-string v1, "java/lang/IllegalArgumentException"

    invoke-virtual {p0, v0, v1}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/16 v2, 0x59

    invoke-virtual {p0, v2}, Ld0/a/a/q;->e(I)V

    const-string v3, "java/lang/StringBuilder"

    invoke-virtual {p0, v0, v3}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    invoke-virtual {p0, v2}, Ld0/a/a/q;->e(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field not declared as "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ld0/a/a/q;->j(Ljava/lang/Object;)V

    const/16 p1, 0xb7

    const-string v0, "<init>"

    const-string v2, "(Ljava/lang/String;)V"

    invoke-virtual {p0, p1, v3, v0, v2}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, 0x15

    const/4 v5, 0x2

    invoke-virtual {p0, v4, v5}, Ld0/a/a/q;->t(II)V

    const/16 v4, 0xb6

    const-string v5, "append"

    const-string v6, "(I)Ljava/lang/StringBuilder;"

    invoke-virtual {p0, v4, v3, v5, v6}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "toString"

    const-string v6, "()Ljava/lang/String;"

    invoke-virtual {p0, v4, v3, v5, v6}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v1, v0, v2}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0xbf

    invoke-virtual {p0, p1}, Ld0/a/a/q;->e(I)V

    return-object p0
.end method


# virtual methods
.method public abstract A(Ljava/lang/Object;IJ)V
.end method

.method public abstract B(Ljava/lang/Object;IS)V
.end method

.method public abstract b(Ljava/lang/Object;I)Ljava/lang/Object;
.end method

.method public abstract c(Ljava/lang/Object;I)Z
.end method

.method public abstract d(Ljava/lang/Object;I)B
.end method

.method public abstract e(Ljava/lang/Object;I)C
.end method

.method public abstract f(Ljava/lang/Object;I)D
.end method

.method public abstract g(Ljava/lang/Object;I)F
.end method

.method public h(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Lf/f/b/c;->a:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lf/f/b/c;->a:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to find non-private field: "

    invoke-static {v1, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract i(Ljava/lang/Object;I)I
.end method

.method public abstract j(Ljava/lang/Object;I)J
.end method

.method public abstract k(Ljava/lang/Object;I)S
.end method

.method public abstract l(Ljava/lang/Object;I)Ljava/lang/String;
.end method

.method public abstract t(Ljava/lang/Object;ILjava/lang/Object;)V
.end method

.method public abstract u(Ljava/lang/Object;IZ)V
.end method

.method public abstract v(Ljava/lang/Object;IB)V
.end method

.method public abstract w(Ljava/lang/Object;IC)V
.end method

.method public abstract x(Ljava/lang/Object;ID)V
.end method

.method public abstract y(Ljava/lang/Object;IF)V
.end method

.method public abstract z(Ljava/lang/Object;II)V
.end method
