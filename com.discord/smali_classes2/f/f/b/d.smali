.class public abstract Lf/f/b/d;
.super Ljava/lang/Object;
.source "MethodAccess.java"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[[Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/reflect/Method;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object p0

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v3

    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static b(Ljava/lang/Class;)Lf/f/b/d;
    .locals 24

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v2, p0

    :goto_0
    const-class v3, Ljava/lang/Object;

    if-eq v2, v3, :cond_0

    invoke-static {v2, v0}, Lf/f/b/d;->a(Ljava/lang/Class;Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object/from16 v2, p0

    goto :goto_1

    :cond_1
    move-object/from16 v2, p0

    invoke-static {v2, v0}, Lf/f/b/d;->e(Ljava/lang/Class;Ljava/util/ArrayList;)V

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v4, v3, [Ljava/lang/String;

    new-array v5, v3, [[Ljava/lang/Class;

    new-array v6, v3, [Ljava/lang/Class;

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v3, :cond_2

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/reflect/Method;

    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v8

    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v10

    aput-object v10, v5, v8

    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v9

    aput-object v9, v6, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "MethodAccess"

    invoke-static {v8, v9}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "java."

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "reflectasm."

    invoke-static {v10, v9}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_3
    invoke-static/range {p0 .. p0}, Lf/f/b/a;->b(Ljava/lang/Class;)Lf/f/b/a;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2, v9}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v19, v5

    goto/16 :goto_d

    :catch_0
    monitor-enter v2

    :try_start_1
    invoke-virtual {v2, v9}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v19, v5

    goto/16 :goto_c

    :catchall_0
    move-exception v0

    goto/16 :goto_e

    :catch_1
    const/16 v10, 0x2f

    const/16 v11, 0x2e

    :try_start_2
    invoke-virtual {v9, v11, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v11, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v8

    new-instance v10, Ld0/a/a/f;

    const/4 v11, 0x1

    invoke-direct {v10, v11}, Ld0/a/a/f;-><init>(I)V

    const v13, 0x3002d

    const/16 v14, 0x21

    const/16 v16, 0x0

    const-string v17, "com/esotericsoftware/reflectasm/MethodAccess"

    const/16 v18, 0x0

    move-object v12, v10

    invoke-virtual/range {v12 .. v18}, Ld0/a/a/f;->c(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    const/16 v17, 0x1

    const-string v18, "<init>"

    const-string v19, "()V"

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v16, v10

    invoke-virtual/range {v16 .. v21}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object v11

    const/16 v12, 0x19

    invoke-virtual {v11, v12, v7}, Ld0/a/a/q;->t(II)V

    const-string v12, "com/esotericsoftware/reflectasm/MethodAccess"

    const-string v13, "<init>"

    const-string v14, "()V"

    const/16 v15, 0xb7

    invoke-virtual {v11, v15, v12, v13, v14}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v12, 0xb1

    invoke-virtual {v11, v12}, Ld0/a/a/q;->e(I)V

    invoke-virtual {v11, v7, v7}, Ld0/a/a/q;->n(II)V

    const/16 v17, 0x81

    const-string v18, "invoke"

    const-string v19, "(Ljava/lang/Object;I[Ljava/lang/Object;)Ljava/lang/Object;"

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v16, v10

    invoke-virtual/range {v16 .. v21}, Ld0/a/a/f;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ld0/a/a/q;

    move-result-object v15

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    const/4 v12, 0x2

    const/16 v13, 0x15

    if-nez v11, :cond_a

    const/16 v11, 0x19

    const/4 v14, 0x1

    invoke-virtual {v15, v11, v14}, Ld0/a/a/q;->t(II)V

    const/16 v11, 0xc0

    invoke-virtual {v15, v11, v8}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/16 v11, 0x3a

    const/4 v14, 0x4

    invoke-virtual {v15, v11, v14}, Ld0/a/a/q;->t(II)V

    invoke-virtual {v15, v13, v12}, Ld0/a/a/q;->t(II)V

    new-array v14, v3, [Ld0/a/a/p;

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v3, :cond_4

    new-instance v12, Ld0/a/a/p;

    invoke-direct {v12}, Ld0/a/a/p;-><init>()V

    aput-object v12, v14, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_4
    new-instance v13, Ld0/a/a/p;

    invoke-direct {v13}, Ld0/a/a/p;-><init>()V

    add-int/lit8 v11, v3, -0x1

    invoke-virtual {v15, v7, v11, v13, v14}, Ld0/a/a/q;->q(IILd0/a/a/p;[Ld0/a/a/p;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v11, 0x80

    invoke-direct {v7, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_4
    if-ge v12, v3, :cond_9

    aget-object v11, v14, v12

    invoke-virtual {v15, v11}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    if-nez v12, :cond_5

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/16 v18, 0x0

    aput-object v8, v11, v18

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v20, v11

    move-object v11, v15

    move/from16 v22, v12

    move/from16 v12, v16

    move-object/from16 v23, v13

    move/from16 v13, v17

    move-object/from16 v17, v14

    move-object/from16 v14, v20

    move-object/from16 p0, v15

    move/from16 v15, v18

    move-object/from16 v16, v19

    invoke-virtual/range {v11 .. v16}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    goto :goto_5

    :cond_5
    move/from16 v22, v12

    move-object/from16 v23, v13

    move-object/from16 v17, v14

    move-object/from16 p0, v15

    const/4 v12, 0x3

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v11, p0

    invoke-virtual/range {v11 .. v16}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    :goto_5
    const/4 v11, 0x4

    const/16 v12, 0x19

    move-object/from16 v15, p0

    invoke-virtual {v15, v12, v11}, Ld0/a/a/q;->t(II)V

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    const/16 v11, 0x28

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move/from16 v11, v22

    aget-object v12, v5, v11

    aget-object v13, v6, v11

    const/4 v14, 0x0

    move/from16 v16, v3

    :goto_6
    array-length v3, v12

    if-ge v14, v3, :cond_6

    const/4 v3, 0x3

    move-object/from16 v18, v6

    const/16 v6, 0x19

    invoke-virtual {v15, v6, v3}, Ld0/a/a/q;->t(II)V

    const/16 v3, 0x10

    invoke-virtual {v15, v3, v14}, Ld0/a/a/q;->g(II)V

    const/16 v3, 0x32

    invoke-virtual {v15, v3}, Ld0/a/a/q;->e(I)V

    aget-object v3, v12, v14

    invoke-static {v3}, Ld0/a/a/w;->j(Ljava/lang/Class;)Ld0/a/a/w;

    move-result-object v3

    invoke-virtual {v3}, Ld0/a/a/w;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    goto/16 :goto_8

    :pswitch_0
    invoke-virtual {v3}, Ld0/a/a/w;->g()Ljava/lang/String;

    move-result-object v6

    move-object/from16 p0, v12

    const/16 v12, 0xc0

    invoke-virtual {v15, v12, v6}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    goto :goto_7

    :pswitch_1
    move-object/from16 p0, v12

    const/16 v6, 0xc0

    invoke-virtual {v3}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v15, v6, v12}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    :goto_7
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    goto/16 :goto_8

    :pswitch_2
    move-object/from16 p0, v12

    const/16 v6, 0xc0

    const-string v12, "java/lang/Double"

    invoke-virtual {v15, v6, v12}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v6, "java/lang/Double"

    const-string v12, "doubleValue"

    move-object/from16 v19, v5

    const-string v5, "()D"

    move-object/from16 v20, v9

    const/16 v9, 0xb6

    invoke-virtual {v15, v9, v6, v12, v5}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :pswitch_3
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    const-string v5, "java/lang/Long"

    const/16 v6, 0xc0

    invoke-virtual {v15, v6, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v5, "java/lang/Long"

    const-string v6, "longValue"

    const-string v9, "()J"

    const/16 v12, 0xb6

    invoke-virtual {v15, v12, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :pswitch_4
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    const-string v5, "java/lang/Float"

    const/16 v6, 0xc0

    invoke-virtual {v15, v6, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v5, "java/lang/Float"

    const-string v6, "floatValue"

    const-string v9, "()F"

    const/16 v12, 0xb6

    invoke-virtual {v15, v12, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :pswitch_5
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    const-string v5, "java/lang/Integer"

    const/16 v6, 0xc0

    invoke-virtual {v15, v6, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v5, "java/lang/Integer"

    const-string v6, "intValue"

    const-string v9, "()I"

    const/16 v12, 0xb6

    invoke-virtual {v15, v12, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :pswitch_6
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    const-string v5, "java/lang/Short"

    const/16 v6, 0xc0

    invoke-virtual {v15, v6, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v5, "java/lang/Short"

    const-string v6, "shortValue"

    const-string v9, "()S"

    const/16 v12, 0xb6

    invoke-virtual {v15, v12, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :pswitch_7
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    const-string v5, "java/lang/Byte"

    const/16 v6, 0xc0

    invoke-virtual {v15, v6, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v5, "java/lang/Byte"

    const-string v6, "byteValue"

    const-string v9, "()B"

    const/16 v12, 0xb6

    invoke-virtual {v15, v12, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :pswitch_8
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    const-string v5, "java/lang/Character"

    const/16 v6, 0xc0

    invoke-virtual {v15, v6, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v5, "java/lang/Character"

    const-string v6, "charValue"

    const-string v9, "()C"

    const/16 v12, 0xb6

    invoke-virtual {v15, v12, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :pswitch_9
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object/from16 p0, v12

    const-string v5, "java/lang/Boolean"

    const/16 v6, 0xc0

    invoke-virtual {v15, v6, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const-string v5, "java/lang/Boolean"

    const-string v6, "booleanValue"

    const-string v9, "()Z"

    const/16 v12, 0xb6

    invoke-virtual {v15, v12, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_8
    invoke-virtual {v3}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v12, p0

    move-object/from16 v6, v18

    move-object/from16 v5, v19

    move-object/from16 v9, v20

    goto/16 :goto_6

    :cond_6
    move-object/from16 v19, v5

    move-object/from16 v18, v6

    move-object/from16 v20, v9

    const/16 v3, 0x29

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v13}, Ld0/a/a/w;->e(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0xb8

    if-eqz v1, :cond_7

    const/16 v5, 0xb9

    goto :goto_9

    :cond_7
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/reflect/Method;

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v5

    invoke-static {v5}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v5

    if-eqz v5, :cond_8

    const/16 v5, 0xb8

    goto :goto_9

    :cond_8
    const/16 v5, 0xb6

    :goto_9
    aget-object v6, v4, v11

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v15, v5, v8, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v13}, Ld0/a/a/w;->j(Ljava/lang/Class;)Ld0/a/a/w;

    move-result-object v5

    invoke-virtual {v5}, Ld0/a/a/w;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    goto :goto_a

    :pswitch_a
    const-string v5, "java/lang/Double"

    const-string v6, "valueOf"

    const-string v9, "(D)Ljava/lang/Double;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_b
    const-string v5, "java/lang/Long"

    const-string v6, "valueOf"

    const-string v9, "(J)Ljava/lang/Long;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_c
    const-string v5, "java/lang/Float"

    const-string v6, "valueOf"

    const-string v9, "(F)Ljava/lang/Float;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_d
    const-string v5, "java/lang/Integer"

    const-string v6, "valueOf"

    const-string v9, "(I)Ljava/lang/Integer;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_e
    const-string v5, "java/lang/Short"

    const-string v6, "valueOf"

    const-string v9, "(S)Ljava/lang/Short;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_f
    const-string v5, "java/lang/Byte"

    const-string v6, "valueOf"

    const-string v9, "(B)Ljava/lang/Byte;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_10
    const-string v5, "java/lang/Character"

    const-string v6, "valueOf"

    const-string v9, "(C)Ljava/lang/Character;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_11
    const-string v5, "java/lang/Boolean"

    const-string v6, "valueOf"

    const-string v9, "(Z)Ljava/lang/Boolean;"

    invoke-virtual {v15, v3, v5, v6, v9}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :pswitch_12
    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Ld0/a/a/q;->e(I)V

    :goto_a
    const/16 v3, 0xb0

    invoke-virtual {v15, v3}, Ld0/a/a/q;->e(I)V

    add-int/lit8 v12, v11, 0x1

    move/from16 v3, v16

    move-object/from16 v14, v17

    move-object/from16 v6, v18

    move-object/from16 v5, v19

    move-object/from16 v9, v20

    move-object/from16 v13, v23

    goto/16 :goto_4

    :cond_9
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object v0, v13

    invoke-virtual {v15, v0}, Ld0/a/a/q;->i(Ld0/a/a/p;)V

    const/4 v12, 0x3

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v0, 0x0

    const/16 v16, 0x0

    move-object v11, v15

    move-object v1, v15

    move v15, v0

    invoke-virtual/range {v11 .. v16}, Ld0/a/a/q;->c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V

    goto :goto_b

    :cond_a
    move-object/from16 v19, v5

    move-object/from16 v20, v9

    move-object v1, v15

    :goto_b
    const-string v0, "java/lang/IllegalArgumentException"

    const/16 v3, 0xbb

    invoke-virtual {v1, v3, v0}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    const/16 v0, 0x59

    invoke-virtual {v1, v0}, Ld0/a/a/q;->e(I)V

    const-string v5, "java/lang/StringBuilder"

    invoke-virtual {v1, v3, v5}, Ld0/a/a/q;->s(ILjava/lang/String;)V

    invoke-virtual {v1, v0}, Ld0/a/a/q;->e(I)V

    const-string v0, "Method not found: "

    invoke-virtual {v1, v0}, Ld0/a/a/q;->j(Ljava/lang/Object;)V

    const-string v0, "java/lang/StringBuilder"

    const-string v3, "<init>"

    const-string v5, "(Ljava/lang/String;)V"

    const/16 v6, 0xb7

    invoke-virtual {v1, v6, v0, v3, v5}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/16 v3, 0x15

    invoke-virtual {v1, v3, v0}, Ld0/a/a/q;->t(II)V

    const-string v0, "java/lang/StringBuilder"

    const-string v3, "append"

    const-string v5, "(I)Ljava/lang/StringBuilder;"

    const/16 v6, 0xb6

    invoke-virtual {v1, v6, v0, v3, v5}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "java/lang/StringBuilder"

    const-string v3, "toString"

    const-string v5, "()Ljava/lang/String;"

    invoke-virtual {v1, v6, v0, v3, v5}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "java/lang/IllegalArgumentException"

    const-string v3, "<init>"

    const-string v5, "(Ljava/lang/String;)V"

    const/16 v6, 0xb7

    invoke-virtual {v1, v6, v0, v3, v5}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xbf

    invoke-virtual {v1, v0}, Ld0/a/a/q;->e(I)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0, v0}, Ld0/a/a/q;->n(II)V

    invoke-virtual {v10}, Ld0/a/a/f;->b()[B

    move-result-object v0

    move-object/from16 v9, v20

    invoke-virtual {v2, v9, v0}, Lf/f/b/a;->a(Ljava/lang/String;[B)Ljava/lang/Class;

    move-result-object v0

    :goto_c
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_d
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/f/b/d;

    iput-object v4, v0, Lf/f/b/d;->a:[Ljava/lang/String;

    move-object/from16 v1, v19

    iput-object v1, v0, Lf/f/b/d;->b:[[Ljava/lang/Class;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-object v0

    :catchall_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error constructing method access class: "

    invoke-static {v2, v9}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :goto_e
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch
.end method

.method public static e(Ljava/lang/Class;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/reflect/Method;",
            ">;)V"
        }
    .end annotation

    invoke-static {p0, p1}, Lf/f/b/d;->a(Ljava/lang/Class;Ljava/util/ArrayList;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object p0

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p0, v1

    invoke-static {v2, p1}, Lf/f/b/d;->e(Ljava/lang/Class;Ljava/util/ArrayList;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public varargs c(Ljava/lang/String;[Ljava/lang/Class;)I
    .locals 3

    iget-object v0, p0, Lf/f/b/d;->a:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lf/f/b/d;->a:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lf/f/b/d;->b:[[Ljava/lang/Class;

    aget-object v2, v2, v1

    invoke-static {p2, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to find non-private method: "

    const-string v2, " "

    invoke-static {v1, p1, v2}, Lf/e/c/a/a;->M(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public varargs abstract d(Ljava/lang/Object;I[Ljava/lang/Object;)Ljava/lang/Object;
.end method
