.class public Lf/h/c/a0/e;
.super Ljava/lang/Object;
.source "FirebaseRemoteConfig.java"


# instance fields
.field public final a:Lf/h/c/j/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Lf/h/c/a0/k/e;

.field public final d:Lf/h/c/a0/k/e;

.field public final e:Lf/h/c/a0/k/e;

.field public final f:Lf/h/c/a0/k/k;

.field public final g:Lf/h/c/a0/k/m;

.field public final h:Lf/h/c/a0/k/n;

.field public final i:Lf/h/c/v/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/c/c;Lf/h/c/v/g;Lf/h/c/j/b;Ljava/util/concurrent/Executor;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/k;Lf/h/c/a0/k/m;Lf/h/c/a0/k/n;)V
    .locals 0
    .param p4    # Lf/h/c/j/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lf/h/c/a0/e;->i:Lf/h/c/v/g;

    iput-object p4, p0, Lf/h/c/a0/e;->a:Lf/h/c/j/b;

    iput-object p5, p0, Lf/h/c/a0/e;->b:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lf/h/c/a0/e;->c:Lf/h/c/a0/k/e;

    iput-object p7, p0, Lf/h/c/a0/e;->d:Lf/h/c/a0/k/e;

    iput-object p8, p0, Lf/h/c/a0/e;->e:Lf/h/c/a0/k/e;

    iput-object p9, p0, Lf/h/c/a0/e;->f:Lf/h/c/a0/k/k;

    iput-object p10, p0, Lf/h/c/a0/e;->g:Lf/h/c/a0/k/m;

    iput-object p11, p0, Lf/h/c/a0/e;->h:Lf/h/c/a0/k/n;

    return-void
.end method

.method public static b(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 7
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Map;
    .locals 12
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lf/h/c/a0/f;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/a0/e;->g:Lf/h/c/a0/k/m;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v2, v0, Lf/h/c/a0/k/m;->c:Lf/h/c/a0/k/e;

    invoke-static {v2}, Lf/h/c/a0/k/m;->b(Lf/h/c/a0/k/e;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v2, v0, Lf/h/c/a0/k/m;->d:Lf/h/c/a0/k/e;

    invoke-static {v2}, Lf/h/c/a0/k/m;->b(Lf/h/c/a0/k/e;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, v0, Lf/h/c/a0/k/m;->c:Lf/h/c/a0/k/e;

    invoke-static {v4}, Lf/h/c/a0/k/m;->a(Lf/h/c/a0/k/e;)Lf/h/c/a0/k/f;

    move-result-object v4

    const/4 v5, 0x0

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    :try_start_0
    iget-object v4, v4, Lf/h/c/a0/k/f;->b:Lorg/json/JSONObject;

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    :goto_1
    move-object v4, v5

    :goto_2
    const/4 v6, 0x2

    if-eqz v4, :cond_3

    iget-object v5, v0, Lf/h/c/a0/k/m;->c:Lf/h/c/a0/k/e;

    invoke-static {v5}, Lf/h/c/a0/k/m;->a(Lf/h/c/a0/k/e;)Lf/h/c/a0/k/f;

    move-result-object v5

    if-nez v5, :cond_1

    goto :goto_4

    :cond_1
    iget-object v7, v0, Lf/h/c/a0/k/m;->a:Ljava/util/Set;

    monitor-enter v7

    :try_start_1
    iget-object v8, v0, Lf/h/c/a0/k/m;->a:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/a/f/f/n/b;

    iget-object v10, v0, Lf/h/c/a0/k/m;->b:Ljava/util/concurrent/Executor;

    new-instance v11, Lf/h/c/a0/k/l;

    invoke-direct {v11, v9, v3, v5}, Lf/h/c/a0/k/l;-><init>(Lf/h/a/f/f/n/b;Ljava/lang/String;Lf/h/c/a0/k/f;)V

    invoke-interface {v10, v11}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_3

    :cond_2
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_4
    new-instance v5, Lf/h/c/a0/k/p;

    invoke-direct {v5, v4, v6}, Lf/h/c/a0/k/p;-><init>(Ljava/lang/String;I)V

    goto :goto_6

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    iget-object v4, v0, Lf/h/c/a0/k/m;->d:Lf/h/c/a0/k/e;

    invoke-static {v4}, Lf/h/c/a0/k/m;->a(Lf/h/c/a0/k/e;)Lf/h/c/a0/k/f;

    move-result-object v4

    if-nez v4, :cond_4

    goto :goto_5

    :cond_4
    :try_start_3
    iget-object v4, v4, Lf/h/c/a0/k/f;->b:Lorg/json/JSONObject;

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_5

    :catch_1
    nop

    :goto_5
    const/4 v4, 0x1

    if-eqz v5, :cond_5

    new-instance v6, Lf/h/c/a0/k/p;

    invoke-direct {v6, v5, v4}, Lf/h/c/a0/k/p;-><init>(Ljava/lang/String;I)V

    move-object v5, v6

    goto :goto_6

    :cond_5
    const-string v5, "FirebaseRemoteConfigValue"

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    aput-object v3, v6, v4

    const-string v4, "No value of type \'%s\' exists for parameter key \'%s\'."

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "FirebaseRemoteConfig"

    invoke-static {v5, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lf/h/c/a0/k/p;

    const-string v4, ""

    invoke-direct {v5, v4, v7}, Lf/h/c/a0/k/p;-><init>(Ljava/lang/String;I)V

    :goto_6
    invoke-virtual {v2, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_6
    return-object v2
.end method
