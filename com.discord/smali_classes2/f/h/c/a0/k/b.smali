.class public final synthetic Lf/h/c/a0/k/b;
.super Ljava/lang/Object;
.source "ConfigCacheClient.java"

# interfaces
.implements Lf/h/a/f/p/f;


# instance fields
.field public final a:Lf/h/c/a0/k/e;

.field public final b:Z

.field public final c:Lf/h/c/a0/k/f;


# direct methods
.method public constructor <init>(Lf/h/c/a0/k/e;ZLf/h/c/a0/k/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/a0/k/b;->a:Lf/h/c/a0/k/e;

    iput-boolean p2, p0, Lf/h/c/a0/k/b;->b:Z

    iput-object p3, p0, Lf/h/c/a0/k/b;->c:Lf/h/c/a0/k/f;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 3

    iget-object v0, p0, Lf/h/c/a0/k/b;->a:Lf/h/c/a0/k/e;

    iget-boolean v1, p0, Lf/h/c/a0/k/b;->b:Z

    iget-object v2, p0, Lf/h/c/a0/k/b;->c:Lf/h/c/a0/k/f;

    check-cast p1, Ljava/lang/Void;

    sget-object p1, Lf/h/c/a0/k/e;->d:Ljava/util/Map;

    if-eqz v1, :cond_0

    monitor-enter v0

    :try_start_0
    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    iput-object p1, v0, Lf/h/c/a0/k/e;->c:Lcom/google/android/gms/tasks/Task;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_0
    :goto_0
    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method
