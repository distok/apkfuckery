.class public final synthetic Lf/h/c/a0/k/h;
.super Ljava/lang/Object;
.source "ConfigFetchHandler.java"

# interfaces
.implements Lf/h/a/f/p/a;


# instance fields
.field public final a:Lf/h/c/a0/k/k;

.field public final b:Lcom/google/android/gms/tasks/Task;

.field public final c:Lcom/google/android/gms/tasks/Task;

.field public final d:Ljava/util/Date;


# direct methods
.method public constructor <init>(Lf/h/c/a0/k/k;Lcom/google/android/gms/tasks/Task;Lcom/google/android/gms/tasks/Task;Ljava/util/Date;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/a0/k/h;->a:Lf/h/c/a0/k/k;

    iput-object p2, p0, Lf/h/c/a0/k/h;->b:Lcom/google/android/gms/tasks/Task;

    iput-object p3, p0, Lf/h/c/a0/k/h;->c:Lcom/google/android/gms/tasks/Task;

    iput-object p4, p0, Lf/h/c/a0/k/h;->d:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    .locals 4

    iget-object p1, p0, Lf/h/c/a0/k/h;->a:Lf/h/c/a0/k/k;

    iget-object v0, p0, Lf/h/c/a0/k/h;->b:Lcom/google/android/gms/tasks/Task;

    iget-object v1, p0, Lf/h/c/a0/k/h;->c:Lcom/google/android/gms/tasks/Task;

    iget-object v2, p0, Lf/h/c/a0/k/h;->d:Ljava/util/Date;

    sget-object v3, Lf/h/c/a0/k/k;->j:[I

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->p()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance p1, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigClientException;

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->k()Ljava/lang/Exception;

    move-result-object v0

    const-string v1, "Firebase Installations failed to get installation ID for fetch."

    invoke-direct {p1, v1, v0}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->s(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/tasks/Task;->p()Z

    move-result v3

    if-nez v3, :cond_1

    new-instance p1, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigClientException;

    invoke-virtual {v1}, Lcom/google/android/gms/tasks/Task;->k()Ljava/lang/Exception;

    move-result-object v0

    const-string v1, "Firebase Installations failed to get installation auth token for fetch."

    invoke-direct {p1, v1, v0}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->s(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->l()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/tasks/Task;->l()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/v/k;

    invoke-virtual {v1}, Lf/h/c/v/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p1, v0, v1, v2}, Lf/h/c/a0/k/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lf/h/c/a0/k/k$a;

    move-result-object v0

    iget v1, v0, Lf/h/c/a0/k/k$a;->a:I

    if-eqz v1, :cond_2

    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lf/h/c/a0/k/k;->e:Lf/h/c/a0/k/e;

    iget-object v2, v0, Lf/h/c/a0/k/k$a;->b:Lf/h/c/a0/k/f;

    invoke-virtual {v1, v2}, Lf/h/c/a0/k/e;->c(Lf/h/c/a0/k/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    iget-object p1, p1, Lf/h/c/a0/k/k;->c:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/h/c/a0/k/j;

    invoke-direct {v2, v0}, Lf/h/c/a0/k/j;-><init>(Lf/h/c/a0/k/k$a;)V

    invoke-virtual {v1, p1, v2}, Lcom/google/android/gms/tasks/Task;->r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->s(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_0
    return-object p1
.end method
