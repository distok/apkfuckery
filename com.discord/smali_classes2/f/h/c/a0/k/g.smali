.class public final synthetic Lf/h/c/a0/k/g;
.super Ljava/lang/Object;
.source "ConfigFetchHandler.java"

# interfaces
.implements Lf/h/a/f/p/a;


# instance fields
.field public final a:Lf/h/c/a0/k/k;

.field public final b:J


# direct methods
.method public constructor <init>(Lf/h/c/a0/k/k;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/a0/k/g;->a:Lf/h/c/a0/k/k;

    iput-wide p2, p0, Lf/h/c/a0/k/g;->b:J

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    .locals 11

    iget-object v0, p0, Lf/h/c/a0/k/g;->a:Lf/h/c/a0/k/k;

    iget-wide v1, p0, Lf/h/c/a0/k/g;->b:J

    sget-object v3, Lf/h/c/a0/k/k;->j:[I

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->p()Z

    move-result p1

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-eqz p1, :cond_1

    iget-object p1, v0, Lf/h/c/a0/k/k;->g:Lf/h/c/a0/k/n;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Ljava/util/Date;

    iget-object p1, p1, Lf/h/c/a0/k/n;->a:Landroid/content/SharedPreferences;

    const-wide/16 v8, -0x1

    const-string v10, "last_fetch_time_in_millis"

    invoke-interface {p1, v10, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    sget-object p1, Lf/h/c/a0/k/n;->d:Ljava/util/Date;

    invoke-virtual {v7, p1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v9, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    add-long/2addr v1, v7

    invoke-direct {p1, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, p1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result p1

    :goto_0
    if-eqz p1, :cond_1

    new-instance p1, Lf/h/c/a0/k/k$a;

    invoke-direct {p1, v3, v4, v5, v5}, Lf/h/c/a0/k/k$a;-><init>(Ljava/util/Date;ILf/h/c/a0/k/f;Ljava/lang/String;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_2

    :cond_1
    iget-object p1, v0, Lf/h/c/a0/k/k;->g:Lf/h/c/a0/k/n;

    invoke-virtual {p1}, Lf/h/c/a0/k/n;->a()Lf/h/c/a0/k/n$a;

    move-result-object p1

    iget-object p1, p1, Lf/h/c/a0/k/n$a;->b:Ljava/util/Date;

    invoke-virtual {v3, p1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v5, p1

    :cond_2
    const/4 p1, 0x1

    if-eqz v5, :cond_3

    new-instance v1, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigFetchThrottledException;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    sub-long/2addr v7, v9

    new-array p1, p1, [Ljava/lang/Object;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v7, v8}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v7

    invoke-static {v7, v8}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v6

    const-string v2, "Fetch is throttled. Please wait before calling fetch again: %s"

    invoke-static {v2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-direct {v1, p1, v4, v5}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigFetchThrottledException;-><init>(Ljava/lang/String;J)V

    invoke-static {v1}, Lf/h/a/f/f/n/g;->s(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_1

    :cond_3
    iget-object v1, v0, Lf/h/c/a0/k/k;->a:Lf/h/c/v/g;

    invoke-interface {v1}, Lf/h/c/v/g;->getId()Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    iget-object v2, v0, Lf/h/c/a0/k/k;->a:Lf/h/c/v/g;

    invoke-interface {v2, v6}, Lf/h/c/v/g;->a(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v2

    new-array v4, v4, [Lcom/google/android/gms/tasks/Task;

    aput-object v1, v4, v6

    aput-object v2, v4, p1

    invoke-static {v4}, Lf/h/a/f/f/n/g;->h0([Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    iget-object v4, v0, Lf/h/c/a0/k/k;->c:Ljava/util/concurrent/Executor;

    new-instance v5, Lf/h/c/a0/k/h;

    invoke-direct {v5, v0, v1, v2, v3}, Lf/h/c/a0/k/h;-><init>(Lf/h/c/a0/k/k;Lcom/google/android/gms/tasks/Task;Lcom/google/android/gms/tasks/Task;Ljava/util/Date;)V

    invoke-virtual {p1, v4, v5}, Lcom/google/android/gms/tasks/Task;->j(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_1
    iget-object v1, v0, Lf/h/c/a0/k/k;->c:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/h/c/a0/k/i;

    invoke-direct {v2, v0, v3}, Lf/h/c/a0/k/i;-><init>(Lf/h/c/a0/k/k;Ljava/util/Date;)V

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/tasks/Task;->j(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_2
    return-object p1
.end method
