.class public final synthetic Lf/h/c/a0/k/a;
.super Ljava/lang/Object;
.source "ConfigCacheClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field public final d:Lf/h/c/a0/k/e;

.field public final e:Lf/h/c/a0/k/f;


# direct methods
.method public constructor <init>(Lf/h/c/a0/k/e;Lf/h/c/a0/k/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/a0/k/a;->d:Lf/h/c/a0/k/e;

    iput-object p2, p0, Lf/h/c/a0/k/a;->e:Lf/h/c/a0/k/f;

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lf/h/c/a0/k/a;->d:Lf/h/c/a0/k/e;

    iget-object v1, p0, Lf/h/c/a0/k/a;->e:Lf/h/c/a0/k/f;

    iget-object v0, v0, Lf/h/c/a0/k/e;->b:Lf/h/c/a0/k/o;

    monitor-enter v0

    :try_start_0
    iget-object v2, v0, Lf/h/c/a0/k/o;->a:Landroid/content/Context;

    iget-object v3, v0, Lf/h/c/a0/k/o;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lf/h/c/a0/k/f;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "UTF-8"

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
.end method
