.class public Lf/h/c/a0/i;
.super Ljava/lang/Object;
.source "RemoteConfigComponent.java"


# static fields
.field public static final j:Ljava/util/Random;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "this"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lf/h/c/a0/e;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Lf/h/c/c;

.field public final e:Lf/h/c/v/g;

.field public final f:Lf/h/c/j/b;

.field public final g:Lf/h/c/k/a/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;

.field public i:Ljava/util/Map;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "this"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lf/h/c/a0/i;->j:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lf/h/c/c;Lf/h/c/v/g;Lf/h/c/j/b;Lf/h/c/k/a/a;)V
    .locals 2
    .param p5    # Lf/h/c/k/a/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lf/h/c/a0/i;->a:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lf/h/c/a0/i;->i:Ljava/util/Map;

    iput-object p1, p0, Lf/h/c/a0/i;->b:Landroid/content/Context;

    iput-object v0, p0, Lf/h/c/a0/i;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lf/h/c/a0/i;->d:Lf/h/c/c;

    iput-object p3, p0, Lf/h/c/a0/i;->e:Lf/h/c/v/g;

    iput-object p4, p0, Lf/h/c/a0/i;->f:Lf/h/c/j/b;

    iput-object p5, p0, Lf/h/c/a0/i;->g:Lf/h/c/k/a/a;

    invoke-virtual {p2}, Lf/h/c/c;->a()V

    iget-object p1, p2, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object p1, p1, Lf/h/c/i;->b:Ljava/lang/String;

    iput-object p1, p0, Lf/h/c/a0/i;->h:Ljava/lang/String;

    new-instance p1, Lf/h/c/a0/g;

    invoke-direct {p1, p0}, Lf/h/c/a0/g;-><init>(Lf/h/c/a0/i;)V

    invoke-static {v0, p1}, Lf/h/a/f/f/n/g;->f(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public static e(Lf/h/c/c;)Z
    .locals 1

    invoke-virtual {p0}, Lf/h/c/c;->a()V

    iget-object p0, p0, Lf/h/c/c;->b:Ljava/lang/String;

    const-string v0, "[DEFAULT]"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public declared-synchronized a(Lf/h/c/c;Ljava/lang/String;Lf/h/c/v/g;Lf/h/c/j/b;Ljava/util/concurrent/Executor;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/k;Lf/h/c/a0/k/m;Lf/h/c/a0/k/n;)Lf/h/c/a0/e;
    .locals 15
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    move-object v1, p0

    move-object/from16 v0, p2

    monitor-enter p0

    :try_start_0
    iget-object v2, v1, Lf/h/c/a0/i;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Lf/h/c/a0/e;

    iget-object v4, v1, Lf/h/c/a0/i;->b:Landroid/content/Context;

    const-string v3, "firebase"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p1 .. p1}, Lf/h/c/c;->a()V

    move-object/from16 v5, p1

    iget-object v3, v5, Lf/h/c/c;->b:Ljava/lang/String;

    const-string v6, "[DEFAULT]"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v5, p1

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_2

    move-object/from16 v7, p4

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    move-object v7, v3

    :goto_1
    move-object v3, v2

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    invoke-direct/range {v3 .. v14}, Lf/h/c/a0/e;-><init>(Landroid/content/Context;Lf/h/c/c;Lf/h/c/v/g;Lf/h/c/j/b;Ljava/util/concurrent/Executor;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/k;Lf/h/c/a0/k/m;Lf/h/c/a0/k/n;)V

    invoke-virtual/range {p7 .. p7}, Lf/h/c/a0/k/e;->b()Lcom/google/android/gms/tasks/Task;

    iget-object v3, v2, Lf/h/c/a0/e;->e:Lf/h/c/a0/k/e;

    invoke-virtual {v3}, Lf/h/c/a0/k/e;->b()Lcom/google/android/gms/tasks/Task;

    iget-object v3, v2, Lf/h/c/a0/e;->c:Lf/h/c/a0/k/e;

    invoke-virtual {v3}, Lf/h/c/a0/k/e;->b()Lcom/google/android/gms/tasks/Task;

    iget-object v3, v1, Lf/h/c/a0/i;->a:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v2, v1, Lf/h/c/a0/i;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/a0/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)Lf/h/c/a0/e;
    .locals 13
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "fetch"

    invoke-virtual {p0, p1, v0}, Lf/h/c/a0/i;->c(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/a0/k/e;

    move-result-object v7

    const-string v0, "activate"

    invoke-virtual {p0, p1, v0}, Lf/h/c/a0/i;->c(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/a0/k/e;

    move-result-object v8

    const-string v0, "defaults"

    invoke-virtual {p0, p1, v0}, Lf/h/c/a0/i;->c(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/a0/k/e;

    move-result-object v9

    iget-object v0, p0, Lf/h/c/a0/i;->b:Landroid/content/Context;

    iget-object v1, p0, Lf/h/c/a0/i;->h:Ljava/lang/String;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "frc"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object v1, v2, v3

    const/4 v1, 0x2

    aput-object p1, v2, v1

    const/4 v1, 0x3

    const-string v3, "settings"

    aput-object v3, v2, v1

    const-string v1, "%s_%s_%s_%s"

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v12, Lf/h/c/a0/k/n;

    invoke-direct {v12, v0}, Lf/h/c/a0/k/n;-><init>(Landroid/content/SharedPreferences;)V

    new-instance v11, Lf/h/c/a0/k/m;

    iget-object v0, p0, Lf/h/c/a0/i;->c:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v11, v0, v8, v9}, Lf/h/c/a0/k/m;-><init>(Ljava/util/concurrent/Executor;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;)V

    iget-object v0, p0, Lf/h/c/a0/i;->d:Lf/h/c/c;

    iget-object v1, p0, Lf/h/c/a0/i;->g:Lf/h/c/k/a/a;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->b:Ljava/lang/String;

    const-string v2, "[DEFAULT]"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "firebase"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    new-instance v0, Lf/h/c/a0/k/q;

    invoke-direct {v0, v1}, Lf/h/c/a0/k/q;-><init>(Lf/h/c/k/a/a;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v1, Lf/h/c/a0/h;

    invoke-direct {v1, v0}, Lf/h/c/a0/h;-><init>(Lf/h/c/a0/k/q;)V

    iget-object v0, v11, Lf/h/c/a0/k/m;->a:Ljava/util/Set;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, v11, Lf/h/c/a0/k/m;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw p1

    :cond_1
    :goto_1
    iget-object v2, p0, Lf/h/c/a0/i;->d:Lf/h/c/c;

    iget-object v4, p0, Lf/h/c/a0/i;->e:Lf/h/c/v/g;

    iget-object v5, p0, Lf/h/c/a0/i;->f:Lf/h/c/j/b;

    iget-object v6, p0, Lf/h/c/a0/i;->c:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {p0, p1, v7, v12}, Lf/h/c/a0/i;->d(Ljava/lang/String;Lf/h/c/a0/k/e;Lf/h/c/a0/k/n;)Lf/h/c/a0/k/k;

    move-result-object v10

    move-object v1, p0

    move-object v3, p1

    invoke-virtual/range {v1 .. v12}, Lf/h/c/a0/i;->a(Lf/h/c/c;Ljava/lang/String;Lf/h/c/v/g;Lf/h/c/j/b;Ljava/util/concurrent/Executor;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/e;Lf/h/c/a0/k/k;Lf/h/c/a0/k/m;Lf/h/c/a0/k/n;)Lf/h/c/a0/e;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/a0/k/e;
    .locals 4

    const-string v0, "%s_%s_%s_%s.json"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "frc"

    aput-object v3, v1, v2

    iget-object v2, p0, Lf/h/c/a0/i;->h:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 p1, 0x3

    aput-object p2, v1, p1

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object p2

    iget-object v0, p0, Lf/h/c/a0/i;->b:Landroid/content/Context;

    sget-object v1, Lf/h/c/a0/k/o;->c:Ljava/util/Map;

    const-class v1, Lf/h/c/a0/k/o;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lf/h/c/a0/k/o;->c:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lf/h/c/a0/k/o;

    invoke-direct {v3, v0, p1}, Lf/h/c/a0/k/o;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/c/a0/k/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v1

    sget-object v0, Lf/h/c/a0/k/e;->d:Ljava/util/Map;

    const-class v0, Lf/h/c/a0/k/e;

    monitor-enter v0

    :try_start_1
    iget-object v1, p1, Lf/h/c/a0/k/o;->b:Ljava/lang/String;

    sget-object v2, Lf/h/c/a0/k/e;->d:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Lf/h/c/a0/k/e;

    invoke-direct {v3, p2, p1}, Lf/h/c/a0/k/e;-><init>(Ljava/util/concurrent/ExecutorService;Lf/h/c/a0/k/o;)V

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/c/a0/k/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit v1

    throw p1
.end method

.method public declared-synchronized d(Ljava/lang/String;Lf/h/c/a0/k/e;Lf/h/c/a0/k/n;)Lf/h/c/a0/k/k;
    .locals 23
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p3

    monitor-enter p0

    :try_start_0
    new-instance v12, Lf/h/c/a0/k/k;

    iget-object v3, v1, Lf/h/c/a0/i;->e:Lf/h/c/v/g;

    iget-object v2, v1, Lf/h/c/a0/i;->d:Lf/h/c/c;

    invoke-static {v2}, Lf/h/c/a0/i;->e(Lf/h/c/c;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lf/h/c/a0/i;->g:Lf/h/c/k/a/a;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    move-object v4, v2

    iget-object v5, v1, Lf/h/c/a0/i;->c:Ljava/util/concurrent/ExecutorService;

    sget-object v6, Lf/h/a/f/f/n/d;->a:Lf/h/a/f/f/n/d;

    sget-object v7, Lf/h/c/a0/i;->j:Ljava/util/Random;

    iget-object v2, v1, Lf/h/c/a0/i;->d:Lf/h/c/c;

    invoke-virtual {v2}, Lf/h/c/c;->a()V

    iget-object v2, v2, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v2, v2, Lf/h/c/i;->a:Ljava/lang/String;

    iget-object v8, v1, Lf/h/c/a0/i;->d:Lf/h/c/c;

    invoke-virtual {v8}, Lf/h/c/c;->a()V

    iget-object v8, v8, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v15, v8, Lf/h/c/i;->b:Ljava/lang/String;

    new-instance v9, Lcom/google/firebase/remoteconfig/internal/ConfigFetchHttpClient;

    iget-object v14, v1, Lf/h/c/a0/i;->b:Landroid/content/Context;

    iget-object v8, v0, Lf/h/c/a0/k/n;->a:Landroid/content/SharedPreferences;

    const-string v10, "fetch_timeout_in_seconds"

    move-object v11, v6

    move-object/from16 v22, v7

    const-wide/16 v6, 0x3c

    invoke-interface {v8, v10, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v18

    iget-object v8, v0, Lf/h/c/a0/k/n;->a:Landroid/content/SharedPreferences;

    invoke-interface {v8, v10, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v20

    move-object v13, v9

    move-object/from16 v16, v2

    move-object/from16 v17, p1

    invoke-direct/range {v13 .. v21}, Lcom/google/firebase/remoteconfig/internal/ConfigFetchHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    iget-object v13, v1, Lf/h/c/a0/i;->i:Ljava/util/Map;

    move-object v2, v12

    move-object v6, v11

    move-object/from16 v7, v22

    move-object/from16 v8, p2

    move-object/from16 v10, p3

    move-object v11, v13

    invoke-direct/range {v2 .. v11}, Lf/h/c/a0/k/k;-><init>(Lf/h/c/v/g;Lf/h/c/k/a/a;Ljava/util/concurrent/Executor;Lf/h/a/f/f/n/c;Ljava/util/Random;Lf/h/c/a0/k/e;Lcom/google/firebase/remoteconfig/internal/ConfigFetchHttpClient;Lf/h/c/a0/k/n;Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v12

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
