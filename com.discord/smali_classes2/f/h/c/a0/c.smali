.class public final synthetic Lf/h/c/a0/c;
.super Ljava/lang/Object;
.source "FirebaseRemoteConfig.java"

# interfaces
.implements Lf/h/a/f/p/a;


# instance fields
.field public final a:Lf/h/c/a0/e;

.field public final b:Lcom/google/android/gms/tasks/Task;

.field public final c:Lcom/google/android/gms/tasks/Task;


# direct methods
.method public constructor <init>(Lf/h/c/a0/e;Lcom/google/android/gms/tasks/Task;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/a0/c;->a:Lf/h/c/a0/e;

    iput-object p2, p0, Lf/h/c/a0/c;->b:Lcom/google/android/gms/tasks/Task;

    iput-object p3, p0, Lf/h/c/a0/c;->c:Lcom/google/android/gms/tasks/Task;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    .locals 4

    iget-object p1, p0, Lf/h/c/a0/c;->a:Lf/h/c/a0/e;

    iget-object v0, p0, Lf/h/c/a0/c;->b:Lcom/google/android/gms/tasks/Task;

    iget-object v1, p0, Lf/h/c/a0/c;->c:Lcom/google/android/gms/tasks/Task;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->p()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->l()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->l()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/a0/k/f;

    invoke-virtual {v1}, Lcom/google/android/gms/tasks/Task;->p()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/tasks/Task;->l()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/a0/k/f;

    if-eqz v1, :cond_2

    iget-object v3, v0, Lf/h/c/a0/k/f;->c:Ljava/util/Date;

    iget-object v1, v1, Lf/h/c/a0/k/f;->c:Ljava/util/Date;

    invoke-virtual {v3, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_3

    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_3

    :cond_3
    iget-object v1, p1, Lf/h/c/a0/e;->d:Lf/h/c/a0/k/e;

    invoke-virtual {v1, v0}, Lf/h/c/a0/k/e;->c(Lf/h/c/a0/k/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    iget-object v1, p1, Lf/h/c/a0/e;->b:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/h/c/a0/a;

    invoke-direct {v2, p1}, Lf/h/c/a0/a;-><init>(Lf/h/c/a0/e;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_3

    :cond_4
    :goto_2
    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_3
    return-object p1
.end method
