.class public abstract Lf/h/c/l/d/e;
.super Lf/h/a/f/f/h/i/p;
.source "com.google.firebase:firebase-appindexing@@19.1.0"

# interfaces
.implements Lf/h/a/f/f/h/i/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/h/i/p<",
        "Lf/h/a/f/i/i/g;",
        "Ljava/lang/Void;",
        ">;",
        "Lf/h/a/f/f/h/i/e<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/google/android/gms/tasks/TaskCompletionSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/c/l/d/d;)V
    .locals 0

    invoke-direct {p0}, Lf/h/a/f/f/h/i/p;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->M0()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Failed result must not be success."

    invoke-static {v0, v1}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    iget-object v0, p0, Lf/h/c/l/d/e;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, p1, Lcom/google/android/gms/common/api/Status;->f:Ljava/lang/String;

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->E0(Lcom/google/android/gms/common/api/Status;Ljava/lang/String;)Lcom/google/firebase/appindexing/FirebaseAppIndexingException;

    move-result-object p1

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {v0, p1}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->M0()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/h/c/l/d/e;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    const/4 v0, 0x0

    iget-object p1, p1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {p1, v0}, Lf/h/a/f/p/b0;->t(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/c/l/d/e;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    const-string v1, "User Action indexing error, please try again."

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->E0(Lcom/google/android/gms/common/api/Status;Ljava/lang/String;)Lcom/google/firebase/appindexing/FirebaseAppIndexingException;

    move-result-object p1

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {v0, p1}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void
.end method

.method public c(Lf/h/a/f/f/h/a$b;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lf/h/a/f/i/i/g;

    iput-object p2, p0, Lf/h/c/l/d/e;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/i/b;

    move-object p2, p0

    check-cast p2, Lf/h/c/l/d/d;

    new-instance v0, Lf/h/a/f/i/i/h;

    invoke-direct {v0, p2}, Lf/h/a/f/i/i/h;-><init>(Lf/h/a/f/f/h/i/e;)V

    iget-object p2, p2, Lf/h/c/l/d/d;->b:[Lcom/google/firebase/appindexing/internal/zza;

    invoke-interface {p1, v0, p2}, Lf/h/a/f/i/i/b;->K(Lf/h/a/f/i/i/c;[Lcom/google/firebase/appindexing/internal/zza;)V

    return-void
.end method
