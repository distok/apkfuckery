.class public final Lf/h/c/l/d/c;
.super Lf/h/a/f/f/h/b;
.source "com.google.firebase:firebase-appindexing@@19.1.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/h/b<",
        "Lf/h/a/f/f/h/a$d$c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    sget-object v0, Lf/h/a/f/i/i/k;->c:Lf/h/a/f/f/h/a;

    new-instance v1, Lf/h/c/h;

    invoke-direct {v1}, Lf/h/c/h;-><init>()V

    const-string v2, "StatusExceptionMapper must not be null."

    invoke-static {v1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lf/h/a/f/f/h/b$a;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4, v2}, Lf/h/a/f/f/h/b$a;-><init>(Lf/h/a/f/f/h/i/n;Landroid/accounts/Account;Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0, v4, v3}, Lf/h/a/f/f/h/b;-><init>(Landroid/content/Context;Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;Lf/h/a/f/f/h/b$a;)V

    return-void
.end method
