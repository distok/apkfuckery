.class public final Lf/h/c/k/b;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-api@@18.0.0"

# interfaces
.implements Lf/h/a/f/j/b/b7;


# instance fields
.field public final synthetic a:Lf/h/a/f/i/j/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/g;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/cc;

    invoke-direct {v1}, Lf/h/a/f/i/j/cc;-><init>()V

    new-instance v2, Lf/h/a/f/i/j/t;

    invoke-direct {v2, v0, v1}, Lf/h/a/f/i/j/t;-><init>(Lf/h/a/f/i/j/g;Lf/h/a/f/i/j/cc;)V

    iget-object v0, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/i/j/cc;->h(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/cc;

    invoke-direct {v1}, Lf/h/a/f/i/j/cc;-><init>()V

    new-instance v2, Lf/h/a/f/i/j/x;

    invoke-direct {v2, v0, v1}, Lf/h/a/f/i/j/x;-><init>(Lf/h/a/f/i/j/g;Lf/h/a/f/i/j/cc;)V

    iget-object v0, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/i/j/cc;->h(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/cc;

    invoke-direct {v1}, Lf/h/a/f/i/j/cc;-><init>()V

    new-instance v2, Lf/h/a/f/i/j/r;

    invoke-direct {v2, v0, v1}, Lf/h/a/f/i/j/r;-><init>(Lf/h/a/f/i/j/g;Lf/h/a/f/i/j/cc;)V

    iget-object v0, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/i/j/cc;->h(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/cc;

    invoke-direct {v1}, Lf/h/a/f/i/j/cc;-><init>()V

    new-instance v2, Lf/h/a/f/i/j/s;

    invoke-direct {v2, v0, v1}, Lf/h/a/f/i/j/s;-><init>(Lf/h/a/f/i/j/g;Lf/h/a/f/i/j/cc;)V

    iget-object v0, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/i/j/cc;->h(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 6

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/cc;

    invoke-direct {v1}, Lf/h/a/f/i/j/cc;-><init>()V

    new-instance v2, Lf/h/a/f/i/j/u;

    invoke-direct {v2, v0, v1}, Lf/h/a/f/i/j/u;-><init>(Lf/h/a/f/i/j/g;Lf/h/a/f/i/j/cc;)V

    iget-object v3, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/i/j/cc;->o0(J)Landroid/os/Bundle;

    move-result-object v1

    const-class v2, Ljava/lang/Long;

    invoke-static {v1, v2}, Lf/h/a/f/i/j/cc;->g(Landroid/os/Bundle;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-object v4, v0, Lf/h/a/f/i/j/g;->b:Lf/h/a/f/f/n/c;

    check-cast v4, Lf/h/a/f/f/n/d;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    xor-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v1

    iget v3, v0, Lf/h/a/f/i/j/g;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lf/h/a/f/i/j/g;->f:I

    int-to-long v3, v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    :goto_0
    return-wide v1
.end method

.method public final f(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/i;

    invoke-direct {v1, v0, p1}, Lf/h/a/f/i/j/i;-><init>(Lf/h/a/f/i/j/g;Landroid/os/Bundle;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/q;

    invoke-direct {v1, v0, p1}, Lf/h/a/f/i/j/q;-><init>(Lf/h/a/f/i/j/g;Ljava/lang/String;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final h(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-virtual {v0, p1, p2}, Lf/h/a/f/i/j/g;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final i(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/j/g;->h(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public final j(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/p;

    invoke-direct {v1, v0, p1}, Lf/h/a/f/i/j/p;-><init>(Lf/h/a/f/i/j/g;Ljava/lang/String;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final k(Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/f/i/j/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public final l(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/i/j/l;

    invoke-direct {v1, v0, p1, p2, p3}, Lf/h/a/f/i/j/l;-><init>(Lf/h/a/f/i/j/g;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final m(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    iget-object v0, p0, Lf/h/c/k/b;->a:Lf/h/a/f/i/j/g;

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, Lf/h/a/f/i/j/g;->d(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZLjava/lang/Long;)V

    return-void
.end method
