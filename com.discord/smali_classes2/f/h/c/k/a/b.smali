.class public Lf/h/c/k/a/b;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-api@@18.0.0"

# interfaces
.implements Lf/h/c/k/a/a;


# static fields
.field public static volatile c:Lf/h/c/k/a/a;


# instance fields
.field public final a:Lf/h/a/f/j/a/a;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/f/j/a/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "null reference"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lf/h/c/k/a/b;->b:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Z)Ljava/util/Map;
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    iget-object v0, v0, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, p1}, Lf/h/a/f/i/j/g;->b(Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public b(Lf/h/c/k/a/a$c;)V
    .locals 7
    .param p1    # Lf/h/c/k/a/a$c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/h/c/k/a/c/c;->a:Ljava/util/Set;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto/16 :goto_3

    :cond_0
    iget-object v1, p1, Lf/h/c/k/a/a$c;->a:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto/16 :goto_3

    :cond_1
    iget-object v2, p1, Lf/h/c/k/a/a$c;->c:Ljava/lang/Object;

    if-eqz v2, :cond_4

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v5, Ljava/io/ObjectOutputStream;

    invoke-direct {v5, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-virtual {v5, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v5}, Ljava/io/ObjectOutputStream;->flush()V

    new-instance v2, Ljava/io/ObjectInputStream;

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-direct {v6, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v6}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v5}, Ljava/io/ObjectOutputStream;->close()V

    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V

    move-object v3, v4

    goto :goto_2

    :catchall_0
    move-exception v4

    goto :goto_0

    :catchall_1
    move-exception v2

    move-object v4, v2

    move-object v2, v3

    goto :goto_0

    :catchall_2
    move-exception v2

    move-object v4, v2

    move-object v2, v3

    move-object v5, v2

    :goto_0
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/ObjectOutputStream;->close()V

    goto :goto_1

    :catch_0
    nop

    goto :goto_2

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V

    :cond_3
    throw v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_2
    if-nez v3, :cond_4

    goto :goto_3

    :cond_4
    invoke-static {v1}, Lf/h/c/k/a/c/c;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_3

    :cond_5
    iget-object v2, p1, Lf/h/c/k/a/a$c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lf/h/c/k/a/c/c;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_3

    :cond_6
    iget-object v2, p1, Lf/h/c/k/a/a$c;->k:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lf/h/c/k/a/a$c;->l:Landroid/os/Bundle;

    invoke-static {v2, v3}, Lf/h/c/k/a/c/c;->b(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_3

    :cond_7
    iget-object v2, p1, Lf/h/c/k/a/a$c;->k:Ljava/lang/String;

    iget-object v3, p1, Lf/h/c/k/a/a$c;->l:Landroid/os/Bundle;

    invoke-static {v1, v2, v3}, Lf/h/c/k/a/c/c;->d(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_8

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lf/h/c/k/a/a$c;->h:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v3, p1, Lf/h/c/k/a/a$c;->i:Landroid/os/Bundle;

    invoke-static {v2, v3}, Lf/h/c/k/a/c/c;->b(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_9
    iget-object v2, p1, Lf/h/c/k/a/a$c;->h:Ljava/lang/String;

    iget-object v3, p1, Lf/h/c/k/a/a$c;->i:Landroid/os/Bundle;

    invoke-static {v1, v2, v3}, Lf/h/c/k/a/c/c;->d(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_a

    goto :goto_3

    :cond_a
    iget-object v2, p1, Lf/h/c/k/a/a$c;->f:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v3, p1, Lf/h/c/k/a/a$c;->g:Landroid/os/Bundle;

    invoke-static {v2, v3}, Lf/h/c/k/a/c/c;->b(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_3

    :cond_b
    iget-object v2, p1, Lf/h/c/k/a/a$c;->f:Ljava/lang/String;

    iget-object v3, p1, Lf/h/c/k/a/a$c;->g:Landroid/os/Bundle;

    invoke-static {v1, v2, v3}, Lf/h/c/k/a/c/c;->d(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v1

    if-nez v1, :cond_c

    goto :goto_3

    :cond_c
    const/4 v0, 0x1

    :cond_d
    :goto_3
    if-nez v0, :cond_e

    return-void

    :cond_e
    iget-object v0, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p1, Lf/h/c/k/a/a$c;->a:Ljava/lang/String;

    if-eqz v2, :cond_f

    const-string v3, "origin"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    iget-object v2, p1, Lf/h/c/k/a/a$c;->b:Ljava/lang/String;

    if-eqz v2, :cond_10

    const-string v3, "name"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    iget-object v2, p1, Lf/h/c/k/a/a$c;->c:Ljava/lang/Object;

    if-eqz v2, :cond_11

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->Q0(Landroid/os/Bundle;Ljava/lang/Object;)V

    :cond_11
    iget-object v2, p1, Lf/h/c/k/a/a$c;->d:Ljava/lang/String;

    if-eqz v2, :cond_12

    const-string v3, "trigger_event_name"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    iget-wide v2, p1, Lf/h/c/k/a/a$c;->e:J

    const-string v4, "trigger_timeout"

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v2, p1, Lf/h/c/k/a/a$c;->f:Ljava/lang/String;

    if-eqz v2, :cond_13

    const-string v3, "timed_out_event_name"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    iget-object v2, p1, Lf/h/c/k/a/a$c;->g:Landroid/os/Bundle;

    if-eqz v2, :cond_14

    const-string v3, "timed_out_event_params"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_14
    iget-object v2, p1, Lf/h/c/k/a/a$c;->h:Ljava/lang/String;

    if-eqz v2, :cond_15

    const-string v3, "triggered_event_name"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    iget-object v2, p1, Lf/h/c/k/a/a$c;->i:Landroid/os/Bundle;

    if-eqz v2, :cond_16

    const-string v3, "triggered_event_params"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_16
    iget-wide v2, p1, Lf/h/c/k/a/a$c;->j:J

    const-string v4, "time_to_live"

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v2, p1, Lf/h/c/k/a/a$c;->k:Ljava/lang/String;

    if-eqz v2, :cond_17

    const-string v3, "expired_event_name"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    iget-object v2, p1, Lf/h/c/k/a/a$c;->l:Landroid/os/Bundle;

    if-eqz v2, :cond_18

    const-string v3, "expired_event_params"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_18
    iget-wide v2, p1, Lf/h/c/k/a/a$c;->m:J

    const-string v4, "creation_timestamp"

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-boolean v2, p1, Lf/h/c/k/a/a$c;->n:Z

    const-string v3, "active"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-wide v2, p1, Lf/h/c/k/a/a$c;->o:J

    const-string p1, "triggered_timestamp"

    invoke-virtual {v1, p1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object p1, v0, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/i/j/i;

    invoke-direct {v0, p1, v1}, Lf/h/a/f/i/j/i;-><init>(Lf/h/a/f/i/j/g;Landroid/os/Bundle;)V

    iget-object p1, p1, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    if-nez p3, :cond_0

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    :cond_0
    move-object v3, p3

    invoke-static {p1}, Lf/h/c/k/a/c/c;->a(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_1

    return-void

    :cond_1
    invoke-static {p2, v3}, Lf/h/c/k/a/c/c;->b(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result p3

    if-nez p3, :cond_2

    return-void

    :cond_2
    invoke-static {p1, p2, v3}, Lf/h/c/k/a/c/c;->d(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result p3

    if-nez p3, :cond_3

    return-void

    :cond_3
    const-string p3, "clx"

    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_4

    const-string p3, "_ae"

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_4

    const-wide/16 v0, 0x1

    const-string p3, "_r"

    invoke-virtual {v3, p3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_4
    iget-object p3, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    iget-object v0, p3, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lf/h/a/f/i/j/g;->d(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZLjava/lang/Long;)V

    return-void
.end method

.method public clearConditionalUserProperty(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation

        .annotation build Landroidx/annotation/Size;
            max = 0x18L
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object p2, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    iget-object p2, p2, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p3, Lf/h/a/f/i/j/l;

    const/4 v0, 0x0

    invoke-direct {p3, p2, p1, v0, v0}, Lf/h/a/f/i/j/l;-><init>(Lf/h/a/f/i/j/g;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p2, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, p3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public d(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation

        .annotation build Landroidx/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    iget-object v0, v0, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/j/g;->h(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation

        .annotation build Landroidx/annotation/Size;
            max = 0x17L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lf/h/c/k/a/a$c;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    iget-object v1, v1, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    invoke-virtual {v1, p1, p2}, Lf/h/a/f/i/j/g;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/os/Bundle;

    sget-object v1, Lf/h/c/k/a/c/c;->a:Ljava/util/Set;

    const-class v1, Ljava/lang/Long;

    const-class v2, Ljava/lang/String;

    const-string v3, "null reference"

    invoke-static {p2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v3, Lf/h/c/k/a/a$c;

    invoke-direct {v3}, Lf/h/c/k/a/a$c;-><init>()V

    const/4 v4, 0x0

    const-string v5, "origin"

    invoke-static {p2, v5, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, v3, Lf/h/c/k/a/a$c;->a:Ljava/lang/String;

    const-string v5, "name"

    invoke-static {p2, v5, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, v3, Lf/h/c/k/a/a$c;->b:Ljava/lang/String;

    const-class v5, Ljava/lang/Object;

    const-string v6, "value"

    invoke-static {p2, v6, v5, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, v3, Lf/h/c/k/a/a$c;->c:Ljava/lang/Object;

    const-string v5, "trigger_event_name"

    invoke-static {p2, v5, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, v3, Lf/h/c/k/a/a$c;->d:Ljava/lang/String;

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string v8, "trigger_timeout"

    invoke-static {p2, v8, v1, v7}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iput-wide v7, v3, Lf/h/c/k/a/a$c;->e:J

    const-string v7, "timed_out_event_name"

    invoke-static {p2, v7, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, v3, Lf/h/c/k/a/a$c;->f:Ljava/lang/String;

    const-class v7, Landroid/os/Bundle;

    const-string v8, "timed_out_event_params"

    invoke-static {p2, v8, v7, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Bundle;

    iput-object v7, v3, Lf/h/c/k/a/a$c;->g:Landroid/os/Bundle;

    const-string v7, "triggered_event_name"

    invoke-static {p2, v7, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, v3, Lf/h/c/k/a/a$c;->h:Ljava/lang/String;

    const-class v7, Landroid/os/Bundle;

    const-string v8, "triggered_event_params"

    invoke-static {p2, v8, v7, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Bundle;

    iput-object v7, v3, Lf/h/c/k/a/a$c;->i:Landroid/os/Bundle;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string v8, "time_to_live"

    invoke-static {p2, v8, v1, v7}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iput-wide v7, v3, Lf/h/c/k/a/a$c;->j:J

    const-string v7, "expired_event_name"

    invoke-static {p2, v7, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lf/h/c/k/a/a$c;->k:Ljava/lang/String;

    const-class v2, Landroid/os/Bundle;

    const-string v7, "expired_event_params"

    invoke-static {p2, v7, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    iput-object v2, v3, Lf/h/c/k/a/a$c;->l:Landroid/os/Bundle;

    const-class v2, Ljava/lang/Boolean;

    sget-object v4, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v7, "active"

    invoke-static {p2, v7, v2, v4}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, v3, Lf/h/c/k/a/a$c;->n:Z

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v4, "creation_timestamp"

    invoke-static {p2, v4, v1, v2}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iput-wide v7, v3, Lf/h/c/k/a/a$c;->m:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v4, "triggered_timestamp"

    invoke-static {p2, v4, v1, v2}, Lf/h/a/f/f/n/g;->F0(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v3, Lf/h/c/k/a/a$c;->o:J

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_0
    return-object v0
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p1}, Lf/h/c/k/a/c/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {p1, p2}, Lf/h/c/k/a/c/c;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    iget-object v0, v0, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lf/h/a/f/i/j/b0;

    const/4 v6, 0x1

    move-object v1, v7

    move-object v2, v0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v1 .. v6}, Lf/h/a/f/i/j/b0;-><init>(Lf/h/a/f/i/j/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v7}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public g(Ljava/lang/String;Lf/h/c/k/a/a$b;)Lf/h/c/k/a/a$a;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    const-string v0, "null reference"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/k/a/c/c;->a(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/c/k/a/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/c/k/a/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    return-object v1

    :cond_2
    iget-object v0, p0, Lf/h/c/k/a/b;->a:Lf/h/a/f/j/a/a;

    const-string v2, "fiam"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Lf/h/c/k/a/c/b;

    invoke-direct {v2, v0, p2}, Lf/h/c/k/a/c/b;-><init>(Lf/h/a/f/j/a/a;Lf/h/c/k/a/a$b;)V

    goto :goto_2

    :cond_3
    const-string v2, "crash"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "clx"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_1

    :cond_4
    move-object v2, v1

    goto :goto_2

    :cond_5
    :goto_1
    new-instance v2, Lf/h/c/k/a/c/d;

    invoke-direct {v2, v0, p2}, Lf/h/c/k/a/c/d;-><init>(Lf/h/a/f/j/a/a;Lf/h/c/k/a/a$b;)V

    :goto_2
    if-eqz v2, :cond_6

    iget-object p2, p0, Lf/h/c/k/a/b;->b:Ljava/util/Map;

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p2, Lf/h/c/k/a/b$a;

    invoke-direct {p2, p0, p1}, Lf/h/c/k/a/b$a;-><init>(Lf/h/c/k/a/b;Ljava/lang/String;)V

    return-object p2

    :cond_6
    return-object v1
.end method
