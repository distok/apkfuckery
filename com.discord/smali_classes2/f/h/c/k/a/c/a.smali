.class public final synthetic Lf/h/c/k/a/c/a;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-api@@18.0.0"

# interfaces
.implements Lf/h/c/m/f;


# static fields
.field public static final a:Lf/h/c/m/f;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/c/k/a/c/a;

    invoke-direct {v0}, Lf/h/c/k/a/c/a;-><init>()V

    sput-object v0, Lf/h/c/k/a/c/a;->a:Lf/h/c/m/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lf/h/c/m/e;)Ljava/lang/Object;
    .locals 7

    const-class v0, Lf/h/c/c;

    invoke-interface {p1, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/c;

    const-class v1, Landroid/content/Context;

    invoke-interface {p1, v1}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, Lf/h/c/r/d;

    invoke-interface {p1, v2}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/c/r/d;

    const-string v2, "null reference"

    invoke-static {v0, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v2, "null reference"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v2, "null reference"

    invoke-static {p1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "null reference"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v2, Lf/h/c/k/a/b;->c:Lf/h/c/k/a/a;

    if-nez v2, :cond_2

    const-class v2, Lf/h/c/k/a/b;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lf/h/c/k/a/b;->c:Lf/h/c/k/a/a;

    if-nez v3, :cond_1

    new-instance v3, Landroid/os/Bundle;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/os/Bundle;-><init>(I)V

    invoke-virtual {v0}, Lf/h/c/c;->h()Z

    move-result v4

    if-eqz v4, :cond_0

    const-class v4, Lf/h/c/a;

    sget-object v5, Lf/h/c/k/a/e;->d:Ljava/util/concurrent/Executor;

    sget-object v6, Lf/h/c/k/a/d;->a:Lf/h/c/r/b;

    invoke-interface {p1, v4, v5, v6}, Lf/h/c/r/d;->b(Ljava/lang/Class;Ljava/util/concurrent/Executor;Lf/h/c/r/b;)V

    const-string p1, "dataCollectionDefaultEnabled"

    invoke-virtual {v0}, Lf/h/c/c;->g()Z

    move-result v0

    invoke-virtual {v3, p1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    new-instance p1, Lf/h/c/k/a/b;

    const/4 v0, 0x0

    invoke-static {v1, v0, v0, v0, v3}, Lf/h/a/f/i/j/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lf/h/a/f/i/j/g;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/i/j/g;->d:Lf/h/a/f/j/a/a;

    invoke-direct {p1, v0}, Lf/h/c/k/a/b;-><init>(Lf/h/a/f/j/a/a;)V

    sput-object p1, Lf/h/c/k/a/b;->c:Lf/h/c/k/a/a;

    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    sget-object p1, Lf/h/c/k/a/b;->c:Lf/h/c/k/a/a;

    return-object p1
.end method
