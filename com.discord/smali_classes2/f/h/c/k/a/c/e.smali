.class public final Lf/h/c/k/a/c/e;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-api@@18.0.0"

# interfaces
.implements Lf/h/a/f/j/a/a$a;


# instance fields
.field public final synthetic a:Lf/h/c/k/a/c/b;


# direct methods
.method public constructor <init>(Lf/h/c/k/a/c/b;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/k/a/c/e;->a:Lf/h/c/k/a/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;J)V
    .locals 0

    iget-object p1, p0, Lf/h/c/k/a/c/e;->a:Lf/h/c/k/a/c/b;

    iget-object p1, p1, Lf/h/c/k/a/c/b;->a:Ljava/util/Set;

    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    sget-object p3, Lf/h/c/k/a/c/c;->a:Ljava/util/Set;

    sget-object p3, Lf/h/a/f/j/b/v5;->c:[Ljava/lang/String;

    sget-object p4, Lf/h/a/f/j/b/v5;->a:[Ljava/lang/String;

    invoke-static {p2, p3, p4}, Lf/h/a/f/f/n/g;->L0(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_1

    move-object p2, p3

    :cond_1
    const-string p3, "events"

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lf/h/c/k/a/c/e;->a:Lf/h/c/k/a/c/b;

    iget-object p2, p2, Lf/h/c/k/a/c/b;->b:Lf/h/c/k/a/a$b;

    const/4 p3, 0x2

    check-cast p2, Lf/h/c/n/a;

    invoke-virtual {p2, p3, p1}, Lf/h/c/n/a;->a(ILandroid/os/Bundle;)V

    return-void
.end method
