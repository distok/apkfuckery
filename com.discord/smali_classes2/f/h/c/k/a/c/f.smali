.class public final Lf/h/c/k/a/c/f;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-api@@18.0.0"

# interfaces
.implements Lf/h/a/f/j/a/a$a;


# instance fields
.field public final synthetic a:Lf/h/c/k/a/c/d;


# direct methods
.method public constructor <init>(Lf/h/c/k/a/c/d;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/k/a/c/f;->a:Lf/h/c/k/a/c/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;J)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "crash"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/k/a/c/c;->a:Ljava/util/Set;

    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "name"

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "timestampInMillis"

    invoke-virtual {p1, p2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string p2, "params"

    invoke-virtual {p1, p2, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p2, p0, Lf/h/c/k/a/c/f;->a:Lf/h/c/k/a/c/d;

    iget-object p2, p2, Lf/h/c/k/a/c/d;->a:Lf/h/c/k/a/a$b;

    const/4 p3, 0x3

    check-cast p2, Lf/h/c/n/a;

    invoke-virtual {p2, p3, p1}, Lf/h/c/n/a;->a(ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method
