.class public Lf/h/c/v/f;
.super Ljava/lang/Object;
.source "FirebaseInstallations.java"

# interfaces
.implements Lf/h/c/v/g;


# static fields
.field public static final l:Ljava/lang/Object;

.field public static final m:Ljava/util/concurrent/ThreadFactory;


# instance fields
.field public final a:Lf/h/c/c;

.field public final b:Lf/h/c/v/p/c;

.field public final c:Lf/h/c/v/o/c;

.field public final d:Lf/h/c/v/n;

.field public final e:Lf/h/c/v/o/b;

.field public final f:Lf/h/c/v/l;

.field public final g:Ljava/lang/Object;

.field public final h:Ljava/util/concurrent/ExecutorService;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field public j:Ljava/lang/String;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final k:Ljava/util/List;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "lock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/c/v/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf/h/c/v/f;->l:Ljava/lang/Object;

    new-instance v0, Lf/h/c/v/f$a;

    invoke-direct {v0}, Lf/h/c/v/f$a;-><init>()V

    sput-object v0, Lf/h/c/v/f;->m:Ljava/util/concurrent/ThreadFactory;

    return-void
.end method

.method public constructor <init>(Lf/h/c/c;Lf/h/c/u/a;Lf/h/c/u/a;)V
    .locals 11
    .param p2    # Lf/h/c/u/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lf/h/c/u/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/c;",
            "Lf/h/c/u/a<",
            "Lf/h/c/z/h;",
            ">;",
            "Lf/h/c/u/a<",
            "Lf/h/c/s/d;",
            ">;)V"
        }
    .end annotation

    new-instance v8, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sget-object v10, Lf/h/c/v/f;->m:Ljava/util/concurrent/ThreadFactory;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-wide/16 v3, 0x1e

    move-object v0, v8

    move-object v5, v9

    move-object v7, v10

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    new-instance v0, Lf/h/c/v/p/c;

    invoke-virtual {p1}, Lf/h/c/c;->a()V

    iget-object v1, p1, Lf/h/c/c;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p3}, Lf/h/c/v/p/c;-><init>(Landroid/content/Context;Lf/h/c/u/a;Lf/h/c/u/a;)V

    new-instance p2, Lf/h/c/v/o/c;

    invoke-direct {p2, p1}, Lf/h/c/v/o/c;-><init>(Lf/h/c/c;)V

    invoke-static {}, Lf/h/c/v/n;->c()Lf/h/c/v/n;

    move-result-object p3

    new-instance v1, Lf/h/c/v/o/b;

    invoke-direct {v1, p1}, Lf/h/c/v/o/b;-><init>(Lf/h/c/c;)V

    new-instance v2, Lf/h/c/v/l;

    invoke-direct {v2}, Lf/h/c/v/l;-><init>()V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lf/h/c/v/f;->g:Ljava/lang/Object;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lf/h/c/v/f;->k:Ljava/util/List;

    iput-object p1, p0, Lf/h/c/v/f;->a:Lf/h/c/c;

    iput-object v0, p0, Lf/h/c/v/f;->b:Lf/h/c/v/p/c;

    iput-object p2, p0, Lf/h/c/v/f;->c:Lf/h/c/v/o/c;

    iput-object p3, p0, Lf/h/c/v/f;->d:Lf/h/c/v/n;

    iput-object v1, p0, Lf/h/c/v/f;->e:Lf/h/c/v/o/b;

    iput-object v2, p0, Lf/h/c/v/f;->f:Lf/h/c/v/l;

    iput-object v8, p0, Lf/h/c/v/f;->h:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-wide/16 v3, 0x1e

    move-object v0, p1

    move-object v5, v9

    move-object v7, v10

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object p1, p0, Lf/h/c/v/f;->i:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static f()Lf/h/c/v/f;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Lf/h/c/c;->b()Lf/h/c/c;

    move-result-object v0

    const/4 v1, 0x1

    const-string v2, "Null is not a valid value of FirebaseApp."

    invoke-static {v1, v2}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    const-class v1, Lf/h/c/v/g;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->d:Lf/h/c/m/k;

    invoke-virtual {v0, v1}, Lf/h/c/m/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/v/f;

    return-object v0
.end method


# virtual methods
.method public a(Z)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/gms/tasks/Task<",
            "Lf/h/c/v/k;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/c/v/f;->h()V

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v1, Lf/h/c/v/i;

    iget-object v2, p0, Lf/h/c/v/f;->d:Lf/h/c/v/n;

    invoke-direct {v1, v2, v0}, Lf/h/c/v/i;-><init>(Lf/h/c/v/n;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    iget-object v2, p0, Lf/h/c/v/f;->g:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lf/h/c/v/f;->k:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    iget-object v1, p0, Lf/h/c/v/f;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lf/h/c/v/d;

    invoke-direct {v2, p0, p1}, Lf/h/c/v/d;-><init>(Lf/h/c/v/f;Z)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-object v0

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final b(Z)V
    .locals 5

    sget-object v0, Lf/h/c/v/f;->l:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v1}, Lf/h/c/c;->a()V

    iget-object v1, v1, Lf/h/c/c;->a:Landroid/content/Context;

    const-string v2, "generatefid.lock"

    invoke-static {v1, v2}, Lf/h/c/v/b;->a(Landroid/content/Context;Ljava/lang/String;)Lf/h/c/v/b;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lf/h/c/v/f;->c:Lf/h/c/v/o/c;

    invoke-virtual {v2}, Lf/h/c/v/o/c;->b()Lf/h/c/v/o/d;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/c/v/o/d;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v2}, Lf/h/c/v/f;->i(Lf/h/c/v/o/d;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lf/h/c/v/f;->c:Lf/h/c/v/o/c;

    invoke-virtual {v2}, Lf/h/c/v/o/d;->k()Lf/h/c/v/o/d$a;

    move-result-object v2

    check-cast v2, Lf/h/c/v/o/a$b;

    iput-object v3, v2, Lf/h/c/v/o/a$b;->a:Ljava/lang/String;

    sget-object v3, Lf/h/c/v/o/c$a;->f:Lf/h/c/v/o/c$a;

    invoke-virtual {v2, v3}, Lf/h/c/v/o/a$b;->b(Lf/h/c/v/o/c$a;)Lf/h/c/v/o/d$a;

    invoke-virtual {v2}, Lf/h/c/v/o/a$b;->a()Lf/h/c/v/o/d;

    move-result-object v2

    invoke-virtual {v4, v2}, Lf/h/c/v/o/c;->a(Lf/h/c/v/o/d;)Lf/h/c/v/o/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Lf/h/c/v/b;->b()V

    :cond_1
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p1, :cond_2

    invoke-virtual {v2}, Lf/h/c/v/o/d;->k()Lf/h/c/v/o/d$a;

    move-result-object v0

    check-cast v0, Lf/h/c/v/o/a$b;

    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/c/v/o/a$b;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/c/v/o/a$b;->a()Lf/h/c/v/o/d;

    move-result-object v2

    :cond_2
    invoke-virtual {p0, v2}, Lf/h/c/v/f;->l(Lf/h/c/v/o/d;)V

    iget-object v0, p0, Lf/h/c/v/f;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lf/h/c/v/e;

    invoke-direct {v1, p0, p1}, Lf/h/c/v/e;-><init>(Lf/h/c/v/f;Z)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v1}, Lf/h/c/v/b;->b()V

    :cond_3
    throw p1

    :catchall_1
    move-exception p1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public final c(Lf/h/c/v/o/d;)Lf/h/c/v/o/d;
    .locals 16
    .param p1    # Lf/h/c/v/o/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/firebase/installations/FirebaseInstallationsException;
        }
    .end annotation

    move-object/from16 v1, p0

    sget-object v0, Lcom/google/firebase/installations/FirebaseInstallationsException$a;->e:Lcom/google/firebase/installations/FirebaseInstallationsException$a;

    iget-object v2, v1, Lf/h/c/v/f;->b:Lf/h/c/v/p/c;

    invoke-virtual/range {p0 .. p0}, Lf/h/c/v/f;->d()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, p1

    check-cast v4, Lf/h/c/v/o/a;

    iget-object v5, v4, Lf/h/c/v/o/a;->b:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lf/h/c/v/f;->g()Ljava/lang/String;

    move-result-object v6

    iget-object v4, v4, Lf/h/c/v/o/a;->e:Ljava/lang/String;

    iget-object v7, v2, Lf/h/c/v/p/c;->d:Lf/h/c/v/p/e;

    invoke-virtual {v7}, Lf/h/c/v/p/e;->a()Z

    move-result v7

    const-string v8, "Firebase Installations Service is unavailable. Please try again later."

    if-eqz v7, :cond_a

    const/4 v7, 0x2

    new-array v9, v7, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v11, 0x1

    aput-object v5, v9, v11

    const-string v5, "projects/%s/installations/%s/authTokens:generate"

    invoke-static {v5, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lf/h/c/v/p/c;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v5

    const/4 v9, 0x0

    :goto_0
    if-gt v9, v11, :cond_9

    invoke-virtual {v2, v5, v3}, Lf/h/c/v/p/c;->c(Ljava/net/URL;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v12

    :try_start_0
    const-string v13, "POST"

    invoke-virtual {v12, v13}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v13, "Authorization"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FIS_v2 "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v11}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {v2, v12}, Lf/h/c/v/p/c;->h(Ljava/net/HttpURLConnection;)V

    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v13

    iget-object v14, v2, Lf/h/c/v/p/c;->d:Lf/h/c/v/p/e;

    invoke-virtual {v14, v13}, Lf/h/c/v/p/e;->b(I)V

    const/16 v14, 0xc8

    if-lt v13, v14, :cond_0

    const/16 v14, 0x12c

    if-ge v13, v14, :cond_0

    const/4 v14, 0x1

    goto :goto_1

    :cond_0
    const/4 v14, 0x0

    :goto_1
    const/4 v15, 0x0

    if-eqz v14, :cond_1

    invoke-virtual {v2, v12}, Lf/h/c/v/p/c;->f(Ljava/net/HttpURLConnection;)Lf/h/c/v/p/f;

    move-result-object v2

    goto :goto_3

    :cond_1
    invoke-static {v12, v15, v3, v6}, Lf/h/c/v/p/c;->b(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v14, 0x191

    if-eq v13, v14, :cond_5

    const/16 v14, 0x194

    if-ne v13, v14, :cond_2

    goto :goto_2

    :cond_2
    const/16 v14, 0x1ad

    if-eq v13, v14, :cond_4

    const/16 v14, 0x1f4

    if-lt v13, v14, :cond_3

    const/16 v14, 0x258

    if-ge v13, v14, :cond_3

    goto/16 :goto_4

    :cond_3
    const-string v13, "Firebase-Installations"

    const-string v14, "Firebase Installations can not communicate with Firebase server APIs due to invalid configuration. Please update your Firebase initialization process and set valid Firebase options (API key, Project ID, Application ID) when initializing Firebase."

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lf/h/c/v/p/f;->a()Lf/h/c/v/p/f$a;

    move-result-object v13

    sget-object v14, Lf/h/c/v/p/f$b;->e:Lf/h/c/v/p/f$b;

    check-cast v13, Lf/h/c/v/p/b$b;

    iput-object v14, v13, Lf/h/c/v/p/b$b;->c:Lf/h/c/v/p/f$b;

    invoke-virtual {v13}, Lf/h/c/v/p/b$b;->a()Lf/h/c/v/p/f;

    move-result-object v2

    goto :goto_3

    :cond_4
    new-instance v13, Lcom/google/firebase/installations/FirebaseInstallationsException;

    const-string v14, "Firebase servers have received too many requests from this client in a short period of time. Please try again later."

    sget-object v15, Lcom/google/firebase/installations/FirebaseInstallationsException$a;->f:Lcom/google/firebase/installations/FirebaseInstallationsException$a;

    invoke-direct {v13, v14, v15}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v13

    :cond_5
    :goto_2
    invoke-static {}, Lf/h/c/v/p/f;->a()Lf/h/c/v/p/f$a;

    move-result-object v13

    sget-object v14, Lf/h/c/v/p/f$b;->f:Lf/h/c/v/p/f$b;

    check-cast v13, Lf/h/c/v/p/b$b;

    iput-object v14, v13, Lf/h/c/v/p/b$b;->c:Lf/h/c/v/p/f$b;

    invoke-virtual {v13}, Lf/h/c/v/p/b$b;->a()Lf/h/c/v/p/f;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_3
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->disconnect()V

    check-cast v2, Lf/h/c/v/p/b;

    iget-object v3, v2, Lf/h/c/v/p/b;->c:Lf/h/c/v/p/f$b;

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    if-eqz v3, :cond_8

    if-eq v3, v11, :cond_7

    if-ne v3, v7, :cond_6

    monitor-enter p0

    :try_start_1
    iput-object v15, v1, Lf/h/c/v/f;->j:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    invoke-virtual/range {p1 .. p1}, Lf/h/c/v/o/d;->k()Lf/h/c/v/o/d$a;

    move-result-object v0

    sget-object v2, Lf/h/c/v/o/c$a;->e:Lf/h/c/v/o/c$a;

    invoke-virtual {v0, v2}, Lf/h/c/v/o/d$a;->b(Lf/h/c/v/o/c$a;)Lf/h/c/v/o/d$a;

    invoke-virtual {v0}, Lf/h/c/v/o/d$a;->a()Lf/h/c/v/o/d;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    move-object v2, v0

    monitor-exit p0

    throw v2

    :cond_6
    new-instance v2, Lcom/google/firebase/installations/FirebaseInstallationsException;

    const-string v3, "Firebase Installations Service is unavailable. Please try again later."

    invoke-direct {v2, v3, v0}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v2

    :cond_7
    const-string v0, "BAD CONFIG"

    invoke-virtual/range {p1 .. p1}, Lf/h/c/v/o/d;->k()Lf/h/c/v/o/d$a;

    move-result-object v2

    check-cast v2, Lf/h/c/v/o/a$b;

    iput-object v0, v2, Lf/h/c/v/o/a$b;->g:Ljava/lang/String;

    sget-object v0, Lf/h/c/v/o/c$a;->h:Lf/h/c/v/o/c$a;

    invoke-virtual {v2, v0}, Lf/h/c/v/o/a$b;->b(Lf/h/c/v/o/c$a;)Lf/h/c/v/o/d$a;

    invoke-virtual {v2}, Lf/h/c/v/o/a$b;->a()Lf/h/c/v/o/d;

    move-result-object v0

    return-object v0

    :cond_8
    iget-object v0, v2, Lf/h/c/v/p/b;->a:Ljava/lang/String;

    iget-wide v2, v2, Lf/h/c/v/p/b;->b:J

    iget-object v4, v1, Lf/h/c/v/f;->d:Lf/h/c/v/n;

    invoke-virtual {v4}, Lf/h/c/v/n;->b()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lf/h/c/v/o/d;->k()Lf/h/c/v/o/d$a;

    move-result-object v6

    check-cast v6, Lf/h/c/v/o/a$b;

    iput-object v0, v6, Lf/h/c/v/o/a$b;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v6, Lf/h/c/v/o/a$b;->e:Ljava/lang/Long;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v6, Lf/h/c/v/o/a$b;->f:Ljava/lang/Long;

    invoke-virtual {v6}, Lf/h/c/v/o/a$b;->a()Lf/h/c/v/o/d;

    move-result-object v0

    return-object v0

    :catchall_1
    move-exception v0

    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v0

    :catch_0
    :goto_4
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->disconnect()V

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    :cond_9
    new-instance v2, Lcom/google/firebase/installations/FirebaseInstallationsException;

    invoke-direct {v2, v8, v0}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v2

    :cond_a
    new-instance v2, Lcom/google/firebase/installations/FirebaseInstallationsException;

    invoke-direct {v2, v8, v0}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v2
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v0, v0, Lf/h/c/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    iget-object v0, p0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v0, v0, Lf/h/c/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v0, v0, Lf/h/c/i;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/c/v/f;->h()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/c/v/f;->j:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit p0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v1, Lf/h/c/v/j;

    invoke-direct {v1, v0}, Lf/h/c/v/j;-><init>(Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    iget-object v2, p0, Lf/h/c/v/f;->g:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v3, p0, Lf/h/c/v/f;->k:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    iget-object v1, p0, Lf/h/c/v/f;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lf/h/c/v/c;

    invoke-direct {v2, p0}, Lf/h/c/v/c;-><init>(Lf/h/c/v/f;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()V
    .locals 4

    invoke-virtual {p0}, Lf/h/c/v/f;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options."

    invoke-static {v0, v1}, Lf/g/j/k/a;->n(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lf/h/c/v/f;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Please set your Project ID. A valid Firebase Project ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options."

    invoke-static {v0, v2}, Lf/g/j/k/a;->n(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lf/h/c/v/f;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options."

    invoke-static {v0, v2}, Lf/g/j/k/a;->n(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lf/h/c/v/f;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lf/h/c/v/n;->c:Ljava/util/regex/Pattern;

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0, v1}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/c/v/f;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lf/h/c/v/n;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    invoke-static {v0, v2}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    return-void
.end method

.method public final i(Lf/h/c/v/o/d;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->b:Ljava/lang/String;

    const-string v1, "CHIME_ANDROID_SDK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v0}, Lf/h/c/c;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    check-cast p1, Lf/h/c/v/o/a;

    iget-object p1, p1, Lf/h/c/v/o/a;->c:Lf/h/c/v/o/c$a;

    sget-object v0, Lf/h/c/v/o/c$a;->d:Lf/h/c/v/o/c$a;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_3

    :cond_2
    iget-object p1, p0, Lf/h/c/v/f;->f:Lf/h/c/v/l;

    invoke-virtual {p1}, Lf/h/c/v/l;->a()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    iget-object p1, p0, Lf/h/c/v/f;->e:Lf/h/c/v/o/b;

    iget-object v0, p1, Lf/h/c/v/o/b;->a:Landroid/content/SharedPreferences;

    monitor-enter v0

    :try_start_0
    iget-object v1, p1, Lf/h/c/v/o/b;->a:Landroid/content/SharedPreferences;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p1, Lf/h/c/v/o/b;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "|S|id"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_4

    :try_start_2
    monitor-exit v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lf/h/c/v/o/b;->a()Ljava/lang/String;

    move-result-object v2

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lf/h/c/v/f;->f:Lf/h/c/v/l;

    invoke-virtual {p1}, Lf/h/c/v/l;->a()Ljava/lang/String;

    move-result-object v2

    :cond_5
    return-object v2

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1

    :catchall_1
    move-exception p1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p1
.end method

.method public final j(Lf/h/c/v/o/d;)Lf/h/c/v/o/d;
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/firebase/installations/FirebaseInstallationsException;
        }
    .end annotation

    move-object/from16 v1, p0

    sget-object v0, Lcom/google/firebase/installations/FirebaseInstallationsException$a;->e:Lcom/google/firebase/installations/FirebaseInstallationsException$a;

    move-object/from16 v2, p1

    check-cast v2, Lf/h/c/v/o/a;

    iget-object v3, v2, Lf/h/c/v/o/a;->b:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v6, 0xb

    if-ne v3, v6, :cond_3

    iget-object v3, v1, Lf/h/c/v/f;->e:Lf/h/c/v/o/b;

    iget-object v6, v3, Lf/h/c/v/o/b;->a:Landroid/content/SharedPreferences;

    monitor-enter v6

    :try_start_0
    sget-object v7, Lf/h/c/v/o/b;->c:[Ljava/lang/String;

    array-length v8, v7

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v8, :cond_2

    aget-object v10, v7, v9

    iget-object v11, v3, Lf/h/c/v/o/b;->b:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "|T|"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v11, "|"

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v3, Lf/h/c/v/o/b;->a:Landroid/content/SharedPreferences;

    invoke-interface {v11, v10, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_1

    const-string/jumbo v3, "{"

    invoke-virtual {v10, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    :try_start_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v7, "token"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_0
    move-object v4, v10

    :catch_0
    :goto_1
    :try_start_2
    monitor-exit v6

    goto :goto_2

    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_2
    monitor-exit v6

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    :goto_2
    iget-object v3, v1, Lf/h/c/v/f;->b:Lf/h/c/v/p/c;

    invoke-virtual/range {p0 .. p0}, Lf/h/c/v/f;->d()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v2, Lf/h/c/v/o/a;->b:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lf/h/c/v/f;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lf/h/c/v/f;->e()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lf/h/c/v/p/c;->d:Lf/h/c/v/p/e;

    invoke-virtual {v9}, Lf/h/c/v/p/e;->a()Z

    move-result v9

    const-string v10, "Firebase Installations Service is unavailable. Please try again later."

    if-eqz v9, :cond_c

    const/4 v9, 0x1

    new-array v11, v9, [Ljava/lang/Object;

    aput-object v7, v11, v5

    const-string v12, "projects/%s/installations"

    invoke-static {v12, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Lf/h/c/v/p/c;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v11

    const/4 v12, 0x0

    :goto_3
    if-gt v12, v9, :cond_b

    invoke-virtual {v3, v11, v6}, Lf/h/c/v/p/c;->c(Ljava/net/URL;Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v13

    :try_start_3
    const-string v14, "POST"

    invoke-virtual {v13, v14}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    invoke-virtual {v13, v9}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    if-eqz v4, :cond_4

    const-string/jumbo v14, "x-goog-fis-android-iid-migration-auth"

    invoke-virtual {v13, v14, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v3, v13, v2, v8}, Lf/h/c/v/p/c;->g(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v14

    iget-object v15, v3, Lf/h/c/v/p/c;->d:Lf/h/c/v/p/e;

    invoke-virtual {v15, v14}, Lf/h/c/v/p/e;->b(I)V

    const/16 v15, 0xc8

    if-lt v14, v15, :cond_5

    const/16 v15, 0x12c

    if-ge v14, v15, :cond_5

    const/4 v15, 0x1

    goto :goto_4

    :cond_5
    const/4 v15, 0x0

    :goto_4
    if-eqz v15, :cond_6

    invoke-virtual {v3, v13}, Lf/h/c/v/p/c;->e(Ljava/net/HttpURLConnection;)Lf/h/c/v/p/d;

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/AssertionError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_5

    :cond_6
    :try_start_4
    invoke-static {v13, v8, v6, v7}, Lf/h/c/v/p/c;->b(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v15, 0x1ad

    if-eq v14, v15, :cond_a

    const/16 v15, 0x1f4

    if-lt v14, v15, :cond_7

    const/16 v15, 0x258

    if-ge v14, v15, :cond_7

    goto/16 :goto_6

    :cond_7
    const-string v14, "Firebase-Installations"

    const-string v15, "Firebase Installations can not communicate with Firebase server APIs due to invalid configuration. Please update your Firebase initialization process and set valid Firebase options (API key, Project ID, Application ID) when initializing Firebase."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v20, 0x0

    const/16 v19, 0x0

    const/16 v18, 0x0

    const/16 v17, 0x0

    sget-object v21, Lf/h/c/v/p/d$a;->e:Lf/h/c/v/p/d$a;

    new-instance v14, Lf/h/c/v/p/a;

    const/16 v22, 0x0

    move-object/from16 v16, v14

    invoke-direct/range {v16 .. v22}, Lf/h/c/v/p/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/h/c/v/p/f;Lf/h/c/v/p/d$a;Lf/h/c/v/p/a$a;)V
    :try_end_4
    .catch Ljava/lang/AssertionError; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v2, v14

    :goto_5
    check-cast v2, Lf/h/c/v/p/a;

    iget-object v3, v2, Lf/h/c/v/p/a;->e:Lf/h/c/v/p/d$a;

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    if-eqz v3, :cond_9

    if-ne v3, v9, :cond_8

    const-string v0, "BAD CONFIG"

    invoke-virtual/range {p1 .. p1}, Lf/h/c/v/o/d;->k()Lf/h/c/v/o/d$a;

    move-result-object v2

    check-cast v2, Lf/h/c/v/o/a$b;

    iput-object v0, v2, Lf/h/c/v/o/a$b;->g:Ljava/lang/String;

    sget-object v0, Lf/h/c/v/o/c$a;->h:Lf/h/c/v/o/c$a;

    invoke-virtual {v2, v0}, Lf/h/c/v/o/a$b;->b(Lf/h/c/v/o/c$a;)Lf/h/c/v/o/d$a;

    invoke-virtual {v2}, Lf/h/c/v/o/a$b;->a()Lf/h/c/v/o/d;

    move-result-object v0

    return-object v0

    :cond_8
    new-instance v2, Lcom/google/firebase/installations/FirebaseInstallationsException;

    const-string v3, "Firebase Installations Service is unavailable. Please try again later."

    invoke-direct {v2, v3, v0}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v2

    :cond_9
    iget-object v0, v2, Lf/h/c/v/p/a;->b:Ljava/lang/String;

    iget-object v3, v2, Lf/h/c/v/p/a;->c:Ljava/lang/String;

    iget-object v4, v1, Lf/h/c/v/f;->d:Lf/h/c/v/n;

    invoke-virtual {v4}, Lf/h/c/v/n;->b()J

    move-result-wide v4

    iget-object v6, v2, Lf/h/c/v/p/a;->d:Lf/h/c/v/p/f;

    invoke-virtual {v6}, Lf/h/c/v/p/f;->c()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v2, Lf/h/c/v/p/a;->d:Lf/h/c/v/p/f;

    invoke-virtual {v2}, Lf/h/c/v/p/f;->d()J

    move-result-wide v7

    invoke-virtual/range {p1 .. p1}, Lf/h/c/v/o/d;->k()Lf/h/c/v/o/d$a;

    move-result-object v2

    check-cast v2, Lf/h/c/v/o/a$b;

    iput-object v0, v2, Lf/h/c/v/o/a$b;->a:Ljava/lang/String;

    sget-object v0, Lf/h/c/v/o/c$a;->g:Lf/h/c/v/o/c$a;

    invoke-virtual {v2, v0}, Lf/h/c/v/o/a$b;->b(Lf/h/c/v/o/c$a;)Lf/h/c/v/o/d$a;

    iput-object v6, v2, Lf/h/c/v/o/a$b;->c:Ljava/lang/String;

    iput-object v3, v2, Lf/h/c/v/o/a$b;->d:Ljava/lang/String;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lf/h/c/v/o/a$b;->e:Ljava/lang/Long;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lf/h/c/v/o/a$b;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Lf/h/c/v/o/a$b;->a()Lf/h/c/v/o/d;

    move-result-object v0

    return-object v0

    :cond_a
    :try_start_5
    new-instance v14, Lcom/google/firebase/installations/FirebaseInstallationsException;

    const-string v15, "Firebase servers have received too many requests from this client in a short period of time. Please try again later."

    sget-object v5, Lcom/google/firebase/installations/FirebaseInstallationsException$a;->f:Lcom/google/firebase/installations/FirebaseInstallationsException$a;

    invoke-direct {v14, v15, v5}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v14
    :try_end_5
    .catch Ljava/lang/AssertionError; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v0

    :catch_1
    :goto_6
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    add-int/lit8 v12, v12, 0x1

    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_b
    new-instance v2, Lcom/google/firebase/installations/FirebaseInstallationsException;

    invoke-direct {v2, v10, v0}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v2

    :cond_c
    new-instance v2, Lcom/google/firebase/installations/FirebaseInstallationsException;

    invoke-direct {v2, v10, v0}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Ljava/lang/String;Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    throw v2
.end method

.method public final k(Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lf/h/c/v/f;->g:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/c/v/f;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/c/v/m;

    invoke-interface {v2, p1}, Lf/h/c/v/m;->a(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final l(Lf/h/c/v/o/d;)V
    .locals 3

    iget-object v0, p0, Lf/h/c/v/f;->g:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/c/v/f;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/c/v/m;

    invoke-interface {v2, p1}, Lf/h/c/v/m;->b(Lf/h/c/v/o/d;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
