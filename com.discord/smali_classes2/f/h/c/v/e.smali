.class public final synthetic Lf/h/c/v/e;
.super Ljava/lang/Object;
.source "FirebaseInstallations.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/c/v/f;

.field public final e:Z


# direct methods
.method public constructor <init>(Lf/h/c/v/f;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/v/e;->d:Lf/h/c/v/f;

    iput-boolean p2, p0, Lf/h/c/v/e;->e:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/h/c/v/e;->d:Lf/h/c/v/f;

    iget-boolean v1, p0, Lf/h/c/v/e;->e:Z

    sget-object v2, Lf/h/c/v/f;->l:Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lf/h/c/v/f;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v3}, Lf/h/c/c;->a()V

    iget-object v3, v3, Lf/h/c/c;->a:Landroid/content/Context;

    const-string v4, "generatefid.lock"

    invoke-static {v3, v4}, Lf/h/c/v/b;->a(Landroid/content/Context;Ljava/lang/String;)Lf/h/c/v/b;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    :try_start_1
    iget-object v4, v0, Lf/h/c/v/f;->c:Lf/h/c/v/o/c;

    invoke-virtual {v4}, Lf/h/c/v/o/c;->b()Lf/h/c/v/o/d;

    move-result-object v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Lf/h/c/v/b;->b()V

    :cond_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :try_start_3
    invoke-virtual {v4}, Lf/h/c/v/o/d;->h()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v4}, Lf/h/c/v/o/d;->f()Lf/h/c/v/o/c$a;

    move-result-object v3

    sget-object v5, Lf/h/c/v/o/c$a;->f:Lf/h/c/v/o/c$a;

    if-ne v3, v5, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    iget-object v1, v0, Lf/h/c/v/f;->d:Lf/h/c/v/n;

    invoke-virtual {v1, v4}, Lf/h/c/v/n;->d(Lf/h/c/v/o/d;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_3
    invoke-virtual {v0, v4}, Lf/h/c/v/f;->c(Lf/h/c/v/o/d;)Lf/h/c/v/o/d;

    move-result-object v1

    goto :goto_2

    :cond_4
    :goto_1
    invoke-virtual {v0, v4}, Lf/h/c/v/f;->j(Lf/h/c/v/o/d;)Lf/h/c/v/o/d;

    move-result-object v1
    :try_end_3
    .catch Lcom/google/firebase/installations/FirebaseInstallationsException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_2
    monitor-enter v2

    :try_start_4
    iget-object v3, v0, Lf/h/c/v/f;->a:Lf/h/c/c;

    invoke-virtual {v3}, Lf/h/c/c;->a()V

    iget-object v3, v3, Lf/h/c/c;->a:Landroid/content/Context;

    const-string v4, "generatefid.lock"

    invoke-static {v3, v4}, Lf/h/c/v/b;->a(Landroid/content/Context;Ljava/lang/String;)Lf/h/c/v/b;

    move-result-object v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    iget-object v4, v0, Lf/h/c/v/f;->c:Lf/h/c/v/o/c;

    invoke-virtual {v4, v1}, Lf/h/c/v/o/c;->a(Lf/h/c/v/o/d;)Lf/h/c/v/o/d;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v3, :cond_5

    :try_start_6
    invoke-virtual {v3}, Lf/h/c/v/b;->b()V

    :cond_5
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    invoke-virtual {v1}, Lf/h/c/v/o/d;->j()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object v2, v1

    check-cast v2, Lf/h/c/v/o/a;

    iget-object v2, v2, Lf/h/c/v/o/a;->b:Ljava/lang/String;

    monitor-enter v0

    :try_start_7
    iput-object v2, v0, Lf/h/c/v/f;->j:Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    monitor-exit v0

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_6
    :goto_3
    invoke-virtual {v1}, Lf/h/c/v/o/d;->h()Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v1, Lcom/google/firebase/installations/FirebaseInstallationsException;

    sget-object v2, Lcom/google/firebase/installations/FirebaseInstallationsException$a;->d:Lcom/google/firebase/installations/FirebaseInstallationsException$a;

    invoke-direct {v1, v2}, Lcom/google/firebase/installations/FirebaseInstallationsException;-><init>(Lcom/google/firebase/installations/FirebaseInstallationsException$a;)V

    invoke-virtual {v0, v1}, Lf/h/c/v/f;->k(Ljava/lang/Exception;)V

    goto :goto_4

    :cond_7
    invoke-virtual {v1}, Lf/h/c/v/o/d;->i()Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Installation ID could not be validated with the Firebase servers (maybe it was deleted). Firebase Installations will need to create a new Installation ID and auth token. Please retry your last request."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lf/h/c/v/f;->k(Ljava/lang/Exception;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v0, v1}, Lf/h/c/v/f;->l(Lf/h/c/v/o/d;)V

    goto :goto_4

    :catchall_1
    move-exception v0

    if-eqz v3, :cond_9

    :try_start_8
    invoke-virtual {v3}, Lf/h/c/v/b;->b()V

    :cond_9
    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    :catch_0
    move-exception v1

    invoke-virtual {v0, v1}, Lf/h/c/v/f;->k(Ljava/lang/Exception;)V

    :cond_a
    :goto_4
    return-void

    :catchall_3
    move-exception v0

    if-eqz v3, :cond_b

    :try_start_9
    invoke-virtual {v3}, Lf/h/c/v/b;->b()V

    :cond_b
    throw v0

    :catchall_4
    move-exception v0

    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0
.end method
