.class public final Lf/h/c/p/b/i;
.super Lf/h/a/f/f/h/i/p;
.source "com.google.firebase:firebase-dynamic-links@@19.1.1"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/h/i/p<",
        "Lf/h/c/p/b/d;",
        "Lcom/google/firebase/dynamiclinks/PendingDynamicLinkData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/h/c/k/a/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/c/k/a/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lf/h/a/f/f/h/i/p;-><init>()V

    iput-object p2, p0, Lf/h/c/p/b/i;->a:Ljava/lang/String;

    iput-object p1, p0, Lf/h/c/p/b/i;->b:Lf/h/c/k/a/a;

    return-void
.end method


# virtual methods
.method public final synthetic c(Lf/h/a/f/f/h/a$b;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lf/h/c/p/b/d;

    new-instance v0, Lf/h/c/p/b/g;

    iget-object v1, p0, Lf/h/c/p/b/i;->b:Lf/h/c/k/a/a;

    invoke-direct {v0, v1, p2}, Lf/h/c/p/b/g;-><init>(Lf/h/c/k/a/a;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    iget-object p2, p0, Lf/h/c/p/b/i;->a:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p1}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object p1

    check-cast p1, Lf/h/c/p/b/j;

    invoke-interface {p1, v0, p2}, Lf/h/c/p/b/j;->S(Lf/h/c/p/b/h;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
