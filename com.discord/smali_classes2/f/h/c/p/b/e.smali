.class public final Lf/h/c/p/b/e;
.super Lf/h/c/p/a;
.source "com.google.firebase:firebase-dynamic-links@@19.1.1"


# instance fields
.field public final a:Lf/h/a/f/f/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/b<",
            "Lf/h/a/f/f/h/a$d$c;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/h/c/k/a/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/c/c;Lf/h/c/k/a/a;)V
    .locals 1
    .param p2    # Lf/h/c/k/a/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Lf/h/c/p/b/c;

    invoke-virtual {p1}, Lf/h/c/c;->a()V

    iget-object p1, p1, Lf/h/c/c;->a:Landroid/content/Context;

    invoke-direct {v0, p1}, Lf/h/c/p/b/c;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lf/h/c/p/a;-><init>()V

    iput-object v0, p0, Lf/h/c/p/b/e;->a:Lf/h/a/f/f/h/b;

    iput-object p2, p0, Lf/h/c/p/b/e;->b:Lf/h/c/k/a/a;

    if-nez p2, :cond_0

    const-string p1, "FDL"

    const-string p2, "FDL logging failed. Add a dependency for Firebase Analytics to your app to enable logging of Dynamic Link events."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/google/android/gms/tasks/Task;
    .locals 6
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Lcom/google/firebase/dynamiclinks/PendingDynamicLinkData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/p/b/e;->a:Lf/h/a/f/f/h/b;

    new-instance v1, Lf/h/c/p/b/i;

    iget-object v2, p0, Lf/h/c/p/b/e;->b:Lf/h/c/k/a/a;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lf/h/c/p/b/i;-><init>(Lf/h/c/k/a/a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/f/h/b;->c(Lf/h/a/f/f/h/i/p;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    sget-object v1, Lcom/google/firebase/dynamiclinks/internal/DynamicLinkData;->CREATOR:Landroid/os/Parcelable$Creator;

    const-string v2, "com.google.firebase.dynamiclinks.DYNAMIC_LINK_DATA"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object p1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    move-object p1, v2

    goto :goto_0

    :cond_0
    const-string v3, "null reference"

    invoke-static {v1, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    array-length v4, p1

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v5, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v3, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-interface {v1, v3}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    :goto_0
    check-cast p1, Lcom/google/firebase/dynamiclinks/internal/DynamicLinkData;

    if-eqz p1, :cond_1

    new-instance v2, Lcom/google/firebase/dynamiclinks/PendingDynamicLinkData;

    invoke-direct {v2, p1}, Lcom/google/firebase/dynamiclinks/PendingDynamicLinkData;-><init>(Lcom/google/firebase/dynamiclinks/internal/DynamicLinkData;)V

    :cond_1
    if-eqz v2, :cond_2

    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    :cond_2
    return-object v0
.end method
