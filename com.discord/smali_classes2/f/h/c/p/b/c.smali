.class public final Lf/h/c/p/b/c;
.super Lf/h/a/f/f/h/b;
.source "com.google.firebase:firebase-dynamic-links@@19.1.1"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/h/b<",
        "Lf/h/a/f/f/h/a$d$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final j:Lf/h/a/f/f/h/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$g<",
            "Lf/h/c/p/b/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "Lf/h/c/p/b/d;",
            "Lf/h/a/f/f/h/a$d$c;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lf/h/a/f/f/h/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a<",
            "Lf/h/a/f/f/h/a$d$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lf/h/a/f/f/h/a$g;

    invoke-direct {v0}, Lf/h/a/f/f/h/a$g;-><init>()V

    sput-object v0, Lf/h/c/p/b/c;->j:Lf/h/a/f/f/h/a$g;

    new-instance v1, Lf/h/c/p/b/b;

    invoke-direct {v1}, Lf/h/c/p/b/b;-><init>()V

    sput-object v1, Lf/h/c/p/b/c;->k:Lf/h/a/f/f/h/a$a;

    new-instance v2, Lf/h/a/f/f/h/a;

    const-string v3, "DynamicLinks.API"

    invoke-direct {v2, v3, v1, v0}, Lf/h/a/f/f/h/a;-><init>(Ljava/lang/String;Lf/h/a/f/f/h/a$a;Lf/h/a/f/f/h/a$g;)V

    sput-object v2, Lf/h/c/p/b/c;->l:Lf/h/a/f/f/h/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/h/c/p/b/c;->l:Lf/h/a/f/f/h/a;

    sget-object v1, Lf/h/a/f/f/h/b$a;->c:Lf/h/a/f/f/h/b$a;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v2, v1}, Lf/h/a/f/f/h/b;-><init>(Landroid/content/Context;Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;Lf/h/a/f/f/h/b$a;)V

    return-void
.end method
