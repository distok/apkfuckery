.class public final synthetic Lf/h/c/z/b;
.super Ljava/lang/Object;
.source "DefaultUserAgentPublisher.java"

# interfaces
.implements Lf/h/c/m/f;


# static fields
.field public static final a:Lf/h/c/z/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/c/z/b;

    invoke-direct {v0}, Lf/h/c/z/b;-><init>()V

    sput-object v0, Lf/h/c/z/b;->a:Lf/h/c/z/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/m/e;)Ljava/lang/Object;
    .locals 3

    new-instance v0, Lf/h/c/z/c;

    const-class v1, Lf/h/c/z/e;

    invoke-interface {p1, v1}, Lf/h/c/m/e;->d(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object p1

    sget-object v1, Lf/h/c/z/d;->b:Lf/h/c/z/d;

    if-nez v1, :cond_1

    const-class v2, Lf/h/c/z/d;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lf/h/c/z/d;->b:Lf/h/c/z/d;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/c/z/d;

    invoke-direct {v1}, Lf/h/c/z/d;-><init>()V

    sput-object v1, Lf/h/c/z/d;->b:Lf/h/c/z/d;

    :cond_0
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    invoke-direct {v0, p1, v1}, Lf/h/c/z/c;-><init>(Ljava/util/Set;Lf/h/c/z/d;)V

    return-object v0
.end method
