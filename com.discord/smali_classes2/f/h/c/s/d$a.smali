.class public final enum Lf/h/c/s/d$a;
.super Ljava/lang/Enum;
.source "HeartBeatInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/s/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/s/d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/s/d$a;

.field public static final enum e:Lf/h/c/s/d$a;

.field public static final enum f:Lf/h/c/s/d$a;

.field public static final enum g:Lf/h/c/s/d$a;

.field public static final synthetic h:[Lf/h/c/s/d$a;


# instance fields
.field private final code:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/h/c/s/d$a;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/h/c/s/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/h/c/s/d$a;->d:Lf/h/c/s/d$a;

    new-instance v1, Lf/h/c/s/d$a;

    const-string v3, "SDK"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/h/c/s/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/h/c/s/d$a;->e:Lf/h/c/s/d$a;

    new-instance v3, Lf/h/c/s/d$a;

    const-string v5, "GLOBAL"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lf/h/c/s/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lf/h/c/s/d$a;->f:Lf/h/c/s/d$a;

    new-instance v5, Lf/h/c/s/d$a;

    const-string v7, "COMBINED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v8}, Lf/h/c/s/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lf/h/c/s/d$a;->g:Lf/h/c/s/d$a;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/h/c/s/d$a;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/h/c/s/d$a;->h:[Lf/h/c/s/d$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/h/c/s/d$a;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/s/d$a;
    .locals 1

    const-class v0, Lf/h/c/s/d$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/s/d$a;

    return-object p0
.end method

.method public static values()[Lf/h/c/s/d$a;
    .locals 1

    sget-object v0, Lf/h/c/s/d$a;->h:[Lf/h/c/s/d$a;

    invoke-virtual {v0}, [Lf/h/c/s/d$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/s/d$a;

    return-object v0
.end method


# virtual methods
.method public f()I
    .locals 1

    iget v0, p0, Lf/h/c/s/d$a;->code:I

    return v0
.end method
