.class public Lf/h/c/s/c;
.super Ljava/lang/Object;
.source "DefaultHeartBeatInfo.java"

# interfaces
.implements Lf/h/c/s/d;


# instance fields
.field public a:Lf/h/c/u/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/u/a<",
            "Lf/h/c/s/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lf/h/c/m/r;

    new-instance v1, Lf/h/c/s/a;

    invoke-direct {v1, p1}, Lf/h/c/s/a;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lf/h/c/m/r;-><init>(Lf/h/c/u/a;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/h/c/s/c;->a:Lf/h/c/u/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lf/h/c/s/d$a;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lf/h/c/s/c;->a:Lf/h/c/u/a;

    invoke-interface {v2}, Lf/h/c/u/a;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/c/s/e;

    invoke-virtual {v2, p1, v0, v1}, Lf/h/c/s/e;->a(Ljava/lang/String;J)Z

    move-result p1

    iget-object v2, p0, Lf/h/c/s/c;->a:Lf/h/c/u/a;

    invoke-interface {v2}, Lf/h/c/u/a;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/c/s/e;

    monitor-enter v2

    :try_start_0
    const-string v3, "fire-global"

    invoke-virtual {v2, v3, v0, v1}, Lf/h/c/s/e;->a(Ljava/lang/String;J)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    sget-object p1, Lf/h/c/s/d$a;->g:Lf/h/c/s/d$a;

    return-object p1

    :cond_0
    if-eqz v0, :cond_1

    sget-object p1, Lf/h/c/s/d$a;->f:Lf/h/c/s/d$a;

    return-object p1

    :cond_1
    if-eqz p1, :cond_2

    sget-object p1, Lf/h/c/s/d$a;->e:Lf/h/c/s/d$a;

    return-object p1

    :cond_2
    sget-object p1, Lf/h/c/s/d$a;->d:Lf/h/c/s/d$a;

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1
.end method
