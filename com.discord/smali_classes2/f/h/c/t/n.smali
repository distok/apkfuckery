.class public Lf/h/c/t/n;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@21.0.0"


# instance fields
.field public final a:Lf/h/c/c;

.field public final b:Lf/h/c/t/q;

.field public final c:Lf/h/a/f/e/b;

.field public final d:Lf/h/c/u/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/u/a<",
            "Lf/h/c/z/h;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lf/h/c/u/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/u/a<",
            "Lf/h/c/s/d;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lf/h/c/v/g;


# direct methods
.method public constructor <init>(Lf/h/c/c;Lf/h/c/t/q;Lf/h/c/u/a;Lf/h/c/u/a;Lf/h/c/v/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/c;",
            "Lf/h/c/t/q;",
            "Lf/h/c/u/a<",
            "Lf/h/c/z/h;",
            ">;",
            "Lf/h/c/u/a<",
            "Lf/h/c/s/d;",
            ">;",
            "Lf/h/c/v/g;",
            ")V"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/e/b;

    invoke-virtual {p1}, Lf/h/c/c;->a()V

    iget-object v1, p1, Lf/h/c/c;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lf/h/a/f/e/b;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/t/n;->a:Lf/h/c/c;

    iput-object p2, p0, Lf/h/c/t/n;->b:Lf/h/c/t/q;

    iput-object v0, p0, Lf/h/c/t/n;->c:Lf/h/a/f/e/b;

    iput-object p3, p0, Lf/h/c/t/n;->d:Lf/h/c/u/a;

    iput-object p4, p0, Lf/h/c/t/n;->e:Lf/h/c/u/a;

    iput-object p5, p0, Lf/h/c/t/n;->f:Lf/h/c/v/g;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "Landroid/os/Bundle;",
            ">;)",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget v0, Lf/h/c/t/h;->a:I

    sget-object v0, Lf/h/c/t/g;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lf/h/c/t/m;

    invoke-direct {v1, p0}, Lf/h/c/t/m;-><init>(Lf/h/c/t/n;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/tasks/Task;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/tasks/Task;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    const-string v0, "scope"

    invoke-virtual {p4, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p3, "sender"

    invoke-virtual {p4, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p3, "subtype"

    invoke-virtual {p4, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "appid"

    invoke-virtual {p4, p2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "gmp_app_id"

    iget-object p2, p0, Lf/h/c/t/n;->a:Lf/h/c/c;

    invoke-virtual {p2}, Lf/h/c/c;->a()V

    iget-object p2, p2, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object p2, p2, Lf/h/c/i;->b:Ljava/lang/String;

    invoke-virtual {p4, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "gmsv"

    iget-object p2, p0, Lf/h/c/t/n;->b:Lf/h/c/t/q;

    monitor-enter p2

    :try_start_0
    iget p3, p2, Lf/h/c/t/q;->d:I

    if-nez p3, :cond_0

    const-string p3, "com.google.android.gms"

    invoke-virtual {p2, p3}, Lf/h/c/t/q;->c(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object p3

    if-eqz p3, :cond_0

    iget p3, p3, Landroid/content/pm/PackageInfo;->versionCode:I

    iput p3, p2, Lf/h/c/t/q;->d:I

    :cond_0
    iget p3, p2, Lf/h/c/t/q;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    monitor-exit p2

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p4, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "osv"

    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p4, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "app_ver"

    iget-object p2, p0, Lf/h/c/t/n;->b:Lf/h/c/t/q;

    invoke-virtual {p2}, Lf/h/c/t/q;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p4, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "app_ver_name"

    iget-object p2, p0, Lf/h/c/t/n;->b:Lf/h/c/t/q;

    monitor-enter p2

    :try_start_1
    iget-object p3, p2, Lf/h/c/t/q;->c:Ljava/lang/String;

    if-nez p3, :cond_1

    invoke-virtual {p2}, Lf/h/c/t/q;->e()V

    :cond_1
    iget-object p3, p2, Lf/h/c/t/q;->c:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    monitor-exit p2

    invoke-virtual {p4, p1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "firebase-app-name-hash"

    iget-object p2, p0, Lf/h/c/t/n;->a:Lf/h/c/c;

    invoke-virtual {p2}, Lf/h/c/c;->a()V

    iget-object p2, p2, Lf/h/c/c;->b:Ljava/lang/String;

    const-string p3, "SHA-1"

    :try_start_2
    invoke-static {p3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object p3

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p2

    const/16 p3, 0xb

    invoke-static {p2, p3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p2
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    const-string p2, "[HASH-ERROR]"

    :goto_0
    invoke-virtual {p4, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    :try_start_3
    iget-object p2, p0, Lf/h/c/t/n;->f:Lf/h/c/v/g;

    invoke-interface {p2, p1}, Lf/h/c/v/g;->a(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object p2

    invoke-static {p2}, Lf/h/a/f/f/n/g;->c(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/c/v/k;

    invoke-virtual {p2}, Lf/h/c/v/k;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_2

    const-string p3, "Goog-Firebase-Installations-Auth"

    invoke-virtual {p4, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    const-string p2, "FirebaseInstanceId"

    const-string p3, "FIS auth token is empty"

    invoke-static {p2, p3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception p2

    goto :goto_1

    :catch_2
    move-exception p2

    :goto_1
    const-string p3, "FirebaseInstanceId"

    const-string v0, "Failed to get FIS auth token"

    invoke-static {p3, v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    const-string p2, "21.0.0"

    const-string p3, "cliv"

    const-string v0, "fiid-"

    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p4, p3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lf/h/c/t/n;->e:Lf/h/c/u/a;

    invoke-interface {p2}, Lf/h/c/u/a;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/c/s/d;

    iget-object p3, p0, Lf/h/c/t/n;->d:Lf/h/c/u/a;

    invoke-interface {p3}, Lf/h/c/u/a;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/h/c/z/h;

    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    const-string v0, "fire-iid"

    invoke-interface {p2, v0}, Lf/h/c/s/d;->a(Ljava/lang/String;)Lf/h/c/s/d$a;

    move-result-object p2

    sget-object v0, Lf/h/c/s/d$a;->d:Lf/h/c/s/d$a;

    if-eq p2, v0, :cond_3

    const-string v0, "Firebase-Client-Log-Type"

    invoke-virtual {p2}, Lf/h/c/s/d$a;->f()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p4, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "Firebase-Client"

    invoke-interface {p3}, Lf/h/c/z/h;->getUserAgent()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object p2, p0, Lf/h/c/t/n;->c:Lf/h/a/f/e/b;

    sget-object p3, Lf/h/a/f/e/y;->d:Ljava/util/concurrent/Executor;

    iget-object v0, p2, Lf/h/a/f/e/b;->c:Lf/h/a/f/e/q;

    monitor-enter v0

    :try_start_4
    iget v1, v0, Lf/h/a/f/e/q;->b:I

    if-nez v1, :cond_4

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Lf/h/a/f/e/q;->b(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-eqz v1, :cond_4

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v1, v0, Lf/h/a/f/e/q;->b:I

    :cond_4
    iget v1, v0, Lf/h/a/f/e/q;->b:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit v0

    const v0, 0xb71b00

    if-lt v1, v0, :cond_5

    iget-object p1, p2, Lf/h/a/f/e/b;->b:Landroid/content/Context;

    invoke-static {p1}, Lf/h/a/f/e/e;->a(Landroid/content/Context;)Lf/h/a/f/e/e;

    move-result-object p1

    new-instance p2, Lf/h/a/f/e/r;

    monitor-enter p1

    :try_start_5
    iget v0, p1, Lf/h/a/f/e/e;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p1, Lf/h/a/f/e/e;->d:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p1

    invoke-direct {p2, v0, p4}, Lf/h/a/f/e/r;-><init>(ILandroid/os/Bundle;)V

    invoke-virtual {p1, p2}, Lf/h/a/f/e/e;->b(Lf/h/a/f/e/p;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    sget-object p2, Lf/h/a/f/e/s;->a:Lf/h/a/f/p/a;

    invoke-virtual {p1, p3, p2}, Lcom/google/android/gms/tasks/Task;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_3

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_5
    iget-object v0, p2, Lf/h/a/f/e/b;->c:Lf/h/a/f/e/q;

    invoke-virtual {v0}, Lf/h/a/f/e/q;->a()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 p1, 0x1

    :cond_6
    if-nez p1, :cond_7

    new-instance p1, Ljava/io/IOException;

    const-string p2, "MISSING_INSTANCEID_SERVICE"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->s(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_3

    :cond_7
    invoke-virtual {p2, p4}, Lf/h/a/f/e/b;->b(Landroid/os/Bundle;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lf/h/a/f/e/u;

    invoke-direct {v0, p2, p4}, Lf/h/a/f/e/u;-><init>(Lf/h/a/f/e/b;Landroid/os/Bundle;)V

    invoke-virtual {p1, p3, v0}, Lcom/google/android/gms/tasks/Task;->j(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_3
    return-object p1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1

    :catchall_2
    move-exception p1

    monitor-exit p2

    throw p1

    :catchall_3
    move-exception p1

    monitor-exit p2

    throw p1
.end method
