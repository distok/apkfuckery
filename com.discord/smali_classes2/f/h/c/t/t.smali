.class public final synthetic Lf/h/c/t/t;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@21.0.0"

# interfaces
.implements Lf/h/a/f/p/a;


# instance fields
.field public final a:Lf/h/c/t/u;

.field public final b:Landroid/util/Pair;


# direct methods
.method public constructor <init>(Lf/h/c/t/u;Landroid/util/Pair;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/t/t;->a:Lf/h/c/t/u;

    iput-object p2, p0, Lf/h/c/t/t;->b:Landroid/util/Pair;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lf/h/c/t/t;->a:Lf/h/c/t/u;

    iget-object v1, p0, Lf/h/c/t/t;->b:Landroid/util/Pair;

    monitor-enter v0

    :try_start_0
    iget-object v2, v0, Lf/h/c/t/u;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
