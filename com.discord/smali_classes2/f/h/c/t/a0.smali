.class public Lf/h/c/t/a0;
.super Landroid/os/Binder;
.source "com.google.firebase:firebase-iid@@21.0.0"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/t/a0$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/c/t/a0$a;


# direct methods
.method public constructor <init>(Lf/h/c/t/a0$a;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    iput-object p1, p0, Lf/h/c/t/a0;->a:Lf/h/c/t/a0$a;

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/t/d0$a;)V
    .locals 3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    const-string v1, "FirebaseInstanceId"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "service received new intent via bind strategy"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lf/h/c/t/a0;->a:Lf/h/c/t/a0$a;

    iget-object v1, p1, Lf/h/c/t/d0$a;->a:Landroid/content/Intent;

    check-cast v0, Lcom/google/firebase/messaging/EnhancedIntentService$a;

    iget-object v0, v0, Lcom/google/firebase/messaging/EnhancedIntentService$a;->a:Lcom/google/firebase/messaging/EnhancedIntentService;

    invoke-static {v0, v1}, Lcom/google/firebase/messaging/EnhancedIntentService;->access$000(Lcom/google/firebase/messaging/EnhancedIntentService;Landroid/content/Intent;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    sget v1, Lf/h/c/t/h;->a:I

    sget-object v1, Lf/h/c/t/g;->d:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/h/c/t/z;

    invoke-direct {v2, p1}, Lf/h/c/t/z;-><init>(Lf/h/c/t/d0$a;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;

    return-void

    :cond_1
    new-instance p1, Ljava/lang/SecurityException;

    const-string v0, "Binding only allowed within app"

    invoke-direct {p1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
