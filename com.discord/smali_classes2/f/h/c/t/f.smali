.class public Lf/h/c/t/f;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@21.0.0"


# static fields
.field public static final c:Ljava/lang/Object;

.field public static d:Lf/h/c/t/d0;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf/h/c/t/f;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/t/f;->a:Landroid/content/Context;

    sget-object p1, Lf/h/c/t/a;->d:Ljava/util/concurrent/Executor;

    iput-object p1, p0, Lf/h/c/t/f;->b:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/t/f;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/h/c/t/f;->b:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gms/tasks/Task;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "FirebaseInstanceId"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "FirebaseInstanceId"

    const-string v2, "Binding to service"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "com.google.firebase.MESSAGING_EVENT"

    sget-object v2, Lf/h/c/t/f;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lf/h/c/t/f;->d:Lf/h/c/t/d0;

    if-nez v3, :cond_1

    new-instance v3, Lf/h/c/t/d0;

    invoke-direct {v3, p0, v0}, Lf/h/c/t/d0;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v3, Lf/h/c/t/f;->d:Lf/h/c/t/d0;

    :cond_1
    sget-object p0, Lf/h/c/t/f;->d:Lf/h/c/t/d0;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-enter p0

    :try_start_1
    const-string v0, "FirebaseInstanceId"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "FirebaseInstanceId"

    const-string v1, "new intent queued in the bind-strategy delivery"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Lf/h/c/t/d0$a;

    invoke-direct {v0, p1}, Lf/h/c/t/d0$a;-><init>(Landroid/content/Intent;)V

    iget-object p1, p0, Lf/h/c/t/d0;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lf/h/c/t/b0;

    invoke-direct {v1, v0}, Lf/h/c/t/b0;-><init>(Lf/h/c/t/d0$a;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x2328

    invoke-interface {p1, v1, v3, v4, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iget-object v2, v0, Lf/h/c/t/d0$a;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v2, v2, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    new-instance v3, Lf/h/c/t/c0;

    invoke-direct {v3, v1}, Lf/h/c/t/c0;-><init>(Ljava/util/concurrent/ScheduledFuture;)V

    iget-object v1, v2, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v4, Lf/h/a/f/p/q;

    sget v5, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v4, p1, v3}, Lf/h/a/f/p/q;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)V

    invoke-virtual {v1, v4}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {v2}, Lf/h/a/f/p/b0;->w()V

    iget-object p1, p0, Lf/h/c/t/d0;->g:Ljava/util/Queue;

    invoke-interface {p1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lf/h/c/t/d0;->b()V

    iget-object p1, v0, Lf/h/c/t/d0$a;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object p1, p1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    sget p0, Lf/h/c/t/h;->a:I

    sget-object p0, Lf/h/c/t/g;->d:Ljava/util/concurrent/Executor;

    sget-object v0, Lf/h/c/t/d;->a:Lf/h/a/f/p/a;

    invoke-virtual {p1, p0, v0}, Lf/h/a/f/p/b0;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    return-object p0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :catchall_1
    move-exception p0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p0
.end method


# virtual methods
.method public b(Landroid/content/Intent;)Lcom/google/android/gms/tasks/Task;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "gcm.rawData64"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    const-string v3, "rawData"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lf/h/c/t/f;->a:Landroid/content/Context;

    invoke-static {}, Lf/h/a/f/f/n/g;->J()Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v4, 0x1a

    if-lt v1, v4, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v4

    const/high16 v5, 0x10000000

    and-int/2addr v4, v5

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    invoke-static {v0, p1}, Lf/h/c/t/f;->a(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lf/h/c/t/f;->b:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/h/c/t/b;

    invoke-direct {v2, v0, p1}, Lf/h/c/t/b;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->f(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    iget-object v2, p0, Lf/h/c/t/f;->b:Ljava/util/concurrent/Executor;

    new-instance v3, Lf/h/c/t/c;

    invoke-direct {v3, v0, p1}, Lf/h/c/t/c;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/tasks/Task;->j(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_1
    return-object p1
.end method
