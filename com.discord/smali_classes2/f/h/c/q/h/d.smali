.class public Lf/h/c/q/h/d;
.super Ljava/lang/Object;
.source "JsonDataEncoderBuilder.java"

# interfaces
.implements Lf/h/c/q/a;


# instance fields
.field public final synthetic a:Lf/h/c/q/h/e;


# direct methods
.method public constructor <init>(Lf/h/c/q/h/e;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/q/h/d;->a:Lf/h/c/q/h/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lf/h/c/q/h/d;->b(Ljava/lang/Object;Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Object;Ljava/io/Writer;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/io/Writer;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v6, Lf/h/c/q/h/f;

    iget-object v0, p0, Lf/h/c/q/h/d;->a:Lf/h/c/q/h/e;

    iget-object v2, v0, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    iget-object v3, v0, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    iget-object v4, v0, Lf/h/c/q/h/e;->c:Lf/h/c/q/c;

    iget-boolean v5, v0, Lf/h/c/q/h/e;->d:Z

    move-object v0, v6

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lf/h/c/q/h/f;-><init>(Ljava/io/Writer;Ljava/util/Map;Ljava/util/Map;Lf/h/c/q/c;Z)V

    const/4 p2, 0x0

    invoke-virtual {v6, p1, p2}, Lf/h/c/q/h/f;->g(Ljava/lang/Object;Z)Lf/h/c/q/h/f;

    invoke-virtual {v6}, Lf/h/c/q/h/f;->i()V

    iget-object p1, v6, Lf/h/c/q/h/f;->b:Landroid/util/JsonWriter;

    invoke-virtual {p1}, Landroid/util/JsonWriter;->flush()V

    return-void
.end method
