.class public final Lf/h/c/q/h/e;
.super Ljava/lang/Object;
.source "JsonDataEncoderBuilder.java"

# interfaces
.implements Lf/h/c/q/g/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/q/h/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/c/q/g/b<",
        "Lf/h/c/q/h/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final e:Lf/h/c/q/h/e$a;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lf/h/c/q/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lf/h/c/q/e<",
            "*>;>;"
        }
    .end annotation
.end field

.field public c:Lf/h/c/q/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/q/c<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/q/h/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/h/c/q/h/e$a;-><init>(Lf/h/c/q/h/d;)V

    sput-object v0, Lf/h/c/q/h/e;->e:Lf/h/c/q/h/e$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    sget-object v2, Lf/h/c/q/h/a;->a:Lf/h/c/q/h/a;

    iput-object v2, p0, Lf/h/c/q/h/e;->c:Lf/h/c/q/c;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lf/h/c/q/h/e;->d:Z

    const-class v2, Ljava/lang/String;

    sget-object v3, Lf/h/c/q/h/b;->a:Lf/h/c/q/h/b;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v2, Ljava/lang/Boolean;

    sget-object v3, Lf/h/c/q/h/c;->a:Lf/h/c/q/h/c;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v2, Ljava/util/Date;

    sget-object v3, Lf/h/c/q/h/e;->e:Lf/h/c/q/h/e$a;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
