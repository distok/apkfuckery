.class public final synthetic Lf/h/c/n/e/b;
.super Ljava/lang/Object;
.source "CrashlyticsNdkRegistrar.java"

# interfaces
.implements Lf/h/c/m/f;


# instance fields
.field public final a:Lcom/google/firebase/crashlytics/ndk/CrashlyticsNdkRegistrar;


# direct methods
.method public constructor <init>(Lcom/google/firebase/crashlytics/ndk/CrashlyticsNdkRegistrar;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/e/b;->a:Lcom/google/firebase/crashlytics/ndk/CrashlyticsNdkRegistrar;

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/m/e;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lf/h/c/n/e/b;->a:Lcom/google/firebase/crashlytics/ndk/CrashlyticsNdkRegistrar;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, ".com.google.firebase.crashlytics-ndk"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lf/h/c/n/e/a;

    new-instance v2, Lcom/google/firebase/crashlytics/ndk/JniNativeApi;

    invoke-direct {v2, p1}, Lcom/google/firebase/crashlytics/ndk/JniNativeApi;-><init>(Landroid/content/Context;)V

    new-instance v3, Lf/h/c/n/e/f;

    invoke-direct {v3, v0}, Lf/h/c/n/e/f;-><init>(Ljava/io/File;)V

    invoke-direct {v1, p1, v2, v3}, Lf/h/c/n/e/a;-><init>(Landroid/content/Context;Lf/h/c/n/e/d;Lf/h/c/n/e/f;)V

    new-instance p1, Lf/h/c/n/e/c;

    invoke-direct {p1, v1}, Lf/h/c/n/e/c;-><init>(Lf/h/c/n/e/e;)V

    return-object p1
.end method
