.class public final synthetic Lf/h/c/n/b;
.super Ljava/lang/Object;
.source "CrashlyticsRegistrar.java"

# interfaces
.implements Lf/h/c/m/f;


# instance fields
.field public final a:Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;


# direct methods
.method public constructor <init>(Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/b;->a:Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/m/e;)Ljava/lang/Object;
    .locals 44

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    iget-object v2, v1, Lf/h/c/n/b;->a:Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v2, Lf/h/c/c;

    invoke-interface {v0, v2}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/c/c;

    const-class v3, Lf/h/c/n/d/a;

    invoke-interface {v0, v3}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/c/n/d/a;

    const-class v4, Lf/h/c/k/a/a;

    invoke-interface {v0, v4}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/c/k/a/a;

    const-class v5, Lf/h/c/v/g;

    invoke-interface {v0, v5}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/v/g;

    const-string v11, "0.0"

    const-string v12, "FirebaseCrashlytics"

    sget-object v13, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    invoke-virtual {v2}, Lf/h/c/c;->a()V

    iget-object v15, v2, Lf/h/c/c;->a:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lf/h/c/n/d/k/w0;

    invoke-direct {v6, v15, v5, v0}, Lf/h/c/n/d/k/w0;-><init>(Landroid/content/Context;Ljava/lang/String;Lf/h/c/v/g;)V

    new-instance v7, Lf/h/c/n/d/k/q0;

    invoke-direct {v7, v2}, Lf/h/c/n/d/k/q0;-><init>(Lf/h/c/c;)V

    if-nez v3, :cond_0

    new-instance v0, Lf/h/c/n/d/c;

    invoke-direct {v0}, Lf/h/c/n/d/c;-><init>()V

    goto :goto_0

    :cond_0
    move-object v0, v3

    :goto_0
    new-instance v14, Lf/h/c/n/d/h;

    invoke-direct {v14, v2, v15, v6, v7}, Lf/h/c/n/d/h;-><init>(Lf/h/c/c;Landroid/content/Context;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/k/q0;)V

    if-eqz v4, :cond_3

    const-string v3, "Firebase Analytics is available."

    invoke-virtual {v13, v3}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v3, Lf/h/c/n/d/i/e;

    invoke-direct {v3, v4}, Lf/h/c/n/d/i/e;-><init>(Lf/h/c/k/a/a;)V

    new-instance v5, Lf/h/c/n/a;

    invoke-direct {v5}, Lf/h/c/n/a;-><init>()V

    const-string v8, "clx"

    invoke-interface {v4, v8, v5}, Lf/h/c/k/a/a;->g(Ljava/lang/String;Lf/h/c/k/a/a$b;)Lf/h/c/k/a/a$a;

    move-result-object v8

    if-nez v8, :cond_1

    const-string v8, "Could not register AnalyticsConnectorListener with Crashlytics origin."

    invoke-virtual {v13, v8}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const-string v8, "crash"

    invoke-interface {v4, v8, v5}, Lf/h/c/k/a/a;->g(Ljava/lang/String;Lf/h/c/k/a/a$b;)Lf/h/c/k/a/a$a;

    move-result-object v8

    if-eqz v8, :cond_1

    const-string v4, "A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version."

    invoke-virtual {v13, v4}, Lf/h/c/n/d/b;->g(Ljava/lang/String;)V

    :cond_1
    if-eqz v8, :cond_2

    const-string v4, "Firebase Analytics listener registered successfully."

    invoke-virtual {v13, v4}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v4, Lf/h/c/n/d/i/d;

    invoke-direct {v4}, Lf/h/c/n/d/i/d;-><init>()V

    new-instance v8, Lf/h/c/n/d/i/c;

    const/16 v9, 0x1f4

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v8, v3, v9, v10}, Lf/h/c/n/d/i/c;-><init>(Lf/h/c/n/d/i/e;ILjava/util/concurrent/TimeUnit;)V

    iput-object v4, v5, Lf/h/c/n/a;->b:Lf/h/c/n/d/i/b;

    iput-object v8, v5, Lf/h/c/n/a;->a:Lf/h/c/n/d/i/b;

    move-object v3, v8

    goto :goto_1

    :cond_2
    const-string v4, "Firebase Analytics listener registration failed."

    invoke-virtual {v13, v4}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v4, Lf/h/c/n/d/j/b;

    invoke-direct {v4}, Lf/h/c/n/d/j/b;-><init>()V

    :goto_1
    move-object v9, v3

    move-object v8, v4

    goto :goto_2

    :cond_3
    const-string v3, "Firebase Analytics is unavailable."

    invoke-virtual {v13, v3}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v3, Lf/h/c/n/d/j/b;

    invoke-direct {v3}, Lf/h/c/n/d/j/b;-><init>()V

    new-instance v4, Lf/h/c/n/d/i/f;

    invoke-direct {v4}, Lf/h/c/n/d/i/f;-><init>()V

    move-object v8, v3

    move-object v9, v4

    :goto_2
    const-string v3, "Crashlytics Exception Handler"

    invoke-static {v3}, Lf/h/a/f/f/n/g;->e(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    new-instance v5, Lf/h/c/n/d/k/k0;

    move-object v3, v5

    move-object v4, v2

    move-object v1, v5

    move-object v5, v6

    move-object v6, v0

    invoke-direct/range {v3 .. v10}, Lf/h/c/n/d/k/k0;-><init>(Lf/h/c/c;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/a;Lf/h/c/n/d/k/q0;Lf/h/c/n/d/j/a;Lf/h/c/n/d/i/a;Ljava/util/concurrent/ExecutorService;)V

    const/4 v3, 0x6

    const/4 v0, 0x0

    :try_start_0
    iget-object v4, v14, Lf/h/c/n/d/h;->l:Lf/h/c/n/d/k/w0;

    invoke-virtual {v4}, Lf/h/c/n/d/k/w0;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v14, Lf/h/c/n/d/h;->i:Ljava/lang/String;

    iget-object v4, v14, Lf/h/c/n/d/h;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, v14, Lf/h/c/n/d/h;->d:Landroid/content/pm/PackageManager;

    iget-object v4, v14, Lf/h/c/n/d/h;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v14, Lf/h/c/n/d/h;->e:Ljava/lang/String;

    iget-object v5, v14, Lf/h/c/n/d/h;->d:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v4, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iput-object v0, v14, Lf/h/c/n/d/h;->f:Landroid/content/pm/PackageInfo;

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v14, Lf/h/c/n/d/h;->g:Ljava/lang/String;

    iget-object v0, v14, Lf/h/c/n/d/h;->f:Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v0, :cond_4

    move-object v0, v11

    :cond_4
    iput-object v0, v14, Lf/h/c/n/d/h;->h:Ljava/lang/String;

    iget-object v0, v14, Lf/h/c/n/d/h;->d:Landroid/content/pm/PackageManager;

    iget-object v4, v14, Lf/h/c/n/d/h;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v14, Lf/h/c/n/d/h;->j:Ljava/lang/String;

    iget-object v0, v14, Lf/h/c/n/d/h;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v14, Lf/h/c/n/d/h;->k:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {v13, v3}, Lf/h/c/n/d/b;->a(I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "Failed init"

    invoke-static {v12, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_5
    const/4 v0, 0x0

    :goto_3
    if-nez v0, :cond_6

    const-string v0, "Unable to start Crashlytics."

    invoke-virtual {v13, v0}, Lf/h/c/n/d/b;->d(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_6
    const-string v0, "com.google.firebase.crashlytics.startup"

    invoke-static {v0}, Lf/h/a/f/f/n/g;->e(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {v2}, Lf/h/c/c;->a()V

    iget-object v0, v2, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v0, v0, Lf/h/c/i;->b:Ljava/lang/String;

    iget-object v2, v14, Lf/h/c/n/d/h;->l:Lf/h/c/n/d/k/w0;

    iget-object v4, v14, Lf/h/c/n/d/h;->a:Lf/h/c/n/d/n/c;

    iget-object v5, v14, Lf/h/c/n/d/h;->g:Ljava/lang/String;

    iget-object v6, v14, Lf/h/c/n/d/h;->h:Ljava/lang/String;

    invoke-virtual {v14}, Lf/h/c/n/d/h;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v14, Lf/h/c/n/d/h;->m:Lf/h/c/n/d/k/q0;

    invoke-virtual {v2}, Lf/h/c/n/d/k/w0;->c()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lf/h/c/n/d/k/f1;

    invoke-direct {v10}, Lf/h/c/n/d/k/f1;-><init>()V

    move-object/from16 p1, v11

    new-instance v11, Lf/h/c/n/d/s/f;

    invoke-direct {v11, v10}, Lf/h/c/n/d/s/f;-><init>(Lf/h/c/n/d/k/f1;)V

    move-object/from16 v26, v12

    new-instance v12, Lf/h/c/n/d/s/a;

    invoke-direct {v12, v15}, Lf/h/c/n/d/s/a;-><init>(Landroid/content/Context;)V

    move-object/from16 v27, v14

    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v28, v13

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v0, v13, v16

    move-object/from16 v29, v1

    const-string v1, "https://firebase-settings.crashlytics.com/spi/v2/platforms/android/gmp/%s/settings"

    invoke-static {v14, v1, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v13, Lf/h/c/n/d/s/j/c;

    invoke-direct {v13, v7, v1, v4}, Lf/h/c/n/d/s/j/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;)V

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    sget-object v7, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lf/h/c/n/d/k/w0;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v16

    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lf/h/c/n/d/k/w0;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/16 v16, 0x1

    aput-object v7, v4, v16

    const-string v7, "%s/%s"

    invoke-static {v14, v7, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    sget-object v4, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lf/h/c/n/d/k/w0;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lf/h/c/n/d/k/w0;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {v15}, Lf/h/c/n/d/k/h;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const/4 v14, 0x0

    aput-object v7, v4, v14

    aput-object v0, v4, v16

    aput-object v6, v4, v1

    const/4 v1, 0x3

    aput-object v5, v4, v1

    invoke-static {v4}, Lf/h/c/n/d/k/h;->f([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static {v9}, Lf/h/c/n/d/k/s0;->f(Ljava/lang/String;)Lf/h/c/n/d/k/s0;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/c/n/d/k/s0;->g()I

    move-result v25

    new-instance v1, Lf/h/c/n/d/s/i/g;

    move-object/from16 v16, v1

    move-object/from16 v17, v0

    move-object/from16 v21, v2

    move-object/from16 v23, v6

    move-object/from16 v24, v5

    invoke-direct/range {v16 .. v25}, Lf/h/c/n/d/s/i/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/x0;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v2, Lf/h/c/n/d/s/d;

    move-object/from16 v4, v27

    move-object v14, v2

    move-object/from16 v17, v10

    move-object/from16 v18, v11

    move-object/from16 v19, v12

    move-object/from16 v20, v13

    move-object/from16 v21, v8

    invoke-direct/range {v14 .. v21}, Lf/h/c/n/d/s/d;-><init>(Landroid/content/Context;Lf/h/c/n/d/s/i/g;Lf/h/c/n/d/k/f1;Lf/h/c/n/d/s/f;Lf/h/c/n/d/s/a;Lf/h/c/n/d/s/j/d;Lf/h/c/n/d/k/q0;)V

    sget-object v0, Lf/h/c/n/d/s/c;->d:Lf/h/c/n/d/s/c;

    invoke-virtual {v2, v0, v3}, Lf/h/c/n/d/s/d;->d(Lf/h/c/n/d/s/c;Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lf/h/c/n/d/g;

    invoke-direct {v1, v4}, Lf/h/c/n/d/g;-><init>(Lf/h/c/n/d/h;)V

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/tasks/Task;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-object/from16 v1, v29

    iget-object v0, v1, Lf/h/c/n/d/k/k0;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/c/n/d/k/h;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Mapping file ID is: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v13, v28

    invoke-virtual {v13, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, v1, Lf/h/c/n/d/k/k0;->a:Landroid/content/Context;

    const-string v5, "com.crashlytics.RequireBuildId"

    const/4 v6, 0x1

    invoke-static {v0, v5, v6}, Lf/h/c/n/d/k/h;->j(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    const-string v5, "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app\'s build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account."

    if-nez v0, :cond_7

    const-string v0, "Configured not to require a build ID."

    invoke-virtual {v13, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    invoke-static {v7}, Lf/h/c/n/d/k/h;->s(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    :goto_4
    const/4 v0, 0x1

    move-object/from16 v14, v26

    goto :goto_5

    :cond_8
    const-string v0, "."

    move-object/from16 v14, v26

    invoke-static {v14, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, ".     |  | "

    invoke-static {v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, ".     |  |"

    invoke-static {v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".   \\ |  | /"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".    \\    /"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".     \\  /"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".      \\/"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".      /\\"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".     /  \\"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".    /    \\"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".   / |  | \\"

    invoke-static {v14, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v14, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_5
    if-eqz v0, :cond_c

    iget-object v0, v1, Lf/h/c/n/d/k/k0;->b:Lf/h/c/c;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v6, v0, Lf/h/c/i;->b:Ljava/lang/String;

    :try_start_1
    const-string v0, "Initializing Crashlytics 17.3.0"

    invoke-virtual {v13, v0}, Lf/h/c/n/d/b;->f(Ljava/lang/String;)V

    new-instance v0, Lf/h/c/n/d/o/h;

    iget-object v5, v1, Lf/h/c/n/d/k/k0;->a:Landroid/content/Context;

    invoke-direct {v0, v5}, Lf/h/c/n/d/o/h;-><init>(Landroid/content/Context;)V

    new-instance v8, Lf/h/c/n/d/k/m0;

    const-string v9, "crash_marker"

    invoke-direct {v8, v9, v0}, Lf/h/c/n/d/k/m0;-><init>(Ljava/lang/String;Lf/h/c/n/d/o/h;)V

    iput-object v8, v1, Lf/h/c/n/d/k/k0;->f:Lf/h/c/n/d/k/m0;

    new-instance v8, Lf/h/c/n/d/k/m0;

    const-string v9, "initialization_marker"

    invoke-direct {v8, v9, v0}, Lf/h/c/n/d/k/m0;-><init>(Ljava/lang/String;Lf/h/c/n/d/o/h;)V

    iput-object v8, v1, Lf/h/c/n/d/k/k0;->e:Lf/h/c/n/d/k/m0;

    new-instance v33, Lf/h/c/n/d/n/c;

    invoke-direct/range {v33 .. v33}, Lf/h/c/n/d/n/c;-><init>()V

    new-instance v12, Lf/h/c/n/d/u/a;

    invoke-direct {v12, v5}, Lf/h/c/n/d/u/a;-><init>(Landroid/content/Context;)V

    iget-object v8, v1, Lf/h/c/n/d/k/k0;->i:Lf/h/c/n/d/k/w0;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Lf/h/c/n/d/k/w0;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget v10, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v5, :cond_9

    move-object/from16 v11, p1

    goto :goto_6

    :cond_9
    move-object v11, v5

    :goto_6
    new-instance v15, Lf/h/c/n/d/k/b;

    move-object v5, v15

    invoke-direct/range {v5 .. v12}, Lf/h/c/n/d/k/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/u/a;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Installer package name is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v15, Lf/h/c/n/d/k/b;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v5, Lf/h/c/n/d/k/x;

    iget-object v6, v1, Lf/h/c/n/d/k/k0;->a:Landroid/content/Context;

    iget-object v7, v1, Lf/h/c/n/d/k/k0;->m:Lf/h/c/n/d/k/i;

    iget-object v8, v1, Lf/h/c/n/d/k/k0;->i:Lf/h/c/n/d/k/w0;

    iget-object v9, v1, Lf/h/c/n/d/k/k0;->c:Lf/h/c/n/d/k/q0;

    iget-object v10, v1, Lf/h/c/n/d/k/k0;->f:Lf/h/c/n/d/k/m0;

    const/16 v39, 0x0

    const/16 v40, 0x0

    iget-object v11, v1, Lf/h/c/n/d/k/k0;->n:Lf/h/c/n/d/a;

    iget-object v12, v1, Lf/h/c/n/d/k/k0;->k:Lf/h/c/n/d/i/a;

    move-object/from16 v30, v5

    move-object/from16 v31, v6

    move-object/from16 v32, v7

    move-object/from16 v34, v8

    move-object/from16 v35, v9

    move-object/from16 v36, v0

    move-object/from16 v37, v10

    move-object/from16 v38, v15

    move-object/from16 v41, v11

    move-object/from16 v42, v12

    move-object/from16 v43, v2

    invoke-direct/range {v30 .. v43}, Lf/h/c/n/d/k/x;-><init>(Landroid/content/Context;Lf/h/c/n/d/k/i;Lf/h/c/n/d/n/c;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/k/q0;Lf/h/c/n/d/o/h;Lf/h/c/n/d/k/m0;Lf/h/c/n/d/k/b;Lf/h/c/n/d/q/a;Lf/h/c/n/d/q/b$b;Lf/h/c/n/d/a;Lf/h/c/n/d/i/a;Lf/h/c/n/d/s/e;)V

    iput-object v5, v1, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v0, v1, Lf/h/c/n/d/k/k0;->e:Lf/h/c/n/d/k/m0;

    invoke-virtual {v0}, Lf/h/c/n/d/k/m0;->b()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    iget-object v5, v1, Lf/h/c/n/d/k/k0;->m:Lf/h/c/n/d/k/i;

    new-instance v6, Lf/h/c/n/d/k/l0;

    invoke-direct {v6, v1}, Lf/h/c/n/d/k/l0;-><init>(Lf/h/c/n/d/k/k0;)V

    invoke-virtual {v5, v6}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    invoke-static {v5}, Lf/h/c/n/d/k/i1;->a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v6, v5}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, v1, Lf/h/c/n/d/k/k0;->g:Z

    goto :goto_7

    :catch_1
    const/4 v5, 0x0

    iput-boolean v5, v1, Lf/h/c/n/d/k/k0;->g:Z

    :goto_7
    iget-object v5, v1, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v6

    iget-object v7, v5, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v8, Lf/h/c/n/d/k/r;

    invoke-direct {v8, v5}, Lf/h/c/n/d/k/r;-><init>(Lf/h/c/n/d/k/x;)V

    invoke-virtual {v7, v8}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    new-instance v7, Lf/h/c/n/d/k/b0;

    invoke-direct {v7, v5}, Lf/h/c/n/d/k/b0;-><init>(Lf/h/c/n/d/k/x;)V

    new-instance v8, Lf/h/c/n/d/k/p0;

    invoke-direct {v8, v7, v2, v6}, Lf/h/c/n/d/k/p0;-><init>(Lf/h/c/n/d/k/p0$a;Lf/h/c/n/d/s/e;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    iput-object v8, v5, Lf/h/c/n/d/k/x;->u:Lf/h/c/n/d/k/p0;

    invoke-static {v8}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    if-eqz v0, :cond_a

    iget-object v0, v1, Lf/h/c/n/d/k/k0;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/c/n/d/k/h;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "Crashlytics did not finish previous background initialization. Initializing synchronously."

    invoke-virtual {v13, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lf/h/c/n/d/k/k0;->b(Lf/h/c/n/d/s/e;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_8

    :cond_a
    const-string v0, "Exception handling initialization successful"

    invoke-virtual {v13, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    const/16 v20, 0x1

    goto :goto_9

    :catch_2
    move-exception v0

    const/4 v5, 0x6

    invoke-virtual {v13, v5}, Lf/h/c/n/d/b;->a(I)Z

    move-result v5

    if-eqz v5, :cond_b

    const-string v5, "Crashlytics was not started due to an exception during initialization"

    invoke-static {v14, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_b
    const/4 v0, 0x0

    iput-object v0, v1, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    :goto_8
    const/4 v0, 0x0

    const/16 v20, 0x0

    :goto_9
    new-instance v0, Lf/h/c/n/c;

    move-object/from16 v16, v0

    move-object/from16 v17, v4

    move-object/from16 v18, v3

    move-object/from16 v19, v2

    move-object/from16 v21, v1

    invoke-direct/range {v16 .. v21}, Lf/h/c/n/c;-><init>(Lf/h/c/n/d/h;Ljava/util/concurrent/ExecutorService;Lf/h/c/n/d/s/d;ZLf/h/c/n/d/k/k0;)V

    invoke-static {v3, v0}, Lf/h/a/f/f/n/g;->f(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    new-instance v0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;

    invoke-direct {v0, v1}, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;-><init>(Lf/h/c/n/d/k/k0;)V

    :goto_a
    return-object v0

    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
