.class public Lf/h/c/n/d/h;
.super Ljava/lang/Object;
.source "Onboarding.java"


# instance fields
.field public final a:Lf/h/c/n/d/n/c;

.field public final b:Lf/h/c/c;

.field public final c:Landroid/content/Context;

.field public d:Landroid/content/pm/PackageManager;

.field public e:Ljava/lang/String;

.field public f:Landroid/content/pm/PackageInfo;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Lf/h/c/n/d/k/w0;

.field public m:Lf/h/c/n/d/k/q0;


# direct methods
.method public constructor <init>(Lf/h/c/c;Landroid/content/Context;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/k/q0;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/c/n/d/n/c;

    invoke-direct {v0}, Lf/h/c/n/d/n/c;-><init>()V

    iput-object v0, p0, Lf/h/c/n/d/h;->a:Lf/h/c/n/d/n/c;

    iput-object p1, p0, Lf/h/c/n/d/h;->b:Lf/h/c/c;

    iput-object p2, p0, Lf/h/c/n/d/h;->c:Landroid/content/Context;

    iput-object p3, p0, Lf/h/c/n/d/h;->l:Lf/h/c/n/d/k/w0;

    iput-object p4, p0, Lf/h/c/n/d/h;->m:Lf/h/c/n/d/k/q0;

    return-void
.end method

.method public static a(Lf/h/c/n/d/h;Lf/h/c/n/d/s/i/b;Ljava/lang/String;Lf/h/c/n/d/s/d;Ljava/util/concurrent/Executor;Z)V
    .locals 5

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    sget-object v1, Lf/h/c/n/d/s/c;->e:Lf/h/c/n/d/s/c;

    iget-object v2, p1, Lf/h/c/n/d/s/i/b;->a:Ljava/lang/String;

    const-string v3, "new"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "17.3.0"

    if-eqz v2, :cond_1

    iget-object v2, p1, Lf/h/c/n/d/s/i/b;->e:Ljava/lang/String;

    invoke-virtual {p0, v2, p2}, Lf/h/c/n/d/h;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/s/i/a;

    move-result-object p2

    new-instance v2, Lf/h/c/n/d/s/j/b;

    invoke-virtual {p0}, Lf/h/c/n/d/h;->c()Ljava/lang/String;

    move-result-object v4

    iget-object p1, p1, Lf/h/c/n/d/s/i/b;->b:Ljava/lang/String;

    iget-object p0, p0, Lf/h/c/n/d/h;->a:Lf/h/c/n/d/n/c;

    invoke-direct {v2, v4, p1, p0, v3}, Lf/h/c/n/d/s/j/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Ljava/lang/String;)V

    invoke-virtual {v2, p2, p5}, Lf/h/c/n/d/s/j/a;->d(Lf/h/c/n/d/s/i/a;Z)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p3, v1, p4}, Lf/h/c/n/d/s/d;->d(Lf/h/c/n/d/s/c;Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    const/4 p1, 0x6

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->a(I)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "FirebaseCrashlytics"

    const-string p2, "Failed to create app with Crashlytics service."

    invoke-static {p1, p2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    iget-object v2, p1, Lf/h/c/n/d/s/i/b;->a:Ljava/lang/String;

    const-string v4, "configured"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p3, v1, p4}, Lf/h/c/n/d/s/d;->d(Lf/h/c/n/d/s/c;Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;

    goto :goto_0

    :cond_2
    iget-boolean p3, p1, Lf/h/c/n/d/s/i/b;->f:Z

    if-eqz p3, :cond_3

    const-string p3, "Server says an update is required - forcing a full App update."

    invoke-virtual {v0, p3}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object p3, p1, Lf/h/c/n/d/s/i/b;->e:Ljava/lang/String;

    invoke-virtual {p0, p3, p2}, Lf/h/c/n/d/h;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/s/i/a;

    move-result-object p2

    new-instance p3, Lf/h/c/n/d/s/j/e;

    invoke-virtual {p0}, Lf/h/c/n/d/h;->c()Ljava/lang/String;

    move-result-object p4

    iget-object p1, p1, Lf/h/c/n/d/s/i/b;->b:Ljava/lang/String;

    iget-object p0, p0, Lf/h/c/n/d/h;->a:Lf/h/c/n/d/n/c;

    invoke-direct {p3, p4, p1, p0, v3}, Lf/h/c/n/d/s/j/e;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Ljava/lang/String;)V

    invoke-virtual {p3, p2, p5}, Lf/h/c/n/d/s/j/a;->d(Lf/h/c/n/d/s/i/a;Z)Z

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/s/i/a;
    .locals 14

    move-object v0, p0

    iget-object v1, v0, Lf/h/c/n/d/h;->c:Landroid/content/Context;

    invoke-static {v1}, Lf/h/c/n/d/k/h;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object p2, v2, v1

    const/4 v1, 0x2

    iget-object v3, v0, Lf/h/c/n/d/h;->h:Ljava/lang/String;

    aput-object v3, v2, v1

    const/4 v1, 0x3

    iget-object v3, v0, Lf/h/c/n/d/h;->g:Ljava/lang/String;

    aput-object v3, v2, v1

    invoke-static {v2}, Lf/h/c/n/d/k/h;->f([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v1, v0, Lf/h/c/n/d/h;->i:Ljava/lang/String;

    invoke-static {v1}, Lf/h/c/n/d/k/s0;->f(Ljava/lang/String;)Lf/h/c/n/d/k/s0;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/c/n/d/k/s0;->g()I

    move-result v11

    iget-object v1, v0, Lf/h/c/n/d/h;->l:Lf/h/c/n/d/k/w0;

    iget-object v6, v1, Lf/h/c/n/d/k/w0;->c:Ljava/lang/String;

    new-instance v1, Lf/h/c/n/d/s/i/a;

    iget-object v7, v0, Lf/h/c/n/d/h;->h:Ljava/lang/String;

    iget-object v8, v0, Lf/h/c/n/d/h;->g:Ljava/lang/String;

    iget-object v10, v0, Lf/h/c/n/d/h;->j:Ljava/lang/String;

    iget-object v12, v0, Lf/h/c/n/d/h;->k:Ljava/lang/String;

    const-string v13, "0"

    move-object v3, v1

    move-object v4, p1

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v13}, Lf/h/c/n/d/s/i/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public c()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lf/h/c/n/d/h;->c:Landroid/content/Context;

    const-string v1, "com.crashlytics.ApiEndpoint"

    const-string v2, "string"

    invoke-static {v0, v1, v2}, Lf/h/c/n/d/k/h;->n(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method
