.class public Lf/h/c/n/d/l/b;
.super Ljava/lang/Object;
.source "LogFileManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/n/d/l/b$c;,
        Lf/h/c/n/d/l/b$b;
    }
.end annotation


# static fields
.field public static final d:Lf/h/c/n/d/l/b$c;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/c/n/d/l/b$b;

.field public c:Lf/h/c/n/d/l/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/n/d/l/b$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/h/c/n/d/l/b$c;-><init>(Lf/h/c/n/d/l/b$a;)V

    sput-object v0, Lf/h/c/n/d/l/b;->d:Lf/h/c/n/d/l/b$c;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lf/h/c/n/d/l/b$b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/l/b;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/h/c/n/d/l/b;->b:Lf/h/c/n/d/l/b$b;

    sget-object p1, Lf/h/c/n/d/l/b;->d:Lf/h/c/n/d/l/b$c;

    iput-object p1, p0, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/h/c/n/d/l/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lf/h/c/n/d/l/b$b;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/l/b;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/h/c/n/d/l/b;->b:Lf/h/c/n/d/l/b$b;

    sget-object p1, Lf/h/c/n/d/l/b;->d:Lf/h/c/n/d/l/b$c;

    iput-object p1, p0, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    invoke-virtual {p0, p3}, Lf/h/c/n/d/l/b;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    invoke-interface {v0}, Lf/h/c/n/d/l/a;->a()V

    sget-object v0, Lf/h/c/n/d/l/b;->d:Lf/h/c/n/d/l/b$c;

    iput-object v0, p0, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/c/n/d/l/b;->a:Landroid/content/Context;

    const/4 v1, 0x1

    const-string v2, "com.crashlytics.CollectCustomLogs"

    invoke-static {v0, v2, v1}, Lf/h/c/n/d/k/h;->j(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object p1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v0, "Preferences requested no custom logs. Aborting log file creation."

    invoke-virtual {p1, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "crashlytics-userlog-"

    const-string v1, ".temp"

    invoke-static {v0, p1, v1}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lf/h/c/n/d/l/b;->b:Lf/h/c/n/d/l/b$b;

    check-cast v1, Lf/h/c/n/d/k/x$j;

    invoke-virtual {v1}, Lf/h/c/n/d/k/x$j;->a()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/high16 p1, 0x10000

    new-instance v1, Lf/h/c/n/d/l/e;

    invoke-direct {v1, v0, p1}, Lf/h/c/n/d/l/e;-><init>(Ljava/io/File;I)V

    iput-object v1, p0, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    return-void
.end method
