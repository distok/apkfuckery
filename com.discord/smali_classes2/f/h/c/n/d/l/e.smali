.class public Lf/h/c/n/d/l/e;
.super Ljava/lang/Object;
.source "QueueFileLogStore.java"

# interfaces
.implements Lf/h/c/n/d/l/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/n/d/l/e$a;
    }
.end annotation


# static fields
.field public static final d:Ljava/nio/charset/Charset;


# instance fields
.field public final a:Ljava/io/File;

.field public final b:I

.field public c:Lf/h/c/n/d/l/c;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lf/h/c/n/d/l/e;->d:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/l/e;->a:Ljava/io/File;

    iput p2, p0, Lf/h/c/n/d/l/e;->b:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    const-string v1, "There was a problem closing the Crashlytics log file."

    invoke-static {v0, v1}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lf/h/c/n/d/l/e;->c()[B

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/String;

    sget-object v2, Lf/h/c/n/d/l/e;->d:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public c()[B
    .locals 7

    iget-object v0, p0, Lf/h/c/n/d/l/e;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    :goto_0
    move-object v4, v1

    goto :goto_2

    :cond_0
    invoke-virtual {p0}, Lf/h/c/n/d/l/e;->f()V

    iget-object v0, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x1

    new-array v3, v3, [I

    aput v2, v3, v2

    invoke-virtual {v0}, Lf/h/c/n/d/l/c;->q()I

    move-result v0

    new-array v0, v0, [B

    :try_start_0
    iget-object v4, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    new-instance v5, Lf/h/c/n/d/l/d;

    invoke-direct {v5, p0, v0, v3}, Lf/h/c/n/d/l/d;-><init>(Lf/h/c/n/d/l/e;[B[I)V

    invoke-virtual {v4, v5}, Lf/h/c/n/d/l/c;->e(Lf/h/c/n/d/l/c$d;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    sget-object v5, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Lf/h/c/n/d/b;->a(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "FirebaseCrashlytics"

    const-string v6, "A problem occurred while reading the Crashlytics log file."

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_1
    new-instance v4, Lf/h/c/n/d/l/e$a;

    aget v3, v3, v2

    invoke-direct {v4, p0, v0, v3}, Lf/h/c/n/d/l/e$a;-><init>(Lf/h/c/n/d/l/e;[BI)V

    :goto_2
    if-nez v4, :cond_3

    return-object v1

    :cond_3
    iget v0, v4, Lf/h/c/n/d/l/e$a;->b:I

    new-array v1, v0, [B

    iget-object v3, v4, Lf/h/c/n/d/l/e$a;->a:[B

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    const-string v1, "There was a problem closing the Crashlytics log file."

    invoke-static {v0, v1}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    iget-object v0, p0, Lf/h/c/n/d/l/e;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method public e(JLjava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lf/h/c/n/d/l/e;->f()V

    const-string v0, " "

    iget-object v1, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    if-nez p3, :cond_1

    const-string p3, "null"

    :cond_1
    :try_start_0
    iget v1, p0, Lf/h/c/n/d/l/e;->b:I

    div-int/lit8 v1, v1, 0x4

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v1, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-virtual {p3, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_2
    const-string v1, "\r"

    invoke-virtual {p3, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "\n"

    invoke-virtual {p3, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%d %s%n"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object p3, v2, p1

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lf/h/c/n/d/l/e;->d:Ljava/nio/charset/Charset;

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    iget-object p2, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    invoke-virtual {p2, p1}, Lf/h/c/n/d/l/c;->b([B)V

    :goto_0
    iget-object p1, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    invoke-virtual {p1}, Lf/h/c/n/d/l/c;->f()Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    invoke-virtual {p1}, Lf/h/c/n/d/l/c;->q()I

    move-result p1

    iget p2, p0, Lf/h/c/n/d/l/e;->b:I

    if-le p1, p2, :cond_3

    iget-object p1, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    invoke-virtual {p1}, Lf/h/c/n/d/l/c;->m()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object p2, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 p3, 0x6

    invoke-virtual {p2, p3}, Lf/h/c/n/d/b;->a(I)Z

    move-result p2

    if-eqz p2, :cond_3

    const-string p2, "FirebaseCrashlytics"

    const-string p3, "There was a problem writing to the Crashlytics log."

    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_1
    return-void
.end method

.method public final f()V
    .locals 4

    iget-object v0, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;

    if-nez v0, :cond_0

    :try_start_0
    new-instance v0, Lf/h/c/n/d/l/c;

    iget-object v1, p0, Lf/h/c/n/d/l/e;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Lf/h/c/n/d/l/c;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lf/h/c/n/d/l/e;->c:Lf/h/c/n/d/l/c;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v2, "Could not open log file: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lf/h/c/n/d/l/e;->a:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lf/h/c/n/d/b;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method
