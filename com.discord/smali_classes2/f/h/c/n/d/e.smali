.class public Lf/h/c/n/d/e;
.super Ljava/lang/Object;
.source "Onboarding.java"

# interfaces
.implements Lf/h/a/f/p/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/f<",
        "Lf/h/c/n/d/s/i/b;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lf/h/c/n/d/s/d;

.field public final synthetic c:Ljava/util/concurrent/Executor;

.field public final synthetic d:Lf/h/c/n/d/h;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/h;Ljava/lang/String;Lf/h/c/n/d/s/d;Ljava/util/concurrent/Executor;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/e;->d:Lf/h/c/n/d/h;

    iput-object p2, p0, Lf/h/c/n/d/e;->a:Ljava/lang/String;

    iput-object p3, p0, Lf/h/c/n/d/e;->b:Lf/h/c/n/d/s/d;

    iput-object p4, p0, Lf/h/c/n/d/e;->c:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object v1, p1

    check-cast v1, Lf/h/c/n/d/s/i/b;

    :try_start_0
    iget-object v0, p0, Lf/h/c/n/d/e;->d:Lf/h/c/n/d/h;

    iget-object v2, p0, Lf/h/c/n/d/e;->a:Ljava/lang/String;

    iget-object v3, p0, Lf/h/c/n/d/e;->b:Lf/h/c/n/d/s/d;

    iget-object v4, p0, Lf/h/c/n/d/e;->c:Ljava/util/concurrent/Executor;

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lf/h/c/n/d/h;->a(Lf/h/c/n/d/h;Lf/h/c/n/d/s/i/b;Ljava/lang/String;Lf/h/c/n/d/s/d;Ljava/util/concurrent/Executor;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "FirebaseCrashlytics"

    const-string v1, "Error performing auto configuration."

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    throw p1
.end method
