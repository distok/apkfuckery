.class public Lf/h/c/n/d/i/c;
.super Ljava/lang/Object;
.source "BlockingAnalyticsEventLogger.java"

# interfaces
.implements Lf/h/c/n/d/i/b;
.implements Lf/h/c/n/d/i/a;


# instance fields
.field public final a:Lf/h/c/n/d/i/e;

.field public final b:Ljava/lang/Object;

.field public c:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/i/e;ILjava/util/concurrent/TimeUnit;)V
    .locals 0
    .param p1    # Lf/h/c/n/d/i/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p2, Ljava/lang/Object;

    invoke-direct {p2}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/c/n/d/i/c;->b:Ljava/lang/Object;

    iput-object p1, p0, Lf/h/c/n/d/i/c;->a:Lf/h/c/n/d/i/e;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v1, p0, Lf/h/c/n/d/i/c;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v2, "Logging Crashlytics event to Firebase"

    invoke-virtual {v0, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v2, p0, Lf/h/c/n/d/i/c;->c:Ljava/util/concurrent/CountDownLatch;

    iget-object v2, p0, Lf/h/c/n/d/i/c;->a:Lf/h/c/n/d/i/e;

    iget-object v2, v2, Lf/h/c/n/d/i/e;->a:Lf/h/c/k/a/a;

    const-string v3, "clx"

    invoke-interface {v2, v3, p1, p2}, Lf/h/c/k/a/a;->c(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string p1, "Awaiting app exception callback from FA..."

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object p1, p0, Lf/h/c/n/d/i/c;->c:Ljava/util/concurrent/CountDownLatch;

    const/16 p2, 0x1f4

    int-to-long v2, p2

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v2, v3, p2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "App exception callback received from FA listener."

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string p1, "Timeout exceeded while awaiting app exception callback from FA listener."

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    const-string p1, "Interrupted while awaiting app exception callback from FA listener."

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :goto_0
    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/c/n/d/i/c;->c:Ljava/util/concurrent/CountDownLatch;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p2, p0, Lf/h/c/n/d/i/c;->c:Ljava/util/concurrent/CountDownLatch;

    if-nez p2, :cond_0

    return-void

    :cond_0
    const-string v0, "_ae"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_1
    return-void
.end method
