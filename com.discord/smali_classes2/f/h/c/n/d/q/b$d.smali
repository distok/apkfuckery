.class public Lf/h/c/n/d/q/b$d;
.super Lf/h/c/n/d/k/d;
.source "ReportUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/q/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "d"
.end annotation


# instance fields
.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/c/n/d/q/c/c;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z

.field public final f:F

.field public final synthetic g:Lf/h/c/n/d/q/b;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/q/b;Ljava/util/List;ZF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/c/n/d/q/c/c;",
            ">;ZF)V"
        }
    .end annotation

    iput-object p1, p0, Lf/h/c/n/d/q/b$d;->g:Lf/h/c/n/d/q/b;

    invoke-direct {p0}, Lf/h/c/n/d/k/d;-><init>()V

    iput-object p2, p0, Lf/h/c/n/d/q/b$d;->d:Ljava/util/List;

    iput-boolean p3, p0, Lf/h/c/n/d/q/b$d;->e:Z

    iput p4, p0, Lf/h/c/n/d/q/b$d;->f:F

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lf/h/c/n/d/q/b$d;->d:Ljava/util/List;

    iget-boolean v1, p0, Lf/h/c/n/d/q/b$d;->e:Z

    invoke-virtual {p0, v0, v1}, Lf/h/c/n/d/q/b$d;->b(Ljava/util/List;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "FirebaseCrashlytics"

    const-string v2, "An unexpected error occurred while attempting to upload crash reports."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/h/c/n/d/q/b$d;->g:Lf/h/c/n/d/q/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/c/n/d/q/b;->g:Ljava/lang/Thread;

    return-void
.end method

.method public final b(Ljava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/c/n/d/q/c/c;",
            ">;Z)V"
        }
    .end annotation

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v1, "Starting report processing in "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/h/c/n/d/q/b$d;->f:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, " second(s)..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget v1, p0, Lf/h/c/n/d/q/b$d;->f:F

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float v1, v1, v2

    float-to-long v1, v1

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    return-void

    :cond_0
    :goto_0
    iget-object v1, p0, Lf/h/c/n/d/q/b$d;->g:Lf/h/c/n/d/q/b;

    iget-object v1, v1, Lf/h/c/n/d/q/b;->f:Lf/h/c/n/d/q/b$a;

    check-cast v1, Lf/h/c/n/d/k/x$l;

    iget-object v1, v1, Lf/h/c/n/d/k/x$l;->a:Lf/h/c/n/d/k/x;

    invoke-virtual {v1}, Lf/h/c/n/d/k/x;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    iget-object v2, p0, Lf/h/c/n/d/q/b$d;->g:Lf/h/c/n/d/q/b;

    iget-object v2, v2, Lf/h/c/n/d/q/b;->f:Lf/h/c/n/d/q/b$a;

    check-cast v2, Lf/h/c/n/d/k/x$l;

    iget-object v2, v2, Lf/h/c/n/d/k/x$l;->a:Lf/h/c/n/d/k/x;

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    return-void

    :cond_2
    const-string v2, "Attempting to send "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " report(s)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/c/n/d/q/c/c;

    iget-object v4, p0, Lf/h/c/n/d/q/b$d;->g:Lf/h/c/n/d/q/b;

    invoke-virtual {v4, v3, p2}, Lf/h/c/n/d/q/b;->a(Lf/h/c/n/d/q/c/c;Z)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_5

    sget-object p1, Lf/h/c/n/d/q/b;->h:[S

    add-int/lit8 v3, v1, 0x1

    array-length v4, p1

    add-int/lit8 v4, v4, -0x1

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    aget-short p1, p1, v1

    int-to-long v4, p1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Report submission: scheduling delayed retry in "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " seconds"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const-wide/16 v6, 0x3e8

    mul-long v4, v4, v6

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move v1, v3

    goto :goto_3

    :catch_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    return-void

    :cond_5
    :goto_3
    move-object p1, v2

    goto/16 :goto_1

    :cond_6
    return-void
.end method
