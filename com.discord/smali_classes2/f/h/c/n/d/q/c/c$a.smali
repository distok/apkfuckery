.class public final enum Lf/h/c/n/d/q/c/c$a;
.super Ljava/lang/Enum;
.source "Report.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/q/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/n/d/q/c/c$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/n/d/q/c/c$a;

.field public static final enum e:Lf/h/c/n/d/q/c/c$a;

.field public static final synthetic f:[Lf/h/c/n/d/q/c/c$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/h/c/n/d/q/c/c$a;

    const-string v1, "JAVA"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/c/n/d/q/c/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/c/n/d/q/c/c$a;->d:Lf/h/c/n/d/q/c/c$a;

    new-instance v1, Lf/h/c/n/d/q/c/c$a;

    const-string v3, "NATIVE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/c/n/d/q/c/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/c/n/d/q/c/c$a;->e:Lf/h/c/n/d/q/c/c$a;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/h/c/n/d/q/c/c$a;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/h/c/n/d/q/c/c$a;->f:[Lf/h/c/n/d/q/c/c$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/n/d/q/c/c$a;
    .locals 1

    const-class v0, Lf/h/c/n/d/q/c/c$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/n/d/q/c/c$a;

    return-object p0
.end method

.method public static values()[Lf/h/c/n/d/q/c/c$a;
    .locals 1

    sget-object v0, Lf/h/c/n/d/q/c/c$a;->f:[Lf/h/c/n/d/q/c/c$a;

    invoke-virtual {v0}, [Lf/h/c/n/d/q/c/c$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/n/d/q/c/c$a;

    return-object v0
.end method
