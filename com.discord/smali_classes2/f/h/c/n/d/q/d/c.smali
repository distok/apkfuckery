.class public Lf/h/c/n/d/q/d/c;
.super Lf/h/c/n/d/k/a;
.source "DefaultCreateReportSpiCall.java"

# interfaces
.implements Lf/h/c/n/d/q/d/b;


# instance fields
.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lf/h/c/n/d/n/a;->e:Lf/h/c/n/d/n/a;

    invoke-direct {p0, p1, p2, p3, v0}, Lf/h/c/n/d/k/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Lf/h/c/n/d/n/a;)V

    iput-object p4, p0, Lf/h/c/n/d/q/d/c;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/n/d/q/c/a;Z)Z
    .locals 11

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lf/h/c/n/d/k/a;->b()Lf/h/c/n/d/n/b;

    move-result-object p2

    iget-object v1, p1, Lf/h/c/n/d/q/c/a;->b:Ljava/lang/String;

    iget-object v2, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v3, "X-CRASHLYTICS-GOOGLE-APP-ID"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v2, "X-CRASHLYTICS-API-CLIENT-TYPE"

    const-string v3, "android"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lf/h/c/n/d/q/d/c;->f:Ljava/lang/String;

    iget-object v2, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v3, "X-CRASHLYTICS-API-CLIENT-VERSION"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/n/d/q/c/a;->c:Lf/h/c/n/d/q/c/c;

    invoke-interface {v1}, Lf/h/c/n/d/q/c/c;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v4, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lf/h/c/n/d/q/c/a;->c:Lf/h/c/n/d/q/c/c;

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "report[identifier]"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->d()[Ljava/io/File;

    move-result-object v1

    array-length v1, v1

    const-string v2, "application/octet-stream"

    const-string v3, " to report "

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    const-string v1, "Adding single file "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->c()Ljava/io/File;

    move-result-object p1

    const-string v3, "report[file]"

    invoke-virtual {p2, v3, v1, v2, p1}, Lf/h/c/n/d/n/b;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lf/h/c/n/d/n/b;

    goto :goto_2

    :cond_1
    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->d()[Ljava/io/File;

    move-result-object v1

    array-length v5, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_1
    if-ge v6, v5, :cond_2

    aget-object v8, v1, v6

    const-string v9, "Adding file "

    invoke-static {v9}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "report[file"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, v9, v10, v2, v8}, Lf/h/c/n/d/n/b;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lf/h/c/n/d/n/b;

    add-int/2addr v7, v4

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    const-string p1, "Sending report to: "

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object v1, p0, Lf/h/c/n/d/k/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2}, Lf/h/c/n/d/n/b;->a()Lf/h/c/n/d/n/d;

    move-result-object p1

    iget p2, p1, Lf/h/c/n/d/n/d;->a:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Create report request ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "X-REQUEST-ID"

    iget-object p1, p1, Lf/h/c/n/d/n/d;->c:Lokhttp3/Headers;

    invoke-virtual {p1, v2}, Lokhttp3/Headers;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Result was: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-static {p2}, Lf/h/a/f/f/n/g;->W(I)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_3

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    :goto_3
    return v4

    :catch_0
    move-exception p1

    const/4 p2, 0x6

    invoke-virtual {v0, p2}, Lf/h/c/n/d/b;->a(I)Z

    move-result p2

    if-eqz p2, :cond_4

    const-string p2, "FirebaseCrashlytics"

    const-string v0, "Create report HTTP request failed."

    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_5
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "An invalid data collection token was used."

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
