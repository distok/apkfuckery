.class public Lf/h/c/n/d/q/b;
.super Ljava/lang/Object;
.source "ReportUploader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/n/d/q/b$d;,
        Lf/h/c/n/d/q/b$c;,
        Lf/h/c/n/d/q/b$b;,
        Lf/h/c/n/d/q/b$a;
    }
.end annotation


# static fields
.field public static final h:[S


# instance fields
.field public final a:Lf/h/c/n/d/q/d/b;

.field public final b:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Lf/h/c/n/d/k/r0;

.field public final e:Lf/h/c/n/d/q/a;

.field public final f:Lf/h/c/n/d/q/b$a;

.field public g:Ljava/lang/Thread;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    sput-object v0, Lf/h/c/n/d/q/b;->h:[S

    return-void

    nop

    :array_0
    .array-data 2
        0xas
        0x14s
        0x1es
        0x3cs
        0x78s
        0x12cs
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/r0;Lf/h/c/n/d/q/a;Lf/h/c/n/d/q/d/b;Lf/h/c/n/d/q/b$a;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lf/h/c/n/d/q/b;->a:Lf/h/c/n/d/q/d/b;

    iput-object p1, p0, Lf/h/c/n/d/q/b;->b:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/q/b;->c:Ljava/lang/String;

    iput-object p3, p0, Lf/h/c/n/d/q/b;->d:Lf/h/c/n/d/k/r0;

    iput-object p4, p0, Lf/h/c/n/d/q/b;->e:Lf/h/c/n/d/q/a;

    iput-object p6, p0, Lf/h/c/n/d/q/b;->f:Lf/h/c/n/d/q/b$a;

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/n/d/q/c/c;Z)Z
    .locals 6

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 v1, 0x1

    :try_start_0
    new-instance v2, Lf/h/c/n/d/q/c/a;

    iget-object v3, p0, Lf/h/c/n/d/q/b;->b:Ljava/lang/String;

    iget-object v4, p0, Lf/h/c/n/d/q/b;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v4, p1}, Lf/h/c/n/d/q/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/q/c/c;)V

    iget-object v3, p0, Lf/h/c/n/d/q/b;->d:Lf/h/c/n/d/k/r0;

    sget-object v4, Lf/h/c/n/d/k/r0;->f:Lf/h/c/n/d/k/r0;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "Report configured to be sent via DataTransport."

    if-ne v3, v4, :cond_0

    :try_start_1
    invoke-virtual {v0, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object v4, Lf/h/c/n/d/k/r0;->e:Lf/h/c/n/d/k/r0;

    if-ne v3, v4, :cond_1

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->getType()Lf/h/c/n/d/q/c/c$a;

    move-result-object v3

    sget-object v4, Lf/h/c/n/d/q/c/c$a;->d:Lf/h/c/n/d/q/c/c$a;

    if-ne v3, v4, :cond_1

    invoke-virtual {v0, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :goto_0
    const/4 p2, 0x1

    goto :goto_2

    :cond_1
    iget-object v3, p0, Lf/h/c/n/d/q/b;->a:Lf/h/c/n/d/q/d/b;

    invoke-interface {v3, v2, p2}, Lf/h/c/n/d/q/d/b;->a(Lf/h/c/n/d/q/c/a;Z)Z

    move-result p2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Crashlytics Reports Endpoint upload "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_2

    const-string v3, "complete: "

    goto :goto_1

    :cond_2
    const-string v3, "FAILED: "

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lf/h/c/n/d/b;->f(Ljava/lang/String;)V

    :goto_2
    if-eqz p2, :cond_3

    iget-object p2, p0, Lf/h/c/n/d/q/b;->e:Lf/h/c/n/d/q/a;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lf/h/c/n/d/q/c/c;->remove()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception p2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error occurred sending report "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lf/h/c/n/d/b;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    const/4 v1, 0x0

    :goto_3
    return v1
.end method
