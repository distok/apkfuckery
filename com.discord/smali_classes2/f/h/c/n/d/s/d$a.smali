.class public Lf/h/c/n/d/s/d$a;
.super Ljava/lang/Object;
.source "SettingsController.java"

# interfaces
.implements Lf/h/a/f/p/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/h/c/n/d/s/d;->d(Lf/h/c/n/d/s/c;Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/f<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/h/c/n/d/s/d;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/s/d;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Ljava/lang/Void;

    const-string p1, "FirebaseCrashlytics"

    iget-object v0, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    iget-object v1, v0, Lf/h/c/n/d/s/d;->f:Lf/h/c/n/d/s/j/d;

    iget-object v0, v0, Lf/h/c/n/d/s/d;->b:Lf/h/c/n/d/s/i/g;

    check-cast v1, Lf/h/c/n/d/s/j/c;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x6

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Lf/h/c/n/d/s/j/c;->f(Lf/h/c/n/d/s/i/g;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v1, v4}, Lf/h/c/n/d/k/a;->c(Ljava/util/Map;)Lf/h/c/n/d/n/b;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Lf/h/c/n/d/s/j/c;->d(Lf/h/c/n/d/n/b;Lf/h/c/n/d/s/i/g;)Lf/h/c/n/d/n/b;

    iget-object v0, v1, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Requesting settings from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lf/h/c/n/d/k/a;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, v1, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Settings query params were: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-virtual {v5}, Lf/h/c/n/d/n/b;->a()Lf/h/c/n/d/n/d;

    move-result-object v0

    iget-object v4, v1, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings request ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "X-REQUEST-ID"

    iget-object v7, v0, Lf/h/c/n/d/n/d;->c:Lokhttp3/Headers;

    invoke-virtual {v7, v6}, Lokhttp3/Headers;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lf/h/c/n/d/s/j/c;->g(Lf/h/c/n/d/n/d;)Lorg/json/JSONObject;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, v1, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Settings request failed."

    invoke-static {p1, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v3

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    iget-object v1, v1, Lf/h/c/n/d/s/d;->c:Lf/h/c/n/d/s/f;

    invoke-virtual {v1, v0}, Lf/h/c/n/d/s/f;->a(Lorg/json/JSONObject;)Lf/h/c/n/d/s/i/f;

    move-result-object v1

    iget-object v4, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    iget-object v4, v4, Lf/h/c/n/d/s/d;->e:Lf/h/c/n/d/s/a;

    iget-wide v5, v1, Lf/h/c/n/d/s/i/f;->d:J

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v7, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v8, "Failed to close settings writer."

    const-string v9, "Writing settings to cache file..."

    invoke-virtual {v7, v9}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :try_start_1
    const-string v9, "expires_at"

    invoke-virtual {v0, v9, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    new-instance v5, Ljava/io/FileWriter;

    new-instance v6, Ljava/io/File;

    new-instance v9, Lf/h/c/n/d/o/h;

    iget-object v4, v4, Lf/h/c/n/d/s/a;->a:Landroid/content/Context;

    invoke-direct {v9, v4}, Lf/h/c/n/d/o/h;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9}, Lf/h/c/n/d/o/h;->a()Ljava/io/File;

    move-result-object v4

    const-string v9, "com.crashlytics.settings.json"

    invoke-direct {v6, v4, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/FileWriter;->flush()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception v4

    goto :goto_1

    :catchall_1
    move-exception p1

    goto :goto_3

    :catch_2
    move-exception v4

    move-object v5, v3

    :goto_1
    :try_start_3
    const-string v6, "Failed to cache settings"

    invoke-virtual {v7, v2}, Lf/h/c/n/d/b;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_1
    :goto_2
    invoke-static {v5, v8}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    iget-object p1, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    const-string v2, "Loaded settings: "

    invoke-virtual {p1, v0, v2}, Lf/h/c/n/d/s/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)V

    iget-object p1, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    iget-object v0, p1, Lf/h/c/n/d/s/d;->b:Lf/h/c/n/d/s/i/g;

    iget-object v0, v0, Lf/h/c/n/d/s/i/g;->f:Ljava/lang/String;

    iget-object p1, p1, Lf/h/c/n/d/s/d;->a:Landroid/content/Context;

    invoke-static {p1}, Lf/h/c/n/d/k/h;->o(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v2, "existing_instance_identifier"

    invoke-interface {p1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object p1, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    iget-object p1, p1, Lf/h/c/n/d/s/d;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object p1, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    iget-object p1, p1, Lf/h/c/n/d/s/d;->i:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, v1, Lf/h/c/n/d/s/i/f;->a:Lf/h/c/n/d/s/i/b;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    new-instance p1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iget-object v0, v1, Lf/h/c/n/d/s/i/f;->a:Lf/h/c/n/d/s/i/b;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    iget-object v0, p0, Lf/h/c/n/d/s/d$a;->a:Lf/h/c/n/d/s/d;

    iget-object v0, v0, Lf/h/c/n/d/s/d;->i:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto :goto_5

    :catchall_2
    move-exception p1

    move-object v3, v5

    :goto_3
    move-object v5, v3

    :goto_4
    invoke-static {v5, v8}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_5
    invoke-static {v3}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method
