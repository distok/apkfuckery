.class public Lf/h/c/n/d/s/d;
.super Ljava/lang/Object;
.source "SettingsController.java"

# interfaces
.implements Lf/h/c/n/d/s/e;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/c/n/d/s/i/g;

.field public final c:Lf/h/c/n/d/s/f;

.field public final d:Lf/h/c/n/d/k/f1;

.field public final e:Lf/h/c/n/d/s/a;

.field public final f:Lf/h/c/n/d/s/j/d;

.field public final g:Lf/h/c/n/d/k/q0;

.field public final h:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lf/h/c/n/d/s/i/e;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "Lf/h/c/n/d/s/i/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/c/n/d/s/i/g;Lf/h/c/n/d/k/f1;Lf/h/c/n/d/s/f;Lf/h/c/n/d/s/a;Lf/h/c/n/d/s/j/d;Lf/h/c/n/d/k/q0;)V
    .locals 13

    move-object v0, p0

    move-object/from16 v1, p3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v2, v0, Lf/h/c/n/d/s/d;->h:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v3, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v4, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v4}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    invoke-direct {v3, v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v0, Lf/h/c/n/d/s/d;->i:Ljava/util/concurrent/atomic/AtomicReference;

    move-object v3, p1

    iput-object v3, v0, Lf/h/c/n/d/s/d;->a:Landroid/content/Context;

    move-object v3, p2

    iput-object v3, v0, Lf/h/c/n/d/s/d;->b:Lf/h/c/n/d/s/i/g;

    iput-object v1, v0, Lf/h/c/n/d/s/d;->d:Lf/h/c/n/d/k/f1;

    move-object/from16 v3, p4

    iput-object v3, v0, Lf/h/c/n/d/s/d;->c:Lf/h/c/n/d/s/f;

    move-object/from16 v3, p5

    iput-object v3, v0, Lf/h/c/n/d/s/d;->e:Lf/h/c/n/d/s/a;

    move-object/from16 v3, p6

    iput-object v3, v0, Lf/h/c/n/d/s/d;->f:Lf/h/c/n/d/s/j/d;

    move-object/from16 v3, p7

    iput-object v3, v0, Lf/h/c/n/d/s/d;->g:Lf/h/c/n/d/k/q0;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "max_custom_exception_events"

    const/16 v5, 0x8

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    new-instance v9, Lf/h/c/n/d/s/i/d;

    const/4 v5, 0x4

    invoke-direct {v9, v4, v5}, Lf/h/c/n/d/s/i/d;-><init>(II)V

    const-string v4, "collect_reports"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    new-instance v10, Lf/h/c/n/d/s/i/c;

    invoke-direct {v10, v4}, Lf/h/c/n/d/s/i/c;-><init>(Z)V

    const-wide/16 v4, 0xe10

    invoke-static {v1, v4, v5, v3}, Lf/h/c/n/d/s/b;->b(Lf/h/c/n/d/k/f1;JLorg/json/JSONObject;)J

    move-result-wide v6

    new-instance v1, Lf/h/c/n/d/s/i/f;

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xe10

    move-object v5, v1

    invoke-direct/range {v5 .. v12}, Lf/h/c/n/d/s/i/f;-><init>(JLf/h/c/n/d/s/i/b;Lf/h/c/n/d/s/i/d;Lf/h/c/n/d/s/i/c;II)V

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/tasks/Task;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Lf/h/c/n/d/s/i/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/s/d;->i:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object v0
.end method

.method public final b(Lf/h/c/n/d/s/c;)Lf/h/c/n/d/s/i/f;
    .locals 10

    const-string v0, "FirebaseCrashlytics"

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 v2, 0x6

    const/4 v3, 0x0

    :try_start_0
    sget-object v4, Lf/h/c/n/d/s/c;->e:Lf/h/c/n/d/s/c;

    invoke-virtual {v4, p1}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lf/h/c/n/d/s/d;->e:Lf/h/c/n/d/s/a;

    invoke-virtual {v4}, Lf/h/c/n/d/s/a;->a()Lorg/json/JSONObject;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v5, p0, Lf/h/c/n/d/s/d;->c:Lf/h/c/n/d/s/f;

    invoke-virtual {v5, v4}, Lf/h/c/n/d/s/f;->a(Lorg/json/JSONObject;)Lf/h/c/n/d/s/i/f;

    move-result-object v5

    if-eqz v5, :cond_3

    const-string v6, "Loaded cached settings: "

    invoke-virtual {p0, v4, v6}, Lf/h/c/n/d/s/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)V

    iget-object v4, p0, Lf/h/c/n/d/s/d;->d:Lf/h/c/n/d/k/f1;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-object v4, Lf/h/c/n/d/s/c;->f:Lf/h/c/n/d/s/c;

    invoke-virtual {v4, p1}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-wide v8, v5, Lf/h/c/n/d/s/i/f;->d:J

    cmp-long p1, v8, v6

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    const-string p1, "Cached settings have expired."

    invoke-virtual {v1, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    goto :goto_2

    :cond_2
    :goto_1
    :try_start_1
    const-string p1, "Returning cached settings."

    invoke-virtual {v1, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v5

    goto :goto_3

    :catch_1
    move-exception p1

    move-object v3, v5

    goto :goto_2

    :cond_3
    :try_start_2
    const-string p1, "Failed to parse cached settings data."

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->a(I)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v0, p1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_4
    const-string p1, "No cached settings data found."

    invoke-virtual {v1, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :goto_2
    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Failed to get cached settings"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_5
    :goto_3
    return-object v3
.end method

.method public c()Lf/h/c/n/d/s/i/e;
    .locals 1

    iget-object v0, p0, Lf/h/c/n/d/s/d;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/n/d/s/i/e;

    return-object v0
.end method

.method public d(Lf/h/c/n/d/s/c;Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/n/d/s/c;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/s/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/c/n/d/k/h;->o(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "existing_instance_identifier"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lf/h/c/n/d/s/d;->b:Lf/h/c/n/d/s/i/g;

    iget-object v1, v1, Lf/h/c/n/d/s/i/g;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lf/h/c/n/d/s/d;->b(Lf/h/c/n/d/s/c;)Lf/h/c/n/d/s/i/f;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lf/h/c/n/d/s/d;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object p2, p0, Lf/h/c/n/d/s/d;->i:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object p1, p1, Lf/h/c/n/d/s/i/f;->a:Lf/h/c/n/d/s/i/b;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :cond_0
    sget-object p1, Lf/h/c/n/d/s/c;->f:Lf/h/c/n/d/s/c;

    invoke-virtual {p0, p1}, Lf/h/c/n/d/s/d;->b(Lf/h/c/n/d/s/c;)Lf/h/c/n/d/s/i/f;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lf/h/c/n/d/s/d;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lf/h/c/n/d/s/d;->i:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object p1, p1, Lf/h/c/n/d/s/i/f;->a:Lf/h/c/n/d/s/i/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    :cond_1
    iget-object p1, p0, Lf/h/c/n/d/s/d;->g:Lf/h/c/n/d/k/q0;

    invoke-virtual {p1}, Lf/h/c/n/d/k/q0;->c()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lf/h/c/n/d/s/d$a;

    invoke-direct {v0, p0}, Lf/h/c/n/d/s/d$a;-><init>(Lf/h/c/n/d/s/d;)V

    invoke-virtual {p1, p2, v0}, Lcom/google/android/gms/tasks/Task;->r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final e(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    invoke-static {p2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    return-void
.end method
