.class public Lf/h/c/n/d/s/f;
.super Ljava/lang/Object;
.source "SettingsJsonParser.java"


# instance fields
.field public final a:Lf/h/c/n/d/k/f1;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/f1;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/s/f;->a:Lf/h/c/n/d/k/f1;

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)Lf/h/c/n/d/s/i/f;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "settings_version"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    new-instance v0, Lf/h/c/n/d/s/b;

    invoke-direct {v0}, Lf/h/c/n/d/s/b;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v0, Lf/h/c/n/d/s/h;

    invoke-direct {v0}, Lf/h/c/n/d/s/h;-><init>()V

    :goto_0
    iget-object v1, p0, Lf/h/c/n/d/s/f;->a:Lf/h/c/n/d/k/f1;

    invoke-interface {v0, v1, p1}, Lf/h/c/n/d/s/g;->a(Lf/h/c/n/d/k/f1;Lorg/json/JSONObject;)Lf/h/c/n/d/s/i/f;

    move-result-object p1

    return-object p1
.end method
