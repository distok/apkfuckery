.class public abstract Lf/h/c/n/d/s/j/a;
.super Lf/h/c/n/d/k/a;
.source "AbstractAppSpiCall.java"


# instance fields
.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Lf/h/c/n/d/n/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lf/h/c/n/d/k/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Lf/h/c/n/d/n/a;)V

    iput-object p5, p0, Lf/h/c/n/d/s/j/a;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public d(Lf/h/c/n/d/s/i/a;Z)Z
    .locals 4

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lf/h/c/n/d/k/a;->b()Lf/h/c/n/d/n/b;

    move-result-object p2

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->a:Ljava/lang/String;

    iget-object v2, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v3, "X-CRASHLYTICS-ORG-ID"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->b:Ljava/lang/String;

    iget-object v2, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v3, "X-CRASHLYTICS-GOOGLE-APP-ID"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v2, "X-CRASHLYTICS-API-CLIENT-TYPE"

    const-string v3, "android"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lf/h/c/n/d/s/j/a;->f:Ljava/lang/String;

    iget-object v2, p2, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v3, "X-CRASHLYTICS-API-CLIENT-VERSION"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->a:Ljava/lang/String;

    const-string v2, "org_id"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->c:Ljava/lang/String;

    const-string v2, "app[identifier]"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->g:Ljava/lang/String;

    const-string v2, "app[name]"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->d:Ljava/lang/String;

    const-string v2, "app[display_version]"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->e:Ljava/lang/String;

    const-string v2, "app[build_version]"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    iget v1, p1, Lf/h/c/n/d/s/i/a;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "app[source]"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->i:Ljava/lang/String;

    const-string v2, "app[minimum_sdk_version]"

    invoke-virtual {p2, v2, v1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    const-string v1, "app[built_sdk_version]"

    const-string v2, "0"

    invoke-virtual {p2, v1, v2}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    iget-object v1, p1, Lf/h/c/n/d/s/i/a;->f:Ljava/lang/String;

    invoke-static {v1}, Lf/h/c/n/d/k/h;->s(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object p1, p1, Lf/h/c/n/d/s/i/a;->f:Ljava/lang/String;

    const-string v1, "app[instance_identifier]"

    invoke-virtual {p2, v1, p1}, Lf/h/c/n/d/n/b;->b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;

    :cond_0
    const-string p1, "Sending app info to "

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object v1, p0, Lf/h/c/n/d/k/a;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2}, Lf/h/c/n/d/n/b;->a()Lf/h/c/n/d/n/d;

    move-result-object p1

    iget v1, p1, Lf/h/c/n/d/n/d;->a:I

    const-string v2, "POST"

    iget-object p2, p2, Lf/h/c/n/d/n/b;->a:Lf/h/c/n/d/n/a;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p2, "Create"

    goto :goto_0

    :cond_1
    const-string p2, "Update"

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " app request ID: "

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "X-REQUEST-ID"

    iget-object p1, p1, Lf/h/c/n/d/n/d;->c:Lokhttp3/Headers;

    invoke-virtual {p1, p2}, Lokhttp3/Headers;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Result was "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-static {v1}, Lf/h/a/f/f/n/g;->W(I)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    return p1

    :catch_0
    move-exception p1

    const/4 p2, 0x6

    invoke-virtual {v0, p2}, Lf/h/c/n/d/b;->a(I)Z

    move-result p2

    if-eqz p2, :cond_3

    const-string p2, "FirebaseCrashlytics"

    const-string v0, "HTTP request failed."

    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "An invalid data collection token was used."

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
