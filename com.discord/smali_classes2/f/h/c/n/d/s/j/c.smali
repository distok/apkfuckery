.class public Lf/h/c/n/d/s/j/c;
.super Lf/h/c/n/d/k/a;
.source "DefaultSettingsSpiCall.java"

# interfaces
.implements Lf/h/c/n/d/s/j/d;


# instance fields
.field public f:Lf/h/c/n/d/b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;)V
    .locals 2

    sget-object v0, Lf/h/c/n/d/n/a;->d:Lf/h/c/n/d/n/a;

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    invoke-direct {p0, p1, p2, p3, v0}, Lf/h/c/n/d/k/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Lf/h/c/n/d/n/a;)V

    iput-object v1, p0, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    return-void
.end method


# virtual methods
.method public final d(Lf/h/c/n/d/n/b;Lf/h/c/n/d/s/i/g;)Lf/h/c/n/d/n/b;
    .locals 2

    iget-object v0, p2, Lf/h/c/n/d/s/i/g;->a:Ljava/lang/String;

    const-string v1, "X-CRASHLYTICS-GOOGLE-APP-ID"

    invoke-virtual {p0, p1, v1, v0}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "X-CRASHLYTICS-API-CLIENT-TYPE"

    const-string v1, "android"

    invoke-virtual {p0, p1, v0, v1}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "X-CRASHLYTICS-API-CLIENT-VERSION"

    const-string v1, "17.3.0"

    invoke-virtual {p0, p1, v0, v1}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Accept"

    const-string v1, "application/json"

    invoke-virtual {p0, p1, v0, v1}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p2, Lf/h/c/n/d/s/i/g;->b:Ljava/lang/String;

    const-string v1, "X-CRASHLYTICS-DEVICE-MODEL"

    invoke-virtual {p0, p1, v1, v0}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p2, Lf/h/c/n/d/s/i/g;->c:Ljava/lang/String;

    const-string v1, "X-CRASHLYTICS-OS-BUILD-VERSION"

    invoke-virtual {p0, p1, v1, v0}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p2, Lf/h/c/n/d/s/i/g;->d:Ljava/lang/String;

    const-string v1, "X-CRASHLYTICS-OS-DISPLAY-VERSION"

    invoke-virtual {p0, p1, v1, v0}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p2, Lf/h/c/n/d/s/i/g;->e:Lf/h/c/n/d/k/x0;

    check-cast p2, Lf/h/c/n/d/k/w0;

    invoke-virtual {p2}, Lf/h/c/n/d/k/w0;->b()Ljava/lang/String;

    move-result-object p2

    const-string v0, "X-CRASHLYTICS-INSTALLATION-ID"

    invoke-virtual {p0, p1, v0, p2}, Lf/h/c/n/d/s/j/c;->e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public final e(Lf/h/c/n/d/n/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    if-eqz p3, :cond_0

    iget-object p1, p1, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final f(Lf/h/c/n/d/s/i/g;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/n/d/s/i/g;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p1, Lf/h/c/n/d/s/i/g;->h:Ljava/lang/String;

    const-string v2, "build_version"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/n/d/s/i/g;->g:Ljava/lang/String;

    const-string v2, "display_version"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, p1, Lf/h/c/n/d/s/i/g;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p1, Lf/h/c/n/d/s/i/g;->f:Ljava/lang/String;

    invoke-static {p1}, Lf/h/c/n/d/k/h;->s(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "instance"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public g(Lf/h/c/n/d/n/d;)Lorg/json/JSONObject;
    .locals 5

    iget v0, p1, Lf/h/c/n/d/n/d;->a:I

    iget-object v1, p0, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Settings result was: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_1

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_1

    const/16 v1, 0xca

    if-eq v0, v1, :cond_1

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object p1, p1, Lf/h/c/n/d/n/d;->b:Ljava/lang/String;

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v2, p0, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    const-string v3, "Failed to parse settings JSON from "

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lf/h/c/n/d/k/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Settings response "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lf/h/c/n/d/s/j/c;->f:Lf/h/c/n/d/b;

    const-string v0, "Failed to retrieve settings from "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lf/h/c/n/d/k/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lf/h/c/n/d/b;->d(Ljava/lang/String;)V

    :goto_2
    return-object v1
.end method
