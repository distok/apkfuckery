.class public final Lf/h/c/n/d/m/a$q;
.super Ljava/lang/Object;
.source "AutoCrashlyticsReportEncoder.java"

# interfaces
.implements Lf/h/c/q/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "q"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/c/q/c<",
        "Lf/h/c/n/d/m/v$d$d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/h/c/n/d/m/a$q;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/c/n/d/m/a$q;

    invoke-direct {v0}, Lf/h/c/n/d/m/a$q;-><init>()V

    sput-object v0, Lf/h/c/n/d/m/a$q;->a:Lf/h/c/n/d/m/a$q;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/c/n/d/m/v$d$d;

    check-cast p2, Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->d()J

    move-result-wide v0

    const-string v2, "timestamp"

    invoke-interface {p2, v2, v0, v1}, Lf/h/c/q/d;->b(Ljava/lang/String;J)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "type"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->a()Lf/h/c/n/d/m/v$d$d$a;

    move-result-object v0

    const-string v1, "app"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->b()Lf/h/c/n/d/m/v$d$d$b;

    move-result-object v0

    const-string v1, "device"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->c()Lf/h/c/n/d/m/v$d$d$c;

    move-result-object p1

    const-string v0, "log"

    invoke-interface {p2, v0, p1}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    return-void
.end method
