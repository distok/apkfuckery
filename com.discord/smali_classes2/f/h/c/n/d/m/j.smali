.class public final Lf/h/c/n/d/m/j;
.super Lf/h/c/n/d/m/v$d$d;
.source "AutoValue_CrashlyticsReport_Session_Event.java"


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Lf/h/c/n/d/m/v$d$d$a;

.field public final d:Lf/h/c/n/d/m/v$d$d$b;

.field public final e:Lf/h/c/n/d/m/v$d$d$c;


# direct methods
.method public constructor <init>(JLjava/lang/String;Lf/h/c/n/d/m/v$d$d$a;Lf/h/c/n/d/m/v$d$d$b;Lf/h/c/n/d/m/v$d$d$c;Lf/h/c/n/d/m/j$a;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$d$d;-><init>()V

    iput-wide p1, p0, Lf/h/c/n/d/m/j;->a:J

    iput-object p3, p0, Lf/h/c/n/d/m/j;->b:Ljava/lang/String;

    iput-object p4, p0, Lf/h/c/n/d/m/j;->c:Lf/h/c/n/d/m/v$d$d$a;

    iput-object p5, p0, Lf/h/c/n/d/m/j;->d:Lf/h/c/n/d/m/v$d$d$b;

    iput-object p6, p0, Lf/h/c/n/d/m/j;->e:Lf/h/c/n/d/m/v$d$d$c;

    return-void
.end method


# virtual methods
.method public a()Lf/h/c/n/d/m/v$d$d$a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/m/j;->c:Lf/h/c/n/d/m/v$d$d$a;

    return-object v0
.end method

.method public b()Lf/h/c/n/d/m/v$d$d$b;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/m/j;->d:Lf/h/c/n/d/m/v$d$d$b;

    return-object v0
.end method

.method public c()Lf/h/c/n/d/m/v$d$d$c;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/m/j;->e:Lf/h/c/n/d/m/v$d$d$c;

    return-object v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/n/d/m/j;->a:J

    return-wide v0
.end method

.method public e()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/m/j;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lf/h/c/n/d/m/v$d$d;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    check-cast p1, Lf/h/c/n/d/m/v$d$d;

    iget-wide v3, p0, Lf/h/c/n/d/m/j;->a:J

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->d()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    iget-object v1, p0, Lf/h/c/n/d/m/j;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lf/h/c/n/d/m/j;->c:Lf/h/c/n/d/m/v$d$d$a;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->a()Lf/h/c/n/d/m/v$d$d$a;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lf/h/c/n/d/m/j;->d:Lf/h/c/n/d/m/v$d$d$b;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->b()Lf/h/c/n/d/m/v$d$d$b;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lf/h/c/n/d/m/j;->e:Lf/h/c/n/d/m/v$d$d$c;

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->c()Lf/h/c/n/d/m/v$d$d$c;

    move-result-object p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d;->c()Lf/h/c/n/d/m/v$d$d$c;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    return v2
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lf/h/c/n/d/m/j;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    const v0, 0xf4243

    xor-int/2addr v1, v0

    mul-int v1, v1, v0

    iget-object v2, p0, Lf/h/c/n/d/m/j;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    mul-int v1, v1, v0

    iget-object v2, p0, Lf/h/c/n/d/m/j;->c:Lf/h/c/n/d/m/v$d$d$a;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    mul-int v1, v1, v0

    iget-object v2, p0, Lf/h/c/n/d/m/j;->d:Lf/h/c/n/d/m/v$d$d$b;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    mul-int v1, v1, v0

    iget-object v0, p0, Lf/h/c/n/d/m/j;->e:Lf/h/c/n/d/m/v$d$d$c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Event{timestamp="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lf/h/c/n/d/m/j;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/c/n/d/m/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/c/n/d/m/j;->c:Lf/h/c/n/d/m/v$d$d$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/c/n/d/m/j;->d:Lf/h/c/n/d/m/v$d$d$b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", log="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/c/n/d/m/j;->e:Lf/h/c/n/d/m/v$d$d$c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
