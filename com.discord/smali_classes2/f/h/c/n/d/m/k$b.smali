.class public final Lf/h/c/n/d/m/k$b;
.super Lf/h/c/n/d/m/v$d$d$a$a;
.source "AutoValue_CrashlyticsReport_Session_Event_Application.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public a:Lf/h/c/n/d/m/v$d$d$a$b;

.field public b:Lf/h/c/n/d/m/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/n/d/m/w<",
            "Lf/h/c/n/d/m/v$b;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$d$d$a$a;-><init>()V

    return-void
.end method

.method public constructor <init>(Lf/h/c/n/d/m/v$d$d$a;Lf/h/c/n/d/m/k$a;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$d$d$a$a;-><init>()V

    check-cast p1, Lf/h/c/n/d/m/k;

    iget-object p2, p1, Lf/h/c/n/d/m/k;->a:Lf/h/c/n/d/m/v$d$d$a$b;

    iput-object p2, p0, Lf/h/c/n/d/m/k$b;->a:Lf/h/c/n/d/m/v$d$d$a$b;

    iget-object p2, p1, Lf/h/c/n/d/m/k;->b:Lf/h/c/n/d/m/w;

    iput-object p2, p0, Lf/h/c/n/d/m/k$b;->b:Lf/h/c/n/d/m/w;

    iget-object p2, p1, Lf/h/c/n/d/m/k;->c:Ljava/lang/Boolean;

    iput-object p2, p0, Lf/h/c/n/d/m/k$b;->c:Ljava/lang/Boolean;

    iget p1, p1, Lf/h/c/n/d/m/k;->d:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lf/h/c/n/d/m/k$b;->d:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public a()Lf/h/c/n/d/m/v$d$d$a;
    .locals 8

    iget-object v0, p0, Lf/h/c/n/d/m/k$b;->a:Lf/h/c/n/d/m/v$d$d$a$b;

    if-nez v0, :cond_0

    const-string v0, " execution"

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    iget-object v1, p0, Lf/h/c/n/d/m/k$b;->d:Ljava/lang/Integer;

    if-nez v1, :cond_1

    const-string v1, " uiOrientation"

    invoke-static {v0, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lf/h/c/n/d/m/k;

    iget-object v3, p0, Lf/h/c/n/d/m/k$b;->a:Lf/h/c/n/d/m/v$d$d$a$b;

    iget-object v4, p0, Lf/h/c/n/d/m/k$b;->b:Lf/h/c/n/d/m/w;

    iget-object v5, p0, Lf/h/c/n/d/m/k$b;->c:Ljava/lang/Boolean;

    iget-object v1, p0, Lf/h/c/n/d/m/k$b;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lf/h/c/n/d/m/k;-><init>(Lf/h/c/n/d/m/v$d$d$a$b;Lf/h/c/n/d/m/w;Ljava/lang/Boolean;ILf/h/c/n/d/m/k$a;)V

    return-object v0

    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v2, v0}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
