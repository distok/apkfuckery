.class public final Lf/h/c/n/d/m/b$b;
.super Lf/h/c/n/d/m/v$a;
.source "AutoValue_CrashlyticsReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lf/h/c/n/d/m/v$d;

.field public h:Lf/h/c/n/d/m/v$c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$a;-><init>()V

    return-void
.end method

.method public constructor <init>(Lf/h/c/n/d/m/v;Lf/h/c/n/d/m/b$a;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$a;-><init>()V

    check-cast p1, Lf/h/c/n/d/m/b;

    iget-object p2, p1, Lf/h/c/n/d/m/b;->b:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/m/b$b;->a:Ljava/lang/String;

    iget-object p2, p1, Lf/h/c/n/d/m/b;->c:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/m/b$b;->b:Ljava/lang/String;

    iget p2, p1, Lf/h/c/n/d/m/b;->d:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p0, Lf/h/c/n/d/m/b$b;->c:Ljava/lang/Integer;

    iget-object p2, p1, Lf/h/c/n/d/m/b;->e:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/m/b$b;->d:Ljava/lang/String;

    iget-object p2, p1, Lf/h/c/n/d/m/b;->f:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/m/b$b;->e:Ljava/lang/String;

    iget-object p2, p1, Lf/h/c/n/d/m/b;->g:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/m/b$b;->f:Ljava/lang/String;

    iget-object p2, p1, Lf/h/c/n/d/m/b;->h:Lf/h/c/n/d/m/v$d;

    iput-object p2, p0, Lf/h/c/n/d/m/b$b;->g:Lf/h/c/n/d/m/v$d;

    iget-object p1, p1, Lf/h/c/n/d/m/b;->i:Lf/h/c/n/d/m/v$c;

    iput-object p1, p0, Lf/h/c/n/d/m/b$b;->h:Lf/h/c/n/d/m/v$c;

    return-void
.end method


# virtual methods
.method public a()Lf/h/c/n/d/m/v;
    .locals 12

    iget-object v0, p0, Lf/h/c/n/d/m/b$b;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, " sdkVersion"

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    iget-object v1, p0, Lf/h/c/n/d/m/b$b;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, " gmpAppId"

    invoke-static {v0, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lf/h/c/n/d/m/b$b;->c:Ljava/lang/Integer;

    if-nez v1, :cond_2

    const-string v1, " platform"

    invoke-static {v0, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lf/h/c/n/d/m/b$b;->d:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, " installationUuid"

    invoke-static {v0, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v1, p0, Lf/h/c/n/d/m/b$b;->e:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, " buildVersion"

    invoke-static {v0, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    iget-object v1, p0, Lf/h/c/n/d/m/b$b;->f:Ljava/lang/String;

    if-nez v1, :cond_5

    const-string v1, " displayVersion"

    invoke-static {v0, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v0, Lf/h/c/n/d/m/b;

    iget-object v3, p0, Lf/h/c/n/d/m/b$b;->a:Ljava/lang/String;

    iget-object v4, p0, Lf/h/c/n/d/m/b$b;->b:Ljava/lang/String;

    iget-object v1, p0, Lf/h/c/n/d/m/b$b;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lf/h/c/n/d/m/b$b;->d:Ljava/lang/String;

    iget-object v7, p0, Lf/h/c/n/d/m/b$b;->e:Ljava/lang/String;

    iget-object v8, p0, Lf/h/c/n/d/m/b$b;->f:Ljava/lang/String;

    iget-object v9, p0, Lf/h/c/n/d/m/b$b;->g:Lf/h/c/n/d/m/v$d;

    iget-object v10, p0, Lf/h/c/n/d/m/b$b;->h:Lf/h/c/n/d/m/v$c;

    const/4 v11, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lf/h/c/n/d/m/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/m/v$d;Lf/h/c/n/d/m/v$c;Lf/h/c/n/d/m/b$a;)V

    return-object v0

    :cond_6
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v2, v0}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
