.class public final Lf/h/c/n/d/m/s;
.super Lf/h/c/n/d/m/v$d$d$c;
.source "AutoValue_CrashlyticsReport_Session_Event_Log.java"


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/h/c/n/d/m/s$a;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$d$d$c;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/m/s;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/m/s;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lf/h/c/n/d/m/v$d$d$c;

    if-eqz v0, :cond_1

    check-cast p1, Lf/h/c/n/d/m/v$d$d$c;

    iget-object v0, p0, Lf/h/c/n/d/m/s;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d$c;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lf/h/c/n/d/m/s;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, 0xf4243

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Log{content="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/h/c/n/d/m/s;->a:Ljava/lang/String;

    const-string/jumbo v2, "}"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
