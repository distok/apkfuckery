.class public final Lf/h/c/n/d/m/t;
.super Lf/h/c/n/d/m/v$d$e;
.source "AutoValue_CrashlyticsReport_Session_OperatingSystem.java"


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;ZLf/h/c/n/d/m/t$a;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$d$e;-><init>()V

    iput p1, p0, Lf/h/c/n/d/m/t;->a:I

    iput-object p2, p0, Lf/h/c/n/d/m/t;->b:Ljava/lang/String;

    iput-object p3, p0, Lf/h/c/n/d/m/t;->c:Ljava/lang/String;

    iput-boolean p4, p0, Lf/h/c/n/d/m/t;->d:Z

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/m/t;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lf/h/c/n/d/m/t;->a:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/m/t;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/c/n/d/m/t;->d:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lf/h/c/n/d/m/v$d$e;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lf/h/c/n/d/m/v$d$e;

    iget v1, p0, Lf/h/c/n/d/m/t;->a:I

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$e;->b()I

    move-result v3

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lf/h/c/n/d/m/t;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/c/n/d/m/t;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$e;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lf/h/c/n/d/m/t;->d:Z

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$e;->d()Z

    move-result p1

    if-ne v1, p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lf/h/c/n/d/m/t;->a:I

    const v1, 0xf4243

    xor-int/2addr v0, v1

    mul-int v0, v0, v1

    iget-object v2, p0, Lf/h/c/n/d/m/t;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    iget-object v2, p0, Lf/h/c/n/d/m/t;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    mul-int v0, v0, v1

    iget-boolean v1, p0, Lf/h/c/n/d/m/t;->d:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    goto :goto_0

    :cond_0
    const/16 v1, 0x4d5

    :goto_0
    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "OperatingSystem{platform="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lf/h/c/n/d/m/t;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/c/n/d/m/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", buildVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/c/n/d/m/t;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", jailbroken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf/h/c/n/d/m/t;->d:Z

    const-string/jumbo v2, "}"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
