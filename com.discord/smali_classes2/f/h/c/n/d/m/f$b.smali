.class public final Lf/h/c/n/d/m/f$b;
.super Lf/h/c/n/d/m/v$d$b;
.source "AutoValue_CrashlyticsReport_Session.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Boolean;

.field public f:Lf/h/c/n/d/m/v$d$a;

.field public g:Lf/h/c/n/d/m/v$d$f;

.field public h:Lf/h/c/n/d/m/v$d$e;

.field public i:Lf/h/c/n/d/m/v$d$c;

.field public j:Lf/h/c/n/d/m/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/n/d/m/w<",
            "Lf/h/c/n/d/m/v$d$d;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/c/n/d/m/v$d$b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lf/h/c/n/d/m/v$d;Lf/h/c/n/d/m/f$a;)V
    .locals 2

    invoke-direct {p0}, Lf/h/c/n/d/m/v$d$b;-><init>()V

    check-cast p1, Lf/h/c/n/d/m/f;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->a:Ljava/lang/String;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->b:Ljava/lang/String;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->b:Ljava/lang/String;

    iget-wide v0, p1, Lf/h/c/n/d/m/f;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->c:Ljava/lang/Long;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->d:Ljava/lang/Long;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->d:Ljava/lang/Long;

    iget-boolean p2, p1, Lf/h/c/n/d/m/f;->e:Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->e:Ljava/lang/Boolean;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->f:Lf/h/c/n/d/m/v$d$a;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->f:Lf/h/c/n/d/m/v$d$a;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->g:Lf/h/c/n/d/m/v$d$f;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->g:Lf/h/c/n/d/m/v$d$f;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->h:Lf/h/c/n/d/m/v$d$e;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->h:Lf/h/c/n/d/m/v$d$e;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->i:Lf/h/c/n/d/m/v$d$c;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->i:Lf/h/c/n/d/m/v$d$c;

    iget-object p2, p1, Lf/h/c/n/d/m/f;->j:Lf/h/c/n/d/m/w;

    iput-object p2, p0, Lf/h/c/n/d/m/f$b;->j:Lf/h/c/n/d/m/w;

    iget p1, p1, Lf/h/c/n/d/m/f;->k:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lf/h/c/n/d/m/f$b;->k:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public a()Lf/h/c/n/d/m/v$d;
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/c/n/d/m/f$b;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, " generator"

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, " identifier"

    invoke-static {v1, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->c:Ljava/lang/Long;

    if-nez v2, :cond_2

    const-string v2, " startedAt"

    invoke-static {v1, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    const-string v2, " crashed"

    invoke-static {v1, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_3
    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->f:Lf/h/c/n/d/m/v$d$a;

    if-nez v2, :cond_4

    const-string v2, " app"

    invoke-static {v1, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_4
    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->k:Ljava/lang/Integer;

    if-nez v2, :cond_5

    const-string v2, " generatorType"

    invoke-static {v1, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v1, Lf/h/c/n/d/m/f;

    iget-object v4, v0, Lf/h/c/n/d/m/f$b;->a:Ljava/lang/String;

    iget-object v5, v0, Lf/h/c/n/d/m/f$b;->b:Ljava/lang/String;

    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v8, v0, Lf/h/c/n/d/m/f$b;->d:Ljava/lang/Long;

    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-object v10, v0, Lf/h/c/n/d/m/f$b;->f:Lf/h/c/n/d/m/v$d$a;

    iget-object v11, v0, Lf/h/c/n/d/m/f$b;->g:Lf/h/c/n/d/m/v$d$f;

    iget-object v12, v0, Lf/h/c/n/d/m/f$b;->h:Lf/h/c/n/d/m/v$d$e;

    iget-object v13, v0, Lf/h/c/n/d/m/f$b;->i:Lf/h/c/n/d/m/v$d$c;

    iget-object v14, v0, Lf/h/c/n/d/m/f$b;->j:Lf/h/c/n/d/m/w;

    iget-object v2, v0, Lf/h/c/n/d/m/f$b;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v15

    const/16 v16, 0x0

    move-object v3, v1

    invoke-direct/range {v3 .. v16}, Lf/h/c/n/d/m/f;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;ZLf/h/c/n/d/m/v$d$a;Lf/h/c/n/d/m/v$d$f;Lf/h/c/n/d/m/v$d$e;Lf/h/c/n/d/m/v$d$c;Lf/h/c/n/d/m/w;ILf/h/c/n/d/m/f$a;)V

    return-object v1

    :cond_6
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Missing required properties:"

    invoke-static {v3, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public b(Z)Lf/h/c/n/d/m/v$d$b;
    .locals 0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lf/h/c/n/d/m/f$b;->e:Ljava/lang/Boolean;

    return-object p0
.end method
