.class public abstract Lf/h/c/n/d/m/v$d;
.super Ljava/lang/Object;
.source "CrashlyticsReport.java"


# annotations
.annotation build Lcom/google/auto/value/AutoValue;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/n/d/m/v$d$d;,
        Lf/h/c/n/d/m/v$d$c;,
        Lf/h/c/n/d/m/v$d$e;,
        Lf/h/c/n/d/m/v$d$a;,
        Lf/h/c/n/d/m/v$d$f;,
        Lf/h/c/n/d/m/v$d$b;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lf/h/c/n/d/m/v$d$a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract b()Lf/h/c/n/d/m/v$d$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Ljava/lang/Long;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Lf/h/c/n/d/m/w;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/h/c/n/d/m/w<",
            "Lf/h/c/n/d/m/v$d$d;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract f()I
.end method

.method public abstract g()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract h()Lf/h/c/n/d/m/v$d$e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract i()J
.end method

.method public abstract j()Lf/h/c/n/d/m/v$d$f;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Z
.end method

.method public abstract l()Lf/h/c/n/d/m/v$d$b;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method
