.class public final Lf/h/c/n/d/m/a$e;
.super Ljava/lang/Object;
.source "AutoCrashlyticsReportEncoder.java"

# interfaces
.implements Lf/h/c/q/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/c/q/c<",
        "Lf/h/c/n/d/m/v$d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/h/c/n/d/m/a$e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/c/n/d/m/a$e;

    invoke-direct {v0}, Lf/h/c/n/d/m/a$e;-><init>()V

    sput-object v0, Lf/h/c/n/d/m/a$e;->a:Lf/h/c/n/d/m/a$e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/c/n/d/m/v$d$a;

    check-cast p2, Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$a;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "identifier"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$a;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "version"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$a;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "displayVersion"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$a;->f()Lf/h/c/n/d/m/v$d$a$a;

    move-result-object v0

    const-string v1, "organization"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$a;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "installationUuid"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$a;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "developmentPlatform"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$a;->b()Ljava/lang/String;

    move-result-object p1

    const-string v0, "developmentPlatformVersion"

    invoke-interface {p2, v0, p1}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    return-void
.end method
