.class public abstract Lf/h/c/n/d/m/v;
.super Ljava/lang/Object;
.source "CrashlyticsReport.java"


# annotations
.annotation build Lcom/google/auto/value/AutoValue;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/n/d/m/v$a;,
        Lf/h/c/n/d/m/v$d;,
        Lf/h/c/n/d/m/v$b;,
        Lf/h/c/n/d/m/v$c;,
        Lf/h/c/n/d/m/v$e;
    }
.end annotation


# static fields
.field public static final a:Ljava/nio/charset/Charset;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lf/h/c/n/d/m/v;->a:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract b()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract c()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract e()Lf/h/c/n/d/m/v$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract f()I
.end method

.method public abstract g()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract h()Lf/h/c/n/d/m/v$d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract i()Lf/h/c/n/d/m/v$a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public j(JZLjava/lang/String;)Lf/h/c/n/d/m/v;
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Lf/h/c/n/d/m/v;->i()Lf/h/c/n/d/m/v$a;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Lf/h/c/n/d/m/b;

    iget-object v1, v1, Lf/h/c/n/d/m/b;->h:Lf/h/c/n/d/m/v$d;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lf/h/c/n/d/m/v$d;->l()Lf/h/c/n/d/m/v$d$b;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    move-object p2, v1

    check-cast p2, Lf/h/c/n/d/m/f$b;

    iput-object p1, p2, Lf/h/c/n/d/m/f$b;->d:Ljava/lang/Long;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p2, Lf/h/c/n/d/m/f$b;->e:Ljava/lang/Boolean;

    if-eqz p4, :cond_0

    new-instance p1, Lf/h/c/n/d/m/u;

    const/4 p3, 0x0

    invoke-direct {p1, p4, p3}, Lf/h/c/n/d/m/u;-><init>(Ljava/lang/String;Lf/h/c/n/d/m/u$a;)V

    iput-object p1, p2, Lf/h/c/n/d/m/f$b;->g:Lf/h/c/n/d/m/v$d$f;

    invoke-virtual {p2}, Lf/h/c/n/d/m/f$b;->a()Lf/h/c/n/d/m/v$d;

    :cond_0
    invoke-virtual {v1}, Lf/h/c/n/d/m/v$d$b;->a()Lf/h/c/n/d/m/v$d;

    move-result-object p1

    move-object p2, v0

    check-cast p2, Lf/h/c/n/d/m/b$b;

    iput-object p1, p2, Lf/h/c/n/d/m/b$b;->g:Lf/h/c/n/d/m/v$d;

    :cond_1
    invoke-virtual {v0}, Lf/h/c/n/d/m/v$a;->a()Lf/h/c/n/d/m/v;

    move-result-object p1

    return-object p1
.end method
