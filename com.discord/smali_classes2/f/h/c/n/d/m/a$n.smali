.class public final Lf/h/c/n/d/m/a$n;
.super Ljava/lang/Object;
.source "AutoCrashlyticsReportEncoder.java"

# interfaces
.implements Lf/h/c/q/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "n"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/c/q/c<",
        "Lf/h/c/n/d/m/v$d$d$a$b$d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/h/c/n/d/m/a$n;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/c/n/d/m/a$n;

    invoke-direct {v0}, Lf/h/c/n/d/m/a$n;-><init>()V

    sput-object v0, Lf/h/c/n/d/m/a$n;->a:Lf/h/c/n/d/m/a$n;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/c/n/d/m/v$d$d$a$b$d;

    check-cast p2, Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d$a$b$d;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "name"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d$a$b$d;->b()I

    move-result v0

    const-string v1, "importance"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->c(Ljava/lang/String;I)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d$d$a$b$d;->a()Lf/h/c/n/d/m/w;

    move-result-object p1

    const-string v0, "frames"

    invoke-interface {p2, v0, p1}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    return-void
.end method
