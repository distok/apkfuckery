.class public final Lf/h/c/n/d/m/a$h;
.super Ljava/lang/Object;
.source "AutoCrashlyticsReportEncoder.java"

# interfaces
.implements Lf/h/c/q/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/m/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/c/q/c<",
        "Lf/h/c/n/d/m/v$d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/h/c/n/d/m/a$h;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/c/n/d/m/a$h;

    invoke-direct {v0}, Lf/h/c/n/d/m/a$h;-><init>()V

    sput-object v0, Lf/h/c/n/d/m/a$h;->a:Lf/h/c/n/d/m/a$h;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/c/n/d/m/v$d;

    check-cast p2, Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "generator"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lf/h/c/n/d/m/v;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const-string v1, "identifier"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->i()J

    move-result-wide v0

    const-string v2, "startedAt"

    invoke-interface {p2, v2, v0, v1}, Lf/h/c/q/d;->b(Ljava/lang/String;J)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->c()Ljava/lang/Long;

    move-result-object v0

    const-string v1, "endedAt"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->k()Z

    move-result v0

    const-string v1, "crashed"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->a(Ljava/lang/String;Z)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->a()Lf/h/c/n/d/m/v$d$a;

    move-result-object v0

    const-string v1, "app"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->j()Lf/h/c/n/d/m/v$d$f;

    move-result-object v0

    const-string v1, "user"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->h()Lf/h/c/n/d/m/v$d$e;

    move-result-object v0

    const-string v1, "os"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->b()Lf/h/c/n/d/m/v$d$c;

    move-result-object v0

    const-string v1, "device"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->d()Lf/h/c/n/d/m/w;

    move-result-object v0

    const-string v1, "events"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/c/n/d/m/v$d;->f()I

    move-result p1

    const-string v0, "generatorType"

    invoke-interface {p2, v0, p1}, Lf/h/c/q/d;->c(Ljava/lang/String;I)Lf/h/c/q/d;

    return-void
.end method
