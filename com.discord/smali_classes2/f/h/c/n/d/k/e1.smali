.class public Lf/h/c/n/d/k/e1;
.super Ljava/lang/Object;
.source "SessionReportingCoordinator.java"


# instance fields
.field public final a:Lf/h/c/n/d/k/n0;

.field public final b:Lf/h/c/n/d/o/g;

.field public final c:Lf/h/c/n/d/r/c;

.field public final d:Lf/h/c/n/d/l/b;

.field public final e:Lf/h/c/n/d/k/g1;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/n0;Lf/h/c/n/d/o/g;Lf/h/c/n/d/r/c;Lf/h/c/n/d/l/b;Lf/h/c/n/d/k/g1;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/k/e1;->a:Lf/h/c/n/d/k/n0;

    iput-object p2, p0, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    iput-object p3, p0, Lf/h/c/n/d/k/e1;->c:Lf/h/c/n/d/r/c;

    iput-object p4, p0, Lf/h/c/n/d/k/e1;->d:Lf/h/c/n/d/l/b;

    iput-object p5, p0, Lf/h/c/n/d/k/e1;->e:Lf/h/c/n/d/k/g1;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;Ljava/lang/Thread;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 32
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Thread;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v1, p0

    move-object/from16 v0, p2

    move-object/from16 v2, p3

    sget-object v3, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v4, "crash"

    move-object/from16 v8, p4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, v1, Lf/h/c/n/d/k/e1;->a:Lf/h/c/n/d/k/n0;

    iget-object v6, v5, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    new-instance v7, Lf/h/c/n/d/t/e;

    iget-object v9, v5, Lf/h/c/n/d/k/n0;->d:Lf/h/c/n/d/t/d;

    move-object/from16 v10, p1

    invoke-direct {v7, v10, v9}, Lf/h/c/n/d/t/e;-><init>(Ljava/lang/Throwable;Lf/h/c/n/d/t/d;)V

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, v5, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v10, v10, Lf/h/c/n/d/k/b;->d:Ljava/lang/String;

    iget-object v11, v5, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    invoke-static {v10, v11}, Lf/h/c/n/d/k/h;->i(Ljava/lang/String;Landroid/content/Context;)Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-result-object v10

    if-eqz v10, :cond_1

    iget v10, v10, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v12, 0x64

    if-eq v10, v12, :cond_0

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    :goto_0
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_1

    :cond_1
    const/4 v10, 0x0

    :goto_1
    move-object v15, v10

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iget-object v13, v7, Lf/h/c/n/d/t/e;->c:[Ljava/lang/StackTraceElement;

    const/4 v14, 0x4

    invoke-virtual {v5, v0, v13, v14}, Lf/h/c/n/d/k/n0;->c(Ljava/lang/Thread;[Ljava/lang/StackTraceElement;I)Lf/h/c/n/d/m/v$d$d$a$b$d;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p7, :cond_3

    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v11, v16

    check-cast v11, Ljava/lang/Thread;

    invoke-virtual {v11, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    iget-object v0, v5, Lf/h/c/n/d/k/n0;->d:Lf/h/c/n/d/t/d;

    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Ljava/lang/StackTraceElement;

    invoke-interface {v0, v14}, Lf/h/c/n/d/t/d;->a([Ljava/lang/StackTraceElement;)[Ljava/lang/StackTraceElement;

    move-result-object v0

    const/4 v14, 0x0

    invoke-virtual {v5, v11, v0, v14}, Lf/h/c/n/d/k/n0;->c(Ljava/lang/Thread;[Ljava/lang/StackTraceElement;I)Lf/h/c/n/d/m/v$d$d$a$b$d;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p2

    goto :goto_2

    :cond_3
    const/4 v14, 0x0

    new-instance v0, Lf/h/c/n/d/m/w;

    invoke-direct {v0, v12}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    const/16 v11, 0x8

    const/4 v12, 0x4

    invoke-virtual {v5, v7, v12, v11, v14}, Lf/h/c/n/d/k/n0;->a(Lf/h/c/n/d/t/e;III)Lf/h/c/n/d/m/v$d$d$a$b$b;

    move-result-object v18

    const-wide/16 v11, 0x0

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string v22, ""

    if-nez v7, :cond_4

    const-string v13, " address"

    goto :goto_3

    :cond_4
    move-object/from16 v13, v22

    :goto_3
    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    const-string v11, "Missing required properties:"

    if-eqz v14, :cond_15

    new-instance v19, Lf/h/c/n/d/m/o;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    const/16 v28, 0x0

    const-string v24, "0"

    const-string v25, "0"

    move-object/from16 v23, v19

    invoke-direct/range {v23 .. v28}, Lf/h/c/n/d/m/o;-><init>(Ljava/lang/String;Ljava/lang/String;JLf/h/c/n/d/m/o$a;)V

    const/4 v7, 0x1

    new-array v7, v7, [Lf/h/c/n/d/m/v$d$d$a$b$a;

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    iget-object v13, v5, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v13, v13, Lf/h/c/n/d/k/b;->d:Ljava/lang/String;

    const-string v8, "Null name"

    invoke-static {v13, v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v8, v5, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v8, v8, Lf/h/c/n/d/k/b;->b:Ljava/lang/String;

    if-nez v14, :cond_5

    const-string v16, " baseAddress"

    move-object/from16 p1, v11

    move-object/from16 v11, v16

    goto :goto_4

    :cond_5
    move-object/from16 p1, v11

    move-object/from16 v11, v22

    :goto_4
    move/from16 v31, v4

    if-nez v12, :cond_6

    const-string v4, " size"

    invoke-static {v11, v4}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    :cond_6
    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_14

    new-instance v4, Lf/h/c/n/d/m/m;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    const/16 v30, 0x0

    move-object/from16 v23, v4

    move-object/from16 v28, v13

    move-object/from16 v29, v8

    invoke-direct/range {v23 .. v30}, Lf/h/c/n/d/m/m;-><init>(JJLjava/lang/String;Ljava/lang/String;Lf/h/c/n/d/m/m$a;)V

    const/4 v8, 0x0

    aput-object v4, v7, v8

    new-instance v4, Lf/h/c/n/d/m/w;

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v4, v7}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    new-instance v13, Lf/h/c/n/d/m/l;

    const/16 v21, 0x0

    move-object/from16 v16, v13

    move-object/from16 v17, v0

    move-object/from16 v20, v4

    invoke-direct/range {v16 .. v21}, Lf/h/c/n/d/m/l;-><init>(Lf/h/c/n/d/m/w;Lf/h/c/n/d/m/v$d$d$a$b$b;Lf/h/c/n/d/m/v$d$d$a$b$c;Lf/h/c/n/d/m/w;Lf/h/c/n/d/m/l$a;)V

    if-nez v10, :cond_7

    const-string v0, " uiOrientation"

    goto :goto_5

    :cond_7
    move-object/from16 v0, v22

    :goto_5
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_13

    new-instance v0, Lf/h/c/n/d/m/k;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v16

    const/16 v17, 0x0

    const/4 v14, 0x0

    move-object v12, v0

    invoke-direct/range {v12 .. v17}, Lf/h/c/n/d/m/k;-><init>(Lf/h/c/n/d/m/v$d$d$a$b;Lf/h/c/n/d/m/w;Ljava/lang/Boolean;ILf/h/c/n/d/m/k$a;)V

    iget-object v4, v5, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    invoke-static {v4}, Lf/h/c/n/d/k/e;->a(Landroid/content/Context;)Lf/h/c/n/d/k/e;

    move-result-object v4

    iget-object v7, v4, Lf/h/c/n/d/k/e;->a:Ljava/lang/Float;

    if-eqz v7, :cond_8

    invoke-virtual {v7}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    goto :goto_6

    :cond_8
    const/4 v7, 0x0

    :goto_6
    invoke-virtual {v4}, Lf/h/c/n/d/k/e;->b()I

    move-result v4

    iget-object v8, v5, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    invoke-static {v8}, Lf/h/c/n/d/k/h;->m(Landroid/content/Context;)Z

    move-result v8

    invoke-static {}, Lf/h/c/n/d/k/h;->p()J

    move-result-wide v10

    iget-object v5, v5, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    new-instance v12, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v12}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    const-string v13, "activity"

    invoke-virtual {v5, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager;

    invoke-virtual {v5, v12}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v12, v12, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    sub-long/2addr v10, v12

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lf/h/c/n/d/k/h;->a(Ljava/lang/String;)J

    move-result-wide v12

    new-instance v5, Lf/h/c/n/d/m/r$b;

    invoke-direct {v5}, Lf/h/c/n/d/m/r$b;-><init>()V

    iput-object v7, v5, Lf/h/c/n/d/m/r$b;->a:Ljava/lang/Double;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v5, Lf/h/c/n/d/m/r$b;->b:Ljava/lang/Integer;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v5, Lf/h/c/n/d/m/r$b;->c:Ljava/lang/Boolean;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v5, Lf/h/c/n/d/m/r$b;->d:Ljava/lang/Integer;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v5, Lf/h/c/n/d/m/r$b;->e:Ljava/lang/Long;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v5, Lf/h/c/n/d/m/r$b;->f:Ljava/lang/Long;

    invoke-virtual {v5}, Lf/h/c/n/d/m/r$b;->a()Lf/h/c/n/d/m/v$d$d$b;

    move-result-object v10

    const-string v4, " timestamp"

    if-nez v9, :cond_9

    move-object v5, v4

    goto :goto_7

    :cond_9
    move-object/from16 v5, v22

    :goto_7
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, v1, Lf/h/c/n/d/k/e1;->d:Lf/h/c/n/d/l/b;

    iget-object v6, v6, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    invoke-interface {v6}, Lf/h/c/n/d/l/a;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    new-instance v7, Lf/h/c/n/d/m/s;

    const/4 v8, 0x0

    invoke-direct {v7, v6, v8}, Lf/h/c/n/d/m/s;-><init>(Ljava/lang/String;Lf/h/c/n/d/m/s$a;)V

    move-object v11, v7

    goto :goto_8

    :cond_a
    const-string v6, "No log data to include with this event."

    invoke-virtual {v3, v6}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v11, v6

    :goto_8
    iget-object v6, v1, Lf/h/c/n/d/k/e1;->e:Lf/h/c/n/d/k/g1;

    invoke-virtual {v6}, Lf/h/c/n/d/k/g1;->a()Ljava/util/Map;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->ensureCapacity(I)V

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v12, "Null key"

    invoke-static {v9, v12}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v12, "Null value"

    invoke-static {v8, v12}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v12, Lf/h/c/n/d/m/c;

    const/4 v13, 0x0

    invoke-direct {v12, v9, v8, v13}, Lf/h/c/n/d/m/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/m/c$a;)V

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_b
    sget-object v6, Lf/h/c/n/d/k/d1;->d:Lf/h/c/n/d/k/d1;

    invoke-static {v7, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_c

    invoke-virtual {v0}, Lf/h/c/n/d/m/k;->e()Lf/h/c/n/d/m/v$d$d$a$a;

    move-result-object v0

    new-instance v6, Lf/h/c/n/d/m/w;

    invoke-direct {v6, v7}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    check-cast v0, Lf/h/c/n/d/m/k$b;

    iput-object v6, v0, Lf/h/c/n/d/m/k$b;->b:Lf/h/c/n/d/m/w;

    invoke-virtual {v0}, Lf/h/c/n/d/m/k$b;->a()Lf/h/c/n/d/m/v$d$d$a;

    move-result-object v0

    :cond_c
    move-object v9, v0

    iget-object v0, v1, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    if-nez v5, :cond_d

    goto :goto_a

    :cond_d
    move-object/from16 v4, v22

    :goto_a
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_11

    new-instance v4, Lf/h/c/n/d/m/j;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v12, 0x0

    move-object v5, v4

    move-object/from16 v8, p4

    invoke-direct/range {v5 .. v12}, Lf/h/c/n/d/m/j;-><init>(JLjava/lang/String;Lf/h/c/n/d/m/v$d$d$a;Lf/h/c/n/d/m/v$d$d$b;Lf/h/c/n/d/m/v$d$d$c;Lf/h/c/n/d/m/j$a;)V

    iget-object v5, v0, Lf/h/c/n/d/o/g;->f:Lf/h/c/n/d/s/e;

    check-cast v5, Lf/h/c/n/d/s/d;

    invoke-virtual {v5}, Lf/h/c/n/d/s/d;->c()Lf/h/c/n/d/s/i/e;

    move-result-object v5

    invoke-interface {v5}, Lf/h/c/n/d/s/i/e;->b()Lf/h/c/n/d/s/i/d;

    move-result-object v5

    iget v5, v5, Lf/h/c/n/d/s/i/d;->a:I

    invoke-virtual {v0, v2}, Lf/h/c/n/d/o/g;->h(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    sget-object v7, Lf/h/c/n/d/o/g;->i:Lf/h/c/n/d/m/x/h;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v7, Lf/h/c/n/d/m/x/h;->a:Lf/h/c/q/a;

    check-cast v7, Lf/h/c/q/h/d;

    invoke-virtual {v7, v4}, Lf/h/c/q/h/d;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lf/h/c/n/d/o/g;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const-string v0, "%010d"

    invoke-static {v7, v0, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v31, :cond_e

    const-string v22, "_"

    :cond_e
    move-object/from16 v7, v22

    const-string v8, "event"

    invoke-static {v8, v0, v7}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7, v4}, Lf/h/c/n/d/o/g;->l(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_b

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not persist event for session "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v0}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_b
    sget-object v0, Lf/h/c/n/d/o/c;->a:Lf/h/c/n/d/o/c;

    invoke-static {v6, v0}, Lf/h/c/n/d/o/g;->g(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v0

    sget-object v2, Lf/h/c/n/d/o/d;->d:Lf/h/c/n/d/o/d;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    if-gt v2, v5, :cond_f

    goto :goto_d

    :cond_f
    invoke-static {v3}, Lf/h/c/n/d/o/g;->k(Ljava/io/File;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_c

    :cond_10
    :goto_d
    return-void

    :cond_11
    new-instance v0, Ljava/lang/IllegalStateException;

    move-object/from16 v2, p1

    invoke-static {v2, v4}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    move-object/from16 v2, p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2, v5}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    move-object/from16 v2, p1

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-static {v2, v0}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_14
    move-object/from16 v2, p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2, v11}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    move-object v2, v11

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2, v13}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/util/concurrent/Executor;Lf/h/c/n/d/k/r0;)Lcom/google/android/gms/tasks/Task;
    .locals 11
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/c/n/d/k/r0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/c/n/d/k/r0;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/h/c/n/d/m/v$e;->f:Lf/h/c/n/d/m/v$e;

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    sget-object v2, Lf/h/c/n/d/k/r0;->d:Lf/h/c/n/d/k/r0;

    const/4 v3, 0x0

    if-ne p2, v2, :cond_0

    const-string p1, "Send via DataTransport disabled. Removing DataTransport reports."

    invoke-virtual {v1, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object p1, p0, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    invoke-virtual {p1}, Lf/h/c/n/d/o/g;->b()V

    invoke-static {v3}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v2, p0, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    invoke-virtual {v2}, Lf/h/c/n/d/o/g;->e()Ljava/util/List;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    invoke-virtual {v2}, Lf/h/c/n/d/o/g;->e()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    :try_start_0
    sget-object v6, Lf/h/c/n/d/o/g;->i:Lf/h/c/n/d/m/x/h;

    invoke-static {v4}, Lf/h/c/n/d/o/g;->j(Ljava/io/File;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lf/h/c/n/d/m/x/h;->f(Ljava/lang/String;)Lf/h/c/n/d/m/v;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lf/h/c/n/d/k/c;

    invoke-direct {v8, v6, v7}, Lf/h/c/n/d/k/c;-><init>(Lf/h/c/n/d/m/v;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not load report file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v8, "; deleting"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7, v6}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/c/n/d/k/o0;

    invoke-virtual {v5}, Lf/h/c/n/d/k/o0;->a()Lf/h/c/n/d/m/v;

    move-result-object v6

    invoke-virtual {v6}, Lf/h/c/n/d/m/v;->h()Lf/h/c/n/d/m/v$d;

    move-result-object v7

    if-eqz v7, :cond_2

    sget-object v6, Lf/h/c/n/d/m/v$e;->e:Lf/h/c/n/d/m/v$e;

    goto :goto_2

    :cond_2
    invoke-virtual {v6}, Lf/h/c/n/d/m/v;->e()Lf/h/c/n/d/m/v$c;

    move-result-object v6

    if-eqz v6, :cond_3

    move-object v6, v0

    goto :goto_2

    :cond_3
    sget-object v6, Lf/h/c/n/d/m/v$e;->d:Lf/h/c/n/d/m/v$e;

    :goto_2
    if-ne v6, v0, :cond_4

    sget-object v6, Lf/h/c/n/d/k/r0;->f:Lf/h/c/n/d/k/r0;

    if-eq p2, v6, :cond_4

    const-string v6, "Send native reports via DataTransport disabled. Removing DataTransport reports."

    invoke-virtual {v1, v6}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v6, p0, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    invoke-virtual {v5}, Lf/h/c/n/d/k/o0;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lf/h/c/n/d/o/g;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v6, p0, Lf/h/c/n/d/k/e1;->c:Lf/h/c/n/d/r/c;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5}, Lf/h/c/n/d/k/o0;->a()Lf/h/c/n/d/m/v;

    move-result-object v7

    new-instance v8, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v8}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iget-object v6, v6, Lf/h/c/n/d/r/c;->a:Lf/h/a/b/f;

    new-instance v9, Lf/h/a/b/a;

    sget-object v10, Lf/h/a/b/d;->f:Lf/h/a/b/d;

    invoke-direct {v9, v3, v7, v10}, Lf/h/a/b/a;-><init>(Ljava/lang/Integer;Ljava/lang/Object;Lf/h/a/b/d;)V

    new-instance v7, Lf/h/c/n/d/r/a;

    invoke-direct {v7, v8, v5}, Lf/h/c/n/d/r/a;-><init>(Lcom/google/android/gms/tasks/TaskCompletionSource;Lf/h/c/n/d/k/o0;)V

    invoke-interface {v6, v9, v7}, Lf/h/a/b/f;->b(Lf/h/a/b/c;Lf/h/a/b/h;)V

    iget-object v5, v8, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    new-instance v6, Lf/h/c/n/d/k/c1;

    invoke-direct {v6, p0}, Lf/h/c/n/d/k/c1;-><init>(Lf/h/c/n/d/k/e1;)V

    invoke-virtual {v5, p1, v6}, Lf/h/a/f/p/b0;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    invoke-static {v2}, Lf/h/a/f/f/n/g;->g0(Ljava/util/Collection;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method
