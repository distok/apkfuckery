.class public Lf/h/c/n/d/k/b0;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Lf/h/c/n/d/k/p0$a;


# instance fields
.field public final synthetic a:Lf/h/c/n/d/k/x;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/b0;->a:Lf/h/c/n/d/k/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/n/d/s/e;Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 9
    .param p1    # Lf/h/c/n/d/s/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Thread;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v6, p0, Lf/h/c/n/d/k/b0;->a:Lf/h/c/n/d/k/x;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Crashlytics is handling uncaught exception \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "\" from thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iget-object v7, v6, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v8, Lf/h/c/n/d/k/d0;

    move-object v0, v8

    move-object v1, v6

    move-object v3, p3

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lf/h/c/n/d/k/d0;-><init>(Lf/h/c/n/d/k/x;Ljava/util/Date;Ljava/lang/Throwable;Ljava/lang/Thread;Lf/h/c/n/d/s/e;)V

    invoke-virtual {v7, v8}, Lf/h/c/n/d/k/i;->c(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p1}, Lf/h/c/n/d/k/i1;->a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    monitor-exit v6

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v6

    throw p1
.end method
