.class public Lf/h/c/n/d/k/p;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/h/c/n/d/k/g1;

.field public final synthetic e:Lf/h/c/n/d/k/x;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;Lf/h/c/n/d/k/g1;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/p;->e:Lf/h/c/n/d/k/x;

    iput-object p2, p0, Lf/h/c/n/d/k/p;->d:Lf/h/c/n/d/k/g1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v1, p0, Lf/h/c/n/d/k/p;->e:Lf/h/c/n/d/k/x;

    invoke-virtual {v1}, Lf/h/c/n/d/k/x;->i()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-string v1, "Tried to cache user data while no session was open."

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_0
    iget-object v3, p0, Lf/h/c/n/d/k/p;->e:Lf/h/c/n/d/k/x;

    iget-object v3, v3, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    const-string v4, "-"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lf/h/c/n/d/k/e1;->e:Lf/h/c/n/d/k/g1;

    iget-object v5, v5, Lf/h/c/n/d/k/g1;->a:Ljava/lang/String;

    if-nez v5, :cond_1

    const-string v3, "Could not persist user ID; no user ID available"

    invoke-virtual {v0, v3}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v3, v3, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    invoke-virtual {v3, v4}, Lf/h/c/n/d/o/g;->h(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    :try_start_0
    new-instance v6, Ljava/io/File;

    const-string v7, "user"

    invoke-direct {v6, v3, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v6, v5}, Lf/h/c/n/d/o/g;->l(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not persist user ID for session "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    new-instance v3, Lf/h/c/n/d/k/a1;

    iget-object v4, p0, Lf/h/c/n/d/k/p;->e:Lf/h/c/n/d/k/x;

    invoke-virtual {v4}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Lf/h/c/n/d/k/a1;-><init>(Ljava/io/File;)V

    iget-object v4, p0, Lf/h/c/n/d/k/p;->d:Lf/h/c/n/d/k/g1;

    const-string v5, "Failed to close user metadata file."

    invoke-virtual {v3, v1}, Lf/h/c/n/d/k/a1;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    :try_start_1
    new-instance v3, Lf/h/c/n/d/k/z0;

    invoke-direct {v3, v4}, Lf/h/c/n/d/k/z0;-><init>(Lf/h/c/n/d/k/g1;)V

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/OutputStreamWriter;

    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sget-object v1, Lf/h/c/n/d/k/a1;->b:Ljava/nio/charset/Charset;

    invoke-direct {v6, v7, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v4, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/Writer;->flush()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v1

    move-object v4, v2

    :goto_1
    :try_start_3
    const-string v3, "Error serializing user metadata."

    const/4 v6, 0x6

    invoke-virtual {v0, v6}, Lf/h/c/n/d/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "FirebaseCrashlytics"

    invoke-static {v0, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_2
    :goto_2
    invoke-static {v4, v5}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    :goto_3
    return-object v2

    :catchall_2
    move-exception v0

    move-object v2, v4

    :goto_4
    move-object v4, v2

    :goto_5
    invoke-static {v4, v5}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v0
.end method
