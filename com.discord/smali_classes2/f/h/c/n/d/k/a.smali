.class public abstract Lf/h/c/n/d/k/a;
.super Ljava/lang/Object;
.source "AbstractSpiCall.java"


# static fields
.field public static final e:Ljava/util/regex/Pattern;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/h/c/n/d/n/c;

.field public final c:Lf/h/c/n/d/n/a;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-string v0, "http(s?)://[^\\/]+"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/h/c/n/d/k/a;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Lf/h/c/n/d/n/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_2

    if-eqz p3, :cond_1

    iput-object p1, p0, Lf/h/c/n/d/k/a;->d:Ljava/lang/String;

    invoke-static {p1}, Lf/h/c/n/d/k/h;->s(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lf/h/c/n/d/k/a;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :cond_0
    iput-object p2, p0, Lf/h/c/n/d/k/a;->a:Ljava/lang/String;

    iput-object p3, p0, Lf/h/c/n/d/k/a;->b:Lf/h/c/n/d/n/c;

    iput-object p4, p0, Lf/h/c/n/d/k/a;->c:Lf/h/c/n/d/n/a;

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "requestFactory must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "url must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public b()Lf/h/c/n/d/n/b;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/h/c/n/d/k/a;->c(Ljava/util/Map;)Lf/h/c/n/d/n/b;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/util/Map;)Lf/h/c/n/d/n/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lf/h/c/n/d/n/b;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/k/a;->b:Lf/h/c/n/d/n/c;

    iget-object v1, p0, Lf/h/c/n/d/k/a;->c:Lf/h/c/n/d/n/a;

    iget-object v2, p0, Lf/h/c/n/d/k/a;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/h/c/n/d/n/b;

    invoke-direct {v0, v1, v2, p1}, Lf/h/c/n/d/n/b;-><init>(Lf/h/c/n/d/n/a;Ljava/lang/String;Ljava/util/Map;)V

    iget-object p1, v0, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v1, "User-Agent"

    const-string v2, "Crashlytics Android SDK/17.3.0"

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, v0, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    const-string v1, "X-CRASHLYTICS-DEVELOPER-TOKEN"

    const-string v2, "470fa2b4ae81cd56ecbcda9735803434cec591fa"

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method
