.class public Lf/h/c/n/d/k/k1;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/util/concurrent/Callable;

.field public final synthetic e:Lcom/google/android/gms/tasks/TaskCompletionSource;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Callable;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/k1;->d:Ljava/util/concurrent/Callable;

    iput-object p2, p0, Lf/h/c/n/d/k/k1;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lf/h/c/n/d/k/k1;->d:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/tasks/Task;

    new-instance v1, Lf/h/c/n/d/k/k1$a;

    invoke-direct {v1, p0}, Lf/h/c/n/d/k/k1$a;-><init>(Lf/h/c/n/d/k/k1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->h(Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/n/d/k/k1;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, v1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method
