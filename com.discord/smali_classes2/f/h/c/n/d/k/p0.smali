.class public Lf/h/c/n/d/k/p0;
.super Ljava/lang/Object;
.source "CrashlyticsUncaughtExceptionHandler.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/n/d/k/p0$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/c/n/d/k/p0$a;

.field public final b:Lf/h/c/n/d/s/e;

.field public final c:Ljava/lang/Thread$UncaughtExceptionHandler;

.field public final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/p0$a;Lf/h/c/n/d/s/e;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/k/p0;->a:Lf/h/c/n/d/k/p0$a;

    iput-object p2, p0, Lf/h/c/n/d/k/p0;->b:Lf/h/c/n/d/s/e;

    iput-object p3, p0, Lf/h/c/n/d/k/p0;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lf/h/c/n/d/k/p0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 6

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v1, p0, Lf/h/c/n/d/k/p0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v1, 0x0

    const-string v2, "Crashlytics completed exception processing. Invoking default exception handler."

    if-nez p1, :cond_0

    :try_start_0
    const-string v3, "Could not handle uncaught exception; null thread"

    invoke-virtual {v0, v3}, Lf/h/c/n/d/b;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    const-string v3, "Could not handle uncaught exception; null throwable"

    invoke-virtual {v0, v3}, Lf/h/c/n/d/b;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lf/h/c/n/d/k/p0;->a:Lf/h/c/n/d/k/p0$a;

    iget-object v4, p0, Lf/h/c/n/d/k/p0;->b:Lf/h/c/n/d/s/e;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast v3, Lf/h/c/n/d/k/b0;

    :try_start_1
    invoke-virtual {v3, v4, p1, p2}, Lf/h/c/n/d/k/b0;->a(Lf/h/c/n/d/s/e;Ljava/lang/Thread;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_0
    invoke-virtual {v0, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/c/n/d/k/p0;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    iget-object p1, p0, Lf/h/c/n/d/k/p0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1

    :catchall_0
    move-exception v3

    goto :goto_2

    :catch_0
    move-exception v3

    :try_start_2
    const-string v4, "An error occurred in the uncaught exception handler"

    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Lf/h/c/n/d/b;->a(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "FirebaseCrashlytics"

    invoke-static {v5, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v0, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/c/n/d/k/p0;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    iget-object p1, p0, Lf/h/c/n/d/k/p0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v3
.end method
