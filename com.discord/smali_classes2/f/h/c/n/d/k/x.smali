.class public Lf/h/c/n/d/k/x;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/n/d/k/x$j;,
        Lf/h/c/n/d/k/x$m;,
        Lf/h/c/n/d/k/x$k;,
        Lf/h/c/n/d/k/x$l;,
        Lf/h/c/n/d/k/x$g;,
        Lf/h/c/n/d/k/x$i;,
        Lf/h/c/n/d/k/x$f;,
        Lf/h/c/n/d/k/x$n;,
        Lf/h/c/n/d/k/x$h;
    }
.end annotation


# static fields
.field public static final A:Ljava/io/FilenameFilter;

.field public static final B:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public static final C:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Ljava/util/regex/Pattern;

.field public static final E:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:[Ljava/lang/String;

.field public static final z:Ljava/io/FilenameFilter;


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final b:Landroid/content/Context;

.field public final c:Lf/h/c/n/d/k/q0;

.field public final d:Lf/h/c/n/d/k/m0;

.field public final e:Lf/h/c/n/d/k/g1;

.field public final f:Lf/h/c/n/d/k/i;

.field public final g:Lf/h/c/n/d/n/c;

.field public final h:Lf/h/c/n/d/k/w0;

.field public final i:Lf/h/c/n/d/o/h;

.field public final j:Lf/h/c/n/d/k/b;

.field public final k:Lf/h/c/n/d/q/b$b;

.field public final l:Lf/h/c/n/d/k/x$j;

.field public final m:Lf/h/c/n/d/l/b;

.field public final n:Lf/h/c/n/d/q/a;

.field public final o:Lf/h/c/n/d/q/b$a;

.field public final p:Lf/h/c/n/d/a;

.field public final q:Lf/h/c/n/d/t/d;

.field public final r:Ljava/lang/String;

.field public final s:Lf/h/c/n/d/i/a;

.field public final t:Lf/h/c/n/d/k/e1;

.field public u:Lf/h/c/n/d/k/p0;

.field public v:Lcom/google/android/gms/tasks/TaskCompletionSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lcom/google/android/gms/tasks/TaskCompletionSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lcom/google/android/gms/tasks/TaskCompletionSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lf/h/c/n/d/k/x$a;

    const-string v1, "BeginSession"

    invoke-direct {v0, v1}, Lf/h/c/n/d/k/x$a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lf/h/c/n/d/k/x;->z:Ljava/io/FilenameFilter;

    new-instance v0, Lf/h/c/n/d/k/x$b;

    invoke-direct {v0}, Lf/h/c/n/d/k/x$b;-><init>()V

    sput-object v0, Lf/h/c/n/d/k/x;->A:Ljava/io/FilenameFilter;

    new-instance v0, Lf/h/c/n/d/k/x$c;

    invoke-direct {v0}, Lf/h/c/n/d/k/x$c;-><init>()V

    sput-object v0, Lf/h/c/n/d/k/x;->B:Ljava/util/Comparator;

    new-instance v0, Lf/h/c/n/d/k/x$d;

    invoke-direct {v0}, Lf/h/c/n/d/k/x$d;-><init>()V

    sput-object v0, Lf/h/c/n/d/k/x;->C:Ljava/util/Comparator;

    const-string v0, "([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/h/c/n/d/k/x;->D:Ljava/util/regex/Pattern;

    const-string v0, "X-CRASHLYTICS-SEND-FLAGS"

    const-string v1, "1"

    invoke-static {v0, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lf/h/c/n/d/k/x;->E:Ljava/util/Map;

    const-string v0, "SessionUser"

    const-string v1, "SessionApp"

    const-string v2, "SessionOS"

    const-string v3, "SessionDevice"

    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf/h/c/n/d/k/x;->F:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lf/h/c/n/d/k/i;Lf/h/c/n/d/n/c;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/k/q0;Lf/h/c/n/d/o/h;Lf/h/c/n/d/k/m0;Lf/h/c/n/d/k/b;Lf/h/c/n/d/q/a;Lf/h/c/n/d/q/b$b;Lf/h/c/n/d/a;Lf/h/c/n/d/i/a;Lf/h/c/n/d/s/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p9, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p10, 0x0

    invoke-direct {p9, p10}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p9, p0, Lf/h/c/n/d/k/x;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance p9, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p9}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iput-object p9, p0, Lf/h/c/n/d/k/x;->v:Lcom/google/android/gms/tasks/TaskCompletionSource;

    new-instance p9, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p9}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iput-object p9, p0, Lf/h/c/n/d/k/x;->w:Lcom/google/android/gms/tasks/TaskCompletionSource;

    new-instance p9, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p9}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iput-object p9, p0, Lf/h/c/n/d/k/x;->x:Lcom/google/android/gms/tasks/TaskCompletionSource;

    new-instance p9, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p9, p10}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p9, p0, Lf/h/c/n/d/k/x;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    iput-object p2, p0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    iput-object p3, p0, Lf/h/c/n/d/k/x;->g:Lf/h/c/n/d/n/c;

    iput-object p4, p0, Lf/h/c/n/d/k/x;->h:Lf/h/c/n/d/k/w0;

    iput-object p5, p0, Lf/h/c/n/d/k/x;->c:Lf/h/c/n/d/k/q0;

    iput-object p6, p0, Lf/h/c/n/d/k/x;->i:Lf/h/c/n/d/o/h;

    iput-object p7, p0, Lf/h/c/n/d/k/x;->d:Lf/h/c/n/d/k/m0;

    iput-object p8, p0, Lf/h/c/n/d/k/x;->j:Lf/h/c/n/d/k/b;

    new-instance p2, Lf/h/c/n/d/k/h0;

    invoke-direct {p2, p0}, Lf/h/c/n/d/k/h0;-><init>(Lf/h/c/n/d/k/x;)V

    iput-object p2, p0, Lf/h/c/n/d/k/x;->k:Lf/h/c/n/d/q/b$b;

    iput-object p11, p0, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    iget-object p2, p8, Lf/h/c/n/d/k/b;->g:Lf/h/c/n/d/u/a;

    invoke-virtual {p2}, Lf/h/c/n/d/u/a;->a()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lf/h/c/n/d/k/x;->r:Ljava/lang/String;

    iput-object p12, p0, Lf/h/c/n/d/k/x;->s:Lf/h/c/n/d/i/a;

    new-instance p2, Lf/h/c/n/d/k/g1;

    invoke-direct {p2}, Lf/h/c/n/d/k/g1;-><init>()V

    iput-object p2, p0, Lf/h/c/n/d/k/x;->e:Lf/h/c/n/d/k/g1;

    new-instance p3, Lf/h/c/n/d/k/x$j;

    invoke-direct {p3, p6}, Lf/h/c/n/d/k/x$j;-><init>(Lf/h/c/n/d/o/h;)V

    iput-object p3, p0, Lf/h/c/n/d/k/x;->l:Lf/h/c/n/d/k/x$j;

    new-instance p7, Lf/h/c/n/d/l/b;

    invoke-direct {p7, p1, p3}, Lf/h/c/n/d/l/b;-><init>(Landroid/content/Context;Lf/h/c/n/d/l/b$b;)V

    iput-object p7, p0, Lf/h/c/n/d/k/x;->m:Lf/h/c/n/d/l/b;

    const/4 p3, 0x0

    new-instance p5, Lf/h/c/n/d/q/a;

    new-instance p9, Lf/h/c/n/d/k/x$k;

    invoke-direct {p9, p0, p3}, Lf/h/c/n/d/k/x$k;-><init>(Lf/h/c/n/d/k/x;Lf/h/c/n/d/k/x$a;)V

    invoke-direct {p5, p9}, Lf/h/c/n/d/q/a;-><init>(Lf/h/c/n/d/q/b$c;)V

    iput-object p5, p0, Lf/h/c/n/d/k/x;->n:Lf/h/c/n/d/q/a;

    new-instance p5, Lf/h/c/n/d/k/x$l;

    invoke-direct {p5, p0, p3}, Lf/h/c/n/d/k/x$l;-><init>(Lf/h/c/n/d/k/x;Lf/h/c/n/d/k/x$a;)V

    iput-object p5, p0, Lf/h/c/n/d/k/x;->o:Lf/h/c/n/d/q/b$a;

    new-instance p3, Lf/h/c/n/d/t/a;

    const/16 p5, 0x400

    const/4 p9, 0x1

    new-array p9, p9, [Lf/h/c/n/d/t/d;

    new-instance p11, Lf/h/c/n/d/t/c;

    const/16 p12, 0xa

    invoke-direct {p11, p12}, Lf/h/c/n/d/t/c;-><init>(I)V

    aput-object p11, p9, p10

    invoke-direct {p3, p5, p9}, Lf/h/c/n/d/t/a;-><init>(I[Lf/h/c/n/d/t/d;)V

    iput-object p3, p0, Lf/h/c/n/d/k/x;->q:Lf/h/c/n/d/t/d;

    new-instance p5, Ljava/io/File;

    new-instance p9, Ljava/io/File;

    iget-object p6, p6, Lf/h/c/n/d/o/h;->a:Landroid/content/Context;

    invoke-virtual {p6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p6

    const-string p10, ".com.google.firebase.crashlytics"

    invoke-direct {p9, p6, p10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p6

    invoke-direct {p5, p6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance p6, Lf/h/c/n/d/k/n0;

    invoke-direct {p6, p1, p4, p8, p3}, Lf/h/c/n/d/k/n0;-><init>(Landroid/content/Context;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/k/b;Lf/h/c/n/d/t/d;)V

    new-instance p8, Lf/h/c/n/d/o/g;

    invoke-direct {p8, p5, p13}, Lf/h/c/n/d/o/g;-><init>(Ljava/io/File;Lf/h/c/n/d/s/e;)V

    sget-object p3, Lf/h/c/n/d/r/c;->b:Lf/h/c/n/d/m/x/h;

    invoke-static {p1}, Lf/h/a/b/j/n;->b(Landroid/content/Context;)V

    invoke-static {}, Lf/h/a/b/j/n;->a()Lf/h/a/b/j/n;

    move-result-object p1

    new-instance p3, Lf/h/a/b/i/a;

    sget-object p4, Lf/h/c/n/d/r/c;->c:Ljava/lang/String;

    sget-object p5, Lf/h/c/n/d/r/c;->d:Ljava/lang/String;

    invoke-direct {p3, p4, p5}, Lf/h/a/b/i/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p3}, Lf/h/a/b/j/n;->c(Lf/h/a/b/j/d;)Lf/h/a/b/g;

    move-result-object p1

    const-class p3, Lf/h/c/n/d/m/v;

    new-instance p4, Lf/h/a/b/b;

    const-string p5, "json"

    invoke-direct {p4, p5}, Lf/h/a/b/b;-><init>(Ljava/lang/String;)V

    sget-object p5, Lf/h/c/n/d/r/c;->e:Lf/h/a/b/e;

    check-cast p1, Lf/h/a/b/j/j;

    const-string p9, "FIREBASE_CRASHLYTICS_REPORT"

    invoke-virtual {p1, p9, p3, p4, p5}, Lf/h/a/b/j/j;->a(Ljava/lang/String;Ljava/lang/Class;Lf/h/a/b/b;Lf/h/a/b/e;)Lf/h/a/b/f;

    move-result-object p1

    new-instance p9, Lf/h/c/n/d/r/c;

    invoke-direct {p9, p1, p5}, Lf/h/c/n/d/r/c;-><init>(Lf/h/a/b/f;Lf/h/a/b/e;)V

    new-instance p1, Lf/h/c/n/d/k/e1;

    move-object p3, p1

    move-object p4, p6

    move-object p5, p8

    move-object p6, p9

    move-object p8, p2

    invoke-direct/range {p3 .. p8}, Lf/h/c/n/d/k/e1;-><init>(Lf/h/c/n/d/k/n0;Lf/h/c/n/d/o/g;Lf/h/c/n/d/r/c;Lf/h/c/n/d/l/b;Lf/h/c/n/d/k/g1;)V

    iput-object p1, p0, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    return-void
.end method

.method public static A(Lf/h/c/n/d/p/c;Ljava/io/File;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "Failed to close file input stream."

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object p0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v0, "Tried to include a file that doesn\'t exist: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/h/c/n/d/b;->d(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v3

    long-to-int p1, v3

    invoke-static {v2, p0, p1}, Lf/h/c/n/d/k/x;->e(Ljava/io/InputStream;Lf/h/c/n/d/p/c;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v2, v0}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception p0

    move-object v1, v2

    goto :goto_0

    :catchall_1
    move-exception p0

    :goto_0
    invoke-static {v1, v0}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lf/h/c/n/d/k/x;)V
    .locals 39
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-static/range {p0 .. p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/c/n/d/k/x;->j()J

    move-result-wide v14

    new-instance v1, Lf/h/c/n/d/k/g;

    iget-object v2, v0, Lf/h/c/n/d/k/x;->h:Lf/h/c/n/d/k/w0;

    invoke-direct {v1, v2}, Lf/h/c/n/d/k/g;-><init>(Lf/h/c/n/d/k/w0;)V

    sget-object v13, Lf/h/c/n/d/k/g;->b:Ljava/lang/String;

    sget-object v12, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Opening a new session with ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v1, v0, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    invoke-interface {v1, v13}, Lf/h/c/n/d/a;->h(Ljava/lang/String;)Z

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const-string v10, "17.3.0"

    const/4 v2, 0x0

    aput-object v10, v1, v2

    const-string v2, "Crashlytics Android SDK/%s"

    invoke-static {v11, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lf/h/c/n/d/k/u;

    move-object v1, v8

    move-object/from16 v2, p0

    move-object v3, v13

    move-object v4, v7

    move-wide v5, v14

    invoke-direct/range {v1 .. v6}, Lf/h/c/n/d/k/u;-><init>(Lf/h/c/n/d/k/x;Ljava/lang/String;Ljava/lang/String;J)V

    const-string v1, "BeginSession"

    invoke-virtual {v0, v13, v1, v8}, Lf/h/c/n/d/k/x;->z(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/x$g;)V

    iget-object v1, v0, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    invoke-interface {v1, v13, v7, v14, v15}, Lf/h/c/n/d/a;->d(Ljava/lang/String;Ljava/lang/String;J)V

    iget-object v1, v0, Lf/h/c/n/d/k/x;->h:Lf/h/c/n/d/k/w0;

    iget-object v8, v1, Lf/h/c/n/d/k/w0;->c:Ljava/lang/String;

    iget-object v2, v0, Lf/h/c/n/d/k/x;->j:Lf/h/c/n/d/k/b;

    iget-object v9, v2, Lf/h/c/n/d/k/b;->e:Ljava/lang/String;

    iget-object v7, v2, Lf/h/c/n/d/k/b;->f:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/c/n/d/k/w0;->b()Ljava/lang/String;

    move-result-object v16

    iget-object v1, v0, Lf/h/c/n/d/k/x;->j:Lf/h/c/n/d/k/b;

    iget-object v1, v1, Lf/h/c/n/d/k/b;->c:Ljava/lang/String;

    invoke-static {v1}, Lf/h/c/n/d/k/s0;->f(Ljava/lang/String;)Lf/h/c/n/d/k/s0;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/c/n/d/k/s0;->g()I

    move-result v17

    new-instance v6, Lf/h/c/n/d/k/v;

    move-object v1, v6

    move-object/from16 v2, p0

    move-object v3, v8

    move-object v4, v9

    move-object v5, v7

    move-object/from16 v18, v10

    move-object v10, v6

    move-object/from16 v6, v16

    move-object/from16 v19, v7

    move/from16 v7, v17

    invoke-direct/range {v1 .. v7}, Lf/h/c/n/d/k/v;-><init>(Lf/h/c/n/d/k/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "SessionApp"

    invoke-virtual {v0, v13, v1, v10}, Lf/h/c/n/d/k/x;->z(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/x$g;)V

    iget-object v3, v0, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    iget-object v10, v0, Lf/h/c/n/d/k/x;->r:Ljava/lang/String;

    move-object v4, v13

    move-object v5, v8

    move-object v6, v9

    move-object/from16 v7, v19

    move-object/from16 v8, v16

    move/from16 v9, v17

    move-object/from16 v2, v18

    invoke-interface/range {v3 .. v10}, Lf/h/c/n/d/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    sget-object v10, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget-object v8, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    invoke-static {v1}, Lf/h/c/n/d/k/h;->t(Landroid/content/Context;)Z

    move-result v1

    new-instance v3, Lf/h/c/n/d/k/w;

    invoke-direct {v3, v0, v10, v8, v1}, Lf/h/c/n/d/k/w;-><init>(Lf/h/c/n/d/k/x;Ljava/lang/String;Ljava/lang/String;Z)V

    const-string v4, "SessionOS"

    invoke-virtual {v0, v13, v4, v3}, Lf/h/c/n/d/k/x;->z(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/x$g;)V

    iget-object v3, v0, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    invoke-interface {v3, v13, v10, v8, v1}, Lf/h/c/n/d/a;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, v0, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    sget-object v4, Lf/h/c/n/d/k/h$b;->k:Lf/h/c/n/d/k/h$b;

    sget-object v9, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Architecture#getValue()::Build.CPU_ABI returned null or empty"

    invoke-virtual {v12, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v9, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lf/h/c/n/d/k/h$b;->n:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/c/n/d/k/h$b;

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    move-object v4, v5

    :goto_0
    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v18

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v20

    invoke-static {}, Lf/h/c/n/d/k/h;->p()J

    move-result-wide v21

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCount()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    move-object v7, v2

    int-to-long v2, v3

    mul-long v23, v4, v2

    invoke-static {v1}, Lf/h/c/n/d/k/h;->r(Landroid/content/Context;)Z

    move-result v25

    invoke-static {v1}, Lf/h/c/n/d/k/h;->k(Landroid/content/Context;)I

    move-result v26

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    new-instance v3, Lf/h/c/n/d/k/y;

    move-object v1, v3

    move-object/from16 v2, p0

    move-wide/from16 v29, v14

    move-object v14, v3

    move/from16 v3, v18

    move-object v15, v4

    move-object v4, v6

    move-object/from16 v31, v5

    move/from16 v5, v20

    move-object/from16 v33, v6

    move-object/from16 v32, v7

    move-wide/from16 v6, v21

    move-object/from16 v34, v8

    move-object/from16 v35, v9

    move-wide/from16 v8, v23

    move-object/from16 v36, v10

    move/from16 v10, v25

    move-object/from16 v37, v11

    move/from16 v11, v26

    move-object/from16 v38, v12

    move-object/from16 v12, v31

    move-object v0, v13

    move-object v13, v15

    invoke-direct/range {v1 .. v13}, Lf/h/c/n/d/k/y;-><init>(Lf/h/c/n/d/k/x;ILjava/lang/String;IJJZILjava/lang/String;Ljava/lang/String;)V

    const-string v1, "SessionDevice"

    invoke-virtual {v2, v0, v1, v14}, Lf/h/c/n/d/k/x;->z(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/x$g;)V

    iget-object v1, v2, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    move-object/from16 v16, v1

    move-object/from16 v17, v0

    move-object/from16 v19, v33

    move-object/from16 v27, v31

    move-object/from16 v28, v15

    invoke-interface/range {v16 .. v28}, Lf/h/c/n/d/a;->c(Ljava/lang/String;ILjava/lang/String;IJJZILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, v2, Lf/h/c/n/d/k/x;->m:Lf/h/c/n/d/l/b;

    invoke-virtual {v1, v0}, Lf/h/c/n/d/l/b;->a(Ljava/lang/String;)V

    iget-object v1, v2, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    invoke-static {v0}, Lf/h/c/n/d/k/x;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lf/h/c/n/d/k/e1;->a:Lf/h/c/n/d/k/n0;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lf/h/c/n/d/m/v;->a:Ljava/nio/charset/Charset;

    new-instance v3, Lf/h/c/n/d/m/b$b;

    invoke-direct {v3}, Lf/h/c/n/d/m/b$b;-><init>()V

    move-object/from16 v4, v32

    iput-object v4, v3, Lf/h/c/n/d/m/b$b;->a:Ljava/lang/String;

    iget-object v4, v2, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v4, v4, Lf/h/c/n/d/k/b;->a:Ljava/lang/String;

    const-string v5, "Null gmpAppId"

    invoke-static {v4, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v4, v3, Lf/h/c/n/d/m/b$b;->b:Ljava/lang/String;

    iget-object v4, v2, Lf/h/c/n/d/k/n0;->b:Lf/h/c/n/d/k/w0;

    invoke-virtual {v4}, Lf/h/c/n/d/k/w0;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Null installationUuid"

    invoke-static {v4, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v4, v3, Lf/h/c/n/d/m/b$b;->d:Ljava/lang/String;

    iget-object v4, v2, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v4, v4, Lf/h/c/n/d/k/b;->e:Ljava/lang/String;

    const-string v5, "Null buildVersion"

    invoke-static {v4, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v4, v3, Lf/h/c/n/d/m/b$b;->e:Ljava/lang/String;

    iget-object v4, v2, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v4, v4, Lf/h/c/n/d/k/b;->f:Ljava/lang/String;

    const-string v6, "Null displayVersion"

    invoke-static {v4, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v4, v3, Lf/h/c/n/d/m/b$b;->f:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lf/h/c/n/d/m/b$b;->c:Ljava/lang/Integer;

    new-instance v4, Lf/h/c/n/d/m/f$b;

    invoke-direct {v4}, Lf/h/c/n/d/m/f$b;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lf/h/c/n/d/m/f$b;->b(Z)Lf/h/c/n/d/m/v$d$b;

    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v4, Lf/h/c/n/d/m/f$b;->c:Ljava/lang/Long;

    const-string v6, "Null identifier"

    invoke-static {v0, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v0, v4, Lf/h/c/n/d/m/f$b;->b:Ljava/lang/String;

    sget-object v0, Lf/h/c/n/d/k/n0;->e:Ljava/lang/String;

    const-string v7, "Null generator"

    invoke-static {v0, v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v0, v4, Lf/h/c/n/d/m/f$b;->a:Ljava/lang/String;

    iget-object v0, v2, Lf/h/c/n/d/k/n0;->b:Lf/h/c/n/d/k/w0;

    iget-object v0, v0, Lf/h/c/n/d/k/w0;->c:Ljava/lang/String;

    invoke-static {v0, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v6, v2, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v6, v6, Lf/h/c/n/d/k/b;->e:Ljava/lang/String;

    const-string v7, "Null version"

    invoke-static {v6, v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v8, v2, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v8, v8, Lf/h/c/n/d/k/b;->f:Ljava/lang/String;

    iget-object v9, v2, Lf/h/c/n/d/k/n0;->b:Lf/h/c/n/d/k/w0;

    invoke-virtual {v9}, Lf/h/c/n/d/k/w0;->b()Ljava/lang/String;

    move-result-object v21

    iget-object v9, v2, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iget-object v9, v9, Lf/h/c/n/d/k/b;->g:Lf/h/c/n/d/u/a;

    invoke-virtual {v9}, Lf/h/c/n/d/u/a;->a()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    if-eqz v9, :cond_2

    const-string v10, "Unity"

    move-object/from16 v23, v9

    move-object/from16 v22, v10

    goto :goto_1

    :cond_2
    move-object/from16 v22, v10

    move-object/from16 v23, v22

    :goto_1
    new-instance v9, Lf/h/c/n/d/m/g;

    const/16 v20, 0x0

    const/16 v24, 0x0

    move-object/from16 v16, v9

    move-object/from16 v17, v0

    move-object/from16 v18, v6

    move-object/from16 v19, v8

    invoke-direct/range {v16 .. v24}, Lf/h/c/n/d/m/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/m/v$d$a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/m/g$a;)V

    iput-object v9, v4, Lf/h/c/n/d/m/f$b;->f:Lf/h/c/n/d/m/v$d$a;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v8, v36

    invoke-static {v8, v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object/from16 v7, v34

    invoke-static {v7, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v5, v2, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    invoke-static {v5}, Lf/h/c/n/d/k/h;->t(Landroid/content/Context;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    if-nez v6, :cond_3

    const-string v9, " platform"

    goto :goto_2

    :cond_3
    const-string v9, ""

    :goto_2
    if-nez v5, :cond_4

    const-string v10, " jailbroken"

    invoke-static {v9, v10}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_4
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_8

    new-instance v9, Lf/h/c/n/d/m/t;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v17

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v20

    const/16 v21, 0x0

    move-object/from16 v16, v9

    move-object/from16 v18, v8

    move-object/from16 v19, v7

    invoke-direct/range {v16 .. v21}, Lf/h/c/n/d/m/t;-><init>(ILjava/lang/String;Ljava/lang/String;ZLf/h/c/n/d/m/t$a;)V

    iput-object v9, v4, Lf/h/c/n/d/m/f$b;->h:Lf/h/c/n/d/m/v$d$e;

    new-instance v5, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-static/range {v35 .. v35}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v7, 0x7

    if-eqz v6, :cond_5

    goto :goto_3

    :cond_5
    sget-object v6, Lf/h/c/n/d/k/n0;->f:Ljava/util/Map;

    move-object/from16 v9, v35

    move-object/from16 v8, v37

    invoke-virtual {v9, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-nez v6, :cond_6

    goto :goto_3

    :cond_6
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    :goto_3
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v6

    invoke-static {}, Lf/h/c/n/d/k/h;->p()J

    move-result-wide v8

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockCount()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v5

    int-to-long v12, v5

    mul-long v10, v10, v12

    iget-object v5, v2, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    invoke-static {v5}, Lf/h/c/n/d/k/h;->r(Landroid/content/Context;)Z

    move-result v5

    iget-object v2, v2, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    invoke-static {v2}, Lf/h/c/n/d/k/h;->k(Landroid/content/Context;)I

    move-result v2

    new-instance v12, Lf/h/c/n/d/m/i$b;

    invoke-direct {v12}, Lf/h/c/n/d/m/i$b;-><init>()V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v12, Lf/h/c/n/d/m/i$b;->a:Ljava/lang/Integer;

    const-string v7, "Null model"

    move-object/from16 v13, v33

    invoke-static {v13, v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v13, v12, Lf/h/c/n/d/m/i$b;->b:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v12, Lf/h/c/n/d/m/i$b;->c:Ljava/lang/Integer;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v12, Lf/h/c/n/d/m/i$b;->d:Ljava/lang/Long;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v12, Lf/h/c/n/d/m/i$b;->e:Ljava/lang/Long;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v12, Lf/h/c/n/d/m/i$b;->f:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v12, Lf/h/c/n/d/m/i$b;->g:Ljava/lang/Integer;

    const-string v2, "Null manufacturer"

    move-object/from16 v5, v31

    invoke-static {v5, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v5, v12, Lf/h/c/n/d/m/i$b;->h:Ljava/lang/String;

    const-string v2, "Null modelClass"

    invoke-static {v15, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v15, v12, Lf/h/c/n/d/m/i$b;->i:Ljava/lang/String;

    invoke-virtual {v12}, Lf/h/c/n/d/m/i$b;->a()Lf/h/c/n/d/m/v$d$c;

    move-result-object v2

    iput-object v2, v4, Lf/h/c/n/d/m/f$b;->i:Lf/h/c/n/d/m/v$d$c;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lf/h/c/n/d/m/f$b;->k:Ljava/lang/Integer;

    invoke-virtual {v4}, Lf/h/c/n/d/m/f$b;->a()Lf/h/c/n/d/m/v$d;

    move-result-object v0

    iput-object v0, v3, Lf/h/c/n/d/m/b$b;->g:Lf/h/c/n/d/m/v$d;

    invoke-virtual {v3}, Lf/h/c/n/d/m/b$b;->a()Lf/h/c/n/d/m/v;

    move-result-object v0

    iget-object v1, v1, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/h/c/n/d/m/v;->h()Lf/h/c/n/d/m/v$d;

    move-result-object v2

    if-nez v2, :cond_7

    const-string v0, "Could not get session for report"

    move-object/from16 v3, v38

    invoke-virtual {v3, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    move-object/from16 v3, v38

    invoke-virtual {v2}, Lf/h/c/n/d/m/v$d;->g()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-virtual {v1, v2}, Lf/h/c/n/d/o/g;->h(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lf/h/c/n/d/o/g;->i(Ljava/io/File;)Ljava/io/File;

    sget-object v4, Lf/h/c/n/d/o/g;->i:Lf/h/c/n/d/m/x/h;

    invoke-virtual {v4, v0}, Lf/h/c/n/d/m/x/h;->g(Lf/h/c/n/d/m/v;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/io/File;

    const-string v5, "report"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v4, v0}, Lf/h/c/n/d/o/g;->l(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not persist report for session "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_4
    return-void

    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Missing required properties:"

    invoke-static {v1, v9}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Lf/h/c/n/d/k/x;)Lcom/google/android/gms/tasks/Task;
    .locals 10

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lf/h/c/n/d/k/m;->a:Lf/h/c/n/d/k/m;

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v3

    invoke-static {v3, v2}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v2, v4

    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v8, 0x1

    :try_start_1
    const-string v9, "com.google.firebase.crash.FirebaseCrash"

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v9, 0x1

    goto :goto_1

    :catch_0
    const/4 v9, 0x0

    :goto_1
    if-eqz v9, :cond_0

    :try_start_2
    const-string v6, "Skipping logging Crashlytics event to Firebase, FirebaseCrash exists"

    invoke-virtual {v0, v6}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-static {v6}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v6

    goto :goto_2

    :cond_0
    new-instance v9, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v9, v8}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    new-instance v8, Lf/h/c/n/d/k/a0;

    invoke-direct {v8, p0, v6, v7}, Lf/h/c/n/d/k/a0;-><init>(Lf/h/c/n/d/k/x;J)V

    invoke-static {v9, v8}, Lf/h/a/f/f/n/g;->f(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v6

    :goto_2
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    const-string v6, "Could not parse timestamp from file "

    invoke-static {v6}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lf/h/a/f/f/n/g;->g0(Ljava/util/Collection;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/lang/String;Ljava/io/File;)V
    .locals 9
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/io/File;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    :cond_0
    const-string v0, "Failed to close "

    const-string v1, "Failed to flush to append to "

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    const/4 v4, 0x1

    invoke-direct {v3, p1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {v3}, Lf/h/c/n/d/p/c;->i(Ljava/io/OutputStream;)Lf/h/c/n/d/p/c;

    move-result-object v2

    sget-object v4, Lf/h/c/n/d/p/d;->a:Lf/h/c/n/d/p/a;

    invoke-static {p0}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object p0

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5}, Lf/h/c/n/d/p/c;->q(II)V

    invoke-static {v5, p0}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v4

    const/4 v6, 0x5

    invoke-static {v6}, Lf/h/c/n/d/p/c;->e(I)I

    move-result v7

    invoke-static {v4}, Lf/h/c/n/d/p/c;->d(I)I

    move-result v8

    add-int/2addr v8, v7

    add-int/2addr v8, v4

    invoke-virtual {v2, v8}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-virtual {v2, v6, v5}, Lf/h/c/n/d/p/c;->q(II)V

    invoke-virtual {v2, v4}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-virtual {v2, v5, p0}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception p0

    goto :goto_0

    :catchall_1
    move-exception p0

    move-object v3, v2

    :goto_0
    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw p0
.end method

.method public static e(Ljava/io/InputStream;Lf/h/c/n/d/p/c;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-array v0, p2, [B

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p2, :cond_0

    sub-int v3, p2, v2

    invoke-virtual {p0, v0, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    if-ltz v3, :cond_0

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget p0, p1, Lf/h/c/n/d/p/c;->e:I

    iget v2, p1, Lf/h/c/n/d/p/c;->f:I

    sub-int/2addr p0, v2

    if-lt p0, p2, :cond_1

    iget-object p0, p1, Lf/h/c/n/d/p/c;->d:[B

    invoke-static {v0, v1, p0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p0, p1, Lf/h/c/n/d/p/c;->f:I

    add-int/2addr p0, p2

    iput p0, p1, Lf/h/c/n/d/p/c;->f:I

    goto :goto_1

    :cond_1
    iget-object v3, p1, Lf/h/c/n/d/p/c;->d:[B

    invoke-static {v0, v1, v3, v2, p0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, p0, 0x0

    sub-int/2addr p2, p0

    iget p0, p1, Lf/h/c/n/d/p/c;->e:I

    iput p0, p1, Lf/h/c/n/d/p/c;->f:I

    invoke-virtual {p1}, Lf/h/c/n/d/p/c;->j()V

    iget p0, p1, Lf/h/c/n/d/p/c;->e:I

    if-gt p2, p0, :cond_2

    iget-object p0, p1, Lf/h/c/n/d/p/c;->d:[B

    invoke-static {v0, v2, p0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput p2, p1, Lf/h/c/n/d/p/c;->f:I

    goto :goto_1

    :cond_2
    iget-object p0, p1, Lf/h/c/n/d/p/c;->g:Ljava/io/OutputStream;

    invoke-virtual {p0, v0, v2, p2}, Ljava/io/OutputStream;->write([BII)V

    :goto_1
    return-void
.end method

.method public static j()J
    .locals 4

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static o(Ljava/io/File;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    const/16 v1, 0x23

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;
    .locals 0

    invoke-virtual {p0, p1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/io/File;

    :cond_0
    return-object p0
.end method

.method public static t(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string v0, "-"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static x(Lf/h/c/n/d/p/c;[Ljava/io/File;Ljava/lang/String;)V
    .locals 10

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    sget-object v1, Lf/h/c/n/d/k/h;->c:Ljava/util/Comparator;

    invoke-static {p1, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, p1, v3

    :try_start_0
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Found Non Fatal for session ID %s in %s "

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p2, v7, v2

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-static {p0, v4}, Lf/h/c/n/d/k/x;->A(Lf/h/c/n/d/p/c;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    const/4 v5, 0x6

    invoke-virtual {v0, v5}, Lf/h/c/n/d/b;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "FirebaseCrashlytics"

    const-string v6, "Error writting non-fatal to session."

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final d(Lf/h/c/n/d/p/b;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lf/h/c/n/d/p/b;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FirebaseCrashlytics"

    const-string v1, "Error closing session file stream in the presence of an exception"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method

.method public final f(IZ)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    sget-object v4, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    add-int/lit8 v0, v3, 0x8

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->s()[Ljava/io/File;

    move-result-object v6

    array-length v7, v6

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v0, :cond_0

    aget-object v8, v6, v7

    invoke-static {v8}, Lf/h/c/n/d/k/x;->o(Ljava/io/File;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lf/h/c/n/d/k/x;->m:Lf/h/c/n/d/l/b;

    iget-object v0, v0, Lf/h/c/n/d/l/b;->b:Lf/h/c/n/d/l/b$b;

    check-cast v0, Lf/h/c/n/d/k/x$j;

    invoke-virtual {v0}, Lf/h/c/n/d/k/x$j;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v6, v0

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_3

    aget-object v8, v0, v7

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".temp"

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_1

    goto :goto_2

    :cond_1
    const/16 v11, 0x14

    invoke-virtual {v9, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    :goto_2
    invoke-virtual {v5, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    new-instance v0, Lf/h/c/n/d/k/x$f;

    const/4 v6, 0x0

    invoke-direct {v0, v6}, Lf/h/c/n/d/k/x$f;-><init>(Lf/h/c/n/d/k/x$a;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v7

    invoke-static {v7, v0}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    array-length v7, v0

    const/4 v8, 0x0

    :goto_3
    const/4 v9, 0x1

    if-ge v8, v7, :cond_6

    aget-object v10, v0, v8

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lf/h/c/n/d/k/x;->D:Ljava/util/regex/Pattern;

    invoke-virtual {v12, v11}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/regex/Matcher;->matches()Z

    move-result v13

    if-nez v13, :cond_4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Deleting unknown file: "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto :goto_4

    :cond_4
    invoke-virtual {v12, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Trimming session file: "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_5
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->s()[Ljava/io/File;

    move-result-object v5

    array-length v0, v5

    if-gt v0, v3, :cond_7

    const-string v0, "No open sessions to be closed."

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    return-void

    :cond_7
    aget-object v0, v5, v3

    invoke-static {v0}, Lf/h/c/n/d/k/x;->o(Ljava/io/File;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v1, Lf/h/c/n/d/k/x;->e:Lf/h/c/n/d/k/g1;

    goto :goto_6

    :cond_8
    new-instance v0, Lf/h/c/n/d/k/a1;

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v8

    invoke-direct {v0, v8}, Lf/h/c/n/d/k/a1;-><init>(Ljava/io/File;)V

    const-string v8, "Failed to close user metadata file."

    invoke-virtual {v0, v7}, Lf/h/c/n/d/k/a1;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_9

    new-instance v0, Lf/h/c/n/d/k/g1;

    invoke-direct {v0}, Lf/h/c/n/d/k/g1;-><init>()V

    goto :goto_6

    :cond_9
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {v9}, Lf/h/c/n/d/k/h;->w(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/h/c/n/d/k/a1;->c(Ljava/lang/String;)Lf/h/c/n/d/k/g1;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v9, v8}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    goto :goto_6

    :catchall_0
    move-exception v0

    goto/16 :goto_26

    :catch_0
    move-exception v0

    move-object v6, v9

    goto :goto_5

    :catchall_1
    move-exception v0

    goto/16 :goto_25

    :catch_1
    move-exception v0

    :goto_5
    :try_start_2
    const-string v9, "Error deserializing user metadata."

    const/4 v10, 0x6

    invoke-virtual {v4, v10}, Lf/h/c/n/d/b;->a(I)Z

    move-result v10

    if-eqz v10, :cond_a

    const-string v10, "FirebaseCrashlytics"

    invoke-static {v10, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_a
    invoke-static {v6, v8}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    new-instance v0, Lf/h/c/n/d/k/g1;

    invoke-direct {v0}, Lf/h/c/n/d/k/g1;-><init>()V

    :goto_6
    new-instance v6, Lf/h/c/n/d/k/z;

    invoke-direct {v6, v1, v0}, Lf/h/c/n/d/k/z;-><init>(Lf/h/c/n/d/k/x;Lf/h/c/n/d/k/g1;)V

    const-string v0, "SessionUser"

    invoke-virtual {v1, v7, v0, v6}, Lf/h/c/n/d/k/x;->z(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/x$g;)V

    iget-object v0, v1, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    invoke-interface {v0, v7}, Lf/h/c/n/d/a;->e(Ljava/lang/String;)Z

    move-result v0

    const-string v6, "report"

    const-string v8, "user"

    if-eqz v0, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Finalizing native report for session "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, v1, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    invoke-interface {v0, v7}, Lf/h/c/n/d/a;->b(Ljava/lang/String;)Lf/h/c/n/d/d;

    move-result-object v0

    invoke-interface {v0}, Lf/h/c/n/d/d;->d()Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_11

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_b

    goto/16 :goto_c

    :cond_b
    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    new-instance v11, Lf/h/c/n/d/l/b;

    iget-object v12, v1, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    iget-object v13, v1, Lf/h/c/n/d/k/x;->l:Lf/h/c/n/d/k/x$j;

    invoke-direct {v11, v12, v13, v7}, Lf/h/c/n/d/l/b;-><init>(Landroid/content/Context;Lf/h/c/n/d/l/b$b;Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->m()Ljava/io/File;

    move-result-object v13

    invoke-direct {v12, v13, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z

    move-result v13

    if-nez v13, :cond_c

    const-string v0, "Couldn\'t create native sessions directory"

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_c
    invoke-virtual {v1, v9, v10}, Lf/h/c/n/d/k/x;->g(J)V

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v9

    iget-object v10, v11, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    invoke-interface {v10}, Lf/h/c/n/d/l/a;->c()[B

    move-result-object v10

    new-instance v13, Lf/h/c/n/d/k/a1;

    invoke-direct {v13, v9}, Lf/h/c/n/d/k/a1;-><init>(Ljava/io/File;)V

    invoke-virtual {v13, v7}, Lf/h/c/n/d/k/a1;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v13, v7}, Lf/h/c/n/d/k/a1;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v13

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Lf/h/c/n/d/k/f;

    const-string v3, "logs_file"

    const-string v2, "logs"

    invoke-direct {v15, v3, v2, v10}, Lf/h/c/n/d/k/f;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/h/c/n/d/k/v0;

    invoke-interface {v0}, Lf/h/c/n/d/d;->f()Ljava/io/File;

    move-result-object v3

    const-string v10, "crash_meta_file"

    const-string v15, "metadata"

    invoke-direct {v2, v10, v15, v3}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/h/c/n/d/k/v0;

    invoke-interface {v0}, Lf/h/c/n/d/d;->e()Ljava/io/File;

    move-result-object v3

    const-string v10, "session_meta_file"

    const-string v15, "session"

    invoke-direct {v2, v10, v15, v3}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/h/c/n/d/k/v0;

    invoke-interface {v0}, Lf/h/c/n/d/d;->a()Ljava/io/File;

    move-result-object v3

    const-string v10, "app_meta_file"

    const-string v15, "app"

    invoke-direct {v2, v10, v15, v3}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/h/c/n/d/k/v0;

    invoke-interface {v0}, Lf/h/c/n/d/d;->c()Ljava/io/File;

    move-result-object v3

    const-string v10, "device_meta_file"

    const-string v15, "device"

    invoke-direct {v2, v10, v15, v3}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/h/c/n/d/k/v0;

    invoke-interface {v0}, Lf/h/c/n/d/d;->b()Ljava/io/File;

    move-result-object v3

    const-string v10, "os_meta_file"

    const-string v15, "os"

    invoke-direct {v2, v10, v15, v3}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/h/c/n/d/k/v0;

    invoke-interface {v0}, Lf/h/c/n/d/d;->d()Ljava/io/File;

    move-result-object v0

    const-string v3, "minidump_file"

    const-string v10, "minidump"

    invoke-direct {v2, v3, v10, v0}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lf/h/c/n/d/k/v0;

    const-string v2, "user_meta_file"

    invoke-direct {v0, v2, v8, v9}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lf/h/c/n/d/k/v0;

    const-string v2, "keys_file"

    const-string v3, "keys"

    invoke-direct {v0, v2, v3, v13}, Lf/h/c/n/d/k/v0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/c/n/d/k/b1;

    :try_start_3
    invoke-interface {v2}, Lf/h/c/n/d/k/b1;->i()Ljava/io/InputStream;

    move-result-object v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    if-nez v3, :cond_d

    goto :goto_9

    :cond_d
    :try_start_4
    new-instance v9, Ljava/io/File;

    invoke-interface {v2}, Lf/h/c/n/d/k/b1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v12, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v3, v9}, Lf/h/a/f/f/n/g;->H(Ljava/io/InputStream;Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_9

    :catchall_2
    move-exception v0

    goto :goto_8

    :catchall_3
    move-exception v0

    const/4 v3, 0x0

    :goto_8
    invoke-static {v3}, Lf/h/c/n/d/k/h;->d(Ljava/io/Closeable;)V

    throw v0

    :catch_2
    const/4 v3, 0x0

    :catch_3
    :goto_9
    invoke-static {v3}, Lf/h/c/n/d/k/h;->d(Ljava/io/Closeable;)V

    goto :goto_7

    :cond_e
    iget-object v0, v1, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    const-string v2, "-"

    const-string v3, ""

    invoke-virtual {v7, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_f
    :goto_a
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lf/h/c/n/d/k/b1;

    invoke-interface {v10}, Lf/h/c/n/d/k/b1;->b()Lf/h/c/n/d/m/v$c$a;

    move-result-object v10

    if-eqz v10, :cond_f

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_10
    iget-object v0, v0, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    new-instance v9, Lf/h/c/n/d/m/w;

    invoke-direct {v9, v3}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    new-instance v3, Lf/h/c/n/d/m/d;

    const/4 v10, 0x0

    invoke-direct {v3, v9, v10, v10}, Lf/h/c/n/d/m/d;-><init>(Lf/h/c/n/d/m/w;Ljava/lang/String;Lf/h/c/n/d/m/d$a;)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v9, Ljava/io/File;

    invoke-virtual {v0, v2}, Lf/h/c/n/d/o/g;->h(Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    invoke-direct {v9, v10, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iget-object v0, v0, Lf/h/c/n/d/o/g;->e:Ljava/io/File;

    :try_start_5
    sget-object v10, Lf/h/c/n/d/o/g;->i:Lf/h/c/n/d/m/x/h;

    invoke-static {v9}, Lf/h/c/n/d/o/g;->j(Ljava/io/File;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Lf/h/c/n/d/m/x/h;->f(Ljava/lang/String;)Lf/h/c/n/d/m/v;

    move-result-object v12

    invoke-virtual {v12}, Lf/h/c/n/d/m/v;->i()Lf/h/c/n/d/m/v$a;

    move-result-object v12

    check-cast v12, Lf/h/c/n/d/m/b$b;

    const/4 v13, 0x0

    iput-object v13, v12, Lf/h/c/n/d/m/b$b;->g:Lf/h/c/n/d/m/v$d;

    iput-object v3, v12, Lf/h/c/n/d/m/b$b;->h:Lf/h/c/n/d/m/v$c;

    invoke-virtual {v12}, Lf/h/c/n/d/m/b$b;->a()Lf/h/c/n/d/m/v;

    move-result-object v3

    new-instance v12, Ljava/io/File;

    invoke-static {v0}, Lf/h/c/n/d/o/g;->i(Ljava/io/File;)Ljava/io/File;

    invoke-direct {v12, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Lf/h/c/n/d/m/x/h;->g(Lf/h/c/n/d/m/v;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v12, v0}, Lf/h/c/n/d/o/g;->l(Ljava/io/File;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_b

    :catch_4
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not synthesize final native report file for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2, v0}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_b
    iget-object v0, v11, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    invoke-interface {v0}, Lf/h/c/n/d/l/a;->d()V

    goto :goto_d

    :cond_11
    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No minidump data found for session "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->g(Ljava/lang/String;)V

    :goto_d
    iget-object v0, v1, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    invoke-interface {v0, v7}, Lf/h/c/n/d/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not finalize native session: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :cond_12
    const-string v0, "Closing open sessions."

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    move/from16 v2, p2

    :goto_e
    array-length v0, v5

    if-ge v2, v0, :cond_1e

    aget-object v0, v5, v2

    invoke-static {v0}, Lf/h/c/n/d/k/x;->o(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Closing session: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Collecting session parts for ID "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v7, Lf/h/c/n/d/k/x$h;

    const-string v9, "SessionCrash"

    invoke-static {v3, v9}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lf/h/c/n/d/k/x$h;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v9

    invoke-static {v9, v7}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v7

    array-length v9, v7

    if-lez v9, :cond_13

    const/4 v9, 0x1

    goto :goto_f

    :cond_13
    const/4 v9, 0x0

    :goto_f
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v11, 0x2

    new-array v12, v11, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v3, v12, v13

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    aput-object v13, v12, v14

    const-string v13, "Session %s has fatal exception: %s"

    invoke-static {v10, v13, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v12, Lf/h/c/n/d/k/x$h;

    const-string v13, "SessionEvent"

    invoke-static {v3, v13}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v14}, Lf/h/c/n/d/k/x$h;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v14

    invoke-static {v14, v12}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v12

    array-length v14, v12

    if-lez v14, :cond_14

    const/4 v14, 0x1

    goto :goto_10

    :cond_14
    const/4 v14, 0x0

    :goto_10
    new-array v11, v11, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v3, v11, v15

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    const/16 v16, 0x1

    aput-object v15, v11, v16

    const-string v15, "Session %s has non-fatal exceptions: %s"

    invoke-static {v10, v15, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    if-nez v9, :cond_16

    if-eqz v14, :cond_15

    goto :goto_11

    :cond_15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No events present for session ID "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    move-object v14, v5

    move-object/from16 v16, v6

    goto/16 :goto_16

    :cond_16
    :goto_11
    array-length v11, v12

    move/from16 v14, p1

    if-le v11, v14, :cond_17

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v15, 0x0

    aput-object v12, v11, v15

    const-string v12, "Trimming down to %d logged exceptions."

    invoke-static {v10, v12, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v14}, Lf/h/c/n/d/k/x;->v(Ljava/lang/String;I)V

    new-instance v10, Lf/h/c/n/d/k/x$h;

    invoke-static {v3, v13}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lf/h/c/n/d/k/x$h;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v11

    invoke-static {v11, v10}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v12

    :cond_17
    if-eqz v9, :cond_18

    const/4 v9, 0x0

    aget-object v7, v7, v9

    goto :goto_12

    :cond_18
    const/4 v7, 0x0

    :goto_12
    const-string v9, "Failed to close CLS file"

    const-string v10, "Error flushing session file stream"

    if-eqz v7, :cond_19

    const/4 v11, 0x1

    goto :goto_13

    :cond_19
    const/4 v11, 0x0

    :goto_13
    if-eqz v11, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->k()Ljava/io/File;

    move-result-object v13

    goto :goto_14

    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->n()Ljava/io/File;

    move-result-object v13

    :goto_14
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_1b

    invoke-virtual {v13}, Ljava/io/File;->mkdirs()Z

    :cond_1b
    :try_start_6
    new-instance v15, Lf/h/c/n/d/p/b;

    invoke-direct {v15, v13, v3}, Lf/h/c/n/d/p/b;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_9
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    :try_start_7
    invoke-static {v15}, Lf/h/c/n/d/p/c;->i(Ljava/io/OutputStream;)Lf/h/c/n/d/p/c;

    move-result-object v13
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    :try_start_8
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-object/from16 v16, v6

    :try_start_9
    const-string v6, "Collecting SessionStart data for session ID "

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    invoke-static {v13, v0}, Lf/h/c/n/d/k/x;->A(Lf/h/c/n/d/p/c;Ljava/io/File;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    move-object v14, v5

    :try_start_a
    invoke-static {}, Lf/h/c/n/d/k/x;->j()J

    move-result-wide v5

    const/4 v0, 0x4

    invoke-virtual {v13, v0, v5, v6}, Lf/h/c/n/d/p/c;->s(IJ)V

    const/4 v0, 0x5

    invoke-virtual {v13, v0, v11}, Lf/h/c/n/d/p/c;->k(IZ)V

    const/16 v0, 0xb

    const/4 v5, 0x1

    invoke-virtual {v13, v0, v5}, Lf/h/c/n/d/p/c;->r(II)V

    const/16 v0, 0xc

    const/4 v5, 0x3

    invoke-virtual {v13, v0, v5}, Lf/h/c/n/d/p/c;->m(II)V

    invoke-virtual {v1, v13, v3}, Lf/h/c/n/d/k/x;->w(Lf/h/c/n/d/p/c;Ljava/lang/String;)V

    invoke-static {v13, v12, v3}, Lf/h/c/n/d/k/x;->x(Lf/h/c/n/d/p/c;[Ljava/io/File;Ljava/lang/String;)V

    if-eqz v11, :cond_1c

    invoke-static {v13, v7}, Lf/h/c/n/d/k/x;->A(Lf/h/c/n/d/p/c;Ljava/io/File;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :cond_1c
    invoke-static {v13, v10}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    invoke-static {v15, v9}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    goto :goto_16

    :catch_5
    move-exception v0

    goto :goto_15

    :catch_6
    move-exception v0

    move-object v14, v5

    goto :goto_15

    :catchall_4
    move-exception v0

    goto/16 :goto_18

    :catch_7
    move-exception v0

    move-object v14, v5

    move-object/from16 v16, v6

    goto :goto_15

    :catchall_5
    move-exception v0

    const/4 v13, 0x0

    goto :goto_18

    :catch_8
    move-exception v0

    move-object v14, v5

    move-object/from16 v16, v6

    const/4 v13, 0x0

    goto :goto_15

    :catchall_6
    move-exception v0

    const/4 v13, 0x0

    const/4 v15, 0x0

    goto :goto_18

    :catch_9
    move-exception v0

    move-object v14, v5

    move-object/from16 v16, v6

    const/4 v13, 0x0

    const/4 v5, 0x0

    move-object v15, v5

    :goto_15
    :try_start_b
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to write session file for session ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lf/h/c/n/d/b;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    invoke-static {v13, v10}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    invoke-virtual {v1, v15}, Lf/h/c/n/d/k/x;->d(Lf/h/c/n/d/p/b;)V

    :goto_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Removing session part files for ID "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v0, Lf/h/c/n/d/k/x$n;

    invoke-direct {v0, v3}, Lf/h/c/n/d/k/x$n;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v3

    invoke-static {v3, v0}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    array-length v3, v0

    const/4 v5, 0x0

    :goto_17
    if-ge v5, v3, :cond_1d

    aget-object v6, v0, v5

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_17

    :cond_1d
    add-int/lit8 v2, v2, 0x1

    move-object v5, v14

    move-object/from16 v6, v16

    goto/16 :goto_e

    :goto_18
    invoke-static {v13, v10}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    invoke-static {v15, v9}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v0

    :cond_1e
    move-object v14, v5

    move-object/from16 v16, v6

    if-eqz p2, :cond_1f

    const/4 v0, 0x0

    aget-object v0, v14, v0

    invoke-static {v0}, Lf/h/c/n/d/k/x;->o(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/h/c/n/d/k/x;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_19

    :cond_1f
    const/4 v0, 0x0

    :goto_19
    iget-object v2, v1, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    invoke-static {}, Lf/h/c/n/d/k/x;->j()J

    move-result-wide v5

    iget-object v2, v2, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    new-instance v3, Lf/h/c/n/d/o/b;

    invoke-direct {v3, v0}, Lf/h/c/n/d/o/b;-><init>(Ljava/lang/String;)V

    iget-object v0, v2, Lf/h/c/n/d/o/g;->b:Ljava/io/File;

    invoke-static {v0, v3}, Lf/h/c/n/d/o/g;->f(Ljava/io/File;Ljava/io/FileFilter;)Ljava/util/List;

    move-result-object v0

    sget-object v3, Lf/h/c/n/d/o/g;->j:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/16 v7, 0x8

    if-gt v3, v7, :cond_20

    goto :goto_1b

    :cond_20
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v0, v7, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_21

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    invoke-static {v9}, Lf/h/c/n/d/o/g;->k(Ljava/io/File;)V

    goto :goto_1a

    :cond_21
    const/4 v3, 0x0

    invoke-interface {v0, v3, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    :goto_1b
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/io/File;

    const-string v0, "Finalizing report for session "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    sget-object v0, Lf/h/c/n/d/o/g;->k:Ljava/io/FilenameFilter;

    invoke-static {v7, v0}, Lf/h/c/n/d/o/g;->g(Ljava/io/File;Ljava/io/FilenameFilter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_22

    const-string v0, "Session "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " has no events."

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto/16 :goto_1f

    :cond_22
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_23
    const/4 v0, 0x0

    const/4 v11, 0x0

    :goto_1d
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Ljava/io/File;

    :try_start_c
    sget-object v0, Lf/h/c/n/d/o/g;->i:Lf/h/c/n/d/m/x/h;

    invoke-static {v12}, Lf/h/c/n/d/o/g;->j(Ljava/io/File;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_b

    :try_start_d
    new-instance v14, Landroid/util/JsonReader;

    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, v13}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v14, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_a
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    :try_start_e
    invoke-static {v14}, Lf/h/c/n/d/m/x/h;->b(Landroid/util/JsonReader;)Lf/h/c/n/d/m/v$d$d;

    move-result-object v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    :try_start_f
    invoke-virtual {v14}, Landroid/util/JsonReader;->close()V
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_f .. :try_end_f} :catch_a
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    :try_start_10
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez v11, :cond_25

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v13, "event"

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_24

    const-string v13, "_"

    invoke-virtual {v0, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    goto :goto_1e

    :cond_24
    const/4 v0, 0x0

    :goto_1e
    if-eqz v0, :cond_23

    :cond_25
    const/4 v11, 0x1

    goto :goto_1d

    :catchall_7
    move-exception v0

    move-object v13, v0

    :try_start_11
    invoke-virtual {v14}, Landroid/util/JsonReader;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    :catchall_8
    :try_start_12
    throw v13
    :try_end_12
    .catch Ljava/lang/IllegalStateException; {:try_start_12 .. :try_end_12} :catch_a
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b

    :catch_a
    move-exception v0

    :try_start_13
    new-instance v13, Ljava/io/IOException;

    invoke-direct {v13, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v13
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b

    :catch_b
    move-exception v0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Could not add event to report for "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12, v0}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1d

    :cond_26
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    const-string v0, "Could not parse event files for session "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :goto_1f
    move-object/from16 v12, v16

    goto/16 :goto_22

    :cond_27
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v10

    if-eqz v10, :cond_28

    :try_start_14
    invoke-static {v0}, Lf/h/c/n/d/o/g;->j(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_c

    goto :goto_20

    :catch_c
    move-exception v0

    move-object v10, v0

    const-string v0, "Could not read user ID file in "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v10}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_28
    const/4 v0, 0x0

    :goto_20
    new-instance v10, Ljava/io/File;

    move-object/from16 v12, v16

    invoke-direct {v10, v7, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v11, :cond_29

    iget-object v13, v2, Lf/h/c/n/d/o/g;->c:Ljava/io/File;

    goto :goto_21

    :cond_29
    iget-object v13, v2, Lf/h/c/n/d/o/g;->d:Ljava/io/File;

    :goto_21
    :try_start_15
    sget-object v14, Lf/h/c/n/d/o/g;->i:Lf/h/c/n/d/m/x/h;

    invoke-static {v10}, Lf/h/c/n/d/o/g;->j(Ljava/io/File;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lf/h/c/n/d/m/x/h;->f(Ljava/lang/String;)Lf/h/c/n/d/m/v;

    move-result-object v15

    invoke-virtual {v15, v5, v6, v11, v0}, Lf/h/c/n/d/m/v;->j(JZLjava/lang/String;)Lf/h/c/n/d/m/v;

    move-result-object v0

    new-instance v11, Lf/h/c/n/d/m/w;

    invoke-direct {v11, v9}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    invoke-virtual {v0}, Lf/h/c/n/d/m/v;->h()Lf/h/c/n/d/m/v$d;

    move-result-object v9

    if-eqz v9, :cond_2b

    invoke-virtual {v0}, Lf/h/c/n/d/m/v;->i()Lf/h/c/n/d/m/v$a;

    move-result-object v9

    invoke-virtual {v0}, Lf/h/c/n/d/m/v;->h()Lf/h/c/n/d/m/v$d;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/n/d/m/v$d;->l()Lf/h/c/n/d/m/v$d$b;

    move-result-object v0

    check-cast v0, Lf/h/c/n/d/m/f$b;

    iput-object v11, v0, Lf/h/c/n/d/m/f$b;->j:Lf/h/c/n/d/m/w;

    invoke-virtual {v0}, Lf/h/c/n/d/m/f$b;->a()Lf/h/c/n/d/m/v$d;

    move-result-object v0

    check-cast v9, Lf/h/c/n/d/m/b$b;

    iput-object v0, v9, Lf/h/c/n/d/m/b$b;->g:Lf/h/c/n/d/m/v$d;

    invoke-virtual {v9}, Lf/h/c/n/d/m/b$b;->a()Lf/h/c/n/d/m/v;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/n/d/m/v;->h()Lf/h/c/n/d/m/v$d;

    move-result-object v9

    if-nez v9, :cond_2a

    goto :goto_22

    :cond_2a
    new-instance v11, Ljava/io/File;

    invoke-static {v13}, Lf/h/c/n/d/o/g;->i(Ljava/io/File;)Ljava/io/File;

    invoke-virtual {v9}, Lf/h/c/n/d/m/v$d;->g()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v11, v13, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v14, v0}, Lf/h/c/n/d/m/x/h;->g(Lf/h/c/n/d/m/v;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lf/h/c/n/d/o/g;->l(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_22

    :cond_2b
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v9, "Reports without sessions cannot have events added to them."

    invoke-direct {v0, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_d

    :catch_d
    move-exception v0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Could not synthesize final report file for "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9, v0}, Lf/h/c/n/d/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_22
    invoke-static {v7}, Lf/h/c/n/d/o/g;->k(Ljava/io/File;)V

    move-object/from16 v16, v12

    goto/16 :goto_1c

    :cond_2c
    iget-object v0, v2, Lf/h/c/n/d/o/g;->f:Lf/h/c/n/d/s/e;

    check-cast v0, Lf/h/c/n/d/s/d;

    invoke-virtual {v0}, Lf/h/c/n/d/s/d;->c()Lf/h/c/n/d/s/i/e;

    move-result-object v0

    invoke-interface {v0}, Lf/h/c/n/d/s/i/e;->b()Lf/h/c/n/d/s/i/d;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Lf/h/c/n/d/o/g;->e()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x4

    if-gt v2, v3, :cond_2d

    goto :goto_24

    :cond_2d
    invoke-virtual {v0, v3, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_23
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_23

    :cond_2e
    :goto_24
    return-void

    :goto_25
    move-object v9, v6

    :goto_26
    invoke-static {v9, v8}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v0
.end method

.method public final g(J)V
    .locals 4

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ".ae"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object p1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string p2, "Could not write app exception marker."

    invoke-virtual {p1, p2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public h(I)Z
    .locals 3

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v1, p0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    invoke-virtual {v1}, Lf/h/c/n/d/k/i;->a()V

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->p()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string p1, "Skipping session finalization because a crash has already occurred."

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    return v2

    :cond_0
    const-string v1, "Finalizing previously open sessions."

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v1}, Lf/h/c/n/d/k/x;->f(IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string p1, "Closed all previously open sessions"

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    return v1

    :catch_0
    move-exception p1

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FirebaseCrashlytics"

    const-string v1, "Unable to finalize previously open sessions."

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    return v2
.end method

.method public final i()Ljava/lang/String;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->s()[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lf/h/c/n/d/k/x;->o(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public k()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v1

    const-string v2, "fatal-sessions"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public l()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lf/h/c/n/d/k/x;->i:Lf/h/c/n/d/o/h;

    invoke-virtual {v0}, Lf/h/c/n/d/o/h;->a()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v1

    const-string v2, "native-sessions"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public n()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v1

    const-string v2, "nonfatal-sessions"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lf/h/c/n/d/k/x;->u:Lf/h/c/n/d/k/p0;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lf/h/c/n/d/k/p0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public q()[Ljava/io/File;
    .locals 4

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->k()Ljava/io/File;

    move-result-object v1

    sget-object v2, Lf/h/c/n/d/k/x;->A:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    new-array v1, v3, [Ljava/io/File;

    :cond_0
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->n()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    new-array v1, v3, [Ljava/io/File;

    :cond_1
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v1

    invoke-static {v1, v2}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/io/File;

    return-object v0
.end method

.method public final s()[Ljava/io/File;
    .locals 2

    sget-object v0, Lf/h/c/n/d/k/x;->z:Ljava/io/FilenameFilter;

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v1

    invoke-static {v1, v0}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    sget-object v1, Lf/h/c/n/d/k/x;->B:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public u(FLcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Lcom/google/android/gms/tasks/Task<",
            "Lf/h/c/n/d/s/i/b;",
            ">;)",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v2, p0, Lf/h/c/n/d/k/x;->n:Lf/h/c/n/d/q/a;

    iget-object v3, v2, Lf/h/c/n/d/q/a;->a:Lf/h/c/n/d/q/b$c;

    check-cast v3, Lf/h/c/n/d/k/x$k;

    iget-object v3, v3, Lf/h/c/n/d/k/x$k;->a:Lf/h/c/n/d/k/x;

    invoke-virtual {v3}, Lf/h/c/n/d/k/x;->q()[Ljava/io/File;

    move-result-object v3

    iget-object v2, v2, Lf/h/c/n/d/q/a;->a:Lf/h/c/n/d/q/b$c;

    check-cast v2, Lf/h/c/n/d/k/x$k;

    iget-object v2, v2, Lf/h/c/n/d/k/x$k;->a:Lf/h/c/n/d/k/x;

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->m()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    const/4 v4, 0x0

    if-nez v2, :cond_0

    new-array v2, v4, [Ljava/io/File;

    :cond_0
    if-eqz v3, :cond_1

    array-length v3, v3

    if-lez v3, :cond_1

    goto :goto_0

    :cond_1
    array-length v2, v2

    if-lez v2, :cond_2

    :goto_0
    const/4 v4, 0x1

    :cond_2
    if-nez v4, :cond_3

    const-string p1, "No reports are available."

    invoke-virtual {v1, p1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object p1, p0, Lf/h/c/n/d/k/x;->v:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :cond_3
    const-string v2, "Unsent reports are available."

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, p0, Lf/h/c/n/d/k/x;->c:Lf/h/c/n/d/k/q0;

    invoke-virtual {v3}, Lf/h/c/n/d/k/q0;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "Automatic data collection is enabled. Allowing upload."

    invoke-virtual {v1, v3}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lf/h/c/n/d/k/x;->v:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "Automatic data collection is disabled."

    invoke-virtual {v1, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const-string v0, "Notifying that unsent reports are available."

    invoke-virtual {v1, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/c/n/d/k/x;->v:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    iget-object v0, p0, Lf/h/c/n/d/k/x;->c:Lf/h/c/n/d/k/q0;

    iget-object v2, v0, Lf/h/c/n/d/k/q0;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, v0, Lf/h/c/n/d/k/q0;->d:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v2, Lf/h/c/n/d/k/e0;

    invoke-direct {v2, p0}, Lf/h/c/n/d/k/e0;-><init>(Lf/h/c/n/d/k/x;)V

    invoke-virtual {v0, v2}, Lf/h/a/f/p/b0;->q(Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    const-string v2, "Waiting for send/deleteUnsentReports to be called."

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lf/h/c/n/d/k/x;->w:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, v1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    sget-object v2, Lf/h/c/n/d/k/i1;->a:Ljava/io/FilenameFilter;

    new-instance v2, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v3, Lf/h/c/n/d/k/j1;

    invoke-direct {v3, v2}, Lf/h/c/n/d/k/j1;-><init>(Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/tasks/Task;->h(Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v1, v3}, Lf/h/a/f/p/b0;->h(Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    iget-object v0, v2, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    :goto_1
    new-instance v1, Lf/h/c/n/d/k/x$e;

    invoke-direct {v1, p0, p2, p1}, Lf/h/c/n/d/k/x$e;-><init>(Lf/h/c/n/d/k/x;Lcom/google/android/gms/tasks/Task;F)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->q(Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final v(Ljava/lang/String;I)V
    .locals 3

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v0

    new-instance v1, Lf/h/c/n/d/k/x$h;

    const-string v2, "SessionEvent"

    invoke-static {p1, v2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lf/h/c/n/d/k/x$h;-><init>(Ljava/lang/String;)V

    sget-object p1, Lf/h/c/n/d/k/x;->C:Ljava/util/Comparator;

    invoke-static {v0, v1, p2, p1}, Lf/h/c/n/d/k/i1;->b(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator;)I

    return-void
.end method

.method public final w(Lf/h/c/n/d/p/c;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    sget-object v1, Lf/h/c/n/d/k/x;->F:[Ljava/lang/String;

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, v1, v4

    new-instance v6, Lf/h/c/n/d/k/x$h;

    const-string v7, ".cls"

    invoke-static {p2, v5, v7}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lf/h/c/n/d/k/x$h;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v7

    invoke-static {v7, v6}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v6

    array-length v7, v6

    const-string v8, " data for session ID "

    if-nez v7, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t find "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Collecting "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    aget-object v5, v6, v3

    invoke-static {p1, v5}, Lf/h/c/n/d/k/x;->A(Lf/h/c/n/d/p/c;Ljava/io/File;)V

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final y(Lf/h/c/n/d/p/c;Ljava/lang/Thread;Ljava/lang/Throwable;JLjava/lang/String;Z)V
    .locals 33
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p4

    new-instance v15, Lf/h/c/n/d/t/e;

    iget-object v4, v0, Lf/h/c/n/d/k/x;->q:Lf/h/c/n/d/t/d;

    move-object/from16 v5, p3

    invoke-direct {v15, v5, v4}, Lf/h/c/n/d/t/e;-><init>(Ljava/lang/Throwable;Lf/h/c/n/d/t/d;)V

    iget-object v4, v0, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    invoke-static {v4}, Lf/h/c/n/d/k/e;->a(Landroid/content/Context;)Lf/h/c/n/d/k/e;

    move-result-object v5

    iget-object v14, v5, Lf/h/c/n/d/k/e;->a:Ljava/lang/Float;

    invoke-virtual {v5}, Lf/h/c/n/d/k/e;->b()I

    move-result v16

    invoke-static {v4}, Lf/h/c/n/d/k/h;->m(Landroid/content/Context;)Z

    move-result v13

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v12, v5, Landroid/content/res/Configuration;->orientation:I

    invoke-static {}, Lf/h/c/n/d/k/h;->p()J

    move-result-wide v5

    new-instance v7, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v7}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    const-string v8, "activity"

    invoke-virtual {v4, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager;

    invoke-virtual {v8, v7}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v7, v7, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    sub-long v10, v5, v7

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lf/h/c/n/d/k/h;->a(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lf/h/c/n/d/k/h;->i(Ljava/lang/String;Landroid/content/Context;)Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-result-object v7

    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    iget-object v5, v15, Lf/h/c/n/d/t/e;->c:[Ljava/lang/StackTraceElement;

    move-object/from16 p3, v5

    iget-object v5, v0, Lf/h/c/n/d/k/x;->j:Lf/h/c/n/d/k/b;

    iget-object v5, v5, Lf/h/c/n/d/k/b;->b:Ljava/lang/String;

    move-object/from16 v17, v7

    iget-object v7, v0, Lf/h/c/n/d/k/x;->h:Lf/h/c/n/d/k/w0;

    iget-object v7, v7, Lf/h/c/n/d/k/w0;->c:Ljava/lang/String;

    move-wide/from16 v18, v8

    if-eqz p7, :cond_1

    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Map;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/Thread;

    invoke-interface/range {v20 .. v20}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v20

    const/16 v22, 0x0

    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/util/Map$Entry;

    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Thread;

    aput-object v24, v8, v22

    iget-object v9, v0, Lf/h/c/n/d/k/x;->q:Lf/h/c/n/d/t/d;

    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v23

    move-object/from16 p7, v8

    move-object/from16 v8, v23

    check-cast v8, [Ljava/lang/StackTraceElement;

    invoke-interface {v9, v8}, Lf/h/c/n/d/t/d;->a([Ljava/lang/StackTraceElement;)[Ljava/lang/StackTraceElement;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x1

    add-int/lit8 v22, v22, 0x1

    move-object/from16 v8, p7

    goto :goto_0

    :cond_0
    move-object/from16 p7, v8

    move-object/from16 v9, p7

    goto :goto_1

    :cond_1
    const/4 v9, 0x0

    new-array v8, v9, [Ljava/lang/Thread;

    move-object v9, v8

    :goto_1
    const-string v8, "com.crashlytics.CollectCustomKeys"

    move-object/from16 v20, v6

    const/4 v6, 0x1

    invoke-static {v4, v8, v6}, Lf/h/c/n/d/k/h;->j(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    goto :goto_2

    :cond_2
    iget-object v4, v0, Lf/h/c/n/d/k/x;->e:Lf/h/c/n/d/k/g1;

    invoke-virtual {v4}, Lf/h/c/n/d/k/g1;->a()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v8

    if-le v8, v6, :cond_3

    new-instance v6, Ljava/util/TreeMap;

    invoke-direct {v6, v4}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    move-object/from16 v22, v6

    goto :goto_3

    :cond_3
    :goto_2
    move-object/from16 v22, v4

    :goto_3
    iget-object v4, v0, Lf/h/c/n/d/k/x;->m:Lf/h/c/n/d/l/b;

    iget-object v4, v4, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    invoke-interface {v4}, Lf/h/c/n/d/l/a;->c()[B

    move-result-object v4

    sget-object v6, Lf/h/c/n/d/p/d;->a:Lf/h/c/n/d/p/a;

    invoke-static {v7}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v8

    const-string v7, ""

    if-nez v5, :cond_4

    const/4 v6, 0x0

    goto :goto_4

    :cond_4
    const-string v6, "-"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v5

    move-object v6, v5

    :goto_4
    if-eqz v4, :cond_5

    array-length v5, v4

    move-object/from16 v23, v6

    new-array v6, v5, [B

    move-object/from16 v25, v7

    const/4 v7, 0x0

    invoke-static {v4, v7, v6, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v4, Lf/h/c/n/d/p/a;

    invoke-direct {v4, v6}, Lf/h/c/n/d/p/a;-><init>([B)V

    move-object v7, v4

    goto :goto_5

    :cond_5
    move-object/from16 v23, v6

    move-object/from16 v25, v7

    sget-object v4, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v5, "No log data to include with this event."

    invoke-virtual {v4, v5}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/4 v7, 0x0

    :goto_5
    const/16 v4, 0xa

    const/4 v6, 0x2

    invoke-virtual {v1, v4, v6}, Lf/h/c/n/d/p/c;->q(II)V

    const/4 v5, 0x1

    invoke-static {v5, v2, v3}, Lf/h/c/n/d/p/c;->g(IJ)I

    move-result v4

    const/16 v21, 0x0

    add-int/lit8 v4, v4, 0x0

    invoke-static/range {p6 .. p6}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v5

    invoke-static {v6, v5}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v5

    add-int v26, v5, v4

    const/16 v27, 0x8

    move-object v4, v15

    move-object/from16 p7, p3

    const/16 v24, 0x1

    move-object/from16 v5, p2

    move-object/from16 p3, v20

    const/4 v0, 0x2

    move-object/from16 v6, p7

    move-object v0, v7

    move-object/from16 v20, v17

    move-object/from16 v17, v25

    move-object v7, v9

    move-object/from16 v21, v8

    move-object/from16 v8, p3

    move-object/from16 v24, v9

    move-object/from16 v28, v15

    const/4 v15, 0x1

    move/from16 v9, v27

    move-wide/from16 v29, v10

    move-object/from16 v10, v21

    move-object/from16 v11, v23

    move/from16 v27, v12

    move-object/from16 v12, v22

    move/from16 v31, v13

    move-object/from16 v13, v20

    move-object/from16 v32, v14

    move/from16 v14, v27

    invoke-static/range {v4 .. v14}, Lf/h/c/n/d/p/d;->f(Lf/h/c/n/d/t/e;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;[Ljava/lang/Thread;Ljava/util/List;ILf/h/c/n/d/p/a;Lf/h/c/n/d/p/a;Ljava/util/Map;Landroid/app/ActivityManager$RunningAppProcessInfo;I)I

    move-result v4

    const/4 v14, 0x3

    invoke-static {v14}, Lf/h/c/n/d/p/c;->e(I)I

    move-result v5

    invoke-static {v4}, Lf/h/c/n/d/p/c;->d(I)I

    move-result v6

    add-int/2addr v6, v5

    add-int/2addr v6, v4

    add-int v4, v6, v26

    move-object/from16 v6, v32

    move/from16 v7, v16

    move/from16 v8, v31

    move/from16 v9, v27

    move-wide/from16 v10, v29

    move-wide/from16 v12, v18

    invoke-static/range {v6 .. v13}, Lf/h/c/n/d/p/d;->g(Ljava/lang/Float;IZIJJ)I

    move-result v5

    const/4 v13, 0x5

    invoke-static {v13}, Lf/h/c/n/d/p/c;->e(I)I

    move-result v6

    invoke-static {v5}, Lf/h/c/n/d/p/c;->d(I)I

    move-result v7

    add-int/2addr v7, v6

    add-int/2addr v7, v5

    add-int/2addr v7, v4

    const/4 v12, 0x6

    if-eqz v0, :cond_6

    invoke-static {v15, v0}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v4

    invoke-static {v12}, Lf/h/c/n/d/p/c;->e(I)I

    move-result v5

    invoke-static {v4}, Lf/h/c/n/d/p/c;->d(I)I

    move-result v6

    add-int/2addr v6, v5

    add-int/2addr v6, v4

    add-int/2addr v7, v6

    :cond_6
    invoke-virtual {v1, v7}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-virtual {v1, v15, v2, v3}, Lf/h/c/n/d/p/c;->s(IJ)V

    invoke-static/range {p6 .. p6}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {v1, v14, v3}, Lf/h/c/n/d/p/c;->q(II)V

    const/16 v26, 0x8

    const/16 v7, 0x8

    move-object/from16 v2, v28

    move-object/from16 v3, p2

    move-object/from16 v4, p7

    move-object/from16 v5, v24

    move-object/from16 v6, p3

    move-object/from16 v8, v21

    move-object/from16 v9, v23

    move-object/from16 v10, v22

    move-object/from16 v11, v20

    move/from16 v12, v27

    invoke-static/range {v2 .. v12}, Lf/h/c/n/d/p/d;->f(Lf/h/c/n/d/t/e;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;[Ljava/lang/Thread;Ljava/util/List;ILf/h/c/n/d/p/a;Lf/h/c/n/d/p/a;Ljava/util/Map;Landroid/app/ActivityManager$RunningAppProcessInfo;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lf/h/c/n/d/p/c;->o(I)V

    const/4 v2, 0x2

    invoke-virtual {v1, v15, v2}, Lf/h/c/n/d/p/c;->q(II)V

    move-object/from16 v2, v28

    move/from16 v7, v26

    invoke-static/range {v2 .. v9}, Lf/h/c/n/d/p/d;->e(Lf/h/c/n/d/t/e;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;[Ljava/lang/Thread;Ljava/util/List;ILf/h/c/n/d/p/a;Lf/h/c/n/d/p/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Lf/h/c/n/d/p/c;->o(I)V

    const/4 v2, 0x4

    invoke-static {v1, v3, v4, v2, v15}, Lf/h/c/n/d/p/d;->m(Lf/h/c/n/d/p/c;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)V

    move-object/from16 v8, v24

    array-length v3, v8

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v3, :cond_7

    aget-object v5, v8, v4

    move-object/from16 v6, p3

    invoke-virtual {v6, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/StackTraceElement;

    const/4 v9, 0x0

    invoke-static {v1, v5, v7, v9, v9}, Lf/h/c/n/d/p/d;->m(Lf/h/c/n/d/p/c;Ljava/lang/Thread;[Ljava/lang/StackTraceElement;IZ)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_7
    const/4 v9, 0x0

    const/16 v3, 0x8

    move-object/from16 v4, v28

    const/4 v5, 0x2

    invoke-static {v1, v4, v15, v3, v5}, Lf/h/c/n/d/p/d;->l(Lf/h/c/n/d/p/c;Lf/h/c/n/d/t/e;III)V

    invoke-virtual {v1, v14, v5}, Lf/h/c/n/d/p/c;->q(II)V

    invoke-static {}, Lf/h/c/n/d/p/d;->d()I

    move-result v3

    invoke-virtual {v1, v3}, Lf/h/c/n/d/p/c;->o(I)V

    sget-object v3, Lf/h/c/n/d/p/d;->a:Lf/h/c/n/d/p/a;

    invoke-virtual {v1, v15, v3}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {v1, v5, v3}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v14, v3, v4}, Lf/h/c/n/d/p/c;->s(IJ)V

    invoke-virtual {v1, v2, v5}, Lf/h/c/n/d/p/c;->q(II)V

    move-object/from16 v6, v21

    move-object/from16 v7, v23

    invoke-static {v6, v7}, Lf/h/c/n/d/p/d;->a(Lf/h/c/n/d/p/a;Lf/h/c/n/d/p/a;)I

    move-result v8

    invoke-virtual {v1, v8}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-virtual {v1, v15, v3, v4}, Lf/h/c/n/d/p/c;->s(IJ)V

    invoke-virtual {v1, v5, v3, v4}, Lf/h/c/n/d/p/c;->s(IJ)V

    invoke-virtual {v1, v14, v6}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    if-eqz v7, :cond_8

    invoke-virtual {v1, v2, v7}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    :cond_8
    if-eqz v22, :cond_a

    invoke-interface/range {v22 .. v22}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    invoke-interface/range {v22 .. v22}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    const/4 v5, 0x2

    invoke-virtual {v1, v5, v5}, Lf/h/c/n/d/p/c;->q(II)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v5, v6}, Lf/h/c/n/d/p/d;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v5

    invoke-virtual {v1, v15, v5}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Ljava/lang/String;

    if-nez v7, :cond_9

    move-object/from16 v7, v17

    :cond_9
    invoke-static {v7}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v1, v5, v4}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    goto :goto_7

    :cond_a
    move-object/from16 v3, v20

    if-eqz v3, :cond_c

    iget v3, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v4, 0x64

    if-eq v3, v4, :cond_b

    const/4 v9, 0x1

    :cond_b
    invoke-virtual {v1, v14, v9}, Lf/h/c/n/d/p/c;->k(IZ)V

    :cond_c
    move/from16 v3, v27

    invoke-virtual {v1, v2, v3}, Lf/h/c/n/d/p/c;->r(II)V

    const/4 v4, 0x2

    invoke-virtual {v1, v13, v4}, Lf/h/c/n/d/p/c;->q(II)V

    move-object/from16 v6, v32

    move/from16 v7, v16

    move/from16 v8, v31

    move v9, v3

    move-wide/from16 v10, v29

    const/4 v4, 0x5

    move-wide/from16 v12, v18

    invoke-static/range {v6 .. v13}, Lf/h/c/n/d/p/d;->g(Ljava/lang/Float;IZIJJ)I

    move-result v5

    invoke-virtual {v1, v5}, Lf/h/c/n/d/p/c;->o(I)V

    if-eqz v32, :cond_d

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/16 v6, 0xd

    invoke-virtual {v1, v6}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-static {v5}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v5

    and-int/lit16 v6, v5, 0xff

    invoke-virtual {v1, v6}, Lf/h/c/n/d/p/c;->n(I)V

    shr-int/lit8 v6, v5, 0x8

    and-int/lit16 v6, v6, 0xff

    invoke-virtual {v1, v6}, Lf/h/c/n/d/p/c;->n(I)V

    shr-int/lit8 v6, v5, 0x10

    and-int/lit16 v6, v6, 0xff

    invoke-virtual {v1, v6}, Lf/h/c/n/d/p/c;->n(I)V

    shr-int/lit8 v5, v5, 0x18

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v1, v5}, Lf/h/c/n/d/p/c;->n(I)V

    :cond_d
    const/16 v5, 0x10

    invoke-virtual {v1, v5}, Lf/h/c/n/d/p/c;->o(I)V

    shl-int/lit8 v5, v16, 0x1

    shr-int/lit8 v6, v16, 0x1f

    xor-int/2addr v5, v6

    invoke-virtual {v1, v5}, Lf/h/c/n/d/p/c;->o(I)V

    move/from16 v5, v31

    invoke-virtual {v1, v14, v5}, Lf/h/c/n/d/p/c;->k(IZ)V

    invoke-virtual {v1, v2, v3}, Lf/h/c/n/d/p/c;->r(II)V

    move-wide/from16 v5, v29

    invoke-virtual {v1, v4, v5, v6}, Lf/h/c/n/d/p/c;->s(IJ)V

    move-wide/from16 v2, v18

    const/4 v4, 0x6

    invoke-virtual {v1, v4, v2, v3}, Lf/h/c/n/d/p/c;->s(IJ)V

    if-eqz v0, :cond_e

    const/4 v2, 0x2

    invoke-virtual {v1, v4, v2}, Lf/h/c/n/d/p/c;->q(II)V

    invoke-static {v15, v0}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-virtual {v1, v15, v0}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/c/n/d/k/x;->m:Lf/h/c/n/d/l/b;

    iget-object v1, v1, Lf/h/c/n/d/l/b;->c:Lf/h/c/n/d/l/a;

    invoke-interface {v1}, Lf/h/c/n/d/l/a;->d()V

    return-void
.end method

.method public final z(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/x$g;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "Failed to close session "

    const-string v1, "Failed to flush to session "

    const-string v2, " file."

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Lf/h/c/n/d/p/b;

    invoke-virtual {p0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, v5, p1}, Lf/h/c/n/d/p/b;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-static {v4}, Lf/h/c/n/d/p/c;->i(Ljava/io/OutputStream;)Lf/h/c/n/d/p/c;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-interface {p3, p1}, Lf/h/c/n/d/k/x$g;->a(Lf/h/c/n/d/p/c;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception p3

    move-object v3, p1

    goto :goto_0

    :catchall_1
    move-exception p3

    goto :goto_0

    :catchall_2
    move-exception p1

    move-object p3, p1

    move-object v4, v3

    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw p3
.end method
