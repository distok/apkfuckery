.class public Lf/h/c/n/d/k/k0;
.super Ljava/lang/Object;
.source "CrashlyticsCore.java"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/c/c;

.field public final c:Lf/h/c/n/d/k/q0;

.field public final d:J

.field public e:Lf/h/c/n/d/k/m0;

.field public f:Lf/h/c/n/d/k/m0;

.field public g:Z

.field public h:Lf/h/c/n/d/k/x;

.field public final i:Lf/h/c/n/d/k/w0;

.field public final j:Lf/h/c/n/d/j/a;

.field public final k:Lf/h/c/n/d/i/a;

.field public l:Ljava/util/concurrent/ExecutorService;

.field public m:Lf/h/c/n/d/k/i;

.field public n:Lf/h/c/n/d/a;


# direct methods
.method public constructor <init>(Lf/h/c/c;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/a;Lf/h/c/n/d/k/q0;Lf/h/c/n/d/j/a;Lf/h/c/n/d/i/a;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/k/k0;->b:Lf/h/c/c;

    iput-object p4, p0, Lf/h/c/n/d/k/k0;->c:Lf/h/c/n/d/k/q0;

    invoke-virtual {p1}, Lf/h/c/c;->a()V

    iget-object p1, p1, Lf/h/c/c;->a:Landroid/content/Context;

    iput-object p1, p0, Lf/h/c/n/d/k/k0;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/h/c/n/d/k/k0;->i:Lf/h/c/n/d/k/w0;

    iput-object p3, p0, Lf/h/c/n/d/k/k0;->n:Lf/h/c/n/d/a;

    iput-object p5, p0, Lf/h/c/n/d/k/k0;->j:Lf/h/c/n/d/j/a;

    iput-object p6, p0, Lf/h/c/n/d/k/k0;->k:Lf/h/c/n/d/i/a;

    iput-object p7, p0, Lf/h/c/n/d/k/k0;->l:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Lf/h/c/n/d/k/i;

    invoke-direct {p1, p7}, Lf/h/c/n/d/k/i;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object p1, p0, Lf/h/c/n/d/k/k0;->m:Lf/h/c/n/d/k/i;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lf/h/c/n/d/k/k0;->d:J

    return-void
.end method

.method public static a(Lf/h/c/n/d/k/k0;Lf/h/c/n/d/s/e;)Lcom/google/android/gms/tasks/Task;
    .locals 5

    const-string v0, "Collection of crash reports disabled in Crashlytics settings."

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v2, p0, Lf/h/c/n/d/k/k0;->m:Lf/h/c/n/d/k/i;

    invoke-virtual {v2}, Lf/h/c/n/d/k/i;->a()V

    iget-object v2, p0, Lf/h/c/n/d/k/k0;->e:Lf/h/c/n/d/k/m0;

    invoke-virtual {v2}, Lf/h/c/n/d/k/m0;->a()Z

    const-string v2, "Initialization marker file created."

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v2, p0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v3, v2, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v4, Lf/h/c/n/d/k/s;

    invoke-direct {v4, v2}, Lf/h/c/n/d/k/s;-><init>(Lf/h/c/n/d/k/x;)V

    new-instance v2, Lf/h/c/n/d/k/j;

    invoke-direct {v2, v3, v4}, Lf/h/c/n/d/k/j;-><init>(Lf/h/c/n/d/k/i;Ljava/lang/Runnable;)V

    invoke-virtual {v3, v2}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    :try_start_0
    iget-object v2, p0, Lf/h/c/n/d/k/k0;->j:Lf/h/c/n/d/j/a;

    new-instance v3, Lf/h/c/n/d/k/i0;

    invoke-direct {v3, p0}, Lf/h/c/n/d/k/i0;-><init>(Lf/h/c/n/d/k/k0;)V

    invoke-interface {v2, v3}, Lf/h/c/n/d/j/a;->a(Lf/h/c/n/d/k/i0;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast p1, Lf/h/c/n/d/s/d;

    :try_start_1
    invoke-virtual {p1}, Lf/h/c/n/d/s/d;->c()Lf/h/c/n/d/s/i/e;

    move-result-object v2

    invoke-interface {v2}, Lf/h/c/n/d/s/i/e;->a()Lf/h/c/n/d/s/i/c;

    move-result-object v3

    iget-boolean v3, v3, Lf/h/c/n/d/s/i/c;->a:Z

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->s(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    invoke-interface {v2}, Lf/h/c/n/d/s/i/e;->b()Lf/h/c/n/d/s/i/d;

    move-result-object v2

    iget v2, v2, Lf/h/c/n/d/s/i/d;->a:I

    invoke-virtual {v0, v2}, Lf/h/c/n/d/k/x;->h(I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Could not finalize previous sessions."

    invoke-virtual {v1, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Lf/h/c/n/d/s/d;->a()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lf/h/c/n/d/k/x;->u(FLcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    const-string v0, "Crashlytics encountered a problem during asynchronous initialization."

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "FirebaseCrashlytics"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    invoke-static {p1}, Lf/h/a/f/f/n/g;->s(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    invoke-virtual {p0}, Lf/h/c/n/d/k/k0;->c()V

    return-object p1

    :goto_1
    invoke-virtual {p0}, Lf/h/c/n/d/k/k0;->c()V

    throw p1
.end method


# virtual methods
.method public final b(Lf/h/c/n/d/s/e;)V
    .locals 6

    const-string v0, "FirebaseCrashlytics"

    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    new-instance v2, Lf/h/c/n/d/k/k0$a;

    invoke-direct {v2, p0, p1}, Lf/h/c/n/d/k/k0$a;-><init>(Lf/h/c/n/d/k/k0;Lf/h/c/n/d/s/e;)V

    iget-object p1, p0, Lf/h/c/n/d/k/k0;->l:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    const-string v2, "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously."

    invoke-virtual {v1, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const-wide/16 v2, 0x4

    const/4 v4, 0x6

    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v2, v3, v5}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {v1, v4}, Lf/h/c/n/d/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Crashlytics timed out during initialization."

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {v1, v4}, Lf/h/c/n/d/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Problem encountered during Crashlytics initialization."

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-virtual {v1, v4}, Lf/h/c/n/d/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Crashlytics was interrupted during initialization."

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lf/h/c/n/d/k/k0;->m:Lf/h/c/n/d/k/i;

    new-instance v1, Lf/h/c/n/d/k/k0$b;

    invoke-direct {v1, p0}, Lf/h/c/n/d/k/k0$b;-><init>(Lf/h/c/n/d/k/k0;)V

    invoke-virtual {v0, v1}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public d(Ljava/lang/Boolean;)V
    .locals 4
    .param p1    # Ljava/lang/Boolean;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/c/n/d/k/k0;->c:Lf/h/c/n/d/k/q0;

    monitor-enter v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    iput-boolean v1, v0, Lf/h/c/n/d/k/q0;->f:Z

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_4

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    move-object v2, p1

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lf/h/c/n/d/k/q0;->b:Lf/h/c/c;

    invoke-virtual {v2}, Lf/h/c/c;->a()V

    iget-object v2, v2, Lf/h/c/c;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lf/h/c/n/d/k/q0;->a(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v2

    :goto_1
    iput-object v2, v0, Lf/h/c/n/d/k/q0;->g:Ljava/lang/Boolean;

    iget-object v2, v0, Lf/h/c/n/d/k/q0;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "firebase_crashlytics_collection_enabled"

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    :cond_2
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_2
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p1, v0, Lf/h/c/n/d/k/q0;->c:Ljava/lang/Object;

    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Lf/h/c/n/d/k/q0;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v1, v0, Lf/h/c/n/d/k/q0;->e:Z

    if-nez v1, :cond_4

    iget-object v1, v0, Lf/h/c/n/d/k/q0;->d:Lcom/google/android/gms/tasks/TaskCompletionSource;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/c/n/d/k/q0;->e:Z

    goto :goto_3

    :cond_3
    iget-boolean v2, v0, Lf/h/c/n/d/k/q0;->e:Z

    if-eqz v2, :cond_4

    new-instance v2, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iput-object v2, v0, Lf/h/c/n/d/k/q0;->d:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iput-boolean v1, v0, Lf/h/c/n/d/k/q0;->e:Z

    :cond_4
    :goto_3
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v0

    return-void

    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_4
    monitor-exit v0

    throw p1
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v1, v0, Lf/h/c/n/d/k/x;->e:Lf/h/c/n/d/k/g1;

    invoke-virtual {v1, p1, p2}, Lf/h/c/n/d/k/g1;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object p1, v0, Lf/h/c/n/d/k/x;->e:Lf/h/c/n/d/k/g1;

    invoke-virtual {p1}, Lf/h/c/n/d/k/g1;->a()Ljava/util/Map;

    move-result-object p1

    iget-object p2, v0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v1, Lf/h/c/n/d/k/q;

    invoke-direct {v1, v0, p1}, Lf/h/c/n/d/k/q;-><init>(Lf/h/c/n/d/k/x;Ljava/util/Map;)V

    invoke-virtual {p2, v1}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    goto :goto_2

    :catch_0
    move-exception p1

    iget-object p2, v0, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p2

    iget p2, p2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    throw p1

    :cond_2
    :goto_1
    sget-object p1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string p2, "Attempting to set custom attribute with null key, ignoring."

    invoke-virtual {p1, p2}, Lf/h/c/n/d/b;->d(Ljava/lang/String;)V

    :goto_2
    return-void
.end method
