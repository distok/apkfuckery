.class public Lf/h/c/n/d/k/n0;
.super Ljava/lang/Object;
.source "CrashlyticsReportDataCapture.java"


# static fields
.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/c/n/d/k/w0;

.field public final c:Lf/h/c/n/d/k/b;

.field public final d:Lf/h/c/n/d/t/d;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "17.3.0"

    const/4 v11, 0x0

    aput-object v3, v2, v11

    const-string v3, "Crashlytics Android SDK/%s"

    invoke-static {v0, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf/h/c/n/d/k/n0;->e:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lf/h/c/n/d/k/n0;->f:Ljava/util/Map;

    const/4 v4, 0x5

    const-string v6, "armeabi"

    const/4 v7, 0x6

    const-string v8, "armeabi-v7a"

    const/16 v9, 0x9

    const-string v10, "arm64-v8a"

    const-string/jumbo v12, "x86"

    move-object v5, v0

    invoke-static/range {v4 .. v12}, Lf/e/c/a/a;->Q(ILjava/util/HashMap;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "x86_64"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lf/h/c/n/d/k/w0;Lf/h/c/n/d/k/b;Lf/h/c/n/d/t/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/k/n0;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/h/c/n/d/k/n0;->b:Lf/h/c/n/d/k/w0;

    iput-object p3, p0, Lf/h/c/n/d/k/n0;->c:Lf/h/c/n/d/k/b;

    iput-object p4, p0, Lf/h/c/n/d/k/n0;->d:Lf/h/c/n/d/t/d;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/c/n/d/t/e;III)Lf/h/c/n/d/m/v$d$d$a$b$b;
    .locals 7

    iget-object v1, p1, Lf/h/c/n/d/t/e;->b:Ljava/lang/String;

    iget-object v2, p1, Lf/h/c/n/d/t/e;->a:Ljava/lang/String;

    iget-object v0, p1, Lf/h/c/n/d/t/e;->c:[Ljava/lang/StackTraceElement;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-array v0, v3, [Ljava/lang/StackTraceElement;

    :goto_0
    iget-object p1, p1, Lf/h/c/n/d/t/e;->d:Lf/h/c/n/d/t/e;

    if-lt p4, p3, :cond_1

    move-object v4, p1

    :goto_1
    if-eqz v4, :cond_1

    iget-object v4, v4, Lf/h/c/n/d/t/e;->d:Lf/h/c/n/d/t/e;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    const-string v5, "Null type"

    invoke-static {v1, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p0, v0, p2}, Lf/h/c/n/d/k/n0;->b([Ljava/lang/StackTraceElement;I)Lf/h/c/n/d/m/w;

    move-result-object v0

    new-instance v5, Lf/h/c/n/d/m/w;

    invoke-direct {v5, v0}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-eqz p1, :cond_2

    if-nez v3, :cond_2

    add-int/lit8 p4, p4, 0x1

    invoke-virtual {p0, p1, p2, p3, p4}, Lf/h/c/n/d/k/n0;->a(Lf/h/c/n/d/t/e;III)Lf/h/c/n/d/m/v$d$d$a$b$b;

    move-result-object p1

    move-object v4, p1

    :cond_2
    if-nez v0, :cond_3

    const-string p1, " overflowCount"

    goto :goto_2

    :cond_3
    const-string p1, ""

    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_4

    new-instance p1, Lf/h/c/n/d/m/n;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    const/4 v6, 0x0

    move-object v0, p1

    move-object v3, v5

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lf/h/c/n/d/m/n;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/m/w;Lf/h/c/n/d/m/v$d$d$a$b$b;ILf/h/c/n/d/m/n$a;)V

    return-object p1

    :cond_4
    new-instance p2, Ljava/lang/IllegalStateException;

    const-string p3, "Missing required properties:"

    invoke-static {p3, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final b([Ljava/lang/StackTraceElement;I)Lf/h/c/n/d/m/w;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/StackTraceElement;",
            "I)",
            "Lf/h/c/n/d/m/w<",
            "Lf/h/c/n/d/m/v$d$d$a$b$d$a;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p1, v2

    new-instance v4, Lf/h/c/n/d/m/q$b;

    invoke-direct {v4}, Lf/h/c/n/d/m/q$b;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lf/h/c/n/d/m/q$b;->e:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->isNativeMethod()Z

    move-result v5

    const-wide/16 v6, 0x0

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v5

    int-to-long v8, v5

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    goto :goto_1

    :cond_0
    move-wide v8, v6

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "."

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->isNativeMethod()Z

    move-result v11

    if-nez v11, :cond_1

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v11

    if-lez v11, :cond_1

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    int-to-long v6, v3

    :cond_1
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v4, Lf/h/c/n/d/m/q$b;->a:Ljava/lang/Long;

    const-string v3, "Null symbol"

    invoke-static {v5, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v5, v4, Lf/h/c/n/d/m/q$b;->b:Ljava/lang/String;

    iput-object v10, v4, Lf/h/c/n/d/m/q$b;->c:Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v4, Lf/h/c/n/d/m/q$b;->d:Ljava/lang/Long;

    invoke-virtual {v4}, Lf/h/c/n/d/m/q$b;->a()Lf/h/c/n/d/m/v$d$d$a$b$d$a;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance p1, Lf/h/c/n/d/m/w;

    invoke-direct {p1, v0}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    return-object p1
.end method

.method public final c(Ljava/lang/Thread;[Ljava/lang/StackTraceElement;I)Lf/h/c/n/d/m/v$d$d$a$b$d;
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Null name"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p2, p3}, Lf/h/c/n/d/k/n0;->b([Ljava/lang/StackTraceElement;I)Lf/h/c/n/d/m/w;

    move-result-object p2

    new-instance p3, Lf/h/c/n/d/m/w;

    invoke-direct {p3, p2}, Lf/h/c/n/d/m/w;-><init>(Ljava/util/List;)V

    if-nez v0, :cond_0

    const-string p2, " importance"

    goto :goto_0

    :cond_0
    const-string p2, ""

    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance p2, Lf/h/c/n/d/m/p;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p2, p1, v0, p3, v1}, Lf/h/c/n/d/m/p;-><init>(Ljava/lang/String;ILf/h/c/n/d/m/w;Lf/h/c/n/d/m/p$a;)V

    return-object p2

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p3, "Missing required properties:"

    invoke-static {p3, p2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
