.class public Lf/h/c/n/d/k/o;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/util/Date;

.field public final synthetic e:Ljava/lang/Throwable;

.field public final synthetic f:Ljava/lang/Thread;

.field public final synthetic g:Lf/h/c/n/d/k/x;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;Ljava/util/Date;Ljava/lang/Throwable;Ljava/lang/Thread;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/o;->g:Lf/h/c/n/d/k/x;

    iput-object p2, p0, Lf/h/c/n/d/k/o;->d:Ljava/util/Date;

    iput-object p3, p0, Lf/h/c/n/d/k/o;->e:Ljava/lang/Throwable;

    iput-object p4, p0, Lf/h/c/n/d/k/o;->f:Ljava/lang/Thread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    move-object/from16 v1, p0

    sget-object v2, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v0, v1, Lf/h/c/n/d/k/o;->g:Lf/h/c/n/d/k/x;

    invoke-virtual {v0}, Lf/h/c/n/d/k/x;->p()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lf/h/c/n/d/k/o;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    iget-object v0, v1, Lf/h/c/n/d/k/o;->g:Lf/h/c/n/d/k/x;

    invoke-virtual {v0}, Lf/h/c/n/d/k/x;->i()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    const-string v0, "Tried to write a non-fatal exception while no session was open."

    invoke-virtual {v2, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, v1, Lf/h/c/n/d/k/o;->g:Lf/h/c/n/d/k/x;

    iget-object v7, v0, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    iget-object v8, v1, Lf/h/c/n/d/k/o;->e:Ljava/lang/Throwable;

    iget-object v9, v1, Lf/h/c/n/d/k/o;->f:Ljava/lang/Thread;

    const-string v0, "-"

    const-string v6, ""

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Persisting non-fatal event for session "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/4 v14, 0x0

    const-string v11, "error"

    move-wide v12, v3

    invoke-virtual/range {v7 .. v14}, Lf/h/c/n/d/k/e1;->a(Ljava/lang/Throwable;Ljava/lang/Thread;Ljava/lang/String;Ljava/lang/String;JZ)V

    iget-object v6, v1, Lf/h/c/n/d/k/o;->g:Lf/h/c/n/d/k/x;

    iget-object v9, v1, Lf/h/c/n/d/k/o;->f:Ljava/lang/Thread;

    iget-object v10, v1, Lf/h/c/n/d/k/o;->e:Ljava/lang/Throwable;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v15, "FirebaseCrashlytics"

    const-string v14, "Failed to close non-fatal file output stream."

    const-string v13, "Failed to flush to non-fatal file."

    const/4 v7, 0x0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Crashlytics is logging non-fatal exception \""

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v8, "\" from thread "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, v6, Lf/h/c/n/d/k/x;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    invoke-static {v0}, Lf/h/c/n/d/k/h;->u(I)Ljava/lang/String;

    move-result-object v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "SessionEvent"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v11, Lf/h/c/n/d/p/b;

    invoke-virtual {v6}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v8

    invoke-direct {v11, v8, v0}, Lf/h/c/n/d/p/b;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    invoke-static {v11}, Lf/h/c/n/d/p/c;->i(Ljava/io/OutputStream;)Lf/h/c/n/d/p/c;

    move-result-object v12
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    const-string v0, "error"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/16 v16, 0x0

    move-object v7, v6

    move-object v8, v12

    move-object/from16 v17, v11

    move-object/from16 v18, v12

    move-wide v11, v3

    move-object v3, v13

    move-object v13, v0

    move-object v4, v14

    move/from16 v14, v16

    :try_start_3
    invoke-virtual/range {v7 .. v14}, Lf/h/c/n/d/k/x;->y(Lf/h/c/n/d/p/c;Ljava/lang/Thread;Ljava/lang/Throwable;JLjava/lang/String;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object/from16 v7, v18

    invoke-static {v7, v3}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    const/4 v0, 0x6

    move-object/from16 v11, v17

    const/4 v10, 0x6

    goto :goto_5

    :catchall_0
    move-exception v0

    move-object/from16 v7, v18

    goto :goto_2

    :catch_0
    move-exception v0

    move-object/from16 v7, v18

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object/from16 v17, v11

    move-object v7, v12

    :goto_0
    move-object v3, v13

    move-object v4, v14

    goto :goto_2

    :catch_1
    move-exception v0

    move-object/from16 v17, v11

    move-object v7, v12

    :goto_1
    move-object v3, v13

    move-object v4, v14

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object/from16 v17, v11

    goto :goto_0

    :goto_2
    move-object v8, v7

    move-object/from16 v7, v17

    goto :goto_6

    :catch_2
    move-exception v0

    move-object/from16 v17, v11

    goto :goto_1

    :goto_3
    move-object v8, v7

    move-object/from16 v7, v17

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v3, v13

    move-object v4, v14

    move-object v2, v7

    goto :goto_7

    :catch_3
    move-exception v0

    move-object v3, v13

    move-object v4, v14

    move-object v8, v7

    :goto_4
    :try_start_4
    const-string v9, "An error occurred in the non-fatal exception logger"

    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Lf/h/c/n/d/b;->a(I)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-static {v15, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :cond_1
    invoke-static {v8, v3}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    move-object v11, v7

    :goto_5
    invoke-static {v11, v4}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    const/16 v0, 0x40

    :try_start_5
    invoke-virtual {v6, v5, v0}, Lf/h/c/n/d/k/x;->v(Ljava/lang/String;I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_8

    :catch_4
    move-exception v0

    move-object v3, v0

    invoke-virtual {v2, v10}, Lf/h/c/n/d/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "An error occurred when trimming non-fatal files."

    invoke-static {v15, v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    :catchall_4
    move-exception v0

    :goto_6
    move-object v2, v7

    move-object v7, v8

    :goto_7
    invoke-static {v7, v3}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    invoke-static {v2, v4}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_8
    return-void
.end method
