.class public Lf/h/c/n/d/k/d0;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/google/android/gms/tasks/Task<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Ljava/util/Date;

.field public final synthetic e:Ljava/lang/Throwable;

.field public final synthetic f:Ljava/lang/Thread;

.field public final synthetic g:Lf/h/c/n/d/s/e;

.field public final synthetic h:Lf/h/c/n/d/k/x;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;Ljava/util/Date;Ljava/lang/Throwable;Ljava/lang/Thread;Lf/h/c/n/d/s/e;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iput-object p2, p0, Lf/h/c/n/d/k/d0;->d:Ljava/util/Date;

    iput-object p3, p0, Lf/h/c/n/d/k/d0;->e:Ljava/lang/Throwable;

    iput-object p4, p0, Lf/h/c/n/d/k/d0;->f:Ljava/lang/Thread;

    iput-object p5, p0, Lf/h/c/n/d/k/d0;->g:Lf/h/c/n/d/s/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v1, p0

    sget-object v2, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v0, v1, Lf/h/c/n/d/k/d0;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    iget-object v0, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    invoke-virtual {v0}, Lf/h/c/n/d/k/x;->i()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    if-nez v0, :cond_0

    const-string v0, "Tried to write a fatal exception while no session was open."

    invoke-virtual {v2, v0}, Lf/h/c/n/d/b;->d(Ljava/lang/String;)V

    invoke-static {v5}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    goto/16 :goto_5

    :cond_0
    iget-object v6, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v6, v6, Lf/h/c/n/d/k/x;->d:Lf/h/c/n/d/k/m0;

    invoke-virtual {v6}, Lf/h/c/n/d/k/m0;->a()Z

    iget-object v6, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v7, v6, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    iget-object v8, v1, Lf/h/c/n/d/k/d0;->e:Ljava/lang/Throwable;

    iget-object v9, v1, Lf/h/c/n/d/k/d0;->f:Ljava/lang/Thread;

    const-string v6, "-"

    const-string v10, ""

    invoke-virtual {v0, v6, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Persisting fatal event for session "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    const/4 v14, 0x1

    const-string v11, "crash"

    move-wide v12, v3

    invoke-virtual/range {v7 .. v14}, Lf/h/c/n/d/k/e1;->a(Ljava/lang/Throwable;Ljava/lang/Thread;Ljava/lang/String;Ljava/lang/String;JZ)V

    iget-object v7, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v9, v1, Lf/h/c/n/d/k/d0;->f:Ljava/lang/Thread;

    iget-object v10, v1, Lf/h/c/n/d/k/d0;->e:Ljava/lang/Throwable;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "Failed to close fatal exception file output stream."

    const-string v15, "Failed to flush to session begin file."

    :try_start_0
    new-instance v14, Lf/h/c/n/d/p/b;

    invoke-virtual {v7}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v8

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "SessionCrash"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v14, v8, v0}, Lf/h/c/n/d/p/b;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    invoke-static {v14}, Lf/h/c/n/d/p/c;->i(Ljava/io/OutputStream;)Lf/h/c/n/d/p/c;

    move-result-object v16
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    const-string v13, "crash"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v0, 0x1

    move-object/from16 v8, v16

    move-wide v11, v3

    move-object v3, v14

    move v14, v0

    :try_start_3
    invoke-virtual/range {v7 .. v14}, Lf/h/c/n/d/k/x;->y(Lf/h/c/n/d/p/c;Ljava/lang/Thread;Ljava/lang/Throwable;JLjava/lang/String;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v14, v3

    goto :goto_2

    :catchall_0
    move-exception v0

    goto/16 :goto_7

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v0

    goto/16 :goto_6

    :catch_1
    move-exception v0

    move-object v3, v14

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v3, v14

    goto/16 :goto_8

    :catch_2
    move-exception v0

    move-object v3, v14

    move-object/from16 v16, v5

    :goto_0
    move-object v14, v3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v3, v5

    goto/16 :goto_8

    :catch_3
    move-exception v0

    move-object v14, v5

    move-object/from16 v16, v14

    :goto_1
    :try_start_4
    const-string v3, "An error occurred in the fatal exception logger"

    const/4 v4, 0x6

    invoke-virtual {v2, v4}, Lf/h/c/n/d/b;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "FirebaseCrashlytics"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :cond_1
    :goto_2
    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    invoke-static {v14, v6}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    iget-object v0, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v2, v1, Lf/h/c/n/d/k/d0;->d:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lf/h/c/n/d/k/x;->g(J)V

    iget-object v0, v1, Lf/h/c/n/d/k/d0;->g:Lf/h/c/n/d/s/e;

    check-cast v0, Lf/h/c/n/d/s/d;

    invoke-virtual {v0}, Lf/h/c/n/d/s/d;->c()Lf/h/c/n/d/s/i/e;

    move-result-object v0

    invoke-interface {v0}, Lf/h/c/n/d/s/i/e;->b()Lf/h/c/n/d/s/i/d;

    move-result-object v2

    iget v2, v2, Lf/h/c/n/d/s/i/d;->a:I

    invoke-interface {v0}, Lf/h/c/n/d/s/i/e;->b()Lf/h/c/n/d/s/i/d;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x4

    iget-object v3, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lf/h/c/n/d/k/x;->f(IZ)V

    iget-object v2, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    invoke-static {v2}, Lf/h/c/n/d/k/x;->a(Lf/h/c/n/d/k/x;)V

    iget-object v2, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->m()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->k()Ljava/io/File;

    move-result-object v6

    sget-object v7, Lf/h/c/n/d/k/x;->C:Ljava/util/Comparator;

    sget-object v8, Lf/h/c/n/d/k/i1;->a:Ljava/io/FilenameFilter;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    sget-object v9, Lf/h/c/n/d/k/i1;->a:Ljava/io/FilenameFilter;

    invoke-virtual {v6, v9}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v6

    if-eqz v3, :cond_2

    goto :goto_3

    :cond_2
    new-array v3, v4, [Ljava/io/File;

    :goto_3
    if-eqz v6, :cond_3

    goto :goto_4

    :cond_3
    new-array v6, v4, [Ljava/io/File;

    :goto_4
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {v8, v0, v7}, Lf/h/c/n/d/k/i1;->c(Ljava/util/List;ILjava/util/Comparator;)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->n()Ljava/io/File;

    move-result-object v3

    invoke-static {v3, v9, v0, v7}, Lf/h/c/n/d/k/i1;->b(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator;)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v2

    sget-object v3, Lf/h/c/n/d/k/x;->A:Ljava/io/FilenameFilter;

    invoke-static {v2, v3, v0, v7}, Lf/h/c/n/d/k/i1;->b(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator;)I

    iget-object v0, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v0, v0, Lf/h/c/n/d/k/x;->c:Lf/h/c/n/d/k/q0;

    invoke-virtual {v0}, Lf/h/c/n/d/k/q0;->b()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v5}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    goto :goto_5

    :cond_4
    iget-object v0, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v0, v0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    iget-object v0, v0, Lf/h/c/n/d/k/i;->a:Ljava/util/concurrent/Executor;

    iget-object v2, v1, Lf/h/c/n/d/k/d0;->g:Lf/h/c/n/d/s/e;

    check-cast v2, Lf/h/c/n/d/s/d;

    invoke-virtual {v2}, Lf/h/c/n/d/s/d;->a()Lcom/google/android/gms/tasks/Task;

    move-result-object v2

    new-instance v3, Lf/h/c/n/d/k/c0;

    invoke-direct {v3, v1, v0}, Lf/h/c/n/d/k/c0;-><init>(Lf/h/c/n/d/k/d0;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/tasks/Task;->r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    :goto_5
    return-object v0

    :catchall_4
    move-exception v0

    :goto_6
    move-object v3, v14

    :goto_7
    move-object/from16 v5, v16

    :goto_8
    invoke-static {v5, v15}, Lf/h/c/n/d/k/h;->h(Ljava/io/Flushable;Ljava/lang/String;)V

    invoke-static {v3, v6}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v0
.end method
