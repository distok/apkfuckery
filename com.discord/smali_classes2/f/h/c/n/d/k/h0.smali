.class public Lf/h/c/n/d/k/h0;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Lf/h/c/n/d/q/b$b;


# instance fields
.field public final synthetic a:Lf/h/c/n/d/k/x;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/h0;->a:Lf/h/c/n/d/k/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/n/d/s/i/b;)Lf/h/c/n/d/q/b;
    .locals 9
    .param p1    # Lf/h/c/n/d/s/i/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p1, Lf/h/c/n/d/s/i/b;->c:Ljava/lang/String;

    iget-object v1, p1, Lf/h/c/n/d/s/i/b;->d:Ljava/lang/String;

    iget-object v3, p1, Lf/h/c/n/d/s/i/b;->e:Ljava/lang/String;

    iget-object v2, p0, Lf/h/c/n/d/k/h0;->a:Lf/h/c/n/d/k/x;

    iget-object v4, v2, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    const-string v5, "com.crashlytics.ApiEndpoint"

    const-string v6, "string"

    invoke-static {v4, v5, v6}, Lf/h/c/n/d/k/h;->n(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    const-string v4, ""

    :goto_0
    new-instance v5, Lf/h/c/n/d/q/d/c;

    iget-object v6, v2, Lf/h/c/n/d/k/x;->g:Lf/h/c/n/d/n/c;

    const-string v7, "17.3.0"

    invoke-direct {v5, v4, v0, v6, v7}, Lf/h/c/n/d/q/d/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Ljava/lang/String;)V

    new-instance v0, Lf/h/c/n/d/q/d/d;

    iget-object v2, v2, Lf/h/c/n/d/k/x;->g:Lf/h/c/n/d/n/c;

    invoke-direct {v0, v4, v1, v2, v7}, Lf/h/c/n/d/q/d/d;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/n/c;Ljava/lang/String;)V

    new-instance v7, Lf/h/c/n/d/q/d/a;

    invoke-direct {v7, v5, v0}, Lf/h/c/n/d/q/d/a;-><init>(Lf/h/c/n/d/q/d/c;Lf/h/c/n/d/q/d/d;)V

    new-instance v0, Lf/h/c/n/d/q/b;

    iget-object v1, p0, Lf/h/c/n/d/k/h0;->a:Lf/h/c/n/d/k/x;

    iget-object v1, v1, Lf/h/c/n/d/k/x;->j:Lf/h/c/n/d/k/b;

    iget-object v4, v1, Lf/h/c/n/d/k/b;->a:Ljava/lang/String;

    invoke-static {p1}, Lf/h/c/n/d/k/r0;->f(Lf/h/c/n/d/s/i/b;)Lf/h/c/n/d/k/r0;

    move-result-object v5

    iget-object p1, p0, Lf/h/c/n/d/k/h0;->a:Lf/h/c/n/d/k/x;

    iget-object v6, p1, Lf/h/c/n/d/k/x;->n:Lf/h/c/n/d/q/a;

    iget-object v8, p1, Lf/h/c/n/d/k/x;->o:Lf/h/c/n/d/q/b$a;

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lf/h/c/n/d/q/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lf/h/c/n/d/k/r0;Lf/h/c/n/d/q/a;Lf/h/c/n/d/q/d/b;Lf/h/c/n/d/q/b$a;)V

    return-object v0
.end method
