.class public Lf/h/c/n/d/k/k1$a;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Lf/h/a/f/p/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/h/c/n/d/k/k1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/a<",
        "TT;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/h/c/n/d/k/k1;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/k1;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/k1$a;->a:Lf/h/c/n/d/k/k1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/android/gms/tasks/Task;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/n/d/k/k1$a;->a:Lf/h/c/n/d/k/k1;

    iget-object v0, v0, Lf/h/c/n/d/k/k1;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->l()Ljava/lang/Object;

    move-result-object p1

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {v0, p1}, Lf/h/a/f/p/b0;->t(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/c/n/d/k/k1$a;->a:Lf/h/c/n/d/k/k1;

    iget-object v0, v0, Lf/h/c/n/d/k/k1;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->k()Ljava/lang/Exception;

    move-result-object p1

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {v0, p1}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method
