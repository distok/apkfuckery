.class public final Lf/h/c/n/d/k/x$m;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/n/d/k/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "m"
.end annotation


# instance fields
.field public final d:Landroid/content/Context;

.field public final e:Lf/h/c/n/d/q/c/c;

.field public final f:Lf/h/c/n/d/q/b;

.field public final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/c/n/d/q/c/c;Lf/h/c/n/d/q/b;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/k/x$m;->d:Landroid/content/Context;

    iput-object p2, p0, Lf/h/c/n/d/k/x$m;->e:Lf/h/c/n/d/q/c/c;

    iput-object p3, p0, Lf/h/c/n/d/k/x$m;->f:Lf/h/c/n/d/q/b;

    iput-boolean p4, p0, Lf/h/c/n/d/k/x$m;->g:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lf/h/c/n/d/k/x$m;->d:Landroid/content/Context;

    invoke-static {v0}, Lf/h/c/n/d/k/h;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v1, "Attempting to send crash report at time of crash..."

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/c/n/d/k/x$m;->f:Lf/h/c/n/d/q/b;

    iget-object v1, p0, Lf/h/c/n/d/k/x$m;->e:Lf/h/c/n/d/q/c/c;

    iget-boolean v2, p0, Lf/h/c/n/d/k/x$m;->g:Z

    invoke-virtual {v0, v1, v2}, Lf/h/c/n/d/q/b;->a(Lf/h/c/n/d/q/c/c;Z)Z

    return-void
.end method
