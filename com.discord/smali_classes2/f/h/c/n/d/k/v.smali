.class public Lf/h/c/n/d/k/v;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Lf/h/c/n/d/k/x$g;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:I

.field public final synthetic f:Lf/h/c/n/d/k/x;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/v;->f:Lf/h/c/n/d/k/x;

    iput-object p2, p0, Lf/h/c/n/d/k/v;->a:Ljava/lang/String;

    iput-object p3, p0, Lf/h/c/n/d/k/v;->b:Ljava/lang/String;

    iput-object p4, p0, Lf/h/c/n/d/k/v;->c:Ljava/lang/String;

    iput-object p5, p0, Lf/h/c/n/d/k/v;->d:Ljava/lang/String;

    iput p6, p0, Lf/h/c/n/d/k/v;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/n/d/p/c;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lf/h/c/n/d/k/v;->a:Ljava/lang/String;

    iget-object v3, v0, Lf/h/c/n/d/k/v;->b:Ljava/lang/String;

    iget-object v4, v0, Lf/h/c/n/d/k/v;->c:Ljava/lang/String;

    iget-object v5, v0, Lf/h/c/n/d/k/v;->d:Ljava/lang/String;

    iget v6, v0, Lf/h/c/n/d/k/v;->e:I

    iget-object v7, v0, Lf/h/c/n/d/k/v;->f:Lf/h/c/n/d/k/x;

    iget-object v7, v7, Lf/h/c/n/d/k/x;->r:Ljava/lang/String;

    sget-object v8, Lf/h/c/n/d/p/d;->a:Lf/h/c/n/d/p/a;

    invoke-static {v2}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v2

    invoke-static {v3}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v3

    invoke-static {v4}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v4

    invoke-static {v5}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v5

    if-eqz v7, :cond_0

    invoke-static {v7}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v7

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x7

    const/4 v9, 0x2

    invoke-virtual {v1, v8, v9}, Lf/h/c/n/d/p/c;->q(II)V

    const/4 v8, 0x1

    invoke-static {v8, v2}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v10

    add-int/lit8 v10, v10, 0x0

    invoke-static {v9, v3}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v11

    add-int/2addr v11, v10

    const/4 v10, 0x3

    invoke-static {v10, v4}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v12

    add-int/2addr v12, v11

    const/4 v11, 0x6

    invoke-static {v11, v5}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v13

    add-int/2addr v13, v12

    const/16 v12, 0x9

    const/16 v14, 0x8

    if-eqz v7, :cond_1

    sget-object v15, Lf/h/c/n/d/p/d;->b:Lf/h/c/n/d/p/a;

    invoke-static {v14, v15}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v15

    add-int/2addr v15, v13

    invoke-static {v12, v7}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v13

    add-int/2addr v13, v15

    :cond_1
    const/16 v15, 0xa

    invoke-static {v15, v6}, Lf/h/c/n/d/p/c;->c(II)I

    move-result v16

    add-int v13, v16, v13

    invoke-virtual {v1, v13}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-virtual {v1, v8, v2}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {v1, v9, v3}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {v1, v10, v4}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {v1, v11, v5}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    if-eqz v7, :cond_2

    sget-object v2, Lf/h/c/n/d/p/d;->b:Lf/h/c/n/d/p/a;

    invoke-virtual {v1, v14, v2}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {v1, v12, v7}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    :cond_2
    invoke-virtual {v1, v15, v6}, Lf/h/c/n/d/p/c;->m(II)V

    return-void
.end method
