.class public Lf/h/c/n/d/k/i;
.super Ljava/lang/Object;
.source "CrashlyticsBackgroundWorker.java"


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public b:Lcom/google/android/gms/tasks/Task;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/Object;

.field public d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/n/d/k/i;->b:Lcom/google/android/gms/tasks/Task;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/h/c/n/d/k/i;->c:Ljava/lang/Object;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lf/h/c/n/d/k/i;->d:Ljava/lang/ThreadLocal;

    iput-object p1, p0, Lf/h/c/n/d/k/i;->a:Ljava/util/concurrent/Executor;

    new-instance v0, Lf/h/c/n/d/k/i$a;

    invoke-direct {v0, p0}, Lf/h/c/n/d/k/i$a;-><init>(Lf/h/c/n/d/k/i;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lf/h/c/n/d/k/i;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not running on background worker thread as intended."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/k/i;->c:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/c/n/d/k/i;->b:Lcom/google/android/gms/tasks/Task;

    iget-object v2, p0, Lf/h/c/n/d/k/i;->a:Ljava/util/concurrent/Executor;

    new-instance v3, Lf/h/c/n/d/k/k;

    invoke-direct {v3, p0, p1}, Lf/h/c/n/d/k/k;-><init>(Lf/h/c/n/d/k/i;Ljava/util/concurrent/Callable;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/tasks/Task;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    iget-object v1, p0, Lf/h/c/n/d/k/i;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/h/c/n/d/k/l;

    invoke-direct {v2, p0}, Lf/h/c/n/d/k/l;-><init>(Lf/h/c/n/d/k/i;)V

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/tasks/Task;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    iput-object v1, p0, Lf/h/c/n/d/k/i;->b:Lcom/google/android/gms/tasks/Task;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public c(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/k/i;->c:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/c/n/d/k/i;->b:Lcom/google/android/gms/tasks/Task;

    iget-object v2, p0, Lf/h/c/n/d/k/i;->a:Ljava/util/concurrent/Executor;

    new-instance v3, Lf/h/c/n/d/k/k;

    invoke-direct {v3, p0, p1}, Lf/h/c/n/d/k/k;-><init>(Lf/h/c/n/d/k/i;Ljava/util/concurrent/Callable;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/tasks/Task;->j(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    iget-object v1, p0, Lf/h/c/n/d/k/i;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/h/c/n/d/k/l;

    invoke-direct {v2, p0}, Lf/h/c/n/d/k/l;-><init>(Lf/h/c/n/d/k/i;)V

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/tasks/Task;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    iput-object v1, p0, Lf/h/c/n/d/k/i;->b:Lcom/google/android/gms/tasks/Task;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
