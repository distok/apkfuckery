.class public Lf/h/c/n/d/k/l0;
.super Ljava/lang/Object;
.source "CrashlyticsCore.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/h/c/n/d/k/k0;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/k0;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/l0;->d:Lf/h/c/n/d/k/k0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/k/l0;->d:Lf/h/c/n/d/k/k0;

    iget-object v0, v0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->d:Lf/h/c/n/d/k/m0;

    invoke-virtual {v1}, Lf/h/c/n/d/k/m0;->b()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lf/h/c/n/d/k/x;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/h/c/n/d/k/x;->p:Lf/h/c/n/d/a;

    invoke-interface {v0, v1}, Lf/h/c/n/d/a;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    sget-object v1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v3, "Found previous crash marker."

    invoke-virtual {v1, v3}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, v0, Lf/h/c/n/d/k/x;->d:Lf/h/c/n/d/k/m0;

    invoke-virtual {v0}, Lf/h/c/n/d/k/m0;->b()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
