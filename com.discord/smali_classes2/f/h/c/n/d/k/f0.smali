.class public Lf/h/c/n/d/k/f0;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Lf/h/a/f/p/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/f<",
        "Lf/h/c/n/d/s/i/b;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Z

.field public final synthetic c:Ljava/util/concurrent/Executor;

.field public final synthetic d:Lf/h/c/n/d/k/g0;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/g0;Ljava/util/List;ZLjava/util/concurrent/Executor;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/f0;->d:Lf/h/c/n/d/k/g0;

    iput-object p2, p0, Lf/h/c/n/d/k/f0;->a:Ljava/util/List;

    iput-boolean p3, p0, Lf/h/c/n/d/k/f0;->b:Z

    iput-object p4, p0, Lf/h/c/n/d/k/f0;->c:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lf/h/c/n/d/s/i/b;

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const-string p1, "Received null app settings, cannot send reports during app startup."

    invoke-virtual {v0, p1}, Lf/h/c/n/d/b;->g(Ljava/lang/String;)V

    invoke-static {v1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto/16 :goto_2

    :cond_0
    iget-object v2, p0, Lf/h/c/n/d/k/f0;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/c/n/d/q/c/c;

    invoke-interface {v3}, Lf/h/c/n/d/q/c/c;->getType()Lf/h/c/n/d/q/c/c$a;

    move-result-object v4

    sget-object v5, Lf/h/c/n/d/q/c/c$a;->d:Lf/h/c/n/d/q/c/c$a;

    if-ne v4, v5, :cond_1

    iget-object v4, p1, Lf/h/c/n/d/s/i/b;->e:Ljava/lang/String;

    invoke-interface {v3}, Lf/h/c/n/d/q/c/c;->c()Ljava/io/File;

    move-result-object v3

    invoke-static {v4, v3}, Lf/h/c/n/d/k/x;->c(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lf/h/c/n/d/k/f0;->d:Lf/h/c/n/d/k/g0;

    iget-object v2, v2, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v2, v2, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    invoke-static {v2}, Lf/h/c/n/d/k/x;->b(Lf/h/c/n/d/k/x;)Lcom/google/android/gms/tasks/Task;

    iget-object v2, p0, Lf/h/c/n/d/k/f0;->d:Lf/h/c/n/d/k/g0;

    iget-object v2, v2, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v2, v2, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v2, v2, Lf/h/c/n/d/k/x;->k:Lf/h/c/n/d/q/b$b;

    check-cast v2, Lf/h/c/n/d/k/h0;

    invoke-virtual {v2, p1}, Lf/h/c/n/d/k/h0;->a(Lf/h/c/n/d/s/i/b;)Lf/h/c/n/d/q/b;

    move-result-object v2

    iget-object v3, p0, Lf/h/c/n/d/k/f0;->a:Ljava/util/List;

    iget-boolean v4, p0, Lf/h/c/n/d/k/f0;->b:Z

    iget-object v5, p0, Lf/h/c/n/d/k/f0;->d:Lf/h/c/n/d/k/g0;

    iget-object v5, v5, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget v5, v5, Lf/h/c/n/d/k/x$e;->b:F

    monitor-enter v2

    :try_start_0
    iget-object v6, v2, Lf/h/c/n/d/q/b;->g:Ljava/lang/Thread;

    if-eqz v6, :cond_3

    const-string v3, "Report upload has already been started."

    invoke-virtual {v0, v3}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    goto :goto_1

    :cond_3
    :try_start_1
    new-instance v0, Lf/h/c/n/d/q/b$d;

    invoke-direct {v0, v2, v3, v4, v5}, Lf/h/c/n/d/q/b$d;-><init>(Lf/h/c/n/d/q/b;Ljava/util/List;ZF)V

    new-instance v3, Ljava/lang/Thread;

    const-string v4, "Crashlytics Report Uploader"

    invoke-direct {v3, v0, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v3, v2, Lf/h/c/n/d/q/b;->g:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    :goto_1
    iget-object v0, p0, Lf/h/c/n/d/k/f0;->d:Lf/h/c/n/d/k/g0;

    iget-object v0, v0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v0, v0, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v0, v0, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    iget-object v2, p0, Lf/h/c/n/d/k/f0;->c:Ljava/util/concurrent/Executor;

    invoke-static {p1}, Lf/h/c/n/d/k/r0;->f(Lf/h/c/n/d/s/i/b;)Lf/h/c/n/d/k/r0;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lf/h/c/n/d/k/e1;->b(Ljava/util/concurrent/Executor;Lf/h/c/n/d/k/r0;)Lcom/google/android/gms/tasks/Task;

    iget-object p1, p0, Lf/h/c/n/d/k/f0;->d:Lf/h/c/n/d/k/g0;

    iget-object p1, p1, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object p1, p1, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object p1, p1, Lf/h/c/n/d/k/x;->x:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    invoke-static {v1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_2
    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1
.end method
