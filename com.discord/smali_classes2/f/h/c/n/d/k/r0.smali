.class public final enum Lf/h/c/n/d/k/r0;
.super Ljava/lang/Enum;
.source "DataTransportState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/n/d/k/r0;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/n/d/k/r0;

.field public static final enum e:Lf/h/c/n/d/k/r0;

.field public static final enum f:Lf/h/c/n/d/k/r0;

.field public static final synthetic g:[Lf/h/c/n/d/k/r0;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lf/h/c/n/d/k/r0;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/c/n/d/k/r0;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/c/n/d/k/r0;->d:Lf/h/c/n/d/k/r0;

    new-instance v1, Lf/h/c/n/d/k/r0;

    const-string v3, "JAVA_ONLY"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/c/n/d/k/r0;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/c/n/d/k/r0;->e:Lf/h/c/n/d/k/r0;

    new-instance v3, Lf/h/c/n/d/k/r0;

    const-string v5, "ALL"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/h/c/n/d/k/r0;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/h/c/n/d/k/r0;->f:Lf/h/c/n/d/k/r0;

    const/4 v5, 0x3

    new-array v5, v5, [Lf/h/c/n/d/k/r0;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lf/h/c/n/d/k/r0;->g:[Lf/h/c/n/d/k/r0;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static f(Lf/h/c/n/d/s/i/b;)Lf/h/c/n/d/k/r0;
    .locals 4
    .param p0    # Lf/h/c/n/d/s/i/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget v0, p0, Lf/h/c/n/d/s/i/b;->g:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget p0, p0, Lf/h/c/n/d/s/i/b;->h:I

    if-ne p0, v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-nez v0, :cond_2

    sget-object p0, Lf/h/c/n/d/k/r0;->d:Lf/h/c/n/d/k/r0;

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    sget-object p0, Lf/h/c/n/d/k/r0;->e:Lf/h/c/n/d/k/r0;

    goto :goto_1

    :cond_3
    sget-object p0, Lf/h/c/n/d/k/r0;->f:Lf/h/c/n/d/k/r0;

    :goto_1
    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/n/d/k/r0;
    .locals 1

    const-class v0, Lf/h/c/n/d/k/r0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/n/d/k/r0;

    return-object p0
.end method

.method public static values()[Lf/h/c/n/d/k/r0;
    .locals 1

    sget-object v0, Lf/h/c/n/d/k/r0;->g:[Lf/h/c/n/d/k/r0;

    invoke-virtual {v0}, [Lf/h/c/n/d/k/r0;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/n/d/k/r0;

    return-object v0
.end method
