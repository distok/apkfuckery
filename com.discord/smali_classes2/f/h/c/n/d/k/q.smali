.class public Lf/h/c/n/d/k/q;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Ljava/util/Map;

.field public final synthetic e:Lf/h/c/n/d/k/x;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/q;->e:Lf/h/c/n/d/k/x;

    iput-object p2, p0, Lf/h/c/n/d/k/q;->d:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/d/k/q;->e:Lf/h/c/n/d/k/x;

    invoke-virtual {v0}, Lf/h/c/n/d/k/x;->i()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/h/c/n/d/k/a1;

    iget-object v2, p0, Lf/h/c/n/d/k/q;->e:Lf/h/c/n/d/k/x;

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Lf/h/c/n/d/k/a1;-><init>(Ljava/io/File;)V

    iget-object v2, p0, Lf/h/c/n/d/k/q;->d:Ljava/util/Map;

    const-string v3, "Failed to close key/value metadata file."

    invoke-virtual {v1, v0}, Lf/h/c/n/d/k/a1;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sget-object v0, Lf/h/c/n/d/k/a1;->b:Ljava/nio/charset/Charset;

    invoke-direct {v5, v6, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v4, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/Writer;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v4, v1

    :goto_0
    :try_start_2
    sget-object v2, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v5, "Error serializing key/value metadata."

    const/4 v6, 0x6

    invoke-virtual {v2, v6}, Lf/h/c/n/d/b;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "FirebaseCrashlytics"

    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_0
    :goto_1
    invoke-static {v4, v3}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    return-object v1

    :catchall_2
    move-exception v0

    move-object v1, v4

    :goto_2
    move-object v4, v1

    :goto_3
    invoke-static {v4, v3}, Lf/h/c/n/d/k/h;->c(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v0
.end method
