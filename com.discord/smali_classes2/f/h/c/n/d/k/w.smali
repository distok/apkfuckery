.class public Lf/h/c/n/d/k/w;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Lf/h/c/n/d/k/x$g;


# instance fields
.field public final synthetic a:Z


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    iput-boolean p4, p0, Lf/h/c/n/d/k/w;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/n/d/p/c;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    iget-boolean v2, p0, Lf/h/c/n/d/k/w;->a:Z

    sget-object v3, Lf/h/c/n/d/p/d;->a:Lf/h/c/n/d/p/a;

    invoke-static {v0}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v0

    invoke-static {v1}, Lf/h/c/n/d/p/a;->a(Ljava/lang/String;)Lf/h/c/n/d/p/a;

    move-result-object v1

    const/16 v3, 0x8

    const/4 v4, 0x2

    invoke-virtual {p1, v3, v4}, Lf/h/c/n/d/p/c;->q(II)V

    const/4 v3, 0x1

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lf/h/c/n/d/p/c;->c(II)I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v4, v0}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v7

    add-int/2addr v7, v6

    invoke-static {v5, v1}, Lf/h/c/n/d/p/c;->b(ILf/h/c/n/d/p/a;)I

    move-result v6

    add-int/2addr v6, v7

    const/4 v7, 0x4

    invoke-static {v7, v2}, Lf/h/c/n/d/p/c;->a(IZ)I

    move-result v8

    add-int/2addr v8, v6

    invoke-virtual {p1, v8}, Lf/h/c/n/d/p/c;->o(I)V

    invoke-virtual {p1, v3, v5}, Lf/h/c/n/d/p/c;->m(II)V

    invoke-virtual {p1, v4, v0}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {p1, v5, v1}, Lf/h/c/n/d/p/c;->l(ILf/h/c/n/d/p/a;)V

    invoke-virtual {p1, v7, v2}, Lf/h/c/n/d/p/c;->k(IZ)V

    return-void
.end method
