.class public Lf/h/c/n/d/k/g0;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/google/android/gms/tasks/Task<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Ljava/lang/Boolean;

.field public final synthetic e:Lf/h/c/n/d/k/x$e;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/x$e;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iput-object p2, p0, Lf/h/c/n/d/k/g0;->d:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    iget-object v1, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v1, v1, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v1, v1, Lf/h/c/n/d/k/x;->n:Lf/h/c/n/d/q/a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Checking for crash reports..."

    invoke-virtual {v0, v2}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v2, v1, Lf/h/c/n/d/q/a;->a:Lf/h/c/n/d/q/b$c;

    check-cast v2, Lf/h/c/n/d/k/x$k;

    iget-object v2, v2, Lf/h/c/n/d/k/x$k;->a:Lf/h/c/n/d/k/x;

    invoke-virtual {v2}, Lf/h/c/n/d/k/x;->q()[Ljava/io/File;

    move-result-object v2

    iget-object v1, v1, Lf/h/c/n/d/q/a;->a:Lf/h/c/n/d/q/b$c;

    check-cast v1, Lf/h/c/n/d/k/x$k;

    iget-object v1, v1, Lf/h/c/n/d/k/x$k;->a:Lf/h/c/n/d/k/x;

    invoke-virtual {v1}, Lf/h/c/n/d/k/x;->m()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    new-array v1, v3, [Ljava/io/File;

    :cond_0
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    if-eqz v2, :cond_1

    array-length v5, v2

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_1

    aget-object v7, v2, v6

    const-string v8, "Found crash report "

    invoke-static {v8}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    new-instance v8, Lf/h/c/n/d/q/c/d;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v9

    invoke-direct {v8, v7, v9}, Lf/h/c/n/d/q/c/d;-><init>(Ljava/io/File;Ljava/util/Map;)V

    invoke-virtual {v4, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    array-length v2, v1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v2, :cond_2

    aget-object v6, v1, v5

    new-instance v7, Lf/h/c/n/d/q/c/b;

    invoke-direct {v7, v6}, Lf/h/c/n/d/q/c/b;-><init>(Ljava/io/File;)V

    invoke-virtual {v4, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "No reports found."

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lf/h/c/n/d/k/g0;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_6

    const-string v1, "Reports are being deleted."

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v0, v0, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    sget-object v1, Lf/h/c/n/d/k/m;->a:Lf/h/c/n/d/k/m;

    invoke-virtual {v0}, Lf/h/c/n/d/k/x;->l()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, v1}, Lf/h/c/n/d/k/x;->r(Ljava/io/File;Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    :goto_2
    if-ge v3, v1, :cond_4

    aget-object v5, v0, v3

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v0, v0, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v0, v0, Lf/h/c/n/d/k/x;->n:Lf/h/c/n/d/q/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/n/d/q/c/c;

    invoke-interface {v1}, Lf/h/c/n/d/q/c/c;->remove()V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v0, v0, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v0, v0, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    iget-object v0, v0, Lf/h/c/n/d/k/e1;->b:Lf/h/c/n/d/o/g;

    invoke-virtual {v0}, Lf/h/c/n/d/o/g;->b()V

    iget-object v0, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v0, v0, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v0, v0, Lf/h/c/n/d/k/x;->x:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    goto :goto_4

    :cond_6
    const-string v1, "Reports are being sent."

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/c/n/d/k/g0;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v1, v1, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v1, v1, Lf/h/c/n/d/k/x;->c:Lf/h/c/n/d/k/q0;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_7

    iget-object v1, v1, Lf/h/c/n/d/k/q0;->h:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    iget-object v1, p0, Lf/h/c/n/d/k/g0;->e:Lf/h/c/n/d/k/x$e;

    iget-object v2, v1, Lf/h/c/n/d/k/x$e;->c:Lf/h/c/n/d/k/x;

    iget-object v2, v2, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    iget-object v2, v2, Lf/h/c/n/d/k/i;->a:Ljava/util/concurrent/Executor;

    iget-object v1, v1, Lf/h/c/n/d/k/x$e;->a:Lcom/google/android/gms/tasks/Task;

    new-instance v3, Lf/h/c/n/d/k/f0;

    invoke-direct {v3, p0, v4, v0, v2}, Lf/h/c/n/d/k/f0;-><init>(Lf/h/c/n/d/k/g0;Ljava/util/List;ZLjava/util/concurrent/Executor;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/tasks/Task;->r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    :goto_4
    return-object v0

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "An invalid data collection token was used."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
