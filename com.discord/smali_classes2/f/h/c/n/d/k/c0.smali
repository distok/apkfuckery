.class public Lf/h/c/n/d/k/c0;
.super Ljava/lang/Object;
.source "CrashlyticsController.java"

# interfaces
.implements Lf/h/a/f/p/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/f<",
        "Lf/h/c/n/d/s/i/b;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/Executor;

.field public final synthetic b:Lf/h/c/n/d/k/d0;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/d0;Ljava/util/concurrent/Executor;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/d/k/c0;->b:Lf/h/c/n/d/k/d0;

    iput-object p2, p0, Lf/h/c/n/d/k/c0;->a:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lf/h/c/n/d/s/i/b;

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v0, "Received null app settings, cannot send reports at crash time."

    invoke-virtual {p1, v0}, Lf/h/c/n/d/b;->g(Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lf/h/c/n/d/k/c0;->b:Lf/h/c/n/d/k/d0;

    iget-object v0, v0, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->b:Landroid/content/Context;

    iget-object v2, v0, Lf/h/c/n/d/k/x;->k:Lf/h/c/n/d/q/b$b;

    check-cast v2, Lf/h/c/n/d/k/h0;

    invoke-virtual {v2, p1}, Lf/h/c/n/d/k/h0;->a(Lf/h/c/n/d/s/i/b;)Lf/h/c/n/d/q/b;

    move-result-object v2

    invoke-virtual {v0}, Lf/h/c/n/d/k/x;->q()[Ljava/io/File;

    move-result-object v3

    array-length v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    const/4 v7, 0x1

    if-ge v6, v4, :cond_1

    aget-object v8, v3, v6

    iget-object v9, p1, Lf/h/c/n/d/s/i/b;->e:Ljava/lang/String;

    invoke-static {v9, v8}, Lf/h/c/n/d/k/x;->c(Ljava/lang/String;Ljava/io/File;)V

    new-instance v9, Lf/h/c/n/d/q/c/d;

    sget-object v10, Lf/h/c/n/d/k/x;->E:Ljava/util/Map;

    invoke-direct {v9, v8, v10}, Lf/h/c/n/d/q/c/d;-><init>(Ljava/io/File;Ljava/util/Map;)V

    iget-object v8, v0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v10, Lf/h/c/n/d/k/x$m;

    invoke-direct {v10, v1, v9, v2, v7}, Lf/h/c/n/d/k/x$m;-><init>(Landroid/content/Context;Lf/h/c/n/d/q/c/c;Lf/h/c/n/d/q/b;Z)V

    new-instance v7, Lf/h/c/n/d/k/j;

    invoke-direct {v7, v8, v10}, Lf/h/c/n/d/k/j;-><init>(Lf/h/c/n/d/k/i;Ljava/lang/Runnable;)V

    invoke-virtual {v8, v7}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/tasks/Task;

    iget-object v1, p0, Lf/h/c/n/d/k/c0;->b:Lf/h/c/n/d/k/d0;

    iget-object v1, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    invoke-static {v1}, Lf/h/c/n/d/k/x;->b(Lf/h/c/n/d/k/x;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v1, p0, Lf/h/c/n/d/k/c0;->b:Lf/h/c/n/d/k/d0;

    iget-object v1, v1, Lf/h/c/n/d/k/d0;->h:Lf/h/c/n/d/k/x;

    iget-object v1, v1, Lf/h/c/n/d/k/x;->t:Lf/h/c/n/d/k/e1;

    iget-object v2, p0, Lf/h/c/n/d/k/c0;->a:Ljava/util/concurrent/Executor;

    invoke-static {p1}, Lf/h/c/n/d/k/r0;->f(Lf/h/c/n/d/s/i/b;)Lf/h/c/n/d/k/r0;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lf/h/c/n/d/k/e1;->b(Ljava/util/concurrent/Executor;Lf/h/c/n/d/k/r0;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    aput-object p1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->g0(Ljava/util/Collection;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :goto_1
    return-object p1
.end method
