.class public Lf/h/c/n/d/n/b;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# static fields
.field public static final f:Lb0/y;


# instance fields
.field public final a:Lf/h/c/n/d/n/a;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lokhttp3/MultipartBody$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lb0/y;

    invoke-direct {v0}, Lb0/y;-><init>()V

    invoke-virtual {v0}, Lb0/y;->e()Lb0/y$a;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-string v2, "unit"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "timeout"

    const-wide/16 v3, 0x2710

    invoke-static {v2, v3, v4, v1}, Lb0/g0/c;->b(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result v1

    iput v1, v0, Lb0/y$a;->w:I

    new-instance v1, Lb0/y;

    invoke-direct {v1, v0}, Lb0/y;-><init>(Lb0/y$a;)V

    sput-object v1, Lf/h/c/n/d/n/b;->f:Lb0/y;

    return-void
.end method

.method public constructor <init>(Lf/h/c/n/d/n/a;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/n/d/n/a;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    iput-object p1, p0, Lf/h/c/n/d/n/b;->a:Lf/h/c/n/d/n/a;

    iput-object p2, p0, Lf/h/c/n/d/n/b;->b:Ljava/lang/String;

    iput-object p3, p0, Lf/h/c/n/d/n/b;->c:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a()Lf/h/c/n/d/n/d;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    new-instance v1, Lb0/a0$a;

    invoke-direct {v1}, Lb0/a0$a;-><init>()V

    const/4 v10, -0x1

    const/4 v3, 0x1

    new-instance v15, Lb0/d;

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, -0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/4 v12, 0x0

    move-object v2, v15

    move-object/from16 v18, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    invoke-direct/range {v2 .. v16}, Lb0/d;-><init>(ZZIIZZZIIZZZLjava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    const-string v2, "cacheControl"

    move-object/from16 v3, v18

    invoke-static {v3, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lb0/d;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const-string v4, "Cache-Control"

    if-eqz v3, :cond_1

    invoke-virtual {v1, v4}, Lb0/a0$a;->d(Ljava/lang/String;)Lb0/a0$a;

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v4, v2}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    :goto_1
    iget-object v2, v0, Lf/h/c/n/d/n/b;->b:Ljava/lang/String;

    invoke-static {v2}, Lb0/x;->h(Ljava/lang/String;)Lb0/x;

    move-result-object v2

    invoke-virtual {v2}, Lb0/x;->f()Lb0/x$a;

    move-result-object v2

    iget-object v3, v0, Lf/h/c/n/d/n/b;->c:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v5, v4}, Lb0/x$a;->a(Ljava/lang/String;Ljava/lang/String;)Lb0/x$a;

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Lb0/x$a;->b()Lb0/x;

    move-result-object v2

    invoke-virtual {v1, v2}, Lb0/a0$a;->g(Lb0/x;)Lb0/a0$a;

    iget-object v2, v0, Lf/h/c/n/d/n/b;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    goto :goto_3

    :cond_3
    iget-object v2, v0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    const/4 v3, 0x0

    if-nez v2, :cond_4

    move-object v2, v3

    goto :goto_4

    :cond_4
    invoke-virtual {v2}, Lokhttp3/MultipartBody$a;->b()Lokhttp3/MultipartBody;

    move-result-object v2

    :goto_4
    iget-object v4, v0, Lf/h/c/n/d/n/b;->a:Lf/h/c/n/d/n/a;

    invoke-virtual {v4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2}, Lb0/a0$a;->c(Ljava/lang/String;Lokhttp3/RequestBody;)Lb0/a0$a;

    invoke-virtual {v1}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object v1

    sget-object v2, Lf/h/c/n/d/n/b;->f:Lb0/y;

    invoke-virtual {v2, v1}, Lb0/y;->b(Lb0/a0;)Lb0/e;

    move-result-object v1

    check-cast v1, Lb0/g0/g/e;

    invoke-virtual {v1}, Lb0/g0/g/e;->execute()Lokhttp3/Response;

    move-result-object v1

    iget-object v2, v1, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-nez v2, :cond_5

    goto :goto_5

    :cond_5
    invoke-virtual {v2}, Lokhttp3/ResponseBody;->d()Ljava/lang/String;

    move-result-object v3

    :goto_5
    new-instance v2, Lf/h/c/n/d/n/d;

    iget v4, v1, Lokhttp3/Response;->g:I

    iget-object v1, v1, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-direct {v2, v4, v3, v1}, Lf/h/c/n/d/n/d;-><init>(ILjava/lang/String;Lokhttp3/Headers;)V

    return-object v2
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/n/d/n/b;
    .locals 3

    iget-object v0, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    if-nez v0, :cond_0

    new-instance v0, Lokhttp3/MultipartBody$a;

    invoke-direct {v0}, Lokhttp3/MultipartBody$a;-><init>()V

    sget-object v1, Lokhttp3/MultipartBody;->g:Lokhttp3/MediaType;

    invoke-virtual {v0, v1}, Lokhttp3/MultipartBody$a;->c(Lokhttp3/MediaType;)Lokhttp3/MultipartBody$a;

    iput-object v0, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    :cond_0
    iget-object v0, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "name"

    invoke-static {p1, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "value"

    invoke-static {p2, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lokhttp3/RequestBody$Companion;->a(Ljava/lang/String;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    move-result-object p2

    invoke-static {p1, v2, p2}, Lokhttp3/MultipartBody$Part;->b(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokhttp3/MultipartBody$a;->a(Lokhttp3/MultipartBody$Part;)Lokhttp3/MultipartBody$a;

    iput-object v0, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    return-object p0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lf/h/c/n/d/n/b;
    .locals 1

    sget-object v0, Lokhttp3/MediaType;->g:Lokhttp3/MediaType$a;

    invoke-static {p3}, Lokhttp3/MediaType$a;->b(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object p3

    invoke-static {p3, p4}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/io/File;)Lokhttp3/RequestBody;

    move-result-object p3

    iget-object p4, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    if-nez p4, :cond_0

    new-instance p4, Lokhttp3/MultipartBody$a;

    invoke-direct {p4}, Lokhttp3/MultipartBody$a;-><init>()V

    sget-object v0, Lokhttp3/MultipartBody;->g:Lokhttp3/MediaType;

    invoke-virtual {p4, v0}, Lokhttp3/MultipartBody$a;->c(Lokhttp3/MediaType;)Lokhttp3/MultipartBody$a;

    iput-object p4, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    :cond_0
    iget-object p4, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "name"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "body"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2, p3}, Lokhttp3/MultipartBody$Part;->b(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object p1

    invoke-virtual {p4, p1}, Lokhttp3/MultipartBody$a;->a(Lokhttp3/MultipartBody$Part;)Lokhttp3/MultipartBody$a;

    iput-object p4, p0, Lf/h/c/n/d/n/b;->e:Lokhttp3/MultipartBody$a;

    return-object p0
.end method
