.class public final enum Lf/h/c/n/d/n/a;
.super Ljava/lang/Enum;
.source "HttpMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/n/d/n/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/n/d/n/a;

.field public static final enum e:Lf/h/c/n/d/n/a;

.field public static final enum f:Lf/h/c/n/d/n/a;

.field public static final enum g:Lf/h/c/n/d/n/a;

.field public static final synthetic h:[Lf/h/c/n/d/n/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/h/c/n/d/n/a;

    const-string v1, "GET"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/c/n/d/n/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/c/n/d/n/a;->d:Lf/h/c/n/d/n/a;

    new-instance v1, Lf/h/c/n/d/n/a;

    const-string v3, "POST"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/c/n/d/n/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/c/n/d/n/a;->e:Lf/h/c/n/d/n/a;

    new-instance v3, Lf/h/c/n/d/n/a;

    const-string v5, "PUT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/h/c/n/d/n/a;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/h/c/n/d/n/a;->f:Lf/h/c/n/d/n/a;

    new-instance v5, Lf/h/c/n/d/n/a;

    const-string v7, "DELETE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/h/c/n/d/n/a;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/h/c/n/d/n/a;->g:Lf/h/c/n/d/n/a;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/h/c/n/d/n/a;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/h/c/n/d/n/a;->h:[Lf/h/c/n/d/n/a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/n/d/n/a;
    .locals 1

    const-class v0, Lf/h/c/n/d/n/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/n/d/n/a;

    return-object p0
.end method

.method public static values()[Lf/h/c/n/d/n/a;
    .locals 1

    sget-object v0, Lf/h/c/n/d/n/a;->h:[Lf/h/c/n/d/n/a;

    invoke-virtual {v0}, [Lf/h/c/n/d/n/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/n/d/n/a;

    return-object v0
.end method
