.class public Lf/h/c/n/c;
.super Ljava/lang/Object;
.source "FirebaseCrashlytics.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/h/c/n/d/h;

.field public final synthetic e:Ljava/util/concurrent/ExecutorService;

.field public final synthetic f:Lf/h/c/n/d/s/d;

.field public final synthetic g:Z

.field public final synthetic h:Lf/h/c/n/d/k/k0;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/h;Ljava/util/concurrent/ExecutorService;Lf/h/c/n/d/s/d;ZLf/h/c/n/d/k/k0;)V
    .locals 0

    iput-object p1, p0, Lf/h/c/n/c;->d:Lf/h/c/n/d/h;

    iput-object p2, p0, Lf/h/c/n/c;->e:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lf/h/c/n/c;->f:Lf/h/c/n/d/s/d;

    iput-boolean p4, p0, Lf/h/c/n/c;->g:Z

    iput-object p5, p0, Lf/h/c/n/c;->h:Lf/h/c/n/d/k/k0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/n/c;->d:Lf/h/c/n/d/h;

    iget-object v1, p0, Lf/h/c/n/c;->e:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lf/h/c/n/c;->f:Lf/h/c/n/d/s/d;

    iget-object v3, v0, Lf/h/c/n/d/h;->b:Lf/h/c/c;

    invoke-virtual {v3}, Lf/h/c/c;->a()V

    iget-object v3, v3, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v3, v3, Lf/h/c/i;->b:Ljava/lang/String;

    iget-object v4, v0, Lf/h/c/n/d/h;->m:Lf/h/c/n/d/k/q0;

    invoke-virtual {v4}, Lf/h/c/n/d/k/q0;->c()Lcom/google/android/gms/tasks/Task;

    move-result-object v4

    new-instance v5, Lf/h/c/n/d/f;

    invoke-direct {v5, v0, v2}, Lf/h/c/n/d/f;-><init>(Lf/h/c/n/d/h;Lf/h/c/n/d/s/d;)V

    invoke-virtual {v4, v1, v5}, Lcom/google/android/gms/tasks/Task;->r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object v4

    new-instance v5, Lf/h/c/n/d/e;

    invoke-direct {v5, v0, v3, v2, v1}, Lf/h/c/n/d/e;-><init>(Lf/h/c/n/d/h;Ljava/lang/String;Lf/h/c/n/d/s/d;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v4, v1, v5}, Lcom/google/android/gms/tasks/Task;->r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    iget-boolean v0, p0, Lf/h/c/n/c;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/n/c;->h:Lf/h/c/n/d/k/k0;

    iget-object v1, p0, Lf/h/c/n/c;->f:Lf/h/c/n/d/s/d;

    iget-object v2, v0, Lf/h/c/n/d/k/k0;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lf/h/c/n/d/k/j0;

    invoke-direct {v3, v0, v1}, Lf/h/c/n/d/k/j0;-><init>(Lf/h/c/n/d/k/k0;Lf/h/c/n/d/s/e;)V

    sget-object v0, Lf/h/c/n/d/k/i1;->a:Ljava/io/FilenameFilter;

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v1, Lf/h/c/n/d/k/k1;

    invoke-direct {v1, v3, v0}, Lf/h/c/n/d/k/k1;-><init>(Ljava/util/concurrent/Callable;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-interface {v2, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
