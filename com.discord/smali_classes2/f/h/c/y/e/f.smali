.class public Lf/h/c/y/e/f;
.super Ljava/lang/Object;
.source "MemoryGaugeCollector.java"


# static fields
.field public static final f:Lf/h/c/y/h/a;

.field public static final g:Lf/h/c/y/e/f;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/concurrent/ScheduledExecutorService;

.field public final b:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lf/h/c/y/m/b;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/Runtime;

.field public d:Ljava/util/concurrent/ScheduledFuture;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/e/f;->f:Lf/h/c/y/h/a;

    new-instance v0, Lf/h/c/y/e/f;

    invoke-direct {v0}, Lf/h/c/y/e/f;-><init>()V

    sput-object v0, Lf/h/c/y/e/f;->g:Lf/h/c/y/e/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    iput-object v2, p0, Lf/h/c/y/e/f;->d:Ljava/util/concurrent/ScheduledFuture;

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lf/h/c/y/e/f;->e:J

    iput-object v0, p0, Lf/h/c/y/e/f;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lf/h/c/y/e/f;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iput-object v1, p0, Lf/h/c/y/e/f;->c:Ljava/lang/Runtime;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(JLcom/google/firebase/perf/util/Timer;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lf/h/c/y/e/f;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lf/h/c/y/e/f;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lf/h/c/y/e/d;

    invoke-direct {v1, p0, p3}, Lf/h/c/y/e/d;-><init>(Lf/h/c/y/e/f;Lcom/google/firebase/perf/util/Timer;)V

    const-wide/16 v2, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v4, p1

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    iput-object p1, p0, Lf/h/c/y/e/f;->d:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    sget-object p2, Lf/h/c/y/e/f;->f:Lf/h/c/y/h/a;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unable to start collecting Memory Metrics: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/concurrent/RejectedExecutionException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p3, 0x0

    new-array p3, p3, [Ljava/lang/Object;

    invoke-virtual {p2, p1, p3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b(Lcom/google/firebase/perf/util/Timer;)Lf/h/c/y/m/b;
    .locals 5
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-wide v0, p1, Lcom/google/firebase/perf/util/Timer;->d:J

    invoke-virtual {p1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    add-long/2addr v2, v0

    invoke-static {}, Lf/h/c/y/m/b;->D()Lf/h/c/y/m/b$b;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/e/r$a;->r()V

    iget-object v0, p1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/b;

    invoke-static {v0, v2, v3}, Lf/h/c/y/m/b;->B(Lf/h/c/y/m/b;J)V

    sget-object v0, Lf/h/c/y/l/f;->h:Lf/h/c/y/l/f;

    iget-object v1, p0, Lf/h/c/y/e/f;->c:Ljava/lang/Runtime;

    invoke-virtual {v1}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v1

    iget-object v3, p0, Lf/h/c/y/e/f;->c:Ljava/lang/Runtime;

    invoke-virtual {v3}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/l/f;->f(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lf/h/c/y/l/g;->b(J)I

    move-result v0

    invoke-virtual {p1}, Lf/h/e/r$a;->r()V

    iget-object v1, p1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/b;

    invoke-static {v1, v0}, Lf/h/c/y/m/b;->C(Lf/h/c/y/m/b;I)V

    invoke-virtual {p1}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object p1

    check-cast p1, Lf/h/c/y/m/b;

    return-object p1
.end method
