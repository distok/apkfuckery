.class public final synthetic Lf/h/c/y/e/d;
.super Ljava/lang/Object;
.source "MemoryGaugeCollector.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/c/y/e/f;

.field public final e:Lcom/google/firebase/perf/util/Timer;


# direct methods
.method public constructor <init>(Lf/h/c/y/e/f;Lcom/google/firebase/perf/util/Timer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/y/e/d;->d:Lf/h/c/y/e/f;

    iput-object p2, p0, Lf/h/c/y/e/d;->e:Lcom/google/firebase/perf/util/Timer;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lf/h/c/y/e/d;->d:Lf/h/c/y/e/f;

    iget-object v1, p0, Lf/h/c/y/e/d;->e:Lcom/google/firebase/perf/util/Timer;

    sget-object v2, Lf/h/c/y/e/f;->f:Lf/h/c/y/h/a;

    invoke-virtual {v0, v1}, Lf/h/c/y/e/f;->b(Lcom/google/firebase/perf/util/Timer;)Lf/h/c/y/m/b;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/h/c/y/e/f;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
