.class public Lf/h/c/y/d/a;
.super Ljava/lang/Object;
.source "ConfigResolver.java"


# static fields
.field public static final d:Lf/h/c/y/h/a;

.field public static volatile e:Lf/h/c/y/d/a;


# instance fields
.field public a:Lf/h/c/y/l/d;

.field public b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

.field public c:Lf/h/c/y/d/v;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/d/a;->d:Lf/h/c/y/h/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/firebase/perf/internal/RemoteConfigManager;Lf/h/c/y/l/d;Lf/h/c/y/d/v;)V
    .locals 0
    .param p1    # Lcom/google/firebase/perf/internal/RemoteConfigManager;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lf/h/c/y/l/d;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lf/h/c/y/d/v;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getInstance()Lcom/google/firebase/perf/internal/RemoteConfigManager;

    move-result-object p1

    iput-object p1, p0, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    new-instance p1, Lf/h/c/y/l/d;

    invoke-direct {p1}, Lf/h/c/y/l/d;-><init>()V

    iput-object p1, p0, Lf/h/c/y/d/a;->a:Lf/h/c/y/l/d;

    sget-object p1, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    const-class p1, Lf/h/c/y/d/v;

    monitor-enter p1

    :try_start_0
    sget-object p2, Lf/h/c/y/d/v;->c:Lf/h/c/y/d/v;

    if-nez p2, :cond_0

    new-instance p2, Lf/h/c/y/d/v;

    invoke-direct {p2}, Lf/h/c/y/d/v;-><init>()V

    sput-object p2, Lf/h/c/y/d/v;->c:Lf/h/c/y/d/v;

    :cond_0
    sget-object p2, Lf/h/c/y/d/v;->c:Lf/h/c/y/d/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    iput-object p2, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2
.end method

.method public static declared-synchronized f()Lf/h/c/y/d/a;
    .locals 3

    const-class v0, Lf/h/c/y/d/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/c/y/d/a;->e:Lf/h/c/y/d/a;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/c/y/d/a;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2, v2}, Lf/h/c/y/d/a;-><init>(Lcom/google/firebase/perf/internal/RemoteConfigManager;Lf/h/c/y/l/d;Lf/h/c/y/d/v;)V

    sput-object v1, Lf/h/c/y/d/a;->e:Lf/h/c/y/d/a;

    :cond_0
    sget-object v1, Lf/h/c/y/d/a;->e:Lf/h/c/y/d/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 6

    sget-object v0, Lf/h/c/y/d/d;->a:Lf/h/c/y/d/d;

    const-class v0, Lf/h/c/y/d/d;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/c/y/d/d;->a:Lf/h/c/y/d/d;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/c/y/d/d;

    invoke-direct {v1}, Lf/h/c/y/d/d;-><init>()V

    sput-object v1, Lf/h/c/y/d/d;->a:Lf/h/c/y/d/d;

    :cond_0
    sget-object v1, Lf/h/c/y/d/d;->a:Lf/h/c/y/d/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    sget v0, Lf/h/c/y/a;->a:I

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "fpr_log_source"

    iget-object v2, p0, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    const-wide/16 v3, -0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getRemoteConfigValueOrDefault(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "com.google.firebase.perf.LogSourceName"

    sget-object v4, Lf/h/c/y/d/d;->b:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-virtual {v1, v0, v2}, Lf/h/c/y/d/v;->e(Ljava/lang/String;Ljava/lang/String;)Z

    return-object v2

    :cond_1
    invoke-virtual {p0, v1}, Lf/h/c/y/d/a;->e(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_2
    const-string v0, "FIREPERF"

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final b(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/c/y/l/e;->b:Lf/h/c/y/l/e;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "Key is null when getting boolean value on device cache."

    invoke-virtual {p1, v2, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lf/h/c/y/d/v;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lf/h/c/y/d/v;->b(Landroid/content/Context;)V

    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    new-instance v3, Lf/h/c/y/l/e;

    invoke-direct {v3, v0}, Lf/h/c/y/l/e;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v3

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    const/4 p1, 0x1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, p1

    const-string p1, "Key %s from sharedPreferences has type other than long: %s"

    invoke-static {p1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method public final c(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/Float;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/c/y/l/e;->b:Lf/h/c/y/l/e;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "Key is null when getting float value on device cache."

    invoke-virtual {p1, v2, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lf/h/c/y/d/v;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lf/h/c/y/d/v;->b(Landroid/content/Context;)V

    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v0, p1, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    new-instance v3, Lf/h/c/y/l/e;

    invoke-direct {v3, v0}, Lf/h/c/y/l/e;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v3

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    const/4 p1, 0x1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, p1

    const-string p1, "Key %s from sharedPreferences has type other than float: %s"

    invoke-static {p1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method public final d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/Long;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/c/y/l/e;->b:Lf/h/c/y/l/e;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "Key is null when getting long value on device cache."

    invoke-virtual {p1, v2, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lf/h/c/y/d/v;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lf/h/c/y/d/v;->b(Landroid/content/Context;)V

    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    const-wide/16 v3, 0x0

    invoke-interface {v0, p1, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v3, Lf/h/c/y/l/e;

    invoke-direct {v3, v0}, Lf/h/c/y/l/e;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v3

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    const/4 p1, 0x1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, p1

    const-string p1, "Key %s from sharedPreferences has type other than long: %s"

    invoke-static {p1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method public final e(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/String;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/c/y/l/e;->b:Lf/h/c/y/l/e;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "Key is null when getting String value on device cache."

    invoke-virtual {p1, v2, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lf/h/c/y/d/v;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lf/h/c/y/d/v;->b(Landroid/content/Context;)V

    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, v0, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    const-string v3, ""

    invoke-interface {v0, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lf/h/c/y/l/e;

    invoke-direct {v3, v0}, Lf/h/c/y/l/e;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v3

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lf/h/c/y/d/v;->b:Lf/h/c/y/h/a;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    const/4 p1, 0x1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, p1

    const-string p1, "Key %s from sharedPreferences has type other than String: %s"

    invoke-static {p1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method public g()Ljava/lang/Boolean;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-class v1, Lf/h/c/y/d/b;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lf/h/c/y/d/b;->a:Lf/h/c/y/d/b;

    if-nez v2, :cond_0

    new-instance v2, Lf/h/c/y/d/b;

    invoke-direct {v2}, Lf/h/c/y/d/b;-><init>()V

    sput-object v2, Lf/h/c/y/d/b;->a:Lf/h/c/y/d/b;

    :cond_0
    sget-object v2, Lf/h/c/y/d/b;->a:Lf/h/c/y/d/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v1

    invoke-virtual {p0, v2}, Lf/h/c/y/d/a;->h(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    goto :goto_0

    :cond_1
    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    return-object v0

    :cond_2
    const-class v0, Lf/h/c/y/d/c;

    monitor-enter v0

    :try_start_1
    sget-object v1, Lf/h/c/y/d/c;->a:Lf/h/c/y/d/c;

    if-nez v1, :cond_3

    new-instance v1, Lf/h/c/y/d/c;

    invoke-direct {v1}, Lf/h/c/y/d/c;-><init>()V

    sput-object v1, Lf/h/c/y/d/c;->a:Lf/h/c/y/d/c;

    :cond_3
    sget-object v1, Lf/h/c/y/d/c;->a:Lf/h/c/y/d/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    invoke-virtual {p0, v1}, Lf/h/c/y/d/a;->b(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0

    :cond_4
    invoke-virtual {p0, v1}, Lf/h/c/y/d/a;->h(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0

    :cond_5
    sget-object v0, Lf/h/c/y/d/a;->d:Lf/h/c/y/h/a;

    const-string v1, "CollectionEnabled metadata key unknown or value not found in manifest."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->a:Lf/h/c/y/l/d;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->b()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lf/h/c/y/l/e;->b:Lf/h/c/y/l/e;

    invoke-virtual {v0, p1}, Lf/h/c/y/l/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object v0, v0, Lf/h/c/y/l/d;->a:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Lf/h/c/y/l/e;->c(Ljava/lang/Object;)Lf/h/c/y/l/e;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lf/h/c/y/l/d;->b:Lf/h/c/y/h/a;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 p1, 0x1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, p1

    const-string p1, "Metadata key %s contains type other than boolean: %s"

    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method public final i(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/Long;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->a:Lf/h/c/y/l/d;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->b()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lf/h/c/y/l/e;->b:Lf/h/c/y/l/e;

    invoke-virtual {v0, p1}, Lf/h/c/y/l/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object v0, v0, Lf/h/c/y/l/d;->a:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Lf/h/c/y/l/e;->c(Ljava/lang/Object;)Lf/h/c/y/l/e;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v2, Lf/h/c/y/l/d;->b:Lf/h/c/y/h/a;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 p1, 0x1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, p1

    const-string p1, "Metadata key %s contains type other than int: %s"

    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    move-object p1, v1

    :goto_1
    invoke-virtual {p1}, Lf/h/c/y/l/e;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance v1, Lf/h/c/y/l/e;

    invoke-direct {v1, p1}, Lf/h/c/y/l/e;-><init>(Ljava/lang/Object;)V

    :cond_1
    return-object v1
.end method

.method public j()J
    .locals 8

    const-class v0, Lf/h/c/y/d/h;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/c/y/d/h;->a:Lf/h/c/y/d/h;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/c/y/d/h;

    invoke-direct {v1}, Lf/h/c/y/d/h;-><init>()V

    sput-object v1, Lf/h/c/y/d/h;->a:Lf/h/c/y/d/h;

    :cond_0
    sget-object v1, Lf/h/c/y/d/h;->a:Lf/h/c/y/d/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    invoke-virtual {p0, v1}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v2

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v3

    if-lez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    iget-object v2, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.google.firebase.perf.TimeLimitSec"

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-static {v3, v2, v1, v0}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_2
    invoke-virtual {p0, v1}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v3

    if-lez v2, :cond_3

    const/4 v5, 0x1

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_4
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v0, 0x258

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final k(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/Float;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getFloat(Ljava/lang/String;)Lf/h/c/y/l/e;

    move-result-object p1

    return-object p1
.end method

.method public final l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/y/d/u<",
            "Ljava/lang/Long;",
            ">;)",
            "Lf/h/c/y/l/e<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    invoke-virtual {p1}, Lf/h/c/y/d/u;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getLong(Ljava/lang/String;)Lf/h/c/y/l/e;

    move-result-object p1

    return-object p1
.end method

.method public final m(J)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final n(Ljava/lang/String;)Z
    .locals 5

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget-object v3, p1, v2

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    sget v4, Lf/h/c/y/a;->a:I

    const-string v4, "19.0.10"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method public final o(J)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public p()Z
    .locals 7

    invoke-virtual {p0}, Lf/h/c/y/d/a;->g()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v2, :cond_a

    :cond_0
    const-class v0, Lf/h/c/y/d/j;

    monitor-enter v0

    :try_start_0
    sget-object v3, Lf/h/c/y/d/j;->a:Lf/h/c/y/d/j;

    if-nez v3, :cond_1

    new-instance v3, Lf/h/c/y/d/j;

    invoke-direct {v3}, Lf/h/c/y/d/j;-><init>()V

    sput-object v3, Lf/h/c/y/d/j;->a:Lf/h/c/y/d/j;

    :cond_1
    sget-object v3, Lf/h/c/y/d/j;->a:Lf/h/c/y/d/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v0

    iget-object v0, p0, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "fpr_enabled"

    invoke-virtual {v0, v4}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getBoolean(Ljava/lang/String;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v3, p0, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    invoke-virtual {v3}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->isLastFetchFailed()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    const-string v4, "com.google.firebase.perf.SdkEnabled"

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, v3, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v6, :cond_3

    invoke-virtual {v3}, Lf/h/c/y/d/v;->a()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v3, v6}, Lf/h/c/y/d/v;->b(Landroid/content/Context;)V

    iget-object v6, v3, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    if-nez v6, :cond_3

    goto :goto_0

    :cond_3
    iget-object v3, v3, Lf/h/c/y/d/v;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v3}, Lf/h/c/y/d/a;->b(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_9

    const-class v0, Lf/h/c/y/d/i;

    monitor-enter v0

    :try_start_1
    sget-object v3, Lf/h/c/y/d/i;->a:Lf/h/c/y/d/i;

    if-nez v3, :cond_6

    new-instance v3, Lf/h/c/y/d/i;

    invoke-direct {v3}, Lf/h/c/y/d/i;-><init>()V

    sput-object v3, Lf/h/c/y/d/i;->a:Lf/h/c/y/d/i;

    :cond_6
    sget-object v3, Lf/h/c/y/d/i;->a:Lf/h/c/y/d/i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    iget-object v0, p0, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "fpr_disabled_android_versions"

    invoke-virtual {v0, v4}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getString(Ljava/lang/String;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v3, p0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    const-string v4, "com.google.firebase.perf.SdkDisabledVersions"

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lf/h/c/y/d/v;->e(Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lf/h/c/y/d/a;->n(Ljava/lang/String;)Z

    move-result v0

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v3}, Lf/h/c/y/d/a;->e(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lf/h/c/y/d/a;->n(Ljava/lang/String;)Z

    move-result v0

    goto :goto_2

    :cond_8
    const-string v0, ""

    invoke-virtual {p0, v0}, Lf/h/c/y/d/a;->n(Ljava/lang/String;)Z

    move-result v0

    :goto_2
    if-nez v0, :cond_9

    const/4 v0, 0x1

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_9
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_a

    const/4 v1, 0x1

    :cond_a
    return v1

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final q(F)Z
    .locals 1

    const/4 v0, 0x0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float p1, p1, v0

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final r(J)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final s(F)Z
    .locals 1

    const/4 v0, 0x0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    cmpg-float p1, p1, v0

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
