.class public Lf/h/c/y/h/a;
.super Ljava/lang/Object;
.source "AndroidLogger.java"


# static fields
.field public static volatile c:Lf/h/c/y/h/a;


# instance fields
.field public final a:Lf/h/c/y/h/b;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/c/y/h/a;->b:Z

    const-class v0, Lf/h/c/y/h/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/c/y/h/b;->a:Lf/h/c/y/h/b;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/c/y/h/b;

    invoke-direct {v1}, Lf/h/c/y/h/b;-><init>()V

    sput-object v1, Lf/h/c/y/h/b;->a:Lf/h/c/y/h/b;

    :cond_0
    sget-object v1, Lf/h/c/y/h/b;->a:Lf/h/c/y/h/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    iput-object v1, p0, Lf/h/c/y/h/a;->a:Lf/h/c/y/h/b;

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static c()Lf/h/c/y/h/a;
    .locals 2

    sget-object v0, Lf/h/c/y/h/a;->c:Lf/h/c/y/h/a;

    if-nez v0, :cond_1

    const-class v0, Lf/h/c/y/h/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/c/y/h/a;->c:Lf/h/c/y/h/a;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/c/y/h/a;

    invoke-direct {v1}, Lf/h/c/y/h/a;-><init>()V

    sput-object v1, Lf/h/c/y/h/a;->c:Lf/h/c/y/h/a;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lf/h/c/y/h/a;->c:Lf/h/c/y/h/a;

    return-object v0
.end method


# virtual methods
.method public varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    iget-boolean v0, p0, Lf/h/c/y/h/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/h/a;->a:Lf/h/c/y/h/b;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v1, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "FirebasePerformance"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    iget-boolean v0, p0, Lf/h/c/y/h/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/h/a;->a:Lf/h/c/y/h/b;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v1, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "FirebasePerformance"

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    iget-boolean v0, p0, Lf/h/c/y/h/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/h/a;->a:Lf/h/c/y/h/b;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v1, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "FirebasePerformance"

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public varargs e(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    iget-boolean v0, p0, Lf/h/c/y/h/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/h/a;->a:Lf/h/c/y/h/b;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v1, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "FirebasePerformance"

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
