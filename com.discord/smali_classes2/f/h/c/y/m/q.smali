.class public final Lf/h/c/y/m/q;
.super Lf/h/e/r;
.source "PerfSession.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/q$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/q;",
        "Lf/h/c/y/m/q$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/q;

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/q;",
            ">;"
        }
    .end annotation
.end field

.field public static final SESSION_ID_FIELD_NUMBER:I = 0x1

.field public static final SESSION_VERBOSITY_FIELD_NUMBER:I = 0x2

.field private static final sessionVerbosity_converter_:Lf/h/e/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/u<",
            "Ljava/lang/Integer;",
            "Lf/h/c/y/m/s;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bitField0_:I

.field private sessionId_:Ljava/lang/String;

.field private sessionVerbosity_:Lf/h/e/t$c;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/q$a;

    invoke-direct {v0}, Lf/h/c/y/m/q$a;-><init>()V

    sput-object v0, Lf/h/c/y/m/q;->sessionVerbosity_converter_:Lf/h/e/u;

    new-instance v0, Lf/h/c/y/m/q;

    invoke-direct {v0}, Lf/h/c/y/m/q;-><init>()V

    sput-object v0, Lf/h/c/y/m/q;->DEFAULT_INSTANCE:Lf/h/c/y/m/q;

    const-class v1, Lf/h/c/y/m/q;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lf/h/c/y/m/q;->sessionId_:Ljava/lang/String;

    sget-object v0, Lf/h/e/s;->g:Lf/h/e/s;

    iput-object v0, p0, Lf/h/c/y/m/q;->sessionVerbosity_:Lf/h/e/t$c;

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/q;
    .locals 1

    sget-object v0, Lf/h/c/y/m/q;->DEFAULT_INSTANCE:Lf/h/c/y/m/q;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/q;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/q;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/c/y/m/q;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/q;->sessionId_:Ljava/lang/String;

    return-void
.end method

.method public static C(Lf/h/c/y/m/q;Lf/h/c/y/m/s;)V
    .locals 2

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lf/h/c/y/m/q;->sessionVerbosity_:Lf/h/e/t$c;

    move-object v1, v0

    check-cast v1, Lf/h/e/c;

    iget-boolean v1, v1, Lf/h/e/c;->d:Z

    if-nez v1, :cond_1

    check-cast v0, Lf/h/e/s;

    iget v1, v0, Lf/h/e/s;->f:I

    if-nez v1, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :cond_0
    mul-int/lit8 v1, v1, 0x2

    :goto_0
    invoke-virtual {v0, v1}, Lf/h/e/s;->k(I)Lf/h/e/t$c;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/q;->sessionVerbosity_:Lf/h/e/t$c;

    :cond_1
    iget-object p0, p0, Lf/h/c/y/m/q;->sessionVerbosity_:Lf/h/e/t$c;

    invoke-virtual {p1}, Lf/h/c/y/m/s;->getNumber()I

    move-result p1

    check-cast p0, Lf/h/e/s;

    invoke-virtual {p0, p1}, Lf/h/e/s;->d(I)V

    return-void
.end method

.method public static F()Lf/h/c/y/m/q$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/q;->DEFAULT_INSTANCE:Lf/h/c/y/m/q;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/q$b;

    return-object v0
.end method


# virtual methods
.method public D(I)Lf/h/c/y/m/s;
    .locals 2

    sget-object v0, Lf/h/c/y/m/q;->sessionVerbosity_converter_:Lf/h/e/u;

    iget-object v1, p0, Lf/h/c/y/m/q;->sessionVerbosity_:Lf/h/e/t$c;

    check-cast v1, Lf/h/e/s;

    invoke-virtual {v1, p1}, Lf/h/e/s;->e(I)V

    iget-object v1, v1, Lf/h/e/s;->e:[I

    aget p1, v1, p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    check-cast v0, Lf/h/c/y/m/q$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lf/h/c/y/m/s;->f(I)Lf/h/c/y/m/s;

    move-result-object p1

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/y/m/s;->d:Lf/h/c/y/m/s;

    :cond_0
    return-object p1
.end method

.method public E()I
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/q;->sessionVerbosity_:Lf/h/e/t$c;

    check-cast v0, Lf/h/e/s;

    invoke-virtual {v0}, Lf/h/e/s;->size()I

    move-result v0

    return v0
.end method

.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/q;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/q;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/q;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/q;->DEFAULT_INSTANCE:Lf/h/c/y/m/q;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/q;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/q;->DEFAULT_INSTANCE:Lf/h/c/y/m/q;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/q$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/q$b;-><init>(Lf/h/c/y/m/q$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/q;

    invoke-direct {p1}, Lf/h/c/y/m/q;-><init>()V

    return-object p1

    :pswitch_4
    const/4 p1, 0x4

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "sessionId_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "sessionVerbosity_"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    sget-object p3, Lf/h/c/y/m/s$a;->a:Lf/h/e/t$b;

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u0008\u0000\u0002\u001e"

    sget-object p3, Lf/h/c/y/m/q;->DEFAULT_INSTANCE:Lf/h/c/y/m/q;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
