.class public final Lf/h/c/y/m/n;
.super Lf/h/e/r;
.source "NetworkRequestMetric.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/n$b;,
        Lf/h/c/y/m/n$c;,
        Lf/h/c/y/m/n$e;,
        Lf/h/c/y/m/n$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/n;",
        "Lf/h/c/y/m/n$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final CLIENT_START_TIME_US_FIELD_NUMBER:I = 0x7

.field public static final CUSTOM_ATTRIBUTES_FIELD_NUMBER:I = 0xc

.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/n;

.field public static final HTTP_METHOD_FIELD_NUMBER:I = 0x2

.field public static final HTTP_RESPONSE_CODE_FIELD_NUMBER:I = 0x5

.field public static final NETWORK_CLIENT_ERROR_REASON_FIELD_NUMBER:I = 0xb

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/n;",
            ">;"
        }
    .end annotation
.end field

.field public static final PERF_SESSIONS_FIELD_NUMBER:I = 0xd

.field public static final REQUEST_PAYLOAD_BYTES_FIELD_NUMBER:I = 0x3

.field public static final RESPONSE_CONTENT_TYPE_FIELD_NUMBER:I = 0x6

.field public static final RESPONSE_PAYLOAD_BYTES_FIELD_NUMBER:I = 0x4

.field public static final TIME_TO_REQUEST_COMPLETED_US_FIELD_NUMBER:I = 0x8

.field public static final TIME_TO_RESPONSE_COMPLETED_US_FIELD_NUMBER:I = 0xa

.field public static final TIME_TO_RESPONSE_INITIATED_US_FIELD_NUMBER:I = 0x9

.field public static final URL_FIELD_NUMBER:I = 0x1


# instance fields
.field private bitField0_:I

.field private clientStartTimeUs_:J

.field private customAttributes_:Lf/h/e/e0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/e0<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private httpMethod_:I

.field private httpResponseCode_:I

.field private networkClientErrorReason_:I

.field private perfSessions_:Lf/h/e/t$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/t$d<",
            "Lf/h/c/y/m/q;",
            ">;"
        }
    .end annotation
.end field

.field private requestPayloadBytes_:J

.field private responseContentType_:Ljava/lang/String;

.field private responsePayloadBytes_:J

.field private timeToRequestCompletedUs_:J

.field private timeToResponseCompletedUs_:J

.field private timeToResponseInitiatedUs_:J

.field private url_:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/n;

    invoke-direct {v0}, Lf/h/c/y/m/n;-><init>()V

    sput-object v0, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    const-class v1, Lf/h/c/y/m/n;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    sget-object v0, Lf/h/e/e0;->d:Lf/h/e/e0;

    iput-object v0, p0, Lf/h/c/y/m/n;->customAttributes_:Lf/h/e/e0;

    const-string v0, ""

    iput-object v0, p0, Lf/h/c/y/m/n;->url_:Ljava/lang/String;

    iput-object v0, p0, Lf/h/c/y/m/n;->responseContentType_:Ljava/lang/String;

    sget-object v0, Lf/h/e/v0;->g:Lf/h/e/v0;

    iput-object v0, p0, Lf/h/c/y/m/n;->perfSessions_:Lf/h/e/t$d;

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/n;
    .locals 1

    sget-object v0, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/n;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/n;->url_:Ljava/lang/String;

    return-void
.end method

.method public static C(Lf/h/c/y/m/n;Lf/h/c/y/m/n$e;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lf/h/c/y/m/n$e;->getNumber()I

    move-result p1

    iput p1, p0, Lf/h/c/y/m/n;->networkClientErrorReason_:I

    iget p1, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit8 p1, p1, 0x10

    iput p1, p0, Lf/h/c/y/m/n;->bitField0_:I

    return-void
.end method

.method public static D(Lf/h/c/y/m/n;I)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput p1, p0, Lf/h/c/y/m/n;->httpResponseCode_:I

    return-void
.end method

.method public static E(Lf/h/c/y/m/n;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/n;->responseContentType_:Ljava/lang/String;

    return-void
.end method

.method public static F(Lf/h/c/y/m/n;)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    sget-object v0, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    iget-object v0, v0, Lf/h/c/y/m/n;->responseContentType_:Ljava/lang/String;

    iput-object v0, p0, Lf/h/c/y/m/n;->responseContentType_:Ljava/lang/String;

    return-void
.end method

.method public static G(Lf/h/c/y/m/n;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/n;->clientStartTimeUs_:J

    return-void
.end method

.method public static H(Lf/h/c/y/m/n;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/n;->timeToRequestCompletedUs_:J

    return-void
.end method

.method public static I(Lf/h/c/y/m/n;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/n;->timeToResponseInitiatedUs_:J

    return-void
.end method

.method public static J(Lf/h/c/y/m/n;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/n;->timeToResponseCompletedUs_:J

    return-void
.end method

.method public static K(Lf/h/c/y/m/n;Ljava/lang/Iterable;)V
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/n;->perfSessions_:Lf/h/e/t$d;

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/n;->perfSessions_:Lf/h/e/t$d;

    invoke-static {v0}, Lf/h/e/r;->x(Lf/h/e/t$d;)Lf/h/e/t$d;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/n;->perfSessions_:Lf/h/e/t$d;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/n;->perfSessions_:Lf/h/e/t$d;

    invoke-static {p1, p0}, Lf/h/e/a;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method public static L(Lf/h/c/y/m/n;Lf/h/c/y/m/n$d;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lf/h/c/y/m/n$d;->getNumber()I

    move-result p1

    iput p1, p0, Lf/h/c/y/m/n;->httpMethod_:I

    iget p1, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lf/h/c/y/m/n;->bitField0_:I

    return-void
.end method

.method public static M(Lf/h/c/y/m/n;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/n;->requestPayloadBytes_:J

    return-void
.end method

.method public static N(Lf/h/c/y/m/n;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/n;->responsePayloadBytes_:J

    return-void
.end method

.method public static P()Lf/h/c/y/m/n;
    .locals 1

    sget-object v0, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    return-object v0
.end method

.method public static h0()Lf/h/c/y/m/n$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/n$b;

    return-object v0
.end method


# virtual methods
.method public O()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/m/n;->clientStartTimeUs_:J

    return-wide v0
.end method

.method public Q()Lf/h/c/y/m/n$d;
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->httpMethod_:I

    invoke-static {v0}, Lf/h/c/y/m/n$d;->f(I)Lf/h/c/y/m/n$d;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lf/h/c/y/m/n$d;->d:Lf/h/c/y/m/n$d;

    :cond_0
    return-object v0
.end method

.method public R()I
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->httpResponseCode_:I

    return v0
.end method

.method public S()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/y/m/q;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/m/n;->perfSessions_:Lf/h/e/t$d;

    return-object v0
.end method

.method public T()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/m/n;->requestPayloadBytes_:J

    return-wide v0
.end method

.method public U()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/m/n;->responsePayloadBytes_:J

    return-wide v0
.end method

.method public V()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/m/n;->timeToRequestCompletedUs_:J

    return-wide v0
.end method

.method public W()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/m/n;->timeToResponseCompletedUs_:J

    return-wide v0
.end method

.method public X()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/m/n;->timeToResponseInitiatedUs_:J

    return-wide v0
.end method

.method public Y()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/n;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public Z()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public a0()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b0()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c0()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d0()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e0()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f0()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g0()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/n;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/n;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/n;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/n;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/n$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/n$b;-><init>(Lf/h/c/y/m/n$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/n;

    invoke-direct {p1}, Lf/h/c/y/m/n;-><init>()V

    return-object p1

    :pswitch_4
    const/16 p1, 0x12

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "url_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "httpMethod_"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    sget-object p3, Lf/h/c/y/m/n$d$a;->a:Lf/h/e/t$b;

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string p3, "requestPayloadBytes_"

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-string p3, "responsePayloadBytes_"

    aput-object p3, p1, p2

    const/4 p2, 0x6

    const-string p3, "httpResponseCode_"

    aput-object p3, p1, p2

    const/4 p2, 0x7

    const-string p3, "responseContentType_"

    aput-object p3, p1, p2

    const/16 p2, 0x8

    const-string p3, "clientStartTimeUs_"

    aput-object p3, p1, p2

    const/16 p2, 0x9

    const-string p3, "timeToRequestCompletedUs_"

    aput-object p3, p1, p2

    const/16 p2, 0xa

    const-string p3, "timeToResponseInitiatedUs_"

    aput-object p3, p1, p2

    const/16 p2, 0xb

    const-string p3, "timeToResponseCompletedUs_"

    aput-object p3, p1, p2

    const/16 p2, 0xc

    const-string p3, "networkClientErrorReason_"

    aput-object p3, p1, p2

    const/16 p2, 0xd

    sget-object p3, Lf/h/c/y/m/n$e$a;->a:Lf/h/e/t$b;

    aput-object p3, p1, p2

    const/16 p2, 0xe

    const-string p3, "customAttributes_"

    aput-object p3, p1, p2

    const/16 p2, 0xf

    sget-object p3, Lf/h/c/y/m/n$c;->a:Lf/h/e/d0;

    aput-object p3, p1, p2

    const/16 p2, 0x10

    const-string p3, "perfSessions_"

    aput-object p3, p1, p2

    const/16 p2, 0x11

    const-class p3, Lf/h/c/y/m/q;

    aput-object p3, p1, p2

    const-string p2, "\u0001\r\u0000\u0001\u0001\r\r\u0001\u0001\u0000\u0001\u0008\u0000\u0002\u000c\u0001\u0003\u0002\u0002\u0004\u0002\u0003\u0005\u0004\u0005\u0006\u0008\u0006\u0007\u0002\u0007\u0008\u0002\u0008\t\u0002\t\n\u0002\n\u000b\u000c\u0004\u000c2\r\u001b"

    sget-object p3, Lf/h/c/y/m/n;->DEFAULT_INSTANCE:Lf/h/c/y/m/n;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
