.class public final Lf/h/c/y/m/t$b;
.super Lf/h/e/r$a;
.source "TraceMetric.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/y/m/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r$a<",
        "Lf/h/c/y/m/t;",
        "Lf/h/c/y/m/t$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/m/t;->A()Lf/h/c/y/m/t;

    move-result-object v0

    invoke-direct {p0, v0}, Lf/h/e/r$a;-><init>(Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>(Lf/h/c/y/m/t$a;)V
    .locals 0

    invoke-static {}, Lf/h/c/y/m/t;->A()Lf/h/c/y/m/t;

    move-result-object p1

    invoke-direct {p0, p1}, Lf/h/e/r$a;-><init>(Lf/h/e/r;)V

    return-void
.end method


# virtual methods
.method public A(J)Lf/h/c/y/m/t$b;
    .locals 1

    invoke-virtual {p0}, Lf/h/e/r$a;->r()V

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/t;

    invoke-static {v0, p1, p2}, Lf/h/c/y/m/t;->J(Lf/h/c/y/m/t;J)V

    return-object p0
.end method

.method public B(Ljava/lang/String;)Lf/h/c/y/m/t$b;
    .locals 1

    invoke-virtual {p0}, Lf/h/e/r$a;->r()V

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/t;

    invoke-static {v0, p1}, Lf/h/c/y/m/t;->B(Lf/h/c/y/m/t;Ljava/lang/String;)V

    return-object p0
.end method

.method public w(Ljava/lang/String;J)Lf/h/c/y/m/t$b;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0}, Lf/h/e/r$a;->r()V

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/t;

    invoke-static {v0}, Lf/h/c/y/m/t;->C(Lf/h/c/y/m/t;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    check-cast v0, Lf/h/e/e0;

    invoke-virtual {v0, p1, p2}, Lf/h/e/e0;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public x(J)Lf/h/c/y/m/t$b;
    .locals 1

    invoke-virtual {p0}, Lf/h/e/r$a;->r()V

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/t;

    invoke-static {v0, p1, p2}, Lf/h/c/y/m/t;->I(Lf/h/c/y/m/t;J)V

    return-object p0
.end method
