.class public final Lf/h/c/y/m/o;
.super Lf/h/e/r;
.source "PerfMetric.java"

# interfaces
.implements Lf/h/c/y/m/p;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/o$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/o;",
        "Lf/h/c/y/m/o$b;",
        ">;",
        "Lf/h/c/y/m/p;"
    }
.end annotation


# static fields
.field public static final APPLICATION_INFO_FIELD_NUMBER:I = 0x1

.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/o;

.field public static final GAUGE_METRIC_FIELD_NUMBER:I = 0x4

.field public static final NETWORK_REQUEST_METRIC_FIELD_NUMBER:I = 0x3

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/o;",
            ">;"
        }
    .end annotation
.end field

.field public static final TRACE_METRIC_FIELD_NUMBER:I = 0x2

.field public static final TRANSPORT_INFO_FIELD_NUMBER:I = 0x5


# instance fields
.field private applicationInfo_:Lf/h/c/y/m/c;

.field private bitField0_:I

.field private gaugeMetric_:Lf/h/c/y/m/h;

.field private networkRequestMetric_:Lf/h/c/y/m/n;

.field private traceMetric_:Lf/h/c/y/m/t;

.field private transportInfo_:Lf/h/c/y/m/u;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/o;

    invoke-direct {v0}, Lf/h/c/y/m/o;-><init>()V

    sput-object v0, Lf/h/c/y/m/o;->DEFAULT_INSTANCE:Lf/h/c/y/m/o;

    const-class v1, Lf/h/c/y/m/o;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/o;
    .locals 1

    sget-object v0, Lf/h/c/y/m/o;->DEFAULT_INSTANCE:Lf/h/c/y/m/o;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/o;Lf/h/c/y/m/c;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/c/y/m/o;->applicationInfo_:Lf/h/c/y/m/c;

    iget p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    return-void
.end method

.method public static C(Lf/h/c/y/m/o;Lf/h/c/y/m/h;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lf/h/c/y/m/o;->gaugeMetric_:Lf/h/c/y/m/h;

    iget p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    or-int/lit8 p1, p1, 0x8

    iput p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    return-void
.end method

.method public static D(Lf/h/c/y/m/o;Lf/h/c/y/m/t;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lf/h/c/y/m/o;->traceMetric_:Lf/h/c/y/m/t;

    iget p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    return-void
.end method

.method public static E(Lf/h/c/y/m/o;Lf/h/c/y/m/n;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lf/h/c/y/m/o;->networkRequestMetric_:Lf/h/c/y/m/n;

    iget p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    or-int/lit8 p1, p1, 0x4

    iput p1, p0, Lf/h/c/y/m/o;->bitField0_:I

    return-void
.end method

.method public static H()Lf/h/c/y/m/o$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/o;->DEFAULT_INSTANCE:Lf/h/c/y/m/o;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/o$b;

    return-object v0
.end method


# virtual methods
.method public F()Lf/h/c/y/m/c;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/o;->applicationInfo_:Lf/h/c/y/m/c;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/c/y/m/c;->I()Lf/h/c/y/m/c;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public G()Z
    .locals 2

    iget v0, p0, Lf/h/c/y/m/o;->bitField0_:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public e()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/o;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/o;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public j()Lf/h/c/y/m/t;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/o;->traceMetric_:Lf/h/c/y/m/t;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/c/y/m/t;->N()Lf/h/c/y/m/t;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public l()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/o;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public m()Lf/h/c/y/m/n;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/o;->networkRequestMetric_:Lf/h/c/y/m/n;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/c/y/m/n;->P()Lf/h/c/y/m/n;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public o()Lf/h/c/y/m/h;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/o;->gaugeMetric_:Lf/h/c/y/m/h;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/c/y/m/h;->H()Lf/h/c/y/m/h;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/o;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/o;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/o;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/o;->DEFAULT_INSTANCE:Lf/h/c/y/m/o;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/o;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/o;->DEFAULT_INSTANCE:Lf/h/c/y/m/o;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/o$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/o$b;-><init>(Lf/h/c/y/m/o$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/o;

    invoke-direct {p1}, Lf/h/c/y/m/o;-><init>()V

    return-object p1

    :pswitch_4
    const/4 p1, 0x6

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "applicationInfo_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "traceMetric_"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-string p3, "networkRequestMetric_"

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string p3, "gaugeMetric_"

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-string p3, "transportInfo_"

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001\u0003\t\u0002\u0004\t\u0003\u0005\t\u0004"

    sget-object p3, Lf/h/c/y/m/o;->DEFAULT_INSTANCE:Lf/h/c/y/m/o;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
