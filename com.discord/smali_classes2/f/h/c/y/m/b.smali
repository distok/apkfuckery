.class public final Lf/h/c/y/m/b;
.super Lf/h/e/r;
.source "AndroidMemoryReading.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/b$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/b;",
        "Lf/h/c/y/m/b$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final CLIENT_TIME_US_FIELD_NUMBER:I = 0x1

.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/b;

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/b;",
            ">;"
        }
    .end annotation
.end field

.field public static final USED_APP_JAVA_HEAP_MEMORY_KB_FIELD_NUMBER:I = 0x2


# instance fields
.field private bitField0_:I

.field private clientTimeUs_:J

.field private usedAppJavaHeapMemoryKb_:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/b;

    invoke-direct {v0}, Lf/h/c/y/m/b;-><init>()V

    sput-object v0, Lf/h/c/y/m/b;->DEFAULT_INSTANCE:Lf/h/c/y/m/b;

    const-class v1, Lf/h/c/y/m/b;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/b;->DEFAULT_INSTANCE:Lf/h/c/y/m/b;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/b;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/b;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/c/y/m/b;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/b;->clientTimeUs_:J

    return-void
.end method

.method public static C(Lf/h/c/y/m/b;I)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/b;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lf/h/c/y/m/b;->bitField0_:I

    iput p1, p0, Lf/h/c/y/m/b;->usedAppJavaHeapMemoryKb_:I

    return-void
.end method

.method public static D()Lf/h/c/y/m/b$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/b;->DEFAULT_INSTANCE:Lf/h/c/y/m/b;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/b$b;

    return-object v0
.end method


# virtual methods
.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/b;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/b;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/b;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/b;->DEFAULT_INSTANCE:Lf/h/c/y/m/b;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/b;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/b;->DEFAULT_INSTANCE:Lf/h/c/y/m/b;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/b$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/b$b;-><init>(Lf/h/c/y/m/b$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/b;

    invoke-direct {p1}, Lf/h/c/y/m/b;-><init>()V

    return-object p1

    :pswitch_4
    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "clientTimeUs_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "usedAppJavaHeapMemoryKb_"

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0004\u0001"

    sget-object p3, Lf/h/c/y/m/b;->DEFAULT_INSTANCE:Lf/h/c/y/m/b;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
