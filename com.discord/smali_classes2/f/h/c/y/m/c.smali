.class public final Lf/h/c/y/m/c;
.super Lf/h/e/r;
.source "ApplicationInfo.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/c$b;,
        Lf/h/c/y/m/c$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/c;",
        "Lf/h/c/y/m/c$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final ANDROID_APP_INFO_FIELD_NUMBER:I = 0x3

.field public static final APPLICATION_PROCESS_STATE_FIELD_NUMBER:I = 0x5

.field public static final APP_INSTANCE_ID_FIELD_NUMBER:I = 0x2

.field public static final CUSTOM_ATTRIBUTES_FIELD_NUMBER:I = 0x6

.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/c;

.field public static final GOOGLE_APP_ID_FIELD_NUMBER:I = 0x1

.field public static final IOS_APP_INFO_FIELD_NUMBER:I = 0x4

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final WEB_APP_INFO_FIELD_NUMBER:I = 0x7


# instance fields
.field private androidAppInfo_:Lf/h/c/y/m/a;

.field private appInstanceId_:Ljava/lang/String;

.field private applicationProcessState_:I

.field private bitField0_:I

.field private customAttributes_:Lf/h/e/e0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/e0<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private googleAppId_:Ljava/lang/String;

.field private iosAppInfo_:Lf/h/c/y/m/i;

.field private webAppInfo_:Lf/h/c/y/m/x;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/c;

    invoke-direct {v0}, Lf/h/c/y/m/c;-><init>()V

    sput-object v0, Lf/h/c/y/m/c;->DEFAULT_INSTANCE:Lf/h/c/y/m/c;

    const-class v1, Lf/h/c/y/m/c;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    sget-object v0, Lf/h/e/e0;->d:Lf/h/e/e0;

    iput-object v0, p0, Lf/h/c/y/m/c;->customAttributes_:Lf/h/e/e0;

    const-string v0, ""

    iput-object v0, p0, Lf/h/c/y/m/c;->googleAppId_:Ljava/lang/String;

    iput-object v0, p0, Lf/h/c/y/m/c;->appInstanceId_:Ljava/lang/String;

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/c;
    .locals 1

    sget-object v0, Lf/h/c/y/m/c;->DEFAULT_INSTANCE:Lf/h/c/y/m/c;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/c;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/c;->googleAppId_:Ljava/lang/String;

    return-void
.end method

.method public static C(Lf/h/c/y/m/c;Lf/h/c/y/m/d;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lf/h/c/y/m/d;->getNumber()I

    move-result p1

    iput p1, p0, Lf/h/c/y/m/c;->applicationProcessState_:I

    iget p1, p0, Lf/h/c/y/m/c;->bitField0_:I

    or-int/lit8 p1, p1, 0x20

    iput p1, p0, Lf/h/c/y/m/c;->bitField0_:I

    return-void
.end method

.method public static D(Lf/h/c/y/m/c;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/c;->customAttributes_:Lf/h/e/e0;

    invoke-virtual {v0}, Lf/h/e/e0;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/c;->customAttributes_:Lf/h/e/e0;

    invoke-virtual {v0}, Lf/h/e/e0;->h()Lf/h/e/e0;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/c;->customAttributes_:Lf/h/e/e0;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/c;->customAttributes_:Lf/h/e/e0;

    return-object p0
.end method

.method public static E(Lf/h/c/y/m/c;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/c;->appInstanceId_:Ljava/lang/String;

    return-void
.end method

.method public static F(Lf/h/c/y/m/c;Lf/h/c/y/m/a;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/c/y/m/c;->androidAppInfo_:Lf/h/c/y/m/a;

    iget p1, p0, Lf/h/c/y/m/c;->bitField0_:I

    or-int/lit8 p1, p1, 0x4

    iput p1, p0, Lf/h/c/y/m/c;->bitField0_:I

    return-void
.end method

.method public static I()Lf/h/c/y/m/c;
    .locals 1

    sget-object v0, Lf/h/c/y/m/c;->DEFAULT_INSTANCE:Lf/h/c/y/m/c;

    return-object v0
.end method

.method public static N()Lf/h/c/y/m/c$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/c;->DEFAULT_INSTANCE:Lf/h/c/y/m/c;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/c$b;

    return-object v0
.end method


# virtual methods
.method public G()Lf/h/c/y/m/a;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/c;->androidAppInfo_:Lf/h/c/y/m/a;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/c/y/m/a;->E()Lf/h/c/y/m/a;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/c;->appInstanceId_:Ljava/lang/String;

    return-object v0
.end method

.method public J()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public K()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public L()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public M()Z
    .locals 2

    iget v0, p0, Lf/h/c/y/m/c;->bitField0_:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/c;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/c;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/c;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/c;->DEFAULT_INSTANCE:Lf/h/c/y/m/c;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/c;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/c;->DEFAULT_INSTANCE:Lf/h/c/y/m/c;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/c$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/c$b;-><init>(Lf/h/c/y/m/c$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/c;

    invoke-direct {p1}, Lf/h/c/y/m/c;-><init>()V

    return-object p1

    :pswitch_4
    const/16 p1, 0xa

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "googleAppId_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "appInstanceId_"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-string p3, "androidAppInfo_"

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string p3, "iosAppInfo_"

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-string p3, "applicationProcessState_"

    aput-object p3, p1, p2

    const/4 p2, 0x6

    sget-object p3, Lf/h/c/y/m/d$a;->a:Lf/h/e/t$b;

    aput-object p3, p1, p2

    const/4 p2, 0x7

    const-string p3, "customAttributes_"

    aput-object p3, p1, p2

    const/16 p2, 0x8

    sget-object p3, Lf/h/c/y/m/c$c;->a:Lf/h/e/d0;

    aput-object p3, p1, p2

    const/16 p2, 0x9

    const-string p3, "webAppInfo_"

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0001\u0000\u0000\u0001\u0008\u0000\u0002\u0008\u0001\u0003\t\u0002\u0004\t\u0003\u0005\u000c\u0005\u00062\u0007\t\u0004"

    sget-object p3, Lf/h/c/y/m/c;->DEFAULT_INSTANCE:Lf/h/c/y/m/c;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
