.class public final Lf/h/c/y/m/h;
.super Lf/h/e/r;
.source "GaugeMetric.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/h$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/h;",
        "Lf/h/c/y/m/h$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final ANDROID_MEMORY_READINGS_FIELD_NUMBER:I = 0x4

.field public static final CPU_METRIC_READINGS_FIELD_NUMBER:I = 0x2

.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/h;

.field public static final GAUGE_METADATA_FIELD_NUMBER:I = 0x3

.field public static final IOS_MEMORY_READINGS_FIELD_NUMBER:I = 0x5

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/h;",
            ">;"
        }
    .end annotation
.end field

.field public static final SESSION_ID_FIELD_NUMBER:I = 0x1


# instance fields
.field private androidMemoryReadings_:Lf/h/e/t$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/t$d<",
            "Lf/h/c/y/m/b;",
            ">;"
        }
    .end annotation
.end field

.field private bitField0_:I

.field private cpuMetricReadings_:Lf/h/e/t$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/t$d<",
            "Lf/h/c/y/m/e;",
            ">;"
        }
    .end annotation
.end field

.field private gaugeMetadata_:Lf/h/c/y/m/g;

.field private iosMemoryReadings_:Lf/h/e/t$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/t$d<",
            "Lf/h/c/y/m/j;",
            ">;"
        }
    .end annotation
.end field

.field private sessionId_:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/h;

    invoke-direct {v0}, Lf/h/c/y/m/h;-><init>()V

    sput-object v0, Lf/h/c/y/m/h;->DEFAULT_INSTANCE:Lf/h/c/y/m/h;

    const-class v1, Lf/h/c/y/m/h;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lf/h/c/y/m/h;->sessionId_:Ljava/lang/String;

    sget-object v0, Lf/h/e/v0;->g:Lf/h/e/v0;

    iput-object v0, p0, Lf/h/c/y/m/h;->cpuMetricReadings_:Lf/h/e/t$d;

    iput-object v0, p0, Lf/h/c/y/m/h;->androidMemoryReadings_:Lf/h/e/t$d;

    iput-object v0, p0, Lf/h/c/y/m/h;->iosMemoryReadings_:Lf/h/e/t$d;

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/h;
    .locals 1

    sget-object v0, Lf/h/c/y/m/h;->DEFAULT_INSTANCE:Lf/h/c/y/m/h;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/h;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/h;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/c/y/m/h;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/h;->sessionId_:Ljava/lang/String;

    return-void
.end method

.method public static C(Lf/h/c/y/m/h;Lf/h/c/y/m/b;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lf/h/c/y/m/h;->androidMemoryReadings_:Lf/h/e/t$d;

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/h;->androidMemoryReadings_:Lf/h/e/t$d;

    invoke-static {v0}, Lf/h/e/r;->x(Lf/h/e/t$d;)Lf/h/e/t$d;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/h;->androidMemoryReadings_:Lf/h/e/t$d;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/h;->androidMemoryReadings_:Lf/h/e/t$d;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static D(Lf/h/c/y/m/h;Lf/h/c/y/m/g;)V
    .locals 0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lf/h/c/y/m/h;->gaugeMetadata_:Lf/h/c/y/m/g;

    iget p1, p0, Lf/h/c/y/m/h;->bitField0_:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lf/h/c/y/m/h;->bitField0_:I

    return-void
.end method

.method public static E(Lf/h/c/y/m/h;Lf/h/c/y/m/e;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lf/h/c/y/m/h;->cpuMetricReadings_:Lf/h/e/t$d;

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/h;->cpuMetricReadings_:Lf/h/e/t$d;

    invoke-static {v0}, Lf/h/e/r;->x(Lf/h/e/t$d;)Lf/h/e/t$d;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/h;->cpuMetricReadings_:Lf/h/e/t$d;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/h;->cpuMetricReadings_:Lf/h/e/t$d;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static H()Lf/h/c/y/m/h;
    .locals 1

    sget-object v0, Lf/h/c/y/m/h;->DEFAULT_INSTANCE:Lf/h/c/y/m/h;

    return-object v0
.end method

.method public static L()Lf/h/c/y/m/h$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/h;->DEFAULT_INSTANCE:Lf/h/c/y/m/h;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/h$b;

    return-object v0
.end method


# virtual methods
.method public F()I
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/h;->androidMemoryReadings_:Lf/h/e/t$d;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public G()I
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/h;->cpuMetricReadings_:Lf/h/e/t$d;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public I()Lf/h/c/y/m/g;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/h;->gaugeMetadata_:Lf/h/c/y/m/g;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/c/y/m/g;->F()Lf/h/c/y/m/g;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public J()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/h;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public K()Z
    .locals 2

    iget v0, p0, Lf/h/c/y/m/h;->bitField0_:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/h;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/h;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/h;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/h;->DEFAULT_INSTANCE:Lf/h/c/y/m/h;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/h;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/h;->DEFAULT_INSTANCE:Lf/h/c/y/m/h;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/h$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/h$b;-><init>(Lf/h/c/y/m/h$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/h;

    invoke-direct {p1}, Lf/h/c/y/m/h;-><init>()V

    return-object p1

    :pswitch_4
    const/16 p1, 0x9

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "sessionId_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "cpuMetricReadings_"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-class p3, Lf/h/c/y/m/e;

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string p3, "gaugeMetadata_"

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-string p3, "androidMemoryReadings_"

    aput-object p3, p1, p2

    const/4 p2, 0x6

    const-class p3, Lf/h/c/y/m/b;

    aput-object p3, p1, p2

    const/4 p2, 0x7

    const-string p3, "iosMemoryReadings_"

    aput-object p3, p1, p2

    const/16 p2, 0x8

    const-class p3, Lf/h/c/y/m/j;

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0003\u0000\u0001\u0008\u0000\u0002\u001b\u0003\t\u0001\u0004\u001b\u0005\u001b"

    sget-object p3, Lf/h/c/y/m/h;->DEFAULT_INSTANCE:Lf/h/c/y/m/h;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
