.class public final enum Lf/h/c/y/m/d;
.super Ljava/lang/Enum;
.source "ApplicationProcessState.java"

# interfaces
.implements Lf/h/e/t$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/y/m/d;",
        ">;",
        "Lf/h/e/t$a;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/y/m/d;

.field public static final enum e:Lf/h/c/y/m/d;

.field public static final enum f:Lf/h/c/y/m/d;

.field public static final enum g:Lf/h/c/y/m/d;

.field public static final synthetic h:[Lf/h/c/y/m/d;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/h/c/y/m/d;

    const-string v1, "APPLICATION_PROCESS_STATE_UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/h/c/y/m/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/h/c/y/m/d;->d:Lf/h/c/y/m/d;

    new-instance v1, Lf/h/c/y/m/d;

    const-string v3, "FOREGROUND"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/h/c/y/m/d;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/h/c/y/m/d;->e:Lf/h/c/y/m/d;

    new-instance v3, Lf/h/c/y/m/d;

    const-string v5, "BACKGROUND"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lf/h/c/y/m/d;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lf/h/c/y/m/d;->f:Lf/h/c/y/m/d;

    new-instance v5, Lf/h/c/y/m/d;

    const-string v7, "FOREGROUND_BACKGROUND"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v8}, Lf/h/c/y/m/d;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lf/h/c/y/m/d;->g:Lf/h/c/y/m/d;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/h/c/y/m/d;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/h/c/y/m/d;->h:[Lf/h/c/y/m/d;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/h/c/y/m/d;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/y/m/d;
    .locals 1

    const-class v0, Lf/h/c/y/m/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/y/m/d;

    return-object p0
.end method

.method public static values()[Lf/h/c/y/m/d;
    .locals 1

    sget-object v0, Lf/h/c/y/m/d;->h:[Lf/h/c/y/m/d;

    invoke-virtual {v0}, [Lf/h/c/y/m/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/y/m/d;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lf/h/c/y/m/d;->value:I

    return v0
.end method
