.class public final Lf/h/c/y/m/t;
.super Lf/h/e/r;
.source "TraceMetric.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/t$b;,
        Lf/h/c/y/m/t$d;,
        Lf/h/c/y/m/t$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/t;",
        "Lf/h/c/y/m/t$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final CLIENT_START_TIME_US_FIELD_NUMBER:I = 0x4

.field public static final COUNTERS_FIELD_NUMBER:I = 0x6

.field public static final CUSTOM_ATTRIBUTES_FIELD_NUMBER:I = 0x8

.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/t;

.field public static final DURATION_US_FIELD_NUMBER:I = 0x5

.field public static final IS_AUTO_FIELD_NUMBER:I = 0x2

.field public static final NAME_FIELD_NUMBER:I = 0x1

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/t;",
            ">;"
        }
    .end annotation
.end field

.field public static final PERF_SESSIONS_FIELD_NUMBER:I = 0x9

.field public static final SUBTRACES_FIELD_NUMBER:I = 0x7


# instance fields
.field private bitField0_:I

.field private clientStartTimeUs_:J

.field private counters_:Lf/h/e/e0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/e0<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private customAttributes_:Lf/h/e/e0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/e0<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private durationUs_:J

.field private isAuto_:Z

.field private name_:Ljava/lang/String;

.field private perfSessions_:Lf/h/e/t$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/t$d<",
            "Lf/h/c/y/m/q;",
            ">;"
        }
    .end annotation
.end field

.field private subtraces_:Lf/h/e/t$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/t$d<",
            "Lf/h/c/y/m/t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/t;

    invoke-direct {v0}, Lf/h/c/y/m/t;-><init>()V

    sput-object v0, Lf/h/c/y/m/t;->DEFAULT_INSTANCE:Lf/h/c/y/m/t;

    const-class v1, Lf/h/c/y/m/t;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    sget-object v0, Lf/h/e/e0;->d:Lf/h/e/e0;

    iput-object v0, p0, Lf/h/c/y/m/t;->counters_:Lf/h/e/e0;

    iput-object v0, p0, Lf/h/c/y/m/t;->customAttributes_:Lf/h/e/e0;

    const-string v0, ""

    iput-object v0, p0, Lf/h/c/y/m/t;->name_:Ljava/lang/String;

    sget-object v0, Lf/h/e/v0;->g:Lf/h/e/v0;

    iput-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    iput-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/t;
    .locals 1

    sget-object v0, Lf/h/c/y/m/t;->DEFAULT_INSTANCE:Lf/h/c/y/m/t;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/t;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/t;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/c/y/m/t;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/t;->name_:Ljava/lang/String;

    return-void
.end method

.method public static C(Lf/h/c/y/m/t;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/t;->counters_:Lf/h/e/e0;

    invoke-virtual {v0}, Lf/h/e/e0;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/t;->counters_:Lf/h/e/e0;

    invoke-virtual {v0}, Lf/h/e/e0;->h()Lf/h/e/e0;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/t;->counters_:Lf/h/e/e0;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/t;->counters_:Lf/h/e/e0;

    return-object p0
.end method

.method public static D(Lf/h/c/y/m/t;Lf/h/c/y/m/t;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    invoke-static {v0}, Lf/h/e/r;->x(Lf/h/e/t$d;)Lf/h/e/t$d;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static E(Lf/h/c/y/m/t;Ljava/lang/Iterable;)V
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    invoke-static {v0}, Lf/h/e/r;->x(Lf/h/e/t$d;)Lf/h/e/t$d;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    invoke-static {p1, p0}, Lf/h/e/a;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method public static F(Lf/h/c/y/m/t;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/t;->customAttributes_:Lf/h/e/e0;

    invoke-virtual {v0}, Lf/h/e/e0;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/t;->customAttributes_:Lf/h/e/e0;

    invoke-virtual {v0}, Lf/h/e/e0;->h()Lf/h/e/e0;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/t;->customAttributes_:Lf/h/e/e0;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/t;->customAttributes_:Lf/h/e/e0;

    return-object p0
.end method

.method public static G(Lf/h/c/y/m/t;Lf/h/c/y/m/q;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    invoke-static {v0}, Lf/h/e/r;->x(Lf/h/e/t$d;)Lf/h/e/t$d;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static H(Lf/h/c/y/m/t;Ljava/lang/Iterable;)V
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    invoke-static {v0}, Lf/h/e/r;->x(Lf/h/e/t$d;)Lf/h/e/t$d;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    :cond_0
    iget-object p0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    invoke-static {p1, p0}, Lf/h/e/a;->a(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method public static I(Lf/h/c/y/m/t;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/t;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lf/h/c/y/m/t;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/t;->clientStartTimeUs_:J

    return-void
.end method

.method public static J(Lf/h/c/y/m/t;J)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/t;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lf/h/c/y/m/t;->bitField0_:I

    iput-wide p1, p0, Lf/h/c/y/m/t;->durationUs_:J

    return-void
.end method

.method public static N()Lf/h/c/y/m/t;
    .locals 1

    sget-object v0, Lf/h/c/y/m/t;->DEFAULT_INSTANCE:Lf/h/c/y/m/t;

    return-object v0
.end method

.method public static T()Lf/h/c/y/m/t$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/t;->DEFAULT_INSTANCE:Lf/h/c/y/m/t;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/t$b;

    return-object v0
.end method


# virtual methods
.method public K()I
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/t;->counters_:Lf/h/e/e0;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public L()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/m/t;->counters_:Lf/h/e/e0;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public M()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/m/t;->customAttributes_:Lf/h/e/e0;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public O()J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/m/t;->durationUs_:J

    return-wide v0
.end method

.method public P()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/m/t;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public Q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/y/m/q;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/m/t;->perfSessions_:Lf/h/e/t$d;

    return-object v0
.end method

.method public R()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/y/m/t;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/m/t;->subtraces_:Lf/h/e/t$d;

    return-object v0
.end method

.method public S()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/t;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    const-class p2, Lf/h/c/y/m/t;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p3, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/t;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/t;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/t;->DEFAULT_INSTANCE:Lf/h/c/y/m/t;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/t;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/t;->DEFAULT_INSTANCE:Lf/h/c/y/m/t;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/t$b;

    invoke-direct {p1, v0}, Lf/h/c/y/m/t$b;-><init>(Lf/h/c/y/m/t$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/t;

    invoke-direct {p1}, Lf/h/c/y/m/t;-><init>()V

    return-object p1

    :pswitch_4
    const/16 p1, 0xd

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v1, "bitField0_"

    aput-object v1, p1, v0

    const-string v0, "name_"

    aput-object v0, p1, p3

    const/4 p3, 0x2

    const-string v0, "isAuto_"

    aput-object v0, p1, p3

    const/4 p3, 0x3

    const-string v0, "clientStartTimeUs_"

    aput-object v0, p1, p3

    const/4 p3, 0x4

    const-string v0, "durationUs_"

    aput-object v0, p1, p3

    const/4 p3, 0x5

    const-string v0, "counters_"

    aput-object v0, p1, p3

    const/4 p3, 0x6

    sget-object v0, Lf/h/c/y/m/t$c;->a:Lf/h/e/d0;

    aput-object v0, p1, p3

    const/4 p3, 0x7

    const-string v0, "subtraces_"

    aput-object v0, p1, p3

    const/16 p3, 0x8

    aput-object p2, p1, p3

    const/16 p2, 0x9

    const-string p3, "customAttributes_"

    aput-object p3, p1, p2

    const/16 p2, 0xa

    sget-object p3, Lf/h/c/y/m/t$d;->a:Lf/h/e/d0;

    aput-object p3, p1, p2

    const/16 p2, 0xb

    const-string p3, "perfSessions_"

    aput-object p3, p1, p2

    const/16 p2, 0xc

    const-class p3, Lf/h/c/y/m/q;

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0008\u0000\u0001\u0001\t\u0008\u0002\u0002\u0000\u0001\u0008\u0000\u0002\u0007\u0001\u0004\u0002\u0002\u0005\u0002\u0003\u00062\u0007\u001b\u00082\t\u001b"

    sget-object p3, Lf/h/c/y/m/t;->DEFAULT_INSTANCE:Lf/h/c/y/m/t;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    :pswitch_5
    return-object v0

    :pswitch_6
    invoke-static {p3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
