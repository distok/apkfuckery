.class public final enum Lf/h/c/y/m/s;
.super Ljava/lang/Enum;
.source "SessionVerbosity.java"

# interfaces
.implements Lf/h/e/t$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/s$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/y/m/s;",
        ">;",
        "Lf/h/e/t$a;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/y/m/s;

.field public static final enum e:Lf/h/c/y/m/s;

.field public static final synthetic f:[Lf/h/c/y/m/s;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/h/c/y/m/s;

    const-string v1, "SESSION_VERBOSITY_NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/h/c/y/m/s;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/h/c/y/m/s;->d:Lf/h/c/y/m/s;

    new-instance v1, Lf/h/c/y/m/s;

    const-string v3, "GAUGES_AND_SYSTEM_EVENTS"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/h/c/y/m/s;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/h/c/y/m/s;->e:Lf/h/c/y/m/s;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/h/c/y/m/s;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/h/c/y/m/s;->f:[Lf/h/c/y/m/s;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/h/c/y/m/s;->value:I

    return-void
.end method

.method public static f(I)Lf/h/c/y/m/s;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    sget-object p0, Lf/h/c/y/m/s;->e:Lf/h/c/y/m/s;

    return-object p0

    :cond_1
    sget-object p0, Lf/h/c/y/m/s;->d:Lf/h/c/y/m/s;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/y/m/s;
    .locals 1

    const-class v0, Lf/h/c/y/m/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/y/m/s;

    return-object p0
.end method

.method public static values()[Lf/h/c/y/m/s;
    .locals 1

    sget-object v0, Lf/h/c/y/m/s;->f:[Lf/h/c/y/m/s;

    invoke-virtual {v0}, [Lf/h/c/y/m/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/y/m/s;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lf/h/c/y/m/s;->value:I

    return v0
.end method
