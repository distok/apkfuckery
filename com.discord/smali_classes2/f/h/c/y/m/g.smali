.class public final Lf/h/c/y/m/g;
.super Lf/h/e/r;
.source "GaugeMetadata.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/g$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/g;",
        "Lf/h/c/y/m/g$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final CPU_CLOCK_RATE_KHZ_FIELD_NUMBER:I = 0x2

.field public static final CPU_PROCESSOR_COUNT_FIELD_NUMBER:I = 0x6

.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/g;

.field public static final DEVICE_RAM_SIZE_KB_FIELD_NUMBER:I = 0x3

.field public static final MAX_APP_JAVA_HEAP_MEMORY_KB_FIELD_NUMBER:I = 0x4

.field public static final MAX_ENCOURAGED_APP_JAVA_HEAP_MEMORY_KB_FIELD_NUMBER:I = 0x5

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/g;",
            ">;"
        }
    .end annotation
.end field

.field public static final PROCESS_NAME_FIELD_NUMBER:I = 0x1


# instance fields
.field private bitField0_:I

.field private cpuClockRateKhz_:I

.field private cpuProcessorCount_:I

.field private deviceRamSizeKb_:I

.field private maxAppJavaHeapMemoryKb_:I

.field private maxEncouragedAppJavaHeapMemoryKb_:I

.field private processName_:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/g;

    invoke-direct {v0}, Lf/h/c/y/m/g;-><init>()V

    sput-object v0, Lf/h/c/y/m/g;->DEFAULT_INSTANCE:Lf/h/c/y/m/g;

    const-class v1, Lf/h/c/y/m/g;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lf/h/c/y/m/g;->processName_:Ljava/lang/String;

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/g;
    .locals 1

    sget-object v0, Lf/h/c/y/m/g;->DEFAULT_INSTANCE:Lf/h/c/y/m/g;

    return-object v0
.end method

.method public static B(Lf/h/c/y/m/g;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    iput-object p1, p0, Lf/h/c/y/m/g;->processName_:Ljava/lang/String;

    return-void
.end method

.method public static C(Lf/h/c/y/m/g;I)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    iput p1, p0, Lf/h/c/y/m/g;->maxAppJavaHeapMemoryKb_:I

    return-void
.end method

.method public static D(Lf/h/c/y/m/g;I)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    iput p1, p0, Lf/h/c/y/m/g;->maxEncouragedAppJavaHeapMemoryKb_:I

    return-void
.end method

.method public static E(Lf/h/c/y/m/g;I)V
    .locals 1

    iget v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    iput p1, p0, Lf/h/c/y/m/g;->deviceRamSizeKb_:I

    return-void
.end method

.method public static F()Lf/h/c/y/m/g;
    .locals 1

    sget-object v0, Lf/h/c/y/m/g;->DEFAULT_INSTANCE:Lf/h/c/y/m/g;

    return-object v0
.end method

.method public static H()Lf/h/c/y/m/g$b;
    .locals 1

    sget-object v0, Lf/h/c/y/m/g;->DEFAULT_INSTANCE:Lf/h/c/y/m/g;

    invoke-virtual {v0}, Lf/h/e/r;->s()Lf/h/e/r$a;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/g$b;

    return-object v0
.end method


# virtual methods
.method public G()Z
    .locals 1

    iget v0, p0, Lf/h/c/y/m/g;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/g;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/g;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/g;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/g;->DEFAULT_INSTANCE:Lf/h/c/y/m/g;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/g;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/g;->DEFAULT_INSTANCE:Lf/h/c/y/m/g;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/g$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/g$b;-><init>(Lf/h/c/y/m/g$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/g;

    invoke-direct {p1}, Lf/h/c/y/m/g;-><init>()V

    return-object p1

    :pswitch_4
    const/4 p1, 0x7

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "processName_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "cpuClockRateKhz_"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-string p3, "deviceRamSizeKb_"

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string p3, "maxAppJavaHeapMemoryKb_"

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-string p3, "maxEncouragedAppJavaHeapMemoryKb_"

    aput-object p3, p1, p2

    const/4 p2, 0x6

    const-string p3, "cpuProcessorCount_"

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0004\u0001\u0003\u0004\u0003\u0004\u0004\u0004\u0005\u0004\u0005\u0006\u0004\u0002"

    sget-object p3, Lf/h/c/y/m/g;->DEFAULT_INSTANCE:Lf/h/c/y/m/g;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
