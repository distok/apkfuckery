.class public final enum Lf/h/c/y/m/n$e;
.super Ljava/lang/Enum;
.source "NetworkRequestMetric.java"

# interfaces
.implements Lf/h/e/t$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/y/m/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/n$e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/y/m/n$e;",
        ">;",
        "Lf/h/e/t$a;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/y/m/n$e;

.field public static final enum e:Lf/h/c/y/m/n$e;

.field public static final synthetic f:[Lf/h/c/y/m/n$e;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/h/c/y/m/n$e;

    const-string v1, "NETWORK_CLIENT_ERROR_REASON_UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/h/c/y/m/n$e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/h/c/y/m/n$e;->d:Lf/h/c/y/m/n$e;

    new-instance v1, Lf/h/c/y/m/n$e;

    const-string v3, "GENERIC_CLIENT_ERROR"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/h/c/y/m/n$e;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/h/c/y/m/n$e;->e:Lf/h/c/y/m/n$e;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/h/c/y/m/n$e;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/h/c/y/m/n$e;->f:[Lf/h/c/y/m/n$e;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/h/c/y/m/n$e;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/y/m/n$e;
    .locals 1

    const-class v0, Lf/h/c/y/m/n$e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/y/m/n$e;

    return-object p0
.end method

.method public static values()[Lf/h/c/y/m/n$e;
    .locals 1

    sget-object v0, Lf/h/c/y/m/n$e;->f:[Lf/h/c/y/m/n$e;

    invoke-virtual {v0}, [Lf/h/c/y/m/n$e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/y/m/n$e;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lf/h/c/y/m/n$e;->value:I

    return v0
.end method
