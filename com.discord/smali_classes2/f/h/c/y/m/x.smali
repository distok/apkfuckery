.class public final Lf/h/c/y/m/x;
.super Lf/h/e/r;
.source "WebApplicationInfo.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/m/x$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/r<",
        "Lf/h/c/y/m/x;",
        "Lf/h/c/y/m/x$b;",
        ">;",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTANCE:Lf/h/c/y/m/x;

.field public static final EFFECTIVE_CONNECTION_TYPE_FIELD_NUMBER:I = 0x5

.field public static final PAGE_URL_FIELD_NUMBER:I = 0x2

.field private static volatile PARSER:Lf/h/e/r0; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/r0<",
            "Lf/h/c/y/m/x;",
            ">;"
        }
    .end annotation
.end field

.field public static final SDK_VERSION_FIELD_NUMBER:I = 0x1

.field public static final SERVICE_WORKER_STATUS_FIELD_NUMBER:I = 0x3

.field public static final VISIBILITY_STATE_FIELD_NUMBER:I = 0x4


# instance fields
.field private bitField0_:I

.field private effectiveConnectionType_:I

.field private pageUrl_:Ljava/lang/String;

.field private sdkVersion_:Ljava/lang/String;

.field private serviceWorkerStatus_:I

.field private visibilityState_:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/c/y/m/x;

    invoke-direct {v0}, Lf/h/c/y/m/x;-><init>()V

    sput-object v0, Lf/h/c/y/m/x;->DEFAULT_INSTANCE:Lf/h/c/y/m/x;

    const-class v1, Lf/h/c/y/m/x;

    invoke-static {v1, v0}, Lf/h/e/r;->z(Ljava/lang/Class;Lf/h/e/r;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lf/h/c/y/m/x;->sdkVersion_:Ljava/lang/String;

    iput-object v0, p0, Lf/h/c/y/m/x;->pageUrl_:Ljava/lang/String;

    return-void
.end method

.method public static synthetic A()Lf/h/c/y/m/x;
    .locals 1

    sget-object v0, Lf/h/c/y/m/x;->DEFAULT_INSTANCE:Lf/h/c/y/m/x;

    return-object v0
.end method


# virtual methods
.method public final t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lf/h/c/y/m/x;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_1

    const-class p2, Lf/h/c/y/m/x;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/c/y/m/x;->PARSER:Lf/h/e/r0;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/e/r$b;

    sget-object p3, Lf/h/c/y/m/x;->DEFAULT_INSTANCE:Lf/h/c/y/m/x;

    invoke-direct {p1, p3}, Lf/h/e/r$b;-><init>(Lf/h/e/r;)V

    sput-object p1, Lf/h/c/y/m/x;->PARSER:Lf/h/e/r0;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_1
    sget-object p1, Lf/h/c/y/m/x;->DEFAULT_INSTANCE:Lf/h/c/y/m/x;

    return-object p1

    :pswitch_2
    new-instance p1, Lf/h/c/y/m/x$b;

    invoke-direct {p1, p3}, Lf/h/c/y/m/x$b;-><init>(Lf/h/c/y/m/x$a;)V

    return-object p1

    :pswitch_3
    new-instance p1, Lf/h/c/y/m/x;

    invoke-direct {p1}, Lf/h/c/y/m/x;-><init>()V

    return-object p1

    :pswitch_4
    const/16 p1, 0x9

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string v0, "bitField0_"

    aput-object v0, p1, p3

    const-string p3, "sdkVersion_"

    aput-object p3, p1, p2

    const/4 p2, 0x2

    const-string p3, "pageUrl_"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-string p3, "serviceWorkerStatus_"

    aput-object p3, p1, p2

    const/4 p2, 0x4

    sget-object p3, Lf/h/c/y/m/r;->a:Lf/h/e/t$b;

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-string p3, "visibilityState_"

    aput-object p3, p1, p2

    const/4 p2, 0x6

    sget-object p3, Lf/h/c/y/m/w;->a:Lf/h/e/t$b;

    aput-object p3, p1, p2

    const/4 p2, 0x7

    const-string p3, "effectiveConnectionType_"

    aput-object p3, p1, p2

    const/16 p2, 0x8

    sget-object p3, Lf/h/c/y/m/f;->a:Lf/h/e/t$b;

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0008\u0001\u0003\u000c\u0002\u0004\u000c\u0003\u0005\u000c\u0004"

    sget-object p3, Lf/h/c/y/m/x;->DEFAULT_INSTANCE:Lf/h/c/y/m/x;

    new-instance v0, Lf/h/e/w0;

    invoke-direct {v0, p3, p2, p1}, Lf/h/e/w0;-><init>(Lf/h/e/k0;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    return-object p3

    :pswitch_6
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
