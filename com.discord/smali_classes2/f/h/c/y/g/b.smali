.class public abstract Lf/h/c/y/g/b;
.super Ljava/lang/Object;
.source "AppStateUpdateHandler.java"

# interfaces
.implements Lf/h/c/y/g/a$a;


# instance fields
.field private mAppStateMonitor:Lf/h/c/y/g/a;

.field private mIsRegisteredForAppState:Z

.field private mState:Lf/h/c/y/m/d;

.field private mWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/h/c/y/g/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/g/a;->a()Lf/h/c/y/g/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lf/h/c/y/g/b;-><init>(Lf/h/c/y/g/a;)V

    return-void
.end method

.method public constructor <init>(Lf/h/c/y/g/a;)V
    .locals 1
    .param p1    # Lf/h/c/y/g/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/h/c/y/m/d;->d:Lf/h/c/y/m/d;

    iput-object v0, p0, Lf/h/c/y/g/b;->mState:Lf/h/c/y/m/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/c/y/g/b;->mIsRegisteredForAppState:Z

    iput-object p1, p0, Lf/h/c/y/g/b;->mAppStateMonitor:Lf/h/c/y/g/a;

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lf/h/c/y/g/b;->mWeakRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public getAppState()Lf/h/c/y/m/d;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/g/b;->mState:Lf/h/c/y/m/d;

    return-object v0
.end method

.method public incrementTsnsCount(I)V
    .locals 1

    iget-object v0, p0, Lf/h/c/y/g/b;->mAppStateMonitor:Lf/h/c/y/g/a;

    iget-object v0, v0, Lf/h/c/y/g/a;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    return-void
.end method

.method public onUpdateAppState(Lf/h/c/y/m/d;)V
    .locals 2

    iget-object v0, p0, Lf/h/c/y/g/b;->mState:Lf/h/c/y/m/d;

    sget-object v1, Lf/h/c/y/m/d;->d:Lf/h/c/y/m/d;

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Lf/h/c/y/g/b;->mState:Lf/h/c/y/m/d;

    goto :goto_0

    :cond_0
    if-eq v0, p1, :cond_1

    if-eq p1, v1, :cond_1

    sget-object p1, Lf/h/c/y/m/d;->g:Lf/h/c/y/m/d;

    iput-object p1, p0, Lf/h/c/y/g/b;->mState:Lf/h/c/y/m/d;

    :cond_1
    :goto_0
    return-void
.end method

.method public registerForAppState()V
    .locals 3

    iget-boolean v0, p0, Lf/h/c/y/g/b;->mIsRegisteredForAppState:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/c/y/g/b;->mAppStateMonitor:Lf/h/c/y/g/a;

    iget-object v1, v0, Lf/h/c/y/g/a;->n:Lf/h/c/y/m/d;

    iput-object v1, p0, Lf/h/c/y/g/b;->mState:Lf/h/c/y/m/d;

    iget-object v1, p0, Lf/h/c/y/g/b;->mWeakRef:Ljava/lang/ref/WeakReference;

    iget-object v2, v0, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v0, v0, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/c/y/g/b;->mIsRegisteredForAppState:Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public unregisterForAppState()V
    .locals 3

    iget-boolean v0, p0, Lf/h/c/y/g/b;->mIsRegisteredForAppState:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/c/y/g/b;->mAppStateMonitor:Lf/h/c/y/g/a;

    iget-object v1, p0, Lf/h/c/y/g/b;->mWeakRef:Ljava/lang/ref/WeakReference;

    iget-object v2, v0, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v0, v0, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/c/y/g/b;->mIsRegisteredForAppState:Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
