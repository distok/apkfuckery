.class public Lf/h/c/y/g/c;
.super Lf/h/c/y/g/j;
.source "FirebasePerfApplicationInfoValidator.java"


# static fields
.field public static final b:Lf/h/c/y/h/a;


# instance fields
.field public final a:Lf/h/c/y/m/c;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    return-void
.end method

.method public constructor <init>(Lf/h/c/y/m/c;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/y/g/j;-><init>()V

    iput-object p1, p0, Lf/h/c/y/g/c;->a:Lf/h/c/y/m/c;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 5

    iget-object v0, p0, Lf/h/c/y/g/c;->a:Lf/h/c/y/m/c;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    sget-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "ApplicationInfo is null"

    invoke-virtual {v0, v4, v3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lf/h/c/y/m/c;->M()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "GoogleAppId is null"

    invoke-virtual {v0, v4, v3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/h/c/y/g/c;->a:Lf/h/c/y/m/c;

    invoke-virtual {v0}, Lf/h/c/y/m/c;->K()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "AppInstanceId is null"

    invoke-virtual {v0, v4, v3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/h/c/y/g/c;->a:Lf/h/c/y/m/c;

    invoke-virtual {v0}, Lf/h/c/y/m/c;->L()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "ApplicationProcessState is null"

    invoke-virtual {v0, v4, v3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lf/h/c/y/g/c;->a:Lf/h/c/y/m/c;

    invoke-virtual {v0}, Lf/h/c/y/m/c;->J()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lf/h/c/y/g/c;->a:Lf/h/c/y/m/c;

    invoke-virtual {v0}, Lf/h/c/y/m/c;->G()Lf/h/c/y/m/a;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/a;->F()Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "AndroidAppInfo.packageName is null"

    invoke-virtual {v0, v4, v3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lf/h/c/y/g/c;->a:Lf/h/c/y/m/c;

    invoke-virtual {v0}, Lf/h/c/y/m/c;->G()Lf/h/c/y/m/a;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/a;->G()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "AndroidAppInfo.sdkVersion is null"

    invoke-virtual {v0, v4, v3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_6

    sget-object v0, Lf/h/c/y/g/c;->b:Lf/h/c/y/h/a;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "ApplicationInfo is invalid"

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_6
    return v1
.end method
