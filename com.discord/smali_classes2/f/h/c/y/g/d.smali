.class public final Lf/h/c/y/g/d;
.super Lf/h/c/y/g/j;
.source "FirebasePerfGaugeMetricValidator.java"


# instance fields
.field public final a:Lf/h/c/y/m/h;


# direct methods
.method public constructor <init>(Lf/h/c/y/m/h;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/y/g/j;-><init>()V

    iput-object p1, p0, Lf/h/c/y/g/d;->a:Lf/h/c/y/m/h;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lf/h/c/y/g/d;->a:Lf/h/c/y/m/h;

    invoke-virtual {v0}, Lf/h/c/y/m/h;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/c/y/g/d;->a:Lf/h/c/y/m/h;

    invoke-virtual {v0}, Lf/h/c/y/m/h;->G()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/g/d;->a:Lf/h/c/y/m/h;

    invoke-virtual {v0}, Lf/h/c/y/m/h;->F()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/g/d;->a:Lf/h/c/y/m/h;

    invoke-virtual {v0}, Lf/h/c/y/m/h;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/c/y/g/d;->a:Lf/h/c/y/m/h;

    invoke-virtual {v0}, Lf/h/c/y/m/h;->I()Lf/h/c/y/m/g;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/g;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
