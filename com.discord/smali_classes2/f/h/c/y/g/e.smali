.class public final Lf/h/c/y/g/e;
.super Lf/h/c/y/g/j;
.source "FirebasePerfNetworkValidator.java"


# static fields
.field public static final c:Lf/h/c/y/h/a;


# instance fields
.field public final a:Lf/h/c/y/m/n;

.field public final b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    return-void
.end method

.method public constructor <init>(Lf/h/c/y/m/n;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Lf/h/c/y/g/j;-><init>()V

    iput-object p2, p0, Lf/h/c/y/g/e;->b:Landroid/content/Context;

    iput-object p1, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 9

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->Y()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "URL is missing:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->Y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_1
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->Y()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    :try_start_0
    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_1
    sget-object v4, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    const-string v0, "getResultUrl throws exception %s"

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v5}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    move-object v0, v3

    :goto_3
    if-nez v0, :cond_3

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "URL cannot be parsed"

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_3
    iget-object v4, p0, Lf/h/c/y/g/e;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "firebase_performance_whitelisted_domains"

    const-string v7, "array"

    invoke-virtual {v5, v6, v7, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_4

    goto :goto_5

    :cond_4
    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v6

    new-array v7, v2, [Ljava/lang/Object;

    const-string v8, "Detected domain whitelist, only whitelisted domains will be measured."

    invoke-virtual {v6, v8, v7}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v6, Lf/h/a/f/f/n/g;->e:[Ljava/lang/String;

    if-nez v6, :cond_5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lf/h/a/f/f/n/g;->e:[Ljava/lang/String;

    :cond_5
    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    goto :goto_5

    :cond_6
    sget-object v5, Lf/h/a/f/f/n/g;->e:[Ljava/lang/String;

    array-length v6, v5

    const/4 v7, 0x0

    :goto_4
    if-ge v7, v6, :cond_8

    aget-object v8, v5, v7

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    :goto_5
    const/4 v4, 0x1

    goto :goto_6

    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_8
    const/4 v4, 0x0

    :goto_6
    if-nez v4, :cond_9

    sget-object v1, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "URL fails whitelist rule: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_9
    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0xff

    if-gt v4, v5, :cond_a

    const/4 v4, 0x1

    goto :goto_7

    :cond_a
    const/4 v4, 0x0

    :goto_7
    if-nez v4, :cond_b

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "URL host is null or invalid"

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_b
    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    goto :goto_8

    :cond_c
    const-string v5, "http"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_e

    const-string v5, "https"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    goto :goto_9

    :cond_d
    :goto_8
    const/4 v4, 0x0

    goto :goto_a

    :cond_e
    :goto_9
    const/4 v4, 0x1

    :goto_a
    if-nez v4, :cond_f

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "URL scheme is null or invalid"

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_f
    invoke-virtual {v0}, Ljava/net/URI;->getUserInfo()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_10

    const/4 v4, 0x1

    goto :goto_b

    :cond_10
    const/4 v4, 0x0

    :goto_b
    if-nez v4, :cond_11

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "URL user info is null"

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_11
    invoke-virtual {v0}, Ljava/net/URI;->getPort()I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_13

    if-lez v0, :cond_12

    goto :goto_c

    :cond_12
    const/4 v0, 0x0

    goto :goto_d

    :cond_13
    :goto_c
    const/4 v0, 0x1

    :goto_d
    if-nez v0, :cond_14

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "URL port is less than or equal to 0"

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_14
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->a0()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->Q()Lf/h/c/y/m/n$d;

    move-result-object v3

    :cond_15
    if-eqz v3, :cond_16

    sget-object v0, Lf/h/c/y/m/n$d;->d:Lf/h/c/y/m/n$d;

    if-eq v3, v0, :cond_16

    const/4 v0, 0x1

    goto :goto_e

    :cond_16
    const/4 v0, 0x0

    :goto_e
    if-nez v0, :cond_17

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "HTTP Method is null or invalid: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->Q()Lf/h/c/y/m/n$d;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_17
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->b0()Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->R()I

    move-result v0

    if-lez v0, :cond_18

    const/4 v0, 0x1

    goto :goto_f

    :cond_18
    const/4 v0, 0x0

    :goto_f
    if-nez v0, :cond_19

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "HTTP ResponseCode is a negative value:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->R()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_19
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->c0()Z

    move-result v0

    const-wide/16 v3, 0x0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->T()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-ltz v0, :cond_1a

    const/4 v0, 0x1

    goto :goto_10

    :cond_1a
    const/4 v0, 0x0

    :goto_10
    if-nez v0, :cond_1b

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "Request Payload is a negative value:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->T()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_1b
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->d0()Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->U()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-ltz v0, :cond_1c

    const/4 v0, 0x1

    goto :goto_11

    :cond_1c
    const/4 v0, 0x0

    :goto_11
    if-nez v0, :cond_1d

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "Response Payload is a negative value:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->U()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_1d
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->Z()Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->O()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-gtz v0, :cond_1e

    goto/16 :goto_15

    :cond_1e
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->e0()Z

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->V()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-ltz v0, :cond_1f

    const/4 v0, 0x1

    goto :goto_12

    :cond_1f
    const/4 v0, 0x0

    :goto_12
    if-nez v0, :cond_20

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "Time to complete the request is a negative value:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->V()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_20
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->g0()Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->X()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-ltz v0, :cond_21

    const/4 v0, 0x1

    goto :goto_13

    :cond_21
    const/4 v0, 0x0

    :goto_13
    if-nez v0, :cond_22

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "Time from the start of the request to the start of the response is null or a negative value:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->X()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_22
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->f0()Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->W()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-gtz v0, :cond_23

    goto :goto_14

    :cond_23
    iget-object v0, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->b0()Z

    move-result v0

    if-nez v0, :cond_24

    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "Did not receive a HTTP Response Code"

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_24
    return v1

    :cond_25
    :goto_14
    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "Time from the start of the request to the end of the response is null, negative or zero:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->W()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_26
    :goto_15
    sget-object v0, Lf/h/c/y/g/e;->c:Lf/h/c/y/h/a;

    const-string v1, "Start time of the request is null, or zero, or a negative value:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lf/h/c/y/g/e;->a:Lf/h/c/y/m/n;

    invoke-virtual {v3}, Lf/h/c/y/m/n;->O()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2
.end method
