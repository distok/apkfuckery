.class public Lf/h/c/y/g/a;
.super Ljava/lang/Object;
.source "AppStateMonitor.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/g/a$a;
    }
.end annotation


# static fields
.field public static final s:Lf/h/c/y/h/a;

.field public static volatile t:Lf/h/c/y/g/a;


# instance fields
.field public d:Z

.field public final e:Lf/h/c/y/k/l;

.field public f:Lf/h/c/y/d/a;

.field public final g:Lf/h/c/y/l/a;

.field public h:Z

.field public final i:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/app/Activity;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/firebase/perf/util/Timer;

.field public k:Lcom/google/firebase/perf/util/Timer;

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/concurrent/atomic/AtomicInteger;

.field public n:Lf/h/c/y/m/d;

.field public o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/ref/WeakReference<",
            "Lf/h/c/y/g/a$a;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:Z

.field public q:Landroidx/core/app/FrameMetricsAggregator;

.field public final r:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/app/Activity;",
            "Lcom/google/firebase/perf/metrics/Trace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/g/a;->s:Lf/h/c/y/h/a;

    return-void
.end method

.method public constructor <init>(Lf/h/c/y/k/l;Lf/h/c/y/l/a;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/c/y/g/a;->d:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/h/c/y/g/a;->h:Z

    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v2, p0, Lf/h/c/y/g/a;->i:Ljava/util/WeakHashMap;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, Lf/h/c/y/g/a;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v2, Lf/h/c/y/m/d;->f:Lf/h/c/y/m/d;

    iput-object v2, p0, Lf/h/c/y/g/a;->n:Lf/h/c/y/m/d;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    iput-boolean v0, p0, Lf/h/c/y/g/a;->p:Z

    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v2, p0, Lf/h/c/y/g/a;->r:Ljava/util/WeakHashMap;

    iput-object p1, p0, Lf/h/c/y/g/a;->e:Lf/h/c/y/k/l;

    iput-object p2, p0, Lf/h/c/y/g/a;->g:Lf/h/c/y/l/a;

    invoke-static {}, Lf/h/c/y/d/a;->f()Lf/h/c/y/d/a;

    move-result-object p1

    iput-object p1, p0, Lf/h/c/y/g/a;->f:Lf/h/c/y/d/a;

    :try_start_0
    const-string p1, "androidx.core.app.FrameMetricsAggregator"

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    nop

    :goto_0
    iput-boolean v0, p0, Lf/h/c/y/g/a;->p:Z

    if-eqz v0, :cond_0

    new-instance p1, Landroidx/core/app/FrameMetricsAggregator;

    invoke-direct {p1}, Landroidx/core/app/FrameMetricsAggregator;-><init>()V

    iput-object p1, p0, Lf/h/c/y/g/a;->q:Landroidx/core/app/FrameMetricsAggregator;

    :cond_0
    return-void
.end method

.method public static a()Lf/h/c/y/g/a;
    .locals 4

    sget-object v0, Lf/h/c/y/g/a;->t:Lf/h/c/y/g/a;

    if-nez v0, :cond_1

    const-class v0, Lf/h/c/y/g/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/c/y/g/a;->t:Lf/h/c/y/g/a;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/c/y/g/a;

    sget-object v2, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    new-instance v3, Lf/h/c/y/l/a;

    invoke-direct {v3}, Lf/h/c/y/l/a;-><init>()V

    invoke-direct {v1, v2, v3}, Lf/h/c/y/g/a;-><init>(Lf/h/c/y/k/l;Lf/h/c/y/l/a;)V

    sput-object v1, Lf/h/c/y/g/a;->t:Lf/h/c/y/g/a;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lf/h/c/y/g/a;->t:Lf/h/c/y/g/a;

    return-object v0
.end method

.method public static b(Landroid/app/Activity;)Ljava/lang/String;
    .locals 1

    const-string v0, "_st_"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public c(Ljava/lang/String;J)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v3, p2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final d(Landroid/app/Activity;)Z
    .locals 1

    iget-boolean v0, p0, Lf/h/c/y/g/a;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p1

    iget p1, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v0, 0x1000000

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final e(Landroid/app/Activity;)V
    .locals 10

    iget-object v0, p0, Lf/h/c/y/g/a;->r:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/c/y/g/a;->r:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/perf/metrics/Trace;

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Lf/h/c/y/g/a;->r:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lf/h/c/y/g/a;->q:Landroidx/core/app/FrameMetricsAggregator;

    invoke-virtual {v1, p1}, Landroidx/core/app/FrameMetricsAggregator;->remove(Landroid/app/Activity;)[Landroid/util/SparseIntArray;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    aget-object v1, v1, v2

    if-eqz v1, :cond_4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v7

    if-ge v3, v7, :cond_5

    invoke-virtual {v1, v3}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v7

    invoke-virtual {v1, v3}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v8

    add-int/2addr v4, v8

    const/16 v9, 0x2bc

    if-le v7, v9, :cond_2

    add-int/2addr v6, v8

    :cond_2
    const/16 v9, 0x10

    if-le v7, v9, :cond_3

    add-int/2addr v5, v8

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :cond_5
    if-lez v4, :cond_6

    sget-object v1, Lf/h/c/y/l/b;->g:Lf/h/c/y/l/b;

    invoke-virtual {v1}, Lf/h/c/y/l/b;->toString()Ljava/lang/String;

    move-result-object v1

    int-to-long v7, v4

    invoke-virtual {v0, v1, v7, v8}, Lcom/google/firebase/perf/metrics/Trace;->putMetric(Ljava/lang/String;J)V

    :cond_6
    if-lez v5, :cond_7

    sget-object v1, Lf/h/c/y/l/b;->h:Lf/h/c/y/l/b;

    invoke-virtual {v1}, Lf/h/c/y/l/b;->toString()Ljava/lang/String;

    move-result-object v1

    int-to-long v7, v5

    invoke-virtual {v0, v1, v7, v8}, Lcom/google/firebase/perf/metrics/Trace;->putMetric(Ljava/lang/String;J)V

    :cond_7
    if-lez v6, :cond_8

    sget-object v1, Lf/h/c/y/l/b;->i:Lf/h/c/y/l/b;

    invoke-virtual {v1}, Lf/h/c/y/l/b;->toString()Ljava/lang/String;

    move-result-object v1

    int-to-long v7, v6

    invoke-virtual {v0, v1, v7, v8}, Lcom/google/firebase/perf/metrics/Trace;->putMetric(Ljava/lang/String;J)V

    :cond_8
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lf/h/c/y/l/g;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_9

    sget-object v1, Lf/h/c/y/g/a;->s:Lf/h/c/y/h/a;

    const-string v3, "sendScreenTrace name:"

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lf/h/c/y/g/a;->b(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " _fr_tot:"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " _fr_slo:"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " _fr_fzn:"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_9
    invoke-virtual {v0}, Lcom/google/firebase/perf/metrics/Trace;->stop()V

    return-void
.end method

.method public final f(Ljava/lang/String;Lcom/google/firebase/perf/util/Timer;Lcom/google/firebase/perf/util/Timer;)V
    .locals 3

    iget-object v0, p0, Lf/h/c/y/g/a;->f:Lf/h/c/y/d/a;

    invoke-virtual {v0}, Lf/h/c/y/d/a;->p()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lf/h/c/y/m/t;->T()Lf/h/c/y/m/t$b;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v1, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/t;

    invoke-static {v1, p1}, Lf/h/c/y/m/t;->B(Lf/h/c/y/m/t;Ljava/lang/String;)V

    iget-wide v1, p2, Lcom/google/firebase/perf/util/Timer;->d:J

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/m/t$b;->x(J)Lf/h/c/y/m/t$b;

    invoke-virtual {p2, p3}, Lcom/google/firebase/perf/util/Timer;->b(Lcom/google/firebase/perf/util/Timer;)J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Lf/h/c/y/m/t$b;->A(J)Lf/h/c/y/m/t$b;

    invoke-static {}, Lcom/google/firebase/perf/internal/SessionManager;->getInstance()Lcom/google/firebase/perf/internal/SessionManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/perf/internal/SessionManager;->perfSession()Lcom/google/firebase/perf/internal/PerfSession;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/perf/internal/PerfSession;->a()Lf/h/c/y/m/q;

    move-result-object p1

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object p2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast p2, Lf/h/c/y/m/t;

    invoke-static {p2, p1}, Lf/h/c/y/m/t;->G(Lf/h/c/y/m/t;Lf/h/c/y/m/q;)V

    iget-object p1, p0, Lf/h/c/y/g/a;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result p1

    iget-object p2, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    monitor-enter p2

    :try_start_0
    iget-object p3, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v1, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/t;

    invoke-static {v1}, Lf/h/c/y/m/t;->C(Lf/h/c/y/m/t;)Ljava/util/Map;

    move-result-object v1

    check-cast v1, Lf/h/e/e0;

    invoke-virtual {v1, p3}, Lf/h/e/e0;->putAll(Ljava/util/Map;)V

    if-eqz p1, :cond_1

    sget-object p3, Lf/h/c/y/l/b;->f:Lf/h/c/y/l/b;

    invoke-virtual {p3}, Lf/h/c/y/l/b;->toString()Ljava/lang/String;

    move-result-object p3

    int-to-long v1, p1

    invoke-virtual {v0, p3, v1, v2}, Lf/h/c/y/m/t$b;->w(Ljava/lang/String;J)Lf/h/c/y/m/t$b;

    :cond_1
    iget-object p1, p0, Lf/h/c/y/g/a;->l:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lf/h/c/y/g/a;->e:Lf/h/c/y/k/l;

    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object p2

    check-cast p2, Lf/h/c/y/m/t;

    sget-object p3, Lf/h/c/y/m/d;->g:Lf/h/c/y/m/d;

    iget-object v0, p1, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lf/h/c/y/k/i;

    invoke-direct {v1, p1, p2, p3}, Lf/h/c/y/k/i;-><init>(Lf/h/c/y/k/l;Lf/h/c/y/m/t;Lf/h/c/y/m/d;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final g(Lf/h/c/y/m/d;)V
    .locals 3

    iput-object p1, p0, Lf/h/c/y/g/a;->n:Lf/h/c/y/m/d;

    iget-object p1, p0, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/y/g/a$a;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lf/h/c/y/g/a;->n:Lf/h/c/y/m/d;

    invoke-interface {v1, v2}, Lf/h/c/y/g/a$a;->onUpdateAppState(Lf/h/c/y/m/d;)V

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lf/h/c/y/g/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/c/y/g/a;->g:Lf/h/c/y/l/a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    iput-object v1, p0, Lf/h/c/y/g/a;->k:Lcom/google/firebase/perf/util/Timer;

    iget-object v1, p0, Lf/h/c/y/g/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lf/h/c/y/m/d;->e:Lf/h/c/y/m/d;

    invoke-virtual {p0, p1}, Lf/h/c/y/g/a;->g(Lf/h/c/y/m/d;)V

    iget-boolean p1, p0, Lf/h/c/y/g/a;->h:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/c/y/g/a;->h:Z

    goto :goto_0

    :cond_0
    sget-object p1, Lf/h/c/y/l/c;->i:Lf/h/c/y/l/c;

    invoke-virtual {p1}, Lf/h/c/y/l/c;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lf/h/c/y/g/a;->j:Lcom/google/firebase/perf/util/Timer;

    iget-object v1, p0, Lf/h/c/y/g/a;->k:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p0, p1, v0, v1}, Lf/h/c/y/g/a;->f(Ljava/lang/String;Lcom/google/firebase/perf/util/Timer;Lcom/google/firebase/perf/util/Timer;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lf/h/c/y/g/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized onActivityStarted(Landroid/app/Activity;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lf/h/c/y/g/a;->d(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/g/a;->f:Lf/h/c/y/d/a;

    invoke-virtual {v0}, Lf/h/c/y/d/a;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/g/a;->q:Landroidx/core/app/FrameMetricsAggregator;

    invoke-virtual {v0, p1}, Landroidx/core/app/FrameMetricsAggregator;->add(Landroid/app/Activity;)V

    new-instance v0, Lcom/google/firebase/perf/metrics/Trace;

    invoke-static {p1}, Lf/h/c/y/g/a;->b(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lf/h/c/y/g/a;->e:Lf/h/c/y/k/l;

    iget-object v3, p0, Lf/h/c/y/g/a;->g:Lf/h/c/y/l/a;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/firebase/perf/metrics/Trace;-><init>(Ljava/lang/String;Lf/h/c/y/k/l;Lf/h/c/y/l/a;Lf/h/c/y/g/a;)V

    invoke-virtual {v0}, Lcom/google/firebase/perf/metrics/Trace;->start()V

    iget-object v1, p0, Lf/h/c/y/g/a;->r:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lf/h/c/y/g/a;->d(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lf/h/c/y/g/a;->e(Landroid/app/Activity;)V

    :cond_0
    iget-object v0, p0, Lf/h/c/y/g/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/c/y/g/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/h/c/y/g/a;->i:Ljava/util/WeakHashMap;

    invoke-virtual {p1}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/h/c/y/g/a;->g:Lf/h/c/y/l/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {p1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    iput-object p1, p0, Lf/h/c/y/g/a;->j:Lcom/google/firebase/perf/util/Timer;

    sget-object p1, Lf/h/c/y/m/d;->f:Lf/h/c/y/m/d;

    invoke-virtual {p0, p1}, Lf/h/c/y/g/a;->g(Lf/h/c/y/m/d;)V

    sget-object p1, Lf/h/c/y/l/c;->h:Lf/h/c/y/l/c;

    invoke-virtual {p1}, Lf/h/c/y/l/c;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lf/h/c/y/g/a;->k:Lcom/google/firebase/perf/util/Timer;

    iget-object v1, p0, Lf/h/c/y/g/a;->j:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p0, p1, v0, v1}, Lf/h/c/y/g/a;->f(Ljava/lang/String;Lcom/google/firebase/perf/util/Timer;Lcom/google/firebase/perf/util/Timer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
