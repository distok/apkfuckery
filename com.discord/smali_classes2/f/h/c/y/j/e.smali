.class public Lf/h/c/y/j/e;
.super Ljava/lang/Object;
.source "InstrURLConnectionBase.java"


# static fields
.field public static final f:Lf/h/c/y/h/a;


# instance fields
.field public final a:Ljava/net/HttpURLConnection;

.field public final b:Lf/h/c/y/f/a;

.field public c:J

.field public d:J

.field public final e:Lcom/google/firebase/perf/util/Timer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/j/e;->f:Lf/h/c/y/h/a;

    return-void
.end method

.method public constructor <init>(Ljava/net/HttpURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/h/c/y/j/e;->c:J

    iput-wide v0, p0, Lf/h/c/y/j/e;->d:J

    iput-object p1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    iput-object p3, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iput-object p2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object p1

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Lf/h/c/y/f/a;->k(Ljava/lang/String;)Lf/h/c/y/f/a;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h/c/y/j/e;->c:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->c()V

    iget-object v0, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    iget-wide v0, v0, Lcom/google/firebase/perf/util/Timer;->d:J

    iput-wide v0, p0, Lf/h/c/y/j/e;->c:J

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public b()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/c/y/j/e;->l()V

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContent()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    instance-of v1, v0, Ljava/io/InputStream;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lf/h/c/y/f/a;->g(Ljava/lang/String;)Lf/h/c/y/f/a;

    new-instance v1, Lf/h/c/y/j/a;

    check-cast v0, Ljava/io/InputStream;

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v3, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1, v0, v2, v3}, Lf/h/c/y/j/a;-><init>(Ljava/io/InputStream;Lf/h/c/y/f/a;Lcom/google/firebase/perf/util/Timer;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lf/h/c/y/f/a;->g(Ljava/lang/String;)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v1}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public c([Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/c/y/j/e;->l()V

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getContent([Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    instance-of v0, p1, Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->g(Ljava/lang/String;)Lf/h/c/y/f/a;

    new-instance v0, Lf/h/c/y/j/a;

    check-cast p1, Ljava/io/InputStream;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v0, p1, v1, v2}, Lf/h/c/y/j/a;-><init>(Ljava/io/InputStream;Lf/h/c/y/f/a;Lcom/google/firebase/perf/util/Timer;)V

    move-object p1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->g(Ljava/lang/String;)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v0}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    :goto_0
    return-object p1

    :catch_0
    move-exception p1

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v0}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getDoOutput()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/io/InputStream;
    .locals 4

    invoke-virtual {p0}, Lf/h/c/y/j/e;->l()V

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v0, Lf/h/c/y/j/e;->f:Lf/h/c/y/h/a;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "IOException thrown trying to obtain the response code"

    invoke-virtual {v0, v2, v1}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lf/h/c/y/j/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v3, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1, v0, v2, v3}, Lf/h/c/y/j/a;-><init>(Ljava/io/InputStream;Lf/h/c/y/f/a;Lcom/google/firebase/perf/util/Timer;)V

    return-object v1

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public f()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/c/y/j/e;->l()V

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->g(Ljava/lang/String;)Lf/h/c/y/f/a;

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lf/h/c/y/j/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v3, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1, v0, v2, v3}, Lf/h/c/y/j/a;-><init>(Ljava/io/InputStream;Lf/h/c/y/f/a;Lcom/google/firebase/perf/util/Timer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public g()Ljava/io/OutputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lf/h/c/y/j/b;

    iget-object v1, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v3, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v0, v1, v2, v3}, Lf/h/c/y/j/b;-><init>(Ljava/io/OutputStream;Lf/h/c/y/f/a;Lcom/google/firebase/perf/util/Timer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public h()Ljava/security/Permission;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getPermission()Ljava/security/Permission;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/c/y/j/e;->l()V

    iget-wide v0, p0, Lf/h/c/y/j/e;->d:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/c/y/j/e;->d:J

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->j(J)Lf/h/c/y/f/a;

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v1, v0}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public k()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/c/y/j/e;->l()V

    iget-wide v0, p0, Lf/h/c/y/j/e;->d:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/c/y/j/e;->d:J

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->j(J)Lf/h/c/y/f/a;

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public final l()V
    .locals 5

    iget-wide v0, p0, Lf/h/c/y/j/e;->c:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->c()V

    iget-object v0, p0, Lf/h/c/y/j/e;->e:Lcom/google/firebase/perf/util/Timer;

    iget-wide v0, v0, Lcom/google/firebase/perf/util/Timer;->d:J

    iput-wide v0, p0, Lf/h/c/y/j/e;->c:J

    iget-object v2, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    :cond_0
    invoke-virtual {p0}, Lf/h/c/y/j/e;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    invoke-virtual {v1, v0}, Lf/h/c/y/f/a;->c(Ljava/lang/String;)Lf/h/c/y/f/a;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/h/c/y/j/e;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->c(Ljava/lang/String;)Lf/h/c/y/f/a;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/h/c/y/j/e;->b:Lf/h/c/y/f/a;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->c(Ljava/lang/String;)Lf/h/c/y/f/a;

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/j/e;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
