.class public final Lf/h/c/y/j/h;
.super Ljava/lang/Object;
.source "NetworkRequestMetricBuilderUtil.java"


# static fields
.field public static final a:Ljava/util/regex/Pattern;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-string v0, "(^|.*\\s)datatransport/\\S+ android/($|\\s.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/h/c/y/j/h;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static a(Lorg/apache/http/HttpMessage;)Ljava/lang/Long;
    .locals 2
    .param p0    # Lorg/apache/http/HttpMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    :try_start_0
    const-string v0, "content-length"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpMessage;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "The content-length value is not a valid number"

    invoke-virtual {p0, v1, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/http/HttpResponse;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "content-type"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static c(Lf/h/c/y/f/a;)V
    .locals 2

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->b0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    sget-object v1, Lf/h/c/y/m/n$e;->e:Lf/h/c/y/m/n$e;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, v1}, Lf/h/c/y/m/n;->C(Lf/h/c/y/m/n;Lf/h/c/y/m/n$e;)V

    :cond_0
    invoke-virtual {p0}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    return-void
.end method
