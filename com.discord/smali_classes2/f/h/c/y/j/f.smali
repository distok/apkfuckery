.class public Lf/h/c/y/j/f;
.super Ljava/lang/Object;
.source "InstrumentApacheHttpResponseHandler.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler<",
            "+TT;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/firebase/perf/util/Timer;

.field public final c:Lf/h/c/y/f/a;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/ResponseHandler;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/ResponseHandler<",
            "+TT;>;",
            "Lcom/google/firebase/perf/util/Timer;",
            "Lf/h/c/y/f/a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/y/j/f;->a:Lorg/apache/http/client/ResponseHandler;

    iput-object p2, p0, Lf/h/c/y/j/f;->b:Lcom/google/firebase/perf/util/Timer;

    iput-object p3, p0, Lf/h/c/y/j/f;->c:Lf/h/c/y/f/a;

    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/j/f;->c:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/f;->b:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/f;->c:Lf/h/c/y/f/a;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;

    invoke-static {p1}, Lf/h/c/y/j/h;->a(Lorg/apache/http/HttpMessage;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/h/c/y/j/f;->c:Lf/h/c/y/f/a;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;

    :cond_0
    invoke-static {p1}, Lf/h/c/y/j/h;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/h/c/y/j/f;->c:Lf/h/c/y/f/a;

    invoke-virtual {v1, v0}, Lf/h/c/y/f/a;->g(Ljava/lang/String;)Lf/h/c/y/f/a;

    :cond_1
    iget-object v0, p0, Lf/h/c/y/j/f;->c:Lf/h/c/y/f/a;

    invoke-virtual {v0}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    iget-object v0, p0, Lf/h/c/y/j/f;->a:Lorg/apache/http/client/ResponseHandler;

    invoke-interface {v0, p1}, Lorg/apache/http/client/ResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
