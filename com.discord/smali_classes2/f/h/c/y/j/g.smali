.class public Lf/h/c/y/j/g;
.super Ljava/lang/Object;
.source "InstrumentOkHttpEnqueueCallback.java"

# interfaces
.implements Lb0/f;


# instance fields
.field public final a:Lb0/f;

.field public final b:Lf/h/c/y/f/a;

.field public final c:J

.field public final d:Lcom/google/firebase/perf/util/Timer;


# direct methods
.method public constructor <init>(Lb0/f;Lf/h/c/y/k/l;Lcom/google/firebase/perf/util/Timer;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/y/j/g;->a:Lb0/f;

    new-instance p1, Lf/h/c/y/f/a;

    invoke-direct {p1, p2}, Lf/h/c/y/f/a;-><init>(Lf/h/c/y/k/l;)V

    iput-object p1, p0, Lf/h/c/y/j/g;->b:Lf/h/c/y/f/a;

    iput-wide p4, p0, Lf/h/c/y/j/g;->c:J

    iput-object p3, p0, Lf/h/c/y/j/g;->d:Lcom/google/firebase/perf/util/Timer;

    return-void
.end method


# virtual methods
.method public a(Lb0/e;Lokhttp3/Response;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/j/g;->d:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v5

    iget-object v2, p0, Lf/h/c/y/j/g;->b:Lf/h/c/y/f/a;

    iget-wide v3, p0, Lf/h/c/y/j/g;->c:J

    move-object v1, p2

    invoke-static/range {v1 .. v6}, Lcom/google/firebase/perf/network/FirebasePerfOkHttpClient;->a(Lokhttp3/Response;Lf/h/c/y/f/a;JJ)V

    iget-object v0, p0, Lf/h/c/y/j/g;->a:Lb0/f;

    invoke-interface {v0, p1, p2}, Lb0/f;->a(Lb0/e;Lokhttp3/Response;)V

    return-void
.end method

.method public b(Lb0/e;Ljava/io/IOException;)V
    .locals 3

    invoke-interface {p1}, Lb0/e;->c()Lb0/a0;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lb0/a0;->b:Lb0/x;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lf/h/c/y/j/g;->b:Lf/h/c/y/f/a;

    invoke-virtual {v1}, Lb0/x;->k()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lf/h/c/y/f/a;->k(Ljava/lang/String;)Lf/h/c/y/f/a;

    :cond_0
    iget-object v0, v0, Lb0/a0;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/h/c/y/j/g;->b:Lf/h/c/y/f/a;

    invoke-virtual {v1, v0}, Lf/h/c/y/f/a;->c(Ljava/lang/String;)Lf/h/c/y/f/a;

    :cond_1
    iget-object v0, p0, Lf/h/c/y/j/g;->b:Lf/h/c/y/f/a;

    iget-wide v1, p0, Lf/h/c/y/j/g;->c:J

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/g;->b:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/g;->d:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/g;->b:Lf/h/c/y/f/a;

    invoke-static {v0}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    iget-object v0, p0, Lf/h/c/y/j/g;->a:Lb0/f;

    invoke-interface {v0, p1, p2}, Lb0/f;->b(Lb0/e;Ljava/io/IOException;)V

    return-void
.end method
