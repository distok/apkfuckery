.class public final Lf/h/c/y/j/a;
.super Ljava/io/InputStream;
.source "InstrHttpInputStream.java"


# instance fields
.field public final d:Ljava/io/InputStream;

.field public final e:Lf/h/c/y/f/a;

.field public final f:Lcom/google/firebase/perf/util/Timer;

.field public g:J

.field public h:J

.field public i:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lf/h/c/y/f/a;Lcom/google/firebase/perf/util/Timer;)V
    .locals 2

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/h/c/y/j/a;->g:J

    iput-wide v0, p0, Lf/h/c/y/j/a;->i:J

    iput-object p3, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    iput-object p1, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    iput-object p2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object p1, p2, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    iget-object p1, p1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast p1, Lf/h/c/y/m/n;

    invoke-virtual {p1}, Lf/h/c/y/m/n;->X()J

    move-result-wide p1

    iput-wide p1, p0, Lf/h/c/y/j/a;->h:J

    return-void
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public close()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lf/h/c/y/j/a;->i:J

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    iput-wide v0, p0, Lf/h/c/y/j/a;->i:J

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    iget-wide v0, p0, Lf/h/c/y/j/a;->g:J

    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    iget-object v2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;

    :cond_1
    iget-wide v0, p0, Lf/h/c/y/j/a;->h:J

    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->j(J)Lf/h/c/y/f/a;

    :cond_2
    iget-object v0, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-wide v1, p0, Lf/h/c/y/j/a;->i:J

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v0}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public mark(I)V
    .locals 1

    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    return-void
.end method

.method public markSupported()Z
    .locals 1

    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    iget-object v1, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    iget-wide v3, p0, Lf/h/c/y/j/a;->h:J

    const-wide/16 v5, -0x1

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    iput-wide v1, p0, Lf/h/c/y/j/a;->h:J

    :cond_0
    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    iget-wide v3, p0, Lf/h/c/y/j/a;->i:J

    cmp-long v7, v3, v5

    if-nez v7, :cond_1

    iput-wide v1, p0, Lf/h/c/y/j/a;->i:J

    iget-object v3, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v3, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v1}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Lf/h/c/y/j/a;->g:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lf/h/c/y/j/a;->g:J

    iget-object v3, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v3, v1, v2}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public read([B)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result p1

    iget-object v0, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lf/h/c/y/j/a;->h:J

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    iput-wide v0, p0, Lf/h/c/y/j/a;->h:J

    :cond_0
    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    iget-wide v2, p0, Lf/h/c/y/j/a;->i:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    iput-wide v0, p0, Lf/h/c/y/j/a;->i:J

    iget-object v2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v0}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lf/h/c/y/j/a;->g:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h/c/y/j/a;->g:J

    iget-object v2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return p1

    :catch_0
    move-exception p1

    iget-object v0, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-static {v0}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method

.method public read([BII)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1

    iget-object p2, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide p2

    iget-wide v0, p0, Lf/h/c/y/j/a;->h:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iput-wide p2, p0, Lf/h/c/y/j/a;->h:J

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    iget-wide v0, p0, Lf/h/c/y/j/a;->i:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    iput-wide p2, p0, Lf/h/c/y/j/a;->i:J

    iget-object v0, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v0, p2, p3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object p2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {p2}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    goto :goto_0

    :cond_1
    iget-wide p2, p0, Lf/h/c/y/j/a;->g:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Lf/h/c/y/j/a;->g:J

    iget-object v0, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v0, p2, p3}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return p1

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object p3, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p3}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object p2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-static {p2}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method

.method public reset()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public skip(J)J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/a;->d:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide p1

    iget-object v0, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lf/h/c/y/j/a;->h:J

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    iput-wide v0, p0, Lf/h/c/y/j/a;->h:J

    :cond_0
    cmp-long v2, p1, v4

    if-nez v2, :cond_1

    iget-wide v2, p0, Lf/h/c/y/j/a;->i:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    iput-wide v0, p0, Lf/h/c/y/j/a;->i:J

    iget-object v2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lf/h/c/y/j/a;->g:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lf/h/c/y/j/a;->g:J

    iget-object v2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-wide p1

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/a;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v0}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object p2, p0, Lf/h/c/y/j/a;->e:Lf/h/c/y/f/a;

    invoke-static {p2}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method
