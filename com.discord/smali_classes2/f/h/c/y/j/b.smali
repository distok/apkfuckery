.class public final Lf/h/c/y/j/b;
.super Ljava/io/OutputStream;
.source "InstrHttpOutputStream.java"


# instance fields
.field public d:Ljava/io/OutputStream;

.field public e:J

.field public f:Lf/h/c/y/f/a;

.field public final g:Lcom/google/firebase/perf/util/Timer;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lf/h/c/y/f/a;Lcom/google/firebase/perf/util/Timer;)V
    .locals 2

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/h/c/y/j/b;->e:J

    iput-object p1, p0, Lf/h/c/y/j/b;->d:Ljava/io/OutputStream;

    iput-object p2, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    iput-object p3, p0, Lf/h/c/y/j/b;->g:Lcom/google/firebase/perf/util/Timer;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h/c/y/j/b;->e:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    iget-object v2, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-virtual {v2, v0, v1}, Lf/h/c/y/f/a;->e(J)Lf/h/c/y/f/a;

    :cond_0
    iget-object v0, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/b;->g:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    iget-object v0, v0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, v1, v2}, Lf/h/c/y/m/n;->H(Lf/h/c/y/m/n;J)V

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/b;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/b;->g:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/b;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    iget-object v2, p0, Lf/h/c/y/j/b;->g:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-static {v1}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public write(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/b;->d:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    iget-wide v0, p0, Lf/h/c/y/j/b;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h/c/y/j/b;->e:J

    iget-object p1, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-virtual {p1, v0, v1}, Lf/h/c/y/f/a;->e(J)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object v0, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/b;->g:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-static {v0}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method

.method public write([B)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/b;->d:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    iget-wide v0, p0, Lf/h/c/y/j/b;->e:J

    array-length p1, p1

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h/c/y/j/b;->e:J

    iget-object p1, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-virtual {p1, v0, v1}, Lf/h/c/y/f/a;->e(J)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object v0, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    iget-object v1, p0, Lf/h/c/y/j/b;->g:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object v0, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-static {v0}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method

.method public write([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/j/b;->d:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    iget-wide p1, p0, Lf/h/c/y/j/b;->e:J

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lf/h/c/y/j/b;->e:J

    iget-object p3, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-virtual {p3, p1, p2}, Lf/h/c/y/f/a;->e(J)Lf/h/c/y/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    iget-object p3, p0, Lf/h/c/y/j/b;->g:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p3}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    iget-object p2, p0, Lf/h/c/y/j/b;->f:Lf/h/c/y/f/a;

    invoke-static {p2}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method
