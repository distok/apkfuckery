.class public final Lf/h/c/y/f/a;
.super Lf/h/c/y/g/b;
.source "NetworkRequestMetricBuilder.java"

# interfaces
.implements Lf/h/c/y/g/m;


# static fields
.field public static final k:Lf/h/c/y/h/a;


# instance fields
.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/firebase/perf/internal/PerfSession;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/google/firebase/perf/internal/GaugeManager;

.field public final f:Lf/h/c/y/k/l;

.field public final g:Lf/h/c/y/m/n$b;

.field public h:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public final j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/h/c/y/g/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/f/a;->k:Lf/h/c/y/h/a;

    return-void
.end method

.method public constructor <init>(Lf/h/c/y/k/l;)V
    .locals 2

    invoke-static {}, Lf/h/c/y/g/a;->a()Lf/h/c/y/g/a;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/perf/internal/GaugeManager;->getInstance()Lcom/google/firebase/perf/internal/GaugeManager;

    move-result-object v1

    invoke-direct {p0, v0}, Lf/h/c/y/g/b;-><init>(Lf/h/c/y/g/a;)V

    invoke-static {}, Lf/h/c/y/m/n;->h0()Lf/h/c/y/m/n$b;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lf/h/c/y/f/a;->j:Ljava/lang/ref/WeakReference;

    iput-object p1, p0, Lf/h/c/y/f/a;->f:Lf/h/c/y/k/l;

    iput-object v1, p0, Lf/h/c/y/f/a;->e:Lcom/google/firebase/perf/internal/GaugeManager;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lf/h/c/y/f/a;->d:Ljava/util/List;

    invoke-virtual {p0}, Lf/h/c/y/g/b;->registerForAppState()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/perf/internal/PerfSession;)V
    .locals 2

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/y/f/a;->k:Lf/h/c/y/h/a;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Unable to add new SessionId to the Network Trace. Continuing without it."

    invoke-virtual {p1, v1, v0}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->Z()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-virtual {v0}, Lf/h/c/y/m/n;->f0()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/c/y/f/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public b()Lf/h/c/y/m/n;
    .locals 6

    invoke-static {}, Lcom/google/firebase/perf/internal/SessionManager;->getInstance()Lcom/google/firebase/perf/internal/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lf/h/c/y/f/a;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0, v1}, Lcom/google/firebase/perf/internal/SessionManager;->unregisterForSessionUpdates(Ljava/lang/ref/WeakReference;)V

    invoke-virtual {p0}, Lf/h/c/y/g/b;->unregisterForAppState()V

    iget-object v0, p0, Lf/h/c/y/f/a;->d:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lf/h/c/y/f/a;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/firebase/perf/internal/PerfSession;

    if-eqz v3, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/google/firebase/perf/internal/PerfSession;->b(Ljava/util/List;)[Lf/h/c/y/m/q;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1}, Lf/h/e/r$a;->r()V

    iget-object v1, v1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/n;

    invoke-static {v1, v0}, Lf/h/c/y/m/n;->K(Lf/h/c/y/m/n;Ljava/lang/Iterable;)V

    :cond_2
    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/n;

    iget-object v1, p0, Lf/h/c/y/f/a;->h:Ljava/lang/String;

    sget-object v2, Lf/h/c/y/j/h;->a:Ljava/util/regex/Pattern;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_4

    sget-object v4, Lf/h/c/y/j/h;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_5

    sget-object v1, Lf/h/c/y/f/a;->k:Lf/h/c/y/h/a;

    const-string v3, "Dropping network request from a \'User-Agent\' that is not allowed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v2}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :cond_5
    iget-boolean v1, p0, Lf/h/c/y/f/a;->i:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lf/h/c/y/f/a;->f:Lf/h/c/y/k/l;

    invoke-virtual {p0}, Lf/h/c/y/g/b;->getAppState()Lf/h/c/y/m/d;

    move-result-object v2

    iget-object v4, v1, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lf/h/c/y/k/j;

    invoke-direct {v5, v1, v0, v2}, Lf/h/c/y/k/j;-><init>(Lf/h/c/y/k/l;Lf/h/c/y/m/n;Lf/h/c/y/m/d;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    iput-boolean v3, p0, Lf/h/c/y/f/a;->i:Z

    :cond_6
    return-object v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public c(Ljava/lang/String;)Lf/h/c/y/f/a;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_9

    sget-object v0, Lf/h/c/y/m/n$d;->d:Lf/h/c/y/m/n$d;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v2, "DELETE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v1, 0x8

    goto/16 :goto_0

    :sswitch_1
    const-string v2, "CONNECT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x7

    goto :goto_0

    :sswitch_2
    const-string v2, "TRACE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x6

    goto :goto_0

    :sswitch_3
    const-string v2, "PATCH"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x5

    goto :goto_0

    :sswitch_4
    const-string v2, "POST"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    goto :goto_0

    :cond_4
    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "HEAD"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v1, 0x3

    goto :goto_0

    :sswitch_6
    const-string v2, "PUT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    goto :goto_0

    :cond_6
    const/4 v1, 0x2

    goto :goto_0

    :sswitch_7
    const-string v2, "GET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    goto :goto_0

    :cond_7
    const/4 v1, 0x1

    goto :goto_0

    :sswitch_8
    const-string v2, "OPTIONS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    goto :goto_0

    :cond_8
    const/4 v1, 0x0

    :goto_0
    packed-switch v1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    sget-object v0, Lf/h/c/y/m/n$d;->h:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_1
    sget-object v0, Lf/h/c/y/m/n$d;->m:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_2
    sget-object v0, Lf/h/c/y/m/n$d;->l:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_3
    sget-object v0, Lf/h/c/y/m/n$d;->j:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_4
    sget-object v0, Lf/h/c/y/m/n$d;->g:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_5
    sget-object v0, Lf/h/c/y/m/n$d;->i:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_6
    sget-object v0, Lf/h/c/y/m/n$d;->f:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_7
    sget-object v0, Lf/h/c/y/m/n$d;->e:Lf/h/c/y/m/n$d;

    goto :goto_1

    :pswitch_8
    sget-object v0, Lf/h/c/y/m/n$d;->k:Lf/h/c/y/m/n$d;

    :goto_1
    iget-object p1, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {p1}, Lf/h/e/r$a;->r()V

    iget-object p1, p1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast p1, Lf/h/c/y/m/n;

    invoke-static {p1, v0}, Lf/h/c/y/m/n;->L(Lf/h/c/y/m/n;Lf/h/c/y/m/n$d;)V

    :cond_9
    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1faded82 -> :sswitch_8
        0x11336 -> :sswitch_7
        0x136ef -> :sswitch_6
        0x21c5e0 -> :sswitch_5
        0x2590a0 -> :sswitch_4
        0x4862828 -> :sswitch_3
        0x4c5f925 -> :sswitch_2
        0x638004ca -> :sswitch_1
        0x77f979ab -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d(I)Lf/h/c/y/f/a;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, p1}, Lf/h/c/y/m/n;->D(Lf/h/c/y/m/n;I)V

    return-object p0
.end method

.method public e(J)Lf/h/c/y/f/a;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, p1, p2}, Lf/h/c/y/m/n;->M(Lf/h/c/y/m/n;J)V

    return-object p0
.end method

.method public f(J)Lf/h/c/y/f/a;
    .locals 3

    invoke-static {}, Lcom/google/firebase/perf/internal/SessionManager;->getInstance()Lcom/google/firebase/perf/internal/SessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/perf/internal/SessionManager;->perfSession()Lcom/google/firebase/perf/internal/PerfSession;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/perf/internal/SessionManager;->getInstance()Lcom/google/firebase/perf/internal/SessionManager;

    move-result-object v1

    iget-object v2, p0, Lf/h/c/y/f/a;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1, v2}, Lcom/google/firebase/perf/internal/SessionManager;->registerForSessionUpdates(Ljava/lang/ref/WeakReference;)V

    iget-object v1, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v1}, Lf/h/e/r$a;->r()V

    iget-object v1, v1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/n;

    invoke-static {v1, p1, p2}, Lf/h/c/y/m/n;->G(Lf/h/c/y/m/n;J)V

    invoke-virtual {p0, v0}, Lf/h/c/y/f/a;->a(Lcom/google/firebase/perf/internal/PerfSession;)V

    iget-boolean p1, v0, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/c/y/f/a;->e:Lcom/google/firebase/perf/internal/GaugeManager;

    iget-object p2, v0, Lcom/google/firebase/perf/internal/PerfSession;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p1, p2}, Lcom/google/firebase/perf/internal/GaugeManager;->collectGaugeMetricOnce(Lcom/google/firebase/perf/util/Timer;)V

    :cond_0
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lf/h/c/y/f/a;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    iget-object p1, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {p1}, Lf/h/e/r$a;->r()V

    iget-object p1, p1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast p1, Lf/h/c/y/m/n;

    invoke-static {p1}, Lf/h/c/y/m/n;->F(Lf/h/c/y/m/n;)V

    return-object p0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x80

    const/4 v2, 0x0

    if-le v0, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_4

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v3, 0x1f

    if-le v1, v3, :cond_3

    const/16 v3, 0x7f

    if-le v1, v3, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, p1}, Lf/h/c/y/m/n;->E(Lf/h/c/y/m/n;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    sget-object v0, Lf/h/c/y/f/a;->k:Lf/h/c/y/h/a;

    const-string v1, "The content type of the response is not a valid content-type:"

    invoke-static {v1, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    return-object p0
.end method

.method public h(J)Lf/h/c/y/f/a;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, p1, p2}, Lf/h/c/y/m/n;->N(Lf/h/c/y/m/n;J)V

    return-object p0
.end method

.method public i(J)Lf/h/c/y/f/a;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, p1, p2}, Lf/h/c/y/m/n;->J(Lf/h/c/y/m/n;J)V

    invoke-static {}, Lcom/google/firebase/perf/internal/SessionManager;->getInstance()Lcom/google/firebase/perf/internal/SessionManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/perf/internal/SessionManager;->perfSession()Lcom/google/firebase/perf/internal/PerfSession;

    move-result-object p1

    iget-boolean p1, p1, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/c/y/f/a;->e:Lcom/google/firebase/perf/internal/GaugeManager;

    invoke-static {}, Lcom/google/firebase/perf/internal/SessionManager;->getInstance()Lcom/google/firebase/perf/internal/SessionManager;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/firebase/perf/internal/SessionManager;->perfSession()Lcom/google/firebase/perf/internal/PerfSession;

    move-result-object p2

    iget-object p2, p2, Lcom/google/firebase/perf/internal/PerfSession;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p1, p2}, Lcom/google/firebase/perf/internal/GaugeManager;->collectGaugeMetricOnce(Lcom/google/firebase/perf/util/Timer;)V

    :cond_0
    return-object p0
.end method

.method public j(J)Lf/h/c/y/f/a;
    .locals 1

    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, p1, p2}, Lf/h/c/y/m/n;->I(Lf/h/c/y/m/n;J)V

    return-object p0
.end method

.method public k(Ljava/lang/String;)Lf/h/c/y/f/a;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_5

    invoke-static {p1}, Lb0/x;->h(Ljava/lang/String;)Lb0/x;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb0/x;->f()Lb0/x$a;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Lb0/x$a;->g(Ljava/lang/String;)Lb0/x$a;

    invoke-virtual {p1, v0}, Lb0/x$a;->f(Ljava/lang/String;)Lb0/x$a;

    const/4 v0, 0x0

    iput-object v0, p1, Lb0/x$a;->g:Ljava/util/List;

    iput-object v0, p1, Lb0/x$a;->h:Ljava/lang/String;

    invoke-virtual {p1}, Lb0/x$a;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lf/h/c/y/f/a;->g:Lf/h/c/y/m/n$b;

    const/16 v1, 0x7d0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    const/4 v4, 0x0

    if-ne v2, v3, :cond_2

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lb0/x;->h(Ljava/lang/String;)Lb0/x;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Lb0/x;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-ltz v2, :cond_4

    const/16 v2, 0x7cf

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v2

    if-ltz v2, :cond_4

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/n;

    invoke-static {v0, p1}, Lf/h/c/y/m/n;->B(Lf/h/c/y/m/n;Ljava/lang/String;)V

    :cond_5
    return-object p0
.end method
