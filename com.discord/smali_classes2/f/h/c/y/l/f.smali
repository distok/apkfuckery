.class public abstract enum Lf/h/c/y/l/f;
.super Ljava/lang/Enum;
.source "StorageUnit.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/c/y/l/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/c/y/l/f;

.field public static final enum e:Lf/h/c/y/l/f;

.field public static final enum f:Lf/h/c/y/l/f;

.field public static final enum g:Lf/h/c/y/l/f;

.field public static final enum h:Lf/h/c/y/l/f;

.field public static final synthetic i:[Lf/h/c/y/l/f;


# instance fields
.field public numBytes:J


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    new-instance v0, Lf/h/c/y/l/f$a;

    const-string v1, "TERABYTES"

    const/4 v2, 0x0

    const-wide v3, 0x10000000000L

    invoke-direct {v0, v1, v2, v3, v4}, Lf/h/c/y/l/f$a;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lf/h/c/y/l/f;->d:Lf/h/c/y/l/f;

    new-instance v1, Lf/h/c/y/l/f$b;

    const-string v3, "GIGABYTES"

    const/4 v4, 0x1

    const-wide/32 v5, 0x40000000

    invoke-direct {v1, v3, v4, v5, v6}, Lf/h/c/y/l/f$b;-><init>(Ljava/lang/String;IJ)V

    sput-object v1, Lf/h/c/y/l/f;->e:Lf/h/c/y/l/f;

    new-instance v3, Lf/h/c/y/l/f$c;

    const-string v5, "MEGABYTES"

    const/4 v6, 0x2

    const-wide/32 v7, 0x100000

    invoke-direct {v3, v5, v6, v7, v8}, Lf/h/c/y/l/f$c;-><init>(Ljava/lang/String;IJ)V

    sput-object v3, Lf/h/c/y/l/f;->f:Lf/h/c/y/l/f;

    new-instance v5, Lf/h/c/y/l/f$d;

    const-string v7, "KILOBYTES"

    const/4 v8, 0x3

    const-wide/16 v9, 0x400

    invoke-direct {v5, v7, v8, v9, v10}, Lf/h/c/y/l/f$d;-><init>(Ljava/lang/String;IJ)V

    sput-object v5, Lf/h/c/y/l/f;->g:Lf/h/c/y/l/f;

    new-instance v7, Lf/h/c/y/l/f$e;

    const-string v9, "BYTES"

    const/4 v10, 0x4

    const-wide/16 v11, 0x1

    invoke-direct {v7, v9, v10, v11, v12}, Lf/h/c/y/l/f$e;-><init>(Ljava/lang/String;IJ)V

    sput-object v7, Lf/h/c/y/l/f;->h:Lf/h/c/y/l/f;

    const/4 v9, 0x5

    new-array v9, v9, [Lf/h/c/y/l/f;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    sput-object v9, Lf/h/c/y/l/f;->i:[Lf/h/c/y/l/f;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJLf/h/c/y/l/f$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-wide p3, p0, Lf/h/c/y/l/f;->numBytes:J

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/c/y/l/f;
    .locals 1

    const-class v0, Lf/h/c/y/l/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/c/y/l/f;

    return-object p0
.end method

.method public static values()[Lf/h/c/y/l/f;
    .locals 1

    sget-object v0, Lf/h/c/y/l/f;->i:[Lf/h/c/y/l/f;

    invoke-virtual {v0}, [Lf/h/c/y/l/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/c/y/l/f;

    return-object v0
.end method


# virtual methods
.method public f(J)J
    .locals 2

    iget-wide v0, p0, Lf/h/c/y/l/f;->numBytes:J

    mul-long p1, p1, v0

    sget-object v0, Lf/h/c/y/l/f;->g:Lf/h/c/y/l/f;

    iget-wide v0, v0, Lf/h/c/y/l/f;->numBytes:J

    div-long/2addr p1, v0

    return-wide p1
.end method
