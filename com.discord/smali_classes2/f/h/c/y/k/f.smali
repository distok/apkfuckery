.class public final synthetic Lf/h/c/y/k/f;
.super Ljava/lang/Object;
.source "TransportManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/c/y/k/l;


# direct methods
.method public constructor <init>(Lf/h/c/y/k/l;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/y/k/f;->d:Lf/h/c/y/k/l;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lf/h/c/y/k/f;->d:Lf/h/c/y/k/l;

    iget-object v1, v0, Lf/h/c/y/k/l;->d:Lf/h/c/c;

    invoke-virtual {v1}, Lf/h/c/c;->a()V

    iget-object v1, v1, Lf/h/c/c;->a:Landroid/content/Context;

    iput-object v1, v0, Lf/h/c/y/k/l;->l:Landroid/content/Context;

    invoke-static {}, Lf/h/c/y/d/a;->f()Lf/h/c/y/d/a;

    move-result-object v1

    iput-object v1, v0, Lf/h/c/y/k/l;->m:Lf/h/c/y/d/a;

    new-instance v1, Lf/h/c/y/k/e;

    iget-object v3, v0, Lf/h/c/y/k/l;->l:Landroid/content/Context;

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x1f4

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lf/h/c/y/k/e;-><init>(Landroid/content/Context;DJ)V

    iput-object v1, v0, Lf/h/c/y/k/l;->n:Lf/h/c/y/k/e;

    invoke-static {}, Lf/h/c/y/g/a;->a()Lf/h/c/y/g/a;

    move-result-object v1

    iput-object v1, v0, Lf/h/c/y/k/l;->o:Lf/h/c/y/g/a;

    new-instance v1, Lf/h/c/y/k/a;

    iget-object v2, v0, Lf/h/c/y/k/l;->l:Landroid/content/Context;

    iget-object v3, v0, Lf/h/c/y/k/l;->m:Lf/h/c/y/d/a;

    invoke-virtual {v3}, Lf/h/c/y/d/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lf/h/c/y/k/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, v0, Lf/h/c/y/k/l;->h:Lf/h/c/y/k/a;

    new-instance v1, Lf/h/c/y/k/c;

    iget-object v2, v0, Lf/h/c/y/k/l;->g:Lf/h/c/u/a;

    iget-object v3, v0, Lf/h/c/y/k/l;->m:Lf/h/c/y/d/a;

    invoke-virtual {v3}, Lf/h/c/y/d/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lf/h/c/y/k/c;-><init>(Lf/h/c/u/a;Ljava/lang/String;)V

    iput-object v1, v0, Lf/h/c/y/k/l;->i:Lf/h/c/y/k/c;

    iget-object v1, v0, Lf/h/c/y/k/l;->o:Lf/h/c/y/g/a;

    new-instance v2, Ljava/lang/ref/WeakReference;

    sget-object v3, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget-object v3, v1, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    monitor-enter v3

    :try_start_0
    iget-object v1, v1, Lf/h/c/y/g/a;->o:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, v0, Lf/h/c/y/k/l;->k:Lf/h/c/y/m/c$b;

    iget-object v2, v0, Lf/h/c/y/k/l;->d:Lf/h/c/c;

    invoke-virtual {v2}, Lf/h/c/c;->a()V

    iget-object v2, v2, Lf/h/c/c;->c:Lf/h/c/i;

    iget-object v2, v2, Lf/h/c/i;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/e/r$a;->r()V

    iget-object v3, v1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v3, Lf/h/c/y/m/c;

    invoke-static {v3, v2}, Lf/h/c/y/m/c;->B(Lf/h/c/y/m/c;Ljava/lang/String;)V

    invoke-static {}, Lf/h/c/y/m/a;->H()Lf/h/c/y/m/a$b;

    move-result-object v2

    iget-object v3, v0, Lf/h/c/y/k/l;->l:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lf/h/e/r$a;->r()V

    iget-object v4, v2, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v4, Lf/h/c/y/m/a;

    invoke-static {v4, v3}, Lf/h/c/y/m/a;->B(Lf/h/c/y/m/a;Ljava/lang/String;)V

    sget v3, Lf/h/c/y/a;->a:I

    const-string v3, "19.0.10"

    invoke-virtual {v2}, Lf/h/e/r$a;->r()V

    iget-object v4, v2, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v4, Lf/h/c/y/m/a;

    invoke-static {v4, v3}, Lf/h/c/y/m/a;->C(Lf/h/c/y/m/a;Ljava/lang/String;)V

    iget-object v3, v0, Lf/h/c/y/k/l;->l:Landroid/content/Context;

    const-string v4, ""

    :try_start_1
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    move-object v4, v3

    :catch_0
    :goto_0
    invoke-virtual {v2}, Lf/h/e/r$a;->r()V

    iget-object v3, v2, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v3, Lf/h/c/y/m/a;

    invoke-static {v3, v4}, Lf/h/c/y/m/a;->D(Lf/h/c/y/m/a;Ljava/lang/String;)V

    invoke-virtual {v1}, Lf/h/e/r$a;->r()V

    iget-object v1, v1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/c;

    invoke-virtual {v2}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object v2

    check-cast v2, Lf/h/c/y/m/a;

    invoke-static {v1, v2}, Lf/h/c/y/m/c;->F(Lf/h/c/y/m/c;Lf/h/c/y/m/a;)V

    iget-object v1, v0, Lf/h/c/y/k/l;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_1
    :goto_1
    iget-object v1, v0, Lf/h/c/y/k/l;->s:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lf/h/c/y/k/l;->s:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/y/k/d;

    if-eqz v1, :cond_1

    iget-object v2, v0, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lf/h/c/y/k/g;

    invoke-direct {v3, v0, v1}, Lf/h/c/y/k/g;-><init>(Lf/h/c/y/k/l;Lf/h/c/y/k/d;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
