.class public Lf/h/c/y/k/l;
.super Ljava/lang/Object;
.source "TransportManager.java"

# interfaces
.implements Lf/h/c/y/g/a$a;


# static fields
.field public static final t:Lf/h/c/y/h/a;

.field public static final u:Lf/h/c/y/k/l;


# instance fields
.field public d:Lf/h/c/c;

.field public e:Lf/h/c/y/c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public f:Lf/h/c/v/g;

.field public g:Lf/h/c/u/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/u/a<",
            "Lf/h/a/b/g;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lf/h/c/y/k/a;

.field public i:Lf/h/c/y/k/c;

.field public j:Ljava/util/concurrent/ExecutorService;

.field public final k:Lf/h/c/y/m/c$b;

.field public l:Landroid/content/Context;

.field public m:Lf/h/c/y/d/a;

.field public n:Lf/h/c/y/k/e;

.field public o:Lf/h/c/y/g/a;

.field public final p:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public q:Z

.field public final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lf/h/c/y/k/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    new-instance v0, Lf/h/c/y/k/l;

    invoke-direct {v0}, Lf/h/c/y/k/l;-><init>()V

    sput-object v0, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lf/h/c/y/k/l;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-boolean v1, p0, Lf/h/c/y/k/l;->q:Z

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lf/h/c/y/k/l;->s:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-wide/16 v4, 0xa

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Lf/h/c/y/m/c;->N()Lf/h/c/y/m/c$b;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/y/k/l;->k:Lf/h/c/y/m/c$b;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lf/h/c/y/k/l;->r:Ljava/util/Map;

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "KEY_AVAILABLE_TRACES_FOR_CACHING"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "KEY_AVAILABLE_NETWORK_REQUESTS_FOR_CACHING"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "KEY_AVAILABLE_GAUGES_FOR_CACHING"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Lf/h/c/y/m/n;)Ljava/lang/String;
    .locals 7

    invoke-virtual {p0}, Lf/h/c/y/m/n;->f0()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/c/y/m/n;->W()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lf/h/c/y/m/n;->b0()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lf/h/c/y/m/n;->R()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, "UNKNOWN"

    :goto_1
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lf/h/c/y/m/n;->Y()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v4, v5

    const/4 p0, 0x1

    aput-object v2, v4, p0

    const/4 p0, 0x2

    long-to-double v0, v0

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, p0

    const-string p0, "network request trace: %s (responseCode: %s, responseTime: %.4fms)"

    invoke-static {v3, p0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static b(Lf/h/c/y/m/p;)Ljava/lang/String;
    .locals 4

    invoke-interface {p0}, Lf/h/c/y/m/p;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lf/h/c/y/m/p;->j()Lf/h/c/y/m/t;

    move-result-object p0

    invoke-static {p0}, Lf/h/c/y/k/l;->c(Lf/h/c/y/m/t;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-interface {p0}, Lf/h/c/y/m/p;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Lf/h/c/y/m/p;->m()Lf/h/c/y/m/n;

    move-result-object p0

    invoke-static {p0}, Lf/h/c/y/k/l;->a(Lf/h/c/y/m/n;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-interface {p0}, Lf/h/c/y/m/p;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Lf/h/c/y/m/p;->o()Lf/h/c/y/m/h;

    move-result-object p0

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lf/h/c/y/m/h;->J()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p0}, Lf/h/c/y/m/h;->G()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {p0}, Lf/h/c/y/m/h;->F()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 v2, 0x2

    aput-object p0, v1, v2

    const-string p0, "gauges (hasMetadata: %b, cpuGaugeCount: %d, memoryGaugeCount: %d)"

    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string p0, "log"

    return-object p0
.end method

.method public static c(Lf/h/c/y/m/t;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p0}, Lf/h/c/y/m/t;->O()J

    move-result-wide v0

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lf/h/c/y/m/t;->P()Ljava/lang/String;

    move-result-object p0

    const/4 v4, 0x0

    aput-object p0, v3, v4

    long-to-double v0, v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    const/4 v0, 0x1

    aput-object p0, v3, v0

    const-string p0, "trace metric: %s (duration: %.4fms)"

    invoke-static {v2, p0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public d()Z
    .locals 1

    iget-object v0, p0, Lf/h/c/y/k/l;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final e(Lf/h/c/y/m/o$b;Lf/h/c/y/m/d;)V
    .locals 12
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/c/y/k/l;->d()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, Lf/h/c/y/k/l;->r:Ljava/util/Map;

    const-string v3, "KEY_AVAILABLE_TRACES_FOR_CACHING"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lf/h/c/y/k/l;->r:Ljava/util/Map;

    const-string v5, "KEY_AVAILABLE_NETWORK_REQUESTS_FOR_CACHING"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v6, p0, Lf/h/c/y/k/l;->r:Ljava/util/Map;

    const-string v7, "KEY_AVAILABLE_GAUGES_FOR_CACHING"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p1}, Lf/h/c/y/m/o$b;->i()Z

    move-result v8

    if-eqz v8, :cond_0

    if-lez v0, :cond_0

    iget-object v4, p0, Lf/h/c/y/k/l;->r:Ljava/util/Map;

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lf/h/c/y/m/o$b;->l()Z

    move-result v3

    if-eqz v3, :cond_1

    if-lez v4, :cond_1

    iget-object v0, p0, Lf/h/c/y/k/l;->r:Ljava/util/Map;

    sub-int/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lf/h/c/y/m/o$b;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    if-lez v6, :cond_2

    iget-object v0, p0, Lf/h/c/y/k/l;->r:Ljava/util/Map;

    sub-int/2addr v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-object v3, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/y/k/l;->b(Lf/h/c/y/m/p;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v0, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v0

    const/4 v0, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v0

    const-string v0, "%s is not allowed to cache. Cache exhausted the limit (availableTracesForCaching: %d, availableNetworkRequestsForCaching: %d, availableGaugesForCaching: %d)."

    invoke-virtual {v3, v0, v5}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    sget-object v0, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const-string v3, "Transport is not initialized yet, %s will be queued for to be dispatched later"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/y/k/l;->b(Lf/h/c/y/m/p;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lf/h/c/y/k/l;->s:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lf/h/c/y/k/d;

    invoke-direct {v1, p1, p2}, Lf/h/c/y/k/d;-><init>(Lf/h/c/y/m/o$b;Lf/h/c/y/m/d;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lf/h/c/y/k/l;->m:Lf/h/c/y/d/a;

    invoke-virtual {v0}, Lf/h/c/y/d/a;->p()Z

    move-result v0

    const/4 v3, 0x0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lf/h/c/y/k/l;->k:Lf/h/c/y/m/c$b;

    iget-object v0, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v0, Lf/h/c/y/m/c;

    invoke-virtual {v0}, Lf/h/c/y/m/c;->K()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lf/h/c/y/k/l;->q:Z

    if-nez v0, :cond_5

    goto :goto_4

    :cond_5
    :try_start_0
    iget-object v0, p0, Lf/h/c/y/k/l;->f:Lf/h/c/v/g;

    invoke-interface {v0}, Lf/h/c/v/g;->getId()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    const-wide/32 v4, 0xea60

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v4, v5, v6}, Lf/h/a/f/f/n/g;->d(Lcom/google/android/gms/tasks/Task;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    sget-object v4, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    const-string v0, "Task to retrieve Installation Id is timed out: %s"

    invoke-virtual {v4, v0, v5}, Lf/h/c/y/h/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :catch_1
    move-exception v0

    sget-object v4, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    const-string v0, "Task to retrieve Installation Id is interrupted: %s"

    invoke-virtual {v4, v0, v5}, Lf/h/c/y/h/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :catch_2
    move-exception v0

    sget-object v4, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    const-string v0, "Unable to retrieve Installation Id: %s"

    invoke-virtual {v4, v0, v5}, Lf/h/c/y/h/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    move-object v0, v3

    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lf/h/c/y/k/l;->k:Lf/h/c/y/m/c$b;

    invoke-virtual {v4}, Lf/h/e/r$a;->r()V

    iget-object v4, v4, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v4, Lf/h/c/y/m/c;

    invoke-static {v4, v0}, Lf/h/c/y/m/c;->E(Lf/h/c/y/m/c;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    sget-object v0, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    new-array v4, v2, [Ljava/lang/Object;

    const-string v5, "Firebase Installation Id is empty, contact Firebase Support for debugging."

    invoke-virtual {v0, v5, v4}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    :goto_4
    iget-object v0, p0, Lf/h/c/y/k/l;->k:Lf/h/c/y/m/c$b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v4, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v4, Lf/h/c/y/m/c;

    invoke-static {v4, p2}, Lf/h/c/y/m/c;->C(Lf/h/c/y/m/c;Lf/h/c/y/m/d;)V

    invoke-virtual {p1}, Lf/h/c/y/m/o$b;->i()Z

    move-result p2

    if-eqz p2, :cond_a

    iget-object p2, v0, Lf/h/e/r$a;->d:Lf/h/e/r;

    invoke-virtual {p2}, Lf/h/e/r;->y()Lf/h/e/r$a;

    move-result-object p2

    invoke-virtual {v0}, Lf/h/e/r$a;->q()Lf/h/e/r;

    move-result-object v0

    invoke-virtual {p2, v0}, Lf/h/e/r$a;->s(Lf/h/e/r;)Lf/h/e/r$a;

    move-object v0, p2

    check-cast v0, Lf/h/c/y/m/c$b;

    iget-object p2, p0, Lf/h/c/y/k/l;->e:Lf/h/c/y/c;

    if-nez p2, :cond_8

    invoke-virtual {p0}, Lf/h/c/y/k/l;->d()Z

    move-result p2

    if-eqz p2, :cond_8

    sget p2, Lf/h/c/y/c;->d:I

    invoke-static {}, Lf/h/c/c;->b()Lf/h/c/c;

    move-result-object p2

    const-class v4, Lf/h/c/y/c;

    invoke-virtual {p2}, Lf/h/c/c;->a()V

    iget-object p2, p2, Lf/h/c/c;->d:Lf/h/c/m/k;

    invoke-virtual {p2, v4}, Lf/h/c/m/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/c/y/c;

    iput-object p2, p0, Lf/h/c/y/k/l;->e:Lf/h/c/y/c;

    :cond_8
    iget-object p2, p0, Lf/h/c/y/k/l;->e:Lf/h/c/y/c;

    if-eqz p2, :cond_9

    new-instance v4, Ljava/util/HashMap;

    iget-object p2, p2, Lf/h/c/y/c;->a:Ljava/util/Map;

    invoke-direct {v4, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_5

    :cond_9
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v4

    :goto_5
    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object p2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast p2, Lf/h/c/y/m/c;

    invoke-static {p2}, Lf/h/c/y/m/c;->D(Lf/h/c/y/m/c;)Ljava/util/Map;

    move-result-object p2

    check-cast p2, Lf/h/e/e0;

    invoke-virtual {p2, v4}, Lf/h/e/e0;->putAll(Ljava/util/Map;)V

    :cond_a
    invoke-virtual {p1}, Lf/h/e/r$a;->r()V

    iget-object p2, p1, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast p2, Lf/h/c/y/m/o;

    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/c;

    invoke-static {p2, v0}, Lf/h/c/y/m/o;->B(Lf/h/c/y/m/o;Lf/h/c/y/m/c;)V

    invoke-virtual {p1}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object p1

    check-cast p1, Lf/h/c/y/m/o;

    iget-object p2, p0, Lf/h/c/y/k/l;->m:Lf/h/c/y/d/a;

    invoke-virtual {p2}, Lf/h/c/y/d/a;->p()Z

    move-result p2

    if-nez p2, :cond_c

    sget-object p2, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const-string v0, "Performance collection is not enabled, dropping %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/y/k/l;->b(Lf/h/c/y/m/p;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p2, v0, v4}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_b
    :goto_6
    const/4 p2, 0x0

    goto/16 :goto_12

    :cond_c
    invoke-virtual {p1}, Lf/h/c/y/m/o;->F()Lf/h/c/y/m/c;

    move-result-object p2

    invoke-virtual {p2}, Lf/h/c/y/m/c;->K()Z

    move-result p2

    if-nez p2, :cond_d

    sget-object p2, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const-string v0, "App Instance ID is null or empty, dropping %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/y/k/l;->b(Lf/h/c/y/m/p;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p2, v0, v4}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    :cond_d
    iget-object p2, p0, Lf/h/c/y/k/l;->l:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lf/h/c/y/m/o;->i()Z

    move-result v4

    if-eqz v4, :cond_e

    new-instance v4, Lf/h/c/y/g/f;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->j()Lf/h/c/y/m/t;

    move-result-object v5

    invoke-direct {v4, v5}, Lf/h/c/y/g/f;-><init>(Lf/h/c/y/m/t;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    invoke-virtual {p1}, Lf/h/c/y/m/o;->l()Z

    move-result v4

    if-eqz v4, :cond_f

    new-instance v4, Lf/h/c/y/g/e;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->m()Lf/h/c/y/m/n;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lf/h/c/y/g/e;-><init>(Lf/h/c/y/m/n;Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_f
    invoke-virtual {p1}, Lf/h/c/y/m/o;->G()Z

    move-result p2

    if-eqz p2, :cond_10

    new-instance p2, Lf/h/c/y/g/c;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->F()Lf/h/c/y/m/c;

    move-result-object v4

    invoke-direct {p2, v4}, Lf/h/c/y/g/c;-><init>(Lf/h/c/y/m/c;)V

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_10
    invoke-virtual {p1}, Lf/h/c/y/m/o;->e()Z

    move-result p2

    if-eqz p2, :cond_11

    new-instance p2, Lf/h/c/y/g/d;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->o()Lf/h/c/y/m/h;

    move-result-object v4

    invoke-direct {p2, v4}, Lf/h/c/y/g/d;-><init>(Lf/h/c/y/m/h;)V

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_11
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_12

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object p2

    new-array v0, v2, [Ljava/lang/Object;

    const-string v4, "No validators found for PerfMetric."

    invoke-virtual {p2, v4, v0}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_7

    :cond_12
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_13
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/y/g/j;

    invoke-virtual {v0}, Lf/h/c/y/g/j;->a()Z

    move-result v0

    if-nez v0, :cond_13

    :goto_7
    const/4 p2, 0x0

    goto :goto_8

    :cond_14
    const/4 p2, 0x1

    :goto_8
    if-nez p2, :cond_15

    sget-object p2, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const-string v0, "Unable to process the PerfMetric (%s) due to missing or invalid values. See earlier log statements for additional information on the specific missing/invalid values."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/y/k/l;->b(Lf/h/c/y/m/p;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p2, v0, v4}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_6

    :cond_15
    iget-object p2, p0, Lf/h/c/y/k/l;->n:Lf/h/c/y/k/e;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->i()Z

    move-result v0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    if-eqz v0, :cond_1a

    iget-object v0, p2, Lf/h/c/y/k/e;->e:Lf/h/c/y/d/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v5, Lf/h/c/y/d/s;

    monitor-enter v5

    :try_start_1
    sget-object v6, Lf/h/c/y/d/s;->a:Lf/h/c/y/d/s;

    if-nez v6, :cond_16

    new-instance v6, Lf/h/c/y/d/s;

    invoke-direct {v6}, Lf/h/c/y/d/s;-><init>()V

    sput-object v6, Lf/h/c/y/d/s;->a:Lf/h/c/y/d/s;

    :cond_16
    sget-object v6, Lf/h/c/y/d/s;->a:Lf/h/c/y/d/s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v5

    invoke-virtual {v0, v6}, Lf/h/c/y/d/a;->k(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_17

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v0, v7}, Lf/h/c/y/d/a;->q(F)Z

    move-result v7

    if-eqz v7, :cond_17

    iget-object v0, v0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "com.google.firebase.perf.TraceSamplingRate"

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v0, v6, v7}, Lf/h/c/y/d/v;->c(Ljava/lang/String;F)Z

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_9

    :cond_17
    invoke-virtual {v0, v6}, Lf/h/c/y/d/a;->c(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_18

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v0, v7}, Lf/h/c/y/d/a;->q(F)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_9

    :cond_18
    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_9
    iget v5, p2, Lf/h/c/y/k/e;->a:F

    cmpg-float v0, v5, v0

    if-gez v0, :cond_19

    const/4 v0, 0x1

    goto :goto_a

    :cond_19
    const/4 v0, 0x0

    :goto_a
    if-nez v0, :cond_1a

    invoke-virtual {p1}, Lf/h/c/y/m/o;->j()Lf/h/c/y/m/t;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/t;->Q()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, v0}, Lf/h/c/y/k/e;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1a

    goto/16 :goto_f

    :catchall_0
    move-exception p1

    monitor-exit v5

    throw p1

    :cond_1a
    invoke-virtual {p1}, Lf/h/c/y/m/o;->l()Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p2, Lf/h/c/y/k/e;->e:Lf/h/c/y/d/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v5, Lf/h/c/y/d/g;

    monitor-enter v5

    :try_start_2
    sget-object v6, Lf/h/c/y/d/g;->a:Lf/h/c/y/d/g;

    if-nez v6, :cond_1b

    new-instance v6, Lf/h/c/y/d/g;

    invoke-direct {v6}, Lf/h/c/y/d/g;-><init>()V

    sput-object v6, Lf/h/c/y/d/g;->a:Lf/h/c/y/d/g;

    :cond_1b
    sget-object v6, Lf/h/c/y/d/g;->a:Lf/h/c/y/d/g;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v5

    invoke-virtual {v0, v6}, Lf/h/c/y/d/a;->k(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_1c

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v0, v7}, Lf/h/c/y/d/a;->q(F)Z

    move-result v7

    if-eqz v7, :cond_1c

    iget-object v0, v0, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "com.google.firebase.perf.NetworkRequestSamplingRate"

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v0, v4, v6}, Lf/h/c/y/d/v;->c(Ljava/lang/String;F)Z

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_b

    :cond_1c
    invoke-virtual {v0, v6}, Lf/h/c/y/d/a;->c(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_1d

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v0, v7}, Lf/h/c/y/d/a;->q(F)Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_b

    :cond_1d
    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_b
    iget v4, p2, Lf/h/c/y/k/e;->a:F

    cmpg-float v0, v4, v0

    if-gez v0, :cond_1e

    const/4 v0, 0x1

    goto :goto_c

    :cond_1e
    const/4 v0, 0x0

    :goto_c
    if-nez v0, :cond_1f

    invoke-virtual {p1}, Lf/h/c/y/m/o;->m()Lf/h/c/y/m/n;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/n;->S()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, v0}, Lf/h/c/y/k/e;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1f

    goto :goto_f

    :catchall_1
    move-exception p1

    monitor-exit v5

    throw p1

    :cond_1f
    invoke-virtual {p1}, Lf/h/c/y/m/o;->i()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-virtual {p1}, Lf/h/c/y/m/o;->j()Lf/h/c/y/m/t;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/t;->P()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lf/h/c/y/l/c;->h:Lf/h/c/y/l/c;

    invoke-virtual {v4}, Lf/h/c/y/l/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    invoke-virtual {p1}, Lf/h/c/y/m/o;->j()Lf/h/c/y/m/t;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/t;->P()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lf/h/c/y/l/c;->i:Lf/h/c/y/l/c;

    invoke-virtual {v4}, Lf/h/c/y/l/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    :cond_20
    invoke-virtual {p1}, Lf/h/c/y/m/o;->j()Lf/h/c/y/m/t;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/t;->K()I

    move-result v0

    if-lez v0, :cond_21

    goto :goto_d

    :cond_21
    invoke-virtual {p1}, Lf/h/c/y/m/o;->e()Z

    move-result v0

    if-eqz v0, :cond_22

    :goto_d
    const/4 v0, 0x0

    goto :goto_e

    :cond_22
    const/4 v0, 0x1

    :goto_e
    if-nez v0, :cond_23

    const/4 p2, 0x1

    goto :goto_10

    :cond_23
    invoke-virtual {p1}, Lf/h/c/y/m/o;->l()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object p2, p2, Lf/h/c/y/k/e;->d:Lf/h/c/y/k/e$a;

    invoke-virtual {p2}, Lf/h/c/y/k/e$a;->b()Z

    move-result p2

    goto :goto_10

    :cond_24
    invoke-virtual {p1}, Lf/h/c/y/m/o;->i()Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object p2, p2, Lf/h/c/y/k/e;->c:Lf/h/c/y/k/e$a;

    invoke-virtual {p2}, Lf/h/c/y/k/e$a;->b()Z

    move-result p2

    goto :goto_10

    :cond_25
    :goto_f
    const/4 p2, 0x0

    :goto_10
    if-nez p2, :cond_29

    invoke-virtual {p1}, Lf/h/c/y/m/o;->i()Z

    move-result p2

    const-wide/16 v4, 0x1

    if-eqz p2, :cond_26

    iget-object p2, p0, Lf/h/c/y/k/l;->o:Lf/h/c/y/g/a;

    sget-object v0, Lf/h/c/y/l/b;->d:Lf/h/c/y/l/b;

    invoke-virtual {v0}, Lf/h/c/y/l/b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v4, v5}, Lf/h/c/y/g/a;->c(Ljava/lang/String;J)V

    goto :goto_11

    :cond_26
    invoke-virtual {p1}, Lf/h/c/y/m/o;->l()Z

    move-result p2

    if-eqz p2, :cond_27

    iget-object p2, p0, Lf/h/c/y/k/l;->o:Lf/h/c/y/g/a;

    sget-object v0, Lf/h/c/y/l/b;->e:Lf/h/c/y/l/b;

    invoke-virtual {v0}, Lf/h/c/y/l/b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v4, v5}, Lf/h/c/y/g/a;->c(Ljava/lang/String;J)V

    :cond_27
    :goto_11
    invoke-virtual {p1}, Lf/h/c/y/m/o;->i()Z

    move-result p2

    if-eqz p2, :cond_28

    sget-object p2, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const-string v0, "Rate Limited - %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->j()Lf/h/c/y/m/t;

    move-result-object v5

    invoke-static {v5}, Lf/h/c/y/k/l;->c(Lf/h/c/y/m/t;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p2, v0, v4}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_6

    :cond_28
    invoke-virtual {p1}, Lf/h/c/y/m/o;->l()Z

    move-result p2

    if-eqz p2, :cond_b

    sget-object p2, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const-string v0, "Rate Limited - %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->m()Lf/h/c/y/m/n;

    move-result-object v5

    invoke-static {v5}, Lf/h/c/y/k/l;->a(Lf/h/c/y/m/n;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p2, v0, v4}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_6

    :cond_29
    const/4 p2, 0x1

    :goto_12
    if-eqz p2, :cond_3a

    sget-object p2, Lf/h/c/y/k/l;->t:Lf/h/c/y/h/a;

    const-string v0, "Logging %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/y/k/l;->b(Lf/h/c/y/m/p;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p2, v0, v4}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p2, p0, Lf/h/c/y/k/l;->m:Lf/h/c/y/d/a;

    invoke-virtual {p1}, Lf/h/c/y/m/o;->F()Lf/h/c/y/m/c;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/c/y/m/c;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_31

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2a

    goto/16 :goto_14

    :cond_2a
    const-class v4, Lf/h/c/y/d/t;

    monitor-enter v4

    :try_start_3
    sget-object v5, Lf/h/c/y/d/t;->a:Lf/h/c/y/d/t;

    if-nez v5, :cond_2b

    new-instance v5, Lf/h/c/y/d/t;

    invoke-direct {v5}, Lf/h/c/y/d/t;-><init>()V

    sput-object v5, Lf/h/c/y/d/t;->a:Lf/h/c/y/d/t;

    :cond_2b
    sget-object v5, Lf/h/c/y/d/t;->a:Lf/h/c/y/d/t;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    monitor-exit v4

    invoke-virtual {p2, v5}, Lf/h/c/y/d/a;->c(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v4

    iget-object v6, p2, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    invoke-virtual {v6}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->isFirebaseRemoteConfigAvailable()Z

    move-result v6

    const/4 v7, 0x0

    if-nez v6, :cond_2c

    invoke-virtual {v4}, Lf/h/c/y/l/e;->b()Z

    move-result v5

    if-eqz v5, :cond_30

    invoke-virtual {v4}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {p2, v5}, Lf/h/c/y/d/a;->s(F)Z

    move-result p2

    if-eqz p2, :cond_30

    invoke-virtual {v4}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    goto :goto_13

    :cond_2c
    invoke-virtual {p2, v5}, Lf/h/c/y/d/a;->k(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v6

    invoke-virtual {v6}, Lf/h/c/y/l/e;->b()Z

    move-result v8

    if-eqz v8, :cond_2e

    invoke-virtual {v6}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {p2, v8}, Lf/h/c/y/d/a;->s(F)Z

    move-result v8

    if-eqz v8, :cond_2d

    iget-object p2, p2, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "com.google.firebase.perf.TransportRolloutPercentage"

    invoke-virtual {v6}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {p2, v4, v5}, Lf/h/c/y/d/v;->c(Ljava/lang/String;F)Z

    invoke-virtual {v6}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    goto :goto_13

    :cond_2d
    invoke-virtual {v4}, Lf/h/c/y/l/e;->b()Z

    move-result v5

    if-eqz v5, :cond_30

    invoke-virtual {v4}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {p2, v5}, Lf/h/c/y/d/a;->s(F)Z

    move-result p2

    if-eqz p2, :cond_30

    invoke-virtual {v4}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    goto :goto_13

    :cond_2e
    iget-object v4, p2, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    invoke-virtual {v4}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->isFirebaseRemoteConfigMapEmpty()Z

    move-result v4

    if-eqz v4, :cond_2f

    goto :goto_13

    :cond_2f
    iget-object p2, p2, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "com.google.firebase.perf.TransportRolloutPercentage"

    const/high16 v7, 0x42c80000    # 100.0f

    invoke-virtual {p2, v4, v7}, Lf/h/c/y/d/v;->c(Ljava/lang/String;F)Z

    :cond_30
    :goto_13
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result p2

    rem-int/lit8 p2, p2, 0x64

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    int-to-float p2, p2

    cmpg-float p2, p2, v7

    if-gez p2, :cond_31

    const/4 p2, 0x1

    goto :goto_15

    :catchall_2
    move-exception p1

    monitor-exit v4

    throw p1

    :cond_31
    :goto_14
    const/4 p2, 0x0

    :goto_15
    if-eqz p2, :cond_36

    iget-object p2, p0, Lf/h/c/y/k/l;->i:Lf/h/c/y/k/c;

    iget-object v0, p2, Lf/h/c/y/k/c;->c:Lf/h/a/b/f;

    if-nez v0, :cond_33

    iget-object v0, p2, Lf/h/c/y/k/c;->b:Lf/h/c/u/a;

    invoke-interface {v0}, Lf/h/c/u/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/b/g;

    if-eqz v0, :cond_32

    iget-object v4, p2, Lf/h/c/y/k/c;->a:Ljava/lang/String;

    const-class v5, Lf/h/c/y/m/o;

    new-instance v6, Lf/h/a/b/b;

    const-string v7, "proto"

    invoke-direct {v6, v7}, Lf/h/a/b/b;-><init>(Ljava/lang/String;)V

    sget-object v7, Lf/h/c/y/k/b;->a:Lf/h/c/y/k/b;

    invoke-interface {v0, v4, v5, v6, v7}, Lf/h/a/b/g;->a(Ljava/lang/String;Ljava/lang/Class;Lf/h/a/b/b;Lf/h/a/b/e;)Lf/h/a/b/f;

    move-result-object v0

    iput-object v0, p2, Lf/h/c/y/k/c;->c:Lf/h/a/b/f;

    goto :goto_16

    :cond_32
    sget-object v0, Lf/h/c/y/k/c;->d:Lf/h/c/y/h/a;

    new-array v4, v2, [Ljava/lang/Object;

    const-string v5, "Flg TransportFactory is not available at the moment"

    invoke-virtual {v0, v5, v4}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_33
    :goto_16
    iget-object p2, p2, Lf/h/c/y/k/c;->c:Lf/h/a/b/f;

    if-eqz p2, :cond_34

    goto :goto_17

    :cond_34
    const/4 v1, 0x0

    :goto_17
    if-nez v1, :cond_35

    sget-object p1, Lf/h/c/y/k/c;->d:Lf/h/c/y/h/a;

    new-array p2, v2, [Ljava/lang/Object;

    const-string v0, "Unable to dispatch event because Flg Transport is not available"

    invoke-virtual {p1, v0, p2}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1a

    :cond_35
    new-instance v0, Lf/h/a/b/a;

    sget-object v1, Lf/h/a/b/d;->d:Lf/h/a/b/d;

    invoke-direct {v0, v3, p1, v1}, Lf/h/a/b/a;-><init>(Ljava/lang/Integer;Ljava/lang/Object;Lf/h/a/b/d;)V

    invoke-interface {p2, v0}, Lf/h/a/b/f;->a(Lf/h/a/b/c;)V

    sget-object p1, Lf/h/c/y/k/c;->d:Lf/h/c/y/h/a;

    new-array p2, v2, [Ljava/lang/Object;

    const-string v0, "Event is dispatched via Flg Transport"

    invoke-virtual {p1, v0, p2}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1a

    :cond_36
    iget-object p2, p0, Lf/h/c/y/k/l;->h:Lf/h/c/y/k/a;

    iget-object v0, p2, Lf/h/c/y/k/a;->c:Lf/h/a/f/d/a;

    if-nez v0, :cond_37

    :try_start_4
    iget-object v5, p2, Lf/h/c/y/k/a;->a:Landroid/content/Context;

    iget-object v6, p2, Lf/h/c/y/k/a;->b:Ljava/lang/String;

    new-instance v0, Lf/h/a/f/d/a;

    new-instance v9, Lf/h/a/f/i/c/l2;

    invoke-direct {v9, v5}, Lf/h/a/f/i/c/l2;-><init>(Landroid/content/Context;)V

    sget-object v10, Lf/h/a/f/f/n/d;->a:Lf/h/a/f/f/n/d;

    new-instance v11, Lf/h/a/f/i/c/z4;

    invoke-direct {v11, v5}, Lf/h/a/f/i/c/z4;-><init>(Landroid/content/Context;)V

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v4, v0

    invoke-direct/range {v4 .. v11}, Lf/h/a/f/d/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLf/h/a/f/d/c;Lf/h/a/f/f/n/c;Lf/h/a/f/d/a$b;)V

    iput-object v0, p2, Lf/h/c/y/k/a;->c:Lf/h/a/f/d/a;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_18

    :catch_3
    move-exception v0

    sget-object v4, Lf/h/c/y/k/a;->d:Lf/h/c/y/h/a;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    const-string v0, "Init Cct Logger failed with exception: %s"

    invoke-virtual {v4, v0, v5}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_37
    :goto_18
    iget-object p2, p2, Lf/h/c/y/k/a;->c:Lf/h/a/f/d/a;

    if-eqz p2, :cond_38

    const/4 v0, 0x1

    goto :goto_19

    :cond_38
    const/4 v0, 0x0

    :goto_19
    if-nez v0, :cond_39

    sget-object p1, Lf/h/c/y/k/a;->d:Lf/h/c/y/h/a;

    new-array p2, v2, [Ljava/lang/Object;

    const-string v0, "Unable to dispatch event because Cct Logger is not available"

    invoke-virtual {p1, v0, p2}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1a

    :cond_39
    :try_start_5
    invoke-virtual {p1}, Lf/h/e/a;->r()[B

    move-result-object p1

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/d/a$a;

    invoke-direct {v0, p2, p1, v3}, Lf/h/a/f/d/a$a;-><init>(Lf/h/a/f/d/a;[BLf/h/a/f/d/b;)V

    invoke-virtual {v0}, Lf/h/a/f/d/a$a;->a()V

    sget-object p1, Lf/h/c/y/k/a;->d:Lf/h/c/y/h/a;

    const-string p2, "Event is dispatched via Cct Transport"

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v0}, Lf/h/c/y/h/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1a

    :catch_4
    move-exception p1

    sget-object p2, Lf/h/c/y/k/a;->d:Lf/h/c/y/h/a;

    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "Dispatch with Cct Logger failed with exception: %s"

    invoke-virtual {p2, p1, v0}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1a
    invoke-static {}, Lcom/google/firebase/perf/internal/SessionManager;->getInstance()Lcom/google/firebase/perf/internal/SessionManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/perf/internal/SessionManager;->updatePerfSessionIfExpired()Z

    :cond_3a
    return-void
.end method

.method public onUpdateAppState(Lf/h/c/y/m/d;)V
    .locals 1

    sget-object v0, Lf/h/c/y/m/d;->e:Lf/h/c/y/m/d;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lf/h/c/y/k/l;->q:Z

    invoke-virtual {p0}, Lf/h/c/y/k/l;->d()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lf/h/c/y/k/h;

    invoke-direct {v0, p0}, Lf/h/c/y/k/h;-><init>(Lf/h/c/y/k/l;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method
