.class public final Lf/h/c/y/k/e;
.super Ljava/lang/Object;
.source "RateLimiter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/c/y/k/e$a;
    }
.end annotation


# instance fields
.field public final a:F

.field public b:Z

.field public c:Lf/h/c/y/k/e$a;

.field public d:Lf/h/c/y/k/e$a;

.field public final e:Lf/h/c/y/d/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;DJ)V
    .locals 13
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object v0, p0

    new-instance v10, Lf/h/c/y/l/a;

    invoke-direct {v10}, Lf/h/c/y/l/a;-><init>()V

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    invoke-static {}, Lf/h/c/y/d/a;->f()Lf/h/c/y/d/a;

    move-result-object v11

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    iput-boolean v2, v0, Lf/h/c/y/k/e;->b:Z

    const/4 v3, 0x0

    iput-object v3, v0, Lf/h/c/y/k/e;->c:Lf/h/c/y/k/e$a;

    iput-object v3, v0, Lf/h/c/y/k/e;->d:Lf/h/c/y/k/e$a;

    const/4 v3, 0x0

    cmpg-float v3, v3, v1

    if-gtz v3, :cond_0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v1, v3

    if-gez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    if-eqz v2, :cond_1

    iput v1, v0, Lf/h/c/y/k/e;->a:F

    iput-object v11, v0, Lf/h/c/y/k/e;->e:Lf/h/c/y/d/a;

    new-instance v12, Lf/h/c/y/k/e$a;

    iget-boolean v9, v0, Lf/h/c/y/k/e;->b:Z

    const-string v8, "Trace"

    move-object v1, v12

    move-wide v2, p2

    move-wide/from16 v4, p4

    move-object v6, v10

    move-object v7, v11

    invoke-direct/range {v1 .. v9}, Lf/h/c/y/k/e$a;-><init>(DJLf/h/c/y/l/a;Lf/h/c/y/d/a;Ljava/lang/String;Z)V

    iput-object v12, v0, Lf/h/c/y/k/e;->c:Lf/h/c/y/k/e$a;

    new-instance v12, Lf/h/c/y/k/e$a;

    iget-boolean v9, v0, Lf/h/c/y/k/e;->b:Z

    const-string v8, "Network"

    move-object v1, v12

    invoke-direct/range {v1 .. v9}, Lf/h/c/y/k/e$a;-><init>(DJLf/h/c/y/l/a;Lf/h/c/y/d/a;Ljava/lang/String;Z)V

    iput-object v12, v0, Lf/h/c/y/k/e;->d:Lf/h/c/y/k/e$a;

    invoke-static {p1}, Lf/h/c/y/l/g;->a(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, v0, Lf/h/c/y/k/e;->b:Z

    return-void

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Sampling bucket ID should be in range [0.0f, 1.0f)."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/c/y/m/q;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/q;

    invoke-virtual {v0}, Lf/h/c/y/m/q;->E()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/c/y/m/q;

    invoke-virtual {p1, v1}, Lf/h/c/y/m/q;->D(I)Lf/h/c/y/m/s;

    move-result-object p1

    sget-object v0, Lf/h/c/y/m/s;->e:Lf/h/c/y/m/s;

    if-ne p1, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method
