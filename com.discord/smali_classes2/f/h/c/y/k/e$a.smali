.class public Lf/h/c/y/k/e$a;
.super Ljava/lang/Object;
.source "RateLimiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/c/y/k/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# static fields
.field public static final k:Lf/h/c/y/h/a;

.field public static final l:J


# instance fields
.field public a:J

.field public b:D

.field public c:Lcom/google/firebase/perf/util/Timer;

.field public d:J

.field public final e:Lf/h/c/y/l/a;

.field public f:D

.field public g:J

.field public h:D

.field public i:J

.field public final j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lf/h/c/y/k/e$a;->k:Lf/h/c/y/h/a;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    sput-wide v0, Lf/h/c/y/k/e$a;->l:J

    return-void
.end method

.method public constructor <init>(DJLf/h/c/y/l/a;Lf/h/c/y/d/a;Ljava/lang/String;Z)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lf/h/c/y/k/e$a;->e:Lf/h/c/y/l/a;

    iput-wide p3, p0, Lf/h/c/y/k/e$a;->a:J

    iput-wide p1, p0, Lf/h/c/y/k/e$a;->b:D

    iput-wide p3, p0, Lf/h/c/y/k/e$a;->d:J

    invoke-static {p5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {p1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    iput-object p1, p0, Lf/h/c/y/k/e$a;->c:Lcom/google/firebase/perf/util/Timer;

    const-string p1, "Trace"

    if-ne p7, p1, :cond_0

    invoke-virtual {p6}, Lf/h/c/y/d/a;->j()J

    move-result-wide p1

    goto :goto_0

    :cond_0
    invoke-virtual {p6}, Lf/h/c/y/d/a;->j()J

    move-result-wide p1

    :goto_0
    const-string p3, "Trace"

    if-ne p7, p3, :cond_4

    invoke-static {p6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class p3, Lf/h/c/y/d/r;

    monitor-enter p3

    :try_start_0
    sget-object p4, Lf/h/c/y/d/r;->a:Lf/h/c/y/d/r;

    if-nez p4, :cond_1

    new-instance p4, Lf/h/c/y/d/r;

    invoke-direct {p4}, Lf/h/c/y/d/r;-><init>()V

    sput-object p4, Lf/h/c/y/d/r;->a:Lf/h/c/y/d/r;

    :cond_1
    sget-object p4, Lf/h/c/y/d/r;->a:Lf/h/c/y/d/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p3

    invoke-virtual {p6, p4}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p3

    invoke-virtual {p3}, Lf/h/c/y/l/e;->b()Z

    move-result p5

    if-eqz p5, :cond_2

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p6, v0, v1}, Lf/h/c/y/d/a;->m(J)Z

    move-result p5

    if-eqz p5, :cond_2

    iget-object p5, p6, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p4, "com.google.firebase.perf.TraceEventCountForeground"

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0, p5, p4, p3}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    goto/16 :goto_1

    :cond_2
    invoke-virtual {p6, p4}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p3

    invoke-virtual {p3}, Lf/h/c/y/l/e;->b()Z

    move-result p5

    if-eqz p5, :cond_3

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p6, v0, v1}, Lf/h/c/y/d/a;->m(J)Z

    move-result p5

    if-eqz p5, :cond_3

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    goto/16 :goto_1

    :cond_3
    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 p3, 0x12c

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    goto/16 :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p3

    throw p1

    :cond_4
    invoke-static {p6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class p3, Lf/h/c/y/d/f;

    monitor-enter p3

    :try_start_1
    sget-object p4, Lf/h/c/y/d/f;->a:Lf/h/c/y/d/f;

    if-nez p4, :cond_5

    new-instance p4, Lf/h/c/y/d/f;

    invoke-direct {p4}, Lf/h/c/y/d/f;-><init>()V

    sput-object p4, Lf/h/c/y/d/f;->a:Lf/h/c/y/d/f;

    :cond_5
    sget-object p4, Lf/h/c/y/d/f;->a:Lf/h/c/y/d/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    monitor-exit p3

    invoke-virtual {p6, p4}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p3

    invoke-virtual {p3}, Lf/h/c/y/l/e;->b()Z

    move-result p5

    if-eqz p5, :cond_6

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p6, v0, v1}, Lf/h/c/y/d/a;->m(J)Z

    move-result p5

    if-eqz p5, :cond_6

    iget-object p5, p6, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p4, "com.google.firebase.perf.NetworkEventCountForeground"

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0, p5, p4, p3}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    goto :goto_1

    :cond_6
    invoke-virtual {p6, p4}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p3

    invoke-virtual {p3}, Lf/h/c/y/l/e;->b()Z

    move-result p5

    if-eqz p5, :cond_7

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p6, v0, v1}, Lf/h/c/y/d/a;->m(J)Z

    move-result p5

    if-eqz p5, :cond_7

    invoke-virtual {p3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    goto :goto_1

    :cond_7
    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 p3, 0x2bc

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    :goto_1
    long-to-double v0, p3

    long-to-double p1, p1

    div-double/2addr v0, p1

    iput-wide v0, p0, Lf/h/c/y/k/e$a;->f:D

    iput-wide p3, p0, Lf/h/c/y/k/e$a;->g:J

    const/4 p1, 0x2

    const/4 p2, 0x1

    const/4 p3, 0x3

    const/4 p4, 0x0

    if-eqz p8, :cond_8

    sget-object p5, Lf/h/c/y/k/e$a;->k:Lf/h/c/y/h/a;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "Foreground %s logging rate:%f, burst capacity:%d"

    new-array v4, p3, [Ljava/lang/Object;

    aput-object p7, v4, p4

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, p2

    iget-wide v0, p0, Lf/h/c/y/k/e$a;->g:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, p1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, p4, [Ljava/lang/Object;

    invoke-virtual {p5, v0, v1}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    const-string p5, "Trace"

    if-ne p7, p5, :cond_9

    invoke-virtual {p6}, Lf/h/c/y/d/a;->j()J

    move-result-wide v0

    goto :goto_2

    :cond_9
    invoke-virtual {p6}, Lf/h/c/y/d/a;->j()J

    move-result-wide v0

    :goto_2
    const-string p5, "Trace"

    if-ne p7, p5, :cond_d

    const-class p5, Lf/h/c/y/d/q;

    monitor-enter p5

    :try_start_2
    sget-object v2, Lf/h/c/y/d/q;->a:Lf/h/c/y/d/q;

    if-nez v2, :cond_a

    new-instance v2, Lf/h/c/y/d/q;

    invoke-direct {v2}, Lf/h/c/y/d/q;-><init>()V

    sput-object v2, Lf/h/c/y/d/q;->a:Lf/h/c/y/d/q;

    :cond_a
    sget-object v2, Lf/h/c/y/d/q;->a:Lf/h/c/y/d/q;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p5

    invoke-virtual {p6, v2}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p5

    invoke-virtual {p5}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p6, v3, v4}, Lf/h/c/y/d/a;->m(J)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object p6, p6, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "com.google.firebase.perf.TraceEventCountBackground"

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-static {v3, p6, v2, p5}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    goto/16 :goto_3

    :cond_b
    invoke-virtual {p6, v2}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p5

    invoke-virtual {p5}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p6, v3, v4}, Lf/h/c/y/d/a;->m(J)Z

    move-result p6

    if-eqz p6, :cond_c

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    goto/16 :goto_3

    :cond_c
    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 p5, 0x1e

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    goto/16 :goto_3

    :catchall_1
    move-exception p1

    monitor-exit p5

    throw p1

    :cond_d
    const-class p5, Lf/h/c/y/d/e;

    monitor-enter p5

    :try_start_3
    sget-object v2, Lf/h/c/y/d/e;->a:Lf/h/c/y/d/e;

    if-nez v2, :cond_e

    new-instance v2, Lf/h/c/y/d/e;

    invoke-direct {v2}, Lf/h/c/y/d/e;-><init>()V

    sput-object v2, Lf/h/c/y/d/e;->a:Lf/h/c/y/d/e;

    :cond_e
    sget-object v2, Lf/h/c/y/d/e;->a:Lf/h/c/y/d/e;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    monitor-exit p5

    invoke-virtual {p6, v2}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p5

    invoke-virtual {p5}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p6, v3, v4}, Lf/h/c/y/d/a;->m(J)Z

    move-result v3

    if-eqz v3, :cond_f

    iget-object p6, p6, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "com.google.firebase.perf.NetworkEventCountBackground"

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-static {v3, p6, v2, p5}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    goto :goto_3

    :cond_f
    invoke-virtual {p6, v2}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object p5

    invoke-virtual {p5}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p6, v3, v4}, Lf/h/c/y/d/a;->m(J)Z

    move-result p6

    if-eqz p6, :cond_10

    invoke-virtual {p5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    goto :goto_3

    :cond_10
    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 p5, 0x46

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    :goto_3
    long-to-double v2, p5

    long-to-double v0, v0

    div-double/2addr v2, v0

    iput-wide v2, p0, Lf/h/c/y/k/e$a;->h:D

    iput-wide p5, p0, Lf/h/c/y/k/e$a;->i:J

    if-eqz p8, :cond_11

    sget-object p5, Lf/h/c/y/k/e$a;->k:Lf/h/c/y/h/a;

    sget-object p6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v0, "Background %s logging rate:%f, capacity:%d"

    new-array p3, p3, [Ljava/lang/Object;

    aput-object p7, p3, p4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p7

    aput-object p7, p3, p2

    iget-wide v1, p0, Lf/h/c/y/k/e$a;->i:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p3, p1

    invoke-static {p6, v0, p3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array p2, p4, [Ljava/lang/Object;

    invoke-virtual {p5, p1, p2}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_11
    iput-boolean p8, p0, Lf/h/c/y/k/e$a;->j:Z

    return-void

    :catchall_2
    move-exception p1

    monitor-exit p5

    throw p1

    :catchall_3
    move-exception p1

    monitor-exit p3

    throw p1
.end method


# virtual methods
.method public declared-synchronized a(Z)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-wide v0, p0, Lf/h/c/y/k/e$a;->f:D

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lf/h/c/y/k/e$a;->h:D

    :goto_0
    iput-wide v0, p0, Lf/h/c/y/k/e$a;->b:D

    if-eqz p1, :cond_1

    iget-wide v0, p0, Lf/h/c/y/k/e$a;->g:J

    goto :goto_1

    :cond_1
    iget-wide v0, p0, Lf/h/c/y/k/e$a;->i:J

    :goto_1
    iput-wide v0, p0, Lf/h/c/y/k/e$a;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized b()Z
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/c/y/k/e$a;->e:Lf/h/c/y/l/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v0}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    iget-object v1, p0, Lf/h/c/y/k/e$a;->c:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v1, v0}, Lcom/google/firebase/perf/util/Timer;->b(Lcom/google/firebase/perf/util/Timer;)J

    move-result-wide v1

    long-to-double v1, v1

    iget-wide v3, p0, Lf/h/c/y/k/e$a;->b:D

    mul-double v1, v1, v3

    sget-wide v3, Lf/h/c/y/k/e$a;->l:J

    long-to-double v3, v3

    div-double/2addr v1, v3

    double-to-long v1, v1

    const-wide/16 v3, 0x0

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iget-wide v5, p0, Lf/h/c/y/k/e$a;->d:J

    add-long/2addr v5, v1

    iget-wide v1, p0, Lf/h/c/y/k/e$a;->a:J

    invoke-static {v5, v6, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lf/h/c/y/k/e$a;->d:J

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lf/h/c/y/k/e$a;->d:J

    iput-object v0, p0, Lf/h/c/y/k/e$a;->c:Lcom/google/firebase/perf/util/Timer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lf/h/c/y/k/e$a;->j:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    sget-object v0, Lf/h/c/y/k/e$a;->k:Lf/h/c/y/h/a;

    const-string v2, "Exceeded log rate limit, dropping the log."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
