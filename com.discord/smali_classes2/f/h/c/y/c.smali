.class public Lf/h/c/y/c;
.super Ljava/lang/Object;
.source "FirebasePerformance.java"


# static fields
.field public static final synthetic d:I


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/h/c/y/d/a;

.field public final c:Lf/h/c/y/l/d;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    return-void
.end method

.method public constructor <init>(Lf/h/c/c;Lf/h/c/u/a;Lf/h/c/v/g;Lf/h/c/u/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/c;",
            "Lf/h/c/u/a<",
            "Lf/h/c/a0/i;",
            ">;",
            "Lf/h/c/v/g;",
            "Lf/h/c/u/a<",
            "Lf/h/a/b/g;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getInstance()Lcom/google/firebase/perf/internal/RemoteConfigManager;

    move-result-object v0

    invoke-static {}, Lf/h/c/y/d/a;->f()Lf/h/c/y/d/a;

    move-result-object v1

    invoke-static {}, Lcom/google/firebase/perf/internal/GaugeManager;->getInstance()Lcom/google/firebase/perf/internal/GaugeManager;

    move-result-object v2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lf/h/c/y/c;->a:Ljava/util/Map;

    if-nez p1, :cond_0

    iput-object v1, p0, Lf/h/c/y/c;->b:Lf/h/c/y/d/a;

    new-instance p1, Lf/h/c/y/l/d;

    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p1, p2}, Lf/h/c/y/l/d;-><init>(Landroid/os/Bundle;)V

    iput-object p1, p0, Lf/h/c/y/c;->c:Lf/h/c/y/l/d;

    goto :goto_3

    :cond_0
    sget-object v3, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    iput-object p1, v3, Lf/h/c/y/k/l;->d:Lf/h/c/c;

    iput-object p3, v3, Lf/h/c/y/k/l;->f:Lf/h/c/v/g;

    iput-object p4, v3, Lf/h/c/y/k/l;->g:Lf/h/c/u/a;

    iget-object p3, v3, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    new-instance p4, Lf/h/c/y/k/f;

    invoke-direct {p4, v3}, Lf/h/c/y/k/f;-><init>(Lf/h/c/y/k/l;)V

    invoke-interface {p3, p4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Lf/h/c/c;->a()V

    iget-object p1, p1, Lf/h/c/c;->a:Landroid/content/Context;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p3

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p4

    const/16 v3, 0x80

    invoke-virtual {p3, p4, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p3

    iget-object p3, p3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p3

    goto :goto_0

    :catch_1
    move-exception p3

    :goto_0
    const-string p4, "No perf enable meta data found "

    invoke-static {p4}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p4

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string p4, "isEnabled"

    invoke-static {p4, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p3, 0x0

    :goto_1
    new-instance p4, Lf/h/c/y/l/d;

    if-eqz p3, :cond_1

    invoke-direct {p4, p3}, Lf/h/c/y/l/d;-><init>(Landroid/os/Bundle;)V

    goto :goto_2

    :cond_1
    invoke-direct {p4}, Lf/h/c/y/l/d;-><init>()V

    :goto_2
    iput-object p4, p0, Lf/h/c/y/c;->c:Lf/h/c/y/l/d;

    invoke-virtual {v0, p2}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->setFirebaseRemoteConfigProvider(Lf/h/c/u/a;)V

    iput-object v1, p0, Lf/h/c/y/c;->b:Lf/h/c/y/d/a;

    iput-object p4, v1, Lf/h/c/y/d/a;->a:Lf/h/c/y/l/d;

    sget-object p2, Lf/h/c/y/d/a;->d:Lf/h/c/y/h/a;

    invoke-static {p1}, Lf/h/c/y/l/g;->a(Landroid/content/Context;)Z

    move-result p3

    iput-boolean p3, p2, Lf/h/c/y/h/a;->b:Z

    iget-object p2, v1, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-virtual {p2, p1}, Lf/h/c/y/d/v;->b(Landroid/content/Context;)V

    invoke-virtual {v2, p1}, Lcom/google/firebase/perf/internal/GaugeManager;->setApplicationContext(Landroid/content/Context;)V

    invoke-virtual {v1}, Lf/h/c/y/d/a;->g()Ljava/lang/Boolean;

    :goto_3
    return-void
.end method
