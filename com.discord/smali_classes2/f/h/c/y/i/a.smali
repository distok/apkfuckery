.class public Lf/h/c/y/i/a;
.super Ljava/lang/Object;
.source "TraceMetricBuilder.java"


# instance fields
.field public final a:Lcom/google/firebase/perf/metrics/Trace;


# direct methods
.method public constructor <init>(Lcom/google/firebase/perf/metrics/Trace;)V
    .locals 0
    .param p1    # Lcom/google/firebase/perf/metrics/Trace;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    return-void
.end method


# virtual methods
.method public a()Lf/h/c/y/m/t;
    .locals 6

    invoke-static {}, Lf/h/c/y/m/t;->T()Lf/h/c/y/m/t$b;

    move-result-object v0

    iget-object v1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    iget-object v1, v1, Lcom/google/firebase/perf/metrics/Trace;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lf/h/c/y/m/t$b;->B(Ljava/lang/String;)Lf/h/c/y/m/t$b;

    iget-object v1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    iget-object v1, v1, Lcom/google/firebase/perf/metrics/Trace;->m:Lcom/google/firebase/perf/util/Timer;

    iget-wide v1, v1, Lcom/google/firebase/perf/util/Timer;->d:J

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/m/t$b;->x(J)Lf/h/c/y/m/t$b;

    iget-object v1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    iget-object v2, v1, Lcom/google/firebase/perf/metrics/Trace;->m:Lcom/google/firebase/perf/util/Timer;

    iget-object v1, v1, Lcom/google/firebase/perf/metrics/Trace;->n:Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {v2, v1}, Lcom/google/firebase/perf/util/Timer;->b(Lcom/google/firebase/perf/util/Timer;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/c/y/m/t$b;->A(J)Lf/h/c/y/m/t$b;

    iget-object v1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    iget-object v1, v1, Lcom/google/firebase/perf/metrics/Trace;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/perf/metrics/Counter;

    iget-object v3, v2, Lcom/google/firebase/perf/metrics/Counter;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/firebase/perf/metrics/Counter;->a()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lf/h/c/y/m/t$b;->w(Ljava/lang/String;J)Lf/h/c/y/m/t$b;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    iget-object v1, v1, Lcom/google/firebase/perf/metrics/Trace;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/perf/metrics/Trace;

    new-instance v3, Lf/h/c/y/i/a;

    invoke-direct {v3, v2}, Lf/h/c/y/i/a;-><init>(Lcom/google/firebase/perf/metrics/Trace;)V

    invoke-virtual {v3}, Lf/h/c/y/i/a;->a()Lf/h/c/y/m/t;

    move-result-object v2

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v3, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v3, Lf/h/c/y/m/t;

    invoke-static {v3, v2}, Lf/h/c/y/m/t;->D(Lf/h/c/y/m/t;Lf/h/c/y/m/t;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    invoke-virtual {v1}, Lcom/google/firebase/perf/metrics/Trace;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/t;

    invoke-static {v2}, Lf/h/c/y/m/t;->F(Lf/h/c/y/m/t;)Ljava/util/Map;

    move-result-object v2

    check-cast v2, Lf/h/e/e0;

    invoke-virtual {v2, v1}, Lf/h/e/e0;->putAll(Ljava/util/Map;)V

    iget-object v1, p0, Lf/h/c/y/i/a;->a:Lcom/google/firebase/perf/metrics/Trace;

    iget-object v2, v1, Lcom/google/firebase/perf/metrics/Trace;->g:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v1, Lcom/google/firebase/perf/metrics/Trace;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/firebase/perf/internal/PerfSession;

    if-eqz v4, :cond_2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/google/firebase/perf/internal/PerfSession;->b(Ljava/util/List;)[Lf/h/c/y/m/q;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/t;

    invoke-static {v2, v1}, Lf/h/c/y/m/t;->H(Lf/h/c/y/m/t;Ljava/lang/Iterable;)V

    :cond_4
    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/t;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
