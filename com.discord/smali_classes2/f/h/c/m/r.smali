.class public Lf/h/c/m/r;
.super Ljava/lang/Object;
.source "Lazy.java"

# interfaces
.implements Lf/h/c/u/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/h/c/u/a<",
        "TT;>;"
    }
.end annotation


# static fields
.field public static final c:Ljava/lang/Object;


# instance fields
.field public volatile a:Ljava/lang/Object;

.field public volatile b:Lf/h/c/u/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/c/u/a<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf/h/c/m/r;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lf/h/c/u/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/u/a<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/h/c/m/r;->c:Ljava/lang/Object;

    iput-object v0, p0, Lf/h/c/m/r;->a:Ljava/lang/Object;

    iput-object p1, p0, Lf/h/c/m/r;->b:Lf/h/c/u/a;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/c/m/r;->a:Ljava/lang/Object;

    sget-object v1, Lf/h/c/m/r;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/c/m/r;->a:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lf/h/c/m/r;->b:Lf/h/c/u/a;

    invoke-interface {v0}, Lf/h/c/u/a;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lf/h/c/m/r;->a:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-object v1, p0, Lf/h/c/m/r;->b:Lf/h/c/u/a;

    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :goto_0
    return-object v0
.end method
