.class public final Lf/h/d/k;
.super Ljava/lang/Object;
.source "GsonBuilder.java"


# instance fields
.field public a:Lf/h/d/w/o;

.field public b:Lf/h/d/t;

.field public c:Lf/h/d/e;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/reflect/Type;",
            "Lf/h/d/l<",
            "*>;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/d/u;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/d/u;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:I

.field public i:I

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/h/d/w/o;->f:Lf/h/d/w/o;

    iput-object v0, p0, Lf/h/d/k;->a:Lf/h/d/w/o;

    sget-object v0, Lf/h/d/t;->d:Lf/h/d/t;

    iput-object v0, p0, Lf/h/d/k;->b:Lf/h/d/t;

    sget-object v0, Lf/h/d/d;->d:Lf/h/d/d;

    iput-object v0, p0, Lf/h/d/k;->c:Lf/h/d/e;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h/d/k;->d:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/d/k;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/d/k;->f:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/d/k;->g:Z

    const/4 v0, 0x2

    iput v0, p0, Lf/h/d/k;->h:I

    iput v0, p0, Lf/h/d/k;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/d/k;->j:Z

    return-void
.end method


# virtual methods
.method public a()Lcom/google/gson/Gson;
    .locals 20

    move-object/from16 v0, p0

    new-instance v15, Ljava/util/ArrayList;

    iget-object v1, v0, Lf/h/d/k;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, v0, Lf/h/d/k;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, 0x3

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, v0, Lf/h/d/k;->e:Ljava/util/List;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {v15}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lf/h/d/k;->f:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget v1, v0, Lf/h/d/k;->h:I

    iget v2, v0, Lf/h/d/k;->i:I

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    if-eq v2, v3, :cond_0

    new-instance v3, Lf/h/d/a;

    const-class v4, Ljava/util/Date;

    invoke-direct {v3, v4, v1, v2}, Lf/h/d/a;-><init>(Ljava/lang/Class;II)V

    new-instance v4, Lf/h/d/a;

    const-class v5, Ljava/sql/Timestamp;

    invoke-direct {v4, v5, v1, v2}, Lf/h/d/a;-><init>(Ljava/lang/Class;II)V

    new-instance v5, Lf/h/d/a;

    const-class v6, Ljava/sql/Date;

    invoke-direct {v5, v6, v1, v2}, Lf/h/d/a;-><init>(Ljava/lang/Class;II)V

    const-class v1, Ljava/util/Date;

    sget-object v2, Lf/h/d/w/y/o;->a:Lcom/google/gson/TypeAdapter;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v3}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-class v1, Ljava/sql/Timestamp;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v4}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-class v1, Ljava/sql/Date;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v5}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v19, Lcom/google/gson/Gson;

    move-object/from16 v1, v19

    iget-object v2, v0, Lf/h/d/k;->a:Lf/h/d/w/o;

    iget-object v3, v0, Lf/h/d/k;->c:Lf/h/d/e;

    iget-object v4, v0, Lf/h/d/k;->d:Ljava/util/Map;

    iget-boolean v5, v0, Lf/h/d/k;->g:Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-boolean v8, v0, Lf/h/d/k;->j:Z

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget-object v12, v0, Lf/h/d/k;->b:Lf/h/d/t;

    const/4 v13, 0x0

    iget v14, v0, Lf/h/d/k;->h:I

    iget v6, v0, Lf/h/d/k;->i:I

    move-object/from16 v18, v15

    move v15, v6

    iget-object v6, v0, Lf/h/d/k;->e:Ljava/util/List;

    move-object/from16 v16, v6

    iget-object v6, v0, Lf/h/d/k;->f:Ljava/util/List;

    move-object/from16 v17, v6

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v18}, Lcom/google/gson/Gson;-><init>(Lf/h/d/w/o;Lf/h/d/e;Ljava/util/Map;ZZZZZZZLf/h/d/t;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v19
.end method
