.class public abstract enum Lf/h/d/t;
.super Ljava/lang/Enum;
.source "LongSerializationPolicy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/d/t;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/d/t;

.field public static final enum e:Lf/h/d/t;

.field public static final synthetic f:[Lf/h/d/t;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/h/d/t$a;

    const-string v1, "DEFAULT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/d/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/d/t;->d:Lf/h/d/t;

    new-instance v1, Lf/h/d/t$b;

    const-string v3, "STRING"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/d/t$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/d/t;->e:Lf/h/d/t;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/h/d/t;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/h/d/t;->f:[Lf/h/d/t;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILf/h/d/t$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/d/t;
    .locals 1

    const-class v0, Lf/h/d/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/d/t;

    return-object p0
.end method

.method public static values()[Lf/h/d/t;
    .locals 1

    sget-object v0, Lf/h/d/t;->f:[Lf/h/d/t;

    invoke-virtual {v0}, [Lf/h/d/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/d/t;

    return-object v0
.end method
