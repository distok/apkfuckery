.class public final Lf/h/d/w/y/o;
.super Ljava/lang/Object;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/d/w/y/o$e0;
    }
.end annotation


# static fields
.field public static final A:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final B:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public static final C:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Lf/h/d/u;

.field public static final E:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:Lf/h/d/u;

.field public static final G:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final H:Lf/h/d/u;

.field public static final I:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final J:Lf/h/d/u;

.field public static final K:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final L:Lf/h/d/u;

.field public static final M:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final N:Lf/h/d/u;

.field public static final O:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final P:Lf/h/d/u;

.field public static final Q:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/Currency;",
            ">;"
        }
    .end annotation
.end field

.field public static final R:Lf/h/d/u;

.field public static final S:Lf/h/d/u;

.field public static final T:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final U:Lf/h/d/u;

.field public static final V:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final W:Lf/h/d/u;

.field public static final X:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/google/gson/JsonElement;",
            ">;"
        }
    .end annotation
.end field

.field public static final Y:Lf/h/d/u;

.field public static final Z:Lf/h/d/u;

.field public static final a:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lf/h/d/u;

.field public static final c:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lf/h/d/u;

.field public static final e:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lf/h/d/u;

.field public static final h:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lf/h/d/u;

.field public static final j:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lf/h/d/u;

.field public static final l:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lf/h/d/u;

.field public static final n:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lf/h/d/u;

.field public static final p:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lf/h/d/u;

.field public static final r:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/util/concurrent/atomic/AtomicIntegerArray;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lf/h/d/u;

.field public static final t:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final x:Lf/h/d/u;

.field public static final y:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:Lf/h/d/u;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lf/h/d/w/y/o$k;

    invoke-direct {v0}, Lf/h/d/w/y/o$k;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/TypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lf/h/d/w/y/o;->a:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/lang/Class;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->b:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$v;

    invoke-direct {v0}, Lf/h/d/w/y/o$v;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/TypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lf/h/d/w/y/o;->c:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/BitSet;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->d:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$x;

    invoke-direct {v0}, Lf/h/d/w/y/o$x;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->e:Lcom/google/gson/TypeAdapter;

    new-instance v1, Lf/h/d/w/y/o$y;

    invoke-direct {v1}, Lf/h/d/w/y/o$y;-><init>()V

    sput-object v1, Lf/h/d/w/y/o;->f:Lcom/google/gson/TypeAdapter;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Boolean;

    new-instance v3, Lf/h/d/w/y/r;

    invoke-direct {v3, v1, v2, v0}, Lf/h/d/w/y/r;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v3, Lf/h/d/w/y/o;->g:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$z;

    invoke-direct {v0}, Lf/h/d/w/y/o$z;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->h:Lcom/google/gson/TypeAdapter;

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Byte;

    new-instance v3, Lf/h/d/w/y/r;

    invoke-direct {v3, v1, v2, v0}, Lf/h/d/w/y/r;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v3, Lf/h/d/w/y/o;->i:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$a0;

    invoke-direct {v0}, Lf/h/d/w/y/o$a0;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->j:Lcom/google/gson/TypeAdapter;

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Short;

    new-instance v3, Lf/h/d/w/y/r;

    invoke-direct {v3, v1, v2, v0}, Lf/h/d/w/y/r;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v3, Lf/h/d/w/y/o;->k:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$b0;

    invoke-direct {v0}, Lf/h/d/w/y/o$b0;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->l:Lcom/google/gson/TypeAdapter;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Integer;

    new-instance v3, Lf/h/d/w/y/r;

    invoke-direct {v3, v1, v2, v0}, Lf/h/d/w/y/r;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v3, Lf/h/d/w/y/o;->m:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$c0;

    invoke-direct {v0}, Lf/h/d/w/y/o$c0;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/TypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lf/h/d/w/y/o;->n:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->o:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$d0;

    invoke-direct {v0}, Lf/h/d/w/y/o$d0;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/TypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lf/h/d/w/y/o;->p:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->q:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$a;

    invoke-direct {v0}, Lf/h/d/w/y/o$a;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/TypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lf/h/d/w/y/o;->r:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/concurrent/atomic/AtomicIntegerArray;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->s:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$b;

    invoke-direct {v0}, Lf/h/d/w/y/o$b;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->t:Lcom/google/gson/TypeAdapter;

    new-instance v0, Lf/h/d/w/y/o$c;

    invoke-direct {v0}, Lf/h/d/w/y/o$c;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->u:Lcom/google/gson/TypeAdapter;

    new-instance v0, Lf/h/d/w/y/o$d;

    invoke-direct {v0}, Lf/h/d/w/y/o$d;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->v:Lcom/google/gson/TypeAdapter;

    new-instance v0, Lf/h/d/w/y/o$e;

    invoke-direct {v0}, Lf/h/d/w/y/o$e;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->w:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/lang/Number;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->x:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$f;

    invoke-direct {v0}, Lf/h/d/w/y/o$f;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->y:Lcom/google/gson/TypeAdapter;

    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Character;

    new-instance v3, Lf/h/d/w/y/r;

    invoke-direct {v3, v1, v2, v0}, Lf/h/d/w/y/r;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v3, Lf/h/d/w/y/o;->z:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$g;

    invoke-direct {v0}, Lf/h/d/w/y/o$g;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->A:Lcom/google/gson/TypeAdapter;

    new-instance v1, Lf/h/d/w/y/o$h;

    invoke-direct {v1}, Lf/h/d/w/y/o$h;-><init>()V

    sput-object v1, Lf/h/d/w/y/o;->B:Lcom/google/gson/TypeAdapter;

    new-instance v1, Lf/h/d/w/y/o$i;

    invoke-direct {v1}, Lf/h/d/w/y/o$i;-><init>()V

    sput-object v1, Lf/h/d/w/y/o;->C:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/lang/String;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->D:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$j;

    invoke-direct {v0}, Lf/h/d/w/y/o$j;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->E:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/lang/StringBuilder;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->F:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$l;

    invoke-direct {v0}, Lf/h/d/w/y/o$l;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->G:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/lang/StringBuffer;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->H:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$m;

    invoke-direct {v0}, Lf/h/d/w/y/o$m;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->I:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/net/URL;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->J:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$n;

    invoke-direct {v0}, Lf/h/d/w/y/o$n;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->K:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/net/URI;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->L:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$o;

    invoke-direct {v0}, Lf/h/d/w/y/o$o;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->M:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/net/InetAddress;

    new-instance v2, Lf/h/d/w/y/t;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/t;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->N:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$p;

    invoke-direct {v0}, Lf/h/d/w/y/o$p;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->O:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/UUID;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->P:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$q;

    invoke-direct {v0}, Lf/h/d/w/y/o$q;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/TypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v0

    sput-object v0, Lf/h/d/w/y/o;->Q:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/Currency;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->R:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$r;

    invoke-direct {v0}, Lf/h/d/w/y/o$r;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->S:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$s;

    invoke-direct {v0}, Lf/h/d/w/y/o$s;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->T:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/Calendar;

    const-class v2, Ljava/util/GregorianCalendar;

    new-instance v3, Lf/h/d/w/y/s;

    invoke-direct {v3, v1, v2, v0}, Lf/h/d/w/y/s;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v3, Lf/h/d/w/y/o;->U:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$t;

    invoke-direct {v0}, Lf/h/d/w/y/o$t;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->V:Lcom/google/gson/TypeAdapter;

    const-class v1, Ljava/util/Locale;

    new-instance v2, Lf/h/d/w/y/q;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/q;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->W:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$u;

    invoke-direct {v0}, Lf/h/d/w/y/o$u;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->X:Lcom/google/gson/TypeAdapter;

    const-class v1, Lcom/google/gson/JsonElement;

    new-instance v2, Lf/h/d/w/y/t;

    invoke-direct {v2, v1, v0}, Lf/h/d/w/y/t;-><init>(Ljava/lang/Class;Lcom/google/gson/TypeAdapter;)V

    sput-object v2, Lf/h/d/w/y/o;->Y:Lf/h/d/u;

    new-instance v0, Lf/h/d/w/y/o$w;

    invoke-direct {v0}, Lf/h/d/w/y/o$w;-><init>()V

    sput-object v0, Lf/h/d/w/y/o;->Z:Lf/h/d/u;

    return-void
.end method
