.class public final Lf/h/d/w/y/m;
.super Lcom/google/gson/TypeAdapter;
.source "TreeTypeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/d/w/y/m$b;,
        Lf/h/d/w/y/m$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/gson/TypeAdapter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/h/d/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/d/s<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final b:Lf/h/d/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/d/o<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/google/gson/Gson;

.field public final d:Lcom/google/gson/reflect/TypeToken;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/reflect/TypeToken<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:Lf/h/d/u;

.field public final f:Lf/h/d/w/y/m$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/d/w/y/m<",
            "TT;>.b;"
        }
    .end annotation
.end field

.field public g:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/d/s;Lf/h/d/o;Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;Lf/h/d/u;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/d/s<",
            "TT;>;",
            "Lf/h/d/o<",
            "TT;>;",
            "Lcom/google/gson/Gson;",
            "Lcom/google/gson/reflect/TypeToken<",
            "TT;>;",
            "Lf/h/d/u;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    new-instance v0, Lf/h/d/w/y/m$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lf/h/d/w/y/m$b;-><init>(Lf/h/d/w/y/m;Lf/h/d/w/y/m$a;)V

    iput-object v0, p0, Lf/h/d/w/y/m;->f:Lf/h/d/w/y/m$b;

    iput-object p1, p0, Lf/h/d/w/y/m;->a:Lf/h/d/s;

    iput-object p2, p0, Lf/h/d/w/y/m;->b:Lf/h/d/o;

    iput-object p3, p0, Lf/h/d/w/y/m;->c:Lcom/google/gson/Gson;

    iput-object p4, p0, Lf/h/d/w/y/m;->d:Lcom/google/gson/reflect/TypeToken;

    iput-object p5, p0, Lf/h/d/w/y/m;->e:Lf/h/d/u;

    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/d/w/y/m;->b:Lf/h/d/o;

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/d/w/y/m;->g:Lcom/google/gson/TypeAdapter;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/d/w/y/m;->c:Lcom/google/gson/Gson;

    iget-object v1, p0, Lf/h/d/w/y/m;->e:Lf/h/d/u;

    iget-object v2, p0, Lf/h/d/w/y/m;->d:Lcom/google/gson/reflect/TypeToken;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->h(Lf/h/d/u;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lf/h/d/w/y/m;->g:Lcom/google/gson/TypeAdapter;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/gson/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    :try_start_1
    sget-object v1, Lf/h/d/w/y/o;->X:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/gson/JsonElement;
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/gson/stream/MalformedJsonException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/google/gson/JsonSyntaxException;

    invoke-direct {v0, p1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_2
    move-exception p1

    new-instance v0, Lcom/google/gson/JsonIOException;

    invoke-direct {v0, p1}, Lcom/google/gson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_3
    move-exception p1

    new-instance v0, Lcom/google/gson/JsonSyntaxException;

    invoke-direct {v0, p1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_4
    move-exception p1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    sget-object p1, Lf/h/d/p;->a:Lf/h/d/p;

    :goto_2
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, p1, Lf/h/d/p;

    if-eqz v0, :cond_2

    const/4 p1, 0x0

    return-object p1

    :cond_2
    iget-object v0, p0, Lf/h/d/w/y/m;->b:Lf/h/d/o;

    iget-object v1, p0, Lf/h/d/w/y/m;->d:Lcom/google/gson/reflect/TypeToken;

    invoke-virtual {v1}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Lf/h/d/w/y/m;->f:Lf/h/d/w/y/m$b;

    invoke-interface {v0, p1, v1, v2}, Lf/h/d/o;->a(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lf/h/d/n;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance v0, Lcom/google/gson/JsonSyntaxException;

    invoke-direct {v0, p1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/d/w/y/m;->a:Lf/h/d/s;

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/d/w/y/m;->g:Lcom/google/gson/TypeAdapter;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/d/w/y/m;->c:Lcom/google/gson/Gson;

    iget-object v1, p0, Lf/h/d/w/y/m;->e:Lf/h/d/u;

    iget-object v2, p0, Lf/h/d/w/y/m;->d:Lcom/google/gson/reflect/TypeToken;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->h(Lf/h/d/u;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lf/h/d/w/y/m;->g:Lcom/google/gson/TypeAdapter;

    :goto_0
    invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    return-void

    :cond_1
    if-nez p2, :cond_2

    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    return-void

    :cond_2
    iget-object v1, p0, Lf/h/d/w/y/m;->d:Lcom/google/gson/reflect/TypeToken;

    invoke-virtual {v1}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Lf/h/d/w/y/m;->f:Lf/h/d/w/y/m$b;

    invoke-interface {v0, p2, v1, v2}, Lf/h/d/s;->serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lf/h/d/r;)Lcom/google/gson/JsonElement;

    move-result-object p2

    sget-object v0, Lf/h/d/w/y/o;->X:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    return-void
.end method
