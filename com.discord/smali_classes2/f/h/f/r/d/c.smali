.class public final Lf/h/f/r/d/c;
.super Ljava/lang/Object;
.source "BoundingBox.java"


# instance fields
.field public final a:Lf/h/f/n/b;

.field public final b:Lf/h/f/k;

.field public final c:Lf/h/f/k;

.field public final d:Lf/h/f/k;

.field public final e:Lf/h/f/k;

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I


# direct methods
.method public constructor <init>(Lf/h/f/n/b;Lf/h/f/k;Lf/h/f/k;Lf/h/f/k;Lf/h/f/k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz p4, :cond_2

    if-nez p5, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    if-eqz v2, :cond_5

    if-nez v0, :cond_4

    goto :goto_2

    :cond_4
    sget-object p1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw p1

    :cond_5
    :goto_2
    if-eqz v2, :cond_6

    new-instance p2, Lf/h/f/k;

    iget p3, p4, Lf/h/f/k;->b:F

    const/4 v0, 0x0

    invoke-direct {p2, v0, p3}, Lf/h/f/k;-><init>(FF)V

    new-instance p3, Lf/h/f/k;

    iget v1, p5, Lf/h/f/k;->b:F

    invoke-direct {p3, v0, v1}, Lf/h/f/k;-><init>(FF)V

    goto :goto_3

    :cond_6
    if-eqz v0, :cond_7

    new-instance p4, Lf/h/f/k;

    iget p5, p1, Lf/h/f/n/b;->d:I

    add-int/lit8 v0, p5, -0x1

    int-to-float v0, v0

    iget v2, p2, Lf/h/f/k;->b:F

    invoke-direct {p4, v0, v2}, Lf/h/f/k;-><init>(FF)V

    new-instance v0, Lf/h/f/k;

    sub-int/2addr p5, v1

    int-to-float p5, p5

    iget v1, p3, Lf/h/f/k;->b:F

    invoke-direct {v0, p5, v1}, Lf/h/f/k;-><init>(FF)V

    move-object p5, v0

    :cond_7
    :goto_3
    iput-object p1, p0, Lf/h/f/r/d/c;->a:Lf/h/f/n/b;

    iput-object p2, p0, Lf/h/f/r/d/c;->b:Lf/h/f/k;

    iput-object p3, p0, Lf/h/f/r/d/c;->c:Lf/h/f/k;

    iput-object p4, p0, Lf/h/f/r/d/c;->d:Lf/h/f/k;

    iput-object p5, p0, Lf/h/f/r/d/c;->e:Lf/h/f/k;

    iget p1, p2, Lf/h/f/k;->a:F

    iget v0, p3, Lf/h/f/k;->a:F

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lf/h/f/r/d/c;->f:I

    iget p1, p4, Lf/h/f/k;->a:F

    iget v0, p5, Lf/h/f/k;->a:F

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lf/h/f/r/d/c;->g:I

    iget p1, p2, Lf/h/f/k;->b:F

    iget p2, p4, Lf/h/f/k;->b:F

    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lf/h/f/r/d/c;->h:I

    iget p1, p3, Lf/h/f/k;->b:F

    iget p2, p5, Lf/h/f/k;->b:F

    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lf/h/f/r/d/c;->i:I

    return-void
.end method

.method public constructor <init>(Lf/h/f/r/d/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lf/h/f/r/d/c;->a:Lf/h/f/n/b;

    iput-object v0, p0, Lf/h/f/r/d/c;->a:Lf/h/f/n/b;

    iget-object v0, p1, Lf/h/f/r/d/c;->b:Lf/h/f/k;

    iput-object v0, p0, Lf/h/f/r/d/c;->b:Lf/h/f/k;

    iget-object v0, p1, Lf/h/f/r/d/c;->c:Lf/h/f/k;

    iput-object v0, p0, Lf/h/f/r/d/c;->c:Lf/h/f/k;

    iget-object v0, p1, Lf/h/f/r/d/c;->d:Lf/h/f/k;

    iput-object v0, p0, Lf/h/f/r/d/c;->d:Lf/h/f/k;

    iget-object v0, p1, Lf/h/f/r/d/c;->e:Lf/h/f/k;

    iput-object v0, p0, Lf/h/f/r/d/c;->e:Lf/h/f/k;

    iget v0, p1, Lf/h/f/r/d/c;->f:I

    iput v0, p0, Lf/h/f/r/d/c;->f:I

    iget v0, p1, Lf/h/f/r/d/c;->g:I

    iput v0, p0, Lf/h/f/r/d/c;->g:I

    iget v0, p1, Lf/h/f/r/d/c;->h:I

    iput v0, p0, Lf/h/f/r/d/c;->h:I

    iget p1, p1, Lf/h/f/r/d/c;->i:I

    iput p1, p0, Lf/h/f/r/d/c;->i:I

    return-void
.end method
