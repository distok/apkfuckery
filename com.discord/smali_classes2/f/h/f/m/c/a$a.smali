.class public final enum Lf/h/f/m/c/a$a;
.super Ljava/lang/Enum;
.source "Decoder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/f/m/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/f/m/c/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/f/m/c/a$a;

.field public static final enum e:Lf/h/f/m/c/a$a;

.field public static final enum f:Lf/h/f/m/c/a$a;

.field public static final enum g:Lf/h/f/m/c/a$a;

.field public static final enum h:Lf/h/f/m/c/a$a;

.field public static final enum i:Lf/h/f/m/c/a$a;

.field public static final synthetic j:[Lf/h/f/m/c/a$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    new-instance v0, Lf/h/f/m/c/a$a;

    const-string v1, "UPPER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/f/m/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/f/m/c/a$a;->d:Lf/h/f/m/c/a$a;

    new-instance v1, Lf/h/f/m/c/a$a;

    const-string v3, "LOWER"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/f/m/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/f/m/c/a$a;->e:Lf/h/f/m/c/a$a;

    new-instance v3, Lf/h/f/m/c/a$a;

    const-string v5, "MIXED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/h/f/m/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/h/f/m/c/a$a;->f:Lf/h/f/m/c/a$a;

    new-instance v5, Lf/h/f/m/c/a$a;

    const-string v7, "DIGIT"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/h/f/m/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/h/f/m/c/a$a;->g:Lf/h/f/m/c/a$a;

    new-instance v7, Lf/h/f/m/c/a$a;

    const-string v9, "PUNCT"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lf/h/f/m/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lf/h/f/m/c/a$a;->h:Lf/h/f/m/c/a$a;

    new-instance v9, Lf/h/f/m/c/a$a;

    const-string v11, "BINARY"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lf/h/f/m/c/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lf/h/f/m/c/a$a;->i:Lf/h/f/m/c/a$a;

    const/4 v11, 0x6

    new-array v11, v11, [Lf/h/f/m/c/a$a;

    aput-object v0, v11, v2

    aput-object v1, v11, v4

    aput-object v3, v11, v6

    aput-object v5, v11, v8

    aput-object v7, v11, v10

    aput-object v9, v11, v12

    sput-object v11, Lf/h/f/m/c/a$a;->j:[Lf/h/f/m/c/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/f/m/c/a$a;
    .locals 1

    const-class v0, Lf/h/f/m/c/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/f/m/c/a$a;

    return-object p0
.end method

.method public static values()[Lf/h/f/m/c/a$a;
    .locals 1

    sget-object v0, Lf/h/f/m/c/a$a;->j:[Lf/h/f/m/c/a$a;

    invoke-virtual {v0}, [Lf/h/f/m/c/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/f/m/c/a$a;

    return-object v0
.end method
