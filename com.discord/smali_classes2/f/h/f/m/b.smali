.class public final Lf/h/f/m/b;
.super Ljava/lang/Object;
.source "AztecReader.java"

# interfaces
.implements Lf/h/f/i;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/f/c;Ljava/util/Map;)Lcom/google/zxing/Result;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/f/c;",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    new-instance v0, Lf/h/f/m/d/a;

    invoke-virtual {p1}, Lf/h/f/c;->a()Lf/h/f/n/b;

    move-result-object p1

    invoke-direct {v0, p1}, Lf/h/f/m/d/a;-><init>(Lf/h/f/n/b;)V

    const/4 p1, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p1}, Lf/h/f/m/d/a;->a(Z)Lf/h/f/m/a;

    move-result-object v2

    iget-object v3, v2, Lf/h/f/n/g;->b:[Lf/h/f/k;
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/zxing/FormatException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    new-instance v4, Lf/h/f/m/c/a;

    invoke-direct {v4}, Lf/h/f/m/c/a;-><init>()V

    invoke-virtual {v4, v2}, Lf/h/f/m/c/a;->a(Lf/h/f/m/a;)Lf/h/f/n/e;

    move-result-object v2
    :try_end_1
    .catch Lcom/google/zxing/NotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/zxing/FormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v4, v3

    move-object v3, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v3, v1

    :goto_0
    move-object v4, v3

    move-object v3, v2

    move-object v2, v1

    goto :goto_2

    :catch_3
    move-exception v2

    move-object v3, v1

    :goto_1
    move-object v4, v3

    move-object v3, v1

    :goto_2
    if-nez v1, :cond_2

    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {v0, v1}, Lf/h/f/m/d/a;->a(Z)Lf/h/f/m/a;

    move-result-object v0

    iget-object v4, v0, Lf/h/f/n/g;->b:[Lf/h/f/k;

    new-instance v1, Lf/h/f/m/c/a;

    invoke-direct {v1}, Lf/h/f/m/c/a;-><init>()V

    invoke-virtual {v1, v0}, Lf/h/f/m/c/a;->a(Lf/h/f/m/a;)Lf/h/f/n/e;

    move-result-object v1
    :try_end_2
    .catch Lcom/google/zxing/NotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/google/zxing/FormatException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_4

    :catch_4
    move-exception p1

    goto :goto_3

    :catch_5
    move-exception p1

    :goto_3
    if-nez v2, :cond_1

    if-eqz v3, :cond_0

    throw v3

    :cond_0
    throw p1

    :cond_1
    throw v2

    :cond_2
    :goto_4
    move-object v6, v4

    if-eqz p2, :cond_3

    sget-object v0, Lf/h/f/d;->m:Lf/h/f/d;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/f/l;

    if-eqz p2, :cond_3

    array-length v0, v6

    :goto_5
    if-ge p1, v0, :cond_3

    aget-object v2, v6, p1

    invoke-interface {p2, v2}, Lf/h/f/l;->a(Lf/h/f/k;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_5

    :cond_3
    new-instance p1, Lcom/google/zxing/Result;

    iget-object v3, v1, Lf/h/f/n/e;->c:Ljava/lang/String;

    iget-object v4, v1, Lf/h/f/n/e;->a:[B

    iget v5, v1, Lf/h/f/n/e;->b:I

    sget-object v7, Lf/h/f/a;->d:Lf/h/f/a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object v2, p1

    invoke-direct/range {v2 .. v9}, Lcom/google/zxing/Result;-><init>(Ljava/lang/String;[BI[Lf/h/f/k;Lf/h/f/a;J)V

    iget-object p2, v1, Lf/h/f/n/e;->d:Ljava/util/List;

    if-eqz p2, :cond_4

    sget-object v0, Lf/h/f/j;->f:Lf/h/f/j;

    invoke-virtual {p1, v0, p2}, Lcom/google/zxing/Result;->b(Lf/h/f/j;Ljava/lang/Object;)V

    :cond_4
    iget-object p2, v1, Lf/h/f/n/e;->e:Ljava/lang/String;

    if-eqz p2, :cond_5

    sget-object v0, Lf/h/f/j;->g:Lf/h/f/j;

    invoke-virtual {p1, v0, p2}, Lcom/google/zxing/Result;->b(Lf/h/f/j;Ljava/lang/Object;)V

    :cond_5
    return-object p1
.end method

.method public reset()V
    .locals 0

    return-void
.end method
