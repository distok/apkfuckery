.class public final Lf/h/f/e;
.super Lf/h/f/f;
.source "InvertedLuminanceSource.java"


# instance fields
.field public final c:Lf/h/f/f;


# direct methods
.method public constructor <init>(Lf/h/f/f;)V
    .locals 2

    iget v0, p1, Lf/h/f/f;->a:I

    iget v1, p1, Lf/h/f/f;->b:I

    invoke-direct {p0, v0, v1}, Lf/h/f/f;-><init>(II)V

    iput-object p1, p0, Lf/h/f/e;->c:Lf/h/f/f;

    return-void
.end method


# virtual methods
.method public a()[B
    .locals 5

    iget-object v0, p0, Lf/h/f/e;->c:Lf/h/f/f;

    invoke-virtual {v0}, Lf/h/f/f;->a()[B

    move-result-object v0

    iget v1, p0, Lf/h/f/f;->a:I

    iget v2, p0, Lf/h/f/f;->b:I

    mul-int v1, v1, v2

    new-array v2, v1, [B

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-byte v4, v0, v3

    and-int/lit16 v4, v4, 0xff

    rsub-int v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public b(I[B)[B
    .locals 2

    iget-object v0, p0, Lf/h/f/e;->c:Lf/h/f/f;

    invoke-virtual {v0, p1, p2}, Lf/h/f/f;->b(I[B)[B

    move-result-object p1

    iget p2, p0, Lf/h/f/f;->a:I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget-byte v1, p1, v0

    and-int/lit16 v1, v1, 0xff

    rsub-int v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lf/h/f/e;->c:Lf/h/f/f;

    invoke-virtual {v0}, Lf/h/f/f;->c()Z

    move-result v0

    return v0
.end method

.method public d()Lf/h/f/f;
    .locals 2

    new-instance v0, Lf/h/f/e;

    iget-object v1, p0, Lf/h/f/e;->c:Lf/h/f/f;

    invoke-virtual {v1}, Lf/h/f/f;->d()Lf/h/f/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lf/h/f/e;-><init>(Lf/h/f/f;)V

    return-object v0
.end method
