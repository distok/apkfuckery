.class public final Lf/h/f/s/b/e;
.super Ljava/lang/Object;
.source "Decoder.java"


# instance fields
.field public final a:Lf/h/f/n/l/c;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/f/n/l/c;

    sget-object v1, Lf/h/f/n/l/a;->l:Lf/h/f/n/l/a;

    invoke-direct {v0, v1}, Lf/h/f/n/l/c;-><init>(Lf/h/f/n/l/a;)V

    iput-object v0, p0, Lf/h/f/s/b/e;->a:Lf/h/f/n/l/c;

    return-void
.end method


# virtual methods
.method public a(Lf/h/f/n/b;Ljava/util/Map;)Lf/h/f/n/e;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/f/n/b;",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lf/h/f/n/e;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/FormatException;,
            Lcom/google/zxing/ChecksumException;
        }
    .end annotation

    new-instance v0, Lf/h/f/s/b/a;

    invoke-direct {v0, p1}, Lf/h/f/s/b/a;-><init>(Lf/h/f/n/b;)V

    const/4 p1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p2}, Lf/h/f/s/b/e;->b(Lf/h/f/s/b/a;Ljava/util/Map;)Lf/h/f/n/e;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/zxing/FormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/zxing/ChecksumException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v1

    move-object v2, v1

    move-object v1, p1

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v2, p1

    :goto_0
    :try_start_1
    invoke-virtual {v0}, Lf/h/f/s/b/a;->e()V

    iput-object p1, v0, Lf/h/f/s/b/a;->b:Lf/h/f/s/b/j;

    iput-object p1, v0, Lf/h/f/s/b/a;->c:Lf/h/f/s/b/g;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lf/h/f/s/b/a;->d:Z

    invoke-virtual {v0}, Lf/h/f/s/b/a;->d()Lf/h/f/s/b/j;

    invoke-virtual {v0}, Lf/h/f/s/b/a;->c()Lf/h/f/s/b/g;

    invoke-virtual {v0}, Lf/h/f/s/b/a;->b()V

    invoke-virtual {p0, v0, p2}, Lf/h/f/s/b/e;->b(Lf/h/f/s/b/a;Ljava/util/Map;)Lf/h/f/n/e;

    move-result-object p2

    new-instance v0, Lf/h/f/s/b/i;

    invoke-direct {v0, p1}, Lf/h/f/s/b/i;-><init>(Z)V

    iput-object v0, p2, Lf/h/f/n/e;->f:Ljava/lang/Object;
    :try_end_1
    .catch Lcom/google/zxing/FormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/zxing/ChecksumException; {:try_start_1 .. :try_end_1} :catch_2

    return-object p2

    :catch_2
    nop

    if-eqz v1, :cond_0

    throw v1

    :cond_0
    throw v2
.end method

.method public final b(Lf/h/f/s/b/a;Ljava/util/Map;)Lf/h/f/n/e;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/f/s/b/a;",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lf/h/f/n/e;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/FormatException;,
            Lcom/google/zxing/ChecksumException;
        }
    .end annotation

    move-object/from16 v0, p1

    invoke-virtual/range {p1 .. p1}, Lf/h/f/s/b/a;->d()Lf/h/f/s/b/j;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lf/h/f/s/b/a;->c()Lf/h/f/s/b/g;

    move-result-object v2

    iget-object v2, v2, Lf/h/f/s/b/g;->a:Lf/h/f/s/b/f;

    invoke-virtual/range {p1 .. p1}, Lf/h/f/s/b/a;->c()Lf/h/f/s/b/g;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lf/h/f/s/b/a;->d()Lf/h/f/s/b/j;

    move-result-object v4

    invoke-static {}, Lf/h/f/s/b/c;->values()[Lf/h/f/s/b/c;

    move-result-object v5

    iget-byte v3, v3, Lf/h/f/s/b/g;->b:B

    aget-object v3, v5, v3

    iget-object v5, v0, Lf/h/f/s/b/a;->a:Lf/h/f/n/b;

    iget v6, v5, Lf/h/f/n/b;->e:I

    invoke-virtual {v3, v5, v6}, Lf/h/f/s/b/c;->g(Lf/h/f/n/b;I)V

    invoke-virtual {v4}, Lf/h/f/s/b/j;->c()I

    move-result v3

    new-instance v5, Lf/h/f/n/b;

    invoke-direct {v5, v3, v3}, Lf/h/f/n/b;-><init>(II)V

    const/4 v7, 0x0

    const/16 v8, 0x9

    invoke-virtual {v5, v7, v7, v8, v8}, Lf/h/f/n/b;->j(IIII)V

    add-int/lit8 v9, v3, -0x8

    const/16 v10, 0x8

    invoke-virtual {v5, v9, v7, v10, v8}, Lf/h/f/n/b;->j(IIII)V

    invoke-virtual {v5, v7, v9, v8, v10}, Lf/h/f/n/b;->j(IIII)V

    iget-object v9, v4, Lf/h/f/s/b/j;->b:[I

    array-length v9, v9

    const/4 v11, 0x0

    :goto_0
    const/4 v12, 0x5

    if-ge v11, v9, :cond_4

    iget-object v13, v4, Lf/h/f/s/b/j;->b:[I

    aget v13, v13, v11

    add-int/lit8 v13, v13, -0x2

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v9, :cond_3

    if-nez v11, :cond_0

    if-eqz v14, :cond_2

    add-int/lit8 v15, v9, -0x1

    if-eq v14, v15, :cond_2

    :cond_0
    add-int/lit8 v15, v9, -0x1

    if-ne v11, v15, :cond_1

    if-eqz v14, :cond_2

    :cond_1
    iget-object v15, v4, Lf/h/f/s/b/j;->b:[I

    aget v15, v15, v14

    add-int/lit8 v15, v15, -0x2

    invoke-virtual {v5, v15, v13, v12, v12}, Lf/h/f/n/b;->j(IIII)V

    :cond_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v9, v3, -0x11

    const/4 v11, 0x6

    const/4 v13, 0x1

    invoke-virtual {v5, v11, v8, v13, v9}, Lf/h/f/n/b;->j(IIII)V

    invoke-virtual {v5, v8, v11, v9, v13}, Lf/h/f/n/b;->j(IIII)V

    iget v9, v4, Lf/h/f/s/b/j;->a:I

    const/4 v14, 0x3

    if-le v9, v11, :cond_5

    add-int/lit8 v3, v3, -0xb

    invoke-virtual {v5, v3, v7, v14, v11}, Lf/h/f/n/b;->j(IIII)V

    invoke-virtual {v5, v7, v3, v11, v14}, Lf/h/f/n/b;->j(IIII)V

    :cond_5
    iget v3, v4, Lf/h/f/s/b/j;->d:I

    new-array v9, v3, [B

    add-int/lit8 v15, v6, -0x1

    move v8, v15

    const/4 v12, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    :goto_2
    const/4 v14, 0x2

    if-lez v8, :cond_d

    if-ne v8, v11, :cond_6

    add-int/lit8 v8, v8, -0x1

    :cond_6
    const/4 v11, 0x0

    :goto_3
    if-ge v11, v6, :cond_c

    if-eqz v16, :cond_7

    sub-int v19, v15, v11

    move/from16 v13, v19

    goto :goto_4

    :cond_7
    move v13, v11

    :goto_4
    if-ge v7, v14, :cond_b

    sub-int v14, v8, v7

    invoke-virtual {v5, v14, v13}, Lf/h/f/n/b;->e(II)Z

    move-result v20

    if-nez v20, :cond_a

    add-int/lit8 v10, v17, 0x1

    shl-int/lit8 v17, v18, 0x1

    move-object/from16 v21, v5

    iget-object v5, v0, Lf/h/f/s/b/a;->a:Lf/h/f/n/b;

    invoke-virtual {v5, v14, v13}, Lf/h/f/n/b;->e(II)Z

    move-result v5

    if-eqz v5, :cond_8

    or-int/lit8 v5, v17, 0x1

    goto :goto_5

    :cond_8
    move/from16 v5, v17

    :goto_5
    const/16 v14, 0x8

    if-ne v10, v14, :cond_9

    add-int/lit8 v10, v12, 0x1

    int-to-byte v5, v5

    aput-byte v5, v9, v12

    move v12, v10

    const/16 v17, 0x0

    const/16 v18, 0x0

    goto :goto_6

    :cond_9
    move/from16 v18, v5

    move/from16 v17, v10

    goto :goto_6

    :cond_a
    move-object/from16 v21, v5

    :goto_6
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v5, v21

    const/16 v10, 0x8

    const/4 v14, 0x2

    goto :goto_4

    :cond_b
    move-object/from16 v21, v5

    add-int/lit8 v11, v11, 0x1

    const/4 v7, 0x0

    const/16 v10, 0x8

    const/4 v13, 0x1

    const/4 v14, 0x2

    goto :goto_3

    :cond_c
    move-object/from16 v21, v5

    xor-int/lit8 v16, v16, 0x1

    add-int/lit8 v8, v8, -0x2

    const/4 v7, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x6

    const/4 v13, 0x1

    goto :goto_2

    :cond_d
    iget v0, v4, Lf/h/f/s/b/j;->d:I

    if-ne v12, v0, :cond_3a

    iget v0, v1, Lf/h/f/s/b/j;->d:I

    if-ne v3, v0, :cond_39

    iget-object v0, v1, Lf/h/f/s/b/j;->c:[Lf/h/f/s/b/j$b;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget-object v0, v0, v3

    iget-object v3, v0, Lf/h/f/s/b/j$b;->b:[Lf/h/f/s/b/j$a;

    array-length v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_7
    if-ge v5, v4, :cond_e

    aget-object v7, v3, v5

    iget v7, v7, Lf/h/f/s/b/j$a;->a:I

    add-int/2addr v6, v7

    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_e
    new-array v4, v6, [Lf/h/f/s/b/b;

    array-length v5, v3

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_8
    if-ge v7, v5, :cond_10

    aget-object v10, v3, v7

    const/4 v11, 0x0

    :goto_9
    iget v12, v10, Lf/h/f/s/b/j$a;->a:I

    if-ge v11, v12, :cond_f

    iget v12, v10, Lf/h/f/s/b/j$a;->b:I

    iget v13, v0, Lf/h/f/s/b/j$b;->a:I

    add-int/2addr v13, v12

    add-int/lit8 v14, v8, 0x1

    new-instance v15, Lf/h/f/s/b/b;

    new-array v13, v13, [B

    invoke-direct {v15, v12, v13}, Lf/h/f/s/b/b;-><init>(I[B)V

    aput-object v15, v4, v8

    add-int/lit8 v11, v11, 0x1

    move v8, v14

    goto :goto_9

    :cond_f
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    :cond_10
    const/4 v7, 0x0

    aget-object v3, v4, v7

    iget-object v3, v3, Lf/h/f/s/b/b;->b:[B

    array-length v3, v3

    add-int/lit8 v5, v6, -0x1

    :goto_a
    if-ltz v5, :cond_11

    aget-object v7, v4, v5

    iget-object v7, v7, Lf/h/f/s/b/b;->b:[B

    array-length v7, v7

    if-eq v7, v3, :cond_11

    add-int/lit8 v5, v5, -0x1

    goto :goto_a

    :cond_11
    const/4 v7, 0x1

    add-int/2addr v5, v7

    iget v0, v0, Lf/h/f/s/b/j$b;->a:I

    sub-int/2addr v3, v0

    const/4 v0, 0x0

    const/4 v7, 0x0

    :goto_b
    if-ge v7, v3, :cond_13

    const/4 v10, 0x0

    :goto_c
    if-ge v10, v8, :cond_12

    aget-object v11, v4, v10

    iget-object v11, v11, Lf/h/f/s/b/b;->b:[B

    add-int/lit8 v12, v0, 0x1

    aget-byte v0, v9, v0

    aput-byte v0, v11, v7

    add-int/lit8 v10, v10, 0x1

    move v0, v12

    goto :goto_c

    :cond_12
    add-int/lit8 v7, v7, 0x1

    goto :goto_b

    :cond_13
    move v7, v5

    :goto_d
    if-ge v7, v8, :cond_14

    aget-object v10, v4, v7

    iget-object v10, v10, Lf/h/f/s/b/b;->b:[B

    add-int/lit8 v11, v0, 0x1

    aget-byte v0, v9, v0

    aput-byte v0, v10, v3

    add-int/lit8 v7, v7, 0x1

    move v0, v11

    goto :goto_d

    :cond_14
    const/4 v7, 0x0

    aget-object v10, v4, v7

    iget-object v10, v10, Lf/h/f/s/b/b;->b:[B

    array-length v10, v10

    :goto_e
    if-ge v3, v10, :cond_17

    const/4 v11, 0x0

    :goto_f
    if-ge v11, v8, :cond_16

    if-ge v11, v5, :cond_15

    move v12, v3

    goto :goto_10

    :cond_15
    add-int/lit8 v12, v3, 0x1

    :goto_10
    aget-object v13, v4, v11

    iget-object v13, v13, Lf/h/f/s/b/b;->b:[B

    add-int/lit8 v14, v0, 0x1

    aget-byte v0, v9, v0

    aput-byte v0, v13, v12

    add-int/lit8 v11, v11, 0x1

    move v0, v14

    goto :goto_f

    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    :cond_17
    const/4 v0, 0x0

    const/4 v3, 0x0

    :goto_11
    if-ge v0, v6, :cond_18

    aget-object v5, v4, v0

    iget v5, v5, Lf/h/f/s/b/b;->a:I

    add-int/2addr v3, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_18
    new-array v9, v3, [B

    const/4 v0, 0x0

    const/4 v3, 0x0

    :goto_12
    if-ge v0, v6, :cond_1c

    aget-object v5, v4, v0

    iget-object v8, v5, Lf/h/f/s/b/b;->b:[B

    iget v5, v5, Lf/h/f/s/b/b;->a:I

    array-length v10, v8

    new-array v11, v10, [I

    const/4 v12, 0x0

    :goto_13
    if-ge v12, v10, :cond_19

    aget-byte v13, v8, v12

    and-int/lit16 v13, v13, 0xff

    aput v13, v11, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_13

    :cond_19
    move-object/from16 v15, p0

    :try_start_0
    iget-object v10, v15, Lf/h/f/s/b/e;->a:Lf/h/f/n/l/c;

    array-length v12, v8

    sub-int/2addr v12, v5

    invoke-virtual {v10, v11, v12}, Lf/h/f/n/l/c;->a([II)V
    :try_end_0
    .catch Lcom/google/zxing/common/reedsolomon/ReedSolomonException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v10, 0x0

    :goto_14
    if-ge v10, v5, :cond_1a

    aget v12, v11, v10

    int-to-byte v12, v12

    aput-byte v12, v8, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_14

    :cond_1a
    const/4 v10, 0x0

    :goto_15
    if-ge v10, v5, :cond_1b

    add-int/lit8 v11, v3, 0x1

    aget-byte v12, v8, v10

    aput-byte v12, v9, v3

    add-int/lit8 v10, v10, 0x1

    move v3, v11

    goto :goto_15

    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :catch_0
    invoke-static {}, Lcom/google/zxing/ChecksumException;->a()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_1c
    move-object/from16 v15, p0

    sget-object v0, Lf/h/f/s/b/d;->a:[C

    sget-object v0, Lf/h/f/s/b/h;->d:Lf/h/f/s/b/h;

    new-instance v3, Lf/h/f/n/c;

    invoke-direct {v3, v9}, Lf/h/f/n/c;-><init>([B)V

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x32

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v8, -0x1

    const/4 v10, -0x1

    const/4 v11, 0x0

    :goto_16
    :try_start_1
    invoke-virtual {v3}, Lf/h/f/n/c;->a()I

    move-result v12

    const/4 v13, 0x7

    const/4 v14, 0x4

    if-ge v12, v14, :cond_1d

    goto :goto_17

    :cond_1d
    invoke-virtual {v3, v14}, Lf/h/f/n/c;->b(I)I

    move-result v12

    if-eqz v12, :cond_27

    const/4 v6, 0x1

    if-eq v12, v6, :cond_26

    const/4 v6, 0x2

    if-eq v12, v6, :cond_25

    const/4 v6, 0x3

    if-eq v12, v6, :cond_24

    if-eq v12, v14, :cond_23

    const/4 v6, 0x5

    if-eq v12, v6, :cond_22

    if-eq v12, v13, :cond_21

    const/16 v6, 0x8

    if-eq v12, v6, :cond_20

    const/16 v6, 0x9

    if-eq v12, v6, :cond_1f

    const/16 v6, 0xd

    if-ne v12, v6, :cond_1e

    sget-object v6, Lf/h/f/s/b/h;->m:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_1e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1f
    sget-object v6, Lf/h/f/s/b/h;->l:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_20
    sget-object v6, Lf/h/f/s/b/h;->j:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_21
    sget-object v6, Lf/h/f/s/b/h;->i:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_22
    sget-object v6, Lf/h/f/s/b/h;->k:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_23
    sget-object v6, Lf/h/f/s/b/h;->h:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_24
    sget-object v6, Lf/h/f/s/b/h;->g:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_25
    sget-object v6, Lf/h/f/s/b/h;->f:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_26
    sget-object v6, Lf/h/f/s/b/h;->e:Lf/h/f/s/b/h;

    goto :goto_18

    :cond_27
    :goto_17
    move-object v6, v0

    :goto_18
    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v12

    if-eqz v12, :cond_36

    const/4 v14, 0x3

    if-eq v12, v14, :cond_34

    const/4 v14, 0x5

    if-eq v12, v14, :cond_2f

    if-eq v12, v13, :cond_2e

    const/16 v13, 0x8

    if-eq v12, v13, :cond_2e

    const/16 v13, 0x9

    if-eq v12, v13, :cond_2c

    invoke-virtual {v6, v1}, Lf/h/f/s/b/h;->f(Lf/h/f/s/b/j;)I

    move-result v12

    invoke-virtual {v3, v12}, Lf/h/f/n/c;->b(I)I

    move-result v12

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v13

    const/4 v14, 0x1

    if-eq v13, v14, :cond_2b

    const/4 v14, 0x2

    if-eq v13, v14, :cond_2a

    const/4 v14, 0x4

    if-eq v13, v14, :cond_29

    const/4 v14, 0x6

    if-ne v13, v14, :cond_28

    invoke-static {v3, v4, v12}, Lf/h/f/s/b/d;->d(Lf/h/f/n/c;Ljava/lang/StringBuilder;I)V

    goto/16 :goto_1b

    :cond_28
    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_29
    const/4 v14, 0x6

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move/from16 v23, v12

    move-object/from16 v24, v11

    move-object/from16 v25, v5

    move-object/from16 v26, p2

    invoke-static/range {v21 .. v26}, Lf/h/f/s/b/d;->b(Lf/h/f/n/c;Ljava/lang/StringBuilder;ILf/h/f/n/d;Ljava/util/Collection;Ljava/util/Map;)V

    goto/16 :goto_1b

    :cond_2a
    const/4 v14, 0x6

    invoke-static {v3, v4, v12, v7}, Lf/h/f/s/b/d;->a(Lf/h/f/n/c;Ljava/lang/StringBuilder;IZ)V

    goto/16 :goto_1b

    :cond_2b
    const/4 v14, 0x6

    invoke-static {v3, v4, v12}, Lf/h/f/s/b/d;->e(Lf/h/f/n/c;Ljava/lang/StringBuilder;I)V

    goto/16 :goto_1b

    :cond_2c
    const/4 v12, 0x4

    const/4 v14, 0x6

    invoke-virtual {v3, v12}, Lf/h/f/n/c;->b(I)I

    move-result v12

    invoke-virtual {v6, v1}, Lf/h/f/s/b/h;->f(Lf/h/f/s/b/j;)I

    move-result v13

    invoke-virtual {v3, v13}, Lf/h/f/n/c;->b(I)I

    move-result v13

    const/4 v14, 0x1

    if-ne v12, v14, :cond_2d

    invoke-static {v3, v4, v13}, Lf/h/f/s/b/d;->c(Lf/h/f/n/c;Ljava/lang/StringBuilder;I)V

    :cond_2d
    const/16 v12, 0x8

    goto :goto_1c

    :cond_2e
    const/4 v14, 0x1

    const/4 v7, 0x1

    goto :goto_1a

    :cond_2f
    const/16 v11, 0x8

    const/4 v14, 0x1

    invoke-virtual {v3, v11}, Lf/h/f/n/c;->b(I)I

    move-result v12

    and-int/lit16 v11, v12, 0x80

    if-nez v11, :cond_30

    and-int/lit8 v11, v12, 0x7f

    goto :goto_19

    :cond_30
    and-int/lit16 v11, v12, 0xc0

    const/16 v13, 0x80

    if-ne v11, v13, :cond_31

    const/16 v11, 0x8

    invoke-virtual {v3, v11}, Lf/h/f/n/c;->b(I)I

    move-result v13

    and-int/lit8 v12, v12, 0x3f

    shl-int/2addr v12, v11

    or-int v11, v12, v13

    goto :goto_19

    :cond_31
    and-int/lit16 v11, v12, 0xe0

    const/16 v13, 0xc0

    if-ne v11, v13, :cond_33

    const/16 v11, 0x10

    invoke-virtual {v3, v11}, Lf/h/f/n/c;->b(I)I

    move-result v13

    and-int/lit8 v12, v12, 0x1f

    shl-int/lit8 v11, v12, 0x10

    or-int/2addr v11, v13

    :goto_19
    invoke-static {v11}, Lf/h/f/n/d;->f(I)Lf/h/f/n/d;

    move-result-object v11

    if-eqz v11, :cond_32

    :goto_1a
    move v13, v8

    move/from16 v16, v10

    const/16 v12, 0x8

    goto :goto_1d

    :cond_32
    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_33
    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_34
    const/4 v14, 0x1

    invoke-virtual {v3}, Lf/h/f/n/c;->a()I

    move-result v8

    const/16 v10, 0x10

    if-lt v8, v10, :cond_35

    const/16 v12, 0x8

    invoke-virtual {v3, v12}, Lf/h/f/n/c;->b(I)I

    move-result v8

    invoke-virtual {v3, v12}, Lf/h/f/n/c;->b(I)I

    move-result v10

    goto :goto_1c

    :cond_35
    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_36
    :goto_1b
    const/16 v12, 0x8

    const/4 v14, 0x1

    :goto_1c
    move v13, v8

    move/from16 v16, v10

    :goto_1d
    if-ne v6, v0, :cond_38

    new-instance v0, Lf/h/f/n/e;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_37

    const/4 v11, 0x0

    goto :goto_1e

    :cond_37
    move-object v11, v5

    :goto_1e
    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v12

    move-object v8, v0

    move/from16 v14, v16

    invoke-direct/range {v8 .. v14}, Lf/h/f/n/e;-><init>([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;II)V

    return-object v0

    :cond_38
    move v8, v13

    move/from16 v10, v16

    goto/16 :goto_16

    :catch_1
    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_39
    move-object/from16 v15, p0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3a
    move-object/from16 v15, p0

    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method
