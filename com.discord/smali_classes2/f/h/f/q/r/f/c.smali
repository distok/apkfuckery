.class public final Lf/h/f/q/r/f/c;
.super Lf/h/f/q/r/a;
.source "RSSExpandedReader.java"


# static fields
.field public static final k:[I

.field public static final l:[I

.field public static final m:[I

.field public static final n:[[I

.field public static final o:[[I

.field public static final p:[[I


# instance fields
.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/a;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/b;",
            ">;"
        }
    .end annotation
.end field

.field public final i:[I

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x5

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lf/h/f/q/r/f/c;->k:[I

    new-array v1, v0, [I

    fill-array-data v1, :array_1

    sput-object v1, Lf/h/f/q/r/f/c;->l:[I

    new-array v1, v0, [I

    fill-array-data v1, :array_2

    sput-object v1, Lf/h/f/q/r/f/c;->m:[I

    const/4 v1, 0x6

    new-array v2, v1, [[I

    const/4 v3, 0x4

    new-array v4, v3, [I

    fill-array-data v4, :array_3

    const/4 v5, 0x0

    aput-object v4, v2, v5

    new-array v4, v3, [I

    fill-array-data v4, :array_4

    const/4 v6, 0x1

    aput-object v4, v2, v6

    new-array v4, v3, [I

    fill-array-data v4, :array_5

    const/4 v7, 0x2

    aput-object v4, v2, v7

    new-array v4, v3, [I

    fill-array-data v4, :array_6

    const/4 v8, 0x3

    aput-object v4, v2, v8

    new-array v4, v3, [I

    fill-array-data v4, :array_7

    aput-object v4, v2, v3

    new-array v4, v3, [I

    fill-array-data v4, :array_8

    aput-object v4, v2, v0

    sput-object v2, Lf/h/f/q/r/f/c;->n:[[I

    const/16 v2, 0x17

    new-array v2, v2, [[I

    const/16 v4, 0x8

    new-array v9, v4, [I

    fill-array-data v9, :array_9

    aput-object v9, v2, v5

    new-array v9, v4, [I

    fill-array-data v9, :array_a

    aput-object v9, v2, v6

    new-array v9, v4, [I

    fill-array-data v9, :array_b

    aput-object v9, v2, v7

    new-array v9, v4, [I

    fill-array-data v9, :array_c

    aput-object v9, v2, v8

    new-array v9, v4, [I

    fill-array-data v9, :array_d

    aput-object v9, v2, v3

    new-array v9, v4, [I

    fill-array-data v9, :array_e

    aput-object v9, v2, v0

    new-array v9, v4, [I

    fill-array-data v9, :array_f

    aput-object v9, v2, v1

    new-array v9, v4, [I

    fill-array-data v9, :array_10

    const/4 v10, 0x7

    aput-object v9, v2, v10

    new-array v9, v4, [I

    fill-array-data v9, :array_11

    aput-object v9, v2, v4

    new-array v9, v4, [I

    fill-array-data v9, :array_12

    const/16 v11, 0x9

    aput-object v9, v2, v11

    new-array v9, v4, [I

    fill-array-data v9, :array_13

    const/16 v12, 0xa

    aput-object v9, v2, v12

    new-array v9, v4, [I

    fill-array-data v9, :array_14

    const/16 v13, 0xb

    aput-object v9, v2, v13

    new-array v9, v4, [I

    fill-array-data v9, :array_15

    const/16 v14, 0xc

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_16

    const/16 v14, 0xd

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_17

    const/16 v14, 0xe

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_18

    const/16 v14, 0xf

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_19

    const/16 v14, 0x10

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_1a

    const/16 v14, 0x11

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_1b

    const/16 v14, 0x12

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_1c

    const/16 v14, 0x13

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_1d

    const/16 v14, 0x14

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_1e

    const/16 v14, 0x15

    aput-object v9, v2, v14

    new-array v9, v4, [I

    fill-array-data v9, :array_1f

    const/16 v14, 0x16

    aput-object v9, v2, v14

    sput-object v2, Lf/h/f/q/r/f/c;->o:[[I

    new-array v2, v12, [[I

    new-array v9, v7, [I

    fill-array-data v9, :array_20

    aput-object v9, v2, v5

    new-array v5, v8, [I

    fill-array-data v5, :array_21

    aput-object v5, v2, v6

    new-array v5, v3, [I

    fill-array-data v5, :array_22

    aput-object v5, v2, v7

    new-array v5, v0, [I

    fill-array-data v5, :array_23

    aput-object v5, v2, v8

    new-array v5, v1, [I

    fill-array-data v5, :array_24

    aput-object v5, v2, v3

    new-array v3, v10, [I

    fill-array-data v3, :array_25

    aput-object v3, v2, v0

    new-array v0, v4, [I

    fill-array-data v0, :array_26

    aput-object v0, v2, v1

    new-array v0, v11, [I

    fill-array-data v0, :array_27

    aput-object v0, v2, v10

    new-array v0, v12, [I

    fill-array-data v0, :array_28

    aput-object v0, v2, v4

    new-array v0, v13, [I

    fill-array-data v0, :array_29

    aput-object v0, v2, v11

    sput-object v2, Lf/h/f/q/r/f/c;->p:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7
        0x5
        0x4
        0x3
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x4
        0x14
        0x34
        0x68
        0xcc
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x15c
        0x56c
        0xb84
        0xf94
    .end array-data

    :array_3
    .array-data 4
        0x1
        0x8
        0x4
        0x1
    .end array-data

    :array_4
    .array-data 4
        0x3
        0x6
        0x4
        0x1
    .end array-data

    :array_5
    .array-data 4
        0x3
        0x4
        0x6
        0x1
    .end array-data

    :array_6
    .array-data 4
        0x3
        0x2
        0x8
        0x1
    .end array-data

    :array_7
    .array-data 4
        0x2
        0x6
        0x5
        0x1
    .end array-data

    :array_8
    .array-data 4
        0x2
        0x2
        0x9
        0x1
    .end array-data

    :array_9
    .array-data 4
        0x1
        0x3
        0x9
        0x1b
        0x51
        0x20
        0x60
        0x4d
    .end array-data

    :array_a
    .array-data 4
        0x14
        0x3c
        0xb4
        0x76
        0x8f
        0x7
        0x15
        0x3f
    .end array-data

    :array_b
    .array-data 4
        0xbd
        0x91
        0xd
        0x27
        0x75
        0x8c
        0xd1
        0xcd
    .end array-data

    :array_c
    .array-data 4
        0xc1
        0x9d
        0x31
        0x93
        0x13
        0x39
        0xab
        0x5b
    .end array-data

    :array_d
    .array-data 4
        0x3e
        0xba
        0x88
        0xc5
        0xa9
        0x55
        0x2c
        0x84
    .end array-data

    :array_e
    .array-data 4
        0xb9
        0x85
        0xbc
        0x8e
        0x4
        0xc
        0x24
        0x6c
    .end array-data

    :array_f
    .array-data 4
        0x71
        0x80
        0xad
        0x61
        0x50
        0x1d
        0x57
        0x32
    .end array-data

    :array_10
    .array-data 4
        0x96
        0x1c
        0x54
        0x29
        0x7b
        0x9e
        0x34
        0x9c
    .end array-data

    :array_11
    .array-data 4
        0x2e
        0x8a
        0xcb
        0xbb
        0x8b
        0xce
        0xc4
        0xa6
    .end array-data

    :array_12
    .array-data 4
        0x4c
        0x11
        0x33
        0x99
        0x25
        0x6f
        0x7a
        0x9b
    .end array-data

    :array_13
    .array-data 4
        0x2b
        0x81
        0xb0
        0x6a
        0x6b
        0x6e
        0x77
        0x92
    .end array-data

    :array_14
    .array-data 4
        0x10
        0x30
        0x90
        0xa
        0x1e
        0x5a
        0x3b
        0xb1
    .end array-data

    :array_15
    .array-data 4
        0x6d
        0x74
        0x89
        0xc8
        0xb2
        0x70
        0x7d
        0xa4
    .end array-data

    :array_16
    .array-data 4
        0x46
        0xd2
        0xd0
        0xca
        0xb8
        0x82
        0xb3
        0x73
    .end array-data

    :array_17
    .array-data 4
        0x86
        0xbf
        0x97
        0x1f
        0x5d
        0x44
        0xcc
        0xbe
    .end array-data

    :array_18
    .array-data 4
        0x94
        0x16
        0x42
        0xc6
        0xac
        0x5e
        0x47
        0x2
    .end array-data

    :array_19
    .array-data 4
        0x6
        0x12
        0x36
        0xa2
        0x40
        0xc0
        0x9a
        0x28
    .end array-data

    :array_1a
    .array-data 4
        0x78
        0x95
        0x19
        0x4b
        0xe
        0x2a
        0x7e
        0xa7
    .end array-data

    :array_1b
    .array-data 4
        0x4f
        0x1a
        0x4e
        0x17
        0x45
        0xcf
        0xc7
        0xaf
    .end array-data

    :array_1c
    .array-data 4
        0x67
        0x62
        0x53
        0x26
        0x72
        0x83
        0xb6
        0x7c
    .end array-data

    :array_1d
    .array-data 4
        0xa1
        0x3d
        0xb7
        0x7f
        0xaa
        0x58
        0x35
        0x9f
    .end array-data

    :array_1e
    .array-data 4
        0x37
        0xa5
        0x49
        0x8
        0x18
        0x48
        0x5
        0xf
    .end array-data

    :array_1f
    .array-data 4
        0x2d
        0x87
        0xc2
        0xa0
        0x3a
        0xae
        0x64
        0x59
    .end array-data

    :array_20
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_21
    .array-data 4
        0x0
        0x1
        0x1
    .end array-data

    :array_22
    .array-data 4
        0x0
        0x2
        0x1
        0x3
    .end array-data

    :array_23
    .array-data 4
        0x0
        0x4
        0x1
        0x3
        0x2
    .end array-data

    :array_24
    .array-data 4
        0x0
        0x4
        0x1
        0x3
        0x3
        0x5
    .end array-data

    :array_25
    .array-data 4
        0x0
        0x4
        0x1
        0x3
        0x4
        0x5
        0x5
    .end array-data

    :array_26
    .array-data 4
        0x0
        0x0
        0x1
        0x1
        0x2
        0x2
        0x3
        0x3
    .end array-data

    :array_27
    .array-data 4
        0x0
        0x0
        0x1
        0x1
        0x2
        0x2
        0x3
        0x4
        0x4
    .end array-data

    :array_28
    .array-data 4
        0x0
        0x0
        0x1
        0x1
        0x2
        0x2
        0x3
        0x4
        0x5
        0x5
    .end array-data

    :array_29
    .array-data 4
        0x0
        0x0
        0x1
        0x1
        0x2
        0x3
        0x3
        0x4
        0x4
        0x5
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lf/h/f/q/r/a;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lf/h/f/q/r/f/c;->i:[I

    return-void
.end method

.method public static n(Ljava/util/List;)Lcom/google/zxing/Result;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/a;",
            ">;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    shl-int/2addr v0, v1

    sub-int/2addr v0, v1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/f/q/r/f/a;

    iget-object v2, v2, Lf/h/f/q/r/f/a;->b:Lf/h/f/q/r/b;

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    const/16 v2, 0xc

    mul-int/lit8 v0, v0, 0xc

    new-instance v3, Lf/h/f/n/a;

    invoke-direct {v3, v0}, Lf/h/f/n/a;-><init>(I)V

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/f/q/r/f/a;

    iget-object v4, v4, Lf/h/f/q/r/f/a;->b:Lf/h/f/q/r/b;

    iget v4, v4, Lf/h/f/q/r/b;->a:I

    const/16 v5, 0xb

    const/4 v6, 0x0

    :goto_0
    if-ltz v5, :cond_2

    shl-int v7, v1, v5

    and-int/2addr v7, v4

    if-eqz v7, :cond_1

    invoke-virtual {v3, v6}, Lf/h/f/n/a;->i(I)V

    :cond_1
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v5, v5, -0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x1

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_7

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/f/q/r/f/a;

    iget-object v7, v5, Lf/h/f/q/r/f/a;->a:Lf/h/f/q/r/b;

    iget v7, v7, Lf/h/f/q/r/b;->a:I

    const/16 v8, 0xb

    :goto_2
    if-ltz v8, :cond_4

    shl-int v9, v1, v8

    and-int/2addr v9, v7

    if-eqz v9, :cond_3

    invoke-virtual {v3, v6}, Lf/h/f/n/a;->i(I)V

    :cond_3
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    :cond_4
    iget-object v5, v5, Lf/h/f/q/r/f/a;->b:Lf/h/f/q/r/b;

    if-eqz v5, :cond_6

    iget v5, v5, Lf/h/f/q/r/b;->a:I

    const/16 v7, 0xb

    :goto_3
    if-ltz v7, :cond_6

    shl-int v8, v1, v7

    and-int/2addr v8, v5

    if-eqz v8, :cond_5

    invoke-virtual {v3, v6}, Lf/h/f/n/a;->i(I)V

    :cond_5
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v7, v7, -0x1

    goto :goto_3

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_7
    invoke-virtual {v3, v1}, Lf/h/f/n/a;->b(I)Z

    move-result v4

    const/4 v5, 0x4

    const/4 v6, 0x2

    if-eqz v4, :cond_8

    new-instance v2, Lf/h/f/q/r/f/d/g;

    invoke-direct {v2, v3}, Lf/h/f/q/r/f/d/g;-><init>(Lf/h/f/n/a;)V

    goto/16 :goto_4

    :cond_8
    invoke-virtual {v3, v6}, Lf/h/f/n/a;->b(I)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v2, Lf/h/f/q/r/f/d/k;

    invoke-direct {v2, v3}, Lf/h/f/q/r/f/d/k;-><init>(Lf/h/f/n/a;)V

    goto/16 :goto_4

    :cond_9
    invoke-static {v3, v1, v5}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v4

    if-eq v4, v5, :cond_d

    const/4 v7, 0x5

    if-eq v4, v7, :cond_c

    invoke-static {v3, v1, v7}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v4

    if-eq v4, v2, :cond_b

    const/16 v2, 0xd

    if-eq v4, v2, :cond_a

    const/4 v2, 0x7

    invoke-static {v3, v1, v2}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v2

    const-string v4, "17"

    const-string v7, "15"

    const-string v8, "13"

    const-string v9, "11"

    const-string v10, "320"

    const-string v11, "310"

    packed-switch v2, :pswitch_data_0

    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "unknown decoder: "

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_0
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v10, v4}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_1
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v11, v4}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_2
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v10, v7}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_3
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v11, v7}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_4
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v10, v8}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_5
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v11, v8}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_6
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v10, v9}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_7
    new-instance v2, Lf/h/f/q/r/f/d/e;

    invoke-direct {v2, v3, v11, v9}, Lf/h/f/q/r/f/d/e;-><init>(Lf/h/f/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_a
    new-instance v2, Lf/h/f/q/r/f/d/d;

    invoke-direct {v2, v3}, Lf/h/f/q/r/f/d/d;-><init>(Lf/h/f/n/a;)V

    goto :goto_4

    :cond_b
    new-instance v2, Lf/h/f/q/r/f/d/c;

    invoke-direct {v2, v3}, Lf/h/f/q/r/f/d/c;-><init>(Lf/h/f/n/a;)V

    goto :goto_4

    :cond_c
    new-instance v2, Lf/h/f/q/r/f/d/b;

    invoke-direct {v2, v3}, Lf/h/f/q/r/f/d/b;-><init>(Lf/h/f/n/a;)V

    goto :goto_4

    :cond_d
    new-instance v2, Lf/h/f/q/r/f/d/a;

    invoke-direct {v2, v3}, Lf/h/f/q/r/f/d/a;-><init>(Lf/h/f/n/a;)V

    :goto_4
    invoke-virtual {v2}, Lf/h/f/q/r/f/d/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/f/q/r/f/a;

    iget-object v3, v3, Lf/h/f/q/r/f/a;->c:Lf/h/f/q/r/c;

    iget-object v3, v3, Lf/h/f/q/r/c;->c:[Lf/h/f/k;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lf/h/f/q/r/f/a;

    iget-object p0, p0, Lf/h/f/q/r/f/a;->c:Lf/h/f/q/r/c;

    iget-object p0, p0, Lf/h/f/q/r/c;->c:[Lf/h/f/k;

    new-instance v4, Lcom/google/zxing/Result;

    const/4 v7, 0x0

    new-array v5, v5, [Lf/h/f/k;

    aget-object v8, v3, v0

    aput-object v8, v5, v0

    aget-object v3, v3, v1

    aput-object v3, v5, v1

    aget-object v0, p0, v0

    aput-object v0, v5, v6

    const/4 v0, 0x3

    aget-object p0, p0, v1

    aput-object p0, v5, v0

    sget-object p0, Lf/h/f/a;->q:Lf/h/f/a;

    invoke-direct {v4, v2, v7, v5, p0}, Lcom/google/zxing/Result;-><init>(Ljava/lang/String;[B[Lf/h/f/k;Lf/h/f/a;)V

    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x38
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static r([I)V
    .locals 5

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    div-int/lit8 v2, v0, 0x2

    if-ge v1, v2, :cond_0

    aget v2, p0, v1

    sub-int v3, v0, v1

    add-int/lit8 v3, v3, -0x1

    aget v4, p0, v3

    aput v4, p0, v1

    aput v2, p0, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public b(ILf/h/f/n/a;Ljava/util/Map;)Lcom/google/zxing/Result;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/f/n/a;",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    iget-object p3, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    const/4 p3, 0x0

    iput-boolean p3, p0, Lf/h/f/q/r/f/c;->j:Z

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lf/h/f/q/r/f/c;->p(ILf/h/f/n/a;)Ljava/util/List;

    move-result-object p3

    invoke-static {p3}, Lf/h/f/q/r/f/c;->n(Ljava/util/List;)Lcom/google/zxing/Result;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    iget-object p3, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->clear()V

    const/4 p3, 0x1

    iput-boolean p3, p0, Lf/h/f/q/r/f/c;->j:Z

    invoke-virtual {p0, p1, p2}, Lf/h/f/q/r/f/c;->p(ILf/h/f/n/a;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lf/h/f/q/r/f/c;->n(Ljava/util/List;)Lcom/google/zxing/Result;

    move-result-object p1

    return-object p1
.end method

.method public final k()Z
    .locals 8

    iget-object v0, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/f/q/r/f/a;

    iget-object v2, v0, Lf/h/f/q/r/f/a;->a:Lf/h/f/q/r/b;

    iget-object v0, v0, Lf/h/f/q/r/f/a;->b:Lf/h/f/q/r/b;

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v0, v0, Lf/h/f/q/r/b;->b:I

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x1

    :goto_0
    iget-object v6, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    iget-object v6, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/f/q/r/f/a;

    iget-object v7, v6, Lf/h/f/q/r/f/a;->a:Lf/h/f/q/r/b;

    iget v7, v7, Lf/h/f/q/r/b;->b:I

    add-int/2addr v0, v7

    add-int/lit8 v3, v3, 0x1

    iget-object v6, v6, Lf/h/f/q/r/f/a;->b:Lf/h/f/q/r/b;

    if-eqz v6, :cond_1

    iget v6, v6, Lf/h/f/q/r/b;->b:I

    add-int/2addr v0, v6

    add-int/lit8 v3, v3, 0x1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    rem-int/lit16 v0, v0, 0xd3

    add-int/lit8 v3, v3, -0x4

    mul-int/lit16 v3, v3, 0xd3

    add-int/2addr v3, v0

    iget v0, v2, Lf/h/f/q/r/b;->a:I

    if-ne v3, v0, :cond_3

    return v4

    :cond_3
    return v1
.end method

.method public final l(Ljava/util/List;I)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/b;",
            ">;I)",
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    :goto_0
    iget-object v0, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_7

    iget-object v0, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/f/q/r/f/b;

    iget-object v1, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/f/q/r/f/b;

    iget-object v3, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    iget-object v2, v2, Lf/h/f/q/r/f/b;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    iget-object v2, v0, Lf/h/f/q/r/f/b;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    sget-object v2, Lf/h/f/q/r/f/c;->p:[[I

    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_2
    const/4 v6, 0x1

    if-ge v5, v3, :cond_4

    aget-object v7, v2, v5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    array-length v9, v7

    if-gt v8, v9, :cond_3

    const/4 v8, 0x0

    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_2

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/f/q/r/f/a;

    iget-object v9, v9, Lf/h/f/q/r/f/a;->c:Lf/h/f/q/r/c;

    iget v9, v9, Lf/h/f/q/r/c;->a:I

    aget v10, v7, v8

    if-eq v9, v10, :cond_1

    const/4 v7, 0x0

    goto :goto_4

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_2
    const/4 v7, 0x1

    :goto_4
    if-eqz v7, :cond_3

    const/4 v4, 0x1

    goto :goto_5

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_4
    :goto_5
    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lf/h/f/q/r/f/c;->k()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object p1, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    return-object p1

    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, p2, 0x1

    :try_start_0
    invoke-virtual {p0, v1, v0}, Lf/h/f/q/r/f/c;->l(Ljava/util/List;I)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    :cond_6
    add-int/lit8 p2, p2, 0x1

    goto/16 :goto_0

    :cond_7
    sget-object p1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw p1
.end method

.method public final m(Z)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x19

    if-le v0, v2, :cond_0

    iget-object p1, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-object v1

    :cond_0
    iget-object v0, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lf/h/f/q/r/f/c;->l(Ljava/util/List;I)Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-eqz p1, :cond_2

    iget-object p1, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_2
    return-object v1
.end method

.method public o(Lf/h/f/n/a;Lf/h/f/q/r/c;ZZ)Lf/h/f/q/r/b;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    sget-object v3, Lf/h/f/q/r/f/c;->o:[[I

    iget-object v4, v0, Lf/h/f/q/r/a;->b:[I

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    array-length v7, v4

    if-ge v6, v7, :cond_0

    aput v5, v4, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x1

    if-eqz p4, :cond_1

    iget-object v7, v2, Lf/h/f/q/r/c;->b:[I

    aget v7, v7, v5

    invoke-static {v1, v7, v4}, Lf/h/f/q/k;->f(Lf/h/f/n/a;I[I)V

    goto :goto_2

    :cond_1
    iget-object v7, v2, Lf/h/f/q/r/c;->b:[I

    aget v7, v7, v6

    invoke-static {v1, v7, v4}, Lf/h/f/q/k;->e(Lf/h/f/n/a;I[I)V

    array-length v1, v4

    sub-int/2addr v1, v6

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v1, :cond_2

    aget v8, v4, v7

    aget v9, v4, v1

    aput v9, v4, v7

    aput v8, v4, v1

    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    :goto_2
    invoke-static {v4}, Lf/h/a/f/f/n/g;->b0([I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v7, 0x41880000    # 17.0f

    div-float/2addr v1, v7

    iget-object v7, v2, Lf/h/f/q/r/c;->b:[I

    aget v8, v7, v6

    aget v7, v7, v5

    sub-int/2addr v8, v7

    int-to-float v7, v8

    const/high16 v8, 0x41700000    # 15.0f

    div-float/2addr v7, v8

    sub-float v8, v1, v7

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    div-float/2addr v8, v7

    const v7, 0x3e99999a    # 0.3f

    cmpl-float v8, v8, v7

    if-gtz v8, :cond_2c

    iget-object v8, v0, Lf/h/f/q/r/a;->e:[I

    iget-object v9, v0, Lf/h/f/q/r/a;->f:[I

    iget-object v10, v0, Lf/h/f/q/r/a;->c:[F

    iget-object v11, v0, Lf/h/f/q/r/a;->d:[F

    const/4 v12, 0x0

    :goto_3
    array-length v13, v4

    if-ge v12, v13, :cond_8

    const/high16 v13, 0x3f800000    # 1.0f

    aget v14, v4, v12

    int-to-float v14, v14

    mul-float v14, v14, v13

    div-float/2addr v14, v1

    const/high16 v13, 0x3f000000    # 0.5f

    add-float/2addr v13, v14

    float-to-int v13, v13

    const/16 v15, 0x8

    if-gtz v13, :cond_4

    cmpg-float v13, v14, v7

    if-ltz v13, :cond_3

    const/4 v13, 0x1

    goto :goto_4

    :cond_3
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_4
    if-le v13, v15, :cond_6

    const v13, 0x410b3333    # 8.7f

    cmpl-float v13, v14, v13

    if-gtz v13, :cond_5

    const/16 v13, 0x8

    goto :goto_4

    :cond_5
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_6
    :goto_4
    div-int/lit8 v15, v12, 0x2

    and-int/lit8 v16, v12, 0x1

    if-nez v16, :cond_7

    aput v13, v8, v15

    int-to-float v13, v13

    sub-float/2addr v14, v13

    aput v14, v10, v15

    goto :goto_5

    :cond_7
    aput v13, v9, v15

    int-to-float v13, v13

    sub-float/2addr v14, v13

    aput v14, v11, v15

    :goto_5
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_8
    iget-object v1, v0, Lf/h/f/q/r/a;->e:[I

    invoke-static {v1}, Lf/h/a/f/f/n/g;->b0([I)I

    move-result v1

    iget-object v4, v0, Lf/h/f/q/r/a;->f:[I

    invoke-static {v4}, Lf/h/a/f/f/n/g;->b0([I)I

    move-result v4

    const/4 v7, 0x4

    const/16 v10, 0xd

    if-le v1, v10, :cond_9

    const/4 v11, 0x0

    const/4 v12, 0x1

    goto :goto_7

    :cond_9
    if-ge v1, v7, :cond_a

    const/4 v11, 0x1

    goto :goto_6

    :cond_a
    const/4 v11, 0x0

    :goto_6
    const/4 v12, 0x0

    :goto_7
    if-le v4, v10, :cond_b

    const/4 v13, 0x0

    const/4 v14, 0x1

    goto :goto_9

    :cond_b
    if-ge v4, v7, :cond_c

    const/4 v13, 0x1

    goto :goto_8

    :cond_c
    const/4 v13, 0x0

    :goto_8
    const/4 v14, 0x0

    :goto_9
    add-int v15, v1, v4

    add-int/lit8 v15, v15, -0x11

    and-int/lit8 v5, v1, 0x1

    if-ne v5, v6, :cond_d

    const/4 v5, 0x1

    goto :goto_a

    :cond_d
    const/4 v5, 0x0

    :goto_a
    and-int/lit8 v17, v4, 0x1

    if-nez v17, :cond_e

    const/16 v17, 0x1

    goto :goto_b

    :cond_e
    const/16 v17, 0x0

    :goto_b
    if-ne v15, v6, :cond_12

    if-eqz v5, :cond_10

    if-nez v17, :cond_f

    goto :goto_d

    :cond_f
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_10
    if-eqz v17, :cond_11

    goto :goto_c

    :cond_11
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_12
    const/4 v10, -0x1

    if-ne v15, v10, :cond_16

    if-eqz v5, :cond_14

    if-nez v17, :cond_13

    const/4 v11, 0x1

    goto :goto_e

    :cond_13
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_14
    if-eqz v17, :cond_15

    const/4 v13, 0x1

    goto :goto_e

    :cond_15
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_16
    if-nez v15, :cond_2b

    if-eqz v5, :cond_19

    if-eqz v17, :cond_18

    if-ge v1, v4, :cond_17

    const/4 v11, 0x1

    :goto_c
    const/4 v14, 0x1

    goto :goto_e

    :cond_17
    const/4 v13, 0x1

    :goto_d
    const/4 v12, 0x1

    goto :goto_e

    :cond_18
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_19
    if-nez v17, :cond_2a

    :goto_e
    if-eqz v11, :cond_1b

    if-nez v12, :cond_1a

    iget-object v1, v0, Lf/h/f/q/r/a;->e:[I

    iget-object v4, v0, Lf/h/f/q/r/a;->c:[F

    invoke-static {v1, v4}, Lf/h/f/q/r/a;->h([I[F)V

    goto :goto_f

    :cond_1a
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_1b
    :goto_f
    if-eqz v12, :cond_1c

    iget-object v1, v0, Lf/h/f/q/r/a;->e:[I

    iget-object v4, v0, Lf/h/f/q/r/a;->c:[F

    invoke-static {v1, v4}, Lf/h/f/q/r/a;->g([I[F)V

    :cond_1c
    if-eqz v13, :cond_1e

    if-nez v14, :cond_1d

    iget-object v1, v0, Lf/h/f/q/r/a;->f:[I

    iget-object v4, v0, Lf/h/f/q/r/a;->c:[F

    invoke-static {v1, v4}, Lf/h/f/q/r/a;->h([I[F)V

    goto :goto_10

    :cond_1d
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_1e
    :goto_10
    if-eqz v14, :cond_1f

    iget-object v1, v0, Lf/h/f/q/r/a;->f:[I

    iget-object v4, v0, Lf/h/f/q/r/a;->d:[F

    invoke-static {v1, v4}, Lf/h/f/q/r/a;->g([I[F)V

    :cond_1f
    iget v1, v2, Lf/h/f/q/r/c;->a:I

    mul-int/lit8 v1, v1, 0x4

    const/4 v4, 0x2

    if-eqz p3, :cond_20

    const/4 v5, 0x0

    goto :goto_11

    :cond_20
    const/4 v5, 0x2

    :goto_11
    add-int/2addr v1, v5

    xor-int/lit8 v5, p4, 0x1

    add-int/2addr v1, v5

    sub-int/2addr v1, v6

    array-length v5, v8

    sub-int/2addr v5, v6

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_12
    if-ltz v5, :cond_24

    iget v12, v2, Lf/h/f/q/r/c;->a:I

    if-nez v12, :cond_22

    if-eqz p3, :cond_22

    if-nez p4, :cond_21

    goto :goto_13

    :cond_21
    const/4 v12, 0x0

    goto :goto_14

    :cond_22
    :goto_13
    const/4 v12, 0x1

    :goto_14
    if-eqz v12, :cond_23

    aget-object v12, v3, v1

    mul-int/lit8 v13, v5, 0x2

    aget v12, v12, v13

    aget v13, v8, v5

    mul-int v13, v13, v12

    add-int/2addr v10, v13

    :cond_23
    aget v12, v8, v5

    add-int/2addr v11, v12

    add-int/lit8 v5, v5, -0x1

    goto :goto_12

    :cond_24
    array-length v5, v9

    sub-int/2addr v5, v6

    const/4 v12, 0x0

    :goto_15
    if-ltz v5, :cond_28

    iget v13, v2, Lf/h/f/q/r/c;->a:I

    if-nez v13, :cond_26

    if-eqz p3, :cond_26

    if-nez p4, :cond_25

    goto :goto_16

    :cond_25
    const/4 v13, 0x0

    goto :goto_17

    :cond_26
    :goto_16
    const/4 v13, 0x1

    :goto_17
    if-eqz v13, :cond_27

    aget-object v13, v3, v1

    mul-int/lit8 v14, v5, 0x2

    add-int/2addr v14, v6

    aget v13, v13, v14

    aget v14, v9, v5

    mul-int v14, v14, v13

    add-int/2addr v12, v14

    :cond_27
    add-int/lit8 v5, v5, -0x1

    goto :goto_15

    :cond_28
    add-int/2addr v10, v12

    and-int/lit8 v1, v11, 0x1

    if-nez v1, :cond_29

    const/16 v1, 0xd

    if-gt v11, v1, :cond_29

    if-lt v11, v7, :cond_29

    sub-int/2addr v1, v11

    div-int/2addr v1, v4

    sget-object v2, Lf/h/f/q/r/f/c;->k:[I

    aget v2, v2, v1

    rsub-int/lit8 v3, v2, 0x9

    invoke-static {v8, v2, v6}, Lf/h/a/f/f/n/g;->E([IIZ)I

    move-result v2

    const/4 v4, 0x0

    invoke-static {v9, v3, v4}, Lf/h/a/f/f/n/g;->E([IIZ)I

    move-result v3

    sget-object v4, Lf/h/f/q/r/f/c;->l:[I

    aget v4, v4, v1

    sget-object v5, Lf/h/f/q/r/f/c;->m:[I

    aget v1, v5, v1

    mul-int v2, v2, v4

    add-int/2addr v2, v3

    add-int/2addr v2, v1

    new-instance v1, Lf/h/f/q/r/b;

    invoke-direct {v1, v2, v10}, Lf/h/f/q/r/b;-><init>(II)V

    return-object v1

    :cond_29
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_2a
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_2b
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_2c
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1
.end method

.method public p(ILf/h/f/n/a;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/f/n/a;",
            ")",
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    :try_start_0
    iget-object v3, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-virtual {p0, p2, v3, p1}, Lf/h/f/q/r/f/c;->q(Lf/h/f/n/a;Ljava/util/List;I)Lf/h/f/q/r/f/a;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    iget-object v3, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    throw v2

    :cond_1
    invoke-virtual {p0}, Lf/h/f/q/r/f/c;->k()Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p1, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    return-object p1

    :cond_2
    iget-object p2, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    iget-object v4, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    iget-object v4, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/f/q/r/f/b;

    iget v5, v4, Lf/h/f/q/r/f/b;->b:I

    if-le v5, p1, :cond_3

    iget-object v5, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    iget-object v4, v4, Lf/h/f/q/r/f/b;->a:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    iget-object v4, v4, Lf/h/f/q/r/f/b;->a:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    :goto_2
    if-nez v4, :cond_12

    if-eqz v3, :cond_5

    goto/16 :goto_9

    :cond_5
    iget-object v3, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    iget-object v4, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/f/q/r/f/b;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/f/q/r/f/a;

    iget-object v8, v5, Lf/h/f/q/r/f/b;->a:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/f/q/r/f/a;

    invoke-virtual {v7, v9}, Lf/h/f/q/r/f/a;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    const/4 v7, 0x1

    goto :goto_3

    :cond_9
    const/4 v7, 0x0

    :goto_3
    if-nez v7, :cond_7

    const/4 v5, 0x0

    goto :goto_4

    :cond_a
    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_6

    const/4 v3, 0x1

    goto :goto_5

    :cond_b
    const/4 v3, 0x0

    :goto_5
    if-eqz v3, :cond_c

    goto :goto_9

    :cond_c
    iget-object v3, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    new-instance v4, Lf/h/f/q/r/f/b;

    iget-object v5, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-direct {v4, v5, p1, v1}, Lf/h/f/q/r/f/b;-><init>(Ljava/util/List;IZ)V

    invoke-interface {v3, v2, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object p1, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    iget-object v2, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/f/q/r/f/b;

    iget-object v4, v3, Lf/h/f/q/r/f/b;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_d

    iget-object v3, v3, Lf/h/f/q/r/f/b;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/f/q/r/f/a;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/f/q/r/f/a;

    invoke-virtual {v4, v6}, Lf/h/f/q/r/f/a;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    const/4 v4, 0x1

    goto :goto_7

    :cond_10
    const/4 v4, 0x0

    :goto_7
    if-nez v4, :cond_e

    const/4 v3, 0x0

    goto :goto_8

    :cond_11
    const/4 v3, 0x1

    :goto_8
    if-eqz v3, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_6

    :cond_12
    :goto_9
    if-eqz p2, :cond_14

    invoke-virtual {p0, v1}, Lf/h/f/q/r/f/c;->m(Z)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_13

    return-object p1

    :cond_13
    invoke-virtual {p0, v0}, Lf/h/f/q/r/f/c;->m(Z)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_14

    return-object p1

    :cond_14
    sget-object p1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw p1
.end method

.method public q(Lf/h/f/n/a;Ljava/util/List;I)Lf/h/f/q/r/f/a;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/f/n/a;",
            "Ljava/util/List<",
            "Lf/h/f/q/r/f/a;",
            ">;I)",
            "Lf/h/f/q/r/f/a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    rem-int/2addr v3, v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    iget-boolean v7, v0, Lf/h/f/q/r/f/c;->j:Z

    if-eqz v7, :cond_1

    xor-int/lit8 v3, v3, 0x1

    :cond_1
    const/4 v7, -0x1

    const/4 v8, 0x1

    :goto_1
    iget-object v9, v0, Lf/h/f/q/r/a;->a:[I

    aput v5, v9, v5

    aput v5, v9, v6

    aput v5, v9, v4

    const/4 v10, 0x3

    aput v5, v9, v10

    iget v5, v1, Lf/h/f/n/a;->e:I

    if-ltz v7, :cond_2

    move v11, v7

    goto :goto_2

    :cond_2
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v11, 0x0

    goto :goto_2

    :cond_3
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v11

    sub-int/2addr v11, v6

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lf/h/f/q/r/f/a;

    iget-object v11, v11, Lf/h/f/q/r/f/a;->c:Lf/h/f/q/r/c;

    iget-object v11, v11, Lf/h/f/q/r/c;->b:[I

    aget v11, v11, v6

    :goto_2
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v12

    rem-int/2addr v12, v4

    if-eqz v12, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    iget-boolean v12, v0, Lf/h/f/q/r/f/c;->j:Z

    if-eqz v12, :cond_5

    xor-int/lit8 v4, v4, 0x1

    :cond_5
    const/4 v12, 0x0

    :goto_4
    if-ge v11, v5, :cond_6

    invoke-virtual {v1, v11}, Lf/h/f/n/a;->b(I)Z

    move-result v12

    xor-int/2addr v12, v6

    if-eqz v12, :cond_6

    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    :cond_6
    const/4 v13, 0x0

    move v13, v12

    const/4 v14, 0x0

    move v12, v11

    :goto_5
    if-ge v11, v5, :cond_14

    invoke-virtual {v1, v11}, Lf/h/f/n/a;->b(I)Z

    move-result v15

    if-eq v15, v13, :cond_7

    aget v15, v9, v14

    add-int/2addr v15, v6

    aput v15, v9, v14

    goto/16 :goto_d

    :cond_7
    if-ne v14, v10, :cond_13

    if-eqz v4, :cond_8

    invoke-static {v9}, Lf/h/f/q/r/f/c;->r([I)V

    :cond_8
    invoke-static {v9}, Lf/h/f/q/r/a;->i([I)Z

    move-result v15

    if-eqz v15, :cond_11

    iget-object v4, v0, Lf/h/f/q/r/f/c;->i:[I

    const/4 v5, 0x0

    aput v12, v4, v5

    aput v11, v4, v6

    if-eqz v3, :cond_a

    aget v4, v4, v5

    sub-int/2addr v4, v6

    :goto_6
    if-ltz v4, :cond_9

    invoke-virtual {v1, v4}, Lf/h/f/n/a;->b(I)Z

    move-result v5

    if-nez v5, :cond_9

    add-int/lit8 v4, v4, -0x1

    goto :goto_6

    :cond_9
    add-int/lit8 v4, v4, 0x1

    iget-object v5, v0, Lf/h/f/q/r/f/c;->i:[I

    const/4 v9, 0x0

    aget v10, v5, v9

    sub-int/2addr v10, v4

    aget v5, v5, v6

    move v14, v4

    move v15, v5

    goto :goto_7

    :cond_a
    const/4 v9, 0x0

    aget v5, v4, v9

    aget v4, v4, v6

    add-int/2addr v4, v6

    invoke-virtual {v1, v4}, Lf/h/f/n/a;->f(I)I

    move-result v4

    iget-object v10, v0, Lf/h/f/q/r/f/c;->i:[I

    aget v10, v10, v6

    sub-int v10, v4, v10

    move v15, v4

    move v14, v5

    :goto_7
    iget-object v4, v0, Lf/h/f/q/r/a;->a:[I

    array-length v5, v4

    sub-int/2addr v5, v6

    invoke-static {v4, v9, v4, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput v10, v4, v9

    const/4 v5, 0x0

    :try_start_0
    sget-object v10, Lf/h/f/q/r/f/c;->n:[[I

    invoke-static {v4, v10}, Lf/h/f/q/r/a;->j([I[[I)I

    move-result v12
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Lf/h/f/q/r/c;

    const/4 v10, 0x2

    new-array v13, v10, [I

    aput v14, v13, v9

    aput v15, v13, v6

    move-object v11, v4

    move/from16 v16, p3

    invoke-direct/range {v11 .. v16}, Lf/h/f/q/r/c;-><init>(I[IIII)V

    goto :goto_8

    :catch_0
    move-object v4, v5

    :goto_8
    if-nez v4, :cond_c

    iget-object v7, v0, Lf/h/f/q/r/f/c;->i:[I

    const/4 v9, 0x0

    aget v7, v7, v9

    invoke-virtual {v1, v7}, Lf/h/f/n/a;->b(I)Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v1, v7}, Lf/h/f/n/a;->f(I)I

    move-result v7

    invoke-virtual {v1, v7}, Lf/h/f/n/a;->e(I)I

    move-result v7

    goto :goto_9

    :cond_b
    invoke-virtual {v1, v7}, Lf/h/f/n/a;->e(I)I

    move-result v7

    invoke-virtual {v1, v7}, Lf/h/f/n/a;->f(I)I

    move-result v7

    goto :goto_9

    :cond_c
    const/4 v8, 0x0

    :goto_9
    if-nez v8, :cond_10

    invoke-virtual {v0, v1, v4, v3, v6}, Lf/h/f/q/r/f/c;->o(Lf/h/f/n/a;Lf/h/f/q/r/c;ZZ)Lf/h/f/q/r/b;

    move-result-object v7

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_f

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v6

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/f/q/r/f/a;

    iget-object v2, v2, Lf/h/f/q/r/f/a;->b:Lf/h/f/q/r/b;

    if-nez v2, :cond_d

    const/4 v2, 0x1

    goto :goto_a

    :cond_d
    const/4 v2, 0x0

    :goto_a
    if-nez v2, :cond_e

    goto :goto_b

    :cond_e
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1

    :cond_f
    :goto_b
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v1, v4, v3, v2}, Lf/h/f/q/r/f/c;->o(Lf/h/f/n/a;Lf/h/f/q/r/c;ZZ)Lf/h/f/q/r/b;

    move-result-object v5
    :try_end_1
    .catch Lcom/google/zxing/NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    new-instance v1, Lf/h/f/q/r/f/a;

    invoke-direct {v1, v7, v5, v4, v6}, Lf/h/f/q/r/f/a;-><init>(Lf/h/f/q/r/b;Lf/h/f/q/r/b;Lf/h/f/q/r/c;Z)V

    return-object v1

    :cond_10
    const/4 v4, 0x2

    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_11
    if-eqz v4, :cond_12

    invoke-static {v9}, Lf/h/f/q/r/f/c;->r([I)V

    :cond_12
    const/4 v15, 0x0

    aget v16, v9, v15

    aget v17, v9, v6

    add-int v16, v16, v17

    add-int v12, v16, v12

    const/16 v16, 0x2

    aget v17, v9, v16

    aput v17, v9, v15

    aget v17, v9, v10

    aput v17, v9, v6

    aput v15, v9, v16

    aput v15, v9, v10

    add-int/lit8 v14, v14, -0x1

    goto :goto_c

    :cond_13
    add-int/lit8 v14, v14, 0x1

    :goto_c
    aput v6, v9, v14

    xor-int/lit8 v13, v13, 0x1

    :goto_d
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_5

    :cond_14
    sget-object v1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw v1
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lf/h/f/q/r/f/c;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lf/h/f/q/r/f/c;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
