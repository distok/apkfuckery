.class public final Lf/h/f/q/r/f/d/s;
.super Ljava/lang/Object;
.source "GeneralAppIdDecoder.java"


# instance fields
.field public final a:Lf/h/f/n/a;

.field public final b:Lf/h/f/q/r/f/d/m;

.field public final c:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lf/h/f/n/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/f/q/r/f/d/m;

    invoke-direct {v0}, Lf/h/f/q/r/f/d/m;-><init>()V

    iput-object v0, p0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    iput-object p1, p0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    return-void
.end method

.method public static d(Lf/h/f/n/a;II)I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    add-int v2, p1, v0

    invoke-virtual {p0, v2}, Lf/h/f/n/a;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    sub-int v2, p2, v0

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    shl-int v2, v3, v2

    or-int/2addr v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method


# virtual methods
.method public a(Ljava/lang/StringBuilder;I)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    invoke-virtual {p0, p2, v1}, Lf/h/f/q/r/f/d/s;->b(ILjava/lang/String;)Lf/h/f/q/r/f/d/o;

    move-result-object v1

    iget-object v2, v1, Lf/h/f/q/r/f/d/o;->b:Ljava/lang/String;

    invoke-static {v2}, Lf/h/f/q/r/f/d/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-boolean v2, v1, Lf/h/f/q/r/f/d/o;->d:Z

    if-eqz v2, :cond_1

    iget v2, v1, Lf/h/f/q/r/f/d/o;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v0

    :goto_1
    iget v1, v1, Lf/h/f/q/r/f/d/q;->a:I

    if-eq p2, v1, :cond_2

    move p2, v1

    move-object v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(ILjava/lang/String;)Lf/h/f/q/r/f/d/o;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    iget-object v2, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    if-eqz v1, :cond_0

    iget-object v2, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    move/from16 v2, p1

    iput v2, v1, Lf/h/f/q/r/f/d/m;->a:I

    sget-object v1, Lf/h/f/q/r/f/d/m$a;->d:Lf/h/f/q/r/f/d/m$a;

    sget-object v2, Lf/h/f/q/r/f/d/m$a;->f:Lf/h/f/q/r/f/d/m$a;

    sget-object v4, Lf/h/f/q/r/f/d/m$a;->e:Lf/h/f/q/r/f/d/m$a;

    :goto_0
    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v6, v5, Lf/h/f/q/r/f/d/m;->a:I

    iget-object v5, v5, Lf/h/f/q/r/f/d/m;->b:Lf/h/f/q/r/f/d/m$a;

    if-ne v5, v4, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    const/16 v9, 0xf

    const/16 v10, 0x10

    const/16 v12, 0x20

    const/16 v13, 0x3f

    const/16 v3, 0x24

    const/4 v14, 0x5

    if-eqz v8, :cond_f

    :goto_2
    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v5, v5, Lf/h/f/q/r/f/d/m;->a:I

    add-int/lit8 v8, v5, 0x5

    iget-object v15, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v7, v15, Lf/h/f/n/a;->e:I

    const/4 v11, 0x6

    if-le v8, v7, :cond_2

    goto :goto_4

    :cond_2
    invoke-static {v15, v5, v14}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v7

    if-lt v7, v14, :cond_3

    if-ge v7, v10, :cond_3

    goto :goto_3

    :cond_3
    add-int/lit8 v7, v5, 0x6

    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v15, v8, Lf/h/f/n/a;->e:I

    if-le v7, v15, :cond_4

    goto :goto_4

    :cond_4
    invoke-static {v8, v5, v11}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v5

    if-lt v5, v10, :cond_5

    if-ge v5, v13, :cond_5

    :goto_3
    const/4 v5, 0x1

    goto :goto_5

    :cond_5
    :goto_4
    const/4 v5, 0x0

    :goto_5
    if-eqz v5, :cond_b

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v5, v5, Lf/h/f/q/r/f/d/m;->a:I

    iget-object v7, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-static {v7, v5, v14}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v7

    if-ne v7, v9, :cond_6

    new-instance v7, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x5

    invoke-direct {v7, v5, v3}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    goto :goto_6

    :cond_6
    if-lt v7, v14, :cond_7

    if-ge v7, v9, :cond_7

    new-instance v8, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x5

    add-int/lit8 v7, v7, 0x30

    sub-int/2addr v7, v14

    int-to-char v7, v7

    invoke-direct {v8, v5, v7}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    move-object v7, v8

    :goto_6
    const/16 v8, 0x3a

    goto :goto_9

    :cond_7
    iget-object v7, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-static {v7, v5, v11}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v7

    const/16 v8, 0x3a

    if-lt v7, v12, :cond_8

    if-ge v7, v8, :cond_8

    new-instance v11, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x6

    add-int/lit8 v7, v7, 0x21

    int-to-char v7, v7

    invoke-direct {v11, v5, v7}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    :goto_7
    move-object v7, v11

    goto :goto_9

    :cond_8
    packed-switch v7, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Decoding invalid alphanumeric value: "

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const/16 v7, 0x2f

    goto :goto_8

    :pswitch_1
    const/16 v7, 0x2e

    goto :goto_8

    :pswitch_2
    const/16 v7, 0x2d

    goto :goto_8

    :pswitch_3
    const/16 v7, 0x2c

    goto :goto_8

    :pswitch_4
    const/16 v7, 0x2a

    :goto_8
    new-instance v11, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x6

    invoke-direct {v11, v5, v7}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    goto :goto_7

    :goto_9
    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v11, v7, Lf/h/f/q/r/f/d/q;->a:I

    iput v11, v5, Lf/h/f/q/r/f/d/m;->a:I

    iget-char v5, v7, Lf/h/f/q/r/f/d/n;->b:C

    if-ne v5, v3, :cond_9

    const/4 v7, 0x1

    goto :goto_a

    :cond_9
    const/4 v7, 0x0

    :goto_a
    if-eqz v7, :cond_a

    new-instance v3, Lf/h/f/q/r/f/d/o;

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v11, v5}, Lf/h/f/q/r/f/d/o;-><init>(ILjava/lang/String;)V

    new-instance v5, Lf/h/f/q/r/f/d/l;

    const/4 v7, 0x1

    invoke-direct {v5, v3, v7}, Lf/h/f/q/r/f/d/l;-><init>(Lf/h/f/q/r/f/d/o;Z)V

    goto :goto_d

    :cond_a
    iget-object v7, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_b
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v3, v3, Lf/h/f/q/r/f/d/m;->a:I

    invoke-virtual {v0, v3}, Lf/h/f/q/r/f/d/s;->e(I)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lf/h/f/q/r/f/d/m;->a(I)V

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iput-object v1, v3, Lf/h/f/q/r/f/d/m;->b:Lf/h/f/q/r/f/d/m$a;

    goto :goto_c

    :cond_c
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v3, v3, Lf/h/f/q/r/f/d/m;->a:I

    invoke-virtual {v0, v3}, Lf/h/f/q/r/f/d/s;->f(I)Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v5, v3, Lf/h/f/q/r/f/d/m;->a:I

    add-int/2addr v5, v14

    iget-object v7, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v7, v7, Lf/h/f/n/a;->e:I

    if-ge v5, v7, :cond_d

    invoke-virtual {v3, v14}, Lf/h/f/q/r/f/d/m;->a(I)V

    goto :goto_b

    :cond_d
    iput v7, v3, Lf/h/f/q/r/f/d/m;->a:I

    :goto_b
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iput-object v2, v3, Lf/h/f/q/r/f/d/m;->b:Lf/h/f/q/r/f/d/m$a;

    :cond_e
    :goto_c
    new-instance v5, Lf/h/f/q/r/f/d/l;

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v3, v7}, Lf/h/f/q/r/f/d/l;-><init>(Lf/h/f/q/r/f/d/o;Z)V

    :goto_d
    iget-boolean v3, v5, Lf/h/f/q/r/f/d/l;->b:Z

    goto/16 :goto_19

    :cond_f
    const/16 v8, 0x3a

    if-ne v5, v2, :cond_10

    const/4 v5, 0x1

    goto :goto_e

    :cond_10
    const/4 v5, 0x0

    :goto_e
    const/4 v7, 0x7

    if-eqz v5, :cond_21

    :goto_f
    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v5, v5, Lf/h/f/q/r/f/d/m;->a:I

    add-int/lit8 v11, v5, 0x5

    iget-object v15, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v8, v15, Lf/h/f/n/a;->e:I

    const/16 v12, 0x74

    const/16 v13, 0x40

    const/16 v3, 0x8

    if-le v11, v8, :cond_11

    goto :goto_11

    :cond_11
    invoke-static {v15, v5, v14}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v8

    if-lt v8, v14, :cond_12

    if-ge v8, v10, :cond_12

    goto :goto_10

    :cond_12
    add-int/lit8 v8, v5, 0x7

    iget-object v11, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v15, v11, Lf/h/f/n/a;->e:I

    if-le v8, v15, :cond_13

    goto :goto_11

    :cond_13
    invoke-static {v11, v5, v7}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v8

    if-lt v8, v13, :cond_14

    if-ge v8, v12, :cond_14

    goto :goto_10

    :cond_14
    add-int/lit8 v8, v5, 0x8

    iget-object v11, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v15, v11, Lf/h/f/n/a;->e:I

    if-le v8, v15, :cond_15

    goto :goto_11

    :cond_15
    invoke-static {v11, v5, v3}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v5

    const/16 v8, 0xe8

    if-lt v5, v8, :cond_16

    const/16 v8, 0xfd

    if-ge v5, v8, :cond_16

    :goto_10
    const/4 v5, 0x1

    goto :goto_12

    :cond_16
    :goto_11
    const/4 v5, 0x0

    :goto_12
    if-eqz v5, :cond_1d

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v5, v5, Lf/h/f/q/r/f/d/m;->a:I

    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-static {v8, v5, v14}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v8

    if-ne v8, v9, :cond_17

    new-instance v3, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x5

    const/16 v8, 0x24

    invoke-direct {v3, v5, v8}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    goto/16 :goto_14

    :cond_17
    if-lt v8, v14, :cond_18

    if-ge v8, v9, :cond_18

    new-instance v3, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x5

    add-int/lit8 v8, v8, 0x30

    sub-int/2addr v8, v14

    int-to-char v8, v8

    invoke-direct {v3, v5, v8}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    goto/16 :goto_14

    :cond_18
    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-static {v8, v5, v7}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v8

    const/16 v11, 0x5a

    if-lt v8, v13, :cond_19

    if-ge v8, v11, :cond_19

    new-instance v3, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x7

    add-int/lit8 v8, v8, 0x1

    int-to-char v8, v8

    invoke-direct {v3, v5, v8}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    goto/16 :goto_14

    :cond_19
    if-lt v8, v11, :cond_1a

    if-ge v8, v12, :cond_1a

    new-instance v3, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x7

    add-int/lit8 v8, v8, 0x7

    int-to-char v8, v8

    invoke-direct {v3, v5, v8}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    goto/16 :goto_14

    :cond_1a
    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-static {v8, v5, v3}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object v1

    throw v1

    :pswitch_5
    const/16 v3, 0x20

    goto :goto_13

    :pswitch_6
    const/16 v3, 0x5f

    goto :goto_13

    :pswitch_7
    const/16 v3, 0x3f

    goto :goto_13

    :pswitch_8
    const/16 v3, 0x3e

    goto :goto_13

    :pswitch_9
    const/16 v3, 0x3d

    goto :goto_13

    :pswitch_a
    const/16 v3, 0x3c

    goto :goto_13

    :pswitch_b
    const/16 v3, 0x3b

    goto :goto_13

    :pswitch_c
    const/16 v3, 0x3a

    goto :goto_13

    :pswitch_d
    const/16 v3, 0x2f

    goto :goto_13

    :pswitch_e
    const/16 v3, 0x2e

    goto :goto_13

    :pswitch_f
    const/16 v3, 0x2d

    goto :goto_13

    :pswitch_10
    const/16 v3, 0x2c

    goto :goto_13

    :pswitch_11
    const/16 v3, 0x2b

    goto :goto_13

    :pswitch_12
    const/16 v3, 0x2a

    goto :goto_13

    :pswitch_13
    const/16 v3, 0x29

    goto :goto_13

    :pswitch_14
    const/16 v3, 0x28

    goto :goto_13

    :pswitch_15
    const/16 v3, 0x27

    goto :goto_13

    :pswitch_16
    const/16 v3, 0x26

    goto :goto_13

    :pswitch_17
    const/16 v3, 0x25

    goto :goto_13

    :pswitch_18
    const/16 v3, 0x22

    goto :goto_13

    :pswitch_19
    const/16 v3, 0x21

    :goto_13
    new-instance v8, Lf/h/f/q/r/f/d/n;

    add-int/lit8 v5, v5, 0x8

    invoke-direct {v8, v5, v3}, Lf/h/f/q/r/f/d/n;-><init>(IC)V

    move-object v3, v8

    :goto_14
    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v8, v3, Lf/h/f/q/r/f/d/q;->a:I

    iput v8, v5, Lf/h/f/q/r/f/d/m;->a:I

    iget-char v3, v3, Lf/h/f/q/r/f/d/n;->b:C

    const/16 v5, 0x24

    if-ne v3, v5, :cond_1b

    const/4 v11, 0x1

    goto :goto_15

    :cond_1b
    const/4 v11, 0x0

    :goto_15
    if-eqz v11, :cond_1c

    new-instance v3, Lf/h/f/q/r/f/d/o;

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v8, v5}, Lf/h/f/q/r/f/d/o;-><init>(ILjava/lang/String;)V

    new-instance v5, Lf/h/f/q/r/f/d/l;

    const/4 v7, 0x1

    invoke-direct {v5, v3, v7}, Lf/h/f/q/r/f/d/l;-><init>(Lf/h/f/q/r/f/d/o;Z)V

    goto :goto_18

    :cond_1c
    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    const/16 v8, 0x3a

    const/16 v12, 0x20

    const/16 v13, 0x3f

    goto/16 :goto_f

    :cond_1d
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v3, v3, Lf/h/f/q/r/f/d/m;->a:I

    invoke-virtual {v0, v3}, Lf/h/f/q/r/f/d/s;->e(I)Z

    move-result v3

    if-eqz v3, :cond_1e

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lf/h/f/q/r/f/d/m;->a(I)V

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iput-object v1, v3, Lf/h/f/q/r/f/d/m;->b:Lf/h/f/q/r/f/d/m$a;

    goto :goto_17

    :cond_1e
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v3, v3, Lf/h/f/q/r/f/d/m;->a:I

    invoke-virtual {v0, v3}, Lf/h/f/q/r/f/d/s;->f(I)Z

    move-result v3

    if-eqz v3, :cond_20

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v5, v3, Lf/h/f/q/r/f/d/m;->a:I

    add-int/2addr v5, v14

    iget-object v7, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v7, v7, Lf/h/f/n/a;->e:I

    if-ge v5, v7, :cond_1f

    invoke-virtual {v3, v14}, Lf/h/f/q/r/f/d/m;->a(I)V

    goto :goto_16

    :cond_1f
    iput v7, v3, Lf/h/f/q/r/f/d/m;->a:I

    :goto_16
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iput-object v4, v3, Lf/h/f/q/r/f/d/m;->b:Lf/h/f/q/r/f/d/m$a;

    :cond_20
    :goto_17
    new-instance v3, Lf/h/f/q/r/f/d/l;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v3, v5, v7}, Lf/h/f/q/r/f/d/l;-><init>(Lf/h/f/q/r/f/d/o;Z)V

    move-object v5, v3

    :goto_18
    iget-boolean v3, v5, Lf/h/f/q/r/f/d/l;->b:Z

    :goto_19
    const/4 v7, 0x0

    const/4 v8, 0x1

    goto/16 :goto_27

    :cond_21
    :goto_1a
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v3, v3, Lf/h/f/q/r/f/d/m;->a:I

    add-int/lit8 v5, v3, 0x7

    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v8, v8, Lf/h/f/n/a;->e:I

    if-le v5, v8, :cond_23

    add-int/lit8 v3, v3, 0x4

    if-gt v3, v8, :cond_22

    goto :goto_1c

    :cond_22
    const/4 v3, 0x0

    goto :goto_1d

    :cond_23
    move v5, v3

    :goto_1b
    add-int/lit8 v8, v3, 0x3

    if-ge v5, v8, :cond_25

    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-virtual {v8, v5}, Lf/h/f/n/a;->b(I)Z

    move-result v8

    if-eqz v8, :cond_24

    :goto_1c
    const/4 v3, 0x1

    goto :goto_1d

    :cond_24
    add-int/lit8 v5, v5, 0x1

    goto :goto_1b

    :cond_25
    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-virtual {v3, v8}, Lf/h/f/n/a;->b(I)Z

    move-result v3

    :goto_1d
    const/4 v5, 0x4

    if-eqz v3, :cond_2e

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v3, v3, Lf/h/f/q/r/f/d/m;->a:I

    add-int/lit8 v8, v3, 0x7

    iget-object v9, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v10, v9, Lf/h/f/n/a;->e:I

    const/16 v11, 0xa

    if-le v8, v10, :cond_27

    invoke-static {v9, v3, v5}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v3

    if-nez v3, :cond_26

    new-instance v3, Lf/h/f/q/r/f/d/p;

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v5, v5, Lf/h/f/n/a;->e:I

    invoke-direct {v3, v5, v11, v11}, Lf/h/f/q/r/f/d/p;-><init>(III)V

    goto :goto_1e

    :cond_26
    new-instance v5, Lf/h/f/q/r/f/d/p;

    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v8, v8, Lf/h/f/n/a;->e:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {v5, v8, v3, v11}, Lf/h/f/q/r/f/d/p;-><init>(III)V

    move-object v3, v5

    goto :goto_1e

    :cond_27
    invoke-static {v9, v3, v7}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result v3

    add-int/lit8 v3, v3, -0x8

    div-int/lit8 v5, v3, 0xb

    rem-int/lit8 v3, v3, 0xb

    new-instance v9, Lf/h/f/q/r/f/d/p;

    invoke-direct {v9, v8, v5, v3}, Lf/h/f/q/r/f/d/p;-><init>(III)V

    move-object v3, v9

    :goto_1e
    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v8, v3, Lf/h/f/q/r/f/d/q;->a:I

    iput v8, v5, Lf/h/f/q/r/f/d/m;->a:I

    iget v5, v3, Lf/h/f/q/r/f/d/p;->b:I

    if-ne v5, v11, :cond_28

    const/4 v9, 0x1

    goto :goto_1f

    :cond_28
    const/4 v9, 0x0

    :goto_1f
    if-eqz v9, :cond_2b

    iget v5, v3, Lf/h/f/q/r/f/d/p;->c:I

    if-ne v5, v11, :cond_29

    const/4 v5, 0x1

    goto :goto_20

    :cond_29
    const/4 v5, 0x0

    :goto_20
    if-eqz v5, :cond_2a

    new-instance v3, Lf/h/f/q/r/f/d/o;

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v8, v5}, Lf/h/f/q/r/f/d/o;-><init>(ILjava/lang/String;)V

    goto :goto_21

    :cond_2a
    new-instance v5, Lf/h/f/q/r/f/d/o;

    iget-object v7, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget v3, v3, Lf/h/f/q/r/f/d/p;->c:I

    invoke-direct {v5, v8, v7, v3}, Lf/h/f/q/r/f/d/o;-><init>(ILjava/lang/String;I)V

    move-object v3, v5

    :goto_21
    new-instance v5, Lf/h/f/q/r/f/d/l;

    const/4 v7, 0x1

    invoke-direct {v5, v3, v7}, Lf/h/f/q/r/f/d/l;-><init>(Lf/h/f/q/r/f/d/o;Z)V

    const/4 v7, 0x0

    const/4 v8, 0x1

    goto :goto_26

    :cond_2b
    iget-object v8, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v3, v3, Lf/h/f/q/r/f/d/p;->c:I

    if-ne v3, v11, :cond_2c

    const/4 v5, 0x1

    goto :goto_22

    :cond_2c
    const/4 v5, 0x0

    :goto_22
    if-eqz v5, :cond_2d

    new-instance v3, Lf/h/f/q/r/f/d/o;

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v5, v5, Lf/h/f/q/r/f/d/m;->a:I

    iget-object v7, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v5, v7}, Lf/h/f/q/r/f/d/o;-><init>(ILjava/lang/String;)V

    new-instance v5, Lf/h/f/q/r/f/d/l;

    const/4 v8, 0x1

    invoke-direct {v5, v3, v8}, Lf/h/f/q/r/f/d/l;-><init>(Lf/h/f/q/r/f/d/o;Z)V

    const/4 v7, 0x0

    goto :goto_26

    :cond_2d
    const/4 v8, 0x1

    iget-object v5, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_1a

    :cond_2e
    const/4 v8, 0x1

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v3, v3, Lf/h/f/q/r/f/d/m;->a:I

    add-int/lit8 v7, v3, 0x1

    iget-object v9, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v9, v9, Lf/h/f/n/a;->e:I

    if-le v7, v9, :cond_2f

    goto :goto_24

    :cond_2f
    const/4 v7, 0x0

    :goto_23
    if-ge v7, v5, :cond_31

    add-int v9, v7, v3

    iget-object v10, v0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v11, v10, Lf/h/f/n/a;->e:I

    if-ge v9, v11, :cond_31

    invoke-virtual {v10, v9}, Lf/h/f/n/a;->b(I)Z

    move-result v9

    if-eqz v9, :cond_30

    :goto_24
    const/4 v7, 0x0

    goto :goto_25

    :cond_30
    add-int/lit8 v7, v7, 0x1

    goto :goto_23

    :cond_31
    const/4 v7, 0x1

    :goto_25
    if-eqz v7, :cond_32

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iput-object v4, v3, Lf/h/f/q/r/f/d/m;->b:Lf/h/f/q/r/f/d/m$a;

    invoke-virtual {v3, v5}, Lf/h/f/q/r/f/d/m;->a(I)V

    :cond_32
    new-instance v3, Lf/h/f/q/r/f/d/l;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v3, v5, v7}, Lf/h/f/q/r/f/d/l;-><init>(Lf/h/f/q/r/f/d/o;Z)V

    move-object v5, v3

    :goto_26
    iget-boolean v3, v5, Lf/h/f/q/r/f/d/l;->b:Z

    :goto_27
    iget-object v9, v0, Lf/h/f/q/r/f/d/s;->b:Lf/h/f/q/r/f/d/m;

    iget v9, v9, Lf/h/f/q/r/f/d/m;->a:I

    if-eq v6, v9, :cond_33

    goto :goto_28

    :cond_33
    const/4 v8, 0x0

    :goto_28
    if-nez v8, :cond_34

    if-eqz v3, :cond_35

    :cond_34
    if-eqz v3, :cond_37

    :cond_35
    iget-object v1, v5, Lf/h/f/q/r/f/d/l;->a:Lf/h/f/q/r/f/d/o;

    if-eqz v1, :cond_36

    iget-boolean v2, v1, Lf/h/f/q/r/f/d/o;->d:Z

    if-eqz v2, :cond_36

    new-instance v2, Lf/h/f/q/r/f/d/o;

    iget-object v3, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget v1, v1, Lf/h/f/q/r/f/d/o;->c:I

    invoke-direct {v2, v9, v3, v1}, Lf/h/f/q/r/f/d/o;-><init>(ILjava/lang/String;I)V

    return-object v2

    :cond_36
    new-instance v1, Lf/h/f/q/r/f/d/o;

    iget-object v2, v0, Lf/h/f/q/r/f/d/s;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v9, v2}, Lf/h/f/q/r/f/d/o;-><init>(ILjava/lang/String;)V

    return-object v1

    :cond_37
    const/4 v3, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3a
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xe8
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public c(II)I
    .locals 1

    iget-object v0, p0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-static {v0, p1, p2}, Lf/h/f/q/r/f/d/s;->d(Lf/h/f/n/a;II)I

    move-result p1

    return p1
.end method

.method public final e(I)Z
    .locals 3

    add-int/lit8 v0, p1, 0x3

    iget-object v1, p0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v1, v1, Lf/h/f/n/a;->e:I

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    return v2

    :cond_0
    :goto_0
    if-ge p1, v0, :cond_2

    iget-object v1, p0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    invoke-virtual {v1, p1}, Lf/h/f/n/a;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public final f(I)Z
    .locals 5

    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v1, v1, Lf/h/f/n/a;->e:I

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_3

    add-int v1, v0, p1

    iget-object v3, p0, Lf/h/f/q/r/f/d/s;->a:Lf/h/f/n/a;

    iget v4, v3, Lf/h/f/n/a;->e:I

    if-ge v1, v4, :cond_3

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    add-int/lit8 v1, p1, 0x2

    invoke-virtual {v3, v1}, Lf/h/f/n/a;->b(I)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_1
    invoke-virtual {v3, v1}, Lf/h/f/n/a;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    return v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method
