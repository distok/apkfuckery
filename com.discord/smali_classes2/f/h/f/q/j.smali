.class public final Lf/h/f/q/j;
.super Lf/h/f/q/k;
.source "MultiFormatUPCEANReader.java"


# instance fields
.field public final a:[Lf/h/f/q/p;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/h/f/q/k;-><init>()V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    sget-object v0, Lf/h/f/d;->f:Lf/h/f/d;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_4

    sget-object v1, Lf/h/f/a;->k:Lf/h/f/a;

    invoke-interface {p1, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lf/h/f/q/e;

    invoke-direct {v1}, Lf/h/f/q/e;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    sget-object v1, Lf/h/f/a;->r:Lf/h/f/a;

    invoke-interface {p1, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lf/h/f/q/l;

    invoke-direct {v1}, Lf/h/f/q/l;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    sget-object v1, Lf/h/f/a;->j:Lf/h/f/a;

    invoke-interface {p1, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lf/h/f/q/f;

    invoke-direct {v1}, Lf/h/f/q/f;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    sget-object v1, Lf/h/f/a;->s:Lf/h/f/a;

    invoke-interface {p1, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p1, Lf/h/f/q/q;

    invoke-direct {p1}, Lf/h/f/q/q;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_5

    new-instance p1, Lf/h/f/q/e;

    invoke-direct {p1}, Lf/h/f/q/e;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance p1, Lf/h/f/q/f;

    invoke-direct {p1}, Lf/h/f/q/f;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance p1, Lf/h/f/q/q;

    invoke-direct {p1}, Lf/h/f/q/q;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array p1, p1, [Lf/h/f/q/p;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lf/h/f/q/p;

    iput-object p1, p0, Lf/h/f/q/j;->a:[Lf/h/f/q/p;

    return-void
.end method


# virtual methods
.method public b(ILf/h/f/n/a;Ljava/util/Map;)Lcom/google/zxing/Result;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/f/n/a;",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    sget-object v0, Lf/h/f/a;->r:Lf/h/f/a;

    invoke-static {p2}, Lf/h/f/q/p;->m(Lf/h/f/n/a;)[I

    move-result-object v1

    iget-object v2, p0, Lf/h/f/q/j;->a:[Lf/h/f/q/p;

    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_5

    aget-object v6, v2, v5

    :try_start_0
    invoke-virtual {v6, p1, p2, v1, p3}, Lf/h/f/q/p;->k(ILf/h/f/n/a;[ILjava/util/Map;)Lcom/google/zxing/Result;

    move-result-object v6

    iget-object v7, v6, Lcom/google/zxing/Result;->d:Lf/h/f/a;

    sget-object v8, Lf/h/f/a;->k:Lf/h/f/a;

    const/4 v9, 0x1

    if-ne v7, v8, :cond_0

    iget-object v7, v6, Lcom/google/zxing/Result;->a:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x30

    if-ne v7, v8, :cond_0

    const/4 v7, 0x1

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    :goto_1
    if-nez p3, :cond_1

    const/4 v8, 0x0

    goto :goto_2

    :cond_1
    sget-object v8, Lf/h/f/d;->f:Lf/h/f/d;

    invoke-interface {p3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Collection;

    :goto_2
    if-eqz v8, :cond_3

    invoke-interface {v8, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_3

    :cond_2
    const/4 v8, 0x0

    goto :goto_4

    :cond_3
    :goto_3
    const/4 v8, 0x1

    :goto_4
    if-eqz v7, :cond_4

    if-eqz v8, :cond_4

    new-instance v7, Lcom/google/zxing/Result;

    iget-object v8, v6, Lcom/google/zxing/Result;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, v6, Lcom/google/zxing/Result;->b:[B

    iget-object v10, v6, Lcom/google/zxing/Result;->c:[Lf/h/f/k;

    invoke-direct {v7, v8, v9, v10, v0}, Lcom/google/zxing/Result;-><init>(Ljava/lang/String;[B[Lf/h/f/k;Lf/h/f/a;)V

    iget-object v6, v6, Lcom/google/zxing/Result;->e:Ljava/util/Map;

    invoke-virtual {v7, v6}, Lcom/google/zxing/Result;->a(Ljava/util/Map;)V
    :try_end_0
    .catch Lcom/google/zxing/ReaderException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v7

    :cond_4
    return-object v6

    :catch_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_5
    sget-object p1, Lcom/google/zxing/NotFoundException;->f:Lcom/google/zxing/NotFoundException;

    throw p1
.end method

.method public reset()V
    .locals 4

    iget-object v0, p0, Lf/h/f/q/j;->a:[Lf/h/f/q/p;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
