.class public final Lf/h/f/q/l;
.super Lf/h/f/q/p;
.source "UPCAReader.java"


# instance fields
.field public final h:Lf/h/f/q/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/f/q/p;-><init>()V

    new-instance v0, Lf/h/f/q/e;

    invoke-direct {v0}, Lf/h/f/q/e;-><init>()V

    iput-object v0, p0, Lf/h/f/q/l;->h:Lf/h/f/q/p;

    return-void
.end method

.method public static o(Lcom/google/zxing/Result;)Lcom/google/zxing/Result;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/zxing/Result;->a:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_1

    new-instance v1, Lcom/google/zxing/Result;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/zxing/Result;->c:[Lf/h/f/k;

    sget-object v4, Lf/h/f/a;->r:Lf/h/f/a;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/zxing/Result;-><init>(Ljava/lang/String;[B[Lf/h/f/k;Lf/h/f/a;)V

    iget-object p0, p0, Lcom/google/zxing/Result;->e:Ljava/util/Map;

    if-eqz p0, :cond_0

    invoke-virtual {v1, p0}, Lcom/google/zxing/Result;->a(Ljava/util/Map;)V

    :cond_0
    return-object v1

    :cond_1
    invoke-static {}, Lcom/google/zxing/FormatException;->a()Lcom/google/zxing/FormatException;

    move-result-object p0

    throw p0
.end method


# virtual methods
.method public a(Lf/h/f/c;Ljava/util/Map;)Lcom/google/zxing/Result;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/f/c;",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/f/q/l;->h:Lf/h/f/q/p;

    invoke-virtual {v0, p1, p2}, Lf/h/f/q/k;->a(Lf/h/f/c;Ljava/util/Map;)Lcom/google/zxing/Result;

    move-result-object p1

    invoke-static {p1}, Lf/h/f/q/l;->o(Lcom/google/zxing/Result;)Lcom/google/zxing/Result;

    move-result-object p1

    return-object p1
.end method

.method public b(ILf/h/f/n/a;Ljava/util/Map;)Lcom/google/zxing/Result;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/f/n/a;",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;,
            Lcom/google/zxing/ChecksumException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/f/q/l;->h:Lf/h/f/q/p;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/f/q/p;->b(ILf/h/f/n/a;Ljava/util/Map;)Lcom/google/zxing/Result;

    move-result-object p1

    invoke-static {p1}, Lf/h/f/q/l;->o(Lcom/google/zxing/Result;)Lcom/google/zxing/Result;

    move-result-object p1

    return-object p1
.end method

.method public j(Lf/h/f/n/a;[ILjava/lang/StringBuilder;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/f/q/l;->h:Lf/h/f/q/p;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/f/q/p;->j(Lf/h/f/n/a;[ILjava/lang/StringBuilder;)I

    move-result p1

    return p1
.end method

.method public k(ILf/h/f/n/a;[ILjava/util/Map;)Lcom/google/zxing/Result;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/f/n/a;",
            "[I",
            "Ljava/util/Map<",
            "Lf/h/f/d;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;,
            Lcom/google/zxing/ChecksumException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/f/q/l;->h:Lf/h/f/q/p;

    invoke-virtual {v0, p1, p2, p3, p4}, Lf/h/f/q/p;->k(ILf/h/f/n/a;[ILjava/util/Map;)Lcom/google/zxing/Result;

    move-result-object p1

    invoke-static {p1}, Lf/h/f/q/l;->o(Lcom/google/zxing/Result;)Lcom/google/zxing/Result;

    move-result-object p1

    return-object p1
.end method

.method public n()Lf/h/f/a;
    .locals 1

    sget-object v0, Lf/h/f/a;->r:Lf/h/f/a;

    return-object v0
.end method
