.class public final Lf/h/a/f/j/b/c8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/measurement/internal/zzaq;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lf/h/a/f/i/j/fc;

.field public final synthetic g:Lf/h/a/f/j/b/q7;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/q7;Lcom/google/android/gms/measurement/internal/zzaq;Ljava/lang/String;Lf/h/a/f/i/j/fc;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    iput-object p2, p0, Lf/h/a/f/j/b/c8;->d:Lcom/google/android/gms/measurement/internal/zzaq;

    iput-object p3, p0, Lf/h/a/f/j/b/c8;->e:Ljava/lang/String;

    iput-object p4, p0, Lf/h/a/f/j/b/c8;->f:Lf/h/a/f/i/j/fc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    iget-object v2, v1, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Discarding data. Failed to send event to service to bundle"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/c8;->f:Lf/h/a/f/i/j/fc;

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/t9;->P(Lf/h/a/f/i/j/fc;[B)V

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lf/h/a/f/j/b/c8;->d:Lcom/google/android/gms/measurement/internal/zzaq;

    iget-object v3, p0, Lf/h/a/f/j/b/c8;->e:Ljava/lang/String;

    invoke-interface {v2, v1, v3}, Lf/h/a/f/j/b/i3;->i(Lcom/google/android/gms/measurement/internal/zzaq;Ljava/lang/String;)[B

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/q7;->F()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/c8;->f:Lf/h/a/f/i/j/fc;

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/t9;->P(Lf/h/a/f/i/j/fc;[B)V

    return-void

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    iget-object v2, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v3, "Failed to send event to the service to bundle"

    invoke-virtual {v2, v3, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/c8;->f:Lf/h/a/f/i/j/fc;

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/t9;->P(Lf/h/a/f/i/j/fc;[B)V

    return-void

    :goto_0
    iget-object v2, p0, Lf/h/a/f/j/b/c8;->g:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v2

    iget-object v3, p0, Lf/h/a/f/j/b/c8;->f:Lf/h/a/f/i/j/fc;

    invoke-virtual {v2, v3, v0}, Lf/h/a/f/j/b/t9;->P(Lf/h/a/f/i/j/fc;[B)V

    throw v1
.end method
