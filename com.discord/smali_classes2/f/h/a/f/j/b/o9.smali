.class public final Lf/h/a/f/j/b/o9;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/google/android/gms/measurement/internal/zzn;

.field public final synthetic e:Lf/h/a/f/j/b/k9;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/k9;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/o9;->e:Lf/h/a/f/j/b/k9;

    iput-object p2, p0, Lf/h/a/f/j/b/o9;->d:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/j/b/o9;->e:Lf/h/a/f/j/b/k9;

    iget-object v0, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->J0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/j/b/o9;->e:Lf/h/a/f/j/b/k9;

    iget-object v1, p0, Lf/h/a/f/j/b/o9;->d:Lcom/google/android/gms/measurement/internal/zzn;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/zzn;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/k9;->a(Ljava/lang/String;)Lf/h/a/f/j/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/d;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/o9;->d:Lcom/google/android/gms/measurement/internal/zzn;

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/zzn;->z:Ljava/lang/String;

    invoke-static {v0}, Lf/h/a/f/j/b/d;->b(Ljava/lang/String;)Lf/h/a/f/j/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/d;->k()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/o9;->e:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v1, "Analytics storage consent denied. Returning null app instance id"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v0, p0, Lf/h/a/f/j/b/o9;->e:Lf/h/a/f/j/b/k9;

    iget-object v1, p0, Lf/h/a/f/j/b/o9;->d:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/k9;->G(Lcom/google/android/gms/measurement/internal/zzn;)Lf/h/a/f/j/b/a4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/a4;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
