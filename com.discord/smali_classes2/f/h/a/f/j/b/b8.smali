.class public final Lf/h/a/f/j/b/b8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/measurement/internal/zzn;

.field public final synthetic e:Lf/h/a/f/j/b/q7;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/q7;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/b8;->e:Lf/h/a/f/j/b/q7;

    iput-object p2, p0, Lf/h/a/f/j/b/b8;->d:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/j/b/b8;->e:Lf/h/a/f/j/b/q7;

    iget-object v1, v0, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "Failed to send measurementEnabled to service"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/h/a/f/j/b/b8;->d:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-interface {v1, v0}, Lf/h/a/f/j/b/i3;->V(Lcom/google/android/gms/measurement/internal/zzn;)V

    iget-object v0, p0, Lf/h/a/f/j/b/b8;->e:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/q7;->F()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/f/j/b/b8;->e:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Failed to send measurementEnabled to the service"

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
