.class public final Lf/h/a/f/j/b/da;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lf/h/a/f/i/j/g1;

.field public d:Ljava/util/BitSet;

.field public e:Ljava/util/BitSet;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field public final synthetic h:Lf/h/a/f/j/b/ba;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/ba;Ljava/lang/String;Lf/h/a/f/i/j/g1;Ljava/util/BitSet;Ljava/util/BitSet;Ljava/util/Map;Ljava/util/Map;Lf/h/a/f/j/b/aa;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/da;->h:Lf/h/a/f/j/b/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/a/f/j/b/da;->a:Ljava/lang/String;

    iput-object p4, p0, Lf/h/a/f/j/b/da;->d:Ljava/util/BitSet;

    iput-object p5, p0, Lf/h/a/f/j/b/da;->e:Ljava/util/BitSet;

    iput-object p6, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    new-instance p1, Landroidx/collection/ArrayMap;

    invoke-direct {p1}, Landroidx/collection/ArrayMap;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    invoke-interface {p7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p7, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Long;

    invoke-virtual {p4, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p5, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    invoke-interface {p5, p2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/f/j/b/da;->b:Z

    iput-object p3, p0, Lf/h/a/f/j/b/da;->c:Lf/h/a/f/i/j/g1;

    return-void
.end method

.method public constructor <init>(Lf/h/a/f/j/b/ba;Ljava/lang/String;Lf/h/a/f/j/b/aa;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/da;->h:Lf/h/a/f/j/b/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/a/f/j/b/da;->a:Ljava/lang/String;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/f/j/b/da;->b:Z

    new-instance p1, Ljava/util/BitSet;

    invoke-direct {p1}, Ljava/util/BitSet;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/da;->d:Ljava/util/BitSet;

    new-instance p1, Ljava/util/BitSet;

    invoke-direct {p1}, Ljava/util/BitSet;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/da;->e:Ljava/util/BitSet;

    new-instance p1, Landroidx/collection/ArrayMap;

    invoke-direct {p1}, Landroidx/collection/ArrayMap;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    new-instance p1, Landroidx/collection/ArrayMap;

    invoke-direct {p1}, Landroidx/collection/ArrayMap;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(I)Lf/h/a/f/i/j/y0;
    .locals 8
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Lf/h/a/f/i/j/y0;->F()Lf/h/a/f/i/j/y0$a;

    move-result-object v0

    iget-boolean v1, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v1, v0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v1, Lf/h/a/f/i/j/y0;

    invoke-static {v1, p1}, Lf/h/a/f/i/j/y0;->u(Lf/h/a/f/i/j/y0;I)V

    iget-boolean p1, p0, Lf/h/a/f/j/b/da;->b:Z

    iget-boolean v1, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_1
    iget-object v1, v0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v1, Lf/h/a/f/i/j/y0;

    invoke-static {v1, p1}, Lf/h/a/f/i/j/y0;->w(Lf/h/a/f/i/j/y0;Z)V

    iget-object p1, p0, Lf/h/a/f/j/b/da;->c:Lf/h/a/f/i/j/g1;

    if-eqz p1, :cond_3

    iget-boolean v1, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_2
    iget-object v1, v0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v1, Lf/h/a/f/i/j/y0;

    invoke-static {v1, p1}, Lf/h/a/f/i/j/y0;->z(Lf/h/a/f/i/j/y0;Lf/h/a/f/i/j/g1;)V

    :cond_3
    invoke-static {}, Lf/h/a/f/i/j/g1;->M()Lf/h/a/f/i/j/g1$a;

    move-result-object p1

    iget-object v1, p0, Lf/h/a/f/j/b/da;->d:Ljava/util/BitSet;

    invoke-static {v1}, Lf/h/a/f/j/b/q9;->B(Ljava/util/BitSet;)Ljava/util/List;

    move-result-object v1

    iget-boolean v3, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_4
    iget-object v3, p1, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v3, Lf/h/a/f/i/j/g1;

    invoke-static {v3, v1}, Lf/h/a/f/i/j/g1;->D(Lf/h/a/f/i/j/g1;Ljava/lang/Iterable;)V

    iget-object v1, p0, Lf/h/a/f/j/b/da;->e:Ljava/util/BitSet;

    invoke-static {v1}, Lf/h/a/f/j/b/q9;->B(Ljava/util/BitSet;)Ljava/util/List;

    move-result-object v1

    iget-boolean v3, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_5
    iget-object v3, p1, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v3, Lf/h/a/f/i/j/g1;

    invoke-static {v3, v1}, Lf/h/a/f/i/j/g1;->y(Lf/h/a/f/i/j/g1;Ljava/lang/Iterable;)V

    iget-object v1, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    if-nez v1, :cond_6

    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {}, Lf/h/a/f/i/j/z0;->A()Lf/h/a/f/i/j/z0$a;

    move-result-object v5

    iget-boolean v6, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v6, :cond_7

    invoke-virtual {v5}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_7
    iget-object v6, v5, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v6, Lf/h/a/f/i/j/z0;

    invoke-static {v6, v4}, Lf/h/a/f/i/j/z0;->u(Lf/h/a/f/i/j/z0;I)V

    iget-object v6, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-boolean v4, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v4, :cond_8

    invoke-virtual {v5}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_8
    iget-object v4, v5, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v4, Lf/h/a/f/i/j/z0;

    invoke-static {v4, v6, v7}, Lf/h/a/f/i/j/z0;->v(Lf/h/a/f/i/j/z0;J)V

    invoke-virtual {v5}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object v4

    check-cast v4, Lf/h/a/f/i/j/u4;

    check-cast v4, Lf/h/a/f/i/j/z0;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_9
    :goto_1
    iget-boolean v3, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v3, :cond_a

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_a
    iget-object v3, p1, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v3, Lf/h/a/f/i/j/g1;

    invoke-static {v3, v1}, Lf/h/a/f/i/j/g1;->F(Lf/h/a/f/i/j/g1;Ljava/lang/Iterable;)V

    iget-object v1, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    if-nez v1, :cond_b

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_3

    :cond_b
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-static {}, Lf/h/a/f/i/j/h1;->B()Lf/h/a/f/i/j/h1$a;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-boolean v7, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v7, :cond_c

    invoke-virtual {v5}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_c
    iget-object v7, v5, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v7, Lf/h/a/f/i/j/h1;

    invoke-static {v7, v6}, Lf/h/a/f/i/j/h1;->v(Lf/h/a/f/i/j/h1;I)V

    iget-object v6, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-eqz v4, :cond_e

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-boolean v6, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v6, :cond_d

    invoke-virtual {v5}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v5, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_d
    iget-object v6, v5, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v6, Lf/h/a/f/i/j/h1;

    invoke-static {v6, v4}, Lf/h/a/f/i/j/h1;->w(Lf/h/a/f/i/j/h1;Ljava/lang/Iterable;)V

    :cond_e
    invoke-virtual {v5}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object v4

    check-cast v4, Lf/h/a/f/i/j/u4;

    check-cast v4, Lf/h/a/f/i/j/h1;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_f
    :goto_3
    iget-boolean v3, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v3, :cond_10

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, p1, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_10
    iget-object v3, p1, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v3, Lf/h/a/f/i/j/g1;

    invoke-static {v3, v1}, Lf/h/a/f/i/j/g1;->H(Lf/h/a/f/i/j/g1;Ljava/lang/Iterable;)V

    iget-boolean v1, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v1, :cond_11

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v2, v0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_11
    iget-object v1, v0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v1, Lf/h/a/f/i/j/y0;

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4;

    check-cast p1, Lf/h/a/f/i/j/g1;

    invoke-static {v1, p1}, Lf/h/a/f/i/j/y0;->v(Lf/h/a/f/i/j/y0;Lf/h/a/f/i/j/g1;)V

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4;

    check-cast p1, Lf/h/a/f/i/j/y0;

    return-object p1
.end method

.method public final b(Lf/h/a/f/j/b/ea;)V
    .locals 8
    .param p1    # Lf/h/a/f/j/b/ea;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Lf/h/a/f/j/b/ea;->a()I

    move-result v0

    iget-object v1, p1, Lf/h/a/f/j/b/ea;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lf/h/a/f/j/b/da;->e:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Ljava/util/BitSet;->set(IZ)V

    :cond_0
    iget-object v1, p1, Lf/h/a/f/j/b/ea;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lf/h/a/f/j/b/da;->d:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Ljava/util/BitSet;->set(IZ)V

    :cond_1
    iget-object v1, p1, Lf/h/a/f/j/b/ea;->e:Ljava/lang/Long;

    const-wide/16 v2, 0x3e8

    if-eqz v1, :cond_3

    iget-object v1, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    iget-object v4, p1, Lf/h/a/f/j/b/ea;->e:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    div-long/2addr v4, v2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lf/h/a/f/j/b/da;->f:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v1, p1, Lf/h/a/f/j/b/ea;->f:Ljava/lang/Long;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lf/h/a/f/j/b/da;->g:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {p1}, Lf/h/a/f/j/b/ea;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/List;->clear()V

    :cond_5
    invoke-static {}, Lf/h/a/f/i/j/f9;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lf/h/a/f/j/b/da;->h:Lf/h/a/f/j/b/ba;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v4, p0, Lf/h/a/f/j/b/da;->a:Ljava/lang/String;

    sget-object v5, Lf/h/a/f/j/b/p;->c0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v4, v5}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lf/h/a/f/j/b/ea;->h()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/List;->clear()V

    :cond_6
    invoke-static {}, Lf/h/a/f/i/j/f9;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lf/h/a/f/j/b/da;->h:Lf/h/a/f/j/b/ba;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v4, p0, Lf/h/a/f/j/b/da;->a:Ljava/lang/String;

    sget-object v5, Lf/h/a/f/j/b/p;->c0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v4, v5}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object p1, p1, Lf/h/a/f/j/b/ea;->f:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    div-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    return-void

    :cond_8
    iget-object p1, p1, Lf/h/a/f/j/b/ea;->f:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    div-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    return-void
.end method
