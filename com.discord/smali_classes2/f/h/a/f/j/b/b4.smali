.class public Lf/h/a/f/j/b/b4;
.super Landroid/content/BroadcastReceiver;
.source "com.google.android.gms:play-services-measurement@@18.0.0"


# instance fields
.field public final a:Lf/h/a/f/j/b/k9;

.field public b:Z

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lf/h/a/f/j/b/b4;

    return-void
.end method

.method public constructor <init>(Lf/h/a/f/j/b/k9;)V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->P()V

    iget-object v0, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/r4;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/r4;->b()V

    iget-boolean v0, p0, Lf/h/a/f/j/b/b4;->b:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v1, "Unregistering connectivity change receiver"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/j/b/b4;->b:Z

    iput-boolean v0, p0, Lf/h/a/f/j/b/b4;->c:Z

    iget-object v0, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    iget-object v0, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    :try_start_0
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v1}, Lf/h/a/f/j/b/k9;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Failed to unregister the network broadcast receiver"

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object p1, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {p1}, Lf/h/a/f/j/b/k9;->P()V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {p2}, Lf/h/a/f/j/b/k9;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v0, "NetworkBroadcastReceiver received action"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const-string p2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p1, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {p1}, Lf/h/a/f/j/b/k9;->J()Lf/h/a/f/j/b/x3;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/a/f/j/b/x3;->u()Z

    move-result p1

    iget-boolean p2, p0, Lf/h/a/f/j/b/b4;->c:Z

    if-eq p2, p1, :cond_0

    iput-boolean p1, p0, Lf/h/a/f/j/b/b4;->c:Z

    iget-object p2, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {p2}, Lf/h/a/f/j/b/k9;->f()Lf/h/a/f/j/b/r4;

    move-result-object p2

    new-instance v0, Lf/h/a/f/j/b/e4;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/j/b/e4;-><init>(Lf/h/a/f/j/b/b4;Z)V

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    :cond_0
    return-void

    :cond_1
    iget-object p2, p0, Lf/h/a/f/j/b/b4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {p2}, Lf/h/a/f/j/b/k9;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v0, "NetworkBroadcastReceiver received unknown action"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
