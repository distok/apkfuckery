.class public abstract Lf/h/a/f/j/b/i9;
.super Lf/h/a/f/j/b/s5;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/j/b/t5;


# instance fields
.field public final b:Lf/h/a/f/j/b/k9;

.field public c:Z


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/k9;)V
    .locals 1

    iget-object v0, p1, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-direct {p0, v0}, Lf/h/a/f/j/b/s5;-><init>(Lf/h/a/f/j/b/u4;)V

    iput-object p1, p0, Lf/h/a/f/j/b/i9;->b:Lf/h/a/f/j/b/k9;

    iget v0, p1, Lf/h/a/f/j/b/k9;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lf/h/a/f/j/b/k9;->o:I

    return-void
.end method


# virtual methods
.method public m()Lf/h/a/f/j/b/q9;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/i9;->b:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->N()Lf/h/a/f/j/b/q9;

    move-result-object v0

    return-object v0
.end method

.method public final n()V
    .locals 2

    iget-boolean v0, p0, Lf/h/a/f/j/b/i9;->c:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final o()V
    .locals 3

    iget-boolean v0, p0, Lf/h/a/f/j/b/i9;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/j/b/i9;->p()Z

    iget-object v0, p0, Lf/h/a/f/j/b/i9;->b:Lf/h/a/f/j/b/k9;

    iget v1, v0, Lf/h/a/f/j/b/k9;->p:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, v0, Lf/h/a/f/j/b/k9;->p:I

    iput-boolean v2, p0, Lf/h/a/f/j/b/i9;->c:Z

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t initialize twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract p()Z
.end method

.method public q()Lf/h/a/f/j/b/g;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/i9;->b:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->K()Lf/h/a/f/j/b/g;

    move-result-object v0

    return-object v0
.end method

.method public r()Lf/h/a/f/j/b/p4;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/i9;->b:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->H()Lf/h/a/f/j/b/p4;

    move-result-object v0

    return-object v0
.end method
