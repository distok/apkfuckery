.class public final Lf/h/a/f/j/b/t6;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicReference;

.field public final synthetic e:Lf/h/a/f/j/b/c6;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/c6;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/t6;->e:Lf/h/a/f/j/b/c6;

    iput-object p2, p0, Lf/h/a/f/j/b/t6;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lf/h/a/f/j/b/t6;->d:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/j/b/t6;->d:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v2, p0, Lf/h/a/f/j/b/t6;->e:Lf/h/a/f/j/b/c6;

    iget-object v3, v2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v2}, Lf/h/a/f/j/b/z1;->o()Lf/h/a/f/j/b/n3;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v2, v2, Lf/h/a/f/j/b/n3;->c:Ljava/lang/String;

    sget-object v4, Lf/h/a/f/j/b/p;->M:Lf/h/a/f/j/b/j3;

    invoke-virtual {v3, v2, v4}, Lf/h/a/f/j/b/c;->n(Ljava/lang/String;Lf/h/a/f/j/b/j3;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lf/h/a/f/j/b/t6;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lf/h/a/f/j/b/t6;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v1
.end method
