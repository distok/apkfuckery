.class public final Lf/h/a/f/j/b/l8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/f/j/b/i3;

.field public final synthetic e:Lf/h/a/f/j/b/k8;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/k8;Lf/h/a/f/j/b/i3;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/l8;->e:Lf/h/a/f/j/b/k8;

    iput-object p2, p0, Lf/h/a/f/j/b/l8;->d:Lf/h/a/f/j/b/i3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/j/b/l8;->e:Lf/h/a/f/j/b/k8;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/j/b/l8;->e:Lf/h/a/f/j/b/k8;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lf/h/a/f/j/b/k8;->d:Z

    iget-object v1, p0, Lf/h/a/f/j/b/l8;->e:Lf/h/a/f/j/b/k8;

    iget-object v1, v1, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/q7;->B()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/j/b/l8;->e:Lf/h/a/f/j/b/k8;

    iget-object v1, v1, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v2, "Connected to remote service"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lf/h/a/f/j/b/l8;->e:Lf/h/a/f/j/b/k8;

    iget-object v1, v1, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    iget-object v2, p0, Lf/h/a/f/j/b/l8;->d:Lf/h/a/f/j/b/i3;

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    const-string v3, "null reference"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v2, v1, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    invoke-virtual {v1}, Lf/h/a/f/j/b/q7;->F()V

    invoke-virtual {v1}, Lf/h/a/f/j/b/q7;->H()V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
