.class public final Lf/h/a/f/j/b/p9;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Landroid/os/Bundle;

.field public final synthetic f:Lf/h/a/f/j/b/n9;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/n9;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/p9;->f:Lf/h/a/f/j/b/n9;

    iput-object p2, p0, Lf/h/a/f/j/b/p9;->d:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/f/j/b/p9;->e:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    iget-object v0, p0, Lf/h/a/f/j/b/p9;->f:Lf/h/a/f/j/b/n9;

    iget-object v0, v0, Lf/h/a/f/j/b/n9;->a:Lf/h/a/f/j/b/k9;

    iget-object v0, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/p9;->d:Ljava/lang/String;

    iget-object v4, p0, Lf/h/a/f/j/b/p9;->e:Landroid/os/Bundle;

    iget-object v0, p0, Lf/h/a/f/j/b/p9;->f:Lf/h/a/f/j/b/n9;

    iget-object v0, v0, Lf/h/a/f/j/b/n9;->a:Lf/h/a/f/j/b/k9;

    iget-object v0, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-string v3, "_err"

    const-string v5, "auto"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Lf/h/a/f/j/b/t9;->B(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;JZZ)Lcom/google/android/gms/measurement/internal/zzaq;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/p9;->f:Lf/h/a/f/j/b/n9;

    iget-object v1, v1, Lf/h/a/f/j/b/n9;->a:Lf/h/a/f/j/b/k9;

    iget-object v2, p0, Lf/h/a/f/j/b/p9;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lf/h/a/f/j/b/k9;->m(Lcom/google/android/gms/measurement/internal/zzaq;Ljava/lang/String;)V

    return-void
.end method
