.class public final synthetic Lf/h/a/f/j/b/e6;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/j/b/c6;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/c6;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/e6;->d:Lf/h/a/f/j/b/c6;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 16

    move-object/from16 v1, p0

    iget-object v0, v1, Lf/h/a/f/j/b/e6;->d:Lf/h/a/f/j/b/c6;

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->x:Lf/h/a/f/j/b/f4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/f4;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v2, "Deferred Deep Link already retrieved. Not fetching again."

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->y:Lf/h/a/f/j/b/h4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/h4;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v4

    iget-object v4, v4, Lf/h/a/f/j/b/d4;->y:Lf/h/a/f/j/b/h4;

    const-wide/16 v5, 0x1

    add-long v7, v2, v5

    invoke-virtual {v4, v7, v8}, Lf/h/a/f/j/b/h4;->b(J)V

    const-wide/16 v7, 0x5

    const/4 v4, 0x1

    cmp-long v9, v2, v7

    if-ltz v9, :cond_1

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v3, "Permanently failed to retrieve Deferred Deep Link. Reached maximum retries."

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->x:Lf/h/a/f/j/b/f4;

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/f4;->a(Z)V

    return-void

    :cond_1
    iget-object v2, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/r4;->b()V

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->n()Lf/h/a/f/j/b/d7;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->q(Lf/h/a/f/j/b/r5;)V

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v3, v0, Lf/h/a/f/j/b/n3;->c:Ljava/lang/String;

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v7

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->b()V

    iget-object v0, v7, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-object v0, v7, Lf/h/a/f/j/b/d4;->m:Ljava/lang/String;

    const-string v10, ""

    if-eqz v0, :cond_2

    iget-wide v11, v7, Lf/h/a/f/j/b/d4;->o:J

    cmp-long v0, v8, v11

    if-gez v0, :cond_2

    new-instance v0, Landroid/util/Pair;

    iget-object v8, v7, Lf/h/a/f/j/b/d4;->m:Ljava/lang/String;

    iget-boolean v7, v7, Lf/h/a/f/j/b/d4;->n:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {v0, v8, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object v0, v7, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v11, Lf/h/a/f/j/b/p;->b:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v3, v11}, Lf/h/a/f/j/b/c;->n(Ljava/lang/String;Lf/h/a/f/j/b/j3;)J

    move-result-wide v11

    add-long/2addr v11, v8

    iput-wide v11, v7, Lf/h/a/f/j/b/d4;->o:J

    :try_start_0
    iget-object v0, v7, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/a/a/a;->b(Landroid/content/Context;)Lf/h/a/f/a/a/a$a;

    move-result-object v0

    iget-object v8, v0, Lf/h/a/f/a/a/a$a;->a:Ljava/lang/String;

    iput-object v8, v7, Lf/h/a/f/j/b/d4;->m:Ljava/lang/String;

    iget-boolean v0, v0, Lf/h/a/f/a/a/a$a;->b:Z

    iput-boolean v0, v7, Lf/h/a/f/j/b/d4;->n:Z

    if-nez v8, :cond_3

    iput-object v10, v7, Lf/h/a/f/j/b/d4;->m:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v8

    iget-object v8, v8, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v9, "Unable to get advertising id"

    invoke-virtual {v8, v9, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v10, v7, Lf/h/a/f/j/b/d4;->m:Ljava/lang/String;

    :cond_3
    :goto_0
    new-instance v0, Landroid/util/Pair;

    iget-object v8, v7, Lf/h/a/f/j/b/d4;->m:Ljava/lang/String;

    iget-boolean v7, v7, Lf/h/a/f/j/b/d4;->n:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {v0, v8, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    iget-object v7, v2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v7}, Lf/h/a/f/j/b/c;->y()Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v7, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/CharSequence;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    goto/16 :goto_6

    :cond_4
    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->n()Lf/h/a/f/j/b/d7;

    move-result-object v7

    invoke-virtual {v7}, Lf/h/a/f/j/b/r5;->o()V

    iget-object v7, v7, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v7, v7, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/ConnectivityManager;

    :try_start_1
    invoke-virtual {v7}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v7
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    const/4 v7, 0x0

    :goto_2
    const/4 v9, 0x0

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x1

    goto :goto_3

    :cond_5
    const/4 v7, 0x0

    :goto_3
    if-nez v7, :cond_6

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v2, "Network is not available for Deferred Deep Link request. Skipping"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_6
    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v7

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    const-wide/32 v11, 0x8101

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v13

    iget-object v13, v13, Lf/h/a/f/j/b/d4;->y:Lf/h/a/f/j/b/h4;

    invoke-virtual {v13}, Lf/h/a/f/j/b/h4;->a()J

    move-result-wide v13

    sub-long/2addr v13, v5

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_2
    invoke-static {v0}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v3}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    const-string v5, "https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const-string v15, "v%s.%s"

    const/4 v8, 0x2

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v4, v9

    invoke-virtual {v7}, Lf/h/a/f/j/b/t9;->y0()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x1

    aput-object v11, v4, v12

    invoke-static {v15, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v9

    aput-object v0, v6, v12

    aput-object v3, v6, v8

    const/4 v0, 0x3

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v7, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v4, v4, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    const-string v5, "debug.deferred.deeplink"

    invoke-virtual {v4, v5, v10}, Lf/h/a/f/j/b/c;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "&ddl_test=1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_7
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v8, v4

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    :goto_4
    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v4

    iget-object v4, v4, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v5, "Failed to create BOW URL for Deferred Deep Link. exception"

    invoke-virtual {v4, v5, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v8, 0x0

    :goto_5
    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->n()Lf/h/a/f/j/b/d7;

    move-result-object v0

    new-instance v4, Lf/h/a/f/j/b/x4;

    invoke-direct {v4, v2}, Lf/h/a/f/j/b/x4;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/r5;->o()V

    const-string v2, "null reference"

    invoke-static {v8, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v2

    new-instance v5, Lf/h/a/f/j/b/f7;

    invoke-direct {v5, v0, v3, v8, v4}, Lf/h/a/f/j/b/f7;-><init>(Lf/h/a/f/j/b/d7;Ljava/lang/String;Ljava/net/URL;Lf/h/a/f/j/b/x4;)V

    invoke-virtual {v2, v5}, Lf/h/a/f/j/b/r4;->x(Ljava/lang/Runnable;)V

    goto :goto_7

    :cond_8
    :goto_6
    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v2, "ADID unavailable to retrieve Deferred Deep Link. Skipping"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_7
    return-void
.end method
