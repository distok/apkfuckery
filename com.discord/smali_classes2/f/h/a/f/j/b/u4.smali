.class public Lf/h/a/f/j/b/u4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Lf/h/a/f/j/b/t5;


# static fields
.field public static volatile H:Lf/h/a/f/j/b/u4;


# instance fields
.field public volatile A:Ljava/lang/Boolean;

.field public B:Ljava/lang/Boolean;

.field public C:Ljava/lang/Boolean;

.field public volatile D:Z

.field public E:I

.field public F:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final G:J

.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Lf/h/a/f/j/b/ga;

.field public final g:Lf/h/a/f/j/b/c;

.field public final h:Lf/h/a/f/j/b/d4;

.field public final i:Lf/h/a/f/j/b/q3;

.field public final j:Lf/h/a/f/j/b/r4;

.field public final k:Lf/h/a/f/j/b/w8;

.field public final l:Lf/h/a/f/j/b/t9;

.field public final m:Lf/h/a/f/j/b/o3;

.field public final n:Lf/h/a/f/f/n/c;

.field public final o:Lf/h/a/f/j/b/h7;

.field public final p:Lf/h/a/f/j/b/c6;

.field public final q:Lf/h/a/f/j/b/a;

.field public final r:Lf/h/a/f/j/b/d7;

.field public s:Lf/h/a/f/j/b/m3;

.field public t:Lf/h/a/f/j/b/q7;

.field public u:Lf/h/a/f/j/b/j;

.field public v:Lf/h/a/f/j/b/n3;

.field public w:Lf/h/a/f/j/b/m4;

.field public x:Z

.field public y:Ljava/lang/Boolean;

.field public z:J


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/y5;)V
    .locals 10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/j/b/u4;->x:Z

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lf/h/a/f/j/b/u4;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p1, Lf/h/a/f/j/b/y5;->a:Landroid/content/Context;

    new-instance v2, Lf/h/a/f/j/b/ga;

    invoke-direct {v2}, Lf/h/a/f/j/b/ga;-><init>()V

    iput-object v2, p0, Lf/h/a/f/j/b/u4;->f:Lf/h/a/f/j/b/ga;

    sput-object v2, Lf/h/a/f/f/n/g;->d:Lf/h/a/f/j/b/ga;

    iput-object v1, p0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    iget-object v2, p1, Lf/h/a/f/j/b/y5;->b:Ljava/lang/String;

    iput-object v2, p0, Lf/h/a/f/j/b/u4;->b:Ljava/lang/String;

    iget-object v2, p1, Lf/h/a/f/j/b/y5;->c:Ljava/lang/String;

    iput-object v2, p0, Lf/h/a/f/j/b/u4;->c:Ljava/lang/String;

    iget-object v2, p1, Lf/h/a/f/j/b/y5;->d:Ljava/lang/String;

    iput-object v2, p0, Lf/h/a/f/j/b/u4;->d:Ljava/lang/String;

    iget-boolean v2, p1, Lf/h/a/f/j/b/y5;->h:Z

    iput-boolean v2, p0, Lf/h/a/f/j/b/u4;->e:Z

    iget-object v2, p1, Lf/h/a/f/j/b/y5;->e:Ljava/lang/Boolean;

    iput-object v2, p0, Lf/h/a/f/j/b/u4;->A:Ljava/lang/Boolean;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lf/h/a/f/j/b/u4;->D:Z

    iget-object v3, p1, Lf/h/a/f/j/b/y5;->g:Lcom/google/android/gms/internal/measurement/zzae;

    if-eqz v3, :cond_1

    iget-object v4, v3, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    if-eqz v4, :cond_1

    const-string v5, "measurementEnabled"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    instance-of v5, v4, Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    check-cast v4, Ljava/lang/Boolean;

    iput-object v4, p0, Lf/h/a/f/j/b/u4;->B:Ljava/lang/Boolean;

    :cond_0
    iget-object v3, v3, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    const-string v4, "measurementDeactivated"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    instance-of v4, v3, Ljava/lang/Boolean;

    if-eqz v4, :cond_1

    check-cast v3, Ljava/lang/Boolean;

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->C:Ljava/lang/Boolean;

    :cond_1
    sget-object v3, Lf/h/a/f/i/j/l2;->g:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    sget-object v4, Lf/h/a/f/i/j/l2;->h:Lf/h/a/f/i/j/t2;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    if-nez v5, :cond_2

    move-object v5, v1

    :cond_2
    const/4 v6, 0x0

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lf/h/a/f/i/j/t2;->a()Landroid/content/Context;

    move-result-object v4

    if-eq v4, v5, :cond_5

    :cond_3
    invoke-static {}, Lf/h/a/f/i/j/y1;->c()V

    invoke-static {}, Lf/h/a/f/i/j/s2;->b()V

    const-class v4, Lf/h/a/f/i/j/c2;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v7, Lf/h/a/f/i/j/c2;->c:Lf/h/a/f/i/j/c2;

    if-eqz v7, :cond_4

    iget-object v8, v7, Lf/h/a/f/i/j/c2;->a:Landroid/content/Context;

    if-eqz v8, :cond_4

    iget-object v7, v7, Lf/h/a/f/i/j/c2;->b:Landroid/database/ContentObserver;

    if-eqz v7, :cond_4

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lf/h/a/f/i/j/c2;->c:Lf/h/a/f/i/j/c2;

    iget-object v8, v8, Lf/h/a/f/i/j/c2;->b:Landroid/database/ContentObserver;

    invoke-virtual {v7, v8}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_4
    sput-object v6, Lf/h/a/f/i/j/c2;->c:Lf/h/a/f/i/j/c2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v4

    new-instance v4, Lf/h/a/f/i/j/k2;

    invoke-direct {v4, v5}, Lf/h/a/f/i/j/k2;-><init>(Landroid/content/Context;)V

    invoke-static {v4}, Lf/h/a/f/f/n/g;->D0(Lf/h/a/f/i/j/z2;)Lf/h/a/f/i/j/z2;

    move-result-object v4

    new-instance v7, Lf/h/a/f/i/j/v1;

    invoke-direct {v7, v5, v4}, Lf/h/a/f/i/j/v1;-><init>(Landroid/content/Context;Lf/h/a/f/i/j/z2;)V

    sput-object v7, Lf/h/a/f/i/j/l2;->h:Lf/h/a/f/i/j/t2;

    sget-object v4, Lf/h/a/f/i/j/l2;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    :cond_5
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    sget-object v3, Lf/h/a/f/f/n/d;->a:Lf/h/a/f/f/n/d;

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    iget-object v3, p1, Lf/h/a/f/j/b/y5;->i:Ljava/lang/Long;

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    goto :goto_0

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :goto_0
    iput-wide v3, p0, Lf/h/a/f/j/b/u4;->G:J

    new-instance v3, Lf/h/a/f/j/b/c;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/c;-><init>(Lf/h/a/f/j/b/u4;)V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    new-instance v3, Lf/h/a/f/j/b/d4;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/d4;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/r5;->p()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->h:Lf/h/a/f/j/b/d4;

    new-instance v3, Lf/h/a/f/j/b/q3;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/q3;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/r5;->p()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->i:Lf/h/a/f/j/b/q3;

    new-instance v3, Lf/h/a/f/j/b/t9;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/t9;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/r5;->p()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->l:Lf/h/a/f/j/b/t9;

    new-instance v3, Lf/h/a/f/j/b/o3;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/o3;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/r5;->p()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->m:Lf/h/a/f/j/b/o3;

    new-instance v3, Lf/h/a/f/j/b/a;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/a;-><init>(Lf/h/a/f/j/b/u4;)V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->q:Lf/h/a/f/j/b/a;

    new-instance v3, Lf/h/a/f/j/b/h7;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/h7;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/a5;->u()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->o:Lf/h/a/f/j/b/h7;

    new-instance v3, Lf/h/a/f/j/b/c6;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/c6;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/a5;->u()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->p:Lf/h/a/f/j/b/c6;

    new-instance v3, Lf/h/a/f/j/b/w8;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/w8;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/a5;->u()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->k:Lf/h/a/f/j/b/w8;

    new-instance v3, Lf/h/a/f/j/b/d7;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/d7;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/r5;->p()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->r:Lf/h/a/f/j/b/d7;

    new-instance v3, Lf/h/a/f/j/b/r4;

    invoke-direct {v3, p0}, Lf/h/a/f/j/b/r4;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v3}, Lf/h/a/f/j/b/r5;->p()V

    iput-object v3, p0, Lf/h/a/f/j/b/u4;->j:Lf/h/a/f/j/b/r4;

    iget-object v4, p1, Lf/h/a/f/j/b/y5;->g:Lcom/google/android/gms/internal/measurement/zzae;

    if-eqz v4, :cond_7

    iget-wide v4, v4, Lcom/google/android/gms/internal/measurement/zzae;->e:J

    const-wide/16 v7, 0x0

    cmp-long v9, v4, v7

    if-eqz v9, :cond_7

    const/4 v0, 0x1

    :cond_7
    xor-int/2addr v0, v2

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Landroid/app/Application;

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v1

    iget-object v2, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Application;

    if-eqz v2, :cond_a

    iget-object v2, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    iget-object v4, v1, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    if-nez v4, :cond_8

    new-instance v4, Lf/h/a/f/j/b/y6;

    invoke-direct {v4, v1, v6}, Lf/h/a/f/j/b/y6;-><init>(Lf/h/a/f/j/b/c6;Lf/h/a/f/j/b/d6;)V

    iput-object v4, v1, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    :cond_8
    if-eqz v0, :cond_a

    iget-object v0, v1, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    invoke-virtual {v2, v0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    iget-object v0, v1, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    invoke-virtual {v2, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v1, "Registered activity lifecycle callback"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v1, "Application context is not an Application"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_a
    :goto_1
    new-instance v0, Lf/h/a/f/j/b/w4;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/j/b/w4;-><init>(Lf/h/a/f/j/b/u4;Lf/h/a/f/j/b/y5;)V

    invoke-virtual {v3, v0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit v4

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzae;Ljava/lang/Long;)Lf/h/a/f/j/b/u4;
    .locals 11

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/internal/measurement/zzae;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/internal/measurement/zzae;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzae;

    iget-wide v2, p1, Lcom/google/android/gms/internal/measurement/zzae;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/internal/measurement/zzae;->e:J

    iget-boolean v6, p1, Lcom/google/android/gms/internal/measurement/zzae;->f:Z

    iget-object v7, p1, Lcom/google/android/gms/internal/measurement/zzae;->g:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/internal/measurement/zzae;-><init>(JJZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object p1, v0

    :cond_1
    const-string v0, "null reference"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null reference"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lf/h/a/f/j/b/u4;->H:Lf/h/a/f/j/b/u4;

    if-nez v0, :cond_3

    const-class v0, Lf/h/a/f/j/b/u4;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/a/f/j/b/u4;->H:Lf/h/a/f/j/b/u4;

    if-nez v1, :cond_2

    new-instance v1, Lf/h/a/f/j/b/y5;

    invoke-direct {v1, p0, p1, p2}, Lf/h/a/f/j/b/y5;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzae;Ljava/lang/Long;)V

    new-instance p0, Lf/h/a/f/j/b/u4;

    invoke-direct {p0, v1}, Lf/h/a/f/j/b/u4;-><init>(Lf/h/a/f/j/b/y5;)V

    sput-object p0, Lf/h/a/f/j/b/u4;->H:Lf/h/a/f/j/b/u4;

    :cond_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_3
    if-eqz p1, :cond_4

    iget-object p0, p1, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    if-eqz p0, :cond_4

    const-string p2, "dataCollectionDefaultEnabled"

    invoke-virtual {p0, p2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_4

    sget-object p0, Lf/h/a/f/j/b/u4;->H:Lf/h/a/f/j/b/u4;

    iget-object p1, p1, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    const-string p2, "dataCollectionDefaultEnabled"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/f/j/b/u4;->A:Ljava/lang/Boolean;

    :cond_4
    :goto_0
    sget-object p0, Lf/h/a/f/j/b/u4;->H:Lf/h/a/f/j/b/u4;

    return-object p0
.end method

.method public static c(Lf/h/a/f/j/b/s5;)V
    .locals 1

    if-eqz p0, :cond_0

    return-void

    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Component not created"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static p(Lf/h/a/f/j/b/a5;)V
    .locals 3

    if-eqz p0, :cond_1

    iget-boolean v0, p0, Lf/h/a/f/j/b/a5;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1b

    const-string v2, "Component not initialized: "

    invoke-static {v1, v2, p0}, Lf/e/c/a/a;->e(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Component not created"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static q(Lf/h/a/f/j/b/r5;)V
    .locals 3

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lf/h/a/f/j/b/r5;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1b

    const-string v2, "Component not initialized: "

    invoke-static {v1, v2, p0}, Lf/e/c/a/a;->e(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Component not created"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public final A()Lf/h/a/f/j/b/a;
    .locals 2

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->q:Lf/h/a/f/j/b/a;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Component not created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final B()Z
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->A:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->A:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final a()Lf/h/a/f/j/b/c;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    return-object v0
.end method

.method public final d()Z
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->e()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/r4;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v0}, Lf/h/a/f/j/b/c;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/u4;->C:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    return v0

    :cond_1
    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->H0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->h()Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x8

    return v0

    :cond_2
    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->x()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    return v1

    :cond_3
    const/4 v0, 0x3

    return v0

    :cond_4
    iget-object v0, p0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    const-string v2, "firebase_analytics_collection_enabled"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    return v1

    :cond_5
    const/4 v0, 0x4

    return v0

    :cond_6
    iget-object v0, p0, Lf/h/a/f/j/b/u4;->B:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    return v1

    :cond_7
    const/4 v0, 0x5

    return v0

    :cond_8
    const-string v0, "isMeasurementExplicitlyDisabled"

    invoke-static {v0}, Lf/h/a/f/f/h/i/h;->a(Ljava/lang/String;)Lf/h/a/f/f/h/i/h;

    move-result-object v0

    iget-boolean v0, v0, Lf/h/a/f/f/h/i/h;->c:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x6

    return v0

    :cond_9
    iget-object v0, p0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->S:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->A:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->A:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    return v1

    :cond_a
    const/4 v0, 0x7

    return v0

    :cond_b
    return v1
.end method

.method public final f()Lf/h/a/f/j/b/r4;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->j:Lf/h/a/f/j/b/r4;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->q(Lf/h/a/f/j/b/r5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->j:Lf/h/a/f/j/b/r4;

    return-object v0
.end method

.method public final g()Lf/h/a/f/j/b/q3;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->i:Lf/h/a/f/j/b/q3;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->q(Lf/h/a/f/j/b/r5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->i:Lf/h/a/f/j/b/q3;

    return-object v0
.end method

.method public final h()Z
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/r4;->b()V

    iget-boolean v0, p0, Lf/h/a/f/j/b/u4;->D:Z

    return v0
.end method

.method public final i()Lf/h/a/f/f/n/c;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    return-object v0
.end method

.method public final j()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final k()V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call on client side"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final l()Lf/h/a/f/j/b/ga;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->f:Lf/h/a/f/j/b/ga;

    return-object v0
.end method

.method public final m()Z
    .locals 6
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/j/b/u4;->x:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/r4;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->y:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lf/h/a/f/j/b/u4;->z:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lf/h/a/f/j/b/u4;->z:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v4, v0, v2

    if-lez v4, :cond_5

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/j/b/u4;->z:J

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v0

    const-string v1, "android.permission.INTERNET"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/t9;->p0(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v0

    const-string v3, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v3}, Lf/h/a/f/j/b/t9;->p0(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/f/o/b;->a(Landroid/content/Context;)Lf/h/a/f/f/o/a;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/f/o/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v0}, Lf/h/a/f/j/b/c;->C()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/j/b/n4;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/j/b/t9;->U(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/j/b/u4;->y:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v0

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v3

    invoke-virtual {v3}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v3, v3, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v4, v4, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v5, v5, Lf/h/a/f/j/b/n3;->m:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v5}, Lf/h/a/f/j/b/t9;->c0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v0, v0, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/j/b/u4;->y:Ljava/lang/Boolean;

    :cond_5
    iget-object v0, p0, Lf/h/a/f/j/b/u4;->y:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AppMeasurement is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final n()Lf/h/a/f/j/b/d7;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->r:Lf/h/a/f/j/b/d7;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->q(Lf/h/a/f/j/b/r5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->r:Lf/h/a/f/j/b/d7;

    return-object v0
.end method

.method public final o()Lf/h/a/f/j/b/d4;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->h:Lf/h/a/f/j/b/d4;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->c(Lf/h/a/f/j/b/s5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->h:Lf/h/a/f/j/b/d4;

    return-object v0
.end method

.method public final r()Lf/h/a/f/j/b/w8;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->k:Lf/h/a/f/j/b/w8;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->p(Lf/h/a/f/j/b/a5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->k:Lf/h/a/f/j/b/w8;

    return-object v0
.end method

.method public final s()Lf/h/a/f/j/b/c6;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->p:Lf/h/a/f/j/b/c6;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->p(Lf/h/a/f/j/b/a5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->p:Lf/h/a/f/j/b/c6;

    return-object v0
.end method

.method public final t()Lf/h/a/f/j/b/t9;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->l:Lf/h/a/f/j/b/t9;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->c(Lf/h/a/f/j/b/s5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->l:Lf/h/a/f/j/b/t9;

    return-object v0
.end method

.method public final u()Lf/h/a/f/j/b/o3;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->m:Lf/h/a/f/j/b/o3;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->c(Lf/h/a/f/j/b/s5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->m:Lf/h/a/f/j/b/o3;

    return-object v0
.end method

.method public final v()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final w()Lf/h/a/f/j/b/h7;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->o:Lf/h/a/f/j/b/h7;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->p(Lf/h/a/f/j/b/a5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->o:Lf/h/a/f/j/b/h7;

    return-object v0
.end method

.method public final x()Lf/h/a/f/j/b/q7;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->t:Lf/h/a/f/j/b/q7;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->p(Lf/h/a/f/j/b/a5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->t:Lf/h/a/f/j/b/q7;

    return-object v0
.end method

.method public final y()Lf/h/a/f/j/b/j;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->u:Lf/h/a/f/j/b/j;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->q(Lf/h/a/f/j/b/r5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->u:Lf/h/a/f/j/b/j;

    return-object v0
.end method

.method public final z()Lf/h/a/f/j/b/n3;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->v:Lf/h/a/f/j/b/n3;

    invoke-static {v0}, Lf/h/a/f/j/b/u4;->p(Lf/h/a/f/j/b/a5;)V

    iget-object v0, p0, Lf/h/a/f/j/b/u4;->v:Lf/h/a/f/j/b/n3;

    return-object v0
.end method
