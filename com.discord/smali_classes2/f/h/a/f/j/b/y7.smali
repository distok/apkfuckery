.class public final Lf/h/a/f/j/b/y7;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-sdk@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/f/i/j/fc;

.field public final synthetic e:Lcom/google/android/gms/measurement/internal/zzaq;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/fc;Lcom/google/android/gms/measurement/internal/zzaq;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/y7;->g:Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;

    iput-object p2, p0, Lf/h/a/f/j/b/y7;->d:Lf/h/a/f/i/j/fc;

    iput-object p3, p0, Lf/h/a/f/j/b/y7;->e:Lcom/google/android/gms/measurement/internal/zzaq;

    iput-object p4, p0, Lf/h/a/f/j/b/y7;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    iget-object v0, p0, Lf/h/a/f/j/b/y7;->g:Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->x()Lf/h/a/f/j/b/q7;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/y7;->d:Lf/h/a/f/i/j/fc;

    iget-object v2, p0, Lf/h/a/f/j/b/y7;->e:Lcom/google/android/gms/measurement/internal/zzaq;

    iget-object v3, p0, Lf/h/a/f/j/b/y7;->f:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lf/h/a/f/f/c;->b:Lf/h/a/f/f/c;

    iget-object v4, v4, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v4, v4, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const v6, 0xbdfcb8

    invoke-virtual {v5, v4, v6}, Lf/h/a/f/f/c;->b(Landroid/content/Context;I)I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v3, "Not bundling data. Service unavailable or out of date"

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/t9;->P(Lf/h/a/f/i/j/fc;[B)V

    goto :goto_0

    :cond_0
    new-instance v4, Lf/h/a/f/j/b/c8;

    invoke-direct {v4, v0, v2, v3, v1}, Lf/h/a/f/j/b/c8;-><init>(Lf/h/a/f/j/b/q7;Lcom/google/android/gms/measurement/internal/zzaq;Ljava/lang/String;Lf/h/a/f/i/j/fc;)V

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/q7;->z(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
