.class public final Lf/h/a/f/j/b/e5;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/measurement/internal/zzz;

.field public final synthetic e:Lf/h/a/f/j/b/z4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/z4;Lcom/google/android/gms/measurement/internal/zzz;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/e5;->e:Lf/h/a/f/j/b/z4;

    iput-object p2, p0, Lf/h/a/f/j/b/e5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/j/b/e5;->e:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->R()V

    iget-object v0, p0, Lf/h/a/f/j/b/e5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/zzz;->f:Lcom/google/android/gms/measurement/internal/zzku;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/zzku;->M0()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/j/b/e5;->e:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    iget-object v1, p0, Lf/h/a/f/j/b/e5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/gms/measurement/internal/zzz;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/k9;->y(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/zzn;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/k9;->F(Lcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/j/b/e5;->e:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    iget-object v1, p0, Lf/h/a/f/j/b/e5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/gms/measurement/internal/zzz;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/k9;->y(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/zzn;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/k9;->q(Lcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;)V

    :cond_2
    return-void
.end method
