.class public final Lf/h/a/f/j/b/y3;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroidx/annotation/WorkerThread;
.end annotation


# instance fields
.field public final d:Lf/h/a/f/j/b/z3;

.field public final e:I

.field public final f:Ljava/lang/Throwable;

.field public final g:[B

.field public final h:Ljava/lang/String;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/h/a/f/j/b/z3;ILjava/lang/Throwable;[BLjava/util/Map;Lf/h/a/f/j/b/w3;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string p7, "null reference"

    invoke-static {p2, p7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p2, p0, Lf/h/a/f/j/b/y3;->d:Lf/h/a/f/j/b/z3;

    iput p3, p0, Lf/h/a/f/j/b/y3;->e:I

    iput-object p4, p0, Lf/h/a/f/j/b/y3;->f:Ljava/lang/Throwable;

    iput-object p5, p0, Lf/h/a/f/j/b/y3;->g:[B

    iput-object p1, p0, Lf/h/a/f/j/b/y3;->h:Ljava/lang/String;

    iput-object p6, p0, Lf/h/a/f/j/b/y3;->i:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lf/h/a/f/j/b/y3;->d:Lf/h/a/f/j/b/z3;

    iget-object v1, p0, Lf/h/a/f/j/b/y3;->h:Ljava/lang/String;

    iget v2, p0, Lf/h/a/f/j/b/y3;->e:I

    iget-object v3, p0, Lf/h/a/f/j/b/y3;->f:Ljava/lang/Throwable;

    iget-object v4, p0, Lf/h/a/f/j/b/y3;->g:[B

    iget-object v5, p0, Lf/h/a/f/j/b/y3;->i:Ljava/util/Map;

    invoke-interface/range {v0 .. v5}, Lf/h/a/f/j/b/z3;->a(Ljava/lang/String;ILjava/lang/Throwable;[BLjava/util/Map;)V

    return-void
.end method
