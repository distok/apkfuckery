.class public final synthetic Lf/h/a/f/j/b/a9;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/j/b/b9;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/b9;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/a9;->d:Lf/h/a/f/j/b/b9;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    iget-object v0, p0, Lf/h/a/f/j/b/a9;->d:Lf/h/a/f/j/b/b9;

    iget-object v1, v0, Lf/h/a/f/j/b/b9;->f:Lf/h/a/f/j/b/x8;

    iget-wide v5, v0, Lf/h/a/f/j/b/b9;->d:J

    iget-wide v2, v0, Lf/h/a/f/j/b/b9;->e:J

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v4, "Application going to the background"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v4, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    const/4 v4, 0x1

    if-eqz v0, :cond_0

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->w:Lf/h/a/f/j/b/f4;

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/f4;->a(Z)V

    :cond_0
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v0}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/w8;->e:Lf/h/a/f/j/b/d9;

    iget-object v0, v0, Lf/h/a/f/j/b/d9;->c:Lf/h/a/f/j/b/i;

    invoke-virtual {v0}, Lf/h/a/f/j/b/i;->c()V

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v8, Lf/h/a/f/j/b/p;->m0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v8}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    const/4 v8, 0x0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/w8;->e:Lf/h/a/f/j/b/d9;

    iget-wide v9, v0, Lf/h/a/f/j/b/d9;->b:J

    sub-long v9, v2, v9

    iput-wide v2, v0, Lf/h/a/f/j/b/d9;->b:J

    const-string v0, "_et"

    invoke-virtual {v7, v0, v9, v10}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->q()Lf/h/a/f/j/b/h7;

    move-result-object v0

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/h7;->w(Z)Lf/h/a/f/j/b/i7;

    move-result-object v0

    invoke-static {v0, v7, v4}, Lf/h/a/f/j/b/h7;->A(Lf/h/a/f/j/b/i7;Landroid/os/Bundle;Z)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0, v8, v4, v2, v3}, Lf/h/a/f/j/b/w8;->w(ZZJ)Z

    :cond_2
    iget-object v0, v1, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->n()Lf/h/a/f/j/b/c6;

    move-result-object v2

    const-string v3, "auto"

    const-string v4, "_ab"

    invoke-virtual/range {v2 .. v7}, Lf/h/a/f/j/b/c6;->E(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    return-void
.end method
