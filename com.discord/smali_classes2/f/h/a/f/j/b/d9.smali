.class public final Lf/h/a/f/j/b/d9;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public a:J

.field public b:J

.field public final c:Lf/h/a/f/j/b/i;

.field public final synthetic d:Lf/h/a/f/j/b/w8;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/w8;)V
    .locals 2

    iput-object p1, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/f/j/b/c9;

    iget-object v1, p1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-direct {v0, p0, v1}, Lf/h/a/f/j/b/c9;-><init>(Lf/h/a/f/j/b/d9;Lf/h/a/f/j/b/t5;)V

    iput-object v0, p0, Lf/h/a/f/j/b/d9;->c:Lf/h/a/f/j/b/i;

    iget-object p1, p1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p1, p1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast p1, Lf/h/a/f/f/n/d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/j/b/d9;->a:J

    iput-wide v0, p0, Lf/h/a/f/j/b/d9;->b:J

    return-void
.end method


# virtual methods
.method public final a(ZZJ)Z
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-static {}, Lf/h/a/f/i/j/r9;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->q0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->u:Lf/h/a/f/j/b/h4;

    iget-object v1, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/h4;->b(J)V

    :cond_1
    iget-wide v0, p0, Lf/h/a/f/j/b/d9;->a:J

    sub-long v0, p3, v0

    if-nez p1, :cond_2

    const-wide/16 v2, 0x3e8

    cmp-long p1, v0, v2

    if-gez p1, :cond_2

    iget-object p1, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const-string p3, "Screen exposed for less than 1000 ms. Event not sent. time"

    invoke-virtual {p1, p3, p2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 p1, 0x0

    return p1

    :cond_2
    iget-object p1, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object p1, p1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p1, p1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->T:Lf/h/a/f/j/b/j3;

    invoke-virtual {p1, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p1

    if-eqz p1, :cond_4

    if-nez p2, :cond_4

    sget-object p1, Lf/h/a/f/i/j/s9;->e:Lf/h/a/f/i/j/s9;

    invoke-virtual {p1}, Lf/h/a/f/i/j/s9;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/v9;

    invoke-interface {p1}, Lf/h/a/f/i/j/v9;->a()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object p1, p1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p1, p1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v0, Lf/h/a/f/j/b/p;->V:Lf/h/a/f/j/b/j3;

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-wide v0, p0, Lf/h/a/f/j/b/d9;->b:J

    sub-long v0, p3, v0

    iput-wide p3, p0, Lf/h/a/f/j/b/d9;->b:J

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lf/h/a/f/j/b/d9;->b()J

    move-result-wide v0

    :cond_4
    :goto_0
    iget-object p1, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "Recording user engagement, ms"

    invoke-virtual {p1, v3, v2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "_et"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v0}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    iget-object v2, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v2}, Lf/h/a/f/j/b/z1;->q()Lf/h/a/f/j/b/h7;

    move-result-object v2

    invoke-virtual {v2, v0}, Lf/h/a/f/j/b/h7;->w(Z)Lf/h/a/f/j/b/i7;

    move-result-object v0

    invoke-static {v0, p1, v1}, Lf/h/a/f/j/b/h7;->A(Lf/h/a/f/j/b/i7;Landroid/os/Bundle;Z)V

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->T:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->U:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz p2, :cond_5

    const-wide/16 v2, 0x1

    const-string v0, "_fr"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_5
    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->U:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_6

    if-nez p2, :cond_7

    :cond_6
    iget-object p2, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {p2}, Lf/h/a/f/j/b/z1;->n()Lf/h/a/f/j/b/c6;

    move-result-object p2

    const-string v0, "auto"

    const-string v2, "_e"

    invoke-virtual {p2, v0, v2, p1}, Lf/h/a/f/j/b/c6;->H(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_7
    iput-wide p3, p0, Lf/h/a/f/j/b/d9;->a:J

    iget-object p1, p0, Lf/h/a/f/j/b/d9;->c:Lf/h/a/f/j/b/i;

    invoke-virtual {p1}, Lf/h/a/f/j/b/i;->c()V

    iget-object p1, p0, Lf/h/a/f/j/b/d9;->c:Lf/h/a/f/j/b/i;

    const-wide/32 p2, 0x36ee80

    invoke-virtual {p1, p2, p3}, Lf/h/a/f/j/b/i;->b(J)V

    return v1
.end method

.method public final b()J
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lf/h/a/f/j/b/d9;->b:J

    sub-long v2, v0, v2

    iput-wide v0, p0, Lf/h/a/f/j/b/d9;->b:J

    return-wide v2
.end method
