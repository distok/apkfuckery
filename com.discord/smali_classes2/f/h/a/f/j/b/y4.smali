.class public final synthetic Lf/h/a/f/j/b/y4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/j/b/z4;

.field public final e:Lcom/google/android/gms/measurement/internal/zzn;

.field public final f:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/z4;Lcom/google/android/gms/measurement/internal/zzn;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/y4;->d:Lf/h/a/f/j/b/z4;

    iput-object p2, p0, Lf/h/a/f/j/b/y4;->e:Lcom/google/android/gms/measurement/internal/zzn;

    iput-object p3, p0, Lf/h/a/f/j/b/y4;->f:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    iget-object v0, p0, Lf/h/a/f/j/b/y4;->d:Lf/h/a/f/j/b/z4;

    iget-object v1, p0, Lf/h/a/f/j/b/y4;->e:Lcom/google/android/gms/measurement/internal/zzn;

    iget-object v2, p0, Lf/h/a/f/j/b/y4;->f:Landroid/os/Bundle;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->K()Lf/h/a/f/j/b/g;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/zzn;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/i9;->n()V

    iget-object v3, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-static {v1}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    const-string v4, "dep"

    invoke-static {v4}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    const-string v4, ""

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    const-wide/16 v4, 0x0

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v6}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    if-nez v7, :cond_0

    invoke-virtual {v3}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v7

    iget-object v7, v7, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v8, "Param name can\'t be null"

    invoke-virtual {v7, v8}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v8

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Lf/h/a/f/j/b/t9;->D(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_1

    invoke-virtual {v3}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v8

    iget-object v8, v8, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    invoke-virtual {v3}, Lf/h/a/f/j/b/u4;->u()Lf/h/a/f/j/b/o3;

    move-result-object v9

    invoke-virtual {v9, v7}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v9, "Param value can\'t be null"

    invoke-virtual {v8, v9, v7}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v9

    invoke-virtual {v9, v6, v7, v8}, Lf/h/a/f/j/b/t9;->I(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/google/android/gms/measurement/internal/zzap;

    invoke-direct {v2, v6}, Lcom/google/android/gms/measurement/internal/zzap;-><init>(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_3
    new-instance v2, Lcom/google/android/gms/measurement/internal/zzap;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v2, v3}, Lcom/google/android/gms/measurement/internal/zzap;-><init>(Landroid/os/Bundle;)V

    :goto_1
    invoke-virtual {v0}, Lf/h/a/f/j/b/i9;->m()Lf/h/a/f/j/b/q9;

    move-result-object v3

    invoke-static {}, Lf/h/a/f/i/j/a1;->M()Lf/h/a/f/i/j/a1$a;

    move-result-object v6

    iget-boolean v7, v6, Lf/h/a/f/i/j/u4$b;->f:Z

    const/4 v8, 0x0

    if-eqz v7, :cond_4

    invoke-virtual {v6}, Lf/h/a/f/i/j/u4$b;->l()V

    iput-boolean v8, v6, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_4
    iget-object v7, v6, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v7, Lf/h/a/f/i/j/a1;

    invoke-static {v7, v4, v5}, Lf/h/a/f/i/j/a1;->E(Lf/h/a/f/i/j/a1;J)V

    iget-object v4, v2, Lcom/google/android/gms/measurement/internal/zzap;->d:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {}, Lf/h/a/f/i/j/c1;->Q()Lf/h/a/f/i/j/c1$a;

    move-result-object v7

    invoke-virtual {v7, v5}, Lf/h/a/f/i/j/c1$a;->r(Ljava/lang/String;)Lf/h/a/f/i/j/c1$a;

    invoke-virtual {v2, v5}, Lcom/google/android/gms/measurement/internal/zzap;->M0(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v7, v5}, Lf/h/a/f/j/b/q9;->F(Lf/h/a/f/i/j/c1$a;Ljava/lang/Object;)V

    invoke-virtual {v6, v7}, Lf/h/a/f/i/j/a1$a;->r(Lf/h/a/f/i/j/c1$a;)Lf/h/a/f/i/j/a1$a;

    goto :goto_2

    :cond_5
    invoke-virtual {v6}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object v2

    check-cast v2, Lf/h/a/f/i/j/u4;

    check-cast v2, Lf/h/a/f/i/j/a1;

    invoke-virtual {v2}, Lf/h/a/f/i/j/l3;->d()[B

    move-result-object v2

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v4

    invoke-virtual {v4, v1}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    array-length v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "Saving default event parameters, appId, data size"

    invoke-virtual {v3, v6, v4, v5}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "app_id"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "parameters"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :try_start_0
    invoke-virtual {v0}, Lf/h/a/f/j/b/g;->t()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v4, "default_event_params"

    const/4 v5, 0x5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_6

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v3, "Failed to insert default event parameters (got -1). appId"

    invoke-static {v1}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v2

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-static {v1}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v3, "Error storing default event parameters. appId"

    invoke-virtual {v0, v3, v1, v2}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_6
    :goto_3
    return-void
.end method
