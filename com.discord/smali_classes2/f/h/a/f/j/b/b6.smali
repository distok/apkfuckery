.class public final synthetic Lf/h/a/f/j/b/b6;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/j/b/c6;

.field public final e:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/c6;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/b6;->d:Lf/h/a/f/j/b/c6;

    iput-object p2, p0, Lf/h/a/f/j/b/b6;->e:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    iget-object v0, p0, Lf/h/a/f/j/b/b6;->d:Lf/h/a/f/j/b/c6;

    iget-object v1, p0, Lf/h/a/f/j/b/b6;->e:Landroid/os/Bundle;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/i/j/ea;->b()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v3, Lf/h/a/f/j/b/p;->z0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_3

    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->C:Lf/h/a/f/j/b/i4;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/i4;->b(Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_1
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->C:Lf/h/a/f/j/b/i4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/i4;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_4

    instance-of v6, v5, Ljava/lang/String;

    if-nez v6, :cond_4

    instance-of v6, v5, Ljava/lang/Long;

    if-nez v6, :cond_4

    instance-of v6, v5, Ljava/lang/Double;

    if-nez v6, :cond_4

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    invoke-static {v5}, Lf/h/a/f/j/b/t9;->W(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v7

    iget-object v8, v0, Lf/h/a/f/j/b/c6;->p:Lf/h/a/f/j/b/v9;

    const/16 v9, 0x1b

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Lf/h/a/f/j/b/t9;->R(Lf/h/a/f/j/b/v9;ILjava/lang/String;Ljava/lang/String;I)V

    :cond_3
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string v7, "Invalid default event parameter type. Name, value"

    invoke-virtual {v6, v7, v4, v5}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    invoke-static {v4}, Lf/h/a/f/j/b/t9;->r0(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v5

    iget-object v5, v5, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string v6, "Invalid default event parameter name. Name"

    invoke-virtual {v5, v6, v4}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    if-nez v5, :cond_6

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v6

    const/16 v7, 0x64

    const-string v8, "param"

    invoke-virtual {v6, v8, v4, v7, v5}, Lf/h/a/f/j/b/t9;->b0(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v6

    invoke-virtual {v6, v2, v4, v5}, Lf/h/a/f/j/b/t9;->I(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_7
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    iget-object v1, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v1}, Lf/h/a/f/j/b/c;->t()I

    move-result v1

    invoke-virtual {v2}, Landroid/os/Bundle;->size()I

    move-result v3

    const/4 v4, 0x0

    if-gt v3, v1, :cond_8

    const/4 v7, 0x0

    goto :goto_2

    :cond_8
    new-instance v3, Ljava/util/TreeSet;

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    :cond_9
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x1

    if-eqz v6, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    add-int/2addr v5, v7

    if-le v5, v1, :cond_9

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    :goto_2
    if-eqz v7, :cond_b

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v8

    iget-object v9, v0, Lf/h/a/f/j/b/c6;->p:Lf/h/a/f/j/b/v9;

    const/16 v10, 0x1a

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Lf/h/a/f/j/b/t9;->R(Lf/h/a/f/j/b/v9;ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string v3, "Too many default event parameters set. Discarding beyond event parameter limit"

    invoke-virtual {v1, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->C:Lf/h/a/f/j/b/i4;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/i4;->b(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->p()Lf/h/a/f/j/b/q7;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/q7;->I(Z)Lcom/google/android/gms/measurement/internal/zzn;

    move-result-object v1

    new-instance v3, Lf/h/a/f/j/b/a8;

    invoke-direct {v3, v0, v2, v1}, Lf/h/a/f/j/b/a8;-><init>(Lf/h/a/f/j/b/q7;Landroid/os/Bundle;Lcom/google/android/gms/measurement/internal/zzn;)V

    invoke-virtual {v0, v3}, Lf/h/a/f/j/b/q7;->z(Ljava/lang/Runnable;)V

    :cond_c
    :goto_3
    return-void
.end method
