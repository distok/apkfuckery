.class public final Lf/h/a/f/j/b/j3;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final g:Ljava/lang/Object;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/h/a/f/j/b/h3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/j/b/h3<",
            "TV;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/Object;

.field public volatile f:Ljava/lang/Object;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "cachingLock"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf/h/a/f/j/b/j3;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Lf/h/a/f/j/b/h3;Lf/h/a/f/j/b/f3;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p5, Ljava/lang/Object;

    invoke-direct {p5}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lf/h/a/f/j/b/j3;->e:Ljava/lang/Object;

    const/4 p5, 0x0

    iput-object p5, p0, Lf/h/a/f/j/b/j3;->f:Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/f/j/b/j3;->a:Ljava/lang/String;

    iput-object p2, p0, Lf/h/a/f/j/b/j3;->c:Ljava/lang/Object;

    iput-object p3, p0, Lf/h/a/f/j/b/j3;->d:Ljava/lang/Object;

    iput-object p4, p0, Lf/h/a/f/j/b/j3;->b:Lf/h/a/f/j/b/h3;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/j/b/j3;->g:Ljava/lang/Object;

    iget-object v1, p0, Lf/h/a/f/j/b/j3;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz p1, :cond_0

    return-object p1

    :cond_0
    sget-object p1, Lf/h/a/f/f/n/g;->d:Lf/h/a/f/j/b/ga;

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/f/j/b/j3;->c:Ljava/lang/Object;

    return-object p1

    :cond_1
    monitor-enter v0

    :try_start_1
    invoke-static {}, Lf/h/a/f/j/b/ga;->a()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lf/h/a/f/j/b/j3;->f:Ljava/lang/Object;

    if-nez p1, :cond_2

    iget-object p1, p0, Lf/h/a/f/j/b/j3;->c:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lf/h/a/f/j/b/j3;->f:Ljava/lang/Object;

    :goto_0
    monitor-exit v0

    return-object p1

    :cond_3
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    sget-object p1, Lf/h/a/f/j/b/p;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/j/b/j3;

    invoke-static {}, Lf/h/a/f/j/b/ga;->a()Z

    move-result v2
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v2, :cond_5

    const/4 v2, 0x0

    :try_start_3
    iget-object v3, v1, Lf/h/a/f/j/b/j3;->b:Lf/h/a/f/j/b/h3;

    if-eqz v3, :cond_4

    invoke-interface {v3}, Lf/h/a/f/j/b/h3;->a()Ljava/lang/Object;

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_0
    :cond_4
    :try_start_4
    monitor-enter v0
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    iput-object v2, v1, Lf/h/a/f/j/b/j3;->f:Ljava/lang/Object;

    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw p1

    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Refreshing flag cache must be done on a worker thread."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_6
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    nop

    :cond_6
    iget-object p1, p0, Lf/h/a/f/j/b/j3;->b:Lf/h/a/f/j/b/h3;

    if-nez p1, :cond_7

    iget-object p1, p0, Lf/h/a/f/j/b/j3;->c:Ljava/lang/Object;

    return-object p1

    :cond_7
    :try_start_7
    invoke-interface {p1}, Lf/h/a/f/j/b/h3;->a()Ljava/lang/Object;

    move-result-object p1
    :try_end_7
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_2

    return-object p1

    :catch_2
    iget-object p1, p0, Lf/h/a/f/j/b/j3;->c:Ljava/lang/Object;

    return-object p1

    :catch_3
    iget-object p1, p0, Lf/h/a/f/j/b/j3;->c:Ljava/lang/Object;

    return-object p1

    :catchall_1
    move-exception p1

    :try_start_8
    monitor-exit v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw p1

    :catchall_2
    move-exception p1

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw p1
.end method
