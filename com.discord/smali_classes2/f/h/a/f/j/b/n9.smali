.class public final Lf/h/a/f/j/b/n9;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/j/b/v9;


# instance fields
.field public final synthetic a:Lf/h/a/f/j/b/k9;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/k9;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/n9;->a:Lf/h/a/f/j/b/k9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/h/a/f/j/b/n9;->a:Lf/h/a/f/j/b/k9;

    iget-object p1, p1, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string p2, "AppId not known when logging error event"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/n9;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/p9;

    invoke-direct {v1, p0, p1, p2}, Lf/h/a/f/j/b/p9;-><init>(Lf/h/a/f/j/b/n9;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method
