.class public final Lf/h/a/f/j/b/ba;
.super Lf/h/a/f/j/b/i9;
.source "com.google.android.gms:play-services-measurement@@18.0.0"


# instance fields
.field public d:Ljava/lang/String;

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lf/h/a/f/j/b/da;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/k9;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/j/b/i9;-><init>(Lf/h/a/f/j/b/k9;)V

    return-void
.end method


# virtual methods
.method public final p()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final s(I)Lf/h/a/f/j/b/da;
    .locals 3

    iget-object v0, p0, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/j/b/da;

    return-object p1

    :cond_0
    new-instance v0, Lf/h/a/f/j/b/da;

    iget-object v1, p0, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lf/h/a/f/j/b/da;-><init>(Lf/h/a/f/j/b/ba;Ljava/lang/String;Lf/h/a/f/j/b/aa;)V

    iget-object v1, p0, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final t(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 62
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/a1;",
            ">;",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/i1;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/y0;",
            ">;"
        }
    .end annotation

    move-object/from16 v10, p0

    const-string v11, "current_results"

    invoke-static/range {p1 .. p1}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    const-string v12, "null reference"

    move-object/from16 v13, p2

    invoke-static {v13, v12}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object/from16 v14, p3

    invoke-static {v14, v12}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object/from16 v0, p1

    iput-object v0, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    iput-object v0, v10, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    move-object/from16 v0, p4

    iput-object v0, v10, Lf/h/a/f/j/b/ba;->g:Ljava/lang/Long;

    move-object/from16 v0, p5

    iput-object v0, v10, Lf/h/a/f/j/b/ba;->h:Ljava/lang/Long;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v15, 0x1

    const/4 v9, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/j/a1;

    invoke-virtual {v1}, Lf/h/a/f/i/j/a1;->F()Ljava/lang/String;

    move-result-object v1

    const-string v2, "_s"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-static {}, Lf/h/a/f/i/j/f9;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v10, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v2, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    sget-object v3, Lf/h/a/f/j/b/p;->c0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2, v3}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v16, 0x1

    goto :goto_1

    :cond_2
    const/16 v16, 0x0

    :goto_1
    invoke-static {}, Lf/h/a/f/i/j/f9;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v10, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v2, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    sget-object v3, Lf/h/a/f/j/b/p;->b0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2, v3}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v17, 0x1

    goto :goto_2

    :cond_3
    const/16 v17, 0x0

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v2

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lf/h/a/f/j/b/i9;->n()V

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->b()V

    invoke-static {v3}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "current_session_count"

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {v2}, Lf/h/a/f/j/b/g;->t()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "events"

    const-string v6, "app_id = ?"

    new-array v7, v15, [Ljava/lang/String;

    aput-object v3, v7, v9

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "Error resetting session-scoped event counts. appId"

    invoke-virtual {v2, v4, v3, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_4
    :goto_3
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    const-string v8, "audience_id"

    if-eqz v17, :cond_a

    if-eqz v16, :cond_a

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v2

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    new-instance v4, Landroidx/collection/ArrayMap;

    invoke-direct {v4}, Landroidx/collection/ArrayMap;-><init>()V

    invoke-virtual {v2}, Lf/h/a/f/j/b/g;->t()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    :try_start_1
    const-string v19, "event_filters"

    const-string v0, "data"

    filled-new-array {v8, v0}, [Ljava/lang/String;

    move-result-object v20

    const-string v21, "app_id=?"

    new-array v0, v15, [Ljava/lang/String;

    aput-object v3, v0, v9

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v22, v0

    invoke-virtual/range {v18 .. v25}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto/16 :goto_8

    :cond_5
    :goto_4
    :try_start_3
    invoke-interface {v5, v15}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {}, Lf/h/a/f/i/j/l0;->I()Lf/h/a/f/i/j/l0$a;

    move-result-object v6

    invoke-static {v6, v0}, Lf/h/a/f/j/b/q9;->x(Lf/h/a/f/i/j/b6;[B)Lf/h/a/f/i/j/b6;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/l0$a;

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/l0;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-virtual {v0}, Lf/h/a/f/i/j/l0;->C()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    if-nez v7, :cond_6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :catch_1
    move-exception v0

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v7, "Failed to merge filter. appId"

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v6, v7, v9, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_7
    :goto_5
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-nez v0, :cond_8

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    move-object v9, v4

    goto :goto_9

    :cond_8
    const/4 v9, 0x0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_6

    :catchall_0
    move-exception v0

    const/4 v7, 0x0

    goto :goto_7

    :catch_3
    move-exception v0

    const/4 v5, 0x0

    :goto_6
    :try_start_6
    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v4, "Database error querying filters. appId"

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v4, v3, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v5, :cond_a

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_8

    :catchall_1
    move-exception v0

    move-object v7, v5

    :goto_7
    if-eqz v7, :cond_9

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    :cond_a
    :goto_8
    move-object v9, v0

    :goto_9
    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v2

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lf/h/a/f/j/b/i9;->n()V

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->b()V

    invoke-static {v3}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v2}, Lf/h/a/f/j/b/g;->t()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    :try_start_7
    const-string v19, "audience_filter_values"

    filled-new-array {v8, v11}, [Ljava/lang/String;

    move-result-object v20

    const-string v21, "app_id=?"

    new-array v0, v15, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v0, v4

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v22, v0

    invoke-virtual/range {v18 .. v25}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {}, Lf/h/a/f/i/j/l9;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, v2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v5, Lf/h/a/f/j/b/p;->F0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v3, v5}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_d

    :cond_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_e

    :cond_c
    :try_start_9
    new-instance v5, Landroidx/collection/ArrayMap;

    invoke-direct {v5}, Landroidx/collection/ArrayMap;-><init>()V

    const/4 v6, 0x0

    :goto_a
    invoke-interface {v4, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-interface {v4, v15}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    :try_start_a
    invoke-static {}, Lf/h/a/f/i/j/g1;->M()Lf/h/a/f/i/j/g1$a;

    move-result-object v6

    invoke-static {v6, v0}, Lf/h/a/f/j/b/q9;->x(Lf/h/a/f/i/j/b6;[B)Lf/h/a/f/i/j/b6;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/g1$a;

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/g1;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    :try_start_b
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v18, v5

    goto :goto_b

    :catch_4
    move-exception v0

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v15, "Failed to merge filter results. appId, audienceId, error"

    move-object/from16 v18, v5

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v15, v5, v7, v0}, Lf/h/a/f/j/b/s3;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_b
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    if-nez v0, :cond_d

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    move-object/from16 v15, v18

    goto :goto_f

    :cond_d
    move-object/from16 v5, v18

    const/4 v6, 0x0

    const/4 v15, 0x1

    goto :goto_a

    :catch_5
    move-exception v0

    goto :goto_c

    :catchall_2
    move-exception v0

    const/4 v7, 0x0

    goto/16 :goto_3b

    :catch_6
    move-exception v0

    const/4 v4, 0x0

    :goto_c
    :try_start_c
    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v5

    iget-object v5, v5, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v6, "Database error querying filter results. appId"

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {}, Lf/h/a/f/i/j/l9;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, v2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->F0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v3, v2}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    if-eqz v4, :cond_e

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_e
    :goto_d
    move-object v15, v0

    goto :goto_f

    :cond_f
    if-eqz v4, :cond_10

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_10
    :goto_e
    const/4 v15, 0x0

    :goto_f
    invoke-static {}, Lf/h/a/f/i/j/l9;->b()Z

    move-result v0

    const/4 v7, 0x2

    if-eqz v0, :cond_11

    iget-object v0, v10, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v2, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    sget-object v3, Lf/h/a/f/j/b/p;->F0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2, v3}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-nez v0, :cond_12

    :cond_11
    if-eqz v15, :cond_3b

    :cond_12
    invoke-interface {v15}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    goto/16 :goto_28

    :cond_13
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {v15}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    if-eqz v1, :cond_2a

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-static {v1}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    new-instance v3, Landroidx/collection/ArrayMap;

    invoke-direct {v3}, Landroidx/collection/ArrayMap;-><init>()V

    invoke-interface {v15}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_28

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/j/b/i9;->n()V

    invoke-virtual {v4}, Lf/h/a/f/j/b/s5;->b()V

    invoke-static {v1}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    invoke-virtual {v4}, Lf/h/a/f/j/b/g;->t()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    :try_start_d
    const-string v6, "select audience_id, filter_id from event_filters where app_id = ? and session_scoped = 1 UNION select audience_id, filter_id from property_filters where app_id = ? and session_scoped = 1;"
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_a
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    move-object/from16 v18, v8

    :try_start_e
    new-array v8, v7, [Ljava/lang/String;

    const/16 v19, 0x0

    aput-object v1, v8, v19

    const/16 v19, 0x1

    aput-object v1, v8, v19

    invoke-virtual {v5, v6, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_e .. :try_end_e} :catch_9
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :try_start_f
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-nez v6, :cond_14

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_8
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto/16 :goto_14

    :cond_14
    :goto_10
    const/4 v6, 0x0

    :try_start_10
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    if-nez v6, :cond_15

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v0, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_10
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10 .. :try_end_10} :catch_8
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :cond_15
    const/4 v8, 0x1

    :try_start_11
    invoke-interface {v5, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_7
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    if-nez v6, :cond_16

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_14

    :cond_16
    const/4 v7, 0x2

    goto :goto_10

    :catch_7
    move-exception v0

    goto :goto_13

    :catch_8
    move-exception v0

    const/4 v8, 0x1

    goto :goto_13

    :catch_9
    move-exception v0

    :goto_11
    const/4 v8, 0x1

    goto :goto_12

    :catchall_3
    move-exception v0

    const/4 v7, 0x0

    goto/16 :goto_1b

    :catch_a
    move-exception v0

    move-object/from16 v18, v8

    goto :goto_11

    :goto_12
    const/4 v5, 0x0

    :goto_13
    :try_start_12
    invoke-virtual {v4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v7, "Database error querying scoped filters. appId"

    invoke-static {v1}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {}, Lf/h/a/f/i/j/l9;->b()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, v4, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v4, Lf/h/a/f/j/b/p;->F0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1, v4}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    if-eqz v5, :cond_19

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_14

    :cond_17
    if-eqz v5, :cond_18

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_18
    const/4 v0, 0x0

    :cond_19
    :goto_14
    invoke-interface {v15}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_29

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v15, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/f/i/j/g1;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    if-eqz v6, :cond_26

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1a

    goto/16 :goto_19

    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->m()Lf/h/a/f/j/b/q9;

    move-result-object v7

    invoke-virtual {v5}, Lf/h/a/f/i/j/g1;->E()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lf/h/a/f/j/b/q9;->C(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_25

    invoke-virtual {v5}, Lf/h/a/f/i/j/u4;->t()Lf/h/a/f/i/j/u4$b;

    move-result-object v8

    check-cast v8, Lf/h/a/f/i/j/g1$a;

    move-object/from16 v20, v0

    iget-boolean v0, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_1b

    invoke-virtual {v8}, Lf/h/a/f/i/j/u4$b;->l()V

    move-object v0, v1

    const/4 v1, 0x0

    iput-boolean v1, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    goto :goto_16

    :cond_1b
    move-object v0, v1

    :goto_16
    iget-object v1, v8, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v1, Lf/h/a/f/i/j/g1;

    invoke-static {v1}, Lf/h/a/f/i/j/g1;->B(Lf/h/a/f/i/j/g1;)V

    iget-boolean v1, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v1, :cond_1c

    invoke-virtual {v8}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v1, 0x0

    iput-boolean v1, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_1c
    iget-object v1, v8, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v1, Lf/h/a/f/i/j/g1;

    invoke-static {v1, v7}, Lf/h/a/f/i/j/g1;->D(Lf/h/a/f/i/j/g1;Ljava/lang/Iterable;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->m()Lf/h/a/f/j/b/q9;

    move-result-object v1

    invoke-virtual {v5}, Lf/h/a/f/i/j/g1;->v()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v1, v7, v6}, Lf/h/a/f/j/b/q9;->C(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v7, :cond_1d

    invoke-virtual {v8}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v7, 0x0

    iput-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_1d
    iget-object v7, v8, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v7, Lf/h/a/f/i/j/g1;

    invoke-static {v7}, Lf/h/a/f/i/j/g1;->w(Lf/h/a/f/i/j/g1;)V

    iget-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v7, :cond_1e

    invoke-virtual {v8}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v7, 0x0

    iput-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_1e
    iget-object v7, v8, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v7, Lf/h/a/f/i/j/g1;

    invoke-static {v7, v1}, Lf/h/a/f/i/j/g1;->y(Lf/h/a/f/i/j/g1;Ljava/lang/Iterable;)V

    const/4 v1, 0x0

    :goto_17
    invoke-virtual {v5}, Lf/h/a/f/i/j/g1;->J()I

    move-result v7

    if-ge v1, v7, :cond_21

    invoke-virtual {v5, v1}, Lf/h/a/f/i/j/g1;->u(I)Lf/h/a/f/i/j/z0;

    move-result-object v7

    invoke-virtual {v7}, Lf/h/a/f/i/j/z0;->x()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_20

    iget-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v7, :cond_1f

    invoke-virtual {v8}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v7, 0x0

    iput-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_1f
    iget-object v7, v8, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v7, Lf/h/a/f/i/j/g1;

    invoke-static {v7, v1}, Lf/h/a/f/i/j/g1;->x(Lf/h/a/f/i/j/g1;I)V

    :cond_20
    add-int/lit8 v1, v1, 0x1

    goto :goto_17

    :cond_21
    const/4 v1, 0x0

    :goto_18
    invoke-virtual {v5}, Lf/h/a/f/i/j/g1;->L()I

    move-result v7

    if-ge v1, v7, :cond_24

    invoke-virtual {v5, v1}, Lf/h/a/f/i/j/g1;->A(I)Lf/h/a/f/i/j/h1;

    move-result-object v7

    invoke-virtual {v7}, Lf/h/a/f/i/j/h1;->y()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_23

    iget-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v7, :cond_22

    invoke-virtual {v8}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v7, 0x0

    iput-boolean v7, v8, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_22
    iget-object v7, v8, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v7, Lf/h/a/f/i/j/g1;

    invoke-static {v7, v1}, Lf/h/a/f/i/j/g1;->C(Lf/h/a/f/i/j/g1;I)V

    :cond_23
    add-int/lit8 v1, v1, 0x1

    goto :goto_18

    :cond_24
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object v4

    check-cast v4, Lf/h/a/f/i/j/u4;

    check-cast v4, Lf/h/a/f/i/j/g1;

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1a

    :cond_25
    move-object/from16 v20, v0

    goto/16 :goto_15

    :cond_26
    :goto_19
    move-object/from16 v20, v0

    move-object v0, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1a
    move-object v1, v0

    move-object/from16 v0, v20

    goto/16 :goto_15

    :catchall_4
    move-exception v0

    move-object v7, v5

    :goto_1b
    if-eqz v7, :cond_27

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_27
    throw v0

    :cond_28
    move-object/from16 v18, v8

    :cond_29
    move-object v0, v3

    goto :goto_1c

    :cond_2a
    move-object/from16 v18, v8

    move-object v0, v15

    :goto_1c
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_1d
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3a

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/j/g1;

    new-instance v5, Ljava/util/BitSet;

    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    new-instance v6, Ljava/util/BitSet;

    invoke-direct {v6}, Ljava/util/BitSet;-><init>()V

    new-instance v7, Landroidx/collection/ArrayMap;

    invoke-direct {v7}, Landroidx/collection/ArrayMap;-><init>()V

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Lf/h/a/f/i/j/g1;->J()I

    move-result v2

    if-nez v2, :cond_2b

    goto :goto_20

    :cond_2b
    invoke-virtual {v1}, Lf/h/a/f/i/j/g1;->I()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2c
    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/f/i/j/z0;

    invoke-virtual {v3}, Lf/h/a/f/i/j/z0;->w()Z

    move-result v4

    if-eqz v4, :cond_2c

    invoke-virtual {v3}, Lf/h/a/f/i/j/z0;->x()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3}, Lf/h/a/f/i/j/z0;->y()Z

    move-result v8

    if-eqz v8, :cond_2d

    invoke-virtual {v3}, Lf/h/a/f/i/j/z0;->z()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_1f

    :cond_2d
    const/4 v3, 0x0

    :goto_1f
    invoke-interface {v7, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1e

    :cond_2e
    :goto_20
    new-instance v8, Landroidx/collection/ArrayMap;

    invoke-direct {v8}, Landroidx/collection/ArrayMap;-><init>()V

    if-eqz v1, :cond_31

    invoke-virtual {v1}, Lf/h/a/f/i/j/g1;->L()I

    move-result v2

    if-nez v2, :cond_2f

    goto :goto_23

    :cond_2f
    invoke-virtual {v1}, Lf/h/a/f/i/j/g1;->K()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_31

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/f/i/j/h1;

    invoke-virtual {v3}, Lf/h/a/f/i/j/h1;->x()Z

    move-result v4

    if-eqz v4, :cond_30

    invoke-virtual {v3}, Lf/h/a/f/i/j/h1;->A()I

    move-result v4

    if-lez v4, :cond_30

    invoke-virtual {v3}, Lf/h/a/f/i/j/h1;->y()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3}, Lf/h/a/f/i/j/h1;->A()I

    move-result v22

    move-object/from16 v23, v0

    add-int/lit8 v0, v22, -0x1

    invoke-virtual {v3, v0}, Lf/h/a/f/i/j/h1;->u(I)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_22

    :cond_30
    move-object/from16 v23, v0

    :goto_22
    move-object/from16 v0, v23

    goto :goto_21

    :cond_31
    :goto_23
    move-object/from16 v23, v0

    if-eqz v1, :cond_34

    const/4 v4, 0x0

    :goto_24
    invoke-virtual {v1}, Lf/h/a/f/i/j/g1;->z()I

    move-result v0

    shl-int/lit8 v0, v0, 0x6

    if-ge v4, v0, :cond_34

    invoke-virtual {v1}, Lf/h/a/f/i/j/g1;->v()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v4}, Lf/h/a/f/j/b/q9;->Q(Ljava/util/List;I)Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v13, "Filter already evaluated. audience ID, filter ID"

    invoke-virtual {v0, v13, v2, v3}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v6, v4}, Ljava/util/BitSet;->set(I)V

    invoke-virtual {v1}, Lf/h/a/f/i/j/g1;->E()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v4}, Lf/h/a/f/j/b/q9;->Q(Ljava/util/List;I)Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-virtual {v5, v4}, Ljava/util/BitSet;->set(I)V

    const/4 v0, 0x1

    goto :goto_25

    :cond_32
    const/4 v0, 0x0

    :goto_25
    if-nez v0, :cond_33

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_33
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v13, p2

    goto :goto_24

    :cond_34
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lf/h/a/f/i/j/g1;

    if-eqz v17, :cond_39

    if-eqz v16, :cond_39

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_39

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->h:Ljava/lang/Long;

    if-eqz v1, :cond_39

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->g:Ljava/lang/Long;

    if-nez v1, :cond_35

    goto :goto_27

    :cond_35
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_36
    :goto_26
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_39

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/j/l0;

    invoke-virtual {v1}, Lf/h/a/f/i/j/l0;->y()I

    move-result v2

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->h:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    const-wide/16 v26, 0x3e8

    div-long v24, v24, v26

    invoke-virtual {v1}, Lf/h/a/f/i/j/l0;->F()Z

    move-result v1

    if-eqz v1, :cond_37

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    div-long v24, v24, v26

    :cond_37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v7, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_38
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v8, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_26

    :cond_39
    :goto_27
    new-instance v0, Lf/h/a/f/j/b/da;

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    const/4 v13, 0x0

    move-object v1, v0

    move-object/from16 v2, p0

    const/16 v22, 0x0

    move-object/from16 v14, v18

    const/16 v18, 0x1

    move-object/from16 v22, v9

    const/16 v19, 0x0

    move-object v9, v13

    invoke-direct/range {v1 .. v9}, Lf/h/a/f/j/b/da;-><init>(Lf/h/a/f/j/b/ba;Ljava/lang/String;Lf/h/a/f/i/j/g1;Ljava/util/BitSet;Ljava/util/BitSet;Ljava/util/Map;Ljava/util/Map;Lf/h/a/f/j/b/aa;)V

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v13, p2

    move-object/from16 v18, v14

    move-object/from16 v9, v22

    move-object/from16 v0, v23

    move-object/from16 v14, p3

    goto/16 :goto_1d

    :cond_3a
    move-object/from16 v14, v18

    goto :goto_29

    :cond_3b
    :goto_28
    move-object v14, v8

    :goto_29
    const/16 v18, 0x1

    const/16 v19, 0x0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const-string v1, "Skipping failed audience ID"

    if-nez v0, :cond_46

    new-instance v0, Lf/h/a/f/j/b/ca;

    const/4 v2, 0x0

    invoke-direct {v0, v10, v2}, Lf/h/a/f/j/b/ca;-><init>(Lf/h/a/f/j/b/ba;Lf/h/a/f/j/b/aa;)V

    new-instance v3, Landroidx/collection/ArrayMap;

    invoke-direct {v3}, Landroidx/collection/ArrayMap;-><init>()V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3c
    :goto_2a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_46

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/f/i/j/a1;

    iget-object v6, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-virtual {v0, v6, v5}, Lf/h/a/f/j/b/ca;->a(Ljava/lang/String;Lf/h/a/f/i/j/a1;)Lf/h/a/f/i/j/a1;

    move-result-object v6

    if-eqz v6, :cond_3c

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v7

    iget-object v8, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-virtual {v6}, Lf/h/a/f/i/j/a1;->F()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5}, Lf/h/a/f/i/j/a1;->F()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v8, v13}, Lf/h/a/f/j/b/g;->z(Ljava/lang/String;Ljava/lang/String;)Lf/h/a/f/j/b/l;

    move-result-object v13

    if-nez v13, :cond_3d

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v13

    iget-object v13, v13, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    invoke-static {v8}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v7, v9}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v9, "Event aggregate wasn\'t created during raw event logging. appId, event"

    invoke-virtual {v13, v9, v15, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v7, Lf/h/a/f/j/b/l;

    move-object/from16 v28, v7

    invoke-virtual {v5}, Lf/h/a/f/i/j/a1;->F()Ljava/lang/String;

    move-result-object v30

    const-wide/16 v31, 0x1

    const-wide/16 v33, 0x1

    const-wide/16 v35, 0x1

    invoke-virtual {v5}, Lf/h/a/f/i/j/a1;->H()J

    move-result-wide v37

    const-wide/16 v39, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    move-object/from16 v29, v8

    invoke-direct/range {v28 .. v44}, Lf/h/a/f/j/b/l;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJJLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V

    goto :goto_2b

    :cond_3d
    new-instance v7, Lf/h/a/f/j/b/l;

    move-object/from16 v45, v7

    iget-object v5, v13, Lf/h/a/f/j/b/l;->a:Ljava/lang/String;

    move-object/from16 v46, v5

    iget-object v5, v13, Lf/h/a/f/j/b/l;->b:Ljava/lang/String;

    move-object/from16 v47, v5

    iget-wide v8, v13, Lf/h/a/f/j/b/l;->c:J

    const-wide/16 v15, 0x1

    add-long v48, v8, v15

    iget-wide v8, v13, Lf/h/a/f/j/b/l;->d:J

    add-long v50, v8, v15

    iget-wide v8, v13, Lf/h/a/f/j/b/l;->e:J

    add-long v52, v8, v15

    iget-wide v8, v13, Lf/h/a/f/j/b/l;->f:J

    move-wide/from16 v54, v8

    iget-wide v8, v13, Lf/h/a/f/j/b/l;->g:J

    move-wide/from16 v56, v8

    iget-object v5, v13, Lf/h/a/f/j/b/l;->h:Ljava/lang/Long;

    move-object/from16 v58, v5

    iget-object v5, v13, Lf/h/a/f/j/b/l;->i:Ljava/lang/Long;

    move-object/from16 v59, v5

    iget-object v5, v13, Lf/h/a/f/j/b/l;->j:Ljava/lang/Long;

    move-object/from16 v60, v5

    iget-object v5, v13, Lf/h/a/f/j/b/l;->k:Ljava/lang/Boolean;

    move-object/from16 v61, v5

    invoke-direct/range {v45 .. v61}, Lf/h/a/f/j/b/l;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJJLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V

    :goto_2b
    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v5

    invoke-virtual {v5, v7}, Lf/h/a/f/j/b/g;->H(Lf/h/a/f/j/b/l;)V

    iget-wide v8, v7, Lf/h/a/f/j/b/l;->c:J

    invoke-virtual {v6}, Lf/h/a/f/i/j/a1;->F()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map;

    if-nez v13, :cond_40

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v13

    iget-object v15, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-virtual {v13, v15, v5}, Lf/h/a/f/j/b/g;->c0(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v13

    invoke-static {}, Lf/h/a/f/i/j/l9;->b()Z

    move-result v15

    if-eqz v15, :cond_3e

    iget-object v15, v10, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v15, v15, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v2, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    move-object/from16 p1, v0

    sget-object v0, Lf/h/a/f/j/b/p;->F0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v15, v2, v0}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-nez v0, :cond_3f

    goto :goto_2c

    :cond_3e
    move-object/from16 p1, v0

    :goto_2c
    if-nez v13, :cond_3f

    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    move-object v13, v0

    :cond_3f
    invoke-interface {v3, v5, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2d

    :cond_40
    move-object/from16 p1, v0

    :goto_2d
    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_45

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v5, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v5, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_41

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v5

    iget-object v5, v5, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2e

    :cond_41
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v13, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v15, 0x1

    :goto_2f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_43

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lf/h/a/f/i/j/l0;

    move-object/from16 p2, v0

    new-instance v0, Lf/h/a/f/j/b/fa;

    move-object/from16 p4, v3

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-direct {v0, v10, v3, v2, v15}, Lf/h/a/f/j/b/fa;-><init>(Lf/h/a/f/j/b/ba;Ljava/lang/String;ILf/h/a/f/i/j/l0;)V

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->g:Ljava/lang/Long;

    move-object/from16 p5, v4

    iget-object v4, v10, Lf/h/a/f/j/b/ba;->h:Ljava/lang/Long;

    invoke-virtual {v15}, Lf/h/a/f/i/j/l0;->y()I

    move-result v15

    invoke-virtual {v10, v2, v15}, Lf/h/a/f/j/b/ba;->u(II)Z

    move-result v27

    move-object/from16 v20, v0

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v23, v6

    move-wide/from16 v24, v8

    move-object/from16 v26, v7

    invoke-virtual/range {v20 .. v27}, Lf/h/a/f/j/b/fa;->i(Ljava/lang/Long;Ljava/lang/Long;Lf/h/a/f/i/j/a1;JLf/h/a/f/j/b/l;Z)Z

    move-result v15

    if-eqz v15, :cond_42

    invoke-virtual {v10, v2}, Lf/h/a/f/j/b/ba;->s(I)Lf/h/a/f/j/b/da;

    move-result-object v3

    invoke-virtual {v3, v0}, Lf/h/a/f/j/b/da;->b(Lf/h/a/f/j/b/ea;)V

    move-object/from16 v0, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    goto :goto_2f

    :cond_42
    iget-object v0, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_30

    :cond_43
    move-object/from16 p2, v0

    move-object/from16 p4, v3

    move-object/from16 p5, v4

    :goto_30
    if-nez v15, :cond_44

    iget-object v0, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_44
    move-object/from16 v0, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    goto/16 :goto_2e

    :cond_45
    move-object/from16 v0, p1

    const/4 v2, 0x0

    goto/16 :goto_2a

    :cond_46
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_54

    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_47
    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_54

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/f/i/j/i1;

    invoke-virtual {v3}, Lf/h/a/f/i/j/i1;->D()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    if-nez v5, :cond_4a

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v5

    iget-object v6, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-virtual {v5, v6, v4}, Lf/h/a/f/j/b/g;->d0(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    invoke-static {}, Lf/h/a/f/i/j/l9;->b()Z

    move-result v6

    if-eqz v6, :cond_48

    iget-object v6, v10, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v6, v6, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v7, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    sget-object v8, Lf/h/a/f/j/b/p;->F0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v6, v7, v8}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v6

    if-nez v6, :cond_49

    :cond_48
    if-nez v5, :cond_49

    new-instance v5, Landroidx/collection/ArrayMap;

    invoke-direct {v5}, Landroidx/collection/ArrayMap;-><init>()V

    :cond_49
    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4a
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_32
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_47

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4b

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_31

    :cond_4b
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    const/4 v9, 0x1

    :goto_33
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_52

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/h/a/f/i/j/o0;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v9

    const/4 v13, 0x2

    invoke-virtual {v9, v13}, Lf/h/a/f/j/b/q3;->x(I)Z

    move-result v9

    if-eqz v9, :cond_4d

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v9

    iget-object v9, v9, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->v()Z

    move-result v16

    if-eqz v16, :cond_4c

    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->w()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 p1, v0

    move-object/from16 v13, v16

    goto :goto_34

    :cond_4c
    move-object/from16 p1, v0

    const/4 v13, 0x0

    :goto_34
    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v0

    move-object/from16 p4, v1

    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/o3;->y(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Evaluating filter. audience, filter, property"

    invoke-virtual {v9, v1, v15, v13, v0}, Lf/h/a/f/j/b/s3;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->m()Lf/h/a/f/j/b/q9;

    move-result-object v1

    invoke-virtual {v1, v8}, Lf/h/a/f/j/b/q9;->y(Lf/h/a/f/i/j/o0;)Ljava/lang/String;

    move-result-object v1

    const-string v9, "Filter definition"

    invoke-virtual {v0, v9, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_35

    :cond_4d
    move-object/from16 p1, v0

    move-object/from16 p4, v1

    :goto_35
    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->v()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->w()I

    move-result v0

    const/16 v1, 0x100

    if-le v0, v1, :cond_4e

    goto :goto_36

    :cond_4e
    new-instance v0, Lf/h/a/f/j/b/ha;

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-direct {v0, v10, v1, v6, v8}, Lf/h/a/f/j/b/ha;-><init>(Lf/h/a/f/j/b/ba;Ljava/lang/String;ILf/h/a/f/i/j/o0;)V

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->g:Ljava/lang/Long;

    iget-object v9, v10, Lf/h/a/f/j/b/ba;->h:Ljava/lang/Long;

    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->w()I

    move-result v8

    invoke-virtual {v10, v6, v8}, Lf/h/a/f/j/b/ba;->u(II)Z

    move-result v8

    invoke-virtual {v0, v1, v9, v3, v8}, Lf/h/a/f/j/b/ha;->i(Ljava/lang/Long;Ljava/lang/Long;Lf/h/a/f/i/j/i1;Z)Z

    move-result v9

    if-eqz v9, :cond_4f

    invoke-virtual {v10, v6}, Lf/h/a/f/j/b/ba;->s(I)Lf/h/a/f/j/b/da;

    move-result-object v1

    invoke-virtual {v1, v0}, Lf/h/a/f/j/b/da;->b(Lf/h/a/f/j/b/ea;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    goto/16 :goto_33

    :cond_4f
    iget-object v0, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_38

    :cond_50
    :goto_36
    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v1, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-static {v1}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->v()Z

    move-result v7

    if-eqz v7, :cond_51

    invoke-virtual {v8}, Lf/h/a/f/i/j/o0;->w()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_37

    :cond_51
    const/4 v7, 0x0

    :goto_37
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Invalid property filter ID. appId, id"

    invoke-virtual {v0, v8, v1, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v9, 0x0

    goto :goto_38

    :cond_52
    move-object/from16 p1, v0

    move-object/from16 p4, v1

    :goto_38
    if-nez v9, :cond_53

    iget-object v0, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_53
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    goto/16 :goto_32

    :cond_54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v10, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v2, v10, Lf/h/a/f/j/b/ba;->e:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_55
    :goto_39
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, v10, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/f/j/b/da;

    invoke-virtual {v3, v0}, Lf/h/a/f/j/b/da;->a(I)Lf/h/a/f/i/j/y0;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/i9;->q()Lf/h/a/f/j/b/g;

    move-result-object v4

    iget-object v5, v10, Lf/h/a/f/j/b/ba;->d:Ljava/lang/String;

    invoke-virtual {v3}, Lf/h/a/f/i/j/y0;->A()Lf/h/a/f/i/j/g1;

    move-result-object v3

    invoke-virtual {v4}, Lf/h/a/f/j/b/i9;->n()V

    invoke-virtual {v4}, Lf/h/a/f/j/b/s5;->b()V

    invoke-static {v5}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v3, v12}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v3}, Lf/h/a/f/i/j/l3;->d()[B

    move-result-object v3

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "app_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v14, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v6, v11, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :try_start_13
    invoke-virtual {v4}, Lf/h/a/f/j/b/g;->t()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "audience_filter_values"
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_c

    const/4 v7, 0x5

    const/4 v8, 0x0

    :try_start_14
    invoke-virtual {v0, v3, v8, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v6

    const-wide/16 v15, -0x1

    cmp-long v0, v6, v15

    if-nez v0, :cond_55

    invoke-virtual {v4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v3, "Failed to insert filter results (got -1). appId"

    invoke-static {v5}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_14
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_14 .. :try_end_14} :catch_b

    goto :goto_39

    :catch_b
    move-exception v0

    goto :goto_3a

    :catch_c
    move-exception v0

    const/4 v8, 0x0

    :goto_3a
    invoke-virtual {v4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-static {v5}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    const-string v5, "Error storing filter results. appId"

    invoke-virtual {v3, v5, v4, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_39

    :cond_56
    return-object v1

    :catchall_5
    move-exception v0

    move-object v7, v4

    :goto_3b
    if-eqz v7, :cond_57

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_57
    throw v0
.end method

.method public final u(II)Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/ba;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/j/b/da;

    iget-object p1, p1, Lf/h/a/f/j/b/da;->d:Ljava/util/BitSet;

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->get(I)Z

    move-result p1

    return p1
.end method
