.class public final Lf/h/a/f/j/b/k;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/f/j/b/t5;

.field public final synthetic e:Lf/h/a/f/j/b/i;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/i;Lf/h/a/f/j/b/t5;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/k;->e:Lf/h/a/f/j/b/i;

    iput-object p2, p0, Lf/h/a/f/j/b/k;->d:Lf/h/a/f/j/b/t5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lf/h/a/f/j/b/k;->d:Lf/h/a/f/j/b/t5;

    invoke-interface {v0}, Lf/h/a/f/j/b/t5;->l()Lf/h/a/f/j/b/ga;

    invoke-static {}, Lf/h/a/f/j/b/ga;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/k;->d:Lf/h/a/f/j/b/t5;

    invoke-interface {v0}, Lf/h/a/f/j/b/t5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    invoke-virtual {v0, p0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/k;->e:Lf/h/a/f/j/b/i;

    iget-wide v0, v0, Lf/h/a/f/j/b/i;->c:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/h/a/f/j/b/k;->e:Lf/h/a/f/j/b/i;

    iput-wide v2, v1, Lf/h/a/f/j/b/i;->c:J

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/f/j/b/k;->e:Lf/h/a/f/j/b/i;

    invoke-virtual {v0}, Lf/h/a/f/j/b/i;->a()V

    :cond_2
    return-void
.end method
