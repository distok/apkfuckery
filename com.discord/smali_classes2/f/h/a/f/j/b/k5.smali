.class public final Lf/h/a/f/j/b/k5;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/measurement/internal/zzn;

.field public final synthetic e:Lf/h/a/f/j/b/z4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/z4;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/k5;->e:Lf/h/a/f/j/b/z4;

    iput-object p2, p0, Lf/h/a/f/j/b/k5;->d:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    iget-object v0, p0, Lf/h/a/f/j/b/k5;->e:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->R()V

    iget-object v0, p0, Lf/h/a/f/j/b/k5;->e:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    iget-object v1, p0, Lf/h/a/f/j/b/k5;->d:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v3, Lf/h/a/f/j/b/p;->J0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->f()Lf/h/a/f/j/b/r4;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/r4;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->P()V

    iget-object v2, v1, Lcom/google/android/gms/measurement/internal/zzn;->d:Ljava/lang/String;

    invoke-static {v2}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, v1, Lcom/google/android/gms/measurement/internal/zzn;->z:Ljava/lang/String;

    invoke-static {v2}, Lf/h/a/f/j/b/d;->b(Ljava/lang/String;)Lf/h/a/f/j/b/d;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/zzn;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lf/h/a/f/j/b/k9;->a(Ljava/lang/String;)Lf/h/a/f/j/b/d;

    move-result-object v3

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->g()Lf/h/a/f/j/b/q3;

    move-result-object v4

    iget-object v4, v4, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    iget-object v5, v1, Lcom/google/android/gms/measurement/internal/zzn;->d:Ljava/lang/String;

    const-string v6, "Setting consent, package, consent"

    invoke-virtual {v4, v6, v5, v2}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v4, v1, Lcom/google/android/gms/measurement/internal/zzn;->d:Ljava/lang/String;

    invoke-virtual {v0, v4, v2}, Lf/h/a/f/j/b/k9;->s(Ljava/lang/String;Lf/h/a/f/j/b/d;)V

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/d;->f(Lf/h/a/f/j/b/d;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/k9;->p(Lcom/google/android/gms/measurement/internal/zzn;)V

    :cond_0
    return-void
.end method
