.class public final Lf/h/a/f/j/b/p5;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:J

.field public final synthetic h:Lf/h/a/f/j/b/z4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/z4;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/p5;->h:Lf/h/a/f/j/b/z4;

    iput-object p2, p0, Lf/h/a/f/j/b/p5;->d:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/f/j/b/p5;->e:Ljava/lang/String;

    iput-object p4, p0, Lf/h/a/f/j/b/p5;->f:Ljava/lang/String;

    iput-wide p5, p0, Lf/h/a/f/j/b/p5;->g:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/j/b/p5;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/p5;->h:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    iget-object v0, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->w()Lf/h/a/f/j/b/h7;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/p5;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/h7;->D(Ljava/lang/String;Lf/h/a/f/j/b/i7;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/p5;->h:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    iget-object v0, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->w()Lf/h/a/f/j/b/h7;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/p5;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    monitor-enter v0

    :try_start_0
    iget-object v2, v0, Lf/h/a/f/j/b/h7;->m:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    :cond_1
    iput-object v1, v0, Lf/h/a/f/j/b/h7;->m:Ljava/lang/String;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
