.class public final synthetic Lf/h/a/f/j/b/s8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/j/b/p8;

.field public final e:I

.field public final f:Lf/h/a/f/j/b/q3;

.field public final g:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/p8;ILf/h/a/f/j/b/q3;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/s8;->d:Lf/h/a/f/j/b/p8;

    iput p2, p0, Lf/h/a/f/j/b/s8;->e:I

    iput-object p3, p0, Lf/h/a/f/j/b/s8;->f:Lf/h/a/f/j/b/q3;

    iput-object p4, p0, Lf/h/a/f/j/b/s8;->g:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lf/h/a/f/j/b/s8;->d:Lf/h/a/f/j/b/p8;

    iget v1, p0, Lf/h/a/f/j/b/s8;->e:I

    iget-object v2, p0, Lf/h/a/f/j/b/s8;->f:Lf/h/a/f/j/b/q3;

    iget-object v3, p0, Lf/h/a/f/j/b/s8;->g:Landroid/content/Intent;

    iget-object v4, v0, Lf/h/a/f/j/b/p8;->a:Landroid/content/Context;

    check-cast v4, Lf/h/a/f/j/b/t8;

    invoke-interface {v4, v1}, Lf/h/a/f/j/b/t8;->f(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v4, "Local AppMeasurementService processed last upload request. StartId"

    invoke-virtual {v2, v4, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/p8;->b()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v2, "Completed wakeful intent."

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lf/h/a/f/j/b/p8;->a:Landroid/content/Context;

    check-cast v0, Lf/h/a/f/j/b/t8;

    invoke-interface {v0, v3}, Lf/h/a/f/j/b/t8;->b(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
