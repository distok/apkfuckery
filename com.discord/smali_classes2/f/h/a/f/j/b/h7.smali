.class public final Lf/h/a/f/j/b/h7;
.super Lf/h/a/f/j/b/a5;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public volatile c:Lf/h/a/f/j/b/i7;

.field public d:Lf/h/a/f/j/b/i7;

.field public e:Lf/h/a/f/j/b/i7;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/app/Activity;",
            "Lf/h/a/f/j/b/i7;",
            ">;"
        }
    .end annotation
.end field

.field public g:Landroid/app/Activity;

.field public volatile h:Z

.field public volatile i:Lf/h/a/f/j/b/i7;

.field public j:Lf/h/a/f/j/b/i7;

.field public k:Z

.field public final l:Ljava/lang/Object;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/u4;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/j/b/a5;-><init>(Lf/h/a/f/j/b/u4;)V

    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/h7;->l:Ljava/lang/Object;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/h7;->f:Ljava/util/Map;

    return-void
.end method

.method public static A(Lf/h/a/f/j/b/i7;Landroid/os/Bundle;Z)V
    .locals 4

    const-string v0, "_si"

    const-string v1, "_sn"

    const-string v2, "_sc"

    if-eqz p0, :cond_3

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz p2, :cond_3

    :cond_0
    iget-object p2, p0, Lf/h/a/f/j/b/i7;->a:Ljava/lang/String;

    if-eqz p2, :cond_1

    invoke-virtual {p1, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :goto_0
    iget-object p2, p0, Lf/h/a/f/j/b/i7;->b:Ljava/lang/String;

    if-eqz p2, :cond_2

    invoke-virtual {p1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :goto_1
    iget-wide v1, p0, Lf/h/a/f/j/b/i7;->c:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void

    :cond_3
    if-nez p0, :cond_4

    if-eqz p2, :cond_4

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method public static x(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length v0, p0

    if-lez v0, :cond_0

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget-object p0, p0, v0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0
.end method


# virtual methods
.method public final B(Lf/h/a/f/j/b/i7;Lf/h/a/f/j/b/i7;JZLandroid/os/Bundle;)V
    .locals 8
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->T:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-eqz p5, :cond_0

    iget-object p5, p0, Lf/h/a/f/j/b/h7;->e:Lf/h/a/f/j/b/i7;

    if-eqz p5, :cond_0

    const/4 p5, 0x1

    goto :goto_0

    :cond_0
    const/4 p5, 0x0

    :goto_0
    if-eqz p5, :cond_3

    iget-object v0, p0, Lf/h/a/f/j/b/h7;->e:Lf/h/a/f/j/b/i7;

    invoke-virtual {p0, v0, v1, p3, p4}, Lf/h/a/f/j/b/h7;->C(Lf/h/a/f/j/b/i7;ZJ)V

    goto :goto_1

    :cond_1
    if-eqz p5, :cond_2

    iget-object p5, p0, Lf/h/a/f/j/b/h7;->e:Lf/h/a/f/j/b/i7;

    if-eqz p5, :cond_2

    invoke-virtual {p0, p5, v1, p3, p4}, Lf/h/a/f/j/b/h7;->C(Lf/h/a/f/j/b/i7;ZJ)V

    :cond_2
    const/4 p5, 0x0

    :cond_3
    :goto_1
    if-eqz p2, :cond_4

    iget-wide v3, p2, Lf/h/a/f/j/b/i7;->c:J

    iget-wide v5, p1, Lf/h/a/f/j/b/i7;->c:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_4

    iget-object v0, p2, Lf/h/a/f/j/b/i7;->b:Ljava/lang/String;

    iget-object v3, p1, Lf/h/a/f/j/b/i7;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lf/h/a/f/j/b/t9;->q0(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p2, Lf/h/a/f/j/b/i7;->a:Ljava/lang/String;

    iget-object v3, p1, Lf/h/a/f/j/b/i7;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lf/h/a/f/j/b/t9;->q0(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    const/4 v2, 0x1

    :cond_5
    if-eqz v2, :cond_11

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v3, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v2

    if-eqz v2, :cond_7

    if-eqz p6, :cond_6

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p6}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto :goto_2

    :cond_6
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_7
    :goto_2
    move-object v7, v0

    invoke-static {p1, v7, v1}, Lf/h/a/f/j/b/h7;->A(Lf/h/a/f/j/b/i7;Landroid/os/Bundle;Z)V

    if-eqz p2, :cond_a

    iget-object p6, p2, Lf/h/a/f/j/b/i7;->a:Ljava/lang/String;

    if-eqz p6, :cond_8

    const-string v0, "_pn"

    invoke-virtual {v7, v0, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object p6, p2, Lf/h/a/f/j/b/i7;->b:Ljava/lang/String;

    if-eqz p6, :cond_9

    const-string v0, "_pc"

    invoke-virtual {v7, v0, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-wide v0, p2, Lf/h/a/f/j/b/i7;->c:J

    const-string p2, "_pi"

    invoke-virtual {v7, p2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_a
    iget-object p2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object p6, Lf/h/a/f/j/b/p;->T:Lf/h/a/f/j/b/j3;

    invoke-virtual {p2, p6}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p2

    const-wide/16 v0, 0x0

    if-eqz p2, :cond_c

    if-eqz p5, :cond_c

    sget-object p2, Lf/h/a/f/i/j/s9;->e:Lf/h/a/f/i/j/s9;

    invoke-virtual {p2}, Lf/h/a/f/i/j/s9;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/f/i/j/v9;

    invoke-interface {p2}, Lf/h/a/f/i/j/v9;->a()Z

    move-result p2

    if-eqz p2, :cond_b

    iget-object p2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object p5, Lf/h/a/f/j/b/p;->V:Lf/h/a/f/j/b/j3;

    invoke-virtual {p2, p5}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p2

    if-eqz p2, :cond_b

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->s()Lf/h/a/f/j/b/w8;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/w8;->e:Lf/h/a/f/j/b/d9;

    iget-wide p5, p2, Lf/h/a/f/j/b/d9;->b:J

    sub-long p5, p3, p5

    iput-wide p3, p2, Lf/h/a/f/j/b/d9;->b:J

    goto :goto_3

    :cond_b
    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->s()Lf/h/a/f/j/b/w8;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/w8;->e:Lf/h/a/f/j/b/d9;

    invoke-virtual {p2}, Lf/h/a/f/j/b/d9;->b()J

    move-result-wide p5

    :goto_3
    cmp-long p2, p5, v0

    if-lez p2, :cond_c

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object p2

    invoke-virtual {p2, v7, p5, p6}, Lf/h/a/f/j/b/t9;->G(Landroid/os/Bundle;J)V

    :cond_c
    iget-object p2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object p3, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {p2, p3}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p2

    if-eqz p2, :cond_e

    iget-object p2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {p2}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_d

    const-wide/16 p2, 0x1

    const-string p4, "_mst"

    invoke-virtual {v7, p4, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_d
    iget-boolean p2, p1, Lf/h/a/f/j/b/i7;->e:Z

    if-eqz p2, :cond_e

    const-string p2, "app"

    goto :goto_4

    :cond_e
    const-string p2, "auto"

    :goto_4
    move-object v3, p2

    iget-object p2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object p3, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {p2, p3}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p2

    if-eqz p2, :cond_10

    iget-object p2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast p2, Lf/h/a/f/f/n/d;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    iget-boolean p4, p1, Lf/h/a/f/j/b/i7;->e:Z

    if-eqz p4, :cond_f

    iget-wide p4, p1, Lf/h/a/f/j/b/i7;->f:J

    cmp-long p6, p4, v0

    if-eqz p6, :cond_f

    move-wide v5, p4

    goto :goto_5

    :cond_f
    move-wide v5, p2

    :goto_5
    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->n()Lf/h/a/f/j/b/c6;

    move-result-object v2

    const-string v4, "_vs"

    invoke-virtual/range {v2 .. v7}, Lf/h/a/f/j/b/c6;->E(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    goto :goto_6

    :cond_10
    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->n()Lf/h/a/f/j/b/c6;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/z1;->b()V

    iget-object p2, v2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast p2, Lf/h/a/f/f/n/d;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-string v4, "_vs"

    invoke-virtual/range {v2 .. v7}, Lf/h/a/f/j/b/c6;->E(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V

    :cond_11
    :goto_6
    iput-object p1, p0, Lf/h/a/f/j/b/h7;->e:Lf/h/a/f/j/b/i7;

    iget-object p2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p2, p2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object p3, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {p2, p3}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p2

    if-eqz p2, :cond_12

    iget-boolean p2, p1, Lf/h/a/f/j/b/i7;->e:Z

    if-eqz p2, :cond_12

    iput-object p1, p0, Lf/h/a/f/j/b/h7;->j:Lf/h/a/f/j/b/i7;

    :cond_12
    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->p()Lf/h/a/f/j/b/q7;

    move-result-object p2

    invoke-virtual {p2}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p2}, Lf/h/a/f/j/b/a5;->t()V

    new-instance p3, Lf/h/a/f/j/b/w7;

    invoke-direct {p3, p2, p1}, Lf/h/a/f/j/b/w7;-><init>(Lf/h/a/f/j/b/q7;Lf/h/a/f/j/b/i7;)V

    invoke-virtual {p2, p3}, Lf/h/a/f/j/b/q7;->z(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final C(Lf/h/a/f/j/b/i7;ZJ)V
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->m()Lf/h/a/f/j/b/a;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/a;->t(J)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-boolean v1, p1, Lf/h/a/f/j/b/i7;->d:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->s()Lf/h/a/f/j/b/w8;

    move-result-object v2

    invoke-virtual {v2, v1, p2, p3, p4}, Lf/h/a/f/j/b/w8;->w(ZZJ)Z

    move-result p2

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    iput-boolean v0, p1, Lf/h/a/f/j/b/i7;->d:Z

    :cond_1
    return-void
.end method

.method public final D(Ljava/lang/String;Lf/h/a/f/j/b/i7;)V
    .locals 0
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    monitor-enter p0

    :try_start_0
    iget-object p2, p0, Lf/h/a/f/j/b/h7;->m:Ljava/lang/String;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lf/h/a/f/j/b/h7;->m:Ljava/lang/String;

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final E(Landroid/app/Activity;)Lf/h/a/f/j/b/i7;
    .locals 5
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "null reference"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/j/b/h7;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/j/b/i7;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/j/b/h7;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/i7;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v3

    invoke-virtual {v3}, Lf/h/a/f/j/b/t9;->t0()J

    move-result-wide v3

    invoke-direct {v1, v2, v0, v3, v4}, Lf/h/a/f/j/b/i7;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    iget-object v0, p0, Lf/h/a/f/j/b/h7;->f:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    :cond_0
    iget-object p1, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p1, p1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {p1, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result p1

    if-nez p1, :cond_1

    return-object v0

    :cond_1
    iget-object p1, p0, Lf/h/a/f/j/b/h7;->i:Lf/h/a/f/j/b/i7;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lf/h/a/f/j/b/h7;->i:Lf/h/a/f/j/b/i7;

    return-object p1

    :cond_2
    return-object v0
.end method

.method public final v()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final w(Z)Lf/h/a/f/j/b/i7;
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/h/a/f/j/b/h7;->e:Lf/h/a/f/j/b/i7;

    if-eqz p1, :cond_1

    return-object p1

    :cond_1
    iget-object p1, p0, Lf/h/a/f/j/b/h7;->j:Lf/h/a/f/j/b/i7;

    return-object p1

    :cond_2
    :goto_0
    iget-object p1, p0, Lf/h/a/f/j/b/h7;->e:Lf/h/a/f/j/b/i7;

    return-object p1
.end method

.method public final y(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 5
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v0}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p2, :cond_1

    return-void

    :cond_1
    const-string v0, "com.google.app_measurement.screen_service"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p2

    if-nez p2, :cond_2

    return-void

    :cond_2
    new-instance v0, Lf/h/a/f/j/b/i7;

    const-string v1, "name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "referrer_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lf/h/a/f/j/b/i7;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    iget-object p2, p0, Lf/h/a/f/j/b/h7;->f:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final z(Landroid/app/Activity;Lf/h/a/f/j/b/i7;Z)V
    .locals 16
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v0, p2

    iget-object v1, v7, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    if-nez v1, :cond_0

    iget-object v1, v7, Lf/h/a/f/j/b/h7;->d:Lf/h/a/f/j/b/i7;

    goto :goto_0

    :cond_0
    iget-object v1, v7, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    :goto_0
    move-object v3, v1

    iget-object v1, v0, Lf/h/a/f/j/b/i7;->b:Ljava/lang/String;

    if-nez v1, :cond_2

    if-eqz p1, :cond_1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf/h/a/f/j/b/h7;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    move-object v10, v1

    new-instance v1, Lf/h/a/f/j/b/i7;

    iget-object v9, v0, Lf/h/a/f/j/b/i7;->a:Ljava/lang/String;

    iget-wide v11, v0, Lf/h/a/f/j/b/i7;->c:J

    iget-boolean v13, v0, Lf/h/a/f/j/b/i7;->e:Z

    iget-wide v14, v0, Lf/h/a/f/j/b/i7;->f:J

    move-object v8, v1

    invoke-direct/range {v8 .. v15}, Lf/h/a/f/j/b/i7;-><init>(Ljava/lang/String;Ljava/lang/String;JZJ)V

    move-object v2, v1

    goto :goto_2

    :cond_2
    move-object v2, v0

    :goto_2
    iget-object v0, v7, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    iput-object v0, v7, Lf/h/a/f/j/b/h7;->d:Lf/h/a/f/j/b/i7;

    iput-object v2, v7, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    iget-object v0, v7, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v8

    new-instance v9, Lf/h/a/f/j/b/j7;

    move-object v0, v9

    move-object/from16 v1, p0

    move/from16 v6, p3

    invoke-direct/range {v0 .. v6}, Lf/h/a/f/j/b/j7;-><init>(Lf/h/a/f/j/b/h7;Lf/h/a/f/j/b/i7;Lf/h/a/f/j/b/i7;JZ)V

    invoke-virtual {v8, v9}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method
