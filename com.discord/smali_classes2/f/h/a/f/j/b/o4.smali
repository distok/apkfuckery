.class public final Lf/h/a/f/j/b/o4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/f/i/j/e2;

.field public final synthetic e:Landroid/content/ServiceConnection;

.field public final synthetic f:Lf/h/a/f/j/b/l4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/l4;Lf/h/a/f/i/j/e2;Landroid/content/ServiceConnection;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/o4;->f:Lf/h/a/f/j/b/l4;

    iput-object p2, p0, Lf/h/a/f/j/b/o4;->d:Lf/h/a/f/i/j/e2;

    iput-object p3, p0, Lf/h/a/f/j/b/o4;->e:Landroid/content/ServiceConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    iget-object v0, p0, Lf/h/a/f/j/b/o4;->f:Lf/h/a/f/j/b/l4;

    iget-object v1, v0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    iget-object v0, v0, Lf/h/a/f/j/b/l4;->d:Ljava/lang/String;

    iget-object v2, p0, Lf/h/a/f/j/b/o4;->d:Lf/h/a/f/i/j/e2;

    iget-object v3, p0, Lf/h/a/f/j/b/o4;->e:Landroid/content/ServiceConnection;

    iget-object v4, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v4}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/j/b/r4;->b()V

    if-nez v2, :cond_0

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v2, "Attempting to use Install Referrer Service while it is not initialized"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "package_name"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-interface {v2, v4}, Lf/h/a/f/i/j/e2;->f(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Install Referrer Service returned a null response"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v2, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Exception occurred while retrieving the Install Referrer"

    invoke-virtual {v2, v4, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x0

    :cond_1
    iget-object v2, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/r4;->b()V

    if-eqz v0, :cond_c

    const-wide/16 v4, 0x0

    const-string v2, "install_begin_timestamp_seconds"

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long v6, v6, v8

    cmp-long v2, v6, v4

    if-nez v2, :cond_2

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v2, "Service response is missing Install Referrer install timestamp"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_2
    const-string v2, "install_referrer"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_3

    goto/16 :goto_3

    :cond_3
    iget-object v10, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v10}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v10

    iget-object v10, v10, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v11, "InstallReferrer API result"

    invoke-virtual {v10, v11, v2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v10, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v10}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v10

    const-string v11, "?"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual {v11, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v10, v2}, Lf/h/a/f/j/b/t9;->x(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_5

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "No campaign params defined in Install Referrer result"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_5
    const-string v10, "medium"

    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_6

    const-string v11, "(not set)"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_6

    const-string v11, "organic"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    const/4 v10, 0x1

    goto :goto_2

    :cond_6
    const/4 v10, 0x0

    :goto_2
    if-eqz v10, :cond_8

    const-string v10, "referrer_click_timestamp_seconds"

    invoke-virtual {v0, v10, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    mul-long v10, v10, v8

    cmp-long v0, v10, v4

    if-nez v0, :cond_7

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Install Referrer is missing click timestamp for ad campaign"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_7
    const-string v0, "click_timestamp"

    invoke-virtual {v2, v0, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_8
    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->k:Lf/h/a/f/j/b/h4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/h4;->a()J

    move-result-wide v4

    cmp-long v0, v6, v4

    if-nez v0, :cond_9

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v2, "Install Referrer campaign has already been logged"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    sget-object v0, Lf/h/a/f/i/j/g9;->e:Lf/h/a/f/i/j/g9;

    invoke-virtual {v0}, Lf/h/a/f/i/j/g9;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/j9;

    invoke-interface {v0}, Lf/h/a/f/i/j/j9;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v4, Lf/h/a/f/j/b/p;->t0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->k:Lf/h/a/f/j/b/h4;

    invoke-virtual {v0, v6, v7}, Lf/h/a/f/j/b/h4;->b(J)V

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v4, "referrer API"

    const-string v5, "Logging Install Referrer campaign from sdk with "

    invoke-virtual {v0, v5, v4}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "_cis"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    const-string v4, "auto"

    const-string v5, "_cmp"

    invoke-virtual {v0, v4, v5, v2}, Lf/h/a/f/j/b/c6;->H(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_4

    :cond_b
    :goto_3
    iget-object v0, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "No referrer defined in Install Referrer response"

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_c
    :goto_4
    if-eqz v3, :cond_d

    invoke-static {}, Lf/h/a/f/f/m/a;->b()Lf/h/a/f/f/m/a;

    move-result-object v0

    iget-object v1, v1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v3}, Lf/h/a/f/f/m/a;->c(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    :cond_d
    return-void
.end method
