.class public final Lf/h/a/f/j/b/i8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/google/android/gms/measurement/internal/zzn;

.field public final synthetic g:Lf/h/a/f/i/j/fc;

.field public final synthetic h:Lf/h/a/f/j/b/q7;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/q7;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzn;Lf/h/a/f/i/j/fc;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    iput-object p2, p0, Lf/h/a/f/j/b/i8;->d:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/f/j/b/i8;->e:Ljava/lang/String;

    iput-object p4, p0, Lf/h/a/f/j/b/i8;->f:Lcom/google/android/gms/measurement/internal/zzn;

    iput-object p5, p0, Lf/h/a/f/j/b/i8;->g:Lf/h/a/f/i/j/fc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    iget-object v2, v1, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Failed to get conditional properties; not connected to service"

    iget-object v3, p0, Lf/h/a/f/j/b/i8;->d:Ljava/lang/String;

    iget-object v4, p0, Lf/h/a/f/j/b/i8;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/i8;->g:Lf/h/a/f/i/j/fc;

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/t9;->N(Lf/h/a/f/i/j/fc;Ljava/util/ArrayList;)V

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lf/h/a/f/j/b/i8;->d:Ljava/lang/String;

    iget-object v3, p0, Lf/h/a/f/j/b/i8;->e:Ljava/lang/String;

    iget-object v4, p0, Lf/h/a/f/j/b/i8;->f:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-interface {v2, v1, v3, v4}, Lf/h/a/f/j/b/i3;->E(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzn;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lf/h/a/f/j/b/t9;->j0(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/q7;->F()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/i8;->g:Lf/h/a/f/i/j/fc;

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/t9;->N(Lf/h/a/f/i/j/fc;Ljava/util/ArrayList;)V

    return-void

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    iget-object v2, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v3, "Failed to get conditional properties; remote exception"

    iget-object v4, p0, Lf/h/a/f/j/b/i8;->d:Ljava/lang/String;

    iget-object v5, p0, Lf/h/a/f/j/b/i8;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v1}, Lf/h/a/f/j/b/s3;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/i8;->g:Lf/h/a/f/i/j/fc;

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/j/b/t9;->N(Lf/h/a/f/i/j/fc;Ljava/util/ArrayList;)V

    return-void

    :goto_0
    iget-object v2, p0, Lf/h/a/f/j/b/i8;->h:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v2

    iget-object v3, p0, Lf/h/a/f/j/b/i8;->g:Lf/h/a/f/i/j/fc;

    invoke-virtual {v2, v3, v0}, Lf/h/a/f/j/b/t9;->N(Lf/h/a/f/i/j/fc;Ljava/util/ArrayList;)V

    throw v1
.end method
