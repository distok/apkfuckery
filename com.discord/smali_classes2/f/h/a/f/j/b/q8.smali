.class public final Lf/h/a/f/j/b/q8;
.super Lf/h/a/f/j/b/i9;
.source "com.google.android.gms:play-services-measurement@@18.0.0"


# instance fields
.field public d:Ljava/lang/String;

.field public e:Z

.field public f:J


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/k9;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/j/b/i9;-><init>(Lf/h/a/f/j/b/k9;)V

    return-void
.end method


# virtual methods
.method public final p()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final s(Ljava/lang/String;Lf/h/a/f/j/b/d;)Landroid/util/Pair;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/h/a/f/j/b/d;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->J0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lf/h/a/f/j/b/d;->j()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/util/Pair;

    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v0, ""

    invoke-direct {p1, v0, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lf/h/a/f/j/b/q8;->u(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object p1

    return-object p1
.end method

.method public final t(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {p0, p1}, Lf/h/a/f/j/b/q8;->u(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-static {}, Lf/h/a/f/j/b/t9;->x0()Ljava/security/MessageDigest;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p1

    invoke-direct {v5, v2, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v5, v3, v4

    const-string p1, "%032X"

    invoke-static {v1, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final u(Ljava/lang/String;)Landroid/util/Pair;
    .locals 6
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, ""

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->b()V

    iget-object v1, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v3, p0, Lf/h/a/f/j/b/q8;->d:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-wide v3, p0, Lf/h/a/f/j/b/q8;->f:J

    cmp-long v5, v1, v3

    if-gez v5, :cond_0

    new-instance p1, Landroid/util/Pair;

    iget-object v0, p0, Lf/h/a/f/j/b/q8;->d:Ljava/lang/String;

    iget-boolean v1, p0, Lf/h/a/f/j/b/q8;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_0
    iget-object v3, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lf/h/a/f/j/b/p;->b:Lf/h/a/f/j/b/j3;

    invoke-virtual {v3, p1, v4}, Lf/h/a/f/j/b/c;->n(Ljava/lang/String;Lf/h/a/f/j/b/j3;)J

    move-result-wide v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lf/h/a/f/j/b/q8;->f:J

    :try_start_0
    iget-object p1, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p1, p1, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {p1}, Lf/h/a/f/a/a/a;->b(Landroid/content/Context;)Lf/h/a/f/a/a/a$a;

    move-result-object p1

    iget-object v1, p1, Lf/h/a/f/a/a/a$a;->a:Ljava/lang/String;

    iput-object v1, p0, Lf/h/a/f/j/b/q8;->d:Ljava/lang/String;

    iget-boolean p1, p1, Lf/h/a/f/a/a/a$a;->b:Z

    iput-boolean p1, p0, Lf/h/a/f/j/b/q8;->e:Z

    if-nez v1, :cond_1

    iput-object v0, p0, Lf/h/a/f/j/b/q8;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v2, "Unable to get advertising id"

    invoke-virtual {v1, v2, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/h/a/f/j/b/q8;->d:Ljava/lang/String;

    :cond_1
    :goto_0
    new-instance p1, Landroid/util/Pair;

    iget-object v0, p0, Lf/h/a/f/j/b/q8;->d:Ljava/lang/String;

    iget-boolean v1, p0, Lf/h/a/f/j/b/q8;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method
