.class public final Lf/h/a/f/j/b/c;
.super Lf/h/a/f/j/b/s5;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Lf/h/a/f/j/b/e;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/u4;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/j/b/s5;-><init>(Lf/h/a/f/j/b/u4;)V

    sget-object p1, Lf/h/a/f/j/b/b;->a:Lf/h/a/f/j/b/e;

    iput-object p1, p0, Lf/h/a/f/j/b/c;->c:Lf/h/a/f/j/b/e;

    return-void
.end method

.method public static B()J
    .locals 2

    sget-object v0, Lf/h/a/f/j/b/p;->D:Lf/h/a/f/j/b/j3;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final A(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/j/b/c;->c:Lf/h/a/f/j/b/e;

    const-string v1, "measurement.event_sampling_enabled"

    invoke-interface {v0, p1, v1}, Lf/h/a/f/j/b/e;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "1"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final C()Z
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/c;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const-string v0, "app_measurement_lite"

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/j/b/c;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lf/h/a/f/j/b/c;->b:Ljava/lang/Boolean;

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-boolean v0, v0, Lf/h/a/f/j/b/u4;->e:Z

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return v0

    :cond_2
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public final D()Landroid/os/Bundle;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Failed to load metadata: PackageManager is null"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v1}, Lf/h/a/f/f/o/b;->a(Landroid/content/Context;)Lf/h/a/f/f/o/a;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/f/o/a;->a(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Failed to load metadata: ApplicationInfo is null"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-object v0

    :cond_1
    iget-object v0, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v3, "Failed to load metadata: Package name not found"

    invoke-virtual {v2, v3, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const-class v0, Ljava/lang/String;

    :try_start_0
    const-string v1, "android.os.SystemProperties"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "get"

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v6, 0x1

    aput-object v0, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v5

    aput-object p2, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "SystemProperties.get() threw an exception"

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "Could not access SystemProperties.get()"

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "Could not find SystemProperties.get() method"

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :catch_3
    move-exception p1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "Could not find SystemProperties class"

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    return-object p2
.end method

.method public final m(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    sget-object v0, Lf/h/a/f/j/b/p;->I:Lf/h/a/f/j/b/j3;

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/j/b/c;->q(Ljava/lang/String;Lf/h/a/f/j/b/j3;)I

    move-result p1

    const/16 v0, 0x64

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/16 v0, 0x19

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1
.end method

.method public final n(Ljava/lang/String;Lf/h/a/f/j/b/j3;)J
    .locals 3
    .param p2    # Lf/h/a/f/j/b/j3;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/h/a/f/j/b/j3<",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    return-wide p1

    :cond_0
    iget-object v1, p0, Lf/h/a/f/j/b/c;->c:Lf/h/a/f/j/b/e;

    iget-object v2, p2, Lf/h/a/f/j/b/j3;->a:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lf/h/a/f/j/b/e;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    return-wide p1

    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p2, p1}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide p1

    :catch_0
    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    return-wide p1
.end method

.method public final o(Lf/h/a/f/j/b/j3;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/j/b/j3<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result p1

    return p1
.end method

.method public final p(Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    invoke-static {}, Lf/h/a/f/i/j/o8;->b()Z

    move-result v0

    const/16 v1, 0x1f4

    if-eqz v0, :cond_0

    sget-object v0, Lf/h/a/f/j/b/p;->w0:Lf/h/a/f/j/b/j3;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lf/h/a/f/j/b/p;->H:Lf/h/a/f/j/b/j3;

    const/16 v2, 0x7d0

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/j/b/c;->q(Ljava/lang/String;Lf/h/a/f/j/b/j3;)I

    move-result p1

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1

    :cond_0
    return v1
.end method

.method public final q(Ljava/lang/String;Lf/h/a/f/j/b/j3;)I
    .locals 3
    .param p2    # Lf/h/a/f/j/b/j3;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/h/a/f/j/b/j3<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_0
    iget-object v1, p0, Lf/h/a/f/j/b/c;->c:Lf/h/a/f/j/b/e;

    iget-object v2, p2, Lf/h/a/f/j/b/j3;->a:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lf/h/a/f/j/b/e;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method public final r(Ljava/lang/String;Lf/h/a/f/j/b/j3;)D
    .locals 3
    .param p2    # Lf/h/a/f/j/b/j3;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/h/a/f/j/b/j3<",
            "Ljava/lang/Double;",
            ">;)D"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p1

    return-wide p1

    :cond_0
    iget-object v1, p0, Lf/h/a/f/j/b/c;->c:Lf/h/a/f/j/b/e;

    iget-object v2, p2, Lf/h/a/f/j/b/j3;->a:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lf/h/a/f/j/b/e;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p1

    return-wide p1

    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {p2, p1}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide p1

    :catch_0
    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p1

    return-wide p1
.end method

.method public final s(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    sget-object v0, Lf/h/a/f/j/b/p;->o:Lf/h/a/f/j/b/j3;

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/j/b/c;->q(Ljava/lang/String;Lf/h/a/f/j/b/j3;)I

    move-result p1

    return p1
.end method

.method public final t()I
    .locals 3

    invoke-static {}, Lf/h/a/f/i/j/o8;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->x0:Lf/h/a/f/j/b/j3;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    iget-object v1, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->x()Lf/h/a/f/j/b/q7;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q7;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lf/h/a/f/j/b/t9;->y0()I

    move-result v0

    const v2, 0x3131c

    if-ge v0, v2, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    const/16 v0, 0x64

    return v0

    :cond_2
    const/16 v0, 0x19

    return v0
.end method

.method public final u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z
    .locals 3
    .param p2    # Lf/h/a/f/j/b/j3;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/h/a/f/j/b/j3<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_0
    iget-object v1, p0, Lf/h/a/f/j/b/c;->c:Lf/h/a/f/j/b/e;

    iget-object v2, p2, Lf/h/a/f/j/b/j3;->a:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lf/h/a/f/j/b/e;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_1
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2, p1}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public final v(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/h/a/f/j/b/j3<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result p1

    return p1
.end method

.method public final w(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-static {p1}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p0}, Lf/h/a/f/j/b/c;->D()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v0, "Failed to load metadata: Metadata bundle is null"

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final x()Z
    .locals 1

    const-string v0, "firebase_analytics_collection_deactivated"

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final y()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "google_analytics_adid_collection_enabled"

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/Boolean;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/pa;->e:Lf/h/a/f/i/j/pa;

    invoke-virtual {v0}, Lf/h/a/f/i/j/pa;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/oa;

    invoke-interface {v0}, Lf/h/a/f/i/j/oa;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lf/h/a/f/j/b/p;->u0:Lf/h/a/f/j/b/j3;

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    :cond_0
    const-string v0, "google_analytics_automatic_screen_reporting_enabled"

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_3
    :goto_2
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method
