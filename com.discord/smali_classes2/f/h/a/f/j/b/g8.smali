.class public final Lf/h/a/f/j/b/g8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Z

.field public final synthetic e:Lcom/google/android/gms/measurement/internal/zzz;

.field public final synthetic f:Lcom/google/android/gms/measurement/internal/zzn;

.field public final synthetic g:Lcom/google/android/gms/measurement/internal/zzz;

.field public final synthetic h:Lf/h/a/f/j/b/q7;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/q7;ZLcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;Lcom/google/android/gms/measurement/internal/zzz;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/g8;->h:Lf/h/a/f/j/b/q7;

    iput-boolean p2, p0, Lf/h/a/f/j/b/g8;->d:Z

    iput-object p3, p0, Lf/h/a/f/j/b/g8;->e:Lcom/google/android/gms/measurement/internal/zzz;

    iput-object p4, p0, Lf/h/a/f/j/b/g8;->f:Lcom/google/android/gms/measurement/internal/zzn;

    iput-object p5, p0, Lf/h/a/f/j/b/g8;->g:Lcom/google/android/gms/measurement/internal/zzz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/j/b/g8;->h:Lf/h/a/f/j/b/q7;

    iget-object v1, v0, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "Discarding data. Failed to send conditional user property to service"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-boolean v2, p0, Lf/h/a/f/j/b/g8;->d:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lf/h/a/f/j/b/g8;->e:Lcom/google/android/gms/measurement/internal/zzz;

    :goto_0
    iget-object v3, p0, Lf/h/a/f/j/b/g8;->f:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v0, v1, v2, v3}, Lf/h/a/f/j/b/q7;->w(Lf/h/a/f/j/b/i3;Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;Lcom/google/android/gms/measurement/internal/zzn;)V

    iget-object v0, p0, Lf/h/a/f/j/b/g8;->h:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/q7;->F()V

    return-void
.end method
