.class public final Lf/h/a/f/j/b/q7;
.super Lf/h/a/f/j/b/a5;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public final c:Lf/h/a/f/j/b/k8;

.field public d:Lf/h/a/f/j/b/i3;

.field public volatile e:Ljava/lang/Boolean;

.field public final f:Lf/h/a/f/j/b/i;

.field public final g:Lf/h/a/f/j/b/e9;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lf/h/a/f/j/b/i;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/u4;)V
    .locals 2

    invoke-direct {p0, p1}, Lf/h/a/f/j/b/a5;-><init>(Lf/h/a/f/j/b/u4;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/a/f/j/b/q7;->h:Ljava/util/List;

    new-instance v0, Lf/h/a/f/j/b/e9;

    iget-object v1, p1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    invoke-direct {v0, v1}, Lf/h/a/f/j/b/e9;-><init>(Lf/h/a/f/f/n/c;)V

    iput-object v0, p0, Lf/h/a/f/j/b/q7;->g:Lf/h/a/f/j/b/e9;

    new-instance v0, Lf/h/a/f/j/b/k8;

    invoke-direct {v0, p0}, Lf/h/a/f/j/b/k8;-><init>(Lf/h/a/f/j/b/q7;)V

    iput-object v0, p0, Lf/h/a/f/j/b/q7;->c:Lf/h/a/f/j/b/k8;

    new-instance v0, Lf/h/a/f/j/b/p7;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/j/b/p7;-><init>(Lf/h/a/f/j/b/q7;Lf/h/a/f/j/b/t5;)V

    iput-object v0, p0, Lf/h/a/f/j/b/q7;->f:Lf/h/a/f/j/b/i;

    new-instance v0, Lf/h/a/f/j/b/z7;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/j/b/z7;-><init>(Lf/h/a/f/j/b/q7;Lf/h/a/f/j/b/t5;)V

    iput-object v0, p0, Lf/h/a/f/j/b/q7;->i:Lf/h/a/f/j/b/i;

    return-void
.end method

.method public static x(Lf/h/a/f/j/b/q7;Landroid/content/ComponentName;)V
    .locals 2

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v1, "Disconnected from device MeasurementService"

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/q7;->C()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final A(Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/q7;->I(Z)Lcom/google/android/gms/measurement/internal/zzn;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/v7;

    invoke-direct {v1, p0, p1, v0}, Lf/h/a/f/j/b/v7;-><init>(Lf/h/a/f/j/b/q7;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/gms/measurement/internal/zzn;)V

    invoke-virtual {p0, v1}, Lf/h/a/f/j/b/q7;->z(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final B()Z
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final C()V
    .locals 7
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/q7;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lf/h/a/f/j/b/q7;->G()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->c:Lf/h/a/f/j/b/k8;

    iget-object v2, v0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v2, v0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    iget-object v2, v2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    monitor-enter v0

    :try_start_0
    iget-boolean v3, v0, Lf/h/a/f/j/b/k8;->d:Z

    if-eqz v3, :cond_1

    iget-object v1, v0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v2, "Connection attempt already in progress"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    monitor-exit v0

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    invoke-virtual {v3}, Lf/h/a/f/f/k/b;->e()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    invoke-virtual {v3}, Lf/h/a/f/f/k/b;->j()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v1, v0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v2, "Already awaiting connection attempt"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    monitor-exit v0

    goto :goto_0

    :cond_3
    new-instance v3, Lf/h/a/f/j/b/r3;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v2, v4, v0, v0}, Lf/h/a/f/j/b/r3;-><init>(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/b$a;Lf/h/a/f/f/k/b$b;)V

    iput-object v3, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    iget-object v2, v0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v3, "Connecting to remote service"

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iput-boolean v1, v0, Lf/h/a/f/j/b/k8;->d:Z

    iget-object v1, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    invoke-virtual {v1}, Lf/h/a/f/f/k/b;->q()V

    monitor-exit v0

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_4
    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v0}, Lf/h/a/f/j/b/c;->C()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v4, "com.google.android.gms.measurement.AppMeasurementService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.measurement.START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v4, "com.google.android.gms.measurement.AppMeasurementService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v2, p0, Lf/h/a/f/j/b/q7;->c:Lf/h/a/f/j/b/k8;

    iget-object v3, v2, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v3}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v3, v2, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    iget-object v3, v3, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {}, Lf/h/a/f/f/m/a;->b()Lf/h/a/f/f/m/a;

    move-result-object v4

    monitor-enter v2

    :try_start_1
    iget-boolean v5, v2, Lf/h/a/f/j/b/k8;->d:Z

    if-eqz v5, :cond_6

    iget-object v0, v2, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v1, "Connection attempt already in progress"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    monitor-exit v2

    goto :goto_2

    :cond_6
    iget-object v5, v2, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v5}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v5

    iget-object v5, v5, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v6, "Using local app measurement service"

    invoke-virtual {v5, v6}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iput-boolean v1, v2, Lf/h/a/f/j/b/k8;->d:Z

    iget-object v1, v2, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    iget-object v1, v1, Lf/h/a/f/j/b/q7;->c:Lf/h/a/f/j/b/k8;

    const/16 v5, 0x81

    invoke-virtual {v4, v3, v0, v1, v5}, Lf/h/a/f/f/m/a;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    monitor-exit v2

    :goto_2
    return-void

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :cond_7
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_8
    return-void
.end method

.method public final D()V
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->c:Lf/h/a/f/j/b/k8;

    iget-object v1, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    invoke-virtual {v1}, Lf/h/a/f/f/k/b;->j()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    invoke-virtual {v1}, Lf/h/a/f/f/k/b;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    invoke-virtual {v1}, Lf/h/a/f/f/k/b;->h()V

    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    :try_start_0
    invoke-static {}, Lf/h/a/f/f/m/a;->b()Lf/h/a/f/f/m/a;

    move-result-object v0

    iget-object v2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    iget-object v3, p0, Lf/h/a/f/j/b/q7;->c:Lf/h/a/f/j/b/k8;

    invoke-virtual {v0, v2, v3}, Lf/h/a/f/f/m/a;->c(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iput-object v1, p0, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    return-void
.end method

.method public final E()Z
    .locals 5
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v1, Lf/h/a/f/j/b/p;->J0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Lf/h/a/f/j/b/q7;->G()Z

    move-result v0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/t9;->y0()I

    move-result v0

    sget-object v3, Lf/h/a/f/j/b/p;->K0:Lf/h/a/f/j/b/j3;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v0, v3, :cond_2

    return v2

    :cond_2
    return v1
.end method

.method public final F()V
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->g:Lf/h/a/f/j/b/e9;

    iget-object v1, v0, Lf/h/a/f/j/b/e9;->a:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lf/h/a/f/j/b/e9;->b:J

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->f:Lf/h/a/f/j/b/i;

    sget-object v1, Lf/h/a/f/j/b/p;->J:Lf/h/a/f/j/b/j3;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/j3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/i;->b(J)V

    return-void
.end method

.method public final G()Z
    .locals 7
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "use_service"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    goto/16 :goto_7

    :cond_1
    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->o()Lf/h/a/f/j/b/n3;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/j/b/a5;->t()V

    iget v4, v4, Lf/h/a/f/j/b/n3;->j:I

    if-ne v4, v1, :cond_2

    goto/16 :goto_4

    :cond_2
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v4

    iget-object v4, v4, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v5, "Checking service availability"

    invoke-virtual {v4, v5}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lf/h/a/f/f/c;->b:Lf/h/a/f/f/c;

    iget-object v4, v4, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v4, v4, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const v6, 0xbdfcb8

    invoke-virtual {v5, v4, v6}, Lf/h/a/f/f/c;->b(Landroid/content/Context;I)I

    move-result v4

    if-eqz v4, :cond_a

    if-eq v4, v1, :cond_9

    const/4 v5, 0x2

    if-eq v4, v5, :cond_6

    const/4 v0, 0x3

    if-eq v4, v0, :cond_5

    const/16 v0, 0x9

    if-eq v4, v0, :cond_4

    const/16 v0, 0x12

    if-eq v4, v0, :cond_3

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v4, "Unexpected service status"

    invoke-virtual {v0, v4, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v4, "Service updating"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v1, "Service invalid"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v1, "Service disabled"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_1
    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v4

    iget-object v4, v4, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v5, "Service container out of date"

    invoke-virtual {v4, v5}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/j/b/t9;->y0()I

    move-result v4

    const/16 v5, 0x4423

    if-ge v4, v5, :cond_7

    goto :goto_3

    :cond_7
    if-nez v0, :cond_8

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    :goto_2
    const/4 v0, 0x0

    goto :goto_5

    :cond_9
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v4, "Service missing"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_3
    move v0, v1

    const/4 v1, 0x0

    goto :goto_5

    :cond_a
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v4, "Service available"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_4
    const/4 v0, 0x1

    :goto_5
    if-nez v1, :cond_b

    iget-object v4, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v4, v4, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v4}, Lf/h/a/f/j/b/c;->C()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v4, "No way to upload. Consider using the full version of Analytics"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_6

    :cond_b
    move v3, v0

    :goto_6
    if-eqz v3, :cond_c

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_c
    :goto_7
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/j/b/q7;->e:Ljava/lang/Boolean;

    :cond_d
    iget-object v0, p0, Lf/h/a/f/j/b/q7;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final H()V
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    iget-object v1, p0, Lf/h/a/f/j/b/q7;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Processing queued up service tasks"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    :try_start_0
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v3, "Task exception while flushing queue"

    invoke-virtual {v2, v3, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/q7;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lf/h/a/f/j/b/q7;->i:Lf/h/a/f/j/b/i;

    invoke-virtual {v0}, Lf/h/a/f/j/b/i;->c()V

    return-void
.end method

.method public final I(Z)Lcom/google/android/gms/measurement/internal/zzn;
    .locals 35
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/z1;->o()Lf/h/a/f/j/b/n3;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/q3;->E()Ljava/lang/String;

    move-result-object v0

    move-object v14, v0

    goto :goto_0

    :cond_0
    const/4 v14, 0x0

    :goto_0
    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    new-instance v33, Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v4, v1, Lf/h/a/f/j/b/n3;->c:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v5, v1, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v6, v1, Lf/h/a/f/j/b/n3;->d:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget v0, v1, Lf/h/a/f/j/b/n3;->e:I

    int-to-long v7, v0

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v9, v1, Lf/h/a/f/j/b/n3;->f:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    iget-wide v10, v1, Lf/h/a/f/j/b/n3;->g:J

    const/4 v3, 0x0

    const-wide/16 v12, 0x0

    cmp-long v0, v10, v12

    if-nez v0, :cond_4

    iget-object v0, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v10

    iget-object v0, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10}, Lf/h/a/f/j/b/s5;->b()V

    invoke-static {v11}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    invoke-static {}, Lf/h/a/f/j/b/t9;->x0()Ljava/security/MessageDigest;

    move-result-object v12

    const-wide/16 v18, -0x1

    if-nez v12, :cond_1

    invoke-virtual {v10}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v10, "Could not get MD5 instance"

    invoke-virtual {v0, v10}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    if-eqz v15, :cond_3

    :try_start_0
    invoke-virtual {v10, v0, v11}, Lf/h/a/f/j/b/t9;->o0(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_3

    invoke-static {v0}, Lf/h/a/f/f/o/b;->a(Landroid/content/Context;)Lf/h/a/f/f/o/a;

    move-result-object v0

    iget-object v11, v10, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v11, v11, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const/16 v13, 0x40

    invoke-virtual {v0, v11, v13}, Lf/h/a/f/f/o/a;->b(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v0, :cond_2

    array-length v11, v0

    if-lez v11, :cond_2

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/j/b/t9;->w([B)J

    move-result-wide v18

    goto :goto_1

    :cond_2
    invoke-virtual {v10}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v11, "Could not get signatures"

    invoke-virtual {v0, v11}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-wide/from16 v10, v18

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v10}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v10

    iget-object v10, v10, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v11, "Package name not found"

    invoke-virtual {v10, v11, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    const-wide/16 v10, 0x0

    :goto_2
    iput-wide v10, v1, Lf/h/a/f/j/b/n3;->g:J

    :cond_4
    iget-wide v12, v1, Lf/h/a/f/j/b/n3;->g:J

    iget-object v0, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v15

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-boolean v0, v0, Lf/h/a/f/j/b/d4;->v:Z

    const/4 v10, 0x1

    xor-int/2addr v0, v10

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v11, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v11}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v11

    if-nez v11, :cond_5

    :catch_1
    :goto_3
    move/from16 v20, v15

    :goto_4
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_5
    sget-object v11, Lf/h/a/f/i/j/ob;->e:Lf/h/a/f/i/j/ob;

    invoke-virtual {v11}, Lf/h/a/f/i/j/ob;->a()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lf/h/a/f/i/j/rb;

    invoke-interface {v11}, Lf/h/a/f/i/j/rb;->a()Z

    move-result v11

    if-eqz v11, :cond_6

    iget-object v11, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v11, v11, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->l0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v11, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v11, "Disabled IID for tests."

    invoke-virtual {v2, v11}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    :try_start_1
    iget-object v2, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    const-string v11, "com.google.firebase.analytics.FirebaseAnalytics"

    invoke-virtual {v2, v11}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v2, :cond_7

    goto :goto_3

    :cond_7
    :try_start_2
    const-string v11, "getInstance"

    new-array v3, v10, [Ljava/lang/Class;

    const-class v19, Landroid/content/Context;

    const/16 v20, 0x0

    aput-object v19, v3, v20

    invoke-virtual {v2, v11, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    new-array v11, v10, [Ljava/lang/Object;

    iget-object v10, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v10, v10, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move/from16 v20, v15

    const/4 v15, 0x0

    :try_start_3
    aput-object v10, v11, v15

    const/4 v10, 0x0

    invoke-virtual {v3, v10, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    if-nez v3, :cond_8

    move-object v2, v10

    goto :goto_5

    :cond_8
    :try_start_4
    const-string v11, "getFirebaseInstanceId"

    new-array v10, v15, [Ljava/lang/Class;

    invoke-virtual {v2, v11, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v10, v15, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_5

    :catch_2
    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string v3, "Failed to retrieve Firebase Instance Id"

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_4

    :catch_3
    move/from16 v20, v15

    :catch_4
    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->j:Lf/h/a/f/j/b/s3;

    const-string v3, "Failed to obtain Firebase Analytics instance"

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    :goto_5
    const-wide/16 v21, 0x0

    iget-object v3, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v3}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v10

    iget-object v10, v10, Lf/h/a/f/j/b/d4;->j:Lf/h/a/f/j/b/h4;

    invoke-virtual {v10}, Lf/h/a/f/j/b/h4;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v23

    const-wide/16 v15, 0x0

    cmp-long v11, v23, v15

    if-nez v11, :cond_9

    iget-wide v10, v3, Lf/h/a/f/j/b/u4;->G:J

    move-object/from16 v17, v2

    move-wide/from16 v23, v10

    goto :goto_6

    :cond_9
    move-object/from16 v17, v2

    iget-wide v2, v3, Lf/h/a/f/j/b/u4;->G:J

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    move-wide/from16 v23, v2

    :goto_6
    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget v2, v1, Lf/h/a/f/j/b/n3;->j:I

    iget-object v3, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v3}, Lf/h/a/f/j/b/c;->y()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v25

    iget-object v3, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    const-string v10, "google_analytics_ssaid_collection_enabled"

    invoke-virtual {v3, v10}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_a

    goto :goto_7

    :cond_a
    const/4 v3, 0x0

    goto :goto_8

    :cond_b
    :goto_7
    const/4 v3, 0x1

    :goto_8
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v3

    invoke-virtual {v3}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v3}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v10, "deferred_analytics_collection"

    const/4 v11, 0x0

    invoke-interface {v3, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v27

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v15, v1, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    iget-object v3, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    const-string v10, "google_analytics_default_allow_ad_personalization_signals"

    invoke-virtual {v3, v10}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_c

    const/16 v28, 0x0

    goto :goto_9

    :cond_c
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v10, 0x1

    xor-int/2addr v3, v10

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v28, v3

    :goto_9
    iget-wide v10, v1, Lf/h/a/f/j/b/n3;->h:J

    iget-object v3, v1, Lf/h/a/f/j/b/n3;->i:Ljava/util/List;

    invoke-static {}, Lf/h/a/f/i/j/da;->b()Z

    move-result v16

    if-eqz v16, :cond_d

    move-object/from16 v16, v3

    iget-object v3, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    move-wide/from16 v29, v10

    sget-object v10, Lf/h/a/f/j/b/p;->j0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v3, v10}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v3, v1, Lf/h/a/f/j/b/n3;->m:Ljava/lang/String;

    move-object/from16 v31, v3

    goto :goto_a

    :cond_d
    move-object/from16 v16, v3

    move-wide/from16 v29, v10

    :cond_e
    const/16 v31, 0x0

    :goto_a
    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v3

    if-eqz v3, :cond_f

    iget-object v3, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v10, Lf/h/a/f/j/b/p;->H0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v3, v10}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/d4;->y()Lf/h/a/f/j/b/d;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/d;->d()Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    :cond_f
    const-string v1, ""

    :goto_b
    move-object/from16 v32, v1

    const-wide/32 v10, 0x8101

    move-object/from16 v1, v16

    move-object/from16 v3, v33

    move-object/from16 v34, v15

    move/from16 v15, v20

    move/from16 v16, v0

    move-wide/from16 v18, v21

    move-wide/from16 v20, v23

    move/from16 v22, v2

    move/from16 v23, v25

    move/from16 v24, v26

    move/from16 v25, v27

    move-object/from16 v26, v34

    move-object/from16 v27, v28

    move-wide/from16 v28, v29

    move-object/from16 v30, v1

    invoke-direct/range {v3 .. v32}, Lcom/google/android/gms/measurement/internal/zzn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJLjava/lang/String;ZZLjava/lang/String;JJIZZZLjava/lang/String;Ljava/lang/Boolean;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v33
.end method

.method public final v()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final w(Lf/h/a/f/j/b/i3;Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 27
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/a5;->t()V

    const/16 v4, 0x64

    const/4 v5, 0x0

    const/16 v0, 0x64

    const/4 v6, 0x0

    :goto_0
    const/16 v7, 0x3e9

    if-ge v6, v7, :cond_1a

    if-ne v0, v4, :cond_1a

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/z1;->r()Lf/h/a/f/j/b/m3;

    move-result-object v8

    const-string v9, "Error reading entries from local database"

    invoke-virtual {v8}, Lf/h/a/f/j/b/z1;->b()V

    iget-boolean v0, v8, Lf/h/a/f/j/b/m3;->d:Z

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v8, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v12, "google_app_measurement_local.db"

    invoke-virtual {v0, v12}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v10, v11

    goto :goto_3

    :cond_1
    const/4 v12, 0x5

    const/4 v13, 0x0

    const/4 v14, 0x5

    :goto_1
    if-ge v13, v12, :cond_13

    const/4 v15, 0x1

    :try_start_0
    invoke-virtual {v8}, Lf/h/a/f/j/b/m3;->A()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    if-nez v10, :cond_3

    :try_start_1
    iput-boolean v15, v8, Lf/h/a/f/j/b/m3;->d:Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    if-eqz v10, :cond_2

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_2
    :goto_2
    const/4 v10, 0x0

    :goto_3
    const/16 v17, 0x0

    goto/16 :goto_15

    :cond_3
    :try_start_2
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    invoke-static {v10}, Lf/h/a/f/j/b/m3;->w(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v16

    const-wide/16 v25, -0x1

    cmp-long v0, v16, v25

    if-eqz v0, :cond_4

    const-string v0, "rowid<?"

    new-array v12, v15, [Ljava/lang/String;

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v12, v5

    move-object/from16 v19, v0

    move-object/from16 v20, v12

    goto :goto_4

    :cond_4
    const/16 v19, 0x0

    const/16 v20, 0x0

    :goto_4
    const-string v17, "messages"

    const-string v0, "rowid"

    const-string v12, "type"

    const-string v15, "entry"

    filled-new-array {v0, v12, v15}, [Ljava/lang/String;

    move-result-object v18

    const/16 v21, 0x0

    const/16 v22, 0x0

    const-string v23, "rowid asc"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v16, v10

    invoke-virtual/range {v16 .. v24}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :goto_5
    :try_start_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v25

    const/4 v15, 0x1

    invoke-interface {v12, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v15, 0x2

    invoke-interface {v12, v15}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    if-nez v0, :cond_5

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v15
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    array-length v0, v4

    invoke-virtual {v15, v4, v5, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v15, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v0, Lcom/google/android/gms/measurement/internal/zzaq;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v15}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/measurement/internal/zzaq;
    :try_end_4
    .catch Lcom/google/android/gms/common/internal/safeparcel/SafeParcelReader$ParseException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    if-eqz v0, :cond_9

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto/16 :goto_b

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_0
    :try_start_6
    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v4, "Failed to load event from local database"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    goto/16 :goto_b

    :goto_6
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    throw v0

    :cond_5
    const/4 v15, 0x1

    if-ne v0, v15, :cond_6

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v15
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :try_start_8
    array-length v0, v4

    invoke-virtual {v15, v4, v5, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v15, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v0, Lcom/google/android/gms/measurement/internal/zzku;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v15}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/measurement/internal/zzku;
    :try_end_8
    .catch Lcom/google/android/gms/common/internal/safeparcel/SafeParcelReader$ParseException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_7

    :catchall_1
    move-exception v0

    goto :goto_8

    :catch_1
    :try_start_a
    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v4, "Failed to load user property from local database"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    const/4 v0, 0x0

    :goto_7
    if-eqz v0, :cond_9

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :goto_8
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    throw v0

    :cond_6
    const/4 v15, 0x2

    if-ne v0, v15, :cond_7

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v15
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_b .. :try_end_b} :catch_7
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_b .. :try_end_b} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    array-length v0, v4

    invoke-virtual {v15, v4, v5, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v15, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v0, Lcom/google/android/gms/measurement/internal/zzz;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v15}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/measurement/internal/zzz;
    :try_end_c
    .catch Lcom/google/android/gms/common/internal/safeparcel/SafeParcelReader$ParseException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_d .. :try_end_d} :catch_7
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_d .. :try_end_d} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_9

    :catchall_2
    move-exception v0

    goto :goto_a

    :catch_2
    :try_start_e
    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v4, "Failed to load conditional user property from local database"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    const/4 v0, 0x0

    :goto_9
    if-eqz v0, :cond_9

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :goto_a
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    throw v0

    :cond_7
    const/4 v4, 0x3

    if-ne v0, v4, :cond_8

    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v4, "Skipping app launch break"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_b

    :cond_8
    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v4, "Unknown record type in local database"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_9
    :goto_b
    const/16 v4, 0x64

    goto/16 :goto_5

    :cond_a
    const-string v0, "messages"

    const-string v4, "rowid <= ?"

    const/4 v15, 0x1

    new-array v5, v15, [Ljava/lang/String;

    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_f .. :try_end_f} :catch_7
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_f .. :try_end_f} :catch_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    const/16 v17, 0x0

    :try_start_10
    aput-object v15, v5, v17

    invoke-virtual {v10, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_b

    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v4, "Fewer entries removed from local database than expected"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_10
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_10 .. :try_end_10} :catch_4
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_10 .. :try_end_10} :catch_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    move-object v10, v11

    goto/16 :goto_15

    :catch_3
    move-exception v0

    goto :goto_e

    :catch_4
    move-exception v0

    goto/16 :goto_12

    :catchall_3
    move-exception v0

    move-object v1, v10

    move-object v10, v12

    goto/16 :goto_14

    :catch_5
    move-exception v0

    const/16 v17, 0x0

    goto :goto_e

    :catch_6
    const/16 v17, 0x0

    goto :goto_10

    :catch_7
    move-exception v0

    const/16 v17, 0x0

    goto :goto_12

    :catchall_4
    move-exception v0

    move-object v1, v10

    goto :goto_c

    :catch_8
    move-exception v0

    const/16 v17, 0x0

    goto :goto_d

    :catch_9
    move-exception v0

    const/16 v17, 0x0

    goto :goto_11

    :catch_a
    const/16 v17, 0x0

    goto :goto_f

    :catchall_5
    move-exception v0

    const/4 v1, 0x0

    :goto_c
    const/4 v10, 0x0

    goto :goto_14

    :catch_b
    move-exception v0

    const/16 v17, 0x0

    const/4 v10, 0x0

    :goto_d
    const/4 v12, 0x0

    :goto_e
    if-eqz v10, :cond_c

    :try_start_11
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_c
    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v4

    iget-object v4, v4, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-virtual {v4, v9, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x1

    iput-boolean v4, v8, Lf/h/a/f/j/b/m3;->d:Z
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    if-eqz v12, :cond_d

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_d
    if-eqz v10, :cond_10

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_13

    :catch_c
    const/16 v17, 0x0

    const/4 v10, 0x0

    :goto_f
    const/4 v12, 0x0

    :catch_d
    :goto_10
    int-to-long v4, v14

    :try_start_12
    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    add-int/lit8 v14, v14, 0x14

    if-eqz v12, :cond_e

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_e
    if-eqz v10, :cond_10

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_13

    :catch_e
    move-exception v0

    const/16 v17, 0x0

    const/4 v10, 0x0

    :goto_11
    const/4 v12, 0x0

    :goto_12
    :try_start_13
    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v4

    iget-object v4, v4, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-virtual {v4, v9, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x1

    iput-boolean v4, v8, Lf/h/a/f/j/b/m3;->d:Z
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    if-eqz v12, :cond_f

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_f
    if-eqz v10, :cond_10

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_10
    :goto_13
    add-int/lit8 v13, v13, 0x1

    const/16 v4, 0x64

    const/4 v5, 0x0

    const/4 v12, 0x5

    goto/16 :goto_1

    :goto_14
    if-eqz v10, :cond_11

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_11
    if-eqz v1, :cond_12

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_12
    throw v0

    :cond_13
    const/16 v17, 0x0

    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v4, "Failed to read events from database in reasonable time"

    invoke-virtual {v0, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    const/4 v10, 0x0

    :goto_15
    if-eqz v10, :cond_14

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    move v4, v0

    goto :goto_16

    :cond_14
    const/4 v4, 0x0

    :goto_16
    const/16 v5, 0x64

    if-eqz v2, :cond_15

    if-ge v4, v5, :cond_15

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_15
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v0, 0x0

    :goto_17
    if-ge v0, v8, :cond_19

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    add-int/lit8 v10, v0, 0x1

    check-cast v9, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;

    instance-of v0, v9, Lcom/google/android/gms/measurement/internal/zzaq;

    if-eqz v0, :cond_16

    :try_start_14
    check-cast v9, Lcom/google/android/gms/measurement/internal/zzaq;

    invoke-interface {v1, v9, v3}, Lf/h/a/f/j/b/i3;->c0(Lcom/google/android/gms/measurement/internal/zzaq;Lcom/google/android/gms/measurement/internal/zzn;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_14} :catch_f

    goto :goto_18

    :catch_f
    move-exception v0

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v9

    iget-object v9, v9, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v11, "Failed to send event to the service"

    invoke-virtual {v9, v11, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_18

    :cond_16
    instance-of v0, v9, Lcom/google/android/gms/measurement/internal/zzku;

    if-eqz v0, :cond_17

    :try_start_15
    check-cast v9, Lcom/google/android/gms/measurement/internal/zzku;

    invoke-interface {v1, v9, v3}, Lf/h/a/f/j/b/i3;->j0(Lcom/google/android/gms/measurement/internal/zzku;Lcom/google/android/gms/measurement/internal/zzn;)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_10

    goto :goto_18

    :catch_10
    move-exception v0

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v9

    iget-object v9, v9, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v11, "Failed to send user property to the service"

    invoke-virtual {v9, v11, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_18

    :cond_17
    instance-of v0, v9, Lcom/google/android/gms/measurement/internal/zzz;

    if-eqz v0, :cond_18

    :try_start_16
    check-cast v9, Lcom/google/android/gms/measurement/internal/zzz;

    invoke-interface {v1, v9, v3}, Lf/h/a/f/j/b/i3;->k0(Lcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_16} :catch_11

    goto :goto_18

    :catch_11
    move-exception v0

    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v9

    iget-object v9, v9, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v11, "Failed to send conditional user property to the service"

    invoke-virtual {v9, v11, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_18

    :cond_18
    invoke-virtual/range {p0 .. p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v9, "Discarding data. Unrecognized parcel type."

    invoke-virtual {v0, v9}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_18
    move v0, v10

    goto :goto_17

    :cond_19
    add-int/lit8 v6, v6, 0x1

    move v0, v4

    const/16 v4, 0x64

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_1a
    return-void
.end method

.method public final y(Lcom/google/android/gms/measurement/internal/zzz;)V
    .locals 7
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->r()Lf/h/a/f/j/b/m3;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    invoke-static {p1}, Lf/h/a/f/j/b/t9;->g0(Landroid/os/Parcelable;)[B

    move-result-object v1

    array-length v2, v1

    const/high16 v3, 0x20000

    if-le v2, v3, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->g:Lf/h/a/f/j/b/s3;

    const-string v1, "Conditional user property too long for local database. Sending directly to service"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/m3;->x(I[B)Z

    move-result v0

    move v3, v0

    :goto_0
    new-instance v4, Lcom/google/android/gms/measurement/internal/zzz;

    invoke-direct {v4, p1}, Lcom/google/android/gms/measurement/internal/zzz;-><init>(Lcom/google/android/gms/measurement/internal/zzz;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/q7;->I(Z)Lcom/google/android/gms/measurement/internal/zzn;

    move-result-object v5

    new-instance v0, Lf/h/a/f/j/b/g8;

    move-object v1, v0

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lf/h/a/f/j/b/g8;-><init>(Lf/h/a/f/j/b/q7;ZLcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;Lcom/google/android/gms/measurement/internal/zzz;)V

    invoke-virtual {p0, v0}, Lf/h/a/f/j/b/q7;->z(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final z(Ljava/lang/Runnable;)V
    .locals 5
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {p0}, Lf/h/a/f/j/b/q7;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/q7;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v0, "Discarding data. Max runnable queue size reached"

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/j/b/q7;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/h/a/f/j/b/q7;->i:Lf/h/a/f/j/b/i;

    const-wide/32 v0, 0xea60

    invoke-virtual {p1, v0, v1}, Lf/h/a/f/j/b/i;->b(J)V

    invoke-virtual {p0}, Lf/h/a/f/j/b/q7;->C()V

    return-void
.end method
