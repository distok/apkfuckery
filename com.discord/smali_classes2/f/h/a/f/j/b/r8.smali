.class public final synthetic Lf/h/a/f/j/b/r8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/j/b/p8;

.field public final e:Lf/h/a/f/j/b/q3;

.field public final f:Landroid/app/job/JobParameters;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/p8;Lf/h/a/f/j/b/q3;Landroid/app/job/JobParameters;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/b/r8;->d:Lf/h/a/f/j/b/p8;

    iput-object p2, p0, Lf/h/a/f/j/b/r8;->e:Lf/h/a/f/j/b/q3;

    iput-object p3, p0, Lf/h/a/f/j/b/r8;->f:Landroid/app/job/JobParameters;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/j/b/r8;->d:Lf/h/a/f/j/b/p8;

    iget-object v1, p0, Lf/h/a/f/j/b/r8;->e:Lf/h/a/f/j/b/q3;

    iget-object v2, p0, Lf/h/a/f/j/b/r8;->f:Landroid/app/job/JobParameters;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v3, "AppMeasurementJobService processed last upload request."

    invoke-virtual {v1, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lf/h/a/f/j/b/p8;->a:Landroid/content/Context;

    check-cast v0, Lf/h/a/f/j/b/t8;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lf/h/a/f/j/b/t8;->a(Landroid/app/job/JobParameters;Z)V

    return-void
.end method
