.class public final Lf/h/a/f/j/b/b5;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/measurement/internal/zzz;

.field public final synthetic e:Lcom/google/android/gms/measurement/internal/zzn;

.field public final synthetic f:Lf/h/a/f/j/b/z4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/z4;Lcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/b5;->f:Lf/h/a/f/j/b/z4;

    iput-object p2, p0, Lf/h/a/f/j/b/b5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    iput-object p3, p0, Lf/h/a/f/j/b/b5;->e:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/j/b/b5;->f:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->R()V

    iget-object v0, p0, Lf/h/a/f/j/b/b5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/zzz;->f:Lcom/google/android/gms/measurement/internal/zzku;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/zzku;->M0()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/b5;->f:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    iget-object v1, p0, Lf/h/a/f/j/b/b5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    iget-object v2, p0, Lf/h/a/f/j/b/b5;->e:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/k9;->F(Lcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/b5;->f:Lf/h/a/f/j/b/z4;

    iget-object v0, v0, Lf/h/a/f/j/b/z4;->a:Lf/h/a/f/j/b/k9;

    iget-object v1, p0, Lf/h/a/f/j/b/b5;->d:Lcom/google/android/gms/measurement/internal/zzz;

    iget-object v2, p0, Lf/h/a/f/j/b/b5;->e:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/k9;->q(Lcom/google/android/gms/measurement/internal/zzz;Lcom/google/android/gms/measurement/internal/zzn;)V

    return-void
.end method
