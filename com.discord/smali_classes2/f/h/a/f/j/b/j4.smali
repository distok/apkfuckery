.class public final Lf/h/a/f/j/b/j4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Z

.field public c:Ljava/lang/String;

.field public final synthetic d:Lf/h/a/f/j/b/d4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/d4;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/j4;->d:Lf/h/a/f/j/b/d4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    iput-object p2, p0, Lf/h/a/f/j/b/j4;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/j/b/j4;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/f/j/b/j4;->b:Z

    iget-object v0, p0, Lf/h/a/f/j/b/j4;->d:Lf/h/a/f/j/b/d4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/j4;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/j/b/j4;->c:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lf/h/a/f/j/b/j4;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/j4;->d:Lf/h/a/f/j/b/d4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/j4;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iput-object p1, p0, Lf/h/a/f/j/b/j4;->c:Ljava/lang/String;

    return-void
.end method
