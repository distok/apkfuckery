.class public abstract Lf/h/a/f/j/b/i;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# static fields
.field public static volatile d:Landroid/os/Handler;


# instance fields
.field public final a:Lf/h/a/f/j/b/t5;

.field public final b:Ljava/lang/Runnable;

.field public volatile c:J


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/t5;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "null reference"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/f/j/b/i;->a:Lf/h/a/f/j/b/t5;

    new-instance v0, Lf/h/a/f/j/b/k;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/j/b/k;-><init>(Lf/h/a/f/j/b/i;Lf/h/a/f/j/b/t5;)V

    iput-object v0, p0, Lf/h/a/f/j/b/i;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final b(J)V
    .locals 3

    invoke-virtual {p0}, Lf/h/a/f/j/b/i;->c()V

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/i;->a:Lf/h/a/f/j/b/t5;

    invoke-interface {v0}, Lf/h/a/f/j/b/t5;->i()Lf/h/a/f/f/n/c;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/j/b/i;->c:J

    invoke-virtual {p0}, Lf/h/a/f/j/b/i;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/i;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/j/b/i;->a:Lf/h/a/f/j/b/t5;

    invoke-interface {v0}, Lf/h/a/f/j/b/t5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "Failed to schedule delayed post. time"

    invoke-virtual {v0, p2, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lf/h/a/f/j/b/i;->c:J

    invoke-virtual {p0}, Lf/h/a/f/j/b/i;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/i;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final d()Landroid/os/Handler;
    .locals 3

    sget-object v0, Lf/h/a/f/j/b/i;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    sget-object v0, Lf/h/a/f/j/b/i;->d:Landroid/os/Handler;

    return-object v0

    :cond_0
    const-class v0, Lf/h/a/f/j/b/i;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/a/f/j/b/i;->d:Landroid/os/Handler;

    if-nez v1, :cond_1

    new-instance v1, Lf/h/a/f/i/j/bc;

    iget-object v2, p0, Lf/h/a/f/j/b/i;->a:Lf/h/a/f/j/b/t5;

    invoke-interface {v2}, Lf/h/a/f/j/b/t5;->j()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Lf/h/a/f/i/j/bc;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lf/h/a/f/j/b/i;->d:Landroid/os/Handler;

    :cond_1
    sget-object v1, Lf/h/a/f/j/b/i;->d:Landroid/os/Handler;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
