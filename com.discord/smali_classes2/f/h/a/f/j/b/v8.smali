.class public final Lf/h/a/f/j/b/v8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:J

.field public final synthetic e:Lf/h/a/f/j/b/w8;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/w8;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/v8;->e:Lf/h/a/f/j/b/w8;

    iput-wide p2, p0, Lf/h/a/f/j/b/v8;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lf/h/a/f/j/b/v8;->e:Lf/h/a/f/j/b/w8;

    iget-wide v1, p0, Lf/h/a/f/j/b/v8;->d:J

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/w8;->x()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "Activity resumed, time"

    invoke-virtual {v3, v5, v4}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v3, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v4, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v3, v4}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v3}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/f/j/b/d4;->w:Lf/h/a/f/j/b/f4;

    invoke-virtual {v3}, Lf/h/a/f/j/b/f4;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, v0, Lf/h/a/f/j/b/w8;->e:Lf/h/a/f/j/b/d9;

    iget-object v4, v3, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v4}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v4, v3, Lf/h/a/f/j/b/d9;->c:Lf/h/a/f/j/b/i;

    invoke-virtual {v4}, Lf/h/a/f/j/b/i;->c()V

    iput-wide v1, v3, Lf/h/a/f/j/b/d9;->a:J

    iput-wide v1, v3, Lf/h/a/f/j/b/d9;->b:J

    :cond_1
    iget-object v1, v0, Lf/h/a/f/j/b/w8;->f:Lf/h/a/f/j/b/x8;

    invoke-virtual {v1}, Lf/h/a/f/j/b/x8;->a()V

    goto :goto_0

    :cond_2
    iget-object v3, v0, Lf/h/a/f/j/b/w8;->f:Lf/h/a/f/j/b/x8;

    invoke-virtual {v3}, Lf/h/a/f/j/b/x8;->a()V

    iget-object v3, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v3}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lf/h/a/f/j/b/w8;->e:Lf/h/a/f/j/b/d9;

    iget-object v4, v3, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v4}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v4, v3, Lf/h/a/f/j/b/d9;->c:Lf/h/a/f/j/b/i;

    invoke-virtual {v4}, Lf/h/a/f/j/b/i;->c()V

    iput-wide v1, v3, Lf/h/a/f/j/b/d9;->a:J

    iput-wide v1, v3, Lf/h/a/f/j/b/d9;->b:J

    :cond_3
    :goto_0
    iget-object v0, v0, Lf/h/a/f/j/b/w8;->d:Lf/h/a/f/j/b/f9;

    iget-object v1, v0, Lf/h/a/f/j/b/f9;->a:Lf/h/a/f/j/b/w8;

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v1, v0, Lf/h/a/f/j/b/f9;->a:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lf/h/a/f/j/b/f9;->a:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_4

    iget-object v1, v0, Lf/h/a/f/j/b/f9;->a:Lf/h/a/f/j/b/w8;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->w:Lf/h/a/f/j/b/f4;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/f4;->a(Z)V

    :cond_4
    iget-object v1, v0, Lf/h/a/f/j/b/f9;->a:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4, v2}, Lf/h/a/f/j/b/f9;->b(JZ)V

    :cond_5
    return-void
.end method
