.class public final Lf/h/a/f/j/b/n3;
.super Lf/h/a/f/j/b/a5;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Ljava/lang/String;

.field public g:J

.field public h:J

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:I

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/u4;J)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/j/b/a5;-><init>(Lf/h/a/f/j/b/u4;)V

    iput-wide p2, p0, Lf/h/a/f/j/b/n3;->h:J

    return-void
.end method


# virtual methods
.method public final v()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final w()V
    .locals 11

    const-string v0, "unknown"

    const-string v1, "Unknown"

    const-string v2, "Unknown"

    iget-object v3, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v4, v4, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    const/high16 v6, -0x80000000

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v7, "PackageManager is null, app identity information might be inaccurate. appId"

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    :cond_0
    :try_start_0
    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v7

    iget-object v7, v7, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v8, "Error retrieving app installer package name. appId"

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "manual_install"

    goto :goto_1

    :cond_1
    const-string v7, "com.android.vending"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v0, ""

    :cond_2
    :goto_1
    :try_start_1
    iget-object v7, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v7, v7, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, v8}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_3
    iget-object v1, v7, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iget v2, v7, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move v6, v2

    goto :goto_2

    :catch_1
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v7

    iget-object v7, v7, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v8, "Error retrieving package info. appId, appName"

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v8, v9, v2}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_4
    :goto_2
    iput-object v3, p0, Lf/h/a/f/j/b/n3;->c:Ljava/lang/String;

    iput-object v0, p0, Lf/h/a/f/j/b/n3;->f:Ljava/lang/String;

    iput-object v1, p0, Lf/h/a/f/j/b/n3;->d:Ljava/lang/String;

    iput v6, p0, Lf/h/a/f/j/b/n3;->e:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lf/h/a/f/j/b/n3;->g:J

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    sget-object v1, Lf/h/a/f/f/h/i/h;->d:Ljava/lang/Object;

    const-string v1, "Context must not be null."

    invoke-static {v0, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/a/f/f/h/i/h;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    sget-object v2, Lf/h/a/f/f/h/i/h;->e:Lf/h/a/f/f/h/i/h;

    if-nez v2, :cond_5

    new-instance v2, Lf/h/a/f/f/h/i/h;

    invoke-direct {v2, v0}, Lf/h/a/f/f/h/i/h;-><init>(Landroid/content/Context;)V

    sput-object v2, Lf/h/a/f/f/h/i/h;->e:Lf/h/a/f/f/h/i/h;

    :cond_5
    sget-object v0, Lf/h/a/f/f/h/i/h;->e:Lf/h/a/f/f/h/i/h;

    iget-object v0, v0, Lf/h/a/f/f/h/i/h;->b:Lcom/google/android/gms/common/api/Status;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->M0()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_3
    iget-object v6, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v6, v6, Lf/h/a/f/j/b/u4;->b:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, "am"

    iget-object v7, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v7, v7, Lf/h/a/f/j/b/u4;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const/4 v6, 0x1

    goto :goto_4

    :cond_7
    const/4 v6, 0x0

    :goto_4
    or-int/2addr v2, v6

    if-nez v2, :cond_9

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->g:Lf/h/a/f/j/b/s3;

    const-string v7, "GoogleService failed to initialize (no status)"

    invoke-virtual {v0, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v7

    iget-object v7, v7, Lf/h/a/f/j/b/q3;->g:Lf/h/a/f/j/b/s3;

    const-string v8, "GoogleService failed to initialize, status"

    iget v9, v0, Lcom/google/android/gms/common/api/Status;->e:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v0, v0, Lcom/google/android/gms/common/api/Status;->f:Ljava/lang/String;

    invoke-virtual {v7, v8, v9, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_9
    :goto_5
    if-eqz v2, :cond_a

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->e()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    goto/16 :goto_6

    :pswitch_0
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement disabled due to denied storage consent"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_7

    :pswitch_1
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement disabled via the global data collection setting"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_2
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement deactivated via resources. This method is being deprecated. Please refer to https://firebase.google.com/support/guides/disable-analytics"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_3
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement disabled via the init parameters"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_4
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement disabled via the manifest"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_5
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement disabled by setAnalyticsCollectionEnabled(false)"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_6
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement deactivated via the init parameters"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_7
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement deactivated via the manifest"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :pswitch_8
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement collection enabled"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_7

    :goto_6
    iget-object v2, v2, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v7, "App measurement disabled"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->g:Lf/h/a/f/j/b/s3;

    const-string v7, "Invalid scion state in identity"

    invoke-virtual {v2, v7}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_7
    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_8

    :cond_a
    const/4 v0, 0x0

    :goto_8
    const-string v2, ""

    iput-object v2, p0, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    const-string v2, ""

    iput-object v2, p0, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    const-string v2, ""

    iput-object v2, p0, Lf/h/a/f/j/b/n3;->m:Ljava/lang/String;

    if-eqz v6, :cond_b

    iget-object v2, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->b:Ljava/lang/String;

    iput-object v2, p0, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    :cond_b
    const/4 v2, 0x0

    :try_start_3
    sget-object v6, Lf/h/a/f/i/j/ib;->e:Lf/h/a/f/i/j/ib;

    invoke-virtual {v6}, Lf/h/a/f/i/j/ib;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/a/f/i/j/lb;

    invoke-interface {v6}, Lf/h/a/f/i/j/lb;->a()Z

    move-result v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v6, v6, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v7, Lf/h/a/f/j/b/p;->C0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v6, v7}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v6, v6, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v7, "google_app_id"

    invoke-static {v6, v7}, Lf/h/a/f/f/n/g;->I0(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_9

    :catch_2
    move-exception v0

    goto/16 :goto_11

    :cond_c
    const-string v6, "getGoogleAppId"

    invoke-static {v6}, Lf/h/a/f/f/h/i/h;->a(Ljava/lang/String;)Lf/h/a/f/f/h/i/h;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/f/h/i/h;->a:Ljava/lang/String;

    :goto_9
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_d

    const-string v7, ""

    goto :goto_a

    :cond_d
    move-object v7, v6

    :goto_a
    iput-object v7, p0, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-static {}, Lf/h/a/f/i/j/da;->b()Z

    move-result v7

    if-eqz v7, :cond_12

    iget-object v7, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v7, v7, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v8, Lf/h/a/f/j/b/p;->j0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v7, v8}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v7

    if-eqz v7, :cond_12

    iget-object v7, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v7, v7, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v8, "null reference"

    invoke-static {v7, v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/gms/common/R$a;->common_google_play_services_unknown_issue:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "ga_app_id"

    const-string v10, "string"

    invoke-virtual {v7, v9, v10, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_e

    move-object v9, v2

    goto :goto_b

    :cond_e
    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    :goto_b
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_f

    const-string v10, ""

    goto :goto_c

    :cond_f
    move-object v10, v9

    :goto_c
    iput-object v10, p0, Lf/h/a/f/j/b/n3;->m:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_14

    :cond_10
    const-string v6, "admob_app_id"

    const-string v9, "string"

    invoke-virtual {v7, v6, v9, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_11

    move-object v6, v2

    goto :goto_d

    :cond_11
    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_d
    iput-object v6, p0, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    goto :goto_f

    :cond_12
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_14

    iget-object v6, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v6, v6, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v7, "null reference"

    invoke-static {v6, v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/gms/common/R$a;->common_google_play_services_unknown_issue:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "admob_app_id"

    const-string v9, "string"

    invoke-virtual {v6, v8, v9, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_13

    move-object v6, v2

    goto :goto_e

    :cond_13
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_e
    iput-object v6, p0, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    :cond_14
    :goto_f
    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v6, "App measurement enabled for app package, google app id"

    iget-object v7, p0, Lf/h/a/f/j/b/n3;->c:Ljava/lang/String;

    iget-object v8, p0, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_15

    iget-object v8, p0, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    goto :goto_10

    :cond_15
    iget-object v8, p0, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    :goto_10
    invoke-virtual {v0, v6, v7, v8}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_12

    :goto_11
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v7, "Fetching Google App Id failed with exception. appId"

    invoke-static {v3}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v6, v7, v3, v0}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_16
    :goto_12
    iput-object v2, p0, Lf/h/a/f/j/b/n3;->i:Ljava/util/List;

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    const-string v3, "analytics.safelisted_events"

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/a/f/j/b/c;->D()Landroid/os/Bundle;

    move-result-object v6

    if-nez v6, :cond_17

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v6, "Failed to load metadata: Metadata bundle is null"

    invoke-virtual {v3, v6}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_13

    :cond_17
    invoke-virtual {v6, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_18

    :goto_13
    move-object v3, v2

    goto :goto_14

    :cond_18
    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_14
    if-nez v3, :cond_19

    goto :goto_15

    :cond_19
    :try_start_4
    iget-object v6, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v6, v6, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1a

    goto :goto_15

    :cond_1a
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2
    :try_end_4
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_15

    :catch_3
    move-exception v3

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v6, "Failed to load string array from metadata: resource not found"

    invoke-virtual {v0, v6, v3}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_15
    if-eqz v2, :cond_1d

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1b

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string v1, "Safelisted event list is empty. Ignoring"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_16
    const/4 v1, 0x0

    goto :goto_17

    :cond_1b
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v6

    const-string v7, "safelisted event"

    invoke-virtual {v6, v7, v3}, Lf/h/a/f/j/b/t9;->m0(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1c

    goto :goto_16

    :cond_1d
    :goto_17
    if-eqz v1, :cond_1e

    iput-object v2, p0, Lf/h/a/f/j/b/n3;->i:Ljava/util/List;

    :cond_1e
    if-eqz v4, :cond_1f

    iget-object v0, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->L(Landroid/content/Context;)Z

    move-result v0

    iput v0, p0, Lf/h/a/f/j/b/n3;->j:I

    return-void

    :cond_1f
    iput v5, p0, Lf/h/a/f/j/b/n3;->j:I

    return-void

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
