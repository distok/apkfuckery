.class public final Lf/h/a/f/j/b/l4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final d:Ljava/lang/String;

.field public final synthetic e:Lf/h/a/f/j/b/m4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/m4;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/a/f/j/b/l4;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    if-nez p2, :cond_0

    iget-object p1, p0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    iget-object p1, p1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string p2, "Install Referrer connection returned with null binder"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_0
    sget p1, Lf/h/a/f/i/j/e3;->a:I

    const-string p1, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService"

    invoke-interface {p2, p1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object p1

    instance-of v0, p1, Lf/h/a/f/i/j/e2;

    if-eqz v0, :cond_1

    check-cast p1, Lf/h/a/f/i/j/e2;

    goto :goto_0

    :cond_1
    new-instance p1, Lf/h/a/f/i/j/d3;

    invoke-direct {p1, p2}, Lf/h/a/f/i/j/d3;-><init>(Landroid/os/IBinder;)V

    :goto_0
    if-nez p1, :cond_2

    iget-object p1, p0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    iget-object p1, p1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string p2, "Install Referrer Service implementation was not found"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_2
    iget-object p2, p0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    iget-object p2, p2, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v0, "Install Referrer Service connected"

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object p2, p0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    iget-object p2, p2, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object p2

    new-instance v0, Lf/h/a/f/j/b/o4;

    invoke-direct {v0, p0, p1, p0}, Lf/h/a/f/j/b/o4;-><init>(Lf/h/a/f/j/b/l4;Lf/h/a/f/i/j/e2;Landroid/content/ServiceConnection;)V

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    iget-object p2, p2, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v0, "Exception occurred while calling Install Referrer API"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object p1, p0, Lf/h/a/f/j/b/l4;->e:Lf/h/a/f/j/b/m4;

    iget-object p1, p1, Lf/h/a/f/j/b/m4;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v0, "Install Referrer Service disconnected"

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void
.end method
