.class public final Lf/h/a/f/j/b/i6;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:J

.field public final synthetic e:Lf/h/a/f/j/b/c6;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/c6;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/i6;->e:Lf/h/a/f/j/b/c6;

    iput-wide p2, p0, Lf/h/a/f/j/b/i6;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/j/b/i6;->e:Lf/h/a/f/j/b/c6;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->p:Lf/h/a/f/j/b/h4;

    iget-wide v1, p0, Lf/h/a/f/j/b/i6;->d:J

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/j/b/h4;->b(J)V

    iget-object v0, p0, Lf/h/a/f/j/b/i6;->e:Lf/h/a/f/j/b/c6;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    iget-wide v1, p0, Lf/h/a/f/j/b/i6;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "Minimum session duration set"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
