.class public final Lf/h/a/f/j/b/u7;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/measurement/internal/zzn;

.field public final synthetic e:Lf/h/a/f/i/j/fc;

.field public final synthetic f:Lf/h/a/f/j/b/q7;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/q7;Lcom/google/android/gms/measurement/internal/zzn;Lf/h/a/f/i/j/fc;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    iput-object p2, p0, Lf/h/a/f/j/b/u7;->d:Lcom/google/android/gms/measurement/internal/zzn;

    iput-object p3, p0, Lf/h/a/f/j/b/u7;->e:Lf/h/a/f/i/j/fc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const-string v0, "Failed to get app instance id"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    iget-object v2, v2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v3, Lf/h/a/f/j/b/p;->H0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/d4;->y()Lf/h/a/f/j/b/d;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/d;->k()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string v3, "Analytics storage consent denied; will not get app instance id"

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/z1;->n()Lf/h/a/f/j/b/c6;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/c6;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->l:Lf/h/a/f/j/b/j4;

    invoke-virtual {v2, v1}, Lf/h/a/f/j/b/j4;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->e:Lf/h/a/f/i/j/fc;

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    iget-object v3, v2, Lf/h/a/f/j/b/q7;->d:Lf/h/a/f/j/b/i3;

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-virtual {v2, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->e:Lf/h/a/f/i/j/fc;

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void

    :cond_1
    :try_start_2
    iget-object v2, p0, Lf/h/a/f/j/b/u7;->d:Lcom/google/android/gms/measurement/internal/zzn;

    invoke-interface {v3, v2}, Lf/h/a/f/j/b/i3;->w(Lcom/google/android/gms/measurement/internal/zzn;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/z1;->n()Lf/h/a/f/j/b/c6;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/c6;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->l:Lf/h/a/f/j/b/j4;

    invoke-virtual {v2, v1}, Lf/h/a/f/j/b/j4;->b(Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/q7;->F()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->e:Lf/h/a/f/i/j/fc;

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_3
    iget-object v3, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v3}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    invoke-virtual {v3, v0, v2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    iget-object v2, p0, Lf/h/a/f/j/b/u7;->e:Lf/h/a/f/i/j/fc;

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void

    :goto_0
    iget-object v2, p0, Lf/h/a/f/j/b/u7;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v2

    iget-object v3, p0, Lf/h/a/f/j/b/u7;->e:Lf/h/a/f/i/j/fc;

    invoke-virtual {v2, v3, v1}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    throw v0
.end method
