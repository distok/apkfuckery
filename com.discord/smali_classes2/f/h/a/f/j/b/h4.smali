.class public final Lf/h/a/f/j/b/h4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public c:Z

.field public d:J

.field public final synthetic e:Lf/h/a/f/j/b/d4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/d4;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/h4;->e:Lf/h/a/f/j/b/d4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    iput-object p2, p0, Lf/h/a/f/j/b/h4;->a:Ljava/lang/String;

    iput-wide p3, p0, Lf/h/a/f/j/b/h4;->b:J

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/j/b/h4;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/f/j/b/h4;->c:Z

    iget-object v0, p0, Lf/h/a/f/j/b/h4;->e:Lf/h/a/f/j/b/d4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/h4;->a:Ljava/lang/String;

    iget-wide v2, p0, Lf/h/a/f/j/b/h4;->b:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/j/b/h4;->d:J

    :cond_0
    iget-wide v0, p0, Lf/h/a/f/j/b/h4;->d:J

    return-wide v0
.end method

.method public final b(J)V
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/h4;->e:Lf/h/a/f/j/b/d4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/j/b/h4;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iput-wide p1, p0, Lf/h/a/f/j/b/h4;->d:J

    return-void
.end method
