.class public final Lf/h/a/f/j/b/k8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lf/h/a/f/f/k/b$a;
.implements Lf/h/a/f/f/k/b$b;


# instance fields
.field public volatile d:Z

.field public volatile e:Lf/h/a/f/j/b/r3;

.field public final synthetic f:Lf/h/a/f/j/b/q7;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/q7;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(I)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string p1, "MeasurementServiceConnection.onConnectionSuspended"

    invoke-static {p1}, Lf/g/j/k/a;->m(Ljava/lang/String;)V

    iget-object p1, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v0, "Service connection suspended"

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object p1, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object p1

    new-instance v0, Lf/h/a/f/j/b/o8;

    invoke-direct {v0, p0}, Lf/h/a/f/j/b/o8;-><init>(Lf/h/a/f/j/b/k8;)V

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final g(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "MeasurementServiceConnection.onConnectionFailed"

    invoke-static {v0}, Lf/g/j/k/a;->m(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->i:Lf/h/a/f/j/b/q3;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lf/h/a/f/j/b/r5;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->i:Lf/h/a/f/j/b/q3;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v1, "Service connection failed"

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    monitor-enter p0

    const/4 p1, 0x0

    :try_start_0
    iput-boolean p1, p0, Lf/h/a/f/j/b/k8;->d:Z

    iput-object v2, p0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object p1

    new-instance v0, Lf/h/a/f/j/b/n8;

    invoke-direct {v0, p0}, Lf/h/a/f/j/b/n8;-><init>(Lf/h/a/f/j/b/k8;)V

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final h(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string p1, "MeasurementServiceConnection.onConnected"

    invoke-static {p1}, Lf/g/j/k/a;->m(Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget-object p1, p0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    invoke-virtual {p1}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object p1

    check-cast p1, Lf/h/a/f/j/b/i3;

    iget-object v0, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/l8;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/j/b/l8;-><init>(Lf/h/a/f/j/b/k8;Lf/h/a/f/j/b/i3;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    const/4 p1, 0x0

    :try_start_1
    iput-object p1, p0, Lf/h/a/f/j/b/k8;->e:Lf/h/a/f/j/b/r3;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/f/j/b/k8;->d:Z

    :goto_0
    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string p1, "MeasurementServiceConnection.onServiceConnected"

    invoke-static {p1}, Lf/g/j/k/a;->m(Ljava/lang/String;)V

    monitor-enter p0

    const/4 p1, 0x0

    if-nez p2, :cond_0

    :try_start_0
    iput-boolean p1, p0, Lf/h/a/f/j/b/k8;->d:Z

    iget-object p1, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string p2, "Service connected with null binder"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_3

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {p2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.measurement.internal.IMeasurementService"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "com.google.android.gms.measurement.internal.IMeasurementService"

    invoke-interface {p2, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    instance-of v2, v1, Lf/h/a/f/j/b/i3;

    if-eqz v2, :cond_1

    check-cast v1, Lf/h/a/f/j/b/i3;

    goto :goto_0

    :cond_1
    new-instance v1, Lf/h/a/f/j/b/k3;

    invoke-direct {v1, p2}, Lf/h/a/f/j/b/k3;-><init>(Landroid/os/IBinder;)V

    :goto_0
    move-object v0, v1

    iget-object p2, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v1, "Bound to IMeasurementService interface"

    invoke-virtual {p2, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object p2, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Got binder with a wrong descriptor"

    invoke-virtual {p2, v2, v1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    :try_start_2
    iget-object p2, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v1, "Service connect failed to get IMeasurementService"

    invoke-virtual {p2, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :goto_1
    if-nez v0, :cond_3

    iput-boolean p1, p0, Lf/h/a/f/j/b/k8;->d:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {}, Lf/h/a/f/f/m/a;->b()Lf/h/a/f/f/m/a;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    iget-object v0, p2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    iget-object p2, p2, Lf/h/a/f/j/b/q7;->c:Lf/h/a/f/j/b/k8;

    invoke-virtual {p1, v0, p2}, Lf/h/a/f/f/m/a;->c(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :cond_3
    :try_start_4
    iget-object p1, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object p1

    new-instance p2, Lf/h/a/f/j/b/j8;

    invoke-direct {p2, p0, v0}, Lf/h/a/f/j/b/j8;-><init>(Lf/h/a/f/j/b/k8;Lf/h/a/f/j/b/i3;)V

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    :catch_1
    :goto_2
    monitor-exit p0

    return-void

    :goto_3
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw p1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "MeasurementServiceConnection.onServiceDisconnected"

    invoke-static {v0}, Lf/g/j/k/a;->m(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v1, "Service disconnected"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/a/f/j/b/k8;->f:Lf/h/a/f/j/b/q7;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/m8;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/j/b/m8;-><init>(Lf/h/a/f/j/b/k8;Landroid/content/ComponentName;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method
