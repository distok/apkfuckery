.class public final Lf/h/a/f/j/b/p6;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Landroid/os/Bundle;

.field public final synthetic e:Lf/h/a/f/j/b/c6;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/c6;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/p6;->e:Lf/h/a/f/j/b/c6;

    iput-object p2, p0, Lf/h/a/f/j/b/p6;->d:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 22

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/f/j/b/p6;->e:Lf/h/a/f/j/b/c6;

    iget-object v2, v0, Lf/h/a/f/j/b/p6;->d:Landroid/os/Bundle;

    const-string v3, "creation_timestamp"

    const-string v4, "origin"

    const-string v5, "app_id"

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    const-string v6, "null reference"

    invoke-static {v2, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v6, "name"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    iget-object v7, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v7}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    const-string v2, "Conditional property not cleared since app measurement is disabled"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_0
    new-instance v12, Lcom/google/android/gms/measurement/internal/zzku;

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, v12

    invoke-direct/range {v6 .. v11}, Lcom/google/android/gms/measurement/internal/zzku;-><init>(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v13

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v6, "expired_event_name"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v6, "expired_event_params"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v16

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    const/16 v20, 0x1

    invoke-static {}, Lf/h/a/f/i/j/x7;->b()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v6, v6, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v7, Lf/h/a/f/j/b/p;->L0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v6, v7}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    const/16 v21, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    const/16 v21, 0x0

    :goto_0
    invoke-virtual/range {v13 .. v21}, Lf/h/a/f/j/b/t9;->B(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;JZZ)Lcom/google/android/gms/measurement/internal/zzaq;

    move-result-object v17
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v15, Lcom/google/android/gms/measurement/internal/zzz;

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    const-string v3, "active"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    const-string v3, "trigger_event_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const-string v3, "trigger_timeout"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v13

    const/16 v16, 0x0

    const-string v3, "time_to_live"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    move-object v3, v15

    move-object v4, v5

    move-object v5, v6

    move-object v6, v12

    move-wide v12, v13

    move-object/from16 v14, v16

    move-object v2, v15

    move-wide/from16 v15, v18

    invoke-direct/range {v3 .. v17}, Lcom/google/android/gms/measurement/internal/zzz;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzku;JZLjava/lang/String;Lcom/google/android/gms/measurement/internal/zzaq;JLcom/google/android/gms/measurement/internal/zzaq;JLcom/google/android/gms/measurement/internal/zzaq;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->p()Lf/h/a/f/j/b/q7;

    move-result-object v1

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/q7;->y(Lcom/google/android/gms/measurement/internal/zzz;)V

    :catch_0
    :goto_1
    return-void
.end method
