.class public final Lf/h/a/f/j/b/r6;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicReference;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Z

.field public final synthetic h:Lf/h/a/f/j/b/c6;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/c6;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/r6;->h:Lf/h/a/f/j/b/c6;

    iput-object p2, p0, Lf/h/a/f/j/b/r6;->d:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p3, p0, Lf/h/a/f/j/b/r6;->e:Ljava/lang/String;

    iput-object p4, p0, Lf/h/a/f/j/b/r6;->f:Ljava/lang/String;

    iput-boolean p5, p0, Lf/h/a/f/j/b/r6;->g:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    iget-object v0, p0, Lf/h/a/f/j/b/r6;->h:Lf/h/a/f/j/b/c6;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->x()Lf/h/a/f/j/b/q7;

    move-result-object v0

    iget-object v3, p0, Lf/h/a/f/j/b/r6;->d:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v5, p0, Lf/h/a/f/j/b/r6;->e:Ljava/lang/String;

    iget-object v6, p0, Lf/h/a/f/j/b/r6;->f:Ljava/lang/String;

    iget-boolean v7, p0, Lf/h/a/f/j/b/r6;->g:Z

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/a5;->t()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/q7;->I(Z)Lcom/google/android/gms/measurement/internal/zzn;

    move-result-object v8

    new-instance v9, Lf/h/a/f/j/b/h8;

    const/4 v4, 0x0

    move-object v1, v9

    move-object v2, v0

    invoke-direct/range {v1 .. v8}, Lf/h/a/f/j/b/h8;-><init>(Lf/h/a/f/j/b/q7;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/measurement/internal/zzn;)V

    invoke-virtual {v0, v9}, Lf/h/a/f/j/b/q7;->z(Ljava/lang/Runnable;)V

    return-void
.end method
