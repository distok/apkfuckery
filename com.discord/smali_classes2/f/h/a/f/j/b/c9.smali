.class public final Lf/h/a/f/j/b/c9;
.super Lf/h/a/f/j/b/i;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public final synthetic e:Lf/h/a/f/j/b/d9;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/d9;Lf/h/a/f/j/b/t5;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/c9;->e:Lf/h/a/f/j/b/d9;

    invoke-direct {p0, p2}, Lf/h/a/f/j/b/i;-><init>(Lf/h/a/f/j/b/t5;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/j/b/c9;->e:Lf/h/a/f/j/b/d9;

    iget-object v1, v0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    iget-object v1, v0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Lf/h/a/f/j/b/d9;->a(ZZJ)Z

    iget-object v1, v0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->m()Lf/h/a/f/j/b/a;

    move-result-object v1

    iget-object v0, v0, Lf/h/a/f/j/b/d9;->d:Lf/h/a/f/j/b/w8;

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/j/b/a;->t(J)V

    return-void
.end method
