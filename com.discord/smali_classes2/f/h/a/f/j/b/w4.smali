.class public final Lf/h/a/f/j/b/w4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/f/j/b/y5;

.field public final synthetic e:Lf/h/a/f/j/b/u4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/u4;Lf/h/a/f/j/b/y5;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/w4;->e:Lf/h/a/f/j/b/u4;

    iput-object p2, p0, Lf/h/a/f/j/b/w4;->d:Lf/h/a/f/j/b/y5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    iget-object v0, p0, Lf/h/a/f/j/b/w4;->e:Lf/h/a/f/j/b/u4;

    iget-object v1, p0, Lf/h/a/f/j/b/w4;->d:Lf/h/a/f/j/b/y5;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/r4;->b()V

    new-instance v2, Lf/h/a/f/j/b/j;

    invoke-direct {v2, v0}, Lf/h/a/f/j/b/j;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v2}, Lf/h/a/f/j/b/r5;->p()V

    iput-object v2, v0, Lf/h/a/f/j/b/u4;->u:Lf/h/a/f/j/b/j;

    new-instance v2, Lf/h/a/f/j/b/n3;

    iget-wide v3, v1, Lf/h/a/f/j/b/y5;->f:J

    invoke-direct {v2, v0, v3, v4}, Lf/h/a/f/j/b/n3;-><init>(Lf/h/a/f/j/b/u4;J)V

    invoke-virtual {v2}, Lf/h/a/f/j/b/a5;->u()V

    iput-object v2, v0, Lf/h/a/f/j/b/u4;->v:Lf/h/a/f/j/b/n3;

    new-instance v1, Lf/h/a/f/j/b/m3;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/m3;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->u()V

    iput-object v1, v0, Lf/h/a/f/j/b/u4;->s:Lf/h/a/f/j/b/m3;

    new-instance v1, Lf/h/a/f/j/b/q7;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/q7;-><init>(Lf/h/a/f/j/b/u4;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->u()V

    iput-object v1, v0, Lf/h/a/f/j/b/u4;->t:Lf/h/a/f/j/b/q7;

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->l:Lf/h/a/f/j/b/t9;

    invoke-virtual {v1}, Lf/h/a/f/j/b/r5;->q()V

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->h:Lf/h/a/f/j/b/d4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/r5;->q()V

    new-instance v1, Lf/h/a/f/j/b/m4;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/m4;-><init>(Lf/h/a/f/j/b/u4;)V

    iput-object v1, v0, Lf/h/a/f/j/b/u4;->w:Lf/h/a/f/j/b/m4;

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->v:Lf/h/a/f/j/b/n3;

    iget-boolean v3, v1, Lf/h/a/f/j/b/a5;->b:Z

    if-nez v3, :cond_22

    invoke-virtual {v1}, Lf/h/a/f/j/b/n3;->w()V

    iget-object v3, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    const/4 v3, 0x1

    iput-boolean v3, v1, Lf/h/a/f/j/b/a5;->b:Z

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-wide/32 v4, 0x8101

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "App measurement initialized, version"

    invoke-virtual {v1, v5, v4}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v4, "To enable debug logging run: adb shell setprop log.tag.FA VERBOSE"

    invoke-virtual {v1, v4}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {v2}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v1, v2, Lf/h/a/f/j/b/n3;->c:Ljava/lang/String;

    iget-object v2, v0, Lf/h/a/f/j/b/u4;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v2

    invoke-virtual {v2, v1}, Lf/h/a/f/j/b/t9;->s0(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v2, "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none."

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v4, "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_0
    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    :goto_1
    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->m:Lf/h/a/f/j/b/s3;

    const-string v2, "Debug-level message logging enabled"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iget v1, v0, Lf/h/a/f/j/b/u4;->E:I

    iget-object v2, v0, Lf/h/a/f/j/b/u4;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    iget v2, v0, Lf/h/a/f/j/b/u4;->E:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v4, v0, Lf/h/a/f/j/b/u4;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "Not all components initialized"

    invoke-virtual {v1, v5, v2, v4}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    iput-boolean v3, v0, Lf/h/a/f/j/b/u4;->x:Z

    iget-object v0, p0, Lf/h/a/f/j/b/w4;->e:Lf/h/a/f/j/b/u4;

    iget-object v1, p0, Lf/h/a/f/j/b/w4;->d:Lf/h/a/f/j/b/y5;

    iget-object v1, v1, Lf/h/a/f/j/b/y5;->g:Lcom/google/android/gms/internal/measurement/zzae;

    sget-object v2, Lf/h/a/f/j/b/d;->c:Lf/h/a/f/j/b/d;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/j/b/r4;->b()V

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-eqz v4, :cond_f

    iget-object v4, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v7, Lf/h/a/f/j/b/p;->H0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v4, v7}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/j/b/d4;->y()Lf/h/a/f/j/b/d;

    move-result-object v4

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v7

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v7}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v7

    const/16 v8, 0x64

    const-string v9, "consent_source"

    invoke-interface {v7, v9, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    iget-object v8, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v9, Lf/h/a/f/j/b/p;->I0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v8, v9}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v8

    const/16 v9, 0x28

    if-eqz v8, :cond_d

    iget-object v8, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-static {v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v10

    if-eqz v10, :cond_5

    sget-object v10, Lf/h/a/f/j/b/p;->I0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v8, v10}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v10

    if-nez v10, :cond_4

    goto :goto_2

    :cond_4
    const-string v10, "google_analytics_default_allow_ad_storage"

    invoke-virtual {v8, v10}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_3

    :cond_5
    :goto_2
    move-object v8, v5

    :goto_3
    iget-object v10, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-static {v10}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v11

    if-eqz v11, :cond_7

    sget-object v11, Lf/h/a/f/j/b/p;->I0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v10, v11}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v11

    if-nez v11, :cond_6

    goto :goto_4

    :cond_6
    const-string v11, "google_analytics_default_allow_analytics_storage"

    invoke-virtual {v10, v11}, Lf/h/a/f/j/b/c;->w(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_5

    :cond_7
    :goto_4
    move-object v10, v5

    :goto_5
    const/16 v11, 0x14

    if-nez v8, :cond_8

    if-eqz v10, :cond_9

    :cond_8
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v12

    invoke-virtual {v12, v11}, Lf/h/a/f/j/b/d4;->t(I)Z

    move-result v12

    if-eqz v12, :cond_9

    new-instance v1, Lf/h/a/f/j/b/d;

    invoke-direct {v1, v8, v10}, Lf/h/a/f/j/b/d;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    const/16 v9, 0x14

    goto :goto_7

    :cond_9
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v8

    invoke-virtual {v8}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v8, v8, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b

    const/16 v8, 0x1e

    if-eq v7, v8, :cond_a

    if-ne v7, v9, :cond_b

    :cond_a
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v1

    iget-wide v7, v0, Lf/h/a/f/j/b/u4;->G:J

    invoke-virtual {v1, v2, v11, v7, v8}, Lf/h/a/f/j/b/c6;->A(Lf/h/a/f/j/b/d;IJ)V

    goto :goto_6

    :cond_b
    if-eqz v1, :cond_c

    iget-object v7, v1, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    if-eqz v7, :cond_c

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v7

    invoke-virtual {v7, v9}, Lf/h/a/f/j/b/d4;->t(I)Z

    move-result v7

    if-eqz v7, :cond_c

    iget-object v1, v1, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    invoke-static {v1}, Lf/h/a/f/j/b/d;->g(Landroid/os/Bundle;)Lf/h/a/f/j/b/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    goto :goto_7

    :cond_c
    :goto_6
    move-object v1, v5

    const/4 v9, 0x0

    :goto_7
    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v2

    iget-wide v7, v0, Lf/h/a/f/j/b/u4;->G:J

    invoke-virtual {v2, v1, v9, v7, v8}, Lf/h/a/f/j/b/c6;->A(Lf/h/a/f/j/b/d;IJ)V

    goto :goto_8

    :cond_d
    if-eqz v1, :cond_e

    iget-object v7, v1, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    if-eqz v7, :cond_e

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v7

    invoke-virtual {v7, v9}, Lf/h/a/f/j/b/d4;->t(I)Z

    move-result v7

    if-eqz v7, :cond_e

    iget-object v1, v1, Lcom/google/android/gms/internal/measurement/zzae;->j:Landroid/os/Bundle;

    invoke-static {v1}, Lf/h/a/f/j/b/d;->g(Landroid/os/Bundle;)Lf/h/a/f/j/b/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v2

    iget-wide v7, v0, Lf/h/a/f/j/b/u4;->G:J

    invoke-virtual {v2, v1, v9, v7, v8}, Lf/h/a/f/j/b/c6;->A(Lf/h/a/f/j/b/d;IJ)V

    :goto_8
    move-object v4, v1

    :cond_e
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v1

    invoke-virtual {v1, v4}, Lf/h/a/f/j/b/c6;->z(Lf/h/a/f/j/b/d;)V

    :cond_f
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->e:Lf/h/a/f/j/b/h4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/h4;->a()J

    move-result-wide v1

    const-wide/16 v7, 0x0

    cmp-long v4, v1, v7

    if-nez v4, :cond_10

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->e:Lf/h/a/f/j/b/h4;

    iget-object v2, v0, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v2, Lf/h/a/f/f/n/d;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v1, v9, v10}, Lf/h/a/f/j/b/h4;->b(J)V

    :cond_10
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->j:Lf/h/a/f/j/b/h4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/h4;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v4, v1, v7

    if-nez v4, :cond_11

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    iget-wide v7, v0, Lf/h/a/f/j/b/u4;->G:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v4, "Persisting first open"

    invoke-virtual {v1, v4, v2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->j:Lf/h/a/f/j/b/h4;

    iget-wide v7, v0, Lf/h/a/f/j/b/u4;->G:J

    invoke-virtual {v1, v7, v8}, Lf/h/a/f/j/b/h4;->b(J)V

    :cond_11
    iget-object v1, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->D0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/c6;->n:Lf/h/a/f/j/b/y9;

    invoke-virtual {v1}, Lf/h/a/f/j/b/y9;->c()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {v1}, Lf/h/a/f/j/b/y9;->b()Z

    move-result v2

    if-eqz v2, :cond_12

    iget-object v1, v1, Lf/h/a/f/j/b/y9;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->A:Lf/h/a/f/j/b/j4;

    invoke-virtual {v1, v5}, Lf/h/a/f/j/b/j4;->b(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->m()Z

    move-result v1

    if-nez v1, :cond_17

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    const-string v2, "android.permission.INTERNET"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/t9;->p0(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "App is missing INTERNET permission"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_13
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    const-string v2, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/t9;->p0(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "App is missing ACCESS_NETWORK_STATE permission"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_14
    iget-object v1, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v1}, Lf/h/a/f/f/o/b;->a(Landroid/content/Context;)Lf/h/a/f/f/o/a;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/f/o/a;->c()Z

    move-result v1

    if-nez v1, :cond_16

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v1}, Lf/h/a/f/j/b/c;->C()Z

    move-result v1

    if-nez v1, :cond_16

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v1}, Lf/h/a/f/j/b/n4;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_15

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "AppMeasurementReceiver not registered/enabled"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_15
    iget-object v1, v0, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v1}, Lf/h/a/f/j/b/t9;->U(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_16

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "AppMeasurementService not registered/enabled"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string v2, "Uploading is not possible. App measurement disabled"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_17
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v1, v1, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v1, v1, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1b

    :cond_18
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v1, v1, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v2}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v4, "gmp_app_id"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v7

    invoke-virtual {v7}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v7, v7, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v8

    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v8}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "admob_app_id"

    invoke-interface {v8, v9, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v2, v7, v8}, Lf/h/a/f/j/b/t9;->d0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->l:Lf/h/a/f/j/b/s3;

    const-string v2, "Rechecking which service to use due to a GMP App Id change"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v1}, Lf/h/a/f/j/b/d4;->x()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    if-eqz v2, :cond_19

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/d4;->s(Ljava/lang/Boolean;)V

    :cond_19
    iget-object v1, v0, Lf/h/a/f/j/b/u4;->s:Lf/h/a/f/j/b/m3;

    invoke-static {v1}, Lf/h/a/f/j/b/u4;->p(Lf/h/a/f/j/b/a5;)V

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->s:Lf/h/a/f/j/b/m3;

    invoke-virtual {v1}, Lf/h/a/f/j/b/m3;->y()V

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->t:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/q7;->D()V

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->t:Lf/h/a/f/j/b/q7;

    invoke-virtual {v1}, Lf/h/a/f/j/b/q7;->C()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->j:Lf/h/a/f/j/b/h4;

    iget-wide v7, v0, Lf/h/a/f/j/b/u4;->G:J

    invoke-virtual {v1, v7, v8}, Lf/h/a/f/j/b/h4;->b(J)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->l:Lf/h/a/f/j/b/j4;

    invoke-virtual {v1, v5}, Lf/h/a/f/j/b/j4;->b(Ljava/lang/String;)V

    :cond_1a
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v2, v2, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v1}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v2, v2, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->b()V

    invoke-virtual {v1}, Lf/h/a/f/j/b/d4;->w()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v9, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1b
    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->H0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/d4;->y()Lf/h/a/f/j/b/d;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/d;->k()Z

    move-result v1

    if-nez v1, :cond_1c

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->l:Lf/h/a/f/j/b/j4;

    invoke-virtual {v1, v5}, Lf/h/a/f/j/b/j4;->b(Ljava/lang/String;)V

    :cond_1c
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v1

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->l:Lf/h/a/f/j/b/j4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/j4;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lf/h/a/f/j/b/c6;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    invoke-static {}, Lf/h/a/f/i/j/a9;->b()Z

    move-result v1

    if-eqz v1, :cond_1d

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->p0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const-string v2, "com.google.firebase.remoteconfig.FirebaseRemoteConfig"

    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_9

    :catch_0
    const/4 v3, 0x0

    :goto_9
    if-nez v3, :cond_1d

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->z:Lf/h/a/f/j/b/j4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/j4;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1d

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v2, "Remote config removed with active feature rollouts"

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->z:Lf/h/a/f/j/b/j4;

    invoke-virtual {v1, v5}, Lf/h/a/f/j/b/j4;->b(Ljava/lang/String;)V

    :cond_1d
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v1, v1, Lf/h/a/f/j/b/n3;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->z()Lf/h/a/f/j/b/n3;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v1, v1, Lf/h/a/f/j/b/n3;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_21

    :cond_1e
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->d()Z

    move-result v1

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->c:Landroid/content/SharedPreferences;

    const-string v3, "deferred_analytics_collection"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1f

    iget-object v2, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v2}, Lf/h/a/f/j/b/c;->x()Z

    move-result v2

    if-nez v2, :cond_1f

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v2

    xor-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Lf/h/a/f/j/b/d4;->v(Z)V

    :cond_1f
    if-eqz v1, :cond_20

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/c6;->N()V

    :cond_20
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->r()Lf/h/a/f/j/b/w8;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/w8;->d:Lf/h/a/f/j/b/f9;

    invoke-virtual {v1}, Lf/h/a/f/j/b/f9;->a()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->x()Lf/h/a/f/j/b/q7;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/q7;->A(Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-static {}, Lf/h/a/f/i/j/ea;->b()Z

    move-result v1

    if-eqz v1, :cond_21

    iget-object v1, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->z0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->x()Lf/h/a/f/j/b/q7;

    move-result-object v1

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/d4;->C:Lf/h/a/f/j/b/i4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/i4;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v1}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {v1, v6}, Lf/h/a/f/j/b/q7;->I(Z)Lcom/google/android/gms/measurement/internal/zzn;

    move-result-object v3

    new-instance v4, Lf/h/a/f/j/b/a8;

    invoke-direct {v4, v1, v2, v3}, Lf/h/a/f/j/b/a8;-><init>(Lf/h/a/f/j/b/q7;Landroid/os/Bundle;Lcom/google/android/gms/measurement/internal/zzn;)V

    invoke-virtual {v1, v4}, Lf/h/a/f/j/b/q7;->z(Ljava/lang/Runnable;)V

    :cond_21
    :goto_a
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->t:Lf/h/a/f/j/b/f4;

    iget-object v0, v0, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->Y:Lf/h/a/f/j/b/j3;

    invoke-virtual {v0, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lf/h/a/f/j/b/f4;->a(Z)V

    return-void

    :cond_22
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t initialize twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
