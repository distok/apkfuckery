.class public final Lf/h/a/f/j/b/y8;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:J

.field public final synthetic e:Lf/h/a/f/j/b/w8;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/w8;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/y8;->e:Lf/h/a/f/j/b/w8;

    iput-wide p2, p0, Lf/h/a/f/j/b/y8;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    iget-object v0, p0, Lf/h/a/f/j/b/y8;->e:Lf/h/a/f/j/b/w8;

    iget-wide v5, p0, Lf/h/a/f/j/b/y8;->d:J

    invoke-virtual {v0}, Lf/h/a/f/j/b/z1;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/w8;->x()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "Activity paused, time"

    invoke-virtual {v1, v3, v2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v7, v0, Lf/h/a/f/j/b/w8;->f:Lf/h/a/f/j/b/x8;

    new-instance v8, Lf/h/a/f/j/b/b9;

    iget-object v1, v7, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object v1, v8

    move-object v2, v7

    invoke-direct/range {v1 .. v6}, Lf/h/a/f/j/b/b9;-><init>(Lf/h/a/f/j/b/x8;JJ)V

    iput-object v8, v7, Lf/h/a/f/j/b/x8;->a:Lf/h/a/f/j/b/b9;

    iget-object v1, v7, Lf/h/a/f/j/b/x8;->b:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/w8;->c:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v8, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v1, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {v1}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/h/a/f/j/b/w8;->e:Lf/h/a/f/j/b/d9;

    iget-object v1, v1, Lf/h/a/f/j/b/d9;->c:Lf/h/a/f/j/b/i;

    invoke-virtual {v1}, Lf/h/a/f/j/b/i;->c()V

    :cond_0
    iget-object v0, v0, Lf/h/a/f/j/b/w8;->d:Lf/h/a/f/j/b/f9;

    iget-object v1, v0, Lf/h/a/f/j/b/f9;->a:Lf/h/a/f/j/b/w8;

    iget-object v1, v1, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->v0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/c;->o(Lf/h/a/f/j/b/j3;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v0, Lf/h/a/f/j/b/f9;->a:Lf/h/a/f/j/b/w8;

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->k()Lf/h/a/f/j/b/d4;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/d4;->w:Lf/h/a/f/j/b/f4;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/f4;->a(Z)V

    :cond_1
    return-void
.end method
