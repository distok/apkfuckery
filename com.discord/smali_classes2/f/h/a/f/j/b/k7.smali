.class public final Lf/h/a/f/j/b/k7;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Landroid/os/Bundle;

.field public final synthetic e:Lf/h/a/f/j/b/i7;

.field public final synthetic f:Lf/h/a/f/j/b/i7;

.field public final synthetic g:J

.field public final synthetic h:Lf/h/a/f/j/b/h7;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/h7;Landroid/os/Bundle;Lf/h/a/f/j/b/i7;Lf/h/a/f/j/b/i7;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/k7;->h:Lf/h/a/f/j/b/h7;

    iput-object p2, p0, Lf/h/a/f/j/b/k7;->d:Landroid/os/Bundle;

    iput-object p3, p0, Lf/h/a/f/j/b/k7;->e:Lf/h/a/f/j/b/i7;

    iput-object p4, p0, Lf/h/a/f/j/b/k7;->f:Lf/h/a/f/j/b/i7;

    iput-wide p5, p0, Lf/h/a/f/j/b/k7;->g:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    iget-object v0, p0, Lf/h/a/f/j/b/k7;->h:Lf/h/a/f/j/b/h7;

    iget-object v4, p0, Lf/h/a/f/j/b/k7;->d:Landroid/os/Bundle;

    iget-object v7, p0, Lf/h/a/f/j/b/k7;->e:Lf/h/a/f/j/b/i7;

    iget-object v8, p0, Lf/h/a/f/j/b/k7;->f:Lf/h/a/f/j/b/i7;

    iget-wide v9, p0, Lf/h/a/f/j/b/k7;->g:J

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v4, :cond_0

    const-string v1, "screen_name"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v1, "screen_class"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-string v3, "screen_view"

    invoke-virtual/range {v1 .. v6}, Lf/h/a/f/j/b/t9;->z(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/List;Z)Landroid/os/Bundle;

    move-result-object v6

    const/4 v5, 0x1

    move-object v1, v7

    move-object v2, v8

    move-wide v3, v9

    invoke-virtual/range {v0 .. v6}, Lf/h/a/f/j/b/h7;->B(Lf/h/a/f/j/b/i7;Lf/h/a/f/j/b/i7;JZLandroid/os/Bundle;)V

    return-void
.end method
