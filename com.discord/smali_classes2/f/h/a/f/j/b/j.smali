.class public final Lf/h/a/f/j/b/j;
.super Lf/h/a/f/j/b/r5;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public c:J

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;

.field public f:Landroid/accounts/AccountManager;

.field public g:Ljava/lang/Boolean;

.field public h:J


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/u4;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/j/b/r5;-><init>(Lf/h/a/f/j/b/u4;)V

    return-void
.end method


# virtual methods
.method public final r()Z
    .locals 4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const/16 v2, 0xf

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v2

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/j/b/j;->c:J

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v0, v2}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "-"

    invoke-static {v2, v1, v3, v0}, Lf/e/c/a/a;->f(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/j/b/j;->d:Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public final s(Landroid/content/Context;)Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/j/b/j;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lf/h/a/f/j/b/j;->e:Ljava/lang/Boolean;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "com.google.android.gms"

    const/16 v1, 0x80

    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object p1, p0, Lf/h/a/f/j/b/j;->e:Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    iget-object p1, p0, Lf/h/a/f/j/b/j;->e:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public final t()J
    .locals 2

    invoke-virtual {p0}, Lf/h/a/f/j/b/r5;->o()V

    iget-wide v0, p0, Lf/h/a/f/j/b/j;->c:J

    return-wide v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lf/h/a/f/j/b/r5;->o()V

    iget-object v0, p0, Lf/h/a/f/j/b/j;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final v()J
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->b()V

    iget-wide v0, p0, Lf/h/a/f/j/b/j;->h:J

    return-wide v0
.end method

.method public final w()Z
    .locals 11
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v1, "com.google"

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->b()V

    iget-object v3, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v3, Lf/h/a/f/f/n/d;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lf/h/a/f/j/b/j;->h:J

    sub-long v5, v3, v5

    const-wide/32 v7, 0x5265c00

    const/4 v9, 0x0

    cmp-long v10, v5, v7

    if-lez v10, :cond_0

    iput-object v9, p0, Lf/h/a/f/j/b/j;->g:Ljava/lang/Boolean;

    :cond_0
    iget-object v5, p0, Lf/h/a/f/j/b/j;->g:Ljava/lang/Boolean;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_1
    iget-object v5, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v5, v5, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    const-string v6, "android.permission.GET_ACCOUNTS"

    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/q3;->j:Lf/h/a/f/j/b/s3;

    const-string v1, "Permission error checking for dasher/unicorn accounts"

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    iput-wide v3, p0, Lf/h/a/f/j/b/j;->h:J

    iput-object v2, p0, Lf/h/a/f/j/b/j;->g:Ljava/lang/Boolean;

    return v6

    :cond_2
    iget-object v5, p0, Lf/h/a/f/j/b/j;->f:Landroid/accounts/AccountManager;

    if-nez v5, :cond_3

    iget-object v5, p0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v5, v5, Lf/h/a/f/j/b/u4;->a:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    iput-object v5, p0, Lf/h/a/f/j/b/j;->f:Landroid/accounts/AccountManager;

    :cond_3
    :try_start_0
    iget-object v5, p0, Lf/h/a/f/j/b/j;->f:Landroid/accounts/AccountManager;

    const-string v7, "service_HOSTED"

    filled-new-array {v7}, [Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v1, v7, v9, v9}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v5

    invoke-interface {v5}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/accounts/Account;

    const/4 v7, 0x1

    if-eqz v5, :cond_4

    array-length v5, v5

    if-lez v5, :cond_4

    iput-object v0, p0, Lf/h/a/f/j/b/j;->g:Ljava/lang/Boolean;

    iput-wide v3, p0, Lf/h/a/f/j/b/j;->h:J

    return v7

    :cond_4
    iget-object v5, p0, Lf/h/a/f/j/b/j;->f:Landroid/accounts/AccountManager;

    const-string v8, "service_uca"

    filled-new-array {v8}, [Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v1, v8, v9, v9}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v1

    invoke-interface {v1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/accounts/Account;

    if-eqz v1, :cond_5

    array-length v1, v1

    if-lez v1, :cond_5

    iput-object v0, p0, Lf/h/a/f/j/b/j;->g:Ljava/lang/Boolean;

    iput-wide v3, p0, Lf/h/a/f/j/b/j;->h:J
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    return v7

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    :goto_0
    invoke-virtual {p0}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->g:Lf/h/a/f/j/b/s3;

    const-string v5, "Exception checking account types"

    invoke-virtual {v1, v5, v0}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_5
    iput-wide v3, p0, Lf/h/a/f/j/b/j;->h:J

    iput-object v2, p0, Lf/h/a/f/j/b/j;->g:Ljava/lang/Boolean;

    return v6
.end method
