.class public final Lf/h/a/f/j/b/t4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field public final a:Ljava/lang/String;

.field public final synthetic b:Lf/h/a/f/j/b/r4;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/r4;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/t4;->b:Lf/h/a/f/j/b/r4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/a/f/j/b/t4;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final declared-synchronized uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object p1, p0, Lf/h/a/f/j/b/t4;->b:Lf/h/a/f/j/b/r4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    iget-object v0, p0, Lf/h/a/f/j/b/t4;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
