.class public final Lf/h/a/f/j/b/fa;
.super Lf/h/a/f/j/b/ea;
.source "com.google.android.gms:play-services-measurement@@18.0.0"


# instance fields
.field public g:Lf/h/a/f/i/j/l0;

.field public final synthetic h:Lf/h/a/f/j/b/ba;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/ba;Ljava/lang/String;ILf/h/a/f/i/j/l0;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-direct {p0, p2, p3}, Lf/h/a/f/j/b/ea;-><init>(Ljava/lang/String;I)V

    iput-object p4, p0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v0}, Lf/h/a/f/i/j/l0;->y()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v0}, Lf/h/a/f/i/j/l0;->C()Z

    move-result v0

    return v0
.end method

.method public final i(Ljava/lang/Long;Ljava/lang/Long;Lf/h/a/f/i/j/a1;JLf/h/a/f/j/b/l;Z)Z
    .locals 17

    move-object/from16 v0, p0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {}, Lf/h/a/f/i/j/f9;->b()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    iget-object v3, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    iget-object v3, v3, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v3, v3, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v6, v0, Lf/h/a/f/j/b/ea;->a:Ljava/lang/String;

    sget-object v7, Lf/h/a/f/j/b/p;->c0:Lf/h/a/f/j/b/j3;

    invoke-virtual {v3, v6, v7}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    iget-object v6, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v6}, Lf/h/a/f/i/j/l0;->H()Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, p6

    iget-wide v6, v6, Lf/h/a/f/j/b/l;->e:J

    goto :goto_1

    :cond_1
    move-wide/from16 v6, p4

    :goto_1
    iget-object v8, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lf/h/a/f/j/b/q3;->x(I)Z

    move-result v8

    const-string v9, "null"

    if-eqz v8, :cond_8

    iget-object v8, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v8

    iget-object v8, v8, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    iget v10, v0, Lf/h/a/f/j/b/ea;->b:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v11, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->x()Z

    move-result v11

    if-eqz v11, :cond_2

    iget-object v11, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->y()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    goto :goto_2

    :cond_2
    const/4 v11, 0x0

    :goto_2
    iget-object v12, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v12}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v12

    iget-object v13, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v13}, Lf/h/a/f/i/j/l0;->z()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "Evaluating filter. audience, filter, event"

    invoke-virtual {v8, v13, v10, v11, v12}, Lf/h/a/f/j/b/s3;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v8, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v8}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v8

    iget-object v8, v8, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    iget-object v10, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v10}, Lf/h/a/f/j/b/i9;->m()Lf/h/a/f/j/b/q9;

    move-result-object v10

    iget-object v11, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-static {v10}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v11, :cond_3

    move-object v10, v9

    goto/16 :goto_4

    :cond_3
    const-string v12, "\nevent_filter {\n"

    invoke-static {v12}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->x()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->y()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const-string v14, "filter_id"

    invoke-static {v12, v5, v14, v13}, Lf/h/a/f/j/b/q9;->L(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    :cond_4
    invoke-virtual {v10}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v13

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->z()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "event_name"

    invoke-static {v12, v5, v14, v13}, Lf/h/a/f/j/b/q9;->L(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->E()Z

    move-result v13

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->F()Z

    move-result v14

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->H()Z

    move-result v15

    invoke-static {v13, v14, v15}, Lf/h/a/f/j/b/q9;->A(ZZZ)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_5

    const-string v14, "filter_type"

    invoke-static {v12, v5, v14, v13}, Lf/h/a/f/j/b/q9;->L(Ljava/lang/StringBuilder;ILjava/lang/String;Ljava/lang/Object;)V

    :cond_5
    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->C()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->D()Lf/h/a/f/i/j/n0;

    move-result-object v13

    const-string v14, "event_count_filter"

    invoke-static {v12, v4, v14, v13}, Lf/h/a/f/j/b/q9;->J(Ljava/lang/StringBuilder;ILjava/lang/String;Lf/h/a/f/i/j/n0;)V

    :cond_6
    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->B()I

    move-result v13

    if-lez v13, :cond_7

    const-string v13, "  filters {\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->A()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lf/h/a/f/i/j/m0;

    const/4 v14, 0x2

    invoke-virtual {v10, v12, v14, v13}, Lf/h/a/f/j/b/q9;->I(Ljava/lang/StringBuilder;ILf/h/a/f/i/j/m0;)V

    goto :goto_3

    :cond_7
    invoke-static {v12, v4}, Lf/h/a/f/j/b/q9;->H(Ljava/lang/StringBuilder;I)V

    const-string/jumbo v10, "}\n}\n"

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    :goto_4
    const-string v11, "Filter definition"

    invoke-virtual {v8, v11, v10}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_8
    iget-object v8, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v8}, Lf/h/a/f/i/j/l0;->x()Z

    move-result v8

    if-eqz v8, :cond_30

    iget-object v8, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v8}, Lf/h/a/f/i/j/l0;->y()I

    move-result v8

    const/16 v10, 0x100

    if-le v8, v10, :cond_9

    goto/16 :goto_13

    :cond_9
    iget-object v8, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v8}, Lf/h/a/f/i/j/l0;->E()Z

    move-result v8

    iget-object v10, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v10}, Lf/h/a/f/i/j/l0;->F()Z

    move-result v10

    iget-object v11, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v11}, Lf/h/a/f/i/j/l0;->H()Z

    move-result v11

    if-nez v8, :cond_b

    if-nez v10, :cond_b

    if-eqz v11, :cond_a

    goto :goto_5

    :cond_a
    const/4 v8, 0x0

    goto :goto_6

    :cond_b
    :goto_5
    const/4 v8, 0x1

    :goto_6
    if-eqz p7, :cond_d

    if-nez v8, :cond_d

    iget-object v1, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    iget v2, v0, Lf/h/a/f/j/b/ea;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v3}, Lf/h/a/f/i/j/l0;->x()Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v3}, Lf/h/a/f/i/j/l0;->y()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_7

    :cond_c
    const/4 v3, 0x0

    :goto_7
    const-string v5, "Event filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"

    invoke-virtual {v1, v5, v2, v3}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return v4

    :cond_d
    iget-object v10, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual/range {p3 .. p3}, Lf/h/a/f/i/j/a1;->F()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10}, Lf/h/a/f/i/j/l0;->C()Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-virtual {v10}, Lf/h/a/f/i/j/l0;->D()Lf/h/a/f/i/j/n0;

    move-result-object v12

    invoke-static {v6, v7, v12}, Lf/h/a/f/j/b/ea;->b(JLf/h/a/f/i/j/n0;)Ljava/lang/Boolean;

    move-result-object v6

    if-nez v6, :cond_e

    goto/16 :goto_f

    :cond_e
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_f

    goto/16 :goto_10

    :cond_f
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v10}, Lf/h/a/f/i/j/l0;->A()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_11

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lf/h/a/f/i/j/m0;

    invoke-virtual {v12}, Lf/h/a/f/i/j/m0;->C()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_10

    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "null or empty param name in filter. event"

    invoke-virtual {v2, v7, v6}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_f

    :cond_10
    invoke-virtual {v12}, Lf/h/a/f/i/j/m0;->C()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_11
    new-instance v7, Landroidx/collection/ArrayMap;

    invoke-direct {v7}, Landroidx/collection/ArrayMap;-><init>()V

    invoke-virtual/range {p3 .. p3}, Lf/h/a/f/i/j/a1;->v()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_12
    :goto_9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_18

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lf/h/a/f/i/j/c1;

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->B()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_12

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->I()Z

    move-result v14

    if-eqz v14, :cond_14

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->B()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->I()Z

    move-result v15

    if-eqz v15, :cond_13

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->J()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    goto :goto_a

    :cond_13
    const/4 v13, 0x0

    :goto_a
    invoke-interface {v7, v14, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    :cond_14
    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->M()Z

    move-result v14

    if-eqz v14, :cond_16

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->B()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->M()Z

    move-result v15

    if-eqz v15, :cond_15

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->N()D

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    goto :goto_b

    :cond_15
    const/4 v13, 0x0

    :goto_b
    invoke-interface {v7, v14, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    :cond_16
    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->F()Z

    move-result v14

    if-eqz v14, :cond_17

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->B()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->G()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v7, v14, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    :cond_17
    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v13}, Lf/h/a/f/i/j/c1;->B()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v10, "Unknown value for param. event, param"

    invoke-virtual {v2, v10, v6, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_f

    :cond_18
    invoke-virtual {v10}, Lf/h/a/f/i/j/l0;->A()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_19
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_28

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lf/h/a/f/i/j/m0;

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->z()Z

    move-result v12

    if-eqz v12, :cond_1a

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->A()Z

    move-result v12

    if-eqz v12, :cond_1a

    const/4 v12, 0x1

    goto :goto_c

    :cond_1a
    const/4 v12, 0x0

    :goto_c
    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->C()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_1b

    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Event has empty param name. event"

    invoke-virtual {v2, v7, v6}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_f

    :cond_1b
    invoke-interface {v7, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    instance-of v15, v14, Ljava/lang/Long;

    if-eqz v15, :cond_1e

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->x()Z

    move-result v15

    if-nez v15, :cond_1c

    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v7, v13}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v10, "No number filter for long param. event, param"

    invoke-virtual {v2, v10, v6, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_f

    :cond_1c
    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->y()Lf/h/a/f/i/j/n0;

    move-result-object v10

    invoke-static {v13, v14, v10}, Lf/h/a/f/j/b/ea;->b(JLf/h/a/f/i/j/n0;)Ljava/lang/Boolean;

    move-result-object v10

    if-nez v10, :cond_1d

    goto/16 :goto_f

    :cond_1d
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-ne v10, v12, :cond_19

    goto/16 :goto_10

    :cond_1e
    instance-of v15, v14, Ljava/lang/Double;

    if-eqz v15, :cond_21

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->x()Z

    move-result v15

    if-nez v15, :cond_1f

    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v7, v13}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v10, "No number filter for double param. event, param"

    invoke-virtual {v2, v10, v6, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_f

    :cond_1f
    check-cast v14, Ljava/lang/Double;

    invoke-virtual {v14}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v13

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->y()Lf/h/a/f/i/j/n0;

    move-result-object v10

    :try_start_0
    new-instance v15, Ljava/math/BigDecimal;

    invoke-direct {v15, v13, v14}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-static {v13, v14}, Ljava/lang/Math;->ulp(D)D

    move-result-wide v13

    invoke-static {v15, v10, v13, v14}, Lf/h/a/f/j/b/ea;->f(Ljava/math/BigDecimal;Lf/h/a/f/i/j/n0;D)Ljava/lang/Boolean;

    move-result-object v10
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_d

    :catch_0
    const/4 v10, 0x0

    :goto_d
    if-nez v10, :cond_20

    goto/16 :goto_f

    :cond_20
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-ne v10, v12, :cond_19

    goto/16 :goto_10

    :cond_21
    instance-of v15, v14, Ljava/lang/String;

    if-eqz v15, :cond_26

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->v()Z

    move-result v15

    if-eqz v15, :cond_22

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->w()Lf/h/a/f/i/j/p0;

    move-result-object v10

    iget-object v13, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v13}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v13

    invoke-static {v14, v10, v13}, Lf/h/a/f/j/b/ea;->e(Ljava/lang/String;Lf/h/a/f/i/j/p0;Lf/h/a/f/j/b/q3;)Ljava/lang/Boolean;

    move-result-object v10

    goto :goto_e

    :cond_22
    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->x()Z

    move-result v15

    if-eqz v15, :cond_25

    check-cast v14, Ljava/lang/String;

    invoke-static {v14}, Lf/h/a/f/j/b/q9;->P(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_24

    invoke-virtual {v10}, Lf/h/a/f/i/j/m0;->y()Lf/h/a/f/i/j/n0;

    move-result-object v10

    invoke-static {v14, v10}, Lf/h/a/f/j/b/ea;->d(Ljava/lang/String;Lf/h/a/f/i/j/n0;)Ljava/lang/Boolean;

    move-result-object v10

    :goto_e
    if-nez v10, :cond_23

    goto/16 :goto_f

    :cond_23
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-ne v10, v12, :cond_19

    goto/16 :goto_10

    :cond_24
    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v7, v13}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v10, "Invalid param value for number filter. event, param"

    invoke-virtual {v2, v10, v6, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_f

    :cond_25
    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v7, v13}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v10, "No filter for String param. event, param"

    invoke-virtual {v2, v10, v6, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_f

    :cond_26
    if-nez v14, :cond_27

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    iget-object v7, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v7, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v10, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v10}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v10

    invoke-virtual {v10, v13}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "Missing param for filter. event, param"

    invoke-virtual {v6, v11, v7, v10}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_10

    :cond_27
    iget-object v2, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v2}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v2

    iget-object v2, v2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v6

    invoke-virtual {v6, v11}, Lf/h/a/f/j/b/o3;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v7}, Lf/h/a/f/j/b/s5;->d()Lf/h/a/f/j/b/o3;

    move-result-object v7

    invoke-virtual {v7, v13}, Lf/h/a/f/j/b/o3;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v10, "Unknown param type. event, param"

    invoke-virtual {v2, v10, v6, v7}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_f
    const/4 v2, 0x0

    goto :goto_10

    :cond_28
    move-object v2, v1

    :goto_10
    iget-object v6, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v6}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v6

    iget-object v6, v6, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    if-nez v2, :cond_29

    goto :goto_11

    :cond_29
    move-object v9, v2

    :goto_11
    const-string v7, "Event filter result"

    invoke-virtual {v6, v7, v9}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v2, :cond_2a

    return v5

    :cond_2a
    iput-object v1, v0, Lf/h/a/f/j/b/ea;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2b

    return v4

    :cond_2b
    iput-object v1, v0, Lf/h/a/f/j/b/ea;->d:Ljava/lang/Boolean;

    if-eqz v8, :cond_2f

    invoke-virtual/range {p3 .. p3}, Lf/h/a/f/i/j/a1;->G()Z

    move-result v1

    if-eqz v1, :cond_2f

    invoke-virtual/range {p3 .. p3}, Lf/h/a/f/i/j/a1;->H()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v2}, Lf/h/a/f/i/j/l0;->F()Z

    move-result v2

    if-eqz v2, :cond_2d

    if-eqz v3, :cond_2c

    iget-object v2, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v2}, Lf/h/a/f/i/j/l0;->C()Z

    move-result v2

    if-eqz v2, :cond_2c

    move-object/from16 v1, p1

    :cond_2c
    iput-object v1, v0, Lf/h/a/f/j/b/ea;->f:Ljava/lang/Long;

    goto :goto_12

    :cond_2d
    if-eqz v3, :cond_2e

    iget-object v2, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v2}, Lf/h/a/f/i/j/l0;->C()Z

    move-result v2

    if-eqz v2, :cond_2e

    move-object/from16 v1, p2

    :cond_2e
    iput-object v1, v0, Lf/h/a/f/j/b/ea;->e:Ljava/lang/Long;

    :cond_2f
    :goto_12
    return v4

    :cond_30
    :goto_13
    iget-object v1, v0, Lf/h/a/f/j/b/fa;->h:Lf/h/a/f/j/b/ba;

    invoke-virtual {v1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    iget-object v2, v0, Lf/h/a/f/j/b/ea;->a:Ljava/lang/String;

    invoke-static {v2}, Lf/h/a/f/j/b/q3;->s(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v3}, Lf/h/a/f/i/j/l0;->x()Z

    move-result v3

    if-eqz v3, :cond_31

    iget-object v3, v0, Lf/h/a/f/j/b/fa;->g:Lf/h/a/f/i/j/l0;

    invoke-virtual {v3}, Lf/h/a/f/i/j/l0;->y()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_14

    :cond_31
    const/4 v3, 0x0

    :goto_14
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Invalid event filter ID. appId, id"

    invoke-virtual {v1, v4, v2, v3}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return v5
.end method
