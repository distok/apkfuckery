.class public final Lf/h/a/f/j/b/j9;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/f/j/b/r9;

.field public final synthetic e:Lf/h/a/f/j/b/k9;


# direct methods
.method public constructor <init>(Lf/h/a/f/j/b/k9;Lf/h/a/f/j/b/r9;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/j/b/j9;->e:Lf/h/a/f/j/b/k9;

    iput-object p2, p0, Lf/h/a/f/j/b/j9;->d:Lf/h/a/f/j/b/r9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lf/h/a/f/j/b/j9;->e:Lf/h/a/f/j/b/k9;

    iget-object v1, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/r4;->b()V

    new-instance v1, Lf/h/a/f/j/b/g;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/g;-><init>(Lf/h/a/f/j/b/k9;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/i9;->o()V

    iput-object v1, v0, Lf/h/a/f/j/b/k9;->c:Lf/h/a/f/j/b/g;

    iget-object v1, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    iget-object v2, v0, Lf/h/a/f/j/b/k9;->a:Lf/h/a/f/j/b/p4;

    iput-object v2, v1, Lf/h/a/f/j/b/c;->c:Lf/h/a/f/j/b/e;

    new-instance v1, Lf/h/a/f/j/b/q8;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/q8;-><init>(Lf/h/a/f/j/b/k9;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/i9;->o()V

    iput-object v1, v0, Lf/h/a/f/j/b/k9;->i:Lf/h/a/f/j/b/q8;

    new-instance v1, Lf/h/a/f/j/b/ba;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/ba;-><init>(Lf/h/a/f/j/b/k9;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/i9;->o()V

    iput-object v1, v0, Lf/h/a/f/j/b/k9;->f:Lf/h/a/f/j/b/ba;

    new-instance v1, Lf/h/a/f/j/b/g7;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/g7;-><init>(Lf/h/a/f/j/b/k9;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/i9;->o()V

    iput-object v1, v0, Lf/h/a/f/j/b/k9;->h:Lf/h/a/f/j/b/g7;

    new-instance v1, Lf/h/a/f/j/b/h9;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/h9;-><init>(Lf/h/a/f/j/b/k9;)V

    invoke-virtual {v1}, Lf/h/a/f/j/b/i9;->o()V

    iput-object v1, v0, Lf/h/a/f/j/b/k9;->e:Lf/h/a/f/j/b/h9;

    new-instance v1, Lf/h/a/f/j/b/b4;

    invoke-direct {v1, v0}, Lf/h/a/f/j/b/b4;-><init>(Lf/h/a/f/j/b/k9;)V

    iput-object v1, v0, Lf/h/a/f/j/b/k9;->d:Lf/h/a/f/j/b/b4;

    iget v1, v0, Lf/h/a/f/j/b/k9;->o:I

    iget v2, v0, Lf/h/a/f/j/b/k9;->p:I

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    iget v2, v0, Lf/h/a/f/j/b/k9;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v0, Lf/h/a/f/j/b/k9;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "Not all upload components initialized"

    invoke-virtual {v1, v4, v2, v3}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/a/f/j/b/k9;->k:Z

    iget-object v0, p0, Lf/h/a/f/j/b/j9;->e:Lf/h/a/f/j/b/k9;

    iget-object v1, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/r4;->b()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->K()Lf/h/a/f/j/b/g;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/j/b/g;->i0()V

    iget-object v1, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->e:Lf/h/a/f/j/b/h4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/h4;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    iget-object v1, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->o()Lf/h/a/f/j/b/d4;

    move-result-object v1

    iget-object v1, v1, Lf/h/a/f/j/b/d4;->e:Lf/h/a/f/j/b/h4;

    iget-object v2, v0, Lf/h/a/f/j/b/k9;->j:Lf/h/a/f/j/b/u4;

    iget-object v2, v2, Lf/h/a/f/j/b/u4;->n:Lf/h/a/f/f/n/c;

    check-cast v2, Lf/h/a/f/f/n/d;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lf/h/a/f/j/b/h4;->b(J)V

    :cond_1
    invoke-virtual {v0}, Lf/h/a/f/j/b/k9;->w()V

    return-void
.end method
