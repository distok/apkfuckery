.class public Lf/h/a/f/j/a/a;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-sdk-api@@18.0.0"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/j/a/a$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/f/i/j/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/f/j/a/a$a;)V
    .locals 5

    iget-object v0, p0, Lf/h/a/f/j/a/a;->a:Lf/h/a/f/i/j/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/h/a/f/i/j/g;->e:Ljava/util/List;

    monitor-enter v1

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    iget-object v3, v0, Lf/h/a/f/i/j/g;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    iget-object v3, v0, Lf/h/a/f/i/j/g;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object p1, v0, Lf/h/a/f/i/j/g;->a:Ljava/lang/String;

    const-string v0, "OnEventListener already registered."

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v2, Lf/h/a/f/i/j/g$c;

    invoke-direct {v2, p1}, Lf/h/a/f/i/j/g$c;-><init>(Lf/h/a/f/j/b/z5;)V

    iget-object v3, v0, Lf/h/a/f/i/j/g;->e:Ljava/util/List;

    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, p1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, v0, Lf/h/a/f/i/j/g;->h:Lf/h/a/f/i/j/ec;

    if-eqz p1, :cond_2

    :try_start_1
    iget-object p1, v0, Lf/h/a/f/i/j/g;->h:Lf/h/a/f/i/j/ec;

    invoke-interface {p1, v2}, Lf/h/a/f/i/j/ec;->registerOnMeasurementEventListener(Lf/h/a/f/i/j/c;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/BadParcelableException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/NetworkOnMainThreadException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    iget-object p1, v0, Lf/h/a/f/i/j/g;->a:Ljava/lang/String;

    const-string v1, "Failed to register event listener on calling thread. Trying again on the dynamite thread."

    invoke-static {p1, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance p1, Lf/h/a/f/i/j/a0;

    invoke-direct {p1, v0, v2}, Lf/h/a/f/i/j/a0;-><init>(Lf/h/a/f/i/j/g;Lf/h/a/f/i/j/g$c;)V

    iget-object v0, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method
