.class public final Lf/h/a/f/f/k/b$i;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-basement@@17.4.0"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/k/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "i"
.end annotation


# instance fields
.field public final d:I

.field public final synthetic e:Lf/h/a/f/f/k/b;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/k/b;I)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/f/k/b$i;->e:Lf/h/a/f/f/k/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lf/h/a/f/f/k/b$i;->d:I

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    if-nez p2, :cond_0

    iget-object p1, p0, Lf/h/a/f/f/k/b$i;->e:Lf/h/a/f/f/k/b;

    invoke-static {p1}, Lf/h/a/f/f/k/b;->C(Lf/h/a/f/f/k/b;)V

    return-void

    :cond_0
    iget-object p1, p0, Lf/h/a/f/f/k/b$i;->e:Lf/h/a/f/f/k/b;

    iget-object p1, p1, Lf/h/a/f/f/k/b;->h:Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/f/k/b$i;->e:Lf/h/a/f/f/k/b;

    const-string v1, "com.google.android.gms.common.internal.IGmsServiceBroker"

    invoke-interface {p2, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    instance-of v2, v1, Lf/h/a/f/f/k/i;

    if-eqz v2, :cond_1

    check-cast v1, Lf/h/a/f/f/k/i;

    goto :goto_0

    :cond_1
    new-instance v1, Lf/h/a/f/f/k/h;

    invoke-direct {v1, p2}, Lf/h/a/f/f/k/h;-><init>(Landroid/os/IBinder;)V

    :goto_0
    iput-object v1, v0, Lf/h/a/f/f/k/b;->i:Lf/h/a/f/f/k/i;

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lf/h/a/f/f/k/b$i;->e:Lf/h/a/f/f/k/b;

    const/4 p2, 0x0

    iget v0, p0, Lf/h/a/f/f/k/b$i;->d:I

    iget-object v1, p1, Lf/h/a/f/f/k/b;->f:Landroid/os/Handler;

    new-instance v2, Lf/h/a/f/f/k/b$l;

    invoke-direct {v2, p1, p2}, Lf/h/a/f/f/k/b$l;-><init>(Lf/h/a/f/f/k/b;I)V

    const/4 p1, 0x7

    const/4 p2, -0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception p2

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p2
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    iget-object p1, p0, Lf/h/a/f/f/k/b$i;->e:Lf/h/a/f/f/k/b;

    iget-object p1, p1, Lf/h/a/f/f/k/b;->h:Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/f/k/b$i;->e:Lf/h/a/f/f/k/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/a/f/f/k/b;->i:Lf/h/a/f/f/k/i;

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, v0, Lf/h/a/f/f/k/b;->f:Landroid/os/Handler;

    const/4 v0, 0x6

    iget v1, p0, Lf/h/a/f/f/k/b$i;->d:I

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
