.class public final Lf/h/a/f/f/k/s;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Lf/h/a/f/f/h/d$a;


# instance fields
.field public final synthetic a:Lf/h/a/f/f/h/d;

.field public final synthetic b:Lcom/google/android/gms/tasks/TaskCompletionSource;

.field public final synthetic c:Lf/h/a/f/f/k/k$a;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/d;Lcom/google/android/gms/tasks/TaskCompletionSource;Lf/h/a/f/f/k/k$a;Lf/h/a/f/f/k/k$b;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/f/k/s;->a:Lf/h/a/f/f/h/d;

    iput-object p2, p0, Lf/h/a/f/f/k/s;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iput-object p3, p0, Lf/h/a/f/f/k/s;->c:Lf/h/a/f/f/k/k$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->M0()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lf/h/a/f/f/k/s;->a:Lf/h/a/f/f/h/d;

    const-wide/16 v0, 0x0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    check-cast p1, Lcom/google/android/gms/common/api/internal/BasePendingResult;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v3, p1, Lcom/google/android/gms/common/api/internal/BasePendingResult;->h:Z

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    const-string v5, "Result has already been consumed."

    invoke-static {v3, v5}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    const-string v3, "Cannot await if then() has been called."

    invoke-static {v4, v3}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v3, p1, Lcom/google/android/gms/common/api/internal/BasePendingResult;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3, v0, v1, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->k:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/internal/BasePendingResult;->e(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->j:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/internal/BasePendingResult;->e(Lcom/google/android/gms/common/api/Status;)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/internal/BasePendingResult;->f()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/internal/BasePendingResult;->j()Lf/h/a/f/f/h/g;

    move-result-object p1

    iget-object v0, p0, Lf/h/a/f/f/k/s;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, p0, Lf/h/a/f/f/k/s;->c:Lf/h/a/f/f/k/k$a;

    check-cast v1, Lf/h/a/f/f/k/u;

    iget-object v1, v1, Lf/h/a/f/f/k/u;->a:Lf/h/a/f/f/h/f;

    iput-object p1, v1, Lf/h/a/f/f/h/f;->a:Lf/h/a/f/f/h/g;

    iget-object p1, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {p1, v1}, Lf/h/a/f/p/b0;->t(Ljava/lang/Object;)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/f/k/s;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-static {p1}, Lf/g/j/k/a;->W(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ApiException;

    move-result-object p1

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {v0, p1}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void
.end method
