.class public abstract Lf/h/a/f/f/k/b$f;
.super Lf/h/a/f/f/k/b$h;
.source "com.google.android.gms:play-services-basement@@17.4.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/k/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/k/b$h<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:I

.field public final e:Landroid/os/Bundle;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final synthetic f:Lf/h/a/f/f/k/b;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/k/b;ILandroid/os/Bundle;)V
    .locals 1
    .param p2    # I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/BinderThread;
    .end annotation

    iput-object p1, p0, Lf/h/a/f/f/k/b$f;->f:Lf/h/a/f/f/k/b;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-direct {p0, p1, v0}, Lf/h/a/f/f/k/b$h;-><init>(Lf/h/a/f/f/k/b;Ljava/lang/Object;)V

    iput p2, p0, Lf/h/a/f/f/k/b$f;->d:I

    iput-object p3, p0, Lf/h/a/f/f/k/b$f;->e:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/lang/Boolean;

    iget p1, p0, Lf/h/a/f/f/k/b$f;->d:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/f/k/b$f;->e()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lf/h/a/f/f/k/b$f;->f:Lf/h/a/f/f/k/b;

    invoke-virtual {p1, v0, v1}, Lf/h/a/f/f/k/b;->B(ILandroid/os/IInterface;)V

    new-instance p1, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v0, 0x8

    invoke-direct {p1, v0, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, p1}, Lf/h/a/f/f/k/b$f;->d(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void

    :cond_0
    iget-object p1, p0, Lf/h/a/f/f/k/b$f;->f:Lf/h/a/f/f/k/b;

    invoke-virtual {p1, v0, v1}, Lf/h/a/f/f/k/b;->B(ILandroid/os/IInterface;)V

    iget-object p1, p0, Lf/h/a/f/f/k/b$f;->e:Landroid/os/Bundle;

    if-eqz p1, :cond_1

    const-string v0, "pendingIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Landroid/app/PendingIntent;

    :cond_1
    new-instance p1, Lcom/google/android/gms/common/ConnectionResult;

    iget v0, p0, Lf/h/a/f/f/k/b$f;->d:I

    invoke-direct {p1, v0, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, p1}, Lf/h/a/f/f/k/b$f;->d(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_2
    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public abstract d(Lcom/google/android/gms/common/ConnectionResult;)V
.end method

.method public abstract e()Z
.end method
