.class public final Lf/h/a/f/f/k/b$j;
.super Lf/h/a/f/f/k/g$a;
.source "com.google.android.gms:play-services-basement@@17.4.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/k/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "j"
.end annotation


# instance fields
.field public a:Lf/h/a/f/f/k/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final b:I


# direct methods
.method public constructor <init>(Lf/h/a/f/f/k/b;I)V
    .locals 0
    .param p1    # Lf/h/a/f/f/k/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Lf/h/a/f/f/k/g$a;-><init>()V

    iput-object p1, p0, Lf/h/a/f/f/k/b$j;->a:Lf/h/a/f/f/k/b;

    iput p2, p0, Lf/h/a/f/f/k/b$j;->b:I

    return-void
.end method


# virtual methods
.method public final g(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/IBinder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/BinderThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/k/b$j;->a:Lf/h/a/f/f/k/b;

    const-string v1, "onPostInitComplete can be called only once per call to getRemoteService"

    invoke-static {v0, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/f/k/b$j;->a:Lf/h/a/f/f/k/b;

    iget v1, p0, Lf/h/a/f/f/k/b$j;->b:I

    iget-object v2, v0, Lf/h/a/f/f/k/b;->f:Landroid/os/Handler;

    new-instance v3, Lf/h/a/f/f/k/b$k;

    invoke-direct {v3, v0, p1, p2, p3}, Lf/h/a/f/f/k/b$k;-><init>(Lf/h/a/f/f/k/b;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    const/4 p1, 0x1

    const/4 p2, -0x1

    invoke-virtual {v2, p1, v1, p2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/f/f/k/b$j;->a:Lf/h/a/f/f/k/b;

    return-void
.end method
