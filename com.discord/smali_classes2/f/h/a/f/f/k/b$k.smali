.class public final Lf/h/a/f/f/k/b$k;
.super Lf/h/a/f/f/k/b$f;
.source "com.google.android.gms:play-services-basement@@17.4.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/k/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/k/b$f;"
    }
.end annotation


# instance fields
.field public final g:Landroid/os/IBinder;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final synthetic h:Lf/h/a/f/f/k/b;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/k/b;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/IBinder;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/BinderThread;
    .end annotation

    iput-object p1, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    invoke-direct {p0, p1, p2, p4}, Lf/h/a/f/f/k/b$f;-><init>(Lf/h/a/f/f/k/b;ILandroid/os/Bundle;)V

    iput-object p3, p0, Lf/h/a/f/f/k/b$k;->g:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method public final d(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    iget-object v0, v0, Lf/h/a/f/f/k/b;->p:Lf/h/a/f/f/k/b$b;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lf/h/a/f/f/k/b$b;->g(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_0
    iget-object p1, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    return-void
.end method

.method public final e()Z
    .locals 7

    const-string v0, "GmsClient"

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lf/h/a/f/f/k/b$k;->g:Landroid/os/IBinder;

    const-string v3, "null reference"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-interface {v2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    invoke-virtual {v3}, Lf/h/a/f/f/k/b;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    invoke-virtual {v3}, Lf/h/a/f/f/k/b;->w()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x22

    invoke-static {v3, v4}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v2, v4}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "service descriptor mismatch: "

    const-string v6, " vs. "

    invoke-static {v4, v5, v3, v6, v2}, Lf/e/c/a/a;->g(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    iget-object v0, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    iget-object v2, p0, Lf/h/a/f/f/k/b$k;->g:Landroid/os/IBinder;

    invoke-virtual {v0, v2}, Lf/h/a/f/f/k/b;->r(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    const/4 v3, 0x2

    const/4 v4, 0x4

    invoke-static {v2, v3, v4, v0}, Lf/h/a/f/f/k/b;->D(Lf/h/a/f/f/k/b;IILandroid/os/IInterface;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    const/4 v3, 0x3

    invoke-static {v2, v3, v4, v0}, Lf/h/a/f/f/k/b;->D(Lf/h/a/f/f/k/b;IILandroid/os/IInterface;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lf/h/a/f/f/k/b$k;->h:Lf/h/a/f/f/k/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/a/f/f/k/b;->s:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, v0, Lf/h/a/f/f/k/b;->o:Lf/h/a/f/f/k/b$a;

    if-eqz v0, :cond_2

    invoke-interface {v0, v1}, Lf/h/a/f/f/k/b$a;->h(Landroid/os/Bundle;)V

    :cond_2
    const/4 v0, 0x1

    return v0

    :cond_3
    return v1

    :catch_0
    const-string v2, "service probably died"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method
