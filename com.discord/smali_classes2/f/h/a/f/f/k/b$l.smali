.class public final Lf/h/a/f/f/k/b$l;
.super Lf/h/a/f/f/k/b$f;
.source "com.google.android.gms:play-services-basement@@17.4.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/k/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "l"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/k/b$f;"
    }
.end annotation


# instance fields
.field public final synthetic g:Lf/h/a/f/f/k/b;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/k/b;I)V
    .locals 1
    .annotation build Landroidx/annotation/BinderThread;
    .end annotation

    iput-object p1, p0, Lf/h/a/f/f/k/b$l;->g:Lf/h/a/f/f/k/b;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lf/h/a/f/f/k/b$f;-><init>(Lf/h/a/f/f/k/b;ILandroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public final d(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/f/f/k/b$l;->g:Lf/h/a/f/f/k/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/f/k/b$l;->g:Lf/h/a/f/f/k/b;

    iget-object v0, v0, Lf/h/a/f/f/k/b;->j:Lf/h/a/f/f/k/b$c;

    invoke-interface {v0, p1}, Lf/h/a/f/f/k/b$c;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object p1, p0, Lf/h/a/f/f/k/b$l;->g:Lf/h/a/f/f/k/b;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    return-void
.end method

.method public final e()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/f/k/b$l;->g:Lf/h/a/f/f/k/b;

    iget-object v0, v0, Lf/h/a/f/f/k/b;->j:Lf/h/a/f/f/k/b$c;

    sget-object v1, Lcom/google/android/gms/common/ConnectionResult;->h:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {v0, v1}, Lf/h/a/f/f/k/b$c;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    const/4 v0, 0x1

    return v0
.end method
