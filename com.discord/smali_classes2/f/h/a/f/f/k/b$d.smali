.class public Lf/h/a/f/f/k/b$d;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-basement@@17.4.0"

# interfaces
.implements Lf/h/a/f/f/k/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/k/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "d"
.end annotation


# instance fields
.field public final synthetic a:Lf/h/a/f/f/k/b;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/k/b;)V
    .locals 0
    .param p1    # Lf/h/a/f/f/k/b;
        .annotation build Landroidx/annotation/RecentlyNonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/h/a/f/f/k/b$d;->a:Lf/h/a/f/f/k/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroidx/annotation/RecentlyNonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->N0()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/h/a/f/f/k/b$d;->a:Lf/h/a/f/f/k/b;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lf/h/a/f/f/k/b;->u()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lf/h/a/f/f/k/b;->b(Lf/h/a/f/f/k/f;Ljava/util/Set;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/f/k/b$d;->a:Lf/h/a/f/f/k/b;

    iget-object v0, v0, Lf/h/a/f/f/k/b;->p:Lf/h/a/f/f/k/b$b;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lf/h/a/f/f/k/b$b;->g(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_1
    return-void
.end method
