.class public abstract Lf/h/a/f/f/k/f$a;
.super Lf/h/a/f/i/e/a;
.source "com.google.android.gms:play-services-basement@@17.4.0"

# interfaces
.implements Lf/h/a/f/f/k/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/k/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/f/k/f$a$a;
    }
.end annotation


# direct methods
.method public static g(Landroid/os/IBinder;)Lf/h/a/f/f/k/f;
    .locals 2
    .param p0    # Landroid/os/IBinder;
        .annotation build Landroidx/annotation/RecentlyNonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/RecentlyNonNull;
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "com.google.android.gms.common.internal.IAccountAccessor"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lf/h/a/f/f/k/f;

    if-eqz v1, :cond_1

    check-cast v0, Lf/h/a/f/f/k/f;

    return-object v0

    :cond_1
    new-instance v0, Lf/h/a/f/f/k/f$a$a;

    invoke-direct {v0, p0}, Lf/h/a/f/f/k/f$a$a;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method
