.class public abstract Lf/h/a/f/f/k/d;
.super Lf/h/a/f/f/k/b;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Lf/h/a/f/f/h/a$f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "Lf/h/a/f/f/k/b<",
        "TT;>;",
        "Lf/h/a/f/f/h/a$f;",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final x:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation
.end field

.field public final y:Landroid/accounts/Account;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;ILf/h/a/f/f/k/c;Lf/h/a/f/f/h/i/f;Lf/h/a/f/f/h/i/l;)V
    .locals 9

    invoke-static {p1}, Lf/h/a/f/f/k/e;->a(Landroid/content/Context;)Lf/h/a/f/f/k/e;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/common/GoogleApiAvailability;->c:Ljava/lang/Object;

    sget-object v4, Lcom/google/android/gms/common/GoogleApiAvailability;->d:Lcom/google/android/gms/common/GoogleApiAvailability;

    const-string v0, "null reference"

    invoke-static {p5, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {p6, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v6, Lf/h/a/f/f/k/q;

    invoke-direct {v6, p5}, Lf/h/a/f/f/k/q;-><init>(Lf/h/a/f/f/h/i/f;)V

    new-instance v7, Lf/h/a/f/f/k/p;

    invoke-direct {v7, p6}, Lf/h/a/f/f/k/p;-><init>(Lf/h/a/f/f/h/i/l;)V

    iget-object v8, p4, Lf/h/a/f/f/k/c;->f:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v8}, Lf/h/a/f/f/k/b;-><init>(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/e;Lf/h/a/f/f/c;ILf/h/a/f/f/k/b$a;Lf/h/a/f/f/k/b$b;Ljava/lang/String;)V

    iget-object p1, p4, Lf/h/a/f/f/k/c;->a:Landroid/accounts/Account;

    iput-object p1, p0, Lf/h/a/f/f/k/d;->y:Landroid/accounts/Account;

    iget-object p1, p4, Lf/h/a/f/f/k/c;->c:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/google/android/gms/common/api/Scope;

    invoke-interface {p1, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Expanding scopes is not permitted, use implied scopes instead"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iput-object p1, p0, Lf/h/a/f/f/k/d;->x:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/f/k/b;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/f/k/d;->x:Ljava/util/Set;

    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final s()Landroid/accounts/Account;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/k/d;->y:Landroid/accounts/Account;

    return-object v0
.end method

.method public final u()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/k/d;->x:Ljava/util/Set;

    return-object v0
.end method
