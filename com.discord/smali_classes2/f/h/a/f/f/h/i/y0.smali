.class public Lf/h/a/f/f/h/i/y0;
.super Lf/h/a/f/f/h/i/r0;
.source "com.google.android.gms:play-services-base@@17.3.0"


# instance fields
.field public final i:Landroidx/collection/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/ArraySet<",
            "Lf/h/a/f/f/h/i/b<",
            "*>;>;"
        }
    .end annotation
.end field

.field public final j:Lf/h/a/f/f/h/i/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/j;Lf/h/a/f/f/h/i/g;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/GoogleApiAvailability;->c:Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/common/GoogleApiAvailability;->d:Lcom/google/android/gms/common/GoogleApiAvailability;

    invoke-direct {p0, p1, v0}, Lf/h/a/f/f/h/i/r0;-><init>(Lf/h/a/f/f/h/i/j;Lcom/google/android/gms/common/GoogleApiAvailability;)V

    new-instance p1, Landroidx/collection/ArraySet;

    invoke-direct {p1}, Landroidx/collection/ArraySet;-><init>()V

    iput-object p1, p0, Lf/h/a/f/f/h/i/y0;->i:Landroidx/collection/ArraySet;

    iput-object p2, p0, Lf/h/a/f/f/h/i/y0;->j:Lf/h/a/f/f/h/i/g;

    iget-object p1, p0, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->d:Lf/h/a/f/f/h/i/j;

    const-string p2, "ConnectionlessLifecycleHelper"

    invoke-interface {p1, p2, p0}, Lf/h/a/f/f/h/i/j;->a(Ljava/lang/String;Lcom/google/android/gms/common/api/internal/LifecycleCallback;)V

    return-void
.end method

.method public static o(Landroid/app/Activity;Lf/h/a/f/f/h/i/g;Lf/h/a/f/f/h/i/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lf/h/a/f/f/h/i/g;",
            "Lf/h/a/f/f/h/i/b<",
            "*>;)V"
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->c(Landroid/app/Activity;)Lf/h/a/f/f/h/i/j;

    move-result-object p0

    const-class v0, Lf/h/a/f/f/h/i/y0;

    const-string v1, "ConnectionlessLifecycleHelper"

    invoke-interface {p0, v1, v0}, Lf/h/a/f/f/h/i/j;->c(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/y0;

    if-nez v0, :cond_0

    new-instance v0, Lf/h/a/f/f/h/i/y0;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/f/h/i/y0;-><init>(Lf/h/a/f/f/h/i/j;Lf/h/a/f/f/h/i/g;)V

    :cond_0
    const-string p0, "ApiKey cannot be null"

    invoke-static {p2, p0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p0, v0, Lf/h/a/f/f/h/i/y0;->i:Landroidx/collection/ArraySet;

    invoke-virtual {p0, p2}, Landroidx/collection/ArraySet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v0}, Lf/h/a/f/f/h/i/g;->b(Lf/h/a/f/f/h/i/y0;)V

    return-void
.end method


# virtual methods
.method public g()V
    .locals 1

    iget-object v0, p0, Lf/h/a/f/f/h/i/y0;->i:Landroidx/collection/ArraySet;

    invoke-virtual {v0}, Landroidx/collection/ArraySet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/f/h/i/y0;->j:Lf/h/a/f/f/h/i/g;

    invoke-virtual {v0, p0}, Lf/h/a/f/f/h/i/g;->b(Lf/h/a/f/f/h/i/y0;)V

    :cond_0
    return-void
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/f/f/h/i/r0;->e:Z

    iget-object v0, p0, Lf/h/a/f/f/h/i/y0;->i:Landroidx/collection/ArraySet;

    invoke-virtual {v0}, Landroidx/collection/ArraySet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/f/h/i/y0;->j:Lf/h/a/f/f/h/i/g;

    invoke-virtual {v0, p0}, Lf/h/a/f/f/h/i/g;->b(Lf/h/a/f/f/h/i/y0;)V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/f/h/i/r0;->e:Z

    iget-object v0, p0, Lf/h/a/f/f/h/i/y0;->j:Lf/h/a/f/f/h/i/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/a/f/f/h/i/g;->r:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lf/h/a/f/f/h/i/g;->k:Lf/h/a/f/f/h/i/y0;

    if-ne v2, p0, :cond_0

    const/4 v2, 0x0

    iput-object v2, v0, Lf/h/a/f/f/h/i/g;->k:Lf/h/a/f/f/h/i/y0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final k()V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/f/h/i/y0;->j:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final l(Lcom/google/android/gms/common/ConnectionResult;I)V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/f/h/i/y0;->j:Lf/h/a/f/f/h/i/g;

    invoke-virtual {v0, p1, p2}, Lf/h/a/f/f/h/i/g;->c(Lcom/google/android/gms/common/ConnectionResult;I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method
