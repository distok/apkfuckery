.class public final Lf/h/a/f/f/h/i/a0;
.super Landroid/content/BroadcastReceiver;
.source "com.google.android.gms:play-services-base@@17.3.0"


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lf/h/a/f/f/h/i/s0;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/s0;)V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lf/h/a/f/f/h/i/a0;->b:Lf/h/a/f/f/h/i/s0;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/f/h/i/a0;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/f/f/h/i/a0;->a:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string p2, "com.google.android.gms"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lf/h/a/f/f/h/i/a0;->b:Lf/h/a/f/f/h/i/s0;

    iget-object p2, p1, Lf/h/a/f/f/h/i/s0;->b:Lf/h/a/f/f/h/i/t0;

    iget-object p2, p2, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    invoke-virtual {p2}, Lf/h/a/f/f/h/i/r0;->m()V

    iget-object p2, p1, Lf/h/a/f/f/h/i/s0;->a:Landroid/app/Dialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->isShowing()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p1, p1, Lf/h/a/f/f/h/i/s0;->a:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    invoke-virtual {p0}, Lf/h/a/f/f/h/i/a0;->a()V

    :cond_2
    return-void
.end method
