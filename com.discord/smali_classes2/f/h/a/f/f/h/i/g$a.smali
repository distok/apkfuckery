.class public final Lf/h/a/f/f/h/i/g$a;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Lf/h/a/f/f/h/c$a;
.implements Lf/h/a/f/f/h/c$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/h/i/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "Lf/h/a/f/f/h/a$d;",
        ">",
        "Ljava/lang/Object;",
        "Lf/h/a/f/f/h/c$a;",
        "Lf/h/a/f/f/h/c$b;",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lf/h/a/f/f/h/i/s;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/h/a/f/f/h/a$f;
    .annotation runtime Lorg/checkerframework/checker/initialization/qual/NotOnlyInitialized;
    .end annotation
.end field

.field public final c:Lf/h/a/f/f/h/a$b;

.field public final d:Lf/h/a/f/f/h/i/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/i/b<",
            "TO;>;"
        }
    .end annotation
.end field

.field public final e:Lf/h/a/f/f/h/i/v0;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/h/a/f/f/h/i/p0;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lf/h/a/f/f/h/i/k$a<",
            "*>;",
            "Lf/h/a/f/f/h/i/d0;",
            ">;"
        }
    .end annotation
.end field

.field public final h:I

.field public final i:Lf/h/a/f/f/h/i/g0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/f/f/h/i/g$c;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/google/android/gms/common/ConnectionResult;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final synthetic m:Lf/h/a/f/f/h/i/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/g;Lf/h/a/f/f/h/b;)V
    .locals 9
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/b<",
            "TO;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g$a;->f:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g$a;->g:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g$a;->k:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/f/f/h/i/g$a;->l:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {p2}, Lf/h/a/f/f/h/b;->a()Lf/h/a/f/f/k/c$a;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/f/k/c$a;->a()Lf/h/a/f/f/k/c;

    move-result-object v5

    iget-object v1, p2, Lf/h/a/f/f/h/b;->b:Lf/h/a/f/f/h/a;

    iget-object v2, v1, Lf/h/a/f/f/h/a;->a:Lf/h/a/f/f/h/a$a;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v3, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder"

    invoke-static {v2, v3}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    iget-object v2, v1, Lf/h/a/f/f/h/a;->a:Lf/h/a/f/f/h/a$a;

    const-string v1, "null reference"

    invoke-static {v2, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v3, p2, Lf/h/a/f/f/h/b;->a:Landroid/content/Context;

    iget-object v6, p2, Lf/h/a/f/f/h/b;->c:Lf/h/a/f/f/h/a$d;

    move-object v7, p0

    move-object v8, p0

    invoke-virtual/range {v2 .. v8}, Lf/h/a/f/f/h/a$a;->a(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/c;Ljava/lang/Object;Lf/h/a/f/f/h/c$a;Lf/h/a/f/f/h/c$b;)Lf/h/a/f/f/h/a$f;

    move-result-object v1

    iput-object v1, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    instance-of v2, v1, Lf/h/a/f/f/k/x;

    if-nez v2, :cond_2

    iput-object v1, p0, Lf/h/a/f/f/h/i/g$a;->c:Lf/h/a/f/f/h/a$b;

    iget-object v2, p2, Lf/h/a/f/f/h/b;->d:Lf/h/a/f/f/h/i/b;

    iput-object v2, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    new-instance v2, Lf/h/a/f/f/h/i/v0;

    invoke-direct {v2}, Lf/h/a/f/f/h/i/v0;-><init>()V

    iput-object v2, p0, Lf/h/a/f/f/h/i/g$a;->e:Lf/h/a/f/f/h/i/v0;

    iget v2, p2, Lf/h/a/f/f/h/b;->f:I

    iput v2, p0, Lf/h/a/f/f/h/i/g$a;->h:I

    invoke-interface {v1}, Lf/h/a/f/f/h/a$f;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p1, Lf/h/a/f/f/h/i/g;->e:Landroid/content/Context;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance v1, Lf/h/a/f/f/h/i/g0;

    invoke-virtual {p2}, Lf/h/a/f/f/h/b;->a()Lf/h/a/f/f/k/c$a;

    move-result-object p2

    invoke-virtual {p2}, Lf/h/a/f/f/k/c$a;->a()Lf/h/a/f/f/k/c;

    move-result-object p2

    invoke-direct {v1, v0, p1, p2}, Lf/h/a/f/f/h/i/g0;-><init>(Landroid/content/Context;Landroid/os/Handler;Lf/h/a/f/f/k/c;)V

    iput-object v1, p0, Lf/h/a/f/f/h/i/g$a;->i:Lf/h/a/f/f/h/i/g0;

    return-void

    :cond_1
    iput-object v0, p0, Lf/h/a/f/f/h/i/g$a;->i:Lf/h/a/f/f/h/i/g0;

    return-void

    :cond_2
    check-cast v1, Lf/h/a/f/f/k/x;

    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method


# virtual methods
.method public final a([Lcom/google/android/gms/common/Feature;)Lcom/google/android/gms/common/Feature;
    .locals 10
    .param p1    # [Lcom/google/android/gms/common/Feature;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    array-length v1, p1

    if-nez v1, :cond_0

    goto :goto_3

    :cond_0
    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v1}, Lf/h/a/f/f/h/a$f;->m()[Lcom/google/android/gms/common/Feature;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    new-array v1, v2, [Lcom/google/android/gms/common/Feature;

    :cond_1
    new-instance v3, Landroidx/collection/ArrayMap;

    array-length v4, v1

    invoke-direct {v3, v4}, Landroidx/collection/ArrayMap;-><init>(I)V

    array-length v4, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v6, v1, v5

    iget-object v7, v6, Lcom/google/android/gms/common/Feature;->d:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/gms/common/Feature;->M0()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v3, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    array-length v1, p1

    :goto_1
    if-ge v2, v1, :cond_5

    aget-object v4, p1, v2

    iget-object v5, v4, Lcom/google/android/gms/common/Feature;->d:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4}, Lcom/google/android/gms/common/Feature;->M0()J

    move-result-wide v7

    cmp-long v9, v5, v7

    if-gez v9, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    return-object v4

    :cond_5
    :goto_3
    return-object v0
.end method

.method public final b()V
    .locals 6
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    sget-object v0, Lf/h/a/f/f/h/i/g;->p:Lcom/google/android/gms/common/api/Status;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v1, v1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v1}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->e:Lf/h/a/f/f/h/i/v0;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/f/h/i/v0;->a(ZLcom/google/android/gms/common/api/Status;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v2, [Lf/h/a/f/f/h/i/k$a;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/a/f/f/h/i/k$a;

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    new-instance v4, Lf/h/a/f/f/h/i/n0;

    new-instance v5, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v5}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    invoke-direct {v4, v3, v5}, Lf/h/a/f/f/h/i/n0;-><init>(Lf/h/a/f/f/h/i/k$a;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-virtual {p0, v4}, Lf/h/a/f/f/h/i/g$a;->i(Lf/h/a/f/f/h/i/s;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(I)V

    invoke-virtual {p0, v0}, Lf/h/a/f/f/h/i/g$a;->m(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    new-instance v1, Lf/h/a/f/f/h/i/x;

    invoke-direct {v1, p0}, Lf/h/a/f/f/h/i/x;-><init>(Lf/h/a/f/f/h/i/g$a;)V

    invoke-interface {v0, v1}, Lf/h/a/f/f/h/a$f;->i(Lf/h/a/f/f/k/b$e;)V

    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 5
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->p()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/f/f/h/i/g$a;->j:Z

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->e:Lf/h/a/f/f/h/i/v0;

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v2}, Lf/h/a/f/f/h/a$f;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The connection to Google Play services was lost"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ne p1, v0, :cond_0

    const-string p1, " due to service disconnection."

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const/4 v4, 0x3

    if-ne p1, v4, :cond_1

    const-string p1, " due to dead object exception."

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    const-string p1, " Last reason for disconnect: "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    new-instance p1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x14

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v0, p1}, Lf/h/a/f/f/h/i/v0;->a(ZLcom/google/android/gms/common/api/Status;)V

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v0, 0x9

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-static {p1, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v1, 0x1388

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v0, 0xb

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-static {p1, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/32 v1, 0x1d4c0

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->g:Lf/h/a/f/f/k/r;

    iget-object p1, p1, Lf/h/a/f/f/k/r;->a:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Landroid/util/SparseIntArray;->clear()V

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->g:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/d0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/d0;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/Exception;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->i:Lf/h/a/f/f/h/i/g0;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->h()V

    :cond_0
    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->p()V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->g:Lf/h/a/f/f/k/r;

    iget-object v0, v0, Lf/h/a/f/f/k/r;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->m(Lcom/google/android/gms/common/ConnectionResult;)V

    iget v0, p1, Lcom/google/android/gms/common/ConnectionResult;->e:I

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-ne v0, v1, :cond_1

    sget-object p1, Lf/h/a/f/f/h/i/g;->p:Lcom/google/android/gms/common/api/Status;

    sget-object p1, Lf/h/a/f/f/h/i/g;->q:Lcom/google/android/gms/common/api/Status;

    iget-object p2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p2, p2, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {p2}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    invoke-virtual {p0, p1, v3, v2}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p1, p0, Lf/h/a/f/f/h/i/g$a;->l:Lcom/google/android/gms/common/ConnectionResult;

    return-void

    :cond_2
    if-eqz p2, :cond_3

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {p1}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    invoke-virtual {p0, v3, p2, v2}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    return-void

    :cond_3
    iget-object p2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-boolean p2, p2, Lf/h/a/f/f/h/i/g;->o:Z

    if-nez p2, :cond_4

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->o(Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/api/Status;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p2, p2, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {p2}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    invoke-virtual {p0, p1, v3, v2}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    return-void

    :cond_4
    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->o(Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/api/Status;

    move-result-object p2

    const/4 v0, 0x1

    invoke-virtual {p0, p2, v3, v0}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    iget-object p2, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {p2}, Ljava/util/Queue;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    return-void

    :cond_5
    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->k(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result p2

    if-eqz p2, :cond_6

    return-void

    :cond_6
    iget-object p2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget v1, p0, Lf/h/a/f/f/h/i/g$a;->h:I

    invoke-virtual {p2, p1, v1}, Lf/h/a/f/f/h/i/g;->c(Lcom/google/android/gms/common/ConnectionResult;I)Z

    move-result p2

    if-nez p2, :cond_9

    iget p2, p1, Lcom/google/android/gms/common/ConnectionResult;->e:I

    const/16 v1, 0x12

    if-ne p2, v1, :cond_7

    iput-boolean v0, p0, Lf/h/a/f/f/h/i/g$a;->j:Z

    :cond_7
    iget-boolean p2, p0, Lf/h/a/f/f/h/i/g$a;->j:Z

    if-eqz p2, :cond_8

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 p2, 0x9

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-static {p1, p2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p2

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v0, 0x1388

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void

    :cond_8
    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->o(Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/api/Status;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p2, p2, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {p2}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    invoke-virtual {p0, p1, v3, v2}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    :cond_9
    return-void
.end method

.method public final e(I)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v1, v1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->c(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance v1, Lf/h/a/f/f/h/i/v;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/f/h/i/v;-><init>(Lf/h/a/f/f/h/i/g$a;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/api/Status;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eq v2, v0, :cond_6

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/f/h/i/s;

    if-eqz p3, :cond_3

    iget v2, v1, Lf/h/a/f/f/h/i/s;->a:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {v1, p1}, Lf/h/a/f/f/h/i/s;->b(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v1, p2}, Lf/h/a/f/f/h/i/s;->e(Ljava/lang/Exception;)V

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_5
    return-void

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Status XOR exception should be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final g(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/f/h/i/g$a;->d(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/Exception;)V

    return-void
.end method

.method public final h(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->s()V

    return-void

    :cond_0
    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance v0, Lf/h/a/f/f/h/i/u;

    invoke-direct {v0, p0}, Lf/h/a/f/f/h/i/u;-><init>(Lf/h/a/f/f/h/i/g$a;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i(Lf/h/a/f/f/h/i/s;)V
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->l(Lf/h/a/f/f/h/i/s;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->v()V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->l:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->M0()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->l:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/f/h/i/g$a;->d(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/Exception;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->q()V

    return-void
.end method

.method public final j(Z)Z
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->j()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->e:Lf/h/a/f/f/h/i/v0;

    iget-object v2, v0, Lf/h/a/f/f/h/i/v0;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    iget-object v0, v0, Lf/h/a/f/f/h/i/v0;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->v()V

    :cond_2
    return v1

    :cond_3
    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    const-string v0, "Timing out service connection."

    invoke-interface {p1, v0}, Lf/h/a/f/f/h/a$f;->c(Ljava/lang/String;)V

    return v3

    :cond_4
    return v1
.end method

.method public final k(Lcom/google/android/gms/common/ConnectionResult;)Z
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    sget-object v0, Lf/h/a/f/f/h/i/g;->p:Lcom/google/android/gms/common/api/Status;

    sget-object v0, Lf/h/a/f/f/h/i/g;->r:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v2, v1, Lf/h/a/f/f/h/i/g;->k:Lf/h/a/f/f/h/i/y0;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lf/h/a/f/f/h/i/g;->l:Ljava/util/Set;

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v1, v1, Lf/h/a/f/f/h/i/g;->k:Lf/h/a/f/f/h/i/y0;

    iget v2, p0, Lf/h/a/f/f/h/i/g$a;->h:I

    invoke-virtual {v1, p1, v2}, Lf/h/a/f/f/h/i/r0;->n(Lcom/google/android/gms/common/ConnectionResult;I)V

    const/4 p1, 0x1

    monitor-exit v0

    return p1

    :cond_0
    const/4 p1, 0x0

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final l(Lf/h/a/f/f/h/i/s;)Z
    .locals 9
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    instance-of v0, p1, Lf/h/a/f/f/h/i/k0;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->n(Lf/h/a/f/f/h/i/s;)V

    return v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lf/h/a/f/f/h/i/k0;

    invoke-virtual {v0, p0}, Lf/h/a/f/f/h/i/k0;->f(Lf/h/a/f/f/h/i/g$a;)[Lcom/google/android/gms/common/Feature;

    move-result-object v2

    invoke-virtual {p0, v2}, Lf/h/a/f/f/h/i/g$a;->a([Lcom/google/android/gms/common/Feature;)Lcom/google/android/gms/common/Feature;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->n(Lf/h/a/f/f/h/i/s;)V

    return v1

    :cond_1
    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->c:Lf/h/a/f/f/h/a$b;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iget-object v3, v2, Lcom/google/android/gms/common/Feature;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/common/Feature;->M0()J

    move-result-wide v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x4d

    invoke-static {v3, v6}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v6

    const-string v7, " could not execute call because it requires feature ("

    const-string v8, ", "

    invoke-static {v6, p1, v7, v3, v8}, Lf/e/c/a/a;->F(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, ")."

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v3, "GoogleApiManager"

    invoke-static {v3, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-boolean p1, p1, Lf/h/a/f/f/h/i/g;->o:Z

    if-eqz p1, :cond_4

    invoke-virtual {v0, p0}, Lf/h/a/f/f/h/i/k0;->g(Lf/h/a/f/f/h/i/g$a;)Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p1, Lf/h/a/f/f/h/i/g$c;

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v2, v1}, Lf/h/a/f/f/h/i/g$c;-><init>(Lf/h/a/f/f/h/i/b;Lcom/google/android/gms/common/Feature;Lf/h/a/f/f/h/i/t;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const-wide/16 v2, 0x1388

    const/16 v4, 0xf

    if-ltz v0, :cond_2

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->k:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/f/h/i/g$c;

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-virtual {v0, v4, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0, v4, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0, v4, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    iget-object v5, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-static {v0, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    new-instance p1, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, 0x2

    invoke-direct {p1, v0, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g$a;->k(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget v1, p0, Lf/h/a/f/f/h/i/g$a;->h:I

    invoke-virtual {v0, p1, v1}, Lf/h/a/f/f/h/i/g;->c(Lcom/google/android/gms/common/ConnectionResult;I)Z

    :cond_3
    :goto_0
    const/4 p1, 0x0

    return p1

    :cond_4
    new-instance p1, Lcom/google/android/gms/common/api/UnsupportedApiCallException;

    invoke-direct {p1, v2}, Lcom/google/android/gms/common/api/UnsupportedApiCallException;-><init>(Lcom/google/android/gms/common/Feature;)V

    invoke-virtual {v0, p1}, Lf/h/a/f/f/h/i/s;->e(Ljava/lang/Exception;)V

    return v1
.end method

.method public final m(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/p0;

    sget-object v1, Lcom/google/android/gms/common/ConnectionResult;->h:Lcom/google/android/gms/common/ConnectionResult;

    invoke-static {p1, v1}, Lf/g/j/k/a;->U(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {p1}, Lf/h/a/f/f/h/a$f;->f()Ljava/lang/String;

    :cond_0
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    throw p1

    :cond_1
    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->f:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public final n(Lf/h/a/f/f/h/i/s;)V
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->e:Lf/h/a/f/f/h/i/v0;

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->r()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lf/h/a/f/f/h/i/s;->d(Lf/h/a/f/f/h/i/v0;Z)V

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, p0}, Lf/h/a/f/f/h/i/s;->c(Lf/h/a/f/f/h/i/g$a;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->c:Lf/h/a/f/f/h/a$b;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Error in GoogleApi implementation for client %s."

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_0
    invoke-virtual {p0, v0}, Lf/h/a/f/f/h/i/g$a;->e(I)V

    iget-object p1, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    const-string v0, "DeadObjectException thrown while running ApiCallRunner."

    invoke-interface {p1, v0}, Lf/h/a/f/f/h/a$f;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final o(Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/api/Status;
    .locals 5

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    iget-object v1, v1, Lf/h/a/f/f/h/i/b;->b:Lf/h/a/f/f/h/a;

    iget-object v1, v1, Lf/h/a/f/f/h/a;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/16 v2, 0x3f

    invoke-static {v1, v2}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v2

    const-string v2, "API: "

    const-string v4, " is not available on this device. Connection failed with: "

    invoke-static {v3, v2, v1, v4, p1}, Lf/e/c/a/a;->g(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/16 v1, 0x11

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    return-object v0
.end method

.method public final p()V
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/f/f/h/i/g$a;->l:Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method public final q()V
    .locals 10
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->j()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    const/16 v0, 0xa

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v2, v1, Lf/h/a/f/f/h/i/g;->g:Lf/h/a/f/f/k/r;

    iget-object v1, v1, Lf/h/a/f/f/h/i/g;->e:Landroid/content/Context;

    iget-object v3, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-virtual {v2, v1, v3}, Lf/h/a/f/f/k/r;->a(Landroid/content/Context;Lf/h/a/f/f/h/a$f;)I

    move-result v1

    if-eqz v1, :cond_1

    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    const-string v1, "GoogleApiManager"

    iget-object v4, p0, Lf/h/a/f/f/h/i/g$a;->c:Lf/h/a/f/f/h/a$b;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x23

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "The service for "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " is not available: "

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2, v3}, Lf/h/a/f/f/h/i/g$a;->d(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :cond_1
    new-instance v1, Lf/h/a/f/f/h/i/g$b;

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v3, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    iget-object v4, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-direct {v1, v2, v3, v4}, Lf/h/a/f/f/h/i/g$b;-><init>(Lf/h/a/f/f/h/i/g;Lf/h/a/f/f/h/a$f;Lf/h/a/f/f/h/i/b;)V

    invoke-interface {v3}, Lf/h/a/f/f/h/a$f;->o()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->i:Lf/h/a/f/f/h/i/g0;

    const-string v3, "null reference"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v3, v2, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lf/h/a/f/f/h/a$f;->h()V

    :cond_2
    iget-object v3, v2, Lf/h/a/f/f/h/i/g0;->e:Lf/h/a/f/f/k/c;

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lf/h/a/f/f/k/c;->h:Ljava/lang/Integer;

    iget-object v3, v2, Lf/h/a/f/f/h/i/g0;->c:Lf/h/a/f/f/h/a$a;

    iget-object v4, v2, Lf/h/a/f/f/h/i/g0;->a:Landroid/content/Context;

    iget-object v5, v2, Lf/h/a/f/f/h/i/g0;->b:Landroid/os/Handler;

    invoke-virtual {v5}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v5

    iget-object v6, v2, Lf/h/a/f/f/h/i/g0;->e:Lf/h/a/f/f/k/c;

    iget-object v7, v6, Lf/h/a/f/f/k/c;->g:Lf/h/a/f/n/a;

    move-object v8, v2

    move-object v9, v2

    invoke-virtual/range {v3 .. v9}, Lf/h/a/f/f/h/a$a;->a(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/c;Ljava/lang/Object;Lf/h/a/f/f/h/c$a;Lf/h/a/f/f/h/c$b;)Lf/h/a/f/f/h/a$f;

    move-result-object v3

    check-cast v3, Lf/h/a/f/n/f;

    iput-object v3, v2, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    iput-object v1, v2, Lf/h/a/f/f/h/i/g0;->g:Lf/h/a/f/f/h/i/h0;

    iget-object v3, v2, Lf/h/a/f/f/h/i/g0;->d:Ljava/util/Set;

    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_3
    iget-object v2, v2, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    invoke-interface {v2}, Lf/h/a/f/n/f;->p()V

    goto :goto_1

    :cond_4
    :goto_0
    iget-object v3, v2, Lf/h/a/f/f/h/i/g0;->b:Landroid/os/Handler;

    new-instance v4, Lf/h/a/f/f/h/i/f0;

    invoke-direct {v4, v2}, Lf/h/a/f/f/h/i/f0;-><init>(Lf/h/a/f/f/h/i/g0;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_5
    :goto_1
    :try_start_1
    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v2, v1}, Lf/h/a/f/f/h/a$f;->g(Lf/h/a/f/f/k/b$c;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    :catch_0
    move-exception v1

    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(I)V

    invoke-virtual {p0, v2, v1}, Lf/h/a/f/f/h/i/g$a;->d(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception v1

    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(I)V

    invoke-virtual {p0, v2, v1}, Lf/h/a/f/f/h/i/g$a;->d(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/Exception;)V

    :cond_6
    :goto_2
    return-void
.end method

.method public final r()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->o()Z

    move-result v0

    return v0
.end method

.method public final s()V
    .locals 5
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->p()V

    sget-object v0, Lcom/google/android/gms/common/ConnectionResult;->h:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0, v0}, Lf/h/a/f/f/h/i/g$a;->m(Lcom/google/android/gms/common/ConnectionResult;)V

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->u()V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/f/h/i/d0;

    iget-object v2, v1, Lf/h/a/f/f/h/i/d0;->a:Lf/h/a/f/f/h/i/m;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lf/h/a/f/f/h/i/g$a;->a([Lcom/google/android/gms/common/Feature;)Lcom/google/android/gms/common/Feature;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object v1, v1, Lf/h/a/f/f/h/i/d0;->a:Lf/h/a/f/f/h/i/m;

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->c:Lf/h/a/f/f/h/a$b;

    new-instance v3, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v3}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    check-cast v1, Lf/h/a/f/k/b/e/v;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v2, Lf/h/a/f/k/b/e/f;

    iget-object v4, v1, Lf/h/a/f/k/b/e/v;->b:Lf/h/a/f/k/b/e/a0;

    iget-object v1, v1, Lf/h/a/f/k/b/e/v;->c:Lf/h/a/f/k/b/e/i;

    invoke-static {v1, v3}, Lf/h/a/f/k/b/e/i;->j(Lf/h/a/f/k/b/e/i;Lcom/google/android/gms/tasks/TaskCompletionSource;)Lf/h/a/f/f/h/i/k;

    move-result-object v1

    invoke-interface {v4, v2, v1}, Lf/h/a/f/k/b/e/a0;->a(Lf/h/a/f/k/b/e/f;Lf/h/a/f/f/h/i/k;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :catch_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lf/h/a/f/f/h/i/g$a;->e(I)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    const-string v1, "DeadObjectException thrown while calling register listener method."

    invoke-interface {v0, v1}, Lf/h/a/f/f/h/a$f;->c(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->t()V

    invoke-virtual {p0}, Lf/h/a/f/f/h/i/g$a;->v()V

    return-void
.end method

.method public final t()V
    .locals 5
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    if-ge v2, v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    check-cast v3, Lf/h/a/f/f/h/i/s;

    iget-object v4, p0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {v4}, Lf/h/a/f/f/h/a$f;->j()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v3}, Lf/h/a/f/f/h/i/g$a;->l(Lf/h/a/f/f/h/i/s;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v4, v3}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final u()V
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/f/h/i/g$a;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v1, 0xb

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v1, 0x9

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/f/h/i/g$a;->j:Z

    :cond_0
    return-void
.end method

.method public final v()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$a;->d:Lf/h/a/f/f/h/i/b;

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-wide v2, v2, Lf/h/a/f/f/h/i/g;->d:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
