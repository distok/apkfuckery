.class public Lf/h/a/f/f/h/a$a;
.super Lf/h/a/f/f/h/a$e;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/h/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lf/h/a/f/f/h/a$f;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lf/h/a/f/f/h/a$e<",
        "TT;TO;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/a/f/f/h/a$e;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/c;Ljava/lang/Object;Lf/h/a/f/f/h/c$a;Lf/h/a/f/f/h/c$b;)Lf/h/a/f/f/h/a$f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "Lf/h/a/f/f/k/c;",
            "TO;",
            "Lf/h/a/f/f/h/c$a;",
            "Lf/h/a/f/f/h/c$b;",
            ")TT;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual/range {p0 .. p6}, Lf/h/a/f/f/h/a$a;->b(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/c;Ljava/lang/Object;Lf/h/a/f/f/h/i/f;Lf/h/a/f/f/h/i/l;)Lf/h/a/f/f/h/a$f;

    move-result-object p1

    return-object p1
.end method

.method public b(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/c;Ljava/lang/Object;Lf/h/a/f/f/h/i/f;Lf/h/a/f/f/h/i/l;)Lf/h/a/f/f/h/a$f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "Lf/h/a/f/f/k/c;",
            "TO;",
            "Lf/h/a/f/f/h/i/f;",
            "Lf/h/a/f/f/h/i/l;",
            ")TT;"
        }
    .end annotation

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "buildClient must be implemented"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
