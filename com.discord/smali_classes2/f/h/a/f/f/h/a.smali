.class public final Lf/h/a/f/f/h/a;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/f/h/a$f;,
        Lf/h/a/f/f/h/a$b;,
        Lf/h/a/f/f/h/a$g;,
        Lf/h/a/f/f/h/a$c;,
        Lf/h/a/f/f/h/a$d;,
        Lf/h/a/f/f/h/a$a;,
        Lf/h/a/f/f/h/a$e;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "Lf/h/a/f/f/h/a$d;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/f/f/h/a$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "*TO;>;"
        }
    .end annotation
.end field

.field public final b:Lf/h/a/f/f/h/a$g;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$g<",
            "*>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/h/a/f/f/h/a$a;Lf/h/a/f/f/h/a$g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Lf/h/a/f/f/h/a$f;",
            ">(",
            "Ljava/lang/String;",
            "Lf/h/a/f/f/h/a$a<",
            "TC;TO;>;",
            "Lf/h/a/f/f/h/a$g<",
            "TC;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Cannot construct an Api with a null ClientBuilder"

    invoke-static {p2, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Cannot construct an Api with a null ClientKey"

    invoke-static {p3, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/f/f/h/a;->c:Ljava/lang/String;

    iput-object p2, p0, Lf/h/a/f/f/h/a;->a:Lf/h/a/f/f/h/a$a;

    iput-object p3, p0, Lf/h/a/f/f/h/a;->b:Lf/h/a/f/f/h/a$g;

    return-void
.end method
