.class public final Lf/h/a/f/f/h/i/g$c;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/h/i/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:Lf/h/a/f/f/h/i/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/i/b<",
            "*>;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/gms/common/Feature;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/b;Lcom/google/android/gms/common/Feature;Lf/h/a/f/f/h/i/t;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    iput-object p2, p0, Lf/h/a/f/f/h/i/g$c;->b:Lcom/google/android/gms/common/Feature;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lf/h/a/f/f/h/i/g$c;

    if-eqz v1, :cond_0

    check-cast p1, Lf/h/a/f/f/h/i/g$c;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    iget-object v2, p1, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    invoke-static {v1, v2}, Lf/g/j/k/a;->U(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$c;->b:Lcom/google/android/gms/common/Feature;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g$c;->b:Lcom/google/android/gms/common/Feature;

    invoke-static {v1, p1}, Lf/g/j/k/a;->U(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$c;->b:Lcom/google/android/gms/common/Feature;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Lf/h/a/f/f/k/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lf/h/a/f/f/k/j;-><init>(Ljava/lang/Object;Lf/h/a/f/f/k/o0;)V

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    const-string v2, "key"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/f/k/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lf/h/a/f/f/k/j;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$c;->b:Lcom/google/android/gms/common/Feature;

    const-string v2, "feature"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/f/k/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lf/h/a/f/f/k/j;

    invoke-virtual {v0}, Lf/h/a/f/f/k/j;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
