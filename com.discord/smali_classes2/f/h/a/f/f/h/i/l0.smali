.class public final Lf/h/a/f/f/h/i/l0;
.super Lf/h/a/f/f/h/i/e0;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/h/i/e0<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lf/h/a/f/f/h/i/d0;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/d0;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/d0;",
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x3

    invoke-direct {p0, v0, p2}, Lf/h/a/f/f/h/i/e0;-><init>(ILcom/google/android/gms/tasks/TaskCompletionSource;)V

    iput-object p1, p0, Lf/h/a/f/f/h/i/l0;->c:Lf/h/a/f/f/h/i/d0;

    return-void
.end method


# virtual methods
.method public final bridge synthetic d(Lf/h/a/f/f/h/i/v0;Z)V
    .locals 0
    .param p1    # Lf/h/a/f/f/h/i/v0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public final f(Lf/h/a/f/f/h/i/g$a;)[Lcom/google/android/gms/common/Feature;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;)[",
            "Lcom/google/android/gms/common/Feature;"
        }
    .end annotation

    iget-object p1, p0, Lf/h/a/f/f/h/i/l0;->c:Lf/h/a/f/f/h/i/d0;

    iget-object p1, p1, Lf/h/a/f/f/h/i/d0;->a:Lf/h/a/f/f/h/i/m;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    return-object p1
.end method

.method public final g(Lf/h/a/f/f/h/i/g$a;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;)Z"
        }
    .end annotation

    iget-object p1, p0, Lf/h/a/f/f/h/i/l0;->c:Lf/h/a/f/f/h/i/d0;

    iget-object p1, p1, Lf/h/a/f/f/h/i/d0;->a:Lf/h/a/f/f/h/i/m;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    return p1
.end method

.method public final h(Lf/h/a/f/f/h/i/g$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/l0;->c:Lf/h/a/f/f/h/i/d0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/d0;->a:Lf/h/a/f/f/h/i/m;

    iget-object v1, p1, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    iget-object v2, p0, Lf/h/a/f/f/h/i/e0;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    check-cast v0, Lf/h/a/f/k/b/e/v;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lf/h/a/f/k/b/e/f;

    iget-object v3, v0, Lf/h/a/f/k/b/e/v;->b:Lf/h/a/f/k/b/e/a0;

    iget-object v0, v0, Lf/h/a/f/k/b/e/v;->c:Lf/h/a/f/k/b/e/i;

    invoke-static {v0, v2}, Lf/h/a/f/k/b/e/i;->j(Lf/h/a/f/k/b/e/i;Lcom/google/android/gms/tasks/TaskCompletionSource;)Lf/h/a/f/f/h/i/k;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Lf/h/a/f/k/b/e/a0;->a(Lf/h/a/f/k/b/e/f;Lf/h/a/f/f/h/i/k;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/l0;->c:Lf/h/a/f/f/h/i/d0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/d0;->a:Lf/h/a/f/f/h/i/m;

    iget-object v0, v0, Lf/h/a/f/f/h/i/m;->a:Lf/h/a/f/f/h/i/k;

    iget-object v0, v0, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lf/h/a/f/f/h/i/g$a;->g:Ljava/util/Map;

    iget-object v1, p0, Lf/h/a/f/f/h/i/l0;->c:Lf/h/a/f/f/h/i/d0;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
