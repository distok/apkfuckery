.class public final Lf/h/a/f/f/h/i/m0;
.super Lf/h/a/f/f/h/i/s;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Lf/h/a/f/f/h/i/d<",
        "+",
        "Lf/h/a/f/f/h/g;",
        "Lf/h/a/f/f/h/a$b;",
        ">;>",
        "Lf/h/a/f/f/h/i/s;"
    }
.end annotation


# instance fields
.field public final b:Lf/h/a/f/f/h/i/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILf/h/a/f/f/h/i/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITA;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/h/a/f/f/h/i/s;-><init>(I)V

    const-string p1, "Null methods are not runnable."

    invoke-static {p2, p1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lf/h/a/f/f/h/i/m0;->b:Lf/h/a/f/f/h/i/d;

    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/api/Status;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/f/f/h/i/m0;->b:Lf/h/a/f/f/h/i/d;

    invoke-virtual {v0, p1}, Lf/h/a/f/f/h/i/d;->a(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method public final c(Lf/h/a/f/f/h/i/g$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/DeadObjectException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/f/h/i/m0;->b:Lf/h/a/f/f/h/i/d;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-virtual {v0, p1}, Lf/h/a/f/f/h/i/d;->l(Lf/h/a/f/f/h/a$b;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/m0;->e(Ljava/lang/Exception;)V

    return-void
.end method

.method public final d(Lf/h/a/f/f/h/i/v0;Z)V
    .locals 2
    .param p1    # Lf/h/a/f/f/h/i/v0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/f/f/h/i/m0;->b:Lf/h/a/f/f/h/i/d;

    iget-object v1, p1, Lf/h/a/f/f/h/i/v0;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p2, Lf/h/a/f/f/h/i/x0;

    invoke-direct {p2, p1, v0}, Lf/h/a/f/f/h/i/x0;-><init>(Lf/h/a/f/f/h/i/v0;Lcom/google/android/gms/common/api/internal/BasePendingResult;)V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/api/internal/BasePendingResult;->c(Lf/h/a/f/f/h/d$a;)V

    return-void
.end method

.method public final e(Ljava/lang/Exception;)V
    .locals 4
    .param p1    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {p1, v2}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v2

    const-string v3, ": "

    invoke-static {v2, v1, v3, p1}, Lf/e/c/a/a;->f(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/16 v1, 0xa

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    iget-object p1, p0, Lf/h/a/f/f/h/i/m0;->b:Lf/h/a/f/f/h/i/d;

    invoke-virtual {p1, v0}, Lf/h/a/f/f/h/i/d;->a(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method
