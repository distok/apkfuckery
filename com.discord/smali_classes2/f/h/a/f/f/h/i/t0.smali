.class public final Lf/h/a/f/f/h/i/t0;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/f/h/i/q0;

.field public final synthetic e:Lf/h/a/f/f/h/i/r0;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/r0;Lf/h/a/f/f/h/i/q0;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/a/f/f/h/i/t0;->d:Lf/h/a/f/f/h/i/q0;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    iget-boolean v0, v0, Lf/h/a/f/f/h/i/r0;->e:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/f/h/i/t0;->d:Lf/h/a/f/f/h/i/q0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/q0;->b:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->M0()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    iget-object v4, v1, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->d:Lf/h/a/f/f/h/i/j;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->b()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/common/ConnectionResult;->f:Landroid/app/PendingIntent;

    const-string v5, "null reference"

    invoke-static {v0, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v5, p0, Lf/h/a/f/f/h/i/t0;->d:Lf/h/a/f/f/h/i/q0;

    iget v5, v5, Lf/h/a/f/f/h/i/q0;->a:I

    sget v6, Lcom/google/android/gms/common/api/GoogleApiActivity;->e:I

    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/google/android/gms/common/api/GoogleApiActivity;

    invoke-direct {v6, v1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "pending_intent"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "failing_client_id"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "notify_manager"

    invoke-virtual {v6, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-interface {v4, v6, v2}, Lf/h/a/f/f/h/i/j;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_1
    iget-object v1, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    iget-object v1, v1, Lf/h/a/f/f/h/i/r0;->h:Lcom/google/android/gms/common/GoogleApiAvailability;

    iget v4, v0, Lcom/google/android/gms/common/ConnectionResult;->e:I

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/GoogleApiAvailability;->d(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    iget-object v2, v1, Lf/h/a/f/f/h/i/r0;->h:Lcom/google/android/gms/common/GoogleApiAvailability;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->b()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    iget-object v4, v3, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->d:Lf/h/a/f/f/h/i/j;

    iget v0, v0, Lcom/google/android/gms/common/ConnectionResult;->e:I

    invoke-virtual {v2, v1, v4, v0, v3}, Lcom/google/android/gms/common/GoogleApiAvailability;->j(Landroid/app/Activity;Lf/h/a/f/f/h/i/j;ILandroid/content/DialogInterface$OnCancelListener;)Z

    return-void

    :cond_2
    iget v1, v0, Lcom/google/android/gms/common/ConnectionResult;->e:I

    const/16 v4, 0x12

    if-ne v1, v4, :cond_5

    iget-object v0, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->b()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    new-instance v5, Landroid/widget/ProgressBar;

    const/4 v6, 0x0

    const v7, 0x101007a

    invoke-direct {v5, v0, v6, v7}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v5, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    invoke-virtual {v5, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-static {v0, v4}, Lf/h/a/f/f/k/l;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v3, ""

    invoke-virtual {v2, v3, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    const-string v3, "GooglePlayServicesUpdatingDialog"

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->h(Landroid/app/Activity;Landroid/app/Dialog;Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    iget-object v1, v0, Lf/h/a/f/f/h/i/r0;->h:Lcom/google/android/gms/common/GoogleApiAvailability;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Lf/h/a/f/f/h/i/s0;

    invoke-direct {v3, p0, v2}, Lf/h/a/f/f/h/i/s0;-><init>(Lf/h/a/f/f/h/i/t0;Landroid/app/Dialog;)V

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v2, Lf/h/a/f/f/h/i/a0;

    invoke-direct {v2, v3}, Lf/h/a/f/f/h/i/a0;-><init>(Lf/h/a/f/f/h/i/s0;)V

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-object v0, v2, Lf/h/a/f/f/h/i/a0;->a:Landroid/content/Context;

    const-string v1, "com.google.android.gms"

    invoke-static {v0, v1}, Lf/h/a/f/f/e;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v3, Lf/h/a/f/f/h/i/s0;->b:Lf/h/a/f/f/h/i/t0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    invoke-virtual {v0}, Lf/h/a/f/f/h/i/r0;->m()V

    iget-object v0, v3, Lf/h/a/f/f/h/i/s0;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v3, Lf/h/a/f/f/h/i/s0;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_3
    invoke-virtual {v2}, Lf/h/a/f/f/h/i/a0;->a()V

    :cond_4
    return-void

    :cond_5
    iget-object v1, p0, Lf/h/a/f/f/h/i/t0;->e:Lf/h/a/f/f/h/i/r0;

    iget-object v2, p0, Lf/h/a/f/f/h/i/t0;->d:Lf/h/a/f/f/h/i/q0;

    iget v2, v2, Lf/h/a/f/f/h/i/q0;->a:I

    invoke-virtual {v1, v0, v2}, Lf/h/a/f/f/h/i/r0;->l(Lcom/google/android/gms/common/ConnectionResult;I)V

    return-void
.end method
