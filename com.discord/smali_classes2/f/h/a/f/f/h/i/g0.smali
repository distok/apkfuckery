.class public final Lf/h/a/f/f/h/i/g0;
.super Lf/h/a/f/n/b/d;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Lf/h/a/f/f/h/c$a;
.implements Lf/h/a/f/f/h/c$b;


# static fields
.field public static h:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "+",
            "Lf/h/a/f/n/f;",
            "Lf/h/a/f/n/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/Handler;

.field public final c:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "+",
            "Lf/h/a/f/n/f;",
            "Lf/h/a/f/n/a;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lf/h/a/f/f/k/c;

.field public f:Lf/h/a/f/n/f;

.field public g:Lf/h/a/f/f/h/i/h0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lf/h/a/f/n/c;->c:Lf/h/a/f/f/h/a$a;

    sput-object v0, Lf/h/a/f/f/h/i/g0;->h:Lf/h/a/f/f/h/a$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lf/h/a/f/f/k/c;)V
    .locals 1
    .param p3    # Lf/h/a/f/f/k/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    sget-object v0, Lf/h/a/f/f/h/i/g0;->h:Lf/h/a/f/f/h/a$a;

    invoke-direct {p0}, Lf/h/a/f/n/b/d;-><init>()V

    iput-object p1, p0, Lf/h/a/f/f/h/i/g0;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/h/a/f/f/h/i/g0;->b:Landroid/os/Handler;

    const-string p1, "ClientSettings must not be null"

    invoke-static {p3, p1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lf/h/a/f/f/h/i/g0;->e:Lf/h/a/f/f/k/c;

    iget-object p1, p3, Lf/h/a/f/f/k/c;->b:Ljava/util/Set;

    iput-object p1, p0, Lf/h/a/f/f/h/i/g0;->d:Ljava/util/Set;

    iput-object v0, p0, Lf/h/a/f/f/h/i/g0;->c:Lf/h/a/f/f/h/a$a;

    return-void
.end method


# virtual methods
.method public final e(I)V
    .locals 0
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object p1, p0, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    invoke-interface {p1}, Lf/h/a/f/f/h/a$f;->h()V

    return-void
.end method

.method public final g(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g0;->g:Lf/h/a/f/f/h/i/h0;

    check-cast v0, Lf/h/a/f/f/h/i/g$b;

    invoke-virtual {v0, p1}, Lf/h/a/f/f/h/i/g$b;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public final h(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object p1, p0, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    invoke-interface {p1, p0}, Lf/h/a/f/n/f;->d(Lf/h/a/f/n/b/c;)V

    return-void
.end method
