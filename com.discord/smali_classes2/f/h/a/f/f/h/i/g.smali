.class public Lf/h/a/f/f/h/i/g;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/f/h/i/g$c;,
        Lf/h/a/f/f/h/i/g$b;,
        Lf/h/a/f/f/h/i/g$a;
    }
.end annotation


# static fields
.field public static final p:Lcom/google/android/gms/common/api/Status;

.field public static final q:Lcom/google/android/gms/common/api/Status;

.field public static final r:Ljava/lang/Object;

.field public static s:Lf/h/a/f/f/h/i/g;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# instance fields
.field public d:J

.field public final e:Landroid/content/Context;

.field public final f:Lcom/google/android/gms/common/GoogleApiAvailability;

.field public final g:Lf/h/a/f/f/k/r;

.field public final h:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final i:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lf/h/a/f/f/h/i/b<",
            "*>;",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;>;"
        }
    .end annotation
.end field

.field public k:Lf/h/a/f/f/h/i/y0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/h/a/f/f/h/i/b<",
            "*>;>;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/h/a/f/f/h/i/b<",
            "*>;>;"
        }
    .end annotation
.end field

.field public final n:Landroid/os/Handler;
    .annotation runtime Lorg/checkerframework/checker/initialization/qual/NotOnlyInitialized;
    .end annotation
.end field

.field public volatile o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x4

    const-string v2, "Sign-out occurred while this API call was in progress."

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    sput-object v0, Lf/h/a/f/f/h/i/g;->p:Lcom/google/android/gms/common/api/Status;

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v2, "The user must be signed in to make this API call."

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    sput-object v0, Lf/h/a/f/f/h/i/g;->q:Lcom/google/android/gms/common/api/Status;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf/h/a/f/f/h/i/g;->r:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/GoogleApiAvailability;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lf/h/a/f/f/h/i/g;->d:J

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v3, 0x5

    const/high16 v4, 0x3f400000    # 0.75f

    invoke-direct {v0, v3, v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/f/f/h/i/g;->k:Lf/h/a/f/f/h/i/y0;

    new-instance v0, Landroidx/collection/ArraySet;

    invoke-direct {v0}, Landroidx/collection/ArraySet;-><init>()V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g;->l:Ljava/util/Set;

    new-instance v0, Landroidx/collection/ArraySet;

    invoke-direct {v0}, Landroidx/collection/ArraySet;-><init>()V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g;->m:Ljava/util/Set;

    iput-boolean v1, p0, Lf/h/a/f/f/h/i/g;->o:Z

    iput-object p1, p0, Lf/h/a/f/f/h/i/g;->e:Landroid/content/Context;

    new-instance v0, Lf/h/a/f/i/b/c;

    invoke-direct {v0, p2, p0}, Lf/h/a/f/i/b/c;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    iput-object p3, p0, Lf/h/a/f/f/h/i/g;->f:Lcom/google/android/gms/common/GoogleApiAvailability;

    new-instance p2, Lf/h/a/f/f/k/r;

    invoke-direct {p2, p3}, Lf/h/a/f/f/k/r;-><init>(Lf/h/a/f/f/c;)V

    iput-object p2, p0, Lf/h/a/f/f/h/i/g;->g:Lf/h/a/f/f/k/r;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    sget-object p2, Lf/g/j/k/a;->f:Ljava/lang/Boolean;

    if-nez p2, :cond_1

    invoke-static {}, Lf/h/a/f/f/n/g;->J()Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "android.hardware.type.automotive"

    invoke-virtual {p1, p2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    sput-object p1, Lf/g/j/k/a;->f:Ljava/lang/Boolean;

    :cond_1
    sget-object p1, Lf/g/j/k/a;->f:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    iput-boolean v2, p0, Lf/h/a/f/f/h/i/g;->o:Z

    :cond_2
    const/4 p1, 0x6

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public static a(Landroid/content/Context;)Lf/h/a/f/f/h/i/g;
    .locals 4

    sget-object v0, Lf/h/a/f/f/h/i/g;->r:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/a/f/f/h/i/g;->s:Lf/h/a/f/f/h/i/g;

    if-nez v1, :cond_0

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "GoogleApiHandler"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lf/h/a/f/f/h/i/g;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sget-object v3, Lcom/google/android/gms/common/GoogleApiAvailability;->c:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v3, Lcom/google/android/gms/common/GoogleApiAvailability;->d:Lcom/google/android/gms/common/GoogleApiAvailability;

    :try_start_1
    invoke-direct {v2, p0, v1, v3}, Lf/h/a/f/f/h/i/g;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/GoogleApiAvailability;)V

    sput-object v2, Lf/h/a/f/f/h/i/g;->s:Lf/h/a/f/f/h/i/g;

    :cond_0
    sget-object p0, Lf/h/a/f/f/h/i/g;->s:Lf/h/a/f/f/h/i/g;

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method


# virtual methods
.method public final b(Lf/h/a/f/f/h/i/y0;)V
    .locals 2
    .param p1    # Lf/h/a/f/f/h/i/y0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/h/a/f/f/h/i/g;->r:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/f/h/i/g;->k:Lf/h/a/f/f/h/i/y0;

    if-eq v1, p1, :cond_0

    iput-object p1, p0, Lf/h/a/f/f/h/i/g;->k:Lf/h/a/f/f/h/i/y0;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g;->l:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    :cond_0
    iget-object v1, p0, Lf/h/a/f/f/h/i/g;->l:Ljava/util/Set;

    iget-object p1, p1, Lf/h/a/f/f/h/i/y0;->i:Landroidx/collection/ArraySet;

    invoke-interface {v1, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final c(Lcom/google/android/gms/common/ConnectionResult;I)Z
    .locals 7

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->f:Lcom/google/android/gms/common/GoogleApiAvailability;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g;->e:Landroid/content/Context;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->M0()Z

    move-result v2

    const/high16 v3, 0x8000000

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/gms/common/ConnectionResult;->f:Landroid/app/PendingIntent;

    goto :goto_0

    :cond_0
    iget v2, p1, Lcom/google/android/gms/common/ConnectionResult;->e:I

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/gms/common/GoogleApiAvailability;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v2, v5

    goto :goto_0

    :cond_1
    invoke-static {v1, v4, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_2

    iget p1, p1, Lcom/google/android/gms/common/ConnectionResult;->e:I

    sget v5, Lcom/google/android/gms/common/api/GoogleApiActivity;->e:I

    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gms/common/api/GoogleApiActivity;

    invoke-direct {v5, v1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "pending_intent"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "failing_client_id"

    invoke-virtual {v5, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 p2, 0x1

    const-string v2, "notify_manager"

    invoke-virtual {v5, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v1, v4, v5, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/gms/common/GoogleApiAvailability;->i(Landroid/content/Context;ILandroid/app/PendingIntent;)V

    const/4 v4, 0x1

    :cond_2
    return v4
.end method

.method public final d(Lf/h/a/f/f/h/b;)Lf/h/a/f/f/h/i/g$a;
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/b<",
            "*>;)",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;"
        }
    .end annotation

    iget-object v0, p1, Lf/h/a/f/f/h/b;->d:Lf/h/a/f/f/h/i/b;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/f/h/i/g$a;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/a/f/f/h/i/g$a;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/f/h/i/g$a;-><init>(Lf/h/a/f/f/h/i/g;Lf/h/a/f/f/h/b;)V

    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v1}, Lf/h/a/f/f/h/i/g$a;->r()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->m:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v1}, Lf/h/a/f/f/h/i/g$a;->q()V

    return-object v1
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 10
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-wide/32 v3, 0x493e0

    const/4 v5, 0x0

    packed-switch v0, :pswitch_data_0

    const-string p1, "GoogleApiManager"

    const/16 v2, 0x1f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown message id: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :pswitch_0
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/f/f/h/i/g$c;

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v3, p1, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v3, p1, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/g$a;

    iget-object v3, v0, Lf/h/a/f/f/h/i/g$a;->k:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, v0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v3, v3, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v4, 0xf

    invoke-virtual {v3, v4, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v3, v0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v3, v3, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v4, 0x10

    invoke-virtual {v3, v4, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object p1, p1, Lf/h/a/f/f/h/i/g$c;->b:Lcom/google/android/gms/common/Feature;

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v4, v0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/f/f/h/i/s;

    instance-of v6, v5, Lf/h/a/f/f/h/i/k0;

    if-eqz v6, :cond_0

    move-object v6, v5

    check-cast v6, Lf/h/a/f/f/h/i/k0;

    invoke-virtual {v6, v0}, Lf/h/a/f/f/h/i/k0;->f(Lf/h/a/f/f/h/i/g$a;)[Lcom/google/android/gms/common/Feature;

    move-result-object v6

    if-eqz v6, :cond_0

    array-length v7, v6

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v7, :cond_2

    aget-object v9, v6, v8

    invoke-static {v9, p1}, Lf/g/j/k/a;->U(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, -0x1

    :goto_2
    if-ltz v8, :cond_3

    const/4 v6, 0x1

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    if-eqz v6, :cond_0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_4
    if-ge v1, v4, :cond_12

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    add-int/lit8 v1, v1, 0x1

    check-cast v5, Lf/h/a/f/f/h/i/s;

    iget-object v6, v0, Lf/h/a/f/f/h/i/g$a;->a:Ljava/util/Queue;

    invoke-interface {v6, v5}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/android/gms/common/api/UnsupportedApiCallException;

    invoke-direct {v6, p1}, Lcom/google/android/gms/common/api/UnsupportedApiCallException;-><init>(Lcom/google/android/gms/common/Feature;)V

    invoke-virtual {v5, v6}, Lf/h/a/f/f/h/i/s;->e(Ljava/lang/Exception;)V

    goto :goto_4

    :pswitch_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/f/f/h/i/g$c;

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v1, p1, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v1, p1, Lf/h/a/f/f/h/i/g$c;->a:Lf/h/a/f/f/h/i/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/g$a;

    iget-object v1, v0, Lf/h/a/f/f/h/i/g$a;->k:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto/16 :goto_a

    :cond_5
    iget-boolean p1, v0, Lf/h/a/f/f/h/i/g$a;->j:Z

    if-nez p1, :cond_12

    iget-object p1, v0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    invoke-interface {p1}, Lf/h/a/f/f/h/a$f;->j()Z

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {v0}, Lf/h/a/f/f/h/i/g$a;->q()V

    goto/16 :goto_a

    :cond_6
    invoke-virtual {v0}, Lf/h/a/f/f/h/i/g$a;->t()V

    goto/16 :goto_a

    :pswitch_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/f/f/h/i/z0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {p1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    throw v5

    :cond_7
    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/f/h/i/g$a;

    invoke-virtual {p1, v1}, Lf/h/a/f/f/h/i/g$a;->j(Z)Z

    throw v5

    :pswitch_3
    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/f/h/i/g$a;

    invoke-virtual {p1, v2}, Lf/h/a/f/f/h/i/g$a;->j(Z)Z

    goto/16 :goto_a

    :pswitch_4
    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/f/h/i/g$a;

    iget-object v0, p1, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    iget-boolean v0, p1, Lf/h/a/f/f/h/i/g$a;->j:Z

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lf/h/a/f/f/h/i/g$a;->u()V

    iget-object v0, p1, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v3, v0, Lf/h/a/f/f/h/i/g;->f:Lcom/google/android/gms/common/GoogleApiAvailability;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->e:Landroid/content/Context;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/GoogleApiAvailability;->c(Landroid/content/Context;)I

    move-result v0

    const/16 v3, 0x12

    const/16 v4, 0x8

    if-ne v0, v3, :cond_8

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v3, "Connection timed out while waiting for Google Play services update to complete."

    invoke-direct {v0, v4, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    goto :goto_5

    :cond_8
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v3, "API failed to connect while resuming due to an unknown error."

    invoke-direct {v0, v4, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    :goto_5
    iget-object v3, p1, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v3, v3, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v3}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    invoke-virtual {p1, v0, v5, v1}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    iget-object p1, p1, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    const-string v0, "Timing out connection while resuming."

    invoke-interface {p1, v0}, Lf/h/a/f/f/h/a$f;->c(Ljava/lang/String;)V

    goto/16 :goto_a

    :pswitch_5
    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->m:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_9
    :goto_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/b;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/g$a;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lf/h/a/f/f/h/i/g$a;->b()V

    goto :goto_6

    :cond_a
    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->m:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->clear()V

    goto/16 :goto_a

    :pswitch_6
    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/f/h/i/g$a;

    iget-object v0, p1, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v0}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    iget-boolean v0, p1, Lf/h/a/f/f/h/i/g$a;->j:Z

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lf/h/a/f/f/h/i/g$a;->q()V

    goto/16 :goto_a

    :pswitch_7
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/f/f/h/b;

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/g;->d(Lf/h/a/f/f/h/b;)Lf/h/a/f/f/h/i/g$a;

    goto/16 :goto_a

    :pswitch_8
    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->e:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    instance-of p1, p1, Landroid/app/Application;

    if-eqz p1, :cond_12

    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->e:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    invoke-static {p1}, Lf/h/a/f/f/h/i/c;->a(Landroid/app/Application;)V

    sget-object p1, Lf/h/a/f/f/h/i/c;->h:Lf/h/a/f/f/h/i/c;

    new-instance v0, Lf/h/a/f/f/h/i/t;

    invoke-direct {v0, p0}, Lf/h/a/f/f/h/i/t;-><init>(Lf/h/a/f/f/h/i/g;)V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    iget-object v1, p1, Lf/h/a/f/f/h/i/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p1, Lf/h/a/f/f/h/i/c;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_b

    new-instance v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    invoke-static {v0}, Landroid/app/ActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    iget-object v1, p1, Lf/h/a/f/f/h/i/c;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-nez v1, :cond_b

    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_b

    iget-object v0, p1, Lf/h/a/f/f/h/i/c;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_b
    iget-object p1, p1, Lf/h/a/f/f/h/i/c;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_12

    iput-wide v3, p0, Lf/h/a/f/f/h/i/g;->d:J

    goto/16 :goto_a

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :pswitch_9
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/gms/common/ConnectionResult;

    iget-object v3, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/f/f/h/i/g$a;

    iget v6, v4, Lf/h/a/f/f/h/i/g$a;->h:I

    if-ne v6, v0, :cond_c

    goto :goto_7

    :cond_d
    move-object v4, v5

    :goto_7
    if-eqz v4, :cond_e

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v3, 0x11

    iget-object v6, p0, Lf/h/a/f/f/h/i/g;->f:Lcom/google/android/gms/common/GoogleApiAvailability;

    iget v7, p1, Lcom/google/android/gms/common/ConnectionResult;->e:I

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v6, Lf/h/a/f/f/e;->a:Z

    invoke-static {v7}, Lcom/google/android/gms/common/ConnectionResult;->O0(I)Ljava/lang/String;

    move-result-object v6

    iget-object p1, p1, Lcom/google/android/gms/common/ConnectionResult;->g:Ljava/lang/String;

    const/16 v7, 0x45

    invoke-static {v6, v7}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v7

    invoke-static {p1, v7}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v7

    const-string v8, "Error resolution was canceled by the user, original error message: "

    const-string v9, ": "

    invoke-static {v7, v8, v6, v9, p1}, Lf/e/c/a/a;->g(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v3, p1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    iget-object p1, v4, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {p1}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    invoke-virtual {v4, v0, v5, v1}, Lf/h/a/f/f/h/i/g$a;->f(Lcom/google/android/gms/common/api/Status;Ljava/lang/Exception;Z)V

    goto/16 :goto_a

    :cond_e
    const-string p1, "GoogleApiManager"

    const/16 v1, 0x4c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Could not find API instance "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " while trying to fail enqueued calls."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-static {p1, v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_a

    :pswitch_a
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/f/f/h/i/c0;

    iget-object v0, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v1, p1, Lf/h/a/f/f/h/i/c0;->c:Lf/h/a/f/f/h/b;

    iget-object v1, v1, Lf/h/a/f/f/h/b;->d:Lf/h/a/f/f/h/i/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/g$a;

    if-nez v0, :cond_f

    iget-object v0, p1, Lf/h/a/f/f/h/i/c0;->c:Lf/h/a/f/f/h/b;

    invoke-virtual {p0, v0}, Lf/h/a/f/f/h/i/g;->d(Lf/h/a/f/f/h/b;)Lf/h/a/f/f/h/i/g$a;

    move-result-object v0

    :cond_f
    invoke-virtual {v0}, Lf/h/a/f/f/h/i/g$a;->r()Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, Lf/h/a/f/f/h/i/g;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    iget v3, p1, Lf/h/a/f/f/h/i/c0;->b:I

    if-eq v1, v3, :cond_10

    iget-object p1, p1, Lf/h/a/f/f/h/i/c0;->a:Lf/h/a/f/f/h/i/s;

    sget-object v1, Lf/h/a/f/f/h/i/g;->p:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1, v1}, Lf/h/a/f/f/h/i/s;->b(Lcom/google/android/gms/common/api/Status;)V

    invoke-virtual {v0}, Lf/h/a/f/f/h/i/g$a;->b()V

    goto :goto_a

    :cond_10
    iget-object p1, p1, Lf/h/a/f/f/h/i/c0;->a:Lf/h/a/f/f/h/i/s;

    invoke-virtual {v0, p1}, Lf/h/a/f/f/h/i/g$a;->i(Lf/h/a/f/f/h/i/s;)V

    goto :goto_a

    :pswitch_b
    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/g$a;

    invoke-virtual {v0}, Lf/h/a/f/f/h/i/g$a;->p()V

    invoke-virtual {v0}, Lf/h/a/f/f/h/i/g$a;->q()V

    goto :goto_8

    :pswitch_c
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/f/f/h/i/p0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    throw v5

    :pswitch_d
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_11

    const-wide/16 v3, 0x2710

    :cond_11
    iput-wide v3, p0, Lf/h/a/f/f/h/i/g;->d:J

    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p1, p0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_9
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/f/h/i/b;

    iget-object v3, p0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-virtual {v3, v0, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-wide v4, p0, Lf/h/a/f/f/h/i/g;->d:J

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_9

    :cond_12
    :goto_a
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_a
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_a
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
