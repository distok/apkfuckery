.class public final Lf/h/a/f/f/h/i/d1;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-basement@@17.4.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/common/api/internal/LifecycleCallback;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lf/h/a/f/f/h/i/c1;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/c1;Lcom/google/android/gms/common/api/internal/LifecycleCallback;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/f/h/i/d1;->f:Lf/h/a/f/f/h/i/c1;

    iput-object p2, p0, Lf/h/a/f/f/h/i/d1;->d:Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    iput-object p3, p0, Lf/h/a/f/f/h/i/d1;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->f:Lf/h/a/f/f/h/i/c1;

    iget v1, v0, Lf/h/a/f/f/h/i/c1;->e:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lf/h/a/f/f/h/i/d1;->d:Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    iget-object v0, v0, Lf/h/a/f/f/h/i/c1;->f:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lf/h/a/f/f/h/i/d1;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->e(Landroid/os/Bundle;)V

    :cond_1
    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->f:Lf/h/a/f/f/h/i/c1;

    iget v0, v0, Lf/h/a/f/f/h/i/c1;->e:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->d:Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->i()V

    :cond_2
    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->f:Lf/h/a/f/f/h/i/c1;

    iget v0, v0, Lf/h/a/f/f/h/i/c1;->e:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->d:Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->g()V

    :cond_3
    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->f:Lf/h/a/f/f/h/i/c1;

    iget v0, v0, Lf/h/a/f/f/h/i/c1;->e:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->d:Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->j()V

    :cond_4
    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->f:Lf/h/a/f/f/h/i/c1;

    iget v0, v0, Lf/h/a/f/f/h/i/c1;->e:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lf/h/a/f/f/h/i/d1;->d:Lcom/google/android/gms/common/api/internal/LifecycleCallback;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->f()V

    :cond_5
    return-void
.end method
