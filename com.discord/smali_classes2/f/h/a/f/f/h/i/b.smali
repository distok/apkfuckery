.class public final Lf/h/a/f/f/h/i/b;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "Lf/h/a/f/f/h/a$d;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Lf/h/a/f/f/h/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a<",
            "TO;>;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/f/f/h/a$d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TO;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;)V
    .locals 2
    .param p2    # Lf/h/a/f/f/h/a$d;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/a<",
            "TO;>;TO;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/f/h/i/b;->b:Lf/h/a/f/f/h/a;

    iput-object p2, p0, Lf/h/a/f/f/h/i/b;->c:Lf/h/a/f/f/h/a$d;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result p1

    iput p1, p0, Lf/h/a/f/f/h/i/b;->a:I

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    if-ne p1, p0, :cond_1

    return v1

    :cond_1
    instance-of v2, p1, Lf/h/a/f/f/h/i/b;

    if-nez v2, :cond_2

    return v0

    :cond_2
    check-cast p1, Lf/h/a/f/f/h/i/b;

    iget-object v2, p0, Lf/h/a/f/f/h/i/b;->b:Lf/h/a/f/f/h/a;

    iget-object v3, p1, Lf/h/a/f/f/h/i/b;->b:Lf/h/a/f/f/h/a;

    invoke-static {v2, v3}, Lf/g/j/k/a;->U(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lf/h/a/f/f/h/i/b;->c:Lf/h/a/f/f/h/a$d;

    iget-object p1, p1, Lf/h/a/f/f/h/i/b;->c:Lf/h/a/f/f/h/a$d;

    invoke-static {v2, p1}, Lf/g/j/k/a;->U(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    return v1

    :cond_3
    return v0
.end method

.method public final hashCode()I
    .locals 1

    iget v0, p0, Lf/h/a/f/f/h/i/b;->a:I

    return v0
.end method
