.class public final Lf/h/a/f/f/h/i/o0;
.super Lf/h/a/f/f/h/i/k0;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResultT:",
        "Ljava/lang/Object;",
        ">",
        "Lf/h/a/f/f/h/i/k0;"
    }
.end annotation


# instance fields
.field public final b:Lf/h/a/f/f/h/i/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/i/p<",
            "Lf/h/a/f/f/h/a$b;",
            "TResultT;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/google/android/gms/tasks/TaskCompletionSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "TResultT;>;"
        }
    .end annotation
.end field

.field public final d:Lf/h/a/f/f/h/i/n;


# direct methods
.method public constructor <init>(ILf/h/a/f/f/h/i/p;Lcom/google/android/gms/tasks/TaskCompletionSource;Lf/h/a/f/f/h/i/n;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/a/f/f/h/i/p<",
            "Lf/h/a/f/f/h/a$b;",
            "TResultT;>;",
            "Lcom/google/android/gms/tasks/TaskCompletionSource<",
            "TResultT;>;",
            "Lf/h/a/f/f/h/i/n;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/h/a/f/f/h/i/k0;-><init>(I)V

    iput-object p3, p0, Lf/h/a/f/f/h/i/o0;->c:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iput-object p2, p0, Lf/h/a/f/f/h/i/o0;->b:Lf/h/a/f/f/h/i/p;

    iput-object p4, p0, Lf/h/a/f/f/h/i/o0;->d:Lf/h/a/f/f/h/i/n;

    const/4 p3, 0x2

    if-ne p1, p3, :cond_0

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/api/Status;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/f/f/h/i/o0;->c:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, p0, Lf/h/a/f/f/h/i/o0;->d:Lf/h/a/f/f/h/i/n;

    invoke-interface {v1, p1}, Lf/h/a/f/f/h/i/n;->a(Lcom/google/android/gms/common/api/Status;)Ljava/lang/Exception;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    return-void
.end method

.method public final c(Lf/h/a/f/f/h/i/g$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/DeadObjectException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/f/h/i/o0;->b:Lf/h/a/f/f/h/i/p;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    iget-object v1, p0, Lf/h/a/f/f/h/i/o0;->c:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v0, p1, v1}, Lf/h/a/f/f/h/i/p;->c(Lf/h/a/f/f/h/a$b;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object v0, p0, Lf/h/a/f/f/h/i/o0;->c:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lf/h/a/f/f/h/i/s;->a(Landroid/os/RemoteException;)Lcom/google/android/gms/common/api/Status;

    move-result-object p1

    iget-object v0, p0, Lf/h/a/f/f/h/i/o0;->c:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, p0, Lf/h/a/f/f/h/i/o0;->d:Lf/h/a/f/f/h/i/n;

    invoke-interface {v1, p1}, Lf/h/a/f/f/h/i/n;->a(Lcom/google/android/gms/common/api/Status;)Ljava/lang/Exception;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    return-void

    :catch_2
    move-exception p1

    throw p1
.end method

.method public final d(Lf/h/a/f/f/h/i/v0;Z)V
    .locals 2
    .param p1    # Lf/h/a/f/f/h/i/v0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/f/f/h/i/o0;->c:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, p1, Lf/h/a/f/f/h/i/v0;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    new-instance v1, Lf/h/a/f/f/h/i/w0;

    invoke-direct {v1, p1, v0}, Lf/h/a/f/f/h/i/w0;-><init>(Lf/h/a/f/f/h/i/v0;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p2, p1, v1}, Lf/h/a/f/p/b0;->c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final e(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/f/f/h/i/o0;->c:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    return-void
.end method

.method public final f(Lf/h/a/f/f/h/i/g$a;)[Lcom/google/android/gms/common/Feature;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;)[",
            "Lcom/google/android/gms/common/Feature;"
        }
    .end annotation

    iget-object p1, p0, Lf/h/a/f/f/h/i/o0;->b:Lf/h/a/f/f/h/i/p;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    return-object p1
.end method

.method public final g(Lf/h/a/f/f/h/i/g$a;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/g$a<",
            "*>;)Z"
        }
    .end annotation

    iget-object p1, p0, Lf/h/a/f/f/h/i/o0;->b:Lf/h/a/f/f/h/i/p;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    return p1
.end method
