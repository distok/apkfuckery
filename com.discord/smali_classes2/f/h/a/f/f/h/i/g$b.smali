.class public final Lf/h/a/f/f/h/i/g$b;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Lf/h/a/f/f/h/i/h0;
.implements Lf/h/a/f/f/k/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/h/i/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final a:Lf/h/a/f/f/h/a$f;

.field public final b:Lf/h/a/f/f/h/i/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/i/b<",
            "*>;"
        }
    .end annotation
.end field

.field public c:Lf/h/a/f/f/k/f;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public final synthetic f:Lf/h/a/f/f/h/i/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/g;Lf/h/a/f/f/h/a$f;Lf/h/a/f/f/h/i/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/a$f;",
            "Lf/h/a/f/f/h/i/b<",
            "*>;)V"
        }
    .end annotation

    iput-object p1, p0, Lf/h/a/f/f/h/i/g$b;->f:Lf/h/a/f/f/h/i/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/f/f/h/i/g$b;->c:Lf/h/a/f/f/k/f;

    iput-object p1, p0, Lf/h/a/f/f/h/i/g$b;->d:Ljava/util/Set;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/f/f/h/i/g$b;->e:Z

    iput-object p2, p0, Lf/h/a/f/f/h/i/g$b;->a:Lf/h/a/f/f/h/a$f;

    iput-object p3, p0, Lf/h/a/f/f/h/i/g$b;->b:Lf/h/a/f/f/h/i/b;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$b;->f:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance v1, Lf/h/a/f/f/h/i/z;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/f/h/i/z;-><init>(Lf/h/a/f/f/h/i/g$b;Lcom/google/android/gms/common/ConnectionResult;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 7
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/g$b;->f:Lf/h/a/f/f/h/i/g;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->j:Ljava/util/Map;

    iget-object v1, p0, Lf/h/a/f/f/h/i/g$b;->b:Lf/h/a/f/f/h/i/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/f/h/i/g$a;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lf/h/a/f/f/h/i/g$a;->m:Lf/h/a/f/f/h/i/g;

    iget-object v1, v1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    invoke-static {v1}, Lf/g/j/k/a;->k(Landroid/os/Handler;)V

    iget-object v1, v0, Lf/h/a/f/f/h/i/g$a;->b:Lf/h/a/f/f/h/a$f;

    iget-object v2, v0, Lf/h/a/f/f/h/i/g$a;->c:Lf/h/a/f/f/h/a$b;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v4

    const-string v4, "onSignInFailed for "

    const-string v6, " with "

    invoke-static {v5, v4, v2, v6, v3}, Lf/e/c/a/a;->g(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lf/h/a/f/f/h/a$f;->c(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lf/h/a/f/f/h/i/g$a;->d(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method
