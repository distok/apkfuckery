.class public final Lf/h/a/f/f/h/i/i0;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/signin/internal/zam;

.field public final synthetic e:Lf/h/a/f/f/h/i/g0;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/g0;Lcom/google/android/gms/signin/internal/zam;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/f/h/i/i0;->e:Lf/h/a/f/f/h/i/g0;

    iput-object p2, p0, Lf/h/a/f/f/h/i/i0;->d:Lcom/google/android/gms/signin/internal/zam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lf/h/a/f/f/h/i/i0;->e:Lf/h/a/f/f/h/i/g0;

    iget-object v1, p0, Lf/h/a/f/f/h/i/i0;->d:Lcom/google/android/gms/signin/internal/zam;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/gms/signin/internal/zam;->e:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v2}, Lcom/google/android/gms/common/ConnectionResult;->N0()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v1, v1, Lcom/google/android/gms/signin/internal/zam;->f:Lcom/google/android/gms/common/internal/zau;

    const-string v2, "null reference"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/gms/common/internal/zau;->f:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v2}, Lcom/google/android/gms/common/ConnectionResult;->N0()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    const-string v4, "Sign-in succeeded with resolve account failure: "

    invoke-static {v3, v4, v1}, Lf/e/c/a/a;->e(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    const-string v4, "SignInCoordinator"

    invoke-static {v4, v1, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, v0, Lf/h/a/f/f/h/i/g0;->g:Lf/h/a/f/f/h/i/h0;

    check-cast v1, Lf/h/a/f/f/h/i/g$b;

    invoke-virtual {v1, v2}, Lf/h/a/f/f/h/i/g$b;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, v0, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->h()V

    goto :goto_2

    :cond_0
    iget-object v2, v0, Lf/h/a/f/f/h/i/g0;->g:Lf/h/a/f/f/h/i/h0;

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/zau;->M0()Lf/h/a/f/f/k/f;

    move-result-object v1

    iget-object v3, v0, Lf/h/a/f/f/h/i/g0;->d:Ljava/util/Set;

    check-cast v2, Lf/h/a/f/f/h/i/g$b;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_2

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    iput-object v1, v2, Lf/h/a/f/f/h/i/g$b;->c:Lf/h/a/f/f/k/f;

    iput-object v3, v2, Lf/h/a/f/f/h/i/g$b;->d:Ljava/util/Set;

    iget-boolean v4, v2, Lf/h/a/f/f/h/i/g$b;->e:Z

    if-eqz v4, :cond_4

    iget-object v2, v2, Lf/h/a/f/f/h/i/g$b;->a:Lf/h/a/f/f/h/a$f;

    invoke-interface {v2, v1, v3}, Lf/h/a/f/f/h/a$f;->b(Lf/h/a/f/f/k/f;Ljava/util/Set;)V

    goto :goto_1

    :cond_2
    :goto_0
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    const-string v3, "GoogleApiManager"

    const-string v4, "Received null response from onSignInSuccess"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v3, 0x4

    invoke-direct {v1, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(I)V

    invoke-virtual {v2, v1}, Lf/h/a/f/f/h/i/g$b;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_1

    :cond_3
    iget-object v1, v0, Lf/h/a/f/f/h/i/g0;->g:Lf/h/a/f/f/h/i/h0;

    check-cast v1, Lf/h/a/f/f/h/i/g$b;

    invoke-virtual {v1, v2}, Lf/h/a/f/f/h/i/g$b;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_4
    :goto_1
    iget-object v0, v0, Lf/h/a/f/f/h/i/g0;->f:Lf/h/a/f/n/f;

    invoke-interface {v0}, Lf/h/a/f/f/h/a$f;->h()V

    :goto_2
    return-void
.end method
