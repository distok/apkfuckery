.class public final Lf/h/a/f/f/h/i/y;
.super Lf/h/a/f/f/h/i/r;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "Lf/h/a/f/f/h/a$d;",
        ">",
        "Lf/h/a/f/f/h/i/r;"
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/f/f/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/b<",
            "TO;>;"
        }
    .end annotation

    .annotation runtime Lorg/checkerframework/checker/initialization/qual/NotOnlyInitialized;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/b<",
            "TO;>;)V"
        }
    .end annotation

    const-string v0, "Method is not supported by connectionless client. APIs supporting connectionless client must not call this method."

    invoke-direct {p0, v0}, Lf/h/a/f/f/h/i/r;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lf/h/a/f/f/h/i/y;->a:Lf/h/a/f/f/h/b;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/f/f/h/i/d;)Lf/h/a/f/f/h/i/d;
    .locals 2
    .param p1    # Lf/h/a/f/f/h/i/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lf/h/a/f/f/h/a$b;",
            "R::",
            "Lf/h/a/f/f/h/g;",
            "T:",
            "Lf/h/a/f/f/h/i/d<",
            "TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/f/h/i/y;->a:Lf/h/a/f/f/h/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/f/h/b;->d(ILf/h/a/f/f/h/i/d;)Lf/h/a/f/f/h/i/d;

    return-object p1
.end method

.method public final b()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/f/h/i/y;->a:Lf/h/a/f/f/h/b;

    iget-object v0, v0, Lf/h/a/f/f/h/b;->e:Landroid/os/Looper;

    return-object v0
.end method
