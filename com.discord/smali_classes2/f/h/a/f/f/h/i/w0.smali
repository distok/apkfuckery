.class public final Lf/h/a/f/f/h/i/w0;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Lf/h/a/f/p/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/c<",
        "TTResult;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/android/gms/tasks/TaskCompletionSource;

.field public final synthetic b:Lf/h/a/f/f/h/i/v0;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/v0;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/f/h/i/w0;->b:Lf/h/a/f/f/h/i/v0;

    iput-object p2, p0, Lf/h/a/f/f/h/i/w0;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/tasks/Task;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;)V"
        }
    .end annotation

    iget-object p1, p0, Lf/h/a/f/f/h/i/w0;->b:Lf/h/a/f/f/h/i/v0;

    iget-object p1, p1, Lf/h/a/f/f/h/i/v0;->b:Ljava/util/Map;

    iget-object v0, p0, Lf/h/a/f/f/h/i/w0;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
