.class public final Lf/h/a/f/f/h/i/k$c;
.super Lf/h/a/f/i/b/c;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/f/h/i/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field public final synthetic a:Lf/h/a/f/f/h/i/k;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/k;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/f/h/i/k$c;->a:Lf/h/a/f/f/h/i/k;

    invoke-direct {p0, p2}, Lf/h/a/f/i/b/c;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->g(Z)V

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/f/f/h/i/k$b;

    iget-object v0, p0, Lf/h/a/f/f/h/i/k$c;->a:Lf/h/a/f/f/h/i/k;

    iget-object v0, v0, Lf/h/a/f/f/h/i/k;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    invoke-interface {p1}, Lf/h/a/f/f/h/i/k$b;->b()V

    goto :goto_1

    :cond_1
    :try_start_0
    invoke-interface {p1, v0}, Lf/h/a/f/f/h/i/k$b;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-interface {p1}, Lf/h/a/f/f/h/i/k$b;->b()V

    throw v0
.end method
