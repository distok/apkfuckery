.class public Lf/h/a/f/f/h/b;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/f/h/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "Lf/h/a/f/f/h/a$d;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Object<",
        "TO;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/a/f/f/h/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a<",
            "TO;>;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/f/f/h/a$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TO;"
        }
    .end annotation
.end field

.field public final d:Lf/h/a/f/f/h/i/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/i/b<",
            "TO;>;"
        }
    .end annotation
.end field

.field public final e:Landroid/os/Looper;

.field public final f:I

.field public final g:Lf/h/a/f/f/h/c;
    .annotation runtime Lorg/checkerframework/checker/initialization/qual/NotOnlyInitialized;
    .end annotation
.end field

.field public final h:Lf/h/a/f/f/h/i/n;

.field public final i:Lf/h/a/f/f/h/i/g;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;Lf/h/a/f/f/h/b$a;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lf/h/a/f/f/h/a<",
            "TO;>;TO;",
            "Lf/h/a/f/f/h/b$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Null activity is not permitted."

    invoke-static {p1, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Api must not be null."

    invoke-static {p2, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead."

    invoke-static {p4, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/f/h/b;->a:Landroid/content/Context;

    invoke-static {p1}, Lf/h/a/f/f/h/b;->e(Ljava/lang/Object;)Ljava/lang/String;

    iput-object p2, p0, Lf/h/a/f/f/h/b;->b:Lf/h/a/f/f/h/a;

    iput-object p3, p0, Lf/h/a/f/f/h/b;->c:Lf/h/a/f/f/h/a$d;

    iget-object v1, p4, Lf/h/a/f/f/h/b$a;->b:Landroid/os/Looper;

    iput-object v1, p0, Lf/h/a/f/f/h/b;->e:Landroid/os/Looper;

    new-instance v1, Lf/h/a/f/f/h/i/b;

    invoke-direct {v1, p2, p3}, Lf/h/a/f/f/h/i/b;-><init>(Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;)V

    iput-object v1, p0, Lf/h/a/f/f/h/b;->d:Lf/h/a/f/f/h/i/b;

    new-instance p2, Lf/h/a/f/f/h/i/y;

    invoke-direct {p2, p0}, Lf/h/a/f/f/h/i/y;-><init>(Lf/h/a/f/f/h/b;)V

    iput-object p2, p0, Lf/h/a/f/f/h/b;->g:Lf/h/a/f/f/h/c;

    invoke-static {v0}, Lf/h/a/f/f/h/i/g;->a(Landroid/content/Context;)Lf/h/a/f/f/h/i/g;

    move-result-object p2

    iput-object p2, p0, Lf/h/a/f/f/h/b;->i:Lf/h/a/f/f/h/i/g;

    iget-object p3, p2, Lf/h/a/f/f/h/i/g;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result p3

    iput p3, p0, Lf/h/a/f/f/h/b;->f:I

    iget-object p3, p4, Lf/h/a/f/f/h/b$a;->a:Lf/h/a/f/f/h/i/n;

    iput-object p3, p0, Lf/h/a/f/f/h/b;->h:Lf/h/a/f/f/h/i/n;

    instance-of p3, p1, Lcom/google/android/gms/common/api/GoogleApiActivity;

    if-nez p3, :cond_0

    :try_start_0
    invoke-static {p1, p2, v1}, Lf/h/a/f/f/h/i/y0;->o(Landroid/app/Activity;Lf/h/a/f/f/h/i/g;Lf/h/a/f/f/h/i/b;)V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    iget-object p1, p0, Lf/h/a/f/f/h/b;->i:Lf/h/a/f/f/h/i/g;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/4 p2, 0x7

    invoke-virtual {p1, p2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;Lf/h/a/f/f/h/b$a;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lf/h/a/f/f/h/a<",
            "TO;>;TO;",
            "Lf/h/a/f/f/h/b$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Null context is not permitted."

    invoke-static {p1, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Api must not be null."

    invoke-static {p2, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead."

    invoke-static {p4, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/f/h/b;->a:Landroid/content/Context;

    invoke-static {p1}, Lf/h/a/f/f/h/b;->e(Ljava/lang/Object;)Ljava/lang/String;

    iput-object p2, p0, Lf/h/a/f/f/h/b;->b:Lf/h/a/f/f/h/a;

    iput-object p3, p0, Lf/h/a/f/f/h/b;->c:Lf/h/a/f/f/h/a$d;

    iget-object p1, p4, Lf/h/a/f/f/h/b$a;->b:Landroid/os/Looper;

    iput-object p1, p0, Lf/h/a/f/f/h/b;->e:Landroid/os/Looper;

    new-instance p1, Lf/h/a/f/f/h/i/b;

    invoke-direct {p1, p2, p3}, Lf/h/a/f/f/h/i/b;-><init>(Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;)V

    iput-object p1, p0, Lf/h/a/f/f/h/b;->d:Lf/h/a/f/f/h/i/b;

    new-instance p1, Lf/h/a/f/f/h/i/y;

    invoke-direct {p1, p0}, Lf/h/a/f/f/h/i/y;-><init>(Lf/h/a/f/f/h/b;)V

    iput-object p1, p0, Lf/h/a/f/f/h/b;->g:Lf/h/a/f/f/h/c;

    invoke-static {v0}, Lf/h/a/f/f/h/i/g;->a(Landroid/content/Context;)Lf/h/a/f/f/h/i/g;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/f/f/h/b;->i:Lf/h/a/f/f/h/i/g;

    iget-object p2, p1, Lf/h/a/f/f/h/i/g;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result p2

    iput p2, p0, Lf/h/a/f/f/h/b;->f:I

    iget-object p2, p4, Lf/h/a/f/f/h/b$a;->a:Lf/h/a/f/f/h/i/n;

    iput-object p2, p0, Lf/h/a/f/f/h/b;->h:Lf/h/a/f/f/h/i/n;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    const/4 p2, 0x7

    invoke-virtual {p1, p2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public static e(Ljava/lang/Object;)Ljava/lang/String;
    .locals 5
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x1d

    if-lt v0, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-nez v3, :cond_1

    goto :goto_2

    :cond_1
    const/16 v3, 0x1e

    if-lt v0, v3, :cond_2

    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v3, "REL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto/16 :goto_5

    :cond_2
    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v1, :cond_3

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x52

    if-lt v3, v4, :cond_3

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x5a

    if-gt v0, v3, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_4

    :goto_2
    const/4 v1, 0x0

    goto :goto_5

    :cond_4
    sget-object v0, Lf/h/a/f/f/n/g;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_5

    :cond_5
    :try_start_0
    const-string v0, "google"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Landroid/os/Build;->ID:Ljava/lang/String;

    const-string v3, "RPP1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->ID:Ljava/lang/String;

    const-string v3, "RPP2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const v3, 0x602711

    if-lt v0, v3, :cond_6

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lf/h/a/f/f/n/g;->a:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lf/h/a/f/f/n/g;->a:Ljava/lang/Boolean;

    :goto_4
    sget-object v0, Lf/h/a/f/f/n/g;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "PlatformVersion"

    const-string v1, "Build version must be at least 6301457 to support R in gmscore"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    sget-object v0, Lf/h/a/f/f/n/g;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_5
    if-eqz v1, :cond_8

    :try_start_1
    const-class v0, Landroid/content/Context;

    const-string v1, "getAttributionTag"

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p0

    :catch_1
    :cond_8
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public a()Lf/h/a/f/f/k/c$a;
    .locals 4

    new-instance v0, Lf/h/a/f/f/k/c$a;

    invoke-direct {v0}, Lf/h/a/f/f/k/c$a;-><init>()V

    iget-object v1, p0, Lf/h/a/f/f/h/b;->c:Lf/h/a/f/f/h/a$d;

    instance-of v2, v1, Lf/h/a/f/f/h/a$d$b;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    check-cast v1, Lf/h/a/f/f/h/a$d$b;

    invoke-interface {v1}, Lf/h/a/f/f/h/a$d$b;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->g:Ljava/lang/String;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Landroid/accounts/Account;

    iget-object v1, v1, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->g:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-direct {v2, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lf/h/a/f/f/h/b;->c:Lf/h/a/f/f/h/a$d;

    instance-of v2, v1, Lf/h/a/f/f/h/a$d$a;

    if-eqz v2, :cond_2

    check-cast v1, Lf/h/a/f/f/h/a$d$a;

    invoke-interface {v1}, Lf/h/a/f/f/h/a$d$a;->b()Landroid/accounts/Account;

    move-result-object v3

    :cond_2
    :goto_0
    iput-object v3, v0, Lf/h/a/f/f/k/c$a;->a:Landroid/accounts/Account;

    iget-object v1, p0, Lf/h/a/f/f/h/b;->c:Lf/h/a/f/f/h/a$d;

    instance-of v2, v1, Lf/h/a/f/f/h/a$d$b;

    if-eqz v2, :cond_3

    check-cast v1, Lf/h/a/f/f/h/a$d$b;

    invoke-interface {v1}, Lf/h/a/f/f/h/a$d$b;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->M0()Ljava/util/Set;

    move-result-object v1

    goto :goto_1

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    :goto_1
    iget-object v2, v0, Lf/h/a/f/f/k/c$a;->b:Landroidx/collection/ArraySet;

    if-nez v2, :cond_4

    new-instance v2, Landroidx/collection/ArraySet;

    invoke-direct {v2}, Landroidx/collection/ArraySet;-><init>()V

    iput-object v2, v0, Lf/h/a/f/f/k/c$a;->b:Landroidx/collection/ArraySet;

    :cond_4
    iget-object v2, v0, Lf/h/a/f/f/k/c$a;->b:Landroidx/collection/ArraySet;

    invoke-virtual {v2, v1}, Landroidx/collection/ArraySet;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lf/h/a/f/f/h/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lf/h/a/f/f/k/c$a;->d:Ljava/lang/String;

    iget-object v1, p0, Lf/h/a/f/f/h/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lf/h/a/f/f/k/c$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lf/h/a/f/f/h/i/k$a;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .param p1    # Lf/h/a/f/f/h/i/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/h/i/k$a<",
            "*>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "Listener key cannot be null."

    invoke-static {p1, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/f/h/b;->i:Lf/h/a/f/f/h/i/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v1}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v2, Lf/h/a/f/f/h/i/n0;

    invoke-direct {v2, p1, v1}, Lf/h/a/f/f/h/i/n0;-><init>(Lf/h/a/f/f/h/i/k$a;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    iget-object p1, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance v3, Lf/h/a/f/f/h/i/c0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {v3, v2, v0, p0}, Lf/h/a/f/f/h/i/c0;-><init>(Lf/h/a/f/f/h/i/s;ILf/h/a/f/f/h/b;)V

    const/16 v0, 0xd

    invoke-virtual {p1, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object p1, v1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object p1
.end method

.method public c(Lf/h/a/f/f/h/i/p;)Lcom/google/android/gms/tasks/Task;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            "A::",
            "Lf/h/a/f/f/h/a$b;",
            ">(",
            "Lf/h/a/f/f/h/i/p<",
            "TA;TTResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iget-object v1, p0, Lf/h/a/f/f/h/b;->i:Lf/h/a/f/f/h/i/g;

    iget-object v2, p0, Lf/h/a/f/f/h/b;->h:Lf/h/a/f/f/h/i/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lf/h/a/f/f/h/i/o0;

    const/4 v4, 0x1

    invoke-direct {v3, v4, p1, v0, v2}, Lf/h/a/f/f/h/i/o0;-><init>(ILf/h/a/f/f/h/i/p;Lcom/google/android/gms/tasks/TaskCompletionSource;Lf/h/a/f/f/h/i/n;)V

    iget-object p1, v1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance v2, Lf/h/a/f/f/h/i/c0;

    iget-object v1, v1, Lf/h/a/f/f/h/i/g;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-direct {v2, v3, v1, p0}, Lf/h/a/f/f/h/i/c0;-><init>(Lf/h/a/f/f/h/i/s;ILf/h/a/f/f/h/b;)V

    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object p1, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object p1
.end method

.method public final d(ILf/h/a/f/f/h/i/d;)Lf/h/a/f/f/h/i/d;
    .locals 3
    .param p2    # Lf/h/a/f/f/h/i/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lf/h/a/f/f/h/a$b;",
            "T:",
            "Lf/h/a/f/f/h/i/d<",
            "+",
            "Lf/h/a/f/f/h/g;",
            "TA;>;>(ITT;)TT;"
        }
    .end annotation

    iget-boolean v0, p2, Lcom/google/android/gms/common/api/internal/BasePendingResult;->j:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/api/internal/BasePendingResult;->k:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p2, Lcom/google/android/gms/common/api/internal/BasePendingResult;->j:Z

    iget-object v0, p0, Lf/h/a/f/f/h/b;->i:Lf/h/a/f/f/h/i/g;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/f/h/i/m0;

    invoke-direct {v1, p1, p2}, Lf/h/a/f/f/h/i/m0;-><init>(ILf/h/a/f/f/h/i/d;)V

    iget-object p1, v0, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance v2, Lf/h/a/f/f/h/i/c0;

    iget-object v0, v0, Lf/h/a/f/f/h/i/g;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {v2, v1, v0, p0}, Lf/h/a/f/f/h/i/c0;-><init>(Lf/h/a/f/f/h/i/s;ILf/h/a/f/f/h/b;)V

    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-object p2
.end method
