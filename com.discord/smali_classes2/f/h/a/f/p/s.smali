.class public final Lf/h/a/f/p/s;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-tasks@@17.2.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/tasks/Task;

.field public final synthetic e:Lf/h/a/f/p/t;


# direct methods
.method public constructor <init>(Lf/h/a/f/p/t;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/p/s;->e:Lf/h/a/f/p/t;

    iput-object p2, p0, Lf/h/a/f/p/s;->d:Lcom/google/android/gms/tasks/Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/p/s;->e:Lf/h/a/f/p/t;

    iget-object v0, v0, Lf/h/a/f/p/t;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/p/s;->e:Lf/h/a/f/p/t;

    iget-object v1, v1, Lf/h/a/f/p/t;->c:Lf/h/a/f/p/d;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lf/h/a/f/p/s;->d:Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v2}, Lcom/google/android/gms/tasks/Task;->k()Ljava/lang/Exception;

    move-result-object v2

    const-string v3, "null reference"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-interface {v1, v2}, Lf/h/a/f/p/d;->onFailure(Ljava/lang/Exception;)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
