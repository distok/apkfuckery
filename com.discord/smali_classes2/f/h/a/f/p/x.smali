.class public final Lf/h/a/f/p/x;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-tasks@@17.2.0"

# interfaces
.implements Lf/h/a/f/p/b;
.implements Lf/h/a/f/p/d;
.implements Lf/h/a/f/p/e;
.implements Lf/h/a/f/p/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        "TContinuationResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/b;",
        "Lf/h/a/f/p/d;",
        "Lf/h/a/f/p/e<",
        "TTContinuationResult;>;",
        "Lf/h/a/f/p/z<",
        "TTResult;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Lf/h/a/f/p/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/p/f<",
            "TTResult;TTContinuationResult;>;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/f/p/b0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/p/b0<",
            "TTContinuationResult;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;Lf/h/a/f/p/b0;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lf/h/a/f/p/b0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/f<",
            "TTResult;TTContinuationResult;>;",
            "Lf/h/a/f/p/b0<",
            "TTContinuationResult;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/p/x;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lf/h/a/f/p/x;->b:Lf/h/a/f/p/f;

    iput-object p3, p0, Lf/h/a/f/p/x;->c:Lf/h/a/f/p/b0;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/tasks/Task;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/tasks/Task;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/x;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lf/h/a/f/p/w;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/p/w;-><init>(Lf/h/a/f/p/x;Lcom/google/android/gms/tasks/Task;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lf/h/a/f/p/x;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v0}, Lf/h/a/f/p/b0;->u()Z

    return-void
.end method

.method public final onFailure(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/f/p/x;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v0, p1}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTContinuationResult;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/x;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v0, p1}, Lf/h/a/f/p/b0;->t(Ljava/lang/Object;)V

    return-void
.end method
