.class public final Lf/h/a/f/p/m;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-tasks@@17.2.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/tasks/Task;

.field public final synthetic e:Lf/h/a/f/p/k;


# direct methods
.method public constructor <init>(Lf/h/a/f/p/k;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/p/m;->e:Lf/h/a/f/p/k;

    iput-object p2, p0, Lf/h/a/f/p/m;->d:Lcom/google/android/gms/tasks/Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/p/m;->d:Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/Task;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/p/m;->e:Lf/h/a/f/p/k;

    iget-object v0, v0, Lf/h/a/f/p/k;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v0}, Lf/h/a/f/p/b0;->u()Z

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/h/a/f/p/m;->e:Lf/h/a/f/p/k;

    iget-object v0, v0, Lf/h/a/f/p/k;->b:Lf/h/a/f/p/a;

    iget-object v1, p0, Lf/h/a/f/p/m;->d:Lcom/google/android/gms/tasks/Task;

    invoke-interface {v0, v1}, Lf/h/a/f/p/a;->a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Lcom/google/android/gms/tasks/RuntimeExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lf/h/a/f/p/m;->e:Lf/h/a/f/p/k;

    iget-object v1, v1, Lf/h/a/f/p/k;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->t(Ljava/lang/Object;)V

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/f/p/m;->e:Lf/h/a/f/p/k;

    iget-object v1, v1, Lf/h/a/f/p/k;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Exception;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/a/f/p/m;->e:Lf/h/a/f/p/k;

    iget-object v1, v1, Lf/h/a/f/p/k;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void

    :cond_1
    iget-object v1, p0, Lf/h/a/f/p/m;->e:Lf/h/a/f/p/k;

    iget-object v1, v1, Lf/h/a/f/p/k;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void
.end method
