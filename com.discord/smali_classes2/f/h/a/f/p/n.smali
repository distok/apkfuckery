.class public final Lf/h/a/f/p/n;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-tasks@@17.2.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lcom/google/android/gms/tasks/Task;

.field public final synthetic e:Lf/h/a/f/p/l;


# direct methods
.method public constructor <init>(Lf/h/a/f/p/l;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    iput-object p2, p0, Lf/h/a/f/p/n;->d:Lcom/google/android/gms/tasks/Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    iget-object v0, v0, Lf/h/a/f/p/l;->b:Lf/h/a/f/p/a;

    iget-object v1, p0, Lf/h/a/f/p/n;->d:Lcom/google/android/gms/tasks/Task;

    invoke-interface {v0, v1}, Lf/h/a/f/p/a;->a(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/tasks/Task;
    :try_end_0
    .catch Lcom/google/android/gms/tasks/RuntimeExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Continuation returned null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lf/h/a/f/p/l;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v0, v1}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void

    :cond_0
    sget-object v1, Lf/h/a/f/p/g;->b:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->g(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)Lcom/google/android/gms/tasks/Task;

    iget-object v2, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->e(Ljava/util/concurrent/Executor;Lf/h/a/f/p/d;)Lcom/google/android/gms/tasks/Task;

    iget-object v2, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->a(Ljava/util/concurrent/Executor;Lf/h/a/f/p/b;)Lcom/google/android/gms/tasks/Task;

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    iget-object v1, v1, Lf/h/a/f/p/l;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Exception;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    iget-object v1, v1, Lf/h/a/f/p/l;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void

    :cond_1
    iget-object v1, p0, Lf/h/a/f/p/n;->e:Lf/h/a/f/p/l;

    iget-object v1, v1, Lf/h/a/f/p/l;->c:Lf/h/a/f/p/b0;

    invoke-virtual {v1, v0}, Lf/h/a/f/p/b0;->s(Ljava/lang/Exception;)V

    return-void
.end method
