.class public final Lf/h/a/f/p/b0;
.super Lcom/google/android/gms/tasks/Task;
.source "com.google.android.gms:play-services-tasks@@17.2.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/gms/tasks/Task<",
        "TTResult;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Lf/h/a/f/p/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/p/y<",
            "TTResult;>;"
        }
    .end annotation
.end field

.field public c:Z

.field public volatile d:Z

.field public e:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTResult;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/tasks/Task;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/p/y;

    invoke-direct {v0}, Lf/h/a/f/p/y;-><init>()V

    iput-object v0, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/Executor;Lf/h/a/f/p/b;)Lcom/google/android/gms/tasks/Task;
    .locals 3
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/b;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v1, Lf/h/a/f/p/p;

    sget v2, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v1, p1, p2}, Lf/h/a/f/p/p;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/b;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->w()V

    return-object p0
.end method

.method public final b(Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;
    .locals 1
    .param p1    # Lf/h/a/f/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/p/c<",
            "TTResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lf/h/a/f/p/b0;->c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;

    return-object p0
.end method

.method public final c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;
    .locals 3
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/c<",
            "TTResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v1, Lf/h/a/f/p/q;

    sget v2, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v1, p1, p2}, Lf/h/a/f/p/q;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->w()V

    return-object p0
.end method

.method public final d(Lf/h/a/f/p/d;)Lcom/google/android/gms/tasks/Task;
    .locals 1
    .param p1    # Lf/h/a/f/p/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/p/d;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lf/h/a/f/p/b0;->e(Ljava/util/concurrent/Executor;Lf/h/a/f/p/d;)Lcom/google/android/gms/tasks/Task;

    return-object p0
.end method

.method public final e(Ljava/util/concurrent/Executor;Lf/h/a/f/p/d;)Lcom/google/android/gms/tasks/Task;
    .locals 3
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/d;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v1, Lf/h/a/f/p/t;

    sget v2, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v1, p1, p2}, Lf/h/a/f/p/t;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/d;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->w()V

    return-object p0
.end method

.method public final f(Lf/h/a/f/p/e;)Lcom/google/android/gms/tasks/Task;
    .locals 1
    .param p1    # Lf/h/a/f/p/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/p/e<",
            "-TTResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lf/h/a/f/p/b0;->g(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)Lcom/google/android/gms/tasks/Task;

    return-object p0
.end method

.method public final g(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)Lcom/google/android/gms/tasks/Task;
    .locals 3
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/e<",
            "-TTResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTResult;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v1, Lf/h/a/f/p/u;

    sget v2, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v1, p1, p2}, Lf/h/a/f/p/u;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->w()V

    return-object p0
.end method

.method public final h(Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;
    .locals 1
    .param p1    # Lf/h/a/f/p/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/h/a/f/p/a<",
            "TTResult;TTContinuationResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTContinuationResult;>;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lf/h/a/f/p/b0;->i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final i(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/a<",
            "TTResult;TTContinuationResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTContinuationResult;>;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/p/b0;

    invoke-direct {v0}, Lf/h/a/f/p/b0;-><init>()V

    iget-object v1, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v2, Lf/h/a/f/p/k;

    sget v3, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v2, p1, p2, v0}, Lf/h/a/f/p/k;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;Lf/h/a/f/p/b0;)V

    invoke-virtual {v1, v2}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->w()V

    return-object v0
.end method

.method public final j(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/a<",
            "TTResult;",
            "Lcom/google/android/gms/tasks/Task<",
            "TTContinuationResult;>;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTContinuationResult;>;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/p/b0;

    invoke-direct {v0}, Lf/h/a/f/p/b0;-><init>()V

    iget-object v1, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v2, Lf/h/a/f/p/l;

    sget v3, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v2, p1, p2, v0}, Lf/h/a/f/p/l;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;Lf/h/a/f/p/b0;)V

    invoke-virtual {v1, v2}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->w()V

    return-object v0
.end method

.method public final k()Ljava/lang/Exception;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final l()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTResult;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    const-string v2, "Task is not yet complete"

    invoke-static {v1, v2}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    iget-boolean v1, p0, Lf/h/a/f/p/b0;->d:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/p/b0;->e:Ljava/lang/Object;

    monitor-exit v0

    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/gms/tasks/RuntimeExecutionException;

    iget-object v2, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    invoke-direct {v1, v2}, Lcom/google/android/gms/tasks/RuntimeExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    new-instance v1, Ljava/util/concurrent/CancellationException;

    const-string v2, "Task is already canceled."

    invoke-direct {v1, v2}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final m(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Class;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Throwable;",
            ">(",
            "Ljava/lang/Class<",
            "TX;>;)TTResult;^TX;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    const-string v2, "Task is not yet complete"

    invoke-static {v1, v2}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    iget-boolean v1, p0, Lf/h/a/f/p/b0;->d:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object p1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    if-nez p1, :cond_0

    iget-object p1, p0, Lf/h/a/f/p/b0;->e:Ljava/lang/Object;

    monitor-exit v0

    return-object p1

    :cond_0
    new-instance p1, Lcom/google/android/gms/tasks/RuntimeExecutionException;

    iget-object v1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    invoke-direct {p1, v1}, Lcom/google/android/gms/tasks/RuntimeExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :cond_1
    iget-object v1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    new-instance p1, Ljava/util/concurrent/CancellationException;

    const-string v1, "Task is already canceled."

    invoke-direct {p1, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/p/b0;->d:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final p()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lf/h/a/f/p/b0;->d:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final q(Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;
    .locals 1
    .param p1    # Lf/h/a/f/p/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/h/a/f/p/f<",
            "TTResult;TTContinuationResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTContinuationResult;>;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lf/h/a/f/p/b0;->r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final r(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TContinuationResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lf/h/a/f/p/f<",
            "TTResult;TTContinuationResult;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TTContinuationResult;>;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/p/b0;

    invoke-direct {v0}, Lf/h/a/f/p/b0;-><init>()V

    iget-object v1, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v2, Lf/h/a/f/p/x;

    sget v3, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v2, p1, p2, v0}, Lf/h/a/f/p/x;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/f;Lf/h/a/f/p/b0;)V

    invoke-virtual {v1, v2}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->w()V

    return-object v0
.end method

.method public final s(Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "Exception must not be null"

    invoke-static {p1, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/f/p/b0;->v()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    iput-object p1, p0, Lf/h/a/f/p/b0;->f:Ljava/lang/Exception;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    invoke-virtual {p1, p0}, Lf/h/a/f/p/y;->a(Lcom/google/android/gms/tasks/Task;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final t(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResult;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/f/p/b0;->v()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    iput-object p1, p0, Lf/h/a/f/p/b0;->e:Ljava/lang/Object;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    invoke-virtual {p1, p0}, Lf/h/a/f/p/y;->a(Lcom/google/android/gms/tasks/Task;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final u()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    monitor-exit v0

    return v1

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    iput-boolean v1, p0, Lf/h/a/f/p/b0;->d:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    invoke-virtual {v0, p0}, Lf/h/a/f/p/y;->a(Lcom/google/android/gms/tasks/Task;)V

    return v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public final v()V
    .locals 5

    iget-boolean v0, p0, Lf/h/a/f/p/b0;->c:Z

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/gms/tasks/DuplicateTaskCompletionException;->d:I

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->k()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "failure"

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lf/h/a/f/p/b0;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lf/h/a/f/p/b0;->l()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x7

    const-string v3, "result "

    invoke-static {v2, v3, v1}, Lf/e/c/a/a;->e(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/h/a/f/p/b0;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "cancellation"

    goto :goto_0

    :cond_2
    const-string v1, "unknown issue"

    :goto_0
    new-instance v2, Lcom/google/android/gms/tasks/DuplicateTaskCompletionException;

    const-string v3, "Complete with: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/tasks/DuplicateTaskCompletionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v0, "DuplicateTaskCompletionException can only be created from completed Task."

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    :goto_2
    throw v2

    :cond_5
    return-void
.end method

.method public final w()V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/p/b0;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lf/h/a/f/p/b0;->c:Z

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    invoke-virtual {v0, p0}, Lf/h/a/f/p/y;->a(Lcom/google/android/gms/tasks/Task;)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
