.class public Lf/h/a/f/d/a$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lf/h/a/f/i/c/i4;

.field public final e:Lf/h/a/f/i/c/r4;

.field public f:Z

.field public final synthetic g:Lf/h/a/f/d/a;


# direct methods
.method public constructor <init>(Lf/h/a/f/d/a;[BLf/h/a/f/d/b;)V
    .locals 5

    iput-object p1, p0, Lf/h/a/f/d/a$a;->g:Lf/h/a/f/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget p3, p1, Lf/h/a/f/d/a;->e:I

    iput p3, p0, Lf/h/a/f/d/a$a;->a:I

    iget-object p3, p1, Lf/h/a/f/d/a;->d:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/f/d/a$a;->b:Ljava/lang/String;

    iget-object p3, p1, Lf/h/a/f/d/a;->f:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/f/d/a$a;->c:Ljava/lang/String;

    iget-object p3, p1, Lf/h/a/f/d/a;->h:Lf/h/a/f/i/c/i4;

    iput-object p3, p0, Lf/h/a/f/d/a$a;->d:Lf/h/a/f/i/c/i4;

    new-instance p3, Lf/h/a/f/i/c/r4;

    invoke-direct {p3}, Lf/h/a/f/i/c/r4;-><init>()V

    iput-object p3, p0, Lf/h/a/f/d/a$a;->e:Lf/h/a/f/i/c/r4;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/d/a$a;->f:Z

    iget-object v1, p1, Lf/h/a/f/d/a;->f:Ljava/lang/String;

    iput-object v1, p0, Lf/h/a/f/d/a$a;->c:Ljava/lang/String;

    iget-object v1, p1, Lf/h/a/f/d/a;->a:Landroid/content/Context;

    sget-object v2, Lf/h/a/f/i/c/a;->a:Landroid/os/UserManager;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v3, 0x1

    const/16 v4, 0x18

    if-lt v2, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_5

    sget-boolean v2, Lf/h/a/f/i/c/a;->b:Z

    if-nez v2, :cond_4

    sget-object v2, Lf/h/a/f/i/c/a;->a:Landroid/os/UserManager;

    if-nez v2, :cond_3

    const-class v4, Lf/h/a/f/i/c/a;

    monitor-enter v4

    :try_start_0
    sget-object v2, Lf/h/a/f/i/c/a;->a:Landroid/os/UserManager;

    if-nez v2, :cond_2

    const-class v2, Landroid/os/UserManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    sput-object v1, Lf/h/a/f/i/c/a;->a:Landroid/os/UserManager;

    if-nez v1, :cond_1

    sput-boolean v3, Lf/h/a/f/i/c/a;->b:Z

    monitor-exit v4

    const/4 v2, 0x1

    goto :goto_2

    :cond_1
    move-object v2, v1

    :cond_2
    monitor-exit v4

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {v2}, Landroid/os/UserManager;->isUserUnlocked()Z

    move-result v2

    sput-boolean v2, Lf/h/a/f/i/c/a;->b:Z

    if-eqz v2, :cond_4

    const/4 v1, 0x0

    sput-object v1, Lf/h/a/f/i/c/a;->a:Landroid/os/UserManager;

    :cond_4
    :goto_2
    if-nez v2, :cond_5

    const/4 v0, 0x1

    :cond_5
    iput-boolean v0, p3, Lf/h/a/f/i/c/r4;->v:Z

    iget-object v0, p1, Lf/h/a/f/d/a;->j:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p3, Lf/h/a/f/i/c/r4;->f:J

    iget-object p1, p1, Lf/h/a/f/d/a;->j:Lf/h/a/f/f/n/c;

    check-cast p1, Lf/h/a/f/f/n/d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p3, Lf/h/a/f/i/c/r4;->g:J

    iget-wide v0, p3, Lf/h/a/f/i/c/r4;->f:J

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result p1

    div-int/lit16 p1, p1, 0x3e8

    int-to-long v0, p1

    iput-wide v0, p3, Lf/h/a/f/i/c/r4;->p:J

    if-eqz p2, :cond_6

    iput-object p2, p3, Lf/h/a/f/i/c/r4;->k:[B

    :cond_6
    return-void
.end method


# virtual methods
.method public a()V
    .locals 20

    move-object/from16 v1, p0

    iget-boolean v0, v1, Lf/h/a/f/d/a$a;->f:Z

    if-nez v0, :cond_1a

    const/4 v2, 0x1

    iput-boolean v2, v1, Lf/h/a/f/d/a$a;->f:Z

    new-instance v10, Lcom/google/android/gms/clearcut/zze;

    new-instance v4, Lcom/google/android/gms/internal/clearcut/zzr;

    iget-object v0, v1, Lf/h/a/f/d/a$a;->g:Lf/h/a/f/d/a;

    iget-object v12, v0, Lf/h/a/f/d/a;->b:Ljava/lang/String;

    iget v13, v0, Lf/h/a/f/d/a;->c:I

    iget v14, v1, Lf/h/a/f/d/a$a;->a:I

    iget-object v15, v1, Lf/h/a/f/d/a$a;->b:Ljava/lang/String;

    iget-object v3, v1, Lf/h/a/f/d/a$a;->c:Ljava/lang/String;

    const/16 v17, 0x0

    iget-boolean v0, v0, Lf/h/a/f/d/a;->g:Z

    iget-object v5, v1, Lf/h/a/f/d/a$a;->d:Lf/h/a/f/i/c/i4;

    move-object v11, v4

    move-object/from16 v16, v3

    move/from16 v18, v0

    move-object/from16 v19, v5

    invoke-direct/range {v11 .. v19}, Lcom/google/android/gms/internal/clearcut/zzr;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLf/h/a/f/i/c/i4;)V

    iget-object v5, v1, Lf/h/a/f/d/a$a;->e:Lf/h/a/f/i/c/r4;

    const/4 v6, 0x0

    sget-object v0, Lf/h/a/f/d/a;->m:Lf/h/a/f/f/h/a$g;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v3, v10

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/clearcut/zze;-><init>(Lcom/google/android/gms/internal/clearcut/zzr;Lf/h/a/f/i/c/r4;Lf/h/a/f/d/a$c;[I[IZ)V

    iget-object v0, v1, Lf/h/a/f/d/a$a;->g:Lf/h/a/f/d/a;

    iget-object v0, v0, Lf/h/a/f/d/a;->l:Lf/h/a/f/d/a$b;

    move-object v3, v0

    check-cast v3, Lf/h/a/f/i/c/z4;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v10, Lcom/google/android/gms/clearcut/zze;->d:Lcom/google/android/gms/internal/clearcut/zzr;

    iget-object v4, v0, Lcom/google/android/gms/internal/clearcut/zzr;->j:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/gms/internal/clearcut/zzr;->f:I

    iget-object v5, v10, Lcom/google/android/gms/clearcut/zze;->l:Lf/h/a/f/i/c/r4;

    sget-object v5, Lf/h/a/f/i/c/z4;->i:Lf/h/a/f/i/c/e;

    invoke-virtual {v5}, Lf/h/a/f/i/c/e;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x0

    if-nez v5, :cond_10

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_0

    :cond_0
    if-ltz v0, :cond_1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    move-object v4, v7

    :goto_0
    if-eqz v4, :cond_18

    iget-object v0, v3, Lf/h/a/f/i/c/z4;->a:Landroid/content/Context;

    if-eqz v0, :cond_4

    invoke-static {v0}, Lf/h/a/f/i/c/z4;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lf/h/a/f/i/c/z4;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/f/i/c/e;

    if-nez v5, :cond_3

    sget-object v5, Lf/h/a/f/i/c/z4;->d:Lf/h/a/f/i/c/o;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v9, Lf/h/a/f/i/c/e;->g:Ljava/lang/Object;

    new-instance v9, Lf/h/a/f/i/c/k;

    invoke-direct {v9, v5, v4, v7}, Lf/h/a/f/i/c/k;-><init>(Lf/h/a/f/i/c/o;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v5, v9

    :cond_3
    invoke-virtual {v5}, Lf/h/a/f/i/c/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    goto :goto_2

    :cond_4
    :goto_1
    move-object v4, v7

    :goto_2
    if-nez v4, :cond_5

    goto/16 :goto_9

    :cond_5
    const/16 v0, 0x2c

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_6

    invoke-virtual {v4, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    add-int/2addr v0, v2

    goto :goto_3

    :cond_6
    const-string v5, ""

    const/4 v0, 0x0

    :goto_3
    const/16 v9, 0x2f

    invoke-virtual {v4, v9, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v9

    const-string v11, "LogSamplerImpl"

    if-gtz v9, :cond_8

    const-string v0, "Failed to parse the rule: "

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :cond_7
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v0, v4

    goto/16 :goto_7

    :cond_8
    :try_start_0
    invoke-virtual {v4, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    add-int/2addr v9, v2

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v16, 0x0

    cmp-long v0, v12, v16

    if-ltz v0, :cond_e

    cmp-long v0, v14, v16

    if-gez v0, :cond_9

    goto :goto_6

    :cond_9
    invoke-static {}, Lf/h/a/f/i/c/m4$b;->v()Lf/h/a/f/i/c/m4$b$a;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/c/y0$a;->g()V

    iget-object v4, v0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    check-cast v4, Lf/h/a/f/i/c/m4$b;

    invoke-static {v4, v5}, Lf/h/a/f/i/c/m4$b;->p(Lf/h/a/f/i/c/m4$b;Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/i/c/y0$a;->g()V

    iget-object v4, v0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    check-cast v4, Lf/h/a/f/i/c/m4$b;

    invoke-static {v4, v12, v13}, Lf/h/a/f/i/c/m4$b;->o(Lf/h/a/f/i/c/m4$b;J)V

    invoke-virtual {v0}, Lf/h/a/f/i/c/y0$a;->g()V

    iget-object v4, v0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    check-cast v4, Lf/h/a/f/i/c/m4$b;

    invoke-static {v4, v14, v15}, Lf/h/a/f/i/c/m4$b;->q(Lf/h/a/f/i/c/m4$b;J)V

    invoke-virtual {v0}, Lf/h/a/f/i/c/y0$a;->m()Lf/h/a/f/i/c/b2;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/y0;

    invoke-virtual {v0, v2, v7, v7}, Lf/h/a/f/i/c/y0;->d(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    if-ne v4, v2, :cond_a

    const/4 v8, 0x1

    goto :goto_5

    :cond_a
    if-nez v4, :cond_b

    goto :goto_5

    :cond_b
    sget-object v4, Lf/h/a/f/i/c/m2;->c:Lf/h/a/f/i/c/m2;

    invoke-virtual {v4, v0}, Lf/h/a/f/i/c/m2;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/r2;

    move-result-object v4

    invoke-interface {v4, v0}, Lf/h/a/f/i/c/r2;->i(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    move-object v4, v0

    goto :goto_4

    :cond_c
    move-object v4, v7

    :goto_4
    invoke-virtual {v0, v6, v4, v7}, Lf/h/a/f/i/c/y0;->d(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    if-eqz v8, :cond_d

    check-cast v0, Lf/h/a/f/i/c/m4$b;

    goto :goto_a

    :cond_d
    new-instance v0, Lcom/google/android/gms/internal/clearcut/zzew;

    invoke-direct {v0}, Lcom/google/android/gms/internal/clearcut/zzew;-><init>()V

    throw v0

    :cond_e
    :goto_6
    const/16 v0, 0x48

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "negative values not supported: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-static {v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    :catch_0
    move-exception v0

    const-string v5, "parseLong() failed while parsing: "

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_f

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    :cond_f
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_8
    invoke-static {v11, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_9
    move-object v0, v7

    :goto_a
    if-eqz v0, :cond_18

    invoke-virtual {v0}, Lf/h/a/f/i/c/m4$b;->s()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v3, Lf/h/a/f/i/c/z4;->a:Landroid/content/Context;

    invoke-static {v3}, Lf/h/a/f/i/c/z4;->d(Landroid/content/Context;)J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lf/h/a/f/i/c/z4;->a(Ljava/lang/String;J)J

    move-result-wide v11

    invoke-virtual {v0}, Lf/h/a/f/i/c/m4$b;->t()J

    move-result-wide v13

    invoke-virtual {v0}, Lf/h/a/f/i/c/m4$b;->u()J

    move-result-wide v15

    invoke-static/range {v11 .. v16}, Lf/h/a/f/i/c/z4;->b(JJJ)Z

    move-result v2

    goto/16 :goto_e

    :cond_10
    if-eqz v4, :cond_11

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_11

    goto :goto_b

    :cond_11
    if-ltz v0, :cond_12

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_b

    :cond_12
    move-object v4, v7

    :goto_b
    if-eqz v4, :cond_18

    iget-object v0, v3, Lf/h/a/f/i/c/z4;->a:Landroid/content/Context;

    if-nez v0, :cond_13

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_d

    :cond_13
    sget-object v0, Lf/h/a/f/i/c/z4;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/f/i/c/e;

    if-nez v5, :cond_15

    sget-object v5, Lf/h/a/f/i/c/z4;->c:Lf/h/a/f/i/c/o;

    invoke-static {}, Lf/h/a/f/i/c/m4;->o()Lf/h/a/f/i/c/m4;

    move-result-object v9

    sget-object v11, Lf/h/a/f/i/c/a5;->a:Lf/h/a/f/i/c/n;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v12, Lf/h/a/f/i/c/e;->g:Ljava/lang/Object;

    new-instance v12, Lf/h/a/f/i/c/l;

    invoke-direct {v12, v5, v4, v9, v11}, Lf/h/a/f/i/c/l;-><init>(Lf/h/a/f/i/c/o;Ljava/lang/String;Ljava/lang/Object;Lf/h/a/f/i/c/n;)V

    invoke-virtual {v0, v4, v12}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lf/h/a/f/i/c/e;

    if-eqz v5, :cond_14

    goto :goto_c

    :cond_14
    move-object v5, v12

    :cond_15
    :goto_c
    invoke-virtual {v5}, Lf/h/a/f/i/c/e;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/m4;

    invoke-virtual {v0}, Lf/h/a/f/i/c/m4;->n()Ljava/util/List;

    move-result-object v0

    :goto_d
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/f/i/c/m4$b;

    invoke-virtual {v4}, Lf/h/a/f/i/c/m4$b;->r()Z

    move-result v5

    if-eqz v5, :cond_17

    invoke-virtual {v4}, Lf/h/a/f/i/c/m4$b;->n()I

    move-result v5

    if-eqz v5, :cond_17

    invoke-virtual {v4}, Lf/h/a/f/i/c/m4$b;->n()I

    move-result v5

    if-nez v5, :cond_16

    :cond_17
    invoke-virtual {v4}, Lf/h/a/f/i/c/m4$b;->s()Ljava/lang/String;

    move-result-object v5

    iget-object v9, v3, Lf/h/a/f/i/c/z4;->a:Landroid/content/Context;

    invoke-static {v9}, Lf/h/a/f/i/c/z4;->d(Landroid/content/Context;)J

    move-result-wide v11

    invoke-static {v5, v11, v12}, Lf/h/a/f/i/c/z4;->a(Ljava/lang/String;J)J

    move-result-wide v13

    invoke-virtual {v4}, Lf/h/a/f/i/c/m4$b;->t()J

    move-result-wide v15

    invoke-virtual {v4}, Lf/h/a/f/i/c/m4$b;->u()J

    move-result-wide v17

    invoke-static/range {v13 .. v18}, Lf/h/a/f/i/c/z4;->b(JJJ)Z

    move-result v4

    if-nez v4, :cond_16

    const/4 v2, 0x0

    :cond_18
    :goto_e
    if-eqz v2, :cond_19

    iget-object v0, v1, Lf/h/a/f/d/a$a;->g:Lf/h/a/f/d/a;

    iget-object v0, v0, Lf/h/a/f/d/a;->i:Lf/h/a/f/d/c;

    check-cast v0, Lf/h/a/f/i/c/l2;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/h/a/f/i/c/q4;

    iget-object v3, v0, Lf/h/a/f/f/h/b;->g:Lf/h/a/f/f/h/c;

    invoke-direct {v2, v10, v3}, Lf/h/a/f/i/c/q4;-><init>(Lcom/google/android/gms/clearcut/zze;Lf/h/a/f/f/h/c;)V

    invoke-virtual {v0, v6, v2}, Lf/h/a/f/f/h/b;->d(ILf/h/a/f/f/h/i/d;)Lf/h/a/f/f/h/i/d;

    return-void

    :cond_19
    sget-object v0, Lcom/google/android/gms/common/api/Status;->i:Lcom/google/android/gms/common/api/Status;

    const-string v2, "Result must not be null"

    invoke-static {v0, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/h/a/f/f/h/i/o;

    invoke-direct {v2, v7}, Lf/h/a/f/f/h/i/o;-><init>(Lf/h/a/f/f/h/c;)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/api/internal/BasePendingResult;->g(Lf/h/a/f/f/h/g;)V

    return-void

    :cond_1a
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "do not reuse LogEventBuilder"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
