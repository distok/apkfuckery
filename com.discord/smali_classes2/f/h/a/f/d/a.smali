.class public final Lf/h/a/f/d/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/d/a$a;,
        Lf/h/a/f/d/a$d;,
        Lf/h/a/f/d/a$b;,
        Lf/h/a/f/d/a$c;
    }
.end annotation


# static fields
.field public static final m:Lf/h/a/f/f/h/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$g<",
            "Lf/h/a/f/i/c/u4;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "Lf/h/a/f/i/c/u4;",
            "Lf/h/a/f/f/h/a$d$c;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lf/h/a/f/f/h/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a<",
            "Lf/h/a/f/f/h/a$d$c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Ljava/lang/String;

.field public final g:Z

.field public h:Lf/h/a/f/i/c/i4;

.field public final i:Lf/h/a/f/d/c;

.field public final j:Lf/h/a/f/f/n/c;

.field public k:Lf/h/a/f/d/a$d;

.field public final l:Lf/h/a/f/d/a$b;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lf/h/a/f/f/h/a$g;

    invoke-direct {v0}, Lf/h/a/f/f/h/a$g;-><init>()V

    sput-object v0, Lf/h/a/f/d/a;->m:Lf/h/a/f/f/h/a$g;

    new-instance v1, Lf/h/a/f/d/b;

    invoke-direct {v1}, Lf/h/a/f/d/b;-><init>()V

    sput-object v1, Lf/h/a/f/d/a;->n:Lf/h/a/f/f/h/a$a;

    new-instance v2, Lf/h/a/f/f/h/a;

    const-string v3, "ClearcutLogger.API"

    invoke-direct {v2, v3, v1, v0}, Lf/h/a/f/f/h/a;-><init>(Ljava/lang/String;Lf/h/a/f/f/h/a$a;Lf/h/a/f/f/h/a$g;)V

    sput-object v2, Lf/h/a/f/d/a;->o:Lf/h/a/f/f/h/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLf/h/a/f/d/c;Lf/h/a/f/f/n/c;Lf/h/a/f/d/a$b;)V
    .locals 4

    sget-object p3, Lf/h/a/f/i/c/i4;->d:Lf/h/a/f/i/c/i4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lf/h/a/f/d/a;->e:I

    iput-object p3, p0, Lf/h/a/f/d/a;->h:Lf/h/a/f/i/c/i4;

    iput-object p1, p0, Lf/h/a/f/d/a;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lf/h/a/f/d/a;->b:Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1

    iget v1, p1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v2, "ClearcutLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v2, v3, p1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    iput v1, p0, Lf/h/a/f/d/a;->c:I

    iput v0, p0, Lf/h/a/f/d/a;->e:I

    iput-object p2, p0, Lf/h/a/f/d/a;->d:Ljava/lang/String;

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/f/d/a;->f:Ljava/lang/String;

    iput-boolean p4, p0, Lf/h/a/f/d/a;->g:Z

    iput-object p5, p0, Lf/h/a/f/d/a;->i:Lf/h/a/f/d/c;

    iput-object p6, p0, Lf/h/a/f/d/a;->j:Lf/h/a/f/f/n/c;

    new-instance p1, Lf/h/a/f/d/a$d;

    invoke-direct {p1}, Lf/h/a/f/d/a$d;-><init>()V

    iput-object p1, p0, Lf/h/a/f/d/a;->k:Lf/h/a/f/d/a$d;

    iput-object p3, p0, Lf/h/a/f/d/a;->h:Lf/h/a/f/i/c/i4;

    iput-object p7, p0, Lf/h/a/f/d/a;->l:Lf/h/a/f/d/a$b;

    if-eqz p4, :cond_0

    const/4 p1, 0x1

    const-string p2, "can\'t be anonymous with an upload account"

    invoke-static {p1, p2}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    :cond_0
    return-void
.end method
