.class public final synthetic Lf/h/a/f/i/h/v0;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/i/h/t0;

.field public final e:Lf/h/a/f/i/h/m0;

.field public final f:Landroid/app/job/JobParameters;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/t0;Lf/h/a/f/i/h/m0;Landroid/app/job/JobParameters;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/h/v0;->d:Lf/h/a/f/i/h/t0;

    iput-object p2, p0, Lf/h/a/f/i/h/v0;->e:Lf/h/a/f/i/h/m0;

    iput-object p3, p0, Lf/h/a/f/i/h/v0;->f:Landroid/app/job/JobParameters;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/i/h/v0;->d:Lf/h/a/f/i/h/t0;

    iget-object v1, p0, Lf/h/a/f/i/h/v0;->e:Lf/h/a/f/i/h/m0;

    iget-object v2, p0, Lf/h/a/f/i/h/v0;->f:Landroid/app/job/JobParameters;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "AnalyticsJobService processed last dispatch request"

    invoke-virtual {v1, v3}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    iget-object v0, v0, Lf/h/a/f/i/h/t0;->b:Landroid/content/Context;

    check-cast v0, Lf/h/a/f/i/h/x0;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lf/h/a/f/i/h/x0;->a(Landroid/app/job/JobParameters;Z)V

    return-void
.end method
