.class public final Lf/h/a/f/i/h/z;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lf/h/a/f/i/h/g;

.field public volatile b:Ljava/lang/Boolean;

.field public c:Ljava/lang/String;

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/h/z;->a:Lf/h/a/f/i/h/g;

    return-void
.end method

.method public static b()J
    .locals 2

    sget-object v0, Lf/h/a/f/i/h/e0;->e:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c()I
    .locals 1

    sget-object v0, Lf/h/a/f/i/h/e0;->g:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static d()Ljava/lang/String;
    .locals 1

    sget-object v0, Lf/h/a/f/i/h/e0;->j:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static e()Ljava/lang/String;
    .locals 1

    sget-object v0, Lf/h/a/f/i/h/e0;->i:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static f()Ljava/lang/String;
    .locals 1

    sget-object v0, Lf/h/a/f/i/h/e0;->k:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_5

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    iget-object v1, p0, Lf/h/a/f/i/h/z;->a:Lf/h/a/f/i/h/g;

    iget-object v1, v1, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-static {}, Lf/h/a/f/f/n/h;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    :cond_1
    iget-object v1, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const-string v1, "com.google.android.gms.analytics"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput-object v0, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    :cond_3
    iget-object v1, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    iput-object v0, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    iget-object v0, p0, Lf/h/a/f/i/h/z;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    const-string v1, "My process not in the list of running processes"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->z(Ljava/lang/String;)V

    :cond_4
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    :goto_1
    iget-object v0, p0, Lf/h/a/f/i/h/z;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/i/h/e0;->s:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lf/h/a/f/i/h/z;->d:Ljava/util/Set;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/i/h/z;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const-string v1, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v1, v4

    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iput-object v0, p0, Lf/h/a/f/i/h/z;->c:Ljava/lang/String;

    iput-object v2, p0, Lf/h/a/f/i/h/z;->d:Ljava/util/Set;

    :cond_2
    iget-object v0, p0, Lf/h/a/f/i/h/z;->d:Ljava/util/Set;

    return-object v0
.end method
