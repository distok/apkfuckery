.class public final Lf/h/a/f/i/h/h0;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/i/h/h0<",
        "Lf/h/a/f/i/h/i0;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/f/i/h/g;

.field public final b:Lf/h/a/f/i/h/i0;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/h/h0;->a:Lf/h/a/f/i/h/g;

    new-instance p1, Lf/h/a/f/i/h/i0;

    invoke-direct {p1}, Lf/h/a/f/i/h/i0;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/h/h0;->b:Lf/h/a/f/i/h/i0;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "ga_dryRun"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/h/a/f/i/h/h0;->b:Lf/h/a/f/i/h/i0;

    iput p2, p1, Lf/h/a/f/i/h/i0;->e:I

    return-void

    :cond_0
    iget-object p2, p0, Lf/h/a/f/i/h/h0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p2}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object p2

    const-string v0, "Bool xml configuration name not recognized"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 1

    const-string v0, "ga_dispatchPeriod"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/h/a/f/i/h/h0;->b:Lf/h/a/f/i/h/i0;

    iput p2, p1, Lf/h/a/f/i/h/i0;->d:I

    return-void

    :cond_0
    iget-object p2, p0, Lf/h/a/f/i/h/h0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p2}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object p2

    const-string v0, "Int xml configuration name not recognized"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "ga_appName"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/h/a/f/i/h/h0;->b:Lf/h/a/f/i/h/i0;

    iput-object p2, p1, Lf/h/a/f/i/h/i0;->a:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "ga_appVersion"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lf/h/a/f/i/h/h0;->b:Lf/h/a/f/i/h/i0;

    iput-object p2, p1, Lf/h/a/f/i/h/i0;->b:Ljava/lang/String;

    return-void

    :cond_1
    const-string v0, "ga_logLevel"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lf/h/a/f/i/h/h0;->b:Lf/h/a/f/i/h/i0;

    iput-object p2, p1, Lf/h/a/f/i/h/i0;->c:Ljava/lang/String;

    return-void

    :cond_2
    iget-object p2, p0, Lf/h/a/f/i/h/h0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p2}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object p2

    const-string v0, "String xml configuration name not recognized"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
