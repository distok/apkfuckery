.class public final Lf/h/a/f/i/h/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Landroid/content/ComponentName;

.field public final synthetic e:Lf/h/a/f/i/h/l;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/l;Landroid/content/ComponentName;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/h/n;->e:Lf/h/a/f/i/h/l;

    iput-object p2, p0, Lf/h/a/f/i/h/n;->d:Landroid/content/ComponentName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/i/h/n;->e:Lf/h/a/f/i/h/l;

    iget-object v0, v0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    iget-object v1, p0, Lf/h/a/f/i/h/n;->d:Landroid/content/ComponentName;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    iget-object v2, v0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-object v2, v0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    const-string v2, "Disconnected from device AnalyticsService"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lf/h/a/f/i/h/d;->m()Lf/h/a/f/i/h/a;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    iget-object v0, v0, Lf/h/a/f/i/h/a;->f:Lf/h/a/f/i/h/r;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    const-string v1, "Service disconnected"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
