.class public final Lf/h/a/f/i/h/t0;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/content/Context;",
        ":",
        "Lf/h/a/f/i/h/x0;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static c:Ljava/lang/Boolean;


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/h/t0;->b:Landroid/content/Context;

    new-instance p1, Lf/h/a/f/i/h/e1;

    invoke-direct {p1}, Lf/h/a/f/i/h/e1;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/h/t0;->a:Landroid/os/Handler;

    return-void
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 4

    const-string v0, "null reference"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lf/h/a/f/i/h/t0;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0

    :cond_0
    const-string v0, "com.google.android.gms.analytics.AnalyticsService"

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object p0

    if-eqz p0, :cond_1

    iget-boolean p0, p0, Landroid/content/pm/ServiceInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p0, :cond_1

    const/4 v1, 0x1

    :catch_0
    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    sput-object p0, Lf/h/a/f/i/h/t0;->c:Ljava/lang/Boolean;

    return v1
.end method


# virtual methods
.method public final a(Landroid/content/Intent;I)I
    .locals 8
    .annotation build Landroidx/annotation/RequiresPermission;
        allOf = {
            "android.permission.INTERNET",
            "android.permission.ACCESS_NETWORK_STATE"
        }
    .end annotation

    :try_start_0
    sget-object v0, Lf/h/a/f/i/h/s0;->a:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v1, Lf/h/a/f/i/h/s0;->b:Lf/h/a/f/o/a;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lf/h/a/f/o/a;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lf/h/a/f/o/a;->b()V

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    nop

    :goto_0
    iget-object v0, p0, Lf/h/a/f/i/h/t0;->b:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/i/h/g;->b(Landroid/content/Context;)Lf/h/a/f/i/h/g;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    const/4 v7, 0x2

    if-nez p1, :cond_1

    const-string p1, "AnalyticsService started with null intent"

    invoke-virtual {v0, p1}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    return v7

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v3, "Local AnalyticsService called. startId, action"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v2, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, Lf/h/a/f/i/h/d;->a(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v1, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Lf/h/a/f/i/h/u0;

    invoke-direct {p1, p0, p2, v0}, Lf/h/a/f/i/h/u0;-><init>(Lf/h/a/f/i/h/t0;ILf/h/a/f/i/h/m0;)V

    invoke-virtual {p0, p1}, Lf/h/a/f/i/h/t0;->b(Ljava/lang/Runnable;)V

    :cond_2
    return v7
.end method

.method public final b(Ljava/lang/Runnable;)V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/i/h/t0;->b:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/i/h/g;->b(Landroid/content/Context;)Lf/h/a/f/i/h/g;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->e()Lf/h/a/f/i/h/a;

    move-result-object v0

    new-instance v1, Lf/h/a/f/i/h/w0;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/i/h/w0;-><init>(Lf/h/a/f/i/h/t0;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    invoke-virtual {v0}, Lf/h/a/f/i/h/d;->i()Lf/h/a/f/b/f;

    move-result-object p1

    new-instance v2, Lf/h/a/f/i/h/c;

    invoke-direct {v2, v0, v1}, Lf/h/a/f/i/h/c;-><init>(Lf/h/a/f/i/h/a;Lf/h/a/f/i/h/d0;)V

    invoke-virtual {p1, v2}, Lf/h/a/f/b/f;->a(Ljava/lang/Runnable;)V

    return-void
.end method
