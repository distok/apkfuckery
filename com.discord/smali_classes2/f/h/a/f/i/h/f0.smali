.class public final Lf/h/a/f/i/h/f0;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/f/f/i/a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/f/i/a<",
            "TV;>;TV;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;II)Lf/h/a/f/i/h/f0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Lf/h/a/f/i/h/f0<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/i/h/f0;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    new-instance v1, Lf/h/a/f/f/i/e;

    invoke-direct {v1, p0, p2}, Lf/h/a/f/f/i/e;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lf/h/a/f/i/h/f0;-><init>(Lf/h/a/f/f/i/a;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;JJ)Lf/h/a/f/i/h/f0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JJ)",
            "Lf/h/a/f/i/h/f0<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/i/h/f0;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lf/h/a/f/f/i/b;

    invoke-direct {p4, p0, p3}, Lf/h/a/f/f/i/b;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-direct {v0, p4, p0}, Lf/h/a/f/i/h/f0;-><init>(Lf/h/a/f/f/i/a;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lf/h/a/f/i/h/f0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lf/h/a/f/i/h/f0<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/i/h/f0;

    new-instance v1, Lf/h/a/f/f/i/f;

    invoke-direct {v1, p0, p2}, Lf/h/a/f/f/i/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lf/h/a/f/i/h/f0;-><init>(Lf/h/a/f/f/i/a;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;ZZ)Lf/h/a/f/i/h/f0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ)",
            "Lf/h/a/f/i/h/f0<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/i/h/f0;

    new-instance v1, Lf/h/a/f/f/i/c;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-direct {v1, p0, p2}, Lf/h/a/f/f/i/c;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lf/h/a/f/i/h/f0;-><init>(Lf/h/a/f/f/i/a;Ljava/lang/Object;)V

    return-object v0
.end method
