.class public final Lf/h/a/f/i/h/k;
.super Lf/h/a/f/i/h/a0;


# instance fields
.field public final synthetic e:Lf/h/a/f/i/h/j;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/j;Lf/h/a/f/i/h/g;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/h/k;->e:Lf/h/a/f/i/h/j;

    invoke-direct {p0, p2}, Lf/h/a/f/i/h/a0;-><init>(Lf/h/a/f/i/h/g;)V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/h/k;->e:Lf/h/a/f/i/h/j;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {v0}, Lf/h/a/f/i/h/j;->I()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "Inactivity, disconnecting from device AnalyticsService"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/i/h/j;->F()V

    :goto_0
    return-void
.end method
