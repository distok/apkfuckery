.class public final Lf/h/a/f/i/h/j;
.super Lf/h/a/f/i/h/e;


# instance fields
.field public final f:Lf/h/a/f/i/h/l;

.field public g:Lf/h/a/f/i/h/k0;

.field public final h:Lf/h/a/f/i/h/a0;

.field public final i:Lf/h/a/f/i/h/y0;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/g;)V
    .locals 2

    invoke-direct {p0, p1}, Lf/h/a/f/i/h/e;-><init>(Lf/h/a/f/i/h/g;)V

    new-instance v0, Lf/h/a/f/i/h/y0;

    iget-object v1, p1, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    invoke-direct {v0, v1}, Lf/h/a/f/i/h/y0;-><init>(Lf/h/a/f/f/n/c;)V

    iput-object v0, p0, Lf/h/a/f/i/h/j;->i:Lf/h/a/f/i/h/y0;

    new-instance v0, Lf/h/a/f/i/h/l;

    invoke-direct {v0, p0}, Lf/h/a/f/i/h/l;-><init>(Lf/h/a/f/i/h/j;)V

    iput-object v0, p0, Lf/h/a/f/i/h/j;->f:Lf/h/a/f/i/h/l;

    new-instance v0, Lf/h/a/f/i/h/k;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/i/h/k;-><init>(Lf/h/a/f/i/h/j;Lf/h/a/f/i/h/g;)V

    iput-object v0, p0, Lf/h/a/f/i/h/j;->h:Lf/h/a/f/i/h/a0;

    return-void
.end method


# virtual methods
.method public final D()V
    .locals 0

    return-void
.end method

.method public final F()V
    .locals 3

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    :try_start_0
    invoke-static {}, Lf/h/a/f/f/m/a;->b()Lf/h/a/f/f/m/a;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v1, v1, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    iget-object v2, p0, Lf/h/a/f/i/h/j;->f:Lf/h/a/f/i/h/l;

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/f/m/a;->c(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :goto_0
    iget-object v0, p0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    invoke-virtual {p0}, Lf/h/a/f/i/h/d;->m()Lf/h/a/f/i/h/a;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    iget-object v0, v0, Lf/h/a/f/i/h/a;->f:Lf/h/a/f/i/h/r;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    const-string v1, "Service disconnected"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final I()Z
    .locals 1

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    iget-object v0, p0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final J(Lf/h/a/f/i/h/j0;)Z
    .locals 7

    const-string v0, "null reference"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    iget-object v1, p0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    const/4 v0, 0x0

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-boolean v2, p1, Lf/h/a/f/i/h/j0;->f:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lf/h/a/f/i/h/z;->d()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    invoke-static {}, Lf/h/a/f/i/h/z;->e()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v5, v2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    :try_start_0
    iget-object v2, p1, Lf/h/a/f/i/h/j0;->a:Ljava/util/Map;

    iget-wide v3, p1, Lf/h/a/f/i/h/j0;->d:J

    invoke-interface/range {v1 .. v6}, Lf/h/a/f/i/h/k0;->Y(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/j;->K()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    const-string p1, "Failed to send hits to AnalyticsService"

    invoke-virtual {p0, p1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    return v0
.end method

.method public final K()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/i/h/j;->i:Lf/h/a/f/i/h/y0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/y0;->a()V

    iget-object v0, p0, Lf/h/a/f/i/h/j;->h:Lf/h/a/f/i/h/a0;

    sget-object v1, Lf/h/a/f/i/h/e0;->x:Lf/h/a/f/i/h/f0;

    iget-object v1, v1, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/i/h/a0;->e(J)V

    return-void
.end method
