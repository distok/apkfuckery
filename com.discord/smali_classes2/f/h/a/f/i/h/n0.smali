.class public Lf/h/a/f/i/h/n0;
.super Landroid/content/BroadcastReceiver;


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public final a:Lf/h/a/f/i/h/g;

.field public b:Z

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lf/h/a/f/i/h/n0;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf/h/a/f/i/h/n0;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lf/h/a/f/i/h/g;)V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, "null reference"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-boolean v0, p0, Lf/h/a/f/i/h/n0;->b:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    const-string v1, "Unregistering connectivity change receiver"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/h/n0;->b:Z

    iput-boolean v0, p0, Lf/h/a/f/i/h/n0;->c:Z

    iget-object v0, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    :try_start_0
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v1}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v1

    const-string v2, "Failed to unregister the network broadcast receiver"

    invoke-virtual {v1, v2, v0}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :catch_0
    :cond_0
    return v1
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iget-object p1, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p1}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    iget-object p1, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p1}, Lf/h/a/f/i/h/g;->e()Lf/h/a/f/i/h/a;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    const-string v1, "NetworkBroadcastReceiver received action"

    invoke-virtual {v0, v1, p1}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lf/h/a/f/i/h/n0;->b()Z

    move-result p1

    iget-boolean p2, p0, Lf/h/a/f/i/h/n0;->c:Z

    if-eq p2, p1, :cond_0

    iput-boolean p1, p0, Lf/h/a/f/i/h/n0;->c:Z

    iget-object p2, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p2}, Lf/h/a/f/i/h/g;->e()Lf/h/a/f/i/h/a;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "Network connectivity status changed"

    invoke-virtual {p2, v1, v0}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p2}, Lf/h/a/f/i/h/d;->i()Lf/h/a/f/b/f;

    move-result-object v0

    new-instance v1, Lf/h/a/f/i/h/b;

    invoke-direct {v1, p2, p1}, Lf/h/a/f/i/h/b;-><init>(Lf/h/a/f/i/h/a;Z)V

    invoke-virtual {v0, v1}, Lf/h/a/f/b/f;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "com.google.analytics.RADIO_POWERED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lf/h/a/f/i/h/n0;->d:Ljava/lang/String;

    invoke-virtual {p2, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p1}, Lf/h/a/f/i/h/g;->e()Lf/h/a/f/i/h/a;

    move-result-object p1

    const-string p2, "Radio powered up"

    invoke-virtual {p1, p2}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->E()V

    iget-object p2, p1, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object p2, p2, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    invoke-static {p2}, Lf/h/a/f/i/h/s0;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lf/h/a/f/i/h/t0;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance p1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms.analytics.AnalyticsService"

    invoke-direct {v0, p2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->E()V

    invoke-virtual {p1}, Lf/h/a/f/i/h/d;->i()Lf/h/a/f/b/f;

    move-result-object v0

    new-instance v1, Lf/h/a/f/i/h/c;

    invoke-direct {v1, p1, p2}, Lf/h/a/f/i/h/c;-><init>(Lf/h/a/f/i/h/a;Lf/h/a/f/i/h/d0;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/b/f;->a(Ljava/lang/Runnable;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object p2, p0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {p2}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object p2

    const-string v0, "NetworkBroadcastReceiver received unknown action"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
