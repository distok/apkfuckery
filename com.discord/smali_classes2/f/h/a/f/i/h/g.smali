.class public Lf/h/a/f/i/h/g;
.super Ljava/lang/Object;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StaticFieldLeak"
    }
.end annotation


# static fields
.field public static volatile k:Lf/h/a/f/i/h/g;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/Context;

.field public final c:Lf/h/a/f/f/n/c;

.field public final d:Lf/h/a/f/i/h/z;

.field public final e:Lf/h/a/f/i/h/m0;

.field public final f:Lf/h/a/f/b/f;

.field public final g:Lf/h/a/f/i/h/a;

.field public final h:Lf/h/a/f/i/h/c0;

.field public final i:Lf/h/a/f/i/h/a1;

.field public final j:Lf/h/a/f/i/h/p0;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/i;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lf/h/a/f/i/h/i;->a:Landroid/content/Context;

    const-string v1, "Application context can\'t be null"

    invoke-static {v0, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/a/f/i/h/i;->b:Landroid/content/Context;

    const-string v2, "null reference"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v0, p0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    iput-object v1, p0, Lf/h/a/f/i/h/g;->b:Landroid/content/Context;

    sget-object v1, Lf/h/a/f/f/n/d;->a:Lf/h/a/f/f/n/d;

    iput-object v1, p0, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    new-instance v2, Lf/h/a/f/i/h/z;

    invoke-direct {v2, p0}, Lf/h/a/f/i/h/z;-><init>(Lf/h/a/f/i/h/g;)V

    iput-object v2, p0, Lf/h/a/f/i/h/g;->d:Lf/h/a/f/i/h/z;

    new-instance v2, Lf/h/a/f/i/h/m0;

    invoke-direct {v2, p0}, Lf/h/a/f/i/h/m0;-><init>(Lf/h/a/f/i/h/g;)V

    invoke-virtual {v2}, Lf/h/a/f/i/h/e;->C()V

    iput-object v2, p0, Lf/h/a/f/i/h/g;->e:Lf/h/a/f/i/h/m0;

    invoke-virtual {p0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v3

    sget-object v2, Lf/h/a/f/i/h/f;->a:Ljava/lang/String;

    const/16 v4, 0x86

    invoke-static {v2, v4}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "Google Analytics "

    const-string v6, " is starting up. To enable debug logging on a device run:\n  adb shell setprop log.tag.GAv4 DEBUG\n  adb logcat -s GAv4"

    invoke-static {v4, v5, v2, v6}, Lf/e/c/a/a;->f(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lf/h/a/f/i/h/d;->a(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v2, Lf/h/a/f/i/h/p0;

    invoke-direct {v2, p0}, Lf/h/a/f/i/h/p0;-><init>(Lf/h/a/f/i/h/g;)V

    invoke-virtual {v2}, Lf/h/a/f/i/h/e;->C()V

    iput-object v2, p0, Lf/h/a/f/i/h/g;->j:Lf/h/a/f/i/h/p0;

    new-instance v2, Lf/h/a/f/i/h/a1;

    invoke-direct {v2, p0}, Lf/h/a/f/i/h/a1;-><init>(Lf/h/a/f/i/h/g;)V

    invoke-virtual {v2}, Lf/h/a/f/i/h/e;->C()V

    iput-object v2, p0, Lf/h/a/f/i/h/g;->i:Lf/h/a/f/i/h/a1;

    new-instance v2, Lf/h/a/f/i/h/a;

    invoke-direct {v2, p0, p1}, Lf/h/a/f/i/h/a;-><init>(Lf/h/a/f/i/h/g;Lf/h/a/f/i/h/i;)V

    const-string p1, "null reference"

    invoke-static {v1, p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance p1, Lf/h/a/f/i/h/q;

    invoke-direct {p1, p0}, Lf/h/a/f/i/h/q;-><init>(Lf/h/a/f/i/h/g;)V

    sget-object v1, Lf/h/a/f/b/f;->e:Lf/h/a/f/b/f;

    if-nez v1, :cond_1

    const-class v1, Lf/h/a/f/b/f;

    monitor-enter v1

    :try_start_0
    sget-object v3, Lf/h/a/f/b/f;->e:Lf/h/a/f/b/f;

    if-nez v3, :cond_0

    new-instance v3, Lf/h/a/f/b/f;

    invoke-direct {v3, v0}, Lf/h/a/f/b/f;-><init>(Landroid/content/Context;)V

    sput-object v3, Lf/h/a/f/b/f;->e:Lf/h/a/f/b/f;

    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    sget-object v0, Lf/h/a/f/b/f;->e:Lf/h/a/f/b/f;

    new-instance v1, Lf/h/a/f/i/h/h;

    invoke-direct {v1, p0}, Lf/h/a/f/i/h/h;-><init>(Lf/h/a/f/i/h/g;)V

    iput-object v1, v0, Lf/h/a/f/b/f;->d:Ljava/lang/Thread$UncaughtExceptionHandler;

    iput-object v0, p0, Lf/h/a/f/i/h/g;->f:Lf/h/a/f/b/f;

    new-instance v0, Lf/h/a/f/b/b;

    invoke-direct {v0, p0}, Lf/h/a/f/b/b;-><init>(Lf/h/a/f/i/h/g;)V

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->C()V

    new-instance p1, Lf/h/a/f/i/h/c0;

    invoke-direct {p1, p0}, Lf/h/a/f/i/h/c0;-><init>(Lf/h/a/f/i/h/g;)V

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->C()V

    iput-object p1, p0, Lf/h/a/f/i/h/g;->h:Lf/h/a/f/i/h/c0;

    invoke-virtual {v2}, Lf/h/a/f/i/h/e;->C()V

    iput-object v2, p0, Lf/h/a/f/i/h/g;->g:Lf/h/a/f/i/h/a;

    iget-object p1, v0, Lf/h/a/f/b/c;->a:Lf/h/a/f/i/h/g;

    iget-object v0, p1, Lf/h/a/f/i/h/g;->i:Lf/h/a/f/i/h/a1;

    invoke-static {v0}, Lf/h/a/f/i/h/g;->a(Lf/h/a/f/i/h/e;)V

    iget-object p1, p1, Lf/h/a/f/i/h/g;->i:Lf/h/a/f/i/h/a1;

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->E()V

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->E()V

    iget-boolean v0, p1, Lf/h/a/f/i/h/a1;->j:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->E()V

    :cond_2
    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->E()V

    iget-object p1, v2, Lf/h/a/f/i/h/a;->f:Lf/h/a/f/i/h/r;

    invoke-virtual {p1}, Lf/h/a/f/i/h/e;->E()V

    iget-boolean v0, p1, Lf/h/a/f/i/h/r;->f:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "Analytics backend already started"

    invoke-static {v0, v2}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    iput-boolean v1, p1, Lf/h/a/f/i/h/r;->f:Z

    invoke-virtual {p1}, Lf/h/a/f/i/h/d;->i()Lf/h/a/f/b/f;

    move-result-object v0

    new-instance v1, Lf/h/a/f/i/h/u;

    invoke-direct {v1, p1}, Lf/h/a/f/i/h/u;-><init>(Lf/h/a/f/i/h/r;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/b/f;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a(Lf/h/a/f/i/h/e;)V
    .locals 1

    const-string v0, "Analytics service not created/initialized"

    invoke-static {p0, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->B()Z

    move-result p0

    const-string v0, "Analytics service not initialized"

    invoke-static {p0, v0}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)Lf/h/a/f/i/h/g;
    .locals 6

    const-string v0, "null reference"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    sget-object v0, Lf/h/a/f/i/h/g;->k:Lf/h/a/f/i/h/g;

    if-nez v0, :cond_3

    const-class v0, Lf/h/a/f/i/h/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/a/f/i/h/g;->k:Lf/h/a/f/i/h/g;

    if-nez v1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    new-instance v3, Lf/h/a/f/i/h/i;

    invoke-direct {v3, p0}, Lf/h/a/f/i/h/i;-><init>(Landroid/content/Context;)V

    new-instance p0, Lf/h/a/f/i/h/g;

    invoke-direct {p0, v3}, Lf/h/a/f/i/h/g;-><init>(Lf/h/a/f/i/h/i;)V

    sput-object p0, Lf/h/a/f/i/h/g;->k:Lf/h/a/f/i/h/g;

    const-class v3, Lf/h/a/f/b/b;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v4, Lf/h/a/f/b/b;->b:Ljava/util/List;

    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Runnable;

    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    sput-object v4, Lf/h/a/f/b/b;->b:Ljava/util/List;

    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long/2addr v3, v1

    sget-object v1, Lf/h/a/f/i/h/e0;->B:Lf/h/a/f/i/h/f0;

    iget-object v1, v1, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v5, v3, v1

    if-lez v5, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object p0

    const-string v5, "Slow initialization (ms)"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v5, v3, v1}, Lf/h/a/f/i/h/d;->f(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_0
    move-exception p0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p0

    :cond_2
    :goto_1
    monitor-exit v0

    goto :goto_2

    :catchall_1
    move-exception p0

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p0

    :cond_3
    :goto_2
    sget-object p0, Lf/h/a/f/i/h/g;->k:Lf/h/a/f/i/h/g;

    return-object p0
.end method


# virtual methods
.method public final c()Lf/h/a/f/i/h/m0;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/h/g;->e:Lf/h/a/f/i/h/m0;

    invoke-static {v0}, Lf/h/a/f/i/h/g;->a(Lf/h/a/f/i/h/e;)V

    iget-object v0, p0, Lf/h/a/f/i/h/g;->e:Lf/h/a/f/i/h/m0;

    return-object v0
.end method

.method public final d()Lf/h/a/f/b/f;
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/h/g;->f:Lf/h/a/f/b/f;

    const-string v1, "null reference"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/i/h/g;->f:Lf/h/a/f/b/f;

    return-object v0
.end method

.method public final e()Lf/h/a/f/i/h/a;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/h/g;->g:Lf/h/a/f/i/h/a;

    invoke-static {v0}, Lf/h/a/f/i/h/g;->a(Lf/h/a/f/i/h/e;)V

    iget-object v0, p0, Lf/h/a/f/i/h/g;->g:Lf/h/a/f/i/h/a;

    return-object v0
.end method
