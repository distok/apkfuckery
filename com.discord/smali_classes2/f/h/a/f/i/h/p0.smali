.class public final Lf/h/a/f/i/h/p0;
.super Lf/h/a/f/i/h/e;


# instance fields
.field public f:Landroid/content/SharedPreferences;

.field public g:J

.field public h:J

.field public final i:Lf/h/a/f/i/h/r0;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/g;)V
    .locals 8

    invoke-direct {p0, p1}, Lf/h/a/f/i/h/e;-><init>(Lf/h/a/f/i/h/g;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/h/a/f/i/h/p0;->h:J

    new-instance p1, Lf/h/a/f/i/h/r0;

    sget-object v0, Lf/h/a/f/i/h/e0;->A:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-string v4, "monitoring"

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lf/h/a/f/i/h/r0;-><init>(Lf/h/a/f/i/h/p0;Ljava/lang/String;JLf/h/a/f/i/h/q0;)V

    iput-object p1, p0, Lf/h/a/f/i/h/p0;->i:Lf/h/a/f/i/h/r0;

    return-void
.end method


# virtual methods
.method public final D()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    const/4 v1, 0x0

    const-string v2, "com.google.android.gms.analytics.prefs"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/i/h/p0;->f:Landroid/content/SharedPreferences;

    return-void
.end method

.method public final F()J
    .locals 5

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    iget-wide v0, p0, Lf/h/a/f/i/h/p0;->h:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/h/p0;->f:Landroid/content/SharedPreferences;

    const-wide/16 v1, 0x0

    const-string v3, "last_dispatch"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/i/h/p0;->h:J

    :cond_0
    iget-wide v0, p0, Lf/h/a/f/i/h/p0;->h:J

    return-wide v0
.end method

.method public final I()V
    .locals 4

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    iget-object v0, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lf/h/a/f/i/h/p0;->f:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "last_dispatch"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    iput-wide v0, p0, Lf/h/a/f/i/h/p0;->h:J

    return-void
.end method
