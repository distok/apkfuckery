.class public final Lf/h/a/f/i/h/u;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/f/i/h/r;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/r;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/h/u;->d:Lf/h/a/f/i/h/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    iget-object v0, p0, Lf/h/a/f/i/h/u;->d:Lf/h/a/f/i/h/r;

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    iget-object v1, v0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v1, v1, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    invoke-static {v1}, Lf/h/a/f/i/h/s0;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {v0, v2}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lf/h/a/f/i/h/t0;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {v0, v2}, Lf/h/a/f/i/h/d;->z(Ljava/lang/String;)V

    :cond_1
    :goto_0
    sget-object v2, Lf/h/a/f/b/a;->a:Ljava/lang/Boolean;

    sget-object v2, Lf/h/a/f/b/a;->a:Ljava/lang/Boolean;

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_1

    :cond_2
    const-string v2, "com.google.android.gms.analytics.CampaignTrackingReceiver"

    invoke-static {v1, v2, v3}, Lf/h/a/f/i/h/z0;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lf/h/a/f/b/a;->a:Ljava/lang/Boolean;

    :goto_1
    if-nez v1, :cond_3

    const-string v1, "CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lf/h/a/f/i/h/d;->o()Lf/h/a/f/i/h/p0;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {v1}, Lf/h/a/f/i/h/e;->E()V

    iget-wide v4, v1, Lf/h/a/f/i/h/p0;->g:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_6

    iget-object v2, v1, Lf/h/a/f/i/h/p0;->f:Landroid/content/SharedPreferences;

    const-string v4, "first_run"

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    cmp-long v2, v8, v6

    if-eqz v2, :cond_4

    iput-wide v8, v1, Lf/h/a/f/i/h/p0;->g:J

    goto :goto_2

    :cond_4
    iget-object v2, v1, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v2, v2, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v2, Lf/h/a/f/f/n/d;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v2, v1, Lf/h/a/f/i/h/p0;->f:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "Failed to commit first run time"

    invoke-virtual {v1, v2}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    :cond_5
    iput-wide v5, v1, Lf/h/a/f/i/h/p0;->g:J

    :cond_6
    :goto_2
    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/r;->U(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->z(Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    iput-boolean v3, v0, Lf/h/a/f/i/h/r;->p:Z

    iget-object v1, v0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v1}, Lf/h/a/f/i/h/j;->F()V

    invoke-virtual {v0}, Lf/h/a/f/i/h/r;->N()V

    :cond_7
    const-string v1, "android.permission.INTERNET"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/r;->U(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->z(Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    iput-boolean v3, v0, Lf/h/a/f/i/h/r;->p:Z

    iget-object v1, v0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v1}, Lf/h/a/f/i/h/j;->F()V

    invoke-virtual {v0}, Lf/h/a/f/i/h/r;->N()V

    :cond_8
    iget-object v1, v0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v1, v1, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    invoke-static {v1}, Lf/h/a/f/i/h/t0;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "AnalyticsService registered in the app manifest and enabled"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    const-string v1, "AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    :goto_3
    iget-boolean v1, v0, Lf/h/a/f/i/h/r;->p:Z

    if-nez v1, :cond_a

    iget-object v1, v0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->I()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lf/h/a/f/i/h/r;->J()V

    :cond_a
    invoke-virtual {v0}, Lf/h/a/f/i/h/r;->N()V

    return-void
.end method
