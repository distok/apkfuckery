.class public final enum Lf/h/a/f/i/h/x;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/a/f/i/h/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/a/f/i/h/x;

.field public static final enum e:Lf/h/a/f/i/h/x;

.field public static final synthetic f:[Lf/h/a/f/i/h/x;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/h/a/f/i/h/x;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/a/f/i/h/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/a/f/i/h/x;->d:Lf/h/a/f/i/h/x;

    new-instance v1, Lf/h/a/f/i/h/x;

    const-string v3, "GZIP"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/a/f/i/h/x;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/a/f/i/h/x;->e:Lf/h/a/f/i/h/x;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/h/a/f/i/h/x;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/h/a/f/i/h/x;->f:[Lf/h/a/f/i/h/x;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lf/h/a/f/i/h/x;
    .locals 1

    sget-object v0, Lf/h/a/f/i/h/x;->f:[Lf/h/a/f/i/h/x;

    invoke-virtual {v0}, [Lf/h/a/f/i/h/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/a/f/i/h/x;

    return-object v0
.end method
