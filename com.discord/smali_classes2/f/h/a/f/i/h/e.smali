.class public abstract Lf/h/a/f/i/h/e;
.super Lf/h/a/f/i/h/d;


# instance fields
.field public e:Z


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/i/h/d;-><init>(Lf/h/a/f/i/h/g;)V

    return-void
.end method


# virtual methods
.method public final B()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/h/e;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final C()V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->D()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/f/i/h/e;->e:Z

    return-void
.end method

.method public abstract D()V
.end method

.method public final E()V
    .locals 2

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
