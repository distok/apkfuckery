.class public final Lf/h/a/f/i/h/t;
.super Lf/h/a/f/i/h/a0;


# instance fields
.field public final synthetic e:Lf/h/a/f/i/h/r;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/r;Lf/h/a/f/i/h/g;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/h/t;->e:Lf/h/a/f/i/h/r;

    invoke-direct {p0, p2}, Lf/h/a/f/i/h/a0;-><init>(Lf/h/a/f/i/h/g;)V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/i/h/t;->e:Lf/h/a/f/i/h/r;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v1, v0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->N()I

    invoke-virtual {v0}, Lf/h/a/f/i/h/r;->N()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Failed to delete stale hits"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    iget-object v0, v0, Lf/h/a/f/i/h/r;->m:Lf/h/a/f/i/h/a0;

    const-wide/32 v1, 0x5265c00

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/i/h/a0;->e(J)V

    return-void
.end method
