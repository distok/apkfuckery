.class public final Lf/h/a/f/i/h/l;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public volatile d:Lf/h/a/f/i/h/k0;

.field public volatile e:Z

.field public final synthetic f:Lf/h/a/f/i/h/j;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/j;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    const-string p1, "AnalyticsServiceConnection.onServiceConnected"

    invoke-static {p1}, Lf/g/j/k/a;->m(Ljava/lang/String;)V

    monitor-enter p0

    if-nez p2, :cond_0

    :try_start_0
    iget-object p1, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string p2, "Service connected with null binder"

    invoke-virtual {p1, p2}, Lf/h/a/f/i/h/d;->z(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception p1

    goto :goto_3

    :cond_0
    const/4 p1, 0x0

    :try_start_2
    invoke-interface {p2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.analytics.internal.IAnalyticsService"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "com.google.android.gms.analytics.internal.IAnalyticsService"

    invoke-interface {p2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lf/h/a/f/i/h/k0;

    if-eqz v1, :cond_1

    check-cast v0, Lf/h/a/f/i/h/k0;

    goto :goto_0

    :cond_1
    new-instance v0, Lf/h/a/f/i/h/l0;

    invoke-direct {v0, p2}, Lf/h/a/f/i/h/l0;-><init>(Landroid/os/IBinder;)V

    :goto_0
    move-object p1, v0

    iget-object p2, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string v0, "Bound to IAnalyticsService interface"

    invoke-virtual {p2, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object p2, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string v1, "Got binder with a wrong descriptor"

    invoke-virtual {p2, v1, v0}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    :try_start_3
    iget-object p2, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string v0, "Service connect failed to get IAnalyticsService"

    invoke-virtual {p2, v0}, Lf/h/a/f/i/h/d;->z(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    if-nez p1, :cond_3

    :try_start_4
    invoke-static {}, Lf/h/a/f/f/m/a;->b()Lf/h/a/f/f/m/a;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    iget-object v0, p2, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    iget-object p2, p2, Lf/h/a/f/i/h/j;->f:Lf/h/a/f/i/h/l;

    invoke-virtual {p1, v0, p2}, Lf/h/a/f/f/m/a;->c(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :cond_3
    :try_start_5
    iget-boolean p2, p0, Lf/h/a/f/i/h/l;->e:Z

    if-nez p2, :cond_4

    iget-object p2, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string v0, "onServiceConnected received after the timeout limit"

    invoke-virtual {p2, v0}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    iget-object p2, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    invoke-virtual {p2}, Lf/h/a/f/i/h/d;->i()Lf/h/a/f/b/f;

    move-result-object p2

    new-instance v0, Lf/h/a/f/i/h/m;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/i/h/m;-><init>(Lf/h/a/f/i/h/l;Lf/h/a/f/i/h/k0;)V

    invoke-virtual {p2, v0}, Lf/h/a/f/b/f;->a(Ljava/lang/Runnable;)V

    goto :goto_2

    :cond_4
    iput-object p1, p0, Lf/h/a/f/i/h/l;->d:Lf/h/a/f/i/h/k0;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    :goto_2
    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    return-void

    :goto_3
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw p1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    const-string v0, "AnalyticsServiceConnection.onServiceDisconnected"

    invoke-static {v0}, Lf/g/j/k/a;->m(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    invoke-virtual {v0}, Lf/h/a/f/i/h/d;->i()Lf/h/a/f/b/f;

    move-result-object v0

    new-instance v1, Lf/h/a/f/i/h/n;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/i/h/n;-><init>(Lf/h/a/f/i/h/l;Landroid/content/ComponentName;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/b/f;->a(Ljava/lang/Runnable;)V

    return-void
.end method
