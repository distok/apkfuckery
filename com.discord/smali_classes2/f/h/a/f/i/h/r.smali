.class public final Lf/h/a/f/i/h/r;
.super Lf/h/a/f/i/h/e;


# instance fields
.field public f:Z

.field public final g:Lf/h/a/f/i/h/o;

.field public final h:Lf/h/a/f/i/h/o0;

.field public final i:Lf/h/a/f/i/h/n0;

.field public final j:Lf/h/a/f/i/h/j;

.field public k:J

.field public final l:Lf/h/a/f/i/h/a0;

.field public final m:Lf/h/a/f/i/h/a0;

.field public final n:Lf/h/a/f/i/h/y0;

.field public o:J

.field public p:Z


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/g;Lf/h/a/f/i/h/i;)V
    .locals 2

    invoke-direct {p0, p1}, Lf/h/a/f/i/h/e;-><init>(Lf/h/a/f/i/h/g;)V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lf/h/a/f/i/h/r;->k:J

    new-instance p2, Lf/h/a/f/i/h/n0;

    invoke-direct {p2, p1}, Lf/h/a/f/i/h/n0;-><init>(Lf/h/a/f/i/h/g;)V

    iput-object p2, p0, Lf/h/a/f/i/h/r;->i:Lf/h/a/f/i/h/n0;

    new-instance p2, Lf/h/a/f/i/h/o;

    invoke-direct {p2, p1}, Lf/h/a/f/i/h/o;-><init>(Lf/h/a/f/i/h/g;)V

    iput-object p2, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    new-instance p2, Lf/h/a/f/i/h/o0;

    invoke-direct {p2, p1}, Lf/h/a/f/i/h/o0;-><init>(Lf/h/a/f/i/h/g;)V

    iput-object p2, p0, Lf/h/a/f/i/h/r;->h:Lf/h/a/f/i/h/o0;

    new-instance p2, Lf/h/a/f/i/h/j;

    invoke-direct {p2, p1}, Lf/h/a/f/i/h/j;-><init>(Lf/h/a/f/i/h/g;)V

    iput-object p2, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    new-instance p2, Lf/h/a/f/i/h/y0;

    iget-object v0, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    invoke-direct {p2, v0}, Lf/h/a/f/i/h/y0;-><init>(Lf/h/a/f/f/n/c;)V

    iput-object p2, p0, Lf/h/a/f/i/h/r;->n:Lf/h/a/f/i/h/y0;

    new-instance p2, Lf/h/a/f/i/h/s;

    invoke-direct {p2, p0, p1}, Lf/h/a/f/i/h/s;-><init>(Lf/h/a/f/i/h/r;Lf/h/a/f/i/h/g;)V

    iput-object p2, p0, Lf/h/a/f/i/h/r;->l:Lf/h/a/f/i/h/a0;

    new-instance p2, Lf/h/a/f/i/h/t;

    invoke-direct {p2, p0, p1}, Lf/h/a/f/i/h/t;-><init>(Lf/h/a/f/i/h/r;Lf/h/a/f/i/h/g;)V

    iput-object p2, p0, Lf/h/a/f/i/h/r;->m:Lf/h/a/f/i/h/a0;

    return-void
.end method


# virtual methods
.method public final D()V
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->C()V

    iget-object v0, p0, Lf/h/a/f/i/h/r;->h:Lf/h/a/f/i/h/o0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->C()V

    iget-object v0, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->C()V

    return-void
.end method

.method public final F()V
    .locals 5

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    sget-object v0, Lf/h/a/f/i/h/e0;->a:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Service client disabled. Can\'t dispatch local hits to device AnalyticsService"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v0}, Lf/h/a/f/i/h/j;->I()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Service not connected"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v0}, Lf/h/a/f/i/h/o;->I()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Dispatching local hits to device AnalyticsService"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    :cond_2
    :try_start_0
    iget-object v0, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-static {}, Lf/h/a/f/i/h/z;->c()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/i/h/o;->K(J)Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    check-cast v0, Ljava/util/ArrayList;

    :try_start_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->N()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :cond_3
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/h/j0;

    iget-object v2, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v2, v1}, Lf/h/a/f/i/h/j;->J(Lf/h/a/f/i/h/j0;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->N()V

    return-void

    :cond_4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :try_start_2
    iget-object v2, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    iget-wide v3, v1, Lf/h/a/f/i/h/j0;->c:J

    invoke-virtual {v2, v3, v4}, Lf/h/a/f/i/h/o;->S(J)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to remove hit that was send for delivery"

    invoke-virtual {p0, v1, v0}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return-void

    :catch_1
    move-exception v0

    const-string v1, "Failed to read hits from store"

    invoke-virtual {p0, v1, v0}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    :cond_5
    return-void
.end method

.method public final I(Lf/h/a/f/i/h/d0;)V
    .locals 7

    iget-wide v0, p0, Lf/h/a/f/i/h/r;->o:J

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/d;->o()Lf/h/a/f/i/h/p0;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/i/h/p0;->F()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-eqz v6, :cond_0

    iget-object v4, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v4, v4, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v4, Lf/h/a/f/f/n/d;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    goto :goto_0

    :cond_0
    const-wide/16 v2, -0x1

    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "Dispatching local hits. Elapsed time since last dispatch (ms)"

    invoke-virtual {p0, v3, v2}, Lf/h/a/f/i/h/d;->d(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->J()V

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->K()Z

    invoke-virtual {p0}, Lf/h/a/f/i/h/d;->o()Lf/h/a/f/i/h/p0;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/i/h/p0;->I()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->N()V

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1, v2}, Lf/h/a/f/i/h/d0;->a(Ljava/lang/Throwable;)V

    :cond_1
    iget-wide v3, p0, Lf/h/a/f/i/h/r;->o:J

    cmp-long v5, v3, v0

    if-eqz v5, :cond_2

    iget-object v0, p0, Lf/h/a/f/i/h/r;->i:Lf/h/a/f/i/h/n0;

    iget-object v0, v0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.analytics.RADIO_POWERED"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lf/h/a/f/i/h/n0;->d:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Local dispatch failed"

    invoke-virtual {p0, v1, v0}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/d;->o()Lf/h/a/f/i/h/p0;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/i/h/p0;->I()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->N()V

    if-eqz p1, :cond_3

    invoke-interface {p1, v0}, Lf/h/a/f/i/h/d0;->a(Ljava/lang/Throwable;)V

    :cond_3
    return-void
.end method

.method public final J()V
    .locals 10

    iget-boolean v0, p0, Lf/h/a/f/i/h/r;->p:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lf/h/a/f/i/h/e0;->a:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v0}, Lf/h/a/f/i/h/j;->I()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    sget-object v0, Lf/h/a/f/i/h/e0;->z:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lf/h/a/f/i/h/r;->n:Lf/h/a/f/i/h/y0;

    invoke-virtual {v2, v0, v1}, Lf/h/a/f/i/h/y0;->b(J)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lf/h/a/f/i/h/r;->n:Lf/h/a/f/i/h/y0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/y0;->a()V

    const-string v0, "Connecting to service"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    iget-object v1, v0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    goto/16 :goto_2

    :cond_3
    iget-object v1, v0, Lf/h/a/f/i/h/j;->f:Lf/h/a/f/i/h/l;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.gms.analytics.service.START"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.google.android.gms"

    const-string v7, "com.google.android.gms.analytics.service.AnalyticsService"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v5, v1, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    iget-object v5, v5, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v5, v5, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    const-string v6, "app_package_name"

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lf/h/a/f/f/m/a;->b()Lf/h/a/f/f/m/a;

    move-result-object v6

    monitor-enter v1

    const/4 v7, 0x0

    :try_start_0
    iput-object v7, v1, Lf/h/a/f/i/h/l;->d:Lf/h/a/f/i/h/k0;

    iput-boolean v2, v1, Lf/h/a/f/i/h/l;->e:Z

    iget-object v8, v1, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    iget-object v8, v8, Lf/h/a/f/i/h/j;->f:Lf/h/a/f/i/h/l;

    const/16 v9, 0x81

    invoke-virtual {v6, v5, v4, v8, v9}, Lf/h/a/f/f/m/a;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v4

    iget-object v5, v1, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string v6, "Bind to service requested"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v4, :cond_4

    iput-boolean v3, v1, Lf/h/a/f/i/h/l;->e:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_4
    :try_start_1
    sget-object v4, Lf/h/a/f/i/h/e0;->y:Lf/h/a/f/i/h/f0;

    iget-object v4, v4, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    iget-object v4, v1, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string v5, "Wait for service connect was interrupted"

    invoke-virtual {v4, v5}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    :goto_0
    iput-boolean v3, v1, Lf/h/a/f/i/h/l;->e:Z

    iget-object v4, v1, Lf/h/a/f/i/h/l;->d:Lf/h/a/f/i/h/k0;

    iput-object v7, v1, Lf/h/a/f/i/h/l;->d:Lf/h/a/f/i/h/k0;

    if-nez v4, :cond_5

    iget-object v5, v1, Lf/h/a/f/i/h/l;->f:Lf/h/a/f/i/h/j;

    const-string v6, "Successfully bound to service but never got onServiceConnected callback"

    invoke-virtual {v5, v6}, Lf/h/a/f/i/h/d;->z(Ljava/lang/String;)V

    :cond_5
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v7, v4

    :goto_1
    if-eqz v7, :cond_6

    iput-object v7, v0, Lf/h/a/f/i/h/j;->g:Lf/h/a/f/i/h/k0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/j;->K()V

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_7

    const-string v0, "Connected to service"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    iget-object v0, p0, Lf/h/a/f/i/h/r;->n:Lf/h/a/f/i/h/y0;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lf/h/a/f/i/h/y0;->b:J

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->F()V

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_7
    :goto_3
    return-void
.end method

.method public final K()Z
    .locals 13

    const-string v0, "Failed to commit local dispatch transaction"

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    const-string v1, "Dispatching a batch of local hits"

    invoke-virtual {p0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    iget-object v1, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v1}, Lf/h/a/f/i/h/j;->I()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lf/h/a/f/i/h/r;->h:Lf/h/a/f/i/h/o0;

    invoke-virtual {v2}, Lf/h/a/f/i/h/o0;->S()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const-string v0, "No network or service available. Will retry later"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    return v3

    :cond_0
    invoke-static {}, Lf/h/a/f/i/h/z;->c()I

    move-result v1

    sget-object v2, Lf/h/a/f/i/h/e0;->h:Lf/h/a/f/i/h/f0;

    iget-object v2, v2, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-long v1, v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v5, 0x0

    :goto_0
    :try_start_0
    iget-object v7, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v7}, Lf/h/a/f/i/h/e;->E()V

    invoke-virtual {v7}, Lf/h/a/f/i/h/o;->F()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v7, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v7, v1, v2}, Lf/h/a/f/i/h/o;->K(J)Ljava/util/List;

    move-result-object v7
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v8, v7

    check-cast v8, Ljava/util/ArrayList;

    :try_start_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v1, "Store is empty, nothing to dispatch"

    invoke-virtual {p0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    return v3

    :catch_0
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3

    :cond_1
    :try_start_4
    const-string v9, "Hits loaded from store. count"

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p0, v9, v10}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lf/h/a/f/i/h/j0;

    iget-wide v10, v10, Lf/h/a/f/i/h/j0;->c:J

    cmp-long v12, v10, v5

    if-nez v12, :cond_2

    const-string v1, "Database contains successfully uploaded hit"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v4}, Lf/h/a/f/i/h/d;->t(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_1

    return v3

    :catch_1
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3

    :cond_3
    :try_start_7
    iget-object v9, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v9}, Lf/h/a/f/i/h/j;->I()Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "Service connected, sending hits to the service"

    invoke-virtual {p0, v9}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/a/f/i/h/j0;

    iget-object v10, p0, Lf/h/a/f/i/h/r;->j:Lf/h/a/f/i/h/j;

    invoke-virtual {v10, v9}, Lf/h/a/f/i/h/j;->J(Lf/h/a/f/i/h/j0;)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-wide v10, v9, Lf/h/a/f/i/h/j0;->c:J

    invoke-static {v5, v6, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string v10, "Hit sent do device AnalyticsService for delivery"

    invoke-virtual {p0, v10, v9}, Lf/h/a/f/i/h/d;->d(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    iget-object v10, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    iget-wide v11, v9, Lf/h/a/f/i/h/j0;->c:J

    invoke-virtual {v10, v11, v12}, Lf/h/a/f/i/h/o;->S(J)V

    iget-wide v9, v9, Lf/h/a/f/i/h/j0;->c:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v1

    :try_start_9
    const-string v2, "Failed to remove hit that was send for delivery"

    invoke-virtual {p0, v2, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_3

    return v3

    :catch_3
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3

    :cond_4
    :try_start_b
    iget-object v8, p0, Lf/h/a/f/i/h/r;->h:Lf/h/a/f/i/h/o0;

    invoke-virtual {v8}, Lf/h/a/f/i/h/o0;->S()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lf/h/a/f/i/h/r;->h:Lf/h/a/f/i/h/o0;

    invoke-virtual {v8, v7}, Lf/h/a/f/i/h/o0;->O(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-static {v5, v6, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_2

    :cond_5
    :try_start_c
    iget-object v8, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v8, v7}, Lf/h/a/f/i/h/o;->J(Ljava/util/List;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_3

    :catch_4
    move-exception v1

    :try_start_d
    const-string v2, "Failed to remove successfully uploaded hits"

    invoke-virtual {p0, v2, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :try_start_e
    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_e .. :try_end_e} :catch_5

    return v3

    :catch_5
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3

    :cond_6
    :goto_3
    :try_start_f
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    if-eqz v7, :cond_7

    :try_start_10
    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_10
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10 .. :try_end_10} :catch_6

    return v3

    :catch_6
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3

    :cond_7
    :try_start_11
    iget-object v7, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v7}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v7, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v7}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_7

    goto/16 :goto_0

    :catch_7
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3

    :catch_8
    move-exception v1

    :try_start_12
    const-string v2, "Failed to read hits from persisted store"

    invoke-virtual {p0, v2, v1}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :try_start_13
    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v1, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v1}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_9

    return v3

    :catch_9
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3

    :catchall_0
    move-exception v1

    :try_start_14
    iget-object v2, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v2}, Lf/h/a/f/i/h/o;->setTransactionSuccessful()V

    iget-object v2, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v2}, Lf/h/a/f/i/h/o;->endTransaction()V
    :try_end_14
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_14 .. :try_end_14} :catch_a

    throw v1

    :catch_a
    move-exception v1

    invoke-virtual {p0, v0, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return v3
.end method

.method public final N()V
    .locals 11

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    iget-boolean v0, p0, Lf/h/a/f/i/h/r;->p:Z

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->T()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/i/h/r;->i:Lf/h/a/f/i/h/n0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/n0;->a()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return-void

    :cond_1
    iget-object v0, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v0}, Lf/h/a/f/i/h/o;->I()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/f/i/h/r;->i:Lf/h/a/f/i/h/n0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/n0;->a()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    return-void

    :cond_2
    sget-object v0, Lf/h/a/f/i/h/e0;->w:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lf/h/a/f/i/h/r;->i:Lf/h/a/f/i/h/n0;

    iget-object v4, v0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v4}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    iget-object v4, v0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v4}, Lf/h/a/f/i/h/g;->e()Lf/h/a/f/i/h/a;

    iget-boolean v4, v0, Lf/h/a/f/i/h/n0;->b:Z

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    iget-object v4, v0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    iget-object v4, v4, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "com.google.analytics.RADIO_POWERED"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {v0}, Lf/h/a/f/i/h/n0;->b()Z

    move-result v4

    iput-boolean v4, v0, Lf/h/a/f/i/h/n0;->c:Z

    iget-object v4, v0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v4}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v4

    iget-boolean v5, v0, Lf/h/a/f/i/h/n0;->c:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "Registering connectivity change receiver. Network connected"

    invoke-virtual {v4, v6, v5}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iput-boolean v1, v0, Lf/h/a/f/i/h/n0;->b:Z

    :goto_1
    iget-object v0, p0, Lf/h/a/f/i/h/r;->i:Lf/h/a/f/i/h/n0;

    iget-boolean v1, v0, Lf/h/a/f/i/h/n0;->b:Z

    if-nez v1, :cond_4

    iget-object v1, v0, Lf/h/a/f/i/h/n0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v1}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v1

    const-string v4, "Connectivity unknown. Receiver not registered"

    invoke-virtual {v1, v4}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    :cond_4
    iget-boolean v1, v0, Lf/h/a/f/i/h/n0;->c:Z

    :cond_5
    if-eqz v1, :cond_e

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->O()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->T()J

    move-result-wide v0

    invoke-virtual {p0}, Lf/h/a/f/i/h/d;->o()Lf/h/a/f/i/h/p0;

    move-result-object v4

    invoke-virtual {v4}, Lf/h/a/f/i/h/p0;->F()J

    move-result-wide v4

    cmp-long v6, v4, v2

    if-eqz v6, :cond_7

    iget-object v6, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v6, v6, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v6, Lf/h/a/f/f/n/d;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    sub-long v4, v0, v4

    cmp-long v6, v4, v2

    if-lez v6, :cond_6

    goto :goto_2

    :cond_6
    sget-object v4, Lf/h/a/f/i/h/e0;->d:Lf/h/a/f/i/h/f0;

    iget-object v4, v4, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_2

    :cond_7
    sget-object v4, Lf/h/a/f/i/h/e0;->d:Lf/h/a/f/i/h/f0;

    iget-object v4, v4, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "Dispatch scheduled (ms)"

    invoke-virtual {p0, v1, v0}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/f/i/h/r;->l:Lf/h/a/f/i/h/a0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/a0;->d()Z

    move-result v0

    if-eqz v0, :cond_d

    const-wide/16 v0, 0x1

    iget-object v6, p0, Lf/h/a/f/i/h/r;->l:Lf/h/a/f/i/h/a0;

    iget-wide v7, v6, Lf/h/a/f/i/h/a0;->c:J

    cmp-long v9, v7, v2

    if-nez v9, :cond_8

    move-wide v6, v2

    goto :goto_3

    :cond_8
    iget-object v7, v6, Lf/h/a/f/i/h/a0;->a:Lf/h/a/f/i/h/g;

    iget-object v7, v7, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v7, Lf/h/a/f/f/n/d;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, v6, Lf/h/a/f/i/h/a0;->c:J

    sub-long/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    :goto_3
    add-long/2addr v4, v6

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v4, p0, Lf/h/a/f/i/h/r;->l:Lf/h/a/f/i/h/a0;

    invoke-virtual {v4}, Lf/h/a/f/i/h/a0;->d()Z

    move-result v5

    if-nez v5, :cond_9

    goto :goto_5

    :cond_9
    cmp-long v5, v0, v2

    if-gez v5, :cond_a

    invoke-virtual {v4}, Lf/h/a/f/i/h/a0;->a()V

    goto :goto_5

    :cond_a
    iget-object v5, v4, Lf/h/a/f/i/h/a0;->a:Lf/h/a/f/i/h/g;

    iget-object v5, v5, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v5, Lf/h/a/f/f/n/d;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, v4, Lf/h/a/f/i/h/a0;->c:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(J)J

    move-result-wide v5

    sub-long/2addr v0, v5

    cmp-long v5, v0, v2

    if-gez v5, :cond_b

    goto :goto_4

    :cond_b
    move-wide v2, v0

    :goto_4
    invoke-virtual {v4}, Lf/h/a/f/i/h/a0;->b()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, v4, Lf/h/a/f/i/h/a0;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Lf/h/a/f/i/h/a0;->b()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, v4, Lf/h/a/f/i/h/a0;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, v4, Lf/h/a/f/i/h/a0;->a:Lf/h/a/f/i/h/g;

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "Failed to adjust delayed post. time"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_c
    :goto_5
    return-void

    :cond_d
    iget-object v0, p0, Lf/h/a/f/i/h/r;->l:Lf/h/a/f/i/h/a0;

    invoke-virtual {v0, v4, v5}, Lf/h/a/f/i/h/a0;->e(J)V

    return-void

    :cond_e
    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->S()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/r;->O()V

    return-void
.end method

.method public final O()V
    .locals 12

    iget-object v0, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v1, v0, Lf/h/a/f/i/h/g;->h:Lf/h/a/f/i/h/c0;

    invoke-static {v1}, Lf/h/a/f/i/h/g;->a(Lf/h/a/f/i/h/e;)V

    iget-object v0, v0, Lf/h/a/f/i/h/g;->h:Lf/h/a/f/i/h/c0;

    iget-boolean v1, v0, Lf/h/a/f/i/h/c0;->f:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-boolean v1, v0, Lf/h/a/f/i/h/c0;->g:Z

    if-nez v1, :cond_3

    invoke-static {}, Lf/h/a/f/b/f;->b()V

    invoke-virtual {p0}, Lf/h/a/f/i/h/e;->E()V

    const-wide/16 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lf/h/a/f/i/h/r;->g:Lf/h/a/f/i/h/o;

    invoke-virtual {v3}, Lf/h/a/f/i/h/o;->O()J

    move-result-wide v3
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v4, "Failed to get min/max hit times from local store"

    invoke-virtual {p0, v4, v3}, Lf/h/a/f/i/h/d;->v(Ljava/lang/String;Ljava/lang/Object;)V

    move-wide v3, v1

    :goto_0
    cmp-long v5, v3, v1

    if-eqz v5, :cond_3

    iget-object v5, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v5, v5, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v5, Lf/h/a/f/f/n/d;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    sget-object v5, Lf/h/a/f/i/h/e0;->f:Lf/h/a/f/i/h/f0;

    iget-object v5, v5, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-gtz v7, :cond_3

    invoke-static {}, Lf/h/a/f/i/h/z;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "Dispatch alarm scheduled (ms)"

    invoke-virtual {p0, v4, v3}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    iget-boolean v3, v0, Lf/h/a/f/i/h/c0;->f:Z

    const-string v4, "Receiver not registered"

    invoke-static {v3, v4}, Lf/g/j/k/a;->v(ZLjava/lang/Object;)V

    invoke-static {}, Lf/h/a/f/i/h/z;->b()J

    move-result-wide v9

    cmp-long v3, v9, v1

    if-lez v3, :cond_3

    invoke-virtual {v0}, Lf/h/a/f/i/h/c0;->F()V

    iget-object v1, v0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v1, v1, Lf/h/a/f/i/h/g;->c:Lf/h/a/f/f/n/c;

    check-cast v1, Lf/h/a/f/f/n/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    add-long v7, v1, v9

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/a/f/i/h/c0;->g:Z

    sget-object v2, Lf/h/a/f/i/h/e0;->C:Lf/h/a/f/i/h/f0;

    iget-object v2, v2, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v2, v3, :cond_2

    const-string v2, "Scheduling upload with JobScheduler"

    invoke-virtual {v0, v2}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    iget-object v2, v0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v2, v2, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/ComponentName;

    const-string v4, "com.google.android.gms.analytics.AnalyticsJobService"

    invoke-direct {v3, v2, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lf/h/a/f/i/h/c0;->I()I

    move-result v4

    new-instance v5, Landroid/os/PersistableBundle;

    invoke-direct {v5}, Landroid/os/PersistableBundle;-><init>()V

    const-string v6, "action"

    const-string v7, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v5, v6, v7}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Landroid/app/job/JobInfo$Builder;

    invoke-direct {v6, v4, v3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    invoke-virtual {v6, v9, v10}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v3

    shl-long v6, v9, v1

    invoke-virtual {v3, v6, v7}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "Scheduling job. JobID"

    invoke-virtual {v0, v4, v3}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    sget-object v0, Lf/h/a/f/i/h/b1;->a:Ljava/lang/reflect/Method;

    const-string v0, "jobscheduler"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    sget-object v2, Lf/h/a/f/i/h/b1;->a:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1

    sget-object v2, Lf/h/a/f/i/h/b1;->c:Lf/h/a/f/i/h/d1;

    check-cast v2, Lf/h/a/f/i/h/c1;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto :goto_1

    :cond_2
    const-string v1, "Scheduling upload with AlarmManager"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    iget-object v5, v0, Lf/h/a/f/i/h/c0;->h:Landroid/app/AlarmManager;

    const/4 v6, 0x2

    invoke-virtual {v0}, Lf/h/a/f/i/h/c0;->J()Landroid/app/PendingIntent;

    move-result-object v11

    invoke-virtual/range {v5 .. v11}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public final S()V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/h/r;->l:Lf/h/a/f/i/h/a0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/a0;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "All hits dispatched or no network/service. Going to power save mode"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/h/r;->l:Lf/h/a/f/i/h/a0;

    invoke-virtual {v0}, Lf/h/a/f/i/h/a0;->a()V

    iget-object v0, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v1, v0, Lf/h/a/f/i/h/g;->h:Lf/h/a/f/i/h/c0;

    invoke-static {v1}, Lf/h/a/f/i/h/g;->a(Lf/h/a/f/i/h/e;)V

    iget-object v0, v0, Lf/h/a/f/i/h/g;->h:Lf/h/a/f/i/h/c0;

    iget-boolean v1, v0, Lf/h/a/f/i/h/c0;->g:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lf/h/a/f/i/h/c0;->F()V

    :cond_1
    return-void
.end method

.method public final T()J
    .locals 5

    iget-wide v0, p0, Lf/h/a/f/i/h/r;->k:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-wide v0

    :cond_0
    sget-object v0, Lf/h/a/f/i/h/e0;->c:Lf/h/a/f/i/h/f0;

    iget-object v0, v0, Lf/h/a/f/i/h/f0;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0}, Lf/h/a/f/i/h/d;->n()Lf/h/a/f/i/h/a1;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/a/f/i/h/e;->E()V

    iget-boolean v2, v2, Lf/h/a/f/i/h/a1;->h:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lf/h/a/f/i/h/d;->n()Lf/h/a/f/i/h/a1;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/e;->E()V

    iget v0, v0, Lf/h/a/f/i/h/a1;->i:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long v0, v0, v2

    :cond_1
    return-wide v0
.end method

.method public final U(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/f/o/b;->a(Landroid/content/Context;)Lf/h/a/f/f/o/a;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/f/o/a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
