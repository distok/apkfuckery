.class public final Lf/h/a/f/i/h/a1;
.super Lf/h/a/f/i/h/e;


# instance fields
.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:I

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(Lf/h/a/f/i/h/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/h/a/f/i/h/e;-><init>(Lf/h/a/f/i/h/g;)V

    return-void
.end method


# virtual methods
.method public final D()V
    .locals 6

    iget-object v0, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v0, v0, Lf/h/a/f/i/h/g;->a:Landroid/content/Context;

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x80

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "PackageManager doesn\'t know about the app package"

    invoke-virtual {p0, v2, v0}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    if-nez v0, :cond_0

    const-string v0, "Couldn\'t get ApplicationInfo to load global config"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_e

    const-string v2, "com.google.android.gms.analytics.globalConfigResource"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_e

    new-instance v2, Lf/h/a/f/i/h/g0;

    iget-object v3, p0, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    invoke-direct {v2, v3}, Lf/h/a/f/i/h/g0;-><init>(Lf/h/a/f/i/h/g;)V

    :try_start_1
    iget-object v3, v2, Lf/h/a/f/i/h/d;->d:Lf/h/a/f/i/h/g;

    iget-object v3, v3, Lf/h/a/f/i/h/g;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    invoke-virtual {v2, v0}, Lf/h/a/f/i/h/y;->B(Landroid/content/res/XmlResourceParser;)Lf/h/a/f/i/h/i0;

    move-result-object v1
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v3, "inflate() called with unknown resourceId"

    invoke-virtual {v2, v3, v0}, Lf/h/a/f/i/h/d;->q(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_1
    if-eqz v1, :cond_e

    const-string v0, "Loading global XML config values"

    invoke-virtual {p0, v0}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    iget-object v0, v1, Lf/h/a/f/i/h/i0;->a:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_2

    iput-object v0, p0, Lf/h/a/f/i/h/a1;->g:Ljava/lang/String;

    const-string v4, "XML config - app name"

    invoke-virtual {p0, v4, v0}, Lf/h/a/f/i/h/d;->d(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    iget-object v0, v1, Lf/h/a/f/i/h/i0;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    const/4 v4, 0x1

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_4

    iput-object v0, p0, Lf/h/a/f/i/h/a1;->f:Ljava/lang/String;

    const-string v4, "XML config - app version"

    invoke-virtual {p0, v4, v0}, Lf/h/a/f/i/h/d;->d(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_4
    iget-object v0, v1, Lf/h/a/f/i/h/i0;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    const/4 v4, 0x1

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    :goto_4
    const/4 v5, -0x1

    if-eqz v4, :cond_a

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "verbose"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v0, 0x0

    goto :goto_5

    :cond_6
    const-string v4, "info"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v0, 0x1

    goto :goto_5

    :cond_7
    const-string v4, "warning"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v0, 0x2

    goto :goto_5

    :cond_8
    const-string v4, "error"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x3

    goto :goto_5

    :cond_9
    const/4 v0, -0x1

    :goto_5
    if-ltz v0, :cond_a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v4, "XML config - log level"

    invoke-virtual {p0, v4, v0}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_a
    iget v0, v1, Lf/h/a/f/i/h/i0;->d:I

    if-ltz v0, :cond_b

    const/4 v4, 0x1

    goto :goto_6

    :cond_b
    const/4 v4, 0x0

    :goto_6
    if-eqz v4, :cond_c

    iput v0, p0, Lf/h/a/f/i/h/a1;->i:I

    iput-boolean v3, p0, Lf/h/a/f/i/h/a1;->h:Z

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v4, "XML config - dispatch period (sec)"

    invoke-virtual {p0, v4, v0}, Lf/h/a/f/i/h/d;->d(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_c
    iget v0, v1, Lf/h/a/f/i/h/i0;->e:I

    if-eq v0, v5, :cond_e

    if-ne v0, v3, :cond_d

    const/4 v2, 0x1

    :cond_d
    iput-boolean v2, p0, Lf/h/a/f/i/h/a1;->k:Z

    iput-boolean v3, p0, Lf/h/a/f/i/h/a1;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "XML config - dry run"

    invoke-virtual {p0, v1, v0}, Lf/h/a/f/i/h/d;->d(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_e
    return-void
.end method
