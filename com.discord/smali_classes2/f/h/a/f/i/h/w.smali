.class public final enum Lf/h/a/f/i/h/w;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/a/f/i/h/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/a/f/i/h/w;

.field public static final enum e:Lf/h/a/f/i/h/w;

.field public static final enum f:Lf/h/a/f/i/h/w;

.field public static final enum g:Lf/h/a/f/i/h/w;

.field public static final enum h:Lf/h/a/f/i/h/w;

.field public static final enum i:Lf/h/a/f/i/h/w;

.field public static final synthetic j:[Lf/h/a/f/i/h/w;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    new-instance v0, Lf/h/a/f/i/h/w;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/a/f/i/h/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/a/f/i/h/w;->d:Lf/h/a/f/i/h/w;

    new-instance v1, Lf/h/a/f/i/h/w;

    const-string v3, "BATCH_BY_SESSION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/a/f/i/h/w;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/a/f/i/h/w;->e:Lf/h/a/f/i/h/w;

    new-instance v3, Lf/h/a/f/i/h/w;

    const-string v5, "BATCH_BY_TIME"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/h/a/f/i/h/w;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/h/a/f/i/h/w;->f:Lf/h/a/f/i/h/w;

    new-instance v5, Lf/h/a/f/i/h/w;

    const-string v7, "BATCH_BY_BRUTE_FORCE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/h/a/f/i/h/w;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/h/a/f/i/h/w;->g:Lf/h/a/f/i/h/w;

    new-instance v7, Lf/h/a/f/i/h/w;

    const-string v9, "BATCH_BY_COUNT"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lf/h/a/f/i/h/w;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lf/h/a/f/i/h/w;->h:Lf/h/a/f/i/h/w;

    new-instance v9, Lf/h/a/f/i/h/w;

    const-string v11, "BATCH_BY_SIZE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lf/h/a/f/i/h/w;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lf/h/a/f/i/h/w;->i:Lf/h/a/f/i/h/w;

    const/4 v11, 0x6

    new-array v11, v11, [Lf/h/a/f/i/h/w;

    aput-object v0, v11, v2

    aput-object v1, v11, v4

    aput-object v3, v11, v6

    aput-object v5, v11, v8

    aput-object v7, v11, v10

    aput-object v9, v11, v12

    sput-object v11, Lf/h/a/f/i/h/w;->j:[Lf/h/a/f/i/h/w;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lf/h/a/f/i/h/w;
    .locals 1

    sget-object v0, Lf/h/a/f/i/h/w;->j:[Lf/h/a/f/i/h/w;

    invoke-virtual {v0}, [Lf/h/a/f/i/h/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/a/f/i/h/w;

    return-object v0
.end method
