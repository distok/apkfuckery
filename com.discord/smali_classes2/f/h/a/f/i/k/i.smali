.class public final Lf/h/a/f/i/k/i;
.super Lf/h/a/f/i/k/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/k/l<",
        "Lcom/google/android/gms/nearby/messages/MessageListener;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/k/i;->a:Ljava/util/List;

    invoke-direct {p0}, Lf/h/a/f/i/k/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lcom/google/android/gms/nearby/messages/MessageListener;

    iget-object v0, p0, Lf/h/a/f/i/k/i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/nearby/messages/internal/Update;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/nearby/messages/internal/Update;->M0(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->f:Lcom/google/android/gms/nearby/messages/Message;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/nearby/messages/MessageListener;->onFound(Lcom/google/android/gms/nearby/messages/Message;)V

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/nearby/messages/internal/Update;->M0(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->f:Lcom/google/android/gms/nearby/messages/Message;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/nearby/messages/MessageListener;->onLost(Lcom/google/android/gms/nearby/messages/Message;)V

    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/nearby/messages/internal/Update;->M0(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->f:Lcom/google/android/gms/nearby/messages/Message;

    iget-object v3, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->g:Lcom/google/android/gms/nearby/messages/internal/zze;

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/nearby/messages/MessageListener;->onDistanceChanged(Lcom/google/android/gms/nearby/messages/Message;Lcom/google/android/gms/nearby/messages/Distance;)V

    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/nearby/messages/internal/Update;->M0(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->f:Lcom/google/android/gms/nearby/messages/Message;

    iget-object v1, v1, Lcom/google/android/gms/nearby/messages/internal/Update;->h:Lcom/google/android/gms/nearby/messages/internal/zza;

    invoke-virtual {p1, v2, v1}, Lcom/google/android/gms/nearby/messages/MessageListener;->onBleSignalChanged(Lcom/google/android/gms/nearby/messages/Message;Lcom/google/android/gms/nearby/messages/BleSignal;)V

    goto :goto_0

    :cond_4
    return-void
.end method
