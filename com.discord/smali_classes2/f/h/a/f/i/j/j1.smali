.class public final enum Lf/h/a/f/i/j/j1;
.super Ljava/lang/Enum;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/y4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/a/f/i/j/j1;",
        ">;",
        "Lf/h/a/f/i/j/y4;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/a/f/i/j/j1;

.field public static final enum e:Lf/h/a/f/i/j/j1;

.field public static final enum f:Lf/h/a/f/i/j/j1;

.field public static final enum g:Lf/h/a/f/i/j/j1;

.field public static final enum h:Lf/h/a/f/i/j/j1;

.field public static final enum i:Lf/h/a/f/i/j/j1;

.field public static final synthetic j:[Lf/h/a/f/i/j/j1;


# instance fields
.field private final zzh:I


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    new-instance v0, Lf/h/a/f/i/j/j1;

    const-string v1, "AT_TRACKING_MANAGER_AUTHORIZATION_STATUS_UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/h/a/f/i/j/j1;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/h/a/f/i/j/j1;->d:Lf/h/a/f/i/j/j1;

    new-instance v1, Lf/h/a/f/i/j/j1;

    const-string v3, "AT_TRACKING_MANAGER_AUTHORIZATION_STATUS_RESTRICTED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/h/a/f/i/j/j1;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/h/a/f/i/j/j1;->e:Lf/h/a/f/i/j/j1;

    new-instance v3, Lf/h/a/f/i/j/j1;

    const-string v5, "AT_TRACKING_MANAGER_AUTHORIZATION_STATUS_DENIED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lf/h/a/f/i/j/j1;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lf/h/a/f/i/j/j1;->f:Lf/h/a/f/i/j/j1;

    new-instance v5, Lf/h/a/f/i/j/j1;

    const-string v7, "AT_TRACKING_MANAGER_AUTHORIZATION_STATUS_AUTHORIZED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v8}, Lf/h/a/f/i/j/j1;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lf/h/a/f/i/j/j1;->g:Lf/h/a/f/i/j/j1;

    new-instance v7, Lf/h/a/f/i/j/j1;

    const-string v9, "AT_TRACKING_MANAGER_AUTHORIZATION_STATUS_NOT_DETERMINED"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10, v10}, Lf/h/a/f/i/j/j1;-><init>(Ljava/lang/String;II)V

    sput-object v7, Lf/h/a/f/i/j/j1;->h:Lf/h/a/f/i/j/j1;

    new-instance v9, Lf/h/a/f/i/j/j1;

    const-string v11, "AT_TRACKING_MANAGER_AUTHORIZATION_STATUS_NOT_CONFIGURED"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12, v12}, Lf/h/a/f/i/j/j1;-><init>(Ljava/lang/String;II)V

    sput-object v9, Lf/h/a/f/i/j/j1;->i:Lf/h/a/f/i/j/j1;

    const/4 v11, 0x6

    new-array v11, v11, [Lf/h/a/f/i/j/j1;

    aput-object v0, v11, v2

    aput-object v1, v11, v4

    aput-object v3, v11, v6

    aput-object v5, v11, v8

    aput-object v7, v11, v10

    aput-object v9, v11, v12

    sput-object v11, Lf/h/a/f/i/j/j1;->j:[Lf/h/a/f/i/j/j1;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/h/a/f/i/j/j1;->zzh:I

    return-void
.end method

.method public static values()[Lf/h/a/f/i/j/j1;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/j1;->j:[Lf/h/a/f/i/j/j1;

    invoke-virtual {v0}, [Lf/h/a/f/i/j/j1;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/a/f/i/j/j1;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lf/h/a/f/i/j/j1;->zzh:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lf/h/a/f/i/j/j1;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lf/h/a/f/i/j/j1;->zzh:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
