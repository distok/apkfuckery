.class public final Lf/h/a/f/i/j/b0;
.super Lf/h/a/f/i/j/g$a;
.source "com.google.android.gms:play-services-measurement-sdk-api@@18.0.0"


# instance fields
.field public final synthetic h:Ljava/lang/String;

.field public final synthetic i:Ljava/lang/String;

.field public final synthetic j:Ljava/lang/Object;

.field public final synthetic k:Z

.field public final synthetic l:Lf/h/a/f/i/j/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/j/b0;->l:Lf/h/a/f/i/j/g;

    iput-object p2, p0, Lf/h/a/f/i/j/b0;->h:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/f/i/j/b0;->i:Ljava/lang/String;

    iput-object p4, p0, Lf/h/a/f/i/j/b0;->j:Ljava/lang/Object;

    iput-boolean p5, p0, Lf/h/a/f/i/j/b0;->k:Z

    const/4 p2, 0x1

    invoke-direct {p0, p1, p2}, Lf/h/a/f/i/j/g$a;-><init>(Lf/h/a/f/i/j/g;Z)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/b0;->l:Lf/h/a/f/i/j/g;

    iget-object v1, v0, Lf/h/a/f/i/j/g;->h:Lf/h/a/f/i/j/ec;

    iget-object v2, p0, Lf/h/a/f/i/j/b0;->h:Ljava/lang/String;

    iget-object v3, p0, Lf/h/a/f/i/j/b0;->i:Ljava/lang/String;

    iget-object v0, p0, Lf/h/a/f/i/j/b0;->j:Ljava/lang/Object;

    new-instance v4, Lf/h/a/f/g/b;

    invoke-direct {v4, v0}, Lf/h/a/f/g/b;-><init>(Ljava/lang/Object;)V

    iget-boolean v5, p0, Lf/h/a/f/i/j/b0;->k:Z

    iget-wide v6, p0, Lf/h/a/f/i/j/g$a;->d:J

    invoke-interface/range {v1 .. v7}, Lf/h/a/f/i/j/ec;->setUserProperty(Ljava/lang/String;Ljava/lang/String;Lf/h/a/f/g/a;ZJ)V

    return-void
.end method
