.class public final Lf/h/a/f/i/j/u0;
.super Lf/h/a/f/i/j/u4;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/i/j/u0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/u4<",
        "Lf/h/a/f/i/j/u0;",
        "Lf/h/a/f/i/j/u0$a;",
        ">;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# static fields
.field private static final zzm:Lf/h/a/f/i/j/u0;

.field private static volatile zzn:Lf/h/a/f/i/j/j6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/j6<",
            "Lf/h/a/f/i/j/u0;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private zzc:I

.field private zzd:J

.field private zze:Ljava/lang/String;

.field private zzf:I

.field private zzg:Lf/h/a/f/i/j/b5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/b5<",
            "Lf/h/a/f/i/j/v0;",
            ">;"
        }
    .end annotation
.end field

.field private zzh:Lf/h/a/f/i/j/b5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/b5<",
            "Lf/h/a/f/i/j/t0;",
            ">;"
        }
    .end annotation
.end field

.field private zzi:Lf/h/a/f/i/j/b5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/b5<",
            "Lf/h/a/f/i/j/k0;",
            ">;"
        }
    .end annotation
.end field

.field private zzj:Ljava/lang/String;

.field private zzk:Z

.field private zzl:Lf/h/a/f/i/j/b5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/b5<",
            "Lf/h/a/f/i/j/p1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/a/f/i/j/u0;

    invoke-direct {v0}, Lf/h/a/f/i/j/u0;-><init>()V

    sput-object v0, Lf/h/a/f/i/j/u0;->zzm:Lf/h/a/f/i/j/u0;

    const-class v1, Lf/h/a/f/i/j/u0;

    invoke-static {v1, v0}, Lf/h/a/f/i/j/u4;->r(Ljava/lang/Class;Lf/h/a/f/i/j/u4;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lf/h/a/f/i/j/u4;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lf/h/a/f/i/j/u0;->zze:Ljava/lang/String;

    sget-object v1, Lf/h/a/f/i/j/m6;->g:Lf/h/a/f/i/j/m6;

    iput-object v1, p0, Lf/h/a/f/i/j/u0;->zzg:Lf/h/a/f/i/j/b5;

    iput-object v1, p0, Lf/h/a/f/i/j/u0;->zzh:Lf/h/a/f/i/j/b5;

    iput-object v1, p0, Lf/h/a/f/i/j/u0;->zzi:Lf/h/a/f/i/j/b5;

    iput-object v0, p0, Lf/h/a/f/i/j/u0;->zzj:Ljava/lang/String;

    iput-object v1, p0, Lf/h/a/f/i/j/u0;->zzl:Lf/h/a/f/i/j/b5;

    return-void
.end method

.method public static F()Lf/h/a/f/i/j/u0$a;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/u0;->zzm:Lf/h/a/f/i/j/u0;

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4;->s()Lf/h/a/f/i/j/u4$b;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/u0$a;

    return-object v0
.end method

.method public static G()Lf/h/a/f/i/j/u0;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/u0;->zzm:Lf/h/a/f/i/j/u0;

    return-object v0
.end method

.method public static synthetic H()Lf/h/a/f/i/j/u0;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/u0;->zzm:Lf/h/a/f/i/j/u0;

    return-object v0
.end method

.method public static v(Lf/h/a/f/i/j/u0;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/h/a/f/i/j/m6;->g:Lf/h/a/f/i/j/m6;

    iput-object v0, p0, Lf/h/a/f/i/j/u0;->zzi:Lf/h/a/f/i/j/b5;

    return-void
.end method

.method public static w(Lf/h/a/f/i/j/u0;ILf/h/a/f/i/j/t0;)V
    .locals 2

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/i/j/u0;->zzh:Lf/h/a/f/i/j/b5;

    invoke-interface {v0}, Lf/h/a/f/i/j/b5;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lf/h/a/f/i/j/u4;->m(Lf/h/a/f/i/j/b5;)Lf/h/a/f/i/j/b5;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/i/j/u0;->zzh:Lf/h/a/f/i/j/b5;

    :cond_0
    iget-object p0, p0, Lf/h/a/f/i/j/u0;->zzh:Lf/h/a/f/i/j/b5;

    invoke-interface {p0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u0;->zze:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/v0;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/u0;->zzg:Lf/h/a/f/i/j/b5;

    return-object v0
.end method

.method public final C()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u0;->zzh:Lf/h/a/f/i/j/b5;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final D()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/k0;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/u0;->zzi:Lf/h/a/f/i/j/b5;

    return-object v0
.end method

.method public final E()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u0;->zzk:Z

    return v0
.end method

.method public final p(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    sget-object p2, Lf/h/a/f/i/j/x0;->a:[I

    const/4 p3, 0x1

    sub-int/2addr p1, p3

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    invoke-static {p3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lf/h/a/f/i/j/u0;->zzn:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_1

    const-class p2, Lf/h/a/f/i/j/u0;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/a/f/i/j/u0;->zzn:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/a/f/i/j/u4$a;

    sget-object p3, Lf/h/a/f/i/j/u0;->zzm:Lf/h/a/f/i/j/u0;

    invoke-direct {p1, p3}, Lf/h/a/f/i/j/u4$a;-><init>(Lf/h/a/f/i/j/u4;)V

    sput-object p1, Lf/h/a/f/i/j/u0;->zzn:Lf/h/a/f/i/j/j6;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lf/h/a/f/i/j/u0;->zzm:Lf/h/a/f/i/j/u0;

    return-object p1

    :pswitch_4
    const/16 p1, 0xe

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string/jumbo v0, "zzc"

    aput-object v0, p1, p2

    const-string/jumbo p2, "zzd"

    aput-object p2, p1, p3

    const/4 p2, 0x2

    const-string/jumbo p3, "zze"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-string/jumbo p3, "zzf"

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string/jumbo p3, "zzg"

    aput-object p3, p1, p2

    const/4 p2, 0x5

    const-class p3, Lf/h/a/f/i/j/v0;

    aput-object p3, p1, p2

    const/4 p2, 0x6

    const-string/jumbo p3, "zzh"

    aput-object p3, p1, p2

    const/4 p2, 0x7

    const-class p3, Lf/h/a/f/i/j/t0;

    aput-object p3, p1, p2

    const/16 p2, 0x8

    const-string/jumbo p3, "zzi"

    aput-object p3, p1, p2

    const/16 p2, 0x9

    const-class p3, Lf/h/a/f/i/j/k0;

    aput-object p3, p1, p2

    const/16 p2, 0xa

    const-string/jumbo p3, "zzj"

    aput-object p3, p1, p2

    const/16 p2, 0xb

    const-string/jumbo p3, "zzk"

    aput-object p3, p1, p2

    const/16 p2, 0xc

    const-string/jumbo p3, "zzl"

    aput-object p3, p1, p2

    const/16 p2, 0xd

    const-class p3, Lf/h/a/f/i/j/p1;

    aput-object p3, p1, p2

    const-string p2, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0004\u0000\u0001\u1002\u0000\u0002\u1008\u0001\u0003\u1004\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\u1008\u0003\u0008\u1007\u0004\t\u001b"

    sget-object p3, Lf/h/a/f/i/j/u0;->zzm:Lf/h/a/f/i/j/u0;

    new-instance v0, Lf/h/a/f/i/j/o6;

    invoke-direct {v0, p3, p2, p1}, Lf/h/a/f/i/j/o6;-><init>(Lf/h/a/f/i/j/c6;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    new-instance p1, Lf/h/a/f/i/j/u0$a;

    invoke-direct {p1, p2}, Lf/h/a/f/i/j/u0$a;-><init>(Lf/h/a/f/i/j/x0;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lf/h/a/f/i/j/u0;

    invoke-direct {p1}, Lf/h/a/f/i/j/u0;-><init>()V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final u(I)Lf/h/a/f/i/j/t0;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u0;->zzh:Lf/h/a/f/i/j/b5;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/t0;

    return-object p1
.end method

.method public final x()Z
    .locals 2

    iget v0, p0, Lf/h/a/f/i/j/u0;->zzc:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final y()J
    .locals 2

    iget-wide v0, p0, Lf/h/a/f/i/j/u0;->zzd:J

    return-wide v0
.end method

.method public final z()Z
    .locals 1

    iget v0, p0, Lf/h/a/f/i/j/u0;->zzc:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
