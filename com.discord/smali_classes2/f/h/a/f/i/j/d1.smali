.class public final Lf/h/a/f/i/j/d1;
.super Lf/h/a/f/i/j/u4;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/i/j/d1$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/u4<",
        "Lf/h/a/f/i/j/d1;",
        "Lf/h/a/f/i/j/d1$a;",
        ">;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# static fields
.field private static final zzd:Lf/h/a/f/i/j/d1;

.field private static volatile zze:Lf/h/a/f/i/j/j6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/j6<",
            "Lf/h/a/f/i/j/d1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private zzc:Lf/h/a/f/i/j/b5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/b5<",
            "Lf/h/a/f/i/j/e1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/a/f/i/j/d1;

    invoke-direct {v0}, Lf/h/a/f/i/j/d1;-><init>()V

    sput-object v0, Lf/h/a/f/i/j/d1;->zzd:Lf/h/a/f/i/j/d1;

    const-class v1, Lf/h/a/f/i/j/d1;

    invoke-static {v1, v0}, Lf/h/a/f/i/j/u4;->r(Ljava/lang/Class;Lf/h/a/f/i/j/u4;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/a/f/i/j/u4;-><init>()V

    sget-object v0, Lf/h/a/f/i/j/m6;->g:Lf/h/a/f/i/j/m6;

    iput-object v0, p0, Lf/h/a/f/i/j/d1;->zzc:Lf/h/a/f/i/j/b5;

    return-void
.end method

.method public static w(Lf/h/a/f/i/j/d1;Lf/h/a/f/i/j/e1;)V
    .locals 2

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/i/j/d1;->zzc:Lf/h/a/f/i/j/b5;

    invoke-interface {v0}, Lf/h/a/f/i/j/b5;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lf/h/a/f/i/j/u4;->m(Lf/h/a/f/i/j/b5;)Lf/h/a/f/i/j/b5;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/i/j/d1;->zzc:Lf/h/a/f/i/j/b5;

    :cond_0
    iget-object p0, p0, Lf/h/a/f/i/j/d1;->zzc:Lf/h/a/f/i/j/b5;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static x()Lf/h/a/f/i/j/d1$a;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/d1;->zzd:Lf/h/a/f/i/j/d1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4;->s()Lf/h/a/f/i/j/u4$b;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/d1$a;

    return-object v0
.end method

.method public static synthetic y()Lf/h/a/f/i/j/d1;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/d1;->zzd:Lf/h/a/f/i/j/d1;

    return-object v0
.end method


# virtual methods
.method public final p(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    sget-object p2, Lf/h/a/f/i/j/k1;->a:[I

    const/4 p3, 0x1

    sub-int/2addr p1, p3

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    invoke-static {p3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lf/h/a/f/i/j/d1;->zze:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_1

    const-class p2, Lf/h/a/f/i/j/d1;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/a/f/i/j/d1;->zze:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/a/f/i/j/u4$a;

    sget-object p3, Lf/h/a/f/i/j/d1;->zzd:Lf/h/a/f/i/j/d1;

    invoke-direct {p1, p3}, Lf/h/a/f/i/j/u4$a;-><init>(Lf/h/a/f/i/j/u4;)V

    sput-object p1, Lf/h/a/f/i/j/d1;->zze:Lf/h/a/f/i/j/j6;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lf/h/a/f/i/j/d1;->zzd:Lf/h/a/f/i/j/d1;

    return-object p1

    :pswitch_4
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string/jumbo v0, "zzc"

    aput-object v0, p1, p2

    const-class p2, Lf/h/a/f/i/j/e1;

    aput-object p2, p1, p3

    const-string p2, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b"

    sget-object p3, Lf/h/a/f/i/j/d1;->zzd:Lf/h/a/f/i/j/d1;

    new-instance v0, Lf/h/a/f/i/j/o6;

    invoke-direct {v0, p3, p2, p1}, Lf/h/a/f/i/j/o6;-><init>(Lf/h/a/f/i/j/c6;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    new-instance p1, Lf/h/a/f/i/j/d1$a;

    invoke-direct {p1, p2}, Lf/h/a/f/i/j/d1$a;-><init>(Lf/h/a/f/i/j/k1;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lf/h/a/f/i/j/d1;

    invoke-direct {p1}, Lf/h/a/f/i/j/d1;-><init>()V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final u()Lf/h/a/f/i/j/e1;
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/d1;->zzc:Lf/h/a/f/i/j/b5;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/e1;

    return-object v0
.end method

.method public final v()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/e1;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/d1;->zzc:Lf/h/a/f/i/j/b5;

    return-object v0
.end method
