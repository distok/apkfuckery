.class public final Lf/h/a/f/i/j/g4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/v7;


# instance fields
.field public final a:Lcom/google/android/gms/internal/measurement/zzhi;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/measurement/zzhi;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/h/a/f/i/j/w4;->a:Ljava/nio/charset/Charset;

    iput-object p1, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    iput-object p0, p1, Lcom/google/android/gms/internal/measurement/zzhi;->a:Lf/h/a/f/i/j/g4;

    return-void
.end method


# virtual methods
.method public final a(ID)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide p2

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzhi;->z(IJ)V

    return-void
.end method

.method public final b(IF)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result p2

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/zzhi;->G(II)V

    return-void
.end method

.method public final c(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    instance-of v0, p2, Lf/h/a/f/i/j/t3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    check-cast p2, Lf/h/a/f/i/j/t3;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/zzhi;->t(ILf/h/a/f/i/j/t3;)V

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    check-cast p2, Lf/h/a/f/i/j/c6;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/zzhi;->i(ILf/h/a/f/i/j/c6;)V

    return-void
.end method

.method public final d(ILjava/lang/Object;Lf/h/a/f/i/j/q6;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    check-cast p2, Lf/h/a/f/i/j/c6;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzhi;->j(ILf/h/a/f/i/j/c6;Lf/h/a/f/i/j/q6;)V

    return-void
.end method

.method public final e(ILjava/lang/Object;Lf/h/a/f/i/j/q6;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    check-cast p2, Lf/h/a/f/i/j/c6;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/internal/measurement/zzhi;->f(II)V

    iget-object v1, v0, Lcom/google/android/gms/internal/measurement/zzhi;->a:Lf/h/a/f/i/j/g4;

    invoke-interface {p3, p2, v1}, Lf/h/a/f/i/j/q6;->h(Ljava/lang/Object;Lf/h/a/f/i/j/v7;)V

    const/4 p2, 0x4

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/zzhi;->f(II)V

    return-void
.end method

.method public final f(IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    invoke-static {p2, p3}, Lcom/google/android/gms/internal/measurement/zzhi;->R(J)J

    move-result-wide p2

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/measurement/zzhi;->g(IJ)V

    return-void
.end method

.method public final g(II)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/g4;->a:Lcom/google/android/gms/internal/measurement/zzhi;

    invoke-static {p2}, Lcom/google/android/gms/internal/measurement/zzhi;->V(I)I

    move-result p2

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/measurement/zzhi;->y(II)V

    return-void
.end method
