.class public final enum Lf/h/a/f/i/j/o4;
.super Ljava/lang/Enum;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/a/f/i/j/o4;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lf/h/a/f/i/j/o4;

.field public static final enum B:Lf/h/a/f/i/j/o4;

.field public static final enum C:Lf/h/a/f/i/j/o4;

.field public static final enum D:Lf/h/a/f/i/j/o4;

.field public static final enum E:Lf/h/a/f/i/j/o4;

.field public static final enum F:Lf/h/a/f/i/j/o4;

.field public static final enum G:Lf/h/a/f/i/j/o4;

.field public static final enum H:Lf/h/a/f/i/j/o4;

.field public static final enum I:Lf/h/a/f/i/j/o4;

.field public static final enum J:Lf/h/a/f/i/j/o4;

.field public static final enum K:Lf/h/a/f/i/j/o4;

.field public static final enum L:Lf/h/a/f/i/j/o4;

.field public static final enum M:Lf/h/a/f/i/j/o4;

.field public static final enum N:Lf/h/a/f/i/j/o4;

.field public static final enum O:Lf/h/a/f/i/j/o4;

.field public static final enum P:Lf/h/a/f/i/j/o4;

.field public static final enum Q:Lf/h/a/f/i/j/o4;

.field public static final enum R:Lf/h/a/f/i/j/o4;

.field public static final enum S:Lf/h/a/f/i/j/o4;

.field public static final enum T:Lf/h/a/f/i/j/o4;

.field public static final enum U:Lf/h/a/f/i/j/o4;

.field public static final enum V:Lf/h/a/f/i/j/o4;

.field public static final enum W:Lf/h/a/f/i/j/o4;

.field public static final enum X:Lf/h/a/f/i/j/o4;

.field public static final enum Y:Lf/h/a/f/i/j/o4;

.field public static final enum Z:Lf/h/a/f/i/j/o4;

.field public static final enum a0:Lf/h/a/f/i/j/o4;

.field public static final enum b0:Lf/h/a/f/i/j/o4;

.field public static final c0:[Lf/h/a/f/i/j/o4;

.field public static final enum d:Lf/h/a/f/i/j/o4;

.field public static final synthetic d0:[Lf/h/a/f/i/j/o4;

.field public static final enum e:Lf/h/a/f/i/j/o4;

.field public static final enum f:Lf/h/a/f/i/j/o4;

.field public static final enum g:Lf/h/a/f/i/j/o4;

.field public static final enum h:Lf/h/a/f/i/j/o4;

.field public static final enum i:Lf/h/a/f/i/j/o4;

.field public static final enum j:Lf/h/a/f/i/j/o4;

.field public static final enum k:Lf/h/a/f/i/j/o4;

.field public static final enum l:Lf/h/a/f/i/j/o4;

.field public static final enum m:Lf/h/a/f/i/j/o4;

.field public static final enum n:Lf/h/a/f/i/j/o4;

.field public static final enum o:Lf/h/a/f/i/j/o4;

.field public static final enum p:Lf/h/a/f/i/j/o4;

.field public static final enum q:Lf/h/a/f/i/j/o4;

.field public static final enum r:Lf/h/a/f/i/j/o4;

.field public static final enum s:Lf/h/a/f/i/j/o4;

.field public static final enum t:Lf/h/a/f/i/j/o4;

.field public static final enum u:Lf/h/a/f/i/j/o4;

.field public static final enum v:Lf/h/a/f/i/j/o4;

.field public static final enum w:Lf/h/a/f/i/j/o4;

.field public static final enum x:Lf/h/a/f/i/j/o4;

.field public static final enum y:Lf/h/a/f/i/j/o4;

.field public static final enum z:Lf/h/a/f/i/j/o4;


# instance fields
.field private final zzaz:Lf/h/a/f/i/j/e5;

.field private final zzba:I

.field private final zzbb:Lf/h/a/f/i/j/q4;

.field private final zzbc:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private final zzbd:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 17

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v7, Lf/h/a/f/i/j/q4;->d:Lf/h/a/f/i/j/q4;

    sget-object v8, Lf/h/a/f/i/j/e5;->h:Lf/h/a/f/i/j/e5;

    const-string v1, "DOUBLE"

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, v6

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->d:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v9, Lf/h/a/f/i/j/e5;->g:Lf/h/a/f/i/j/e5;

    const-string v1, "FLOAT"

    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object v0, v6

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->e:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v10, Lf/h/a/f/i/j/e5;->f:Lf/h/a/f/i/j/e5;

    const-string v1, "INT64"

    const/4 v2, 0x2

    const/4 v3, 0x2

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->f:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "UINT64"

    const/4 v2, 0x3

    const/4 v3, 0x3

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->g:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v11, Lf/h/a/f/i/j/e5;->e:Lf/h/a/f/i/j/e5;

    const-string v1, "INT32"

    const/4 v2, 0x4

    const/4 v3, 0x4

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->h:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FIXED64"

    const/4 v2, 0x5

    const/4 v3, 0x5

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->i:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FIXED32"

    const/4 v2, 0x6

    const/4 v3, 0x6

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->j:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v12, Lf/h/a/f/i/j/e5;->i:Lf/h/a/f/i/j/e5;

    const-string v1, "BOOL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    move-object v0, v6

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->k:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v13, Lf/h/a/f/i/j/e5;->j:Lf/h/a/f/i/j/e5;

    const-string v1, "STRING"

    const/16 v2, 0x8

    const/16 v3, 0x8

    move-object v0, v6

    move-object v5, v13

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->l:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v14, Lf/h/a/f/i/j/e5;->m:Lf/h/a/f/i/j/e5;

    const-string v1, "MESSAGE"

    const/16 v2, 0x9

    const/16 v3, 0x9

    move-object v0, v6

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->m:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v15, Lf/h/a/f/i/j/e5;->k:Lf/h/a/f/i/j/e5;

    const-string v1, "BYTES"

    const/16 v2, 0xa

    const/16 v3, 0xa

    move-object v0, v6

    move-object v5, v15

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->n:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "UINT32"

    const/16 v2, 0xb

    const/16 v3, 0xb

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->o:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v16, Lf/h/a/f/i/j/e5;->l:Lf/h/a/f/i/j/e5;

    const-string v1, "ENUM"

    const/16 v2, 0xc

    const/16 v3, 0xc

    move-object v0, v6

    move-object/from16 v5, v16

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->p:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SFIXED32"

    const/16 v2, 0xd

    const/16 v3, 0xd

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->q:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SFIXED64"

    const/16 v2, 0xe

    const/16 v3, 0xe

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->r:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SINT32"

    const/16 v2, 0xf

    const/16 v3, 0xf

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->s:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SINT64"

    const/16 v2, 0x10

    const/16 v3, 0x10

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->t:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "GROUP"

    const/16 v2, 0x11

    const/16 v3, 0x11

    move-object v0, v6

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->u:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v7, Lf/h/a/f/i/j/q4;->e:Lf/h/a/f/i/j/q4;

    const-string v1, "DOUBLE_LIST"

    const/16 v2, 0x12

    const/16 v3, 0x12

    move-object v0, v6

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->v:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FLOAT_LIST"

    const/16 v2, 0x13

    const/16 v3, 0x13

    move-object v0, v6

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->w:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "INT64_LIST"

    const/16 v2, 0x14

    const/16 v3, 0x14

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->x:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "UINT64_LIST"

    const/16 v2, 0x15

    const/16 v3, 0x15

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->y:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "INT32_LIST"

    const/16 v2, 0x16

    const/16 v3, 0x16

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->z:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FIXED64_LIST"

    const/16 v2, 0x17

    const/16 v3, 0x17

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->A:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FIXED32_LIST"

    const/16 v2, 0x18

    const/16 v3, 0x18

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->B:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "BOOL_LIST"

    const/16 v2, 0x19

    const/16 v3, 0x19

    move-object v0, v6

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->C:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "STRING_LIST"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    move-object v0, v6

    move-object v5, v13

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->D:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "MESSAGE_LIST"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    move-object v0, v6

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->E:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "BYTES_LIST"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    move-object v0, v6

    move-object v5, v15

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->F:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "UINT32_LIST"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->G:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "ENUM_LIST"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    move-object v0, v6

    move-object/from16 v5, v16

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->H:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SFIXED32_LIST"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->I:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SFIXED64_LIST"

    const/16 v2, 0x20

    const/16 v3, 0x20

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->J:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SINT32_LIST"

    const/16 v2, 0x21

    const/16 v3, 0x21

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->K:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SINT64_LIST"

    const/16 v2, 0x22

    const/16 v3, 0x22

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->L:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v13, Lf/h/a/f/i/j/q4;->f:Lf/h/a/f/i/j/q4;

    const-string v1, "DOUBLE_LIST_PACKED"

    const/16 v2, 0x23

    const/16 v3, 0x23

    move-object v0, v6

    move-object v4, v13

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->M:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FLOAT_LIST_PACKED"

    const/16 v2, 0x24

    const/16 v3, 0x24

    move-object v0, v6

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->N:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "INT64_LIST_PACKED"

    const/16 v2, 0x25

    const/16 v3, 0x25

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->O:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "UINT64_LIST_PACKED"

    const/16 v2, 0x26

    const/16 v3, 0x26

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->P:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "INT32_LIST_PACKED"

    const/16 v2, 0x27

    const/16 v3, 0x27

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->Q:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FIXED64_LIST_PACKED"

    const/16 v2, 0x28

    const/16 v3, 0x28

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->R:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "FIXED32_LIST_PACKED"

    const/16 v2, 0x29

    const/16 v3, 0x29

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->S:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "BOOL_LIST_PACKED"

    const/16 v2, 0x2a

    const/16 v3, 0x2a

    move-object v0, v6

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->T:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "UINT32_LIST_PACKED"

    const/16 v2, 0x2b

    const/16 v3, 0x2b

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->U:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "ENUM_LIST_PACKED"

    const/16 v2, 0x2c

    const/16 v3, 0x2c

    move-object v0, v6

    move-object/from16 v5, v16

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->V:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SFIXED32_LIST_PACKED"

    const/16 v2, 0x2d

    const/16 v3, 0x2d

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->W:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v8, Lf/h/a/f/i/j/e5;->f:Lf/h/a/f/i/j/e5;

    const-string v1, "SFIXED64_LIST_PACKED"

    const/16 v2, 0x2e

    const/16 v3, 0x2e

    move-object v0, v6

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->X:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    sget-object v5, Lf/h/a/f/i/j/e5;->e:Lf/h/a/f/i/j/e5;

    const-string v1, "SINT32_LIST_PACKED"

    const/16 v2, 0x2f

    const/16 v3, 0x2f

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->Y:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "SINT64_LIST_PACKED"

    const/16 v2, 0x30

    const/16 v3, 0x30

    move-object v0, v6

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->Z:Lf/h/a/f/i/j/o4;

    new-instance v6, Lf/h/a/f/i/j/o4;

    const-string v1, "GROUP_LIST"

    const/16 v2, 0x31

    const/16 v3, 0x31

    move-object v0, v6

    move-object v4, v7

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v6, Lf/h/a/f/i/j/o4;->a0:Lf/h/a/f/i/j/o4;

    new-instance v0, Lf/h/a/f/i/j/o4;

    sget-object v12, Lf/h/a/f/i/j/q4;->g:Lf/h/a/f/i/j/q4;

    sget-object v13, Lf/h/a/f/i/j/e5;->d:Lf/h/a/f/i/j/e5;

    const-string v9, "MAP"

    const/16 v10, 0x32

    const/16 v11, 0x32

    move-object v8, v0

    invoke-direct/range {v8 .. v13}, Lf/h/a/f/i/j/o4;-><init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V

    sput-object v0, Lf/h/a/f/i/j/o4;->b0:Lf/h/a/f/i/j/o4;

    const/16 v0, 0x33

    new-array v1, v0, [Lf/h/a/f/i/j/o4;

    sget-object v2, Lf/h/a/f/i/j/o4;->d:Lf/h/a/f/i/j/o4;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lf/h/a/f/i/j/o4;->e:Lf/h/a/f/i/j/o4;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->f:Lf/h/a/f/i/j/o4;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->g:Lf/h/a/f/i/j/o4;

    const/4 v4, 0x3

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->h:Lf/h/a/f/i/j/o4;

    const/4 v4, 0x4

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->i:Lf/h/a/f/i/j/o4;

    const/4 v4, 0x5

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->j:Lf/h/a/f/i/j/o4;

    const/4 v4, 0x6

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->k:Lf/h/a/f/i/j/o4;

    const/4 v4, 0x7

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->l:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x8

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->m:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x9

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->n:Lf/h/a/f/i/j/o4;

    const/16 v4, 0xa

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->o:Lf/h/a/f/i/j/o4;

    const/16 v4, 0xb

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->p:Lf/h/a/f/i/j/o4;

    const/16 v4, 0xc

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->q:Lf/h/a/f/i/j/o4;

    const/16 v4, 0xd

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->r:Lf/h/a/f/i/j/o4;

    const/16 v4, 0xe

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->s:Lf/h/a/f/i/j/o4;

    const/16 v4, 0xf

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->t:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x10

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->u:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x11

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->v:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x12

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->w:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x13

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->x:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x14

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->y:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x15

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->z:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x16

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->A:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x17

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->B:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x18

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->C:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x19

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->D:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x1a

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->E:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x1b

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->F:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x1c

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->G:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x1d

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->H:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x1e

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->I:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x1f

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->J:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x20

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->K:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x21

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->L:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x22

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->M:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x23

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->N:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x24

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->O:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x25

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->P:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x26

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->Q:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x27

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->R:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x28

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->S:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x29

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->T:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x2a

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->U:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x2b

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->V:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x2c

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->W:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x2d

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->X:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x2e

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->Y:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x2f

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->Z:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x30

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->a0:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x31

    aput-object v2, v1, v4

    sget-object v2, Lf/h/a/f/i/j/o4;->b0:Lf/h/a/f/i/j/o4;

    const/16 v4, 0x32

    aput-object v2, v1, v4

    sput-object v1, Lf/h/a/f/i/j/o4;->d0:[Lf/h/a/f/i/j/o4;

    invoke-static {}, Lf/h/a/f/i/j/o4;->values()[Lf/h/a/f/i/j/o4;

    move-result-object v1

    new-array v0, v0, [Lf/h/a/f/i/j/o4;

    sput-object v0, Lf/h/a/f/i/j/o4;->c0:[Lf/h/a/f/i/j/o4;

    array-length v0, v1

    :goto_0
    if-ge v3, v0, :cond_0

    aget-object v2, v1, v3

    sget-object v4, Lf/h/a/f/i/j/o4;->c0:[Lf/h/a/f/i/j/o4;

    iget v5, v2, Lf/h/a/f/i/j/o4;->zzba:I

    aput-object v2, v4, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILf/h/a/f/i/j/q4;Lf/h/a/f/i/j/e5;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/a/f/i/j/q4;",
            "Lf/h/a/f/i/j/e5;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/h/a/f/i/j/o4;->zzba:I

    iput-object p4, p0, Lf/h/a/f/i/j/o4;->zzbb:Lf/h/a/f/i/j/q4;

    iput-object p5, p0, Lf/h/a/f/i/j/o4;->zzaz:Lf/h/a/f/i/j/e5;

    sget-object p1, Lf/h/a/f/i/j/r4;->a:[I

    invoke-virtual {p4}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x2

    const/4 p3, 0x1

    if-eq p1, p3, :cond_1

    if-eq p1, p2, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/f/i/j/o4;->zzbc:Ljava/lang/Class;

    goto :goto_0

    :cond_0
    invoke-virtual {p5}, Lf/h/a/f/i/j/e5;->f()Ljava/lang/Class;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/f/i/j/o4;->zzbc:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    invoke-virtual {p5}, Lf/h/a/f/i/j/e5;->f()Ljava/lang/Class;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/f/i/j/o4;->zzbc:Ljava/lang/Class;

    :goto_0
    sget-object p1, Lf/h/a/f/i/j/q4;->d:Lf/h/a/f/i/j/q4;

    if-ne p4, p1, :cond_2

    sget-object p1, Lf/h/a/f/i/j/r4;->b:[I

    invoke-virtual {p5}, Ljava/lang/Enum;->ordinal()I

    move-result p4

    aget p1, p1, p4

    if-eq p1, p3, :cond_2

    if-eq p1, p2, :cond_2

    const/4 p2, 0x3

    if-eq p1, p2, :cond_2

    goto :goto_1

    :cond_2
    const/4 p3, 0x0

    :goto_1
    iput-boolean p3, p0, Lf/h/a/f/i/j/o4;->zzbd:Z

    return-void
.end method

.method public static values()[Lf/h/a/f/i/j/o4;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/o4;->d0:[Lf/h/a/f/i/j/o4;

    invoke-virtual {v0}, [Lf/h/a/f/i/j/o4;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/a/f/i/j/o4;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lf/h/a/f/i/j/o4;->zzba:I

    return v0
.end method
