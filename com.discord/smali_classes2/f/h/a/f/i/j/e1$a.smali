.class public final Lf/h/a/f/i/j/e1$a;
.super Lf/h/a/f/i/j/u4$b;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/j/e1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/u4$b<",
        "Lf/h/a/f/i/j/e1;",
        "Lf/h/a/f/i/j/e1$a;",
        ">;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lf/h/a/f/i/j/e1;->v0()Lf/h/a/f/i/j/e1;

    move-result-object v0

    invoke-direct {p0, v0}, Lf/h/a/f/i/j/u4$b;-><init>(Lf/h/a/f/i/j/u4;)V

    return-void
.end method

.method public constructor <init>(Lf/h/a/f/i/j/k1;)V
    .locals 0

    invoke-static {}, Lf/h/a/f/i/j/e1;->v0()Lf/h/a/f/i/j/e1;

    move-result-object p1

    invoke-direct {p0, p1}, Lf/h/a/f/i/j/u4$b;-><init>(Lf/h/a/f/i/j/u4;)V

    return-void
.end method


# virtual methods
.method public final A(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->C(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final B(Z)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->D(Lf/h/a/f/i/j/e1;Z)V

    return-object p0
.end method

.method public final C()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/a1;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->y0()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final D()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->E0()I

    move-result v0

    return v0
.end method

.method public final E(I)Lf/h/a/f/i/j/a1;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/j/e1;->u(I)Lf/h/a/f/i/j/a1;

    move-result-object p1

    return-object p1
.end method

.method public final F(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->k0(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final G(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->l0(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final H()Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0}, Lf/h/a/f/i/j/e1;->v(Lf/h/a/f/i/j/e1;)V

    return-object p0
.end method

.method public final I(I)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->j0(Lf/h/a/f/i/j/e1;I)V

    return-object p0
.end method

.method public final J(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->B0(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final K(Ljava/lang/Iterable;)Lf/h/a/f/i/j/e1$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lf/h/a/f/i/j/y0;",
            ">;)",
            "Lf/h/a/f/i/j/e1$a;"
        }
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->C0(Lf/h/a/f/i/j/e1;Ljava/lang/Iterable;)V

    return-object p0
.end method

.method public final L(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->D0(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final M(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->H0(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final N(Ljava/lang/Iterable;)Lf/h/a/f/i/j/e1$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Integer;",
            ">;)",
            "Lf/h/a/f/i/j/e1$a;"
        }
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->I0(Lf/h/a/f/i/j/e1;Ljava/lang/Iterable;)V

    return-object p0
.end method

.method public final O(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->J0(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final P(I)Lf/h/a/f/i/j/i1;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/j/e1;->h0(I)Lf/h/a/f/i/j/i1;

    move-result-object p1

    return-object p1
.end method

.method public final Q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/i1;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->K0()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final R()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->P0()I

    move-result v0

    return v0
.end method

.method public final S(I)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->A0(Lf/h/a/f/i/j/e1;I)V

    return-object p0
.end method

.method public final T(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->N0(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final U(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->O0(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final V()J
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->h1()J

    move-result-wide v0

    return-wide v0
.end method

.method public final W(I)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->G0(Lf/h/a/f/i/j/e1;I)V

    return-object p0
.end method

.method public final X(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->S0(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final Y(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->T0(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final Z()J
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->o1()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a0(I)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->M0(Lf/h/a/f/i/j/e1;I)V

    return-object p0
.end method

.method public final b0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->X0(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final c0()Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0}, Lf/h/a/f/i/j/e1;->i0(Lf/h/a/f/i/j/e1;)V

    return-object p0
.end method

.method public final d0(I)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->R0(Lf/h/a/f/i/j/e1;I)V

    return-object p0
.end method

.method public final e0(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->b1(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final f0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->c1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final g0()Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0}, Lf/h/a/f/i/j/e1;->z0(Lf/h/a/f/i/j/e1;)V

    return-object p0
.end method

.method public final h0(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->e1(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final i0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->f1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final j0(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->j1(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final k0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->k1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final l0()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->G1()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m0(J)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->l1(Lf/h/a/f/i/j/e1;J)V

    return-object p0
.end method

.method public final n0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->m1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final o0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->q1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final p()Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0}, Lf/h/a/f/i/j/e1;->G(Lf/h/a/f/i/j/e1;)V

    return-object p0
.end method

.method public final p0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->r1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final q(ILf/h/a/f/i/j/a1$a;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {p2}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object p2

    check-cast p2, Lf/h/a/f/i/j/u4;

    check-cast p2, Lf/h/a/f/i/j/a1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->w(Lf/h/a/f/i/j/e1;ILf/h/a/f/i/j/a1;)V

    return-object p0
.end method

.method public final q0()Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0}, Lf/h/a/f/i/j/e1;->U0(Lf/h/a/f/i/j/e1;)V

    return-object p0
.end method

.method public final r(ILf/h/a/f/i/j/i1;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/e1;->x(Lf/h/a/f/i/j/e1;ILf/h/a/f/i/j/i1;)V

    return-object p0
.end method

.method public final r0()Lf/h/a/f/i/j/e1$a;
    .locals 2

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    sget v1, Lf/h/a/f/i/j/e1;->d:I

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    throw v0
.end method

.method public final s(Lf/h/a/f/i/j/a1$a;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4;

    check-cast p1, Lf/h/a/f/i/j/a1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->z(Lf/h/a/f/i/j/e1;Lf/h/a/f/i/j/a1;)V

    return-object p0
.end method

.method public final s0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->u1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final t0()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->S()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u(Lf/h/a/f/i/j/i1$a;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4;

    check-cast p1, Lf/h/a/f/i/j/i1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->A(Lf/h/a/f/i/j/e1;Lf/h/a/f/i/j/i1;)V

    return-object p0
.end method

.method public final u0()Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0}, Lf/h/a/f/i/j/e1;->a1(Lf/h/a/f/i/j/e1;)V

    return-object p0
.end method

.method public final v0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->x1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final w(Lf/h/a/f/i/j/i1;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->A(Lf/h/a/f/i/j/e1;Lf/h/a/f/i/j/i1;)V

    return-object p0
.end method

.method public final w0()Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0}, Lf/h/a/f/i/j/e1;->d1(Lf/h/a/f/i/j/e1;)V

    return-object p0
.end method

.method public final x(Ljava/lang/Iterable;)Lf/h/a/f/i/j/e1$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lf/h/a/f/i/j/a1;",
            ">;)",
            "Lf/h/a/f/i/j/e1$a;"
        }
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->B(Lf/h/a/f/i/j/e1;Ljava/lang/Iterable;)V

    return-object p0
.end method

.method public final x0(Ljava/lang/String;)Lf/h/a/f/i/j/e1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/e1;->z1(Lf/h/a/f/i/j/e1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final y0()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/e1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/e1;->r0()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
