.class public final Lf/h/a/f/i/j/l4;
.super Lf/h/a/f/i/j/j4;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/j4<",
        "Lf/h/a/f/i/j/u4$c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/a/f/i/j/j4;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map$Entry;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)I"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4$c;

    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method public final b(Ljava/lang/Object;)Lf/h/a/f/i/j/n4;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/a/f/i/j/n4<",
            "Lf/h/a/f/i/j/u4$c;",
            ">;"
        }
    .end annotation

    check-cast p1, Lf/h/a/f/i/j/u4$d;

    iget-object p1, p1, Lf/h/a/f/i/j/u4$d;->zzc:Lf/h/a/f/i/j/n4;

    return-object p1
.end method

.method public final c(Lf/h/a/f/i/j/h4;Lf/h/a/f/i/j/c6;I)Ljava/lang/Object;
    .locals 1

    iget-object p1, p1, Lf/h/a/f/i/j/h4;->a:Ljava/util/Map;

    new-instance v0, Lf/h/a/f/i/j/h4$a;

    invoke-direct {v0, p2, p3}, Lf/h/a/f/i/j/h4$a;-><init>(Ljava/lang/Object;I)V

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4$f;

    return-object p1
.end method

.method public final d(Lf/h/a/f/i/j/v7;Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/i/j/v7;",
            "Ljava/util/Map$Entry<",
            "**>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4$c;

    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method public final e(Lf/h/a/f/i/j/c6;)Z
    .locals 0

    instance-of p1, p1, Lf/h/a/f/i/j/u4$d;

    return p1
.end method

.method public final f(Ljava/lang/Object;)Lf/h/a/f/i/j/n4;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/a/f/i/j/n4<",
            "Lf/h/a/f/i/j/u4$c;",
            ">;"
        }
    .end annotation

    check-cast p1, Lf/h/a/f/i/j/u4$d;

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$d;->u()Lf/h/a/f/i/j/n4;

    move-result-object p1

    return-object p1
.end method

.method public final g(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lf/h/a/f/i/j/u4$d;

    iget-object p1, p1, Lf/h/a/f/i/j/u4$d;->zzc:Lf/h/a/f/i/j/n4;

    invoke-virtual {p1}, Lf/h/a/f/i/j/n4;->h()V

    return-void
.end method
