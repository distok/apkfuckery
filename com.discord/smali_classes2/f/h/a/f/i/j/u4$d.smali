.class public abstract Lf/h/a/f/i/j/u4$d;
.super Lf/h/a/f/i/j/u4;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/j/u4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lf/h/a/f/i/j/u4$d<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Ljava/lang/Object;",
        ">",
        "Lf/h/a/f/i/j/u4<",
        "TMessageType;TBuilderType;>;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# instance fields
.field public zzc:Lf/h/a/f/i/j/n4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/n4<",
            "Lf/h/a/f/i/j/u4$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/a/f/i/j/u4;-><init>()V

    sget-object v0, Lf/h/a/f/i/j/n4;->d:Lf/h/a/f/i/j/n4;

    iput-object v0, p0, Lf/h/a/f/i/j/u4$d;->zzc:Lf/h/a/f/i/j/n4;

    return-void
.end method


# virtual methods
.method public final u()Lf/h/a/f/i/j/n4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/h/a/f/i/j/n4<",
            "Lf/h/a/f/i/j/u4$c;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/u4$d;->zzc:Lf/h/a/f/i/j/n4;

    iget-boolean v1, v0, Lf/h/a/f/i/j/n4;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/i/j/n4;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/n4;

    iput-object v0, p0, Lf/h/a/f/i/j/u4$d;->zzc:Lf/h/a/f/i/j/n4;

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$d;->zzc:Lf/h/a/f/i/j/n4;

    return-object v0
.end method
