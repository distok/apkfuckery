.class public final Lf/h/a/f/i/j/g$b;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-sdk-api@@18.0.0"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/j/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final synthetic d:Lf/h/a/f/i/j/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/g;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    new-instance v1, Lf/h/a/f/i/j/e0;

    invoke-direct {v1, p0, p2, p1}, Lf/h/a/f/i/j/e0;-><init>(Lf/h/a/f/i/j/g$b;Landroid/os/Bundle;Landroid/app/Activity;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    new-instance v1, Lf/h/a/f/i/j/j0;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/i/j/j0;-><init>(Lf/h/a/f/i/j/g$b;Landroid/app/Activity;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    new-instance v1, Lf/h/a/f/i/j/f0;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/i/j/f0;-><init>(Lf/h/a/f/i/j/g$b;Landroid/app/Activity;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    new-instance v1, Lf/h/a/f/i/j/g0;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/i/j/g0;-><init>(Lf/h/a/f/i/j/g$b;Landroid/app/Activity;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    new-instance v0, Lf/h/a/f/i/j/cc;

    invoke-direct {v0}, Lf/h/a/f/i/j/cc;-><init>()V

    iget-object v1, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    new-instance v2, Lf/h/a/f/i/j/h0;

    invoke-direct {v2, p0, p1, v0}, Lf/h/a/f/i/j/h0;-><init>(Lf/h/a/f/i/j/g$b;Landroid/app/Activity;Lf/h/a/f/i/j/cc;)V

    iget-object p1, v1, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/i/j/cc;->o0(J)Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    new-instance v1, Lf/h/a/f/i/j/d0;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/i/j/d0;-><init>(Lf/h/a/f/i/j/g$b;Landroid/app/Activity;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    new-instance v1, Lf/h/a/f/i/j/i0;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/i/j/i0;-><init>(Lf/h/a/f/i/j/g$b;Landroid/app/Activity;)V

    iget-object p1, v0, Lf/h/a/f/i/j/g;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
