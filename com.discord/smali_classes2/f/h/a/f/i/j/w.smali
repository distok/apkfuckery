.class public final Lf/h/a/f/i/j/w;
.super Lf/h/a/f/i/j/g$a;
.source "com.google.android.gms:play-services-measurement-sdk-api@@18.0.0"


# instance fields
.field public final synthetic h:Ljava/lang/String;

.field public final synthetic i:Ljava/lang/String;

.field public final synthetic j:Z

.field public final synthetic k:Lf/h/a/f/i/j/cc;

.field public final synthetic l:Lf/h/a/f/i/j/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/g;Ljava/lang/String;Ljava/lang/String;ZLf/h/a/f/i/j/cc;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/j/w;->l:Lf/h/a/f/i/j/g;

    iput-object p2, p0, Lf/h/a/f/i/j/w;->h:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/f/i/j/w;->i:Ljava/lang/String;

    iput-boolean p4, p0, Lf/h/a/f/i/j/w;->j:Z

    iput-object p5, p0, Lf/h/a/f/i/j/w;->k:Lf/h/a/f/i/j/cc;

    const/4 p2, 0x1

    invoke-direct {p0, p1, p2}, Lf/h/a/f/i/j/g$a;-><init>(Lf/h/a/f/i/j/g;Z)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/w;->l:Lf/h/a/f/i/j/g;

    iget-object v0, v0, Lf/h/a/f/i/j/g;->h:Lf/h/a/f/i/j/ec;

    iget-object v1, p0, Lf/h/a/f/i/j/w;->h:Ljava/lang/String;

    iget-object v2, p0, Lf/h/a/f/i/j/w;->i:Ljava/lang/String;

    iget-boolean v3, p0, Lf/h/a/f/i/j/w;->j:Z

    iget-object v4, p0, Lf/h/a/f/i/j/w;->k:Lf/h/a/f/i/j/cc;

    invoke-interface {v0, v1, v2, v3, v4}, Lf/h/a/f/i/j/ec;->getUserProperties(Ljava/lang/String;Ljava/lang/String;ZLf/h/a/f/i/j/fc;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/w;->k:Lf/h/a/f/i/j/cc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/f/i/j/cc;->f(Landroid/os/Bundle;)V

    return-void
.end method
