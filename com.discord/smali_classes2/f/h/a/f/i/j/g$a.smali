.class public abstract Lf/h/a/f/i/j/g$a;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-sdk-api@@18.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/j/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "a"
.end annotation


# instance fields
.field public final d:J

.field public final e:J

.field public final f:Z

.field public final synthetic g:Lf/h/a/f/i/j/g;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/g;Z)V
    .locals 2

    iput-object p1, p0, Lf/h/a/f/i/j/g$a;->g:Lf/h/a/f/i/j/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lf/h/a/f/i/j/g;->b:Lf/h/a/f/f/n/c;

    check-cast v0, Lf/h/a/f/f/n/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/i/j/g$a;->d:J

    iget-object p1, p1, Lf/h/a/f/i/j/g;->b:Lf/h/a/f/f/n/c;

    check-cast p1, Lf/h/a/f/f/n/d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/f/i/j/g$a;->e:J

    iput-boolean p2, p0, Lf/h/a/f/i/j/g$a;->f:Z

    return-void
.end method


# virtual methods
.method public abstract a()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/i/j/g$a;->g:Lf/h/a/f/i/j/g;

    iget-boolean v0, v0, Lf/h/a/f/i/j/g;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/g$a;->b()V

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lf/h/a/f/i/j/g$a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/f/i/j/g$a;->g:Lf/h/a/f/i/j/g;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lf/h/a/f/i/j/g$a;->f:Z

    invoke-virtual {v1, v0, v2, v3}, Lf/h/a/f/i/j/g;->c(Ljava/lang/Exception;ZZ)V

    invoke-virtual {p0}, Lf/h/a/f/i/j/g$a;->b()V

    return-void
.end method
