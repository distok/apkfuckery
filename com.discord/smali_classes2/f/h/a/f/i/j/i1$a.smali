.class public final Lf/h/a/f/i/j/i1$a;
.super Lf/h/a/f/i/j/u4$b;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/j/i1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/u4$b<",
        "Lf/h/a/f/i/j/i1;",
        "Lf/h/a/f/i/j/i1$a;",
        ">;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lf/h/a/f/i/j/i1;->M()Lf/h/a/f/i/j/i1;

    move-result-object v0

    invoke-direct {p0, v0}, Lf/h/a/f/i/j/u4$b;-><init>(Lf/h/a/f/i/j/u4;)V

    return-void
.end method

.method public constructor <init>(Lf/h/a/f/i/j/k1;)V
    .locals 0

    invoke-static {}, Lf/h/a/f/i/j/i1;->M()Lf/h/a/f/i/j/i1;

    move-result-object p1

    invoke-direct {p0, p1}, Lf/h/a/f/i/j/u4$b;-><init>(Lf/h/a/f/i/j/u4;)V

    return-void
.end method


# virtual methods
.method public final p(J)Lf/h/a/f/i/j/i1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/i1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/i1;->w(Lf/h/a/f/i/j/i1;J)V

    return-object p0
.end method

.method public final q(Ljava/lang/String;)Lf/h/a/f/i/j/i1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/i1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/i1;->x(Lf/h/a/f/i/j/i1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final r(J)Lf/h/a/f/i/j/i1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/i1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/i1;->B(Lf/h/a/f/i/j/i1;J)V

    return-object p0
.end method
