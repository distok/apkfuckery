.class public final Lf/h/a/f/i/j/q1;
.super Lf/h/a/f/i/j/u4;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/i/j/q1$b;,
        Lf/h/a/f/i/j/q1$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/u4<",
        "Lf/h/a/f/i/j/q1;",
        "Lf/h/a/f/i/j/q1$b;",
        ">;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# static fields
.field private static final zzk:Lf/h/a/f/i/j/q1;

.field private static volatile zzl:Lf/h/a/f/i/j/j6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/j6<",
            "Lf/h/a/f/i/j/q1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private zzc:I

.field private zzd:I

.field private zze:Ljava/lang/String;

.field private zzf:Z

.field private zzg:J

.field private zzh:D

.field private zzi:Lf/h/a/f/i/j/b5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/b5<",
            "Lf/h/a/f/i/j/q1;",
            ">;"
        }
    .end annotation
.end field

.field private zzj:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/a/f/i/j/q1;

    invoke-direct {v0}, Lf/h/a/f/i/j/q1;-><init>()V

    sput-object v0, Lf/h/a/f/i/j/q1;->zzk:Lf/h/a/f/i/j/q1;

    const-class v1, Lf/h/a/f/i/j/q1;

    invoke-static {v1, v0}, Lf/h/a/f/i/j/u4;->r(Ljava/lang/Class;Lf/h/a/f/i/j/u4;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lf/h/a/f/i/j/u4;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lf/h/a/f/i/j/q1;->zze:Ljava/lang/String;

    sget-object v1, Lf/h/a/f/i/j/m6;->g:Lf/h/a/f/i/j/m6;

    iput-object v1, p0, Lf/h/a/f/i/j/q1;->zzi:Lf/h/a/f/i/j/b5;

    iput-object v0, p0, Lf/h/a/f/i/j/q1;->zzj:Ljava/lang/String;

    return-void
.end method

.method public static synthetic u()Lf/h/a/f/i/j/q1;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/q1;->zzk:Lf/h/a/f/i/j/q1;

    return-object v0
.end method


# virtual methods
.method public final p(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    const-class p2, Lf/h/a/f/i/j/q1;

    sget-object p3, Lf/h/a/f/i/j/r1;->a:[I

    const/4 v0, 0x1

    sub-int/2addr p1, v0

    aget p1, p3, p1

    const/4 p3, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p3

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lf/h/a/f/i/j/q1;->zzl:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_1

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/a/f/i/j/q1;->zzl:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/a/f/i/j/u4$a;

    sget-object p3, Lf/h/a/f/i/j/q1;->zzk:Lf/h/a/f/i/j/q1;

    invoke-direct {p1, p3}, Lf/h/a/f/i/j/u4$a;-><init>(Lf/h/a/f/i/j/u4;)V

    sput-object p1, Lf/h/a/f/i/j/q1;->zzl:Lf/h/a/f/i/j/j6;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lf/h/a/f/i/j/q1;->zzk:Lf/h/a/f/i/j/q1;

    return-object p1

    :pswitch_4
    const/16 p1, 0xa

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p3, 0x0

    const-string/jumbo v1, "zzc"

    aput-object v1, p1, p3

    const-string/jumbo p3, "zzd"

    aput-object p3, p1, v0

    const/4 p3, 0x2

    sget-object v0, Lf/h/a/f/i/j/s1;->a:Lf/h/a/f/i/j/a5;

    aput-object v0, p1, p3

    const/4 p3, 0x3

    const-string/jumbo v0, "zze"

    aput-object v0, p1, p3

    const/4 p3, 0x4

    const-string/jumbo v0, "zzf"

    aput-object v0, p1, p3

    const/4 p3, 0x5

    const-string/jumbo v0, "zzg"

    aput-object v0, p1, p3

    const/4 p3, 0x6

    const-string/jumbo v0, "zzh"

    aput-object v0, p1, p3

    const/4 p3, 0x7

    const-string/jumbo v0, "zzi"

    aput-object v0, p1, p3

    const/16 p3, 0x8

    aput-object p2, p1, p3

    const/16 p2, 0x9

    const-string/jumbo p3, "zzj"

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0001\u0000\u0001\u100c\u0000\u0002\u1008\u0001\u0003\u1007\u0002\u0004\u1002\u0003\u0005\u1000\u0004\u0006\u001b\u0007\u1008\u0005"

    sget-object p3, Lf/h/a/f/i/j/q1;->zzk:Lf/h/a/f/i/j/q1;

    new-instance v0, Lf/h/a/f/i/j/o6;

    invoke-direct {v0, p3, p2, p1}, Lf/h/a/f/i/j/o6;-><init>(Lf/h/a/f/i/j/c6;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    new-instance p1, Lf/h/a/f/i/j/q1$b;

    invoke-direct {p1, p3}, Lf/h/a/f/i/j/q1$b;-><init>(Lf/h/a/f/i/j/r1;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lf/h/a/f/i/j/q1;

    invoke-direct {p1}, Lf/h/a/f/i/j/q1;-><init>()V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
