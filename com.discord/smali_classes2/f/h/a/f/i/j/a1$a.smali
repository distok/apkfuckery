.class public final Lf/h/a/f/i/j/a1$a;
.super Lf/h/a/f/i/j/u4$b;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/j/a1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/u4$b<",
        "Lf/h/a/f/i/j/a1;",
        "Lf/h/a/f/i/j/a1$a;",
        ">;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lf/h/a/f/i/j/a1;->N()Lf/h/a/f/i/j/a1;

    move-result-object v0

    invoke-direct {p0, v0}, Lf/h/a/f/i/j/u4$b;-><init>(Lf/h/a/f/i/j/u4;)V

    return-void
.end method

.method public constructor <init>(Lf/h/a/f/i/j/k1;)V
    .locals 0

    invoke-static {}, Lf/h/a/f/i/j/a1;->N()Lf/h/a/f/i/j/a1;

    move-result-object p1

    invoke-direct {p0, p1}, Lf/h/a/f/i/j/u4$b;-><init>(Lf/h/a/f/i/j/u4;)V

    return-void
.end method


# virtual methods
.method public final A(I)Lf/h/a/f/i/j/a1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/a1;->x(Lf/h/a/f/i/j/a1;I)V

    return-object p0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/a1;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final C()J
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/a1;->H()J

    move-result-wide v0

    return-wide v0
.end method

.method public final D()J
    .locals 2

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/a1;->J()J

    move-result-wide v0

    return-wide v0
.end method

.method public final p(ILf/h/a/f/i/j/c1;)Lf/h/a/f/i/j/a1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/a1;->y(Lf/h/a/f/i/j/a1;ILf/h/a/f/i/j/c1;)V

    return-object p0
.end method

.method public final q(J)Lf/h/a/f/i/j/a1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/j/a1;->z(Lf/h/a/f/i/j/a1;J)V

    return-object p0
.end method

.method public final r(Lf/h/a/f/i/j/c1$a;)Lf/h/a/f/i/j/a1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-virtual {p1}, Lf/h/a/f/i/j/u4$b;->n()Lf/h/a/f/i/j/c6;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4;

    check-cast p1, Lf/h/a/f/i/j/c1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/a1;->A(Lf/h/a/f/i/j/a1;Lf/h/a/f/i/j/c1;)V

    return-object p0
.end method

.method public final s(Ljava/lang/String;)Lf/h/a/f/i/j/a1$a;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-static {v0, p1}, Lf/h/a/f/i/j/a1;->C(Lf/h/a/f/i/j/a1;Ljava/lang/String;)V

    return-object p0
.end method

.method public final u(I)Lf/h/a/f/i/j/c1;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/j/a1;->u(I)Lf/h/a/f/i/j/c1;

    move-result-object p1

    return-object p1
.end method

.method public final w()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/f/i/j/c1;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/a1;->v()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final x()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    check-cast v0, Lf/h/a/f/i/j/a1;

    invoke-virtual {v0}, Lf/h/a/f/i/j/a1;->D()I

    move-result v0

    return v0
.end method
