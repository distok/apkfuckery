.class public final Lf/h/a/f/i/j/qb;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/rb;


# static fields
.field public static final a:Lf/h/a/f/i/j/l2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/l2<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lf/h/a/f/i/j/q2;

    const-string v1, "com.google.android.gms.measurement"

    invoke-static {v1}, Lf/h/a/f/i/j/i2;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lf/h/a/f/i/j/q2;-><init>(Landroid/net/Uri;)V

    const-string v1, "measurement.integration.disable_firebase_instance_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/i/j/q2;->c(Ljava/lang/String;Z)Lf/h/a/f/i/j/l2;

    move-result-object v0

    sput-object v0, Lf/h/a/f/i/j/qb;->a:Lf/h/a/f/i/j/l2;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/qb;->a:Lf/h/a/f/i/j/l2;

    invoke-virtual {v0}, Lf/h/a/f/i/j/l2;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
