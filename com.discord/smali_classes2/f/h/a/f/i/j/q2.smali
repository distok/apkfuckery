.class public final Lf/h/a/f/i/j/q2;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"


# instance fields
.field public final a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/j/q2;->a:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)Lf/h/a/f/i/j/l2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lf/h/a/f/i/j/l2<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/i/j/l2;->g:Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/i/j/m2;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-direct {v0, p0, p1, p2}, Lf/h/a/f/i/j/m2;-><init>(Lf/h/a/f/i/j/q2;Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lf/h/a/f/i/j/l2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lf/h/a/f/i/j/l2<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/i/j/l2;->g:Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/i/j/r2;

    invoke-direct {v0, p0, p1, p2}, Lf/h/a/f/i/j/r2;-><init>(Lf/h/a/f/i/j/q2;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;Z)Lf/h/a/f/i/j/l2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lf/h/a/f/i/j/l2<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/i/j/l2;->g:Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/i/j/p2;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-direct {v0, p0, p1, p2}, Lf/h/a/f/i/j/p2;-><init>(Lf/h/a/f/i/j/q2;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method
