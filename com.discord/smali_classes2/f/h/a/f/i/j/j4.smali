.class public abstract Lf/h/a/f/i/j/j4;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lf/h/a/f/i/j/p4<",
        "TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/util/Map$Entry;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)I"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;)Lf/h/a/f/i/j/n4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/a/f/i/j/n4<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract c(Lf/h/a/f/i/j/h4;Lf/h/a/f/i/j/c6;I)Ljava/lang/Object;
.end method

.method public abstract d(Lf/h/a/f/i/j/v7;Ljava/util/Map$Entry;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/i/j/v7;",
            "Ljava/util/Map$Entry<",
            "**>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract e(Lf/h/a/f/i/j/c6;)Z
.end method

.method public abstract f(Ljava/lang/Object;)Lf/h/a/f/i/j/n4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/a/f/i/j/n4<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract g(Ljava/lang/Object;)V
.end method
