.class public Lf/h/a/f/i/j/h5;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"


# instance fields
.field public volatile a:Lf/h/a/f/i/j/c6;

.field public volatile b:Lf/h/a/f/i/j/t3;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lf/h/a/f/i/j/h4;->a()Lf/h/a/f/i/j/h4;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    invoke-virtual {v0}, Lf/h/a/f/i/j/t3;->d()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    invoke-interface {v0}, Lf/h/a/f/i/j/c6;->e()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lf/h/a/f/i/j/c6;)Lf/h/a/f/i/j/c6;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    if-eqz v0, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_0
    :try_start_1
    iput-object p1, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    sget-object v0, Lf/h/a/f/i/j/t3;->d:Lf/h/a/f/i/j/t3;

    iput-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;
    :try_end_1
    .catch Lcom/google/android/gms/internal/measurement/zzij; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    iput-object p1, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    sget-object p1, Lf/h/a/f/i/j/t3;->d:Lf/h/a/f/i/j/t3;

    iput-object p1, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    :goto_0
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :cond_1
    :goto_1
    iget-object p1, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    return-object p1
.end method

.method public final c()Lf/h/a/f/i/j/t3;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    return-object v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    monitor-exit p0

    return-object v0

    :cond_1
    iget-object v0, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    if-nez v0, :cond_2

    sget-object v0, Lf/h/a/f/i/j/t3;->d:Lf/h/a/f/i/j/t3;

    iput-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    invoke-interface {v0}, Lf/h/a/f/i/j/c6;->b()Lf/h/a/f/i/j/t3;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    :goto_0
    iget-object v0, p0, Lf/h/a/f/i/j/h5;->b:Lf/h/a/f/i/j/t3;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lf/h/a/f/i/j/h5;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    check-cast p1, Lf/h/a/f/i/j/h5;

    iget-object v0, p0, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    iget-object v1, p1, Lf/h/a/f/i/j/h5;->a:Lf/h/a/f/i/j/c6;

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/i/j/h5;->c()Lf/h/a/f/i/j/t3;

    move-result-object v0

    invoke-virtual {p1}, Lf/h/a/f/i/j/h5;->c()Lf/h/a/f/i/j/t3;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/a/f/i/j/t3;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lf/h/a/f/i/j/d6;->i()Lf/h/a/f/i/j/c6;

    move-result-object v1

    invoke-virtual {p1, v1}, Lf/h/a/f/i/j/h5;->b(Lf/h/a/f/i/j/c6;)Lf/h/a/f/i/j/c6;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_4
    invoke-interface {v1}, Lf/h/a/f/i/j/d6;->i()Lf/h/a/f/i/j/c6;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/h/a/f/i/j/h5;->b(Lf/h/a/f/i/j/c6;)Lf/h/a/f/i/j/c6;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
