.class public enum Lf/h/a/f/i/j/p7;
.super Ljava/lang/Enum;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/a/f/i/j/p7;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/a/f/i/j/p7;

.field public static final enum e:Lf/h/a/f/i/j/p7;

.field public static final enum f:Lf/h/a/f/i/j/p7;

.field public static final enum g:Lf/h/a/f/i/j/p7;

.field public static final enum h:Lf/h/a/f/i/j/p7;

.field public static final enum i:Lf/h/a/f/i/j/p7;

.field public static final enum j:Lf/h/a/f/i/j/p7;

.field public static final enum k:Lf/h/a/f/i/j/p7;

.field public static final enum l:Lf/h/a/f/i/j/p7;

.field public static final enum m:Lf/h/a/f/i/j/p7;

.field public static final enum n:Lf/h/a/f/i/j/p7;

.field public static final enum o:Lf/h/a/f/i/j/p7;

.field public static final enum p:Lf/h/a/f/i/j/p7;

.field public static final enum q:Lf/h/a/f/i/j/p7;

.field public static final enum r:Lf/h/a/f/i/j/p7;

.field public static final enum s:Lf/h/a/f/i/j/p7;

.field public static final enum t:Lf/h/a/f/i/j/p7;

.field public static final enum u:Lf/h/a/f/i/j/p7;

.field public static final synthetic v:[Lf/h/a/f/i/j/p7;


# instance fields
.field private final zzs:Lf/h/a/f/i/j/w7;

.field private final zzt:I


# direct methods
.method public static constructor <clinit>()V
    .locals 22

    new-instance v0, Lf/h/a/f/i/j/p7;

    sget-object v1, Lf/h/a/f/i/j/w7;->g:Lf/h/a/f/i/j/w7;

    const-string v2, "DOUBLE"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v0, v2, v3, v1, v4}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v0, Lf/h/a/f/i/j/p7;->d:Lf/h/a/f/i/j/p7;

    new-instance v1, Lf/h/a/f/i/j/p7;

    sget-object v2, Lf/h/a/f/i/j/w7;->f:Lf/h/a/f/i/j/w7;

    const-string v5, "FLOAT"

    const/4 v6, 0x5

    invoke-direct {v1, v5, v4, v2, v6}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v1, Lf/h/a/f/i/j/p7;->e:Lf/h/a/f/i/j/p7;

    new-instance v2, Lf/h/a/f/i/j/p7;

    sget-object v5, Lf/h/a/f/i/j/w7;->e:Lf/h/a/f/i/j/w7;

    const-string v7, "INT64"

    const/4 v8, 0x2

    invoke-direct {v2, v7, v8, v5, v3}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v2, Lf/h/a/f/i/j/p7;->f:Lf/h/a/f/i/j/p7;

    new-instance v7, Lf/h/a/f/i/j/p7;

    const-string v9, "UINT64"

    const/4 v10, 0x3

    invoke-direct {v7, v9, v10, v5, v3}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v7, Lf/h/a/f/i/j/p7;->g:Lf/h/a/f/i/j/p7;

    new-instance v9, Lf/h/a/f/i/j/p7;

    sget-object v11, Lf/h/a/f/i/j/w7;->d:Lf/h/a/f/i/j/w7;

    const-string v12, "INT32"

    const/4 v13, 0x4

    invoke-direct {v9, v12, v13, v11, v3}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v9, Lf/h/a/f/i/j/p7;->h:Lf/h/a/f/i/j/p7;

    new-instance v12, Lf/h/a/f/i/j/p7;

    const-string v14, "FIXED64"

    invoke-direct {v12, v14, v6, v5, v4}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v12, Lf/h/a/f/i/j/p7;->i:Lf/h/a/f/i/j/p7;

    new-instance v14, Lf/h/a/f/i/j/p7;

    const-string v15, "FIXED32"

    const/4 v13, 0x6

    invoke-direct {v14, v15, v13, v11, v6}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v14, Lf/h/a/f/i/j/p7;->j:Lf/h/a/f/i/j/p7;

    new-instance v15, Lf/h/a/f/i/j/p7;

    sget-object v13, Lf/h/a/f/i/j/w7;->h:Lf/h/a/f/i/j/w7;

    const-string v10, "BOOL"

    const/4 v8, 0x7

    invoke-direct {v15, v10, v8, v13, v3}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v15, Lf/h/a/f/i/j/p7;->k:Lf/h/a/f/i/j/p7;

    new-instance v10, Lf/h/a/f/i/j/s7;

    sget-object v13, Lf/h/a/f/i/j/w7;->i:Lf/h/a/f/i/j/w7;

    const-string v8, "STRING"

    invoke-direct {v10, v8, v13}, Lf/h/a/f/i/j/s7;-><init>(Ljava/lang/String;Lf/h/a/f/i/j/w7;)V

    sput-object v10, Lf/h/a/f/i/j/p7;->l:Lf/h/a/f/i/j/p7;

    new-instance v8, Lf/h/a/f/i/j/r7;

    sget-object v13, Lf/h/a/f/i/j/w7;->l:Lf/h/a/f/i/j/w7;

    const-string v4, "GROUP"

    invoke-direct {v8, v4, v13}, Lf/h/a/f/i/j/r7;-><init>(Ljava/lang/String;Lf/h/a/f/i/j/w7;)V

    sput-object v8, Lf/h/a/f/i/j/p7;->m:Lf/h/a/f/i/j/p7;

    new-instance v4, Lf/h/a/f/i/j/u7;

    const-string v6, "MESSAGE"

    invoke-direct {v4, v6, v13}, Lf/h/a/f/i/j/u7;-><init>(Ljava/lang/String;Lf/h/a/f/i/j/w7;)V

    sput-object v4, Lf/h/a/f/i/j/p7;->n:Lf/h/a/f/i/j/p7;

    new-instance v6, Lf/h/a/f/i/j/t7;

    sget-object v13, Lf/h/a/f/i/j/w7;->j:Lf/h/a/f/i/j/w7;

    const-string v3, "BYTES"

    invoke-direct {v6, v3, v13}, Lf/h/a/f/i/j/t7;-><init>(Ljava/lang/String;Lf/h/a/f/i/j/w7;)V

    sput-object v6, Lf/h/a/f/i/j/p7;->o:Lf/h/a/f/i/j/p7;

    new-instance v3, Lf/h/a/f/i/j/p7;

    const-string v13, "UINT32"

    move-object/from16 v16, v6

    const/16 v6, 0xc

    move-object/from16 v17, v4

    const/4 v4, 0x0

    invoke-direct {v3, v13, v6, v11, v4}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v3, Lf/h/a/f/i/j/p7;->p:Lf/h/a/f/i/j/p7;

    new-instance v13, Lf/h/a/f/i/j/p7;

    sget-object v6, Lf/h/a/f/i/j/w7;->k:Lf/h/a/f/i/j/w7;

    move-object/from16 v18, v3

    const-string v3, "ENUM"

    move-object/from16 v19, v8

    const/16 v8, 0xd

    invoke-direct {v13, v3, v8, v6, v4}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v13, Lf/h/a/f/i/j/p7;->q:Lf/h/a/f/i/j/p7;

    new-instance v3, Lf/h/a/f/i/j/p7;

    const-string v4, "SFIXED32"

    const/16 v6, 0xe

    const/4 v8, 0x5

    invoke-direct {v3, v4, v6, v11, v8}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v3, Lf/h/a/f/i/j/p7;->r:Lf/h/a/f/i/j/p7;

    new-instance v4, Lf/h/a/f/i/j/p7;

    const-string v8, "SFIXED64"

    const/16 v6, 0xf

    move-object/from16 v20, v3

    const/4 v3, 0x1

    invoke-direct {v4, v8, v6, v5, v3}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v4, Lf/h/a/f/i/j/p7;->s:Lf/h/a/f/i/j/p7;

    new-instance v3, Lf/h/a/f/i/j/p7;

    const-string v8, "SINT32"

    const/16 v6, 0x10

    move-object/from16 v21, v4

    const/4 v4, 0x0

    invoke-direct {v3, v8, v6, v11, v4}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v3, Lf/h/a/f/i/j/p7;->t:Lf/h/a/f/i/j/p7;

    new-instance v8, Lf/h/a/f/i/j/p7;

    const-string v11, "SINT64"

    const/16 v6, 0x11

    invoke-direct {v8, v11, v6, v5, v4}, Lf/h/a/f/i/j/p7;-><init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V

    sput-object v8, Lf/h/a/f/i/j/p7;->u:Lf/h/a/f/i/j/p7;

    const/16 v5, 0x12

    new-array v5, v5, [Lf/h/a/f/i/j/p7;

    aput-object v0, v5, v4

    const/4 v0, 0x1

    aput-object v1, v5, v0

    const/4 v0, 0x2

    aput-object v2, v5, v0

    const/4 v0, 0x3

    aput-object v7, v5, v0

    const/4 v0, 0x4

    aput-object v9, v5, v0

    const/4 v0, 0x5

    aput-object v12, v5, v0

    const/4 v0, 0x6

    aput-object v14, v5, v0

    const/4 v0, 0x7

    aput-object v15, v5, v0

    const/16 v0, 0x8

    aput-object v10, v5, v0

    const/16 v0, 0x9

    aput-object v19, v5, v0

    const/16 v0, 0xa

    aput-object v17, v5, v0

    const/16 v0, 0xb

    aput-object v16, v5, v0

    const/16 v0, 0xc

    aput-object v18, v5, v0

    const/16 v0, 0xd

    aput-object v13, v5, v0

    const/16 v0, 0xe

    aput-object v20, v5, v0

    const/16 v0, 0xf

    aput-object v21, v5, v0

    const/16 v0, 0x10

    aput-object v3, v5, v0

    aput-object v8, v5, v6

    sput-object v5, Lf/h/a/f/i/j/p7;->v:[Lf/h/a/f/i/j/p7;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/i/j/w7;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lf/h/a/f/i/j/p7;->zzs:Lf/h/a/f/i/j/w7;

    iput p4, p0, Lf/h/a/f/i/j/p7;->zzt:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILf/h/a/f/i/j/w7;ILf/h/a/f/i/j/q7;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lf/h/a/f/i/j/p7;->zzs:Lf/h/a/f/i/j/w7;

    iput p4, p0, Lf/h/a/f/i/j/p7;->zzt:I

    return-void
.end method

.method public static values()[Lf/h/a/f/i/j/p7;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/p7;->v:[Lf/h/a/f/i/j/p7;

    invoke-virtual {v0}, [Lf/h/a/f/i/j/p7;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/a/f/i/j/p7;

    return-object v0
.end method


# virtual methods
.method public final f()Lf/h/a/f/i/j/w7;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/p7;->zzs:Lf/h/a/f/i/j/w7;

    return-object v0
.end method
