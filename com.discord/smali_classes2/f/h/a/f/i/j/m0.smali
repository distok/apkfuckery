.class public final Lf/h/a/f/i/j/m0;
.super Lf/h/a/f/i/j/u4;
.source "com.google.android.gms:play-services-measurement@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/d6;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/f/i/j/m0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/j/u4<",
        "Lf/h/a/f/i/j/m0;",
        "Lf/h/a/f/i/j/m0$a;",
        ">;",
        "Lf/h/a/f/i/j/d6;"
    }
.end annotation


# static fields
.field private static final zzh:Lf/h/a/f/i/j/m0;

.field private static volatile zzi:Lf/h/a/f/i/j/j6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/j6<",
            "Lf/h/a/f/i/j/m0;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private zzc:I

.field private zzd:Lf/h/a/f/i/j/p0;

.field private zze:Lf/h/a/f/i/j/n0;

.field private zzf:Z

.field private zzg:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/a/f/i/j/m0;

    invoke-direct {v0}, Lf/h/a/f/i/j/m0;-><init>()V

    sput-object v0, Lf/h/a/f/i/j/m0;->zzh:Lf/h/a/f/i/j/m0;

    const-class v1, Lf/h/a/f/i/j/m0;

    invoke-static {v1, v0}, Lf/h/a/f/i/j/u4;->r(Ljava/lang/Class;Lf/h/a/f/i/j/u4;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/a/f/i/j/u4;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lf/h/a/f/i/j/m0;->zzg:Ljava/lang/String;

    return-void
.end method

.method public static D()Lf/h/a/f/i/j/m0;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/m0;->zzh:Lf/h/a/f/i/j/m0;

    return-object v0
.end method

.method public static synthetic E()Lf/h/a/f/i/j/m0;
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/m0;->zzh:Lf/h/a/f/i/j/m0;

    return-object v0
.end method

.method public static u(Lf/h/a/f/i/j/m0;Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lf/h/a/f/i/j/m0;->zzc:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lf/h/a/f/i/j/m0;->zzc:I

    iput-object p1, p0, Lf/h/a/f/i/j/m0;->zzg:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/i/j/m0;->zzf:Z

    return v0
.end method

.method public final B()Z
    .locals 1

    iget v0, p0, Lf/h/a/f/i/j/m0;->zzc:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final C()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/m0;->zzg:Ljava/lang/String;

    return-object v0
.end method

.method public final p(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    sget-object p2, Lf/h/a/f/i/j/q0;->a:[I

    const/4 p3, 0x1

    sub-int/2addr p1, p3

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    invoke-static {p3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lf/h/a/f/i/j/m0;->zzi:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_1

    const-class p2, Lf/h/a/f/i/j/m0;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lf/h/a/f/i/j/m0;->zzi:Lf/h/a/f/i/j/j6;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/a/f/i/j/u4$a;

    sget-object p3, Lf/h/a/f/i/j/m0;->zzh:Lf/h/a/f/i/j/m0;

    invoke-direct {p1, p3}, Lf/h/a/f/i/j/u4$a;-><init>(Lf/h/a/f/i/j/u4;)V

    sput-object p1, Lf/h/a/f/i/j/m0;->zzi:Lf/h/a/f/i/j/j6;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lf/h/a/f/i/j/m0;->zzh:Lf/h/a/f/i/j/m0;

    return-object p1

    :pswitch_4
    const/4 p1, 0x5

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string/jumbo v0, "zzc"

    aput-object v0, p1, p2

    const-string/jumbo p2, "zzd"

    aput-object p2, p1, p3

    const/4 p2, 0x2

    const-string/jumbo p3, "zze"

    aput-object p3, p1, p2

    const/4 p2, 0x3

    const-string/jumbo p3, "zzf"

    aput-object p3, p1, p2

    const/4 p2, 0x4

    const-string/jumbo p3, "zzg"

    aput-object p3, p1, p2

    const-string p2, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1009\u0000\u0002\u1009\u0001\u0003\u1007\u0002\u0004\u1008\u0003"

    sget-object p3, Lf/h/a/f/i/j/m0;->zzh:Lf/h/a/f/i/j/m0;

    new-instance v0, Lf/h/a/f/i/j/o6;

    invoke-direct {v0, p3, p2, p1}, Lf/h/a/f/i/j/o6;-><init>(Lf/h/a/f/i/j/c6;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    :pswitch_5
    new-instance p1, Lf/h/a/f/i/j/m0$a;

    invoke-direct {p1, p2}, Lf/h/a/f/i/j/m0$a;-><init>(Lf/h/a/f/i/j/q0;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lf/h/a/f/i/j/m0;

    invoke-direct {p1}, Lf/h/a/f/i/j/m0;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final v()Z
    .locals 2

    iget v0, p0, Lf/h/a/f/i/j/m0;->zzc:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final w()Lf/h/a/f/i/j/p0;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/m0;->zzd:Lf/h/a/f/i/j/p0;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/a/f/i/j/p0;->C()Lf/h/a/f/i/j/p0;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final x()Z
    .locals 1

    iget v0, p0, Lf/h/a/f/i/j/m0;->zzc:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final y()Lf/h/a/f/i/j/n0;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/m0;->zze:Lf/h/a/f/i/j/n0;

    if-nez v0, :cond_0

    invoke-static {}, Lf/h/a/f/i/j/n0;->E()Lf/h/a/f/i/j/n0;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final z()Z
    .locals 1

    iget v0, p0, Lf/h/a/f/i/j/m0;->zzc:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
