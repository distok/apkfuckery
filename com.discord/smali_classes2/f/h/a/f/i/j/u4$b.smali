.class public Lf/h/a/f/i/j/u4$b;
.super Lf/h/a/f/i/j/n3;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/j/u4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lf/h/a/f/i/j/u4<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Lf/h/a/f/i/j/u4$b<",
        "TMessageType;TBuilderType;>;>",
        "Lf/h/a/f/i/j/n3<",
        "TMessageType;TBuilderType;>;"
    }
.end annotation


# instance fields
.field public final d:Lf/h/a/f/i/j/u4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field public e:Lf/h/a/f/i/j/u4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/u4;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/h/a/f/i/j/n3;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/j/u4$b;->d:Lf/h/a/f/i/j/u4;

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v1}, Lf/h/a/f/i/j/u4;->p(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/u4;

    iput-object p1, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    return-void
.end method


# virtual methods
.method public synthetic clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->d:Lf/h/a/f/i/j/u4;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lf/h/a/f/i/j/u4;->p(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/u4$b;

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->m()Lf/h/a/f/i/j/c6;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/j/u4;

    invoke-virtual {v0, v1}, Lf/h/a/f/i/j/u4$b;->j(Lf/h/a/f/i/j/u4;)Lf/h/a/f/i/j/u4$b;

    return-object v0
.end method

.method public final synthetic i()Lf/h/a/f/i/j/c6;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->d:Lf/h/a/f/i/j/u4;

    return-object v0
.end method

.method public final j(Lf/h/a/f/i/j/u4;)Lf/h/a/f/i/j/u4$b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TBuilderType;"
        }
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    sget-object v1, Lf/h/a/f/i/j/n6;->c:Lf/h/a/f/i/j/n6;

    invoke-virtual {v1, v0}, Lf/h/a/f/i/j/n6;->b(Ljava/lang/Object;)Lf/h/a/f/i/j/q6;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lf/h/a/f/i/j/q6;->g(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p0
.end method

.method public final k([BILf/h/a/f/i/j/h4;)Lf/h/a/f/i/j/u4$b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Lf/h/a/f/i/j/h4;",
            ")TBuilderType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/measurement/zzij;
        }
    .end annotation

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    :cond_0
    :try_start_0
    sget-object v0, Lf/h/a/f/i/j/n6;->c:Lf/h/a/f/i/j/n6;

    iget-object v1, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    invoke-virtual {v0, v1}, Lf/h/a/f/i/j/n6;->b(Ljava/lang/Object;)Lf/h/a/f/i/j/q6;

    move-result-object v2

    iget-object v3, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    const/4 v5, 0x0

    new-instance v7, Lf/h/a/f/i/j/s3;

    invoke-direct {v7, p3}, Lf/h/a/f/i/j/s3;-><init>(Lf/h/a/f/i/j/h4;)V

    move-object v4, p1

    move v6, p2

    invoke-interface/range {v2 .. v7}, Lf/h/a/f/i/j/q6;->i(Ljava/lang/Object;[BIILf/h/a/f/i/j/s3;)V
    :try_end_0
    .catch Lcom/google/android/gms/internal/measurement/zzij; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_1

    :goto_0
    new-instance p2, Ljava/lang/RuntimeException;

    const-string p3, "Reading from byte array should not throw IOException."

    invoke-direct {p2, p3, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_2
    invoke-static {}, Lcom/google/android/gms/internal/measurement/zzij;->a()Lcom/google/android/gms/internal/measurement/zzij;

    move-result-object p1

    throw p1

    :goto_1
    throw p1
.end method

.method public l()V
    .locals 3

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lf/h/a/f/i/j/u4;->p(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/u4;

    iget-object v1, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    sget-object v2, Lf/h/a/f/i/j/n6;->c:Lf/h/a/f/i/j/n6;

    invoke-virtual {v2, v0}, Lf/h/a/f/i/j/n6;->b(Ljava/lang/Object;)Lf/h/a/f/i/j/q6;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lf/h/a/f/i/j/q6;->g(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    return-void
.end method

.method public m()Lf/h/a/f/i/j/c6;
    .locals 2

    iget-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    sget-object v1, Lf/h/a/f/i/j/n6;->c:Lf/h/a/f/i/j/n6;

    invoke-virtual {v1, v0}, Lf/h/a/f/i/j/n6;->b(Ljava/lang/Object;)Lf/h/a/f/i/j/q6;

    move-result-object v1

    invoke-interface {v1, v0}, Lf/h/a/f/i/j/q6;->b(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/f/i/j/u4$b;->f:Z

    iget-object v0, p0, Lf/h/a/f/i/j/u4$b;->e:Lf/h/a/f/i/j/u4;

    :goto_0
    return-object v0
.end method

.method public n()Lf/h/a/f/i/j/c6;
    .locals 2

    invoke-virtual {p0}, Lf/h/a/f/i/j/u4$b;->m()Lf/h/a/f/i/j/c6;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/u4;

    invoke-virtual {v0}, Lf/h/a/f/i/j/u4;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/measurement/zzkq;

    invoke-direct {v0}, Lcom/google/android/gms/internal/measurement/zzkq;-><init>()V

    throw v0
.end method
