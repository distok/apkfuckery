.class public final Lf/h/a/f/i/j/n6;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-base@@18.0.0"


# static fields
.field public static final c:Lf/h/a/f/i/j/n6;


# instance fields
.field public final a:Lf/h/a/f/i/j/p6;

.field public final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/Class<",
            "*>;",
            "Lf/h/a/f/i/j/q6<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/f/i/j/n6;

    invoke-direct {v0}, Lf/h/a/f/i/j/n6;-><init>()V

    sput-object v0, Lf/h/a/f/i/j/n6;->c:Lf/h/a/f/i/j/n6;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lf/h/a/f/i/j/n6;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, Lf/h/a/f/i/j/p5;

    invoke-direct {v0}, Lf/h/a/f/i/j/p5;-><init>()V

    iput-object v0, p0, Lf/h/a/f/i/j/n6;->a:Lf/h/a/f/i/j/p6;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Lf/h/a/f/i/j/q6;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lf/h/a/f/i/j/q6<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lf/h/a/f/i/j/w4;->a:Ljava/nio/charset/Charset;

    const-string v0, "messageType"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/f/i/j/n6;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/q6;

    if-nez v0, :cond_b

    iget-object v0, p0, Lf/h/a/f/i/j/n6;->a:Lf/h/a/f/i/j/p6;

    check-cast v0, Lf/h/a/f/i/j/p5;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v1, Lf/h/a/f/i/j/u4;

    sget-object v2, Lf/h/a/f/i/j/s6;->a:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lf/h/a/f/i/j/s6;->a:Ljava/lang/Class;

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Message classes must extend GeneratedMessage or GeneratedMessageLite"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, v0, Lf/h/a/f/i/j/p5;->a:Lf/h/a/f/i/j/z5;

    invoke-interface {v0, p1}, Lf/h/a/f/i/j/z5;->b(Ljava/lang/Class;)Lf/h/a/f/i/j/a6;

    move-result-object v2

    invoke-interface {v2}, Lf/h/a/f/i/j/a6;->b()Z

    move-result v0

    const-string v3, "Protobuf runtime is not correctly loaded."

    if-eqz v0, :cond_4

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lf/h/a/f/i/j/s6;->d:Lf/h/a/f/i/j/d7;

    sget-object v1, Lf/h/a/f/i/j/k4;->a:Lf/h/a/f/i/j/j4;

    sget-object v1, Lf/h/a/f/i/j/k4;->a:Lf/h/a/f/i/j/j4;

    invoke-interface {v2}, Lf/h/a/f/i/j/a6;->c()Lf/h/a/f/i/j/c6;

    move-result-object v2

    new-instance v3, Lf/h/a/f/i/j/g6;

    invoke-direct {v3, v0, v1, v2}, Lf/h/a/f/i/j/g6;-><init>(Lf/h/a/f/i/j/d7;Lf/h/a/f/i/j/j4;Lf/h/a/f/i/j/c6;)V

    :goto_1
    move-object v0, v3

    goto/16 :goto_2

    :cond_2
    sget-object v0, Lf/h/a/f/i/j/s6;->b:Lf/h/a/f/i/j/d7;

    sget-object v1, Lf/h/a/f/i/j/k4;->b:Lf/h/a/f/i/j/j4;

    if-eqz v1, :cond_3

    invoke-interface {v2}, Lf/h/a/f/i/j/a6;->c()Lf/h/a/f/i/j/c6;

    move-result-object v2

    new-instance v3, Lf/h/a/f/i/j/g6;

    invoke-direct {v3, v0, v1, v2}, Lf/h/a/f/i/j/g6;-><init>(Lf/h/a/f/i/j/d7;Lf/h/a/f/i/j/j4;Lf/h/a/f/i/j/c6;)V

    goto :goto_1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_7

    invoke-interface {v2}, Lf/h/a/f/i/j/a6;->a()I

    move-result v0

    if-ne v0, v4, :cond_5

    const/4 v1, 0x1

    :cond_5
    if-eqz v1, :cond_6

    sget-object v3, Lf/h/a/f/i/j/k6;->b:Lf/h/a/f/i/j/i6;

    sget-object v4, Lf/h/a/f/i/j/m5;->b:Lf/h/a/f/i/j/m5;

    sget-object v5, Lf/h/a/f/i/j/s6;->d:Lf/h/a/f/i/j/d7;

    sget-object v0, Lf/h/a/f/i/j/k4;->a:Lf/h/a/f/i/j/j4;

    sget-object v6, Lf/h/a/f/i/j/k4;->a:Lf/h/a/f/i/j/j4;

    sget-object v7, Lf/h/a/f/i/j/x5;->b:Lf/h/a/f/i/j/v5;

    invoke-static/range {v2 .. v7}, Lf/h/a/f/i/j/e6;->n(Lf/h/a/f/i/j/a6;Lf/h/a/f/i/j/i6;Lf/h/a/f/i/j/m5;Lf/h/a/f/i/j/d7;Lf/h/a/f/i/j/j4;Lf/h/a/f/i/j/v5;)Lf/h/a/f/i/j/e6;

    move-result-object v0

    goto :goto_2

    :cond_6
    sget-object v3, Lf/h/a/f/i/j/k6;->b:Lf/h/a/f/i/j/i6;

    sget-object v4, Lf/h/a/f/i/j/m5;->b:Lf/h/a/f/i/j/m5;

    sget-object v5, Lf/h/a/f/i/j/s6;->d:Lf/h/a/f/i/j/d7;

    const/4 v6, 0x0

    sget-object v7, Lf/h/a/f/i/j/x5;->b:Lf/h/a/f/i/j/v5;

    invoke-static/range {v2 .. v7}, Lf/h/a/f/i/j/e6;->n(Lf/h/a/f/i/j/a6;Lf/h/a/f/i/j/i6;Lf/h/a/f/i/j/m5;Lf/h/a/f/i/j/d7;Lf/h/a/f/i/j/j4;Lf/h/a/f/i/j/v5;)Lf/h/a/f/i/j/e6;

    move-result-object v0

    goto :goto_2

    :cond_7
    invoke-interface {v2}, Lf/h/a/f/i/j/a6;->a()I

    move-result v0

    if-ne v0, v4, :cond_8

    const/4 v1, 0x1

    :cond_8
    if-eqz v1, :cond_a

    sget-object v0, Lf/h/a/f/i/j/k6;->a:Lf/h/a/f/i/j/i6;

    sget-object v4, Lf/h/a/f/i/j/m5;->a:Lf/h/a/f/i/j/m5;

    sget-object v5, Lf/h/a/f/i/j/s6;->b:Lf/h/a/f/i/j/d7;

    sget-object v6, Lf/h/a/f/i/j/k4;->b:Lf/h/a/f/i/j/j4;

    if-eqz v6, :cond_9

    sget-object v7, Lf/h/a/f/i/j/x5;->a:Lf/h/a/f/i/j/v5;

    move-object v3, v0

    invoke-static/range {v2 .. v7}, Lf/h/a/f/i/j/e6;->n(Lf/h/a/f/i/j/a6;Lf/h/a/f/i/j/i6;Lf/h/a/f/i/j/m5;Lf/h/a/f/i/j/d7;Lf/h/a/f/i/j/j4;Lf/h/a/f/i/j/v5;)Lf/h/a/f/i/j/e6;

    move-result-object v0

    goto :goto_2

    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_a
    sget-object v3, Lf/h/a/f/i/j/k6;->a:Lf/h/a/f/i/j/i6;

    sget-object v4, Lf/h/a/f/i/j/m5;->a:Lf/h/a/f/i/j/m5;

    sget-object v5, Lf/h/a/f/i/j/s6;->c:Lf/h/a/f/i/j/d7;

    const/4 v6, 0x0

    sget-object v7, Lf/h/a/f/i/j/x5;->a:Lf/h/a/f/i/j/v5;

    invoke-static/range {v2 .. v7}, Lf/h/a/f/i/j/e6;->n(Lf/h/a/f/i/j/a6;Lf/h/a/f/i/j/i6;Lf/h/a/f/i/j/m5;Lf/h/a/f/i/j/d7;Lf/h/a/f/i/j/j4;Lf/h/a/f/i/j/v5;)Lf/h/a/f/i/j/e6;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lf/h/a/f/i/j/n6;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/j/q6;

    if-eqz p1, :cond_b

    move-object v0, p1

    :cond_b
    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Lf/h/a/f/i/j/q6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lf/h/a/f/i/j/q6<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/h/a/f/i/j/n6;->a(Ljava/lang/Class;)Lf/h/a/f/i/j/q6;

    move-result-object p1

    return-object p1
.end method
