.class public final Lf/h/a/f/i/j/e0;
.super Lf/h/a/f/i/j/g$a;
.source "com.google.android.gms:play-services-measurement-sdk-api@@18.0.0"


# instance fields
.field public final synthetic h:Landroid/os/Bundle;

.field public final synthetic i:Landroid/app/Activity;

.field public final synthetic j:Lf/h/a/f/i/j/g$b;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/j/g$b;Landroid/os/Bundle;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/f/i/j/e0;->j:Lf/h/a/f/i/j/g$b;

    iput-object p2, p0, Lf/h/a/f/i/j/e0;->h:Landroid/os/Bundle;

    iput-object p3, p0, Lf/h/a/f/i/j/e0;->i:Landroid/app/Activity;

    iget-object p1, p1, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    const/4 p2, 0x1

    invoke-direct {p0, p1, p2}, Lf/h/a/f/i/j/g$a;-><init>(Lf/h/a/f/i/j/g;Z)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/j/e0;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lf/h/a/f/i/j/e0;->h:Landroid/os/Bundle;

    const-string v2, "com.google.app_measurement.screen_service"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/a/f/i/j/e0;->h:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Landroid/os/Bundle;

    if-eqz v3, :cond_1

    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    iget-object v1, p0, Lf/h/a/f/i/j/e0;->j:Lf/h/a/f/i/j/g$b;

    iget-object v1, v1, Lf/h/a/f/i/j/g$b;->d:Lf/h/a/f/i/j/g;

    iget-object v1, v1, Lf/h/a/f/i/j/g;->h:Lf/h/a/f/i/j/ec;

    iget-object v2, p0, Lf/h/a/f/i/j/e0;->i:Landroid/app/Activity;

    new-instance v3, Lf/h/a/f/g/b;

    invoke-direct {v3, v2}, Lf/h/a/f/g/b;-><init>(Ljava/lang/Object;)V

    iget-wide v4, p0, Lf/h/a/f/i/j/g$a;->e:J

    invoke-interface {v1, v3, v0, v4, v5}, Lf/h/a/f/i/j/ec;->onActivityCreated(Lf/h/a/f/g/a;Landroid/os/Bundle;J)V

    return-void
.end method
