.class public final Lf/h/a/f/i/j/hb;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-impl@@18.0.0"

# interfaces
.implements Lf/h/a/f/i/j/z2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/i/j/z2<",
        "Lf/h/a/f/i/j/gb;",
        ">;"
    }
.end annotation


# static fields
.field public static e:Lf/h/a/f/i/j/hb;


# instance fields
.field public final d:Lf/h/a/f/i/j/z2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/j/z2<",
            "Lf/h/a/f/i/j/gb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/f/i/j/hb;

    invoke-direct {v0}, Lf/h/a/f/i/j/hb;-><init>()V

    sput-object v0, Lf/h/a/f/i/j/hb;->e:Lf/h/a/f/i/j/hb;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    new-instance v0, Lf/h/a/f/i/j/jb;

    invoke-direct {v0}, Lf/h/a/f/i/j/jb;-><init>()V

    new-instance v1, Lf/h/a/f/i/j/b3;

    invoke-direct {v1, v0}, Lf/h/a/f/i/j/b3;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {v1}, Lf/h/a/f/f/n/g;->D0(Lf/h/a/f/i/j/z2;)Lf/h/a/f/i/j/z2;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/f/i/j/hb;->d:Lf/h/a/f/i/j/z2;

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-object v0, Lf/h/a/f/i/j/hb;->e:Lf/h/a/f/i/j/hb;

    invoke-virtual {v0}, Lf/h/a/f/i/j/hb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/gb;

    invoke-interface {v0}, Lf/h/a/f/i/j/gb;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/j/hb;->d:Lf/h/a/f/i/j/z2;

    invoke-interface {v0}, Lf/h/a/f/i/j/z2;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/j/gb;

    return-object v0
.end method
