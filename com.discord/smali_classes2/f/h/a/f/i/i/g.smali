.class public final Lf/h/a/f/i/i/g;
.super Lf/h/a/f/f/k/d;
.source "com.google.firebase:firebase-appindexing@@19.1.0"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/k/d<",
        "Lf/h/a/f/i/i/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/k/c;Lf/h/a/f/f/h/i/f;Lf/h/a/f/f/h/i/l;)V
    .locals 7

    const/16 v3, 0x13

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lf/h/a/f/f/k/d;-><init>(Landroid/content/Context;Landroid/os/Looper;ILf/h/a/f/f/k/c;Lf/h/a/f/f/h/i/f;Lf/h/a/f/f/h/i/l;)V

    return-void
.end method


# virtual methods
.method public final l()I
    .locals 1

    const v0, 0xc042c0

    return v0
.end method

.method public final synthetic r(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, "com.google.android.gms.appdatasearch.internal.ILightweightAppDataSearch"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lf/h/a/f/i/i/b;

    if-eqz v1, :cond_1

    check-cast v0, Lf/h/a/f/i/i/b;

    return-object v0

    :cond_1
    new-instance v0, Lf/h/a/f/i/i/d;

    invoke-direct {v0, p1}, Lf/h/a/f/i/i/d;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.appdatasearch.internal.ILightweightAppDataSearch"

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.icing.LIGHTWEIGHT_INDEX_SERVICE"

    return-object v0
.end method
