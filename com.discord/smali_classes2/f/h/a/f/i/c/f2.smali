.class public final Lf/h/a/f/i/c/f2;
.super Ljava/lang/Object;

# interfaces
.implements Lf/h/a/f/i/c/r2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/h/a/f/i/c/r2<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/f/i/c/b2;

.field public final b:Lf/h/a/f/i/c/e3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/c/e3<",
            "**>;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Lf/h/a/f/i/c/l0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/c/l0<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/f/i/c/e3;Lf/h/a/f/i/c/l0;Lf/h/a/f/i/c/b2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/i/c/e3<",
            "**>;",
            "Lf/h/a/f/i/c/l0<",
            "*>;",
            "Lf/h/a/f/i/c/b2;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    invoke-virtual {p2, p3}, Lf/h/a/f/i/c/l0;->f(Lf/h/a/f/i/c/b2;)Z

    move-result p1

    iput-boolean p1, p0, Lf/h/a/f/i/c/f2;->c:Z

    iput-object p2, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    iput-object p3, p0, Lf/h/a/f/i/c/f2;->a:Lf/h/a/f/i/c/b2;

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/e3;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/l0;->e(Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/e3;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    invoke-virtual {v1, p2}, Lf/h/a/f/i/c/e3;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-boolean v0, p0, Lf/h/a/f/i/c/f2;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/l0;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;

    move-result-object p1

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-virtual {v0, p2}, Lf/h/a/f/i/c/l0;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;

    move-result-object p2

    invoke-virtual {p1, p2}, Lf/h/a/f/i/c/p0;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final d(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/e3;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-boolean v1, p0, Lf/h/a/f/i/c/f2;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-virtual {v1, p1}, Lf/h/a/f/i/c/l0;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;

    move-result-object p1

    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p1}, Lf/h/a/f/i/c/p0;->hashCode()I

    move-result p1

    add-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method public final e(Ljava/lang/Object;Lf/h/a/f/i/c/y3;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lf/h/a/f/i/c/y3;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/l0;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/c/p0;->c()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/f/i/c/s0;

    invoke-interface {v2}, Lf/h/a/f/i/c/s0;->S()Lf/h/a/f/i/c/x3;

    move-result-object v3

    sget-object v4, Lf/h/a/f/i/c/x3;->l:Lf/h/a/f/i/c/x3;

    if-ne v3, v4, :cond_1

    invoke-interface {v2}, Lf/h/a/f/i/c/s0;->Y()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v2}, Lf/h/a/f/i/c/s0;->y()Z

    move-result v3

    if-nez v3, :cond_1

    instance-of v3, v1, Lf/h/a/f/i/c/h1;

    invoke-interface {v2}, Lf/h/a/f/i/c/s0;->c()I

    move-result v2

    if-eqz v3, :cond_0

    check-cast v1, Lf/h/a/f/i/c/h1;

    iget-object v1, v1, Lf/h/a/f/i/c/h1;->d:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/c/f1;

    invoke-virtual {v1}, Lf/h/a/f/i/c/j1;->c()Lf/h/a/f/i/c/y;

    move-result-object v1

    goto :goto_1

    :cond_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    :goto_1
    move-object v3, p2

    check-cast v3, Lf/h/a/f/i/c/h0;

    invoke-virtual {v3, v2, v1}, Lf/h/a/f/i/c/h0;->c(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Found invalid MessageSet item."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    iget-object v0, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/e3;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lf/h/a/f/i/c/e3;->d(Ljava/lang/Object;Lf/h/a/f/i/c/y3;)V

    return-void
.end method

.method public final f(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    sget-object v1, Lf/h/a/f/i/c/t2;->a:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/e3;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2}, Lf/h/a/f/i/c/e3;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lf/h/a/f/i/c/e3;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lf/h/a/f/i/c/e3;->f(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-boolean v0, p0, Lf/h/a/f/i/c/f2;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-static {v0, p1, p2}, Lf/h/a/f/i/c/t2;->e(Lf/h/a/f/i/c/l0;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final g(Ljava/lang/Object;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->b:Lf/h/a/f/i/c/e3;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/e3;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/a/f/i/c/e3;->j(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    add-int/2addr v0, v1

    iget-boolean v2, p0, Lf/h/a/f/i/c/f2;->c:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-virtual {v2, p1}, Lf/h/a/f/i/c/l0;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;

    move-result-object p1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p1, Lf/h/a/f/i/c/p0;->a:Lf/h/a/f/i/c/u2;

    invoke-virtual {v3}, Lf/h/a/f/i/c/u2;->e()I

    move-result v3

    if-ge v1, v3, :cond_0

    iget-object v3, p1, Lf/h/a/f/i/c/p0;->a:Lf/h/a/f/i/c/u2;

    invoke-virtual {v3, v1}, Lf/h/a/f/i/c/u2;->c(I)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-static {v3}, Lf/h/a/f/i/c/p0;->k(Ljava/util/Map$Entry;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lf/h/a/f/i/c/p0;->a:Lf/h/a/f/i/c/u2;

    invoke-virtual {p1}, Lf/h/a/f/i/c/u2;->f()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-static {v1}, Lf/h/a/f/i/c/p0;->k(Ljava/util/Map$Entry;)I

    move-result v1

    add-int/2addr v2, v1

    goto :goto_1

    :cond_1
    add-int/2addr v0, v2

    :cond_2
    return v0
.end method

.method public final h(Ljava/lang/Object;[BIILf/h/a/f/i/c/u;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;[BII",
            "Lf/h/a/f/i/c/u;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/a/f/i/c/y0;

    iget-object v0, p1, Lf/h/a/f/i/c/y0;->zzjp:Lf/h/a/f/i/c/f3;

    sget-object v1, Lf/h/a/f/i/c/f3;->f:Lf/h/a/f/i/c/f3;

    if-ne v0, v1, :cond_0

    invoke-static {}, Lf/h/a/f/i/c/f3;->e()Lf/h/a/f/i/c/f3;

    move-result-object v0

    iput-object v0, p1, Lf/h/a/f/i/c/y0;->zzjp:Lf/h/a/f/i/c/f3;

    :cond_0
    :goto_0
    if-ge p3, p4, :cond_9

    invoke-static {p2, p3, p5}, Lf/h/a/f/f/n/g;->x0([BILf/h/a/f/i/c/u;)I

    move-result v3

    iget v1, p5, Lf/h/a/f/i/c/u;->a:I

    const/16 p1, 0xb

    const/4 p3, 0x2

    if-eq v1, p1, :cond_2

    and-int/lit8 p1, v1, 0x7

    if-ne p1, p3, :cond_1

    move-object v2, p2

    move v4, p4

    move-object v5, v0

    move-object v6, p5

    invoke-static/range {v1 .. v6}, Lf/h/a/f/f/n/g;->n0(I[BIILf/h/a/f/i/c/f3;Lf/h/a/f/i/c/u;)I

    move-result p3

    goto :goto_0

    :cond_1
    invoke-static {v1, p2, v3, p4, p5}, Lf/h/a/f/f/n/g;->l0(I[BIILf/h/a/f/i/c/u;)I

    move-result p3

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    const/4 v1, 0x0

    :goto_1
    if-ge v3, p4, :cond_6

    invoke-static {p2, v3, p5}, Lf/h/a/f/f/n/g;->x0([BILf/h/a/f/i/c/u;)I

    move-result v2

    iget v3, p5, Lf/h/a/f/i/c/u;->a:I

    ushr-int/lit8 v4, v3, 0x3

    and-int/lit8 v5, v3, 0x7

    if-eq v4, p3, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    goto :goto_2

    :cond_3
    if-ne v5, p3, :cond_5

    invoke-static {p2, v2, p5}, Lf/h/a/f/f/n/g;->p1([BILf/h/a/f/i/c/u;)I

    move-result v3

    iget-object v1, p5, Lf/h/a/f/i/c/u;->c:Ljava/lang/Object;

    check-cast v1, Lf/h/a/f/i/c/y;

    goto :goto_1

    :cond_4
    if-nez v5, :cond_5

    invoke-static {p2, v2, p5}, Lf/h/a/f/f/n/g;->x0([BILf/h/a/f/i/c/u;)I

    move-result v3

    iget p1, p5, Lf/h/a/f/i/c/u;->a:I

    goto :goto_1

    :cond_5
    :goto_2
    const/16 v4, 0xc

    if-eq v3, v4, :cond_7

    invoke-static {v3, p2, v2, p4, p5}, Lf/h/a/f/f/n/g;->l0(I[BIILf/h/a/f/i/c/u;)I

    move-result v3

    goto :goto_1

    :cond_6
    move v2, v3

    :cond_7
    if-eqz v1, :cond_8

    shl-int/lit8 p1, p1, 0x3

    or-int/2addr p1, p3

    invoke-virtual {v0, p1, v1}, Lf/h/a/f/i/c/f3;->b(ILjava/lang/Object;)V

    :cond_8
    move p3, v2

    goto :goto_0

    :cond_9
    if-ne p3, p4, :cond_a

    return-void

    :cond_a
    invoke-static {}, Lcom/google/android/gms/internal/clearcut/zzco;->c()Lcom/google/android/gms/internal/clearcut/zzco;

    move-result-object p1

    throw p1
.end method

.method public final i(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->d:Lf/h/a/f/i/c/l0;

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/l0;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/a/f/i/c/p0;->b()Z

    move-result p1

    return p1
.end method

.method public final newInstance()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/f2;->a:Lf/h/a/f/i/c/b2;

    invoke-interface {v0}, Lf/h/a/f/i/c/b2;->k()Lf/h/a/f/i/c/c2;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/y0$a;

    invoke-virtual {v0}, Lf/h/a/f/i/c/y0$a;->m()Lf/h/a/f/i/c/b2;

    move-result-object v0

    return-object v0
.end method
