.class public final Lf/h/a/f/i/c/r1;
.super Ljava/lang/Object;

# interfaces
.implements Lf/h/a/f/i/c/s2;


# static fields
.field public static final b:Lf/h/a/f/i/c/a2;


# instance fields
.field public final a:Lf/h/a/f/i/c/a2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/f/i/c/s1;

    invoke-direct {v0}, Lf/h/a/f/i/c/s1;-><init>()V

    sput-object v0, Lf/h/a/f/i/c/r1;->b:Lf/h/a/f/i/c/a2;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    new-instance v0, Lf/h/a/f/i/c/t1;

    const/4 v1, 0x2

    new-array v1, v1, [Lf/h/a/f/i/c/a2;

    sget-object v2, Lf/h/a/f/i/c/x0;->a:Lf/h/a/f/i/c/x0;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    :try_start_0
    const-string v2, "com.google.protobuf.DescriptorMessageInfoFactory"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "getInstance"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v4, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/f/i/c/a2;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v2, Lf/h/a/f/i/c/r1;->b:Lf/h/a/f/i/c/a2;

    :goto_0
    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lf/h/a/f/i/c/t1;-><init>([Lf/h/a/f/i/c/a2;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lf/h/a/f/i/c/a1;->a:Ljava/nio/charset/Charset;

    iput-object v0, p0, Lf/h/a/f/i/c/r1;->a:Lf/h/a/f/i/c/a2;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Lf/h/a/f/i/c/r2;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lf/h/a/f/i/c/r2<",
            "TT;>;"
        }
    .end annotation

    const-class v0, Lf/h/a/f/i/c/y0;

    sget-object v1, Lf/h/a/f/i/c/t2;->a:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lf/h/a/f/i/c/t2;->a:Ljava/lang/Class;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Message classes must extend GeneratedMessage or GeneratedMessageLite"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v1, p0, Lf/h/a/f/i/c/r1;->a:Lf/h/a/f/i/c/a2;

    invoke-interface {v1, p1}, Lf/h/a/f/i/c/a2;->b(Ljava/lang/Class;)Lf/h/a/f/i/c/z1;

    move-result-object v2

    invoke-interface {v2}, Lf/h/a/f/i/c/z1;->b()Z

    move-result v1

    const-string v3, "Protobuf runtime is not correctly loaded."

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-eqz v1, :cond_4

    if-eqz p1, :cond_2

    sget-object p1, Lf/h/a/f/i/c/t2;->d:Lf/h/a/f/i/c/e3;

    sget-object v0, Lf/h/a/f/i/c/o0;->a:Lf/h/a/f/i/c/l0;

    sget-object v0, Lf/h/a/f/i/c/o0;->a:Lf/h/a/f/i/c/l0;

    invoke-interface {v2}, Lf/h/a/f/i/c/z1;->c()Lf/h/a/f/i/c/b2;

    move-result-object v1

    new-instance v2, Lf/h/a/f/i/c/f2;

    invoke-direct {v2, p1, v0, v1}, Lf/h/a/f/i/c/f2;-><init>(Lf/h/a/f/i/c/e3;Lf/h/a/f/i/c/l0;Lf/h/a/f/i/c/b2;)V

    return-object v2

    :cond_2
    sget-object p1, Lf/h/a/f/i/c/t2;->b:Lf/h/a/f/i/c/e3;

    sget-object v0, Lf/h/a/f/i/c/o0;->b:Lf/h/a/f/i/c/l0;

    if-eqz v0, :cond_3

    invoke-interface {v2}, Lf/h/a/f/i/c/z1;->c()Lf/h/a/f/i/c/b2;

    move-result-object v1

    new-instance v2, Lf/h/a/f/i/c/f2;

    invoke-direct {v2, p1, v0, v1}, Lf/h/a/f/i/c/f2;-><init>(Lf/h/a/f/i/c/e3;Lf/h/a/f/i/c/l0;Lf/h/a/f/i/c/b2;)V

    return-object v2

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_7

    invoke-interface {v2}, Lf/h/a/f/i/c/z1;->a()I

    move-result p1

    if-ne p1, v1, :cond_5

    const/4 v0, 0x1

    :cond_5
    if-eqz v0, :cond_6

    sget-object v3, Lf/h/a/f/i/c/j2;->b:Lf/h/a/f/i/c/h2;

    sget-object v4, Lf/h/a/f/i/c/m1;->b:Lf/h/a/f/i/c/m1;

    sget-object v5, Lf/h/a/f/i/c/t2;->d:Lf/h/a/f/i/c/e3;

    sget-object p1, Lf/h/a/f/i/c/o0;->a:Lf/h/a/f/i/c/l0;

    sget-object v6, Lf/h/a/f/i/c/o0;->a:Lf/h/a/f/i/c/l0;

    :goto_1
    sget-object v7, Lf/h/a/f/i/c/y1;->b:Lf/h/a/f/i/c/w1;

    :goto_2
    invoke-static/range {v2 .. v7}, Lf/h/a/f/i/c/e2;->p(Lf/h/a/f/i/c/z1;Lf/h/a/f/i/c/h2;Lf/h/a/f/i/c/m1;Lf/h/a/f/i/c/e3;Lf/h/a/f/i/c/l0;Lf/h/a/f/i/c/w1;)Lf/h/a/f/i/c/e2;

    move-result-object p1

    return-object p1

    :cond_6
    sget-object v3, Lf/h/a/f/i/c/j2;->b:Lf/h/a/f/i/c/h2;

    sget-object v4, Lf/h/a/f/i/c/m1;->b:Lf/h/a/f/i/c/m1;

    sget-object v5, Lf/h/a/f/i/c/t2;->d:Lf/h/a/f/i/c/e3;

    const/4 v6, 0x0

    goto :goto_1

    :cond_7
    invoke-interface {v2}, Lf/h/a/f/i/c/z1;->a()I

    move-result p1

    if-ne p1, v1, :cond_8

    const/4 v0, 0x1

    :cond_8
    if-eqz v0, :cond_a

    sget-object p1, Lf/h/a/f/i/c/j2;->a:Lf/h/a/f/i/c/h2;

    sget-object v4, Lf/h/a/f/i/c/m1;->a:Lf/h/a/f/i/c/m1;

    sget-object v5, Lf/h/a/f/i/c/t2;->b:Lf/h/a/f/i/c/e3;

    sget-object v6, Lf/h/a/f/i/c/o0;->b:Lf/h/a/f/i/c/l0;

    if-eqz v6, :cond_9

    sget-object v7, Lf/h/a/f/i/c/y1;->a:Lf/h/a/f/i/c/w1;

    move-object v3, p1

    goto :goto_2

    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_a
    sget-object v3, Lf/h/a/f/i/c/j2;->a:Lf/h/a/f/i/c/h2;

    sget-object v4, Lf/h/a/f/i/c/m1;->a:Lf/h/a/f/i/c/m1;

    sget-object v5, Lf/h/a/f/i/c/t2;->c:Lf/h/a/f/i/c/e3;

    const/4 v6, 0x0

    sget-object v7, Lf/h/a/f/i/c/y1;->a:Lf/h/a/f/i/c/w1;

    goto :goto_2
.end method
