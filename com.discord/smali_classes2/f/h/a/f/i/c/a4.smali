.class public Lf/h/a/f/i/c/a4;
.super Lf/h/a/f/i/c/e4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lf/h/a/f/i/c/a4<",
        "TM;>;>",
        "Lf/h/a/f/i/c/e4;"
    }
.end annotation


# instance fields
.field public e:Lf/h/a/f/i/c/b4;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/a/f/i/c/e4;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lf/h/a/f/i/c/z3;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p1, p0, Lf/h/a/f/i/c/a4;->e:Lf/h/a/f/i/c/b4;

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lf/h/a/f/i/c/a4;->e:Lf/h/a/f/i/c/b4;

    iget v1, v0, Lf/h/a/f/i/c/b4;->f:I

    if-ge p1, v1, :cond_1

    iget-object v0, v0, Lf/h/a/f/i/c/b4;->e:[Lf/h/a/f/i/c/c4;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lf/h/a/f/i/c/c4;->b()V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/c/a4;->i()Lf/h/a/f/i/c/a4;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 4

    iget-object v0, p0, Lf/h/a/f/i/c/a4;->e:Lf/h/a/f/i/c/b4;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lf/h/a/f/i/c/a4;->e:Lf/h/a/f/i/c/b4;

    iget v3, v2, Lf/h/a/f/i/c/b4;->f:I

    if-ge v0, v3, :cond_0

    iget-object v2, v2, Lf/h/a/f/i/c/b4;->e:[Lf/h/a/f/i/c/c4;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lf/h/a/f/i/c/c4;->e()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public synthetic h()Lf/h/a/f/i/c/e4;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/c/a4;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/a4;

    return-object v0
.end method

.method public i()Lf/h/a/f/i/c/a4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Lf/h/a/f/i/c/e4;->h()Lf/h/a/f/i/c/e4;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/a4;

    sget-object v1, Lf/h/a/f/i/c/d4;->a:Ljava/lang/Object;

    iget-object v1, p0, Lf/h/a/f/i/c/a4;->e:Lf/h/a/f/i/c/b4;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lf/h/a/f/i/c/b4;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/c/b4;

    iput-object v1, v0, Lf/h/a/f/i/c/a4;->e:Lf/h/a/f/i/c/b4;

    :cond_0
    return-object v0
.end method
