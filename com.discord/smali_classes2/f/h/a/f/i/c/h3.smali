.class public final Lf/h/a/f/i/c/h3;
.super Ljava/util/AbstractList;

# interfaces
.implements Lf/h/a/f/i/c/l1;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList<",
        "Ljava/lang/String;",
        ">;",
        "Lf/h/a/f/i/c/l1;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field public final d:Lf/h/a/f/i/c/l1;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/c/l1;)V
    .locals 0

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/c/h3;->d:Lf/h/a/f/i/c/l1;

    return-void
.end method


# virtual methods
.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/h3;->d:Lf/h/a/f/i/c/l1;

    invoke-interface {v0}, Lf/h/a/f/i/c/l1;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/c/h3;->d:Lf/h/a/f/i/c/l1;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/i/c/j3;

    invoke-direct {v0, p0}, Lf/h/a/f/i/c/j3;-><init>(Lf/h/a/f/i/c/h3;)V

    return-object v0
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/i/c/i3;

    invoke-direct {v0, p0, p1}, Lf/h/a/f/i/c/i3;-><init>(Lf/h/a/f/i/c/h3;I)V

    return-object v0
.end method

.method public final o()Lf/h/a/f/i/c/l1;
    .locals 0

    return-object p0
.end method

.method public final q(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/c/h3;->d:Lf/h/a/f/i/c/l1;

    invoke-interface {v0, p1}, Lf/h/a/f/i/c/l1;->q(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/c/h3;->d:Lf/h/a/f/i/c/l1;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
