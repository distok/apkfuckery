.class public final Lf/h/a/f/i/c/q4;
.super Lf/h/a/f/f/h/i/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/h/i/d<",
        "Lcom/google/android/gms/common/api/Status;",
        "Lf/h/a/f/i/c/u4;",
        ">;"
    }
.end annotation


# instance fields
.field public final l:Lcom/google/android/gms/clearcut/zze;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/clearcut/zze;Lf/h/a/f/f/h/c;)V
    .locals 1

    sget-object v0, Lf/h/a/f/d/a;->o:Lf/h/a/f/f/h/a;

    invoke-direct {p0, v0, p2}, Lf/h/a/f/f/h/i/d;-><init>(Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/c;)V

    iput-object p1, p0, Lf/h/a/f/i/c/q4;->l:Lcom/google/android/gms/clearcut/zze;

    return-void
.end method


# virtual methods
.method public final synthetic d(Lcom/google/android/gms/common/api/Status;)Lf/h/a/f/f/h/g;
    .locals 0

    return-object p1
.end method

.method public final synthetic k(Lf/h/a/f/f/h/a$b;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lf/h/a/f/i/c/u4;

    new-instance v0, Lf/h/a/f/i/c/t4;

    invoke-direct {v0, p0}, Lf/h/a/f/i/c/t4;-><init>(Lf/h/a/f/i/c/q4;)V

    :try_start_0
    iget-object v1, p0, Lf/h/a/f/i/c/q4;->l:Lcom/google/android/gms/clearcut/zze;

    iget-object v2, v1, Lcom/google/android/gms/clearcut/zze;->m:Lf/h/a/f/d/a$c;

    if-eqz v2, :cond_0

    iget-object v3, v1, Lcom/google/android/gms/clearcut/zze;->l:Lf/h/a/f/i/c/r4;

    iget-object v4, v3, Lf/h/a/f/i/c/r4;->k:[B

    array-length v4, v4

    if-nez v4, :cond_0

    invoke-interface {v2}, Lf/h/a/f/d/a$c;->a()[B

    move-result-object v2

    iput-object v2, v3, Lf/h/a/f/i/c/r4;->k:[B

    :cond_0
    iget-object v2, v1, Lcom/google/android/gms/clearcut/zze;->l:Lf/h/a/f/i/c/r4;

    invoke-virtual {v2}, Lf/h/a/f/i/c/e4;->f()I

    move-result v3

    new-array v4, v3, [B

    invoke-static {v2, v4, v3}, Lf/h/a/f/i/c/e4;->e(Lf/h/a/f/i/c/e4;[BI)V

    iput-object v4, v1, Lcom/google/android/gms/clearcut/zze;->e:[B
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p1}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/c/x4;

    iget-object v1, p0, Lf/h/a/f/i/c/q4;->l:Lcom/google/android/gms/clearcut/zze;

    invoke-interface {p1, v0, v1}, Lf/h/a/f/i/c/x4;->u(Lf/h/a/f/i/c/v4;Lcom/google/android/gms/clearcut/zze;)V

    return-void

    :catch_0
    move-exception p1

    const-string v0, "ClearcutLoggerApiImpl"

    const-string v1, "derived ClearcutLogger.MessageProducer "

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance p1, Lcom/google/android/gms/common/api/Status;

    const/16 v0, 0xa

    const-string v1, "MessageProducer"

    invoke-direct {p1, v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, p1}, Lf/h/a/f/f/h/i/d;->a(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method
