.class public final Lf/h/a/f/i/c/l;
.super Lf/h/a/f/i/c/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/c/e<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final k:Ljava/lang/Object;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final synthetic n:Lf/h/a/f/i/c/n;


# direct methods
.method public constructor <init>(Lf/h/a/f/i/c/o;Ljava/lang/String;Ljava/lang/Object;Lf/h/a/f/i/c/n;)V
    .locals 0

    iput-object p4, p0, Lf/h/a/f/i/c/l;->n:Lf/h/a/f/i/c/n;

    const/4 p4, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lf/h/a/f/i/c/e;-><init>(Lf/h/a/f/i/c/o;Ljava/lang/String;Ljava/lang/Object;Lf/h/a/f/i/c/i;)V

    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/c/l;->k:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final b(Landroid/content/SharedPreferences;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")TT;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/i/c/e;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/h/a/f/i/c/l;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string v0, "Invalid byte[] value in SharedPreferences for "

    iget-object v1, p0, Lf/h/a/f/i/c/e;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    const-string v1, "PhenotypeFlag"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p1, 0x0

    return-object p1
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/i/c/l;->k:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v1, p0, Lf/h/a/f/i/c/l;->l:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/h/a/f/i/c/l;->n:Lf/h/a/f/i/c/n;

    const/4 v2, 0x3

    invoke-static {p1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    check-cast v1, Lf/h/a/f/i/c/a5;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lf/h/a/f/i/c/m4;->q([B)Lf/h/a/f/i/c/m4;

    move-result-object v1

    iput-object p1, p0, Lf/h/a/f/i/c/l;->l:Ljava/lang/String;

    iput-object v1, p0, Lf/h/a/f/i/c/l;->m:Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lf/h/a/f/i/c/l;->m:Ljava/lang/Object;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    const-string v0, "PhenotypeFlag"

    iget-object v1, p0, Lf/h/a/f/i/c/e;->b:Ljava/lang/String;

    const/16 v2, 0x1b

    invoke-static {v1, v2}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v2

    invoke-static {p1, v2}, Lf/e/c/a/a;->I(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "Invalid byte[] value for "

    const-string v4, ": "

    invoke-static {v2, v3, v1, v4, p1}, Lf/e/c/a/a;->g(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    return-object p1
.end method
