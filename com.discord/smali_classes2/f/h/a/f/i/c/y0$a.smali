.class public Lf/h/a/f/i/c/y0$a;
.super Lf/h/a/f/i/c/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/f/i/c/y0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lf/h/a/f/i/c/y0<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Lf/h/a/f/i/c/y0$a<",
        "TMessageType;TBuilderType;>;>",
        "Lf/h/a/f/i/c/q<",
        "TMessageType;TBuilderType;>;"
    }
.end annotation


# instance fields
.field public final d:Lf/h/a/f/i/c/y0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field public e:Lf/h/a/f/i/c/y0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(Lf/h/a/f/i/c/y0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/h/a/f/i/c/q;-><init>()V

    iput-object p1, p0, Lf/h/a/f/i/c/y0$a;->d:Lf/h/a/f/i/c/y0;

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v1}, Lf/h/a/f/i/c/y0;->d(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/c/y0;

    iput-object p1, p0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/f/i/c/y0$a;->f:Z

    return-void
.end method


# virtual methods
.method public synthetic clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/f/i/c/y0$a;->d:Lf/h/a/f/i/c/y0;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lf/h/a/f/i/c/y0;->d(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/y0$a;

    invoke-virtual {p0}, Lf/h/a/f/i/c/y0$a;->m()Lf/h/a/f/i/c/b2;

    move-result-object v1

    check-cast v1, Lf/h/a/f/i/c/y0;

    invoke-virtual {v0, v1}, Lf/h/a/f/i/c/y0$a;->f(Lf/h/a/f/i/c/y0;)Lf/h/a/f/i/c/y0$a;

    return-object v0
.end method

.method public final synthetic e()Lf/h/a/f/i/c/b2;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/c/y0$a;->d:Lf/h/a/f/i/c/y0;

    return-object v0
.end method

.method public final f(Lf/h/a/f/i/c/y0;)Lf/h/a/f/i/c/y0$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TBuilderType;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/c/y0$a;->g()V

    iget-object v0, p0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    sget-object v1, Lf/h/a/f/i/c/m2;->c:Lf/h/a/f/i/c/m2;

    invoke-virtual {v1, v0}, Lf/h/a/f/i/c/m2;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/r2;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lf/h/a/f/i/c/r2;->f(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p0
.end method

.method public g()V
    .locals 3

    iget-boolean v0, p0, Lf/h/a/f/i/c/y0$a;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lf/h/a/f/i/c/y0;->d(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/y0;

    iget-object v1, p0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    sget-object v2, Lf/h/a/f/i/c/m2;->c:Lf/h/a/f/i/c/m2;

    invoke-virtual {v2, v0}, Lf/h/a/f/i/c/m2;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/r2;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lf/h/a/f/i/c/r2;->f(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/f/i/c/y0$a;->f:Z

    :cond_0
    return-void
.end method

.method public m()Lf/h/a/f/i/c/b2;
    .locals 2

    iget-boolean v0, p0, Lf/h/a/f/i/c/y0$a;->f:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    sget-object v1, Lf/h/a/f/i/c/m2;->c:Lf/h/a/f/i/c/m2;

    invoke-virtual {v1, v0}, Lf/h/a/f/i/c/m2;->b(Ljava/lang/Object;)Lf/h/a/f/i/c/r2;

    move-result-object v1

    invoke-interface {v1, v0}, Lf/h/a/f/i/c/r2;->b(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/f/i/c/y0$a;->f:Z

    :goto_0
    iget-object v0, p0, Lf/h/a/f/i/c/y0$a;->e:Lf/h/a/f/i/c/y0;

    return-object v0
.end method
