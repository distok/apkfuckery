.class public abstract Lf/h/a/f/i/c/p;
.super Ljava/lang/Object;

# interfaces
.implements Lf/h/a/f/i/c/b2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lf/h/a/f/i/c/p<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Lf/h/a/f/i/c/q<",
        "TMessageType;TBuilderType;>;>",
        "Ljava/lang/Object;",
        "Lf/h/a/f/i/c/b2;"
    }
.end annotation


# static fields
.field private static zzey:Z = false


# instance fields
.field public zzex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/f/i/c/p;->zzex:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public c()I
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final h()Lf/h/a/f/i/c/y;
    .locals 7

    move-object v0, p0

    check-cast v0, Lf/h/a/f/i/c/y0;

    :try_start_0
    invoke-virtual {v0}, Lf/h/a/f/i/c/y0;->l()I

    move-result v1

    sget-object v2, Lf/h/a/f/i/c/y;->d:Lf/h/a/f/i/c/y;

    new-array v2, v1, [B

    sget-object v3, Lcom/google/android/gms/internal/clearcut/zzbn;->b:Ljava/util/logging/Logger;

    new-instance v3, Lcom/google/android/gms/internal/clearcut/zzbn$a;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4, v1}, Lcom/google/android/gms/internal/clearcut/zzbn$a;-><init>([BII)V

    invoke-virtual {v0, v3}, Lf/h/a/f/i/c/y0;->j(Lcom/google/android/gms/internal/clearcut/zzbn;)V

    invoke-virtual {v3}, Lcom/google/android/gms/internal/clearcut/zzbn;->l()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lf/h/a/f/i/c/d0;

    invoke-direct {v0, v2}, Lf/h/a/f/i/c/d0;-><init>([B)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "ByteString"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3e

    add-int/lit8 v4, v4, 0xa

    const-string v5, "Serializing "

    const-string v6, " to a "

    invoke-static {v4, v5, v3, v6, v2}, Lf/e/c/a/a;->F(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " threw an IOException (should never happen)."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
