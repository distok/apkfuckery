.class public final Lf/h/a/f/i/c/m0;
.super Lf/h/a/f/i/c/l0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/i/c/l0<",
        "Lf/h/a/f/i/c/y0$d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/a/f/i/c/l0;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map$Entry;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)I"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/c/y0$d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    return p1
.end method

.method public final b(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/a/f/i/c/p0<",
            "Lf/h/a/f/i/c/y0$d;",
            ">;"
        }
    .end annotation

    check-cast p1, Lf/h/a/f/i/c/y0$c;

    iget-object p1, p1, Lf/h/a/f/i/c/y0$c;->zzjv:Lf/h/a/f/i/c/p0;

    return-object p1
.end method

.method public final c(Lf/h/a/f/i/c/y3;Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/f/i/c/y3;",
            "Ljava/util/Map$Entry<",
            "**>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/f/i/c/y0$d;

    sget-object p2, Lf/h/a/f/i/c/n0;->a:[I

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    throw p1
.end method

.method public final d(Ljava/lang/Object;)Lf/h/a/f/i/c/p0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/a/f/i/c/p0<",
            "Lf/h/a/f/i/c/y0$d;",
            ">;"
        }
    .end annotation

    check-cast p1, Lf/h/a/f/i/c/y0$c;

    iget-object v0, p1, Lf/h/a/f/i/c/y0$c;->zzjv:Lf/h/a/f/i/c/p0;

    iget-boolean v1, v0, Lf/h/a/f/i/c/p0;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lf/h/a/f/i/c/p0;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/f/i/c/p0;

    iput-object v0, p1, Lf/h/a/f/i/c/y0$c;->zzjv:Lf/h/a/f/i/c/p0;

    :cond_0
    return-object v0
.end method

.method public final e(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lf/h/a/f/i/c/y0$c;

    iget-object p1, p1, Lf/h/a/f/i/c/y0$c;->zzjv:Lf/h/a/f/i/c/p0;

    iget-boolean v0, p1, Lf/h/a/f/i/c/p0;->b:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lf/h/a/f/i/c/p0;->a:Lf/h/a/f/i/c/u2;

    invoke-virtual {v0}, Lf/h/a/f/i/c/u2;->i()V

    const/4 v0, 0x1

    iput-boolean v0, p1, Lf/h/a/f/i/c/p0;->b:Z

    :goto_0
    return-void
.end method

.method public final f(Lf/h/a/f/i/c/b2;)Z
    .locals 0

    instance-of p1, p1, Lf/h/a/f/i/c/y0$c;

    return p1
.end method
