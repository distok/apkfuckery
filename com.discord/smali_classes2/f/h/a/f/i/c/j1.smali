.class public Lf/h/a/f/i/c/j1;
.super Ljava/lang/Object;


# instance fields
.field public volatile a:Lf/h/a/f/i/c/b2;

.field public volatile b:Lf/h/a/f/i/c/y;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lf/h/a/f/i/c/k0;->a()Lf/h/a/f/i/c/k0;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    invoke-virtual {v0}, Lf/h/a/f/i/c/y;->size()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    invoke-interface {v0}, Lf/h/a/f/i/c/b2;->l()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lf/h/a/f/i/c/b2;)Lf/h/a/f/i/c/b2;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    if-nez v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_0
    :try_start_1
    iput-object p1, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    sget-object v0, Lf/h/a/f/i/c/y;->d:Lf/h/a/f/i/c/y;

    iput-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;
    :try_end_1
    .catch Lcom/google/android/gms/internal/clearcut/zzco; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    iput-object p1, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    sget-object p1, Lf/h/a/f/i/c/y;->d:Lf/h/a/f/i/c/y;

    iput-object p1, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :cond_1
    :goto_1
    iget-object p1, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    return-object p1
.end method

.method public final c()Lf/h/a/f/i/c/y;
    .locals 1

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    return-object v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    monitor-exit p0

    return-object v0

    :cond_1
    iget-object v0, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    if-nez v0, :cond_2

    sget-object v0, Lf/h/a/f/i/c/y;->d:Lf/h/a/f/i/c/y;

    :goto_0
    iput-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    invoke-interface {v0}, Lf/h/a/f/i/c/b2;->h()Lf/h/a/f/i/c/y;

    move-result-object v0

    goto :goto_0

    :goto_1
    iget-object v0, p0, Lf/h/a/f/i/c/j1;->b:Lf/h/a/f/i/c/y;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lf/h/a/f/i/c/j1;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    check-cast p1, Lf/h/a/f/i/c/j1;

    iget-object v0, p0, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    iget-object v1, p1, Lf/h/a/f/i/c/j1;->a:Lf/h/a/f/i/c/b2;

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lf/h/a/f/i/c/j1;->c()Lf/h/a/f/i/c/y;

    move-result-object v0

    invoke-virtual {p1}, Lf/h/a/f/i/c/j1;->c()Lf/h/a/f/i/c/y;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/a/f/i/c/y;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lf/h/a/f/i/c/d2;->e()Lf/h/a/f/i/c/b2;

    move-result-object v1

    invoke-virtual {p1, v1}, Lf/h/a/f/i/c/j1;->b(Lf/h/a/f/i/c/b2;)Lf/h/a/f/i/c/b2;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_4
    invoke-interface {v1}, Lf/h/a/f/i/c/d2;->e()Lf/h/a/f/i/c/b2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/h/a/f/i/c/j1;->b(Lf/h/a/f/i/c/b2;)Lf/h/a/f/i/c/b2;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
