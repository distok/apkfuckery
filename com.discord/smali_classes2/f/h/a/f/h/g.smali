.class public interface abstract Lf/h/a/f/h/g;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-basement@@17.4.0"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract I(Lf/h/a/f/g/a;Ljava/lang/String;Z)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract T(Lf/h/a/f/g/a;Ljava/lang/String;Z)Lf/h/a/f/g/a;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract U(Lf/h/a/f/g/a;Ljava/lang/String;Z)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract X(Lf/h/a/f/g/a;Ljava/lang/String;I)Lf/h/a/f/g/a;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract b()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract d(Lf/h/a/f/g/a;Ljava/lang/String;ILf/h/a/f/g/a;)Lf/h/a/f/g/a;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract x(Lf/h/a/f/g/a;Ljava/lang/String;I)Lf/h/a/f/g/a;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
