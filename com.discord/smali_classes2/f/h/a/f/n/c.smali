.class public final Lf/h/a/f/n/c;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-base@@17.3.0"


# static fields
.field public static final a:Lf/h/a/f/f/h/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$g<",
            "Lf/h/a/f/n/b/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lf/h/a/f/f/h/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$g<",
            "Lf/h/a/f/n/b/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "Lf/h/a/f/n/b/a;",
            "Lf/h/a/f/n/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "Lf/h/a/f/n/b/a;",
            "Lf/h/a/f/n/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    new-instance v0, Lf/h/a/f/f/h/a$g;

    invoke-direct {v0}, Lf/h/a/f/f/h/a$g;-><init>()V

    sput-object v0, Lf/h/a/f/n/c;->a:Lf/h/a/f/f/h/a$g;

    new-instance v1, Lf/h/a/f/f/h/a$g;

    invoke-direct {v1}, Lf/h/a/f/f/h/a$g;-><init>()V

    sput-object v1, Lf/h/a/f/n/c;->b:Lf/h/a/f/f/h/a$g;

    new-instance v2, Lf/h/a/f/n/e;

    invoke-direct {v2}, Lf/h/a/f/n/e;-><init>()V

    sput-object v2, Lf/h/a/f/n/c;->c:Lf/h/a/f/f/h/a$a;

    new-instance v3, Lf/h/a/f/n/d;

    invoke-direct {v3}, Lf/h/a/f/n/d;-><init>()V

    sput-object v3, Lf/h/a/f/n/c;->d:Lf/h/a/f/f/h/a$a;

    const-string v4, "profile"

    const-string v5, "scopeUri must not be null or empty"

    invoke-static {v4, v5}, Lf/g/j/k/a;->n(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v4, "email"

    invoke-static {v4, v5}, Lf/g/j/k/a;->n(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v4, "Cannot construct an Api with a null ClientBuilder"

    invoke-static {v2, v4}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Cannot construct an Api with a null ClientKey"

    invoke-static {v0, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3, v4}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
