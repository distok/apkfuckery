.class public Lf/h/a/f/n/b/a;
.super Lf/h/a/f/f/k/d;
.source "com.google.android.gms:play-services-base@@17.3.0"

# interfaces
.implements Lf/h/a/f/n/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/k/d<",
        "Lf/h/a/f/n/b/e;",
        ">;",
        "Lf/h/a/f/n/f;"
    }
.end annotation


# instance fields
.field public final A:Lf/h/a/f/f/k/c;

.field public final B:Landroid/os/Bundle;

.field public final C:Ljava/lang/Integer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;ZLf/h/a/f/f/k/c;Landroid/os/Bundle;Lf/h/a/f/f/h/c$a;Lf/h/a/f/f/h/c$b;)V
    .locals 7

    const/16 v3, 0x2c

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lf/h/a/f/f/k/d;-><init>(Landroid/content/Context;Landroid/os/Looper;ILf/h/a/f/f/k/c;Lf/h/a/f/f/h/i/f;Lf/h/a/f/f/h/i/l;)V

    iput-boolean p3, p0, Lf/h/a/f/n/b/a;->z:Z

    iput-object p4, p0, Lf/h/a/f/n/b/a;->A:Lf/h/a/f/f/k/c;

    iput-object p5, p0, Lf/h/a/f/n/b/a;->B:Landroid/os/Bundle;

    iget-object p1, p4, Lf/h/a/f/f/k/c;->h:Ljava/lang/Integer;

    iput-object p1, p0, Lf/h/a/f/n/b/a;->C:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public final d(Lf/h/a/f/n/b/c;)V
    .locals 5

    const-string v0, "Expecting a valid ISignInCallbacks"

    invoke-static {p1, v0}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lf/h/a/f/n/b/a;->A:Lf/h/a/f/f/k/c;

    iget-object v0, v0, Lf/h/a/f/f/k/c;->a:Landroid/accounts/Account;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "<<default account>>"

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 v2, 0x0

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/a/f/f/k/b;->c:Landroid/content/Context;

    invoke-static {v1}, Lf/h/a/f/c/a/a/a/a;->a(Landroid/content/Context;)Lf/h/a/f/c/a/a/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/f/c/a/a/a/a;->b()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v2

    :cond_1
    new-instance v1, Lcom/google/android/gms/common/internal/zas;

    iget-object v3, p0, Lf/h/a/f/n/b/a;->C:Ljava/lang/Integer;

    const-string v4, "null reference"

    invoke-static {v3, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v1, v0, v3, v2}, Lcom/google/android/gms/common/internal/zas;-><init>(Landroid/accounts/Account;ILcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    invoke-virtual {p0}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lf/h/a/f/n/b/e;

    new-instance v2, Lcom/google/android/gms/signin/internal/zak;

    invoke-direct {v2, v1}, Lcom/google/android/gms/signin/internal/zak;-><init>(Lcom/google/android/gms/common/internal/zas;)V

    invoke-interface {v0, v2, p1}, Lf/h/a/f/n/b/e;->W(Lcom/google/android/gms/signin/internal/zak;Lf/h/a/f/n/b/c;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "SignInClientImpl"

    const-string v2, "Remote service probably died when signIn is called"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    new-instance v2, Lcom/google/android/gms/signin/internal/zam;

    invoke-direct {v2}, Lcom/google/android/gms/signin/internal/zam;-><init>()V

    check-cast p1, Lf/h/a/f/f/h/i/g0;

    iget-object v3, p1, Lf/h/a/f/f/h/i/g0;->b:Landroid/os/Handler;

    new-instance v4, Lf/h/a/f/f/h/i/i0;

    invoke-direct {v4, p1, v2}, Lf/h/a/f/f/h/i/i0;-><init>(Lf/h/a/f/f/h/i/g0;Lcom/google/android/gms/signin/internal/zam;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :catch_1
    const-string p1, "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException."

    invoke-static {v1, p1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public l()I
    .locals 1

    const v0, 0xbdfcb8

    return v0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/f/n/b/a;->z:Z

    return v0
.end method

.method public final p()V
    .locals 1

    new-instance v0, Lf/h/a/f/f/k/b$d;

    invoke-direct {v0, p0}, Lf/h/a/f/f/k/b$d;-><init>(Lf/h/a/f/f/k/b;)V

    invoke-virtual {p0, v0}, Lf/h/a/f/f/k/b;->g(Lf/h/a/f/f/k/b$c;)V

    return-void
.end method

.method public synthetic r(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, "com.google.android.gms.signin.internal.ISignInService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lf/h/a/f/n/b/e;

    if-eqz v1, :cond_1

    check-cast v0, Lf/h/a/f/n/b/e;

    return-object v0

    :cond_1
    new-instance v0, Lf/h/a/f/n/b/g;

    invoke-direct {v0, p1}, Lf/h/a/f/n/b/g;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method

.method public t()Landroid/os/Bundle;
    .locals 3

    iget-object v0, p0, Lf/h/a/f/n/b/a;->A:Lf/h/a/f/f/k/c;

    iget-object v0, v0, Lf/h/a/f/f/k/c;->e:Ljava/lang/String;

    iget-object v1, p0, Lf/h/a/f/f/k/b;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/f/n/b/a;->B:Landroid/os/Bundle;

    iget-object v1, p0, Lf/h/a/f/n/b/a;->A:Lf/h/a/f/f/k/c;

    iget-object v1, v1, Lf/h/a/f/f/k/c;->e:Ljava/lang/String;

    const-string v2, "com.google.android.gms.signin.internal.realClientPackageName"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lf/h/a/f/n/b/a;->B:Landroid/os/Bundle;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.signin.internal.ISignInService"

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.signin.service.START"

    return-object v0
.end method
