.class public final synthetic Lf/h/a/f/e/k;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-cloud-messaging@@16.0.0"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/f/e/f;

.field public final e:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Lf/h/a/f/e/f;Landroid/os/IBinder;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/e/k;->d:Lf/h/a/f/e/f;

    iput-object p2, p0, Lf/h/a/f/e/k;->e:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lf/h/a/f/e/k;->d:Lf/h/a/f/e/f;

    iget-object v1, p0, Lf/h/a/f/e/k;->e:Landroid/os/IBinder;

    monitor-enter v0

    const/4 v2, 0x0

    if-nez v1, :cond_0

    :try_start_0
    const-string v1, "Null service connection"

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/e/f;->a(ILjava/lang/String;)V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    new-instance v3, Lf/h/a/f/e/o;

    invoke-direct {v3, v1}, Lf/h/a/f/e/o;-><init>(Landroid/os/IBinder;)V

    iput-object v3, v0, Lf/h/a/f/e/f;->f:Lf/h/a/f/e/o;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x2

    :try_start_2
    iput v1, v0, Lf/h/a/f/e/f;->d:I

    iget-object v1, v0, Lf/h/a/f/e/f;->i:Lf/h/a/f/e/e;

    iget-object v1, v1, Lf/h/a/f/e/e;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lf/h/a/f/e/j;

    invoke-direct {v2, v0}, Lf/h/a/f/e/j;-><init>(Lf/h/a/f/e/f;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lf/h/a/f/e/f;->a(ILjava/lang/String;)V

    monitor-exit v0

    return-void

    :goto_0
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
