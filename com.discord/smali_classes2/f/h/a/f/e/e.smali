.class public final Lf/h/a/f/e/e;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-cloud-messaging@@16.0.0"


# static fields
.field public static e:Lf/h/a/f/e/e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public c:Lf/h/a/f/e/f;

.field public d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/f/e/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lf/h/a/f/e/f;-><init>(Lf/h/a/f/e/e;Lf/h/a/f/e/g;)V

    iput-object v0, p0, Lf/h/a/f/e/e;->c:Lf/h/a/f/e/f;

    const/4 v0, 0x1

    iput v0, p0, Lf/h/a/f/e/e;->d:I

    iput-object p2, p0, Lf/h/a/f/e/e;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/f/e/e;->a:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lf/h/a/f/e/e;
    .locals 4

    const-class v0, Lf/h/a/f/e/e;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/a/f/e/e;->e:Lf/h/a/f/e/e;

    if-nez v1, :cond_0

    new-instance v1, Lf/h/a/f/e/e;

    new-instance v2, Lf/h/a/f/f/n/k/a;

    const-string v3, "MessengerIpcClient"

    invoke-direct {v2, v3}, Lf/h/a/f/f/n/k/a;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    invoke-static {v2}, Ljava/util/concurrent/Executors;->unconfigurableScheduledExecutorService(Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lf/h/a/f/e/e;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V

    sput-object v1, Lf/h/a/f/e/e;->e:Lf/h/a/f/e/e;

    :cond_0
    sget-object p0, Lf/h/a/f/e/e;->e:Lf/h/a/f/e/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public final declared-synchronized b(Lf/h/a/f/e/p;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/h/a/f/e/p<",
            "TT;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, "MessengerIpcClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessengerIpcClient"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Queueing "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lf/h/a/f/e/e;->c:Lf/h/a/f/e/f;

    invoke-virtual {v0, p1}, Lf/h/a/f/e/f;->b(Lf/h/a/f/e/p;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lf/h/a/f/e/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lf/h/a/f/e/f;-><init>(Lf/h/a/f/e/e;Lf/h/a/f/e/g;)V

    iput-object v0, p0, Lf/h/a/f/e/e;->c:Lf/h/a/f/e/f;

    invoke-virtual {v0, p1}, Lf/h/a/f/e/f;->b(Lf/h/a/f/e/p;)Z

    :cond_1
    iget-object p1, p1, Lf/h/a/f/e/p;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object p1, p1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
