.class public final Lf/h/a/f/l/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lf/h/a/f/f/h/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$g<",
            "Lf/h/a/f/i/l/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "Lf/h/a/f/i/l/c;",
            "Lf/h/a/f/f/h/a$d$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lf/h/a/f/f/h/a$g;

    invoke-direct {v0}, Lf/h/a/f/f/h/a$g;-><init>()V

    sput-object v0, Lf/h/a/f/l/a;->a:Lf/h/a/f/f/h/a$g;

    new-instance v1, Lf/h/a/f/l/c;

    invoke-direct {v1}, Lf/h/a/f/l/c;-><init>()V

    sput-object v1, Lf/h/a/f/l/a;->b:Lf/h/a/f/f/h/a$a;

    const-string v2, "Cannot construct an Api with a null ClientBuilder"

    invoke-static {v1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Cannot construct an Api with a null ClientKey"

    invoke-static {v0, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const-string v1, "content://com.google.android.gms.phenotype/"

    if-eqz v0, :cond_0

    invoke-virtual {v1, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method
