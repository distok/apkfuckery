.class public final Lf/h/a/f/k/b/e/i;
.super Lcom/google/android/gms/nearby/messages/MessagesClient;


# static fields
.field public static final k:Lf/h/a/f/f/h/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$g<",
            "Lf/h/a/f/k/b/e/f;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lf/h/a/f/f/h/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a$a<",
            "Lf/h/a/f/k/b/e/f;",
            "Lf/h/a/f/k/b/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lf/h/a/f/f/h/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/f/h/a<",
            "Lf/h/a/f/k/b/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lf/h/a/f/f/h/a$g;

    invoke-direct {v0}, Lf/h/a/f/f/h/a$g;-><init>()V

    sput-object v0, Lf/h/a/f/k/b/e/i;->k:Lf/h/a/f/f/h/a$g;

    new-instance v1, Lf/h/a/f/k/b/e/q;

    invoke-direct {v1}, Lf/h/a/f/k/b/e/q;-><init>()V

    sput-object v1, Lf/h/a/f/k/b/e/i;->l:Lf/h/a/f/f/h/a$a;

    new-instance v2, Lf/h/a/f/f/h/a;

    const-string v3, "Nearby.MESSAGES_API"

    invoke-direct {v2, v3, v1, v0}, Lf/h/a/f/f/h/a;-><init>(Ljava/lang/String;Lf/h/a/f/f/h/a$a;Lf/h/a/f/f/h/a$g;)V

    sput-object v2, Lf/h/a/f/k/b/e/i;->m:Lf/h/a/f/f/h/a;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lf/h/a/f/k/b/a;)V
    .locals 2
    .param p2    # Lf/h/a/f/k/b/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lf/h/a/f/k/b/e/i;->m:Lf/h/a/f/f/h/a;

    sget-object v1, Lf/h/a/f/f/h/b$a;->c:Lf/h/a/f/f/h/b$a;

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/gms/nearby/messages/MessagesClient;-><init>(Landroid/app/Activity;Lf/h/a/f/f/h/a;Lf/h/a/f/k/b/a;Lf/h/a/f/f/h/b$a;)V

    const/4 p2, 0x1

    iput p2, p0, Lf/h/a/f/k/b/e/i;->j:I

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object p2

    new-instance v0, Lf/h/a/f/k/b/e/z;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lf/h/a/f/k/b/e/z;-><init>(Landroid/app/Activity;Lf/h/a/f/k/b/e/i;Lf/h/a/f/k/b/e/q;)V

    invoke-virtual {p2, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method

.method public static j(Lf/h/a/f/k/b/e/i;Lcom/google/android/gms/tasks/TaskCompletionSource;)Lf/h/a/f/f/h/i/k;
    .locals 2

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/h/a/f/k/b/e/t;

    invoke-direct {v0, p1}, Lf/h/a/f/k/b/e/t;-><init>(Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    const-class p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lf/h/a/f/f/h/b;->e:Landroid/os/Looper;

    const-string v1, "Listener must not be null"

    invoke-static {v0, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Looper must not be null"

    invoke-static {p0, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Listener type must not be null"

    invoke-static {p1, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/a/f/f/h/i/k;

    invoke-direct {v1, p0, v0, p1}, Lf/h/a/f/f/h/i/k;-><init>(Landroid/os/Looper;Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final a()Lf/h/a/f/f/k/c$a;
    .locals 1

    invoke-super {p0}, Lf/h/a/f/f/h/b;->a()Lf/h/a/f/f/k/c$a;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/gms/nearby/messages/Message;Lcom/google/android/gms/nearby/messages/PublishOptions;)Lcom/google/android/gms/tasks/Task;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/nearby/messages/Message;",
            "Lcom/google/android/gms/nearby/messages/PublishOptions;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lf/h/a/f/k/b/e/i;->m(Ljava/lang/Object;)Lf/h/a/f/f/h/i/k;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/gms/nearby/messages/PublishOptions;->b:Lf/h/a/f/k/b/b;

    invoke-virtual {p0, v1}, Lf/h/a/f/k/b/e/i;->m(Ljava/lang/Object;)Lf/h/a/f/f/h/i/k;

    move-result-object v1

    new-instance v2, Lf/h/a/f/k/b/e/r;

    invoke-direct {v2, p0, v1, v0}, Lf/h/a/f/k/b/e/r;-><init>(Lf/h/a/f/k/b/e/i;Lf/h/a/f/f/h/i/k;Lf/h/a/f/f/h/i/k;)V

    new-instance v1, Lf/h/a/f/k/b/e/j;

    invoke-direct {v1, p0, p1, v2, p2}, Lf/h/a/f/k/b/e/j;-><init>(Lf/h/a/f/k/b/e/i;Lcom/google/android/gms/nearby/messages/Message;Lf/h/a/f/k/b/e/b0;Lcom/google/android/gms/nearby/messages/PublishOptions;)V

    new-instance p2, Lf/h/a/f/k/b/e/k;

    invoke-direct {p2, p1}, Lf/h/a/f/k/b/e/k;-><init>(Lcom/google/android/gms/nearby/messages/Message;)V

    invoke-virtual {p0, v0, v1, p2}, Lf/h/a/f/k/b/e/i;->k(Lf/h/a/f/f/h/i/k;Lf/h/a/f/k/b/e/a0;Lf/h/a/f/k/b/e/a0;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final g(Lcom/google/android/gms/nearby/messages/MessageListener;Lcom/google/android/gms/nearby/messages/SubscribeOptions;)Lcom/google/android/gms/tasks/Task;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/nearby/messages/MessageListener;",
            "Lcom/google/android/gms/nearby/messages/SubscribeOptions;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p2, Lcom/google/android/gms/nearby/messages/SubscribeOptions;->a:Lcom/google/android/gms/nearby/messages/Strategy;

    iget v0, v0, Lcom/google/android/gms/nearby/messages/Strategy;->k:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Strategy.setBackgroundScanMode() is only supported by background subscribe (the version which takes a PendingIntent)."

    invoke-static {v0, v1}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lf/h/a/f/k/b/e/i;->m(Ljava/lang/Object;)Lf/h/a/f/f/h/i/k;

    move-result-object p1

    iget-object v0, p2, Lcom/google/android/gms/nearby/messages/SubscribeOptions;->c:Lf/h/a/f/k/b/d;

    invoke-virtual {p0, v0}, Lf/h/a/f/k/b/e/i;->m(Ljava/lang/Object;)Lf/h/a/f/f/h/i/k;

    move-result-object v0

    new-instance v1, Lf/h/a/f/k/b/e/s;

    invoke-direct {v1, p0, v0, p1}, Lf/h/a/f/k/b/e/s;-><init>(Lf/h/a/f/k/b/e/i;Lf/h/a/f/f/h/i/k;Lf/h/a/f/f/h/i/k;)V

    new-instance v0, Lf/h/a/f/k/b/e/l;

    invoke-direct {v0, p0, p1, v1, p2}, Lf/h/a/f/k/b/e/l;-><init>(Lf/h/a/f/k/b/e/i;Lf/h/a/f/f/h/i/k;Lf/h/a/f/k/b/e/d0;Lcom/google/android/gms/nearby/messages/SubscribeOptions;)V

    new-instance p2, Lf/h/a/f/k/b/e/m;

    invoke-direct {p2, p1}, Lf/h/a/f/k/b/e/m;-><init>(Lf/h/a/f/f/h/i/k;)V

    invoke-virtual {p0, p1, v0, p2}, Lf/h/a/f/k/b/e/i;->k(Lf/h/a/f/f/h/i/k;Lf/h/a/f/k/b/e/a0;Lf/h/a/f/k/b/e/a0;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final h(Lcom/google/android/gms/nearby/messages/Message;)Lcom/google/android/gms/tasks/Task;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/nearby/messages/Message;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lf/h/a/f/k/b/e/i;->l(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final i(Lcom/google/android/gms/nearby/messages/MessageListener;)Lcom/google/android/gms/tasks/Task;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/nearby/messages/MessageListener;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lf/h/a/f/k/b/e/i;->l(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final k(Lf/h/a/f/f/h/i/k;Lf/h/a/f/k/b/e/a0;Lf/h/a/f/k/b/e/a0;)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/h/a/f/f/h/i/k<",
            "TT;>;",
            "Lf/h/a/f/k/b/e/a0;",
            "Lf/h/a/f/k/b/e/a0;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/a/f/k/b/e/v;

    invoke-direct {v0, p0, p1, p2}, Lf/h/a/f/k/b/e/v;-><init>(Lf/h/a/f/k/b/e/i;Lf/h/a/f/f/h/i/k;Lf/h/a/f/k/b/e/a0;)V

    new-instance p2, Lf/h/a/f/k/b/e/x;

    iget-object p1, p1, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-direct {p2, p0, p1, p3}, Lf/h/a/f/k/b/e/x;-><init>(Lf/h/a/f/k/b/e/i;Lf/h/a/f/f/h/i/k$a;Lf/h/a/f/k/b/e/a0;)V

    iget-object p1, v0, Lf/h/a/f/f/h/i/m;->a:Lf/h/a/f/f/h/i/k;

    iget-object p1, p1, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    const-string p3, "Listener has already been released."

    invoke-static {p1, p3}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p2, Lf/h/a/f/f/h/i/q;->a:Lf/h/a/f/f/h/i/k$a;

    invoke-static {p1, p3}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, v0, Lf/h/a/f/f/h/i/m;->a:Lf/h/a/f/f/h/i/k;

    iget-object p1, p1, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    iget-object p3, p2, Lf/h/a/f/f/h/i/q;->a:Lf/h/a/f/f/h/i/k$a;

    invoke-static {p1, p3}, Lf/g/j/k/a;->U(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const-string p3, "Listener registration and unregistration methods must be constructed with the same ListenerHolder."

    invoke-static {p1, p3}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    iget-object p1, p0, Lf/h/a/f/f/h/b;->i:Lf/h/a/f/f/h/i/g;

    sget-object p3, Lf/h/a/f/f/h/j;->d:Ljava/lang/Runnable;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v1}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v2, Lf/h/a/f/f/h/i/l0;

    new-instance v3, Lf/h/a/f/f/h/i/d0;

    invoke-direct {v3, v0, p2, p3}, Lf/h/a/f/f/h/i/d0;-><init>(Lf/h/a/f/f/h/i/m;Lf/h/a/f/f/h/i/q;Ljava/lang/Runnable;)V

    invoke-direct {v2, v3, v1}, Lf/h/a/f/f/h/i/l0;-><init>(Lf/h/a/f/f/h/i/d0;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    iget-object p2, p1, Lf/h/a/f/f/h/i/g;->n:Landroid/os/Handler;

    new-instance p3, Lf/h/a/f/f/h/i/c0;

    iget-object p1, p1, Lf/h/a/f/f/h/i/g;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result p1

    invoke-direct {p3, v2, p1, p0}, Lf/h/a/f/f/h/i/c0;-><init>(Lf/h/a/f/f/h/i/s;ILf/h/a/f/f/h/b;)V

    const/16 p1, 0x8

    invoke-virtual {p2, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object p1, v1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object p1
.end method

.method public final l(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Listener must not be null"

    invoke-static {p1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Listener type must not be null"

    invoke-static {v1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Listener type must not be empty"

    invoke-static {v1, v2}, Lf/g/j/k/a;->n(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    new-instance v2, Lf/h/a/f/f/h/i/k$a;

    invoke-direct {v2, p1, v1}, Lf/h/a/f/f/h/i/k$a;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lf/h/a/f/f/h/b;->b(Lf/h/a/f/f/h/i/k$a;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v1, Lf/h/a/f/k/b/e/u;

    invoke-direct {v1, v0}, Lf/h/a/f/k/b/e/u;-><init>(Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    check-cast p1, Lf/h/a/f/p/b0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, v2, v1}, Lf/h/a/f/p/b0;->c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;

    iget-object p1, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object p1
.end method

.method public final m(Ljava/lang/Object;)Lf/h/a/f/f/h/i/k;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lf/h/a/f/f/h/i/k<",
            "TT;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/f/f/h/b;->e:Landroid/os/Looper;

    const-string v2, "Listener must not be null"

    invoke-static {p1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Looper must not be null"

    invoke-static {v1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "Listener type must not be null"

    invoke-static {v0, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/h/a/f/f/h/i/k;

    invoke-direct {v2, v1, p1, v0}, Lf/h/a/f/f/h/i/k;-><init>(Landroid/os/Looper;Ljava/lang/Object;Ljava/lang/String;)V

    return-object v2
.end method
