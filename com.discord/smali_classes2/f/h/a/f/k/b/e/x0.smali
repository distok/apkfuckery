.class public abstract Lf/h/a/f/k/b/e/x0;
.super Lf/h/a/f/i/k/b;

# interfaces
.implements Lf/h/a/f/k/b/e/w0;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.nearby.messages.internal.IPublishCallback"

    invoke-direct {p0, v0}, Lf/h/a/f/i/k/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final e(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 p2, 0x1

    if-ne p1, p2, :cond_1

    move-object p1, p0

    check-cast p1, Lf/h/a/f/k/b/e/r;

    iget-object p3, p1, Lf/h/a/f/k/b/e/r;->d:Lf/h/a/f/k/b/e/i;

    iget-object p4, p1, Lf/h/a/f/k/b/e/r;->c:Lf/h/a/f/f/h/i/k;

    iget-object p4, p4, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-virtual {p3, p4}, Lf/h/a/f/f/h/b;->b(Lf/h/a/f/f/h/i/k$a;)Lcom/google/android/gms/tasks/Task;

    iget-object p1, p1, Lf/h/a/f/k/b/e/b0;->a:Lf/h/a/f/f/h/i/k;

    if-eqz p1, :cond_0

    sget-object p3, Lf/h/a/f/k/b/e/b0;->b:Lf/h/a/f/i/k/l;

    invoke-virtual {p1, p3}, Lf/h/a/f/f/h/i/k;->a(Lf/h/a/f/f/h/i/k$b;)V

    :cond_0
    return p2

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
