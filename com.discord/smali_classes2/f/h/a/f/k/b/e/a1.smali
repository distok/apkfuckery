.class public abstract Lf/h/a/f/k/b/e/a1;
.super Lf/h/a/f/i/k/b;

# interfaces
.implements Lf/h/a/f/k/b/e/z0;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.nearby.messages.internal.IStatusCallback"

    invoke-direct {p0, v0}, Lf/h/a/f/i/k/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final e(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 p3, 0x0

    const/4 p4, 0x1

    if-ne p1, p4, :cond_1

    sget p1, Lf/h/a/f/i/k/c;->a:I

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-eqz p1, :cond_0

    const/4 p3, 0x1

    :cond_0
    move-object p1, p0

    check-cast p1, Lf/h/a/f/i/k/m;

    iget-object p1, p1, Lf/h/a/f/i/k/m;->a:Lf/h/a/f/f/h/i/k;

    new-instance p2, Lf/h/a/f/i/k/n;

    invoke-direct {p2, p3}, Lf/h/a/f/i/k/n;-><init>(Z)V

    invoke-virtual {p1, p2}, Lf/h/a/f/f/h/i/k;->a(Lf/h/a/f/f/h/i/k$b;)V

    return p4

    :cond_1
    return p3
.end method
