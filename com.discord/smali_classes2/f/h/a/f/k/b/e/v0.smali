.class public final Lf/h/a/f/k/b/e/v0;
.super Lf/h/a/f/i/k/a;

# interfaces
.implements Lf/h/a/f/k/b/e/u0;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    const-string v0, "com.google.android.gms.nearby.messages.internal.INearbyMessagesService"

    invoke-direct {p0, p1, v0}, Lf/h/a/f/i/k/a;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final G(Lcom/google/android/gms/nearby/messages/internal/zzce;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/k/a;->e()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lf/h/a/f/i/k/c;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 p1, 0x2

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/i/k/a;->g(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final a0(Lcom/google/android/gms/nearby/messages/internal/zzj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/k/a;->e()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lf/h/a/f/i/k/c;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 p1, 0x9

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/i/k/a;->g(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final b0(Lcom/google/android/gms/nearby/messages/internal/zzcg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/k/a;->e()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lf/h/a/f/i/k/c;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 p1, 0x4

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/i/k/a;->g(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final l(Lcom/google/android/gms/nearby/messages/internal/zzbz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/k/a;->e()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lf/h/a/f/i/k/c;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/i/k/a;->g(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final p(Lcom/google/android/gms/nearby/messages/internal/zzcb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/k/a;->e()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lf/h/a/f/i/k/c;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 p1, 0x8

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/i/k/a;->g(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final y(Lcom/google/android/gms/nearby/messages/internal/SubscribeRequest;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/f/i/k/a;->e()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lf/h/a/f/i/k/c;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 p1, 0x3

    invoke-virtual {p0, p1, v0}, Lf/h/a/f/i/k/a;->g(ILandroid/os/Parcel;)V

    return-void
.end method
