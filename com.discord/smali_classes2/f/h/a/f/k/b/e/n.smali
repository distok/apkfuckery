.class public final synthetic Lf/h/a/f/k/b/e/n;
.super Ljava/lang/Object;

# interfaces
.implements Lf/h/a/f/k/b/e/a0;


# instance fields
.field public final a:Lf/h/a/f/f/h/i/k;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/k;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/k/b/e/n;->a:Lf/h/a/f/f/h/i/k;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/f/k/b/e/f;Lf/h/a/f/f/h/i/k;)V
    .locals 12

    iget-object v0, p0, Lf/h/a/f/k/b/e/n;->a:Lf/h/a/f/f/h/i/k;

    iget-object v1, p1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v2, v0, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-virtual {v1, v2}, Lf/h/a/f/i/k/o;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v2, v0, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    new-instance v3, Lf/h/a/f/i/k/m;

    invoke-direct {v3, v0}, Lf/h/a/f/i/k/m;-><init>(Lf/h/a/f/f/h/i/k;)V

    iget-object v1, v1, Lf/h/a/f/i/k/o;->a:Ljava/util/Map;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v1, Lcom/google/android/gms/nearby/messages/internal/zzcb;

    new-instance v7, Lf/h/a/f/i/k/j;

    invoke-direct {v7, p2}, Lf/h/a/f/i/k/j;-><init>(Lf/h/a/f/f/h/i/k;)V

    iget-object p2, p1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v0, v0, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-virtual {p2, v0}, Lf/h/a/f/i/k/o;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v8, p2

    check-cast v8, Landroid/os/IBinder;

    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v5, v1

    invoke-direct/range {v5 .. v11}, Lcom/google/android/gms/nearby/messages/internal/zzcb;-><init>(ILandroid/os/IBinder;Landroid/os/IBinder;ZLjava/lang/String;Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;)V

    const/4 p2, 0x1

    iput-boolean p2, v1, Lcom/google/android/gms/nearby/messages/internal/zzcb;->g:Z

    invoke-virtual {p1}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object p1

    check-cast p1, Lf/h/a/f/k/b/e/u0;

    invoke-interface {p1, v1}, Lf/h/a/f/k/b/e/u0;->p(Lcom/google/android/gms/nearby/messages/internal/zzcb;)V

    return-void
.end method
