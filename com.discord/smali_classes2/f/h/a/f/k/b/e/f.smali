.class public final Lf/h/a/f/k/b/e/f;
.super Lf/h/a/f/f/k/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/k/d<",
        "Lf/h/a/f/k/b/e/u0;",
        ">;"
    }
.end annotation


# instance fields
.field public final A:Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;

.field public final B:I

.field public final z:Lf/h/a/f/i/k/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/k/o<",
            "Lf/h/a/f/f/h/i/k$a;",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lf/h/a/f/f/h/c$a;Lf/h/a/f/f/h/c$b;Lf/h/a/f/f/k/c;Lf/h/a/f/k/b/a;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const/16 v3, 0x3e

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p5

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lf/h/a/f/f/k/d;-><init>(Landroid/content/Context;Landroid/os/Looper;ILf/h/a/f/f/k/c;Lf/h/a/f/f/h/i/f;Lf/h/a/f/f/h/i/l;)V

    new-instance p2, Lf/h/a/f/i/k/o;

    invoke-direct {p2}, Lf/h/a/f/i/k/o;-><init>()V

    iput-object p2, p0, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v1, p5, Lf/h/a/f/f/k/c;->e:Ljava/lang/String;

    instance-of p2, p1, Landroid/app/Activity;

    const/4 p3, 0x0

    const/4 p4, 0x2

    const/4 p5, 0x1

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    instance-of p2, p1, Landroid/app/Application;

    if-eqz p2, :cond_1

    const/4 p2, 0x2

    goto :goto_0

    :cond_1
    instance-of p2, p1, Landroid/app/Service;

    if-eqz p2, :cond_2

    const/4 p2, 0x3

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-eqz p6, :cond_3

    new-instance v6, Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, v6

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    iput-object v6, p0, Lf/h/a/f/k/b/e/f;->A:Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;

    iget p6, p6, Lf/h/a/f/k/b/a;->a:I

    goto :goto_1

    :cond_3
    new-instance p6, Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p6

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    iput-object p6, p0, Lf/h/a/f/k/b/e/f;->A:Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;

    const/4 p6, -0x1

    :goto_1
    iput p6, p0, Lf/h/a/f/k/b/e/f;->B:I

    if-ne p2, p5, :cond_5

    check-cast p1, Landroid/app/Activity;

    const-string p2, "NearbyMessagesClient"

    invoke-static {p2, p4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p4

    if-eqz p4, :cond_4

    new-array p4, p5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object p5

    aput-object p5, p4, p3

    const-string p3, "Registering ClientLifecycleSafetyNet\'s ActivityLifecycleCallbacks for %s"

    invoke-static {p3, p4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object p2

    new-instance p3, Lf/h/a/f/k/b/e/h;

    const/4 p4, 0x0

    invoke-direct {p3, p1, p0, p4}, Lf/h/a/f/k/b/e/h;-><init>(Landroid/app/Activity;Lf/h/a/f/k/b/e/f;Lf/h/a/f/k/b/e/g;)V

    invoke-virtual {p2, p3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_5
    return-void
.end method


# virtual methods
.method public final F(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-string v2, "NearbyMessagesClient"

    if-eq p1, v1, :cond_2

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v0

    const-string p1, "Received unknown/unforeseen client lifecycle event %d, can\'t do anything with it."

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    const-string v3, "CLIENT_DISCONNECTED"

    goto :goto_0

    :cond_2
    const-string v3, "ACTIVITY_STOPPED"

    :goto_0
    invoke-virtual {p0}, Lf/h/a/f/f/k/b;->j()Z

    move-result v4

    const/4 v5, 0x3

    if-eqz v4, :cond_4

    new-instance v4, Lcom/google/android/gms/nearby/messages/internal/zzj;

    invoke-direct {v4, p1}, Lcom/google/android/gms/nearby/messages/internal/zzj;-><init>(I)V

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_3

    new-array p1, v1, [Ljava/lang/Object;

    aput-object v3, p1, v0

    const-string v0, "Emitting client lifecycle event %s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p0}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object p1

    check-cast p1, Lf/h/a/f/k/b/e/u0;

    invoke-interface {p1, v4}, Lf/h/a/f/k/b/e/u0;->a0(Lcom/google/android/gms/nearby/messages/internal/zzj;)V

    return-void

    :cond_4
    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_5

    new-array p1, v1, [Ljava/lang/Object;

    aput-object v3, p1, v0

    const-string v0, "Failed to emit client lifecycle event %s due to GmsClient being disconnected"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-void
.end method

.method public final h()V
    .locals 4

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0}, Lf/h/a/f/k/b/e/f;->F(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "NearbyMessagesClient"

    invoke-static {v2, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v0, v3

    const-string v1, "Failed to emit CLIENT_DISCONNECTED from override of GmsClient#disconnect(): %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v0, v0, Lf/h/a/f/i/k/o;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-super {p0}, Lf/h/a/f/f/k/b;->h()V

    return-void
.end method

.method public final k()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/f/f/k/b;->c:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/k/a;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 1

    const v0, 0xbdfcb8

    return v0
.end method

.method public final synthetic r(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, "com.google.android.gms.nearby.messages.internal.INearbyMessagesService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lf/h/a/f/k/b/e/u0;

    if-eqz v1, :cond_1

    check-cast v0, Lf/h/a/f/k/b/e/u0;

    return-object v0

    :cond_1
    new-instance v0, Lf/h/a/f/k/b/e/v0;

    invoke-direct {v0, p1}, Lf/h/a/f/k/b/e/v0;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method

.method public final t()Landroid/os/Bundle;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget v1, p0, Lf/h/a/f/k/b/e/f;->B:I

    const-string v2, "NearbyPermissions"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lf/h/a/f/k/b/e/f;->A:Lcom/google/android/gms/nearby/messages/internal/ClientAppContext;

    const-string v2, "ClientAppContext"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string v0, "com.google.android.gms.nearby.messages.internal.INearbyMessagesService"

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string v0, "com.google.android.gms.nearby.messages.service.NearbyMessagesService.START"

    return-object v0
.end method
