.class public final synthetic Lf/h/a/f/k/b/e/m;
.super Ljava/lang/Object;

# interfaces
.implements Lf/h/a/f/k/b/e/a0;


# instance fields
.field public final a:Lf/h/a/f/f/h/i/k;


# direct methods
.method public constructor <init>(Lf/h/a/f/f/h/i/k;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/k/b/e/m;->a:Lf/h/a/f/f/h/i/k;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/f/k/b/e/f;Lf/h/a/f/f/h/i/k;)V
    .locals 11

    iget-object v0, p0, Lf/h/a/f/k/b/e/m;->a:Lf/h/a/f/f/h/i/k;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lf/h/a/f/i/k/j;

    invoke-direct {v4, p2}, Lf/h/a/f/i/k/j;-><init>(Lf/h/a/f/f/h/i/k;)V

    iget-object p2, p1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v1, v0, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-virtual {p2, v1}, Lf/h/a/f/i/k/o;->a(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    new-instance p1, Lcom/google/android/gms/common/api/Status;

    const/4 p2, 0x0

    const/4 v0, 0x0

    invoke-direct {p1, p2, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, p1}, Lf/h/a/f/i/k/j;->g(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/google/android/gms/nearby/messages/internal/zzcg;

    iget-object v1, p1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v2, v0, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-virtual {v1, v2}, Lf/h/a/f/i/k/o;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/os/IBinder;

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p2

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/nearby/messages/internal/zzcg;-><init>(ILandroid/os/IBinder;Landroid/os/IBinder;Landroid/app/PendingIntent;ILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/nearby/messages/internal/ClientAppContext;)V

    invoke-virtual {p1}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lf/h/a/f/k/b/e/u0;

    invoke-interface {v1, p2}, Lf/h/a/f/k/b/e/u0;->b0(Lcom/google/android/gms/nearby/messages/internal/zzcg;)V

    iget-object p1, p1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object p2, v0, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    iget-object p1, p1, Lf/h/a/f/i/k/o;->a:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method
