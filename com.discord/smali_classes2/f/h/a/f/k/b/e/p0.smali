.class public abstract Lf/h/a/f/k/b/e/p0;
.super Lf/h/a/f/i/k/b;

# interfaces
.implements Lf/h/a/f/k/b/e/o0;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.nearby.messages.internal.IMessageListener"

    invoke-direct {p0, v0}, Lf/h/a/f/i/k/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final e(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 p3, 0x1

    if-eq p1, p3, :cond_1

    const/4 p4, 0x2

    if-eq p1, p4, :cond_1

    const/4 p4, 0x4

    if-eq p1, p4, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    sget-object p1, Lcom/google/android/gms/nearby/messages/internal/Update;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object p1

    move-object p2, p0

    check-cast p2, Lf/h/a/f/i/k/h;

    iget-object p2, p2, Lf/h/a/f/i/k/h;->a:Lf/h/a/f/f/h/i/k;

    new-instance p4, Lf/h/a/f/i/k/i;

    invoke-direct {p4, p1}, Lf/h/a/f/i/k/i;-><init>(Ljava/util/List;)V

    invoke-virtual {p2, p4}, Lf/h/a/f/f/h/i/k;->a(Lf/h/a/f/f/h/i/k$b;)V

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/google/android/gms/nearby/messages/internal/zzaf;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, p1}, Lf/h/a/f/i/k/c;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/nearby/messages/internal/zzaf;

    :goto_0
    return p3
.end method
