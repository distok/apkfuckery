.class public final synthetic Lf/h/a/f/k/b/e/l;
.super Ljava/lang/Object;

# interfaces
.implements Lf/h/a/f/k/b/e/a0;


# instance fields
.field public final a:Lf/h/a/f/k/b/e/i;

.field public final b:Lf/h/a/f/f/h/i/k;

.field public final c:Lf/h/a/f/k/b/e/d0;

.field public final d:Lcom/google/android/gms/nearby/messages/SubscribeOptions;


# direct methods
.method public constructor <init>(Lf/h/a/f/k/b/e/i;Lf/h/a/f/f/h/i/k;Lf/h/a/f/k/b/e/d0;Lcom/google/android/gms/nearby/messages/SubscribeOptions;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/f/k/b/e/l;->a:Lf/h/a/f/k/b/e/i;

    iput-object p2, p0, Lf/h/a/f/k/b/e/l;->b:Lf/h/a/f/f/h/i/k;

    iput-object p3, p0, Lf/h/a/f/k/b/e/l;->c:Lf/h/a/f/k/b/e/d0;

    iput-object p4, p0, Lf/h/a/f/k/b/e/l;->d:Lcom/google/android/gms/nearby/messages/SubscribeOptions;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/f/k/b/e/f;Lf/h/a/f/f/h/i/k;)V
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lf/h/a/f/k/b/e/l;->a:Lf/h/a/f/k/b/e/i;

    iget-object v3, v0, Lf/h/a/f/k/b/e/l;->b:Lf/h/a/f/f/h/i/k;

    iget-object v10, v0, Lf/h/a/f/k/b/e/l;->c:Lf/h/a/f/k/b/e/d0;

    iget-object v5, v0, Lf/h/a/f/k/b/e/l;->d:Lcom/google/android/gms/nearby/messages/SubscribeOptions;

    iget v2, v2, Lf/h/a/f/k/b/e/i;->j:I

    iget-object v4, v1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v6, v3, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-virtual {v4, v6}, Lf/h/a/f/i/k/o;->a(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v6, v3, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    new-instance v7, Lf/h/a/f/i/k/h;

    invoke-direct {v7, v3}, Lf/h/a/f/i/k/h;-><init>(Lf/h/a/f/f/h/i/k;)V

    iget-object v4, v4, Lf/h/a/f/i/k/o;->a:Ljava/util/Map;

    new-instance v8, Ljava/lang/ref/WeakReference;

    invoke-direct {v8, v7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v4, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v15, Lcom/google/android/gms/nearby/messages/internal/SubscribeRequest;

    move-object v4, v15

    iget-object v6, v1, Lf/h/a/f/k/b/e/f;->z:Lf/h/a/f/i/k/o;

    iget-object v3, v3, Lf/h/a/f/f/h/i/k;->c:Lf/h/a/f/f/h/i/k$a;

    invoke-virtual {v6, v3}, Lf/h/a/f/i/k/o;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Landroid/os/IBinder;

    iget-object v7, v5, Lcom/google/android/gms/nearby/messages/SubscribeOptions;->a:Lcom/google/android/gms/nearby/messages/Strategy;

    new-instance v3, Lf/h/a/f/i/k/j;

    move-object v8, v3

    move-object/from16 v9, p2

    invoke-direct {v3, v9}, Lf/h/a/f/i/k/j;-><init>(Lf/h/a/f/f/h/i/k;)V

    iget-object v9, v5, Lcom/google/android/gms/nearby/messages/SubscribeOptions;->b:Lcom/google/android/gms/nearby/messages/MessageFilter;

    const/4 v14, 0x0

    iget-boolean v3, v5, Lcom/google/android/gms/nearby/messages/SubscribeOptions;->d:Z

    move/from16 v19, v3

    const/4 v5, 0x3

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v3, 0x0

    move-object/from16 v22, v15

    move v15, v3

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/4 v3, 0x0

    move-object/from16 v16, v10

    move-object v10, v3

    const/16 v20, 0x0

    move/from16 v21, v2

    invoke-direct/range {v4 .. v21}, Lcom/google/android/gms/nearby/messages/internal/SubscribeRequest;-><init>(ILandroid/os/IBinder;Lcom/google/android/gms/nearby/messages/Strategy;Landroid/os/IBinder;Lcom/google/android/gms/nearby/messages/MessageFilter;Landroid/app/PendingIntent;ILjava/lang/String;Ljava/lang/String;[BZLandroid/os/IBinder;ZLcom/google/android/gms/nearby/messages/internal/ClientAppContext;ZII)V

    invoke-virtual/range {p1 .. p1}, Lf/h/a/f/f/k/b;->v()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lf/h/a/f/k/b/e/u0;

    move-object/from16 v2, v22

    invoke-interface {v1, v2}, Lf/h/a/f/k/b/e/u0;->y(Lcom/google/android/gms/nearby/messages/internal/SubscribeRequest;)V

    return-void
.end method
