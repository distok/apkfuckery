.class public Lf/h/a/g/a/c;
.super Landroidx/coordinatorlayout/widget/CoordinatorLayout$Behavior;
.source "ViewOffsetBehavior.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Landroidx/coordinatorlayout/widget/CoordinatorLayout$Behavior<",
        "TV;>;"
    }
.end annotation


# instance fields
.field private tempLeftRightOffset:I

.field private tempTopBottomOffset:I

.field private viewOffsetHelper:Lf/h/a/g/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/coordinatorlayout/widget/CoordinatorLayout$Behavior;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/g/a/c;->tempTopBottomOffset:I

    iput v0, p0, Lf/h/a/g/a/c;->tempLeftRightOffset:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/coordinatorlayout/widget/CoordinatorLayout$Behavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/g/a/c;->tempTopBottomOffset:I

    iput p1, p0, Lf/h/a/g/a/c;->tempLeftRightOffset:I

    return-void
.end method


# virtual methods
.method public getLeftAndRightOffset()I
    .locals 1

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-eqz v0, :cond_0

    iget v0, v0, Lf/h/a/g/a/d;->e:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getTopAndBottomOffset()I
    .locals 1

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-eqz v0, :cond_0

    iget v0, v0, Lf/h/a/g/a/d;->d:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isHorizontalOffsetEnabled()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lf/h/a/g/a/d;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVerticalOffsetEnabled()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lf/h/a/g/a/d;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public layoutChild(Landroidx/coordinatorlayout/widget/CoordinatorLayout;Landroid/view/View;I)V
    .locals 0
    .param p1    # Landroidx/coordinatorlayout/widget/CoordinatorLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/coordinatorlayout/widget/CoordinatorLayout;",
            "TV;I)V"
        }
    .end annotation

    invoke-virtual {p1, p2, p3}, Landroidx/coordinatorlayout/widget/CoordinatorLayout;->onLayoutChild(Landroid/view/View;I)V

    return-void
.end method

.method public onLayoutChild(Landroidx/coordinatorlayout/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 1
    .param p1    # Landroidx/coordinatorlayout/widget/CoordinatorLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/coordinatorlayout/widget/CoordinatorLayout;",
            "TV;I)Z"
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Lf/h/a/g/a/c;->layoutChild(Landroidx/coordinatorlayout/widget/CoordinatorLayout;Landroid/view/View;I)V

    iget-object p1, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-nez p1, :cond_0

    new-instance p1, Lf/h/a/g/a/d;

    invoke-direct {p1, p2}, Lf/h/a/g/a/d;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    :cond_0
    iget-object p1, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    iget-object p2, p1, Lf/h/a/g/a/d;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result p2

    iput p2, p1, Lf/h/a/g/a/d;->b:I

    iget-object p2, p1, Lf/h/a/g/a/d;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result p2

    iput p2, p1, Lf/h/a/g/a/d;->c:I

    iget-object p1, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    invoke-virtual {p1}, Lf/h/a/g/a/d;->a()V

    iget p1, p0, Lf/h/a/g/a/c;->tempTopBottomOffset:I

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    iget-object p3, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    invoke-virtual {p3, p1}, Lf/h/a/g/a/d;->b(I)Z

    iput p2, p0, Lf/h/a/g/a/c;->tempTopBottomOffset:I

    :cond_1
    iget p1, p0, Lf/h/a/g/a/c;->tempLeftRightOffset:I

    if-eqz p1, :cond_3

    iget-object p3, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    iget-boolean v0, p3, Lf/h/a/g/a/d;->g:Z

    if-eqz v0, :cond_2

    iget v0, p3, Lf/h/a/g/a/d;->e:I

    if-eq v0, p1, :cond_2

    iput p1, p3, Lf/h/a/g/a/d;->e:I

    invoke-virtual {p3}, Lf/h/a/g/a/d;->a()V

    :cond_2
    iput p2, p0, Lf/h/a/g/a/c;->tempLeftRightOffset:I

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method public setHorizontalOffsetEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-eqz v0, :cond_0

    iput-boolean p1, v0, Lf/h/a/g/a/d;->g:Z

    :cond_0
    return-void
.end method

.method public setLeftAndRightOffset(I)Z
    .locals 3

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lf/h/a/g/a/d;->g:Z

    if-eqz v2, :cond_0

    iget v2, v0, Lf/h/a/g/a/d;->e:I

    if-eq v2, p1, :cond_0

    iput p1, v0, Lf/h/a/g/a/d;->e:I

    invoke-virtual {v0}, Lf/h/a/g/a/d;->a()V

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    iput p1, p0, Lf/h/a/g/a/c;->tempLeftRightOffset:I

    return v1
.end method

.method public setTopAndBottomOffset(I)Z
    .locals 1

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lf/h/a/g/a/d;->b(I)Z

    move-result p1

    return p1

    :cond_0
    iput p1, p0, Lf/h/a/g/a/c;->tempTopBottomOffset:I

    const/4 p1, 0x0

    return p1
.end method

.method public setVerticalOffsetEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lf/h/a/g/a/c;->viewOffsetHelper:Lf/h/a/g/a/d;

    if-eqz v0, :cond_0

    iput-boolean p1, v0, Lf/h/a/g/a/d;->f:Z

    :cond_0
    return-void
.end method
