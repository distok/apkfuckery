.class public Lf/h/a/g/h/a;
.super Ljava/lang/Object;
.source "SnackbarManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/g/h/a$c;,
        Lf/h/a/g/h/a$b;
    }
.end annotation


# static fields
.field public static e:Lf/h/a/g/h/a;


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final b:Landroid/os/Handler;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public c:Lf/h/a/g/h/a$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public d:Lf/h/a/g/h/a$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/h/a/g/h/a;->a:Ljava/lang/Object;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lf/h/a/g/h/a$a;

    invoke-direct {v2, p0}, Lf/h/a/g/h/a$a;-><init>(Lf/h/a/g/h/a;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lf/h/a/g/h/a;->b:Landroid/os/Handler;

    return-void
.end method

.method public static b()Lf/h/a/g/h/a;
    .locals 1

    sget-object v0, Lf/h/a/g/h/a;->e:Lf/h/a/g/h/a;

    if-nez v0, :cond_0

    new-instance v0, Lf/h/a/g/h/a;

    invoke-direct {v0}, Lf/h/a/g/h/a;-><init>()V

    sput-object v0, Lf/h/a/g/h/a;->e:Lf/h/a/g/h/a;

    :cond_0
    sget-object v0, Lf/h/a/g/h/a;->e:Lf/h/a/g/h/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lf/h/a/g/h/a$c;I)Z
    .locals 2
    .param p1    # Lf/h/a/g/h/a$c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p1, Lf/h/a/g/h/a$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/g/h/a$b;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/h/a/g/h/a;->b:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-interface {v0, p2}, Lf/h/a/g/h/a$b;->a(I)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final c(Lf/h/a/g/h/a$b;)Z
    .locals 3

    iget-object v0, p0, Lf/h/a/g/h/a;->c:Lf/h/a/g/h/a$c;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, v0, Lf/h/a/g/h/a$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public final d(Lf/h/a/g/h/a$b;)Z
    .locals 3

    iget-object v0, p0, Lf/h/a/g/h/a;->d:Lf/h/a/g/h/a$c;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, v0, Lf/h/a/g/h/a$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public e(Lf/h/a/g/h/a$b;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/g/h/a;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0, p1}, Lf/h/a/g/h/a;->c(Lf/h/a/g/h/a$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/a/g/h/a;->c:Lf/h/a/g/h/a$c;

    iget-boolean v1, p1, Lf/h/a/g/h/a$c;->c:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p1, Lf/h/a/g/h/a$c;->c:Z

    iget-object v1, p0, Lf/h/a/g/h/a;->b:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public f(Lf/h/a/g/h/a$b;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/g/h/a;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0, p1}, Lf/h/a/g/h/a;->c(Lf/h/a/g/h/a$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/a/g/h/a;->c:Lf/h/a/g/h/a$c;

    iget-boolean v1, p1, Lf/h/a/g/h/a$c;->c:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p1, Lf/h/a/g/h/a$c;->c:Z

    invoke-virtual {p0, p1}, Lf/h/a/g/h/a;->g(Lf/h/a/g/h/a$c;)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final g(Lf/h/a/g/h/a$c;)V
    .locals 4
    .param p1    # Lf/h/a/g/h/a$c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p1, Lf/h/a/g/h/a$c;->b:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    const/16 v1, 0xabe

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    const/16 v0, 0x5dc

    goto :goto_0

    :cond_2
    const/16 v0, 0xabe

    :goto_0
    iget-object v1, p0, Lf/h/a/g/h/a;->b:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lf/h/a/g/h/a;->b:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    int-to-long v2, v0

    invoke-virtual {v1, p1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lf/h/a/g/h/a;->d:Lf/h/a/g/h/a$c;

    if-eqz v0, :cond_1

    iput-object v0, p0, Lf/h/a/g/h/a;->c:Lf/h/a/g/h/a$c;

    const/4 v1, 0x0

    iput-object v1, p0, Lf/h/a/g/h/a;->d:Lf/h/a/g/h/a$c;

    iget-object v0, v0, Lf/h/a/g/h/a$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/g/h/a$b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/h/a/g/h/a$b;->show()V

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lf/h/a/g/h/a;->c:Lf/h/a/g/h/a$c;

    :cond_1
    :goto_0
    return-void
.end method
