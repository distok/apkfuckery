.class public Lf/h/a/g/e/h;
.super Ljava/lang/Object;
.source "FloatingActionButtonImpl.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic d:Lf/h/a/g/e/f;


# direct methods
.method public constructor <init>(Lf/h/a/g/e/f;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/g/e/h;->d:Lf/h/a/g/e/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 3

    iget-object v0, p0, Lf/h/a/g/e/h;->d:Lf/h/a/g/e/f;

    iget-object v1, v0, Lf/h/a/g/e/f;->y:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getRotation()F

    move-result v1

    iget v2, v0, Lf/h/a/g/e/f;->r:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_0

    iput v1, v0, Lf/h/a/g/e/f;->r:F

    invoke-virtual {v0}, Lf/h/a/g/e/f;->v()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
