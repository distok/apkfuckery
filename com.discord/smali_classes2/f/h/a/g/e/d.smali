.class public Lf/h/a/g/e/d;
.super Landroid/animation/AnimatorListenerAdapter;
.source "FloatingActionButtonImpl.java"


# instance fields
.field public a:Z

.field public final synthetic b:Z

.field public final synthetic c:Lf/h/a/g/e/f$f;

.field public final synthetic d:Lf/h/a/g/e/f;


# direct methods
.method public constructor <init>(Lf/h/a/g/e/f;ZLf/h/a/g/e/f$f;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/g/e/d;->d:Lf/h/a/g/e/f;

    iput-boolean p2, p0, Lf/h/a/g/e/d;->b:Z

    iput-object p3, p0, Lf/h/a/g/e/d;->c:Lf/h/a/g/e/f$f;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/g/e/d;->a:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object p1, p0, Lf/h/a/g/e/d;->d:Lf/h/a/g/e/f;

    const/4 v0, 0x0

    iput v0, p1, Lf/h/a/g/e/f;->u:I

    const/4 v0, 0x0

    iput-object v0, p1, Lf/h/a/g/e/f;->o:Landroid/animation/Animator;

    iget-boolean v0, p0, Lf/h/a/g/e/d;->a:Z

    if-nez v0, :cond_1

    iget-object p1, p1, Lf/h/a/g/e/f;->y:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget-boolean v0, p0, Lf/h/a/g/e/d;->b:Z

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :cond_0
    const/4 v1, 0x4

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/android/material/internal/VisibilityAwareImageButton;->internalSetVisibility(IZ)V

    iget-object p1, p0, Lf/h/a/g/e/d;->c:Lf/h/a/g/e/f$f;

    if-eqz p1, :cond_1

    check-cast p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton$a;

    iget-object v0, p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton$a;->a:Lcom/google/android/material/floatingactionbutton/FloatingActionButton$OnVisibilityChangedListener;

    iget-object p1, p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton$a;->b:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton$OnVisibilityChangedListener;->onHidden(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;)V

    :cond_1
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    iget-object v0, p0, Lf/h/a/g/e/d;->d:Lf/h/a/g/e/f;

    iget-object v0, v0, Lf/h/a/g/e/f;->y:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iget-boolean v1, p0, Lf/h/a/g/e/d;->b:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/internal/VisibilityAwareImageButton;->internalSetVisibility(IZ)V

    iget-object v0, p0, Lf/h/a/g/e/d;->d:Lf/h/a/g/e/f;

    const/4 v1, 0x1

    iput v1, v0, Lf/h/a/g/e/f;->u:I

    iput-object p1, v0, Lf/h/a/g/e/f;->o:Landroid/animation/Animator;

    iput-boolean v2, p0, Lf/h/a/g/e/d;->a:Z

    return-void
.end method
