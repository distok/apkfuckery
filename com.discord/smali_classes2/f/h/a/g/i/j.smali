.class public Lf/h/a/g/i/j;
.super Ljava/lang/Object;
.source "DropdownMenuEndIconDelegate.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic d:Landroid/widget/AutoCompleteTextView;

.field public final synthetic e:Lf/h/a/g/i/h;


# direct methods
.method public constructor <init>(Lf/h/a/g/i/h;Landroid/widget/AutoCompleteTextView;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/g/i/j;->e:Lf/h/a/g/i/h;

    iput-object p2, p0, Lf/h/a/g/i/j;->d:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lf/h/a/g/i/j;->e:Lf/h/a/g/i/h;

    invoke-virtual {p1}, Lf/h/a/g/i/h;->i()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/a/g/i/j;->e:Lf/h/a/g/i/h;

    iput-boolean p2, p1, Lf/h/a/g/i/h;->i:Z

    :cond_0
    iget-object p1, p0, Lf/h/a/g/i/j;->e:Lf/h/a/g/i/h;

    iget-object v0, p0, Lf/h/a/g/i/j;->d:Landroid/widget/AutoCompleteTextView;

    invoke-static {p1, v0}, Lf/h/a/g/i/h;->g(Lf/h/a/g/i/h;Landroid/widget/AutoCompleteTextView;)V

    :cond_1
    return p2
.end method
