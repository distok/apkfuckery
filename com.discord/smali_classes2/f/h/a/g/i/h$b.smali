.class public Lf/h/a/g/i/h$b;
.super Ljava/lang/Object;
.source "DropdownMenuEndIconDelegate.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/g/i/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/h/a/g/i/h;


# direct methods
.method public constructor <init>(Lf/h/a/g/i/h;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/g/i/h$b;->d:Lf/h/a/g/i/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0

    iget-object p1, p0, Lf/h/a/g/i/h$b;->d:Lf/h/a/g/i/h;

    iget-object p1, p1, Lf/h/a/g/i/m;->a:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1, p2}, Lcom/google/android/material/textfield/TextInputLayout;->setEndIconActivated(Z)V

    if-nez p2, :cond_0

    iget-object p1, p0, Lf/h/a/g/i/h$b;->d:Lf/h/a/g/i/h;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lf/h/a/g/i/h;->f(Lf/h/a/g/i/h;Z)V

    iget-object p1, p0, Lf/h/a/g/i/h$b;->d:Lf/h/a/g/i/h;

    iput-boolean p2, p1, Lf/h/a/g/i/h;->i:Z

    :cond_0
    return-void
.end method
