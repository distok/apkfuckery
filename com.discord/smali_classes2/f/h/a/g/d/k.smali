.class public Lf/h/a/g/d/k;
.super Ljava/lang/Object;
.source "TimeSource.java"


# static fields
.field public static final c:Lf/h/a/g/d/k;


# instance fields
.field public final a:Ljava/lang/Long;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/util/TimeZone;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/h/a/g/d/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lf/h/a/g/d/k;-><init>(Ljava/lang/Long;Ljava/util/TimeZone;)V

    sput-object v0, Lf/h/a/g/d/k;->c:Lf/h/a/g/d/k;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/util/TimeZone;)V
    .locals 0
    .param p1    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/TimeZone;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/g/d/k;->a:Ljava/lang/Long;

    iput-object p1, p0, Lf/h/a/g/d/k;->b:Ljava/util/TimeZone;

    return-void
.end method
