.class public Lf/h/a/e/c;
.super Ljava/lang/Object;
.source "FlexboxHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/e/c$b;,
        Lf/h/a/e/c$c;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/e/a;

.field public b:[Z

.field public c:[I
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public d:[J
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public e:[J
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/e/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    return-void
.end method


# virtual methods
.method public A(I)V
    .locals 14

    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getFlexItemCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getFlexDirection()I

    move-result v0

    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1}, Lf/h/a/e/a;->getAlignItems()I

    move-result v1

    const-string v2, "Invalid flex direction: "

    const/4 v3, 0x4

    const/4 v4, 0x1

    if-ne v1, v3, :cond_a

    iget-object v1, p0, Lf/h/a/e/c;->c:[I

    if-eqz v1, :cond_1

    aget p1, v1, p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1}, Lf/h/a/e/a;->getFlexLinesInternal()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    :goto_1
    if-ge p1, v5, :cond_f

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/a/e/b;

    iget v7, v6, Lf/h/a/e/b;->h:I

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v7, :cond_9

    iget v9, v6, Lf/h/a/e/b;->o:I

    add-int/2addr v9, v8

    iget-object v10, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v10}, Lf/h/a/e/a;->getFlexItemCount()I

    move-result v10

    if-lt v8, v10, :cond_2

    goto :goto_4

    :cond_2
    iget-object v10, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v10, v9}, Lf/h/a/e/a;->c(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_8

    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_3

    goto :goto_4

    :cond_3
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Lcom/google/android/flexbox/FlexItem;

    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->F()I

    move-result v12

    const/4 v13, -0x1

    if-eq v12, v13, :cond_4

    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->F()I

    move-result v11

    if-eq v11, v3, :cond_4

    goto :goto_4

    :cond_4
    if-eqz v0, :cond_7

    if-eq v0, v4, :cond_7

    const/4 v11, 0x2

    if-eq v0, v11, :cond_6

    const/4 v11, 0x3

    if-ne v0, v11, :cond_5

    goto :goto_3

    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-static {v2, v0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    :goto_3
    iget v11, v6, Lf/h/a/e/b;->g:I

    invoke-virtual {p0, v10, v11, v9}, Lf/h/a/e/c;->y(Landroid/view/View;II)V

    goto :goto_4

    :cond_7
    iget v11, v6, Lf/h/a/e/b;->g:I

    invoke-virtual {p0, v10, v11, v9}, Lf/h/a/e/c;->z(Landroid/view/View;II)V

    :cond_8
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_9
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_a
    iget-object p1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p1}, Lf/h/a/e/a;->getFlexLinesInternal()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_b
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/e/b;

    iget-object v3, v1, Lf/h/a/e/b;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    iget-object v6, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v6, v7}, Lf/h/a/e/a;->c(I)Landroid/view/View;

    move-result-object v6

    if-eqz v0, :cond_e

    if-eq v0, v4, :cond_e

    const/4 v7, 0x2

    const/4 v8, 0x3

    if-eq v0, v7, :cond_d

    if-ne v0, v8, :cond_c

    goto :goto_6

    :cond_c
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-static {v2, v0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_d
    :goto_6
    iget v7, v1, Lf/h/a/e/b;->g:I

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v6, v7, v5}, Lf/h/a/e/c;->y(Landroid/view/View;II)V

    goto :goto_5

    :cond_e
    iget v7, v1, Lf/h/a/e/b;->g:I

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v6, v7, v5}, Lf/h/a/e/c;->z(Landroid/view/View;II)V

    goto :goto_5

    :cond_f
    return-void
.end method

.method public final B(IIILandroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lf/h/a/e/c;->d:[J

    const-wide v1, 0xffffffffL

    const/16 v3, 0x20

    if-eqz v0, :cond_0

    int-to-long v4, p3

    shl-long/2addr v4, v3

    int-to-long p2, p2

    and-long/2addr p2, v1

    or-long/2addr p2, v4

    aput-wide p2, v0, p1

    :cond_0
    iget-object p2, p0, Lf/h/a/e/c;->e:[J

    if-eqz p2, :cond_1

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    int-to-long v4, p4

    shl-long v3, v4, v3

    int-to-long p3, p3

    and-long/2addr p3, v1

    or-long/2addr p3, v3

    aput-wide p3, p2, p1

    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;Lf/h/a/e/b;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/e/b;",
            ">;",
            "Lf/h/a/e/b;",
            "II)V"
        }
    .end annotation

    iput p4, p2, Lf/h/a/e/b;->m:I

    iget-object p4, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p4, p2}, Lf/h/a/e/a;->b(Lf/h/a/e/b;)V

    iput p3, p2, Lf/h/a/e/b;->p:I

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lf/h/a/e/c$b;IIIIILjava/util/List;)V
    .locals 23
    .param p7    # Ljava/util/List;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/e/c$b;",
            "IIIII",
            "Ljava/util/List<",
            "Lf/h/a/e/b;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p6

    iget-object v5, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v5}, Lf/h/a/e/a;->i()Z

    move-result v5

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    if-nez p7, :cond_0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_0
    move-object/from16 v8, p7

    :goto_0
    iput-object v8, v1, Lf/h/a/e/c$b;->a:Ljava/util/List;

    const/4 v9, -0x1

    if-ne v4, v9, :cond_1

    const/4 v9, 0x1

    goto :goto_1

    :cond_1
    const/4 v9, 0x0

    :goto_1
    if-eqz v5, :cond_2

    iget-object v10, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v10}, Lf/h/a/e/a;->getPaddingStart()I

    move-result v10

    goto :goto_2

    :cond_2
    iget-object v10, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v10}, Lf/h/a/e/a;->getPaddingTop()I

    move-result v10

    :goto_2
    if-eqz v5, :cond_3

    iget-object v11, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v11}, Lf/h/a/e/a;->getPaddingEnd()I

    move-result v11

    goto :goto_3

    :cond_3
    iget-object v11, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v11}, Lf/h/a/e/a;->getPaddingBottom()I

    move-result v11

    :goto_3
    if-eqz v5, :cond_4

    iget-object v12, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v12}, Lf/h/a/e/a;->getPaddingTop()I

    move-result v12

    goto :goto_4

    :cond_4
    iget-object v12, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v12}, Lf/h/a/e/a;->getPaddingStart()I

    move-result v12

    :goto_4
    if-eqz v5, :cond_5

    iget-object v13, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v13}, Lf/h/a/e/a;->getPaddingBottom()I

    move-result v13

    goto :goto_5

    :cond_5
    iget-object v13, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v13}, Lf/h/a/e/a;->getPaddingEnd()I

    move-result v13

    :goto_5
    new-instance v14, Lf/h/a/e/b;

    invoke-direct {v14}, Lf/h/a/e/b;-><init>()V

    move/from16 v15, p5

    iput v15, v14, Lf/h/a/e/b;->o:I

    add-int/2addr v10, v11

    iput v10, v14, Lf/h/a/e/b;->e:I

    iget-object v11, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v11}, Lf/h/a/e/a;->getFlexItemCount()I

    move-result v11

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/high16 v19, -0x80000000

    move/from16 p5, v9

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    :goto_6
    if-ge v15, v11, :cond_24

    move/from16 v16, v1

    iget-object v1, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1, v15}, Lf/h/a/e/a;->c(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_7

    invoke-virtual {v0, v15, v11, v14}, Lf/h/a/e/c;->t(IILf/h/a/e/b;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0, v8, v14, v15, v9}, Lf/h/a/e/c;->a(Ljava/util/List;Lf/h/a/e/b;II)V

    :cond_6
    move/from16 v17, v4

    goto :goto_7

    :cond_7
    move/from16 v17, v4

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v3, 0x8

    if-ne v4, v3, :cond_9

    iget v1, v14, Lf/h/a/e/b;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v14, Lf/h/a/e/b;->i:I

    iget v1, v14, Lf/h/a/e/b;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v14, Lf/h/a/e/b;->h:I

    invoke-virtual {v0, v15, v11, v14}, Lf/h/a/e/c;->t(IILf/h/a/e/b;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0, v8, v14, v15, v9}, Lf/h/a/e/c;->a(Ljava/util/List;Lf/h/a/e/b;II)V

    :cond_8
    :goto_7
    move/from16 v3, p5

    move/from16 v20, v6

    move/from16 v18, v7

    move-object v7, v8

    move v1, v11

    move/from16 v4, v17

    move/from16 v8, p3

    move/from16 v11, p4

    move/from16 v6, p6

    goto/16 :goto_1a

    :cond_9
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/flexbox/FlexItem;

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->F()I

    move-result v4

    move/from16 p7, v11

    const/4 v11, 0x4

    if-ne v4, v11, :cond_a

    iget-object v4, v14, Lf/h/a/e/b;->n:Ljava/util/List;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    if-eqz v5, :cond_b

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result v4

    goto :goto_8

    :cond_b
    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result v4

    :goto_8
    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->g0()F

    move-result v11

    const/high16 v18, -0x40800000    # -1.0f

    cmpl-float v11, v11, v18

    if-eqz v11, :cond_c

    const/high16 v11, 0x40000000    # 2.0f

    if-ne v6, v11, :cond_c

    int-to-float v4, v7

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->g0()F

    move-result v11

    mul-float v11, v11, v4

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v4

    :cond_c
    if-eqz v5, :cond_d

    iget-object v11, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    move/from16 v18, v7

    const/4 v7, 0x1

    invoke-virtual {v0, v3, v7}, Lf/h/a/e/c;->s(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v20

    add-int v20, v20, v10

    invoke-virtual {v0, v3, v7}, Lf/h/a/e/c;->q(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v7

    add-int v7, v7, v20

    invoke-interface {v11, v2, v7, v4}, Lf/h/a/e/a;->d(III)I

    move-result v4

    iget-object v7, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    add-int v11, v12, v13

    move/from16 v20, v6

    const/4 v6, 0x1

    invoke-virtual {v0, v3, v6}, Lf/h/a/e/c;->r(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v21

    add-int v21, v21, v11

    invoke-virtual {v0, v3, v6}, Lf/h/a/e/c;->p(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v6

    add-int v6, v6, v21

    add-int/2addr v6, v9

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result v11

    move-object/from16 v21, v8

    move/from16 v8, p3

    invoke-interface {v7, v8, v6, v11}, Lf/h/a/e/a;->h(III)I

    move-result v6

    invoke-virtual {v1, v4, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0, v15, v4, v6, v1}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    goto :goto_9

    :cond_d
    move/from16 v20, v6

    move/from16 v18, v7

    move-object/from16 v21, v8

    move/from16 v8, p3

    iget-object v6, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    add-int v7, v12, v13

    const/4 v11, 0x0

    invoke-virtual {v0, v3, v11}, Lf/h/a/e/c;->r(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v22

    add-int v22, v22, v7

    invoke-virtual {v0, v3, v11}, Lf/h/a/e/c;->p(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v7

    add-int v7, v7, v22

    add-int/2addr v7, v9

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result v11

    invoke-interface {v6, v8, v7, v11}, Lf/h/a/e/a;->d(III)I

    move-result v6

    iget-object v7, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    const/4 v11, 0x0

    invoke-virtual {v0, v3, v11}, Lf/h/a/e/c;->s(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v22

    add-int v22, v22, v10

    invoke-virtual {v0, v3, v11}, Lf/h/a/e/c;->q(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v11

    add-int v11, v11, v22

    invoke-interface {v7, v2, v11, v4}, Lf/h/a/e/a;->h(III)I

    move-result v4

    invoke-virtual {v1, v6, v4}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0, v15, v6, v4, v1}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    :goto_9
    iget-object v6, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v6, v15, v1}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    invoke-virtual {v0, v1, v15}, Lf/h/a/e/c;->c(Landroid/view/View;I)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v6

    move/from16 v7, v17

    invoke-static {v7, v6}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v6

    iget v7, v14, Lf/h/a/e/b;->e:I

    if-eqz v5, :cond_e

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    goto :goto_a

    :cond_e
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    :goto_a
    invoke-virtual {v0, v3, v5}, Lf/h/a/e/c;->s(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v17

    add-int v17, v17, v11

    invoke-virtual {v0, v3, v5}, Lf/h/a/e/c;->q(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v11

    add-int v11, v11, v17

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v17

    iget-object v2, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2}, Lf/h/a/e/a;->getFlexWrap()I

    move-result v2

    if-nez v2, :cond_f

    goto :goto_b

    :cond_f
    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->w0()Z

    move-result v2

    if-eqz v2, :cond_10

    move/from16 v22, v6

    move/from16 v6, v16

    move/from16 v2, v18

    goto :goto_d

    :cond_10
    if-nez v20, :cond_11

    :goto_b
    move/from16 v22, v6

    goto :goto_c

    :cond_11
    iget-object v2, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2}, Lf/h/a/e/a;->getMaxLine()I

    move-result v2

    move/from16 v22, v6

    const/4 v6, -0x1

    if-eq v2, v6, :cond_12

    add-int/lit8 v6, v17, 0x1

    if-gt v2, v6, :cond_12

    :goto_c
    move/from16 v6, v16

    move/from16 v2, v18

    goto :goto_e

    :cond_12
    iget-object v2, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    move/from16 v6, v16

    invoke-interface {v2, v1, v15, v6}, Lf/h/a/e/a;->g(Landroid/view/View;II)I

    move-result v2

    if-lez v2, :cond_13

    add-int/2addr v11, v2

    :cond_13
    add-int/2addr v7, v11

    move/from16 v2, v18

    if-ge v2, v7, :cond_14

    :goto_d
    const/4 v7, 0x1

    goto :goto_f

    :cond_14
    :goto_e
    const/4 v7, 0x0

    :goto_f
    if-eqz v7, :cond_19

    invoke-virtual {v14}, Lf/h/a/e/b;->a()I

    move-result v6

    if-lez v6, :cond_16

    if-lez v15, :cond_15

    add-int/lit8 v6, v15, -0x1

    goto :goto_10

    :cond_15
    const/4 v6, 0x0

    :goto_10
    move-object/from16 v7, v21

    invoke-virtual {v0, v7, v14, v6, v9}, Lf/h/a/e/c;->a(Ljava/util/List;Lf/h/a/e/b;II)V

    iget v6, v14, Lf/h/a/e/b;->g:I

    add-int/2addr v9, v6

    goto :goto_11

    :cond_16
    move-object/from16 v7, v21

    :goto_11
    if-eqz v5, :cond_17

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result v6

    const/4 v11, -0x1

    if-ne v6, v11, :cond_18

    iget-object v6, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v6}, Lf/h/a/e/a;->getPaddingTop()I

    move-result v11

    iget-object v14, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v14}, Lf/h/a/e/a;->getPaddingBottom()I

    move-result v14

    add-int/2addr v14, v11

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v11

    add-int/2addr v11, v14

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v14

    add-int/2addr v14, v11

    add-int/2addr v14, v9

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result v11

    invoke-interface {v6, v8, v14, v11}, Lf/h/a/e/a;->h(III)I

    move-result v6

    invoke-virtual {v1, v4, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0, v1, v15}, Lf/h/a/e/c;->c(Landroid/view/View;I)V

    goto :goto_12

    :cond_17
    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result v6

    const/4 v11, -0x1

    if-ne v6, v11, :cond_18

    iget-object v6, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v6}, Lf/h/a/e/a;->getPaddingLeft()I

    move-result v11

    iget-object v14, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v14}, Lf/h/a/e/a;->getPaddingRight()I

    move-result v14

    add-int/2addr v14, v11

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result v11

    add-int/2addr v11, v14

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result v14

    add-int/2addr v14, v11

    add-int/2addr v14, v9

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result v11

    invoke-interface {v6, v8, v14, v11}, Lf/h/a/e/a;->d(III)I

    move-result v6

    invoke-virtual {v1, v6, v4}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0, v1, v15}, Lf/h/a/e/c;->c(Landroid/view/View;I)V

    :cond_18
    :goto_12
    new-instance v14, Lf/h/a/e/b;

    invoke-direct {v14}, Lf/h/a/e/b;-><init>()V

    const/4 v4, 0x1

    iput v4, v14, Lf/h/a/e/b;->h:I

    iput v10, v14, Lf/h/a/e/b;->e:I

    iput v15, v14, Lf/h/a/e/b;->o:I

    const/4 v4, 0x0

    const/high16 v19, -0x80000000

    const/high16 v6, -0x80000000

    goto :goto_13

    :cond_19
    move-object/from16 v7, v21

    iget v4, v14, Lf/h/a/e/b;->h:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v14, Lf/h/a/e/b;->h:I

    add-int/lit8 v4, v6, 0x1

    move/from16 v6, v19

    :goto_13
    iget-boolean v11, v14, Lf/h/a/e/b;->q:Z

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v16

    const/16 v17, 0x0

    cmpl-float v16, v16, v17

    if-eqz v16, :cond_1a

    const/16 v16, 0x1

    goto :goto_14

    :cond_1a
    const/16 v16, 0x0

    :goto_14
    or-int v11, v11, v16

    iput-boolean v11, v14, Lf/h/a/e/b;->q:Z

    iget-boolean v11, v14, Lf/h/a/e/b;->r:Z

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v16

    cmpl-float v16, v16, v17

    if-eqz v16, :cond_1b

    const/16 v16, 0x1

    goto :goto_15

    :cond_1b
    const/16 v16, 0x0

    :goto_15
    or-int v11, v11, v16

    iput-boolean v11, v14, Lf/h/a/e/b;->r:Z

    iget-object v11, v0, Lf/h/a/e/c;->c:[I

    if-eqz v11, :cond_1c

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    aput v16, v11, v15

    :cond_1c
    iget v11, v14, Lf/h/a/e/b;->e:I

    if-eqz v5, :cond_1d

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v16

    goto :goto_16

    :cond_1d
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    :goto_16
    invoke-virtual {v0, v3, v5}, Lf/h/a/e/c;->s(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v17

    add-int v17, v17, v16

    invoke-virtual {v0, v3, v5}, Lf/h/a/e/c;->q(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v16

    add-int v16, v16, v17

    add-int v11, v16, v11

    iput v11, v14, Lf/h/a/e/b;->e:I

    iget v11, v14, Lf/h/a/e/b;->j:F

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v16

    add-float v11, v16, v11

    iput v11, v14, Lf/h/a/e/b;->j:F

    iget v11, v14, Lf/h/a/e/b;->k:F

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v16

    add-float v11, v16, v11

    iput v11, v14, Lf/h/a/e/b;->k:F

    iget-object v11, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v11, v1, v15, v4, v14}, Lf/h/a/e/a;->a(Landroid/view/View;IILf/h/a/e/b;)V

    if-eqz v5, :cond_1e

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    goto :goto_17

    :cond_1e
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    :goto_17
    invoke-virtual {v0, v3, v5}, Lf/h/a/e/c;->r(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v16

    add-int v16, v16, v11

    invoke-virtual {v0, v3, v5}, Lf/h/a/e/c;->p(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v11

    add-int v11, v11, v16

    move/from16 v18, v2

    iget-object v2, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2, v1}, Lf/h/a/e/a;->j(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v11

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v6, v14, Lf/h/a/e/b;->g:I

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, v14, Lf/h/a/e/b;->g:I

    if-eqz v5, :cond_20

    iget-object v6, v0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v6}, Lf/h/a/e/a;->getFlexWrap()I

    move-result v6

    const/4 v11, 0x2

    if-eq v6, v11, :cond_1f

    iget v6, v14, Lf/h/a/e/b;->l:I

    invoke-virtual {v1}, Landroid/view/View;->getBaseline()I

    move-result v1

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v3

    add-int/2addr v3, v1

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v14, Lf/h/a/e/b;->l:I

    goto :goto_18

    :cond_1f
    iget v6, v14, Lf/h/a/e/b;->l:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-virtual {v1}, Landroid/view/View;->getBaseline()I

    move-result v1

    sub-int/2addr v11, v1

    invoke-interface {v3}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v1

    add-int/2addr v1, v11

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v14, Lf/h/a/e/b;->l:I

    :cond_20
    :goto_18
    move/from16 v1, p7

    invoke-virtual {v0, v15, v1, v14}, Lf/h/a/e/c;->t(IILf/h/a/e/b;)Z

    move-result v3

    if-eqz v3, :cond_21

    invoke-virtual {v0, v7, v14, v15, v9}, Lf/h/a/e/c;->a(Ljava/util/List;Lf/h/a/e/b;II)V

    iget v3, v14, Lf/h/a/e/b;->g:I

    add-int/2addr v9, v3

    :cond_21
    const/4 v3, -0x1

    move/from16 v6, p6

    if-eq v6, v3, :cond_22

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_22

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    add-int/2addr v11, v3

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/e/b;

    iget v3, v3, Lf/h/a/e/b;->p:I

    if-lt v3, v6, :cond_22

    if-lt v15, v6, :cond_22

    if-nez p5, :cond_22

    iget v3, v14, Lf/h/a/e/b;->g:I

    neg-int v9, v3

    const/4 v3, 0x1

    move/from16 v11, p4

    goto :goto_19

    :cond_22
    move/from16 v11, p4

    move/from16 v3, p5

    :goto_19
    if-le v9, v11, :cond_23

    if-eqz v3, :cond_23

    move-object/from16 v1, p1

    move/from16 v4, v22

    goto :goto_1b

    :cond_23
    move/from16 v19, v2

    move/from16 v16, v4

    move/from16 v4, v22

    :goto_1a
    add-int/lit8 v15, v15, 0x1

    move/from16 v2, p2

    move v11, v1

    move/from16 p5, v3

    move v3, v8

    move/from16 v1, v16

    move/from16 v6, v20

    move-object v8, v7

    move/from16 v7, v18

    goto/16 :goto_6

    :cond_24
    move-object/from16 v1, p1

    :goto_1b
    iput v4, v1, Lf/h/a/e/c$b;->b:I

    return-void
.end method

.method public final c(Landroid/view/View;I)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->O()I

    move-result v3

    const/4 v4, 0x1

    if-ge v1, v3, :cond_0

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->O()I

    move-result v1

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->J0()I

    move-result v3

    if-le v1, v3, :cond_1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->J0()I

    move-result v1

    :goto_0
    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->s0()I

    move-result v5

    if-ge v2, v5, :cond_2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->s0()I

    move-result v2

    goto :goto_2

    :cond_2
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->z0()I

    move-result v5

    if-le v2, v5, :cond_3

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->z0()I

    move-result v2

    goto :goto_2

    :cond_3
    move v4, v3

    :goto_2
    if-eqz v4, :cond_4

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0, p2, v1, v0, p1}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0, p2, p1}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    :cond_4
    return-void
.end method

.method public d(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/e/b;",
            ">;I)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/e/c;->c:[I

    aget v0, v0, p2

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-lt v2, v0, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/h/a/e/c;->c:[I

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    if-le p2, v0, :cond_2

    invoke-static {p1, v1}, Ljava/util/Arrays;->fill([II)V

    goto :goto_1

    :cond_2
    invoke-static {p1, p2, v0, v1}, Ljava/util/Arrays;->fill([IIII)V

    :goto_1
    iget-object p1, p0, Lf/h/a/e/c;->d:[J

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    const-wide/16 v1, 0x0

    if-le p2, v0, :cond_3

    invoke-static {p1, v1, v2}, Ljava/util/Arrays;->fill([JJ)V

    goto :goto_2

    :cond_3
    invoke-static {p1, p2, v0, v1, v2}, Ljava/util/Arrays;->fill([JIIJ)V

    :goto_2
    return-void
.end method

.method public final e(Ljava/util/List;II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/e/b;",
            ">;II)",
            "Ljava/util/List<",
            "Lf/h/a/e/b;",
            ">;"
        }
    .end annotation

    sub-int/2addr p2, p3

    div-int/lit8 p2, p2, 0x2

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lf/h/a/e/b;

    invoke-direct {v0}, Lf/h/a/e/b;-><init>()V

    iput p2, v0, Lf/h/a/e/b;->g:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_2

    if-nez v1, :cond_0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/e/b;

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object p3
.end method

.method public final f(I)Ljava/util/List;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lf/h/a/e/c$c;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    iget-object v2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2, v1}, Lf/h/a/e/a;->f(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/flexbox/FlexItem;

    new-instance v3, Lf/h/a/e/c$c;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lf/h/a/e/c$c;-><init>(Lf/h/a/e/c$a;)V

    invoke-interface {v2}, Lcom/google/android/flexbox/FlexItem;->getOrder()I

    move-result v2

    iput v2, v3, Lf/h/a/e/c$c;->e:I

    iput v1, v3, Lf/h/a/e/c$c;->d:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public g(III)V
    .locals 11

    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getFlexDirection()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Invalid flex direction: "

    invoke-static {p2, v0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    goto :goto_1

    :cond_2
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    move v10, p2

    move p2, p1

    move p1, v10

    :goto_1
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getFlexLinesInternal()Ljava/util/List;

    move-result-object v0

    const/high16 v4, 0x40000000    # 2.0f

    if-ne p2, v4, :cond_15

    iget-object p2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p2}, Lf/h/a/e/a;->getSumOfCrossSize()I

    move-result p2

    add-int/2addr p2, p3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x0

    if-ne v4, v3, :cond_3

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/e/b;

    sub-int/2addr p1, p3

    iput p1, p2, Lf/h/a/e/b;->g:I

    goto/16 :goto_8

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p3

    if-lt p3, v2, :cond_15

    iget-object p3, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p3}, Lf/h/a/e/a;->getAlignContent()I

    move-result p3

    if-eq p3, v3, :cond_14

    if-eq p3, v2, :cond_13

    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    if-eq p3, v1, :cond_c

    const/4 v1, 0x4

    if-eq p3, v1, :cond_9

    const/4 v1, 0x5

    if-eq p3, v1, :cond_4

    goto/16 :goto_8

    :cond_4
    if-lt p2, p1, :cond_5

    goto/16 :goto_8

    :cond_5
    sub-int/2addr p1, p2

    int-to-float p1, p1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p1, p2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x0

    :goto_2
    if-ge v5, p2, :cond_15

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/e/b;

    iget v2, v1, Lf/h/a/e/b;->g:I

    int-to-float v2, v2

    add-float/2addr v2, p1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v3

    if-ne v5, v7, :cond_6

    add-float/2addr v2, p3

    const/4 p3, 0x0

    :cond_6
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v8, v7

    sub-float/2addr v2, v8

    add-float/2addr v2, p3

    cmpl-float p3, v2, v6

    if-lez p3, :cond_7

    add-int/lit8 v7, v7, 0x1

    sub-float/2addr v2, v6

    goto :goto_3

    :cond_7
    cmpg-float p3, v2, v4

    if-gez p3, :cond_8

    add-int/lit8 v7, v7, -0x1

    add-float/2addr v2, v6

    :cond_8
    :goto_3
    move p3, v2

    iput v7, v1, Lf/h/a/e/b;->g:I

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_9
    if-lt p2, p1, :cond_a

    iget-object p3, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-virtual {p0, v0, p1, p2}, Lf/h/a/e/c;->e(Ljava/util/List;II)Ljava/util/List;

    move-result-object p1

    invoke-interface {p3, p1}, Lf/h/a/e/a;->setFlexLines(Ljava/util/List;)V

    goto/16 :goto_8

    :cond_a
    sub-int/2addr p1, p2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    mul-int/lit8 p2, p2, 0x2

    div-int/2addr p1, p2

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    new-instance p3, Lf/h/a/e/b;

    invoke-direct {p3}, Lf/h/a/e/b;-><init>()V

    iput p1, p3, Lf/h/a/e/b;->g:I

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/e/b;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_b
    iget-object p1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p1, p2}, Lf/h/a/e/a;->setFlexLines(Ljava/util/List;)V

    goto/16 :goto_8

    :cond_c
    if-lt p2, p1, :cond_d

    goto/16 :goto_8

    :cond_d
    sub-int/2addr p1, p2

    int-to-float p1, p1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v3

    int-to-float p2, p2

    div-float/2addr p1, p2

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p3

    const/4 v1, 0x0

    :goto_5
    if-ge v5, p3, :cond_12

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/a/e/b;

    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v3

    if-eq v5, v7, :cond_11

    new-instance v7, Lf/h/a/e/b;

    invoke-direct {v7}, Lf/h/a/e/b;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v2

    if-ne v5, v8, :cond_e

    add-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v7, Lf/h/a/e/b;->g:I

    const/4 v1, 0x0

    goto :goto_6

    :cond_e
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v7, Lf/h/a/e/b;->g:I

    :goto_6
    iget v8, v7, Lf/h/a/e/b;->g:I

    int-to-float v9, v8

    sub-float v9, p1, v9

    add-float/2addr v9, v1

    cmpl-float v1, v9, v6

    if-lez v1, :cond_f

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lf/h/a/e/b;->g:I

    sub-float/2addr v9, v6

    goto :goto_7

    :cond_f
    cmpg-float v1, v9, v4

    if-gez v1, :cond_10

    add-int/lit8 v8, v8, -0x1

    iput v8, v7, Lf/h/a/e/b;->g:I

    add-float/2addr v9, v6

    :cond_10
    :goto_7
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v9

    :cond_11
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_12
    iget-object p1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p1, p2}, Lf/h/a/e/a;->setFlexLines(Ljava/util/List;)V

    goto :goto_8

    :cond_13
    iget-object p3, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-virtual {p0, v0, p1, p2}, Lf/h/a/e/c;->e(Ljava/util/List;II)Ljava/util/List;

    move-result-object p1

    invoke-interface {p3, p1}, Lf/h/a/e/a;->setFlexLines(Ljava/util/List;)V

    goto :goto_8

    :cond_14
    sub-int/2addr p1, p2

    new-instance p2, Lf/h/a/e/b;

    invoke-direct {p2}, Lf/h/a/e/b;-><init>()V

    iput p1, p2, Lf/h/a/e/b;->g:I

    invoke-interface {v0, v5, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_15
    :goto_8
    return-void
.end method

.method public h(III)V
    .locals 11

    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getFlexItemCount()I

    move-result v0

    iget-object v1, p0, Lf/h/a/e/c;->b:[Z

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-nez v1, :cond_1

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    const/16 v0, 0xa

    :cond_0
    new-array v0, v0, [Z

    iput-object v0, p0, Lf/h/a/e/c;->b:[Z

    goto :goto_0

    :cond_1
    array-length v4, v1

    if-ge v4, v0, :cond_3

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    if-lt v1, v0, :cond_2

    move v0, v1

    :cond_2
    new-array v0, v0, [Z

    iput-object v0, p0, Lf/h/a/e/c;->b:[Z

    goto :goto_0

    :cond_3
    invoke-static {v1, v3}, Ljava/util/Arrays;->fill([ZZ)V

    :goto_0
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getFlexItemCount()I

    move-result v0

    if-lt p3, v0, :cond_4

    return-void

    :cond_4
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getFlexDirection()I

    move-result v0

    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1}, Lf/h/a/e/a;->getFlexDirection()I

    move-result v1

    const/high16 v4, 0x40000000    # 2.0f

    if-eqz v1, :cond_8

    const/4 v5, 0x1

    if-eq v1, v5, :cond_8

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    goto :goto_1

    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Invalid flex direction: "

    invoke-static {p2, v0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    :goto_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-ne v0, v4, :cond_7

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getLargestMainSize()I

    move-result v1

    :goto_2
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getPaddingTop()I

    move-result v0

    iget-object v2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2}, Lf/h/a/e/a;->getPaddingBottom()I

    move-result v2

    goto :goto_4

    :cond_8
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-ne v0, v4, :cond_9

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getLargestMainSize()I

    move-result v0

    move v1, v0

    :goto_3
    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getPaddingLeft()I

    move-result v0

    iget-object v2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2}, Lf/h/a/e/a;->getPaddingRight()I

    move-result v2

    :goto_4
    add-int/2addr v2, v0

    iget-object v0, p0, Lf/h/a/e/c;->c:[I

    if-eqz v0, :cond_a

    aget v3, v0, p3

    :cond_a
    iget-object p3, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p3}, Lf/h/a/e/a;->getFlexLinesInternal()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    :goto_5
    if-ge v3, v0, :cond_d

    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Lf/h/a/e/b;

    iget v4, v7, Lf/h/a/e/b;->e:I

    if-ge v4, v1, :cond_b

    iget-boolean v5, v7, Lf/h/a/e/b;->q:Z

    if-eqz v5, :cond_b

    const/4 v10, 0x0

    move-object v4, p0

    move v5, p1

    move v6, p2

    move v8, v1

    move v9, v2

    invoke-virtual/range {v4 .. v10}, Lf/h/a/e/c;->l(IILf/h/a/e/b;IIZ)V

    goto :goto_6

    :cond_b
    if-le v4, v1, :cond_c

    iget-boolean v4, v7, Lf/h/a/e/b;->r:Z

    if-eqz v4, :cond_c

    const/4 v10, 0x0

    move-object v4, p0

    move v5, p1

    move v6, p2

    move v8, v1

    move v9, v2

    invoke-virtual/range {v4 .. v10}, Lf/h/a/e/c;->w(IILf/h/a/e/b;IIZ)V

    :cond_c
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_d
    return-void
.end method

.method public i(I)V
    .locals 2

    iget-object v0, p0, Lf/h/a/e/c;->c:[I

    if-nez v0, :cond_1

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    const/16 p1, 0xa

    :cond_0
    new-array p1, p1, [I

    iput-object p1, p0, Lf/h/a/e/c;->c:[I

    goto :goto_0

    :cond_1
    array-length v1, v0

    if-ge v1, p1, :cond_3

    array-length v1, v0

    mul-int/lit8 v1, v1, 0x2

    if-lt v1, p1, :cond_2

    move p1, v1

    :cond_2
    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object p1

    iput-object p1, p0, Lf/h/a/e/c;->c:[I

    :cond_3
    :goto_0
    return-void
.end method

.method public j(I)V
    .locals 2

    iget-object v0, p0, Lf/h/a/e/c;->d:[J

    if-nez v0, :cond_1

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    const/16 p1, 0xa

    :cond_0
    new-array p1, p1, [J

    iput-object p1, p0, Lf/h/a/e/c;->d:[J

    goto :goto_0

    :cond_1
    array-length v1, v0

    if-ge v1, p1, :cond_3

    array-length v1, v0

    mul-int/lit8 v1, v1, 0x2

    if-lt v1, p1, :cond_2

    move p1, v1

    :cond_2
    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object p1

    iput-object p1, p0, Lf/h/a/e/c;->d:[J

    :cond_3
    :goto_0
    return-void
.end method

.method public k(I)V
    .locals 2

    iget-object v0, p0, Lf/h/a/e/c;->e:[J

    if-nez v0, :cond_1

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    const/16 p1, 0xa

    :cond_0
    new-array p1, p1, [J

    iput-object p1, p0, Lf/h/a/e/c;->e:[J

    goto :goto_0

    :cond_1
    array-length v1, v0

    if-ge v1, p1, :cond_3

    array-length v1, v0

    mul-int/lit8 v1, v1, 0x2

    if-lt v1, p1, :cond_2

    move p1, v1

    :cond_2
    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object p1

    iput-object p1, p0, Lf/h/a/e/c;->e:[J

    :cond_3
    :goto_0
    return-void
.end method

.method public final l(IILf/h/a/e/b;IIZ)V
    .locals 21

    move-object/from16 v7, p0

    move-object/from16 v3, p3

    move/from16 v4, p4

    iget v0, v3, Lf/h/a/e/b;->j:F

    const/4 v1, 0x0

    cmpg-float v2, v0, v1

    if-lez v2, :cond_15

    iget v2, v3, Lf/h/a/e/b;->e:I

    if-ge v4, v2, :cond_0

    goto/16 :goto_b

    :cond_0
    sub-int v5, v4, v2

    int-to-float v5, v5

    div-float/2addr v5, v0

    iget v0, v3, Lf/h/a/e/b;->f:I

    add-int v0, p5, v0

    iput v0, v3, Lf/h/a/e/b;->e:I

    if-nez p6, :cond_1

    const/high16 v0, -0x80000000

    iput v0, v3, Lf/h/a/e/b;->g:I

    :cond_1
    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_0
    iget v10, v3, Lf/h/a/e/b;->h:I

    if-ge v0, v10, :cond_14

    iget v10, v3, Lf/h/a/e/b;->o:I

    add-int/2addr v10, v0

    iget-object v11, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v11, v10}, Lf/h/a/e/a;->c(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_13

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    goto/16 :goto_9

    :cond_2
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Lcom/google/android/flexbox/FlexItem;

    iget-object v13, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v13}, Lf/h/a/e/a;->getFlexDirection()I

    move-result v13

    const-wide/high16 v18, -0x4010000000000000L    # -1.0

    const/4 v15, 0x1

    if-eqz v13, :cond_b

    if-ne v13, v15, :cond_3

    goto/16 :goto_4

    :cond_3
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    iget-object v14, v7, Lf/h/a/e/c;->e:[J

    if-eqz v14, :cond_4

    aget-wide v13, v14, v10

    invoke-virtual {v7, v13, v14}, Lf/h/a/e/c;->m(J)I

    move-result v13

    :cond_4
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    iget-object v15, v7, Lf/h/a/e/c;->e:[J

    if-eqz v15, :cond_5

    aget-wide v14, v15, v10

    long-to-int v14, v14

    :cond_5
    iget-object v15, v7, Lf/h/a/e/c;->b:[Z

    aget-boolean v15, v15, v10

    if-nez v15, :cond_a

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v15

    cmpl-float v15, v15, v1

    if-lez v15, :cond_a

    int-to-float v13, v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v14

    mul-float v14, v14, v5

    add-float/2addr v14, v13

    iget v13, v3, Lf/h/a/e/b;->h:I

    const/4 v15, 0x1

    sub-int/2addr v13, v15

    if-ne v0, v13, :cond_6

    add-float/2addr v14, v9

    const/4 v9, 0x0

    :cond_6
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    move-result v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->z0()I

    move-result v1

    if-le v13, v1, :cond_7

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->z0()I

    move-result v13

    iget-object v1, v7, Lf/h/a/e/c;->b:[Z

    aput-boolean v15, v1, v10

    iget v1, v3, Lf/h/a/e/b;->j:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v6

    sub-float/2addr v1, v6

    iput v1, v3, Lf/h/a/e/b;->j:F

    move v15, v2

    const/4 v6, 0x1

    goto :goto_2

    :cond_7
    int-to-float v1, v13

    sub-float/2addr v14, v1

    add-float/2addr v14, v9

    move v15, v2

    float-to-double v1, v14

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v9, v1, v16

    if-lez v9, :cond_8

    add-int/lit8 v13, v13, 0x1

    sub-double v1, v1, v16

    goto :goto_1

    :cond_8
    cmpg-double v9, v1, v18

    if-gez v9, :cond_9

    add-int/lit8 v13, v13, -0x1

    add-double v1, v1, v16

    :goto_1
    double-to-float v1, v1

    move v9, v1

    goto :goto_2

    :cond_9
    move v9, v14

    :goto_2
    iget v1, v3, Lf/h/a/e/b;->m:I

    move/from16 v2, p1

    invoke-virtual {v7, v2, v12, v1}, Lf/h/a/e/c;->o(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v1

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v1, v13}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    invoke-virtual {v7, v10, v1, v13, v11}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    iget-object v1, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1, v10, v11}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    move/from16 v13, v16

    goto :goto_3

    :cond_a
    move v15, v2

    move/from16 v2, p1

    :goto_3
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result v1

    add-int/2addr v1, v14

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result v10

    add-int/2addr v10, v1

    iget-object v1, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1, v11}, Lf/h/a/e/a;->j(Landroid/view/View;)I

    move-result v1

    add-int/2addr v1, v10

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v8, v3, Lf/h/a/e/b;->e:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v10

    add-int/2addr v10, v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v11

    add-int/2addr v11, v10

    add-int/2addr v11, v8

    iput v11, v3, Lf/h/a/e/b;->e:I

    move/from16 v13, p2

    move/from16 v20, v15

    goto/16 :goto_8

    :cond_b
    :goto_4
    move v15, v2

    move/from16 v2, p1

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v13, v7, Lf/h/a/e/c;->e:[J

    if-eqz v13, :cond_c

    aget-wide v1, v13, v10

    long-to-int v1, v1

    :cond_c
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v13, v7, Lf/h/a/e/c;->e:[J

    move/from16 v20, v15

    if-eqz v13, :cond_d

    aget-wide v14, v13, v10

    invoke-virtual {v7, v14, v15}, Lf/h/a/e/c;->m(J)I

    move-result v2

    :cond_d
    iget-object v13, v7, Lf/h/a/e/c;->b:[Z

    aget-boolean v13, v13, v10

    if-nez v13, :cond_12

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_12

    int-to-float v1, v1

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v2

    mul-float v2, v2, v5

    add-float/2addr v2, v1

    iget v1, v3, Lf/h/a/e/b;->h:I

    const/4 v13, 0x1

    sub-int/2addr v1, v13

    if-ne v0, v1, :cond_e

    add-float/2addr v2, v9

    const/4 v9, 0x0

    :cond_e
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->J0()I

    move-result v15

    if-le v1, v15, :cond_f

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->J0()I

    move-result v1

    iget-object v2, v7, Lf/h/a/e/c;->b:[Z

    aput-boolean v13, v2, v10

    iget v2, v3, Lf/h/a/e/b;->j:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->c0()F

    move-result v6

    sub-float/2addr v2, v6

    iput v2, v3, Lf/h/a/e/b;->j:F

    const/4 v6, 0x1

    goto :goto_6

    :cond_f
    int-to-float v13, v1

    sub-float/2addr v2, v13

    add-float/2addr v2, v9

    float-to-double v14, v2

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v9, v14, v16

    if-lez v9, :cond_10

    add-int/lit8 v1, v1, 0x1

    sub-double v14, v14, v16

    goto :goto_5

    :cond_10
    cmpg-double v9, v14, v18

    if-gez v9, :cond_11

    add-int/lit8 v1, v1, -0x1

    add-double v14, v14, v16

    :goto_5
    double-to-float v2, v14

    :cond_11
    move v9, v2

    :goto_6
    iget v2, v3, Lf/h/a/e/b;->m:I

    move/from16 v13, p2

    invoke-virtual {v7, v13, v12, v2}, Lf/h/a/e/c;->n(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v2

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v1, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v11, v1, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    invoke-virtual {v7, v10, v1, v2, v11}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    iget-object v1, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1, v10, v11}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    move v1, v14

    move v2, v15

    goto :goto_7

    :cond_12
    move/from16 v13, p2

    :goto_7
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v10

    add-int/2addr v10, v2

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v2

    add-int/2addr v2, v10

    iget-object v10, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v10, v11}, Lf/h/a/e/a;->j(Landroid/view/View;)I

    move-result v10

    add-int/2addr v10, v2

    invoke-static {v8, v10}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v8, v3, Lf/h/a/e/b;->e:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result v10

    add-int/2addr v10, v1

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result v1

    add-int/2addr v1, v10

    add-int/2addr v1, v8

    iput v1, v3, Lf/h/a/e/b;->e:I

    move v1, v2

    :goto_8
    iget v2, v3, Lf/h/a/e/b;->g:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v3, Lf/h/a/e/b;->g:I

    move v8, v1

    goto :goto_a

    :cond_13
    :goto_9
    move/from16 v13, p2

    move/from16 v20, v2

    :goto_a
    add-int/lit8 v0, v0, 0x1

    move/from16 v2, v20

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_14
    move/from16 v13, p2

    move/from16 v20, v2

    if-eqz v6, :cond_15

    iget v0, v3, Lf/h/a/e/b;->e:I

    move/from16 v1, v20

    if-eq v1, v0, :cond_15

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-virtual/range {v0 .. v6}, Lf/h/a/e/c;->l(IILf/h/a/e/b;IIZ)V

    :cond_15
    :goto_b
    return-void
.end method

.method public m(J)I
    .locals 1

    const/16 v0, 0x20

    shr-long/2addr p1, v0

    long-to-int p2, p1

    return p2
.end method

.method public final n(ILcom/google/android/flexbox/FlexItem;I)I
    .locals 3

    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2}, Lf/h/a/e/a;->getPaddingBottom()I

    move-result v2

    add-int/2addr v2, v1

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v1

    add-int/2addr v1, v2

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v2

    add-int/2addr v2, v1

    add-int/2addr v2, p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result p3

    invoke-interface {v0, p1, v2, p3}, Lf/h/a/e/a;->h(III)I

    move-result p1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->z0()I

    move-result v0

    if-le p3, v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->z0()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->s0()I

    move-result v0

    if-ge p3, v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->s0()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_1
    :goto_0
    return p1
.end method

.method public final o(ILcom/google/android/flexbox/FlexItem;I)I
    .locals 3

    iget-object v0, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0}, Lf/h/a/e/a;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v2}, Lf/h/a/e/a;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, v1

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result v1

    add-int/2addr v1, v2

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result v2

    add-int/2addr v2, v1

    add-int/2addr v2, p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result p3

    invoke-interface {v0, p1, v2, p3}, Lf/h/a/e/a;->d(III)I

    move-result p1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->J0()I

    move-result v0

    if-le p3, v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->J0()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->O()I

    move-result v0

    if-ge p3, v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->O()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_1
    :goto_0
    return p1
.end method

.method public final p(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result p1

    return p1
.end method

.method public final q(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p1

    return p1
.end method

.method public final r(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result p1

    return p1
.end method

.method public final s(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result p1

    return p1
.end method

.method public final t(IILf/h/a/e/b;)Z
    .locals 1

    const/4 v0, 0x1

    sub-int/2addr p2, v0

    if-ne p1, p2, :cond_0

    invoke-virtual {p3}, Lf/h/a/e/b;->a()I

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public u(Landroid/view/View;Lf/h/a/e/b;IIII)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1}, Lf/h/a/e/a;->getAlignItems()I

    move-result v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->F()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->F()I

    move-result v1

    :cond_0
    iget v2, p2, Lf/h/a/e/b;->g:I

    const/4 v3, 0x2

    if-eqz v1, :cond_7

    const/4 v4, 0x1

    if-eq v1, v4, :cond_5

    if-eq v1, v3, :cond_3

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 p2, 0x4

    if-eq v1, p2, :cond_7

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1}, Lf/h/a/e/a;->getFlexWrap()I

    move-result v1

    if-eq v1, v3, :cond_2

    iget p2, p2, Lf/h/a/e/b;->l:I

    invoke-virtual {p1}, Landroid/view/View;->getBaseline()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    add-int/2addr p4, p2

    add-int/2addr p6, p2

    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0

    :cond_2
    iget p2, p2, Lf/h/a/e/b;->l:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-virtual {p1}, Landroid/view/View;->getBaseline()I

    move-result v1

    add-int/2addr v1, p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p2

    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    sub-int/2addr p4, p2

    sub-int/2addr p6, p2

    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    sub-int/2addr v2, p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result p2

    add-int/2addr p2, v2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p6

    sub-int/2addr p2, p6

    div-int/2addr p2, v3

    iget-object p6, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p6}, Lf/h/a/e/a;->getFlexWrap()I

    move-result p6

    if-eq p6, v3, :cond_4

    add-int/2addr p4, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    add-int/2addr p2, p4

    invoke-virtual {p1, p3, p4, p5, p2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_4
    sub-int/2addr p4, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    add-int/2addr p2, p4

    invoke-virtual {p1, p3, p4, p5, p2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_5
    iget-object p2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p2}, Lf/h/a/e/a;->getFlexWrap()I

    move-result p2

    if-eq p2, v3, :cond_6

    add-int/2addr p4, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    sub-int p2, p4, p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p6

    sub-int/2addr p2, p6

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p6

    sub-int/2addr p4, p6

    invoke-virtual {p1, p3, p2, p5, p4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_6
    sub-int/2addr p4, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    add-int/2addr p2, p4

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result p4

    add-int/2addr p4, p2

    sub-int/2addr p6, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    add-int/2addr p2, p6

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result p6

    add-int/2addr p6, p2

    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_7
    iget-object p2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p2}, Lf/h/a/e/a;->getFlexWrap()I

    move-result p2

    if-eq p2, v3, :cond_8

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result p2

    add-int/2addr p2, p4

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result p4

    add-int/2addr p4, p6

    invoke-virtual {p1, p3, p2, p5, p4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_8
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p2

    sub-int/2addr p4, p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result p2

    sub-int/2addr p6, p2

    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    :goto_0
    return-void
.end method

.method public v(Landroid/view/View;Lf/h/a/e/b;ZIIII)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1}, Lf/h/a/e/a;->getAlignItems()I

    move-result v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->F()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->F()I

    move-result v1

    :cond_0
    iget p2, p2, Lf/h/a/e/b;->g:I

    if-eqz v1, :cond_5

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 p2, 0x3

    if-eq v1, p2, :cond_5

    const/4 p2, 0x4

    if-eq v1, p2, :cond_5

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v1

    add-int/2addr v1, p2

    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result p2

    sub-int/2addr v1, p2

    div-int/2addr v1, v2

    if-nez p3, :cond_2

    add-int/2addr p4, v1

    add-int/2addr p6, v1

    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_2
    sub-int/2addr p4, v1

    sub-int/2addr p6, v1

    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_3
    if-nez p3, :cond_4

    add-int/2addr p4, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    sub-int/2addr p4, p3

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result p3

    sub-int/2addr p4, p3

    add-int/2addr p6, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    sub-int/2addr p6, p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result p2

    sub-int/2addr p6, p2

    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_4
    sub-int/2addr p4, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    add-int/2addr p3, p4

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result p4

    add-int/2addr p4, p3

    sub-int/2addr p6, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    add-int/2addr p2, p6

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result p3

    add-int/2addr p3, p2

    invoke-virtual {p1, p4, p5, p3, p7}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_5
    if-nez p3, :cond_6

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result p2

    add-int/2addr p2, p4

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result p3

    add-int/2addr p3, p6

    invoke-virtual {p1, p2, p5, p3, p7}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_6
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result p2

    sub-int/2addr p4, p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result p2

    sub-int/2addr p6, p2

    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    :goto_0
    return-void
.end method

.method public final w(IILf/h/a/e/b;IIZ)V
    .locals 22

    move-object/from16 v7, p0

    move-object/from16 v3, p3

    move/from16 v4, p4

    iget v0, v3, Lf/h/a/e/b;->e:I

    iget v1, v3, Lf/h/a/e/b;->k:F

    const/4 v2, 0x0

    cmpg-float v5, v1, v2

    if-lez v5, :cond_15

    if-le v4, v0, :cond_0

    goto/16 :goto_b

    :cond_0
    sub-int v5, v0, v4

    int-to-float v5, v5

    div-float/2addr v5, v1

    iget v1, v3, Lf/h/a/e/b;->f:I

    add-int v1, p5, v1

    iput v1, v3, Lf/h/a/e/b;->e:I

    if-nez p6, :cond_1

    const/high16 v1, -0x80000000

    iput v1, v3, Lf/h/a/e/b;->g:I

    :cond_1
    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_0
    iget v10, v3, Lf/h/a/e/b;->h:I

    if-ge v1, v10, :cond_14

    iget v10, v3, Lf/h/a/e/b;->o:I

    add-int/2addr v10, v1

    iget-object v11, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v11, v10}, Lf/h/a/e/a;->c(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_13

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    goto/16 :goto_9

    :cond_2
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Lcom/google/android/flexbox/FlexItem;

    iget-object v13, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v13}, Lf/h/a/e/a;->getFlexDirection()I

    move-result v13

    const/high16 v16, 0x3f800000    # 1.0f

    const-wide/high16 v19, -0x4010000000000000L    # -1.0

    const/4 v15, 0x1

    if-eqz v13, :cond_b

    if-ne v13, v15, :cond_3

    goto/16 :goto_4

    :cond_3
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    iget-object v14, v7, Lf/h/a/e/c;->e:[J

    if-eqz v14, :cond_4

    aget-wide v13, v14, v10

    invoke-virtual {v7, v13, v14}, Lf/h/a/e/c;->m(J)I

    move-result v13

    :cond_4
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    iget-object v15, v7, Lf/h/a/e/c;->e:[J

    if-eqz v15, :cond_5

    aget-wide v14, v15, v10

    long-to-int v14, v14

    :cond_5
    iget-object v15, v7, Lf/h/a/e/c;->b:[Z

    aget-boolean v15, v15, v10

    if-nez v15, :cond_a

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v15

    cmpl-float v15, v15, v2

    if-lez v15, :cond_a

    int-to-float v13, v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v14

    mul-float v14, v14, v5

    sub-float/2addr v13, v14

    iget v14, v3, Lf/h/a/e/b;->h:I

    const/4 v15, 0x1

    sub-int/2addr v14, v15

    if-ne v1, v14, :cond_6

    add-float/2addr v13, v9

    const/4 v9, 0x0

    :cond_6
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v14

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->s0()I

    move-result v2

    if-ge v14, v2, :cond_7

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->s0()I

    move-result v2

    iget-object v6, v7, Lf/h/a/e/c;->b:[Z

    aput-boolean v15, v6, v10

    iget v6, v3, Lf/h/a/e/b;->k:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v13

    sub-float/2addr v6, v13

    iput v6, v3, Lf/h/a/e/b;->k:F

    move v15, v1

    move v14, v2

    const/4 v6, 0x1

    move v2, v0

    goto :goto_2

    :cond_7
    int-to-float v2, v14

    sub-float/2addr v13, v2

    add-float/2addr v13, v9

    move v2, v0

    move v15, v1

    float-to-double v0, v13

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    cmpl-double v9, v0, v17

    if-lez v9, :cond_8

    add-int/lit8 v14, v14, 0x1

    sub-float v13, v13, v16

    goto :goto_1

    :cond_8
    cmpg-double v9, v0, v19

    if-gez v9, :cond_9

    add-int/lit8 v14, v14, -0x1

    add-float v13, v13, v16

    :cond_9
    :goto_1
    move v9, v13

    :goto_2
    iget v0, v3, Lf/h/a/e/b;->m:I

    move/from16 v1, p1

    invoke-virtual {v7, v1, v12, v0}, Lf/h/a/e/c;->o(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v0

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v14, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v0, v13}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    invoke-virtual {v7, v10, v0, v13, v11}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    iget-object v0, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0, v10, v11}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    move/from16 v13, v16

    goto :goto_3

    :cond_a
    move v2, v0

    move v15, v1

    move/from16 v1, p1

    :goto_3
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result v0

    add-int/2addr v0, v14

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result v10

    add-int/2addr v10, v0

    iget-object v0, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0, v11}, Lf/h/a/e/a;->j(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v10

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v8, v3, Lf/h/a/e/b;->e:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v10

    add-int/2addr v10, v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v11

    add-int/2addr v11, v10

    add-int/2addr v11, v8

    iput v11, v3, Lf/h/a/e/b;->e:I

    move/from16 v13, p2

    goto/16 :goto_8

    :cond_b
    :goto_4
    move v2, v0

    move v15, v1

    move/from16 v1, p1

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget-object v13, v7, Lf/h/a/e/c;->e:[J

    if-eqz v13, :cond_c

    aget-wide v0, v13, v10

    long-to-int v0, v0

    :cond_c
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v13, v7, Lf/h/a/e/c;->e:[J

    move/from16 v21, v15

    if-eqz v13, :cond_d

    aget-wide v14, v13, v10

    invoke-virtual {v7, v14, v15}, Lf/h/a/e/c;->m(J)I

    move-result v1

    :cond_d
    iget-object v13, v7, Lf/h/a/e/c;->b:[Z

    aget-boolean v13, v13, v10

    if-nez v13, :cond_12

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_12

    int-to-float v0, v0

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v1

    mul-float v1, v1, v5

    sub-float/2addr v0, v1

    iget v1, v3, Lf/h/a/e/b;->h:I

    const/4 v13, 0x1

    sub-int/2addr v1, v13

    move/from16 v15, v21

    if-ne v15, v1, :cond_e

    add-float/2addr v0, v9

    const/4 v9, 0x0

    :cond_e
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->O()I

    move-result v14

    if-ge v1, v14, :cond_f

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->O()I

    move-result v0

    iget-object v1, v7, Lf/h/a/e/c;->b:[Z

    aput-boolean v13, v1, v10

    iget v1, v3, Lf/h/a/e/b;->k:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->I()F

    move-result v6

    sub-float/2addr v1, v6

    iput v1, v3, Lf/h/a/e/b;->k:F

    const/4 v6, 0x1

    goto :goto_6

    :cond_f
    int-to-float v13, v1

    sub-float/2addr v0, v13

    add-float/2addr v0, v9

    float-to-double v13, v0

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    cmpl-double v9, v13, v17

    if-lez v9, :cond_10

    add-int/lit8 v1, v1, 0x1

    sub-float v0, v0, v16

    goto :goto_5

    :cond_10
    cmpg-double v9, v13, v19

    if-gez v9, :cond_11

    add-int/lit8 v1, v1, -0x1

    add-float v0, v0, v16

    :cond_11
    :goto_5
    move v9, v0

    move v0, v1

    :goto_6
    iget v1, v3, Lf/h/a/e/b;->m:I

    move/from16 v13, p2

    invoke-virtual {v7, v13, v12, v1}, Lf/h/a/e/c;->n(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v1

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v0, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v11, v0, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    invoke-virtual {v7, v10, v0, v1, v11}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    iget-object v0, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v0, v10, v11}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    move v0, v14

    move/from16 v1, v16

    goto :goto_7

    :cond_12
    move/from16 v13, p2

    move/from16 v15, v21

    :goto_7
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v10

    add-int/2addr v10, v1

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v1

    add-int/2addr v1, v10

    iget-object v10, v7, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v10, v11}, Lf/h/a/e/a;->j(Landroid/view/View;)I

    move-result v10

    add-int/2addr v10, v1

    invoke-static {v8, v10}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v8, v3, Lf/h/a/e/b;->e:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result v10

    add-int/2addr v10, v0

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result v0

    add-int/2addr v0, v10

    add-int/2addr v0, v8

    iput v0, v3, Lf/h/a/e/b;->e:I

    move v0, v1

    :goto_8
    iget v1, v3, Lf/h/a/e/b;->g:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v3, Lf/h/a/e/b;->g:I

    move v8, v0

    goto :goto_a

    :cond_13
    :goto_9
    move/from16 v13, p2

    move v2, v0

    move v15, v1

    :goto_a
    add-int/lit8 v1, v15, 0x1

    move v0, v2

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_14
    move/from16 v13, p2

    move v2, v0

    if-eqz v6, :cond_15

    iget v0, v3, Lf/h/a/e/b;->e:I

    if-eq v2, v0, :cond_15

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-virtual/range {v0 .. v6}, Lf/h/a/e/c;->w(IILf/h/a/e/b;IIZ)V

    :cond_15
    :goto_b
    return-void
.end method

.method public final x(ILjava/util/List;Landroid/util/SparseIntArray;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lf/h/a/e/c$c;",
            ">;",
            "Landroid/util/SparseIntArray;",
            ")[I"
        }
    .end annotation

    invoke-static {p2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {p3}, Landroid/util/SparseIntArray;->clear()V

    new-array p1, p1, [I

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/e/c$c;

    iget v2, v1, Lf/h/a/e/c$c;->d:I

    aput v2, p1, v0

    iget v1, v1, Lf/h/a/e/c$c;->e:I

    invoke-virtual {p3, v2, v1}, Landroid/util/SparseIntArray;->append(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method public final y(Landroid/view/View;II)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->V()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->q0()I

    move-result v1

    sub-int/2addr p2, v1

    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1, p1}, Lf/h/a/e/a;->j(Landroid/view/View;)I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->O()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->J0()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v0, p0, Lf/h/a/e/c;->e:[J

    if-eqz v0, :cond_0

    aget-wide v1, v0, p3

    invoke-virtual {p0, v1, v2}, Lf/h/a/e/c;->m(J)I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-virtual {p1, p2, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0, p3, p2, v0, p1}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    iget-object p2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p2, p3, p1}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    return-void
.end method

.method public final z(Landroid/view/View;II)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->Z()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->T()I

    move-result v1

    sub-int/2addr p2, v1

    iget-object v1, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {v1, p1}, Lf/h/a/e/a;->j(Landroid/view/View;)I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->s0()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->z0()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v0, p0, Lf/h/a/e/c;->e:[J

    if-eqz v0, :cond_0

    aget-wide v1, v0, p3

    long-to-int v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0, p3, v0, p2, p1}, Lf/h/a/e/c;->B(IIILandroid/view/View;)V

    iget-object p2, p0, Lf/h/a/e/c;->a:Lf/h/a/e/a;

    invoke-interface {p2, p3, p1}, Lf/h/a/e/a;->e(ILandroid/view/View;)V

    return-void
.end method
