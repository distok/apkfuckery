.class public interface abstract Lf/h/a/e/a;
.super Ljava/lang/Object;
.source "FlexContainer.java"


# virtual methods
.method public abstract a(Landroid/view/View;IILf/h/a/e/b;)V
.end method

.method public abstract b(Lf/h/a/e/b;)V
.end method

.method public abstract c(I)Landroid/view/View;
.end method

.method public abstract d(III)I
.end method

.method public abstract e(ILandroid/view/View;)V
.end method

.method public abstract f(I)Landroid/view/View;
.end method

.method public abstract g(Landroid/view/View;II)I
.end method

.method public abstract getAlignContent()I
.end method

.method public abstract getAlignItems()I
.end method

.method public abstract getFlexDirection()I
.end method

.method public abstract getFlexItemCount()I
.end method

.method public abstract getFlexLinesInternal()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/e/b;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFlexWrap()I
.end method

.method public abstract getLargestMainSize()I
.end method

.method public abstract getMaxLine()I
.end method

.method public abstract getPaddingBottom()I
.end method

.method public abstract getPaddingEnd()I
.end method

.method public abstract getPaddingLeft()I
.end method

.method public abstract getPaddingRight()I
.end method

.method public abstract getPaddingStart()I
.end method

.method public abstract getPaddingTop()I
.end method

.method public abstract getSumOfCrossSize()I
.end method

.method public abstract h(III)I
.end method

.method public abstract i()Z
.end method

.method public abstract j(Landroid/view/View;)I
.end method

.method public abstract setFlexLines(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/e/b;",
            ">;)V"
        }
    .end annotation
.end method
