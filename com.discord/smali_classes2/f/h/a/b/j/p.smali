.class public final Lf/h/a/b/j/p;
.super Ljava/lang/Object;
.source "TransportRuntime_Factory.java"

# interfaces
.implements Lw/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Object<",
        "Lf/h/a/b/j/n;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/e;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/l;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/e;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/l;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/p;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/p;->a:Lw/a/a;

    iput-object p2, p0, Lf/h/a/b/j/p;->b:Lw/a/a;

    iput-object p3, p0, Lf/h/a/b/j/p;->c:Lw/a/a;

    iput-object p4, p0, Lf/h/a/b/j/p;->d:Lw/a/a;

    iput-object p5, p0, Lf/h/a/b/j/p;->e:Lw/a/a;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lf/h/a/b/j/p;->a:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lf/h/a/b/j/v/a;

    iget-object v0, p0, Lf/h/a/b/j/p;->b:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lf/h/a/b/j/v/a;

    iget-object v0, p0, Lf/h/a/b/j/p;->c:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lf/h/a/b/j/t/e;

    iget-object v0, p0, Lf/h/a/b/j/p;->d:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lf/h/a/b/j/t/h/l;

    iget-object v0, p0, Lf/h/a/b/j/p;->e:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lf/h/a/b/j/t/h/p;

    new-instance v0, Lf/h/a/b/j/n;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lf/h/a/b/j/n;-><init>(Lf/h/a/b/j/v/a;Lf/h/a/b/j/v/a;Lf/h/a/b/j/t/e;Lf/h/a/b/j/t/h/l;Lf/h/a/b/j/t/h/p;)V

    return-object v0
.end method
