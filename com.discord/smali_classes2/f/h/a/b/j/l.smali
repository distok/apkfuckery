.class public final Lf/h/a/b/j/l;
.super Ljava/lang/Object;
.source "TransportImpl.java"

# interfaces
.implements Lf/h/a/b/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/h/a/b/f<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/b/j/i;

.field public final b:Ljava/lang/String;

.field public final c:Lf/h/a/b/b;

.field public final d:Lf/h/a/b/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/b/e<",
            "TT;[B>;"
        }
    .end annotation
.end field

.field public final e:Lf/h/a/b/j/m;


# direct methods
.method public constructor <init>(Lf/h/a/b/j/i;Ljava/lang/String;Lf/h/a/b/b;Lf/h/a/b/e;Lf/h/a/b/j/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/b/j/i;",
            "Ljava/lang/String;",
            "Lf/h/a/b/b;",
            "Lf/h/a/b/e<",
            "TT;[B>;",
            "Lf/h/a/b/j/m;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/l;->a:Lf/h/a/b/j/i;

    iput-object p2, p0, Lf/h/a/b/j/l;->b:Ljava/lang/String;

    iput-object p3, p0, Lf/h/a/b/j/l;->c:Lf/h/a/b/b;

    iput-object p4, p0, Lf/h/a/b/j/l;->d:Lf/h/a/b/e;

    iput-object p5, p0, Lf/h/a/b/j/l;->e:Lf/h/a/b/j/m;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/b/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/b/c<",
            "TT;>;)V"
        }
    .end annotation

    sget-object v0, Lf/h/a/b/j/k;->a:Lf/h/a/b/j/k;

    invoke-virtual {p0, p1, v0}, Lf/h/a/b/j/l;->b(Lf/h/a/b/c;Lf/h/a/b/h;)V

    return-void
.end method

.method public b(Lf/h/a/b/c;Lf/h/a/b/h;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/b/c<",
            "TT;>;",
            "Lf/h/a/b/h;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/b/j/l;->e:Lf/h/a/b/j/m;

    iget-object v1, p0, Lf/h/a/b/j/l;->a:Lf/h/a/b/j/i;

    const-string v2, "Null transportContext"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v2, "Null event"

    invoke-static {p1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v2, p0, Lf/h/a/b/j/l;->b:Ljava/lang/String;

    const-string v3, "Null transportName"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v3, p0, Lf/h/a/b/j/l;->d:Lf/h/a/b/e;

    const-string v4, "Null transformer"

    invoke-static {v3, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v4, p0, Lf/h/a/b/j/l;->c:Lf/h/a/b/b;

    const-string v5, "Null encoding"

    invoke-static {v4, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lf/h/a/b/j/n;

    iget-object v5, v0, Lf/h/a/b/j/n;->c:Lf/h/a/b/j/t/e;

    invoke-virtual {p1}, Lf/h/a/b/c;->c()Lf/h/a/b/d;

    move-result-object v6

    invoke-static {}, Lf/h/a/b/j/i;->a()Lf/h/a/b/j/i$a;

    move-result-object v7

    invoke-virtual {v1}, Lf/h/a/b/j/i;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lf/h/a/b/j/i$a;->b(Ljava/lang/String;)Lf/h/a/b/j/i$a;

    invoke-virtual {v7, v6}, Lf/h/a/b/j/i$a;->c(Lf/h/a/b/d;)Lf/h/a/b/j/i$a;

    invoke-virtual {v1}, Lf/h/a/b/j/i;->c()[B

    move-result-object v1

    check-cast v7, Lf/h/a/b/j/b$b;

    iput-object v1, v7, Lf/h/a/b/j/b$b;->b:[B

    invoke-virtual {v7}, Lf/h/a/b/j/b$b;->a()Lf/h/a/b/j/i;

    move-result-object v1

    new-instance v6, Lf/h/a/b/j/a$b;

    invoke-direct {v6}, Lf/h/a/b/j/a$b;-><init>()V

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, v6, Lf/h/a/b/j/a$b;->f:Ljava/util/Map;

    iget-object v7, v0, Lf/h/a/b/j/n;->a:Lf/h/a/b/j/v/a;

    invoke-interface {v7}, Lf/h/a/b/j/v/a;->a()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lf/h/a/b/j/a$b;->e(J)Lf/h/a/b/j/f$a;

    iget-object v0, v0, Lf/h/a/b/j/n;->b:Lf/h/a/b/j/v/a;

    invoke-interface {v0}, Lf/h/a/b/j/v/a;->a()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lf/h/a/b/j/a$b;->g(J)Lf/h/a/b/j/f$a;

    invoke-virtual {v6, v2}, Lf/h/a/b/j/a$b;->f(Ljava/lang/String;)Lf/h/a/b/j/f$a;

    new-instance v0, Lf/h/a/b/j/e;

    invoke-virtual {p1}, Lf/h/a/b/c;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3, v2}, Lf/h/a/b/e;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-direct {v0, v4, v2}, Lf/h/a/b/j/e;-><init>(Lf/h/a/b/b;[B)V

    invoke-virtual {v6, v0}, Lf/h/a/b/j/a$b;->d(Lf/h/a/b/j/e;)Lf/h/a/b/j/f$a;

    invoke-virtual {p1}, Lf/h/a/b/c;->a()Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, v6, Lf/h/a/b/j/a$b;->b:Ljava/lang/Integer;

    invoke-virtual {v6}, Lf/h/a/b/j/a$b;->b()Lf/h/a/b/j/f;

    move-result-object p1

    invoke-interface {v5, v1, p1, p2}, Lf/h/a/b/j/t/e;->a(Lf/h/a/b/j/i;Lf/h/a/b/j/f;Lf/h/a/b/h;)V

    return-void
.end method
