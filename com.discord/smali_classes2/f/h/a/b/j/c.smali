.class public final Lf/h/a/b/j/c;
.super Lf/h/a/b/j/o;
.source "DaggerTransportRuntimeComponent.java"


# instance fields
.field public d:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lw/a/a;

.field public g:Lw/a/a;

.field public h:Lw/a/a;

.field public i:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/t;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/f;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/r;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/c;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/l;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/p;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/a/b/j/c$a;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {p0 .. p0}, Lf/h/a/b/j/o;-><init>()V

    sget-object v2, Lf/h/a/b/j/g$a;->a:Lf/h/a/b/j/g;

    sget-object v3, Lf/h/a/b/j/r/a/a;->c:Ljava/lang/Object;

    instance-of v3, v2, Lf/h/a/b/j/r/a/a;

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    new-instance v3, Lf/h/a/b/j/r/a/a;

    invoke-direct {v3, v2}, Lf/h/a/b/j/r/a/a;-><init>(Lw/a/a;)V

    move-object v2, v3

    :goto_0
    iput-object v2, v0, Lf/h/a/b/j/c;->d:Lw/a/a;

    new-instance v2, Lf/h/a/b/j/r/a/b;

    const-string v3, "instance cannot be null"

    invoke-static {v1, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-direct {v2, v1}, Lf/h/a/b/j/r/a/b;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, Lf/h/a/b/j/c;->e:Lw/a/a;

    sget-object v1, Lf/h/a/b/j/v/b$a;->a:Lf/h/a/b/j/v/b;

    sget-object v3, Lf/h/a/b/j/v/c$a;->a:Lf/h/a/b/j/v/c;

    new-instance v4, Lf/h/a/b/j/q/j;

    invoke-direct {v4, v2, v1, v3}, Lf/h/a/b/j/q/j;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;)V

    iput-object v4, v0, Lf/h/a/b/j/c;->f:Lw/a/a;

    new-instance v5, Lf/h/a/b/j/q/l;

    invoke-direct {v5, v2, v4}, Lf/h/a/b/j/q/l;-><init>(Lw/a/a;Lw/a/a;)V

    instance-of v2, v5, Lf/h/a/b/j/r/a/a;

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    new-instance v2, Lf/h/a/b/j/r/a/a;

    invoke-direct {v2, v5}, Lf/h/a/b/j/r/a/a;-><init>(Lw/a/a;)V

    move-object v5, v2

    :goto_1
    iput-object v5, v0, Lf/h/a/b/j/c;->g:Lw/a/a;

    iget-object v2, v0, Lf/h/a/b/j/c;->e:Lw/a/a;

    sget-object v4, Lf/h/a/b/j/t/i/e$a;->a:Lf/h/a/b/j/t/i/e;

    sget-object v5, Lf/h/a/b/j/t/i/f$a;->a:Lf/h/a/b/j/t/i/f;

    new-instance v6, Lf/h/a/b/j/t/i/a0;

    invoke-direct {v6, v2, v4, v5}, Lf/h/a/b/j/t/i/a0;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;)V

    iput-object v6, v0, Lf/h/a/b/j/c;->h:Lw/a/a;

    sget-object v2, Lf/h/a/b/j/t/i/g$a;->a:Lf/h/a/b/j/t/i/g;

    new-instance v4, Lf/h/a/b/j/t/i/u;

    invoke-direct {v4, v1, v3, v2, v6}, Lf/h/a/b/j/t/i/u;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V

    instance-of v2, v4, Lf/h/a/b/j/r/a/a;

    if-eqz v2, :cond_2

    move-object v2, v4

    goto :goto_2

    :cond_2
    new-instance v2, Lf/h/a/b/j/r/a/a;

    invoke-direct {v2, v4}, Lf/h/a/b/j/r/a/a;-><init>(Lw/a/a;)V

    :goto_2
    iput-object v2, v0, Lf/h/a/b/j/c;->i:Lw/a/a;

    new-instance v4, Lf/h/a/b/j/t/f;

    invoke-direct {v4, v1}, Lf/h/a/b/j/t/f;-><init>(Lw/a/a;)V

    iput-object v4, v0, Lf/h/a/b/j/c;->j:Lw/a/a;

    iget-object v11, v0, Lf/h/a/b/j/c;->e:Lw/a/a;

    new-instance v12, Lf/h/a/b/j/t/g;

    invoke-direct {v12, v11, v2, v4, v3}, Lf/h/a/b/j/t/g;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V

    iput-object v12, v0, Lf/h/a/b/j/c;->k:Lw/a/a;

    iget-object v13, v0, Lf/h/a/b/j/c;->d:Lw/a/a;

    iget-object v14, v0, Lf/h/a/b/j/c;->g:Lw/a/a;

    new-instance v15, Lf/h/a/b/j/t/d;

    move-object v5, v15

    move-object v6, v13

    move-object v7, v14

    move-object v8, v12

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v5 .. v10}, Lf/h/a/b/j/t/d;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V

    iput-object v15, v0, Lf/h/a/b/j/c;->l:Lw/a/a;

    new-instance v10, Lf/h/a/b/j/t/h/m;

    move-object v4, v10

    move-object v5, v11

    move-object v6, v14

    move-object v7, v2

    move-object v9, v13

    move-object v14, v10

    move-object v10, v2

    move-object v11, v1

    invoke-direct/range {v4 .. v11}, Lf/h/a/b/j/t/h/m;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V

    iput-object v14, v0, Lf/h/a/b/j/c;->m:Lw/a/a;

    new-instance v9, Lf/h/a/b/j/t/h/q;

    invoke-direct {v9, v13, v2, v12, v2}, Lf/h/a/b/j/t/h/q;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V

    iput-object v9, v0, Lf/h/a/b/j/c;->n:Lw/a/a;

    new-instance v2, Lf/h/a/b/j/p;

    move-object v4, v2

    move-object v5, v1

    move-object v6, v3

    move-object v7, v15

    move-object v8, v14

    invoke-direct/range {v4 .. v9}, Lf/h/a/b/j/p;-><init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V

    instance-of v1, v2, Lf/h/a/b/j/r/a/a;

    if-eqz v1, :cond_3

    goto :goto_3

    :cond_3
    new-instance v1, Lf/h/a/b/j/r/a/a;

    invoke-direct {v1, v2}, Lf/h/a/b/j/r/a/a;-><init>(Lw/a/a;)V

    move-object v2, v1

    :goto_3
    iput-object v2, v0, Lf/h/a/b/j/c;->o:Lw/a/a;

    return-void
.end method
