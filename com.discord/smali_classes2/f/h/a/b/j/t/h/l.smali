.class public Lf/h/a/b/j/t/h/l;
.super Ljava/lang/Object;
.source "Uploader.java"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/a/b/j/q/e;

.field public final c:Lf/h/a/b/j/t/i/c;

.field public final d:Lf/h/a/b/j/t/h/r;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:Lf/h/a/b/j/u/a;

.field public final g:Lf/h/a/b/j/v/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/a/b/j/q/e;Lf/h/a/b/j/t/i/c;Lf/h/a/b/j/t/h/r;Ljava/util/concurrent/Executor;Lf/h/a/b/j/u/a;Lf/h/a/b/j/v/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/h/l;->a:Landroid/content/Context;

    iput-object p2, p0, Lf/h/a/b/j/t/h/l;->b:Lf/h/a/b/j/q/e;

    iput-object p3, p0, Lf/h/a/b/j/t/h/l;->c:Lf/h/a/b/j/t/i/c;

    iput-object p4, p0, Lf/h/a/b/j/t/h/l;->d:Lf/h/a/b/j/t/h/r;

    iput-object p5, p0, Lf/h/a/b/j/t/h/l;->e:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lf/h/a/b/j/t/h/l;->f:Lf/h/a/b/j/u/a;

    iput-object p7, p0, Lf/h/a/b/j/t/h/l;->g:Lf/h/a/b/j/v/a;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/b/j/i;I)V
    .locals 8

    iget-object v0, p0, Lf/h/a/b/j/t/h/l;->b:Lf/h/a/b/j/q/e;

    invoke-virtual {p1}, Lf/h/a/b/j/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lf/h/a/b/j/q/e;->get(Ljava/lang/String;)Lf/h/a/b/j/q/m;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/b/j/t/h/l;->f:Lf/h/a/b/j/u/a;

    new-instance v2, Lf/h/a/b/j/t/h/h;

    invoke-direct {v2, p0, p1}, Lf/h/a/b/j/t/h/h;-><init>(Lf/h/a/b/j/t/h/l;Lf/h/a/b/j/i;)V

    invoke-interface {v1, v2}, Lf/h/a/b/j/u/a;->a(Lf/h/a/b/j/u/a$a;)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljava/lang/Iterable;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    if-nez v0, :cond_1

    const-string v0, "Uploader"

    const-string v1, "Unknown backend for %s, deleting event batch for it..."

    invoke-static {v0, v1, p1}, Lf/g/j/k/a;->M(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {}, Lf/h/a/b/j/q/g;->a()Lf/h/a/b/j/q/g;

    move-result-object v0

    :goto_0
    move-object v4, v0

    goto :goto_2

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/b/j/t/i/h;

    invoke-virtual {v3}, Lf/h/a/b/j/t/i/h;->a()Lf/h/a/b/j/f;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lf/h/a/b/j/i;->c()[B

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v3, :cond_3

    new-instance v3, Lf/h/a/b/j/q/a;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v2, v4}, Lf/h/a/b/j/q/a;-><init>(Ljava/lang/Iterable;[BLf/h/a/b/j/q/a$a;)V

    invoke-interface {v0, v3}, Lf/h/a/b/j/q/m;->a(Lf/h/a/b/j/q/f;)Lf/h/a/b/j/q/g;

    move-result-object v0

    goto :goto_0

    :goto_2
    iget-object v0, p0, Lf/h/a/b/j/t/h/l;->f:Lf/h/a/b/j/u/a;

    new-instance v1, Lf/h/a/b/j/t/h/i;

    move-object v2, v1

    move-object v3, p0

    move-object v6, p1

    move v7, p2

    invoke-direct/range {v2 .. v7}, Lf/h/a/b/j/t/h/i;-><init>(Lf/h/a/b/j/t/h/l;Lf/h/a/b/j/q/g;Ljava/lang/Iterable;Lf/h/a/b/j/i;I)V

    invoke-interface {v0, v1}, Lf/h/a/b/j/u/a;->a(Lf/h/a/b/j/u/a$a;)Ljava/lang/Object;

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Missing required properties:"

    const-string v0, ""

    invoke-static {p2, v0}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
