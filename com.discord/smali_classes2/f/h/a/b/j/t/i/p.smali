.class public final synthetic Lf/h/a/b/j/t/i/p;
.super Ljava/lang/Object;
.source "SQLiteEventStore.java"

# interfaces
.implements Lf/h/a/b/j/t/i/t$b;


# static fields
.field public static final a:Lf/h/a/b/j/t/i/p;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/b/j/t/i/p;

    invoke-direct {v0}, Lf/h/a/b/j/t/i/p;-><init>()V

    sput-object v0, Lf/h/a/b/j/t/i/p;->a:Lf/h/a/b/j/t/i/p;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Landroid/database/Cursor;

    sget-object v0, Lf/h/a/b/j/t/i/t;->h:Lf/h/a/b/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lf/h/a/b/j/i;->a()Lf/h/a/b/j/i$a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lf/h/a/b/j/i$a;->b(Ljava/lang/String;)Lf/h/a/b/j/i$a;

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lf/h/a/b/j/w/a;->b(I)Lf/h/a/b/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lf/h/a/b/j/i$a;->c(Lf/h/a/b/d;)Lf/h/a/b/j/i$a;

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    :goto_1
    check-cast v1, Lf/h/a/b/j/b$b;

    iput-object v2, v1, Lf/h/a/b/j/b$b;->b:[B

    invoke-virtual {v1}, Lf/h/a/b/j/b$b;->a()Lf/h/a/b/j/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method
