.class public final synthetic Lf/h/a/b/j/t/a;
.super Ljava/lang/Object;
.source "DefaultScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/b/j/t/c;

.field public final e:Lf/h/a/b/j/i;

.field public final f:Lf/h/a/b/h;

.field public final g:Lf/h/a/b/j/f;


# direct methods
.method public constructor <init>(Lf/h/a/b/j/t/c;Lf/h/a/b/j/i;Lf/h/a/b/h;Lf/h/a/b/j/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/a;->d:Lf/h/a/b/j/t/c;

    iput-object p2, p0, Lf/h/a/b/j/t/a;->e:Lf/h/a/b/j/i;

    iput-object p3, p0, Lf/h/a/b/j/t/a;->f:Lf/h/a/b/h;

    iput-object p4, p0, Lf/h/a/b/j/t/a;->g:Lf/h/a/b/j/f;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/h/a/b/j/t/a;->d:Lf/h/a/b/j/t/c;

    iget-object v1, p0, Lf/h/a/b/j/t/a;->e:Lf/h/a/b/j/i;

    iget-object v2, p0, Lf/h/a/b/j/t/a;->f:Lf/h/a/b/h;

    iget-object v3, p0, Lf/h/a/b/j/t/a;->g:Lf/h/a/b/j/f;

    sget-object v4, Lf/h/a/b/j/t/c;->f:Ljava/util/logging/Logger;

    :try_start_0
    iget-object v4, v0, Lf/h/a/b/j/t/c;->c:Lf/h/a/b/j/q/e;

    invoke-virtual {v1}, Lf/h/a/b/j/i;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lf/h/a/b/j/q/e;->get(Ljava/lang/String;)Lf/h/a/b/j/q/m;

    move-result-object v4

    if-nez v4, :cond_0

    const-string v0, "Transport backend \'%s\' is not registered"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lf/h/a/b/j/i;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lf/h/a/b/j/t/c;->f:Ljava/util/logging/Logger;

    invoke-virtual {v1, v0}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v1}, Lf/h/a/b/h;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    invoke-interface {v4, v3}, Lf/h/a/b/j/q/m;->b(Lf/h/a/b/j/f;)Lf/h/a/b/j/f;

    move-result-object v3

    iget-object v4, v0, Lf/h/a/b/j/t/c;->e:Lf/h/a/b/j/u/a;

    new-instance v5, Lf/h/a/b/j/t/b;

    invoke-direct {v5, v0, v1, v3}, Lf/h/a/b/j/t/b;-><init>(Lf/h/a/b/j/t/c;Lf/h/a/b/j/i;Lf/h/a/b/j/f;)V

    invoke-interface {v4, v5}, Lf/h/a/b/j/u/a;->a(Lf/h/a/b/j/u/a$a;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Lf/h/a/b/h;->a(Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lf/h/a/b/j/t/c;->f:Ljava/util/logging/Logger;

    const-string v3, "Error scheduling event "

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    invoke-interface {v2, v0}, Lf/h/a/b/h;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method
