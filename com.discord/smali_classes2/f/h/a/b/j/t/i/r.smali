.class public final synthetic Lf/h/a/b/j/t/i/r;
.super Ljava/lang/Object;
.source "SQLiteEventStore.java"

# interfaces
.implements Lf/h/a/b/j/t/i/t$b;


# static fields
.field public static final a:Lf/h/a/b/j/t/i/r;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/b/j/t/i/r;

    invoke-direct {v0}, Lf/h/a/b/j/t/i/r;-><init>()V

    sput-object v0, Lf/h/a/b/j/t/i/r;->a:Lf/h/a/b/j/t/i/r;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Throwable;

    sget-object v0, Lf/h/a/b/j/t/i/t;->h:Lf/h/a/b/b;

    new-instance v0, Lcom/google/android/datatransport/runtime/synchronization/SynchronizationException;

    const-string v1, "Timed out while trying to open db."

    invoke-direct {v0, v1, p1}, Lcom/google/android/datatransport/runtime/synchronization/SynchronizationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
