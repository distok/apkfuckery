.class public final Lf/h/a/b/j/t/d;
.super Ljava/lang/Object;
.source "DefaultScheduler_Factory.java"

# interfaces
.implements Lw/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Object<",
        "Lf/h/a/b/j/t/c;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/q/e;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/r;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/c;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/u/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw/a/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/q/e;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/r;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/c;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/u/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/d;->a:Lw/a/a;

    iput-object p2, p0, Lf/h/a/b/j/t/d;->b:Lw/a/a;

    iput-object p3, p0, Lf/h/a/b/j/t/d;->c:Lw/a/a;

    iput-object p4, p0, Lf/h/a/b/j/t/d;->d:Lw/a/a;

    iput-object p5, p0, Lf/h/a/b/j/t/d;->e:Lw/a/a;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lf/h/a/b/j/t/d;->a:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lf/h/a/b/j/t/d;->b:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lf/h/a/b/j/q/e;

    iget-object v0, p0, Lf/h/a/b/j/t/d;->c:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lf/h/a/b/j/t/h/r;

    iget-object v0, p0, Lf/h/a/b/j/t/d;->d:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lf/h/a/b/j/t/i/c;

    iget-object v0, p0, Lf/h/a/b/j/t/d;->e:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lf/h/a/b/j/u/a;

    new-instance v0, Lf/h/a/b/j/t/c;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lf/h/a/b/j/t/c;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/b/j/q/e;Lf/h/a/b/j/t/h/r;Lf/h/a/b/j/t/i/c;Lf/h/a/b/j/u/a;)V

    return-object v0
.end method
