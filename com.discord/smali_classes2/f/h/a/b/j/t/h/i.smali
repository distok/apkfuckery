.class public final synthetic Lf/h/a/b/j/t/h/i;
.super Ljava/lang/Object;
.source "Uploader.java"

# interfaces
.implements Lf/h/a/b/j/u/a$a;


# instance fields
.field public final a:Lf/h/a/b/j/t/h/l;

.field public final b:Lf/h/a/b/j/q/g;

.field public final c:Ljava/lang/Iterable;

.field public final d:Lf/h/a/b/j/i;

.field public final e:I


# direct methods
.method public constructor <init>(Lf/h/a/b/j/t/h/l;Lf/h/a/b/j/q/g;Ljava/lang/Iterable;Lf/h/a/b/j/i;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/h/i;->a:Lf/h/a/b/j/t/h/l;

    iput-object p2, p0, Lf/h/a/b/j/t/h/i;->b:Lf/h/a/b/j/q/g;

    iput-object p3, p0, Lf/h/a/b/j/t/h/i;->c:Ljava/lang/Iterable;

    iput-object p4, p0, Lf/h/a/b/j/t/h/i;->d:Lf/h/a/b/j/i;

    iput p5, p0, Lf/h/a/b/j/t/h/i;->e:I

    return-void
.end method


# virtual methods
.method public execute()Ljava/lang/Object;
    .locals 10

    iget-object v0, p0, Lf/h/a/b/j/t/h/i;->a:Lf/h/a/b/j/t/h/l;

    iget-object v1, p0, Lf/h/a/b/j/t/h/i;->b:Lf/h/a/b/j/q/g;

    iget-object v2, p0, Lf/h/a/b/j/t/h/i;->c:Ljava/lang/Iterable;

    iget-object v3, p0, Lf/h/a/b/j/t/h/i;->d:Lf/h/a/b/j/i;

    iget v4, p0, Lf/h/a/b/j/t/h/i;->e:I

    invoke-virtual {v1}, Lf/h/a/b/j/q/g;->c()Lf/h/a/b/j/q/g$a;

    move-result-object v5

    sget-object v6, Lf/h/a/b/j/q/g$a;->e:Lf/h/a/b/j/q/g$a;

    const/4 v7, 0x1

    if-ne v5, v6, :cond_0

    iget-object v1, v0, Lf/h/a/b/j/t/h/l;->c:Lf/h/a/b/j/t/i/c;

    invoke-interface {v1, v2}, Lf/h/a/b/j/t/i/c;->C0(Ljava/lang/Iterable;)V

    iget-object v0, v0, Lf/h/a/b/j/t/h/l;->d:Lf/h/a/b/j/t/h/r;

    add-int/2addr v4, v7

    invoke-interface {v0, v3, v4}, Lf/h/a/b/j/t/h/r;->a(Lf/h/a/b/j/i;I)V

    goto :goto_0

    :cond_0
    iget-object v4, v0, Lf/h/a/b/j/t/h/l;->c:Lf/h/a/b/j/t/i/c;

    invoke-interface {v4, v2}, Lf/h/a/b/j/t/i/c;->s(Ljava/lang/Iterable;)V

    invoke-virtual {v1}, Lf/h/a/b/j/q/g;->c()Lf/h/a/b/j/q/g$a;

    move-result-object v2

    sget-object v4, Lf/h/a/b/j/q/g$a;->d:Lf/h/a/b/j/q/g$a;

    if-ne v2, v4, :cond_1

    iget-object v2, v0, Lf/h/a/b/j/t/h/l;->c:Lf/h/a/b/j/t/i/c;

    iget-object v4, v0, Lf/h/a/b/j/t/h/l;->g:Lf/h/a/b/j/v/a;

    invoke-interface {v4}, Lf/h/a/b/j/v/a;->a()J

    move-result-wide v4

    invoke-virtual {v1}, Lf/h/a/b/j/q/g;->b()J

    move-result-wide v8

    add-long/2addr v8, v4

    invoke-interface {v2, v3, v8, v9}, Lf/h/a/b/j/t/i/c;->G(Lf/h/a/b/j/i;J)V

    :cond_1
    iget-object v1, v0, Lf/h/a/b/j/t/h/l;->c:Lf/h/a/b/j/t/i/c;

    invoke-interface {v1, v3}, Lf/h/a/b/j/t/i/c;->y0(Lf/h/a/b/j/i;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Lf/h/a/b/j/t/h/l;->d:Lf/h/a/b/j/t/h/r;

    invoke-interface {v0, v3, v7}, Lf/h/a/b/j/t/h/r;->a(Lf/h/a/b/j/i;I)V

    :cond_2
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method
