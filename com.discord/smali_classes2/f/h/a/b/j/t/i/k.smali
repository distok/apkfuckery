.class public final synthetic Lf/h/a/b/j/t/i/k;
.super Ljava/lang/Object;
.source "SQLiteEventStore.java"

# interfaces
.implements Lf/h/a/b/j/t/i/t$b;


# instance fields
.field public final a:Lf/h/a/b/j/t/i/t;

.field public final b:Ljava/util/List;

.field public final c:Lf/h/a/b/j/i;


# direct methods
.method public constructor <init>(Lf/h/a/b/j/t/i/t;Ljava/util/List;Lf/h/a/b/j/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/i/k;->a:Lf/h/a/b/j/t/i/t;

    iput-object p2, p0, Lf/h/a/b/j/t/i/k;->b:Ljava/util/List;

    iput-object p3, p0, Lf/h/a/b/j/t/i/k;->c:Lf/h/a/b/j/i;

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 20

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/b/j/t/i/k;->a:Lf/h/a/b/j/t/i/t;

    iget-object v2, v0, Lf/h/a/b/j/t/i/k;->b:Ljava/util/List;

    iget-object v3, v0, Lf/h/a/b/j/t/i/k;->c:Lf/h/a/b/j/i;

    move-object/from16 v4, p1

    check-cast v4, Landroid/database/Cursor;

    sget-object v5, Lf/h/a/b/j/t/i/t;->h:Lf/h/a/b/b;

    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v8, 0x7

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v9, 0x1

    if-eqz v8, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    :cond_0
    const/4 v8, 0x0

    :goto_1
    new-instance v10, Lf/h/a/b/j/a$b;

    invoke-direct {v10}, Lf/h/a/b/j/a$b;-><init>()V

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    iput-object v11, v10, Lf/h/a/b/j/a$b;->f:Ljava/util/Map;

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lf/h/a/b/j/a$b;->f(Ljava/lang/String;)Lf/h/a/b/j/f$a;

    const/4 v11, 0x2

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Lf/h/a/b/j/a$b;->e(J)Lf/h/a/b/j/f$a;

    const/4 v11, 0x3

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Lf/h/a/b/j/a$b;->g(J)Lf/h/a/b/j/f$a;

    const/4 v11, 0x4

    if-eqz v8, :cond_2

    new-instance v5, Lf/h/a/b/j/e;

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    sget-object v8, Lf/h/a/b/j/t/i/t;->h:Lf/h/a/b/b;

    goto :goto_2

    :cond_1
    new-instance v9, Lf/h/a/b/b;

    invoke-direct {v9, v8}, Lf/h/a/b/b;-><init>(Ljava/lang/String;)V

    move-object v8, v9

    :goto_2
    const/4 v9, 0x5

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    invoke-direct {v5, v8, v9}, Lf/h/a/b/j/e;-><init>(Lf/h/a/b/b;[B)V

    invoke-virtual {v10, v5}, Lf/h/a/b/j/a$b;->d(Lf/h/a/b/j/e;)Lf/h/a/b/j/f$a;

    goto :goto_4

    :cond_2
    new-instance v8, Lf/h/a/b/j/e;

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_3

    sget-object v11, Lf/h/a/b/j/t/i/t;->h:Lf/h/a/b/b;

    goto :goto_3

    :cond_3
    new-instance v12, Lf/h/a/b/b;

    invoke-direct {v12, v11}, Lf/h/a/b/b;-><init>(Ljava/lang/String;)V

    move-object v11, v12

    :goto_3
    invoke-virtual {v1}, Lf/h/a/b/j/t/i/t;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    const-string v13, "bytes"

    filled-new-array {v13}, [Ljava/lang/String;

    move-result-object v14

    new-array v9, v9, [Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v5

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-string v13, "event_payloads"

    const-string v15, "event_id = ?"

    const-string v19, "sequence_num"

    move-object/from16 v16, v9

    invoke-virtual/range {v12 .. v19}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    sget-object v9, Lf/h/a/b/j/t/i/l;->a:Lf/h/a/b/j/t/i/l;

    invoke-static {v5, v9}, Lf/h/a/b/j/t/i/t;->g(Landroid/database/Cursor;Lf/h/a/b/j/t/i/t$b;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    invoke-direct {v8, v11, v5}, Lf/h/a/b/j/e;-><init>(Lf/h/a/b/b;[B)V

    invoke-virtual {v10, v8}, Lf/h/a/b/j/a$b;->d(Lf/h/a/b/j/e;)Lf/h/a/b/j/f$a;

    :goto_4
    const/4 v5, 0x6

    invoke-interface {v4, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v10, Lf/h/a/b/j/a$b;->b:Ljava/lang/Integer;

    :cond_4
    invoke-virtual {v10}, Lf/h/a/b/j/a$b;->b()Lf/h/a/b/j/f;

    move-result-object v5

    new-instance v8, Lf/h/a/b/j/t/i/b;

    invoke-direct {v8, v6, v7, v3, v5}, Lf/h/a/b/j/t/i/b;-><init>(JLf/h/a/b/j/i;Lf/h/a/b/j/f;)V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    return-object v1
.end method
