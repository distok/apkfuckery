.class public final Lf/h/a/b/j/t/h/m;
.super Ljava/lang/Object;
.source "Uploader_Factory.java"

# interfaces
.implements Lw/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Object<",
        "Lf/h/a/b/j/t/h/l;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/q/e;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/c;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/r;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/u/a;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw/a/a<",
            "Landroid/content/Context;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/q/e;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/c;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/h/r;",
            ">;",
            "Lw/a/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/u/a;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/h/m;->a:Lw/a/a;

    iput-object p2, p0, Lf/h/a/b/j/t/h/m;->b:Lw/a/a;

    iput-object p3, p0, Lf/h/a/b/j/t/h/m;->c:Lw/a/a;

    iput-object p4, p0, Lf/h/a/b/j/t/h/m;->d:Lw/a/a;

    iput-object p5, p0, Lf/h/a/b/j/t/h/m;->e:Lw/a/a;

    iput-object p6, p0, Lf/h/a/b/j/t/h/m;->f:Lw/a/a;

    iput-object p7, p0, Lf/h/a/b/j/t/h/m;->g:Lw/a/a;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 9

    iget-object v0, p0, Lf/h/a/b/j/t/h/m;->a:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/content/Context;

    iget-object v0, p0, Lf/h/a/b/j/t/h/m;->b:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lf/h/a/b/j/q/e;

    iget-object v0, p0, Lf/h/a/b/j/t/h/m;->c:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lf/h/a/b/j/t/i/c;

    iget-object v0, p0, Lf/h/a/b/j/t/h/m;->d:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lf/h/a/b/j/t/h/r;

    iget-object v0, p0, Lf/h/a/b/j/t/h/m;->e:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lf/h/a/b/j/t/h/m;->f:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lf/h/a/b/j/u/a;

    iget-object v0, p0, Lf/h/a/b/j/t/h/m;->g:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lf/h/a/b/j/v/a;

    new-instance v0, Lf/h/a/b/j/t/h/l;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lf/h/a/b/j/t/h/l;-><init>(Landroid/content/Context;Lf/h/a/b/j/q/e;Lf/h/a/b/j/t/i/c;Lf/h/a/b/j/t/h/r;Ljava/util/concurrent/Executor;Lf/h/a/b/j/u/a;Lf/h/a/b/j/v/a;)V

    return-object v0
.end method
