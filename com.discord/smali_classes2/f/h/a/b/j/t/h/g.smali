.class public final synthetic Lf/h/a/b/j/t/h/g;
.super Ljava/lang/Object;
.source "Uploader.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final d:Lf/h/a/b/j/t/h/l;

.field public final e:Lf/h/a/b/j/i;

.field public final f:I

.field public final g:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lf/h/a/b/j/t/h/l;Lf/h/a/b/j/i;ILjava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/h/g;->d:Lf/h/a/b/j/t/h/l;

    iput-object p2, p0, Lf/h/a/b/j/t/h/g;->e:Lf/h/a/b/j/i;

    iput p3, p0, Lf/h/a/b/j/t/h/g;->f:I

    iput-object p4, p0, Lf/h/a/b/j/t/h/g;->g:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lf/h/a/b/j/t/h/g;->d:Lf/h/a/b/j/t/h/l;

    iget-object v1, p0, Lf/h/a/b/j/t/h/g;->e:Lf/h/a/b/j/i;

    iget v2, p0, Lf/h/a/b/j/t/h/g;->f:I

    iget-object v3, p0, Lf/h/a/b/j/t/h/g;->g:Ljava/lang/Runnable;

    const/4 v4, 0x1

    :try_start_0
    iget-object v5, v0, Lf/h/a/b/j/t/h/l;->f:Lf/h/a/b/j/u/a;

    iget-object v6, v0, Lf/h/a/b/j/t/h/l;->c:Lf/h/a/b/j/t/i/c;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v7, Lf/h/a/b/j/t/h/j;

    invoke-direct {v7, v6}, Lf/h/a/b/j/t/h/j;-><init>(Lf/h/a/b/j/t/i/c;)V

    invoke-interface {v5, v7}, Lf/h/a/b/j/u/a;->a(Lf/h/a/b/j/u/a$a;)Ljava/lang/Object;

    iget-object v5, v0, Lf/h/a/b/j/t/h/l;->a:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/ConnectivityManager;

    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    if-nez v5, :cond_1

    iget-object v5, v0, Lf/h/a/b/j/t/h/l;->f:Lf/h/a/b/j/u/a;

    new-instance v6, Lf/h/a/b/j/t/h/k;

    invoke-direct {v6, v0, v1, v2}, Lf/h/a/b/j/t/h/k;-><init>(Lf/h/a/b/j/t/h/l;Lf/h/a/b/j/i;I)V

    invoke-interface {v5, v6}, Lf/h/a/b/j/u/a;->a(Lf/h/a/b/j/u/a$a;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1, v2}, Lf/h/a/b/j/t/h/l;->a(Lf/h/a/b/j/i;I)V
    :try_end_0
    .catch Lcom/google/android/datatransport/runtime/synchronization/SynchronizationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    :try_start_1
    iget-object v0, v0, Lf/h/a/b/j/t/h/l;->d:Lf/h/a/b/j/t/h/r;

    add-int/2addr v2, v4

    invoke-interface {v0, v1, v2}, Lf/h/a/b/j/t/h/r;->a(Lf/h/a/b/j/i;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    return-void

    :goto_2
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    throw v0
.end method
