.class public Lf/h/a/b/j/t/c;
.super Ljava/lang/Object;
.source "DefaultScheduler.java"

# interfaces
.implements Lf/h/a/b/j/t/e;


# static fields
.field public static final f:Ljava/util/logging/Logger;


# instance fields
.field public final a:Lf/h/a/b/j/t/h/r;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Lf/h/a/b/j/q/e;

.field public final d:Lf/h/a/b/j/t/i/c;

.field public final e:Lf/h/a/b/j/u/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lf/h/a/b/j/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lf/h/a/b/j/t/c;->f:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lf/h/a/b/j/q/e;Lf/h/a/b/j/t/h/r;Lf/h/a/b/j/t/i/c;Lf/h/a/b/j/u/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/c;->b:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lf/h/a/b/j/t/c;->c:Lf/h/a/b/j/q/e;

    iput-object p3, p0, Lf/h/a/b/j/t/c;->a:Lf/h/a/b/j/t/h/r;

    iput-object p4, p0, Lf/h/a/b/j/t/c;->d:Lf/h/a/b/j/t/i/c;

    iput-object p5, p0, Lf/h/a/b/j/t/c;->e:Lf/h/a/b/j/u/a;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/b/j/i;Lf/h/a/b/j/f;Lf/h/a/b/h;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/b/j/t/c;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lf/h/a/b/j/t/a;

    invoke-direct {v1, p0, p1, p3, p2}, Lf/h/a/b/j/t/a;-><init>(Lf/h/a/b/j/t/c;Lf/h/a/b/j/i;Lf/h/a/b/h;Lf/h/a/b/j/f;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
