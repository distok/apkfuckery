.class public interface abstract Lf/h/a/b/j/t/i/c;
.super Ljava/lang/Object;
.source "EventStore.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Landroidx/annotation/WorkerThread;
.end annotation


# virtual methods
.method public abstract A(Lf/h/a/b/j/i;)Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/b/j/i;",
            ")",
            "Ljava/lang/Iterable<",
            "Lf/h/a/b/j/t/i/h;",
            ">;"
        }
    .end annotation
.end method

.method public abstract C0(Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lf/h/a/b/j/t/i/h;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract G(Lf/h/a/b/j/i;J)V
.end method

.method public abstract L()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lf/h/a/b/j/i;",
            ">;"
        }
    .end annotation
.end method

.method public abstract m0(Lf/h/a/b/j/i;Lf/h/a/b/j/f;)Lf/h/a/b/j/t/i/h;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()I
.end method

.method public abstract s(Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lf/h/a/b/j/t/i/h;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract u0(Lf/h/a/b/j/i;)J
.end method

.method public abstract y0(Lf/h/a/b/j/i;)Z
.end method
