.class public final Lf/h/a/b/j/t/f;
.super Ljava/lang/Object;
.source "SchedulingConfigModule_ConfigFactory.java"

# interfaces
.implements Lw/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Object<",
        "Lf/h/a/b/j/t/h/f;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lw/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/f;->a:Lw/a/a;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 8

    iget-object v0, p0, Lf/h/a/b/j/t/f;->a:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/b/j/v/a;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sget-object v2, Lf/h/a/b/d;->d:Lf/h/a/b/d;

    invoke-static {}, Lf/h/a/b/j/t/h/f$a;->a()Lf/h/a/b/j/t/h/f$a$a;

    move-result-object v3

    const-wide/16 v4, 0x7530

    invoke-virtual {v3, v4, v5}, Lf/h/a/b/j/t/h/f$a$a;->b(J)Lf/h/a/b/j/t/h/f$a$a;

    const-wide/32 v4, 0x5265c00

    invoke-virtual {v3, v4, v5}, Lf/h/a/b/j/t/h/f$a$a;->c(J)Lf/h/a/b/j/t/h/f$a$a;

    invoke-virtual {v3}, Lf/h/a/b/j/t/h/f$a$a;->a()Lf/h/a/b/j/t/h/f$a;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lf/h/a/b/d;->f:Lf/h/a/b/d;

    invoke-static {}, Lf/h/a/b/j/t/h/f$a;->a()Lf/h/a/b/j/t/h/f$a$a;

    move-result-object v3

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v6, v7}, Lf/h/a/b/j/t/h/f$a$a;->b(J)Lf/h/a/b/j/t/h/f$a$a;

    invoke-virtual {v3, v4, v5}, Lf/h/a/b/j/t/h/f$a$a;->c(J)Lf/h/a/b/j/t/h/f$a$a;

    invoke-virtual {v3}, Lf/h/a/b/j/t/h/f$a$a;->a()Lf/h/a/b/j/t/h/f$a;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lf/h/a/b/d;->e:Lf/h/a/b/d;

    invoke-static {}, Lf/h/a/b/j/t/h/f$a;->a()Lf/h/a/b/j/t/h/f$a$a;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Lf/h/a/b/j/t/h/f$a$a;->b(J)Lf/h/a/b/j/t/h/f$a$a;

    invoke-virtual {v3, v4, v5}, Lf/h/a/b/j/t/h/f$a$a;->c(J)Lf/h/a/b/j/t/h/f$a$a;

    const/4 v4, 0x2

    new-array v4, v4, [Lf/h/a/b/j/t/h/f$b;

    const/4 v5, 0x0

    sget-object v6, Lf/h/a/b/j/t/h/f$b;->d:Lf/h/a/b/j/t/h/f$b;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lf/h/a/b/j/t/h/f$b;->e:Lf/h/a/b/j/t/h/f$b;

    aput-object v6, v4, v5

    new-instance v5, Ljava/util/HashSet;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    check-cast v3, Lf/h/a/b/j/t/h/c$b;

    const-string v5, "Null flags"

    invoke-static {v4, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object v4, v3, Lf/h/a/b/j/t/h/c$b;->c:Ljava/util/Set;

    invoke-virtual {v3}, Lf/h/a/b/j/t/h/c$b;->a()Lf/h/a/b/j/t/h/f$a;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "missing required property: clock"

    invoke-static {v0, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {}, Lf/h/a/b/d;->values()[Lf/h/a/b/d;

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    new-instance v2, Lf/h/a/b/j/t/h/b;

    invoke-direct {v2, v0, v1}, Lf/h/a/b/j/t/h/b;-><init>(Lf/h/a/b/j/v/a;Ljava/util/Map;)V

    return-object v2

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not all priorities have been configured"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
