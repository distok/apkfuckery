.class public final Lf/h/a/b/j/t/i/u;
.super Ljava/lang/Object;
.source "SQLiteEventStore_Factory.java"

# interfaces
.implements Lw/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Object<",
        "Lf/h/a/b/j/t/i/t;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/d;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lw/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lw/a/a;Lw/a/a;Lw/a/a;Lw/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/v/a;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/d;",
            ">;",
            "Lw/a/a<",
            "Lf/h/a/b/j/t/i/z;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/t/i/u;->a:Lw/a/a;

    iput-object p2, p0, Lf/h/a/b/j/t/i/u;->b:Lw/a/a;

    iput-object p3, p0, Lf/h/a/b/j/t/i/u;->c:Lw/a/a;

    iput-object p4, p0, Lf/h/a/b/j/t/i/u;->d:Lw/a/a;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lf/h/a/b/j/t/i/u;->a:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/b/j/v/a;

    iget-object v1, p0, Lf/h/a/b/j/t/i/u;->b:Lw/a/a;

    invoke-interface {v1}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/b/j/v/a;

    iget-object v2, p0, Lf/h/a/b/j/t/i/u;->c:Lw/a/a;

    invoke-interface {v2}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lf/h/a/b/j/t/i/u;->d:Lw/a/a;

    invoke-interface {v3}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Lf/h/a/b/j/t/i/t;

    check-cast v2, Lf/h/a/b/j/t/i/d;

    check-cast v3, Lf/h/a/b/j/t/i/z;

    invoke-direct {v4, v0, v1, v2, v3}, Lf/h/a/b/j/t/i/t;-><init>(Lf/h/a/b/j/v/a;Lf/h/a/b/j/v/a;Lf/h/a/b/j/t/i/d;Lf/h/a/b/j/t/i/z;)V

    return-object v4
.end method
