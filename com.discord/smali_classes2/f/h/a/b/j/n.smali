.class public Lf/h/a/b/j/n;
.super Ljava/lang/Object;
.source "TransportRuntime.java"

# interfaces
.implements Lf/h/a/b/j/m;


# static fields
.field public static volatile e:Lf/h/a/b/j/o;


# instance fields
.field public final a:Lf/h/a/b/j/v/a;

.field public final b:Lf/h/a/b/j/v/a;

.field public final c:Lf/h/a/b/j/t/e;

.field public final d:Lf/h/a/b/j/t/h/l;


# direct methods
.method public constructor <init>(Lf/h/a/b/j/v/a;Lf/h/a/b/j/v/a;Lf/h/a/b/j/t/e;Lf/h/a/b/j/t/h/l;Lf/h/a/b/j/t/h/p;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/b/j/n;->a:Lf/h/a/b/j/v/a;

    iput-object p2, p0, Lf/h/a/b/j/n;->b:Lf/h/a/b/j/v/a;

    iput-object p3, p0, Lf/h/a/b/j/n;->c:Lf/h/a/b/j/t/e;

    iput-object p4, p0, Lf/h/a/b/j/n;->d:Lf/h/a/b/j/t/h/l;

    iget-object p1, p5, Lf/h/a/b/j/t/h/p;->a:Ljava/util/concurrent/Executor;

    new-instance p2, Lf/h/a/b/j/t/h/n;

    invoke-direct {p2, p5}, Lf/h/a/b/j/t/h/n;-><init>(Lf/h/a/b/j/t/h/p;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static a()Lf/h/a/b/j/n;
    .locals 2

    sget-object v0, Lf/h/a/b/j/n;->e:Lf/h/a/b/j/o;

    if-eqz v0, :cond_0

    check-cast v0, Lf/h/a/b/j/c;

    iget-object v0, v0, Lf/h/a/b/j/c;->o:Lw/a/a;

    invoke-interface {v0}, Lw/a/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/b/j/n;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not initialized!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lf/h/a/b/j/n;->e:Lf/h/a/b/j/o;

    if-nez v0, :cond_1

    const-class v0, Lf/h/a/b/j/n;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/h/a/b/j/n;->e:Lf/h/a/b/j/o;

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v1, Landroid/content/Context;

    new-instance v1, Lf/h/a/b/j/c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lf/h/a/b/j/c;-><init>(Landroid/content/Context;Lf/h/a/b/j/c$a;)V

    sput-object v1, Lf/h/a/b/j/n;->e:Lf/h/a/b/j/o;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public c(Lf/h/a/b/j/d;)Lf/h/a/b/g;
    .locals 4

    new-instance v0, Lf/h/a/b/j/j;

    instance-of v1, p1, Lf/h/a/b/j/d;

    if-eqz v1, :cond_0

    move-object v1, p1

    check-cast v1, Lf/h/a/b/i/a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/a/b/i/a;->f:Ljava/util/Set;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    goto :goto_0

    :cond_0
    new-instance v1, Lf/h/a/b/b;

    const-string v2, "proto"

    invoke-direct {v1, v2}, Lf/h/a/b/b;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    :goto_0
    invoke-static {}, Lf/h/a/b/j/i;->a()Lf/h/a/b/j/i$a;

    move-result-object v2

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "cct"

    invoke-virtual {v2, v3}, Lf/h/a/b/j/i$a;->b(Ljava/lang/String;)Lf/h/a/b/j/i$a;

    check-cast p1, Lf/h/a/b/i/a;

    invoke-virtual {p1}, Lf/h/a/b/i/a;->b()[B

    move-result-object p1

    check-cast v2, Lf/h/a/b/j/b$b;

    iput-object p1, v2, Lf/h/a/b/j/b$b;->b:[B

    invoke-virtual {v2}, Lf/h/a/b/j/b$b;->a()Lf/h/a/b/j/i;

    move-result-object p1

    invoke-direct {v0, v1, p1, p0}, Lf/h/a/b/j/j;-><init>(Ljava/util/Set;Lf/h/a/b/j/i;Lf/h/a/b/j/m;)V

    return-object v0
.end method
