.class public final Lf/h/a/b/i/e/b$e;
.super Ljava/lang/Object;
.source "AutoBatchedLogRequestEncoder.java"

# interfaces
.implements Lf/h/c/q/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/b/i/e/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/c/q/c<",
        "Lf/h/a/b/i/e/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lf/h/a/b/i/e/b$e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/b/i/e/b$e;

    invoke-direct {v0}, Lf/h/a/b/i/e/b$e;-><init>()V

    sput-object v0, Lf/h/a/b/i/e/b$e;->a:Lf/h/a/b/i/e/b$e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/a/b/i/e/m;

    check-cast p2, Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/a/b/i/e/m;->f()J

    move-result-wide v0

    const-string v2, "requestTimeMs"

    invoke-interface {p2, v2, v0, v1}, Lf/h/c/q/d;->b(Ljava/lang/String;J)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/a/b/i/e/m;->g()J

    move-result-wide v0

    const-string v2, "requestUptimeMs"

    invoke-interface {p2, v2, v0, v1}, Lf/h/c/q/d;->b(Ljava/lang/String;J)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/a/b/i/e/m;->a()Lf/h/a/b/i/e/k;

    move-result-object v0

    const-string v1, "clientInfo"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/a/b/i/e/m;->c()Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "logSource"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/a/b/i/e/m;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "logSourceName"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/a/b/i/e/m;->b()Ljava/util/List;

    move-result-object v0

    const-string v1, "logEvent"

    invoke-interface {p2, v1, v0}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    invoke-virtual {p1}, Lf/h/a/b/i/e/m;->e()Lf/h/a/b/i/e/p;

    move-result-object p1

    const-string v0, "qosTier"

    invoke-interface {p2, v0, p1}, Lf/h/c/q/d;->f(Ljava/lang/String;Ljava/lang/Object;)Lf/h/c/q/d;

    return-void
.end method
