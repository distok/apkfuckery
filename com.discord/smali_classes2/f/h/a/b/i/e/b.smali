.class public final Lf/h/a/b/i/e/b;
.super Ljava/lang/Object;
.source "AutoBatchedLogRequestEncoder.java"

# interfaces
.implements Lf/h/c/q/g/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/b/i/e/b$f;,
        Lf/h/a/b/i/e/b$d;,
        Lf/h/a/b/i/e/b$a;,
        Lf/h/a/b/i/e/b$c;,
        Lf/h/a/b/i/e/b$e;,
        Lf/h/a/b/i/e/b$b;
    }
.end annotation


# static fields
.field public static final a:Lf/h/c/q/g/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/b/i/e/b;

    invoke-direct {v0}, Lf/h/a/b/i/e/b;-><init>()V

    sput-object v0, Lf/h/a/b/i/e/b;->a:Lf/h/c/q/g/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/c/q/g/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/q/g/b<",
            "*>;)V"
        }
    .end annotation

    const-class v0, Lf/h/a/b/i/e/j;

    sget-object v1, Lf/h/a/b/i/e/b$b;->a:Lf/h/a/b/i/e/b$b;

    check-cast p1, Lf/h/c/q/h/e;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/d;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/m;

    sget-object v1, Lf/h/a/b/i/e/b$e;->a:Lf/h/a/b/i/e/b$e;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/g;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/k;

    sget-object v1, Lf/h/a/b/i/e/b$c;->a:Lf/h/a/b/i/e/b$c;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/e;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/a;

    sget-object v1, Lf/h/a/b/i/e/b$a;->a:Lf/h/a/b/i/e/b$a;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/c;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/l;

    sget-object v1, Lf/h/a/b/i/e/b$d;->a:Lf/h/a/b/i/e/b$d;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/f;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/o;

    sget-object v1, Lf/h/a/b/i/e/b$f;->a:Lf/h/a/b/i/e/b$f;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lf/h/a/b/i/e/i;

    iget-object v2, p1, Lf/h/c/q/h/e;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p1, Lf/h/c/q/h/e;->b:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
