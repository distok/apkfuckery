.class public abstract Lf/h/a/c/s;
.super Ljava/lang/Object;
.source "BasePlayer.java"

# interfaces
.implements Lf/h/a/c/m0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/s$b;,
        Lf/h/a/c/s$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/t0$c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/t0$c;

    invoke-direct {v0}, Lf/h/a/c/t0$c;-><init>()V

    iput-object v0, p0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s;->v()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasPrevious()Z
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s;->q()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final i()Z
    .locals 3

    invoke-interface {p0}, Lf/h/a/c/m0;->A()Lf/h/a/c/t0;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p0}, Lf/h/a/c/m0;->m()I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/t0;->m(ILf/h/a/c/t0$c;)Lf/h/a/c/t0$c;

    move-result-object v0

    iget-boolean v0, v0, Lf/h/a/c/t0$c;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final q()I
    .locals 4

    invoke-interface {p0}, Lf/h/a/c/m0;->A()Lf/h/a/c/t0;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lf/h/a/c/m0;->m()I

    move-result v1

    invoke-interface {p0}, Lf/h/a/c/m0;->y()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-interface {p0}, Lf/h/a/c/m0;->C()Z

    if-eqz v2, :cond_3

    if-eq v2, v3, :cond_5

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Lf/h/a/c/t0;->a()I

    move-result v2

    if-ne v1, v2, :cond_4

    invoke-virtual {v0}, Lf/h/a/c/t0;->c()I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    invoke-virtual {v0}, Lf/h/a/c/t0;->a()I

    move-result v0

    if-ne v1, v0, :cond_4

    :goto_0
    const/4 v1, -0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v1, -0x1

    :cond_5
    :goto_1
    return v1
.end method

.method public final s()Z
    .locals 2

    invoke-interface {p0}, Lf/h/a/c/m0;->r()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lf/h/a/c/m0;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lf/h/a/c/m0;->w()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final v()I
    .locals 4

    invoke-interface {p0}, Lf/h/a/c/m0;->A()Lf/h/a/c/t0;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lf/h/a/c/m0;->m()I

    move-result v1

    invoke-interface {p0}, Lf/h/a/c/m0;->y()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-interface {p0}, Lf/h/a/c/m0;->C()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lf/h/a/c/t0;->e(IIZ)I

    move-result v0

    :goto_0
    return v0
.end method
