.class public interface abstract Lf/h/a/c/p0;
.super Ljava/lang/Object;
.source "Renderer.java"

# interfaces
.implements Lf/h/a/c/n0$b;


# virtual methods
.method public abstract a()Z
.end method

.method public abstract f(I)V
.end method

.method public abstract g()Z
.end method

.method public abstract getState()I
.end method

.method public abstract h()V
.end method

.method public abstract i()Lf/h/a/c/d1/v;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()Z
.end method

.method public abstract k(Lf/h/a/c/q0;[Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d1/v;JZJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract l()V
.end method

.method public abstract m()Lf/h/a/c/t;
.end method

.method public abstract n(JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract o(F)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract p()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract q()J
.end method

.method public abstract r(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract reset()V
.end method

.method public abstract s()Z
.end method

.method public abstract start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract stop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract t()Lf/h/a/c/i1/n;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()I
.end method

.method public abstract v([Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d1/v;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method
