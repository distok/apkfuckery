.class public final Lf/h/a/c/a0;
.super Lf/h/a/c/s;
.source "ExoPlayerImpl.java"

# interfaces
.implements Lf/h/a/c/m0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a0$b;
    }
.end annotation


# instance fields
.field public final b:Lf/h/a/c/f1/i;

.field public final c:[Lf/h/a/c/p0;

.field public final d:Lf/h/a/c/f1/h;

.field public final e:Landroid/os/Handler;

.field public final f:Lf/h/a/c/b0;

.field public final g:Landroid/os/Handler;

.field public final h:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lf/h/a/c/s$a;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lf/h/a/c/t0$b;

.field public final j:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:I

.field public m:I

.field public n:Z

.field public o:I

.field public p:Z

.field public q:Z

.field public r:I

.field public s:Lf/h/a/c/j0;

.field public t:Lf/h/a/c/i0;

.field public u:I

.field public v:I

.field public w:J


# direct methods
.method public constructor <init>([Lf/h/a/c/p0;Lf/h/a/c/f1/h;Lf/h/a/c/e0;Lf/h/a/c/h1/e;Lf/h/a/c/i1/f;Landroid/os/Looper;)V
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    move-object v0, p0

    move-object v2, p1

    invoke-direct {p0}, Lf/h/a/c/s;-><init>()V

    const-string v1, "Init "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "ExoPlayerLib/2.11.3"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "] ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lf/h/a/c/i1/a0;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ExoPlayerImpl"

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    array-length v1, v2

    const/4 v3, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iput-object v2, v0, Lf/h/a/c/a0;->c:[Lf/h/a/c/p0;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, p2

    iput-object v4, v0, Lf/h/a/c/a0;->d:Lf/h/a/c/f1/h;

    iput-boolean v3, v0, Lf/h/a/c/a0;->k:Z

    iput v3, v0, Lf/h/a/c/a0;->m:I

    iput-boolean v3, v0, Lf/h/a/c/a0;->n:Z

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, v0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v5, Lf/h/a/c/f1/i;

    array-length v1, v2

    new-array v1, v1, [Lf/h/a/c/q0;

    array-length v6, v2

    new-array v6, v6, [Lf/h/a/c/f1/f;

    const/4 v7, 0x0

    invoke-direct {v5, v1, v6, v7}, Lf/h/a/c/f1/i;-><init>([Lf/h/a/c/q0;[Lf/h/a/c/f1/f;Ljava/lang/Object;)V

    iput-object v5, v0, Lf/h/a/c/a0;->b:Lf/h/a/c/f1/i;

    new-instance v1, Lf/h/a/c/t0$b;

    invoke-direct {v1}, Lf/h/a/c/t0$b;-><init>()V

    iput-object v1, v0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    sget-object v1, Lf/h/a/c/j0;->e:Lf/h/a/c/j0;

    iput-object v1, v0, Lf/h/a/c/a0;->s:Lf/h/a/c/j0;

    sget-object v1, Lf/h/a/c/r0;->d:Lf/h/a/c/r0;

    iput v3, v0, Lf/h/a/c/a0;->l:I

    new-instance v10, Lf/h/a/c/a0$a;

    move-object/from16 v1, p6

    invoke-direct {v10, p0, v1}, Lf/h/a/c/a0$a;-><init>(Lf/h/a/c/a0;Landroid/os/Looper;)V

    iput-object v10, v0, Lf/h/a/c/a0;->e:Landroid/os/Handler;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7, v5}, Lf/h/a/c/i0;->d(JLf/h/a/c/f1/i;)Lf/h/a/c/i0;

    move-result-object v1

    iput-object v1, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, v0, Lf/h/a/c/a0;->j:Ljava/util/ArrayDeque;

    new-instance v12, Lf/h/a/c/b0;

    iget-boolean v7, v0, Lf/h/a/c/a0;->k:Z

    iget v8, v0, Lf/h/a/c/a0;->m:I

    iget-boolean v9, v0, Lf/h/a/c/a0;->n:Z

    move-object v1, v12

    move-object v2, p1

    move-object v3, p2

    move-object v4, v5

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v11, p5

    invoke-direct/range {v1 .. v11}, Lf/h/a/c/b0;-><init>([Lf/h/a/c/p0;Lf/h/a/c/f1/h;Lf/h/a/c/f1/i;Lf/h/a/c/e0;Lf/h/a/c/h1/e;ZIZLandroid/os/Handler;Lf/h/a/c/i1/f;)V

    iput-object v12, v0, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    new-instance v1, Landroid/os/Handler;

    iget-object v2, v12, Lf/h/a/c/b0;->k:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, v0, Lf/h/a/c/a0;->g:Landroid/os/Handler;

    return-void
.end method

.method public static J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lf/h/a/c/s$a;",
            ">;",
            "Lf/h/a/c/s$b;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/s$a;

    iget-boolean v1, v0, Lf/h/a/c/s$a;->b:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Lf/h/a/c/s$a;->a:Lf/h/a/c/m0$a;

    invoke-interface {p1, v0}, Lf/h/a/c/s$b;->a(Lf/h/a/c/m0$a;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public A()Lf/h/a/c/t0;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    return-object v0
.end method

.method public B()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->e:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public C()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/a0;->n:Z

    return v0
.end method

.method public D()J
    .locals 6

    invoke-virtual {p0}, Lf/h/a/c/a0;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lf/h/a/c/a0;->w:J

    return-wide v0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v1, v0, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    iget-wide v1, v1, Lf/h/a/c/d1/p$a;->d:J

    iget-object v3, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v3, v3, Lf/h/a/c/d1/p$a;->d:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {p0}, Lf/h/a/c/a0;->m()I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/t0;->m(ILf/h/a/c/t0$c;)Lf/h/a/c/t0$c;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/c/t0$c;->a()J

    move-result-wide v0

    return-wide v0

    :cond_1
    iget-wide v0, v0, Lf/h/a/c/i0;->k:J

    iget-object v2, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    invoke-virtual {v2}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v1, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v0, v0, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    iget-object v0, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    invoke-virtual {v1, v0, v2}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v1, v1, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    iget v1, v1, Lf/h/a/c/d1/p$a;->b:I

    invoke-virtual {v0, v1}, Lf/h/a/c/t0$b;->d(I)J

    move-result-wide v1

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v5, v1, v3

    if-nez v5, :cond_2

    iget-wide v0, v0, Lf/h/a/c/t0$b;->c:J

    goto :goto_0

    :cond_2
    move-wide v0, v1

    :cond_3
    :goto_0
    iget-object v2, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    invoke-virtual {p0, v2, v0, v1}, Lf/h/a/c/a0;->M(Lf/h/a/c/d1/p$a;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public E()Lf/h/a/c/f1/g;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    iget-object v0, v0, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    return-object v0
.end method

.method public F(I)I
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->c:[Lf/h/a/c/p0;

    aget-object p1, v0, p1

    invoke-interface {p1}, Lf/h/a/c/p0;->u()I

    move-result p1

    return p1
.end method

.method public G()J
    .locals 4

    invoke-virtual {p0}, Lf/h/a/c/a0;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lf/h/a/c/a0;->w:J

    return-wide v0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    invoke-virtual {v0}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-wide v0, v0, Lf/h/a/c/i0;->m:J

    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v0

    return-wide v0

    :cond_1
    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v1, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v2, v0, Lf/h/a/c/i0;->m:J

    invoke-virtual {p0, v1, v2, v3}, Lf/h/a/c/a0;->M(Lf/h/a/c/d1/p$a;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public H()Lf/h/a/c/m0$b;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final I(ZZZI)Lf/h/a/c/i0;
    .locals 24

    move-object/from16 v0, p0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iput v3, v0, Lf/h/a/c/a0;->u:I

    iput v3, v0, Lf/h/a/c/a0;->v:I

    iput-wide v1, v0, Lf/h/a/c/a0;->w:J

    goto :goto_1

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a0;->m()I

    move-result v4

    iput v4, v0, Lf/h/a/c/a0;->u:I

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a0;->O()Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, v0, Lf/h/a/c/a0;->v:I

    goto :goto_0

    :cond_1
    iget-object v4, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v5, v4, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v4, v4, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-object v4, v4, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v5, v4}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v4

    :goto_0
    iput v4, v0, Lf/h/a/c/a0;->v:I

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a0;->G()J

    move-result-wide v4

    iput-wide v4, v0, Lf/h/a/c/a0;->w:J

    :goto_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    :cond_2
    const/4 v3, 0x1

    :cond_3
    if-eqz v3, :cond_4

    iget-object v4, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-boolean v5, v0, Lf/h/a/c/a0;->n:Z

    iget-object v6, v0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    iget-object v7, v0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    invoke-virtual {v4, v5, v6, v7}, Lf/h/a/c/i0;->e(ZLf/h/a/c/t0$c;Lf/h/a/c/t0$b;)Lf/h/a/c/d1/p$a;

    move-result-object v4

    goto :goto_2

    :cond_4
    iget-object v4, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v4, v4, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    :goto_2
    move-object/from16 v17, v4

    if-eqz v3, :cond_5

    goto :goto_3

    :cond_5
    iget-object v1, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-wide v1, v1, Lf/h/a/c/i0;->m:J

    :goto_3
    move-wide/from16 v22, v1

    if-eqz v3, :cond_6

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_4

    :cond_6
    iget-object v1, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-wide v1, v1, Lf/h/a/c/i0;->d:J

    :goto_4
    move-wide v10, v1

    new-instance v1, Lf/h/a/c/i0;

    if-eqz p2, :cond_7

    sget-object v2, Lf/h/a/c/t0;->a:Lf/h/a/c/t0;

    goto :goto_5

    :cond_7
    iget-object v2, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    :goto_5
    move-object v6, v2

    if-eqz p3, :cond_8

    const/4 v2, 0x0

    goto :goto_6

    :cond_8
    iget-object v2, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    :goto_6
    move-object v13, v2

    const/4 v14, 0x0

    if-eqz p2, :cond_9

    sget-object v2, Lcom/google/android/exoplayer2/source/TrackGroupArray;->g:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    goto :goto_7

    :cond_9
    iget-object v2, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    :goto_7
    move-object v15, v2

    if-eqz p2, :cond_a

    iget-object v2, v0, Lf/h/a/c/a0;->b:Lf/h/a/c/f1/i;

    goto :goto_8

    :cond_a
    iget-object v2, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    :goto_8
    move-object/from16 v16, v2

    const-wide/16 v20, 0x0

    move-object v5, v1

    move-object/from16 v7, v17

    move-wide/from16 v8, v22

    move/from16 v12, p4

    move-wide/from16 v18, v22

    invoke-direct/range {v5 .. v23}, Lf/h/a/c/i0;-><init>(Lf/h/a/c/t0;Lf/h/a/c/d1/p$a;JJILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;Lf/h/a/c/d1/p$a;JJJ)V

    return-object v1
.end method

.method public final K(Lf/h/a/c/s$b;)V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v1, Lf/h/a/c/m;

    invoke-direct {v1, v0, p1}, Lf/h/a/c/m;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    invoke-virtual {p0, v1}, Lf/h/a/c/a0;->L(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final L(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a0;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lf/h/a/c/a0;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v1, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :goto_0
    iget-object p1, p0, Lf/h/a/c/a0;->j:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/a0;->j:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->peekFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    iget-object p1, p0, Lf/h/a/c/a0;->j:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final M(Lf/h/a/c/d1/p$a;J)J
    .locals 2

    invoke-static {p2, p3}, Lf/h/a/c/u;->b(J)J

    move-result-wide p2

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object p1, p1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v1, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    invoke-virtual {v0, p1, v1}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    iget-object p1, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    iget-wide v0, p1, Lf/h/a/c/t0$b;->d:J

    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v0

    add-long/2addr p2, v0

    return-wide p2
.end method

.method public N(ZI)V
    .locals 12

    invoke-virtual {p0}, Lf/h/a/c/s;->s()Z

    move-result v0

    iget-boolean v1, p0, Lf/h/a/c/a0;->k:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget v1, p0, Lf/h/a/c/a0;->l:I

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eq v1, v4, :cond_2

    iget-object v1, p0, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    iget-object v1, v1, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    invoke-virtual {v1, v2, v4, v3}, Lf/h/a/c/i1/x;->a(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    iget-boolean v1, p0, Lf/h/a/c/a0;->k:Z

    if-eq v1, p1, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    iget v1, p0, Lf/h/a/c/a0;->l:I

    if-eq v1, p2, :cond_4

    const/4 v8, 0x1

    goto :goto_3

    :cond_4
    const/4 v8, 0x0

    :goto_3
    iput-boolean p1, p0, Lf/h/a/c/a0;->k:Z

    iput p2, p0, Lf/h/a/c/a0;->l:I

    invoke-virtual {p0}, Lf/h/a/c/s;->s()Z

    move-result v11

    if-eq v0, v11, :cond_5

    const/4 v10, 0x1

    goto :goto_4

    :cond_5
    const/4 v10, 0x0

    :goto_4
    if-nez v5, :cond_6

    if-nez v8, :cond_6

    if-eqz v10, :cond_7

    :cond_6
    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget v7, v0, Lf/h/a/c/i0;->e:I

    new-instance v0, Lf/h/a/c/d;

    move-object v4, v0

    move v6, p1

    move v9, p2

    invoke-direct/range {v4 .. v11}, Lf/h/a/c/d;-><init>(ZZIZIZZ)V

    invoke-virtual {p0, v0}, Lf/h/a/c/a0;->K(Lf/h/a/c/s$b;)V

    :cond_7
    return-void
.end method

.method public final O()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lf/h/a/c/a0;->o:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final P(Lf/h/a/c/i0;ZIIZ)V
    .locals 14

    move-object v0, p0

    invoke-virtual {p0}, Lf/h/a/c/s;->s()Z

    move-result v1

    iget-object v4, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    move-object v3, p1

    iput-object v3, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    invoke-virtual {p0}, Lf/h/a/c/s;->s()Z

    move-result v2

    new-instance v13, Lf/h/a/c/a0$b;

    iget-object v5, v0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v6, v0, Lf/h/a/c/a0;->d:Lf/h/a/c/f1/h;

    iget-boolean v11, v0, Lf/h/a/c/a0;->k:Z

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    const/4 v12, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v12, 0x0

    :goto_0
    move-object v2, v13

    move-object v3, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v2 .. v12}, Lf/h/a/c/a0$b;-><init>(Lf/h/a/c/i0;Lf/h/a/c/i0;Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/f1/h;ZIIZZZ)V

    invoke-virtual {p0, v13}, Lf/h/a/c/a0;->L(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;
    .locals 7

    new-instance v6, Lf/h/a/c/n0;

    iget-object v1, p0, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v3, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {p0}, Lf/h/a/c/a0;->m()I

    move-result v4

    iget-object v5, p0, Lf/h/a/c/a0;->g:Landroid/os/Handler;

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lf/h/a/c/n0;-><init>(Lf/h/a/c/n0$a;Lf/h/a/c/n0$b;Lf/h/a/c/t0;ILandroid/os/Handler;)V

    return-object v6
.end method

.method public b()Lf/h/a/c/j0;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->s:Lf/h/a/c/j0;

    return-object v0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/a0;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    invoke-virtual {v0}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-wide v0, v0, Lf/h/a/c/i0;->l:J

    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public e(IJ)V
    .locals 9

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    if-ltz p1, :cond_5

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lf/h/a/c/t0;->o()I

    move-result v1

    if-ge p1, v1, :cond_5

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/h/a/c/a0;->q:Z

    iget v2, p0, Lf/h/a/c/a0;->o:I

    add-int/2addr v2, v1

    iput v2, p0, Lf/h/a/c/a0;->o:I

    invoke-virtual {p0}, Lf/h/a/c/a0;->c()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const-string p1, "ExoPlayerImpl"

    const-string p2, "seekTo ignored because an ad is playing"

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lf/h/a/c/a0;->e:Landroid/os/Handler;

    const/4 p2, -0x1

    iget-object p3, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    invoke-virtual {p1, v3, v1, p2, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_1
    iput p1, p0, Lf/h/a/c/a0;->u:I

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v1

    const-wide/16 v4, 0x0

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v1, :cond_3

    cmp-long v1, p2, v6

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    move-wide v4, p2

    :goto_0
    iput-wide v4, p0, Lf/h/a/c/a0;->w:J

    iput v3, p0, Lf/h/a/c/a0;->v:I

    goto :goto_2

    :cond_3
    cmp-long v1, p2, v6

    if-nez v1, :cond_4

    iget-object v1, p0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    invoke-virtual {v0, p1, v1, v4, v5}, Lf/h/a/c/t0;->n(ILf/h/a/c/t0$c;J)Lf/h/a/c/t0$c;

    move-result-object v1

    iget-wide v1, v1, Lf/h/a/c/t0$c;->h:J

    goto :goto_1

    :cond_4
    invoke-static {p2, p3}, Lf/h/a/c/u;->a(J)J

    move-result-wide v1

    :goto_1
    move-wide v7, v1

    iget-object v2, p0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    iget-object v3, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    move-object v1, v0

    move v4, p1

    move-wide v5, v7

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/t0;->j(Lf/h/a/c/t0$c;Lf/h/a/c/t0$b;IJ)Landroid/util/Pair;

    move-result-object v1

    invoke-static {v7, v8}, Lf/h/a/c/u;->b(J)J

    move-result-wide v2

    iput-wide v2, p0, Lf/h/a/c/a0;->w:J

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v1

    iput v1, p0, Lf/h/a/c/a0;->v:I

    :goto_2
    iget-object v1, p0, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    invoke-static {p2, p3}, Lf/h/a/c/u;->a(J)J

    move-result-wide p2

    iget-object v1, v1, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    new-instance v2, Lf/h/a/c/b0$e;

    invoke-direct {v2, v0, p1, p2, p3}, Lf/h/a/c/b0$e;-><init>(Lf/h/a/c/t0;IJ)V

    const/4 p1, 0x3

    invoke-virtual {v1, p1, v2}, Lf/h/a/c/i1/x;->b(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    sget-object p1, Lf/h/a/c/c;->a:Lf/h/a/c/c;

    invoke-virtual {p0, p1}, Lf/h/a/c/a0;->K(Lf/h/a/c/s$b;)V

    return-void

    :cond_5
    new-instance v1, Lcom/google/android/exoplayer2/IllegalSeekPositionException;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/google/android/exoplayer2/IllegalSeekPositionException;-><init>(Lf/h/a/c/t0;IJ)V

    throw v1
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/a0;->k:Z

    return v0
.end method

.method public g(Z)V
    .locals 3

    iget-boolean v0, p0, Lf/h/a/c/a0;->n:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lf/h/a/c/a0;->n:Z

    iget-object v0, p0, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    iget-object v0, v0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/4 v1, 0x0

    const/16 v2, 0xd

    invoke-virtual {v0, v2, p1, v1}, Lf/h/a/c/i1/x;->a(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    new-instance v0, Lf/h/a/c/l;

    invoke-direct {v0, p1}, Lf/h/a/c/l;-><init>(Z)V

    invoke-virtual {p0, v0}, Lf/h/a/c/a0;->K(Lf/h/a/c/s$b;)V

    :cond_0
    return-void
.end method

.method public h()Lcom/google/android/exoplayer2/ExoPlaybackException;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    return-object v0
.end method

.method public j(Lf/h/a/c/m0$a;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/s$a;

    invoke-direct {v1, p1}, Lf/h/a/c/s$a;-><init>(Lf/h/a/c/m0$a;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    return-void
.end method

.method public k()I
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/a0;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget v0, v0, Lf/h/a/c/d1/p$a;->c:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public l(Lf/h/a/c/m0$a;)V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/s$a;

    iget-object v2, v1, Lf/h/a/c/s$a;->a:Lf/h/a/c/m0$a;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v1, Lf/h/a/c/s$a;->b:Z

    iget-object v2, p0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public m()I
    .locals 3

    invoke-virtual {p0}, Lf/h/a/c/a0;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lf/h/a/c/a0;->u:I

    return v0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v1, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-object v0, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    invoke-virtual {v1, v0, v2}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object v0

    iget v0, v0, Lf/h/a/c/t0$b;->b:I

    return v0
.end method

.method public n(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lf/h/a/c/a0;->N(ZI)V

    return-void
.end method

.method public o()Lf/h/a/c/m0$c;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public p()J
    .locals 6

    invoke-virtual {p0}, Lf/h/a/c/a0;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v1, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-object v0, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    invoke-virtual {v1, v0, v2}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-wide v1, v0, Lf/h/a/c/i0;->d:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {p0}, Lf/h/a/c/a0;->m()I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/t0;->m(ILf/h/a/c/t0$c;)Lf/h/a/c/t0$c;

    move-result-object v0

    iget-wide v0, v0, Lf/h/a/c/t0$c;->h:J

    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    iget-wide v0, v0, Lf/h/a/c/t0$b;->d:J

    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v0

    iget-object v2, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-wide v2, v2, Lf/h/a/c/i0;->d:J

    invoke-static {v2, v3}, Lf/h/a/c/u;->b(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lf/h/a/c/a0;->G()J

    move-result-wide v0

    return-wide v0
.end method

.method public r()I
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    return v0
.end method

.method public t()I
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/a0;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget v0, v0, Lf/h/a/c/d1/p$a;->b:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public u(I)V
    .locals 3

    iget v0, p0, Lf/h/a/c/a0;->m:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lf/h/a/c/a0;->m:I

    iget-object v0, p0, Lf/h/a/c/a0;->f:Lf/h/a/c/b0;

    iget-object v0, v0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lf/h/a/c/i1/x;->a(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    new-instance v0, Lf/h/a/c/n;

    invoke-direct {v0, p1}, Lf/h/a/c/n;-><init>(I)V

    invoke-virtual {p0, v0}, Lf/h/a/c/a0;->K(Lf/h/a/c/s$b;)V

    :cond_0
    return-void
.end method

.method public w()I
    .locals 1

    iget v0, p0, Lf/h/a/c/a0;->l:I

    return v0
.end method

.method public x()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    return-object v0
.end method

.method public y()I
    .locals 1

    iget v0, p0, Lf/h/a/c/a0;->m:I

    return v0
.end method

.method public z()J
    .locals 4

    invoke-virtual {p0}, Lf/h/a/c/a0;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v1, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v2, v1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v3, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    invoke-virtual {v0, v2, v3}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    iget-object v0, p0, Lf/h/a/c/a0;->i:Lf/h/a/c/t0$b;

    iget v2, v1, Lf/h/a/c/d1/p$a;->b:I

    iget v1, v1, Lf/h/a/c/d1/p$a;->c:I

    invoke-virtual {v0, v2, v1}, Lf/h/a/c/t0$b;->a(II)J

    move-result-wide v0

    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v0

    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lf/h/a/c/a0;->A()Lf/h/a/c/t0;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/h/a/c/a0;->m()I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/s;->a:Lf/h/a/c/t0$c;

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/t0;->m(ILf/h/a/c/t0$c;)Lf/h/a/c/t0$c;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/c/t0$c;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method
