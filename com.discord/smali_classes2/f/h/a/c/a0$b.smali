.class public final Lf/h/a/c/a0$b;
.super Ljava/lang/Object;
.source "ExoPlayerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final d:Lf/h/a/c/i0;

.field public final e:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lf/h/a/c/s$a;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lf/h/a/c/f1/h;

.field public final g:Z

.field public final h:I

.field public final i:I

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:Z

.field public final q:Z


# direct methods
.method public constructor <init>(Lf/h/a/c/i0;Lf/h/a/c/i0;Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/f1/h;ZIIZZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/c/i0;",
            "Lf/h/a/c/i0;",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lf/h/a/c/s$a;",
            ">;",
            "Lf/h/a/c/f1/h;",
            "ZIIZZZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a0$b;->d:Lf/h/a/c/i0;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0, p3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-object p4, p0, Lf/h/a/c/a0$b;->f:Lf/h/a/c/f1/h;

    iput-boolean p5, p0, Lf/h/a/c/a0$b;->g:Z

    iput p6, p0, Lf/h/a/c/a0$b;->h:I

    iput p7, p0, Lf/h/a/c/a0$b;->i:I

    iput-boolean p8, p0, Lf/h/a/c/a0$b;->j:Z

    iput-boolean p9, p0, Lf/h/a/c/a0$b;->p:Z

    iput-boolean p10, p0, Lf/h/a/c/a0$b;->q:Z

    iget p3, p2, Lf/h/a/c/i0;->e:I

    iget p4, p1, Lf/h/a/c/i0;->e:I

    const/4 p5, 0x1

    const/4 p6, 0x0

    if-eq p3, p4, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    iput-boolean p3, p0, Lf/h/a/c/a0$b;->k:Z

    iget-object p3, p2, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-object p4, p1, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    if-eq p3, p4, :cond_1

    if-eqz p4, :cond_1

    const/4 p3, 0x1

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    :goto_1
    iput-boolean p3, p0, Lf/h/a/c/a0$b;->l:Z

    iget-object p3, p2, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object p4, p1, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    if-eq p3, p4, :cond_2

    const/4 p3, 0x1

    goto :goto_2

    :cond_2
    const/4 p3, 0x0

    :goto_2
    iput-boolean p3, p0, Lf/h/a/c/a0$b;->m:Z

    iget-boolean p3, p2, Lf/h/a/c/i0;->g:Z

    iget-boolean p4, p1, Lf/h/a/c/i0;->g:Z

    if-eq p3, p4, :cond_3

    const/4 p3, 0x1

    goto :goto_3

    :cond_3
    const/4 p3, 0x0

    :goto_3
    iput-boolean p3, p0, Lf/h/a/c/a0$b;->n:Z

    iget-object p2, p2, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    iget-object p1, p1, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    if-eq p2, p1, :cond_4

    goto :goto_4

    :cond_4
    const/4 p5, 0x0

    :goto_4
    iput-boolean p5, p0, Lf/h/a/c/a0$b;->o:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-boolean v0, p0, Lf/h/a/c/a0$b;->m:Z

    if-nez v0, :cond_0

    iget v0, p0, Lf/h/a/c/a0$b;->i:I

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/f;

    invoke-direct {v1, p0}, Lf/h/a/c/f;-><init>(Lf/h/a/c/a0$b;)V

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_1
    iget-boolean v0, p0, Lf/h/a/c/a0$b;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/h;

    invoke-direct {v1, p0}, Lf/h/a/c/h;-><init>(Lf/h/a/c/a0$b;)V

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_2
    iget-boolean v0, p0, Lf/h/a/c/a0$b;->l:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/e;

    invoke-direct {v1, p0}, Lf/h/a/c/e;-><init>(Lf/h/a/c/a0$b;)V

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_3
    iget-boolean v0, p0, Lf/h/a/c/a0$b;->o:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/h/a/c/a0$b;->f:Lf/h/a/c/f1/h;

    iget-object v1, p0, Lf/h/a/c/a0$b;->d:Lf/h/a/c/i0;

    iget-object v1, v1, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    iget-object v1, v1, Lf/h/a/c/f1/i;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lf/h/a/c/f1/h;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/i;

    invoke-direct {v1, p0}, Lf/h/a/c/i;-><init>(Lf/h/a/c/a0$b;)V

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_4
    iget-boolean v0, p0, Lf/h/a/c/a0$b;->n:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/g;

    invoke-direct {v1, p0}, Lf/h/a/c/g;-><init>(Lf/h/a/c/a0$b;)V

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_5
    iget-boolean v0, p0, Lf/h/a/c/a0$b;->k:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/k;

    invoke-direct {v1, p0}, Lf/h/a/c/k;-><init>(Lf/h/a/c/a0$b;)V

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_6
    iget-boolean v0, p0, Lf/h/a/c/a0$b;->q:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/j;

    invoke-direct {v1, p0}, Lf/h/a/c/j;-><init>(Lf/h/a/c/a0$b;)V

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_7
    iget-boolean v0, p0, Lf/h/a/c/a0$b;->j:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lf/h/a/c/a0$b;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    sget-object v1, Lf/h/a/c/p;->a:Lf/h/a/c/p;

    invoke-static {v0, v1}, Lf/h/a/c/a0;->J(Ljava/util/concurrent/CopyOnWriteArrayList;Lf/h/a/c/s$b;)V

    :cond_8
    return-void
.end method
