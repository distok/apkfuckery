.class public abstract Lf/h/a/c/h1/f;
.super Ljava/lang/Object;
.source "BaseDataSource.java"

# interfaces
.implements Lf/h/a/c/h1/j;


# instance fields
.field public final a:Z

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lf/h/a/c/h1/x;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Lf/h/a/c/h1/k;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lf/h/a/c/h1/f;->a:Z

    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/h1/f;->b:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final b(Lf/h/a/c/h1/x;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/h1/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/h1/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget p1, p0, Lf/h/a/c/h1/f;->c:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lf/h/a/c/h1/f;->c:I

    :cond_0
    return-void
.end method

.method public synthetic c()Ljava/util/Map;
    .locals 1

    invoke-static {p0}, Lf/h/a/c/h1/i;->a(Lf/h/a/c/h1/j;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/h1/f;->d:Lf/h/a/c/h1/k;

    sget v1, Lf/h/a/c/i1/a0;->a:I

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lf/h/a/c/h1/f;->c:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lf/h/a/c/h1/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/h1/x;

    iget-boolean v3, p0, Lf/h/a/c/h1/f;->a:Z

    invoke-interface {v2, p0, v0, v3, p1}, Lf/h/a/c/h1/x;->e(Lf/h/a/c/h1/j;Lf/h/a/c/h1/k;ZI)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/h1/f;->d:Lf/h/a/c/h1/k;

    sget v1, Lf/h/a/c/i1/a0;->a:I

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lf/h/a/c/h1/f;->c:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lf/h/a/c/h1/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/h1/x;

    iget-boolean v3, p0, Lf/h/a/c/h1/f;->a:Z

    invoke-interface {v2, p0, v0, v3}, Lf/h/a/c/h1/x;->a(Lf/h/a/c/h1/j;Lf/h/a/c/h1/k;Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/h1/f;->d:Lf/h/a/c/h1/k;

    return-void
.end method

.method public final g(Lf/h/a/c/h1/k;)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lf/h/a/c/h1/f;->c:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/h/a/c/h1/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/h1/x;

    iget-boolean v2, p0, Lf/h/a/c/h1/f;->a:Z

    invoke-interface {v1, p0, p1, v2}, Lf/h/a/c/h1/x;->g(Lf/h/a/c/h1/j;Lf/h/a/c/h1/k;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final h(Lf/h/a/c/h1/k;)V
    .locals 3

    iput-object p1, p0, Lf/h/a/c/h1/f;->d:Lf/h/a/c/h1/k;

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lf/h/a/c/h1/f;->c:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/h/a/c/h1/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/h1/x;

    iget-boolean v2, p0, Lf/h/a/c/h1/f;->a:Z

    invoke-interface {v1, p0, p1, v2}, Lf/h/a/c/h1/x;->b(Lf/h/a/c/h1/j;Lf/h/a/c/h1/k;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
