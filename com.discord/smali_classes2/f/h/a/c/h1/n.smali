.class public final Lf/h/a/c/h1/n;
.super Ljava/lang/Object;
.source "DefaultDataSource.java"

# interfaces
.implements Lf/h/a/c/h1/j;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/c/h1/x;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/c/h1/j;

.field public d:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public e:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public f:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public g:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public h:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public i:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public j:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public k:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/a/c/h1/j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/h1/n;->a:Landroid/content/Context;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lf/h/a/c/h1/n;->c:Lf/h/a/c/h1/j;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lf/h/a/c/h1/n;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/h1/k;)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iget-object v0, p1, Lf/h/a/c/h1/k;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p1, Lf/h/a/c/h1/k;->a:Landroid/net/Uri;

    sget v4, Lf/h/a/c/i1/a0;->a:I

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "file"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-eqz v1, :cond_6

    iget-object v0, p1, Lf/h/a/c/h1/k;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, "/android_asset/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/h/a/c/h1/n;->e:Lf/h/a/c/h1/j;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/exoplayer2/upstream/AssetDataSource;

    iget-object v1, p0, Lf/h/a/c/h1/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/AssetDataSource;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lf/h/a/c/h1/n;->e:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V

    :cond_3
    iget-object v0, p0, Lf/h/a/c/h1/n;->e:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto/16 :goto_3

    :cond_4
    iget-object v0, p0, Lf/h/a/c/h1/n;->d:Lf/h/a/c/h1/j;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/exoplayer2/upstream/FileDataSource;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/upstream/FileDataSource;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/n;->d:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V

    :cond_5
    iget-object v0, p0, Lf/h/a/c/h1/n;->d:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto/16 :goto_3

    :cond_6
    const-string v1, "asset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v0, p0, Lf/h/a/c/h1/n;->e:Lf/h/a/c/h1/j;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/exoplayer2/upstream/AssetDataSource;

    iget-object v1, p0, Lf/h/a/c/h1/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/AssetDataSource;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lf/h/a/c/h1/n;->e:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V

    :cond_7
    iget-object v0, p0, Lf/h/a/c/h1/n;->e:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto/16 :goto_3

    :cond_8
    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v0, p0, Lf/h/a/c/h1/n;->f:Lf/h/a/c/h1/j;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/exoplayer2/upstream/ContentDataSource;

    iget-object v1, p0, Lf/h/a/c/h1/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/ContentDataSource;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lf/h/a/c/h1/n;->f:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V

    :cond_9
    iget-object v0, p0, Lf/h/a/c/h1/n;->f:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto/16 :goto_3

    :cond_a
    const-string v1, "rtmp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v0, p0, Lf/h/a/c/h1/n;->g:Lf/h/a/c/h1/j;

    if-nez v0, :cond_b

    :try_start_0
    const-string v0, "com.google.android.exoplayer2.ext.rtmp.RtmpDataSource"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->g:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error instantiating RTMP extension"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    const-string v0, "DefaultDataSource"

    const-string v1, "Attempting to play RTMP stream without depending on the RTMP extension"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v0, p0, Lf/h/a/c/h1/n;->g:Lf/h/a/c/h1/j;

    if-nez v0, :cond_b

    iget-object v0, p0, Lf/h/a/c/h1/n;->c:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->g:Lf/h/a/c/h1/j;

    :cond_b
    iget-object v0, p0, Lf/h/a/c/h1/n;->g:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto :goto_3

    :cond_c
    const-string v1, "udp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v0, p0, Lf/h/a/c/h1/n;->h:Lf/h/a/c/h1/j;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/android/exoplayer2/upstream/UdpDataSource;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/upstream/UdpDataSource;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/n;->h:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V

    :cond_d
    iget-object v0, p0, Lf/h/a/c/h1/n;->h:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto :goto_3

    :cond_e
    const-string v1, "data"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v0, p0, Lf/h/a/c/h1/n;->i:Lf/h/a/c/h1/j;

    if-nez v0, :cond_f

    new-instance v0, Lf/h/a/c/h1/g;

    invoke-direct {v0}, Lf/h/a/c/h1/g;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/n;->i:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V

    :cond_f
    iget-object v0, p0, Lf/h/a/c/h1/n;->i:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto :goto_3

    :cond_10
    const-string v1, "rawresource"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lf/h/a/c/h1/n;->j:Lf/h/a/c/h1/j;

    if-nez v0, :cond_11

    new-instance v0, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;

    iget-object v1, p0, Lf/h/a/c/h1/n;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/RawResourceDataSource;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lf/h/a/c/h1/n;->j:Lf/h/a/c/h1/j;

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/n;->e(Lf/h/a/c/h1/j;)V

    :cond_11
    iget-object v0, p0, Lf/h/a/c/h1/n;->j:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto :goto_3

    :cond_12
    iget-object v0, p0, Lf/h/a/c/h1/n;->c:Lf/h/a/c/h1/j;

    iput-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    :goto_3
    iget-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->a(Lf/h/a/c/h1/k;)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(Lf/h/a/c/h1/x;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/h1/n;->c:Lf/h/a/c/h1/j;

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    iget-object v0, p0, Lf/h/a/c/h1/n;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lf/h/a/c/h1/n;->d:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_0
    iget-object v0, p0, Lf/h/a/c/h1/n;->e:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_1
    iget-object v0, p0, Lf/h/a/c/h1/n;->f:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_2
    iget-object v0, p0, Lf/h/a/c/h1/n;->g:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_3

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_3
    iget-object v0, p0, Lf/h/a/c/h1/n;->h:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_4

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_4
    iget-object v0, p0, Lf/h/a/c/h1/n;->i:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_5

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_5
    iget-object v0, p0, Lf/h/a/c/h1/n;->j:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_6

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_6
    return-void
.end method

.method public c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lf/h/a/c/h1/j;->c()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0}, Lf/h/a/c/h1/j;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v1, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    throw v0

    :cond_0
    :goto_0
    return-void
.end method

.method public d()Landroid/net/Uri;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lf/h/a/c/h1/j;->d()Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final e(Lf/h/a/c/h1/j;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/h/a/c/h1/n;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/h/a/c/h1/n;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/h1/x;

    invoke-interface {p1, v1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/n;->k:Lf/h/a/c/h1/j;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, p1, p2, p3}, Lf/h/a/c/h1/j;->read([BII)I

    move-result p1

    return p1
.end method
