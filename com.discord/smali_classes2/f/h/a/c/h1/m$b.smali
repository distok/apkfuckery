.class public Lf/h/a/c/h1/m$b;
.super Landroid/content/BroadcastReceiver;
.source "DefaultBandwidthMeter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/h1/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# static fields
.field public static c:Lf/h/a/c/h1/m$b;


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Lf/h/a/c/h1/m;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lf/h/a/c/h1/m$b;->a:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/m$b;->b:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/h1/m$b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lf/h/a/c/h1/m$b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/h1/m;

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/h/a/c/h1/m$b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(Lf/h/a/c/h1/m;)V
    .locals 10

    sget-object v0, Lf/h/a/c/h1/m;->n:Ljava/util/Map;

    monitor-enter p1

    :try_start_0
    iget-object v0, p1, Lf/h/a/c/h1/m;->a:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lf/h/a/c/i1/a0;->j(Landroid/content/Context;)I

    move-result v0

    :goto_0
    iget v2, p1, Lf/h/a/c/h1/m;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v0, :cond_1

    monitor-exit p1

    goto :goto_3

    :cond_1
    :try_start_1
    iput v0, p1, Lf/h/a/c/h1/m;->i:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    if-eqz v0, :cond_4

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p1, v0}, Lf/h/a/c/h1/m;->h(I)J

    move-result-wide v2

    iput-wide v2, p1, Lf/h/a/c/h1/m;->l:J

    iget-object v0, p1, Lf/h/a/c/h1/m;->e:Lf/h/a/c/i1/f;

    invoke-interface {v0}, Lf/h/a/c/i1/f;->c()J

    move-result-wide v2

    iget v0, p1, Lf/h/a/c/h1/m;->f:I

    if-lez v0, :cond_3

    iget-wide v4, p1, Lf/h/a/c/h1/m;->g:J

    sub-long v4, v2, v4

    long-to-int v0, v4

    move v5, v0

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :goto_1
    iget-wide v6, p1, Lf/h/a/c/h1/m;->h:J

    iget-wide v8, p1, Lf/h/a/c/h1/m;->l:J

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Lf/h/a/c/h1/m;->i(IJJ)V

    iput-wide v2, p1, Lf/h/a/c/h1/m;->g:J

    const-wide/16 v2, 0x0

    iput-wide v2, p1, Lf/h/a/c/h1/m;->h:J

    iput-wide v2, p1, Lf/h/a/c/h1/m;->k:J

    iput-wide v2, p1, Lf/h/a/c/h1/m;->j:J

    iget-object v0, p1, Lf/h/a/c/h1/m;->d:Lf/h/a/c/i1/u;

    iget-object v2, v0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v2, -0x1

    iput v2, v0, Lf/h/a/c/i1/u;->d:I

    iput v1, v0, Lf/h/a/c/i1/u;->e:I

    iput v1, v0, Lf/h/a/c/i1/u;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p1

    goto :goto_3

    :cond_4
    :goto_2
    monitor-exit p1

    :goto_3
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->isInitialStickyBroadcast()Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lf/h/a/c/h1/m$b;->a()V

    const/4 p1, 0x0

    :goto_0
    iget-object p2, p0, Lf/h/a/c/h1/m$b;->b:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-ge p1, p2, :cond_2

    iget-object p2, p0, Lf/h/a/c/h1/m$b;->b:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/h1/m;

    if-eqz p2, :cond_1

    invoke-virtual {p0, p2}, Lf/h/a/c/h1/m$b;->b(Lf/h/a/c/h1/m;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
