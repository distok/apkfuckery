.class public final Lf/h/a/c/h1/q;
.super Lf/h/a/c/h1/s;
.source "DefaultHttpDataSourceFactory.java"


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lf/h/a/c/h1/x;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lf/h/a/c/h1/x;)V
    .locals 1
    .param p2    # Lf/h/a/c/h1/x;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lf/h/a/c/h1/s;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lf/h/a/c/h1/q;->b:Ljava/lang/String;

    iput-object p2, p0, Lf/h/a/c/h1/q;->c:Lf/h/a/c/h1/x;

    const/16 p1, 0x1f40

    iput p1, p0, Lf/h/a/c/h1/q;->d:I

    iput p1, p0, Lf/h/a/c/h1/q;->e:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method
