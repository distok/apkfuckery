.class public final Lf/h/a/c/h1/v;
.super Ljava/lang/Object;
.source "StatsDataSource.java"

# interfaces
.implements Lf/h/a/c/h1/j;


# instance fields
.field public final a:Lf/h/a/c/h1/j;

.field public b:J

.field public c:Landroid/net/Uri;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/c/h1/j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    sget-object p1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object p1, p0, Lf/h/a/c/h1/v;->c:Landroid/net/Uri;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/h1/v;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/h1/k;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p1, Lf/h/a/c/h1/k;->a:Landroid/net/Uri;

    iput-object v0, p0, Lf/h/a/c/h1/v;->c:Landroid/net/Uri;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/h1/v;->d:Ljava/util/Map;

    iget-object v0, p0, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->a(Lf/h/a/c/h1/k;)J

    move-result-wide v0

    invoke-virtual {p0}, Lf/h/a/c/h1/v;->d()Landroid/net/Uri;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/c/h1/v;->c:Landroid/net/Uri;

    invoke-virtual {p0}, Lf/h/a/c/h1/v;->c()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/h1/v;->d:Ljava/util/Map;

    return-wide v0
.end method

.method public b(Lf/h/a/c/h1/x;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    return-void
.end method

.method public c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v0}, Lf/h/a/c/h1/j;->c()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v0}, Lf/h/a/c/h1/j;->close()V

    return-void
.end method

.method public d()Landroid/net/Uri;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v0}, Lf/h/a/c/h1/j;->d()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public read([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v0, p1, p2, p3}, Lf/h/a/c/h1/j;->read([BII)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    iget-wide p2, p0, Lf/h/a/c/h1/v;->b:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Lf/h/a/c/h1/v;->b:J

    :cond_0
    return p1
.end method
