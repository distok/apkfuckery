.class public Lf/h/a/c/h1/y/k;
.super Ljava/lang/Object;
.source "CachedContentIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/h1/y/k$a;,
        Lf/h/a/c/h1/y/k$b;,
        Lf/h/a/c/h1/y/k$c;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lf/h/a/c/h1/y/j;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/util/SparseBooleanArray;

.field public final d:Landroid/util/SparseBooleanArray;

.field public e:Lf/h/a/c/h1/y/k$c;

.field public f:Lf/h/a/c/h1/y/k$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/c/x0/a;Ljava/io/File;[BZZ)V
    .locals 3
    .param p1    # Lf/h/a/c/x0/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/io/File;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [B
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/y/k;->c:Landroid/util/SparseBooleanArray;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h1/y/k;->d:Landroid/util/SparseBooleanArray;

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    new-instance v1, Lf/h/a/c/h1/y/k$a;

    invoke-direct {v1, p1}, Lf/h/a/c/h1/y/k$a;-><init>(Lf/h/a/c/x0/a;)V

    goto :goto_2

    :cond_2
    move-object v1, v0

    :goto_2
    if-eqz p2, :cond_3

    new-instance v0, Lf/h/a/c/h1/y/k$b;

    new-instance p1, Ljava/io/File;

    const-string v2, "cached_content_index.exi"

    invoke-direct {p1, p2, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, p1, p3, p4}, Lf/h/a/c/h1/y/k$b;-><init>(Ljava/io/File;[BZ)V

    :cond_3
    if-eqz v1, :cond_5

    if-eqz v0, :cond_4

    if-eqz p5, :cond_4

    goto :goto_3

    :cond_4
    iput-object v1, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    iput-object v0, p0, Lf/h/a/c/h1/y/k;->f:Lf/h/a/c/h1/y/k$c;

    goto :goto_4

    :cond_5
    :goto_3
    iput-object v0, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    iput-object v1, p0, Lf/h/a/c/h1/y/k;->f:Lf/h/a/c/h1/y/k$c;

    :goto_4
    return-void
.end method

.method public static a(Ljava/io/DataInputStream;)Lf/h/a/c/h1/y/o;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    if-ltz v4, :cond_1

    const/high16 v5, 0xa00000

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v6

    sget-object v7, Lf/h/a/c/i1/a0;->f:[B

    const/4 v8, 0x0

    :goto_1
    if-eq v8, v4, :cond_0

    add-int v9, v8, v6

    invoke-static {v7, v9}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v7

    invoke-virtual {p0, v7, v8, v6}, Ljava/io/DataInputStream;->readFully([BII)V

    sub-int v6, v4, v9

    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v8, v9

    goto :goto_1

    :cond_0
    invoke-virtual {v1, v3, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/io/IOException;

    const-string v0, "Invalid value size: "

    invoke-static {v0, v4}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance p0, Lf/h/a/c/h1/y/o;

    invoke-direct {p0, v1}, Lf/h/a/c/h1/y/o;-><init>(Ljava/util/Map;)V

    return-object p0
.end method

.method public static b(Lf/h/a/c/h1/y/o;Ljava/io/DataOutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p0, p0, Lf/h/a/c/h1/y/o;->b:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v1, v0

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->write([B)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public c(Ljava/lang/String;)Lf/h/a/c/h1/y/j;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/c/h1/y/j;

    return-object p1
.end method

.method public d(Ljava/lang/String;)Lf/h/a/c/h1/y/j;
    .locals 5

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/h1/y/j;

    if-nez v0, :cond_4

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    add-int/2addr v4, v2

    :goto_0
    if-gez v4, :cond_3

    :goto_1
    if-ge v3, v1, :cond_2

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    if-eq v3, v4, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    move v4, v3

    :cond_3
    new-instance v0, Lf/h/a/c/h1/y/j;

    sget-object v1, Lf/h/a/c/h1/y/o;->c:Lf/h/a/c/h1/y/o;

    invoke-direct {v0, v4, p1, v1}, Lf/h/a/c/h1/y/j;-><init>(ILjava/lang/String;Lf/h/a/c/h1/y/o;)V

    iget-object v1, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v4, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object p1, p0, Lf/h/a/c/h1/y/k;->d:Landroid/util/SparseBooleanArray;

    invoke-virtual {p1, v4, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    iget-object p1, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    invoke-interface {p1, v0}, Lf/h/a/c/h1/y/k$c;->c(Lf/h/a/c/h1/y/j;)V

    :cond_4
    return-object v0
.end method

.method public e(J)V
    .locals 1
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    invoke-interface {v0, p1, p2}, Lf/h/a/c/h1/y/k$c;->f(J)V

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->f:Lf/h/a/c/h1/y/k$c;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lf/h/a/c/h1/y/k$c;->f(J)V

    :cond_0
    iget-object p1, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    invoke-interface {p1}, Lf/h/a/c/h1/y/k$c;->d()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/h1/y/k;->f:Lf/h/a/c/h1/y/k$c;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lf/h/a/c/h1/y/k$c;->d()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/h1/y/k;->f:Lf/h/a/c/h1/y/k$c;

    iget-object p2, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    invoke-interface {p1, p2, v0}, Lf/h/a/c/h1/y/k$c;->g(Ljava/util/HashMap;Landroid/util/SparseArray;)V

    iget-object p1, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    iget-object p2, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    invoke-interface {p1, p2}, Lf/h/a/c/h1/y/k$c;->b(Ljava/util/HashMap;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    iget-object p2, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    invoke-interface {p1, p2, v0}, Lf/h/a/c/h1/y/k$c;->g(Ljava/util/HashMap;Landroid/util/SparseArray;)V

    :goto_0
    iget-object p1, p0, Lf/h/a/c/h1/y/k;->f:Lf/h/a/c/h1/y/k$c;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lf/h/a/c/h1/y/k$c;->h()V

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/h1/y/k;->f:Lf/h/a/c/h1/y/k$c;

    :cond_2
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/h1/y/j;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lf/h/a/c/h1/y/j;->c:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lf/h/a/c/h1/y/j;->e:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget p1, v0, Lf/h/a/c/h1/y/j;->a:I

    iget-object v1, p0, Lf/h/a/c/h1/y/k;->d:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    iget-object v2, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    invoke-interface {v2, v0, v1}, Lf/h/a/c/h1/y/k$c;->a(Lf/h/a/c/h1/y/j;Z)V

    if-eqz v1, :cond_0

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->d:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->c:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    :cond_1
    :goto_0
    return-void
.end method

.method public g()V
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->e:Lf/h/a/c/h1/y/k$c;

    iget-object v1, p0, Lf/h/a/c/h1/y/k;->a:Ljava/util/HashMap;

    invoke-interface {v0, v1}, Lf/h/a/c/h1/y/k$c;->e(Ljava/util/HashMap;)V

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lf/h/a/c/h1/y/k;->b:Landroid/util/SparseArray;

    iget-object v3, p0, Lf/h/a/c/h1/y/k;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->remove(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/h1/y/k;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    iget-object v0, p0, Lf/h/a/c/h1/y/k;->d:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    return-void
.end method
