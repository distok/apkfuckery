.class public final Lf/h/a/c/h1/y/c;
.super Ljava/lang/Object;
.source "CacheDataSource.java"

# interfaces
.implements Lf/h/a/c/h1/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/h1/y/c$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

.field public final b:Lf/h/a/c/h1/j;

.field public final c:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lf/h/a/c/h1/j;

.field public final e:Lf/h/a/c/h1/y/g;

.field public final f:Lf/h/a/c/h1/y/c$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public j:Lf/h/a/c/h1/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Landroid/net/Uri;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public m:Landroid/net/Uri;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:[B
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public q:I

.field public r:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public s:J

.field public t:J

.field public u:Lf/h/a/c/h1/y/h;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:Z

.field public x:J

.field public y:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/upstream/cache/Cache;Lf/h/a/c/h1/j;Lf/h/a/c/h1/j;Lf/h/a/c/h1/h;ILf/h/a/c/h1/y/c$a;)V
    .locals 0
    .param p4    # Lf/h/a/c/h1/h;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lf/h/a/c/h1/y/c$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p6

    iput-object p6, p0, Lf/h/a/c/h1/y/c;->p:Ljava/util/Map;

    iput-object p1, p0, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    iput-object p3, p0, Lf/h/a/c/h1/y/c;->b:Lf/h/a/c/h1/j;

    sget p1, Lf/h/a/c/h1/y/i;->a:I

    sget-object p1, Lf/h/a/c/h1/y/a;->a:Lf/h/a/c/h1/y/a;

    iput-object p1, p0, Lf/h/a/c/h1/y/c;->e:Lf/h/a/c/h1/y/g;

    and-int/lit8 p1, p5, 0x1

    const/4 p3, 0x0

    const/4 p6, 0x1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lf/h/a/c/h1/y/c;->g:Z

    and-int/lit8 p1, p5, 0x2

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput-boolean p1, p0, Lf/h/a/c/h1/y/c;->h:Z

    and-int/lit8 p1, p5, 0x4

    if-eqz p1, :cond_2

    const/4 p3, 0x1

    :cond_2
    iput-boolean p3, p0, Lf/h/a/c/h1/y/c;->i:Z

    iput-object p2, p0, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    new-instance p1, Lf/h/a/c/h1/w;

    invoke-direct {p1, p2, p4}, Lf/h/a/c/h1/w;-><init>(Lf/h/a/c/h1/j;Lf/h/a/c/h1/h;)V

    iput-object p1, p0, Lf/h/a/c/h1/y/c;->c:Lf/h/a/c/h1/j;

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/h1/y/c;->f:Lf/h/a/c/h1/y/c$a;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/h1/k;)J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/a/c/h1/y/c;->e:Lf/h/a/c/h1/y/g;

    check-cast v0, Lf/h/a/c/h1/y/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lf/h/a/c/h1/y/i;->a:I

    iget-object v0, p1, Lf/h/a/c/h1/k;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lf/h/a/c/h1/k;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    iget-object v1, p1, Lf/h/a/c/h1/k;->a:Landroid/net/Uri;

    iput-object v1, p0, Lf/h/a/c/h1/y/c;->l:Landroid/net/Uri;

    iget-object v2, p0, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->b(Ljava/lang/String;)Lf/h/a/c/h1/y/m;

    move-result-object v0

    check-cast v0, Lf/h/a/c/h1/y/o;

    iget-object v2, v0, Lf/h/a/c/h1/y/o;->b:Ljava/util/Map;

    const-string v3, "exo_redir"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    iget-object v0, v0, Lf/h/a/c/h1/y/o;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_1

    :cond_1
    move-object v2, v4

    :goto_1
    if-nez v2, :cond_2

    goto :goto_2

    :cond_2
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :goto_2
    if-eqz v4, :cond_3

    move-object v1, v4

    :cond_3
    iput-object v1, p0, Lf/h/a/c/h1/y/c;->m:Landroid/net/Uri;

    iget v0, p1, Lf/h/a/c/h1/k;->b:I

    iput v0, p0, Lf/h/a/c/h1/y/c;->n:I

    iget-object v0, p1, Lf/h/a/c/h1/k;->c:[B

    iput-object v0, p0, Lf/h/a/c/h1/y/c;->o:[B

    iget-object v0, p1, Lf/h/a/c/h1/k;->d:Ljava/util/Map;

    iput-object v0, p0, Lf/h/a/c/h1/y/c;->p:Ljava/util/Map;

    iget v0, p1, Lf/h/a/c/h1/k;->i:I

    iput v0, p0, Lf/h/a/c/h1/y/c;->q:I

    iget-wide v0, p1, Lf/h/a/c/h1/k;->f:J

    iput-wide v0, p0, Lf/h/a/c/h1/y/c;->s:J

    iget-boolean v0, p0, Lf/h/a/c/h1/y/c;->h:Z

    const/4 v1, 0x1

    const/4 v2, -0x1

    const-wide/16 v3, -0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lf/h/a/c/h1/y/c;->v:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    iget-boolean v0, p0, Lf/h/a/c/h1/y/c;->i:Z

    if-eqz v0, :cond_5

    iget-wide v6, p1, Lf/h/a/c/h1/k;->g:J

    cmp-long v0, v6, v3

    if-nez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_3

    :cond_5
    const/4 v0, -0x1

    :goto_3
    if-eq v0, v2, :cond_6

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    :goto_4
    iput-boolean v1, p0, Lf/h/a/c/h1/y/c;->w:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lf/h/a/c/h1/y/c;->f:Lf/h/a/c/h1/y/c$a;

    if-eqz v1, :cond_7

    invoke-interface {v1, v0}, Lf/h/a/c/h1/y/c$a;->a(I)V

    :cond_7
    iget-wide v0, p1, Lf/h/a/c/h1/k;->g:J

    cmp-long v2, v0, v3

    if-nez v2, :cond_a

    iget-boolean v2, p0, Lf/h/a/c/h1/y/c;->w:Z

    if-eqz v2, :cond_8

    goto :goto_5

    :cond_8
    iget-object v0, p0, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    iget-object v1, p0, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->b(Ljava/lang/String;)Lf/h/a/c/h1/y/m;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/c/h1/y/l;->a(Lf/h/a/c/h1/y/m;)J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/c/h1/y/c;->t:J

    cmp-long v2, v0, v3

    if-eqz v2, :cond_b

    iget-wide v2, p1, Lf/h/a/c/h1/k;->f:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lf/h/a/c/h1/y/c;->t:J

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_9

    goto :goto_6

    :cond_9
    new-instance p1, Lcom/google/android/exoplayer2/upstream/DataSourceException;

    invoke-direct {p1, v5}, Lcom/google/android/exoplayer2/upstream/DataSourceException;-><init>(I)V

    throw p1

    :cond_a
    :goto_5
    iput-wide v0, p0, Lf/h/a/c/h1/y/c;->t:J

    :cond_b
    :goto_6
    invoke-virtual {p0, v5}, Lf/h/a/c/h1/y/c;->h(Z)V

    iget-wide v0, p0, Lf/h/a/c/h1/y/c;->t:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v0

    :catchall_0
    move-exception p1

    invoke-virtual {p0, p1}, Lf/h/a/c/h1/y/c;->f(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public b(Lf/h/a/c/h1/x;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->b:Lf/h/a/c/h1/j;

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    invoke-interface {v0, p1}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    return-void
.end method

.method public c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/c/h1/y/c;->g()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    invoke-interface {v0}, Lf/h/a/c/h1/j;->c()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public close()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/h1/y/c;->l:Landroid/net/Uri;

    iput-object v0, p0, Lf/h/a/c/h1/y/c;->m:Landroid/net/Uri;

    const/4 v1, 0x1

    iput v1, p0, Lf/h/a/c/h1/y/c;->n:I

    iput-object v0, p0, Lf/h/a/c/h1/y/c;->o:[B

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lf/h/a/c/h1/y/c;->p:Ljava/util/Map;

    const/4 v1, 0x0

    iput v1, p0, Lf/h/a/c/h1/y/c;->q:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lf/h/a/c/h1/y/c;->s:J

    iput-object v0, p0, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->f:Lf/h/a/c/h1/y/c$a;

    if-eqz v0, :cond_0

    iget-wide v3, p0, Lf/h/a/c/h1/y/c;->x:J

    cmp-long v5, v3, v1

    if-lez v5, :cond_0

    iget-object v3, p0, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    invoke-interface {v3}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->f()J

    move-result-wide v3

    iget-wide v5, p0, Lf/h/a/c/h1/y/c;->x:J

    invoke-interface {v0, v3, v4, v5, v6}, Lf/h/a/c/h1/y/c$a;->b(JJ)V

    iput-wide v1, p0, Lf/h/a/c/h1/y/c;->x:J

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lf/h/a/c/h1/y/c;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/y/c;->f(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public d()Landroid/net/Uri;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->m:Landroid/net/Uri;

    return-object v0
.end method

.method public final e()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v0}, Lf/h/a/c/h1/j;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    iput-boolean v1, p0, Lf/h/a/c/h1/y/c;->k:Z

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->u:Lf/h/a/c/h1/y/h;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->h(Lf/h/a/c/h1/y/h;)V

    iput-object v2, p0, Lf/h/a/c/h1/y/c;->u:Lf/h/a/c/h1/y/h;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    iput-boolean v1, p0, Lf/h/a/c/h1/y/c;->k:Z

    iget-object v1, p0, Lf/h/a/c/h1/y/c;->u:Lf/h/a/c/h1/y/h;

    if-eqz v1, :cond_2

    iget-object v3, p0, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    invoke-interface {v3, v1}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->h(Lf/h/a/c/h1/y/h;)V

    iput-object v2, p0, Lf/h/a/c/h1/y/c;->u:Lf/h/a/c/h1/y/h;

    :cond_2
    throw v0
.end method

.method public final f(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/h1/y/c;->g()Z

    move-result v0

    if-nez v0, :cond_0

    instance-of p1, p1, Lcom/google/android/exoplayer2/upstream/cache/Cache$CacheException;

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/h1/y/c;->v:Z

    :cond_1
    return-void
.end method

.method public final g()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    iget-object v1, p0, Lf/h/a/c/h1/y/c;->b:Lf/h/a/c/h1/j;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final h(Z)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-boolean v0, v1, Lf/h/a/c/h1/y/c;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-boolean v0, v1, Lf/h/a/c/h1/y/c;->g:Z

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, v1, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    iget-object v3, v1, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    iget-wide v4, v1, Lf/h/a/c/h1/y/c;->s:J

    invoke-interface {v0, v3, v4, v5}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->g(Ljava/lang/String;J)Lf/h/a/c/h1/y/h;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v1, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    iget-object v3, v1, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    iget-wide v4, v1, Lf/h/a/c/h1/y/c;->s:J

    invoke-interface {v0, v3, v4, v5}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->i(Ljava/lang/String;J)Lf/h/a/c/h1/y/h;

    move-result-object v0

    :goto_0
    const-wide/16 v3, -0x1

    const/4 v6, 0x1

    if-nez v0, :cond_2

    iget-object v7, v1, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    new-instance v21, Lf/h/a/c/h1/k;

    iget-object v9, v1, Lf/h/a/c/h1/y/c;->l:Landroid/net/Uri;

    iget v10, v1, Lf/h/a/c/h1/y/c;->n:I

    iget-object v11, v1, Lf/h/a/c/h1/y/c;->o:[B

    iget-wide v14, v1, Lf/h/a/c/h1/y/c;->s:J

    iget-wide v12, v1, Lf/h/a/c/h1/y/c;->t:J

    iget-object v8, v1, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    iget v2, v1, Lf/h/a/c/h1/y/c;->q:I

    iget-object v5, v1, Lf/h/a/c/h1/y/c;->p:Ljava/util/Map;

    move-object/from16 v18, v8

    move-object/from16 v8, v21

    move-wide/from16 v16, v12

    move-wide v12, v14

    move/from16 v19, v2

    move-object/from16 v20, v5

    invoke-direct/range {v8 .. v20}, Lf/h/a/c/h1/k;-><init>(Landroid/net/Uri;I[BJJJLjava/lang/String;ILjava/util/Map;)V

    goto/16 :goto_3

    :cond_2
    iget-boolean v2, v0, Lf/h/a/c/h1/y/h;->g:Z

    if-eqz v2, :cond_4

    iget-object v2, v0, Lf/h/a/c/h1/y/h;->h:Ljava/io/File;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    iget-wide v9, v1, Lf/h/a/c/h1/y/c;->s:J

    iget-wide v11, v0, Lf/h/a/c/h1/y/h;->e:J

    sub-long v13, v9, v11

    iget-wide v9, v0, Lf/h/a/c/h1/y/h;->f:J

    sub-long/2addr v9, v13

    iget-wide v11, v1, Lf/h/a/c/h1/y/c;->t:J

    cmp-long v2, v11, v3

    if-eqz v2, :cond_3

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v9

    :cond_3
    move-wide v15, v9

    new-instance v21, Lf/h/a/c/h1/k;

    iget-wide v11, v1, Lf/h/a/c/h1/y/c;->s:J

    iget-object v2, v1, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    iget v5, v1, Lf/h/a/c/h1/y/c;->q:I

    const/4 v9, 0x1

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v19

    const/4 v10, 0x0

    move-object/from16 v7, v21

    move-object/from16 v17, v2

    move/from16 v18, v5

    invoke-direct/range {v7 .. v19}, Lf/h/a/c/h1/k;-><init>(Landroid/net/Uri;I[BJJJLjava/lang/String;ILjava/util/Map;)V

    iget-object v7, v1, Lf/h/a/c/h1/y/c;->b:Lf/h/a/c/h1/j;

    goto :goto_3

    :cond_4
    iget-wide v7, v0, Lf/h/a/c/h1/y/h;->f:J

    cmp-long v2, v7, v3

    if-nez v2, :cond_5

    const/4 v2, 0x1

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_6

    iget-wide v7, v1, Lf/h/a/c/h1/y/c;->t:J

    goto :goto_2

    :cond_6
    iget-wide v9, v1, Lf/h/a/c/h1/y/c;->t:J

    cmp-long v2, v9, v3

    if-eqz v2, :cond_7

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    :cond_7
    :goto_2
    move-wide/from16 v17, v7

    new-instance v2, Lf/h/a/c/h1/k;

    iget-object v10, v1, Lf/h/a/c/h1/y/c;->l:Landroid/net/Uri;

    iget v11, v1, Lf/h/a/c/h1/y/c;->n:I

    iget-object v12, v1, Lf/h/a/c/h1/y/c;->o:[B

    iget-wide v7, v1, Lf/h/a/c/h1/y/c;->s:J

    iget-object v5, v1, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    iget v15, v1, Lf/h/a/c/h1/y/c;->q:I

    iget-object v13, v1, Lf/h/a/c/h1/y/c;->p:Ljava/util/Map;

    move-object v9, v2

    move-object/from16 v21, v13

    move-wide v13, v7

    move/from16 v20, v15

    move-wide v15, v7

    move-object/from16 v19, v5

    invoke-direct/range {v9 .. v21}, Lf/h/a/c/h1/k;-><init>(Landroid/net/Uri;I[BJJJLjava/lang/String;ILjava/util/Map;)V

    iget-object v7, v1, Lf/h/a/c/h1/y/c;->c:Lf/h/a/c/h1/j;

    if-eqz v7, :cond_8

    move-object/from16 v21, v2

    :goto_3
    move-object v2, v0

    move-object/from16 v0, v21

    goto :goto_4

    :cond_8
    iget-object v7, v1, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    iget-object v5, v1, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    invoke-interface {v5, v0}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->h(Lf/h/a/c/h1/y/h;)V

    move-object v0, v2

    const/4 v2, 0x0

    :goto_4
    iget-boolean v5, v1, Lf/h/a/c/h1/y/c;->w:Z

    if-nez v5, :cond_9

    iget-object v5, v1, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    if-ne v7, v5, :cond_9

    iget-wide v8, v1, Lf/h/a/c/h1/y/c;->s:J

    const-wide/32 v10, 0x19000

    add-long/2addr v8, v10

    goto :goto_5

    :cond_9
    const-wide v8, 0x7fffffffffffffffL

    :goto_5
    iput-wide v8, v1, Lf/h/a/c/h1/y/c;->y:J

    if-eqz p1, :cond_d

    iget-object v5, v1, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    iget-object v8, v1, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    if-ne v5, v8, :cond_a

    const/4 v5, 0x1

    goto :goto_6

    :cond_a
    const/4 v5, 0x0

    :goto_6
    invoke-static {v5}, Lf/g/j/k/a;->s(Z)V

    iget-object v5, v1, Lf/h/a/c/h1/y/c;->d:Lf/h/a/c/h1/j;

    if-ne v7, v5, :cond_b

    return-void

    :cond_b
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/h1/y/c;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_7

    :catchall_0
    move-exception v0

    move-object v3, v0

    iget-boolean v0, v2, Lf/h/a/c/h1/y/h;->g:Z

    xor-int/2addr v0, v6

    if-eqz v0, :cond_c

    iget-object v0, v1, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->h(Lf/h/a/c/h1/y/h;)V

    :cond_c
    throw v3

    :cond_d
    :goto_7
    if-eqz v2, :cond_e

    iget-boolean v5, v2, Lf/h/a/c/h1/y/h;->g:Z

    xor-int/2addr v5, v6

    if-eqz v5, :cond_e

    iput-object v2, v1, Lf/h/a/c/h1/y/c;->u:Lf/h/a/c/h1/y/h;

    :cond_e
    iput-object v7, v1, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    iget-wide v8, v0, Lf/h/a/c/h1/k;->g:J

    cmp-long v2, v8, v3

    if-nez v2, :cond_f

    const/4 v2, 0x1

    goto :goto_8

    :cond_f
    const/4 v2, 0x0

    :goto_8
    iput-boolean v2, v1, Lf/h/a/c/h1/y/c;->k:Z

    invoke-interface {v7, v0}, Lf/h/a/c/h1/j;->a(Lf/h/a/c/h1/k;)J

    move-result-wide v7

    new-instance v0, Lf/h/a/c/h1/y/n;

    invoke-direct {v0}, Lf/h/a/c/h1/y/n;-><init>()V

    iget-boolean v2, v1, Lf/h/a/c/h1/y/c;->k:Z

    if-eqz v2, :cond_10

    cmp-long v2, v7, v3

    if-eqz v2, :cond_10

    iput-wide v7, v1, Lf/h/a/c/h1/y/c;->t:J

    iget-wide v2, v1, Lf/h/a/c/h1/y/c;->s:J

    add-long/2addr v2, v7

    invoke-static {v0, v2, v3}, Lf/h/a/c/h1/y/n;->a(Lf/h/a/c/h1/y/n;J)Lf/h/a/c/h1/y/n;

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/h1/y/c;->g()Z

    move-result v2

    xor-int/2addr v2, v6

    if-eqz v2, :cond_13

    iget-object v2, v1, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    invoke-interface {v2}, Lf/h/a/c/h1/j;->d()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v1, Lf/h/a/c/h1/y/c;->m:Landroid/net/Uri;

    iget-object v3, v1, Lf/h/a/c/h1/y/c;->l:Landroid/net/Uri;

    invoke-virtual {v3, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v6

    if-eqz v2, :cond_11

    iget-object v2, v1, Lf/h/a/c/h1/y/c;->m:Landroid/net/Uri;

    goto :goto_9

    :cond_11
    const/4 v2, 0x0

    :goto_9
    const-string v3, "exo_redir"

    if-nez v2, :cond_12

    iget-object v2, v0, Lf/h/a/c/h1/y/n;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lf/h/a/c/h1/y/n;->a:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    :cond_12
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, v0, Lf/h/a/c/h1/y/n;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lf/h/a/c/h1/y/n;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_13
    :goto_a
    iget-object v2, v1, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    iget-object v3, v1, Lf/h/a/c/h1/y/c;->c:Lf/h/a/c/h1/j;

    if-ne v2, v3, :cond_14

    const/4 v5, 0x1

    goto :goto_b

    :cond_14
    const/4 v5, 0x0

    :goto_b
    if-eqz v5, :cond_15

    iget-object v2, v1, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    iget-object v3, v1, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->c(Ljava/lang/String;Lf/h/a/c/h1/y/n;)V

    :cond_15
    return-void
.end method

.method public final i()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lf/h/a/c/h1/y/c;->t:J

    iget-object v0, p0, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    iget-object v1, p0, Lf/h/a/c/h1/y/c;->c:Lf/h/a/c/h1/j;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lf/h/a/c/h1/y/n;

    invoke-direct {v0}, Lf/h/a/c/h1/y/n;-><init>()V

    iget-wide v1, p0, Lf/h/a/c/h1/y/c;->s:J

    invoke-static {v0, v1, v2}, Lf/h/a/c/h1/y/n;->a(Lf/h/a/c/h1/y/n;J)Lf/h/a/c/h1/y/n;

    iget-object v1, p0, Lf/h/a/c/h1/y/c;->a:Lcom/google/android/exoplayer2/upstream/cache/Cache;

    iget-object v2, p0, Lf/h/a/c/h1/y/c;->r:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/exoplayer2/upstream/cache/Cache;->c(Ljava/lang/String;Lf/h/a/c/h1/y/n;)V

    :cond_1
    return-void
.end method

.method public read([BII)I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p3, :cond_0

    return v0

    :cond_0
    iget-wide v1, p0, Lf/h/a/c/h1/y/c;->t:J

    const-wide/16 v3, 0x0

    const/4 v5, -0x1

    cmp-long v6, v1, v3

    if-nez v6, :cond_1

    return v5

    :cond_1
    const/4 v1, 0x1

    :try_start_0
    iget-wide v6, p0, Lf/h/a/c/h1/y/c;->s:J

    iget-wide v8, p0, Lf/h/a/c/h1/y/c;->y:J

    cmp-long v2, v6, v8

    if-ltz v2, :cond_2

    invoke-virtual {p0, v1}, Lf/h/a/c/h1/y/c;->h(Z)V

    :cond_2
    iget-object v2, p0, Lf/h/a/c/h1/y/c;->j:Lf/h/a/c/h1/j;

    invoke-interface {v2, p1, p2, p3}, Lf/h/a/c/h1/j;->read([BII)I

    move-result v2

    const-wide/16 v6, -0x1

    if-eq v2, v5, :cond_4

    invoke-virtual {p0}, Lf/h/a/c/h1/y/c;->g()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-wide p1, p0, Lf/h/a/c/h1/y/c;->x:J

    int-to-long v3, v2

    add-long/2addr p1, v3

    iput-wide p1, p0, Lf/h/a/c/h1/y/c;->x:J

    :cond_3
    iget-wide p1, p0, Lf/h/a/c/h1/y/c;->s:J

    int-to-long v3, v2

    add-long/2addr p1, v3

    iput-wide p1, p0, Lf/h/a/c/h1/y/c;->s:J

    iget-wide p1, p0, Lf/h/a/c/h1/y/c;->t:J

    cmp-long p3, p1, v6

    if-eqz p3, :cond_6

    sub-long/2addr p1, v3

    iput-wide p1, p0, Lf/h/a/c/h1/y/c;->t:J

    goto :goto_0

    :cond_4
    iget-boolean v8, p0, Lf/h/a/c/h1/y/c;->k:Z

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lf/h/a/c/h1/y/c;->i()V

    goto :goto_0

    :cond_5
    iget-wide v8, p0, Lf/h/a/c/h1/y/c;->t:J

    cmp-long v10, v8, v3

    if-gtz v10, :cond_7

    cmp-long v3, v8, v6

    if-nez v3, :cond_6

    goto :goto_1

    :cond_6
    :goto_0
    return v2

    :cond_7
    :goto_1
    invoke-virtual {p0}, Lf/h/a/c/h1/y/c;->e()V

    invoke-virtual {p0, v0}, Lf/h/a/c/h1/y/c;->h(Z)V

    invoke-virtual {p0, p1, p2, p3}, Lf/h/a/c/h1/y/c;->read([BII)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return p1

    :catchall_0
    move-exception p1

    invoke-virtual {p0, p1}, Lf/h/a/c/h1/y/c;->f(Ljava/lang/Throwable;)V

    throw p1

    :catch_0
    move-exception p1

    iget-boolean p2, p0, Lf/h/a/c/h1/y/c;->k:Z

    if-eqz p2, :cond_a

    sget p2, Lf/h/a/c/h1/y/i;->a:I

    move-object p2, p1

    :goto_2
    if-eqz p2, :cond_9

    instance-of p3, p2, Lcom/google/android/exoplayer2/upstream/DataSourceException;

    if-eqz p3, :cond_8

    move-object p3, p2

    check-cast p3, Lcom/google/android/exoplayer2/upstream/DataSourceException;

    iget p3, p3, Lcom/google/android/exoplayer2/upstream/DataSourceException;->reason:I

    if-nez p3, :cond_8

    const/4 v0, 0x1

    goto :goto_3

    :cond_8
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    goto :goto_2

    :cond_9
    :goto_3
    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lf/h/a/c/h1/y/c;->i()V

    return v5

    :cond_a
    invoke-virtual {p0, p1}, Lf/h/a/c/h1/y/c;->f(Ljava/lang/Throwable;)V

    throw p1
.end method
