.class public final synthetic Lf/h/a/c/h1/y/b;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final synthetic d:Lf/h/a/c/h1/y/b;


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/c/h1/y/b;

    invoke-direct {v0}, Lf/h/a/c/h1/y/b;-><init>()V

    sput-object v0, Lf/h/a/c/h1/y/b;->d:Lf/h/a/c/h1/y/b;

    return-void
.end method

.method public synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 9

    check-cast p1, Lf/h/a/c/h1/y/h;

    check-cast p2, Lf/h/a/c/h1/y/h;

    iget-wide v0, p1, Lf/h/a/c/h1/y/h;->i:J

    iget-wide v2, p2, Lf/h/a/c/h1/y/h;->i:J

    sub-long v4, v0, v2

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-nez v8, :cond_0

    invoke-virtual {p1, p2}, Lf/h/a/c/h1/y/h;->f(Lf/h/a/c/h1/y/h;)I

    move-result p1

    goto :goto_0

    :cond_0
    cmp-long p1, v0, v2

    if-gez p1, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    return p1
.end method
