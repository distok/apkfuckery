.class public Lf/h/a/c/h1/y/h;
.super Ljava/lang/Object;
.source "CacheSpan.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lf/h/a/c/h1/y/h;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:J

.field public final g:Z

.field public final h:Ljava/io/File;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final i:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JJJLjava/io/File;)V
    .locals 0
    .param p8    # Ljava/io/File;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/h1/y/h;->d:Ljava/lang/String;

    iput-wide p2, p0, Lf/h/a/c/h1/y/h;->e:J

    iput-wide p4, p0, Lf/h/a/c/h1/y/h;->f:J

    if-eqz p8, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lf/h/a/c/h1/y/h;->g:Z

    iput-object p8, p0, Lf/h/a/c/h1/y/h;->h:Ljava/io/File;

    iput-wide p6, p0, Lf/h/a/c/h1/y/h;->i:J

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lf/h/a/c/h1/y/h;

    invoke-virtual {p0, p1}, Lf/h/a/c/h1/y/h;->f(Lf/h/a/c/h1/y/h;)I

    move-result p1

    return p1
.end method

.method public f(Lf/h/a/c/h1/y/h;)I
    .locals 4
    .param p1    # Lf/h/a/c/h1/y/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/c/h1/y/h;->d:Ljava/lang/String;

    iget-object v1, p1, Lf/h/a/c/h1/y/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/h1/y/h;->d:Ljava/lang/String;

    iget-object p1, p1, Lf/h/a/c/h1/y/h;->d:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_0
    iget-wide v0, p0, Lf/h/a/c/h1/y/h;->e:J

    iget-wide v2, p1, Lf/h/a/c/h1/y/h;->e:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    if-gez p1, :cond_2

    const/4 p1, -0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    :goto_0
    return p1
.end method
