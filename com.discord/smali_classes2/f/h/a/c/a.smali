.class public final synthetic Lf/h/a/c/a;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/c/r$a;

.field public final synthetic e:I


# direct methods
.method public synthetic constructor <init>(Lf/h/a/c/r$a;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a;->d:Lf/h/a/c/r$a;

    iput p2, p0, Lf/h/a/c/a;->e:I

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    iget-object v0, p0, Lf/h/a/c/a;->d:Lf/h/a/c/r$a;

    iget v1, p0, Lf/h/a/c/a;->e:I

    iget-object v0, v0, Lf/h/a/c/r$a;->b:Lf/h/a/c/r;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, -0x3

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, -0x1

    const/4 v6, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, -0x2

    if-eq v1, v2, :cond_2

    if-eq v1, v5, :cond_1

    if-eq v1, v6, :cond_0

    const-string v0, "Unknown focus change type: "

    const-string v2, "AudioFocusManager"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_3

    :cond_0
    iput v6, v0, Lf/h/a/c/r;->d:I

    goto :goto_0

    :cond_1
    iput v5, v0, Lf/h/a/c/r;->d:I

    goto :goto_0

    :cond_2
    iput v3, v0, Lf/h/a/c/r;->d:I

    goto :goto_0

    :cond_3
    iput v4, v0, Lf/h/a/c/r;->d:I

    :goto_0
    iget v1, v0, Lf/h/a/c/r;->d:I

    if-eq v1, v5, :cond_7

    if-eqz v1, :cond_8

    if-eq v1, v6, :cond_6

    if-eq v1, v3, :cond_5

    if-ne v1, v4, :cond_4

    goto :goto_1

    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown audio focus state: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lf/h/a/c/r;->d:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    iget-object v1, v0, Lf/h/a/c/r;->c:Lf/h/a/c/r$b;

    const/4 v2, 0x0

    check-cast v1, Lf/h/a/c/s0$c;

    invoke-virtual {v1, v2}, Lf/h/a/c/s0$c;->d(I)V

    goto :goto_1

    :cond_6
    iget-object v1, v0, Lf/h/a/c/r;->c:Lf/h/a/c/r$b;

    check-cast v1, Lf/h/a/c/s0$c;

    invoke-virtual {v1, v6}, Lf/h/a/c/s0$c;->d(I)V

    goto :goto_1

    :cond_7
    iget-object v1, v0, Lf/h/a/c/r;->c:Lf/h/a/c/r$b;

    check-cast v1, Lf/h/a/c/s0$c;

    invoke-virtual {v1, v5}, Lf/h/a/c/s0$c;->d(I)V

    invoke-virtual {v0, v6}, Lf/h/a/c/r;->a(Z)V

    :cond_8
    :goto_1
    iget v1, v0, Lf/h/a/c/r;->d:I

    if-ne v1, v4, :cond_9

    const v1, 0x3e4ccccd    # 0.2f

    goto :goto_2

    :cond_9
    const/high16 v1, 0x3f800000    # 1.0f

    :goto_2
    iget v2, v0, Lf/h/a/c/r;->e:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_a

    iput v1, v0, Lf/h/a/c/r;->e:F

    iget-object v0, v0, Lf/h/a/c/r;->c:Lf/h/a/c/r$b;

    check-cast v0, Lf/h/a/c/s0$c;

    iget-object v0, v0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-virtual {v0}, Lf/h/a/c/s0;->L()V

    :cond_a
    :goto_3
    return-void
.end method
