.class public Lf/h/a/c/s0;
.super Lf/h/a/c/s;
.source "SimpleExoPlayer.java"

# interfaces
.implements Lf/h/a/c/m0;
.implements Lf/h/a/c/m0$c;
.implements Lf/h/a/c/m0$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/s0$c;,
        Lf/h/a/c/s0$b;
    }
.end annotation


# instance fields
.field public A:Lf/h/a/c/j1/n;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public B:Lf/h/a/c/j1/s/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public C:Z

.field public D:Z

.field public final b:[Lf/h/a/c/p0;

.field public final c:Lf/h/a/c/a0;

.field public final d:Landroid/os/Handler;

.field public final e:Lf/h/a/c/s0$c;

.field public final f:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lf/h/a/c/j1/q;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lf/h/a/c/w0/k;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lf/h/a/c/e1/j;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lf/h/a/c/c1/e;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lf/h/a/c/j1/r;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lf/h/a/c/w0/l;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lf/h/a/c/h1/e;

.field public final m:Lf/h/a/c/v0/a;

.field public final n:Lf/h/a/c/q;

.field public final o:Lf/h/a/c/r;

.field public final p:Lf/h/a/c/u0;

.field public q:Landroid/view/Surface;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Landroid/view/SurfaceHolder;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public t:Landroid/view/TextureView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public u:I

.field public v:I

.field public w:I

.field public x:F

.field public y:Lf/h/a/c/d1/p;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/a/c/z;Lf/h/a/c/f1/h;Lf/h/a/c/e0;Lf/h/a/c/h1/e;Lf/h/a/c/v0/a;Lf/h/a/c/i1/f;Landroid/os/Looper;)V
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    sget-object v8, Lf/h/a/c/z0/b;->a:Lf/h/a/c/z0/b;

    invoke-direct/range {p0 .. p0}, Lf/h/a/c/s;-><init>()V

    iput-object v9, v0, Lf/h/a/c/s0;->l:Lf/h/a/c/h1/e;

    iput-object v10, v0, Lf/h/a/c/s0;->m:Lf/h/a/c/v0/a;

    new-instance v7, Lf/h/a/c/s0$c;

    const/4 v6, 0x0

    invoke-direct {v7, v0, v6}, Lf/h/a/c/s0$c;-><init>(Lf/h/a/c/s0;Lf/h/a/c/s0$a;)V

    iput-object v7, v0, Lf/h/a/c/s0;->e:Lf/h/a/c/s0$c;

    new-instance v5, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v5}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v5, v0, Lf/h/a/c/s0;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v4, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v4}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v4, v0, Lf/h/a/c/s0;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v3, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v3, v0, Lf/h/a/c/s0;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v3, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v3, v0, Lf/h/a/c/s0;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v14, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v14}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v14, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v15, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v15}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v15, v0, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance v13, Landroid/os/Handler;

    move-object/from16 v12, p8

    invoke-direct {v13, v12}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v13, v0, Lf/h/a/c/s0;->d:Landroid/os/Handler;

    invoke-static/range {p2 .. p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, v2, Lf/h/a/c/z;->a:Landroid/content/Context;

    sget-object v22, Lf/h/a/c/b1/f;->a:Lf/h/a/c/b1/f;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v16, v14

    new-instance v14, Lcom/google/android/exoplayer2/video/MediaCodecVideoRenderer;

    const/16 v21, 0x32

    const-wide/16 v19, 0x1388

    move-object/from16 v23, v11

    move-object v11, v14

    move-object v12, v6

    move-object v6, v13

    move-object/from16 v13, v22

    move-object/from16 v24, v3

    move-object v3, v14

    move-object v9, v15

    move-object/from16 v1, v16

    move-wide/from16 v14, v19

    move-object/from16 v16, v8

    move-object/from16 v19, v6

    move-object/from16 v20, v7

    invoke-direct/range {v11 .. v21}, Lcom/google/android/exoplayer2/video/MediaCodecVideoRenderer;-><init>(Landroid/content/Context;Lf/h/a/c/b1/f;JLf/h/a/c/z0/b;ZZLandroid/os/Handler;Lf/h/a/c/j1/r;I)V

    move-object/from16 v15, v23

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v12, v2, Lf/h/a/c/z;->a:Landroid/content/Context;

    const/16 v16, 0x0

    const/4 v3, 0x0

    new-array v11, v3, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    new-instance v14, Lf/h/a/c/w0/t;

    new-instance v13, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    sget-object v17, Lf/h/a/c/w0/j;->c:Lf/h/a/c/w0/j;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v12, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    sget v3, Lf/h/a/c/i1/a0;->a:I

    move-object/from16 v21, v4

    const/16 v4, 0x11

    const/4 v15, 0x1

    if-lt v3, v4, :cond_0

    sget-object v3, Lf/h/a/c/i1/a0;->c:Ljava/lang/String;

    const-string v4, "Amazon"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "external_surround_sound_enabled"

    move-object/from16 v25, v5

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v15, :cond_2

    sget-object v2, Lf/h/a/c/w0/j;->d:Lf/h/a/c/w0/j;

    goto :goto_2

    :cond_1
    move-object/from16 v25, v5

    const/4 v5, 0x0

    :cond_2
    if-eqz v2, :cond_4

    const-string v3, "android.media.extra.AUDIO_PLUG_STATE"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_3

    goto :goto_1

    :cond_3
    new-instance v3, Lf/h/a/c/w0/j;

    const-string v4, "android.media.extra.ENCODINGS"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v4

    const/16 v5, 0x8

    const-string v15, "android.media.extra.MAX_CHANNEL_COUNT"

    invoke-virtual {v2, v15, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {v3, v4, v2}, Lf/h/a/c/w0/j;-><init>([II)V

    move-object v2, v3

    goto :goto_2

    :cond_4
    :goto_1
    sget-object v2, Lf/h/a/c/w0/j;->c:Lf/h/a/c/w0/j;

    :goto_2
    invoke-direct {v13, v2, v11}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;-><init>(Lf/h/a/c/w0/j;[Lcom/google/android/exoplayer2/audio/AudioProcessor;)V

    move-object v11, v14

    move-object v2, v13

    move-object/from16 v13, v22

    move-object v3, v14

    move-object v14, v8

    move-object/from16 v4, v23

    const/4 v5, 0x0

    const/16 v22, 0x1

    move v15, v5

    move-object/from16 v17, v6

    move-object/from16 v18, v7

    move-object/from16 v19, v2

    invoke-direct/range {v11 .. v19}, Lf/h/a/c/w0/t;-><init>(Landroid/content/Context;Lf/h/a/c/b1/f;Lf/h/a/c/z0/b;ZZLandroid/os/Handler;Lf/h/a/c/w0/l;Lcom/google/android/exoplayer2/audio/AudioSink;)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lf/h/a/c/e1/k;

    invoke-direct {v3, v7, v2}, Lf/h/a/c/e1/k;-><init>(Lf/h/a/c/e1/j;Landroid/os/Looper;)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lf/h/a/c/c1/f;

    invoke-direct {v3, v7, v2}, Lf/h/a/c/c1/f;-><init>(Lf/h/a/c/c1/e;Landroid/os/Looper;)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lf/h/a/c/j1/s/b;

    invoke-direct {v2}, Lf/h/a/c/j1/s/b;-><init>()V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    new-array v2, v3, [Lf/h/a/c/p0;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, [Lf/h/a/c/p0;

    iput-object v4, v0, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v0, Lf/h/a/c/s0;->x:F

    iput v3, v0, Lf/h/a/c/s0;->w:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lf/h/a/c/s0;->z:Ljava/util/List;

    new-instance v11, Lf/h/a/c/a0;

    move-object v2, v11

    move-object/from16 v13, v24

    const/4 v12, 0x0

    const/4 v14, 0x0

    move-object v3, v4

    move-object/from16 v15, v21

    move-object/from16 v4, p3

    move-object/from16 v12, v25

    move-object/from16 v5, p4

    move-object/from16 v16, v14

    move-object v14, v6

    move-object/from16 v6, p5

    move-object/from16 v17, v14

    move-object v14, v7

    move-object/from16 v7, p7

    move-object/from16 v26, v8

    move-object/from16 v8, p8

    invoke-direct/range {v2 .. v8}, Lf/h/a/c/a0;-><init>([Lf/h/a/c/p0;Lf/h/a/c/f1/h;Lf/h/a/c/e0;Lf/h/a/c/h1/e;Lf/h/a/c/i1/f;Landroid/os/Looper;)V

    iput-object v11, v0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v2, v10, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    if-eqz v2, :cond_6

    iget-object v2, v10, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v2, v2, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    :goto_3
    const/4 v3, 0x1

    :goto_4
    invoke-static {v3}, Lf/g/j/k/a;->s(Z)V

    iput-object v11, v10, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/s0;->S()V

    iget-object v2, v11, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v3, Lf/h/a/c/s$a;

    invoke-direct {v3, v10}, Lf/h/a/c/s$a;-><init>(Lf/h/a/c/m0$a;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    invoke-virtual {v0, v14}, Lf/h/a/c/s0;->j(Lf/h/a/c/m0$a;)V

    invoke-virtual {v1, v10}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v10}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9, v10}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v15, v10}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v13, v10}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    move-object/from16 v1, p5

    move-object/from16 v2, v17

    invoke-interface {v1, v2, v10}, Lf/h/a/c/h1/e;->f(Landroid/os/Handler;Lf/h/a/c/h1/e$a;)V

    move-object/from16 v1, v26

    instance-of v3, v1, Lcom/google/android/exoplayer2/drm/DefaultDrmSessionManager;

    if-nez v3, :cond_7

    new-instance v1, Lf/h/a/c/q;

    move-object/from16 v3, p1

    invoke-direct {v1, v3, v2, v14}, Lf/h/a/c/q;-><init>(Landroid/content/Context;Landroid/os/Handler;Lf/h/a/c/q$b;)V

    iput-object v1, v0, Lf/h/a/c/s0;->n:Lf/h/a/c/q;

    new-instance v1, Lf/h/a/c/r;

    invoke-direct {v1, v3, v2, v14}, Lf/h/a/c/r;-><init>(Landroid/content/Context;Landroid/os/Handler;Lf/h/a/c/r$b;)V

    iput-object v1, v0, Lf/h/a/c/s0;->o:Lf/h/a/c/r;

    new-instance v1, Lf/h/a/c/u0;

    invoke-direct {v1, v3}, Lf/h/a/c/u0;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lf/h/a/c/s0;->p:Lf/h/a/c/u0;

    return-void

    :cond_7
    move-object v8, v1

    check-cast v8, Lcom/google/android/exoplayer2/drm/DefaultDrmSessionManager;

    throw v16
.end method


# virtual methods
.method public A()Lf/h/a/c/t0;
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    return-object v0
.end method

.method public B()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->B()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public C()Z
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-boolean v0, v0, Lf/h/a/c/a0;->n:Z

    return v0
.end method

.method public D()J
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->D()J

    move-result-wide v0

    return-wide v0
.end method

.method public E()Lf/h/a/c/f1/g;
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    iget-object v0, v0, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    return-object v0
.end method

.method public F(I)I
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->c:[Lf/h/a/c/p0;

    aget-object p1, v0, p1

    invoke-interface {p1}, Lf/h/a/c/p0;->u()I

    move-result p1

    return p1
.end method

.method public G()J
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->G()J

    move-result-wide v0

    return-wide v0
.end method

.method public H()Lf/h/a/c/m0$b;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    return-object p0
.end method

.method public I(Landroid/view/Surface;)V
    .locals 1
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/h/a/c/s0;->q:Landroid/view/Surface;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    invoke-virtual {p0}, Lf/h/a/c/s0;->K()V

    const/4 p1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    invoke-virtual {p0, v0, v0}, Lf/h/a/c/s0;->J(II)V

    :cond_0
    return-void
.end method

.method public final J(II)V
    .locals 2

    iget v0, p0, Lf/h/a/c/s0;->u:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lf/h/a/c/s0;->v:I

    if-eq p2, v0, :cond_1

    :cond_0
    iput p1, p0, Lf/h/a/c/s0;->u:I

    iput p2, p0, Lf/h/a/c/s0;->v:I

    iget-object v0, p0, Lf/h/a/c/s0;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/q;

    invoke-interface {v1, p1, p2}, Lf/h/a/c/j1/q;->B(II)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final K()V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/s0;->t:Landroid/view/TextureView;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    iget-object v2, p0, Lf/h/a/c/s0;->e:Lf/h/a/c/s0$c;

    if-eq v0, v2, :cond_0

    const-string v0, "SimpleExoPlayer"

    const-string v2, "SurfaceTextureListener already unset or replaced."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/s0;->t:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    :goto_0
    iput-object v1, p0, Lf/h/a/c/s0;->t:Landroid/view/TextureView;

    :cond_1
    iget-object v0, p0, Lf/h/a/c/s0;->s:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lf/h/a/c/s0;->e:Lf/h/a/c/s0$c;

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    iput-object v1, p0, Lf/h/a/c/s0;->s:Landroid/view/SurfaceHolder;

    :cond_2
    return-void
.end method

.method public final L()V
    .locals 7

    iget v0, p0, Lf/h/a/c/s0;->x:F

    iget-object v1, p0, Lf/h/a/c/s0;->o:Lf/h/a/c/r;

    iget v1, v1, Lf/h/a/c/r;->e:F

    mul-float v0, v0, v1

    iget-object v1, p0, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    invoke-interface {v4}, Lf/h/a/c/p0;->u()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v5, v4}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v4, v5}, Lf/h/a/c/n0;->d(Ljava/lang/Object;)Lf/h/a/c/n0;

    invoke-virtual {v4}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final M(Lf/h/a/c/j1/l;)V
    .locals 6
    .param p1    # Lf/h/a/c/j1/l;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-interface {v3}, Lf/h/a/c/p0;->u()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v4, v3}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    iget-boolean v4, v3, Lf/h/a/c/n0;->h:Z

    xor-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Lf/g/j/k/a;->s(Z)V

    iput-object p1, v3, Lf/h/a/c/n0;->e:Ljava/lang/Object;

    invoke-virtual {v3}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public N(Landroid/view/Surface;)V
    .locals 1
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    invoke-virtual {p0}, Lf/h/a/c/s0;->K()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/s0;->a()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0, v0}, Lf/h/a/c/s0;->J(II)V

    return-void
.end method

.method public O(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1    # Landroid/view/SurfaceHolder;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    invoke-virtual {p0}, Lf/h/a/c/s0;->K()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/s0;->a()V

    :cond_0
    iput-object p1, p0, Lf/h/a/c/s0;->s:Landroid/view/SurfaceHolder;

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_1

    invoke-virtual {p0, v0, v1}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    invoke-virtual {p0, v1, v1}, Lf/h/a/c/s0;->J(II)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lf/h/a/c/s0;->e:Lf/h/a/c/s0$c;

    invoke-interface {p1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/Surface;->isValid()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, v2, v1}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lf/h/a/c/s0;->J(II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0, v1}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    invoke-virtual {p0, v1, v1}, Lf/h/a/c/s0;->J(II)V

    :goto_0
    return-void
.end method

.method public final P(Landroid/view/Surface;Z)V
    .locals 9
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x1

    if-ge v4, v2, :cond_1

    aget-object v6, v1, v4

    invoke-interface {v6}, Lf/h/a/c/p0;->u()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v7, v6}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v6

    invoke-virtual {v6, v5}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    iget-boolean v7, v6, Lf/h/a/c/n0;->h:Z

    xor-int/2addr v5, v7

    invoke-static {v5}, Lf/g/j/k/a;->s(Z)V

    iput-object p1, v6, Lf/h/a/c/n0;->e:Ljava/lang/Object;

    invoke-virtual {v6}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lf/h/a/c/s0;->q:Landroid/view/Surface;

    if-eqz v1, :cond_5

    if-eq v1, p1, :cond_5

    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/n0;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-boolean v2, v1, Lf/h/a/c/n0;->h:Z

    invoke-static {v2}, Lf/g/j/k/a;->s(Z)V

    iget-object v2, v1, Lf/h/a/c/n0;->f:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-eq v2, v4, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-static {v2}, Lf/g/j/k/a;->s(Z)V

    :goto_3
    iget-boolean v2, v1, Lf/h/a/c/n0;->j:Z

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_3
    :try_start_2
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_4
    iget-boolean v0, p0, Lf/h/a/c/s0;->r:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lf/h/a/c/s0;->q:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_5
    iput-object p1, p0, Lf/h/a/c/s0;->q:Landroid/view/Surface;

    iput-boolean p2, p0, Lf/h/a/c/s0;->r:Z

    return-void
.end method

.method public Q(Landroid/view/TextureView;)V
    .locals 5
    .param p1    # Landroid/view/TextureView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    invoke-virtual {p0}, Lf/h/a/c/s0;->K()V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/s0;->a()V

    :cond_0
    iput-object p1, p0, Lf/h/a/c/s0;->t:Landroid/view/TextureView;

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_1

    invoke-virtual {p0, v1, v0}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    invoke-virtual {p0, v2, v2}, Lf/h/a/c/s0;->J(II)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string v3, "SimpleExoPlayer"

    const-string v4, "Replacing existing SurfaceTextureListener."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v3, p0, Lf/h/a/c/s0;->e:Lf/h/a/c/s0$c;

    invoke-virtual {p1, v3}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    invoke-virtual {p1}, Landroid/view/TextureView;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v3

    goto :goto_0

    :cond_3
    move-object v3, v1

    :goto_0
    if-nez v3, :cond_4

    invoke-virtual {p0, v1, v0}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    invoke-virtual {p0, v2, v2}, Lf/h/a/c/s0;->J(II)V

    goto :goto_1

    :cond_4
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, v3}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {p0, v1, v0}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    invoke-virtual {p1}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/TextureView;->getHeight()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lf/h/a/c/s0;->J(II)V

    :goto_1
    return-void
.end method

.method public final R(ZI)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-eq p2, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object p2, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {p2, p1, v0}, Lf/h/a/c/a0;->N(ZI)V

    return-void
.end method

.method public final S()V
    .locals 3

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0}, Lf/h/a/c/s0;->B()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lf/h/a/c/s0;->C:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    :goto_0
    const-string v1, "SimpleExoPlayer"

    const-string v2, "Player is accessed on the wrong thread. See https://exoplayer.dev/issues/player-accessed-on-wrong-thread"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/s0;->C:Z

    :cond_1
    return-void
.end method

.method public a()V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lf/h/a/c/s0;->M(Lf/h/a/c/j1/l;)V

    return-void
.end method

.method public b()Lf/h/a/c/j0;
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->s:Lf/h/a/c/j0;

    return-object v0
.end method

.method public c()Z
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->c()Z

    move-result v0

    return v0
.end method

.method public d()J
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-wide v0, v0, Lf/h/a/c/i0;->l:J

    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public e(IJ)V
    .locals 3

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->m:Lf/h/a/c/v0/a;

    iget-object v1, v0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-boolean v1, v1, Lf/h/a/c/v0/a$b;->h:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object v1, v0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lf/h/a/c/v0/a$b;->h:Z

    iget-object v0, v0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/v0/b;

    invoke-interface {v1}, Lf/h/a/c/v0/b;->G()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/c/a0;->e(IJ)V

    return-void
.end method

.method public f()Z
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-boolean v0, v0, Lf/h/a/c/a0;->k:Z

    return v0
.end method

.method public g(Z)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0, p1}, Lf/h/a/c/a0;->g(Z)V

    return-void
.end method

.method public h()Lcom/google/android/exoplayer2/ExoPlaybackException;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    return-object v0
.end method

.method public j(Lf/h/a/c/m0$a;)V
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/s$a;

    invoke-direct {v1, p1}, Lf/h/a/c/s$a;-><init>(Lf/h/a/c/m0$a;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->addIfAbsent(Ljava/lang/Object;)Z

    return-void
.end method

.method public k()I
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget v0, v0, Lf/h/a/c/d1/p$a;->c:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public l(Lf/h/a/c/m0$a;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0, p1}, Lf/h/a/c/a0;->l(Lf/h/a/c/m0$a;)V

    return-void
.end method

.method public m()I
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->m()I

    move-result v0

    return v0
.end method

.method public n(Z)V
    .locals 4

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->o:Lf/h/a/c/r;

    invoke-virtual {p0}, Lf/h/a/c/s0;->r()I

    move-result v1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, -0x1

    if-nez p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/c/r;->a(Z)V

    goto :goto_1

    :cond_0
    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    if-eqz p1, :cond_3

    :cond_1
    :goto_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    iget v1, v0, Lf/h/a/c/r;->d:I

    if-eqz v1, :cond_1

    invoke-virtual {v0, v3}, Lf/h/a/c/r;->a(Z)V

    goto :goto_0

    :cond_3
    :goto_1
    invoke-virtual {p0, p1, v2}, Lf/h/a/c/s0;->R(ZI)V

    return-void
.end method

.method public o()Lf/h/a/c/m0$c;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    return-object p0
.end method

.method public p()J
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->p()J

    move-result-wide v0

    return-wide v0
.end method

.method public r()I
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    return v0
.end method

.method public t()I
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget v0, v0, Lf/h/a/c/d1/p$a;->b:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public u(I)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0, p1}, Lf/h/a/c/a0;->u(I)V

    return-void
.end method

.method public w()I
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget v0, v0, Lf/h/a/c/a0;->l:I

    return v0
.end method

.method public x()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget-object v0, v0, Lf/h/a/c/a0;->t:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    return-object v0
.end method

.method public y()I
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    iget v0, v0, Lf/h/a/c/a0;->m:I

    return v0
.end method

.method public z()J
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/s0;->S()V

    iget-object v0, p0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v0}, Lf/h/a/c/a0;->z()J

    move-result-wide v0

    return-wide v0
.end method
