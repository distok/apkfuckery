.class public final Lf/h/a/c/h0;
.super Ljava/lang/Object;
.source "MediaPeriodQueue.java"


# instance fields
.field public final a:Lf/h/a/c/t0$b;

.field public final b:Lf/h/a/c/t0$c;

.field public c:J

.field public d:Lf/h/a/c/t0;

.field public e:I

.field public f:Z

.field public g:Lf/h/a/c/f0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public h:Lf/h/a/c/f0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public i:Lf/h/a/c/f0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public j:I

.field public k:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public l:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/t0$b;

    invoke-direct {v0}, Lf/h/a/c/t0$b;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    new-instance v0, Lf/h/a/c/t0$c;

    invoke-direct {v0}, Lf/h/a/c/t0$c;-><init>()V

    iput-object v0, p0, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    sget-object v0, Lf/h/a/c/t0;->a:Lf/h/a/c/t0;

    iput-object v0, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    return-void
.end method


# virtual methods
.method public a()Lf/h/a/c/f0;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v2, p0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-ne v0, v2, :cond_1

    iget-object v2, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    iput-object v2, p0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    :cond_1
    invoke-virtual {v0}, Lf/h/a/c/f0;->g()V

    iget v0, p0, Lf/h/a/c/h0;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lf/h/a/c/h0;->j:I

    if-nez v0, :cond_2

    iput-object v1, p0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iget-object v0, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v1, v0, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    iput-object v1, p0, Lf/h/a/c/h0;->k:Ljava/lang/Object;

    iget-object v0, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v0, v0, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v0, v0, Lf/h/a/c/d1/p$a;->d:J

    iput-wide v0, p0, Lf/h/a/c/h0;->l:J

    :cond_2
    iget-object v0, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v0, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    iput-object v0, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    return-object v0
.end method

.method public b(Z)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object p1, v0, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    iput-object p1, p0, Lf/h/a/c/h0;->k:Ljava/lang/Object;

    iget-object p1, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object p1, p1, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v2, p1, Lf/h/a/c/d1/p$a;->d:J

    iput-wide v2, p0, Lf/h/a/c/h0;->l:J

    invoke-virtual {p0, v0}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    invoke-virtual {v0}, Lf/h/a/c/f0;->g()V

    goto :goto_1

    :cond_1
    if-nez p1, :cond_2

    iput-object v1, p0, Lf/h/a/c/h0;->k:Ljava/lang/Object;

    :cond_2
    :goto_1
    iput-object v1, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iput-object v1, p0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iput-object v1, p0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/h0;->j:I

    return-void
.end method

.method public final c(Lf/h/a/c/f0;J)Lf/h/a/c/g0;
    .locals 20
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v0, p1

    iget-object v1, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v2, v0, Lf/h/a/c/f0;->n:J

    iget-wide v4, v1, Lf/h/a/c/g0;->e:J

    add-long/2addr v2, v4

    sub-long v2, v2, p2

    iget-boolean v4, v1, Lf/h/a/c/g0;->f:Z

    const/4 v7, -0x1

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    if-eqz v4, :cond_4

    iget-object v4, v8, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v12, v1, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-object v12, v12, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v4, v12}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v14

    iget-object v13, v8, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v15, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-object v4, v8, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    iget v12, v8, Lf/h/a/c/h0;->e:I

    iget-boolean v5, v8, Lf/h/a/c/h0;->f:Z

    move-object/from16 v16, v4

    move/from16 v17, v12

    move/from16 v18, v5

    invoke-virtual/range {v13 .. v18}, Lf/h/a/c/t0;->d(ILf/h/a/c/t0$b;Lf/h/a/c/t0$c;IZ)I

    move-result v4

    if-ne v4, v7, :cond_0

    return-object v11

    :cond_0
    iget-object v5, v8, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v6, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    const/4 v7, 0x1

    invoke-virtual {v5, v4, v6, v7}, Lf/h/a/c/t0;->g(ILf/h/a/c/t0$b;Z)Lf/h/a/c/t0$b;

    move-result-object v5

    iget v15, v5, Lf/h/a/c/t0$b;->b:I

    iget-object v5, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-object v5, v5, Lf/h/a/c/t0$b;->a:Ljava/lang/Object;

    iget-object v1, v1, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v6, v1, Lf/h/a/c/d1/p$a;->d:J

    iget-object v1, v8, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v12, v8, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    invoke-virtual {v1, v15, v12}, Lf/h/a/c/t0;->m(ILf/h/a/c/t0$c;)Lf/h/a/c/t0$c;

    move-result-object v1

    iget v1, v1, Lf/h/a/c/t0$c;->f:I

    if-ne v1, v4, :cond_3

    iget-object v12, v8, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v13, v8, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    iget-object v14, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    const-wide v16, -0x7fffffffffffffffL    # -4.9E-324

    invoke-static {v9, v10, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v18

    invoke-virtual/range {v12 .. v19}, Lf/h/a/c/t0;->k(Lf/h/a/c/t0$c;Lf/h/a/c/t0$b;IJJ)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_1

    return-object v11

    :cond_1
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v0, v0, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v0, v0, Lf/h/a/c/d1/p$a;->d:J

    goto :goto_0

    :cond_2
    iget-wide v0, v8, Lf/h/a/c/h0;->c:J

    const-wide/16 v5, 0x1

    add-long/2addr v5, v0

    iput-wide v5, v8, Lf/h/a/c/h0;->c:J

    :goto_0
    move-wide v6, v3

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    move-wide v4, v0

    move-object v1, v2

    goto :goto_1

    :cond_3
    move-object v1, v5

    move-wide v4, v6

    move-wide v6, v9

    :goto_1
    move-object/from16 v0, p0

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/h0;->l(Ljava/lang/Object;JJ)Lf/h/a/c/d1/p$a;

    move-result-object v1

    move-wide v2, v9

    move-wide v4, v6

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/h0;->d(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/g0;

    move-result-object v0

    return-object v0

    :cond_4
    iget-object v0, v1, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-object v4, v8, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v5, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v6, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v4, v5, v6}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    invoke-virtual {v0}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v4

    if-eqz v4, :cond_a

    iget v4, v0, Lf/h/a/c/d1/p$a;->b:I

    iget-object v5, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-object v5, v5, Lf/h/a/c/t0$b;->e:Lf/h/a/c/d1/y/a;

    iget-object v5, v5, Lf/h/a/c/d1/y/a;->c:[Lf/h/a/c/d1/y/a$a;

    aget-object v6, v5, v4

    iget v6, v6, Lf/h/a/c/d1/y/a$a;->a:I

    if-ne v6, v7, :cond_5

    return-object v11

    :cond_5
    iget v7, v0, Lf/h/a/c/d1/p$a;->c:I

    aget-object v5, v5, v4

    invoke-virtual {v5, v7}, Lf/h/a/c/d1/y/a$a;->a(I)I

    move-result v5

    if-ge v5, v6, :cond_7

    iget-object v2, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v2, v4, v5}, Lf/h/a/c/t0$b;->f(II)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_2

    :cond_6
    iget-object v2, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-wide v6, v1, Lf/h/a/c/g0;->c:J

    iget-wide v9, v0, Lf/h/a/c/d1/p$a;->d:J

    move-object/from16 v0, p0

    move-object v1, v2

    move v2, v4

    move v3, v5

    move-wide v4, v6

    move-wide v6, v9

    invoke-virtual/range {v0 .. v7}, Lf/h/a/c/h0;->e(Ljava/lang/Object;IIJJ)Lf/h/a/c/g0;

    move-result-object v11

    :goto_2
    return-object v11

    :cond_7
    iget-wide v4, v1, Lf/h/a/c/g0;->c:J

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v4, v6

    if-nez v1, :cond_9

    iget-object v12, v8, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v13, v8, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    iget-object v14, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget v15, v14, Lf/h/a/c/t0$b;->b:I

    const-wide v16, -0x7fffffffffffffffL    # -4.9E-324

    invoke-static {v9, v10, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v18

    invoke-virtual/range {v12 .. v19}, Lf/h/a/c/t0;->k(Lf/h/a/c/t0$c;Lf/h/a/c/t0$b;IJJ)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_8

    return-object v11

    :cond_8
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    move-wide v2, v1

    goto :goto_3

    :cond_9
    move-wide v2, v4

    :goto_3
    iget-object v1, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-wide v4, v0, Lf/h/a/c/d1/p$a;->d:J

    move-object/from16 v0, p0

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/h0;->f(Ljava/lang/Object;JJ)Lf/h/a/c/g0;

    move-result-object v0

    return-object v0

    :cond_a
    iget-object v2, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-wide v3, v1, Lf/h/a/c/g0;->d:J

    invoke-virtual {v2, v3, v4}, Lf/h/a/c/t0$b;->c(J)I

    move-result v2

    if-ne v2, v7, :cond_b

    iget-object v2, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-wide v3, v1, Lf/h/a/c/g0;->e:J

    iget-wide v5, v0, Lf/h/a/c/d1/p$a;->d:J

    move-object/from16 v0, p0

    move-object v1, v2

    move-wide v2, v3

    move-wide v4, v5

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/h0;->f(Ljava/lang/Object;JJ)Lf/h/a/c/g0;

    move-result-object v0

    return-object v0

    :cond_b
    iget-object v3, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v3, v2}, Lf/h/a/c/t0$b;->e(I)I

    move-result v3

    iget-object v4, v8, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v4, v2, v3}, Lf/h/a/c/t0$b;->f(II)Z

    move-result v4

    if-nez v4, :cond_c

    goto :goto_4

    :cond_c
    iget-object v4, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-wide v5, v1, Lf/h/a/c/g0;->e:J

    iget-wide v9, v0, Lf/h/a/c/d1/p$a;->d:J

    move-object/from16 v0, p0

    move-object v1, v4

    move-wide v4, v5

    move-wide v6, v9

    invoke-virtual/range {v0 .. v7}, Lf/h/a/c/h0;->e(Ljava/lang/Object;IIJJ)Lf/h/a/c/g0;

    move-result-object v11

    :goto_4
    return-object v11
.end method

.method public final d(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/g0;
    .locals 8

    iget-object v0, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v1, p1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v2, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    invoke-virtual {p1}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p4, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget p5, p1, Lf/h/a/c/d1/p$a;->b:I

    iget v0, p1, Lf/h/a/c/d1/p$a;->c:I

    invoke-virtual {p4, p5, v0}, Lf/h/a/c/t0$b;->f(II)Z

    move-result p4

    if-nez p4, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget v2, p1, Lf/h/a/c/d1/p$a;->b:I

    iget v3, p1, Lf/h/a/c/d1/p$a;->c:I

    iget-wide v6, p1, Lf/h/a/c/d1/p$a;->d:J

    move-object v0, p0

    move-wide v4, p2

    invoke-virtual/range {v0 .. v7}, Lf/h/a/c/h0;->e(Ljava/lang/Object;IIJJ)Lf/h/a/c/g0;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object v1, p1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-wide v4, p1, Lf/h/a/c/d1/p$a;->d:J

    move-object v0, p0

    move-wide v2, p4

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/h0;->f(Ljava/lang/Object;JJ)Lf/h/a/c/g0;

    move-result-object p1

    return-object p1
.end method

.method public final e(Ljava/lang/Object;IIJJ)Lf/h/a/c/g0;
    .locals 16

    move-object/from16 v0, p0

    new-instance v7, Lf/h/a/c/d1/p$a;

    move-object v1, v7

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-wide/from16 v5, p6

    invoke-direct/range {v1 .. v6}, Lf/h/a/c/d1/p$a;-><init>(Ljava/lang/Object;IIJ)V

    iget-object v1, v0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v2, v0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    move-object/from16 v3, p1

    invoke-virtual {v1, v3}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v2, v4}, Lf/h/a/c/t0;->g(ILf/h/a/c/t0$b;Z)Lf/h/a/c/t0$b;

    move-result-object v1

    iget v2, v7, Lf/h/a/c/d1/p$a;->b:I

    iget v3, v7, Lf/h/a/c/d1/p$a;->c:I

    invoke-virtual {v1, v2, v3}, Lf/h/a/c/t0$b;->a(II)J

    move-result-wide v9

    iget-object v1, v0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-object v1, v1, Lf/h/a/c/t0$b;->e:Lf/h/a/c/d1/y/a;

    iget-object v1, v1, Lf/h/a/c/d1/y/a;->c:[Lf/h/a/c/d1/y/a$a;

    aget-object v1, v1, p2

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lf/h/a/c/d1/y/a$a;->a(I)I

    move-result v1

    move/from16 v2, p3

    if-ne v2, v1, :cond_0

    iget-object v1, v0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-object v1, v1, Lf/h/a/c/t0$b;->e:Lf/h/a/c/d1/y/a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-wide/16 v3, 0x0

    new-instance v13, Lf/h/a/c/g0;

    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v1, v13

    move-object v2, v7

    move-wide/from16 v5, p4

    move-wide v7, v11

    move v11, v14

    move v12, v15

    invoke-direct/range {v1 .. v12}, Lf/h/a/c/g0;-><init>(Lf/h/a/c/d1/p$a;JJJJZZ)V

    return-object v13
.end method

.method public final f(Ljava/lang/Object;JJ)Lf/h/a/c/g0;
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    move-wide/from16 v4, p2

    invoke-virtual {v1, v4, v5}, Lf/h/a/c/t0$b;->b(J)I

    move-result v1

    new-instance v3, Lf/h/a/c/d1/p$a;

    move-object/from16 v2, p1

    move-wide/from16 v6, p4

    invoke-direct {v3, v2, v6, v7, v1}, Lf/h/a/c/d1/p$a;-><init>(Ljava/lang/Object;JI)V

    invoke-virtual {v3}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v2

    const/4 v6, -0x1

    if-nez v2, :cond_0

    iget v2, v3, Lf/h/a/c/d1/p$a;->e:I

    if-ne v2, v6, :cond_0

    const/4 v2, 0x1

    const/4 v12, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v12, 0x0

    :goto_0
    invoke-virtual {v0, v3, v12}, Lf/h/a/c/h0;->h(Lf/h/a/c/d1/p$a;Z)Z

    move-result v13

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    if-eq v1, v6, :cond_1

    iget-object v2, v0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v2, v1}, Lf/h/a/c/t0$b;->d(I)J

    move-result-wide v1

    move-wide v9, v1

    goto :goto_1

    :cond_1
    move-wide v9, v7

    :goto_1
    cmp-long v1, v9, v7

    if-eqz v1, :cond_3

    const-wide/high16 v1, -0x8000000000000000L

    cmp-long v6, v9, v1

    if-nez v6, :cond_2

    goto :goto_2

    :cond_2
    move-wide v14, v9

    goto :goto_3

    :cond_3
    :goto_2
    iget-object v1, v0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-wide v1, v1, Lf/h/a/c/t0$b;->c:J

    move-wide v14, v1

    :goto_3
    new-instance v1, Lf/h/a/c/g0;

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    move-object v2, v1

    move-wide/from16 v4, p2

    move-wide v8, v9

    move-wide v10, v14

    invoke-direct/range {v2 .. v13}, Lf/h/a/c/g0;-><init>(Lf/h/a/c/d1/p$a;JJJJZZ)V

    return-object v1
.end method

.method public g(Lf/h/a/c/g0;)Lf/h/a/c/g0;
    .locals 13

    iget-object v1, p1, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {v1}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, v1, Lf/h/a/c/d1/p$a;->e:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v10, 0x0

    :goto_0
    invoke-virtual {p0, v1, v10}, Lf/h/a/c/h0;->h(Lf/h/a/c/d1/p$a;Z)Z

    move-result v11

    iget-object v0, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v2, p1, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-object v2, v2, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v3, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v0, v2, v3}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    invoke-virtual {v1}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget v2, v1, Lf/h/a/c/d1/p$a;->b:I

    iget v3, v1, Lf/h/a/c/d1/p$a;->c:I

    invoke-virtual {v0, v2, v3}, Lf/h/a/c/t0$b;->a(II)J

    move-result-wide v2

    :cond_1
    :goto_1
    move-wide v8, v2

    goto :goto_2

    :cond_2
    iget-wide v2, p1, Lf/h/a/c/g0;->d:J

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    :cond_3
    iget-object v0, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-wide v2, v0, Lf/h/a/c/t0$b;->c:J

    goto :goto_1

    :goto_2
    new-instance v12, Lf/h/a/c/g0;

    iget-wide v2, p1, Lf/h/a/c/g0;->b:J

    iget-wide v4, p1, Lf/h/a/c/g0;->c:J

    iget-wide v6, p1, Lf/h/a/c/g0;->d:J

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lf/h/a/c/g0;-><init>(Lf/h/a/c/d1/p$a;JJJJZZ)V

    return-object v12
.end method

.method public final h(Lf/h/a/c/d1/p$a;Z)Z
    .locals 8

    iget-object v0, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object p1, p1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v2

    iget-object p1, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v0, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {p1, v2, v0}, Lf/h/a/c/t0;->f(ILf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object p1

    iget p1, p1, Lf/h/a/c/t0$b;->b:I

    iget-object v0, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v1, p0, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    invoke-virtual {v0, p1, v1}, Lf/h/a/c/t0;->m(ILf/h/a/c/t0$c;)Lf/h/a/c/t0$c;

    move-result-object p1

    iget-boolean p1, p1, Lf/h/a/c/t0$c;->e:Z

    const/4 v0, 0x0

    const/4 v7, 0x1

    if-nez p1, :cond_1

    iget-object v1, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v3, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-object v4, p0, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    iget v5, p0, Lf/h/a/c/h0;->e:I

    iget-boolean v6, p0, Lf/h/a/c/h0;->f:Z

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/t0;->d(ILf/h/a/c/t0$b;Lf/h/a/c/t0$c;IZ)I

    move-result p1

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public i(J)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/h/a/c/f0;->f()Z

    move-result v1

    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iget-boolean v1, v0, Lf/h/a/c/f0;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    iget-wide v2, v0, Lf/h/a/c/f0;->n:J

    sub-long/2addr p1, v2

    invoke-interface {v1, p1, p2}, Lf/h/a/c/d1/o;->s(J)V

    :cond_0
    return-void
.end method

.method public j(Lf/h/a/c/f0;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Lf/g/j/k/a;->s(Z)V

    iput-object p1, p0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    :goto_1
    iget-object p1, p1, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-eqz p1, :cond_2

    iget-object v2, p0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-ne p1, v2, :cond_1

    iget-object v0, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iput-object v0, p0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p1}, Lf/h/a/c/f0;->g()V

    iget v2, p0, Lf/h/a/c/h0;->j:I

    sub-int/2addr v2, v1

    iput v2, p0, Lf/h/a/c/h0;->j:I

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    const/4 v1, 0x0

    iget-object v2, p1, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-nez v2, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lf/h/a/c/f0;->b()V

    iput-object v1, p1, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    invoke-virtual {p1}, Lf/h/a/c/f0;->c()V

    :goto_2
    return v0
.end method

.method public k(Ljava/lang/Object;J)Lf/h/a/c/d1/p$a;
    .locals 9

    iget-object v0, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v1, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v0, p1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Lf/h/a/c/t0;->g(ILf/h/a/c/t0$b;Z)Lf/h/a/c/t0$b;

    move-result-object v0

    iget v0, v0, Lf/h/a/c/t0$b;->b:I

    iget-object v1, p0, Lf/h/a/c/h0;->k:Ljava/lang/Object;

    const/4 v2, -0x1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    invoke-virtual {v3, v1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v1

    if-eq v1, v2, :cond_1

    iget-object v3, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v4, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v3, v1, v4}, Lf/h/a/c/t0;->f(ILf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object v1

    iget v1, v1, Lf/h/a/c/t0$b;->b:I

    if-ne v1, v0, :cond_1

    iget-wide v0, p0, Lf/h/a/c/h0;->l:J

    :cond_0
    :goto_0
    move-wide v7, v0

    goto :goto_3

    :cond_1
    iget-object v1, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    :goto_1
    if-eqz v1, :cond_3

    iget-object v3, v1, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v0, v0, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v0, v0, Lf/h/a/c/d1/p$a;->d:J

    goto :goto_0

    :cond_2
    iget-object v1, v1, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    :goto_2
    if-eqz v1, :cond_5

    iget-object v3, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v4, v1, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v3

    if-eq v3, v2, :cond_4

    iget-object v4, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v5, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v4, v3, v5}, Lf/h/a/c/t0;->f(ILf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object v3

    iget v3, v3, Lf/h/a/c/t0$b;->b:I

    if-ne v3, v0, :cond_4

    iget-object v0, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v0, v0, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v0, v0, Lf/h/a/c/d1/p$a;->d:J

    goto :goto_0

    :cond_4
    iget-object v1, v1, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    goto :goto_2

    :cond_5
    iget-wide v0, p0, Lf/h/a/c/h0;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lf/h/a/c/h0;->c:J

    iget-object v2, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-nez v2, :cond_0

    iput-object p1, p0, Lf/h/a/c/h0;->k:Ljava/lang/Object;

    iput-wide v0, p0, Lf/h/a/c/h0;->l:J

    goto :goto_0

    :goto_3
    move-object v3, p0

    move-object v4, p1

    move-wide v5, p2

    invoke-virtual/range {v3 .. v8}, Lf/h/a/c/h0;->l(Ljava/lang/Object;JJ)Lf/h/a/c/d1/p$a;

    move-result-object p1

    return-object p1
.end method

.method public final l(Ljava/lang/Object;JJ)Lf/h/a/c/d1/p$a;
    .locals 7

    iget-object v0, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v1, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v0, p1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Lf/h/a/c/t0;->g(ILf/h/a/c/t0$b;Z)Lf/h/a/c/t0$b;

    iget-object v0, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v0, p2, p3}, Lf/h/a/c/t0$b;->c(J)I

    move-result v3

    const/4 v0, -0x1

    if-ne v3, v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {v0, p2, p3}, Lf/h/a/c/t0$b;->b(J)I

    move-result p2

    new-instance p3, Lf/h/a/c/d1/p$a;

    invoke-direct {p3, p1, p4, p5, p2}, Lf/h/a/c/d1/p$a;-><init>(Ljava/lang/Object;JI)V

    return-object p3

    :cond_0
    iget-object p2, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    invoke-virtual {p2, v3}, Lf/h/a/c/t0$b;->e(I)I

    move-result v4

    new-instance p2, Lf/h/a/c/d1/p$a;

    move-object v1, p2

    move-object v2, p1

    move-wide v5, p4

    invoke-direct/range {v1 .. v6}, Lf/h/a/c/d1/p$a;-><init>(Ljava/lang/Object;IIJ)V

    return-object p2
.end method

.method public final m()Z
    .locals 8

    iget-object v0, p0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v3, v0, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v2

    move v3, v2

    :goto_0
    iget-object v2, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v4, p0, Lf/h/a/c/h0;->a:Lf/h/a/c/t0$b;

    iget-object v5, p0, Lf/h/a/c/h0;->b:Lf/h/a/c/t0$c;

    iget v6, p0, Lf/h/a/c/h0;->e:I

    iget-boolean v7, p0, Lf/h/a/c/h0;->f:Z

    invoke-virtual/range {v2 .. v7}, Lf/h/a/c/t0;->d(ILf/h/a/c/t0$b;Lf/h/a/c/t0$c;IZ)I

    move-result v3

    :goto_1
    iget-object v2, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-eqz v2, :cond_1

    iget-object v4, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-boolean v4, v4, Lf/h/a/c/g0;->f:Z

    if-nez v4, :cond_1

    move-object v0, v2

    goto :goto_1

    :cond_1
    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    if-nez v2, :cond_2

    goto :goto_2

    :cond_2
    iget-object v4, p0, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v5, v2, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v4

    if-eq v4, v3, :cond_3

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_0

    :cond_4
    :goto_2
    invoke-virtual {p0, v0}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    move-result v2

    iget-object v3, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    invoke-virtual {p0, v3}, Lf/h/a/c/h0;->g(Lf/h/a/c/g0;)Lf/h/a/c/g0;

    move-result-object v3

    iput-object v3, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    xor-int/lit8 v0, v2, 0x1

    return v0
.end method
