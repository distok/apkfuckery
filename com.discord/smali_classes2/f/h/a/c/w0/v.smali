.class public final Lf/h/a/c/w0/v;
.super Lf/h/a/c/w0/p;
.source "SilenceSkippingAudioProcessor.java"


# instance fields
.field public i:I

.field public j:Z

.field public k:[B

.field public l:[B

.field public m:I

.field public n:I

.field public o:I

.field public p:Z

.field public q:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/a/c/w0/p;-><init>()V

    sget-object v0, Lf/h/a/c/i1/a0;->f:[B

    iput-object v0, p0, Lf/h/a/c/w0/v;->k:[B

    iput-object v0, p0, Lf/h/a/c/w0/v;->l:[B

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/w0/v;->j:Z

    return v0
.end method

.method public b(Ljava/nio/ByteBuffer;)V
    .locals 9

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lf/h/a/c/w0/p;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_b

    iget v0, p0, Lf/h/a/c/w0/v;->m:I

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eq v0, v1, :cond_2

    if-ne v0, v3, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p0, p1}, Lf/h/a/c/w0/v;->k(Ljava/nio/ByteBuffer;)I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-wide v3, p0, Lf/h/a/c/w0/v;->q:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    iget v6, p0, Lf/h/a/c/w0/v;->i:I

    div-int/2addr v5, v6

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, p0, Lf/h/a/c/w0/v;->q:J

    iget-object v3, p0, Lf/h/a/c/w0/v;->l:[B

    iget v4, p0, Lf/h/a/c/w0/v;->o:I

    invoke-virtual {p0, p1, v3, v4}, Lf/h/a/c/w0/v;->m(Ljava/nio/ByteBuffer;[BI)V

    if-ge v1, v0, :cond_0

    iget-object v1, p0, Lf/h/a/c/w0/v;->l:[B

    iget v3, p0, Lf/h/a/c/w0/v;->o:I

    invoke-virtual {p0, v1, v3}, Lf/h/a/c/w0/v;->l([BI)V

    iput v2, p0, Lf/h/a/c/w0/v;->m:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p0, p1}, Lf/h/a/c/w0/v;->k(Ljava/nio/ByteBuffer;)I

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    sub-int v4, v1, v4

    iget-object v5, p0, Lf/h/a/c/w0/v;->k:[B

    array-length v6, v5

    iget v7, p0, Lf/h/a/c/w0/v;->n:I

    sub-int/2addr v6, v7

    if-ge v1, v0, :cond_3

    if-ge v4, v6, :cond_3

    invoke-virtual {p0, v5, v7}, Lf/h/a/c/w0/v;->l([BI)V

    iput v2, p0, Lf/h/a/c/w0/v;->n:I

    iput v2, p0, Lf/h/a/c/w0/v;->m:I

    goto :goto_0

    :cond_3
    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v4, p0, Lf/h/a/c/w0/v;->k:[B

    iget v5, p0, Lf/h/a/c/w0/v;->n:I

    invoke-virtual {p1, v4, v5, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    iget v4, p0, Lf/h/a/c/w0/v;->n:I

    add-int/2addr v4, v1

    iput v4, p0, Lf/h/a/c/w0/v;->n:I

    iget-object v1, p0, Lf/h/a/c/w0/v;->k:[B

    array-length v5, v1

    if-ne v4, v5, :cond_5

    iget-boolean v5, p0, Lf/h/a/c/w0/v;->p:Z

    if-eqz v5, :cond_4

    iget v4, p0, Lf/h/a/c/w0/v;->o:I

    invoke-virtual {p0, v1, v4}, Lf/h/a/c/w0/v;->l([BI)V

    iget-wide v4, p0, Lf/h/a/c/w0/v;->q:J

    iget v1, p0, Lf/h/a/c/w0/v;->n:I

    iget v6, p0, Lf/h/a/c/w0/v;->o:I

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v1, v6

    iget v6, p0, Lf/h/a/c/w0/v;->i:I

    div-int/2addr v1, v6

    int-to-long v6, v1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lf/h/a/c/w0/v;->q:J

    goto :goto_1

    :cond_4
    iget-wide v5, p0, Lf/h/a/c/w0/v;->q:J

    iget v1, p0, Lf/h/a/c/w0/v;->o:I

    sub-int/2addr v4, v1

    iget v1, p0, Lf/h/a/c/w0/v;->i:I

    div-int/2addr v4, v1

    int-to-long v7, v4

    add-long/2addr v5, v7

    iput-wide v5, p0, Lf/h/a/c/w0/v;->q:J

    :goto_1
    iget-object v1, p0, Lf/h/a/c/w0/v;->k:[B

    iget v4, p0, Lf/h/a/c/w0/v;->n:I

    invoke-virtual {p0, p1, v1, v4}, Lf/h/a/c/w0/v;->m(Ljava/nio/ByteBuffer;[BI)V

    iput v2, p0, Lf/h/a/c/w0/v;->n:I

    iput v3, p0, Lf/h/a/c/w0/v;->m:I

    :cond_5
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    iget-object v3, p0, Lf/h/a/c/w0/v;->k:[B

    array-length v3, v3

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    if-lt v2, v3, :cond_8

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_7

    iget v3, p0, Lf/h/a/c/w0/v;->i:I

    div-int/2addr v2, v3

    mul-int v2, v2, v3

    add-int/2addr v2, v3

    goto :goto_3

    :cond_7
    add-int/lit8 v2, v2, -0x2

    goto :goto_2

    :cond_8
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    :goto_3
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    if-ne v2, v3, :cond_9

    iput v1, p0, Lf/h/a/c/w0/v;->m:I

    goto :goto_4

    :cond_9
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {p0, v2}, Lf/h/a/c/w0/p;->j(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    if-lez v2, :cond_a

    iput-boolean v1, p0, Lf/h/a/c/w0/v;->p:Z

    :cond_a
    :goto_4
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto/16 :goto_0

    :cond_b
    return-void
.end method

.method public e(Lcom/google/android/exoplayer2/audio/AudioProcessor$a;)Lcom/google/android/exoplayer2/audio/AudioProcessor$a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioProcessor$UnhandledAudioFormatException;
        }
    .end annotation

    iget v0, p1, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lf/h/a/c/w0/v;->j:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->e:Lcom/google/android/exoplayer2/audio/AudioProcessor$a;

    :goto_0
    return-object p1

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/audio/AudioProcessor$UnhandledAudioFormatException;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/audio/AudioProcessor$UnhandledAudioFormatException;-><init>(Lcom/google/android/exoplayer2/audio/AudioProcessor$a;)V

    throw v0
.end method

.method public f()V
    .locals 8

    iget-boolean v0, p0, Lf/h/a/c/w0/v;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/c/w0/p;->b:Lcom/google/android/exoplayer2/audio/AudioProcessor$a;

    iget v1, v0, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->d:I

    iput v1, p0, Lf/h/a/c/w0/v;->i:I

    const-wide/32 v2, 0x249f0

    iget v0, v0, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->a:I

    int-to-long v4, v0

    mul-long v2, v2, v4

    const-wide/32 v6, 0xf4240

    div-long/2addr v2, v6

    long-to-int v0, v2

    mul-int v0, v0, v1

    iget-object v2, p0, Lf/h/a/c/w0/v;->k:[B

    array-length v2, v2

    if-eq v2, v0, :cond_0

    new-array v0, v0, [B

    iput-object v0, p0, Lf/h/a/c/w0/v;->k:[B

    :cond_0
    const-wide/16 v2, 0x4e20

    mul-long v4, v4, v2

    div-long/2addr v4, v6

    long-to-int v0, v4

    mul-int v0, v0, v1

    iput v0, p0, Lf/h/a/c/w0/v;->o:I

    iget-object v1, p0, Lf/h/a/c/w0/v;->l:[B

    array-length v1, v1

    if-eq v1, v0, :cond_1

    new-array v0, v0, [B

    iput-object v0, p0, Lf/h/a/c/w0/v;->l:[B

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/w0/v;->m:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lf/h/a/c/w0/v;->q:J

    iput v0, p0, Lf/h/a/c/w0/v;->n:I

    iput-boolean v0, p0, Lf/h/a/c/w0/v;->p:Z

    return-void
.end method

.method public h()V
    .locals 4

    iget v0, p0, Lf/h/a/c/w0/v;->n:I

    if-lez v0, :cond_0

    iget-object v1, p0, Lf/h/a/c/w0/v;->k:[B

    invoke-virtual {p0, v1, v0}, Lf/h/a/c/w0/v;->l([BI)V

    :cond_0
    iget-boolean v0, p0, Lf/h/a/c/w0/v;->p:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lf/h/a/c/w0/v;->q:J

    iget v2, p0, Lf/h/a/c/w0/v;->o:I

    iget v3, p0, Lf/h/a/c/w0/v;->i:I

    div-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h/a/c/w0/v;->q:J

    :cond_1
    return-void
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/w0/v;->j:Z

    iput v0, p0, Lf/h/a/c/w0/v;->o:I

    sget-object v0, Lf/h/a/c/i1/a0;->f:[B

    iput-object v0, p0, Lf/h/a/c/w0/v;->k:[B

    iput-object v0, p0, Lf/h/a/c/w0/v;->l:[B

    return-void
.end method

.method public final k(Ljava/nio/ByteBuffer;)I
    .locals 3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/4 v2, 0x4

    if-le v1, v2, :cond_0

    iget p1, p0, Lf/h/a/c/w0/v;->i:I

    div-int/2addr v0, p1

    mul-int v0, v0, p1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result p1

    return p1
.end method

.method public final l([BI)V
    .locals 2

    invoke-virtual {p0, p2}, Lf/h/a/c/w0/p;->j(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    if-lez p2, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/w0/v;->p:Z

    :cond_0
    return-void
.end method

.method public final m(Ljava/nio/ByteBuffer;[BI)V
    .locals 4

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget v1, p0, Lf/h/a/c/w0/v;->o:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lf/h/a/c/w0/v;->o:I

    sub-int/2addr v1, v0

    sub-int/2addr p3, v1

    iget-object v2, p0, Lf/h/a/c/w0/v;->l:[B

    const/4 v3, 0x0

    invoke-static {p2, p3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result p2

    sub-int/2addr p2, v0

    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object p2, p0, Lf/h/a/c/w0/v;->l:[B

    invoke-virtual {p1, p2, v1, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    return-void
.end method
