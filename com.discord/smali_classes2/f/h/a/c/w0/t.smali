.class public Lf/h/a/c/w0/t;
.super Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;
.source "MediaCodecAudioRenderer.java"

# interfaces
.implements Lf/h/a/c/i1/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/w0/t$b;
    }
.end annotation


# instance fields
.field public A0:Z

.field public B0:Z

.field public C0:Z

.field public D0:Landroid/media/MediaFormat;

.field public E0:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public F0:J

.field public G0:Z

.field public H0:Z

.field public I0:J

.field public J0:I

.field public final v0:Landroid/content/Context;

.field public final w0:Lf/h/a/c/w0/l$a;

.field public final x0:Lcom/google/android/exoplayer2/audio/AudioSink;

.field public final y0:[J

.field public z0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/h/a/c/b1/f;Lf/h/a/c/z0/b;ZZLandroid/os/Handler;Lf/h/a/c/w0/l;Lcom/google/android/exoplayer2/audio/AudioSink;)V
    .locals 7
    .param p3    # Lf/h/a/c/z0/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lf/h/a/c/w0/l;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lf/h/a/c/b1/f;",
            "Lf/h/a/c/z0/b<",
            "Lf/h/a/c/z0/e;",
            ">;ZZ",
            "Landroid/os/Handler;",
            "Lf/h/a/c/w0/l;",
            "Lcom/google/android/exoplayer2/audio/AudioSink;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v1, 0x1

    const v6, 0x472c4400    # 44100.0f

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;-><init>(ILf/h/a/c/b1/f;Lf/h/a/c/z0/b;ZZF)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/w0/t;->v0:Landroid/content/Context;

    iput-object p8, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lf/h/a/c/w0/t;->I0:J

    const/16 p1, 0xa

    new-array p1, p1, [J

    iput-object p1, p0, Lf/h/a/c/w0/t;->y0:[J

    new-instance p1, Lf/h/a/c/w0/l$a;

    invoke-direct {p1, p6, p7}, Lf/h/a/c/w0/l$a;-><init>(Landroid/os/Handler;Lf/h/a/c/w0/l;)V

    iput-object p1, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    new-instance p1, Lf/h/a/c/w0/t$b;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lf/h/a/c/w0/t$b;-><init>(Lf/h/a/c/w0/t;Lf/h/a/c/w0/t$a;)V

    check-cast p8, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iput-object p1, p8, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j:Lcom/google/android/exoplayer2/audio/AudioSink$a;

    return-void
.end method


# virtual methods
.method public A(JZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 p3, 0x0

    iput-boolean p3, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->n0:Z

    iput-boolean p3, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->o0:Z

    iput-boolean p3, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->s0:Z

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->P()Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->v:Lf/h/a/c/i1/y;

    invoke-virtual {v0}, Lf/h/a/c/i1/y;->b()V

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V

    iput-wide p1, p0, Lf/h/a/c/w0/t;->F0:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/w0/t;->G0:Z

    iput-boolean p1, p0, Lf/h/a/c/w0/t;->H0:Z

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lf/h/a/c/w0/t;->I0:J

    iput p3, p0, Lf/h/a/c/w0/t;->J0:I

    return-void
.end method

.method public B()V
    .locals 2

    :try_start_0
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->B()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->n()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->n()V

    throw v0
.end method

.method public C()V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k()V

    return-void
.end method

.method public D()V
    .locals 8

    invoke-virtual {p0}, Lf/h/a/c/w0/t;->t0()V

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->L:Z

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lf/h/a/c/w0/n;->j:J

    iput v1, v2, Lf/h/a/c/w0/n;->u:I

    iput v1, v2, Lf/h/a/c/w0/n;->t:I

    iput-wide v3, v2, Lf/h/a/c/w0/n;->k:J

    iget-wide v3, v2, Lf/h/a/c/w0/n;->v:J

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    iget-object v1, v2, Lf/h/a/c/w0/n;->f:Lf/h/a/c/w0/m;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lf/h/a/c/w0/m;->a()V

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    :cond_1
    return-void
.end method

.method public E([Lcom/google/android/exoplayer2/Format;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-wide p1, p0, Lf/h/a/c/w0/t;->I0:J

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p3, p1, v0

    if-eqz p3, :cond_1

    iget p1, p0, Lf/h/a/c/w0/t;->J0:I

    iget-object p2, p0, Lf/h/a/c/w0/t;->y0:[J

    array-length p2, p2

    if-ne p1, p2, :cond_0

    const-string p1, "Too many stream changes, so dropping change at "

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/c/w0/t;->y0:[J

    iget p3, p0, Lf/h/a/c/w0/t;->J0:I

    add-int/lit8 p3, p3, -0x1

    aget-wide v0, p2, p3

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MediaCodecAudioRenderer"

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lf/h/a/c/w0/t;->J0:I

    :goto_0
    iget-object p1, p0, Lf/h/a/c/w0/t;->y0:[J

    iget p2, p0, Lf/h/a/c/w0/t;->J0:I

    add-int/lit8 p2, p2, -0x1

    iget-wide v0, p0, Lf/h/a/c/w0/t;->I0:J

    aput-wide v0, p1, p2

    :cond_1
    return-void
.end method

.method public J(Landroid/media/MediaCodec;Lf/h/a/c/b1/e;Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/Format;)I
    .locals 2

    invoke-virtual {p0, p2, p4}, Lf/h/a/c/w0/t;->r0(Lf/h/a/c/b1/e;Lcom/google/android/exoplayer2/Format;)I

    move-result p1

    iget v0, p0, Lf/h/a/c/w0/t;->z0:I

    const/4 v1, 0x0

    if-gt p1, v0, :cond_3

    iget p1, p3, Lcom/google/android/exoplayer2/Format;->B:I

    if-nez p1, :cond_3

    iget p1, p3, Lcom/google/android/exoplayer2/Format;->C:I

    if-nez p1, :cond_3

    iget p1, p4, Lcom/google/android/exoplayer2/Format;->B:I

    if-nez p1, :cond_3

    iget p1, p4, Lcom/google/android/exoplayer2/Format;->C:I

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p2, p3, p4, p1}, Lf/h/a/c/b1/e;->f(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/Format;Z)Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p1, 0x3

    return p1

    :cond_1
    iget-object p2, p3, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    iget-object v0, p4, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {p2, v0}, Lf/h/a/c/i1/a0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    iget p2, p3, Lcom/google/android/exoplayer2/Format;->y:I

    iget v0, p4, Lcom/google/android/exoplayer2/Format;->y:I

    if-ne p2, v0, :cond_2

    iget p2, p3, Lcom/google/android/exoplayer2/Format;->z:I

    iget v0, p4, Lcom/google/android/exoplayer2/Format;->z:I

    if-ne p2, v0, :cond_2

    iget p2, p3, Lcom/google/android/exoplayer2/Format;->A:I

    iget v0, p4, Lcom/google/android/exoplayer2/Format;->A:I

    if-ne p2, v0, :cond_2

    invoke-virtual {p3, p4}, Lcom/google/android/exoplayer2/Format;->p(Lcom/google/android/exoplayer2/Format;)Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p3, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string p3, "audio/opus"

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_3

    return p1

    :cond_3
    :goto_1
    return v1
.end method

.method public K(Lf/h/a/c/b1/e;Landroid/media/MediaCodec;Lcom/google/android/exoplayer2/Format;Landroid/media/MediaCrypto;F)V
    .locals 8
    .param p4    # Landroid/media/MediaCrypto;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/c/t;->j:[Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0, p1, p3}, Lf/h/a/c/w0/t;->r0(Lf/h/a/c/b1/e;Lcom/google/android/exoplayer2/Format;)I

    move-result v1

    array-length v2, v0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v2, v3, :cond_0

    goto :goto_1

    :cond_0
    array-length v2, v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v2, :cond_2

    aget-object v6, v0, v5

    invoke-virtual {p1, p3, v6, v4}, Lf/h/a/c/b1/e;->f(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/Format;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p0, p1, v6}, Lf/h/a/c/w0/t;->r0(Lf/h/a/c/b1/e;Lcom/google/android/exoplayer2/Format;)I

    move-result v6

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    iput v1, p0, Lf/h/a/c/w0/t;->z0:I

    iget-object v0, p1, Lf/h/a/c/b1/e;->a:Ljava/lang/String;

    sget v1, Lf/h/a/c/i1/a0;->a:I

    const/16 v2, 0x18

    const-string v5, "samsung"

    if-ge v1, v2, :cond_4

    const-string v2, "OMX.SEC.aac.dec"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lf/h/a/c/i1/a0;->c:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lf/h/a/c/i1/a0;->b:Ljava/lang/String;

    const-string/jumbo v2, "zeroflte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "herolte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "heroqlte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lf/h/a/c/w0/t;->B0:Z

    iget-object v0, p1, Lf/h/a/c/b1/e;->a:Ljava/lang/String;

    const/16 v2, 0x15

    if-ge v1, v2, :cond_6

    const-string v2, "OMX.SEC.mp3.dec"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lf/h/a/c/i1/a0;->c:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lf/h/a/c/i1/a0;->b:Ljava/lang/String;

    const-string v2, "baffin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "grand"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "fortuna"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "gprimelte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "j2y18lte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "ms01"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    :goto_3
    iput-boolean v0, p0, Lf/h/a/c/w0/t;->C0:Z

    iget-boolean v0, p1, Lf/h/a/c/b1/e;->g:Z

    iput-boolean v0, p0, Lf/h/a/c/w0/t;->A0:Z

    if-eqz v0, :cond_7

    const-string p1, "audio/raw"

    goto :goto_4

    :cond_7
    iget-object p1, p1, Lf/h/a/c/b1/e;->c:Ljava/lang/String;

    :goto_4
    iget v0, p0, Lf/h/a/c/w0/t;->z0:I

    new-instance v2, Landroid/media/MediaFormat;

    invoke-direct {v2}, Landroid/media/MediaFormat;-><init>()V

    const-string v5, "mime"

    invoke-virtual {v2, v5, p1}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    iget p1, p3, Lcom/google/android/exoplayer2/Format;->y:I

    const-string v6, "channel-count"

    invoke-virtual {v2, v6, p1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    iget p1, p3, Lcom/google/android/exoplayer2/Format;->z:I

    const-string v6, "sample-rate"

    invoke-virtual {v2, v6, p1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    iget-object p1, p3, Lcom/google/android/exoplayer2/Format;->n:Ljava/util/List;

    invoke-static {v2, p1}, Lf/g/j/k/a;->N0(Landroid/media/MediaFormat;Ljava/util/List;)V

    const-string p1, "max-input-size"

    invoke-static {v2, p1, v0}, Lf/g/j/k/a;->u0(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const/16 p1, 0x17

    if-lt v1, p1, :cond_a

    const-string v0, "priority"

    invoke-virtual {v2, v0, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p5, v0

    if-eqz v0, :cond_a

    if-ne v1, p1, :cond_9

    sget-object p1, Lf/h/a/c/i1/a0;->d:Ljava/lang/String;

    const-string v0, "ZTE B2017G"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "AXON 7 mini"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    :cond_8
    const/4 p1, 0x1

    goto :goto_5

    :cond_9
    const/4 p1, 0x0

    :goto_5
    if-nez p1, :cond_a

    const-string p1, "operating-rate"

    invoke-virtual {v2, p1, p5}, Landroid/media/MediaFormat;->setFloat(Ljava/lang/String;F)V

    :cond_a
    const/16 p1, 0x1c

    if-gt v1, p1, :cond_b

    iget-object p1, p3, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string p5, "audio/ac4"

    invoke-virtual {p5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    const-string p1, "ac4-is-sync"

    invoke-virtual {v2, p1, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    :cond_b
    const/4 p1, 0x0

    invoke-virtual {p2, v2, p1, p4, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    iget-boolean p2, p0, Lf/h/a/c/w0/t;->A0:Z

    if-eqz p2, :cond_c

    iput-object v2, p0, Lf/h/a/c/w0/t;->D0:Landroid/media/MediaFormat;

    iget-object p1, p3, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-virtual {v2, v5, p1}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_c
    iput-object p1, p0, Lf/h/a/c/w0/t;->D0:Landroid/media/MediaFormat;

    :goto_6
    return-void
.end method

.method public T(FLcom/google/android/exoplayer2/Format;[Lcom/google/android/exoplayer2/Format;)F
    .locals 4

    array-length p2, p3

    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    :goto_0
    if-ge v1, p2, :cond_1

    aget-object v3, p3, v1

    iget v3, v3, Lcom/google/android/exoplayer2/Format;->z:I

    if-eq v3, v0, :cond_0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-ne v2, v0, :cond_2

    const/high16 p1, -0x40800000    # -1.0f

    goto :goto_1

    :cond_2
    int-to-float p2, v2

    mul-float p1, p1, p2

    :goto_1
    return p1
.end method

.method public U(Lf/h/a/c/b1/f;Lcom/google/android/exoplayer2/Format;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/c/b1/f;",
            "Lcom/google/android/exoplayer2/Format;",
            "Z)",
            "Ljava/util/List<",
            "Lf/h/a/c/b1/e;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;
        }
    .end annotation

    iget-object v0, p2, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    iget v1, p2, Lcom/google/android/exoplayer2/Format;->y:I

    invoke-virtual {p0, v1, v0}, Lf/h/a/c/w0/t;->s0(ILjava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {p1}, Lf/h/a/c/b1/f;->a()Lf/h/a/c/b1/e;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-interface {p1, v0, p3, v2}, Lf/h/a/c/b1/f;->b(Ljava/lang/String;ZZ)Ljava/util/List;

    move-result-object v1

    sget-object v3, Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil;->a:Ljava/util/regex/Pattern;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v1, Lf/h/a/c/b1/c;

    invoke-direct {v1, p2}, Lf/h/a/c/b1/c;-><init>(Lcom/google/android/exoplayer2/Format;)V

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil;->i(Ljava/util/List;Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$f;)V

    const-string p2, "audio/eac3-joc"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v0, "audio/eac3"

    invoke-interface {p1, v0, p3, v2}, Lf/h/a/c/b1/f;->b(Ljava/lang/String;ZZ)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v3, p2

    :cond_3
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public Z(Ljava/lang/String;JJ)V
    .locals 9

    iget-object v1, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v7, v1, Lf/h/a/c/w0/l$a;->a:Landroid/os/Handler;

    if-eqz v7, :cond_0

    new-instance v8, Lf/h/a/c/w0/d;

    move-object v0, v8

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Lf/h/a/c/w0/d;-><init>(Lf/h/a/c/w0/l$a;Ljava/lang/String;JJ)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public a0(Lf/h/a/c/d0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->a0(Lf/h/a/c/d0;)V

    iget-object p1, p1, Lf/h/a/c/d0;->c:Lcom/google/android/exoplayer2/Format;

    iput-object p1, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    iget-object v0, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v1, v0, Lf/h/a/c/w0/l$a;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    new-instance v2, Lf/h/a/c/w0/a;

    invoke-direct {v2, v0, p1}, Lf/h/a/c/w0/a;-><init>(Lf/h/a/c/w0/l$a;Lcom/google/android/exoplayer2/Format;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public b()Lf/h/a/c/j0;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->f()Lf/h/a/c/j0;

    move-result-object v0

    return-object v0
.end method

.method public b0(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object p1, p0, Lf/h/a/c/w0/t;->D0:Landroid/media/MediaFormat;

    const-string v0, "channel-count"

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result p2

    const-string v1, "mime"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lf/h/a/c/w0/t;->s0(ILjava/lang/String;)I

    move-result p2

    move v2, p2

    move-object p2, p1

    goto :goto_1

    :cond_0
    const-string p1, "v-bits-per-sample"

    invoke-virtual {p2, p1}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, p1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Lf/h/a/c/i1/a0;->k(I)I

    move-result p1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    iget-object v1, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v2, "audio/raw"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget p1, p1, Lcom/google/android/exoplayer2/Format;->A:I

    goto :goto_0

    :cond_2
    const/4 p1, 0x2

    :goto_0
    move v2, p1

    :goto_1
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    const-string p1, "sample-rate"

    invoke-virtual {p2, p1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v4

    iget-boolean p1, p0, Lf/h/a/c/w0/t;->B0:Z

    if-eqz p1, :cond_3

    const/4 p1, 0x6

    if-ne v3, p1, :cond_3

    iget-object p2, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    iget p2, p2, Lcom/google/android/exoplayer2/Format;->y:I

    if-ge p2, p1, :cond_3

    new-array p1, p2, [I

    const/4 p2, 0x0

    :goto_2
    iget-object v0, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    iget v0, v0, Lcom/google/android/exoplayer2/Format;->y:I

    if-ge p2, v0, :cond_4

    aput p2, p1, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :cond_4
    move-object v6, p1

    :try_start_0
    iget-object p1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    const/4 v5, 0x0

    iget-object p2, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    iget v7, p2, Lcom/google/android/exoplayer2/Format;->B:I

    iget v8, p2, Lcom/google/android/exoplayer2/Format;->C:I
    :try_end_0
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$ConfigurationException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->b(IIII[III)V
    :try_end_1
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$ConfigurationException; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/t;->w(Ljava/lang/Exception;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    throw p1
.end method

.method public c()J
    .locals 2

    iget v0, p0, Lf/h/a/c/t;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/w0/t;->t0()V

    :cond_0
    iget-wide v0, p0, Lf/h/a/c/w0/t;->F0:J

    return-wide v0
.end method

.method public c0(J)V
    .locals 6
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    :goto_0
    iget v0, p0, Lf/h/a/c/w0/t;->J0:I

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/h/a/c/w0/t;->y0:[J

    const/4 v2, 0x0

    aget-wide v3, v1, v2

    cmp-long v5, p1, v3

    if-ltz v5, :cond_1

    iget-object v3, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v3, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget v4, v3, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    const/4 v4, 0x2

    iput v4, v3, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    :cond_0
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lf/h/a/c/w0/t;->J0:I

    invoke-static {v1, v5, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public d(ILjava/lang/Object;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x2

    if-eq p1, v0, :cond_7

    const/4 v0, 0x3

    if-eq p1, v0, :cond_4

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    check-cast p2, Lf/h/a/c/w0/o;

    iget-object p1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget-object v0, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->N:Lf/h/a/c/w0/o;

    invoke-virtual {v0, p2}, Lf/h/a/c/w0/o;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget v0, p2, Lf/h/a/c/w0/o;->a:I

    iget v1, p2, Lf/h/a/c/w0/o;->b:F

    iget-object v2, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    if-eqz v2, :cond_3

    iget-object v3, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->N:Lf/h/a/c/w0/o;

    iget v3, v3, Lf/h/a/c/w0/o;->a:I

    if-eq v3, v0, :cond_2

    invoke-virtual {v2, v0}, Landroid/media/AudioTrack;->attachAuxEffect(I)I

    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {v0, v1}, Landroid/media/AudioTrack;->setAuxEffectSendLevel(F)I

    :cond_3
    iput-object p2, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->N:Lf/h/a/c/w0/o;

    goto :goto_0

    :cond_4
    check-cast p2, Lf/h/a/c/w0/i;

    iget-object p1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget-object v0, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->n:Lf/h/a/c/w0/i;

    invoke-virtual {v0, p2}, Lf/h/a/c/w0/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_0

    :cond_5
    iput-object p2, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->n:Lf/h/a/c/w0/i;

    iget-boolean p2, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->O:Z

    if-eqz p2, :cond_6

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V

    const/4 p2, 0x0

    iput p2, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    goto :goto_0

    :cond_7
    iget-object p1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    check-cast p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget v0, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->B:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_8

    iput p2, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->B:F

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o()V

    :cond_8
    :goto_0
    return-void
.end method

.method public d0(Lf/h/a/c/y0/e;)V
    .locals 5

    iget-boolean v0, p0, Lf/h/a/c/w0/t;->G0:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lf/h/a/c/y0/a;->isDecodeOnly()Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p1, Lf/h/a/c/y0/e;->f:J

    iget-wide v2, p0, Lf/h/a/c/w0/t;->F0:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v2, 0x7a120

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    iget-wide v0, p1, Lf/h/a/c/y0/e;->f:J

    iput-wide v0, p0, Lf/h/a/c/w0/t;->F0:J

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/w0/t;->G0:Z

    :cond_1
    iget-wide v0, p1, Lf/h/a/c/y0/e;->f:J

    iget-wide v2, p0, Lf/h/a/c/w0/t;->I0:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/c/w0/t;->I0:J

    return-void
.end method

.method public e(Lf/h/a/c/j0;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->j:Z

    if-nez v1, :cond_0

    sget-object p1, Lf/h/a/c/j0;->e:Lf/h/a/c/j0;

    iput-object p1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->f()Lf/h/a/c/j0;

    move-result-object v1

    invoke-virtual {p1, v1}, Lf/h/a/c/j0;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object p1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o:Lf/h/a/c/j0;

    goto :goto_0

    :cond_1
    iput-object p1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    :cond_2
    :goto_0
    return-void
.end method

.method public f0(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;IIJZZLcom/google/android/exoplayer2/Format;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-boolean p1, p0, Lf/h/a/c/w0/t;->C0:Z

    if-eqz p1, :cond_0

    const-wide/16 p1, 0x0

    cmp-long p3, p9, p1

    if-nez p3, :cond_0

    and-int/lit8 p1, p8, 0x4

    if-eqz p1, :cond_0

    iget-wide p1, p0, Lf/h/a/c/w0/t;->I0:J

    const-wide p3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p12, p1, p3

    if-eqz p12, :cond_0

    move-wide p9, p1

    :cond_0
    iget-boolean p1, p0, Lf/h/a/c/w0/t;->A0:Z

    const/4 p2, 0x0

    const/4 p3, 0x1

    const/4 p4, 0x2

    if-eqz p1, :cond_1

    and-int/lit8 p1, p8, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p5, p7, p2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    return p3

    :cond_1
    if-eqz p11, :cond_3

    invoke-virtual {p5, p7, p2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t0:Lf/h/a/c/y0/d;

    iget p2, p1, Lf/h/a/c/y0/d;->f:I

    add-int/2addr p2, p3

    iput p2, p1, Lf/h/a/c/y0/d;->f:I

    iget-object p1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget p2, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    if-ne p2, p3, :cond_2

    iput p4, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    :cond_2
    return p3

    :cond_3
    :try_start_0
    iget-object p1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;
    :try_end_0
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$InitializationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$WriteException; {:try_start_0 .. :try_end_0} :catch_0

    check-cast p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    :try_start_1
    invoke-virtual {p1, p6, p9, p10}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h(Ljava/nio/ByteBuffer;J)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {p5, p7, p2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    iget-object p1, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t0:Lf/h/a/c/y0/d;

    iget p2, p1, Lf/h/a/c/y0/d;->e:I

    add-int/2addr p2, p3

    iput p2, p1, Lf/h/a/c/y0/d;->e:I
    :try_end_1
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$InitializationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$WriteException; {:try_start_1 .. :try_end_1} :catch_0

    return p3

    :cond_4
    return p2

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    :goto_0
    iget-object p2, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/t;->w(Ljava/lang/Exception;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    throw p1
.end method

.method public g()Z
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->o0:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->J:Z

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method public i0()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->J:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->J:Z
    :try_end_0
    .catch Lcom/google/android/exoplayer2/audio/AudioSink$WriteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/c/w0/t;->E0:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0, v0, v1}, Lf/h/a/c/t;->w(Ljava/lang/Exception;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v0

    throw v0
.end method

.method public o0(Lf/h/a/c/b1/f;Lf/h/a/c/z0/b;Lcom/google/android/exoplayer2/Format;)I
    .locals 6
    .param p2    # Lf/h/a/c/z0/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/c/b1/f;",
            "Lf/h/a/c/z0/b<",
            "Lf/h/a/c/z0/e;",
            ">;",
            "Lcom/google/android/exoplayer2/Format;",
            ")I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;
        }
    .end annotation

    iget-object v0, p3, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v0}, Lf/h/a/c/i1/o;->f(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    sget v1, Lf/h/a/c/i1/a0;->a:I

    const/16 v3, 0x15

    if-lt v1, v3, :cond_1

    const/16 v1, 0x20

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iget-object v3, p3, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    const-class v3, Lf/h/a/c/z0/e;

    iget-object v5, p3, Lcom/google/android/exoplayer2/Format;->F:Ljava/lang/Class;

    invoke-virtual {v3, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p3, Lcom/google/android/exoplayer2/Format;->F:Ljava/lang/Class;

    if-nez v3, :cond_2

    iget-object v3, p3, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    invoke-static {p2, v3}, Lf/h/a/c/t;->H(Lf/h/a/c/z0/b;Lcom/google/android/exoplayer2/drm/DrmInitData;)Z

    move-result p2

    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p2, 0x1

    :goto_2
    if-eqz p2, :cond_5

    iget v3, p3, Lcom/google/android/exoplayer2/Format;->y:I

    invoke-virtual {p0, v3, v0}, Lf/h/a/c/w0/t;->s0(ILjava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    :goto_3
    if-eqz v3, :cond_5

    invoke-interface {p1}, Lf/h/a/c/b1/f;->a()Lf/h/a/c/b1/e;

    move-result-object v3

    if-eqz v3, :cond_5

    or-int/lit8 p1, v1, 0xc

    return p1

    :cond_5
    const-string v3, "audio/raw"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    iget v3, p3, Lcom/google/android/exoplayer2/Format;->y:I

    iget v5, p3, Lcom/google/android/exoplayer2/Format;->A:I

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0, v3, v5}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p(II)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    iget v3, p3, Lcom/google/android/exoplayer2/Format;->y:I

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    const/4 v5, 0x2

    invoke-virtual {v0, v3, v5}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p(II)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    return v4

    :cond_8
    invoke-virtual {p0, p1, p3, v2}, Lf/h/a/c/w0/t;->U(Lf/h/a/c/b1/f;Lcom/google/android/exoplayer2/Format;Z)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    return v4

    :cond_9
    if-nez p2, :cond_a

    return v5

    :cond_a
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/c/b1/e;

    invoke-virtual {p1, p3}, Lf/h/a/c/b1/e;->d(Lcom/google/android/exoplayer2/Format;)Z

    move-result p2

    if-eqz p2, :cond_b

    invoke-virtual {p1, p3}, Lf/h/a/c/b1/e;->e(Lcom/google/android/exoplayer2/Format;)Z

    move-result p1

    if-eqz p1, :cond_b

    const/16 p1, 0x10

    goto :goto_4

    :cond_b
    const/16 p1, 0x8

    :goto_4
    if-eqz p2, :cond_c

    const/4 p2, 0x4

    goto :goto_5

    :cond_c
    const/4 p2, 0x3

    :goto_5
    or-int/2addr p1, p2

    or-int/2addr p1, v1

    return p1
.end method

.method public final r0(Lf/h/a/c/b1/e;Lcom/google/android/exoplayer2/Format;)I
    .locals 1

    iget-object p1, p1, Lf/h/a/c/b1/e;->a:Ljava/lang/String;

    const-string v0, "OMX.google.raw.decoder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lf/h/a/c/i1/a0;->a:I

    const/16 v0, 0x18

    if-ge p1, v0, :cond_1

    const/16 v0, 0x17

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lf/h/a/c/w0/t;->v0:Landroid/content/Context;

    invoke-static {p1}, Lf/h/a/c/i1/a0;->t(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    const/4 p1, -0x1

    return p1

    :cond_1
    iget p1, p2, Lcom/google/android/exoplayer2/Format;->m:I

    return p1
.end method

.method public s0(ILjava/lang/String;)I
    .locals 3

    const-string v0, "audio/eac3-joc"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object p2, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    const/4 v1, -0x1

    const/16 v2, 0x12

    check-cast p2, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {p2, v1, v2}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p(II)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {v0}, Lf/h/a/c/i1/o;->a(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_0
    const-string p2, "audio/eac3"

    :cond_1
    invoke-static {p2}, Lf/h/a/c/i1/o;->a(Ljava/lang/String;)I

    move-result p2

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p(II)Z

    move-result p1

    if-eqz p1, :cond_2

    return p2

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public t()Lf/h/a/c/i1/n;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    return-object p0
.end method

.method public final t0()V
    .locals 31

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/w0/t;->g()Z

    move-result v2

    check-cast v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v3

    if-eqz v3, :cond_28

    iget v3, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    if-nez v3, :cond_0

    goto/16 :goto_15

    :cond_0
    iget-object v3, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    iget-object v7, v3, Lf/h/a/c/w0/n;->c:Landroid/media/AudioTrack;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v7}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v7

    const/4 v8, 0x3

    const/4 v9, 0x2

    const/4 v15, 0x1

    const-wide/16 v16, 0x3e8

    if-ne v7, v8, :cond_19

    invoke-virtual {v3}, Lf/h/a/c/w0/n;->b()J

    move-result-wide v11

    invoke-virtual {v3, v11, v12}, Lf/h/a/c/w0/n;->a(J)J

    move-result-wide v27

    const-wide/16 v11, 0x0

    cmp-long v7, v27, v11

    if-nez v7, :cond_1

    goto/16 :goto_9

    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v20

    div-long v13, v20, v16

    iget-wide v4, v3, Lf/h/a/c/w0/n;->k:J

    sub-long v4, v13, v4

    const-wide/16 v20, 0x7530

    cmp-long v7, v4, v20

    if-ltz v7, :cond_3

    iget-object v4, v3, Lf/h/a/c/w0/n;->b:[J

    iget v5, v3, Lf/h/a/c/w0/n;->t:I

    sub-long v20, v27, v13

    aput-wide v20, v4, v5

    add-int/2addr v5, v15

    const/16 v4, 0xa

    rem-int/2addr v5, v4

    iput v5, v3, Lf/h/a/c/w0/n;->t:I

    iget v5, v3, Lf/h/a/c/w0/n;->u:I

    if-ge v5, v4, :cond_2

    add-int/2addr v5, v15

    iput v5, v3, Lf/h/a/c/w0/n;->u:I

    :cond_2
    iput-wide v13, v3, Lf/h/a/c/w0/n;->k:J

    iput-wide v11, v3, Lf/h/a/c/w0/n;->j:J

    const/4 v4, 0x0

    :goto_0
    iget v5, v3, Lf/h/a/c/w0/n;->u:I

    if-ge v4, v5, :cond_3

    iget-wide v10, v3, Lf/h/a/c/w0/n;->j:J

    iget-object v12, v3, Lf/h/a/c/w0/n;->b:[J

    aget-wide v20, v12, v4

    int-to-long v6, v5

    div-long v20, v20, v6

    add-long v5, v20, v10

    iput-wide v5, v3, Lf/h/a/c/w0/n;->j:J

    add-int/lit8 v4, v4, 0x1

    const-wide/16 v11, 0x0

    goto :goto_0

    :cond_3
    iget-boolean v4, v3, Lf/h/a/c/w0/n;->h:Z

    if-eqz v4, :cond_4

    goto/16 :goto_9

    :cond_4
    iget-object v4, v3, Lf/h/a/c/w0/n;->f:Lf/h/a/c/w0/m;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, v4, Lf/h/a/c/w0/m;->a:Lf/h/a/c/w0/m$a;

    if-eqz v5, :cond_10

    iget-wide v10, v4, Lf/h/a/c/w0/m;->e:J

    sub-long v10, v13, v10

    iget-wide v6, v4, Lf/h/a/c/w0/m;->d:J

    cmp-long v20, v10, v6

    if-gez v20, :cond_5

    goto/16 :goto_3

    :cond_5
    iput-wide v13, v4, Lf/h/a/c/w0/m;->e:J

    iget-object v6, v5, Lf/h/a/c/w0/m$a;->a:Landroid/media/AudioTrack;

    iget-object v7, v5, Lf/h/a/c/w0/m$a;->b:Landroid/media/AudioTimestamp;

    invoke-virtual {v6, v7}, Landroid/media/AudioTrack;->getTimestamp(Landroid/media/AudioTimestamp;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v7, v5, Lf/h/a/c/w0/m$a;->b:Landroid/media/AudioTimestamp;

    iget-wide v10, v7, Landroid/media/AudioTimestamp;->framePosition:J

    move-wide/from16 v29, v13

    iget-wide v12, v5, Lf/h/a/c/w0/m$a;->d:J

    cmp-long v14, v12, v10

    if-lez v14, :cond_6

    iget-wide v12, v5, Lf/h/a/c/w0/m$a;->c:J

    const-wide/16 v20, 0x1

    add-long v12, v12, v20

    iput-wide v12, v5, Lf/h/a/c/w0/m$a;->c:J

    :cond_6
    iput-wide v10, v5, Lf/h/a/c/w0/m$a;->d:J

    iget-wide v12, v5, Lf/h/a/c/w0/m$a;->c:J

    const/16 v14, 0x20

    shl-long/2addr v12, v14

    add-long/2addr v10, v12

    iput-wide v10, v5, Lf/h/a/c/w0/m$a;->e:J

    goto :goto_1

    :cond_7
    move-wide/from16 v29, v13

    :goto_1
    iget v5, v4, Lf/h/a/c/w0/m;->b:I

    if-eqz v5, :cond_d

    if-eq v5, v15, :cond_b

    if-eq v5, v9, :cond_a

    if-eq v5, v8, :cond_9

    const/4 v8, 0x4

    if-ne v5, v8, :cond_8

    goto :goto_2

    :cond_8
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_9
    if-eqz v6, :cond_f

    invoke-virtual {v4}, Lf/h/a/c/w0/m;->a()V

    goto :goto_2

    :cond_a
    if-nez v6, :cond_f

    invoke-virtual {v4}, Lf/h/a/c/w0/m;->a()V

    goto :goto_2

    :cond_b
    if-eqz v6, :cond_c

    iget-object v5, v4, Lf/h/a/c/w0/m;->a:Lf/h/a/c/w0/m$a;

    iget-wide v10, v5, Lf/h/a/c/w0/m$a;->e:J

    iget-wide v12, v4, Lf/h/a/c/w0/m;->f:J

    cmp-long v5, v10, v12

    if-lez v5, :cond_f

    invoke-virtual {v4, v9}, Lf/h/a/c/w0/m;->b(I)V

    goto :goto_2

    :cond_c
    invoke-virtual {v4}, Lf/h/a/c/w0/m;->a()V

    goto :goto_2

    :cond_d
    if-eqz v6, :cond_e

    iget-object v5, v4, Lf/h/a/c/w0/m;->a:Lf/h/a/c/w0/m$a;

    iget-object v8, v5, Lf/h/a/c/w0/m$a;->b:Landroid/media/AudioTimestamp;

    iget-wide v10, v8, Landroid/media/AudioTimestamp;->nanoTime:J

    div-long v10, v10, v16

    iget-wide v12, v4, Lf/h/a/c/w0/m;->c:J

    cmp-long v8, v10, v12

    if-ltz v8, :cond_11

    iget-wide v10, v5, Lf/h/a/c/w0/m$a;->e:J

    iput-wide v10, v4, Lf/h/a/c/w0/m;->f:J

    invoke-virtual {v4, v15}, Lf/h/a/c/w0/m;->b(I)V

    goto :goto_2

    :cond_e
    iget-wide v10, v4, Lf/h/a/c/w0/m;->c:J

    sub-long v13, v29, v10

    const-wide/32 v10, 0x7a120

    cmp-long v5, v13, v10

    if-lez v5, :cond_f

    invoke-virtual {v4, v8}, Lf/h/a/c/w0/m;->b(I)V

    :cond_f
    :goto_2
    move v12, v6

    goto :goto_4

    :cond_10
    :goto_3
    move-wide/from16 v29, v13

    :cond_11
    const/4 v12, 0x0

    :goto_4
    const-wide/32 v5, 0x4c4b40

    if-nez v12, :cond_12

    goto :goto_7

    :cond_12
    iget-object v8, v4, Lf/h/a/c/w0/m;->a:Lf/h/a/c/w0/m$a;

    if-eqz v8, :cond_13

    iget-object v10, v8, Lf/h/a/c/w0/m$a;->b:Landroid/media/AudioTimestamp;

    iget-wide v10, v10, Landroid/media/AudioTimestamp;->nanoTime:J

    div-long v10, v10, v16

    move-wide/from16 v23, v10

    goto :goto_5

    :cond_13
    const-wide v23, -0x7fffffffffffffffL    # -4.9E-324

    :goto_5
    if-eqz v8, :cond_14

    iget-wide v10, v8, Lf/h/a/c/w0/m$a;->e:J

    goto :goto_6

    :cond_14
    const-wide/16 v10, -0x1

    :goto_6
    sub-long v12, v23, v29

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    cmp-long v8, v12, v5

    if-lez v8, :cond_15

    iget-object v8, v3, Lf/h/a/c/w0/n;->a:Lf/h/a/c/w0/n$a;

    move-object/from16 v20, v8

    move-wide/from16 v21, v10

    move-wide/from16 v25, v29

    invoke-interface/range {v20 .. v28}, Lf/h/a/c/w0/n$a;->d(JJJJ)V

    const/4 v8, 0x4

    invoke-virtual {v4, v8}, Lf/h/a/c/w0/m;->b(I)V

    goto :goto_7

    :cond_15
    invoke-virtual {v3, v10, v11}, Lf/h/a/c/w0/n;->a(J)J

    move-result-wide v12

    sub-long v12, v12, v27

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    cmp-long v8, v12, v5

    if-lez v8, :cond_16

    iget-object v8, v3, Lf/h/a/c/w0/n;->a:Lf/h/a/c/w0/n$a;

    move-object/from16 v20, v8

    move-wide/from16 v21, v10

    move-wide/from16 v25, v29

    invoke-interface/range {v20 .. v28}, Lf/h/a/c/w0/n$a;->c(JJJJ)V

    const/4 v8, 0x4

    invoke-virtual {v4, v8}, Lf/h/a/c/w0/m;->b(I)V

    goto :goto_7

    :cond_16
    const/4 v8, 0x4

    iget v10, v4, Lf/h/a/c/w0/m;->b:I

    if-ne v10, v8, :cond_17

    invoke-virtual {v4}, Lf/h/a/c/w0/m;->a()V

    :cond_17
    :goto_7
    iget-boolean v4, v3, Lf/h/a/c/w0/n;->o:Z

    if-eqz v4, :cond_19

    iget-object v4, v3, Lf/h/a/c/w0/n;->l:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_19

    iget-wide v10, v3, Lf/h/a/c/w0/n;->p:J

    sub-long v13, v29, v10

    const-wide/32 v10, 0x7a120

    cmp-long v8, v13, v10

    if-ltz v8, :cond_19

    :try_start_0
    iget-object v8, v3, Lf/h/a/c/w0/n;->c:Landroid/media/AudioTrack;

    invoke-static {v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v7, 0x0

    new-array v10, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v8, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    sget v7, Lf/h/a/c/i1/a0;->a:I

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v7, v4

    mul-long v7, v7, v16

    iget-wide v10, v3, Lf/h/a/c/w0/n;->i:J

    sub-long/2addr v7, v10

    iput-wide v7, v3, Lf/h/a/c/w0/n;->m:J

    const-wide/16 v10, 0x0

    invoke-static {v7, v8, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    iput-wide v7, v3, Lf/h/a/c/w0/n;->m:J

    cmp-long v4, v7, v5

    if-lez v4, :cond_18

    iget-object v4, v3, Lf/h/a/c/w0/n;->a:Lf/h/a/c/w0/n$a;

    invoke-interface {v4, v7, v8}, Lf/h/a/c/w0/n$a;->b(J)V

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lf/h/a/c/w0/n;->m:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_18
    move-wide/from16 v5, v29

    const/4 v4, 0x0

    goto :goto_8

    :catch_0
    const/4 v4, 0x0

    iput-object v4, v3, Lf/h/a/c/w0/n;->l:Ljava/lang/reflect/Method;

    move-wide/from16 v5, v29

    :goto_8
    iput-wide v5, v3, Lf/h/a/c/w0/n;->p:J

    goto :goto_a

    :cond_19
    :goto_9
    const/4 v4, 0x0

    :goto_a
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v5

    div-long v5, v5, v16

    iget-object v7, v3, Lf/h/a/c/w0/n;->f:Lf/h/a/c/w0/m;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget v8, v7, Lf/h/a/c/w0/m;->b:I

    if-eq v8, v15, :cond_1b

    if-ne v8, v9, :cond_1a

    goto :goto_b

    :cond_1a
    const/4 v8, 0x0

    goto :goto_c

    :cond_1b
    :goto_b
    const/4 v8, 0x1

    :goto_c
    if-eqz v8, :cond_20

    iget-object v2, v7, Lf/h/a/c/w0/m;->a:Lf/h/a/c/w0/m$a;

    if-eqz v2, :cond_1c

    iget-wide v13, v2, Lf/h/a/c/w0/m$a;->e:J

    goto :goto_d

    :cond_1c
    const-wide/16 v13, -0x1

    :goto_d
    invoke-virtual {v3, v13, v14}, Lf/h/a/c/w0/n;->a(J)J

    move-result-wide v2

    iget v8, v7, Lf/h/a/c/w0/m;->b:I

    if-ne v8, v9, :cond_1d

    goto :goto_e

    :cond_1d
    const/4 v15, 0x0

    :goto_e
    if-nez v15, :cond_1e

    goto :goto_11

    :cond_1e
    iget-object v7, v7, Lf/h/a/c/w0/m;->a:Lf/h/a/c/w0/m$a;

    if-eqz v7, :cond_1f

    iget-object v7, v7, Lf/h/a/c/w0/m$a;->b:Landroid/media/AudioTimestamp;

    iget-wide v7, v7, Landroid/media/AudioTimestamp;->nanoTime:J

    div-long v7, v7, v16

    move-wide/from16 v18, v7

    goto :goto_f

    :cond_1f
    const-wide v18, -0x7fffffffffffffffL    # -4.9E-324

    :goto_f
    sub-long v5, v5, v18

    add-long/2addr v2, v5

    goto :goto_11

    :cond_20
    iget v7, v3, Lf/h/a/c/w0/n;->u:I

    if-nez v7, :cond_21

    invoke-virtual {v3}, Lf/h/a/c/w0/n;->b()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lf/h/a/c/w0/n;->a(J)J

    move-result-wide v5

    goto :goto_10

    :cond_21
    iget-wide v7, v3, Lf/h/a/c/w0/n;->j:J

    add-long/2addr v5, v7

    :goto_10
    if-nez v2, :cond_22

    iget-wide v2, v3, Lf/h/a/c/w0/n;->m:J

    sub-long/2addr v5, v2

    :cond_22
    move-wide v2, v5

    :goto_11
    iget-object v5, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a(J)J

    move-result-wide v5

    invoke-static {v2, v3, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iget-wide v5, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->A:J

    move-object v10, v4

    :goto_12
    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_23

    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;

    iget-wide v7, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;->c:J

    cmp-long v4, v2, v7

    if-ltz v4, :cond_23

    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;

    goto :goto_12

    :cond_23
    if-eqz v10, :cond_24

    iget-object v4, v10, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;->a:Lf/h/a/c/j0;

    iput-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    iget-wide v7, v10, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;->c:J

    iput-wide v7, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->r:J

    iget-wide v7, v10, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;->b:J

    iget-wide v9, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->A:J

    sub-long/2addr v7, v9

    iput-wide v7, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q:J

    :cond_24
    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    iget v4, v4, Lf/h/a/c/j0;->a:F

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v7

    if-nez v4, :cond_25

    iget-wide v7, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q:J

    add-long/2addr v2, v7

    iget-wide v7, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->r:J

    sub-long/2addr v2, v7

    goto :goto_14

    :cond_25
    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_26

    iget-wide v7, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q:J

    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->b:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;

    iget-wide v9, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->r:J

    sub-long/2addr v2, v9

    invoke-interface {v4, v2, v3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;->b(J)J

    move-result-wide v2

    add-long/2addr v2, v7

    goto :goto_14

    :cond_26
    iget-wide v8, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q:J

    iget-wide v10, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->r:J

    sub-long/2addr v2, v10

    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    iget v4, v4, Lf/h/a/c/j0;->a:F

    sget v10, Lf/h/a/c/i1/a0;->a:I

    cmpl-float v7, v4, v7

    if-nez v7, :cond_27

    goto :goto_13

    :cond_27
    long-to-double v2, v2

    float-to-double v10, v4

    mul-double v2, v2, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    :goto_13
    add-long/2addr v2, v8

    :goto_14
    iget-object v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-object v1, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->b:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;->c()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a(J)J

    move-result-wide v7

    add-long/2addr v7, v2

    add-long/2addr v7, v5

    goto :goto_16

    :cond_28
    :goto_15
    const-wide/high16 v7, -0x8000000000000000L

    :goto_16
    const-wide/high16 v1, -0x8000000000000000L

    cmp-long v3, v7, v1

    if-eqz v3, :cond_2a

    iget-boolean v1, v0, Lf/h/a/c/w0/t;->H0:Z

    if-eqz v1, :cond_29

    goto :goto_17

    :cond_29
    iget-wide v1, v0, Lf/h/a/c/w0/t;->F0:J

    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    :goto_17
    iput-wide v7, v0, Lf/h/a/c/w0/t;->F0:J

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/h/a/c/w0/t;->H0:Z

    :cond_2a
    return-void
.end method

.method public y()V
    .locals 3

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    :try_start_0
    iput-wide v0, p0, Lf/h/a/c/w0/t;->I0:J

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/w0/t;->J0:I

    iget-object v0, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->y()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t0:Lf/h/a/c/y0/d;

    invoke-virtual {v0, v1}, Lf/h/a/c/w0/l$a;->a(Lf/h/a/c/y0/d;)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v2, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t0:Lf/h/a/c/y0/d;

    invoke-virtual {v1, v2}, Lf/h/a/c/w0/l$a;->a(Lf/h/a/c/y0/d;)V

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->y()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    iget-object v1, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v2, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t0:Lf/h/a/c/y0/d;

    invoke-virtual {v1, v2}, Lf/h/a/c/w0/l$a;->a(Lf/h/a/c/y0/d;)V

    throw v0

    :catchall_2
    move-exception v0

    iget-object v1, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v2, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t0:Lf/h/a/c/y0/d;

    invoke-virtual {v1, v2}, Lf/h/a/c/w0/l$a;->a(Lf/h/a/c/y0/d;)V

    throw v0
.end method

.method public z(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->z(Z)V

    iget-object p1, p0, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t0:Lf/h/a/c/y0/d;

    iget-object v1, p1, Lf/h/a/c/w0/l$a;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    new-instance v2, Lf/h/a/c/w0/e;

    invoke-direct {v2, p1, v0}, Lf/h/a/c/w0/e;-><init>(Lf/h/a/c/w0/l$a;Lf/h/a/c/y0/d;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object p1, p0, Lf/h/a/c/t;->f:Lf/h/a/c/q0;

    iget p1, p1, Lf/h/a/c/q0;->a:I

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    iget-object v1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget v2, Lf/h/a/c/i1/a0;->a:I

    const/16 v3, 0x15

    const/4 v4, 0x1

    if-lt v2, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iget-boolean v0, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->O:Z

    if-eqz v0, :cond_2

    iget v0, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    if-eq v0, p1, :cond_4

    :cond_2
    iput-boolean v4, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->O:Z

    iput p1, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lf/h/a/c/w0/t;->x0:Lcom/google/android/exoplayer2/audio/AudioSink;

    check-cast p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    iget-boolean v1, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->O:Z

    if-eqz v1, :cond_4

    iput-boolean v0, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->O:Z

    iput v0, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V

    :cond_4
    :goto_0
    return-void
.end method
