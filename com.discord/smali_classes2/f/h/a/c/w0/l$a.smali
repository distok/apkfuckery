.class public final Lf/h/a/c/w0/l$a;
.super Ljava/lang/Object;
.source "AudioRendererEventListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/w0/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/os/Handler;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lf/h/a/c/w0/l;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lf/h/a/c/w0/l;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lf/h/a/c/w0/l;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_0

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lf/h/a/c/w0/l$a;->a:Landroid/os/Handler;

    iput-object p2, p0, Lf/h/a/c/w0/l$a;->b:Lf/h/a/c/w0/l;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/y0/d;)V
    .locals 2

    monitor-enter p1

    monitor-exit p1

    iget-object v0, p0, Lf/h/a/c/w0/l$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf/h/a/c/w0/f;

    invoke-direct {v1, p0, p1}, Lf/h/a/c/w0/f;-><init>(Lf/h/a/c/w0/l$a;Lf/h/a/c/y0/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
