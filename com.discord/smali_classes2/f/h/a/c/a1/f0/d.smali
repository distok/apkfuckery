.class public final Lf/h/a/c/a1/f0/d;
.super Ljava/lang/Object;
.source "WavSeekMap.java"

# interfaces
.implements Lf/h/a/c/a1/q;


# instance fields
.field public final a:Lf/h/a/c/a1/f0/b;

.field public final b:I

.field public final c:J

.field public final d:J

.field public final e:J


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/f0/b;IJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/f0/d;->a:Lf/h/a/c/a1/f0/b;

    iput p2, p0, Lf/h/a/c/a1/f0/d;->b:I

    iput-wide p3, p0, Lf/h/a/c/a1/f0/d;->c:J

    sub-long/2addr p5, p3

    iget p1, p1, Lf/h/a/c/a1/f0/b;->d:I

    int-to-long p1, p1

    div-long/2addr p5, p1

    iput-wide p5, p0, Lf/h/a/c/a1/f0/d;->d:J

    invoke-virtual {p0, p5, p6}, Lf/h/a/c/a1/f0/d;->d(J)J

    move-result-wide p1

    iput-wide p1, p0, Lf/h/a/c/a1/f0/d;->e:J

    return-void
.end method


# virtual methods
.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final d(J)J
    .locals 8

    iget v0, p0, Lf/h/a/c/a1/f0/d;->b:I

    int-to-long v0, v0

    mul-long v2, p1, v0

    iget-object p1, p0, Lf/h/a/c/a1/f0/d;->a:Lf/h/a/c/a1/f0/b;

    iget p1, p1, Lf/h/a/c/a1/f0/b;->c:I

    int-to-long v6, p1

    const-wide/32 v4, 0xf4240

    invoke-static/range {v2 .. v7}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public g(J)Lf/h/a/c/a1/q$a;
    .locals 10

    iget-object v0, p0, Lf/h/a/c/a1/f0/d;->a:Lf/h/a/c/a1/f0/b;

    iget v0, v0, Lf/h/a/c/a1/f0/b;->c:I

    int-to-long v0, v0

    mul-long v0, v0, p1

    iget v2, p0, Lf/h/a/c/a1/f0/d;->b:I

    int-to-long v2, v2

    const-wide/32 v4, 0xf4240

    mul-long v2, v2, v4

    div-long v4, v0, v2

    iget-wide v0, p0, Lf/h/a/c/a1/f0/d;->d:J

    const-wide/16 v2, 0x1

    sub-long v8, v0, v2

    const-wide/16 v6, 0x0

    invoke-static/range {v4 .. v9}, Lf/h/a/c/i1/a0;->g(JJJ)J

    move-result-wide v0

    iget-wide v4, p0, Lf/h/a/c/a1/f0/d;->c:J

    iget-object v6, p0, Lf/h/a/c/a1/f0/d;->a:Lf/h/a/c/a1/f0/b;

    iget v6, v6, Lf/h/a/c/a1/f0/b;->d:I

    int-to-long v6, v6

    mul-long v6, v6, v0

    add-long/2addr v6, v4

    invoke-virtual {p0, v0, v1}, Lf/h/a/c/a1/f0/d;->d(J)J

    move-result-wide v4

    new-instance v8, Lf/h/a/c/a1/r;

    invoke-direct {v8, v4, v5, v6, v7}, Lf/h/a/c/a1/r;-><init>(JJ)V

    cmp-long v6, v4, p1

    if-gez v6, :cond_1

    iget-wide p1, p0, Lf/h/a/c/a1/f0/d;->d:J

    sub-long/2addr p1, v2

    cmp-long v4, v0, p1

    if-nez v4, :cond_0

    goto :goto_0

    :cond_0
    add-long/2addr v0, v2

    iget-wide p1, p0, Lf/h/a/c/a1/f0/d;->c:J

    iget-object v2, p0, Lf/h/a/c/a1/f0/d;->a:Lf/h/a/c/a1/f0/b;

    iget v2, v2, Lf/h/a/c/a1/f0/b;->d:I

    int-to-long v2, v2

    mul-long v2, v2, v0

    add-long/2addr v2, p1

    invoke-virtual {p0, v0, v1}, Lf/h/a/c/a1/f0/d;->d(J)J

    move-result-wide p1

    new-instance v0, Lf/h/a/c/a1/r;

    invoke-direct {v0, p1, p2, v2, v3}, Lf/h/a/c/a1/r;-><init>(JJ)V

    new-instance p1, Lf/h/a/c/a1/q$a;

    invoke-direct {p1, v8, v0}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;Lf/h/a/c/a1/r;)V

    return-object p1

    :cond_1
    :goto_0
    new-instance p1, Lf/h/a/c/a1/q$a;

    invoke-direct {p1, v8}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;)V

    return-object p1
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, Lf/h/a/c/a1/f0/d;->e:J

    return-wide v0
.end method
