.class public final Lf/h/a/c/a1/f0/a$c;
.super Ljava/lang/Object;
.source "WavExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/f0/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/f0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/i;

.field public final b:Lf/h/a/c/a1/s;

.field public final c:Lf/h/a/c/a1/f0/b;

.field public final d:Lcom/google/android/exoplayer2/Format;

.field public final e:I

.field public f:J

.field public g:I

.field public h:J


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/i;Lf/h/a/c/a1/s;Lf/h/a/c/a1/f0/b;Ljava/lang/String;I)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v2, p1

    iput-object v2, v0, Lf/h/a/c/a1/f0/a$c;->a:Lf/h/a/c/a1/i;

    move-object/from16 v2, p2

    iput-object v2, v0, Lf/h/a/c/a1/f0/a$c;->b:Lf/h/a/c/a1/s;

    iput-object v1, v0, Lf/h/a/c/a1/f0/a$c;->c:Lf/h/a/c/a1/f0/b;

    iget v2, v1, Lf/h/a/c/a1/f0/b;->b:I

    iget v3, v1, Lf/h/a/c/a1/f0/b;->e:I

    mul-int v2, v2, v3

    div-int/lit8 v2, v2, 0x8

    iget v3, v1, Lf/h/a/c/a1/f0/b;->d:I

    if-ne v3, v2, :cond_0

    iget v3, v1, Lf/h/a/c/a1/f0/b;->c:I

    mul-int v3, v3, v2

    div-int/lit8 v3, v3, 0xa

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v8

    iput v8, v0, Lf/h/a/c/a1/f0/a$c;->e:I

    const/4 v4, 0x0

    const/4 v6, 0x0

    iget v10, v1, Lf/h/a/c/a1/f0/b;->c:I

    mul-int v2, v2, v10

    mul-int/lit8 v7, v2, 0x8

    iget v9, v1, Lf/h/a/c/a1/f0/b;->b:I

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v5, p4

    move/from16 v11, p5

    invoke-static/range {v4 .. v15}, Lcom/google/android/exoplayer2/Format;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    iput-object v1, v0, Lf/h/a/c/a1/f0/a$c;->d:Lcom/google/android/exoplayer2/Format;

    return-void

    :cond_0
    new-instance v3, Lcom/google/android/exoplayer2/ParserException;

    const-string v4, "Expected block size: "

    const-string v5, "; got: "

    invoke-static {v4, v2, v5}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lf/h/a/c/a1/f0/b;->d:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;J)Z
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    cmp-long v3, p2, v1

    if-nez v3, :cond_0

    move-object/from16 v5, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    move-object/from16 v5, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    const/4 v4, 0x0

    :goto_0
    if-nez v4, :cond_2

    iget v6, v5, Lf/h/a/c/a1/f0/a$c;->g:I

    iget v7, v5, Lf/h/a/c/a1/f0/a$c;->e:I

    if-ge v6, v7, :cond_2

    sub-int/2addr v7, v6

    int-to-long v6, v7

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v7, v6

    iget-object v6, v5, Lf/h/a/c/a1/f0/a$c;->b:Lf/h/a/c/a1/s;

    invoke-interface {v6, v1, v7, v0}, Lf/h/a/c/a1/s;->a(Lf/h/a/c/a1/e;IZ)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    :goto_1
    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    iget v7, v5, Lf/h/a/c/a1/f0/a$c;->g:I

    add-int/2addr v7, v6

    iput v7, v5, Lf/h/a/c/a1/f0/a$c;->g:I

    goto :goto_0

    :cond_2
    iget-object v0, v5, Lf/h/a/c/a1/f0/a$c;->c:Lf/h/a/c/a1/f0/b;

    iget v1, v0, Lf/h/a/c/a1/f0/b;->d:I

    iget v2, v5, Lf/h/a/c/a1/f0/a$c;->g:I

    div-int/2addr v2, v1

    if-lez v2, :cond_3

    iget-wide v6, v5, Lf/h/a/c/a1/f0/a$c;->f:J

    iget-wide v8, v5, Lf/h/a/c/a1/f0/a$c;->h:J

    const-wide/32 v10, 0xf4240

    iget v0, v0, Lf/h/a/c/a1/f0/b;->c:I

    int-to-long v12, v0

    invoke-static/range {v8 .. v13}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v8

    add-long v11, v6, v8

    mul-int v14, v2, v1

    iget v0, v5, Lf/h/a/c/a1/f0/a$c;->g:I

    sub-int/2addr v0, v14

    iget-object v10, v5, Lf/h/a/c/a1/f0/a$c;->b:Lf/h/a/c/a1/s;

    const/4 v13, 0x1

    const/16 v16, 0x0

    move v15, v0

    invoke-interface/range {v10 .. v16}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iget-wide v6, v5, Lf/h/a/c/a1/f0/a$c;->h:J

    int-to-long v1, v2

    add-long/2addr v6, v1

    iput-wide v6, v5, Lf/h/a/c/a1/f0/a$c;->h:J

    iput v0, v5, Lf/h/a/c/a1/f0/a$c;->g:I

    :cond_3
    return v4
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/a1/f0/a$c;->f:J

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/a1/f0/a$c;->g:I

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lf/h/a/c/a1/f0/a$c;->h:J

    return-void
.end method

.method public c(IJ)V
    .locals 9

    iget-object v0, p0, Lf/h/a/c/a1/f0/a$c;->a:Lf/h/a/c/a1/i;

    new-instance v8, Lf/h/a/c/a1/f0/d;

    iget-object v2, p0, Lf/h/a/c/a1/f0/a$c;->c:Lf/h/a/c/a1/f0/b;

    int-to-long v4, p1

    const/4 v3, 0x1

    move-object v1, v8

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Lf/h/a/c/a1/f0/d;-><init>(Lf/h/a/c/a1/f0/b;IJJ)V

    invoke-interface {v0, v8}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    iget-object p1, p0, Lf/h/a/c/a1/f0/a$c;->b:Lf/h/a/c/a1/s;

    iget-object p2, p0, Lf/h/a/c/a1/f0/a$c;->d:Lcom/google/android/exoplayer2/Format;

    invoke-interface {p1, p2}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    return-void
.end method
