.class public final Lf/h/a/c/a1/f0/c;
.super Ljava/lang/Object;
.source "WavHeaderReader.java"


# instance fields
.field public final a:I

.field public final b:J


# direct methods
.method public constructor <init>(IJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/h/a/c/a1/f0/c;->a:I

    iput-wide p2, p0, Lf/h/a/c/a1/f0/c;->b:J

    return-void
.end method

.method public static a(Lf/h/a/c/a1/e;Lf/h/a/c/i1/r;)Lf/h/a/c/a1/f0/c;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p1, Lf/h/a/c/i1/r;->a:[B

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-virtual {p0, v0, v1, v2, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    invoke-virtual {p1, v1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->e()I

    move-result p0

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->h()J

    move-result-wide v0

    new-instance p1, Lf/h/a/c/a1/f0/c;

    invoke-direct {p1, p0, v0, v1}, Lf/h/a/c/a1/f0/c;-><init>(IJ)V

    return-object p1
.end method
