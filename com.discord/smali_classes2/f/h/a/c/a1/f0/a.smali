.class public final Lf/h/a/c/a1/f0/a;
.super Ljava/lang/Object;
.source "WavExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/f0/a$a;,
        Lf/h/a/c/a1/f0/a$c;,
        Lf/h/a/c/a1/f0/a$b;
    }
.end annotation


# instance fields
.field public a:Lf/h/a/c/a1/i;

.field public b:Lf/h/a/c/a1/s;

.field public c:Lf/h/a/c/a1/f0/a$b;

.field public d:I

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lf/h/a/c/a1/f0/a;->d:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/h/a/c/a1/f0/a;->e:J

    return-void
.end method


# virtual methods
.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object p2, p0, Lf/h/a/c/a1/f0/a;->b:Lf/h/a/c/a1/s;

    invoke-static {p2}, Lf/g/j/k/a;->w(Ljava/lang/Object;)Ljava/lang/Object;

    sget p2, Lf/h/a/c/i1/a0;->a:I

    iget-object p2, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_8

    invoke-static {p1}, Lf/g/j/k/a;->x0(Lf/h/a/c/a1/e;)Lf/h/a/c/a1/f0/b;

    move-result-object v5

    if-eqz v5, :cond_7

    iget p2, v5, Lf/h/a/c/a1/f0/b;->a:I

    const/16 v2, 0x11

    if-ne p2, v2, :cond_0

    new-instance p2, Lf/h/a/c/a1/f0/a$a;

    iget-object v2, p0, Lf/h/a/c/a1/f0/a;->a:Lf/h/a/c/a1/i;

    iget-object v3, p0, Lf/h/a/c/a1/f0/a;->b:Lf/h/a/c/a1/s;

    invoke-direct {p2, v2, v3, v5}, Lf/h/a/c/a1/f0/a$a;-><init>(Lf/h/a/c/a1/i;Lf/h/a/c/a1/s;Lf/h/a/c/a1/f0/b;)V

    iput-object p2, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    goto/16 :goto_2

    :cond_0
    const/4 v2, 0x6

    if-ne p2, v2, :cond_1

    new-instance p2, Lf/h/a/c/a1/f0/a$c;

    iget-object v3, p0, Lf/h/a/c/a1/f0/a;->a:Lf/h/a/c/a1/i;

    iget-object v4, p0, Lf/h/a/c/a1/f0/a;->b:Lf/h/a/c/a1/s;

    const/4 v7, -0x1

    const-string v6, "audio/g711-alaw"

    move-object v2, p2

    invoke-direct/range {v2 .. v7}, Lf/h/a/c/a1/f0/a$c;-><init>(Lf/h/a/c/a1/i;Lf/h/a/c/a1/s;Lf/h/a/c/a1/f0/b;Ljava/lang/String;I)V

    iput-object p2, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    goto :goto_2

    :cond_1
    const/4 v2, 0x7

    if-ne p2, v2, :cond_2

    new-instance p2, Lf/h/a/c/a1/f0/a$c;

    iget-object v3, p0, Lf/h/a/c/a1/f0/a;->a:Lf/h/a/c/a1/i;

    iget-object v4, p0, Lf/h/a/c/a1/f0/a;->b:Lf/h/a/c/a1/s;

    const/4 v7, -0x1

    const-string v6, "audio/g711-mlaw"

    move-object v2, p2

    invoke-direct/range {v2 .. v7}, Lf/h/a/c/a1/f0/a$c;-><init>(Lf/h/a/c/a1/i;Lf/h/a/c/a1/s;Lf/h/a/c/a1/f0/b;Ljava/lang/String;I)V

    iput-object p2, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    goto :goto_2

    :cond_2
    iget v2, v5, Lf/h/a/c/a1/f0/b;->e:I

    if-eq p2, v0, :cond_5

    const/4 v3, 0x3

    if-eq p2, v3, :cond_3

    const v3, 0xfffe

    if-eq p2, v3, :cond_5

    const/4 p2, 0x0

    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    const/16 p2, 0x20

    if-ne v2, p2, :cond_4

    const/4 p2, 0x4

    goto :goto_0

    :cond_4
    const/4 p2, 0x0

    goto :goto_0

    :cond_5
    invoke-static {v2}, Lf/h/a/c/i1/a0;->k(I)I

    move-result p2

    :goto_0
    move v7, p2

    :goto_1
    if-eqz v7, :cond_6

    new-instance p2, Lf/h/a/c/a1/f0/a$c;

    iget-object v3, p0, Lf/h/a/c/a1/f0/a;->a:Lf/h/a/c/a1/i;

    iget-object v4, p0, Lf/h/a/c/a1/f0/a;->b:Lf/h/a/c/a1/s;

    const-string v6, "audio/raw"

    move-object v2, p2

    invoke-direct/range {v2 .. v7}, Lf/h/a/c/a1/f0/a$c;-><init>(Lf/h/a/c/a1/i;Lf/h/a/c/a1/s;Lf/h/a/c/a1/f0/b;Ljava/lang/String;I)V

    iput-object p2, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    goto :goto_2

    :cond_6
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "Unsupported WAV format type: "

    invoke-static {p2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    iget v0, v5, Lf/h/a/c/a1/f0/b;->a:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "Unsupported or unrecognized wav header."

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_2
    iget p2, p0, Lf/h/a/c/a1/f0/a;->d:I

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    if-ne p2, v4, :cond_e

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput v1, p1, Lf/h/a/c/a1/e;->f:I

    new-instance p2, Lf/h/a/c/i1/r;

    const/16 v4, 0x8

    invoke-direct {p2, v4}, Lf/h/a/c/i1/r;-><init>(I)V

    invoke-static {p1, p2}, Lf/h/a/c/a1/f0/c;->a(Lf/h/a/c/a1/e;Lf/h/a/c/i1/r;)Lf/h/a/c/a1/f0/c;

    move-result-object v5

    :goto_3
    iget v6, v5, Lf/h/a/c/a1/f0/c;->a:I

    const v7, 0x64617461

    const-string v8, "WavHeaderReader"

    if-eq v6, v7, :cond_c

    const v7, 0x52494646

    if-eq v6, v7, :cond_9

    const v9, 0x666d7420

    if-eq v6, v9, :cond_9

    const-string v6, "Ignoring unknown WAV chunk: "

    invoke-static {v6}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v9, v5, Lf/h/a/c/a1/f0/c;->a:I

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const-wide/16 v8, 0x8

    iget-wide v10, v5, Lf/h/a/c/a1/f0/c;->b:J

    add-long/2addr v10, v8

    iget v6, v5, Lf/h/a/c/a1/f0/c;->a:I

    if-ne v6, v7, :cond_a

    const-wide/16 v10, 0xc

    :cond_a
    const-wide/32 v6, 0x7fffffff

    cmp-long v8, v10, v6

    if-gtz v8, :cond_b

    long-to-int v5, v10

    invoke-virtual {p1, v5}, Lf/h/a/c/a1/e;->i(I)V

    invoke-static {p1, p2}, Lf/h/a/c/a1/f0/c;->a(Lf/h/a/c/a1/e;Lf/h/a/c/i1/r;)Lf/h/a/c/a1/f0/c;

    move-result-object v5

    goto :goto_3

    :cond_b
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "Chunk is too large (~2GB+) to skip; id: "

    invoke-static {p2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    iget v0, v5, Lf/h/a/c/a1/f0/c;->a:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    invoke-virtual {p1, v4}, Lf/h/a/c/a1/e;->i(I)V

    iget-wide v6, p1, Lf/h/a/c/a1/e;->d:J

    iget-wide v4, v5, Lf/h/a/c/a1/f0/c;->b:J

    add-long/2addr v4, v6

    iget-wide v9, p1, Lf/h/a/c/a1/e;->c:J

    cmp-long p2, v9, v2

    if-eqz p2, :cond_d

    cmp-long p2, v4, v9

    if-lez p2, :cond_d

    const-string p2, "Data exceeds input length: "

    const-string v11, ", "

    invoke-static {p2, v4, v5, v11}, Lf/e/c/a/a;->K(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v8, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v4, v9

    :cond_d
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p2, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p2

    iget-object v4, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->intValue()I

    move-result v4

    iput v4, p0, Lf/h/a/c/a1/f0/a;->d:I

    iget-object p2, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lf/h/a/c/a1/f0/a;->e:J

    iget-object p2, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    iget v6, p0, Lf/h/a/c/a1/f0/a;->d:I

    invoke-interface {p2, v6, v4, v5}, Lf/h/a/c/a1/f0/a$b;->c(IJ)V

    goto :goto_4

    :cond_e
    iget-wide v4, p1, Lf/h/a/c/a1/e;->d:J

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-nez v8, :cond_f

    invoke-virtual {p1, p2}, Lf/h/a/c/a1/e;->i(I)V

    :cond_f
    :goto_4
    iget-wide v4, p0, Lf/h/a/c/a1/f0/a;->e:J

    cmp-long p2, v4, v2

    if-eqz p2, :cond_10

    goto :goto_5

    :cond_10
    const/4 v0, 0x0

    :goto_5
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iget-wide v2, p0, Lf/h/a/c/a1/f0/a;->e:J

    iget-wide v4, p1, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v2, v4

    iget-object p2, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    invoke-interface {p2, p1, v2, v3}, Lf/h/a/c/a1/f0/a$b;->a(Lf/h/a/c/a1/e;J)Z

    move-result p1

    if-eqz p1, :cond_11

    const/4 v1, -0x1

    :cond_11
    return v1
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 2

    iput-object p1, p0, Lf/h/a/c/a1/f0/a;->a:Lf/h/a/c/a1/i;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/f0/a;->b:Lf/h/a/c/a1/s;

    invoke-interface {p1}, Lf/h/a/c/a1/i;->k()V

    return-void
.end method

.method public f(JJ)V
    .locals 0

    iget-object p1, p0, Lf/h/a/c/a1/f0/a;->c:Lf/h/a/c/a1/f0/a$b;

    if-eqz p1, :cond_0

    invoke-interface {p1, p3, p4}, Lf/h/a/c/a1/f0/a$b;->b(J)V

    :cond_0
    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-static {p1}, Lf/g/j/k/a;->x0(Lf/h/a/c/a1/e;)Lf/h/a/c/a1/f0/b;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public release()V
    .locals 0

    return-void
.end method
