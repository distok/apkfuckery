.class public final Lf/h/a/c/a1/z/d;
.super Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;
.source "VideoTagPayloadReader.java"


# instance fields
.field public final b:Lf/h/a/c/i1/r;

.field public final c:Lf/h/a/c/i1/r;

.field public d:I

.field public e:Z

.field public f:Z

.field public g:I


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/s;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;-><init>(Lf/h/a/c/a1/s;)V

    new-instance p1, Lf/h/a/c/i1/r;

    sget-object v0, Lf/h/a/c/i1/p;->a:[B

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object p1, p0, Lf/h/a/c/a1/z/d;->b:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    const/4 v0, 0x4

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/z/d;->c:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public b(Lf/h/a/c/i1/r;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader$UnsupportedFormatException;
        }
    .end annotation

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->q()I

    move-result p1

    shr-int/lit8 v0, p1, 0x4

    and-int/lit8 v0, v0, 0xf

    and-int/lit8 p1, p1, 0xf

    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    iput v0, p0, Lf/h/a/c/a1/z/d;->g:I

    const/4 p1, 0x5

    if-eq v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader$UnsupportedFormatException;

    const-string v1, "Video format not supported: "

    invoke-static {v1, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader$UnsupportedFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(Lf/h/a/c/i1/r;J)Z
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    iget-object v3, v1, Lf/h/a/c/i1/r;->a:[B

    iget v4, v1, Lf/h/a/c/i1/r;->b:I

    add-int/lit8 v5, v4, 0x1

    iput v5, v1, Lf/h/a/c/i1/r;->b:I

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    shr-int/lit8 v4, v4, 0x8

    add-int/lit8 v6, v5, 0x1

    iput v6, v1, Lf/h/a/c/i1/r;->b:I

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    add-int/lit8 v5, v6, 0x1

    iput v5, v1, Lf/h/a/c/i1/r;->b:I

    aget-byte v3, v3, v6

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v3, v4

    int-to-long v3, v3

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    add-long v6, v3, p2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    iget-boolean v5, v0, Lf/h/a/c/a1/z/d;->e:Z

    if-nez v5, :cond_0

    new-instance v2, Lf/h/a/c/i1/r;

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v5

    new-array v5, v5, [B

    invoke-direct {v2, v5}, Lf/h/a/c/i1/r;-><init>([B)V

    iget-object v5, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v6

    invoke-virtual {v1, v5, v4, v6}, Lf/h/a/c/i1/r;->d([BII)V

    invoke-static {v2}, Lf/h/a/c/j1/h;->b(Lf/h/a/c/i1/r;)Lf/h/a/c/j1/h;

    move-result-object v1

    iget v2, v1, Lf/h/a/c/j1/h;->b:I

    iput v2, v0, Lf/h/a/c/a1/z/d;->d:I

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v9, -0x1

    iget v10, v1, Lf/h/a/c/j1/h;->c:I

    iget v11, v1, Lf/h/a/c/j1/h;->d:I

    const/high16 v12, -0x40800000    # -1.0f

    iget-object v13, v1, Lf/h/a/c/j1/h;->a:Ljava/util/List;

    const/4 v14, -0x1

    iget v15, v1, Lf/h/a/c/j1/h;->e:F

    const/16 v16, 0x0

    const-string v6, "video/avc"

    invoke-static/range {v5 .. v16}, Lcom/google/android/exoplayer2/Format;->m(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIFLjava/util/List;IFLcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;->a:Lf/h/a/c/a1/s;

    invoke-interface {v2, v1}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    iput-boolean v3, v0, Lf/h/a/c/a1/z/d;->e:Z

    return v4

    :cond_0
    if-ne v2, v3, :cond_4

    iget-boolean v2, v0, Lf/h/a/c/a1/z/d;->e:Z

    if-eqz v2, :cond_4

    iget v2, v0, Lf/h/a/c/a1/z/d;->g:I

    if-ne v2, v3, :cond_1

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    :goto_0
    iget-boolean v2, v0, Lf/h/a/c/a1/z/d;->f:Z

    if-nez v2, :cond_2

    if-nez v8, :cond_2

    return v4

    :cond_2
    iget-object v2, v0, Lf/h/a/c/a1/z/d;->c:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    aput-byte v4, v2, v4

    aput-byte v4, v2, v3

    const/4 v5, 0x2

    aput-byte v4, v2, v5

    iget v2, v0, Lf/h/a/c/a1/z/d;->d:I

    const/4 v5, 0x4

    rsub-int/lit8 v2, v2, 0x4

    const/4 v9, 0x0

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    if-lez v10, :cond_3

    iget-object v10, v0, Lf/h/a/c/a1/z/d;->c:Lf/h/a/c/i1/r;

    iget-object v10, v10, Lf/h/a/c/i1/r;->a:[B

    iget v11, v0, Lf/h/a/c/a1/z/d;->d:I

    invoke-virtual {v1, v10, v2, v11}, Lf/h/a/c/i1/r;->d([BII)V

    iget-object v10, v0, Lf/h/a/c/a1/z/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v10, v4}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v10, v0, Lf/h/a/c/a1/z/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v10}, Lf/h/a/c/i1/r;->t()I

    move-result v10

    iget-object v11, v0, Lf/h/a/c/a1/z/d;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v11, v4}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v11, v0, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;->a:Lf/h/a/c/a1/s;

    iget-object v12, v0, Lf/h/a/c/a1/z/d;->b:Lf/h/a/c/i1/r;

    invoke-interface {v11, v12, v5}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    add-int/lit8 v9, v9, 0x4

    iget-object v11, v0, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;->a:Lf/h/a/c/a1/s;

    invoke-interface {v11, v1, v10}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    add-int/2addr v9, v10

    goto :goto_1

    :cond_3
    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;->a:Lf/h/a/c/a1/s;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-interface/range {v5 .. v11}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iput-boolean v3, v0, Lf/h/a/c/a1/z/d;->f:Z

    return v3

    :cond_4
    return v4
.end method
