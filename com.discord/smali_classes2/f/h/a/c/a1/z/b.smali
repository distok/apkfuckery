.class public final Lf/h/a/c/a1/z/b;
.super Ljava/lang/Object;
.source "FlvExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# instance fields
.field public final a:Lf/h/a/c/i1/r;

.field public final b:Lf/h/a/c/i1/r;

.field public final c:Lf/h/a/c/i1/r;

.field public final d:Lf/h/a/c/i1/r;

.field public final e:Lf/h/a/c/a1/z/c;

.field public f:Lf/h/a/c/a1/i;

.field public g:I

.field public h:Z

.field public i:J

.field public j:I

.field public k:I

.field public l:I

.field public m:J

.field public n:Z

.field public o:Lf/h/a/c/a1/z/a;

.field public p:Lf/h/a/c/a1/z/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/r;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/a1/z/b;->b:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/z/b;->d:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/a1/z/c;

    invoke-direct {v0}, Lf/h/a/c/a1/z/c;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/z/b;->e:Lf/h/a/c/a1/z/c;

    const/4 v0, 0x1

    iput v0, p0, Lf/h/a/c/a1/z/b;->g:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    iget-boolean v0, p0, Lf/h/a/c/a1/z/b;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->f:Lf/h/a/c/a1/i;

    new-instance v1, Lf/h/a/c/a1/q$b;

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    const-wide/16 v4, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {v0, v1}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/a1/z/b;->n:Z

    :cond_0
    return-void
.end method

.method public final b(Lf/h/a/c/a1/e;)Lf/h/a/c/i1/r;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget v0, p0, Lf/h/a/c/a1/z/b;->l:I

    iget-object v1, p0, Lf/h/a/c/a1/z/b;->d:Lf/h/a/c/i1/r;

    iget-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    array-length v3, v2

    const/4 v4, 0x0

    if-le v0, v3, :cond_0

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, v1, Lf/h/a/c/i1/r;->a:[B

    iput v4, v1, Lf/h/a/c/i1/r;->c:I

    iput v4, v1, Lf/h/a/c/i1/r;->b:I

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v4}, Lf/h/a/c/i1/r;->C(I)V

    :goto_0
    iget-object v0, p0, Lf/h/a/c/a1/z/b;->d:Lf/h/a/c/i1/r;

    iget v1, p0, Lf/h/a/c/a1/z/b;->l:I

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->B(I)V

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->d:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    iget v1, p0, Lf/h/a/c/a1/z/b;->l:I

    invoke-virtual {p1, v0, v4, v1, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object p1, p0, Lf/h/a/c/a1/z/b;->d:Lf/h/a/c/i1/r;

    return-object p1
.end method

.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    :cond_0
    :goto_0
    iget v2, v0, Lf/h/a/c/a1/z/b;->g:I

    const/4 v3, -0x1

    const/16 v4, 0x8

    const/16 v5, 0x9

    const/4 v6, 0x2

    const/4 v7, 0x4

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-eq v2, v9, :cond_d

    const/4 v10, 0x3

    if-eq v2, v6, :cond_c

    if-eq v2, v10, :cond_a

    if-ne v2, v7, :cond_9

    iget-boolean v2, v0, Lf/h/a/c/a1/z/b;->h:Z

    const-wide/16 v10, 0x0

    const-wide v12, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v2, :cond_1

    iget-wide v2, v0, Lf/h/a/c/a1/z/b;->i:J

    iget-wide v14, v0, Lf/h/a/c/a1/z/b;->m:J

    add-long/2addr v2, v14

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lf/h/a/c/a1/z/b;->e:Lf/h/a/c/a1/z/c;

    iget-wide v2, v2, Lf/h/a/c/a1/z/c;->b:J

    cmp-long v14, v2, v12

    if-nez v14, :cond_2

    move-wide v2, v10

    goto :goto_1

    :cond_2
    iget-wide v2, v0, Lf/h/a/c/a1/z/b;->m:J

    :goto_1
    iget v14, v0, Lf/h/a/c/a1/z/b;->k:I

    if-ne v14, v4, :cond_3

    iget-object v4, v0, Lf/h/a/c/a1/z/b;->o:Lf/h/a/c/a1/z/a;

    if-eqz v4, :cond_3

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/z/b;->a()V

    iget-object v4, v0, Lf/h/a/c/a1/z/b;->o:Lf/h/a/c/a1/z/a;

    invoke-virtual/range {p0 .. p1}, Lf/h/a/c/a1/z/b;->b(Lf/h/a/c/a1/e;)Lf/h/a/c/i1/r;

    move-result-object v5

    invoke-virtual {v4, v5, v2, v3}, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;->a(Lf/h/a/c/i1/r;J)Z

    move-result v2

    goto :goto_2

    :cond_3
    if-ne v14, v5, :cond_4

    iget-object v4, v0, Lf/h/a/c/a1/z/b;->p:Lf/h/a/c/a1/z/d;

    if-eqz v4, :cond_4

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/z/b;->a()V

    iget-object v4, v0, Lf/h/a/c/a1/z/b;->p:Lf/h/a/c/a1/z/d;

    invoke-virtual/range {p0 .. p1}, Lf/h/a/c/a1/z/b;->b(Lf/h/a/c/a1/e;)Lf/h/a/c/i1/r;

    move-result-object v5

    invoke-virtual {v4, v5, v2, v3}, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;->a(Lf/h/a/c/i1/r;J)Z

    move-result v2

    goto :goto_2

    :cond_4
    const/16 v4, 0x12

    if-ne v14, v4, :cond_6

    iget-boolean v4, v0, Lf/h/a/c/a1/z/b;->n:Z

    if-nez v4, :cond_6

    iget-object v4, v0, Lf/h/a/c/a1/z/b;->e:Lf/h/a/c/a1/z/c;

    invoke-virtual/range {p0 .. p1}, Lf/h/a/c/a1/z/b;->b(Lf/h/a/c/a1/e;)Lf/h/a/c/i1/r;

    move-result-object v5

    invoke-virtual {v4, v5, v2, v3}, Lcom/google/android/exoplayer2/extractor/flv/TagPayloadReader;->a(Lf/h/a/c/i1/r;J)Z

    move-result v2

    iget-object v3, v0, Lf/h/a/c/a1/z/b;->e:Lf/h/a/c/a1/z/c;

    iget-wide v3, v3, Lf/h/a/c/a1/z/c;->b:J

    cmp-long v5, v3, v12

    if-eqz v5, :cond_5

    iget-object v5, v0, Lf/h/a/c/a1/z/b;->f:Lf/h/a/c/a1/i;

    new-instance v14, Lf/h/a/c/a1/q$b;

    invoke-direct {v14, v3, v4, v10, v11}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {v5, v14}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    iput-boolean v9, v0, Lf/h/a/c/a1/z/b;->n:Z

    :cond_5
    :goto_2
    const/4 v3, 0x1

    goto :goto_3

    :cond_6
    iget v2, v0, Lf/h/a/c/a1/z/b;->l:I

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e;->i(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_3
    iget-boolean v4, v0, Lf/h/a/c/a1/z/b;->h:Z

    if-nez v4, :cond_8

    if-eqz v2, :cond_8

    iput-boolean v9, v0, Lf/h/a/c/a1/z/b;->h:Z

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->e:Lf/h/a/c/a1/z/c;

    iget-wide v4, v2, Lf/h/a/c/a1/z/c;->b:J

    cmp-long v2, v4, v12

    if-nez v2, :cond_7

    iget-wide v4, v0, Lf/h/a/c/a1/z/b;->m:J

    neg-long v10, v4

    :cond_7
    iput-wide v10, v0, Lf/h/a/c/a1/z/b;->i:J

    :cond_8
    iput v7, v0, Lf/h/a/c/a1/z/b;->j:I

    iput v6, v0, Lf/h/a/c/a1/z/b;->g:I

    if-eqz v3, :cond_0

    return v8

    :cond_9
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_a
    iget-object v2, v0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    const/16 v4, 0xb

    invoke-virtual {v1, v2, v8, v4, v9}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_4

    :cond_b
    iget-object v2, v0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v8}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    iput v2, v0, Lf/h/a/c/a1/z/b;->k:I

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->s()I

    move-result v2

    iput v2, v0, Lf/h/a/c/a1/z/b;->l:I

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->s()I

    move-result v2

    int-to-long v4, v2

    iput-wide v4, v0, Lf/h/a/c/a1/z/b;->m:J

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    shl-int/lit8 v2, v2, 0x18

    int-to-long v4, v2

    iget-wide v11, v0, Lf/h/a/c/a1/z/b;->m:J

    or-long/2addr v4, v11

    const-wide/16 v11, 0x3e8

    mul-long v4, v4, v11

    iput-wide v4, v0, Lf/h/a/c/a1/z/b;->m:J

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v10}, Lf/h/a/c/i1/r;->D(I)V

    iput v7, v0, Lf/h/a/c/a1/z/b;->g:I

    const/4 v8, 0x1

    :goto_4
    if-nez v8, :cond_0

    return v3

    :cond_c
    iget v2, v0, Lf/h/a/c/a1/z/b;->j:I

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e;->i(I)V

    iput v8, v0, Lf/h/a/c/a1/z/b;->j:I

    iput v10, v0, Lf/h/a/c/a1/z/b;->g:I

    goto/16 :goto_0

    :cond_d
    iget-object v2, v0, Lf/h/a/c/a1/z/b;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v2, v8, v5, v9}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    move-result v2

    if-nez v2, :cond_e

    goto :goto_6

    :cond_e
    iget-object v2, v0, Lf/h/a/c/a1/z/b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v8}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v7}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    and-int/lit8 v10, v2, 0x4

    if-eqz v10, :cond_f

    const/4 v10, 0x1

    goto :goto_5

    :cond_f
    const/4 v10, 0x0

    :goto_5
    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_10

    const/4 v8, 0x1

    :cond_10
    if-eqz v10, :cond_11

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->o:Lf/h/a/c/a1/z/a;

    if-nez v2, :cond_11

    new-instance v2, Lf/h/a/c/a1/z/a;

    iget-object v10, v0, Lf/h/a/c/a1/z/b;->f:Lf/h/a/c/a1/i;

    invoke-interface {v10, v4, v9}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v4

    invoke-direct {v2, v4}, Lf/h/a/c/a1/z/a;-><init>(Lf/h/a/c/a1/s;)V

    iput-object v2, v0, Lf/h/a/c/a1/z/b;->o:Lf/h/a/c/a1/z/a;

    :cond_11
    if-eqz v8, :cond_12

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->p:Lf/h/a/c/a1/z/d;

    if-nez v2, :cond_12

    new-instance v2, Lf/h/a/c/a1/z/d;

    iget-object v4, v0, Lf/h/a/c/a1/z/b;->f:Lf/h/a/c/a1/i;

    invoke-interface {v4, v5, v6}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v4

    invoke-direct {v2, v4}, Lf/h/a/c/a1/z/d;-><init>(Lf/h/a/c/a1/s;)V

    iput-object v2, v0, Lf/h/a/c/a1/z/b;->p:Lf/h/a/c/a1/z/d;

    :cond_12
    iget-object v2, v0, Lf/h/a/c/a1/z/b;->f:Lf/h/a/c/a1/i;

    invoke-interface {v2}, Lf/h/a/c/a1/i;->k()V

    iget-object v2, v0, Lf/h/a/c/a1/z/b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v2

    sub-int/2addr v2, v5

    add-int/2addr v2, v7

    iput v2, v0, Lf/h/a/c/a1/z/b;->j:I

    iput v6, v0, Lf/h/a/c/a1/z/b;->g:I

    const/4 v8, 0x1

    :goto_6
    if-nez v8, :cond_0

    return v3
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/z/b;->f:Lf/h/a/c/a1/i;

    return-void
.end method

.method public f(JJ)V
    .locals 0

    const/4 p1, 0x1

    iput p1, p0, Lf/h/a/c/a1/z/b;->g:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/c/a1/z/b;->h:Z

    iput p1, p0, Lf/h/a/c/a1/z/b;->j:I

    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {p1, v0, v1, v2, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->s()I

    move-result v0

    const v2, 0x464c56

    if-eq v0, v2, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    const/4 v2, 0x2

    invoke-virtual {p1, v0, v1, v2, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->v()I

    move-result v0

    and-int/lit16 v0, v0, 0xfa

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    const/4 v2, 0x4

    invoke-virtual {p1, v0, v1, v2, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v0

    iput v1, p1, Lf/h/a/c/a1/e;->f:I

    invoke-virtual {p1, v0, v1}, Lf/h/a/c/a1/e;->a(IZ)Z

    iget-object v0, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v0, v1, v2, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object p1, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {p1, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object p1, p0, Lf/h/a/c/a1/z/b;->a:Lf/h/a/c/i1/r;

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->e()I

    move-result p1

    if-nez p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public release()V
    .locals 0

    return-void
.end method
