.class public final Lf/h/a/c/a1/d0/i;
.super Lf/h/a/c/a1/d0/h;
.source "VorbisReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/d0/i$a;
    }
.end annotation


# instance fields
.field public n:Lf/h/a/c/a1/d0/i$a;

.field public o:I

.field public p:Z

.field public q:Lf/h/a/c/a1/w;

.field public r:Lf/h/a/c/a1/u;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/a/c/a1/d0/h;-><init>()V

    return-void
.end method


# virtual methods
.method public b(J)V
    .locals 4

    iput-wide p1, p0, Lf/h/a/c/a1/d0/h;->g:J

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    cmp-long v3, p1, v0

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lf/h/a/c/a1/d0/i;->p:Z

    iget-object p1, p0, Lf/h/a/c/a1/d0/i;->q:Lf/h/a/c/a1/w;

    if-eqz p1, :cond_1

    iget v2, p1, Lf/h/a/c/a1/w;->d:I

    :cond_1
    iput v2, p0, Lf/h/a/c/a1/d0/i;->o:I

    return-void
.end method

.method public c(Lf/h/a/c/i1/r;)J
    .locals 11

    iget-object v0, p1, Lf/h/a/c/i1/r;->a:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-ne v2, v3, :cond_0

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_0
    aget-byte v0, v0, v1

    iget-object v2, p0, Lf/h/a/c/a1/d0/i;->n:Lf/h/a/c/a1/d0/i$a;

    iget v4, v2, Lf/h/a/c/a1/d0/i$a;->d:I

    shr-int/2addr v0, v3

    const/16 v5, 0x8

    rsub-int/lit8 v4, v4, 0x8

    const/16 v6, 0xff

    ushr-int v4, v6, v4

    and-int/2addr v0, v4

    iget-object v4, v2, Lf/h/a/c/a1/d0/i$a;->c:[Lf/h/a/c/a1/v;

    aget-object v0, v4, v0

    iget-boolean v0, v0, Lf/h/a/c/a1/v;->a:Z

    if-nez v0, :cond_1

    iget-object v0, v2, Lf/h/a/c/a1/d0/i$a;->a:Lf/h/a/c/a1/w;

    iget v0, v0, Lf/h/a/c/a1/w;->d:I

    goto :goto_0

    :cond_1
    iget-object v0, v2, Lf/h/a/c/a1/d0/i$a;->a:Lf/h/a/c/a1/w;

    iget v0, v0, Lf/h/a/c/a1/w;->e:I

    :goto_0
    iget-boolean v2, p0, Lf/h/a/c/a1/d0/i;->p:Z

    if-eqz v2, :cond_2

    iget v1, p0, Lf/h/a/c/a1/d0/i;->o:I

    add-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x4

    :cond_2
    int-to-long v1, v1

    iget v4, p1, Lf/h/a/c/i1/r;->c:I

    add-int/lit8 v4, v4, 0x4

    invoke-virtual {p1, v4}, Lf/h/a/c/i1/r;->B(I)V

    iget-object v4, p1, Lf/h/a/c/i1/r;->a:[B

    iget p1, p1, Lf/h/a/c/i1/r;->c:I

    add-int/lit8 v6, p1, -0x4

    const-wide/16 v7, 0xff

    and-long v9, v1, v7

    long-to-int v10, v9

    int-to-byte v9, v10

    aput-byte v9, v4, v6

    add-int/lit8 v6, p1, -0x3

    ushr-long v9, v1, v5

    and-long/2addr v9, v7

    long-to-int v5, v9

    int-to-byte v5, v5

    aput-byte v5, v4, v6

    add-int/lit8 v5, p1, -0x2

    const/16 v6, 0x10

    ushr-long v9, v1, v6

    and-long/2addr v9, v7

    long-to-int v6, v9

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    add-int/lit8 p1, p1, -0x1

    const/16 v5, 0x18

    ushr-long v5, v1, v5

    and-long/2addr v5, v7

    long-to-int v6, v5

    int-to-byte v5, v6

    aput-byte v5, v4, p1

    iput-boolean v3, p0, Lf/h/a/c/a1/d0/i;->p:Z

    iput v0, p0, Lf/h/a/c/a1/d0/i;->o:I

    return-wide v1
.end method

.method public d(Lf/h/a/c/i1/r;JLf/h/a/c/a1/d0/h$b;)Z
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lf/h/a/c/a1/d0/i;->n:Lf/h/a/c/a1/d0/i$a;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    return v3

    :cond_0
    iget-object v2, v0, Lf/h/a/c/a1/d0/i;->q:Lf/h/a/c/a1/w;

    const/4 v4, 0x1

    if-nez v2, :cond_2

    invoke-static {v4, v1, v3}, Lf/g/j/k/a;->S0(ILf/h/a/c/i1/r;Z)Z

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->h()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->h()J

    move-result-wide v9

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->g()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->g()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->g()I

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    and-int/lit8 v3, v2, 0xf

    int-to-double v3, v3

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    double-to-int v3, v3

    and-int/lit16 v2, v2, 0xf0

    shr-int/lit8 v2, v2, 0x4

    int-to-double v4, v2

    invoke-static {v14, v15, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v15, v4

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-lez v2, :cond_1

    const/4 v2, 0x1

    const/16 v16, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    const/16 v16, 0x0

    :goto_0
    iget-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    iget v1, v1, Lf/h/a/c/i1/r;->c:I

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v17

    new-instance v1, Lf/h/a/c/a1/w;

    move-object v5, v1

    move v14, v3

    invoke-direct/range {v5 .. v17}, Lf/h/a/c/a1/w;-><init>(JIJIIIIIZ[B)V

    iput-object v1, v0, Lf/h/a/c/a1/d0/i;->q:Lf/h/a/c/a1/w;

    goto :goto_1

    :cond_2
    iget-object v2, v0, Lf/h/a/c/a1/d0/i;->r:Lf/h/a/c/a1/u;

    if-nez v2, :cond_3

    const/4 v2, 0x1

    invoke-static {v1, v2, v2}, Lf/g/j/k/a;->L0(Lf/h/a/c/i1/r;ZZ)Lf/h/a/c/a1/u;

    move-result-object v1

    iput-object v1, v0, Lf/h/a/c/a1/d0/i;->r:Lf/h/a/c/a1/u;

    :goto_1
    const/4 v1, 0x0

    goto/16 :goto_1c

    :cond_3
    iget v2, v1, Lf/h/a/c/i1/r;->c:I

    new-array v6, v2, [B

    iget-object v3, v1, Lf/h/a/c/i1/r;->a:[B

    const/4 v4, 0x0

    invoke-static {v3, v4, v6, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, v0, Lf/h/a/c/a1/d0/i;->q:Lf/h/a/c/a1/w;

    iget v2, v2, Lf/h/a/c/a1/w;->a:I

    const/4 v3, 0x5

    invoke-static {v3, v1, v4}, Lf/g/j/k/a;->S0(ILf/h/a/c/i1/r;Z)Z

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    new-instance v7, Lf/h/a/c/a1/t;

    iget-object v8, v1, Lf/h/a/c/i1/r;->a:[B

    invoke-direct {v7, v8}, Lf/h/a/c/a1/t;-><init>([B)V

    iget v1, v1, Lf/h/a/c/i1/r;->b:I

    mul-int/lit8 v1, v1, 0x8

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    const/4 v1, 0x0

    :goto_2
    const/16 v8, 0x10

    const/16 v9, 0x18

    if-ge v1, v5, :cond_f

    invoke-virtual {v7, v9}, Lf/h/a/c/a1/t;->b(I)I

    move-result v10

    const v11, 0x564342

    if-ne v10, v11, :cond_e

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->b(I)I

    move-result v8

    invoke-virtual {v7, v9}, Lf/h/a/c/a1/t;->b(I)I

    move-result v9

    new-array v10, v9, [J

    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v11

    const-wide/16 v12, 0x0

    if-nez v11, :cond_6

    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v11

    :goto_3
    if-ge v4, v9, :cond_8

    if-eqz v11, :cond_5

    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-virtual {v7, v3}, Lf/h/a/c/a1/t;->b(I)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    int-to-long v14, v14

    aput-wide v14, v10, v4

    goto :goto_4

    :cond_4
    aput-wide v12, v10, v4

    goto :goto_4

    :cond_5
    invoke-virtual {v7, v3}, Lf/h/a/c/a1/t;->b(I)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    int-to-long v14, v14

    aput-wide v14, v10, v4

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {v7, v3}, Lf/h/a/c/a1/t;->b(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/4 v11, 0x0

    :goto_5
    if-ge v11, v9, :cond_8

    sub-int v14, v9, v11

    invoke-static {v14}, Lf/g/j/k/a;->m0(I)I

    move-result v14

    invoke-virtual {v7, v14}, Lf/h/a/c/a1/t;->b(I)I

    move-result v14

    const/4 v15, 0x0

    :goto_6
    if-ge v15, v14, :cond_7

    if-ge v11, v9, :cond_7

    int-to-long v12, v4

    aput-wide v12, v10, v11

    add-int/lit8 v11, v11, 0x1

    add-int/lit8 v15, v15, 0x1

    const-wide/16 v12, 0x0

    goto :goto_6

    :cond_7
    add-int/lit8 v4, v4, 0x1

    const-wide/16 v12, 0x0

    goto :goto_5

    :cond_8
    const/4 v4, 0x4

    invoke-virtual {v7, v4}, Lf/h/a/c/a1/t;->b(I)I

    move-result v10

    const/4 v11, 0x2

    if-gt v10, v11, :cond_d

    const/4 v12, 0x1

    if-eq v10, v12, :cond_9

    if-ne v10, v11, :cond_c

    :cond_9
    const/16 v11, 0x20

    invoke-virtual {v7, v11}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v11}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v4}, Lf/h/a/c/a1/t;->b(I)I

    move-result v4

    add-int/2addr v4, v12

    invoke-virtual {v7, v12}, Lf/h/a/c/a1/t;->c(I)V

    if-ne v10, v12, :cond_b

    if-eqz v8, :cond_a

    int-to-long v9, v9

    int-to-long v11, v8

    long-to-double v8, v9

    long-to-double v10, v11

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    div-double/2addr v12, v10

    invoke-static {v8, v9, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-long v12, v8

    goto :goto_7

    :cond_a
    const-wide/16 v12, 0x0

    goto :goto_7

    :cond_b
    int-to-long v9, v9

    int-to-long v11, v8

    mul-long v12, v9, v11

    :goto_7
    int-to-long v8, v4

    mul-long v8, v8, v12

    long-to-int v4, v8

    invoke-virtual {v7, v4}, Lf/h/a/c/a1/t;->c(I)V

    :cond_c
    add-int/lit8 v1, v1, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_d
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "lookup type greater than 2 not decodable: "

    invoke-static {v2, v10}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "expected code book to start with [0x56, 0x43, 0x42] at "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v7, Lf/h/a/c/a1/t;->c:I

    mul-int/lit8 v3, v3, 0x8

    iget v4, v7, Lf/h/a/c/a1/t;->d:I

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_f
    const/4 v1, 0x6

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->b(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x0

    :goto_8
    if-ge v5, v4, :cond_11

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->b(I)I

    move-result v9

    if-nez v9, :cond_10

    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    :cond_10
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "placeholder of time domain transforms not zeroed out"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_11
    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->b(I)I

    move-result v4

    const/4 v5, 0x1

    add-int/2addr v4, v5

    const/4 v9, 0x0

    :goto_9
    const/4 v10, 0x3

    if-ge v9, v4, :cond_1b

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->b(I)I

    move-result v11

    if-eqz v11, :cond_19

    if-ne v11, v5, :cond_18

    invoke-virtual {v7, v3}, Lf/h/a/c/a1/t;->b(I)I

    move-result v3

    new-array v5, v3, [I

    const/4 v11, -0x1

    const/4 v12, 0x0

    :goto_a
    if-ge v12, v3, :cond_13

    const/4 v13, 0x4

    invoke-virtual {v7, v13}, Lf/h/a/c/a1/t;->b(I)I

    move-result v13

    aput v13, v5, v12

    aget v13, v5, v12

    if-le v13, v11, :cond_12

    aget v11, v5, v12

    :cond_12
    add-int/lit8 v12, v12, 0x1

    goto :goto_a

    :cond_13
    add-int/lit8 v11, v11, 0x1

    new-array v12, v11, [I

    const/4 v13, 0x0

    :goto_b
    if-ge v13, v11, :cond_16

    invoke-virtual {v7, v10}, Lf/h/a/c/a1/t;->b(I)I

    move-result v10

    const/4 v14, 0x1

    add-int/lit8 v10, v10, 0x1

    aput v10, v12, v13

    const/4 v10, 0x2

    invoke-virtual {v7, v10}, Lf/h/a/c/a1/t;->b(I)I

    move-result v10

    const/16 v15, 0x8

    if-lez v10, :cond_14

    invoke-virtual {v7, v15}, Lf/h/a/c/a1/t;->c(I)V

    :cond_14
    const/16 v16, 0x0

    const/4 v1, 0x0

    :goto_c
    shl-int/2addr v14, v10

    if-ge v1, v14, :cond_15

    invoke-virtual {v7, v15}, Lf/h/a/c/a1/t;->c(I)V

    add-int/lit8 v1, v1, 0x1

    const/16 v15, 0x8

    const/4 v14, 0x1

    goto :goto_c

    :cond_15
    add-int/lit8 v13, v13, 0x1

    const/4 v10, 0x3

    const/4 v1, 0x6

    goto :goto_b

    :cond_16
    const/4 v1, 0x2

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    const/4 v1, 0x4

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->b(I)I

    move-result v1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    :goto_d
    if-ge v10, v3, :cond_1a

    aget v14, v5, v10

    aget v14, v12, v14

    add-int/2addr v11, v14

    :goto_e
    if-ge v13, v11, :cond_17

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_e

    :cond_17
    add-int/lit8 v10, v10, 0x1

    goto :goto_d

    :cond_18
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "floor type greater than 1 not decodable: "

    invoke-static {v2, v11}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_19
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->c(I)V

    const/4 v3, 0x6

    invoke-virtual {v7, v3}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    const/4 v3, 0x4

    invoke-virtual {v7, v3}, Lf/h/a/c/a1/t;->b(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const/4 v5, 0x0

    :goto_f
    if-ge v5, v3, :cond_1a

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    add-int/lit8 v5, v5, 0x1

    const/16 v1, 0x8

    goto :goto_f

    :cond_1a
    add-int/lit8 v9, v9, 0x1

    const/4 v3, 0x5

    const/4 v5, 0x1

    const/4 v1, 0x6

    goto/16 :goto_9

    :cond_1b
    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->b(I)I

    move-result v3

    const/4 v1, 0x1

    add-int/2addr v3, v1

    const/4 v4, 0x0

    :goto_10
    if-ge v4, v3, :cond_22

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->b(I)I

    move-result v5

    const/4 v9, 0x2

    if-gt v5, v9, :cond_21

    const/16 v5, 0x18

    invoke-virtual {v7, v5}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v5}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v5}, Lf/h/a/c/a1/t;->c(I)V

    const/4 v5, 0x6

    invoke-virtual {v7, v5}, Lf/h/a/c/a1/t;->b(I)I

    move-result v9

    add-int/2addr v9, v1

    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    new-array v5, v9, [I

    const/4 v10, 0x0

    :goto_11
    if-ge v10, v9, :cond_1d

    const/4 v11, 0x3

    invoke-virtual {v7, v11}, Lf/h/a/c/a1/t;->b(I)I

    move-result v11

    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v12

    if-eqz v12, :cond_1c

    const/4 v12, 0x5

    invoke-virtual {v7, v12}, Lf/h/a/c/a1/t;->b(I)I

    move-result v12

    goto :goto_12

    :cond_1c
    const/4 v12, 0x0

    :goto_12
    mul-int/lit8 v12, v12, 0x8

    add-int/2addr v12, v11

    aput v12, v5, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_11

    :cond_1d
    const/4 v10, 0x0

    :goto_13
    if-ge v10, v9, :cond_20

    const/4 v11, 0x0

    :goto_14
    if-ge v11, v1, :cond_1f

    aget v12, v5, v10

    const/4 v13, 0x1

    shl-int/2addr v13, v11

    and-int/2addr v12, v13

    if-eqz v12, :cond_1e

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->c(I)V

    :cond_1e
    add-int/lit8 v11, v11, 0x1

    const/16 v1, 0x8

    goto :goto_14

    :cond_1f
    add-int/lit8 v10, v10, 0x1

    const/16 v1, 0x8

    goto :goto_13

    :cond_20
    add-int/lit8 v4, v4, 0x1

    const/4 v1, 0x1

    goto :goto_10

    :cond_21
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "residueType greater than 2 is not decodable"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_22
    const/4 v1, 0x6

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->b(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const/4 v1, 0x0

    :goto_15
    if-ge v1, v3, :cond_29

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->b(I)I

    move-result v4

    if-eqz v4, :cond_23

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mapping type other than 0 not supported: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "VorbisUtil"

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1a

    :cond_23
    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v4

    if-eqz v4, :cond_24

    const/4 v4, 0x4

    invoke-virtual {v7, v4}, Lf/h/a/c/a1/t;->b(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_16

    :cond_24
    const/4 v4, 0x1

    :goto_16
    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v5

    if-eqz v5, :cond_25

    const/16 v5, 0x8

    invoke-virtual {v7, v5}, Lf/h/a/c/a1/t;->b(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    const/4 v9, 0x0

    :goto_17
    if-ge v9, v5, :cond_25

    add-int/lit8 v10, v2, -0x1

    invoke-static {v10}, Lf/g/j/k/a;->m0(I)I

    move-result v11

    invoke-virtual {v7, v11}, Lf/h/a/c/a1/t;->c(I)V

    invoke-static {v10}, Lf/g/j/k/a;->m0(I)I

    move-result v10

    invoke-virtual {v7, v10}, Lf/h/a/c/a1/t;->c(I)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_17

    :cond_25
    const/4 v5, 0x2

    invoke-virtual {v7, v5}, Lf/h/a/c/a1/t;->b(I)I

    move-result v5

    if-nez v5, :cond_28

    const/4 v5, 0x1

    if-le v4, v5, :cond_26

    const/4 v5, 0x0

    :goto_18
    if-ge v5, v2, :cond_26

    const/4 v9, 0x4

    invoke-virtual {v7, v9}, Lf/h/a/c/a1/t;->c(I)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_18

    :cond_26
    const/4 v5, 0x0

    :goto_19
    if-ge v5, v4, :cond_27

    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v9}, Lf/h/a/c/a1/t;->c(I)V

    invoke-virtual {v7, v9}, Lf/h/a/c/a1/t;->c(I)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_19

    :cond_27
    :goto_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    :cond_28
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "to reserved bits must be zero after mapping coupling steps"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_29
    const/4 v1, 0x6

    invoke-virtual {v7, v1}, Lf/h/a/c/a1/t;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-array v2, v1, [Lf/h/a/c/a1/v;

    const/4 v3, 0x0

    :goto_1b
    if-ge v3, v1, :cond_2a

    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v4

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->b(I)I

    move-result v5

    invoke-virtual {v7, v8}, Lf/h/a/c/a1/t;->b(I)I

    move-result v9

    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Lf/h/a/c/a1/t;->b(I)I

    move-result v10

    new-instance v11, Lf/h/a/c/a1/v;

    invoke-direct {v11, v4, v5, v9, v10}, Lf/h/a/c/a1/v;-><init>(ZIII)V

    aput-object v11, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1b

    :cond_2a
    invoke-virtual {v7}, Lf/h/a/c/a1/t;->a()Z

    move-result v3

    if-eqz v3, :cond_2c

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lf/g/j/k/a;->m0(I)I

    move-result v8

    new-instance v1, Lf/h/a/c/a1/d0/i$a;

    iget-object v4, v0, Lf/h/a/c/a1/d0/i;->q:Lf/h/a/c/a1/w;

    iget-object v5, v0, Lf/h/a/c/a1/d0/i;->r:Lf/h/a/c/a1/u;

    move-object v3, v1

    move-object v7, v2

    invoke-direct/range {v3 .. v8}, Lf/h/a/c/a1/d0/i$a;-><init>(Lf/h/a/c/a1/w;Lf/h/a/c/a1/u;[B[Lf/h/a/c/a1/v;I)V

    :goto_1c
    iput-object v1, v0, Lf/h/a/c/a1/d0/i;->n:Lf/h/a/c/a1/d0/i$a;

    if-nez v1, :cond_2b

    const/4 v1, 0x1

    return v1

    :cond_2b
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v0, Lf/h/a/c/a1/d0/i;->n:Lf/h/a/c/a1/d0/i$a;

    iget-object v1, v1, Lf/h/a/c/a1/d0/i$a;->a:Lf/h/a/c/a1/w;

    iget-object v1, v1, Lf/h/a/c/a1/w;->f:[B

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lf/h/a/c/a1/d0/i;->n:Lf/h/a/c/a1/d0/i$a;

    iget-object v1, v1, Lf/h/a/c/a1/d0/i$a;->b:[B

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v1, v0, Lf/h/a/c/a1/d0/i;->n:Lf/h/a/c/a1/d0/i$a;

    iget-object v1, v1, Lf/h/a/c/a1/d0/i$a;->a:Lf/h/a/c/a1/w;

    iget v5, v1, Lf/h/a/c/a1/w;->c:I

    const/4 v6, -0x1

    iget v7, v1, Lf/h/a/c/a1/w;->a:I

    iget-wide v10, v1, Lf/h/a/c/a1/w;->b:J

    long-to-int v8, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v3, "audio/vorbis"

    invoke-static/range {v2 .. v12}, Lcom/google/android/exoplayer2/Format;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    move-object/from16 v2, p4

    iput-object v1, v2, Lf/h/a/c/a1/d0/h$b;->a:Lcom/google/android/exoplayer2/Format;

    const/4 v1, 0x1

    return v1

    :cond_2c
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "framing bit after modes not set as expected"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public e(Z)V
    .locals 0

    invoke-super {p0, p1}, Lf/h/a/c/a1/d0/h;->e(Z)V

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/a1/d0/i;->n:Lf/h/a/c/a1/d0/i$a;

    iput-object p1, p0, Lf/h/a/c/a1/d0/i;->q:Lf/h/a/c/a1/w;

    iput-object p1, p0, Lf/h/a/c/a1/d0/i;->r:Lf/h/a/c/a1/u;

    :cond_0
    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/a1/d0/i;->o:I

    iput-boolean p1, p0, Lf/h/a/c/a1/d0/i;->p:Z

    return-void
.end method
