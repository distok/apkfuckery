.class public final Lf/h/a/c/a1/d0/b;
.super Lf/h/a/c/a1/d0/h;
.source "FlacReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/d0/b$a;
    }
.end annotation


# instance fields
.field public n:Lf/h/a/c/i1/l;

.field public o:Lf/h/a/c/a1/d0/b$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/a/c/a1/d0/h;-><init>()V

    return-void
.end method


# virtual methods
.method public c(Lf/h/a/c/i1/r;)J
    .locals 4

    iget-object v0, p1, Lf/h/a/c/i1/r;->a:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_1

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_1
    const/4 v2, 0x2

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    const/4 v2, 0x4

    shr-int/2addr v0, v2

    const/4 v3, 0x6

    if-eq v0, v3, :cond_2

    const/4 v3, 0x7

    if-ne v0, v3, :cond_3

    :cond_2
    invoke-virtual {p1, v2}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->w()J

    :cond_3
    invoke-static {p1, v0}, Lf/h/a/c/a1/k;->c(Lf/h/a/c/i1/r;I)I

    move-result v0

    invoke-virtual {p1, v1}, Lf/h/a/c/i1/r;->C(I)V

    int-to-long v0, v0

    return-wide v0
.end method

.method public d(Lf/h/a/c/i1/r;JLf/h/a/c/a1/d0/h$b;)Z
    .locals 5

    iget-object v0, p1, Lf/h/a/c/i1/r;->a:[B

    iget-object v1, p0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    const/4 v2, 0x1

    if-nez v1, :cond_0

    new-instance p2, Lf/h/a/c/i1/l;

    const/16 p3, 0x11

    invoke-direct {p2, v0, p3}, Lf/h/a/c/i1/l;-><init>([BI)V

    iput-object p2, p0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    const/16 p2, 0x9

    iget p1, p1, Lf/h/a/c/i1/r;->c:I

    invoke-static {v0, p2, p1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    iget-object p2, p0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Lf/h/a/c/i1/l;->e([BLcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format;

    move-result-object p1

    iput-object p1, p4, Lf/h/a/c/a1/d0/h$b;->a:Lcom/google/android/exoplayer2/Format;

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    aget-byte v3, v0, v1

    and-int/lit8 v3, v3, 0x7f

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    new-instance p2, Lf/h/a/c/a1/d0/b$a;

    invoke-direct {p2, p0}, Lf/h/a/c/a1/d0/b$a;-><init>(Lf/h/a/c/a1/d0/b;)V

    iput-object p2, p0, Lf/h/a/c/a1/d0/b;->o:Lf/h/a/c/a1/d0/b$a;

    invoke-static {p1}, Lf/g/j/k/a;->J0(Lf/h/a/c/i1/r;)Lf/h/a/c/i1/l$a;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    invoke-virtual {p2, p1}, Lf/h/a/c/i1/l;->b(Lf/h/a/c/i1/l$a;)Lf/h/a/c/i1/l;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    goto :goto_1

    :cond_1
    aget-byte p1, v0, v1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_4

    iget-object p1, p0, Lf/h/a/c/a1/d0/b;->o:Lf/h/a/c/a1/d0/b$a;

    if-eqz p1, :cond_3

    iput-wide p2, p1, Lf/h/a/c/a1/d0/b$a;->a:J

    iput-object p1, p4, Lf/h/a/c/a1/d0/h$b;->b:Lf/h/a/c/a1/d0/f;

    :cond_3
    return v1

    :cond_4
    :goto_1
    return v2
.end method

.method public e(Z)V
    .locals 0

    invoke-super {p0, p1}, Lf/h/a/c/a1/d0/h;->e(Z)V

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    iput-object p1, p0, Lf/h/a/c/a1/d0/b;->o:Lf/h/a/c/a1/d0/b$a;

    :cond_0
    return-void
.end method
