.class public final Lf/h/a/c/a1/d0/e;
.super Ljava/lang/Object;
.source "OggPageHeader.java"


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field public d:I

.field public e:I

.field public f:I

.field public final g:[I

.field public final h:Lf/h/a/c/i1/r;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xff

    new-array v1, v0, [I

    iput-object v1, p0, Lf/h/a/c/a1/d0/e;->g:[I

    new-instance v1, Lf/h/a/c/i1/r;

    invoke-direct {v1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v1, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;Z)Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->x()V

    invoke-virtual {p0}, Lf/h/a/c/a1/d0/e;->b()V

    iget-wide v0, p1, Lf/h/a/c/a1/e;->c:J

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    cmp-long v6, v0, v2

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1b

    cmp-long v6, v0, v2

    if-ltz v6, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_8

    iget-object v0, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    const/16 v1, 0x1b

    invoke-virtual {p1, v0, v4, v1, v5}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_3

    :cond_2
    iget-object v0, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v2

    const-wide/32 v6, 0x4f676753

    cmp-long v0, v2, v6

    if-eqz v0, :cond_4

    if-eqz p2, :cond_3

    return v4

    :cond_3
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "expected OggS capture pattern at begin of page"

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    iget-object v0, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->q()I

    move-result v0

    iput v0, p0, Lf/h/a/c/a1/d0/e;->a:I

    if-eqz v0, :cond_6

    if-eqz p2, :cond_5

    return v4

    :cond_5
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "unsupported bit stream revision"

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->q()I

    move-result p2

    iput p2, p0, Lf/h/a/c/a1/d0/e;->b:I

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    iget-object v0, p2, Lf/h/a/c/i1/r;->a:[B

    iget v2, p2, Lf/h/a/c/i1/r;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v2, v0, v2

    int-to-long v6, v2

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    add-int/lit8 v2, v3, 0x1

    iput v2, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v3, v0, v3

    int-to-long v10, v3

    and-long/2addr v10, v8

    const/16 v3, 0x8

    shl-long/2addr v10, v3

    or-long/2addr v6, v10

    add-int/lit8 v3, v2, 0x1

    iput v3, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v2, v0, v2

    int-to-long v10, v2

    and-long/2addr v10, v8

    const/16 v2, 0x10

    shl-long/2addr v10, v2

    or-long/2addr v6, v10

    add-int/lit8 v2, v3, 0x1

    iput v2, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v3, v0, v3

    int-to-long v10, v3

    and-long/2addr v10, v8

    const/16 v3, 0x18

    shl-long/2addr v10, v3

    or-long/2addr v6, v10

    add-int/lit8 v3, v2, 0x1

    iput v3, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v2, v0, v2

    int-to-long v10, v2

    and-long/2addr v10, v8

    const/16 v2, 0x20

    shl-long/2addr v10, v2

    or-long/2addr v6, v10

    add-int/lit8 v2, v3, 0x1

    iput v2, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v3, v0, v3

    int-to-long v10, v3

    and-long/2addr v10, v8

    const/16 v3, 0x28

    shl-long/2addr v10, v3

    or-long/2addr v6, v10

    add-int/lit8 v3, v2, 0x1

    iput v3, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v2, v0, v2

    int-to-long v10, v2

    and-long/2addr v10, v8

    const/16 v2, 0x30

    shl-long/2addr v10, v2

    or-long/2addr v6, v10

    add-int/lit8 v2, v3, 0x1

    iput v2, p2, Lf/h/a/c/i1/r;->b:I

    aget-byte v0, v0, v3

    int-to-long v2, v0

    and-long/2addr v2, v8

    const/16 v0, 0x38

    shl-long/2addr v2, v0

    or-long/2addr v2, v6

    iput-wide v2, p0, Lf/h/a/c/a1/d0/e;->c:J

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->h()J

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->h()J

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->h()J

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->q()I

    move-result p2

    iput p2, p0, Lf/h/a/c/a1/d0/e;->d:I

    add-int/2addr p2, v1

    iput p2, p0, Lf/h/a/c/a1/d0/e;->e:I

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->x()V

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    iget-object p2, p2, Lf/h/a/c/i1/r;->a:[B

    iget v0, p0, Lf/h/a/c/a1/d0/e;->d:I

    invoke-virtual {p1, p2, v4, v0, v4}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    :goto_2
    iget p1, p0, Lf/h/a/c/a1/d0/e;->d:I

    if-ge v4, p1, :cond_7

    iget-object p1, p0, Lf/h/a/c/a1/d0/e;->g:[I

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->h:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->q()I

    move-result p2

    aput p2, p1, v4

    iget p1, p0, Lf/h/a/c/a1/d0/e;->f:I

    iget-object p2, p0, Lf/h/a/c/a1/d0/e;->g:[I

    aget p2, p2, v4

    add-int/2addr p1, p2

    iput p1, p0, Lf/h/a/c/a1/d0/e;->f:I

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_7
    return v5

    :cond_8
    :goto_3
    if-eqz p2, :cond_9

    return v4

    :cond_9
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1
.end method

.method public b()V
    .locals 3

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/d0/e;->a:I

    iput v0, p0, Lf/h/a/c/a1/d0/e;->b:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lf/h/a/c/a1/d0/e;->c:J

    iput v0, p0, Lf/h/a/c/a1/d0/e;->d:I

    iput v0, p0, Lf/h/a/c/a1/d0/e;->e:I

    iput v0, p0, Lf/h/a/c/a1/d0/e;->f:I

    return-void
.end method
