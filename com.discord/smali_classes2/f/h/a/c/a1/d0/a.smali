.class public final Lf/h/a/c/a1/d0/a;
.super Ljava/lang/Object;
.source "DefaultOggSeeker.java"

# interfaces
.implements Lf/h/a/c/a1/d0/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/d0/a$b;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/d0/e;

.field public final b:J

.field public final c:J

.field public final d:Lf/h/a/c/a1/d0/h;

.field public e:I

.field public f:J

.field public g:J

.field public h:J

.field public i:J

.field public j:J

.field public k:J

.field public l:J


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/d0/h;JJJJZ)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/a1/d0/e;

    invoke-direct {v0}, Lf/h/a/c/a1/d0/e;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmp-long v3, p2, v1

    if-ltz v3, :cond_0

    cmp-long v1, p4, p2

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->d(Z)V

    iput-object p1, p0, Lf/h/a/c/a1/d0/a;->d:Lf/h/a/c/a1/d0/h;

    iput-wide p2, p0, Lf/h/a/c/a1/d0/a;->b:J

    iput-wide p4, p0, Lf/h/a/c/a1/d0/a;->c:J

    sub-long/2addr p4, p2

    cmp-long p1, p6, p4

    if-eqz p1, :cond_2

    if-eqz p10, :cond_1

    goto :goto_1

    :cond_1
    iput v0, p0, Lf/h/a/c/a1/d0/a;->e:I

    goto :goto_2

    :cond_2
    :goto_1
    iput-wide p8, p0, Lf/h/a/c/a1/d0/a;->f:J

    const/4 p1, 0x4

    iput p1, p0, Lf/h/a/c/a1/d0/a;->e:I

    :goto_2
    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;)J
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget v2, v0, Lf/h/a/c/a1/d0/a;->e:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x4

    if-eqz v2, :cond_c

    if-eq v2, v3, :cond_d

    const/4 v3, 0x2

    const/4 v8, 0x3

    const-wide/16 v9, -0x1

    if-eq v2, v3, :cond_1

    if-eq v2, v8, :cond_a

    if-ne v2, v5, :cond_0

    return-wide v9

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_1
    iget-wide v2, v0, Lf/h/a/c/a1/d0/a;->i:J

    iget-wide v11, v0, Lf/h/a/c/a1/d0/a;->j:J

    cmp-long v13, v2, v11

    if-nez v13, :cond_2

    :goto_0
    move-wide v11, v9

    goto/16 :goto_3

    :cond_2
    iget-wide v2, v1, Lf/h/a/c/a1/e;->d:J

    invoke-virtual {v0, v1, v11, v12}, Lf/h/a/c/a1/d0/a;->d(Lf/h/a/c/a1/e;J)Z

    move-result v11

    if-nez v11, :cond_4

    iget-wide v11, v0, Lf/h/a/c/a1/d0/a;->i:J

    cmp-long v13, v11, v2

    if-eqz v13, :cond_3

    goto/16 :goto_3

    :cond_3
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No ogg page can be found."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    iget-object v11, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    invoke-virtual {v11, v1, v4}, Lf/h/a/c/a1/d0/e;->a(Lf/h/a/c/a1/e;Z)Z

    iput v4, v1, Lf/h/a/c/a1/e;->f:I

    iget-wide v11, v0, Lf/h/a/c/a1/d0/a;->h:J

    iget-object v13, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    iget-wide v14, v13, Lf/h/a/c/a1/d0/e;->c:J

    sub-long/2addr v11, v14

    iget v6, v13, Lf/h/a/c/a1/d0/e;->e:I

    iget v7, v13, Lf/h/a/c/a1/d0/e;->f:I

    add-int/2addr v6, v7

    const-wide/16 v16, 0x0

    cmp-long v7, v16, v11

    if-gtz v7, :cond_5

    const-wide/32 v18, 0x11940

    cmp-long v7, v11, v18

    if-gez v7, :cond_5

    goto :goto_0

    :cond_5
    cmp-long v7, v11, v16

    if-gez v7, :cond_6

    iput-wide v2, v0, Lf/h/a/c/a1/d0/a;->j:J

    iput-wide v14, v0, Lf/h/a/c/a1/d0/a;->l:J

    goto :goto_1

    :cond_6
    iget-wide v2, v1, Lf/h/a/c/a1/e;->d:J

    int-to-long v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v0, Lf/h/a/c/a1/d0/a;->i:J

    iput-wide v14, v0, Lf/h/a/c/a1/d0/a;->k:J

    :goto_1
    iget-wide v2, v0, Lf/h/a/c/a1/d0/a;->j:J

    iget-wide v4, v0, Lf/h/a/c/a1/d0/a;->i:J

    sub-long v14, v2, v4

    const-wide/32 v17, 0x186a0

    cmp-long v19, v14, v17

    if-gez v19, :cond_7

    iput-wide v4, v0, Lf/h/a/c/a1/d0/a;->j:J

    move-wide v11, v4

    goto :goto_3

    :cond_7
    int-to-long v8, v6

    const-wide/16 v17, 0x1

    if-gtz v7, :cond_8

    const-wide/16 v6, 0x2

    goto :goto_2

    :cond_8
    move-wide/from16 v6, v17

    :goto_2
    mul-long v8, v8, v6

    iget-wide v6, v1, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v6, v8

    mul-long v14, v14, v11

    iget-wide v8, v0, Lf/h/a/c/a1/d0/a;->l:J

    iget-wide v10, v0, Lf/h/a/c/a1/d0/a;->k:J

    sub-long/2addr v8, v10

    div-long/2addr v14, v8

    add-long/2addr v6, v14

    sub-long v21, v2, v17

    move-wide/from16 v17, v6

    move-wide/from16 v19, v4

    invoke-static/range {v17 .. v22}, Lf/h/a/c/i1/a0;->g(JJJ)J

    move-result-wide v11

    :goto_3
    const-wide/16 v2, -0x1

    cmp-long v4, v11, v2

    if-eqz v4, :cond_9

    return-wide v11

    :cond_9
    const/4 v2, 0x3

    iput v2, v0, Lf/h/a/c/a1/d0/a;->e:I

    :cond_a
    iget-object v2, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lf/h/a/c/a1/d0/e;->a(Lf/h/a/c/a1/e;Z)Z

    :goto_4
    iget-object v2, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    iget-wide v3, v2, Lf/h/a/c/a1/d0/e;->c:J

    iget-wide v5, v0, Lf/h/a/c/a1/d0/a;->h:J

    cmp-long v7, v3, v5

    if-gtz v7, :cond_b

    iget v3, v2, Lf/h/a/c/a1/d0/e;->e:I

    iget v2, v2, Lf/h/a/c/a1/d0/e;->f:I

    add-int/2addr v3, v2

    invoke-virtual {v1, v3}, Lf/h/a/c/a1/e;->i(I)V

    iget-wide v2, v1, Lf/h/a/c/a1/e;->d:J

    iput-wide v2, v0, Lf/h/a/c/a1/d0/a;->i:J

    iget-object v2, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    iget-wide v3, v2, Lf/h/a/c/a1/d0/e;->c:J

    iput-wide v3, v0, Lf/h/a/c/a1/d0/a;->k:J

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lf/h/a/c/a1/d0/e;->a(Lf/h/a/c/a1/e;Z)Z

    goto :goto_4

    :cond_b
    const/4 v3, 0x0

    iput v3, v1, Lf/h/a/c/a1/e;->f:I

    const/4 v1, 0x4

    iput v1, v0, Lf/h/a/c/a1/d0/a;->e:I

    iget-wide v1, v0, Lf/h/a/c/a1/d0/a;->k:J

    const-wide/16 v3, 0x2

    add-long/2addr v1, v3

    neg-long v1, v1

    return-wide v1

    :cond_c
    iget-wide v4, v1, Lf/h/a/c/a1/e;->d:J

    iput-wide v4, v0, Lf/h/a/c/a1/d0/a;->g:J

    iput v3, v0, Lf/h/a/c/a1/d0/a;->e:I

    iget-wide v2, v0, Lf/h/a/c/a1/d0/a;->c:J

    const-wide/32 v6, 0xff1b

    sub-long/2addr v2, v6

    cmp-long v6, v2, v4

    if-lez v6, :cond_d

    return-wide v2

    :cond_d
    iget-wide v2, v0, Lf/h/a/c/a1/d0/a;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lf/h/a/c/a1/d0/a;->d(Lf/h/a/c/a1/e;J)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    invoke-virtual {v2}, Lf/h/a/c/a1/d0/e;->b()V

    :goto_5
    iget-object v2, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    iget v3, v2, Lf/h/a/c/a1/d0/e;->b:I

    const/4 v4, 0x4

    and-int/2addr v3, v4

    if-eq v3, v4, :cond_e

    iget-wide v3, v1, Lf/h/a/c/a1/e;->d:J

    iget-wide v5, v0, Lf/h/a/c/a1/d0/a;->c:J

    cmp-long v7, v3, v5

    if-gez v7, :cond_e

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lf/h/a/c/a1/d0/e;->a(Lf/h/a/c/a1/e;Z)Z

    iget-object v2, v0, Lf/h/a/c/a1/d0/a;->a:Lf/h/a/c/a1/d0/e;

    iget v4, v2, Lf/h/a/c/a1/d0/e;->e:I

    iget v2, v2, Lf/h/a/c/a1/d0/e;->f:I

    add-int/2addr v4, v2

    invoke-virtual {v1, v4}, Lf/h/a/c/a1/e;->i(I)V

    goto :goto_5

    :cond_e
    iget-wide v1, v2, Lf/h/a/c/a1/d0/e;->c:J

    iput-wide v1, v0, Lf/h/a/c/a1/d0/a;->f:J

    const/4 v1, 0x4

    iput v1, v0, Lf/h/a/c/a1/d0/a;->e:I

    iget-wide v1, v0, Lf/h/a/c/a1/d0/a;->g:J

    return-wide v1

    :cond_f
    new-instance v1, Ljava/io/EOFException;

    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    throw v1
.end method

.method public b()Lf/h/a/c/a1/q;
    .locals 6

    iget-wide v0, p0, Lf/h/a/c/a1/d0/a;->f:J

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-eqz v5, :cond_0

    new-instance v0, Lf/h/a/c/a1/d0/a$b;

    invoke-direct {v0, p0, v2}, Lf/h/a/c/a1/d0/a$b;-><init>(Lf/h/a/c/a1/d0/a;Lf/h/a/c/a1/d0/a$a;)V

    move-object v2, v0

    :cond_0
    return-object v2
.end method

.method public c(J)V
    .locals 10

    iget-wide v0, p0, Lf/h/a/c/a1/d0/a;->f:J

    const-wide/16 v2, 0x1

    sub-long v8, v0, v2

    const-wide/16 v6, 0x0

    move-wide v4, p1

    invoke-static/range {v4 .. v9}, Lf/h/a/c/i1/a0;->g(JJJ)J

    move-result-wide p1

    iput-wide p1, p0, Lf/h/a/c/a1/d0/a;->h:J

    const/4 p1, 0x2

    iput p1, p0, Lf/h/a/c/a1/d0/a;->e:I

    iget-wide p1, p0, Lf/h/a/c/a1/d0/a;->b:J

    iput-wide p1, p0, Lf/h/a/c/a1/d0/a;->i:J

    iget-wide p1, p0, Lf/h/a/c/a1/d0/a;->c:J

    iput-wide p1, p0, Lf/h/a/c/a1/d0/a;->j:J

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lf/h/a/c/a1/d0/a;->k:J

    iget-wide p1, p0, Lf/h/a/c/a1/d0/a;->f:J

    iput-wide p1, p0, Lf/h/a/c/a1/d0/a;->l:J

    return-void
.end method

.method public final d(Lf/h/a/c/a1/e;J)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-wide/16 v0, 0x3

    add-long/2addr p2, v0

    iget-wide v0, p0, Lf/h/a/c/a1/d0/a;->c:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    const/16 v0, 0x800

    new-array v1, v0, [B

    :goto_0
    iget-wide v2, p1, Lf/h/a/c/a1/e;->d:J

    int-to-long v4, v0

    add-long/2addr v4, v2

    const/4 v6, 0x0

    cmp-long v7, v4, p2

    if-lez v7, :cond_0

    sub-long v2, p2, v2

    long-to-int v0, v2

    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    return v6

    :cond_0
    invoke-virtual {p1, v1, v6, v0, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    :goto_1
    add-int/lit8 v2, v0, -0x3

    if-ge v6, v2, :cond_2

    aget-byte v2, v1, v6

    const/16 v3, 0x4f

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v6, 0x1

    aget-byte v2, v1, v2

    const/16 v3, 0x67

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v6, 0x2

    aget-byte v2, v1, v2

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v6, 0x3

    aget-byte v2, v1, v2

    const/16 v3, 0x53

    if-ne v2, v3, :cond_1

    invoke-virtual {p1, v6}, Lf/h/a/c/a1/e;->i(I)V

    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v2}, Lf/h/a/c/a1/e;->i(I)V

    goto :goto_0
.end method
