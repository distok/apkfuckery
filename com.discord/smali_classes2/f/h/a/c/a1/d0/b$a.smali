.class public Lf/h/a/c/a1/d0/b$a;
.super Ljava/lang/Object;
.source "FlacReader.java"

# interfaces
.implements Lf/h/a/c/a1/d0/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/d0/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public a:J

.field public b:J

.field public final synthetic c:Lf/h/a/c/a1/d0/b;


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/d0/b;)V
    .locals 2

    iput-object p1, p0, Lf/h/a/c/a1/d0/b$a;->c:Lf/h/a/c/a1/d0/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/h/a/c/a1/d0/b$a;->a:J

    iput-wide v0, p0, Lf/h/a/c/a1/d0/b$a;->b:J

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-wide v0, p0, Lf/h/a/c/a1/d0/b$a;->b:J

    const-wide/16 v2, -0x1

    const-wide/16 v4, 0x0

    cmp-long p1, v0, v4

    if-ltz p1, :cond_0

    const-wide/16 v4, 0x2

    add-long/2addr v0, v4

    neg-long v0, v0

    iput-wide v2, p0, Lf/h/a/c/a1/d0/b$a;->b:J

    return-wide v0

    :cond_0
    return-wide v2
.end method

.method public b()Lf/h/a/c/a1/q;
    .locals 5

    iget-wide v0, p0, Lf/h/a/c/a1/d0/b$a;->a:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    new-instance v0, Lf/h/a/c/a1/l;

    iget-object v1, p0, Lf/h/a/c/a1/d0/b$a;->c:Lf/h/a/c/a1/d0/b;

    iget-object v1, v1, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    iget-wide v2, p0, Lf/h/a/c/a1/d0/b$a;->a:J

    invoke-direct {v0, v1, v2, v3}, Lf/h/a/c/a1/l;-><init>(Lf/h/a/c/i1/l;J)V

    return-object v0
.end method

.method public c(J)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/d0/b$a;->c:Lf/h/a/c/a1/d0/b;

    iget-object v0, v0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    iget-object v0, v0, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/c/a1/d0/b$a;->c:Lf/h/a/c/a1/d0/b;

    iget-object v0, v0, Lf/h/a/c/a1/d0/b;->n:Lf/h/a/c/i1/l;

    iget-object v0, v0, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    iget-object v0, v0, Lf/h/a/c/i1/l$a;->a:[J

    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1, v1}, Lf/h/a/c/i1/a0;->c([JJZZ)I

    move-result p1

    aget-wide p1, v0, p1

    iput-wide p1, p0, Lf/h/a/c/a1/d0/b$a;->b:J

    return-void
.end method
