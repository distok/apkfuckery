.class public Lf/h/a/c/a1/d0/c;
.super Ljava/lang/Object;
.source "OggExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# instance fields
.field public a:Lf/h/a/c/a1/i;

.field public b:Lf/h/a/c/a1/d0/h;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/c/a1/e;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    new-instance v0, Lf/h/a/c/a1/d0/e;

    invoke-direct {v0}, Lf/h/a/c/a1/d0/e;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lf/h/a/c/a1/d0/e;->a(Lf/h/a/c/a1/e;Z)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_5

    iget v2, v0, Lf/h/a/c/a1/d0/e;->b:I

    const/4 v4, 0x2

    and-int/2addr v2, v4

    if-eq v2, v4, :cond_0

    goto/16 :goto_4

    :cond_0
    iget v0, v0, Lf/h/a/c/a1/d0/e;->f:I

    const/16 v2, 0x8

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v2, Lf/h/a/c/i1/r;

    invoke-direct {v2, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iget-object v4, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v4, v3, v0, v3}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->a()I

    move-result p1

    const/4 v0, 0x5

    if-lt p1, v0, :cond_1

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result p1

    const/16 v0, 0x7f

    if-ne p1, v0, :cond_1

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v4

    const-wide/32 v6, 0x464c4143

    cmp-long p1, v4, v6

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    new-instance p1, Lf/h/a/c/a1/d0/b;

    invoke-direct {p1}, Lf/h/a/c/a1/d0/b;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/d0/c;->b:Lf/h/a/c/a1/d0/h;

    goto :goto_3

    :cond_2
    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    :try_start_0
    invoke-static {v1, v2, v1}, Lf/g/j/k/a;->S0(ILf/h/a/c/i1/r;Z)Z

    move-result p1
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    new-instance p1, Lf/h/a/c/a1/d0/i;

    invoke-direct {p1}, Lf/h/a/c/a1/d0/i;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/d0/c;->b:Lf/h/a/c/a1/d0/h;

    goto :goto_3

    :cond_3
    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->a()I

    move-result p1

    sget-object v0, Lf/h/a/c/a1/d0/g;->o:[B

    array-length v4, v0

    if-ge p1, v4, :cond_4

    const/4 p1, 0x0

    goto :goto_2

    :cond_4
    array-length p1, v0

    new-array p1, p1, [B

    array-length v4, v0

    iget-object v5, v2, Lf/h/a/c/i1/r;->a:[B

    iget v6, v2, Lf/h/a/c/i1/r;->b:I

    invoke-static {v5, v6, p1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v5, v2, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v5, v4

    iput v5, v2, Lf/h/a/c/i1/r;->b:I

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    :goto_2
    if-eqz p1, :cond_5

    new-instance p1, Lf/h/a/c/a1/d0/g;

    invoke-direct {p1}, Lf/h/a/c/a1/d0/g;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/d0/c;->b:Lf/h/a/c/a1/d0/h;

    :goto_3
    return v1

    :cond_5
    :goto_4
    return v3
.end method

.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lf/h/a/c/a1/d0/c;->b:Lf/h/a/c/a1/d0/h;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    invoke-virtual/range {p0 .. p1}, Lf/h/a/c/a1/d0/c;->a(Lf/h/a/c/a1/e;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput v3, v1, Lf/h/a/c/a1/e;->f:I

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Failed to determine bitstream type"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    iget-boolean v2, v0, Lf/h/a/c/a1/d0/c;->c:Z

    const/4 v4, 0x1

    if-nez v2, :cond_2

    iget-object v2, v0, Lf/h/a/c/a1/d0/c;->a:Lf/h/a/c/a1/i;

    invoke-interface {v2, v3, v4}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v2

    iget-object v5, v0, Lf/h/a/c/a1/d0/c;->a:Lf/h/a/c/a1/i;

    invoke-interface {v5}, Lf/h/a/c/a1/i;->k()V

    iget-object v5, v0, Lf/h/a/c/a1/d0/c;->b:Lf/h/a/c/a1/d0/h;

    iget-object v6, v0, Lf/h/a/c/a1/d0/c;->a:Lf/h/a/c/a1/i;

    iput-object v6, v5, Lf/h/a/c/a1/d0/h;->c:Lf/h/a/c/a1/i;

    iput-object v2, v5, Lf/h/a/c/a1/d0/h;->b:Lf/h/a/c/a1/s;

    invoke-virtual {v5, v4}, Lf/h/a/c/a1/d0/h;->e(Z)V

    iput-boolean v4, v0, Lf/h/a/c/a1/d0/c;->c:Z

    :cond_2
    iget-object v2, v0, Lf/h/a/c/a1/d0/c;->b:Lf/h/a/c/a1/d0/h;

    iget v5, v2, Lf/h/a/c/a1/d0/h;->h:I

    const-wide/16 v6, -0x1

    const/4 v8, -0x1

    const/4 v9, 0x3

    const/4 v15, 0x2

    if-eqz v5, :cond_b

    if-eq v5, v4, :cond_a

    if-ne v5, v15, :cond_9

    iget-object v5, v2, Lf/h/a/c/a1/d0/h;->d:Lf/h/a/c/a1/d0/f;

    invoke-interface {v5, v1}, Lf/h/a/c/a1/d0/f;->a(Lf/h/a/c/a1/e;)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v5, v10, v12

    if-ltz v5, :cond_3

    move-object/from16 v5, p2

    iput-wide v10, v5, Lf/h/a/c/a1/p;->a:J

    const/4 v3, 0x1

    goto/16 :goto_7

    :cond_3
    cmp-long v5, v10, v6

    if-gez v5, :cond_4

    const-wide/16 v14, 0x2

    add-long/2addr v10, v14

    neg-long v10, v10

    invoke-virtual {v2, v10, v11}, Lf/h/a/c/a1/d0/h;->b(J)V

    :cond_4
    iget-boolean v5, v2, Lf/h/a/c/a1/d0/h;->l:Z

    if-nez v5, :cond_5

    iget-object v5, v2, Lf/h/a/c/a1/d0/h;->d:Lf/h/a/c/a1/d0/f;

    invoke-interface {v5}, Lf/h/a/c/a1/d0/f;->b()Lf/h/a/c/a1/q;

    move-result-object v5

    iget-object v10, v2, Lf/h/a/c/a1/d0/h;->c:Lf/h/a/c/a1/i;

    invoke-interface {v10, v5}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    iput-boolean v4, v2, Lf/h/a/c/a1/d0/h;->l:Z

    :cond_5
    iget-wide v4, v2, Lf/h/a/c/a1/d0/h;->k:J

    cmp-long v10, v4, v12

    if-gtz v10, :cond_7

    iget-object v4, v2, Lf/h/a/c/a1/d0/h;->a:Lf/h/a/c/a1/d0/d;

    invoke-virtual {v4, v1}, Lf/h/a/c/a1/d0/d;->b(Lf/h/a/c/a1/e;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_2

    :cond_6
    iput v9, v2, Lf/h/a/c/a1/d0/h;->h:I

    :goto_1
    const/4 v3, -0x1

    goto/16 :goto_7

    :cond_7
    :goto_2
    iput-wide v12, v2, Lf/h/a/c/a1/d0/h;->k:J

    iget-object v1, v2, Lf/h/a/c/a1/d0/h;->a:Lf/h/a/c/a1/d0/d;

    iget-object v1, v1, Lf/h/a/c/a1/d0/d;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v1}, Lf/h/a/c/a1/d0/h;->c(Lf/h/a/c/i1/r;)J

    move-result-wide v4

    cmp-long v8, v4, v12

    if-ltz v8, :cond_8

    iget-wide v8, v2, Lf/h/a/c/a1/d0/h;->g:J

    add-long v10, v8, v4

    iget-wide v12, v2, Lf/h/a/c/a1/d0/h;->e:J

    cmp-long v14, v10, v12

    if-ltz v14, :cond_8

    const-wide/32 v10, 0xf4240

    mul-long v8, v8, v10

    iget v10, v2, Lf/h/a/c/a1/d0/h;->i:I

    int-to-long v10, v10

    div-long v13, v8, v10

    iget-object v8, v2, Lf/h/a/c/a1/d0/h;->b:Lf/h/a/c/a1/s;

    iget v9, v1, Lf/h/a/c/i1/r;->c:I

    invoke-interface {v8, v1, v9}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget-object v12, v2, Lf/h/a/c/a1/d0/h;->b:Lf/h/a/c/a1/s;

    const/4 v15, 0x1

    iget v1, v1, Lf/h/a/c/i1/r;->c:I

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v16, v1

    invoke-interface/range {v12 .. v18}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iput-wide v6, v2, Lf/h/a/c/a1/d0/h;->e:J

    :cond_8
    iget-wide v6, v2, Lf/h/a/c/a1/d0/h;->g:J

    add-long/2addr v6, v4

    iput-wide v6, v2, Lf/h/a/c/a1/d0/h;->g:J

    goto/16 :goto_7

    :cond_9
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_a
    iget-wide v4, v2, Lf/h/a/c/a1/d0/h;->f:J

    long-to-int v5, v4

    invoke-virtual {v1, v5}, Lf/h/a/c/a1/e;->i(I)V

    iput v15, v2, Lf/h/a/c/a1/d0/h;->h:I

    goto/16 :goto_7

    :cond_b
    const/4 v5, 0x1

    :cond_c
    :goto_3
    if-eqz v5, :cond_e

    iget-object v5, v2, Lf/h/a/c/a1/d0/h;->a:Lf/h/a/c/a1/d0/d;

    invoke-virtual {v5, v1}, Lf/h/a/c/a1/d0/d;->b(Lf/h/a/c/a1/e;)Z

    move-result v5

    if-nez v5, :cond_d

    iput v9, v2, Lf/h/a/c/a1/d0/h;->h:I

    goto :goto_1

    :cond_d
    iget-wide v10, v1, Lf/h/a/c/a1/e;->d:J

    iget-wide v12, v2, Lf/h/a/c/a1/d0/h;->f:J

    sub-long/2addr v10, v12

    iput-wide v10, v2, Lf/h/a/c/a1/d0/h;->k:J

    iget-object v5, v2, Lf/h/a/c/a1/d0/h;->a:Lf/h/a/c/a1/d0/d;

    iget-object v5, v5, Lf/h/a/c/a1/d0/d;->b:Lf/h/a/c/i1/r;

    iget-object v10, v2, Lf/h/a/c/a1/d0/h;->j:Lf/h/a/c/a1/d0/h$b;

    invoke-virtual {v2, v5, v12, v13, v10}, Lf/h/a/c/a1/d0/h;->d(Lf/h/a/c/i1/r;JLf/h/a/c/a1/d0/h$b;)Z

    move-result v5

    if-eqz v5, :cond_c

    iget-wide v10, v1, Lf/h/a/c/a1/e;->d:J

    iput-wide v10, v2, Lf/h/a/c/a1/d0/h;->f:J

    goto :goto_3

    :cond_e
    iget-object v5, v2, Lf/h/a/c/a1/d0/h;->j:Lf/h/a/c/a1/d0/h$b;

    iget-object v5, v5, Lf/h/a/c/a1/d0/h$b;->a:Lcom/google/android/exoplayer2/Format;

    iget v8, v5, Lcom/google/android/exoplayer2/Format;->z:I

    iput v8, v2, Lf/h/a/c/a1/d0/h;->i:I

    iget-boolean v8, v2, Lf/h/a/c/a1/d0/h;->m:Z

    if-nez v8, :cond_f

    iget-object v8, v2, Lf/h/a/c/a1/d0/h;->b:Lf/h/a/c/a1/s;

    invoke-interface {v8, v5}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    iput-boolean v4, v2, Lf/h/a/c/a1/d0/h;->m:Z

    :cond_f
    iget-object v5, v2, Lf/h/a/c/a1/d0/h;->j:Lf/h/a/c/a1/d0/h$b;

    iget-object v5, v5, Lf/h/a/c/a1/d0/h$b;->b:Lf/h/a/c/a1/d0/f;

    const/4 v13, 0x0

    if-eqz v5, :cond_10

    iput-object v5, v2, Lf/h/a/c/a1/d0/h;->d:Lf/h/a/c/a1/d0/f;

    :goto_4
    move-object v1, v13

    const/4 v5, 0x2

    goto :goto_6

    :cond_10
    iget-wide v11, v1, Lf/h/a/c/a1/e;->c:J

    cmp-long v1, v11, v6

    if-nez v1, :cond_11

    new-instance v1, Lf/h/a/c/a1/d0/h$c;

    invoke-direct {v1, v13}, Lf/h/a/c/a1/d0/h$c;-><init>(Lf/h/a/c/a1/d0/h$a;)V

    iput-object v1, v2, Lf/h/a/c/a1/d0/h;->d:Lf/h/a/c/a1/d0/f;

    goto :goto_4

    :cond_11
    iget-object v1, v2, Lf/h/a/c/a1/d0/h;->a:Lf/h/a/c/a1/d0/d;

    iget-object v1, v1, Lf/h/a/c/a1/d0/d;->a:Lf/h/a/c/a1/d0/e;

    iget v5, v1, Lf/h/a/c/a1/d0/e;->b:I

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_12

    const/16 v17, 0x1

    goto :goto_5

    :cond_12
    const/16 v17, 0x0

    :goto_5
    new-instance v4, Lf/h/a/c/a1/d0/a;

    iget-wide v9, v2, Lf/h/a/c/a1/d0/h;->f:J

    iget v5, v1, Lf/h/a/c/a1/d0/e;->e:I

    iget v6, v1, Lf/h/a/c/a1/d0/e;->f:I

    add-int/2addr v5, v6

    int-to-long v5, v5

    iget-wide v7, v1, Lf/h/a/c/a1/d0/e;->c:J

    move-wide/from16 v18, v7

    move-object v7, v4

    move-object v8, v2

    move-object v1, v13

    move-wide v13, v5

    const/4 v5, 0x2

    move-wide/from16 v15, v18

    invoke-direct/range {v7 .. v17}, Lf/h/a/c/a1/d0/a;-><init>(Lf/h/a/c/a1/d0/h;JJJJZ)V

    iput-object v4, v2, Lf/h/a/c/a1/d0/h;->d:Lf/h/a/c/a1/d0/f;

    :goto_6
    iput-object v1, v2, Lf/h/a/c/a1/d0/h;->j:Lf/h/a/c/a1/d0/h$b;

    iput v5, v2, Lf/h/a/c/a1/d0/h;->h:I

    iget-object v1, v2, Lf/h/a/c/a1/d0/h;->a:Lf/h/a/c/a1/d0/d;

    iget-object v1, v1, Lf/h/a/c/a1/d0/d;->b:Lf/h/a/c/i1/r;

    iget-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    array-length v4, v2

    const v5, 0xfe01

    if-ne v4, v5, :cond_13

    goto :goto_7

    :cond_13
    iget v4, v1, Lf/h/a/c/i1/r;->c:I

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v2, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    :goto_7
    return v3
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/d0/c;->a:Lf/h/a/c/a1/i;

    return-void
.end method

.method public f(JJ)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/a1/d0/c;->b:Lf/h/a/c/a1/d0/h;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lf/h/a/c/a1/d0/h;->a:Lf/h/a/c/a1/d0/d;

    iget-object v2, v1, Lf/h/a/c/a1/d0/d;->a:Lf/h/a/c/a1/d0/e;

    invoke-virtual {v2}, Lf/h/a/c/a1/d0/e;->b()V

    iget-object v2, v1, Lf/h/a/c/a1/d0/d;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->x()V

    const/4 v2, -0x1

    iput v2, v1, Lf/h/a/c/a1/d0/d;->c:I

    const/4 v2, 0x0

    iput-boolean v2, v1, Lf/h/a/c/a1/d0/d;->e:Z

    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    iget-boolean p1, v0, Lf/h/a/c/a1/d0/h;->l:Z

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lf/h/a/c/a1/d0/h;->e(Z)V

    goto :goto_0

    :cond_0
    iget p1, v0, Lf/h/a/c/a1/d0/h;->h:I

    if-eqz p1, :cond_1

    iget p1, v0, Lf/h/a/c/a1/d0/h;->i:I

    int-to-long p1, p1

    mul-long p1, p1, p3

    const-wide/32 p3, 0xf4240

    div-long/2addr p1, p3

    iput-wide p1, v0, Lf/h/a/c/a1/d0/h;->e:J

    iget-object p3, v0, Lf/h/a/c/a1/d0/h;->d:Lf/h/a/c/a1/d0/f;

    invoke-interface {p3, p1, p2}, Lf/h/a/c/a1/d0/f;->c(J)V

    const/4 p1, 0x2

    iput p1, v0, Lf/h/a/c/a1/d0/h;->h:I

    :cond_1
    :goto_0
    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lf/h/a/c/a1/d0/c;->a(Lf/h/a/c/a1/e;)Z

    move-result p1
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method public release()V
    .locals 0

    return-void
.end method
