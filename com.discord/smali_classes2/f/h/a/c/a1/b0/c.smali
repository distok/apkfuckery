.class public final Lf/h/a/c/a1/b0/c;
.super Ljava/lang/Object;
.source "MlltSeeker.java"

# interfaces
.implements Lf/h/a/c/a1/b0/e;


# instance fields
.field public final a:[J

.field public final b:[J

.field public final c:J


# direct methods
.method public constructor <init>([J[J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/b0/c;->a:[J

    iput-object p2, p0, Lf/h/a/c/a1/b0/c;->b:[J

    array-length p1, p2

    add-int/lit8 p1, p1, -0x1

    aget-wide p1, p2, p1

    invoke-static {p1, p2}, Lf/h/a/c/u;->a(J)J

    move-result-wide p1

    iput-wide p1, p0, Lf/h/a/c/a1/b0/c;->c:J

    return-void
.end method

.method public static d(J[J[J)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J[J[J)",
            "Landroid/util/Pair<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-static {p2, p0, p1, v0, v0}, Lf/h/a/c/i1/a0;->c([JJZZ)I

    move-result v1

    aget-wide v2, p2, v1

    aget-wide v4, p3, v1

    add-int/2addr v1, v0

    array-length v0, p2

    if-ne v1, v0, :cond_0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    return-object p0

    :cond_0
    aget-wide v6, p2, v1

    aget-wide p2, p3, v1

    cmp-long v0, v6, v2

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    long-to-double v0, p0

    long-to-double v8, v2

    sub-double/2addr v0, v8

    sub-long/2addr v6, v2

    long-to-double v2, v6

    div-double/2addr v0, v2

    :goto_0
    sub-long/2addr p2, v4

    long-to-double p2, p2

    mul-double v0, v0, p2

    double-to-long p2, v0

    add-long/2addr p2, v4

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(J)J
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/b0/c;->a:[J

    iget-object v1, p0, Lf/h/a/c/a1/b0/c;->b:[J

    invoke-static {p1, p2, v0, v1}, Lf/h/a/c/a1/b0/c;->d(J[J[J)Landroid/util/Pair;

    move-result-object p1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    invoke-static {p1, p2}, Lf/h/a/c/u;->a(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public g(J)Lf/h/a/c/a1/q$a;
    .locals 6

    iget-wide v4, p0, Lf/h/a/c/a1/b0/c;->c:J

    const-wide/16 v2, 0x0

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lf/h/a/c/i1/a0;->g(JJJ)J

    move-result-wide p1

    invoke-static {p1, p2}, Lf/h/a/c/u;->b(J)J

    move-result-wide p1

    iget-object v0, p0, Lf/h/a/c/a1/b0/c;->b:[J

    iget-object v1, p0, Lf/h/a/c/a1/b0/c;->a:[J

    invoke-static {p1, p2, v0, v1}, Lf/h/a/c/a1/b0/c;->d(J[J[J)Landroid/util/Pair;

    move-result-object p1

    iget-object p2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lf/h/a/c/u;->a(J)J

    move-result-wide v0

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    new-instance v2, Lf/h/a/c/a1/q$a;

    new-instance v3, Lf/h/a/c/a1/r;

    invoke-direct {v3, v0, v1, p1, p2}, Lf/h/a/c/a1/r;-><init>(JJ)V

    invoke-direct {v2, v3}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;)V

    return-object v2
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, Lf/h/a/c/a1/b0/c;->c:J

    return-wide v0
.end method
