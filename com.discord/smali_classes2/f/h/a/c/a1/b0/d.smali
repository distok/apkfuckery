.class public final Lf/h/a/c/a1/b0/d;
.super Ljava/lang/Object;
.source "Mp3Extractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# static fields
.field public static final synthetic q:I


# instance fields
.field public final a:I

.field public final b:J

.field public final c:Lf/h/a/c/i1/r;

.field public final d:Lf/h/a/c/a1/o;

.field public final e:Lf/h/a/c/a1/m;

.field public final f:Lf/h/a/c/a1/n;

.field public g:Lf/h/a/c/a1/i;

.field public h:Lf/h/a/c/a1/s;

.field public i:I

.field public j:Lcom/google/android/exoplayer2/metadata/Metadata;

.field public k:Lf/h/a/c/a1/b0/e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:J

.field public n:J

.field public o:J

.field public p:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/h/a/c/a1/b0/d;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/h/a/c/a1/b0/d;->a:I

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lf/h/a/c/a1/b0/d;->b:J

    new-instance p1, Lf/h/a/c/i1/r;

    const/16 v2, 0xa

    invoke-direct {p1, v2}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/a1/o;

    invoke-direct {p1}, Lf/h/a/c/a1/o;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    new-instance p1, Lf/h/a/c/a1/m;

    invoke-direct {p1}, Lf/h/a/c/a1/m;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/b0/d;->e:Lf/h/a/c/a1/m;

    iput-wide v0, p0, Lf/h/a/c/a1/b0/d;->m:J

    new-instance p1, Lf/h/a/c/a1/n;

    invoke-direct {p1}, Lf/h/a/c/a1/n;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/b0/d;->f:Lf/h/a/c/a1/n;

    return-void
.end method

.method public static b(IJ)Z
    .locals 4

    const v0, -0x1f400

    and-int/2addr p0, v0

    int-to-long v0, p0

    const-wide/32 v2, -0x1f400

    and-long p0, p1, v2

    cmp-long p2, v0, p0

    if-nez p2, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public final a(Lf/h/a/c/a1/e;)Lf/h/a/c/a1/b0/e;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {p1, v0, v1, v2, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v0, p0, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v0, p0, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v0

    iget-object v1, p0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    invoke-static {v0, v1}, Lf/h/a/c/a1/o;->d(ILf/h/a/c/a1/o;)Z

    new-instance v0, Lf/h/a/c/a1/b0/b;

    iget-wide v3, p1, Lf/h/a/c/a1/e;->c:J

    iget-wide v5, p1, Lf/h/a/c/a1/e;->d:J

    iget-object v7, p0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lf/h/a/c/a1/b0/b;-><init>(JJLf/h/a/c/a1/o;)V

    return-object v0
.end method

.method public final c(Lf/h/a/c/a1/e;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/h/a/c/a1/b0/e;->a()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    sub-long/2addr v2, v6

    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-virtual {p1, v0, v2, v3, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    xor-int/2addr p1, v1

    return p1

    :catch_0
    return v1
.end method

.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 31
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget v2, v0, Lf/h/a/c/a1/b0/d;->i:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    :try_start_0
    invoke-virtual {v0, v1, v4}, Lf/h/a/c/a1/b0/d;->g(Lf/h/a/c/a1/e;Z)Z
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    return v3

    :cond_0
    :goto_0
    iget-object v2, v0, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    const-wide/32 v5, 0xf4240

    const/4 v3, 0x1

    if-nez v2, :cond_24

    new-instance v2, Lf/h/a/c/i1/r;

    iget-object v7, v0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v7, v7, Lf/h/a/c/a1/o;->c:I

    invoke-direct {v2, v7}, Lf/h/a/c/i1/r;-><init>(I)V

    iget-object v7, v2, Lf/h/a/c/i1/r;->a:[B

    iget-object v8, v0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v8, v8, Lf/h/a/c/a1/o;->c:I

    invoke-virtual {v1, v7, v4, v8, v4}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v4, v0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v7, v4, Lf/h/a/c/a1/o;->a:I

    and-int/2addr v7, v3

    const/16 v8, 0x15

    iget v4, v4, Lf/h/a/c/a1/o;->e:I

    const/16 v9, 0x24

    if-eqz v7, :cond_1

    if-eq v4, v3, :cond_3

    const/16 v8, 0x24

    goto :goto_1

    :cond_1
    if-eq v4, v3, :cond_2

    goto :goto_1

    :cond_2
    const/16 v8, 0xd

    :cond_3
    :goto_1
    iget v3, v2, Lf/h/a/c/i1/r;->c:I

    add-int/lit8 v4, v8, 0x4

    const v7, 0x58696e67

    const v10, 0x496e666f

    const v11, 0x56425249

    if-lt v3, v4, :cond_4

    invoke-virtual {v2, v8}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    if-eq v3, v7, :cond_6

    if-ne v3, v10, :cond_4

    goto :goto_2

    :cond_4
    iget v3, v2, Lf/h/a/c/i1/r;->c:I

    const/16 v4, 0x28

    if-lt v3, v4, :cond_5

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    if-ne v3, v11, :cond_5

    const v3, 0x56425249

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    :cond_6
    :goto_2
    const-string v4, ", "

    if-eq v3, v7, :cond_11

    if-ne v3, v10, :cond_7

    goto/16 :goto_9

    :cond_7
    if-ne v3, v11, :cond_10

    iget-wide v7, v1, Lf/h/a/c/a1/e;->c:J

    iget-wide v10, v1, Lf/h/a/c/a1/e;->d:J

    iget-object v3, v0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    const/16 v15, 0xa

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v15

    if-gtz v15, :cond_8

    goto :goto_5

    :cond_8
    iget v14, v3, Lf/h/a/c/a1/o;->d:I

    int-to-long v12, v15

    const/16 v15, 0x7d00

    if-lt v14, v15, :cond_9

    const/16 v15, 0x480

    goto :goto_3

    :cond_9
    const/16 v15, 0x240

    :goto_3
    move-wide/from16 v22, v10

    int-to-long v9, v15

    mul-long v18, v9, v5

    int-to-long v5, v14

    move-wide/from16 v16, v12

    move-wide/from16 v20, v5

    invoke-static/range {v16 .. v21}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v27

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->v()I

    move-result v5

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->v()I

    move-result v6

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->v()I

    move-result v9

    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Lf/h/a/c/i1/r;->D(I)V

    iget v3, v3, Lf/h/a/c/a1/o;->c:I

    int-to-long v10, v3

    add-long v10, v10, v22

    new-array v3, v5, [J

    new-array v12, v5, [J

    const/4 v13, 0x0

    move-wide/from16 v14, v22

    :goto_4
    if-ge v13, v5, :cond_e

    int-to-long v0, v13

    mul-long v0, v0, v27

    move-wide/from16 v16, v7

    int-to-long v7, v5

    div-long/2addr v0, v7

    aput-wide v0, v3, v13

    invoke-static {v14, v15, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    aput-wide v0, v12, v13

    const/4 v0, 0x1

    if-eq v9, v0, :cond_d

    const/4 v0, 0x2

    if-eq v9, v0, :cond_c

    const/4 v0, 0x3

    if-eq v9, v0, :cond_b

    const/4 v0, 0x4

    if-eq v9, v0, :cond_a

    :goto_5
    const/4 v0, 0x0

    :goto_6
    move-object/from16 v1, p0

    goto :goto_8

    :cond_a
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->t()I

    move-result v0

    goto :goto_7

    :cond_b
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->s()I

    move-result v0

    goto :goto_7

    :cond_c
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->v()I

    move-result v0

    goto :goto_7

    :cond_d
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v0

    :goto_7
    mul-int v0, v0, v6

    int-to-long v0, v0

    add-long/2addr v14, v0

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v7, v16

    goto :goto_4

    :cond_e
    move-wide/from16 v16, v7

    const-wide/16 v0, -0x1

    cmp-long v2, v16, v0

    if-eqz v2, :cond_f

    cmp-long v0, v16, v14

    if-eqz v0, :cond_f

    const-string v0, "VBRI data size mismatch: "

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2, v4}, Lf/e/c/a/a;->K(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VbriSeeker"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    new-instance v0, Lf/h/a/c/a1/b0/f;

    move-object/from16 v24, v0

    move-object/from16 v25, v3

    move-object/from16 v26, v12

    move-wide/from16 v29, v14

    invoke-direct/range {v24 .. v30}, Lf/h/a/c/a1/b0/f;-><init>([J[JJJ)V

    goto :goto_6

    :goto_8
    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v2, v2, Lf/h/a/c/a1/o;->c:I

    move-object/from16 v5, p1

    invoke-virtual {v5, v2}, Lf/h/a/c/a1/e;->i(I)V

    goto/16 :goto_e

    :cond_10
    move-object v5, v1

    move-object v1, v0

    const/4 v0, 0x0

    iput v0, v5, Lf/h/a/c/a1/e;->f:I

    const/4 v0, 0x0

    goto/16 :goto_e

    :cond_11
    :goto_9
    move-object v5, v1

    move-object v1, v0

    iget-wide v6, v5, Lf/h/a/c/a1/e;->c:J

    iget-wide v10, v5, Lf/h/a/c/a1/e;->d:J

    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v9, v0, Lf/h/a/c/a1/o;->g:I

    iget v12, v0, Lf/h/a/c/a1/o;->d:I

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v13

    and-int/lit8 v14, v13, 0x1

    const/4 v15, 0x1

    if-ne v14, v15, :cond_16

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->t()I

    move-result v14

    if-nez v14, :cond_12

    goto/16 :goto_b

    :cond_12
    int-to-long v14, v14

    move/from16 v21, v8

    int-to-long v8, v9

    const-wide/32 v16, 0xf4240

    mul-long v17, v8, v16

    int-to-long v8, v12

    move-wide v15, v14

    move-wide/from16 v19, v8

    invoke-static/range {v15 .. v20}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v14

    const/4 v8, 0x6

    and-int/lit8 v9, v13, 0x6

    if-eq v9, v8, :cond_13

    new-instance v2, Lf/h/a/c/a1/b0/g;

    iget v12, v0, Lf/h/a/c/a1/o;->c:I

    const-wide/16 v6, -0x1

    const/16 v17, 0x0

    move-object v9, v2

    move-wide v13, v14

    move-wide v15, v6

    invoke-direct/range {v9 .. v17}, Lf/h/a/c/a1/b0/g;-><init>(JIJJ[J)V

    move-object v0, v2

    move/from16 v18, v3

    goto :goto_c

    :cond_13
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->t()I

    move-result v8

    int-to-long v12, v8

    const/16 v8, 0x64

    new-array v9, v8, [J

    const/16 v16, 0x0

    move/from16 v18, v3

    const/4 v3, 0x0

    :goto_a
    if-ge v3, v8, :cond_14

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v8

    move-object/from16 v16, v2

    int-to-long v1, v8

    aput-wide v1, v9, v3

    add-int/lit8 v3, v3, 0x1

    const/16 v8, 0x64

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    goto :goto_a

    :cond_14
    const-wide/16 v1, -0x1

    cmp-long v3, v6, v1

    if-eqz v3, :cond_15

    add-long v1, v10, v12

    cmp-long v3, v6, v1

    if-eqz v3, :cond_15

    const-string v3, "XING data size mismatch: "

    invoke-static {v3, v6, v7, v4}, Lf/e/c/a/a;->K(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "XingSeeker"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    new-instance v1, Lf/h/a/c/a1/b0/g;

    iget v0, v0, Lf/h/a/c/a1/o;->c:I

    move-object v2, v9

    move-object v9, v1

    move-wide v3, v12

    move v12, v0

    move-wide v13, v14

    move-wide v15, v3

    move-object/from16 v17, v2

    invoke-direct/range {v9 .. v17}, Lf/h/a/c/a1/b0/g;-><init>(JIJJ[J)V

    move-object v0, v1

    goto :goto_c

    :cond_16
    :goto_b
    move/from16 v18, v3

    move/from16 v21, v8

    const/4 v0, 0x0

    :goto_c
    move-object/from16 v1, p0

    if-eqz v0, :cond_19

    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->e:Lf/h/a/c/a1/m;

    iget v3, v2, Lf/h/a/c/a1/m;->a:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_17

    iget v2, v2, Lf/h/a/c/a1/m;->b:I

    if-eq v2, v4, :cond_17

    const/4 v2, 0x1

    goto :goto_d

    :cond_17
    const/4 v2, 0x0

    :goto_d
    if-nez v2, :cond_19

    const/4 v2, 0x0

    iput v2, v5, Lf/h/a/c/a1/e;->f:I

    move/from16 v8, v21

    add-int/lit16 v8, v8, 0x8d

    invoke-virtual {v5, v8, v2}, Lf/h/a/c/a1/e;->a(IZ)Z

    iget-object v3, v1, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    const/4 v4, 0x3

    invoke-virtual {v5, v3, v2, v4, v2}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v3, v1, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->e:Lf/h/a/c/a1/m;

    iget-object v3, v1, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->s()I

    move-result v3

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    shr-int/lit8 v4, v3, 0xc

    and-int/lit16 v3, v3, 0xfff

    if-gtz v4, :cond_18

    if-lez v3, :cond_19

    :cond_18
    iput v4, v2, Lf/h/a/c/a1/m;->a:I

    iput v3, v2, Lf/h/a/c/a1/m;->b:I

    :cond_19
    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v2, v2, Lf/h/a/c/a1/o;->c:I

    invoke-virtual {v5, v2}, Lf/h/a/c/a1/e;->i(I)V

    if-eqz v0, :cond_1a

    invoke-virtual {v0}, Lf/h/a/c/a1/b0/g;->b()Z

    move-result v2

    if-nez v2, :cond_1a

    const v2, 0x496e666f

    move/from16 v3, v18

    if-ne v3, v2, :cond_1a

    invoke-virtual/range {p0 .. p1}, Lf/h/a/c/a1/b0/d;->a(Lf/h/a/c/a1/e;)Lf/h/a/c/a1/b0/e;

    move-result-object v0

    :cond_1a
    :goto_e
    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->j:Lcom/google/android/exoplayer2/metadata/Metadata;

    iget-wide v3, v5, Lf/h/a/c/a1/e;->d:J

    if-eqz v2, :cond_1d

    iget-object v6, v2, Lcom/google/android/exoplayer2/metadata/Metadata;->d:[Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    array-length v6, v6

    const/4 v7, 0x0

    :goto_f
    if-ge v7, v6, :cond_1d

    iget-object v8, v2, Lcom/google/android/exoplayer2/metadata/Metadata;->d:[Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    aget-object v8, v8, v7

    instance-of v9, v8, Lcom/google/android/exoplayer2/metadata/id3/MlltFrame;

    if-eqz v9, :cond_1c

    check-cast v8, Lcom/google/android/exoplayer2/metadata/id3/MlltFrame;

    iget-object v2, v8, Lcom/google/android/exoplayer2/metadata/id3/MlltFrame;->h:[I

    array-length v2, v2

    add-int/lit8 v6, v2, 0x1

    new-array v7, v6, [J

    new-array v6, v6, [J

    const/4 v9, 0x0

    aput-wide v3, v7, v9

    const-wide/16 v10, 0x0

    aput-wide v10, v6, v9

    const-wide/16 v9, 0x0

    const/4 v11, 0x1

    :goto_10
    if-gt v11, v2, :cond_1b

    iget v12, v8, Lcom/google/android/exoplayer2/metadata/id3/MlltFrame;->f:I

    iget-object v13, v8, Lcom/google/android/exoplayer2/metadata/id3/MlltFrame;->h:[I

    add-int/lit8 v14, v11, -0x1

    aget v13, v13, v14

    add-int/2addr v12, v13

    int-to-long v12, v12

    add-long/2addr v3, v12

    iget v12, v8, Lcom/google/android/exoplayer2/metadata/id3/MlltFrame;->g:I

    iget-object v13, v8, Lcom/google/android/exoplayer2/metadata/id3/MlltFrame;->i:[I

    aget v13, v13, v14

    add-int/2addr v12, v13

    int-to-long v12, v12

    add-long/2addr v9, v12

    aput-wide v3, v7, v11

    aput-wide v9, v6, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_10

    :cond_1b
    new-instance v2, Lf/h/a/c/a1/b0/c;

    invoke-direct {v2, v7, v6}, Lf/h/a/c/a1/b0/c;-><init>([J[J)V

    goto :goto_11

    :cond_1c
    add-int/lit8 v7, v7, 0x1

    goto :goto_f

    :cond_1d
    const/4 v2, 0x0

    :goto_11
    iget-boolean v3, v1, Lf/h/a/c/a1/b0/d;->l:Z

    if-eqz v3, :cond_1e

    new-instance v0, Lf/h/a/c/a1/b0/e$a;

    invoke-direct {v0}, Lf/h/a/c/a1/b0/e$a;-><init>()V

    iput-object v0, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    goto :goto_13

    :cond_1e
    if-eqz v2, :cond_1f

    iput-object v2, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    goto :goto_12

    :cond_1f
    if-eqz v0, :cond_20

    iput-object v0, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    :cond_20
    :goto_12
    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    if-eqz v0, :cond_21

    invoke-interface {v0}, Lf/h/a/c/a1/q;->b()Z

    move-result v0

    if-nez v0, :cond_22

    iget v0, v1, Lf/h/a/c/a1/b0/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_22

    :cond_21
    invoke-virtual/range {p0 .. p1}, Lf/h/a/c/a1/b0/d;->a(Lf/h/a/c/a1/e;)Lf/h/a/c/a1/b0/e;

    move-result-object v0

    iput-object v0, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    :cond_22
    :goto_13
    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->g:Lf/h/a/c/a1/i;

    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    invoke-interface {v0, v2}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->h:Lf/h/a/c/a1/s;

    const/4 v6, 0x0

    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget-object v7, v2, Lf/h/a/c/a1/o;->b:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/16 v10, 0x1000

    iget v11, v2, Lf/h/a/c/a1/o;->e:I

    iget v12, v2, Lf/h/a/c/a1/o;->d:I

    const/4 v13, -0x1

    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->e:Lf/h/a/c/a1/m;

    iget v14, v2, Lf/h/a/c/a1/m;->a:I

    iget v15, v2, Lf/h/a/c/a1/m;->b:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    iget v2, v1, Lf/h/a/c/a1/b0/d;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_23

    const/16 v20, 0x0

    goto :goto_14

    :cond_23
    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->j:Lcom/google/android/exoplayer2/metadata/Metadata;

    move-object/from16 v20, v2

    :goto_14
    invoke-static/range {v6 .. v20}, Lcom/google/android/exoplayer2/Format;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format;

    move-result-object v2

    invoke-interface {v0, v2}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    iget-wide v2, v5, Lf/h/a/c/a1/e;->d:J

    iput-wide v2, v1, Lf/h/a/c/a1/b0/d;->o:J

    goto :goto_15

    :cond_24
    move-object v5, v1

    move-object v1, v0

    iget-wide v2, v1, Lf/h/a/c/a1/b0/d;->o:J

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-eqz v0, :cond_25

    iget-wide v6, v5, Lf/h/a/c/a1/e;->d:J

    cmp-long v0, v6, v2

    if-gez v0, :cond_25

    sub-long/2addr v2, v6

    long-to-int v0, v2

    invoke-virtual {v5, v0}, Lf/h/a/c/a1/e;->i(I)V

    :cond_25
    :goto_15
    iget v0, v1, Lf/h/a/c/a1/b0/d;->p:I

    if-nez v0, :cond_2a

    const/4 v0, 0x0

    iput v0, v5, Lf/h/a/c/a1/e;->f:I

    invoke-virtual/range {p0 .. p1}, Lf/h/a/c/a1/b0/d;->c(Lf/h/a/c/a1/e;)Z

    move-result v2

    if-eqz v2, :cond_26

    goto :goto_18

    :cond_26
    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v0}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v0

    iget v2, v1, Lf/h/a/c/a1/b0/d;->i:I

    int-to-long v2, v2

    invoke-static {v0, v2, v3}, Lf/h/a/c/a1/b0/d;->b(IJ)Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-static {v0}, Lf/h/a/c/a1/o;->a(I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_27

    goto :goto_16

    :cond_27
    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    invoke-static {v0, v2}, Lf/h/a/c/a1/o;->d(ILf/h/a/c/a1/o;)Z

    iget-wide v2, v1, Lf/h/a/c/a1/b0/d;->m:J

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v2, v6

    if-nez v0, :cond_28

    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    iget-wide v2, v5, Lf/h/a/c/a1/e;->d:J

    invoke-interface {v0, v2, v3}, Lf/h/a/c/a1/b0/e;->c(J)J

    move-result-wide v2

    iput-wide v2, v1, Lf/h/a/c/a1/b0/d;->m:J

    iget-wide v2, v1, Lf/h/a/c/a1/b0/d;->b:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_28

    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->k:Lf/h/a/c/a1/b0/e;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lf/h/a/c/a1/b0/e;->c(J)J

    move-result-wide v2

    iget-wide v6, v1, Lf/h/a/c/a1/b0/d;->m:J

    iget-wide v8, v1, Lf/h/a/c/a1/b0/d;->b:J

    sub-long/2addr v8, v2

    add-long/2addr v8, v6

    iput-wide v8, v1, Lf/h/a/c/a1/b0/d;->m:J

    :cond_28
    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v0, v0, Lf/h/a/c/a1/o;->c:I

    iput v0, v1, Lf/h/a/c/a1/b0/d;->p:I

    goto :goto_17

    :cond_29
    :goto_16
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lf/h/a/c/a1/e;->i(I)V

    const/4 v0, 0x0

    iput v0, v1, Lf/h/a/c/a1/b0/d;->i:I

    goto :goto_19

    :cond_2a
    :goto_17
    const/4 v0, 0x1

    iget-object v2, v1, Lf/h/a/c/a1/b0/d;->h:Lf/h/a/c/a1/s;

    iget v3, v1, Lf/h/a/c/a1/b0/d;->p:I

    invoke-interface {v2, v5, v3, v0}, Lf/h/a/c/a1/s;->a(Lf/h/a/c/a1/e;IZ)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2b

    :goto_18
    const/4 v0, -0x1

    goto :goto_1a

    :cond_2b
    iget v2, v1, Lf/h/a/c/a1/b0/d;->p:I

    sub-int/2addr v2, v0

    iput v2, v1, Lf/h/a/c/a1/b0/d;->p:I

    if-lez v2, :cond_2c

    goto :goto_19

    :cond_2c
    iget-wide v2, v1, Lf/h/a/c/a1/b0/d;->m:J

    iget-wide v4, v1, Lf/h/a/c/a1/b0/d;->n:J

    const-wide/32 v6, 0xf4240

    mul-long v4, v4, v6

    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v6, v0, Lf/h/a/c/a1/o;->d:I

    int-to-long v6, v6

    div-long/2addr v4, v6

    add-long v7, v4, v2

    iget-object v6, v1, Lf/h/a/c/a1/b0/d;->h:Lf/h/a/c/a1/s;

    const/4 v9, 0x1

    iget v10, v0, Lf/h/a/c/a1/o;->c:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-interface/range {v6 .. v12}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iget-wide v2, v1, Lf/h/a/c/a1/b0/d;->n:J

    iget-object v0, v1, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    iget v0, v0, Lf/h/a/c/a1/o;->g:I

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, v1, Lf/h/a/c/a1/b0/d;->n:J

    const/4 v0, 0x0

    iput v0, v1, Lf/h/a/c/a1/b0/d;->p:I

    :goto_19
    const/4 v0, 0x0

    :goto_1a
    return v0
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 2

    iput-object p1, p0, Lf/h/a/c/a1/b0/d;->g:Lf/h/a/c/a1/i;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/b0/d;->h:Lf/h/a/c/a1/s;

    iget-object p1, p0, Lf/h/a/c/a1/b0/d;->g:Lf/h/a/c/a1/i;

    invoke-interface {p1}, Lf/h/a/c/a1/i;->k()V

    return-void
.end method

.method public f(JJ)V
    .locals 0

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/a1/b0/d;->i:I

    const-wide p2, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p2, p0, Lf/h/a/c/a1/b0/d;->m:J

    const-wide/16 p2, 0x0

    iput-wide p2, p0, Lf/h/a/c/a1/b0/d;->n:J

    iput p1, p0, Lf/h/a/c/a1/b0/d;->p:I

    return-void
.end method

.method public final g(Lf/h/a/c/a1/e;Z)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    if-eqz p2, :cond_0

    const/16 v0, 0x4000

    goto :goto_0

    :cond_0
    const/high16 v0, 0x20000

    :goto_0
    const/4 v1, 0x0

    iput v1, p1, Lf/h/a/c/a1/e;->f:I

    iget-wide v2, p1, Lf/h/a/c/a1/e;->d:J

    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    cmp-long v7, v2, v4

    if-nez v7, :cond_5

    iget v2, p0, Lf/h/a/c/a1/b0/d;->a:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    sget-object v2, Lf/h/a/c/a1/b0/a;->a:Lf/h/a/c/a1/b0/a;

    :goto_2
    iget-object v3, p0, Lf/h/a/c/a1/b0/d;->f:Lf/h/a/c/a1/n;

    invoke-virtual {v3, p1, v2}, Lf/h/a/c/a1/n;->a(Lf/h/a/c/a1/e;Lf/h/a/c/c1/i/b$a;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v2

    iput-object v2, p0, Lf/h/a/c/a1/b0/d;->j:Lcom/google/android/exoplayer2/metadata/Metadata;

    if-eqz v2, :cond_3

    iget-object v3, p0, Lf/h/a/c/a1/b0/d;->e:Lf/h/a/c/a1/m;

    invoke-virtual {v3, v2}, Lf/h/a/c/a1/m;->b(Lcom/google/android/exoplayer2/metadata/Metadata;)Z

    :cond_3
    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v2

    long-to-int v3, v2

    if-nez p2, :cond_4

    invoke-virtual {p1, v3}, Lf/h/a/c/a1/e;->i(I)V

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_3
    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_4
    invoke-virtual {p0, p1}, Lf/h/a/c/a1/b0/d;->c(Lf/h/a/c/a1/e;)Z

    move-result v7

    if-eqz v7, :cond_7

    if-lez v4, :cond_6

    goto :goto_6

    :cond_6
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_7
    iget-object v7, p0, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v7, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v7, p0, Lf/h/a/c/a1/b0/d;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v7}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    if-eqz v2, :cond_8

    int-to-long v8, v2

    invoke-static {v7, v8, v9}, Lf/h/a/c/a1/b0/d;->b(IJ)Z

    move-result v8

    if-eqz v8, :cond_9

    :cond_8
    invoke-static {v7}, Lf/h/a/c/a1/o;->a(I)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_d

    :cond_9
    add-int/lit8 v2, v5, 0x1

    if-ne v5, v0, :cond_b

    if-eqz p2, :cond_a

    return v1

    :cond_a
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "Searched too many bytes."

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_b
    if-eqz p2, :cond_c

    iput v1, p1, Lf/h/a/c/a1/e;->f:I

    add-int v4, v3, v2

    invoke-virtual {p1, v4, v1}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto :goto_5

    :cond_c
    invoke-virtual {p1, v6}, Lf/h/a/c/a1/e;->i(I)V

    :goto_5
    move v5, v2

    const/4 v2, 0x0

    const/4 v4, 0x0

    goto :goto_4

    :cond_d
    add-int/lit8 v4, v4, 0x1

    if-ne v4, v6, :cond_e

    iget-object v2, p0, Lf/h/a/c/a1/b0/d;->d:Lf/h/a/c/a1/o;

    invoke-static {v7, v2}, Lf/h/a/c/a1/o;->d(ILf/h/a/c/a1/o;)Z

    move v2, v7

    goto :goto_8

    :cond_e
    const/4 v7, 0x4

    if-ne v4, v7, :cond_10

    :goto_6
    if-eqz p2, :cond_f

    add-int/2addr v3, v5

    invoke-virtual {p1, v3}, Lf/h/a/c/a1/e;->i(I)V

    goto :goto_7

    :cond_f
    iput v1, p1, Lf/h/a/c/a1/e;->f:I

    :goto_7
    iput v2, p0, Lf/h/a/c/a1/b0/d;->i:I

    return v6

    :cond_10
    :goto_8
    add-int/lit8 v8, v8, -0x4

    invoke-virtual {p1, v8, v1}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto :goto_4
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lf/h/a/c/a1/b0/d;->g(Lf/h/a/c/a1/e;Z)Z

    move-result p1

    return p1
.end method

.method public release()V
    .locals 0

    return-void
.end method
