.class public final Lf/h/a/c/a1/n;
.super Ljava/lang/Object;
.source "Id3Peeker.java"


# instance fields
.field public final a:Lf/h/a/c/i1/r;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/a1/n;->a:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;Lf/h/a/c/c1/i/b$a;)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 7
    .param p2    # Lf/h/a/c/c1/i/b$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    iget-object v3, p0, Lf/h/a/c/a1/n;->a:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    const/16 v4, 0xa

    invoke-virtual {p1, v3, v0, v4, v0}, Lf/h/a/c/a1/e;->e([BIIZ)Z
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Lf/h/a/c/a1/n;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v3, v0}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v3, p0, Lf/h/a/c/a1/n;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->s()I

    move-result v3

    const v5, 0x494433

    if-eq v3, v5, :cond_0

    goto :goto_2

    :cond_0
    iget-object v3, p0, Lf/h/a/c/a1/n;->a:Lf/h/a/c/i1/r;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v3, p0, Lf/h/a/c/a1/n;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->p()I

    move-result v3

    add-int/lit8 v5, v3, 0xa

    if-nez v1, :cond_1

    new-array v1, v5, [B

    iget-object v6, p0, Lf/h/a/c/a1/n;->a:Lf/h/a/c/i1/r;

    iget-object v6, v6, Lf/h/a/c/i1/r;->a:[B

    invoke-static {v6, v0, v1, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p1, v1, v4, v3, v0}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    new-instance v3, Lf/h/a/c/c1/i/b;

    invoke-direct {v3, p2}, Lf/h/a/c/c1/i/b;-><init>(Lf/h/a/c/c1/i/b$a;)V

    invoke-virtual {v3, v1, v5}, Lf/h/a/c/c1/i/b;->c([BI)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v3, v0}, Lf/h/a/c/a1/e;->a(IZ)Z

    :goto_1
    add-int/2addr v2, v5

    goto :goto_0

    :catch_0
    :goto_2
    iput v0, p1, Lf/h/a/c/a1/e;->f:I

    invoke-virtual {p1, v2, v0}, Lf/h/a/c/a1/e;->a(IZ)Z

    return-object v1
.end method
