.class public Lf/h/a/c/a1/a0/d;
.super Ljava/lang/Object;
.source "MatroskaExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/a0/d$c;,
        Lf/h/a/c/a1/a0/d$d;,
        Lf/h/a/c/a1/a0/d$b;
    }
.end annotation


# static fields
.field public static final b0:[B

.field public static final c0:[B

.field public static final d0:[B

.field public static final e0:Ljava/util/UUID;


# instance fields
.field public A:J

.field public B:J

.field public C:Lf/h/a/c/i1/m;

.field public D:Lf/h/a/c/i1/m;

.field public E:Z

.field public F:Z

.field public G:I

.field public H:J

.field public I:J

.field public J:I

.field public K:I

.field public L:[I

.field public M:I

.field public N:I

.field public O:I

.field public P:I

.field public Q:Z

.field public R:I

.field public S:I

.field public T:I

.field public U:Z

.field public V:Z

.field public W:Z

.field public X:I

.field public Y:B

.field public Z:Z

.field public final a:Lf/h/a/c/a1/a0/c;

.field public a0:Lf/h/a/c/a1/i;

.field public final b:Lf/h/a/c/a1/a0/f;

.field public final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/a1/a0/d$c;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Lf/h/a/c/i1/r;

.field public final f:Lf/h/a/c/i1/r;

.field public final g:Lf/h/a/c/i1/r;

.field public final h:Lf/h/a/c/i1/r;

.field public final i:Lf/h/a/c/i1/r;

.field public final j:Lf/h/a/c/i1/r;

.field public final k:Lf/h/a/c/i1/r;

.field public final l:Lf/h/a/c/i1/r;

.field public final m:Lf/h/a/c/i1/r;

.field public final n:Lf/h/a/c/i1/r;

.field public o:Ljava/nio/ByteBuffer;

.field public p:J

.field public q:J

.field public r:J

.field public s:J

.field public t:J

.field public u:Lf/h/a/c/a1/a0/d$c;

.field public v:Z

.field public w:I

.field public x:J

.field public y:Z

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x20

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    sput-object v1, Lf/h/a/c/a1/a0/d;->b0:[B

    const-string v1, "Format: Start, End, ReadOrder, Layer, Style, Name, MarginL, MarginR, MarginV, Effect, Text"

    invoke-static {v1}, Lf/h/a/c/i1/a0;->p(Ljava/lang/String;)[B

    move-result-object v1

    sput-object v1, Lf/h/a/c/a1/a0/d;->c0:[B

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lf/h/a/c/a1/a0/d;->d0:[B

    new-instance v0, Ljava/util/UUID;

    const-wide v1, 0x100000000001000L

    const-wide v3, -0x7fffff55ffc7648fL    # -3.607411173533E-312

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lf/h/a/c/a1/a0/d;->e0:Ljava/util/UUID;

    return-void

    :array_0
    .array-data 1
        0x31t
        0xat
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0x20t
        0x2dt
        0x2dt
        0x3et
        0x20t
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0xat
    .end array-data

    :array_1
    .array-data 1
        0x44t
        0x69t
        0x61t
        0x6ct
        0x6ft
        0x67t
        0x75t
        0x65t
        0x3at
        0x20t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 5

    new-instance v0, Lf/h/a/c/a1/a0/a;

    invoke-direct {v0}, Lf/h/a/c/a1/a0/a;-><init>()V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lf/h/a/c/a1/a0/d;->q:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v3, p0, Lf/h/a/c/a1/a0/d;->r:J

    iput-wide v3, p0, Lf/h/a/c/a1/a0/d;->s:J

    iput-wide v3, p0, Lf/h/a/c/a1/a0/d;->t:J

    iput-wide v1, p0, Lf/h/a/c/a1/a0/d;->z:J

    iput-wide v1, p0, Lf/h/a/c/a1/a0/d;->A:J

    iput-wide v3, p0, Lf/h/a/c/a1/a0/d;->B:J

    iput-object v0, p0, Lf/h/a/c/a1/a0/d;->a:Lf/h/a/c/a1/a0/c;

    new-instance v1, Lf/h/a/c/a1/a0/d$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lf/h/a/c/a1/a0/d$b;-><init>(Lf/h/a/c/a1/a0/d;Lf/h/a/c/a1/a0/d$a;)V

    iput-object v1, v0, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    const/4 v0, 0x1

    and-int/2addr p1, v0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lf/h/a/c/a1/a0/d;->d:Z

    new-instance p1, Lf/h/a/c/a1/a0/f;

    invoke-direct {p1}, Lf/h/a/c/a1/a0/f;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->b:Lf/h/a/c/a1/a0/f;

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    new-instance p1, Lf/h/a/c/i1/r;

    const/4 v0, 0x4

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-direct {p1, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->h:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->i:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    sget-object v1, Lf/h/a/c/i1/p;->a:[B

    invoke-direct {p1, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->e:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->f:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->k:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    const/16 v0, 0x8

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->l:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->m:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->n:Lf/h/a/c/i1/r;

    return-void
.end method

.method public static b([II)[I
    .locals 1

    if-nez p0, :cond_0

    new-array p0, p1, [I

    return-object p0

    :cond_0
    array-length v0, p0

    if-lt v0, p1, :cond_1

    return-object p0

    :cond_1
    array-length p0, p0

    mul-int/lit8 p0, p0, 0x2

    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result p0

    new-array p0, p0, [I

    return-object p0
.end method

.method public static c(JLjava/lang/String;J)[B
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, p0, v2

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Lf/g/j/k/a;->d(Z)V

    const-wide v2, 0xd693a400L

    div-long v2, p0, v2

    long-to-int v3, v2

    mul-int/lit16 v2, v3, 0xe10

    int-to-long v4, v2

    const-wide/32 v6, 0xf4240

    mul-long v4, v4, v6

    sub-long/2addr p0, v4

    const-wide/32 v4, 0x3938700

    div-long v4, p0, v4

    long-to-int v2, v4

    mul-int/lit8 v4, v2, 0x3c

    int-to-long v4, v4

    mul-long v4, v4, v6

    sub-long/2addr p0, v4

    div-long v4, p0, v6

    long-to-int v5, v4

    int-to-long v8, v5

    mul-long v8, v8, v6

    sub-long/2addr p0, v8

    div-long/2addr p0, p3

    long-to-int p1, p0

    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 p3, 0x4

    new-array p3, p3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p3, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p3, v1

    const/4 p4, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p3, p4

    const/4 p4, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p3, p4

    invoke-static {p0, p2, p3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lf/h/a/c/i1/a0;->p(Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a(Lf/h/a/c/a1/a0/d$c;JIII)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    iget-object v3, v1, Lf/h/a/c/a1/a0/d$c;->R:Lf/h/a/c/a1/a0/d$d;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    iget-boolean v6, v3, Lf/h/a/c/a1/a0/d$d;->b:Z

    if-nez v6, :cond_0

    goto/16 :goto_4

    :cond_0
    iget v6, v3, Lf/h/a/c/a1/a0/d$d;->c:I

    add-int/lit8 v7, v6, 0x1

    iput v7, v3, Lf/h/a/c/a1/a0/d$d;->c:I

    if-nez v6, :cond_1

    move-wide/from16 v9, p2

    iput-wide v9, v3, Lf/h/a/c/a1/a0/d$d;->d:J

    iput v2, v3, Lf/h/a/c/a1/a0/d$d;->e:I

    iput v5, v3, Lf/h/a/c/a1/a0/d$d;->f:I

    :cond_1
    iget v2, v3, Lf/h/a/c/a1/a0/d$d;->f:I

    add-int v2, v2, p5

    iput v2, v3, Lf/h/a/c/a1/a0/d$d;->f:I

    move/from16 v6, p6

    iput v6, v3, Lf/h/a/c/a1/a0/d$d;->g:I

    const/16 v2, 0x10

    if-lt v7, v2, :cond_b

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/a0/d$d;->a(Lf/h/a/c/a1/a0/d$c;)V

    goto/16 :goto_4

    :cond_2
    move-wide/from16 v9, p2

    move/from16 v6, p6

    iget-object v3, v1, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string v7, "S_TEXT/UTF8"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v8, "S_TEXT/ASS"

    if-nez v3, :cond_3

    iget-object v3, v1, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_3
    iget v3, v0, Lf/h/a/c/a1/a0/d;->K:I

    const-string v11, "MatroskaExtractor"

    if-le v3, v4, :cond_4

    const-string v3, "Skipping subtitle sample in laced block."

    invoke-static {v11, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-wide v12, v0, Lf/h/a/c/a1/a0/d;->I:J

    const-wide v14, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, v12, v14

    if-nez v3, :cond_6

    const-string v3, "Skipping subtitle sample with no duration."

    invoke-static {v11, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    :goto_0
    move/from16 v3, p5

    goto :goto_2

    :cond_6
    iget-object v3, v1, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    iget-object v11, v0, Lf/h/a/c/a1/a0/d;->k:Lf/h/a/c/i1/r;

    iget-object v11, v11, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-wide/16 v7, 0x3e8

    const-string v3, "%02d:%02d:%02d,%03d"

    invoke-static {v12, v13, v3, v7, v8}, Lf/h/a/c/a1/a0/d;->c(JLjava/lang/String;J)[B

    move-result-object v3

    const/16 v7, 0x13

    goto :goto_1

    :cond_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_8
    const-wide/16 v7, 0x2710

    const-string v3, "%01d:%02d:%02d:%02d"

    invoke-static {v12, v13, v3, v7, v8}, Lf/h/a/c/a1/a0/d;->c(JLjava/lang/String;J)[B

    move-result-object v3

    const/16 v7, 0x15

    :goto_1
    array-length v8, v3

    invoke-static {v3, v5, v11, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, v1, Lf/h/a/c/a1/a0/d$c;->V:Lf/h/a/c/a1/s;

    iget-object v5, v0, Lf/h/a/c/a1/a0/d;->k:Lf/h/a/c/i1/r;

    iget v7, v5, Lf/h/a/c/i1/r;->c:I

    invoke-interface {v3, v5, v7}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget-object v3, v0, Lf/h/a/c/a1/a0/d;->k:Lf/h/a/c/i1/r;

    iget v3, v3, Lf/h/a/c/i1/r;->c:I

    add-int v3, p5, v3

    :goto_2
    const/high16 v5, 0x10000000

    and-int/2addr v5, v2

    if-eqz v5, :cond_a

    iget v5, v0, Lf/h/a/c/a1/a0/d;->K:I

    if-le v5, v4, :cond_9

    const v5, -0x10000001

    and-int/2addr v2, v5

    goto :goto_3

    :cond_9
    iget-object v5, v0, Lf/h/a/c/a1/a0/d;->n:Lf/h/a/c/i1/r;

    iget v7, v5, Lf/h/a/c/i1/r;->c:I

    iget-object v8, v1, Lf/h/a/c/a1/a0/d$c;->V:Lf/h/a/c/a1/s;

    invoke-interface {v8, v5, v7}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    add-int/2addr v3, v7

    :cond_a
    :goto_3
    move v11, v2

    move v12, v3

    iget-object v8, v1, Lf/h/a/c/a1/a0/d$c;->V:Lf/h/a/c/a1/s;

    iget-object v14, v1, Lf/h/a/c/a1/a0/d$c;->i:Lf/h/a/c/a1/s$a;

    move-wide/from16 v9, p2

    move/from16 v13, p6

    invoke-interface/range {v8 .. v14}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    :cond_b
    :goto_4
    iput-boolean v4, v0, Lf/h/a/c/a1/a0/d;->F:Z

    return-void
.end method

.method public final d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    iput-boolean v3, v0, Lf/h/a/c/a1/a0/d;->F:Z

    const/4 v4, 0x1

    const/4 v5, 0x1

    :goto_0
    const/4 v6, -0x1

    if-eqz v5, :cond_6b

    iget-boolean v7, v0, Lf/h/a/c/a1/a0/d;->F:Z

    if-nez v7, :cond_6b

    iget-object v5, v0, Lf/h/a/c/a1/a0/d;->a:Lf/h/a/c/a1/a0/c;

    move-object v7, v5

    check-cast v7, Lf/h/a/c/a1/a0/a;

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v5}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v5

    const-wide/16 v8, -0x1

    if-nez v5, :cond_0

    iget-wide v10, v1, Lf/h/a/c/a1/e;->d:J

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v5}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/a1/a0/a$b;

    iget-wide v12, v5, Lf/h/a/c/a1/a0/a$b;->b:J

    cmp-long v5, v10, v12

    if-ltz v5, :cond_0

    iget-object v3, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    iget-object v4, v7, Lf/h/a/c/a1/a0/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/a1/a0/a$b;

    iget v4, v4, Lf/h/a/c/a1/a0/a$b;->a:I

    check-cast v3, Lf/h/a/c/a1/a0/d$b;

    invoke-virtual {v3, v4}, Lf/h/a/c/a1/a0/d$b;->a(I)V

    goto/16 :goto_22

    :cond_0
    iget v5, v7, Lf/h/a/c/a1/a0/a;->e:I

    const v10, 0x1c53bb6b

    const v11, 0x1f43b675

    const/4 v12, 0x4

    if-nez v5, :cond_6

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->c:Lf/h/a/c/a1/a0/f;

    invoke-virtual {v5, v1, v4, v3, v12}, Lf/h/a/c/a1/a0/f;->c(Lf/h/a/c/a1/e;ZZI)J

    move-result-wide v13

    const-wide/16 v15, -0x2

    cmp-long v5, v13, v15

    if-nez v5, :cond_4

    iput v3, v1, Lf/h/a/c/a1/e;->f:I

    :goto_2
    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->a:[B

    invoke-virtual {v1, v5, v3, v12, v3}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->a:[B

    aget-byte v5, v5, v3

    invoke-static {v5}, Lf/h/a/c/a1/a0/f;->b(I)I

    move-result v5

    if-eq v5, v6, :cond_3

    if-gt v5, v12, :cond_3

    iget-object v13, v7, Lf/h/a/c/a1/a0/a;->a:[B

    invoke-static {v13, v5, v3}, Lf/h/a/c/a1/a0/f;->a([BIZ)J

    move-result-wide v13

    long-to-int v14, v13

    iget-object v13, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    check-cast v13, Lf/h/a/c/a1/a0/d$b;

    iget-object v13, v13, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v13}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const v13, 0x1549a966

    if-eq v14, v13, :cond_2

    if-eq v14, v11, :cond_2

    if-eq v14, v10, :cond_2

    const v13, 0x1654ae6b

    if-ne v14, v13, :cond_1

    goto :goto_3

    :cond_1
    const/4 v13, 0x0

    goto :goto_4

    :cond_2
    :goto_3
    const/4 v13, 0x1

    :goto_4
    if-eqz v13, :cond_3

    invoke-virtual {v1, v5}, Lf/h/a/c/a1/e;->i(I)V

    int-to-long v13, v14

    goto :goto_5

    :cond_3
    invoke-virtual {v1, v4}, Lf/h/a/c/a1/e;->i(I)V

    goto :goto_2

    :cond_4
    :goto_5
    cmp-long v5, v13, v8

    if-nez v5, :cond_5

    const/4 v3, 0x0

    const/4 v5, 0x0

    goto/16 :goto_23

    :cond_5
    long-to-int v5, v13

    iput v5, v7, Lf/h/a/c/a1/a0/a;->f:I

    iput v4, v7, Lf/h/a/c/a1/a0/a;->e:I

    :cond_6
    iget v5, v7, Lf/h/a/c/a1/a0/a;->e:I

    const/16 v8, 0x8

    const/4 v9, 0x2

    if-ne v5, v4, :cond_7

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->c:Lf/h/a/c/a1/a0/f;

    invoke-virtual {v5, v1, v3, v4, v8}, Lf/h/a/c/a1/a0/f;->c(Lf/h/a/c/a1/e;ZZI)J

    move-result-wide v10

    iput-wide v10, v7, Lf/h/a/c/a1/a0/a;->g:J

    iput v9, v7, Lf/h/a/c/a1/a0/a;->e:I

    :cond_7
    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    iget v10, v7, Lf/h/a/c/a1/a0/a;->f:I

    check-cast v5, Lf/h/a/c/a1/a0/d$b;

    iget-object v5, v5, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x3

    const/4 v11, 0x5

    sparse-switch v10, :sswitch_data_0

    const/4 v10, 0x0

    goto :goto_6

    :sswitch_0
    const/4 v10, 0x5

    goto :goto_6

    :sswitch_1
    const/4 v10, 0x4

    goto :goto_6

    :sswitch_2
    const/4 v10, 0x1

    goto :goto_6

    :sswitch_3
    const/4 v10, 0x3

    goto :goto_6

    :sswitch_4
    const/4 v10, 0x2

    :goto_6
    if-eqz v10, :cond_6a

    if-eq v10, v4, :cond_59

    const-string v6, " not supported"

    const-wide/16 v13, 0x8

    if-eq v10, v9, :cond_3b

    const-wide/32 v17, 0x7fffffff

    if-eq v10, v5, :cond_31

    if-eq v10, v12, :cond_e

    if-ne v10, v11, :cond_d

    iget-wide v4, v7, Lf/h/a/c/a1/a0/a;->g:J

    const-wide/16 v8, 0x4

    cmp-long v6, v4, v8

    if-eqz v6, :cond_9

    cmp-long v6, v4, v13

    if-nez v6, :cond_8

    goto :goto_7

    :cond_8
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Invalid float size: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v7, Lf/h/a/c/a1/a0/a;->g:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    :goto_7
    iget-object v6, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    iget v8, v7, Lf/h/a/c/a1/a0/a;->f:I

    long-to-int v5, v4

    invoke-virtual {v7, v1, v5}, Lf/h/a/c/a1/a0/a;->a(Lf/h/a/c/a1/e;I)J

    move-result-wide v9

    if-ne v5, v12, :cond_a

    long-to-int v4, v9

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    float-to-double v4, v4

    goto :goto_8

    :cond_a
    invoke-static {v9, v10}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    :goto_8
    check-cast v6, Lf/h/a/c/a1/a0/d$b;

    iget-object v6, v6, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v9, 0xb5

    if-eq v8, v9, :cond_c

    const/16 v9, 0x4489

    if-eq v8, v9, :cond_b

    packed-switch v8, :pswitch_data_0

    packed-switch v8, :pswitch_data_1

    goto :goto_9

    :pswitch_0
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->L:F

    goto :goto_9

    :pswitch_1
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->K:F

    goto :goto_9

    :pswitch_2
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->J:F

    goto :goto_9

    :pswitch_3
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->I:F

    goto :goto_9

    :pswitch_4
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->H:F

    goto :goto_9

    :pswitch_5
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->G:F

    goto :goto_9

    :pswitch_6
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->F:F

    goto :goto_9

    :pswitch_7
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->E:F

    goto :goto_9

    :pswitch_8
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->D:F

    goto :goto_9

    :pswitch_9
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->C:F

    goto :goto_9

    :pswitch_a
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->t:F

    goto :goto_9

    :pswitch_b
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->s:F

    goto :goto_9

    :pswitch_c
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-float v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->r:F

    goto :goto_9

    :cond_b
    double-to-long v4, v4

    iput-wide v4, v6, Lf/h/a/c/a1/a0/d;->s:J

    goto :goto_9

    :cond_c
    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    double-to-int v4, v4

    iput v4, v6, Lf/h/a/c/a1/a0/d$c;->O:I

    :goto_9
    iput v3, v7, Lf/h/a/c/a1/a0/a;->e:I

    goto/16 :goto_22

    :cond_d
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Invalid element type "

    invoke-static {v2, v10}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    iget-object v6, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    iget v10, v7, Lf/h/a/c/a1/a0/a;->f:I

    iget-wide v13, v7, Lf/h/a/c/a1/a0/a;->g:J

    long-to-int v11, v13

    check-cast v6, Lf/h/a/c/a1/a0/d$b;

    iget-object v6, v6, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v13, 0xa1

    const/16 v14, 0xa3

    if-eq v10, v13, :cond_18

    if-eq v10, v14, :cond_18

    const/16 v5, 0xa5

    if-eq v10, v5, :cond_14

    const/16 v5, 0x4255

    if-eq v10, v5, :cond_13

    const/16 v5, 0x47e2

    if-eq v10, v5, :cond_12

    const/16 v4, 0x53ab

    if-eq v10, v4, :cond_11

    const/16 v4, 0x63a2

    if-eq v10, v4, :cond_10

    const/16 v4, 0x7672

    if-ne v10, v4, :cond_f

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    new-array v5, v11, [B

    iput-object v5, v4, Lf/h/a/c/a1/a0/d$c;->u:[B

    invoke-virtual {v1, v5, v3, v11, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    goto/16 :goto_18

    :cond_f
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Unexpected id: "

    invoke-static {v2, v10}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_10
    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    new-array v5, v11, [B

    iput-object v5, v4, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-virtual {v1, v5, v3, v11, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    goto/16 :goto_18

    :cond_11
    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->i:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    invoke-static {v4, v3}, Ljava/util/Arrays;->fill([BB)V

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->i:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    rsub-int/lit8 v5, v11, 0x4

    invoke-virtual {v1, v4, v5, v11, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->i:Lf/h/a/c/i1/r;

    invoke-virtual {v4, v3}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v3, v6, Lf/h/a/c/a1/a0/d;->i:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v3

    long-to-int v4, v3

    iput v4, v6, Lf/h/a/c/a1/a0/d;->w:I

    goto/16 :goto_18

    :cond_12
    new-array v5, v11, [B

    invoke-virtual {v1, v5, v3, v11, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object v6, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    new-instance v8, Lf/h/a/c/a1/s$a;

    invoke-direct {v8, v4, v5, v3, v3}, Lf/h/a/c/a1/s$a;-><init>(I[BII)V

    iput-object v8, v6, Lf/h/a/c/a1/a0/d$c;->i:Lf/h/a/c/a1/s$a;

    goto/16 :goto_18

    :cond_13
    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    new-array v5, v11, [B

    iput-object v5, v4, Lf/h/a/c/a1/a0/d$c;->h:[B

    invoke-virtual {v1, v5, v3, v11, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    goto/16 :goto_18

    :cond_14
    iget v4, v6, Lf/h/a/c/a1/a0/d;->G:I

    if-eq v4, v9, :cond_15

    goto/16 :goto_18

    :cond_15
    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    iget v5, v6, Lf/h/a/c/a1/a0/d;->M:I

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/a1/a0/d$c;

    iget v5, v6, Lf/h/a/c/a1/a0/d;->P:I

    if-ne v5, v12, :cond_17

    iget-object v4, v4, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string v5, "V_VP9"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->n:Lf/h/a/c/i1/r;

    iget-object v5, v4, Lf/h/a/c/i1/r;->a:[B

    array-length v8, v5

    if-ge v8, v11, :cond_16

    new-array v5, v11, [B

    :cond_16
    invoke-virtual {v4, v5, v11}, Lf/h/a/c/i1/r;->A([BI)V

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->n:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v4, v3, v11, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    goto/16 :goto_18

    :cond_17
    invoke-virtual {v1, v11}, Lf/h/a/c/a1/e;->i(I)V

    goto/16 :goto_18

    :cond_18
    iget v12, v6, Lf/h/a/c/a1/a0/d;->G:I

    if-nez v12, :cond_19

    iget-object v12, v6, Lf/h/a/c/a1/a0/d;->b:Lf/h/a/c/a1/a0/f;

    invoke-virtual {v12, v1, v3, v4, v8}, Lf/h/a/c/a1/a0/f;->c(Lf/h/a/c/a1/e;ZZI)J

    move-result-wide v12

    long-to-int v8, v12

    iput v8, v6, Lf/h/a/c/a1/a0/d;->M:I

    iget-object v8, v6, Lf/h/a/c/a1/a0/d;->b:Lf/h/a/c/a1/a0/f;

    iget v8, v8, Lf/h/a/c/a1/a0/f;->c:I

    iput v8, v6, Lf/h/a/c/a1/a0/d;->N:I

    const-wide v12, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v12, v6, Lf/h/a/c/a1/a0/d;->I:J

    iput v4, v6, Lf/h/a/c/a1/a0/d;->G:I

    iget-object v8, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->x()V

    :cond_19
    iget-object v8, v6, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    iget v12, v6, Lf/h/a/c/a1/a0/d;->M:I

    invoke-virtual {v8, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/h/a/c/a1/a0/d$c;

    if-nez v8, :cond_1a

    iget v4, v6, Lf/h/a/c/a1/a0/d;->N:I

    sub-int/2addr v11, v4

    invoke-virtual {v1, v11}, Lf/h/a/c/a1/e;->i(I)V

    iput v3, v6, Lf/h/a/c/a1/a0/d;->G:I

    goto/16 :goto_18

    :cond_1a
    iget v12, v6, Lf/h/a/c/a1/a0/d;->G:I

    if-ne v12, v4, :cond_2d

    invoke-virtual {v6, v1, v5}, Lf/h/a/c/a1/a0/d;->g(Lf/h/a/c/a1/e;I)V

    iget-object v12, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v12, v12, Lf/h/a/c/i1/r;->a:[B

    aget-byte v12, v12, v9

    and-int/lit8 v12, v12, 0x6

    shr-int/2addr v12, v4

    const/16 v13, 0xff

    if-nez v12, :cond_1b

    iput v4, v6, Lf/h/a/c/a1/a0/d;->K:I

    iget-object v9, v6, Lf/h/a/c/a1/a0/d;->L:[I

    invoke-static {v9, v4}, Lf/h/a/c/a1/a0/d;->b([II)[I

    move-result-object v4

    iput-object v4, v6, Lf/h/a/c/a1/a0/d;->L:[I

    iget v9, v6, Lf/h/a/c/a1/a0/d;->N:I

    sub-int/2addr v11, v9

    sub-int/2addr v11, v5

    aput v11, v4, v3

    goto/16 :goto_10

    :cond_1b
    const/4 v14, 0x4

    invoke-virtual {v6, v1, v14}, Lf/h/a/c/a1/a0/d;->g(Lf/h/a/c/a1/e;I)V

    iget-object v14, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v14, v14, Lf/h/a/c/i1/r;->a:[B

    aget-byte v14, v14, v5

    and-int/2addr v14, v13

    add-int/2addr v14, v4

    iput v14, v6, Lf/h/a/c/a1/a0/d;->K:I

    iget-object v15, v6, Lf/h/a/c/a1/a0/d;->L:[I

    invoke-static {v15, v14}, Lf/h/a/c/a1/a0/d;->b([II)[I

    move-result-object v14

    iput-object v14, v6, Lf/h/a/c/a1/a0/d;->L:[I

    if-ne v12, v9, :cond_1c

    iget v4, v6, Lf/h/a/c/a1/a0/d;->N:I

    sub-int/2addr v11, v4

    add-int/lit8 v11, v11, -0x4

    iget v4, v6, Lf/h/a/c/a1/a0/d;->K:I

    div-int/2addr v11, v4

    invoke-static {v14, v3, v4, v11}, Ljava/util/Arrays;->fill([IIII)V

    goto/16 :goto_10

    :cond_1c
    if-ne v12, v4, :cond_1f

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x4

    :goto_a
    iget v12, v6, Lf/h/a/c/a1/a0/d;->K:I

    add-int/lit8 v14, v12, -0x1

    if-ge v4, v14, :cond_1e

    iget-object v12, v6, Lf/h/a/c/a1/a0/d;->L:[I

    aput v3, v12, v4

    :cond_1d
    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v6, v1, v9}, Lf/h/a/c/a1/a0/d;->g(Lf/h/a/c/a1/e;I)V

    iget-object v12, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v12, v12, Lf/h/a/c/i1/r;->a:[B

    add-int/lit8 v14, v9, -0x1

    aget-byte v12, v12, v14

    and-int/2addr v12, v13

    iget-object v14, v6, Lf/h/a/c/a1/a0/d;->L:[I

    aget v15, v14, v4

    add-int/2addr v15, v12

    aput v15, v14, v4

    if-eq v12, v13, :cond_1d

    aget v12, v14, v4

    add-int/2addr v5, v12

    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_1e
    iget-object v3, v6, Lf/h/a/c/a1/a0/d;->L:[I

    add-int/lit8 v12, v12, -0x1

    iget v4, v6, Lf/h/a/c/a1/a0/d;->N:I

    sub-int/2addr v11, v4

    sub-int/2addr v11, v9

    sub-int/2addr v11, v5

    aput v11, v3, v12

    goto/16 :goto_10

    :cond_1f
    if-ne v12, v5, :cond_2c

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x4

    :goto_b
    iget v14, v6, Lf/h/a/c/a1/a0/d;->K:I

    add-int/lit8 v15, v14, -0x1

    if-ge v5, v15, :cond_27

    iget-object v14, v6, Lf/h/a/c/a1/a0/d;->L:[I

    aput v3, v14, v5

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {v6, v1, v12}, Lf/h/a/c/a1/a0/d;->g(Lf/h/a/c/a1/e;I)V

    iget-object v3, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    add-int/lit8 v14, v12, -0x1

    aget-byte v3, v3, v14

    if-eqz v3, :cond_26

    const/16 v3, 0x8

    const/4 v15, 0x0

    :goto_c
    if-ge v15, v3, :cond_22

    rsub-int/lit8 v3, v15, 0x7

    shl-int v3, v4, v3

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    aget-byte v4, v4, v14

    and-int/2addr v4, v3

    if-eqz v4, :cond_21

    add-int/2addr v12, v15

    invoke-virtual {v6, v1, v12}, Lf/h/a/c/a1/a0/d;->g(Lf/h/a/c/a1/e;I)V

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    add-int/lit8 v16, v14, 0x1

    aget-byte v4, v4, v14

    and-int/2addr v4, v13

    not-int v3, v3

    and-int/2addr v3, v4

    int-to-long v3, v3

    move/from16 v14, v16

    :goto_d
    if-ge v14, v12, :cond_20

    const/16 v16, 0x8

    shl-long v3, v3, v16

    iget-object v13, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v13, v13, Lf/h/a/c/i1/r;->a:[B

    add-int/lit8 v21, v14, 0x1

    aget-byte v13, v13, v14

    const/16 v14, 0xff

    and-int/2addr v13, v14

    int-to-long v13, v13

    or-long/2addr v3, v13

    move/from16 v14, v21

    const/16 v13, 0xff

    goto :goto_d

    :cond_20
    if-lez v5, :cond_23

    mul-int/lit8 v15, v15, 0x7

    add-int/lit8 v15, v15, 0x6

    const-wide/16 v13, 0x1

    shl-long v19, v13, v15

    sub-long v21, v19, v13

    sub-long v3, v3, v21

    goto :goto_e

    :cond_21
    add-int/lit8 v15, v15, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x1

    const/16 v13, 0xff

    goto :goto_c

    :cond_22
    const-wide/16 v3, 0x0

    :cond_23
    :goto_e
    const-wide/32 v13, -0x80000000

    cmp-long v15, v3, v13

    if-ltz v15, :cond_25

    cmp-long v13, v3, v17

    if-gtz v13, :cond_25

    long-to-int v4, v3

    iget-object v3, v6, Lf/h/a/c/a1/a0/d;->L:[I

    if-nez v5, :cond_24

    goto :goto_f

    :cond_24
    add-int/lit8 v13, v5, -0x1

    aget v13, v3, v13

    add-int/2addr v4, v13

    :goto_f
    aput v4, v3, v5

    aget v3, v3, v5

    add-int/2addr v9, v3

    add-int/lit8 v5, v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/16 v13, 0xff

    goto/16 :goto_b

    :cond_25
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "EBML lacing sample size out of range."

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_26
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "No valid varint length mask found"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_27
    iget-object v3, v6, Lf/h/a/c/a1/a0/d;->L:[I

    add-int/lit8 v14, v14, -0x1

    iget v4, v6, Lf/h/a/c/a1/a0/d;->N:I

    sub-int/2addr v11, v4

    sub-int/2addr v11, v12

    sub-int/2addr v11, v9

    aput v11, v3, v14

    :goto_10
    iget-object v3, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    const/4 v4, 0x0

    aget-byte v4, v3, v4

    shl-int/lit8 v4, v4, 0x8

    const/4 v5, 0x1

    aget-byte v3, v3, v5

    const/16 v5, 0xff

    and-int/2addr v3, v5

    or-int/2addr v3, v4

    iget-wide v4, v6, Lf/h/a/c/a1/a0/d;->B:J

    int-to-long v11, v3

    invoke-virtual {v6, v11, v12}, Lf/h/a/c/a1/a0/d;->j(J)J

    move-result-wide v11

    add-long/2addr v11, v4

    iput-wide v11, v6, Lf/h/a/c/a1/a0/d;->H:J

    iget-object v3, v6, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    const/4 v4, 0x2

    aget-byte v5, v3, v4

    const/16 v9, 0x8

    and-int/2addr v5, v9

    if-ne v5, v9, :cond_28

    const/4 v5, 0x1

    goto :goto_11

    :cond_28
    const/4 v5, 0x0

    :goto_11
    iget v9, v8, Lf/h/a/c/a1/a0/d$c;->d:I

    if-eq v9, v4, :cond_2a

    const/16 v9, 0xa3

    if-ne v10, v9, :cond_29

    aget-byte v3, v3, v4

    const/16 v4, 0x80

    and-int/2addr v3, v4

    if-ne v3, v4, :cond_29

    goto :goto_12

    :cond_29
    const/4 v3, 0x0

    goto :goto_13

    :cond_2a
    :goto_12
    const/4 v3, 0x1

    :goto_13
    if-eqz v5, :cond_2b

    const/high16 v4, -0x80000000

    goto :goto_14

    :cond_2b
    const/4 v4, 0x0

    :goto_14
    or-int/2addr v3, v4

    iput v3, v6, Lf/h/a/c/a1/a0/d;->O:I

    const/4 v3, 0x2

    iput v3, v6, Lf/h/a/c/a1/a0/d;->G:I

    const/4 v3, 0x0

    iput v3, v6, Lf/h/a/c/a1/a0/d;->J:I

    goto :goto_15

    :cond_2c
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Unexpected lacing value: "

    invoke-static {v2, v12}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2d
    :goto_15
    const/16 v3, 0xa3

    if-ne v10, v3, :cond_2f

    :goto_16
    iget v3, v6, Lf/h/a/c/a1/a0/d;->J:I

    iget v4, v6, Lf/h/a/c/a1/a0/d;->K:I

    if-ge v3, v4, :cond_2e

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->L:[I

    aget v3, v4, v3

    invoke-virtual {v6, v1, v8, v3}, Lf/h/a/c/a1/a0/d;->k(Lf/h/a/c/a1/e;Lf/h/a/c/a1/a0/d$c;I)I

    move-result v24

    iget-wide v3, v6, Lf/h/a/c/a1/a0/d;->H:J

    iget v5, v6, Lf/h/a/c/a1/a0/d;->J:I

    iget v9, v8, Lf/h/a/c/a1/a0/d$c;->e:I

    mul-int v5, v5, v9

    div-int/lit16 v5, v5, 0x3e8

    int-to-long v9, v5

    add-long v21, v9, v3

    iget v3, v6, Lf/h/a/c/a1/a0/d;->O:I

    const/16 v25, 0x0

    move-object/from16 v19, v6

    move-object/from16 v20, v8

    move/from16 v23, v3

    invoke-virtual/range {v19 .. v25}, Lf/h/a/c/a1/a0/d;->a(Lf/h/a/c/a1/a0/d$c;JIII)V

    iget v3, v6, Lf/h/a/c/a1/a0/d;->J:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v6, Lf/h/a/c/a1/a0/d;->J:I

    goto :goto_16

    :cond_2e
    const/4 v3, 0x0

    iput v3, v6, Lf/h/a/c/a1/a0/d;->G:I

    goto :goto_18

    :cond_2f
    :goto_17
    iget v3, v6, Lf/h/a/c/a1/a0/d;->J:I

    iget v4, v6, Lf/h/a/c/a1/a0/d;->K:I

    if-ge v3, v4, :cond_30

    iget-object v4, v6, Lf/h/a/c/a1/a0/d;->L:[I

    aget v5, v4, v3

    invoke-virtual {v6, v1, v8, v5}, Lf/h/a/c/a1/a0/d;->k(Lf/h/a/c/a1/e;Lf/h/a/c/a1/a0/d$c;I)I

    move-result v5

    aput v5, v4, v3

    iget v3, v6, Lf/h/a/c/a1/a0/d;->J:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v6, Lf/h/a/c/a1/a0/d;->J:I

    goto :goto_17

    :cond_30
    :goto_18
    const/4 v3, 0x0

    iput v3, v7, Lf/h/a/c/a1/a0/a;->e:I

    goto/16 :goto_22

    :cond_31
    iget-wide v3, v7, Lf/h/a/c/a1/a0/a;->g:J

    cmp-long v5, v3, v17

    if-gtz v5, :cond_3a

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    iget v8, v7, Lf/h/a/c/a1/a0/a;->f:I

    long-to-int v4, v3

    if-nez v4, :cond_32

    const-string v3, ""

    goto :goto_1a

    :cond_32
    new-array v3, v4, [B

    const/4 v9, 0x0

    invoke-virtual {v1, v3, v9, v4, v9}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    :goto_19
    if-lez v4, :cond_33

    add-int/lit8 v10, v4, -0x1

    aget-byte v11, v3, v10

    if-nez v11, :cond_33

    move v4, v10

    goto :goto_19

    :cond_33
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v3, v9, v4}, Ljava/lang/String;-><init>([BII)V

    move-object v3, v10

    :goto_1a
    check-cast v5, Lf/h/a/c/a1/a0/d$b;

    iget-object v4, v5, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v5, 0x86

    if-eq v8, v5, :cond_38

    const/16 v5, 0x4282

    if-eq v8, v5, :cond_36

    const/16 v5, 0x536e

    if-eq v8, v5, :cond_35

    const v5, 0x22b59c

    if-eq v8, v5, :cond_34

    goto :goto_1b

    :cond_34
    iget-object v4, v4, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput-object v3, v4, Lf/h/a/c/a1/a0/d$c;->U:Ljava/lang/String;

    goto :goto_1b

    :cond_35
    iget-object v4, v4, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput-object v3, v4, Lf/h/a/c/a1/a0/d$c;->a:Ljava/lang/String;

    goto :goto_1b

    :cond_36
    const-string v4, "webm"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_39

    const-string v4, "matroska"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_37

    goto :goto_1b

    :cond_37
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "DocType "

    invoke-static {v2, v3, v6}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_38
    iget-object v4, v4, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput-object v3, v4, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    :cond_39
    :goto_1b
    const/4 v3, 0x0

    iput v3, v7, Lf/h/a/c/a1/a0/a;->e:I

    goto/16 :goto_22

    :cond_3a
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "String element size: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v7, Lf/h/a/c/a1/a0/a;->g:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3b
    iget-wide v3, v7, Lf/h/a/c/a1/a0/a;->g:J

    cmp-long v8, v3, v13

    if-gtz v8, :cond_58

    iget-object v8, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    iget v9, v7, Lf/h/a/c/a1/a0/a;->f:I

    long-to-int v4, v3

    invoke-virtual {v7, v1, v4}, Lf/h/a/c/a1/a0/a;->a(Lf/h/a/c/a1/e;I)J

    move-result-wide v3

    check-cast v8, Lf/h/a/c/a1/a0/d$b;

    iget-object v8, v8, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v10, 0x5031

    if-eq v9, v10, :cond_55

    const/16 v10, 0x5032

    if-eq v9, v10, :cond_53

    sparse-switch v9, :sswitch_data_1

    const/4 v6, 0x7

    packed-switch v9, :pswitch_data_2

    goto/16 :goto_1e

    :sswitch_5
    iput-wide v3, v8, Lf/h/a/c/a1/a0/d;->r:J

    goto/16 :goto_1e

    :sswitch_6
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->e:I

    goto/16 :goto_1e

    :sswitch_7
    long-to-int v4, v3

    if-eqz v4, :cond_3f

    const/4 v3, 0x1

    if-eq v4, v3, :cond_3e

    const/4 v3, 0x2

    if-eq v4, v3, :cond_3d

    if-eq v4, v5, :cond_3c

    goto/16 :goto_1e

    :cond_3c
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v5, v3, Lf/h/a/c/a1/a0/d$c;->q:I

    goto/16 :goto_1e

    :cond_3d
    iget-object v4, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v3, v4, Lf/h/a/c/a1/a0/d$c;->q:I

    goto/16 :goto_1e

    :cond_3e
    iget-object v4, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v3, v4, Lf/h/a/c/a1/a0/d$c;->q:I

    goto/16 :goto_1e

    :cond_3f
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    const/4 v4, 0x0

    iput v4, v3, Lf/h/a/c/a1/a0/d$c;->q:I

    goto/16 :goto_1e

    :sswitch_8
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->N:I

    goto/16 :goto_1e

    :sswitch_9
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput-wide v3, v5, Lf/h/a/c/a1/a0/d$c;->Q:J

    goto/16 :goto_1e

    :sswitch_a
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput-wide v3, v5, Lf/h/a/c/a1/a0/d$c;->P:J

    goto/16 :goto_1e

    :sswitch_b
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->f:I

    goto/16 :goto_1e

    :sswitch_c
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    const-wide/16 v8, 0x1

    cmp-long v6, v3, v8

    if-nez v6, :cond_40

    const/4 v3, 0x1

    goto :goto_1c

    :cond_40
    const/4 v3, 0x0

    :goto_1c
    iput-boolean v3, v5, Lf/h/a/c/a1/a0/d$c;->S:Z

    goto/16 :goto_1e

    :sswitch_d
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->o:I

    goto/16 :goto_1e

    :sswitch_e
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->p:I

    goto/16 :goto_1e

    :sswitch_f
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->n:I

    goto/16 :goto_1e

    :sswitch_10
    long-to-int v4, v3

    if-eqz v4, :cond_44

    const/4 v3, 0x1

    if-eq v4, v3, :cond_43

    if-eq v4, v5, :cond_42

    const/16 v3, 0xf

    if-eq v4, v3, :cond_41

    goto/16 :goto_1e

    :cond_41
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v5, v3, Lf/h/a/c/a1/a0/d$c;->v:I

    goto/16 :goto_1e

    :cond_42
    iget-object v4, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v3, v4, Lf/h/a/c/a1/a0/d$c;->v:I

    goto/16 :goto_1e

    :cond_43
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    const/4 v4, 0x2

    iput v4, v3, Lf/h/a/c/a1/a0/d$c;->v:I

    goto/16 :goto_1e

    :cond_44
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    const/4 v4, 0x0

    iput v4, v3, Lf/h/a/c/a1/a0/d$c;->v:I

    goto/16 :goto_1e

    :sswitch_11
    iget-wide v5, v8, Lf/h/a/c/a1/a0/d;->q:J

    add-long/2addr v3, v5

    iput-wide v3, v8, Lf/h/a/c/a1/a0/d;->x:J

    goto/16 :goto_1e

    :sswitch_12
    const-wide/16 v8, 0x1

    cmp-long v5, v3, v8

    if-nez v5, :cond_45

    goto/16 :goto_1e

    :cond_45
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "AESSettingsCipherMode "

    invoke-static {v2, v3, v4, v6}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_13
    const-wide/16 v8, 0x5

    cmp-long v5, v3, v8

    if-nez v5, :cond_46

    goto/16 :goto_1e

    :cond_46
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "ContentEncAlgo "

    invoke-static {v2, v3, v4, v6}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_14
    const-wide/16 v8, 0x1

    cmp-long v5, v3, v8

    if-nez v5, :cond_47

    goto/16 :goto_1e

    :cond_47
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "EBMLReadVersion "

    invoke-static {v2, v3, v4, v6}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_15
    const-wide/16 v8, 0x1

    cmp-long v5, v3, v8

    if-ltz v5, :cond_48

    const-wide/16 v8, 0x2

    cmp-long v5, v3, v8

    if-gtz v5, :cond_48

    goto/16 :goto_1e

    :cond_48
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "DocTypeReadVersion "

    invoke-static {v2, v3, v4, v6}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_16
    const-wide/16 v8, 0x3

    cmp-long v5, v3, v8

    if-nez v5, :cond_49

    goto/16 :goto_1e

    :cond_49
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "ContentCompAlgo "

    invoke-static {v2, v3, v4, v6}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_17
    const/4 v3, 0x1

    iput-boolean v3, v8, Lf/h/a/c/a1/a0/d;->Q:Z

    goto/16 :goto_1e

    :sswitch_18
    const/4 v5, 0x1

    iget-boolean v6, v8, Lf/h/a/c/a1/a0/d;->E:Z

    if-nez v6, :cond_56

    iget-object v6, v8, Lf/h/a/c/a1/a0/d;->D:Lf/h/a/c/i1/m;

    invoke-virtual {v6, v3, v4}, Lf/h/a/c/i1/m;->a(J)V

    iput-boolean v5, v8, Lf/h/a/c/a1/a0/d;->E:Z

    goto/16 :goto_1e

    :sswitch_19
    long-to-int v4, v3

    iput v4, v8, Lf/h/a/c/a1/a0/d;->P:I

    goto/16 :goto_1e

    :sswitch_1a
    invoke-virtual {v8, v3, v4}, Lf/h/a/c/a1/a0/d;->j(J)J

    move-result-wide v3

    iput-wide v3, v8, Lf/h/a/c/a1/a0/d;->B:J

    goto/16 :goto_1e

    :sswitch_1b
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->c:I

    goto/16 :goto_1e

    :sswitch_1c
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->m:I

    goto/16 :goto_1e

    :sswitch_1d
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->C:Lf/h/a/c/i1/m;

    invoke-virtual {v8, v3, v4}, Lf/h/a/c/a1/a0/d;->j(J)J

    move-result-wide v3

    invoke-virtual {v5, v3, v4}, Lf/h/a/c/i1/m;->a(J)V

    goto/16 :goto_1e

    :sswitch_1e
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->l:I

    goto/16 :goto_1e

    :sswitch_1f
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->M:I

    goto/16 :goto_1e

    :sswitch_20
    invoke-virtual {v8, v3, v4}, Lf/h/a/c/a1/a0/d;->j(J)J

    move-result-wide v3

    iput-wide v3, v8, Lf/h/a/c/a1/a0/d;->I:J

    goto/16 :goto_1e

    :sswitch_21
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    const-wide/16 v8, 0x1

    cmp-long v6, v3, v8

    if-nez v6, :cond_4a

    const/4 v3, 0x1

    goto :goto_1d

    :cond_4a
    const/4 v3, 0x0

    :goto_1d
    iput-boolean v3, v5, Lf/h/a/c/a1/a0/d$c;->T:Z

    goto/16 :goto_1e

    :sswitch_22
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->d:I

    goto/16 :goto_1e

    :pswitch_d
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->B:I

    goto/16 :goto_1e

    :pswitch_e
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    long-to-int v4, v3

    iput v4, v5, Lf/h/a/c/a1/a0/d$c;->A:I

    goto/16 :goto_1e

    :pswitch_f
    iget-object v5, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    const/4 v8, 0x1

    iput-boolean v8, v5, Lf/h/a/c/a1/a0/d$c;->w:Z

    long-to-int v4, v3

    if-eq v4, v8, :cond_4d

    const/16 v3, 0x9

    if-eq v4, v3, :cond_4c

    const/4 v3, 0x4

    if-eq v4, v3, :cond_4b

    if-eq v4, v11, :cond_4b

    const/4 v3, 0x6

    if-eq v4, v3, :cond_4b

    if-eq v4, v6, :cond_4b

    goto :goto_1e

    :cond_4b
    const/4 v3, 0x2

    iput v3, v5, Lf/h/a/c/a1/a0/d$c;->x:I

    goto :goto_1e

    :cond_4c
    const/4 v3, 0x6

    iput v3, v5, Lf/h/a/c/a1/a0/d$c;->x:I

    goto :goto_1e

    :cond_4d
    const/4 v3, 0x1

    iput v3, v5, Lf/h/a/c/a1/a0/d$c;->x:I

    goto :goto_1e

    :pswitch_10
    const/4 v9, 0x6

    const/4 v10, 0x1

    long-to-int v4, v3

    if-eq v4, v10, :cond_50

    const/16 v3, 0x10

    if-eq v4, v3, :cond_4f

    const/16 v3, 0x12

    if-eq v4, v3, :cond_4e

    if-eq v4, v9, :cond_50

    if-eq v4, v6, :cond_50

    goto :goto_1e

    :cond_4e
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v6, v3, Lf/h/a/c/a1/a0/d$c;->y:I

    goto :goto_1e

    :cond_4f
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v9, v3, Lf/h/a/c/a1/a0/d$c;->y:I

    goto :goto_1e

    :cond_50
    iget-object v3, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v5, v3, Lf/h/a/c/a1/a0/d$c;->y:I

    goto :goto_1e

    :pswitch_11
    long-to-int v4, v3

    const/4 v3, 0x1

    if-eq v4, v3, :cond_52

    const/4 v5, 0x2

    if-eq v4, v5, :cond_51

    goto :goto_1e

    :cond_51
    iget-object v4, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v3, v4, Lf/h/a/c/a1/a0/d$c;->z:I

    goto :goto_1e

    :cond_52
    const/4 v3, 0x2

    iget-object v4, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput v3, v4, Lf/h/a/c/a1/a0/d$c;->z:I

    goto :goto_1e

    :cond_53
    const-wide/16 v8, 0x1

    cmp-long v5, v3, v8

    if-nez v5, :cond_54

    goto :goto_1e

    :cond_54
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "ContentEncodingScope "

    invoke-static {v2, v3, v4, v6}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_55
    const-wide/16 v8, 0x0

    cmp-long v5, v3, v8

    if-nez v5, :cond_57

    :cond_56
    :goto_1e
    const/4 v3, 0x0

    iput v3, v7, Lf/h/a/c/a1/a0/a;->e:I

    goto/16 :goto_22

    :cond_57
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "ContentEncodingOrder "

    invoke-static {v2, v3, v4, v6}, Lf/e/c/a/a;->p(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_58
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Invalid integer size: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v7, Lf/h/a/c/a1/a0/a;->g:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_59
    iget-wide v3, v1, Lf/h/a/c/a1/e;->d:J

    iget-wide v8, v7, Lf/h/a/c/a1/a0/a;->g:J

    add-long/2addr v8, v3

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->b:Ljava/util/ArrayDeque;

    new-instance v10, Lf/h/a/c/a1/a0/a$b;

    iget v11, v7, Lf/h/a/c/a1/a0/a;->f:I

    const/4 v12, 0x0

    invoke-direct {v10, v11, v8, v9, v12}, Lf/h/a/c/a1/a0/a$b;-><init>(IJLf/h/a/c/a1/a0/a$a;)V

    invoke-virtual {v5, v10}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    iget-object v5, v7, Lf/h/a/c/a1/a0/a;->d:Lf/h/a/c/a1/a0/b;

    iget v8, v7, Lf/h/a/c/a1/a0/a;->f:I

    iget-wide v9, v7, Lf/h/a/c/a1/a0/a;->g:J

    check-cast v5, Lf/h/a/c/a1/a0/d$b;

    iget-object v5, v5, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v11, 0xa0

    if-eq v8, v11, :cond_66

    const/16 v11, 0xae

    if-eq v8, v11, :cond_65

    const/16 v11, 0xbb

    if-eq v8, v11, :cond_64

    const/16 v11, 0x4dbb

    if-eq v8, v11, :cond_62

    const/16 v6, 0x5035

    if-eq v8, v6, :cond_61

    const/16 v6, 0x55d0

    if-eq v8, v6, :cond_60

    const v6, 0x18538067

    if-eq v8, v6, :cond_5d

    const v3, 0x1c53bb6b

    if-eq v8, v3, :cond_5c

    const v3, 0x1f43b675

    if-eq v8, v3, :cond_5a

    goto :goto_20

    :cond_5a
    iget-boolean v3, v5, Lf/h/a/c/a1/a0/d;->v:Z

    if-nez v3, :cond_63

    iget-boolean v3, v5, Lf/h/a/c/a1/a0/d;->d:Z

    if-eqz v3, :cond_5b

    iget-wide v3, v5, Lf/h/a/c/a1/a0/d;->z:J

    const-wide/16 v8, -0x1

    cmp-long v6, v3, v8

    if-eqz v6, :cond_5b

    const/4 v3, 0x1

    iput-boolean v3, v5, Lf/h/a/c/a1/a0/d;->y:Z

    goto :goto_20

    :cond_5b
    const/4 v3, 0x1

    iget-object v4, v5, Lf/h/a/c/a1/a0/d;->a0:Lf/h/a/c/a1/i;

    new-instance v6, Lf/h/a/c/a1/q$b;

    iget-wide v8, v5, Lf/h/a/c/a1/a0/d;->t:J

    const-wide/16 v10, 0x0

    invoke-direct {v6, v8, v9, v10, v11}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {v4, v6}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    iput-boolean v3, v5, Lf/h/a/c/a1/a0/d;->v:Z

    goto :goto_20

    :cond_5c
    new-instance v3, Lf/h/a/c/i1/m;

    invoke-direct {v3}, Lf/h/a/c/i1/m;-><init>()V

    iput-object v3, v5, Lf/h/a/c/a1/a0/d;->C:Lf/h/a/c/i1/m;

    new-instance v3, Lf/h/a/c/i1/m;

    invoke-direct {v3}, Lf/h/a/c/i1/m;-><init>()V

    iput-object v3, v5, Lf/h/a/c/a1/a0/d;->D:Lf/h/a/c/i1/m;

    goto :goto_20

    :cond_5d
    iget-wide v11, v5, Lf/h/a/c/a1/a0/d;->q:J

    const-wide/16 v13, -0x1

    cmp-long v6, v11, v13

    if-eqz v6, :cond_5f

    cmp-long v6, v11, v3

    if-nez v6, :cond_5e

    goto :goto_1f

    :cond_5e
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Multiple Segment elements not supported"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5f
    :goto_1f
    iput-wide v3, v5, Lf/h/a/c/a1/a0/d;->q:J

    iput-wide v9, v5, Lf/h/a/c/a1/a0/d;->p:J

    goto :goto_20

    :cond_60
    iget-object v3, v5, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lf/h/a/c/a1/a0/d$c;->w:Z

    goto :goto_20

    :cond_61
    const/4 v3, 0x1

    iget-object v4, v5, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iput-boolean v3, v4, Lf/h/a/c/a1/a0/d$c;->g:Z

    goto :goto_20

    :cond_62
    iput v6, v5, Lf/h/a/c/a1/a0/d;->w:I

    const-wide/16 v3, -0x1

    iput-wide v3, v5, Lf/h/a/c/a1/a0/d;->x:J

    :cond_63
    :goto_20
    const/4 v3, 0x0

    goto :goto_21

    :cond_64
    const/4 v3, 0x0

    iput-boolean v3, v5, Lf/h/a/c/a1/a0/d;->E:Z

    goto :goto_21

    :cond_65
    const/4 v3, 0x0

    new-instance v4, Lf/h/a/c/a1/a0/d$c;

    invoke-direct {v4, v12}, Lf/h/a/c/a1/a0/d$c;-><init>(Lf/h/a/c/a1/a0/d$a;)V

    iput-object v4, v5, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    goto :goto_21

    :cond_66
    const/4 v3, 0x0

    iput-boolean v3, v5, Lf/h/a/c/a1/a0/d;->Q:Z

    :goto_21
    iput v3, v7, Lf/h/a/c/a1/a0/a;->e:I

    :goto_22
    const/4 v3, 0x1

    const/4 v5, 0x1

    :goto_23
    if-eqz v5, :cond_69

    iget-wide v3, v1, Lf/h/a/c/a1/e;->d:J

    iget-boolean v6, v0, Lf/h/a/c/a1/a0/d;->y:Z

    if-eqz v6, :cond_67

    iput-wide v3, v0, Lf/h/a/c/a1/a0/d;->A:J

    iget-wide v3, v0, Lf/h/a/c/a1/a0/d;->z:J

    iput-wide v3, v2, Lf/h/a/c/a1/p;->a:J

    const/4 v3, 0x0

    iput-boolean v3, v0, Lf/h/a/c/a1/a0/d;->y:Z

    goto :goto_24

    :cond_67
    iget-boolean v3, v0, Lf/h/a/c/a1/a0/d;->v:Z

    if-eqz v3, :cond_68

    iget-wide v3, v0, Lf/h/a/c/a1/a0/d;->A:J

    const-wide/16 v6, -0x1

    cmp-long v8, v3, v6

    if-eqz v8, :cond_68

    iput-wide v3, v2, Lf/h/a/c/a1/p;->a:J

    iput-wide v6, v0, Lf/h/a/c/a1/a0/d;->A:J

    :goto_24
    const/4 v3, 0x1

    goto :goto_25

    :cond_68
    const/4 v3, 0x0

    :goto_25
    if-eqz v3, :cond_69

    const/4 v1, 0x1

    return v1

    :cond_69
    const/4 v3, 0x0

    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_6a
    iget-wide v3, v7, Lf/h/a/c/a1/a0/a;->g:J

    long-to-int v4, v3

    invoke-virtual {v1, v4}, Lf/h/a/c/a1/e;->i(I)V

    const/4 v3, 0x0

    iput v3, v7, Lf/h/a/c/a1/a0/a;->e:I

    const/4 v4, 0x1

    goto/16 :goto_1

    :cond_6b
    if-nez v5, :cond_6e

    const/4 v1, 0x0

    :goto_26
    iget-object v2, v0, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_6d

    iget-object v2, v0, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/a1/a0/d$c;

    iget-object v3, v2, Lf/h/a/c/a1/a0/d$c;->R:Lf/h/a/c/a1/a0/d$d;

    if-eqz v3, :cond_6c

    invoke-virtual {v3, v2}, Lf/h/a/c/a1/a0/d$d;->a(Lf/h/a/c/a1/a0/d$c;)V

    :cond_6c
    add-int/lit8 v1, v1, 0x1

    goto :goto_26

    :cond_6d
    return v6

    :cond_6e
    const/4 v1, 0x0

    return v1

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_4
        0x86 -> :sswitch_3
        0x88 -> :sswitch_4
        0x9b -> :sswitch_4
        0x9f -> :sswitch_4
        0xa0 -> :sswitch_2
        0xa1 -> :sswitch_1
        0xa3 -> :sswitch_1
        0xa5 -> :sswitch_1
        0xa6 -> :sswitch_2
        0xae -> :sswitch_2
        0xb0 -> :sswitch_4
        0xb3 -> :sswitch_4
        0xb5 -> :sswitch_0
        0xb7 -> :sswitch_2
        0xba -> :sswitch_4
        0xbb -> :sswitch_2
        0xd7 -> :sswitch_4
        0xe0 -> :sswitch_2
        0xe1 -> :sswitch_2
        0xe7 -> :sswitch_4
        0xee -> :sswitch_4
        0xf1 -> :sswitch_4
        0xfb -> :sswitch_4
        0x4254 -> :sswitch_4
        0x4255 -> :sswitch_1
        0x4282 -> :sswitch_3
        0x4285 -> :sswitch_4
        0x42f7 -> :sswitch_4
        0x4489 -> :sswitch_0
        0x47e1 -> :sswitch_4
        0x47e2 -> :sswitch_1
        0x47e7 -> :sswitch_2
        0x47e8 -> :sswitch_4
        0x4dbb -> :sswitch_2
        0x5031 -> :sswitch_4
        0x5032 -> :sswitch_4
        0x5034 -> :sswitch_2
        0x5035 -> :sswitch_2
        0x536e -> :sswitch_3
        0x53ab -> :sswitch_1
        0x53ac -> :sswitch_4
        0x53b8 -> :sswitch_4
        0x54b0 -> :sswitch_4
        0x54b2 -> :sswitch_4
        0x54ba -> :sswitch_4
        0x55aa -> :sswitch_4
        0x55b0 -> :sswitch_2
        0x55b9 -> :sswitch_4
        0x55ba -> :sswitch_4
        0x55bb -> :sswitch_4
        0x55bc -> :sswitch_4
        0x55bd -> :sswitch_4
        0x55d0 -> :sswitch_2
        0x55d1 -> :sswitch_0
        0x55d2 -> :sswitch_0
        0x55d3 -> :sswitch_0
        0x55d4 -> :sswitch_0
        0x55d5 -> :sswitch_0
        0x55d6 -> :sswitch_0
        0x55d7 -> :sswitch_0
        0x55d8 -> :sswitch_0
        0x55d9 -> :sswitch_0
        0x55da -> :sswitch_0
        0x55ee -> :sswitch_4
        0x56aa -> :sswitch_4
        0x56bb -> :sswitch_4
        0x6240 -> :sswitch_2
        0x6264 -> :sswitch_4
        0x63a2 -> :sswitch_1
        0x6d80 -> :sswitch_2
        0x75a1 -> :sswitch_2
        0x7670 -> :sswitch_2
        0x7671 -> :sswitch_4
        0x7672 -> :sswitch_1
        0x7673 -> :sswitch_0
        0x7674 -> :sswitch_0
        0x7675 -> :sswitch_0
        0x22b59c -> :sswitch_3
        0x23e383 -> :sswitch_4
        0x2ad7b1 -> :sswitch_4
        0x114d9b74 -> :sswitch_2
        0x1549a966 -> :sswitch_2
        0x1654ae6b -> :sswitch_2
        0x18538067 -> :sswitch_2
        0x1a45dfa3 -> :sswitch_2
        0x1c53bb6b -> :sswitch_2
        0x1f43b675 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x55d1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7673
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x83 -> :sswitch_22
        0x88 -> :sswitch_21
        0x9b -> :sswitch_20
        0x9f -> :sswitch_1f
        0xb0 -> :sswitch_1e
        0xb3 -> :sswitch_1d
        0xba -> :sswitch_1c
        0xd7 -> :sswitch_1b
        0xe7 -> :sswitch_1a
        0xee -> :sswitch_19
        0xf1 -> :sswitch_18
        0xfb -> :sswitch_17
        0x4254 -> :sswitch_16
        0x4285 -> :sswitch_15
        0x42f7 -> :sswitch_14
        0x47e1 -> :sswitch_13
        0x47e8 -> :sswitch_12
        0x53ac -> :sswitch_11
        0x53b8 -> :sswitch_10
        0x54b0 -> :sswitch_f
        0x54b2 -> :sswitch_e
        0x54ba -> :sswitch_d
        0x55aa -> :sswitch_c
        0x55ee -> :sswitch_b
        0x56aa -> :sswitch_a
        0x56bb -> :sswitch_9
        0x6264 -> :sswitch_8
        0x7671 -> :sswitch_7
        0x23e383 -> :sswitch_6
        0x2ad7b1 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x55b9
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
    .end packed-switch
.end method

.method public final e(Lf/h/a/c/a1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/a0/d;->a0:Lf/h/a/c/a1/i;

    return-void
.end method

.method public f(JJ)V
    .locals 0
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lf/h/a/c/a1/a0/d;->B:J

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/a1/a0/d;->G:I

    iget-object p2, p0, Lf/h/a/c/a1/a0/d;->a:Lf/h/a/c/a1/a0/c;

    check-cast p2, Lf/h/a/c/a1/a0/a;

    iput p1, p2, Lf/h/a/c/a1/a0/a;->e:I

    iget-object p3, p2, Lf/h/a/c/a1/a0/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {p3}, Ljava/util/ArrayDeque;->clear()V

    iget-object p2, p2, Lf/h/a/c/a1/a0/a;->c:Lf/h/a/c/a1/a0/f;

    iput p1, p2, Lf/h/a/c/a1/a0/f;->b:I

    iput p1, p2, Lf/h/a/c/a1/a0/f;->c:I

    iget-object p2, p0, Lf/h/a/c/a1/a0/d;->b:Lf/h/a/c/a1/a0/f;

    iput p1, p2, Lf/h/a/c/a1/a0/f;->b:I

    iput p1, p2, Lf/h/a/c/a1/a0/f;->c:I

    invoke-virtual {p0}, Lf/h/a/c/a1/a0/d;->i()V

    const/4 p2, 0x0

    :goto_0
    iget-object p3, p0, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result p3

    if-ge p2, p3, :cond_1

    iget-object p3, p0, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {p3, p2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/h/a/c/a1/a0/d$c;

    iget-object p3, p3, Lf/h/a/c/a1/a0/d$c;->R:Lf/h/a/c/a1/a0/d$d;

    if-eqz p3, :cond_0

    iput-boolean p1, p3, Lf/h/a/c/a1/a0/d$d;->b:Z

    iput p1, p3, Lf/h/a/c/a1/a0/d$d;->c:I

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final g(Lf/h/a/c/a1/e;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget v1, v0, Lf/h/a/c/i1/r;->c:I

    if-lt v1, p2, :cond_0

    return-void

    :cond_0
    iget-object v1, v0, Lf/h/a/c/i1/r;->a:[B

    array-length v2, v1

    if-ge v2, p2, :cond_1

    array-length v2, v1

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iget-object v2, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget v2, v2, Lf/h/a/c/i1/r;->c:I

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/i1/r;->A([BI)V

    :cond_1
    iget-object v0, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v1, v0, Lf/h/a/c/i1/r;->a:[B

    iget v0, v0, Lf/h/a/c/i1/r;->c:I

    sub-int v2, p2, v0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v0, v2, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object p1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-virtual {p1, p2}, Lf/h/a/c/i1/r;->B(I)V

    return-void
.end method

.method public final h(Lf/h/a/c/a1/e;)Z
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p1

    new-instance v1, Lf/h/a/c/a1/a0/e;

    invoke-direct {v1}, Lf/h/a/c/a1/a0/e;-><init>()V

    iget-wide v2, v0, Lf/h/a/c/a1/e;->c:J

    const-wide/16 v4, -0x1

    const-wide/16 v6, 0x400

    cmp-long v8, v2, v4

    if-eqz v8, :cond_1

    cmp-long v4, v2, v6

    if-lez v4, :cond_0

    goto :goto_0

    :cond_0
    move-wide v6, v2

    :cond_1
    :goto_0
    long-to-int v4, v6

    iget-object v5, v1, Lf/h/a/c/a1/a0/e;->a:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-virtual {v0, v5, v6, v7, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v5, v1, Lf/h/a/c/a1/a0/e;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v9

    iput v7, v1, Lf/h/a/c/a1/a0/e;->b:I

    :goto_1
    const-wide/32 v11, 0x1a45dfa3

    const/4 v5, 0x1

    cmp-long v7, v9, v11

    if-eqz v7, :cond_3

    iget v7, v1, Lf/h/a/c/a1/a0/e;->b:I

    add-int/2addr v7, v5

    iput v7, v1, Lf/h/a/c/a1/a0/e;->b:I

    if-ne v7, v4, :cond_2

    goto :goto_3

    :cond_2
    iget-object v7, v1, Lf/h/a/c/a1/a0/e;->a:Lf/h/a/c/i1/r;

    iget-object v7, v7, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v0, v7, v6, v5, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    const/16 v5, 0x8

    shl-long/2addr v9, v5

    const-wide/16 v11, -0x100

    and-long/2addr v9, v11

    iget-object v5, v1, Lf/h/a/c/a1/a0/e;->a:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    int-to-long v11, v5

    or-long/2addr v9, v11

    goto :goto_1

    :cond_3
    invoke-virtual {v1, v0}, Lf/h/a/c/a1/a0/e;->a(Lf/h/a/c/a1/e;)J

    move-result-wide v9

    iget v4, v1, Lf/h/a/c/a1/a0/e;->b:I

    int-to-long v11, v4

    const-wide/high16 v13, -0x8000000000000000L

    cmp-long v4, v9, v13

    if-eqz v4, :cond_8

    if-eqz v8, :cond_4

    add-long v7, v11, v9

    cmp-long v4, v7, v2

    if-ltz v4, :cond_4

    goto :goto_3

    :cond_4
    :goto_2
    iget v2, v1, Lf/h/a/c/a1/a0/e;->b:I

    int-to-long v2, v2

    add-long v7, v11, v9

    cmp-long v4, v2, v7

    if-gez v4, :cond_7

    invoke-virtual {v1, v0}, Lf/h/a/c/a1/a0/e;->a(Lf/h/a/c/a1/e;)J

    move-result-wide v2

    cmp-long v4, v2, v13

    if-nez v4, :cond_5

    goto :goto_3

    :cond_5
    invoke-virtual {v1, v0}, Lf/h/a/c/a1/a0/e;->a(Lf/h/a/c/a1/e;)J

    move-result-wide v2

    const-wide/16 v7, 0x0

    cmp-long v4, v2, v7

    if-ltz v4, :cond_8

    const-wide/32 v7, 0x7fffffff

    cmp-long v15, v2, v7

    if-lez v15, :cond_6

    goto :goto_3

    :cond_6
    if-eqz v4, :cond_4

    long-to-int v3, v2

    invoke-virtual {v0, v3, v6}, Lf/h/a/c/a1/e;->a(IZ)Z

    iget v2, v1, Lf/h/a/c/a1/a0/e;->b:I

    add-int/2addr v2, v3

    iput v2, v1, Lf/h/a/c/a1/a0/e;->b:I

    goto :goto_2

    :cond_7
    if-nez v4, :cond_8

    const/4 v6, 0x1

    :cond_8
    :goto_3
    return v6
.end method

.method public final i()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/a0/d;->R:I

    iput v0, p0, Lf/h/a/c/a1/a0/d;->S:I

    iput v0, p0, Lf/h/a/c/a1/a0/d;->T:I

    iput-boolean v0, p0, Lf/h/a/c/a1/a0/d;->U:Z

    iput-boolean v0, p0, Lf/h/a/c/a1/a0/d;->V:Z

    iput-boolean v0, p0, Lf/h/a/c/a1/a0/d;->W:Z

    iput v0, p0, Lf/h/a/c/a1/a0/d;->X:I

    iput-byte v0, p0, Lf/h/a/c/a1/a0/d;->Y:B

    iput-boolean v0, p0, Lf/h/a/c/a1/a0/d;->Z:Z

    iget-object v0, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->x()V

    return-void
.end method

.method public final j(J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    iget-wide v2, p0, Lf/h/a/c/a1/a0/d;->r:J

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x3e8

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide p1

    return-wide p1

    :cond_0
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "Can\'t scale timecode prior to timecodeScale being set."

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final k(Lf/h/a/c/a1/e;Lf/h/a/c/a1/a0/d$c;I)I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p2, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string v1, "S_TEXT/UTF8"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p2, Lf/h/a/c/a1/a0/d;->b0:[B

    invoke-virtual {p0, p1, p2, p3}, Lf/h/a/c/a1/a0/d;->l(Lf/h/a/c/a1/e;[BI)V

    iget p1, p0, Lf/h/a/c/a1/a0/d;->S:I

    invoke-virtual {p0}, Lf/h/a/c/a1/a0/d;->i()V

    return p1

    :cond_0
    iget-object v0, p2, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string v1, "S_TEXT/ASS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p2, Lf/h/a/c/a1/a0/d;->d0:[B

    invoke-virtual {p0, p1, p2, p3}, Lf/h/a/c/a1/a0/d;->l(Lf/h/a/c/a1/e;[BI)V

    iget p1, p0, Lf/h/a/c/a1/a0/d;->S:I

    invoke-virtual {p0}, Lf/h/a/c/a1/a0/d;->i()V

    return p1

    :cond_1
    iget-object v0, p2, Lf/h/a/c/a1/a0/d$c;->V:Lf/h/a/c/a1/s;

    iget-boolean v1, p0, Lf/h/a/c/a1/a0/d;->U:Z

    const/4 v2, 0x2

    const/16 v3, 0x8

    const/4 v4, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-nez v1, :cond_11

    iget-boolean v1, p2, Lf/h/a/c/a1/a0/d$c;->g:Z

    if-eqz v1, :cond_e

    iget v1, p0, Lf/h/a/c/a1/a0/d;->O:I

    const v7, -0x40000001    # -1.9999999f

    and-int/2addr v1, v7

    iput v1, p0, Lf/h/a/c/a1/a0/d;->O:I

    iget-boolean v1, p0, Lf/h/a/c/a1/a0/d;->V:Z

    const/16 v7, 0x80

    if-nez v1, :cond_3

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v1, v6, v5, v6}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v1, p0, Lf/h/a/c/a1/a0/d;->R:I

    add-int/2addr v1, v5

    iput v1, p0, Lf/h/a/c/a1/a0/d;->R:I

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    aget-byte v8, v1, v6

    and-int/2addr v8, v7

    if-eq v8, v7, :cond_2

    aget-byte v1, v1, v6

    iput-byte v1, p0, Lf/h/a/c/a1/a0/d;->Y:B

    iput-boolean v5, p0, Lf/h/a/c/a1/a0/d;->V:Z

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/google/android/exoplayer2/ParserException;

    const-string p2, "Extension bit is set in signal byte"

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    iget-byte v1, p0, Lf/h/a/c/a1/a0/d;->Y:B

    and-int/lit8 v8, v1, 0x1

    if-ne v8, v5, :cond_4

    const/4 v8, 0x1

    goto :goto_1

    :cond_4
    const/4 v8, 0x0

    :goto_1
    if-eqz v8, :cond_f

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    :goto_2
    iget v8, p0, Lf/h/a/c/a1/a0/d;->O:I

    const/high16 v9, 0x40000000    # 2.0f

    or-int/2addr v8, v9

    iput v8, p0, Lf/h/a/c/a1/a0/d;->O:I

    iget-boolean v8, p0, Lf/h/a/c/a1/a0/d;->Z:Z

    if-nez v8, :cond_7

    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->l:Lf/h/a/c/i1/r;

    iget-object v8, v8, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v8, v6, v3, v6}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v8, p0, Lf/h/a/c/a1/a0/d;->R:I

    add-int/2addr v8, v3

    iput v8, p0, Lf/h/a/c/a1/a0/d;->R:I

    iput-boolean v5, p0, Lf/h/a/c/a1/a0/d;->Z:Z

    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v9, v8, Lf/h/a/c/i1/r;->a:[B

    if-eqz v1, :cond_6

    goto :goto_3

    :cond_6
    const/4 v7, 0x0

    :goto_3
    or-int/2addr v7, v3

    int-to-byte v7, v7

    aput-byte v7, v9, v6

    invoke-virtual {v8, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v7, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-interface {v0, v7, v5}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v7, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr v7, v5

    iput v7, p0, Lf/h/a/c/a1/a0/d;->S:I

    iget-object v7, p0, Lf/h/a/c/a1/a0/d;->l:Lf/h/a/c/i1/r;

    invoke-virtual {v7, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v7, p0, Lf/h/a/c/a1/a0/d;->l:Lf/h/a/c/i1/r;

    invoke-interface {v0, v7, v3}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v7, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr v7, v3

    iput v7, p0, Lf/h/a/c/a1/a0/d;->S:I

    :cond_7
    if-eqz v1, :cond_f

    iget-boolean v1, p0, Lf/h/a/c/a1/a0/d;->W:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v1, v6, v5, v6}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v1, p0, Lf/h/a/c/a1/a0/d;->R:I

    add-int/2addr v1, v5

    iput v1, p0, Lf/h/a/c/a1/a0/d;->R:I

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v1, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->q()I

    move-result v1

    iput v1, p0, Lf/h/a/c/a1/a0/d;->X:I

    iput-boolean v5, p0, Lf/h/a/c/a1/a0/d;->W:Z

    :cond_8
    iget v1, p0, Lf/h/a/c/a1/a0/d;->X:I

    mul-int/lit8 v1, v1, 0x4

    iget-object v7, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v7, v1}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v7, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v7, v7, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v7, v6, v1, v6}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v7, p0, Lf/h/a/c/a1/a0/d;->R:I

    add-int/2addr v7, v1

    iput v7, p0, Lf/h/a/c/a1/a0/d;->R:I

    iget v1, p0, Lf/h/a/c/a1/a0/d;->X:I

    div-int/2addr v1, v2

    add-int/2addr v1, v5

    int-to-short v1, v1

    mul-int/lit8 v7, v1, 0x6

    add-int/2addr v7, v2

    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    if-eqz v8, :cond_9

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v8

    if-ge v8, v7, :cond_a

    :cond_9
    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    iput-object v8, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    :cond_a
    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    const/4 v8, 0x0

    :goto_4
    iget v9, p0, Lf/h/a/c/a1/a0/d;->X:I

    if-ge v1, v9, :cond_c

    iget-object v9, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->t()I

    move-result v9

    rem-int/lit8 v10, v1, 0x2

    if-nez v10, :cond_b

    iget-object v10, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    sub-int v8, v9, v8

    int-to-short v8, v8

    invoke-virtual {v10, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    goto :goto_5

    :cond_b
    iget-object v10, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    sub-int v8, v9, v8

    invoke-virtual {v10, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_5
    add-int/lit8 v1, v1, 0x1

    move v8, v9

    goto :goto_4

    :cond_c
    iget v1, p0, Lf/h/a/c/a1/a0/d;->R:I

    sub-int v1, p3, v1

    sub-int/2addr v1, v8

    rem-int/2addr v9, v2

    if-ne v9, v5, :cond_d

    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_6

    :cond_d
    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    int-to-short v1, v1

    invoke-virtual {v8, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :goto_6
    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->m:Lf/h/a/c/i1/r;

    iget-object v8, p0, Lf/h/a/c/a1/a0/d;->o:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    invoke-virtual {v1, v8, v7}, Lf/h/a/c/i1/r;->A([BI)V

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->m:Lf/h/a/c/i1/r;

    invoke-interface {v0, v1, v7}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v1, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr v1, v7

    iput v1, p0, Lf/h/a/c/a1/a0/d;->S:I

    goto :goto_7

    :cond_e
    iget-object v1, p2, Lf/h/a/c/a1/a0/d$c;->h:[B

    if-eqz v1, :cond_f

    iget-object v7, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    array-length v8, v1

    iput-object v1, v7, Lf/h/a/c/i1/r;->a:[B

    iput v8, v7, Lf/h/a/c/i1/r;->c:I

    iput v6, v7, Lf/h/a/c/i1/r;->b:I

    :cond_f
    :goto_7
    iget v1, p2, Lf/h/a/c/a1/a0/d$c;->f:I

    if-lez v1, :cond_10

    iget v1, p0, Lf/h/a/c/a1/a0/d;->O:I

    const/high16 v7, 0x10000000

    or-int/2addr v1, v7

    iput v1, p0, Lf/h/a/c/a1/a0/d;->O:I

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->n:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->x()V

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v1, v4}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->g:Lf/h/a/c/i1/r;

    iget-object v7, v1, Lf/h/a/c/i1/r;->a:[B

    shr-int/lit8 v8, p3, 0x18

    and-int/lit16 v8, v8, 0xff

    int-to-byte v8, v8

    aput-byte v8, v7, v6

    shr-int/lit8 v8, p3, 0x10

    and-int/lit16 v8, v8, 0xff

    int-to-byte v8, v8

    aput-byte v8, v7, v5

    shr-int/lit8 v8, p3, 0x8

    and-int/lit16 v8, v8, 0xff

    int-to-byte v8, v8

    aput-byte v8, v7, v2

    const/4 v8, 0x3

    and-int/lit16 v9, p3, 0xff

    int-to-byte v9, v9

    aput-byte v9, v7, v8

    invoke-interface {v0, v1, v4}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v1, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr v1, v4

    iput v1, p0, Lf/h/a/c/a1/a0/d;->S:I

    :cond_10
    iput-boolean v5, p0, Lf/h/a/c/a1/a0/d;->U:Z

    :cond_11
    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    iget v1, v1, Lf/h/a/c/i1/r;->c:I

    add-int/2addr p3, v1

    iget-object v1, p2, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string v7, "V_MPEG4/ISO/AVC"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    iget-object v1, p2, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string v7, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    goto/16 :goto_d

    :cond_12
    iget-object v1, p2, Lf/h/a/c/a1/a0/d$c;->R:Lf/h/a/c/a1/a0/d$d;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    iget v1, v1, Lf/h/a/c/i1/r;->c:I

    if-nez v1, :cond_13

    const/4 v1, 0x1

    goto :goto_8

    :cond_13
    const/4 v1, 0x0

    :goto_8
    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iget-object v1, p2, Lf/h/a/c/a1/a0/d$c;->R:Lf/h/a/c/a1/a0/d$d;

    iget-boolean v2, v1, Lf/h/a/c/a1/a0/d$d;->b:Z

    if-eqz v2, :cond_14

    goto :goto_c

    :cond_14
    iget-object v2, v1, Lf/h/a/c/a1/a0/d$d;->a:[B

    const/16 v7, 0xa

    invoke-virtual {p1, v2, v6, v7, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iput v6, p1, Lf/h/a/c/a1/e;->f:I

    iget-object v2, v1, Lf/h/a/c/a1/a0/d$d;->a:[B

    aget-byte v7, v2, v4

    const/4 v8, -0x8

    if-ne v7, v8, :cond_18

    const/4 v7, 0x5

    aget-byte v7, v2, v7

    const/16 v8, 0x72

    if-ne v7, v8, :cond_18

    const/4 v7, 0x6

    aget-byte v7, v2, v7

    const/16 v8, 0x6f

    if-ne v7, v8, :cond_18

    const/4 v7, 0x7

    aget-byte v8, v2, v7

    and-int/lit16 v8, v8, 0xfe

    const/16 v9, 0xba

    if-eq v8, v9, :cond_15

    goto :goto_a

    :cond_15
    aget-byte v8, v2, v7

    and-int/lit16 v8, v8, 0xff

    const/16 v9, 0xbb

    if-ne v8, v9, :cond_16

    const/4 v8, 0x1

    goto :goto_9

    :cond_16
    const/4 v8, 0x0

    :goto_9
    const/16 v9, 0x28

    if-eqz v8, :cond_17

    const/16 v3, 0x9

    :cond_17
    aget-byte v2, v2, v3

    shr-int/2addr v2, v4

    and-int/2addr v2, v7

    shl-int v2, v9, v2

    goto :goto_b

    :cond_18
    :goto_a
    const/4 v2, 0x0

    :goto_b
    if-nez v2, :cond_19

    goto :goto_c

    :cond_19
    iput-boolean v5, v1, Lf/h/a/c/a1/a0/d$d;->b:Z

    :cond_1a
    :goto_c
    iget v1, p0, Lf/h/a/c/a1/a0/d;->R:I

    if-ge v1, p3, :cond_1e

    sub-int v1, p3, v1

    invoke-virtual {p0, p1, v0, v1}, Lf/h/a/c/a1/a0/d;->m(Lf/h/a/c/a1/e;Lf/h/a/c/a1/s;I)I

    move-result v1

    iget v2, p0, Lf/h/a/c/a1/a0/d;->R:I

    add-int/2addr v2, v1

    iput v2, p0, Lf/h/a/c/a1/a0/d;->R:I

    iget v2, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr v2, v1

    iput v2, p0, Lf/h/a/c/a1/a0/d;->S:I

    goto :goto_c

    :cond_1b
    :goto_d
    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->f:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    aput-byte v6, v1, v6

    aput-byte v6, v1, v5

    aput-byte v6, v1, v2

    iget v2, p2, Lf/h/a/c/a1/a0/d$c;->W:I

    rsub-int/lit8 v3, v2, 0x4

    :goto_e
    iget v5, p0, Lf/h/a/c/a1/a0/d;->R:I

    if-ge v5, p3, :cond_1e

    iget v5, p0, Lf/h/a/c/a1/a0/d;->T:I

    if-nez v5, :cond_1d

    iget-object v5, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->a()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    add-int v7, v3, v5

    sub-int v8, v2, v5

    invoke-virtual {p1, v1, v7, v8, v6}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    if-lez v5, :cond_1c

    iget-object v7, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    iget-object v8, v7, Lf/h/a/c/i1/r;->a:[B

    iget v9, v7, Lf/h/a/c/i1/r;->b:I

    invoke-static {v8, v9, v1, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v8, v7, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v8, v5

    iput v8, v7, Lf/h/a/c/i1/r;->b:I

    :cond_1c
    iget v5, p0, Lf/h/a/c/a1/a0/d;->R:I

    add-int/2addr v5, v2

    iput v5, p0, Lf/h/a/c/a1/a0/d;->R:I

    iget-object v5, p0, Lf/h/a/c/a1/a0/d;->f:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v5, p0, Lf/h/a/c/a1/a0/d;->f:Lf/h/a/c/i1/r;

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->t()I

    move-result v5

    iput v5, p0, Lf/h/a/c/a1/a0/d;->T:I

    iget-object v5, p0, Lf/h/a/c/a1/a0/d;->e:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v5, p0, Lf/h/a/c/a1/a0/d;->e:Lf/h/a/c/i1/r;

    invoke-interface {v0, v5, v4}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v5, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr v5, v4

    iput v5, p0, Lf/h/a/c/a1/a0/d;->S:I

    goto :goto_e

    :cond_1d
    invoke-virtual {p0, p1, v0, v5}, Lf/h/a/c/a1/a0/d;->m(Lf/h/a/c/a1/e;Lf/h/a/c/a1/s;I)I

    move-result v5

    iget v7, p0, Lf/h/a/c/a1/a0/d;->R:I

    add-int/2addr v7, v5

    iput v7, p0, Lf/h/a/c/a1/a0/d;->R:I

    iget v7, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr v7, v5

    iput v7, p0, Lf/h/a/c/a1/a0/d;->S:I

    iget v7, p0, Lf/h/a/c/a1/a0/d;->T:I

    sub-int/2addr v7, v5

    iput v7, p0, Lf/h/a/c/a1/a0/d;->T:I

    goto :goto_e

    :cond_1e
    iget-object p1, p2, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string p2, "A_VORBIS"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1f

    iget-object p1, p0, Lf/h/a/c/a1/a0/d;->h:Lf/h/a/c/i1/r;

    invoke-virtual {p1, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object p1, p0, Lf/h/a/c/a1/a0/d;->h:Lf/h/a/c/i1/r;

    invoke-interface {v0, p1, v4}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget p1, p0, Lf/h/a/c/a1/a0/d;->S:I

    add-int/2addr p1, v4

    iput p1, p0, Lf/h/a/c/a1/a0/d;->S:I

    :cond_1f
    iget p1, p0, Lf/h/a/c/a1/a0/d;->S:I

    invoke-virtual {p0}, Lf/h/a/c/a1/a0/d;->i()V

    return p1
.end method

.method public final l(Lf/h/a/c/a1/e;[BI)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    array-length v0, p2

    add-int/2addr v0, p3

    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->k:Lf/h/a/c/i1/r;

    iget-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    array-length v3, v2

    const/4 v4, 0x0

    if-ge v3, v0, :cond_0

    add-int v2, v0, p3

    invoke-static {p2, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    goto :goto_0

    :cond_0
    array-length v1, p2

    invoke-static {p2, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    iget-object v1, p0, Lf/h/a/c/a1/a0/d;->k:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    array-length p2, p2

    invoke-virtual {p1, v1, p2, p3, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object p1, p0, Lf/h/a/c/a1/a0/d;->k:Lf/h/a/c/i1/r;

    invoke-virtual {p1, v0}, Lf/h/a/c/i1/r;->y(I)V

    return-void
.end method

.method public final m(Lf/h/a/c/a1/e;Lf/h/a/c/a1/s;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iget-object p3, p0, Lf/h/a/c/a1/a0/d;->j:Lf/h/a/c/i1/r;

    invoke-interface {p2, p3, p1}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, p1, p3, v0}, Lf/h/a/c/a1/s;->a(Lf/h/a/c/a1/e;IZ)I

    move-result p1

    :goto_0
    return p1
.end method

.method public final release()V
    .locals 0

    return-void
.end method
