.class public final Lf/h/a/c/a1/a0/d$b;
.super Ljava/lang/Object;
.source "MatroskaExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/a0/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/a0/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lf/h/a/c/a1/a0/d;


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/a0/d;Lf/h/a/c/a1/a0/d$a;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 48
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p1

    iget-object v8, v0, Lf/h/a/c/a1/a0/d$b;->a:Lf/h/a/c/a1/a0/d;

    invoke-static {v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xa0

    const/4 v3, 0x0

    if-eq v1, v2, :cond_65

    const/16 v2, 0xae

    const/4 v4, -0x1

    if-eq v1, v2, :cond_10

    const/16 v2, 0x4dbb

    const v5, 0x1c53bb6b

    const-wide/16 v6, -0x1

    if-eq v1, v2, :cond_e

    const/16 v2, 0x6240

    if-eq v1, v2, :cond_c

    const/16 v2, 0x6d80

    if-eq v1, v2, :cond_a

    const v2, 0x1549a966

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    if-eq v1, v2, :cond_8

    const v2, 0x1654ae6b

    if-eq v1, v2, :cond_6

    if-eq v1, v5, :cond_0

    goto/16 :goto_2c

    :cond_0
    iget-boolean v1, v8, Lf/h/a/c/a1/a0/d;->v:Z

    if-nez v1, :cond_6a

    iget-object v1, v8, Lf/h/a/c/a1/a0/d;->a0:Lf/h/a/c/a1/i;

    iget-wide v4, v8, Lf/h/a/c/a1/a0/d;->q:J

    cmp-long v2, v4, v6

    if-eqz v2, :cond_5

    iget-wide v4, v8, Lf/h/a/c/a1/a0/d;->t:J

    cmp-long v2, v4, v9

    if-eqz v2, :cond_5

    iget-object v2, v8, Lf/h/a/c/a1/a0/d;->C:Lf/h/a/c/i1/m;

    if-eqz v2, :cond_5

    iget v2, v2, Lf/h/a/c/i1/m;->a:I

    if-eqz v2, :cond_5

    iget-object v4, v8, Lf/h/a/c/a1/a0/d;->D:Lf/h/a/c/i1/m;

    if-eqz v4, :cond_5

    iget v4, v4, Lf/h/a/c/i1/m;->a:I

    if-eq v4, v2, :cond_1

    goto/16 :goto_2

    :cond_1
    new-array v4, v2, [I

    new-array v5, v2, [J

    new-array v6, v2, [J

    new-array v7, v2, [J

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v2, :cond_2

    iget-object v10, v8, Lf/h/a/c/a1/a0/d;->C:Lf/h/a/c/i1/m;

    invoke-virtual {v10, v9}, Lf/h/a/c/i1/m;->b(I)J

    move-result-wide v10

    aput-wide v10, v7, v9

    iget-wide v10, v8, Lf/h/a/c/a1/a0/d;->q:J

    iget-object v12, v8, Lf/h/a/c/a1/a0/d;->D:Lf/h/a/c/i1/m;

    invoke-virtual {v12, v9}, Lf/h/a/c/i1/m;->b(I)J

    move-result-wide v12

    add-long/2addr v12, v10

    aput-wide v12, v5, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    add-int/lit8 v9, v2, -0x1

    if-ge v3, v9, :cond_3

    add-int/lit8 v9, v3, 0x1

    aget-wide v10, v5, v9

    aget-wide v12, v5, v3

    sub-long/2addr v10, v12

    long-to-int v11, v10

    aput v11, v4, v3

    aget-wide v10, v7, v9

    aget-wide v12, v7, v3

    sub-long/2addr v10, v12

    aput-wide v10, v6, v3

    move v3, v9

    goto :goto_1

    :cond_3
    iget-wide v2, v8, Lf/h/a/c/a1/a0/d;->q:J

    iget-wide v10, v8, Lf/h/a/c/a1/a0/d;->p:J

    add-long/2addr v2, v10

    aget-wide v10, v5, v9

    sub-long/2addr v2, v10

    long-to-int v3, v2

    aput v3, v4, v9

    iget-wide v2, v8, Lf/h/a/c/a1/a0/d;->t:J

    aget-wide v10, v7, v9

    sub-long/2addr v2, v10

    aput-wide v2, v6, v9

    aget-wide v2, v6, v9

    const-wide/16 v10, 0x0

    cmp-long v12, v2, v10

    if-gtz v12, :cond_4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Discarding last cue point with unexpected duration: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MatroskaExtractor"

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v4, v9}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v4

    invoke-static {v5, v9}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v5

    invoke-static {v6, v9}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v6

    invoke-static {v7, v9}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v7

    :cond_4
    const/4 v2, 0x0

    iput-object v2, v8, Lf/h/a/c/a1/a0/d;->C:Lf/h/a/c/i1/m;

    iput-object v2, v8, Lf/h/a/c/a1/a0/d;->D:Lf/h/a/c/i1/m;

    new-instance v2, Lf/h/a/c/a1/c;

    invoke-direct {v2, v4, v5, v6, v7}, Lf/h/a/c/a1/c;-><init>([I[J[J[J)V

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v2, 0x0

    iput-object v2, v8, Lf/h/a/c/a1/a0/d;->C:Lf/h/a/c/i1/m;

    iput-object v2, v8, Lf/h/a/c/a1/a0/d;->D:Lf/h/a/c/i1/m;

    new-instance v2, Lf/h/a/c/a1/q$b;

    iget-wide v3, v8, Lf/h/a/c/a1/a0/d;->t:J

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    :goto_3
    invoke-interface {v1, v2}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    const/4 v1, 0x1

    iput-boolean v1, v8, Lf/h/a/c/a1/a0/d;->v:Z

    goto/16 :goto_2c

    :cond_6
    iget-object v1, v8, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v8, Lf/h/a/c/a1/a0/d;->a0:Lf/h/a/c/a1/i;

    invoke-interface {v1}, Lf/h/a/c/a1/i;->k()V

    goto/16 :goto_2c

    :cond_7
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "No valid tracks were found"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    iget-wide v1, v8, Lf/h/a/c/a1/a0/d;->r:J

    cmp-long v3, v1, v9

    if-nez v3, :cond_9

    const-wide/32 v1, 0xf4240

    iput-wide v1, v8, Lf/h/a/c/a1/a0/d;->r:J

    :cond_9
    iget-wide v1, v8, Lf/h/a/c/a1/a0/d;->s:J

    cmp-long v3, v1, v9

    if-eqz v3, :cond_6a

    invoke-virtual {v8, v1, v2}, Lf/h/a/c/a1/a0/d;->j(J)J

    move-result-wide v1

    iput-wide v1, v8, Lf/h/a/c/a1/a0/d;->t:J

    goto/16 :goto_2c

    :cond_a
    iget-object v1, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iget-boolean v2, v1, Lf/h/a/c/a1/a0/d$c;->g:Z

    if-eqz v2, :cond_6a

    iget-object v1, v1, Lf/h/a/c/a1/a0/d$c;->h:[B

    if-nez v1, :cond_b

    goto/16 :goto_2c

    :cond_b
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Combining encryption and compression is not supported"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_c
    iget-object v1, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iget-boolean v2, v1, Lf/h/a/c/a1/a0/d$c;->g:Z

    if-eqz v2, :cond_6a

    iget-object v2, v1, Lf/h/a/c/a1/a0/d$c;->i:Lf/h/a/c/a1/s$a;

    if-eqz v2, :cond_d

    new-instance v4, Lcom/google/android/exoplayer2/drm/DrmInitData;

    const/4 v5, 0x1

    new-array v6, v5, [Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;

    new-instance v7, Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;

    sget-object v8, Lf/h/a/c/u;->a:Ljava/util/UUID;

    iget-object v2, v2, Lf/h/a/c/a1/s$a;->b:[B

    const-string v9, "video/webm"

    invoke-direct {v7, v8, v9, v2}, Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;-><init>(Ljava/util/UUID;Ljava/lang/String;[B)V

    aput-object v7, v6, v3

    const/4 v2, 0x0

    invoke-direct {v4, v2, v5, v6}, Lcom/google/android/exoplayer2/drm/DrmInitData;-><init>(Ljava/lang/String;Z[Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;)V

    iput-object v4, v1, Lf/h/a/c/a1/a0/d$c;->k:Lcom/google/android/exoplayer2/drm/DrmInitData;

    goto/16 :goto_2c

    :cond_d
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Encrypted Track found but ContentEncKeyID was not found"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    iget v1, v8, Lf/h/a/c/a1/a0/d;->w:I

    if-eq v1, v4, :cond_f

    iget-wide v2, v8, Lf/h/a/c/a1/a0/d;->x:J

    cmp-long v4, v2, v6

    if-eqz v4, :cond_f

    if-ne v1, v5, :cond_6a

    iput-wide v2, v8, Lf/h/a/c/a1/a0/d;->z:J

    goto/16 :goto_2c

    :cond_f
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Mandatory element SeekID or SeekPosition not found"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_10
    iget-object v1, v8, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iget-object v1, v1, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    const-string v2, "V_VP8"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "A_VORBIS"

    const-string v5, "A_OPUS"

    const-string v6, "V_THEORA"

    const-string v7, "V_MS/VFW/FOURCC"

    const-string v9, "V_MPEGH/ISO/HEVC"

    const-string v10, "V_MPEG4/ISO/AVC"

    const-string v11, "V_MPEG4/ISO/AP"

    const-string v12, "V_MPEG4/ISO/ASP"

    const-string v13, "V_MPEG4/ISO/SP"

    const-string v14, "V_MPEG2"

    const-string v15, "V_AV1"

    const-string v0, "V_VP9"

    move-object/from16 p1, v2

    const-string v2, "A_DTS/EXPRESS"

    move-object/from16 v16, v8

    const-string v8, "S_HDMV/PGS"

    move-object/from16 v17, v8

    const-string v8, "A_DTS"

    move-object/from16 v18, v2

    const-string v2, "A_AC3"

    move-object/from16 v19, v8

    const-string v8, "A_AAC"

    move-object/from16 v20, v2

    const-string v2, "A_DTS/LOSSLESS"

    move-object/from16 v21, v2

    const-string v2, "S_VOBSUB"

    move-object/from16 v22, v2

    const-string v2, "S_DVBSUB"

    move-object/from16 v23, v2

    const-string v2, "A_MPEG/L3"

    move-object/from16 v24, v2

    const-string v2, "A_MPEG/L2"

    move-object/from16 v25, v2

    const-string v2, "A_TRUEHD"

    move-object/from16 v26, v2

    const-string v2, "A_MS/ACM"

    if-nez v3, :cond_1d

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    move-object/from16 v3, v25

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_1c

    move-object/from16 v25, v11

    move-object/from16 v11, v24

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_13

    move-object/from16 v24, v13

    move-object/from16 v13, v20

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_14

    move-object/from16 v20, v4

    const-string v4, "A_EAC3"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_15

    move-object/from16 v4, v26

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_12

    move-object/from16 v26, v4

    move-object/from16 v4, v19

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_16

    move-object/from16 v19, v3

    move-object/from16 v3, v18

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_17

    move-object/from16 v18, v11

    move-object/from16 v11, v21

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_18

    move-object/from16 v21, v7

    const-string v7, "A_FLAC"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_19

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_19

    const-string v7, "A_PCM/INT/LIT"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_19

    const-string v7, "S_TEXT/UTF8"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_19

    const-string v7, "S_TEXT/ASS"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_19

    move-object/from16 v7, v22

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_1a

    move-object/from16 v22, v2

    move-object/from16 v2, v17

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1b

    move-object/from16 v17, v12

    move-object/from16 v12, v23

    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    goto/16 :goto_6

    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_7

    :cond_12
    move-object/from16 v26, v4

    goto :goto_4

    :cond_13
    move-object/from16 v24, v13

    move-object/from16 v13, v20

    :cond_14
    move-object/from16 v20, v4

    :cond_15
    :goto_4
    move-object/from16 v4, v19

    :cond_16
    move-object/from16 v19, v3

    move-object/from16 v3, v18

    :cond_17
    move-object/from16 v18, v11

    :goto_5
    move-object/from16 v11, v21

    :cond_18
    move-object/from16 v21, v7

    :cond_19
    move-object/from16 v7, v22

    :cond_1a
    move-object/from16 v22, v2

    move-object/from16 v2, v17

    :cond_1b
    move-object/from16 v17, v12

    move-object/from16 v12, v23

    goto :goto_6

    :cond_1c
    move-object/from16 v25, v11

    move-object/from16 v11, v21

    move-object/from16 v21, v7

    move-object/from16 v7, v22

    move-object/from16 v22, v2

    move-object/from16 v2, v17

    move-object/from16 v17, v12

    move-object/from16 v12, v23

    move-object/from16 v47, v19

    move-object/from16 v19, v3

    move-object/from16 v3, v18

    move-object/from16 v18, v24

    move-object/from16 v24, v13

    move-object/from16 v13, v20

    move-object/from16 v20, v4

    move-object/from16 v4, v47

    goto :goto_6

    :cond_1d
    move-object/from16 v3, v18

    move-object/from16 v18, v24

    move-object/from16 v24, v13

    move-object/from16 v13, v20

    move-object/from16 v20, v4

    move-object/from16 v4, v19

    move-object/from16 v19, v25

    move-object/from16 v25, v11

    goto :goto_5

    :goto_6
    const/4 v1, 0x1

    :goto_7
    if-eqz v1, :cond_64

    move-object/from16 v23, v12

    move-object/from16 v1, v16

    iget-object v12, v1, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    move-object/from16 v16, v10

    iget-object v10, v1, Lf/h/a/c/a1/a0/d;->a0:Lf/h/a/c/a1/i;

    move-object/from16 v27, v1

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->c:I

    move-object/from16 v28, v10

    iget-object v10, v12, Lf/h/a/c/a1/a0/d$c;->b:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v29

    const/16 v30, 0x8

    move/from16 v31, v1

    const/4 v1, 0x3

    sparse-switch v29, :sswitch_data_0

    goto/16 :goto_8

    :sswitch_0
    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    goto/16 :goto_8

    :cond_1e
    const/16 v0, 0x1d

    goto/16 :goto_9

    :sswitch_1
    const-string v0, "A_FLAC"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    goto/16 :goto_8

    :cond_1f
    const/16 v0, 0x1c

    goto/16 :goto_9

    :sswitch_2
    const-string v0, "A_EAC3"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    goto/16 :goto_8

    :cond_20
    const/16 v0, 0x1b

    goto/16 :goto_9

    :sswitch_3
    invoke-virtual {v10, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    goto/16 :goto_8

    :cond_21
    const/16 v0, 0x1a

    goto/16 :goto_9

    :sswitch_4
    const-string v0, "S_TEXT/UTF8"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    goto/16 :goto_8

    :cond_22
    const/16 v0, 0x19

    goto/16 :goto_9

    :sswitch_5
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    goto/16 :goto_8

    :cond_23
    const/16 v0, 0x18

    goto/16 :goto_9

    :sswitch_6
    const-string v0, "S_TEXT/ASS"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    goto/16 :goto_8

    :cond_24
    const/16 v0, 0x17

    goto/16 :goto_9

    :sswitch_7
    const-string v0, "A_PCM/INT/LIT"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    goto/16 :goto_8

    :cond_25
    const/16 v0, 0x16

    goto/16 :goto_9

    :sswitch_8
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_26

    goto/16 :goto_8

    :cond_26
    const/16 v0, 0x15

    goto/16 :goto_9

    :sswitch_9
    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_27

    goto/16 :goto_8

    :cond_27
    const/16 v0, 0x14

    goto/16 :goto_9

    :sswitch_a
    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    goto/16 :goto_8

    :cond_28
    const/16 v0, 0x13

    goto/16 :goto_9

    :sswitch_b
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_29

    goto/16 :goto_8

    :cond_29
    const/16 v0, 0x12

    goto/16 :goto_9

    :sswitch_c
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    goto/16 :goto_8

    :cond_2a
    const/16 v0, 0x11

    goto/16 :goto_9

    :sswitch_d
    invoke-virtual {v10, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    goto/16 :goto_8

    :cond_2b
    const/16 v0, 0x10

    goto/16 :goto_9

    :sswitch_e
    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2c

    goto/16 :goto_8

    :cond_2c
    const/16 v0, 0xf

    goto/16 :goto_9

    :sswitch_f
    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2d

    goto/16 :goto_8

    :cond_2d
    const/16 v0, 0xe

    goto/16 :goto_9

    :sswitch_10
    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2e

    goto/16 :goto_8

    :cond_2e
    const/16 v0, 0xd

    goto/16 :goto_9

    :sswitch_11
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2f

    goto/16 :goto_8

    :cond_2f
    const/16 v0, 0xc

    goto/16 :goto_9

    :sswitch_12
    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_30

    goto/16 :goto_8

    :cond_30
    const/16 v0, 0xb

    goto/16 :goto_9

    :sswitch_13
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_31

    goto/16 :goto_8

    :cond_31
    const/16 v0, 0xa

    goto/16 :goto_9

    :sswitch_14
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_32

    goto/16 :goto_8

    :cond_32
    const/16 v0, 0x9

    goto/16 :goto_9

    :sswitch_15
    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_33

    goto/16 :goto_8

    :cond_33
    const/16 v0, 0x8

    goto/16 :goto_9

    :sswitch_16
    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_34

    goto :goto_8

    :cond_34
    const/4 v0, 0x7

    goto :goto_9

    :sswitch_17
    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_35

    goto :goto_8

    :cond_35
    const/4 v0, 0x6

    goto :goto_9

    :sswitch_18
    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_36

    goto :goto_8

    :cond_36
    const/4 v0, 0x5

    goto :goto_9

    :sswitch_19
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_37

    goto :goto_8

    :cond_37
    const/4 v0, 0x4

    goto :goto_9

    :sswitch_1a
    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_38

    goto :goto_8

    :cond_38
    const/4 v0, 0x3

    goto :goto_9

    :sswitch_1b
    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_39

    goto :goto_8

    :cond_39
    const/4 v0, 0x2

    goto :goto_9

    :sswitch_1c
    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3a

    goto :goto_8

    :cond_3a
    const/4 v0, 0x1

    goto :goto_9

    :sswitch_1d
    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3b

    goto :goto_8

    :cond_3b
    const/4 v0, 0x0

    goto :goto_9

    :goto_8
    const/4 v0, -0x1

    :goto_9
    const-string v2, "application/x-subrip"

    const-string v3, "video/x-unknown"

    const-string v4, "audio/x-unknown"

    const-string v5, "MatroskaExtractor"

    const-string v6, "audio/raw"

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Unrecognized codec identifier."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {v30 .. v30}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-wide v4, v12, Lf/h/a/c/a1/a0/d$c;->P:J

    invoke-virtual {v1, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {v30 .. v30}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-wide v3, v12, Lf/h/a/c/a1/a0/d$c;->Q:J

    invoke-virtual {v1, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x1680

    const-string v3, "audio/opus"

    goto/16 :goto_14

    :pswitch_1
    iget-object v0, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "audio/flac"

    goto/16 :goto_10

    :pswitch_2
    const-string v0, "audio/eac3"

    goto/16 :goto_b

    :pswitch_3
    const-string v0, "video/mpeg2"

    goto :goto_b

    :pswitch_4
    move-object v3, v2

    goto :goto_a

    :pswitch_5
    new-instance v0, Lf/h/a/c/i1/r;

    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    invoke-static {v0}, Lf/h/a/c/j1/j;->a(Lf/h/a/c/i1/r;)Lf/h/a/c/j1/j;

    move-result-object v0

    iget-object v1, v0, Lf/h/a/c/j1/j;->a:Ljava/util/List;

    iget v0, v0, Lf/h/a/c/j1/j;->b:I

    iput v0, v12, Lf/h/a/c/a1/a0/d$c;->W:I

    const-string v0, "video/hevc"

    goto/16 :goto_c

    :pswitch_6
    const-string v0, "text/x-ssa"

    goto :goto_b

    :pswitch_7
    iget v0, v12, Lf/h/a/c/a1/a0/d$c;->N:I

    invoke-static {v0}, Lf/h/a/c/i1/a0;->k(I)I

    move-result v0

    if-nez v0, :cond_3c

    const-string v0, "Unsupported PCM bit depth: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->N:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ". Setting mimeType to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_18

    :cond_3c
    const/4 v1, 0x0

    goto/16 :goto_1b

    :goto_a
    :pswitch_8
    move-object v4, v3

    goto/16 :goto_18

    :pswitch_9
    const-string v0, "application/pgs"

    goto :goto_b

    :pswitch_a
    const-string v0, "video/x-vnd.on2.vp9"

    goto :goto_b

    :pswitch_b
    const-string v0, "video/x-vnd.on2.vp8"

    goto :goto_b

    :pswitch_c
    const-string v0, "video/av01"

    goto :goto_b

    :pswitch_d
    const-string v0, "audio/vnd.dts"

    goto :goto_b

    :pswitch_e
    const-string v0, "audio/ac3"

    goto :goto_b

    :pswitch_f
    iget-object v0, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "audio/mp4a-latm"

    goto/16 :goto_10

    :pswitch_10
    const-string v0, "audio/vnd.dts.hd"

    :goto_b
    move-object v4, v0

    goto/16 :goto_18

    :pswitch_11
    iget-object v0, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "application/vobsub"

    goto/16 :goto_10

    :pswitch_12
    new-instance v0, Lf/h/a/c/i1/r;

    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    invoke-static {v0}, Lf/h/a/c/j1/h;->b(Lf/h/a/c/i1/r;)Lf/h/a/c/j1/h;

    move-result-object v0

    iget-object v1, v0, Lf/h/a/c/j1/h;->a:Ljava/util/List;

    iget v0, v0, Lf/h/a/c/j1/h;->b:I

    iput v0, v12, Lf/h/a/c/a1/a0/d$c;->W:I

    const-string v0, "video/avc"

    :goto_c
    move-object/from16 v47, v1

    move-object v1, v0

    move-object/from16 v0, v47

    goto/16 :goto_10

    :pswitch_13
    const/4 v0, 0x4

    new-array v0, v0, [B

    iget-object v3, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    const/4 v4, 0x0

    aget-byte v5, v3, v4

    aput-byte v5, v0, v4

    const/4 v4, 0x1

    aget-byte v5, v3, v4

    aput-byte v5, v0, v4

    const/4 v4, 0x2

    aget-byte v5, v3, v4

    aput-byte v5, v0, v4

    aget-byte v3, v3, v1

    aput-byte v3, v0, v1

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "application/dvbsubs"

    goto/16 :goto_10

    :pswitch_14
    iget-object v0, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    array-length v1, v0

    const/16 v4, 0x10

    if-gt v4, v1, :cond_3d

    const/4 v1, 0x1

    goto :goto_d

    :cond_3d
    const/4 v1, 0x0

    :goto_d
    :try_start_0
    invoke-static {v1}, Lf/g/j/k/a;->d(Z)V

    const/16 v1, 0x11

    aget-byte v4, v0, v4

    int-to-long v6, v4

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    const/16 v4, 0x12

    aget-byte v1, v0, v1

    int-to-long v10, v1

    and-long/2addr v10, v8

    shl-long v10, v10, v30

    or-long/2addr v6, v10

    const/16 v1, 0x13

    aget-byte v4, v0, v4

    int-to-long v10, v4

    and-long/2addr v10, v8

    const/16 v4, 0x10

    shl-long/2addr v10, v4

    or-long/2addr v6, v10

    aget-byte v1, v0, v1

    int-to-long v10, v1

    and-long/2addr v8, v10

    const/16 v1, 0x18

    shl-long/2addr v8, v1

    or-long/2addr v6, v8

    const-wide/32 v8, 0x58564944

    cmp-long v1, v6, v8

    if-nez v1, :cond_3e

    new-instance v0, Landroid/util/Pair;

    const-string v1, "video/divx"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_f

    :cond_3e
    const-wide/32 v8, 0x33363248

    cmp-long v1, v6, v8

    if-nez v1, :cond_3f

    new-instance v0, Landroid/util/Pair;

    const-string v1, "video/3gpp"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_f

    :cond_3f
    const-wide/32 v8, 0x31435657

    cmp-long v1, v6, v8

    if-nez v1, :cond_42

    const/16 v1, 0x28

    :goto_e
    array-length v3, v0

    add-int/lit8 v3, v3, -0x4

    if-ge v1, v3, :cond_41

    aget-byte v3, v0, v1

    if-nez v3, :cond_40

    add-int/lit8 v3, v1, 0x1

    aget-byte v3, v0, v3

    if-nez v3, :cond_40

    add-int/lit8 v3, v1, 0x2

    aget-byte v3, v0, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_40

    add-int/lit8 v3, v1, 0x3

    aget-byte v3, v0, v3

    const/16 v4, 0xf

    if-ne v3, v4, :cond_40

    array-length v3, v0

    invoke-static {v0, v1, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    new-instance v1, Landroid/util/Pair;

    const-string v3, "video/wvc1"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_f

    :cond_40
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :cond_41
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Failed to find FourCC VC1 initialization data"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_42
    const-string v0, "Unknown FourCC. Setting mimeType to video/x-unknown"

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x0

    invoke-direct {v0, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_f
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    :goto_10
    move-object v4, v1

    goto/16 :goto_15

    :catch_0
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing FourCC private data"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_15
    const-string v0, "audio/mpeg"

    goto :goto_11

    :pswitch_16
    const-string v0, "audio/mpeg-L2"

    :goto_11
    move-object v3, v0

    const/16 v1, 0x1000

    const/4 v0, 0x0

    goto :goto_14

    :pswitch_17
    iget-object v0, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    const-string v3, "Error parsing vorbis codec private"

    const/4 v4, 0x0

    :try_start_1
    aget-byte v4, v0, v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_48

    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_12
    aget-byte v6, v0, v4

    const/4 v7, -0x1

    if-ne v6, v7, :cond_43

    add-int/lit16 v5, v5, 0xff

    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    :cond_43
    add-int/lit8 v6, v4, 0x1

    aget-byte v4, v0, v4

    add-int/2addr v5, v4

    const/4 v4, 0x0

    :goto_13
    aget-byte v7, v0, v6

    const/4 v8, -0x1

    if-ne v7, v8, :cond_44

    add-int/lit16 v4, v4, 0xff

    add-int/lit8 v6, v6, 0x1

    goto :goto_13

    :cond_44
    add-int/lit8 v7, v6, 0x1

    aget-byte v6, v0, v6

    add-int/2addr v4, v6

    aget-byte v6, v0, v7

    const/4 v8, 0x1

    if-ne v6, v8, :cond_47

    new-array v6, v5, [B

    const/4 v8, 0x0

    invoke-static {v0, v7, v6, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v7, v5

    aget-byte v5, v0, v7

    if-ne v5, v1, :cond_46

    add-int/2addr v7, v4

    aget-byte v1, v0, v7

    const/4 v4, 0x5

    if-ne v1, v4, :cond_45

    array-length v1, v0

    sub-int/2addr v1, v7

    new-array v1, v1, [B

    array-length v4, v0

    sub-int/2addr v4, v7

    const/4 v5, 0x0

    invoke-static {v0, v7, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v4, 0x2

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0x2000

    const-string v3, "audio/vorbis"

    :goto_14
    const/4 v4, -0x1

    move/from16 v36, v1

    move-object v14, v3

    const/16 v20, -0x1

    goto/16 :goto_1c

    :cond_45
    :try_start_2
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_46
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_47
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_48
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_18
    new-instance v0, Lf/h/a/c/a1/a0/d$d;

    invoke-direct {v0}, Lf/h/a/c/a1/a0/d$d;-><init>()V

    iput-object v0, v12, Lf/h/a/c/a1/a0/d$c;->R:Lf/h/a/c/a1/a0/d$d;

    const-string v4, "audio/true-hd"

    goto :goto_18

    :goto_15
    move-object v1, v0

    goto/16 :goto_1a

    :pswitch_19
    new-instance v0, Lf/h/a/c/i1/r;

    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    :try_start_3
    invoke-virtual {v0}, Lf/h/a/c/i1/r;->j()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_49

    goto :goto_16

    :cond_49
    const v3, 0xfffe

    if-ne v1, v3, :cond_4a

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->k()J

    move-result-wide v7

    sget-object v1, Lf/h/a/c/a1/a0/d;->e0:Ljava/util/UUID;

    invoke-virtual {v1}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v9

    cmp-long v3, v7, v9

    if-nez v3, :cond_4a

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->k()J

    move-result-wide v7

    invoke-virtual {v1}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v0
    :try_end_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2

    cmp-long v3, v7, v0

    if-nez v3, :cond_4a

    :goto_16
    const/4 v0, 0x1

    goto :goto_17

    :cond_4a
    const/4 v0, 0x0

    :goto_17
    if-eqz v0, :cond_4b

    iget v0, v12, Lf/h/a/c/a1/a0/d$c;->N:I

    invoke-static {v0}, Lf/h/a/c/i1/a0;->k(I)I

    move-result v0

    if-nez v0, :cond_3c

    const-string v0, "Unsupported PCM bit depth: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->N:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ". Setting mimeType to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_18

    :cond_4b
    const-string v0, "Non-PCM MS/ACM is unsupported. Setting mimeType to "

    invoke-static {v0, v4, v5}, Lf/e/c/a/a;->V(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_18
    const/4 v0, 0x0

    goto :goto_15

    :catch_2
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing MS/ACM codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1a
    iget-object v0, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    if-nez v0, :cond_4c

    const/4 v0, 0x0

    goto :goto_19

    :cond_4c
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_19
    const-string v4, "video/mp4v-es"

    goto :goto_15

    :goto_1a
    const/4 v0, -0x1

    move-object v6, v4

    :goto_1b
    const/4 v3, -0x1

    move/from16 v20, v0

    move-object v0, v1

    move-object v14, v6

    const/16 v36, -0x1

    :goto_1c
    iget-boolean v1, v12, Lf/h/a/c/a1/a0/d$c;->T:Z

    or-int/lit8 v1, v1, 0x0

    iget-boolean v3, v12, Lf/h/a/c/a1/a0/d$c;->S:Z

    if-eqz v3, :cond_4d

    const/4 v3, 0x2

    goto :goto_1d

    :cond_4d
    const/4 v3, 0x0

    :goto_1d
    or-int/2addr v1, v3

    invoke-static {v14}, Lf/h/a/c/i1/o;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4e

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x0

    const/16 v16, -0x1

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->M:I

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->O:I

    iget-object v4, v12, Lf/h/a/c/a1/a0/d$c;->k:Lcom/google/android/exoplayer2/drm/DrmInitData;

    iget-object v5, v12, Lf/h/a/c/a1/a0/d$c;->U:Ljava/lang/String;

    move/from16 v17, v36

    move/from16 v18, v2

    move/from16 v19, v3

    move-object/from16 v21, v0

    move-object/from16 v22, v4

    move/from16 v23, v1

    move-object/from16 v24, v5

    invoke-static/range {v13 .. v24}, Lcom/google/android/exoplayer2/Format;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    const/4 v1, 0x1

    goto/16 :goto_28

    :cond_4e
    invoke-static {v14}, Lf/h/a/c/i1/o;->g(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5f

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->p:I

    if-nez v1, :cond_51

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->n:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4f

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->l:I

    :cond_4f
    iput v1, v12, Lf/h/a/c/a1/a0/d$c;->n:I

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->o:I

    if-ne v1, v2, :cond_50

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->m:I

    :cond_50
    iput v1, v12, Lf/h/a/c/a1/a0/d$c;->o:I

    goto :goto_1e

    :cond_51
    const/4 v2, -0x1

    :goto_1e
    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->n:I

    if-eq v1, v2, :cond_52

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->o:I

    if-eq v3, v2, :cond_52

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->m:I

    mul-int v2, v2, v1

    int-to-float v1, v2

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->l:I

    mul-int v2, v2, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    move/from16 v42, v1

    goto :goto_1f

    :cond_52
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v42, -0x40800000    # -1.0f

    :goto_1f
    iget-boolean v1, v12, Lf/h/a/c/a1/a0/d$c;->w:Z

    if-eqz v1, :cond_55

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->C:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->D:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->E:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->F:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->G:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->H:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->I:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->J:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->K:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_54

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->L:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_53

    goto/16 :goto_20

    :cond_53
    const/16 v1, 0x19

    new-array v1, v1, [B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->C:F

    const v4, 0x47435000    # 50000.0f

    mul-float v3, v3, v4

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->D:F

    mul-float v3, v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->E:F

    mul-float v3, v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->F:F

    mul-float v3, v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->G:F

    mul-float v3, v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->H:F

    mul-float v3, v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->I:F

    mul-float v3, v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->J:F

    mul-float v3, v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->K:F

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->L:F

    add-float/2addr v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->A:I

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->B:I

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    goto :goto_21

    :cond_54
    :goto_20
    const/4 v1, 0x0

    :goto_21
    new-instance v2, Lcom/google/android/exoplayer2/video/ColorInfo;

    iget v3, v12, Lf/h/a/c/a1/a0/d$c;->x:I

    iget v4, v12, Lf/h/a/c/a1/a0/d$c;->z:I

    iget v5, v12, Lf/h/a/c/a1/a0/d$c;->y:I

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/google/android/exoplayer2/video/ColorInfo;-><init>(III[B)V

    move-object/from16 v45, v2

    goto :goto_22

    :cond_55
    const/4 v1, 0x0

    move-object/from16 v45, v1

    :goto_22
    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->a:Ljava/lang/String;

    const-string v2, "htc_video_rotA-000"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    const/4 v1, 0x0

    goto :goto_23

    :cond_56
    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->a:Ljava/lang/String;

    const-string v2, "htc_video_rotA-090"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_57

    const/16 v1, 0x5a

    goto :goto_23

    :cond_57
    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->a:Ljava/lang/String;

    const-string v2, "htc_video_rotA-180"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_58

    const/16 v1, 0xb4

    goto :goto_23

    :cond_58
    iget-object v1, v12, Lf/h/a/c/a1/a0/d$c;->a:Ljava/lang/String;

    const-string v2, "htc_video_rotA-270"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_59

    const/16 v1, 0x10e

    goto :goto_23

    :cond_59
    const/4 v1, -0x1

    :goto_23
    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->q:I

    if-nez v2, :cond_5e

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->r:F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_5e

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->s:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_5e

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->t:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_5a

    const/4 v1, 0x0

    const/16 v41, 0x0

    goto :goto_25

    :cond_5a
    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->s:F

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_5b

    const/16 v1, 0x5a

    const/16 v41, 0x5a

    goto :goto_25

    :cond_5b
    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->s:F

    const/high16 v3, -0x3ccc0000    # -180.0f

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_5d

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->s:F

    const/high16 v3, 0x43340000    # 180.0f

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_5c

    goto :goto_24

    :cond_5c
    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->s:F

    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_5e

    const/16 v1, 0x10e

    const/16 v41, 0x10e

    goto :goto_25

    :cond_5d
    :goto_24
    const/16 v1, 0xb4

    const/16 v41, 0xb4

    goto :goto_25

    :cond_5e
    move/from16 v41, v1

    :goto_25
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v32

    const/16 v34, 0x0

    const/16 v35, -0x1

    iget v1, v12, Lf/h/a/c/a1/a0/d$c;->l:I

    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->m:I

    const/high16 v39, -0x40800000    # -1.0f

    iget-object v3, v12, Lf/h/a/c/a1/a0/d$c;->u:[B

    iget v4, v12, Lf/h/a/c/a1/a0/d$c;->v:I

    iget-object v5, v12, Lf/h/a/c/a1/a0/d$c;->k:Lcom/google/android/exoplayer2/drm/DrmInitData;

    move-object/from16 v33, v14

    move/from16 v37, v1

    move/from16 v38, v2

    move-object/from16 v40, v0

    move-object/from16 v43, v3

    move/from16 v44, v4

    move-object/from16 v46, v5

    invoke-static/range {v32 .. v46}, Lcom/google/android/exoplayer2/Format;->n(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIFLjava/util/List;IF[BILcom/google/android/exoplayer2/video/ColorInfo;Lcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    const/4 v1, 0x2

    goto/16 :goto_28

    :cond_5f
    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_60

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v12, Lf/h/a/c/a1/a0/d$c;->U:Ljava/lang/String;

    iget-object v3, v12, Lf/h/a/c/a1/a0/d$c;->k:Lcom/google/android/exoplayer2/drm/DrmInitData;

    invoke-static {v0, v14, v1, v2, v3}, Lcom/google/android/exoplayer2/Format;->k(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    goto/16 :goto_27

    :cond_60
    const-string v2, "text/x-ssa"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_61

    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v2, Lf/h/a/c/a1/a0/d;->c0:[B

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v12, Lf/h/a/c/a1/a0/d$c;->j:[B

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x0

    const/16 v16, -0x1

    iget-object v2, v12, Lf/h/a/c/a1/a0/d$c;->U:Ljava/lang/String;

    const/16 v19, -0x1

    iget-object v3, v12, Lf/h/a/c/a1/a0/d$c;->k:Lcom/google/android/exoplayer2/drm/DrmInitData;

    const-wide v21, 0x7fffffffffffffffL

    move/from16 v17, v1

    move-object/from16 v18, v2

    move-object/from16 v20, v3

    move-object/from16 v23, v0

    invoke-static/range {v13 .. v23}, Lcom/google/android/exoplayer2/Format;->l(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/google/android/exoplayer2/drm/DrmInitData;JLjava/util/List;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    goto :goto_27

    :cond_61
    const-string v2, "application/vobsub"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_63

    const-string v2, "application/pgs"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_63

    const-string v2, "application/dvbsubs"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_62

    goto :goto_26

    :cond_62
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Unexpected MIME type."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_63
    :goto_26
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x0

    const/16 v16, -0x1

    iget-object v2, v12, Lf/h/a/c/a1/a0/d$c;->U:Ljava/lang/String;

    iget-object v3, v12, Lf/h/a/c/a1/a0/d$c;->k:Lcom/google/android/exoplayer2/drm/DrmInitData;

    move/from16 v17, v1

    move-object/from16 v18, v0

    move-object/from16 v19, v2

    move-object/from16 v20, v3

    invoke-static/range {v13 .. v20}, Lcom/google/android/exoplayer2/Format;->h(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/lang/String;Lcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    :goto_27
    const/4 v1, 0x3

    :goto_28
    iget v2, v12, Lf/h/a/c/a1/a0/d$c;->c:I

    move-object/from16 v3, v28

    invoke-interface {v3, v2, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v1

    iput-object v1, v12, Lf/h/a/c/a1/a0/d$c;->V:Lf/h/a/c/a1/s;

    invoke-interface {v1, v0}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    move-object/from16 v0, v27

    iget-object v1, v0, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    iget-object v2, v0, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    iget v3, v2, Lf/h/a/c/a1/a0/d$c;->c:I

    invoke-virtual {v1, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_29

    :cond_64
    move-object/from16 v0, v16

    :goto_29
    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/a/c/a1/a0/d;->u:Lf/h/a/c/a1/a0/d$c;

    goto :goto_2c

    :cond_65
    move-object v0, v8

    iget v1, v0, Lf/h/a/c/a1/a0/d;->G:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_66

    goto :goto_2c

    :cond_66
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_2a
    iget v3, v0, Lf/h/a/c/a1/a0/d;->K:I

    if-ge v2, v3, :cond_67

    iget-object v3, v0, Lf/h/a/c/a1/a0/d;->L:[I

    aget v3, v3, v2

    add-int/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_2a

    :cond_67
    iget-object v2, v0, Lf/h/a/c/a1/a0/d;->c:Landroid/util/SparseArray;

    iget v3, v0, Lf/h/a/c/a1/a0/d;->M:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lf/h/a/c/a1/a0/d$c;

    const/4 v2, 0x0

    const/4 v9, 0x0

    :goto_2b
    iget v2, v0, Lf/h/a/c/a1/a0/d;->K:I

    if-ge v9, v2, :cond_69

    iget-wide v2, v0, Lf/h/a/c/a1/a0/d;->H:J

    iget v4, v8, Lf/h/a/c/a1/a0/d$c;->e:I

    mul-int v4, v4, v9

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long v3, v2, v4

    iget v2, v0, Lf/h/a/c/a1/a0/d;->O:I

    if-nez v9, :cond_68

    iget-boolean v5, v0, Lf/h/a/c/a1/a0/d;->Q:Z

    if-nez v5, :cond_68

    or-int/lit8 v2, v2, 0x1

    :cond_68
    move v5, v2

    iget-object v2, v0, Lf/h/a/c/a1/a0/d;->L:[I

    aget v6, v2, v9

    sub-int v10, v1, v6

    move-object v1, v0

    move-object v2, v8

    move v7, v10

    invoke-virtual/range {v1 .. v7}, Lf/h/a/c/a1/a0/d;->a(Lf/h/a/c/a1/a0/d$c;JIII)V

    add-int/lit8 v9, v9, 0x1

    move v1, v10

    goto :goto_2b

    :cond_69
    const/4 v1, 0x0

    iput v1, v0, Lf/h/a/c/a1/a0/d;->G:I

    :cond_6a
    :goto_2c
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ce7f5de -> :sswitch_1d
        -0x7ce7f3b0 -> :sswitch_1c
        -0x76567dc0 -> :sswitch_1b
        -0x6a615338 -> :sswitch_1a
        -0x672350af -> :sswitch_19
        -0x585f4fce -> :sswitch_18
        -0x585f4fcd -> :sswitch_17
        -0x51dc40b2 -> :sswitch_16
        -0x37a9c464 -> :sswitch_15
        -0x2016c535 -> :sswitch_14
        -0x2016c4e5 -> :sswitch_13
        -0x19552dbd -> :sswitch_12
        -0x1538b2ba -> :sswitch_11
        0x3c02325 -> :sswitch_10
        0x3c02353 -> :sswitch_f
        0x3c030c5 -> :sswitch_e
        0x4e81333 -> :sswitch_d
        0x4e86155 -> :sswitch_c
        0x4e86156 -> :sswitch_b
        0x5e8da3e -> :sswitch_a
        0x1a8350d6 -> :sswitch_9
        0x2056f406 -> :sswitch_8
        0x2b453ce4 -> :sswitch_7
        0x2c0618eb -> :sswitch_6
        0x32fdf009 -> :sswitch_5
        0x54c61e47 -> :sswitch_4
        0x6bd6c624 -> :sswitch_3
        0x7446132a -> :sswitch_2
        0x7446b0a6 -> :sswitch_1
        0x744ad97d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_1a
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_d
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
