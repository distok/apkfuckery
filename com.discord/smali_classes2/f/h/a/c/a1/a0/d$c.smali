.class public final Lf/h/a/c/a1/a0/d$c;
.super Ljava/lang/Object;
.source "MatroskaExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/a0/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:F

.field public D:F

.field public E:F

.field public F:F

.field public G:F

.field public H:F

.field public I:F

.field public J:F

.field public K:F

.field public L:F

.field public M:I

.field public N:I

.field public O:I

.field public P:J

.field public Q:J

.field public R:Lf/h/a/c/a1/a0/d$d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public S:Z

.field public T:Z

.field public U:Ljava/lang/String;

.field public V:Lf/h/a/c/a1/s;

.field public W:I

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Z

.field public h:[B

.field public i:Lf/h/a/c/a1/s$a;

.field public j:[B

.field public k:Lcom/google/android/exoplayer2/drm/DrmInitData;

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:F

.field public s:F

.field public t:F

.field public u:[B

.field public v:I

.field public w:Z

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->l:I

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->m:I

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->n:I

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->o:I

    const/4 v1, 0x0

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->p:I

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->q:I

    const/4 v2, 0x0

    iput v2, p0, Lf/h/a/c/a1/a0/d$c;->r:F

    iput v2, p0, Lf/h/a/c/a1/a0/d$c;->s:F

    iput v2, p0, Lf/h/a/c/a1/a0/d$c;->t:F

    const/4 v2, 0x0

    iput-object v2, p0, Lf/h/a/c/a1/a0/d$c;->u:[B

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->v:I

    iput-boolean v1, p0, Lf/h/a/c/a1/a0/d$c;->w:Z

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->x:I

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->y:I

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->z:I

    const/16 v1, 0x3e8

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->A:I

    const/16 v1, 0xc8

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->B:I

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->C:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->D:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->E:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->F:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->G:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->H:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->I:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->J:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->K:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->L:F

    const/4 v1, 0x1

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->M:I

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->N:I

    const/16 v0, 0x1f40

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->O:I

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lf/h/a/c/a1/a0/d$c;->P:J

    iput-wide v2, p0, Lf/h/a/c/a1/a0/d$c;->Q:J

    iput-boolean v1, p0, Lf/h/a/c/a1/a0/d$c;->T:Z

    const-string v0, "eng"

    iput-object v0, p0, Lf/h/a/c/a1/a0/d$c;->U:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lf/h/a/c/a1/a0/d$a;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, -0x1

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->l:I

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->m:I

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->n:I

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->o:I

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->p:I

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->q:I

    const/4 v1, 0x0

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->r:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->s:F

    iput v1, p0, Lf/h/a/c/a1/a0/d$c;->t:F

    const/4 v1, 0x0

    iput-object v1, p0, Lf/h/a/c/a1/a0/d$c;->u:[B

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->v:I

    iput-boolean v0, p0, Lf/h/a/c/a1/a0/d$c;->w:Z

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->x:I

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->y:I

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->z:I

    const/16 v0, 0x3e8

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->A:I

    const/16 v0, 0xc8

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->B:I

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->C:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->D:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->E:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->F:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->G:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->H:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->I:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->J:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->K:F

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->L:F

    const/4 v0, 0x1

    iput v0, p0, Lf/h/a/c/a1/a0/d$c;->M:I

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->N:I

    const/16 p1, 0x1f40

    iput p1, p0, Lf/h/a/c/a1/a0/d$c;->O:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lf/h/a/c/a1/a0/d$c;->P:J

    iput-wide v1, p0, Lf/h/a/c/a1/a0/d$c;->Q:J

    iput-boolean v0, p0, Lf/h/a/c/a1/a0/d$c;->T:Z

    const-string p1, "eng"

    iput-object p1, p0, Lf/h/a/c/a1/a0/d$c;->U:Ljava/lang/String;

    return-void
.end method
