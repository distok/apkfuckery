.class public final Lf/h/a/c/a1/y/c;
.super Ljava/lang/Object;
.source "FlacExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# instance fields
.field public final a:[B

.field public final b:Lf/h/a/c/i1/r;

.field public final c:Lf/h/a/c/a1/k$a;

.field public d:Lf/h/a/c/a1/i;

.field public e:Lf/h/a/c/a1/s;

.field public f:I

.field public g:Lcom/google/android/exoplayer2/metadata/Metadata;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public h:Lf/h/a/c/i1/l;

.field public i:I

.field public j:I

.field public k:Lf/h/a/c/a1/y/b;

.field public l:I

.field public m:J


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2a

    new-array v0, v0, [B

    iput-object v0, p0, Lf/h/a/c/a1/y/c;->a:[B

    new-instance v0, Lf/h/a/c/i1/r;

    const v1, 0x8000

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/a/c/i1/r;-><init>([BI)V

    iput-object v0, p0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/a1/k$a;

    invoke-direct {v0}, Lf/h/a/c/a1/k$a;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/y/c;->c:Lf/h/a/c/a1/k$a;

    iput v2, p0, Lf/h/a/c/a1/y/c;->f:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    iget-wide v0, p0, Lf/h/a/c/a1/y/c;->m:J

    const-wide/32 v2, 0xf4240

    mul-long v0, v0, v2

    iget-object v2, p0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    sget v3, Lf/h/a/c/i1/a0;->a:I

    iget v2, v2, Lf/h/a/c/i1/l;->e:I

    int-to-long v2, v2

    div-long v5, v0, v2

    iget-object v4, p0, Lf/h/a/c/a1/y/c;->e:Lf/h/a/c/a1/s;

    const/4 v7, 0x1

    iget v8, p0, Lf/h/a/c/a1/y/c;->l:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-interface/range {v4 .. v10}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    return-void
.end method

.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget v2, v0, Lf/h/a/c/a1/y/c;->f:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_22

    const/4 v5, 0x2

    if-eq v2, v3, :cond_21

    const/16 v6, 0x8

    const/16 v7, 0x10

    const/16 v8, 0x18

    const/4 v9, 0x4

    const/4 v10, 0x3

    if-eq v2, v5, :cond_1f

    const/4 v11, 0x7

    const/4 v12, 0x6

    if-eq v2, v10, :cond_18

    const-wide/16 v13, 0x0

    const-wide/16 v15, -0x1

    const/4 v8, 0x5

    if-eq v2, v9, :cond_14

    if-ne v2, v8, :cond_13

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->e:Lf/h/a/c/a1/s;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->k:Lf/h/a/c/a1/y/b;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lf/h/a/c/a1/a;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->k:Lf/h/a/c/a1/y/b;

    move-object/from16 v3, p2

    invoke-virtual {v2, v1, v3}, Lf/h/a/c/a1/a;->a(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I

    move-result v4

    goto/16 :goto_b

    :cond_0
    iget-wide v8, v0, Lf/h/a/c/a1/y/c;->m:J

    cmp-long v2, v8, v15

    if-nez v2, :cond_5

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    iput v4, v1, Lf/h/a/c/a1/e;->f:I

    invoke-virtual {v1, v3, v4}, Lf/h/a/c/a1/e;->a(IZ)Z

    new-array v6, v3, [B

    invoke-virtual {v1, v6, v4, v3, v4}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    aget-byte v6, v6, v4

    and-int/2addr v6, v3

    if-ne v6, v3, :cond_1

    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {v1, v5, v4}, Lf/h/a/c/a1/e;->a(IZ)Z

    if-eqz v6, :cond_2

    goto :goto_1

    :cond_2
    const/4 v11, 0x6

    :goto_1
    new-instance v5, Lf/h/a/c/i1/r;

    invoke-direct {v5, v11}, Lf/h/a/c/i1/r;-><init>(I)V

    iget-object v7, v5, Lf/h/a/c/i1/r;->a:[B

    invoke-static {v1, v7, v4, v11}, Lf/g/j/k/a;->z0(Lf/h/a/c/a1/e;[BII)I

    move-result v7

    invoke-virtual {v5, v7}, Lf/h/a/c/i1/r;->B(I)V

    iput v4, v1, Lf/h/a/c/a1/e;->f:I

    :try_start_0
    invoke-virtual {v5}, Lf/h/a/c/i1/r;->w()J

    move-result-wide v7
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v6, :cond_3

    goto :goto_2

    :cond_3
    iget v1, v2, Lf/h/a/c/i1/l;->b:I

    int-to-long v1, v1

    mul-long v7, v7, v1

    :goto_2
    move-wide v13, v7

    goto :goto_3

    :catch_0
    const/4 v3, 0x0

    :goto_3
    if-eqz v3, :cond_4

    iput-wide v13, v0, Lf/h/a/c/a1/y/c;->m:J

    goto/16 :goto_b

    :cond_4
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/ParserException;-><init>()V

    throw v1

    :cond_5
    iget-object v2, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    iget v5, v2, Lf/h/a/c/i1/r;->c:I

    const v6, 0x8000

    if-ge v5, v6, :cond_8

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    sub-int/2addr v6, v5

    invoke-virtual {v1, v2, v5, v6}, Lf/h/a/c/a1/e;->f([BII)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_6

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    :goto_4
    if-nez v3, :cond_7

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    add-int/2addr v5, v1

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/r;->B(I)V

    goto :goto_5

    :cond_7
    iget-object v1, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/y/c;->a()V

    const/4 v4, -0x1

    goto/16 :goto_b

    :cond_8
    const/4 v3, 0x0

    :cond_9
    :goto_5
    iget-object v1, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v5, v0, Lf/h/a/c/a1/y/c;->l:I

    iget v6, v0, Lf/h/a/c/a1/y/c;->i:I

    if-ge v5, v6, :cond_a

    sub-int/2addr v6, v5

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v5

    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->D(I)V

    :cond_a
    iget-object v1, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    iget-object v5, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget v5, v1, Lf/h/a/c/i1/r;->b:I

    :goto_6
    iget v6, v1, Lf/h/a/c/i1/r;->c:I

    add-int/lit8 v6, v6, -0x10

    if-gt v5, v6, :cond_c

    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v6, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    iget v8, v0, Lf/h/a/c/a1/y/c;->j:I

    iget-object v9, v0, Lf/h/a/c/a1/y/c;->c:Lf/h/a/c/a1/k$a;

    invoke-static {v1, v6, v8, v9}, Lf/h/a/c/a1/k;->b(Lf/h/a/c/i1/r;Lf/h/a/c/i1/l;ILf/h/a/c/a1/k$a;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->c:Lf/h/a/c/a1/k$a;

    iget-wide v5, v1, Lf/h/a/c/a1/k$a;->a:J

    goto :goto_a

    :cond_b
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_c
    if-eqz v3, :cond_10

    :goto_7
    iget v3, v1, Lf/h/a/c/i1/r;->c:I

    iget v6, v0, Lf/h/a/c/a1/y/c;->i:I

    sub-int v6, v3, v6

    if-gt v5, v6, :cond_f

    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->C(I)V

    :try_start_1
    iget-object v3, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    iget v6, v0, Lf/h/a/c/a1/y/c;->j:I

    iget-object v8, v0, Lf/h/a/c/a1/y/c;->c:Lf/h/a/c/a1/k$a;

    invoke-static {v1, v3, v6, v8}, Lf/h/a/c/a1/k;->b(Lf/h/a/c/i1/r;Lf/h/a/c/i1/l;ILf/h/a/c/a1/k$a;)Z

    move-result v3
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_8

    :catch_1
    const/4 v3, 0x0

    :goto_8
    iget v6, v1, Lf/h/a/c/i1/r;->b:I

    iget v8, v1, Lf/h/a/c/i1/r;->c:I

    if-le v6, v8, :cond_d

    const/4 v3, 0x0

    :cond_d
    if-eqz v3, :cond_e

    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->c:Lf/h/a/c/a1/k$a;

    iget-wide v5, v1, Lf/h/a/c/a1/k$a;->a:J

    goto :goto_a

    :cond_e
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_f
    invoke-virtual {v1, v3}, Lf/h/a/c/i1/r;->C(I)V

    goto :goto_9

    :cond_10
    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->C(I)V

    :goto_9
    move-wide v5, v15

    :goto_a
    iget-object v1, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    iget v3, v1, Lf/h/a/c/i1/r;->b:I

    sub-int/2addr v3, v2

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->e:Lf/h/a/c/a1/s;

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    invoke-interface {v1, v2, v3}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v1, v0, Lf/h/a/c/a1/y/c;->l:I

    add-int/2addr v1, v3

    iput v1, v0, Lf/h/a/c/a1/y/c;->l:I

    cmp-long v1, v5, v15

    if-eqz v1, :cond_11

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/y/c;->a()V

    iput v4, v0, Lf/h/a/c/a1/y/c;->l:I

    iput-wide v5, v0, Lf/h/a/c/a1/y/c;->m:J

    :cond_11
    iget-object v1, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v1

    if-ge v1, v7, :cond_12

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    iget-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    iget v3, v1, Lf/h/a/c/i1/r;->b:I

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v1

    invoke-static {v2, v3, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/r;->y(I)V

    :cond_12
    :goto_b
    return v4

    :cond_13
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_14
    iput v4, v1, Lf/h/a/c/a1/e;->f:I

    new-array v2, v5, [B

    invoke-virtual {v1, v2, v4, v5, v4}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    aget-byte v5, v2, v4

    and-int/lit16 v5, v5, 0xff

    shl-int/2addr v5, v6

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v5

    shr-int/lit8 v3, v2, 0x2

    const/16 v5, 0x3ffe

    if-ne v3, v5, :cond_17

    iput v4, v1, Lf/h/a/c/a1/e;->f:I

    iput v2, v0, Lf/h/a/c/a1/y/c;->j:I

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->d:Lf/h/a/c/a1/i;

    sget v3, Lf/h/a/c/i1/a0;->a:I

    iget-wide v5, v1, Lf/h/a/c/a1/e;->d:J

    iget-wide v9, v1, Lf/h/a/c/a1/e;->c:J

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    iget-object v3, v1, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    if-eqz v3, :cond_15

    new-instance v3, Lf/h/a/c/a1/l;

    invoke-direct {v3, v1, v5, v6}, Lf/h/a/c/a1/l;-><init>(Lf/h/a/c/i1/l;J)V

    goto :goto_c

    :cond_15
    cmp-long v3, v9, v15

    if-eqz v3, :cond_16

    iget-wide v11, v1, Lf/h/a/c/i1/l;->j:J

    cmp-long v3, v11, v13

    if-lez v3, :cond_16

    new-instance v3, Lf/h/a/c/a1/y/b;

    iget v7, v0, Lf/h/a/c/a1/y/c;->j:I

    move-object/from16 v17, v3

    move-object/from16 v18, v1

    move/from16 v19, v7

    move-wide/from16 v20, v5

    move-wide/from16 v22, v9

    invoke-direct/range {v17 .. v23}, Lf/h/a/c/a1/y/b;-><init>(Lf/h/a/c/i1/l;IJJ)V

    iput-object v3, v0, Lf/h/a/c/a1/y/c;->k:Lf/h/a/c/a1/y/b;

    iget-object v3, v3, Lf/h/a/c/a1/a;->a:Lf/h/a/c/a1/a$a;

    goto :goto_c

    :cond_16
    new-instance v3, Lf/h/a/c/a1/q$b;

    invoke-virtual {v1}, Lf/h/a/c/i1/l;->d()J

    move-result-wide v5

    invoke-direct {v3, v5, v6, v13, v14}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    :goto_c
    invoke-interface {v2, v3}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    iput v8, v0, Lf/h/a/c/a1/y/c;->f:I

    return v4

    :cond_17
    iput v4, v1, Lf/h/a/c/a1/e;->f:I

    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "First frame does not start with sync code."

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_18
    iget-object v2, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    const/4 v3, 0x0

    :goto_d
    if-nez v3, :cond_1e

    iput v4, v1, Lf/h/a/c/a1/e;->f:I

    new-instance v3, Lf/h/a/c/i1/q;

    new-array v5, v9, [B

    invoke-direct {v3, v5}, Lf/h/a/c/i1/q;-><init>([B)V

    iget-object v5, v3, Lf/h/a/c/i1/q;->a:[B

    invoke-virtual {v1, v5, v4, v9, v4}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    invoke-virtual {v3}, Lf/h/a/c/i1/q;->e()Z

    move-result v5

    invoke-virtual {v3, v11}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    invoke-virtual {v3, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v3

    add-int/2addr v3, v9

    if-nez v6, :cond_19

    const/16 v2, 0x26

    new-array v3, v2, [B

    invoke-virtual {v1, v3, v4, v2, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    new-instance v2, Lf/h/a/c/i1/l;

    invoke-direct {v2, v3, v9}, Lf/h/a/c/i1/l;-><init>([BI)V

    :goto_e
    move/from16 p2, v5

    goto/16 :goto_10

    :cond_19
    if-eqz v2, :cond_1d

    if-ne v6, v10, :cond_1a

    new-instance v6, Lf/h/a/c/i1/r;

    invoke-direct {v6, v3}, Lf/h/a/c/i1/r;-><init>(I)V

    iget-object v7, v6, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v7, v4, v3, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    invoke-static {v6}, Lf/g/j/k/a;->J0(Lf/h/a/c/i1/r;)Lf/h/a/c/i1/l$a;

    move-result-object v3

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/l;->b(Lf/h/a/c/i1/l$a;)Lf/h/a/c/i1/l;

    move-result-object v2

    goto :goto_e

    :cond_1a
    if-ne v6, v9, :cond_1b

    new-instance v6, Lf/h/a/c/i1/r;

    invoke-direct {v6, v3}, Lf/h/a/c/i1/r;-><init>(I)V

    iget-object v7, v6, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v7, v4, v3, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    invoke-virtual {v6, v9}, Lf/h/a/c/i1/r;->D(I)V

    invoke-static {v6, v4, v4}, Lf/g/j/k/a;->L0(Lf/h/a/c/i1/r;ZZ)Lf/h/a/c/a1/u;

    move-result-object v3

    iget-object v3, v3, Lf/h/a/c/a1/u;->a:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    invoke-static {v3, v6}, Lf/h/a/c/i1/l;->a(Ljava/util/List;Ljava/util/List;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v3

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/l;->f(Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v24

    new-instance v3, Lf/h/a/c/i1/l;

    iget v14, v2, Lf/h/a/c/i1/l;->a:I

    iget v15, v2, Lf/h/a/c/i1/l;->b:I

    iget v6, v2, Lf/h/a/c/i1/l;->c:I

    iget v7, v2, Lf/h/a/c/i1/l;->d:I

    iget v13, v2, Lf/h/a/c/i1/l;->e:I

    iget v11, v2, Lf/h/a/c/i1/l;->g:I

    iget v10, v2, Lf/h/a/c/i1/l;->h:I

    iget-wide v8, v2, Lf/h/a/c/i1/l;->j:J

    iget-object v2, v2, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    move/from16 v18, v13

    move-object v13, v3

    move/from16 v16, v6

    move/from16 v17, v7

    move/from16 v19, v11

    move/from16 v20, v10

    move-wide/from16 v21, v8

    move-object/from16 v23, v2

    invoke-direct/range {v13 .. v24}, Lf/h/a/c/i1/l;-><init>(IIIIIIIJLf/h/a/c/i1/l$a;Lcom/google/android/exoplayer2/metadata/Metadata;)V

    move/from16 p2, v5

    goto/16 :goto_f

    :cond_1b
    if-ne v6, v12, :cond_1c

    new-instance v6, Lf/h/a/c/i1/r;

    invoke-direct {v6, v3}, Lf/h/a/c/i1/r;-><init>(I)V

    iget-object v7, v6, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v7, v4, v3, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    const/4 v3, 0x4

    invoke-virtual {v6, v3}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v14

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    const-string v7, "US-ASCII"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Lf/h/a/c/i1/r;->o(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    invoke-virtual {v6, v3}, Lf/h/a/c/i1/r;->n(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v17

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v18

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v19

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v20

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    new-array v7, v3, [B

    iget-object v8, v6, Lf/h/a/c/i1/r;->a:[B

    iget v9, v6, Lf/h/a/c/i1/r;->b:I

    invoke-static {v8, v9, v7, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v8, v6, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v8, v3

    iput v8, v6, Lf/h/a/c/i1/r;->b:I

    new-instance v3, Lcom/google/android/exoplayer2/metadata/flac/PictureFrame;

    move-object v13, v3

    move-object/from16 v21, v7

    invoke-direct/range {v13 .. v21}, Lcom/google/android/exoplayer2/metadata/flac/PictureFrame;-><init>(ILjava/lang/String;Ljava/lang/String;IIII[B)V

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    invoke-static {v6, v3}, Lf/h/a/c/i1/l;->a(Ljava/util/List;Ljava/util/List;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v3

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/l;->f(Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v24

    new-instance v3, Lf/h/a/c/i1/l;

    iget v14, v2, Lf/h/a/c/i1/l;->a:I

    iget v15, v2, Lf/h/a/c/i1/l;->b:I

    iget v6, v2, Lf/h/a/c/i1/l;->c:I

    iget v7, v2, Lf/h/a/c/i1/l;->d:I

    iget v8, v2, Lf/h/a/c/i1/l;->e:I

    iget v9, v2, Lf/h/a/c/i1/l;->g:I

    iget v10, v2, Lf/h/a/c/i1/l;->h:I

    move/from16 p2, v5

    iget-wide v4, v2, Lf/h/a/c/i1/l;->j:J

    iget-object v2, v2, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    move-object v13, v3

    move/from16 v16, v6

    move/from16 v17, v7

    move/from16 v18, v8

    move/from16 v19, v9

    move/from16 v20, v10

    move-wide/from16 v21, v4

    move-object/from16 v23, v2

    invoke-direct/range {v13 .. v24}, Lf/h/a/c/i1/l;-><init>(IIIIIIIJLf/h/a/c/i1/l$a;Lcom/google/android/exoplayer2/metadata/Metadata;)V

    :goto_f
    move-object v2, v3

    goto :goto_10

    :cond_1c
    move/from16 p2, v5

    invoke-virtual {v1, v3}, Lf/h/a/c/a1/e;->i(I)V

    :goto_10
    sget v3, Lf/h/a/c/i1/a0;->a:I

    iput-object v2, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    move/from16 v3, p2

    const/4 v4, 0x0

    const/16 v8, 0x18

    const/4 v9, 0x4

    const/4 v10, 0x3

    const/4 v11, 0x7

    goto/16 :goto_d

    :cond_1d
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_1e
    iget-object v1, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    iget v1, v1, Lf/h/a/c/i1/l;->c:I

    invoke-static {v1, v12}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lf/h/a/c/a1/y/c;->i:I

    iget-object v1, v0, Lf/h/a/c/a1/y/c;->e:Lf/h/a/c/a1/s;

    sget v2, Lf/h/a/c/i1/a0;->a:I

    iget-object v2, v0, Lf/h/a/c/a1/y/c;->h:Lf/h/a/c/i1/l;

    iget-object v3, v0, Lf/h/a/c/a1/y/c;->a:[B

    iget-object v4, v0, Lf/h/a/c/a1/y/c;->g:Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-virtual {v2, v3, v4}, Lf/h/a/c/i1/l;->e([BLcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format;

    move-result-object v2

    invoke-interface {v1, v2}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    const/4 v2, 0x4

    iput v2, v0, Lf/h/a/c/a1/y/c;->f:I

    const/4 v4, 0x0

    return v4

    :cond_1f
    const/4 v2, 0x4

    new-array v8, v2, [B

    invoke-virtual {v1, v8, v4, v2, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    aget-byte v1, v8, v4

    int-to-long v1, v1

    const-wide/16 v9, 0xff

    and-long/2addr v1, v9

    const/16 v4, 0x18

    shl-long/2addr v1, v4

    aget-byte v3, v8, v3

    int-to-long v3, v3

    and-long/2addr v3, v9

    shl-long/2addr v3, v7

    or-long/2addr v1, v3

    aget-byte v3, v8, v5

    int-to-long v3, v3

    and-long/2addr v3, v9

    shl-long/2addr v3, v6

    or-long/2addr v1, v3

    const/4 v3, 0x3

    aget-byte v4, v8, v3

    int-to-long v4, v4

    and-long/2addr v4, v9

    or-long/2addr v1, v4

    const-wide/32 v4, 0x664c6143

    cmp-long v6, v1, v4

    if-nez v6, :cond_20

    iput v3, v0, Lf/h/a/c/a1/y/c;->f:I

    const/4 v2, 0x0

    return v2

    :cond_20
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Failed to read FLAC stream marker."

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_21
    const/4 v2, 0x0

    iget-object v3, v0, Lf/h/a/c/a1/y/c;->a:[B

    array-length v4, v3

    invoke-virtual {v1, v3, v2, v4, v2}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iput v2, v1, Lf/h/a/c/a1/e;->f:I

    iput v5, v0, Lf/h/a/c/a1/y/c;->f:I

    return v2

    :cond_22
    const/4 v2, 0x0

    iput v2, v1, Lf/h/a/c/a1/e;->f:I

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v4

    invoke-static {v1, v3}, Lf/g/j/k/a;->y0(Lf/h/a/c/a1/e;Z)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v7

    sub-long/2addr v7, v4

    long-to-int v4, v7

    invoke-virtual {v1, v4}, Lf/h/a/c/a1/e;->i(I)V

    iput-object v6, v0, Lf/h/a/c/a1/y/c;->g:Lcom/google/android/exoplayer2/metadata/Metadata;

    iput v3, v0, Lf/h/a/c/a1/y/c;->f:I

    return v2
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 2

    iput-object p1, p0, Lf/h/a/c/a1/y/c;->d:Lf/h/a/c/a1/i;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/y/c;->e:Lf/h/a/c/a1/s;

    invoke-interface {p1}, Lf/h/a/c/a1/i;->k()V

    return-void
.end method

.method public f(JJ)V
    .locals 4

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    iput v0, p0, Lf/h/a/c/a1/y/c;->f:I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/h/a/c/a1/y/c;->k:Lf/h/a/c/a1/y/b;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p3, p4}, Lf/h/a/c/a1/a;->e(J)V

    :cond_1
    :goto_0
    cmp-long p1, p3, v1

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    const-wide/16 v1, -0x1

    :goto_1
    iput-wide v1, p0, Lf/h/a/c/a1/y/c;->m:J

    iput v0, p0, Lf/h/a/c/a1/y/c;->l:I

    iget-object p1, p0, Lf/h/a/c/a1/y/c;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->x()V

    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lf/g/j/k/a;->y0(Lf/h/a/c/a1/e;Z)Lcom/google/android/exoplayer2/metadata/Metadata;

    const/4 v1, 0x4

    new-array v2, v1, [B

    invoke-virtual {p1, v2, v0, v1, v0}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    aget-byte p1, v2, v0

    int-to-long v3, p1

    const-wide/16 v5, 0xff

    and-long/2addr v3, v5

    const/16 p1, 0x18

    shl-long/2addr v3, p1

    const/4 p1, 0x2

    const/4 v1, 0x1

    aget-byte v7, v2, v1

    int-to-long v7, v7

    and-long/2addr v7, v5

    const/16 v9, 0x10

    shl-long/2addr v7, v9

    or-long/2addr v3, v7

    const/4 v7, 0x3

    aget-byte p1, v2, p1

    int-to-long v8, p1

    and-long/2addr v8, v5

    const/16 p1, 0x8

    shl-long/2addr v8, p1

    or-long/2addr v3, v8

    aget-byte p1, v2, v7

    int-to-long v7, p1

    and-long/2addr v5, v7

    or-long v2, v3, v5

    const-wide/32 v4, 0x664c6143

    cmp-long p1, v2, v4

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public release()V
    .locals 0

    return-void
.end method
