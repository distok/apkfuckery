.class public final Lf/h/a/c/a1/y/b;
.super Lf/h/a/c/a1/a;
.source "FlacBinarySearchSeeker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/y/b$b;
    }
.end annotation


# direct methods
.method public constructor <init>(Lf/h/a/c/i1/l;IJJ)V
    .locals 16

    move-object/from16 v0, p1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lf/h/a/c/a1/y/a;

    invoke-direct {v1, v0}, Lf/h/a/c/a1/y/a;-><init>(Lf/h/a/c/i1/l;)V

    new-instance v2, Lf/h/a/c/a1/y/b$b;

    const/4 v3, 0x0

    move/from16 v4, p2

    invoke-direct {v2, v0, v4, v3}, Lf/h/a/c/a1/y/b$b;-><init>(Lf/h/a/c/i1/l;ILf/h/a/c/a1/y/b$a;)V

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/l;->d()J

    move-result-wide v3

    iget-wide v7, v0, Lf/h/a/c/i1/l;->j:J

    iget v5, v0, Lf/h/a/c/i1/l;->d:I

    if-lez v5, :cond_0

    int-to-long v5, v5

    iget v9, v0, Lf/h/a/c/i1/l;->c:I

    int-to-long v9, v9

    add-long/2addr v5, v9

    const-wide/16 v9, 0x2

    div-long/2addr v5, v9

    const-wide/16 v9, 0x1

    goto :goto_1

    :cond_0
    iget v5, v0, Lf/h/a/c/i1/l;->a:I

    iget v6, v0, Lf/h/a/c/i1/l;->b:I

    if-ne v5, v6, :cond_1

    if-lez v5, :cond_1

    int-to-long v5, v5

    goto :goto_0

    :cond_1
    const-wide/16 v5, 0x1000

    :goto_0
    iget v9, v0, Lf/h/a/c/i1/l;->g:I

    int-to-long v9, v9

    mul-long v5, v5, v9

    iget v9, v0, Lf/h/a/c/i1/l;->h:I

    int-to-long v9, v9

    mul-long v5, v5, v9

    const-wide/16 v9, 0x8

    div-long/2addr v5, v9

    const-wide/16 v9, 0x40

    :goto_1
    add-long v13, v5, v9

    const/4 v5, 0x6

    iget v0, v0, Lf/h/a/c/i1/l;->c:I

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    const-wide/16 v5, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v9, p3

    move-wide/from16 v11, p5

    invoke-direct/range {v0 .. v15}, Lf/h/a/c/a1/a;-><init>(Lf/h/a/c/a1/a$d;Lf/h/a/c/a1/a$f;JJJJJJI)V

    return-void
.end method
