.class public final Lf/h/a/c/a1/y/b$b;
.super Ljava/lang/Object;
.source "FlacBinarySearchSeeker.java"

# interfaces
.implements Lf/h/a/c/a1/a$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/y/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/i1/l;

.field public final b:I

.field public final c:Lf/h/a/c/a1/k$a;


# direct methods
.method public constructor <init>(Lf/h/a/c/i1/l;ILf/h/a/c/a1/y/b$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/y/b$b;->a:Lf/h/a/c/i1/l;

    iput p2, p0, Lf/h/a/c/a1/y/b$b;->b:I

    new-instance p1, Lf/h/a/c/a1/k$a;

    invoke-direct {p1}, Lf/h/a/c/a1/k$a;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/y/b$b;->c:Lf/h/a/c/a1/k$a;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;J)Lf/h/a/c/a1/a$e;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-wide v0, p1, Lf/h/a/c/a1/e;->d:J

    invoke-virtual {p0, p1}, Lf/h/a/c/a1/y/b$b;->c(Lf/h/a/c/a1/e;)J

    move-result-wide v2

    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v4

    iget-object v6, p0, Lf/h/a/c/a1/y/b$b;->a:Lf/h/a/c/i1/l;

    iget v6, v6, Lf/h/a/c/i1/l;->c:I

    const/4 v7, 0x6

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Lf/h/a/c/a1/e;->a(IZ)Z

    invoke-virtual {p0, p1}, Lf/h/a/c/a1/y/b$b;->c(Lf/h/a/c/a1/e;)J

    move-result-wide v6

    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v8

    cmp-long p1, v2, p2

    if-gtz p1, :cond_0

    cmp-long p1, v6, p2

    if-lez p1, :cond_0

    invoke-static {v4, v5}, Lf/h/a/c/a1/a$e;->b(J)Lf/h/a/c/a1/a$e;

    move-result-object p1

    return-object p1

    :cond_0
    cmp-long p1, v6, p2

    if-gtz p1, :cond_1

    invoke-static {v6, v7, v8, v9}, Lf/h/a/c/a1/a$e;->c(JJ)Lf/h/a/c/a1/a$e;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-static {v2, v3, v0, v1}, Lf/h/a/c/a1/a$e;->a(JJ)Lf/h/a/c/a1/a$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b()V
    .locals 0

    invoke-static {p0}, Lf/h/a/c/a1/b;->a(Lf/h/a/c/a1/a$f;)V

    return-void
.end method

.method public final c(Lf/h/a/c/a1/e;)J
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    :goto_0
    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v0

    iget-wide v2, p1, Lf/h/a/c/a1/e;->c:J

    const-wide/16 v4, 0x6

    sub-long/2addr v2, v4

    const/4 v6, 0x0

    cmp-long v7, v0, v2

    if-gez v7, :cond_1

    iget-object v0, p0, Lf/h/a/c/a1/y/b$b;->a:Lf/h/a/c/i1/l;

    iget v1, p0, Lf/h/a/c/a1/y/b$b;->b:I

    iget-object v2, p0, Lf/h/a/c/a1/y/b$b;->c:Lf/h/a/c/a1/k$a;

    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v7

    const/4 v3, 0x2

    new-array v9, v3, [B

    invoke-virtual {p1, v9, v6, v3, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    aget-byte v10, v9, v6

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    const/4 v11, 0x1

    aget-byte v12, v9, v11

    and-int/lit16 v12, v12, 0xff

    or-int/2addr v10, v12

    if-eq v10, v1, :cond_0

    iput v6, p1, Lf/h/a/c/a1/e;->f:I

    iget-wide v0, p1, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v7, v0

    long-to-int v0, v7

    invoke-virtual {p1, v0, v6}, Lf/h/a/c/a1/e;->a(IZ)Z

    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    new-instance v10, Lf/h/a/c/i1/r;

    const/16 v12, 0x10

    invoke-direct {v10, v12}, Lf/h/a/c/i1/r;-><init>(I)V

    iget-object v12, v10, Lf/h/a/c/i1/r;->a:[B

    invoke-static {v9, v6, v12, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v9, v10, Lf/h/a/c/i1/r;->a:[B

    const/16 v12, 0xe

    invoke-static {p1, v9, v3, v12}, Lf/g/j/k/a;->z0(Lf/h/a/c/a1/e;[BII)I

    move-result v3

    invoke-virtual {v10, v3}, Lf/h/a/c/i1/r;->B(I)V

    iput v6, p1, Lf/h/a/c/a1/e;->f:I

    iget-wide v12, p1, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v7, v12

    long-to-int v3, v7

    invoke-virtual {p1, v3, v6}, Lf/h/a/c/a1/e;->a(IZ)Z

    invoke-static {v10, v0, v1, v2}, Lf/h/a/c/a1/k;->b(Lf/h/a/c/i1/r;Lf/h/a/c/i1/l;ILf/h/a/c/a1/k$a;)Z

    move-result v0

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {p1, v11, v6}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v0

    iget-wide v2, p1, Lf/h/a/c/a1/e;->c:J

    sub-long v4, v2, v4

    cmp-long v7, v0, v4

    if-ltz v7, :cond_2

    invoke-virtual {p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v0

    sub-long/2addr v2, v0

    long-to-int v0, v2

    invoke-virtual {p1, v0, v6}, Lf/h/a/c/a1/e;->a(IZ)Z

    iget-object p1, p0, Lf/h/a/c/a1/y/b$b;->a:Lf/h/a/c/i1/l;

    iget-wide v0, p1, Lf/h/a/c/i1/l;->j:J

    return-wide v0

    :cond_2
    iget-object p1, p0, Lf/h/a/c/a1/y/b$b;->c:Lf/h/a/c/a1/k$a;

    iget-wide v0, p1, Lf/h/a/c/a1/k$a;->a:J

    return-wide v0
.end method
