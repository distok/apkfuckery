.class public final Lf/h/a/c/a1/a$e;
.super Ljava/lang/Object;
.source "BinarySearchSeeker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static final d:Lf/h/a/c/a1/a$e;


# instance fields
.field public final a:I

.field public final b:J

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v6, Lf/h/a/c/a1/a$e;

    const/4 v1, -0x3

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    const-wide/16 v4, -0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/a/c/a1/a$e;-><init>(IJJ)V

    sput-object v6, Lf/h/a/c/a1/a$e;->d:Lf/h/a/c/a1/a$e;

    return-void
.end method

.method public constructor <init>(IJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/h/a/c/a1/a$e;->a:I

    iput-wide p2, p0, Lf/h/a/c/a1/a$e;->b:J

    iput-wide p4, p0, Lf/h/a/c/a1/a$e;->c:J

    return-void
.end method

.method public static a(JJ)Lf/h/a/c/a1/a$e;
    .locals 7

    new-instance v6, Lf/h/a/c/a1/a$e;

    const/4 v1, -0x1

    move-object v0, v6

    move-wide v2, p0

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lf/h/a/c/a1/a$e;-><init>(IJJ)V

    return-object v6
.end method

.method public static b(J)Lf/h/a/c/a1/a$e;
    .locals 7

    new-instance v6, Lf/h/a/c/a1/a$e;

    const/4 v1, 0x0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, v6

    move-wide v4, p0

    invoke-direct/range {v0 .. v5}, Lf/h/a/c/a1/a$e;-><init>(IJJ)V

    return-object v6
.end method

.method public static c(JJ)Lf/h/a/c/a1/a$e;
    .locals 7

    new-instance v6, Lf/h/a/c/a1/a$e;

    const/4 v1, -0x2

    move-object v0, v6

    move-wide v2, p0

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lf/h/a/c/a1/a$e;-><init>(IJJ)V

    return-object v6
.end method
