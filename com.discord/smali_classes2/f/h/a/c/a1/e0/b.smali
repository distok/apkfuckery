.class public final Lf/h/a/c/a1/e0/b;
.super Ljava/lang/Object;
.source "Ac3Reader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/j;


# instance fields
.field public final a:Lf/h/a/c/i1/q;

.field public final b:Lf/h/a/c/i1/r;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lf/h/a/c/a1/s;

.field public f:I

.field public g:I

.field public h:Z

.field public i:J

.field public j:Lcom/google/android/exoplayer2/Format;

.field public k:I

.field public l:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/q;

    const/16 v1, 0x80

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lf/h/a/c/i1/q;-><init>([B)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/b;->a:Lf/h/a/c/i1/q;

    new-instance v1, Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/q;->a:[B

    invoke-direct {v1, v0}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object v1, p0, Lf/h/a/c/a1/e0/b;->b:Lf/h/a/c/i1/r;

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/b;->f:I

    iput-object p1, p0, Lf/h/a/c/a1/e0/b;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public b(Lf/h/a/c/i1/r;)V
    .locals 31

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-lez v2, :cond_3d

    iget v2, v0, Lf/h/a/c/a1/e0/b;->f:I

    const/16 v3, 0xb

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eqz v2, :cond_37

    if-eq v2, v6, :cond_2

    if-eq v2, v5, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    iget v3, v0, Lf/h/a/c/a1/e0/b;->k:I

    iget v5, v0, Lf/h/a/c/a1/e0/b;->g:I

    sub-int/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/b;->e:Lf/h/a/c/a1/s;

    invoke-interface {v3, v1, v2}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v3, v0, Lf/h/a/c/a1/e0/b;->g:I

    add-int/2addr v3, v2

    iput v3, v0, Lf/h/a/c/a1/e0/b;->g:I

    iget v9, v0, Lf/h/a/c/a1/e0/b;->k:I

    if-ne v3, v9, :cond_0

    iget-object v5, v0, Lf/h/a/c/a1/e0/b;->e:Lf/h/a/c/a1/s;

    iget-wide v6, v0, Lf/h/a/c/a1/e0/b;->l:J

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-interface/range {v5 .. v11}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iget-wide v2, v0, Lf/h/a/c/a1/e0/b;->l:J

    iget-wide v5, v0, Lf/h/a/c/a1/e0/b;->i:J

    add-long/2addr v2, v5

    iput-wide v2, v0, Lf/h/a/c/a1/e0/b;->l:J

    iput v4, v0, Lf/h/a/c/a1/e0/b;->f:I

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lf/h/a/c/a1/e0/b;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v7

    iget v8, v0, Lf/h/a/c/a1/e0/b;->g:I

    const/16 v9, 0x80

    rsub-int v8, v8, 0x80

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    iget v8, v0, Lf/h/a/c/a1/e0/b;->g:I

    iget-object v10, v1, Lf/h/a/c/i1/r;->a:[B

    iget v11, v1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v10, v11, v2, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v2, v7

    iput v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v2, v0, Lf/h/a/c/a1/e0/b;->g:I

    add-int/2addr v2, v7

    iput v2, v0, Lf/h/a/c/a1/e0/b;->g:I

    if-ne v2, v9, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/h/a/c/a1/e0/b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/b;->a:Lf/h/a/c/i1/q;

    sget-object v7, Lf/h/a/c/w0/g;->d:[I

    sget-object v8, Lf/h/a/c/w0/g;->b:[I

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->d()I

    move-result v10

    const/16 v11, 0x28

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    const/4 v11, 0x5

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->f(I)I

    move-result v12

    const/16 v13, 0xa

    if-le v12, v13, :cond_4

    const/4 v12, 0x1

    goto :goto_2

    :cond_4
    const/4 v12, 0x0

    :goto_2
    invoke-virtual {v2, v10}, Lf/h/a/c/i1/q;->j(I)V

    const/4 v14, 0x3

    const/16 v15, 0x8

    if-eqz v12, :cond_2f

    const/16 v12, 0x10

    invoke-virtual {v2, v12}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v9

    if-eqz v9, :cond_7

    if-eq v9, v6, :cond_6

    if-eq v9, v5, :cond_5

    const/4 v9, -0x1

    goto :goto_3

    :cond_5
    const/4 v9, 0x2

    goto :goto_3

    :cond_6
    const/4 v9, 0x1

    goto :goto_3

    :cond_7
    const/4 v9, 0x0

    :goto_3
    invoke-virtual {v2, v14}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v3

    add-int/2addr v3, v6

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v4

    if-ne v4, v14, :cond_8

    sget-object v8, Lf/h/a/c/w0/g;->c:[I

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v16

    aget v8, v8, v16

    const/4 v10, 0x6

    const/16 v18, 0x3

    goto :goto_4

    :cond_8
    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v16

    sget-object v17, Lf/h/a/c/w0/g;->a:[I

    aget v17, v17, v16

    aget v8, v8, v4

    move/from16 v18, v16

    move/from16 v10, v17

    :goto_4
    mul-int/lit16 v5, v10, 0x100

    invoke-virtual {v2, v14}, Lf/h/a/c/i1/q;->f(I)I

    move-result v12

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    aget v7, v7, v12

    add-int v7, v7, v16

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_9
    if-nez v12, :cond_a

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v13

    if-eqz v13, :cond_a

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_a
    if-ne v9, v6, :cond_b

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v13

    if-eqz v13, :cond_b

    const/16 v13, 0x10

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    :cond_b
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v13

    if-eqz v13, :cond_24

    const/4 v13, 0x2

    if-le v12, v13, :cond_c

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    :cond_c
    and-int/lit8 v19, v12, 0x1

    if-eqz v19, :cond_d

    if-le v12, v13, :cond_d

    const/4 v13, 0x6

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_5

    :cond_d
    const/4 v13, 0x6

    :goto_5
    and-int/lit8 v17, v12, 0x4

    if-eqz v17, :cond_e

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    :cond_e
    if-eqz v16, :cond_f

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v13

    if-eqz v13, :cond_f

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    :cond_f
    if-nez v9, :cond_24

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v13

    if-eqz v13, :cond_10

    const/4 v13, 0x6

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_6

    :cond_10
    const/4 v13, 0x6

    :goto_6
    if-nez v12, :cond_11

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_11

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    :cond_11
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_12

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    :cond_12
    const/4 v13, 0x2

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->f(I)I

    move-result v15

    if-ne v15, v6, :cond_14

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    :cond_13
    :goto_7
    const/4 v6, 0x2

    goto/16 :goto_a

    :cond_14
    if-ne v15, v13, :cond_15

    const/16 v13, 0xc

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_7

    :cond_15
    if-ne v15, v14, :cond_13

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->f(I)I

    move-result v13

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v15

    if-eqz v15, :cond_1e

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v15

    if-eqz v15, :cond_16

    const/4 v15, 0x4

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_8

    :cond_16
    const/4 v15, 0x4

    :goto_8
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_17

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_17
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_18

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_18
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_19

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_19
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_1a

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_1a
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_1b

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_1b
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_1c

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_1c
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_1e

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_1d

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_1d
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v16

    if-eqz v16, :cond_1e

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    :cond_1e
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v15

    if-eqz v15, :cond_1f

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v15

    if-eqz v15, :cond_1f

    const/4 v15, 0x7

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v15

    if-eqz v15, :cond_1f

    const/16 v15, 0x8

    invoke-virtual {v2, v15}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_9

    :cond_1f
    const/16 v15, 0x8

    :goto_9
    const/4 v6, 0x2

    add-int/2addr v13, v6

    mul-int/lit8 v13, v13, 0x8

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->c()V

    :goto_a
    if-ge v12, v6, :cond_21

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v6

    const/16 v13, 0xe

    if-eqz v6, :cond_20

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    :cond_20
    if-nez v12, :cond_21

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v6

    if-eqz v6, :cond_21

    invoke-virtual {v2, v13}, Lf/h/a/c/i1/q;->l(I)V

    :cond_21
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v6

    if-eqz v6, :cond_24

    move/from16 v6, v18

    if-nez v6, :cond_22

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_c

    :cond_22
    const/4 v13, 0x0

    :goto_b
    if-ge v13, v10, :cond_25

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v15

    if-eqz v15, :cond_23

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    :cond_23
    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    :cond_24
    move/from16 v6, v18

    :cond_25
    :goto_c
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v10

    if-eqz v10, :cond_2a

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    const/4 v10, 0x2

    if-ne v12, v10, :cond_26

    const/4 v11, 0x4

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->l(I)V

    :cond_26
    const/4 v11, 0x6

    if-lt v12, v11, :cond_27

    invoke-virtual {v2, v10}, Lf/h/a/c/i1/q;->l(I)V

    :cond_27
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v10

    if-eqz v10, :cond_28

    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_d

    :cond_28
    const/16 v10, 0x8

    :goto_d
    if-nez v12, :cond_29

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v11

    if-eqz v11, :cond_29

    invoke-virtual {v2, v10}, Lf/h/a/c/i1/q;->l(I)V

    :cond_29
    if-ge v4, v14, :cond_2a

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->k()V

    :cond_2a
    if-nez v9, :cond_2b

    if-eq v6, v14, :cond_2b

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->k()V

    :cond_2b
    const/4 v4, 0x2

    if-ne v9, v4, :cond_2d

    if-eq v6, v14, :cond_2c

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v4

    if-eqz v4, :cond_2d

    :cond_2c
    const/4 v4, 0x6

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_e

    :cond_2d
    const/4 v4, 0x6

    :goto_e
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v6

    if-eqz v6, :cond_2e

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/q;->f(I)I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_2e

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    if-ne v2, v6, :cond_2e

    const-string v2, "audio/eac3-joc"

    goto :goto_12

    :cond_2e
    const-string v2, "audio/eac3"

    goto :goto_12

    :cond_2f
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->l(I)V

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v4

    if-ne v4, v14, :cond_30

    const/4 v3, 0x0

    goto :goto_f

    :cond_30
    const-string v3, "audio/ac3"

    :goto_f
    const/4 v5, 0x6

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    invoke-static {v4, v5}, Lf/h/a/c/w0/g;->a(II)I

    move-result v5

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v2, v14}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    and-int/lit8 v9, v6, 0x1

    if-eqz v9, :cond_31

    const/4 v9, 0x1

    if-eq v6, v9, :cond_31

    const/4 v9, 0x2

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_10

    :cond_31
    const/4 v9, 0x2

    :goto_10
    and-int/lit8 v10, v6, 0x4

    if-eqz v10, :cond_32

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/q;->l(I)V

    :cond_32
    if-ne v6, v9, :cond_33

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/q;->l(I)V

    :cond_33
    array-length v9, v8

    if-ge v4, v9, :cond_34

    aget v10, v8, v4

    goto :goto_11

    :cond_34
    const/4 v10, -0x1

    :goto_11
    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v2

    aget v4, v7, v6

    add-int v7, v4, v2

    const/16 v2, 0x600

    move-object v2, v3

    move v3, v5

    move v8, v10

    const/16 v5, 0x600

    :goto_12
    iget-object v4, v0, Lf/h/a/c/a1/e0/b;->j:Lcom/google/android/exoplayer2/Format;

    if-eqz v4, :cond_35

    iget v6, v4, Lcom/google/android/exoplayer2/Format;->y:I

    if-ne v7, v6, :cond_35

    iget v6, v4, Lcom/google/android/exoplayer2/Format;->z:I

    if-ne v8, v6, :cond_35

    iget-object v4, v4, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    if-eq v2, v4, :cond_36

    :cond_35
    iget-object v4, v0, Lf/h/a/c/a1/e0/b;->d:Ljava/lang/String;

    const/16 v22, 0x0

    const/16 v23, -0x1

    const/16 v24, -0x1

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    iget-object v6, v0, Lf/h/a/c/a1/e0/b;->c:Ljava/lang/String;

    move-object/from16 v20, v4

    move-object/from16 v21, v2

    move/from16 v25, v7

    move/from16 v26, v8

    move-object/from16 v30, v6

    invoke-static/range {v20 .. v30}, Lcom/google/android/exoplayer2/Format;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v2

    iput-object v2, v0, Lf/h/a/c/a1/e0/b;->j:Lcom/google/android/exoplayer2/Format;

    iget-object v4, v0, Lf/h/a/c/a1/e0/b;->e:Lf/h/a/c/a1/s;

    invoke-interface {v4, v2}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    :cond_36
    iput v3, v0, Lf/h/a/c/a1/e0/b;->k:I

    const-wide/32 v2, 0xf4240

    int-to-long v4, v5

    mul-long v4, v4, v2

    iget-object v2, v0, Lf/h/a/c/a1/e0/b;->j:Lcom/google/android/exoplayer2/Format;

    iget v2, v2, Lcom/google/android/exoplayer2/Format;->z:I

    int-to-long v2, v2

    div-long/2addr v4, v2

    iput-wide v4, v0, Lf/h/a/c/a1/e0/b;->i:J

    iget-object v2, v0, Lf/h/a/c/a1/e0/b;->b:Lf/h/a/c/i1/r;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/b;->e:Lf/h/a/c/a1/s;

    iget-object v3, v0, Lf/h/a/c/a1/e0/b;->b:Lf/h/a/c/i1/r;

    const/16 v4, 0x80

    invoke-interface {v2, v3, v4}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    const/4 v2, 0x2

    iput v2, v0, Lf/h/a/c/a1/e0/b;->f:I

    goto/16 :goto_0

    :cond_37
    :goto_13
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    const/16 v4, 0x77

    if-lez v2, :cond_3c

    iget-boolean v2, v0, Lf/h/a/c/a1/e0/b;->h:Z

    if-nez v2, :cond_39

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    if-ne v2, v3, :cond_38

    const/4 v2, 0x1

    goto :goto_14

    :cond_38
    const/4 v2, 0x0

    :goto_14
    iput-boolean v2, v0, Lf/h/a/c/a1/e0/b;->h:Z

    goto :goto_13

    :cond_39
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    if-ne v2, v4, :cond_3a

    const/4 v5, 0x0

    iput-boolean v5, v0, Lf/h/a/c/a1/e0/b;->h:Z

    const/4 v6, 0x1

    goto :goto_16

    :cond_3a
    if-ne v2, v3, :cond_3b

    const/4 v6, 0x1

    goto :goto_15

    :cond_3b
    const/4 v6, 0x0

    :goto_15
    iput-boolean v6, v0, Lf/h/a/c/a1/e0/b;->h:Z

    goto :goto_13

    :cond_3c
    const/4 v6, 0x0

    :goto_16
    if-eqz v6, :cond_0

    const/4 v2, 0x1

    iput v2, v0, Lf/h/a/c/a1/e0/b;->f:I

    iget-object v5, v0, Lf/h/a/c/a1/e0/b;->b:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    const/4 v6, 0x0

    aput-byte v3, v5, v6

    aput-byte v4, v5, v2

    const/4 v2, 0x2

    iput v2, v0, Lf/h/a/c/a1/e0/b;->g:I

    goto/16 :goto_0

    :cond_3d
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/b;->f:I

    iput v0, p0, Lf/h/a/c/a1/e0/b;->g:I

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/b;->h:Z

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 1

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/b;->d:Ljava/lang/String;

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result p2

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/e0/b;->e:Lf/h/a/c/a1/s;

    return-void
.end method

.method public f(JI)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/a1/e0/b;->l:J

    return-void
.end method
