.class public final Lf/h/a/c/a1/e0/l;
.super Ljava/lang/Object;
.source "H264Reader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/e0/l$b;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/e0/x;

.field public final b:Z

.field public final c:Z

.field public final d:Lf/h/a/c/a1/e0/q;

.field public final e:Lf/h/a/c/a1/e0/q;

.field public final f:Lf/h/a/c/a1/e0/q;

.field public g:J

.field public final h:[Z

.field public i:Ljava/lang/String;

.field public j:Lf/h/a/c/a1/s;

.field public k:Lf/h/a/c/a1/e0/l$b;

.field public l:Z

.field public m:J

.field public n:Z

.field public final o:Lf/h/a/c/i1/r;


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/e0/x;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l;->a:Lf/h/a/c/a1/e0/x;

    iput-boolean p2, p0, Lf/h/a/c/a1/e0/l;->b:Z

    iput-boolean p3, p0, Lf/h/a/c/a1/e0/l;->c:Z

    const/4 p1, 0x3

    new-array p1, p1, [Z

    iput-object p1, p0, Lf/h/a/c/a1/e0/l;->h:[Z

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/4 p2, 0x7

    const/16 p3, 0x80

    invoke-direct {p1, p2, p3}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/16 p2, 0x8

    invoke-direct {p1, p2, p3}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/4 p2, 0x6

    invoke-direct {p1, p2, p3}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l;->f:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l;->o:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public final a([BII)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    iget-boolean v4, v0, Lf/h/a/c/a1/e0/l;->l:Z

    if-eqz v4, :cond_0

    iget-object v4, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-boolean v4, v4, Lf/h/a/c/a1/e0/l$b;->c:Z

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v4, v1, v2, v3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    iget-object v4, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v4, v1, v2, v3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    :cond_1
    iget-object v4, v0, Lf/h/a/c/a1/e0/l;->f:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v4, v1, v2, v3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    iget-object v4, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-boolean v5, v4, Lf/h/a/c/a1/e0/l$b;->k:Z

    if-nez v5, :cond_2

    goto/16 :goto_5

    :cond_2
    sub-int/2addr v3, v2

    iget-object v5, v4, Lf/h/a/c/a1/e0/l$b;->g:[B

    array-length v6, v5

    iget v7, v4, Lf/h/a/c/a1/e0/l$b;->h:I

    add-int/2addr v7, v3

    const/4 v8, 0x2

    if-ge v6, v7, :cond_3

    mul-int/lit8 v7, v7, 0x2

    invoke-static {v5, v7}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v5

    iput-object v5, v4, Lf/h/a/c/a1/e0/l$b;->g:[B

    :cond_3
    iget-object v5, v4, Lf/h/a/c/a1/e0/l$b;->g:[B

    iget v6, v4, Lf/h/a/c/a1/e0/l$b;->h:I

    invoke-static {v1, v2, v5, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, v4, Lf/h/a/c/a1/e0/l$b;->h:I

    add-int/2addr v1, v3

    iput v1, v4, Lf/h/a/c/a1/e0/l$b;->h:I

    iget-object v2, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    iget-object v3, v4, Lf/h/a/c/a1/e0/l$b;->g:[B

    iput-object v3, v2, Lf/h/a/c/i1/s;->a:[B

    const/4 v3, 0x0

    iput v3, v2, Lf/h/a/c/i1/s;->c:I

    iput v1, v2, Lf/h/a/c/i1/s;->b:I

    iput v3, v2, Lf/h/a/c/i1/s;->d:I

    invoke-virtual {v2}, Lf/h/a/c/i1/s;->a()V

    iget-object v1, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/s;->b(I)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_5

    :cond_4
    iget-object v1, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v1}, Lf/h/a/c/i1/s;->i()V

    iget-object v1, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v1, v8}, Lf/h/a/c/i1/s;->e(I)I

    move-result v1

    iget-object v2, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    const/4 v5, 0x5

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/s;->j(I)V

    iget-object v2, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v2}, Lf/h/a/c/i1/s;->c()Z

    move-result v2

    if-nez v2, :cond_5

    goto/16 :goto_5

    :cond_5
    iget-object v2, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v2}, Lf/h/a/c/i1/s;->f()I

    iget-object v2, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v2}, Lf/h/a/c/i1/s;->c()Z

    move-result v2

    if-nez v2, :cond_6

    goto/16 :goto_5

    :cond_6
    iget-object v2, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v2}, Lf/h/a/c/i1/s;->f()I

    move-result v2

    iget-boolean v6, v4, Lf/h/a/c/a1/e0/l$b;->c:Z

    const/4 v7, 0x1

    if-nez v6, :cond_7

    iput-boolean v3, v4, Lf/h/a/c/a1/e0/l$b;->k:Z

    iget-object v1, v4, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    iput v2, v1, Lf/h/a/c/a1/e0/l$b$a;->e:I

    iput-boolean v7, v1, Lf/h/a/c/a1/e0/l$b$a;->b:Z

    goto/16 :goto_5

    :cond_7
    iget-object v6, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v6}, Lf/h/a/c/i1/s;->c()Z

    move-result v6

    if-nez v6, :cond_8

    goto/16 :goto_5

    :cond_8
    iget-object v6, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v6}, Lf/h/a/c/i1/s;->f()I

    move-result v6

    iget-object v9, v4, Lf/h/a/c/a1/e0/l$b;->e:Landroid/util/SparseArray;

    invoke-virtual {v9, v6}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v9

    if-gez v9, :cond_9

    iput-boolean v3, v4, Lf/h/a/c/a1/e0/l$b;->k:Z

    goto/16 :goto_5

    :cond_9
    iget-object v9, v4, Lf/h/a/c/a1/e0/l$b;->e:Landroid/util/SparseArray;

    invoke-virtual {v9, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/a/c/i1/p$a;

    iget-object v10, v4, Lf/h/a/c/a1/e0/l$b;->d:Landroid/util/SparseArray;

    iget v11, v9, Lf/h/a/c/i1/p$a;->b:I

    invoke-virtual {v10, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lf/h/a/c/i1/p$b;

    iget-boolean v11, v10, Lf/h/a/c/i1/p$b;->h:Z

    if-eqz v11, :cond_b

    iget-object v11, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v11, v8}, Lf/h/a/c/i1/s;->b(I)Z

    move-result v11

    if-nez v11, :cond_a

    goto/16 :goto_5

    :cond_a
    iget-object v11, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v11, v8}, Lf/h/a/c/i1/s;->j(I)V

    :cond_b
    iget-object v8, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    iget v11, v10, Lf/h/a/c/i1/p$b;->j:I

    invoke-virtual {v8, v11}, Lf/h/a/c/i1/s;->b(I)Z

    move-result v8

    if-nez v8, :cond_c

    goto/16 :goto_5

    :cond_c
    iget-object v8, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    iget v11, v10, Lf/h/a/c/i1/p$b;->j:I

    invoke-virtual {v8, v11}, Lf/h/a/c/i1/s;->e(I)I

    move-result v8

    iget-boolean v11, v10, Lf/h/a/c/i1/p$b;->i:Z

    if-nez v11, :cond_f

    iget-object v11, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v11, v7}, Lf/h/a/c/i1/s;->b(I)Z

    move-result v11

    if-nez v11, :cond_d

    goto/16 :goto_5

    :cond_d
    iget-object v11, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v11}, Lf/h/a/c/i1/s;->d()Z

    move-result v11

    if-eqz v11, :cond_10

    iget-object v12, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v12, v7}, Lf/h/a/c/i1/s;->b(I)Z

    move-result v12

    if-nez v12, :cond_e

    goto/16 :goto_5

    :cond_e
    iget-object v12, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v12}, Lf/h/a/c/i1/s;->d()Z

    move-result v12

    const/4 v13, 0x1

    goto :goto_0

    :cond_f
    const/4 v11, 0x0

    :cond_10
    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_0
    iget v14, v4, Lf/h/a/c/a1/e0/l$b;->i:I

    if-ne v14, v5, :cond_11

    const/4 v5, 0x1

    goto :goto_1

    :cond_11
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_13

    iget-object v14, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v14}, Lf/h/a/c/i1/s;->c()Z

    move-result v14

    if-nez v14, :cond_12

    goto/16 :goto_5

    :cond_12
    iget-object v14, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v14}, Lf/h/a/c/i1/s;->f()I

    move-result v14

    goto :goto_2

    :cond_13
    const/4 v14, 0x0

    :goto_2
    iget v15, v10, Lf/h/a/c/i1/p$b;->k:I

    if-nez v15, :cond_17

    iget-object v15, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    iget v3, v10, Lf/h/a/c/i1/p$b;->l:I

    invoke-virtual {v15, v3}, Lf/h/a/c/i1/s;->b(I)Z

    move-result v3

    if-nez v3, :cond_14

    goto/16 :goto_5

    :cond_14
    iget-object v3, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    iget v15, v10, Lf/h/a/c/i1/p$b;->l:I

    invoke-virtual {v3, v15}, Lf/h/a/c/i1/s;->e(I)I

    move-result v3

    iget-boolean v9, v9, Lf/h/a/c/i1/p$a;->c:Z

    if-eqz v9, :cond_16

    if-nez v11, :cond_16

    iget-object v9, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v9}, Lf/h/a/c/i1/s;->c()Z

    move-result v9

    if-nez v9, :cond_15

    goto/16 :goto_5

    :cond_15
    iget-object v9, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v9}, Lf/h/a/c/i1/s;->g()I

    move-result v9

    goto :goto_3

    :cond_16
    const/4 v9, 0x0

    :goto_3
    move v15, v9

    const/4 v7, 0x0

    const/4 v9, 0x0

    goto :goto_4

    :cond_17
    if-ne v15, v7, :cond_1a

    iget-boolean v3, v10, Lf/h/a/c/i1/p$b;->m:Z

    if-nez v3, :cond_1a

    iget-object v3, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->c()Z

    move-result v3

    if-nez v3, :cond_18

    goto :goto_5

    :cond_18
    iget-object v3, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->g()I

    move-result v3

    iget-boolean v9, v9, Lf/h/a/c/i1/p$a;->c:Z

    if-eqz v9, :cond_1b

    if-nez v11, :cond_1b

    iget-object v9, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v9}, Lf/h/a/c/i1/s;->c()Z

    move-result v9

    if-nez v9, :cond_19

    goto :goto_5

    :cond_19
    iget-object v9, v4, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    invoke-virtual {v9}, Lf/h/a/c/i1/s;->g()I

    move-result v9

    move v7, v9

    const/4 v15, 0x0

    move v9, v3

    const/4 v3, 0x0

    goto :goto_4

    :cond_1a
    const/4 v3, 0x0

    :cond_1b
    move v9, v3

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v15, 0x0

    :goto_4
    iget-object v0, v4, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    iput-object v10, v0, Lf/h/a/c/a1/e0/l$b$a;->c:Lf/h/a/c/i1/p$b;

    iput v1, v0, Lf/h/a/c/a1/e0/l$b$a;->d:I

    iput v2, v0, Lf/h/a/c/a1/e0/l$b$a;->e:I

    iput v8, v0, Lf/h/a/c/a1/e0/l$b$a;->f:I

    iput v6, v0, Lf/h/a/c/a1/e0/l$b$a;->g:I

    iput-boolean v11, v0, Lf/h/a/c/a1/e0/l$b$a;->h:Z

    iput-boolean v13, v0, Lf/h/a/c/a1/e0/l$b$a;->i:Z

    iput-boolean v12, v0, Lf/h/a/c/a1/e0/l$b$a;->j:Z

    iput-boolean v5, v0, Lf/h/a/c/a1/e0/l$b$a;->k:Z

    iput v14, v0, Lf/h/a/c/a1/e0/l$b$a;->l:I

    iput v3, v0, Lf/h/a/c/a1/e0/l$b$a;->m:I

    iput v15, v0, Lf/h/a/c/a1/e0/l$b$a;->n:I

    iput v9, v0, Lf/h/a/c/a1/e0/l$b$a;->o:I

    iput v7, v0, Lf/h/a/c/a1/e0/l$b$a;->p:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/l$b$a;->a:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/l$b$a;->b:Z

    const/4 v0, 0x0

    iput-boolean v0, v4, Lf/h/a/c/a1/e0/l$b;->k:Z

    :goto_5
    return-void
.end method

.method public b(Lf/h/a/c/i1/r;)V
    .locals 32

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v3, v1, Lf/h/a/c/i1/r;->c:I

    iget-object v4, v1, Lf/h/a/c/i1/r;->a:[B

    iget-wide v5, v0, Lf/h/a/c/a1/e0/l;->g:J

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v5, v7

    iput-wide v5, v0, Lf/h/a/c/a1/e0/l;->g:J

    iget-object v5, v0, Lf/h/a/c/a1/e0/l;->j:Lf/h/a/c/a1/s;

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v6

    invoke-interface {v5, v1, v6}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    :goto_0
    iget-object v1, v0, Lf/h/a/c/a1/e0/l;->h:[Z

    invoke-static {v4, v2, v3, v1}, Lf/h/a/c/i1/p;->b([BII[Z)I

    move-result v1

    if-ne v1, v3, :cond_0

    invoke-virtual {v0, v4, v2, v3}, Lf/h/a/c/a1/e0/l;->a([BII)V

    return-void

    :cond_0
    add-int/lit8 v5, v1, 0x3

    aget-byte v6, v4, v5

    and-int/lit8 v6, v6, 0x1f

    sub-int v7, v1, v2

    if-lez v7, :cond_1

    invoke-virtual {v0, v4, v2, v1}, Lf/h/a/c/a1/e0/l;->a([BII)V

    :cond_1
    sub-int v1, v3, v1

    iget-wide v8, v0, Lf/h/a/c/a1/e0/l;->g:J

    int-to-long v10, v1

    sub-long/2addr v8, v10

    if-gez v7, :cond_2

    neg-int v7, v7

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_1
    iget-wide v10, v0, Lf/h/a/c/a1/e0/l;->m:J

    iget-boolean v12, v0, Lf/h/a/c/a1/e0/l;->l:Z

    if-eqz v12, :cond_4

    iget-object v12, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-boolean v12, v12, Lf/h/a/c/a1/e0/l$b;->c:Z

    if-eqz v12, :cond_3

    goto :goto_2

    :cond_3
    move/from16 v28, v3

    move-object/from16 v29, v4

    move/from16 v30, v5

    move/from16 v31, v6

    goto/16 :goto_3

    :cond_4
    :goto_2
    iget-object v12, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v12, v7}, Lf/h/a/c/a1/e0/q;->b(I)Z

    iget-object v12, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v12, v7}, Lf/h/a/c/a1/e0/q;->b(I)Z

    iget-boolean v12, v0, Lf/h/a/c/a1/e0/l;->l:Z

    const/4 v15, 0x3

    if-nez v12, :cond_5

    iget-object v12, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    iget-boolean v12, v12, Lf/h/a/c/a1/e0/q;->c:Z

    if-eqz v12, :cond_3

    iget-object v12, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    iget-boolean v12, v12, Lf/h/a/c/a1/e0/q;->c:Z

    if-eqz v12, :cond_3

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iget-object v13, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    iget-object v14, v13, Lf/h/a/c/a1/e0/q;->d:[B

    iget v13, v13, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v14, v13}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v13, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    iget-object v14, v13, Lf/h/a/c/a1/e0/q;->d:[B

    iget v13, v13, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v14, v13}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v13, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    iget-object v14, v13, Lf/h/a/c/a1/e0/q;->d:[B

    iget v13, v13, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v14, v15, v13}, Lf/h/a/c/i1/p;->d([BII)Lf/h/a/c/i1/p$b;

    move-result-object v13

    iget-object v14, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    iget-object v2, v14, Lf/h/a/c/a1/e0/q;->d:[B

    iget v14, v14, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v2, v15, v14}, Lf/h/a/c/i1/p;->c([BII)Lf/h/a/c/i1/p$a;

    move-result-object v2

    iget-object v14, v0, Lf/h/a/c/a1/e0/l;->j:Lf/h/a/c/a1/s;

    iget-object v15, v0, Lf/h/a/c/a1/e0/l;->i:Ljava/lang/String;

    move/from16 v28, v3

    iget v3, v13, Lf/h/a/c/i1/p$b;->a:I

    move-object/from16 v29, v4

    iget v4, v13, Lf/h/a/c/i1/p$b;->b:I

    move/from16 v30, v5

    iget v5, v13, Lf/h/a/c/i1/p$b;->c:I

    move/from16 v31, v6

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v16, 0x0

    aput-object v3, v6, v16

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v6, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v6, v4

    const-string v3, "avc1.%02X%02X%02X"

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    const/16 v19, -0x1

    const/16 v20, -0x1

    iget v3, v13, Lf/h/a/c/i1/p$b;->e:I

    iget v4, v13, Lf/h/a/c/i1/p$b;->f:I

    const/high16 v23, -0x40800000    # -1.0f

    const/16 v25, -0x1

    iget v5, v13, Lf/h/a/c/i1/p$b;->g:F

    const/16 v27, 0x0

    const-string v17, "video/avc"

    move-object/from16 v16, v15

    move/from16 v21, v3

    move/from16 v22, v4

    move-object/from16 v24, v12

    move/from16 v26, v5

    invoke-static/range {v16 .. v27}, Lcom/google/android/exoplayer2/Format;->m(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIFLjava/util/List;IFLcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    invoke-interface {v14, v3}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    const/4 v3, 0x1

    iput-boolean v3, v0, Lf/h/a/c/a1/e0/l;->l:Z

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-object v3, v3, Lf/h/a/c/a1/e0/l$b;->d:Landroid/util/SparseArray;

    iget v4, v13, Lf/h/a/c/i1/p$b;->d:I

    invoke-virtual {v3, v4, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-object v3, v3, Lf/h/a/c/a1/e0/l$b;->e:Landroid/util/SparseArray;

    iget v4, v2, Lf/h/a/c/i1/p$a;->a:I

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v2}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v2}, Lf/h/a/c/a1/e0/q;->c()V

    goto :goto_3

    :cond_5
    move/from16 v28, v3

    move-object/from16 v29, v4

    move/from16 v30, v5

    move/from16 v31, v6

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    iget-boolean v3, v2, Lf/h/a/c/a1/e0/q;->c:Z

    if-eqz v3, :cond_6

    iget-object v3, v2, Lf/h/a/c/a1/e0/q;->d:[B

    iget v2, v2, Lf/h/a/c/a1/e0/q;->e:I

    const/4 v4, 0x3

    invoke-static {v3, v4, v2}, Lf/h/a/c/i1/p;->d([BII)Lf/h/a/c/i1/p$b;

    move-result-object v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-object v3, v3, Lf/h/a/c/a1/e0/l$b;->d:Landroid/util/SparseArray;

    iget v4, v2, Lf/h/a/c/i1/p$b;->d:I

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v2}, Lf/h/a/c/a1/e0/q;->c()V

    goto :goto_3

    :cond_6
    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    iget-boolean v3, v2, Lf/h/a/c/a1/e0/q;->c:Z

    if-eqz v3, :cond_7

    iget-object v3, v2, Lf/h/a/c/a1/e0/q;->d:[B

    iget v2, v2, Lf/h/a/c/a1/e0/q;->e:I

    const/4 v4, 0x3

    invoke-static {v3, v4, v2}, Lf/h/a/c/i1/p;->c([BII)Lf/h/a/c/i1/p$a;

    move-result-object v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-object v3, v3, Lf/h/a/c/a1/e0/l$b;->e:Landroid/util/SparseArray;

    iget v4, v2, Lf/h/a/c/i1/p$a;->a:I

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v2}, Lf/h/a/c/a1/e0/q;->c()V

    :cond_7
    :goto_3
    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->f:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v2, v7}, Lf/h/a/c/a1/e0/q;->b(I)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->f:Lf/h/a/c/a1/e0/q;

    iget-object v3, v2, Lf/h/a/c/a1/e0/q;->d:[B

    iget v2, v2, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v3, v2}, Lf/h/a/c/i1/p;->e([BI)I

    move-result v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->o:Lf/h/a/c/i1/r;

    iget-object v4, v0, Lf/h/a/c/a1/e0/l;->f:Lf/h/a/c/a1/e0/q;

    iget-object v4, v4, Lf/h/a/c/a1/e0/q;->d:[B

    invoke-virtual {v3, v4, v2}, Lf/h/a/c/i1/r;->A([BI)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->o:Lf/h/a/c/i1/r;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->a:Lf/h/a/c/a1/e0/x;

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->o:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/a1/e0/x;->b:[Lf/h/a/c/a1/s;

    invoke-static {v10, v11, v3, v2}, Lf/g/j/k/a;->z(JLf/h/a/c/i1/r;[Lf/h/a/c/a1/s;)V

    :cond_8
    iget-object v2, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-boolean v3, v0, Lf/h/a/c/a1/e0/l;->l:Z

    iget-boolean v4, v0, Lf/h/a/c/a1/e0/l;->n:Z

    iget v5, v2, Lf/h/a/c/a1/e0/l$b;->i:I

    const/16 v6, 0x9

    if-eq v5, v6, :cond_f

    iget-boolean v5, v2, Lf/h/a/c/a1/e0/l$b;->c:Z

    if-eqz v5, :cond_11

    iget-object v5, v2, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    iget-object v6, v2, Lf/h/a/c/a1/e0/l$b;->m:Lf/h/a/c/a1/e0/l$b$a;

    iget-boolean v7, v5, Lf/h/a/c/a1/e0/l$b$a;->a:Z

    if-eqz v7, :cond_e

    iget-boolean v7, v6, Lf/h/a/c/a1/e0/l$b$a;->a:Z

    if-eqz v7, :cond_d

    iget v7, v5, Lf/h/a/c/a1/e0/l$b$a;->f:I

    iget v10, v6, Lf/h/a/c/a1/e0/l$b$a;->f:I

    if-ne v7, v10, :cond_d

    iget v7, v5, Lf/h/a/c/a1/e0/l$b$a;->g:I

    iget v10, v6, Lf/h/a/c/a1/e0/l$b$a;->g:I

    if-ne v7, v10, :cond_d

    iget-boolean v7, v5, Lf/h/a/c/a1/e0/l$b$a;->h:Z

    iget-boolean v10, v6, Lf/h/a/c/a1/e0/l$b$a;->h:Z

    if-ne v7, v10, :cond_d

    iget-boolean v7, v5, Lf/h/a/c/a1/e0/l$b$a;->i:Z

    if-eqz v7, :cond_9

    iget-boolean v7, v6, Lf/h/a/c/a1/e0/l$b$a;->i:Z

    if-eqz v7, :cond_9

    iget-boolean v7, v5, Lf/h/a/c/a1/e0/l$b$a;->j:Z

    iget-boolean v10, v6, Lf/h/a/c/a1/e0/l$b$a;->j:Z

    if-ne v7, v10, :cond_d

    :cond_9
    iget v7, v5, Lf/h/a/c/a1/e0/l$b$a;->d:I

    iget v10, v6, Lf/h/a/c/a1/e0/l$b$a;->d:I

    if-eq v7, v10, :cond_a

    if-eqz v7, :cond_d

    if-eqz v10, :cond_d

    :cond_a
    iget-object v7, v5, Lf/h/a/c/a1/e0/l$b$a;->c:Lf/h/a/c/i1/p$b;

    iget v7, v7, Lf/h/a/c/i1/p$b;->k:I

    if-nez v7, :cond_b

    iget-object v10, v6, Lf/h/a/c/a1/e0/l$b$a;->c:Lf/h/a/c/i1/p$b;

    iget v10, v10, Lf/h/a/c/i1/p$b;->k:I

    if-nez v10, :cond_b

    iget v10, v5, Lf/h/a/c/a1/e0/l$b$a;->m:I

    iget v11, v6, Lf/h/a/c/a1/e0/l$b$a;->m:I

    if-ne v10, v11, :cond_d

    iget v10, v5, Lf/h/a/c/a1/e0/l$b$a;->n:I

    iget v11, v6, Lf/h/a/c/a1/e0/l$b$a;->n:I

    if-ne v10, v11, :cond_d

    :cond_b
    const/4 v10, 0x1

    if-ne v7, v10, :cond_c

    iget-object v7, v6, Lf/h/a/c/a1/e0/l$b$a;->c:Lf/h/a/c/i1/p$b;

    iget v7, v7, Lf/h/a/c/i1/p$b;->k:I

    if-ne v7, v10, :cond_c

    iget v7, v5, Lf/h/a/c/a1/e0/l$b$a;->o:I

    iget v10, v6, Lf/h/a/c/a1/e0/l$b$a;->o:I

    if-ne v7, v10, :cond_d

    iget v7, v5, Lf/h/a/c/a1/e0/l$b$a;->p:I

    iget v10, v6, Lf/h/a/c/a1/e0/l$b$a;->p:I

    if-ne v7, v10, :cond_d

    :cond_c
    iget-boolean v7, v5, Lf/h/a/c/a1/e0/l$b$a;->k:Z

    iget-boolean v10, v6, Lf/h/a/c/a1/e0/l$b$a;->k:Z

    if-ne v7, v10, :cond_d

    if-eqz v7, :cond_e

    if-eqz v10, :cond_e

    iget v5, v5, Lf/h/a/c/a1/e0/l$b$a;->l:I

    iget v6, v6, Lf/h/a/c/a1/e0/l$b$a;->l:I

    if-eq v5, v6, :cond_e

    :cond_d
    const/4 v5, 0x1

    goto :goto_4

    :cond_e
    const/4 v5, 0x0

    :goto_4
    if-eqz v5, :cond_11

    :cond_f
    if-eqz v3, :cond_10

    iget-boolean v3, v2, Lf/h/a/c/a1/e0/l$b;->o:Z

    if-eqz v3, :cond_10

    iget-wide v5, v2, Lf/h/a/c/a1/e0/l$b;->j:J

    sub-long v10, v8, v5

    long-to-int v3, v10

    add-int v15, v1, v3

    iget-boolean v13, v2, Lf/h/a/c/a1/e0/l$b;->r:Z

    iget-wide v10, v2, Lf/h/a/c/a1/e0/l$b;->p:J

    sub-long/2addr v5, v10

    long-to-int v14, v5

    iget-object v10, v2, Lf/h/a/c/a1/e0/l$b;->a:Lf/h/a/c/a1/s;

    iget-wide v11, v2, Lf/h/a/c/a1/e0/l$b;->q:J

    const/16 v16, 0x0

    invoke-interface/range {v10 .. v16}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    :cond_10
    iget-wide v5, v2, Lf/h/a/c/a1/e0/l$b;->j:J

    iput-wide v5, v2, Lf/h/a/c/a1/e0/l$b;->p:J

    iget-wide v5, v2, Lf/h/a/c/a1/e0/l$b;->l:J

    iput-wide v5, v2, Lf/h/a/c/a1/e0/l$b;->q:J

    const/4 v1, 0x0

    iput-boolean v1, v2, Lf/h/a/c/a1/e0/l$b;->r:Z

    const/4 v1, 0x1

    iput-boolean v1, v2, Lf/h/a/c/a1/e0/l$b;->o:Z

    :cond_11
    iget-boolean v1, v2, Lf/h/a/c/a1/e0/l$b;->b:Z

    if-eqz v1, :cond_14

    iget-object v1, v2, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    iget-boolean v3, v1, Lf/h/a/c/a1/e0/l$b$a;->b:Z

    if-eqz v3, :cond_13

    iget v1, v1, Lf/h/a/c/a1/e0/l$b$a;->e:I

    const/4 v3, 0x7

    if-eq v1, v3, :cond_12

    const/4 v3, 0x2

    if-ne v1, v3, :cond_13

    :cond_12
    const/4 v4, 0x1

    goto :goto_5

    :cond_13
    const/4 v4, 0x0

    :cond_14
    :goto_5
    iget-boolean v1, v2, Lf/h/a/c/a1/e0/l$b;->r:Z

    iget v3, v2, Lf/h/a/c/a1/e0/l$b;->i:I

    const/4 v5, 0x5

    if-eq v3, v5, :cond_16

    if-eqz v4, :cond_15

    const/4 v4, 0x1

    if-ne v3, v4, :cond_15

    goto :goto_6

    :cond_15
    const/4 v3, 0x0

    goto :goto_7

    :cond_16
    :goto_6
    const/4 v3, 0x1

    :goto_7
    or-int/2addr v1, v3

    iput-boolean v1, v2, Lf/h/a/c/a1/e0/l$b;->r:Z

    if-eqz v1, :cond_17

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/l;->n:Z

    :cond_17
    iget-wide v1, v0, Lf/h/a/c/a1/e0/l;->m:J

    iget-boolean v3, v0, Lf/h/a/c/a1/e0/l;->l:Z

    if-eqz v3, :cond_19

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-boolean v3, v3, Lf/h/a/c/a1/e0/l$b;->c:Z

    if-eqz v3, :cond_18

    goto :goto_8

    :cond_18
    move/from16 v4, v31

    goto :goto_9

    :cond_19
    :goto_8
    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    move/from16 v4, v31

    invoke-virtual {v3, v4}, Lf/h/a/c/a1/e0/q;->d(I)V

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v3, v4}, Lf/h/a/c/a1/e0/q;->d(I)V

    :goto_9
    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->f:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v3, v4}, Lf/h/a/c/a1/e0/q;->d(I)V

    iget-object v3, v0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iput v4, v3, Lf/h/a/c/a1/e0/l$b;->i:I

    iput-wide v1, v3, Lf/h/a/c/a1/e0/l$b;->l:J

    iput-wide v8, v3, Lf/h/a/c/a1/e0/l$b;->j:J

    iget-boolean v1, v3, Lf/h/a/c/a1/e0/l$b;->b:Z

    if-eqz v1, :cond_1a

    const/4 v1, 0x1

    if-eq v4, v1, :cond_1b

    goto :goto_a

    :cond_1a
    const/4 v1, 0x1

    :goto_a
    iget-boolean v2, v3, Lf/h/a/c/a1/e0/l$b;->c:Z

    if-eqz v2, :cond_1c

    if-eq v4, v5, :cond_1b

    if-eq v4, v1, :cond_1b

    const/4 v1, 0x2

    if-ne v4, v1, :cond_1c

    :cond_1b
    iget-object v1, v3, Lf/h/a/c/a1/e0/l$b;->m:Lf/h/a/c/a1/e0/l$b$a;

    iget-object v2, v3, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    iput-object v2, v3, Lf/h/a/c/a1/e0/l$b;->m:Lf/h/a/c/a1/e0/l$b$a;

    iput-object v1, v3, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lf/h/a/c/a1/e0/l$b$a;->b:Z

    iput-boolean v2, v1, Lf/h/a/c/a1/e0/l$b$a;->a:Z

    iput v2, v3, Lf/h/a/c/a1/e0/l$b;->h:I

    const/4 v1, 0x1

    iput-boolean v1, v3, Lf/h/a/c/a1/e0/l$b;->k:Z

    :cond_1c
    move/from16 v3, v28

    move-object/from16 v4, v29

    move/from16 v2, v30

    goto/16 :goto_0
.end method

.method public c()V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/a1/e0/l;->h:[Z

    invoke-static {v0}, Lf/h/a/c/i1/p;->a([Z)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/l;->d:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/l;->e:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/l;->f:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/l$b;->k:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/l$b;->o:Z

    iget-object v0, v0, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/l$b$a;->b:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/l$b$a;->a:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lf/h/a/c/a1/e0/l;->g:J

    iput-boolean v1, p0, Lf/h/a/c/a1/e0/l;->n:Z

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 4

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/l;->i:Ljava/lang/String;

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result v0

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/l;->j:Lf/h/a/c/a1/s;

    new-instance v1, Lf/h/a/c/a1/e0/l$b;

    iget-boolean v2, p0, Lf/h/a/c/a1/e0/l;->b:Z

    iget-boolean v3, p0, Lf/h/a/c/a1/e0/l;->c:Z

    invoke-direct {v1, v0, v2, v3}, Lf/h/a/c/a1/e0/l$b;-><init>(Lf/h/a/c/a1/s;ZZ)V

    iput-object v1, p0, Lf/h/a/c/a1/e0/l;->k:Lf/h/a/c/a1/e0/l$b;

    iget-object v0, p0, Lf/h/a/c/a1/e0/l;->a:Lf/h/a/c/a1/e0/x;

    invoke-virtual {v0, p1, p2}, Lf/h/a/c/a1/e0/x;->a(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    return-void
.end method

.method public f(JI)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/a1/e0/l;->m:J

    iget-boolean p1, p0, Lf/h/a/c/a1/e0/l;->n:Z

    and-int/lit8 p2, p3, 0x2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    or-int/2addr p1, p2

    iput-boolean p1, p0, Lf/h/a/c/a1/e0/l;->n:Z

    return-void
.end method
