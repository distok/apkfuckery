.class public final Lf/h/a/c/a1/e0/u;
.super Ljava/lang/Object;
.source "PsExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/e0/u$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/i1/z;

.field public final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/a1/e0/u$a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/c/i1/r;

.field public final d:Lf/h/a/c/a1/e0/t;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:J

.field public i:Lf/h/a/c/a1/e0/s;

.field public j:Lf/h/a/c/a1/i;

.field public k:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    new-instance v0, Lf/h/a/c/i1/z;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/a/c/i1/z;-><init>(J)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/e0/u;->a:Lf/h/a/c/i1/z;

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/e0/u;->b:Landroid/util/SparseArray;

    new-instance v0, Lf/h/a/c/a1/e0/t;

    invoke-direct {v0}, Lf/h/a/c/a1/e0/t;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/e0/u;->d:Lf/h/a/c/a1/e0/t;

    return-void
.end method


# virtual methods
.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    iget-wide v9, v1, Lf/h/a/c/a1/e;->c:J

    const/4 v11, 0x0

    const-wide/16 v12, -0x1

    const/4 v14, 0x1

    cmp-long v15, v9, v12

    if-eqz v15, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const/16 v7, 0x1ba

    if-eqz v3, :cond_b

    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->d:Lf/h/a/c/a1/e0/t;

    iget-boolean v6, v3, Lf/h/a/c/a1/e0/t;->c:Z

    if-nez v6, :cond_b

    iget-boolean v6, v3, Lf/h/a/c/a1/e0/t;->e:Z

    const-wide/16 v12, 0x4e20

    if-nez v6, :cond_4

    invoke-static {v12, v13, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    long-to-int v6, v12

    int-to-long v12, v6

    sub-long/2addr v9, v12

    iget-wide v12, v1, Lf/h/a/c/a1/e;->d:J

    cmp-long v8, v12, v9

    if-eqz v8, :cond_1

    iput-wide v9, v2, Lf/h/a/c/a1/p;->a:J

    goto :goto_3

    :cond_1
    iget-object v2, v3, Lf/h/a/c/a1/e0/t;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v6}, Lf/h/a/c/i1/r;->y(I)V

    iput v11, v1, Lf/h/a/c/a1/e;->f:I

    iget-object v2, v3, Lf/h/a/c/a1/e0/t;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v2, v11, v6, v11}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v1, v3, Lf/h/a/c/a1/e0/t;->b:Lf/h/a/c/i1/r;

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v6, v1, Lf/h/a/c/i1/r;->c:I

    add-int/lit8 v6, v6, -0x4

    :goto_1
    if-lt v6, v2, :cond_3

    iget-object v8, v1, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v3, v8, v6}, Lf/h/a/c/a1/e0/t;->b([BI)I

    move-result v8

    if-ne v8, v7, :cond_2

    add-int/lit8 v8, v6, 0x4

    invoke-virtual {v1, v8}, Lf/h/a/c/i1/r;->C(I)V

    invoke-static {v1}, Lf/h/a/c/a1/e0/t;->c(Lf/h/a/c/i1/r;)J

    move-result-wide v8

    cmp-long v10, v8, v4

    if-eqz v10, :cond_2

    move-wide v4, v8

    goto :goto_2

    :cond_2
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    :cond_3
    :goto_2
    iput-wide v4, v3, Lf/h/a/c/a1/e0/t;->g:J

    iput-boolean v14, v3, Lf/h/a/c/a1/e0/t;->e:Z

    goto/16 :goto_6

    :cond_4
    iget-wide v14, v3, Lf/h/a/c/a1/e0/t;->g:J

    cmp-long v6, v14, v4

    if-nez v6, :cond_5

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/t;->a(Lf/h/a/c/a1/e;)I

    goto :goto_6

    :cond_5
    iget-boolean v6, v3, Lf/h/a/c/a1/e0/t;->d:Z

    if-nez v6, :cond_9

    invoke-static {v12, v13, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    long-to-int v6, v8

    iget-wide v8, v1, Lf/h/a/c/a1/e;->d:J

    int-to-long v12, v11

    cmp-long v10, v8, v12

    if-eqz v10, :cond_6

    iput-wide v12, v2, Lf/h/a/c/a1/p;->a:J

    :goto_3
    const/4 v11, 0x1

    goto :goto_6

    :cond_6
    iget-object v2, v3, Lf/h/a/c/a1/e0/t;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v6}, Lf/h/a/c/i1/r;->y(I)V

    iput v11, v1, Lf/h/a/c/a1/e;->f:I

    iget-object v2, v3, Lf/h/a/c/a1/e0/t;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v2, v11, v6, v11}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v1, v3, Lf/h/a/c/a1/e0/t;->b:Lf/h/a/c/i1/r;

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v6, v1, Lf/h/a/c/i1/r;->c:I

    :goto_4
    add-int/lit8 v8, v6, -0x3

    if-ge v2, v8, :cond_8

    iget-object v8, v1, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v3, v8, v2}, Lf/h/a/c/a1/e0/t;->b([BI)I

    move-result v8

    if-ne v8, v7, :cond_7

    add-int/lit8 v8, v2, 0x4

    invoke-virtual {v1, v8}, Lf/h/a/c/i1/r;->C(I)V

    invoke-static {v1}, Lf/h/a/c/a1/e0/t;->c(Lf/h/a/c/i1/r;)J

    move-result-wide v8

    cmp-long v10, v8, v4

    if-eqz v10, :cond_7

    move-wide v4, v8

    goto :goto_5

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_8
    :goto_5
    iput-wide v4, v3, Lf/h/a/c/a1/e0/t;->f:J

    const/4 v1, 0x1

    iput-boolean v1, v3, Lf/h/a/c/a1/e0/t;->d:Z

    goto :goto_6

    :cond_9
    iget-wide v6, v3, Lf/h/a/c/a1/e0/t;->f:J

    cmp-long v2, v6, v4

    if-nez v2, :cond_a

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/t;->a(Lf/h/a/c/a1/e;)I

    goto :goto_6

    :cond_a
    iget-object v2, v3, Lf/h/a/c/a1/e0/t;->a:Lf/h/a/c/i1/z;

    invoke-virtual {v2, v6, v7}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v4

    iget-object v2, v3, Lf/h/a/c/a1/e0/t;->a:Lf/h/a/c/i1/z;

    iget-wide v6, v3, Lf/h/a/c/a1/e0/t;->g:J

    invoke-virtual {v2, v6, v7}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v6

    sub-long/2addr v6, v4

    iput-wide v6, v3, Lf/h/a/c/a1/e0/t;->h:J

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/t;->a(Lf/h/a/c/a1/e;)I

    :goto_6
    return v11

    :cond_b
    iget-boolean v3, v0, Lf/h/a/c/a1/e0/u;->k:Z

    const-wide/16 v12, 0x0

    if-nez v3, :cond_d

    const/4 v3, 0x1

    iput-boolean v3, v0, Lf/h/a/c/a1/e0/u;->k:Z

    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->d:Lf/h/a/c/a1/e0/t;

    iget-wide v7, v3, Lf/h/a/c/a1/e0/t;->h:J

    cmp-long v6, v7, v4

    if-eqz v6, :cond_c

    new-instance v5, Lf/h/a/c/a1/e0/s;

    iget-object v4, v3, Lf/h/a/c/a1/e0/t;->a:Lf/h/a/c/i1/z;

    move-object v3, v5

    move-object v14, v5

    move-wide v5, v7

    move-wide v7, v9

    invoke-direct/range {v3 .. v8}, Lf/h/a/c/a1/e0/s;-><init>(Lf/h/a/c/i1/z;JJ)V

    iput-object v14, v0, Lf/h/a/c/a1/e0/u;->i:Lf/h/a/c/a1/e0/s;

    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->j:Lf/h/a/c/a1/i;

    iget-object v4, v14, Lf/h/a/c/a1/a;->a:Lf/h/a/c/a1/a$a;

    invoke-interface {v3, v4}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    goto :goto_7

    :cond_c
    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->j:Lf/h/a/c/a1/i;

    new-instance v4, Lf/h/a/c/a1/q$b;

    invoke-direct {v4, v7, v8, v12, v13}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {v3, v4}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    :cond_d
    :goto_7
    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->i:Lf/h/a/c/a1/e0/s;

    if-eqz v3, :cond_e

    invoke-virtual {v3}, Lf/h/a/c/a1/a;->b()Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->i:Lf/h/a/c/a1/e0/s;

    invoke-virtual {v3, v1, v2}, Lf/h/a/c/a1/a;->a(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I

    move-result v1

    return v1

    :cond_e
    iput v11, v1, Lf/h/a/c/a1/e;->f:I

    if-eqz v15, :cond_f

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/a1/e;->d()J

    move-result-wide v2

    sub-long/2addr v9, v2

    goto :goto_8

    :cond_f
    const-wide/16 v9, -0x1

    :goto_8
    const/4 v2, -0x1

    const-wide/16 v3, -0x1

    cmp-long v5, v9, v3

    if-eqz v5, :cond_10

    const-wide/16 v3, 0x4

    cmp-long v5, v9, v3

    if-gez v5, :cond_10

    return v2

    :cond_10
    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    const/4 v4, 0x4

    const/4 v5, 0x1

    invoke-virtual {v1, v3, v11, v4, v5}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    move-result v3

    if-nez v3, :cond_11

    return v2

    :cond_11
    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v3, v11}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    const/16 v5, 0x1b9

    if-ne v3, v5, :cond_12

    return v2

    :cond_12
    const/16 v2, 0x1ba

    if-ne v3, v2, :cond_13

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    const/16 v3, 0xa

    invoke-virtual {v1, v2, v11, v3, v11}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    and-int/lit8 v2, v2, 0x7

    add-int/lit8 v2, v2, 0xe

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e;->i(I)V

    return v11

    :cond_13
    const/16 v2, 0x1bb

    const/4 v5, 0x2

    const/4 v6, 0x6

    if-ne v3, v2, :cond_14

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v2, v11, v5, v11}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->v()I

    move-result v2

    add-int/2addr v2, v6

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e;->i(I)V

    return v11

    :cond_14
    and-int/lit16 v2, v3, -0x100

    const/16 v7, 0x8

    shr-int/2addr v2, v7

    const/4 v8, 0x1

    if-eq v2, v8, :cond_15

    invoke-virtual {v1, v8}, Lf/h/a/c/a1/e;->i(I)V

    return v11

    :cond_15
    and-int/lit16 v2, v3, 0xff

    iget-object v3, v0, Lf/h/a/c/a1/e0/u;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/a1/e0/u$a;

    iget-boolean v8, v0, Lf/h/a/c/a1/e0/u;->e:Z

    if-nez v8, :cond_1b

    if-nez v3, :cond_19

    const/16 v8, 0xbd

    const/4 v9, 0x0

    if-ne v2, v8, :cond_16

    new-instance v8, Lf/h/a/c/a1/e0/b;

    invoke-direct {v8, v9}, Lf/h/a/c/a1/e0/b;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x1

    iput-boolean v10, v0, Lf/h/a/c/a1/e0/u;->f:Z

    iget-wide v14, v1, Lf/h/a/c/a1/e;->d:J

    iput-wide v14, v0, Lf/h/a/c/a1/e0/u;->h:J

    :goto_9
    move-object v9, v8

    goto :goto_a

    :cond_16
    const/4 v10, 0x1

    and-int/lit16 v8, v2, 0xe0

    const/16 v14, 0xc0

    if-ne v8, v14, :cond_17

    new-instance v8, Lf/h/a/c/a1/e0/p;

    invoke-direct {v8, v9}, Lf/h/a/c/a1/e0/p;-><init>(Ljava/lang/String;)V

    iput-boolean v10, v0, Lf/h/a/c/a1/e0/u;->f:Z

    iget-wide v14, v1, Lf/h/a/c/a1/e;->d:J

    iput-wide v14, v0, Lf/h/a/c/a1/e0/u;->h:J

    goto :goto_9

    :cond_17
    and-int/lit16 v8, v2, 0xf0

    const/16 v14, 0xe0

    if-ne v8, v14, :cond_18

    new-instance v8, Lf/h/a/c/a1/e0/k;

    invoke-direct {v8, v9}, Lf/h/a/c/a1/e0/k;-><init>(Lf/h/a/c/a1/e0/d0;)V

    iput-boolean v10, v0, Lf/h/a/c/a1/e0/u;->g:Z

    iget-wide v9, v1, Lf/h/a/c/a1/e;->d:J

    iput-wide v9, v0, Lf/h/a/c/a1/e0/u;->h:J

    goto :goto_9

    :cond_18
    :goto_a
    if-eqz v9, :cond_19

    new-instance v3, Lf/h/a/c/a1/e0/c0$d;

    const/16 v8, 0x100

    const/high16 v10, -0x80000000

    invoke-direct {v3, v10, v2, v8}, Lf/h/a/c/a1/e0/c0$d;-><init>(III)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/u;->j:Lf/h/a/c/a1/i;

    invoke-interface {v9, v8, v3}, Lf/h/a/c/a1/e0/j;->e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    new-instance v3, Lf/h/a/c/a1/e0/u$a;

    iget-object v8, v0, Lf/h/a/c/a1/e0/u;->a:Lf/h/a/c/i1/z;

    invoke-direct {v3, v9, v8}, Lf/h/a/c/a1/e0/u$a;-><init>(Lf/h/a/c/a1/e0/j;Lf/h/a/c/i1/z;)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/u;->b:Landroid/util/SparseArray;

    invoke-virtual {v8, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_19
    iget-boolean v2, v0, Lf/h/a/c/a1/e0/u;->f:Z

    if-eqz v2, :cond_1a

    iget-boolean v2, v0, Lf/h/a/c/a1/e0/u;->g:Z

    if-eqz v2, :cond_1a

    iget-wide v8, v0, Lf/h/a/c/a1/e0/u;->h:J

    const-wide/16 v14, 0x2000

    add-long/2addr v8, v14

    goto :goto_b

    :cond_1a
    const-wide/32 v8, 0x100000

    :goto_b
    iget-wide v14, v1, Lf/h/a/c/a1/e;->d:J

    cmp-long v2, v14, v8

    if-lez v2, :cond_1b

    const/4 v2, 0x1

    iput-boolean v2, v0, Lf/h/a/c/a1/e0/u;->e:Z

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->j:Lf/h/a/c/a1/i;

    invoke-interface {v2}, Lf/h/a/c/a1/i;->k()V

    :cond_1b
    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v2, v11, v5, v11}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->v()I

    move-result v2

    add-int/2addr v2, v6

    if-nez v3, :cond_1c

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e;->i(I)V

    goto/16 :goto_c

    :cond_1c
    iget-object v5, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v2}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v5, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v5, v11, v2, v11}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object v1, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v1, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    iget-object v2, v2, Lf/h/a/c/i1/q;->a:[B

    const/4 v5, 0x3

    invoke-virtual {v1, v2, v11, v5}, Lf/h/a/c/i1/r;->d([BII)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v7}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v2

    iput-boolean v2, v3, Lf/h/a/c/a1/e0/u$a;->d:Z

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->e()Z

    move-result v2

    iput-boolean v2, v3, Lf/h/a/c/a1/e0/u$a;->e:Z

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v6}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iput v2, v3, Lf/h/a/c/a1/e0/u$a;->g:I

    iget-object v6, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    iget-object v6, v6, Lf/h/a/c/i1/q;->a:[B

    invoke-virtual {v1, v6, v11, v2}, Lf/h/a/c/i1/r;->d([BII)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v11}, Lf/h/a/c/i1/q;->j(I)V

    iput-wide v12, v3, Lf/h/a/c/a1/e0/u$a;->h:J

    iget-boolean v2, v3, Lf/h/a/c/a1/e0/u$a;->d:Z

    if-eqz v2, :cond_1e

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    int-to-long v6, v2

    const/16 v2, 0x1e

    shl-long/2addr v6, v2

    iget-object v8, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v8, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    const/16 v10, 0xf

    invoke-virtual {v8, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v8

    shl-int/2addr v8, v10

    int-to-long v12, v8

    or-long/2addr v6, v12

    iget-object v8, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v8, v9}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v8, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v8, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v8

    int-to-long v12, v8

    or-long/2addr v6, v12

    iget-object v8, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v8, v9}, Lf/h/a/c/i1/q;->l(I)V

    iget-boolean v8, v3, Lf/h/a/c/a1/e0/u$a;->f:Z

    if-nez v8, :cond_1d

    iget-boolean v8, v3, Lf/h/a/c/a1/e0/u$a;->e:Z

    if-eqz v8, :cond_1d

    iget-object v8, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v8, v4}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v8, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v8, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    int-to-long v8, v5

    shl-long/2addr v8, v2

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    shl-int/2addr v2, v10

    int-to-long v12, v2

    or-long/2addr v8, v12

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    int-to-long v12, v2

    or-long/2addr v8, v12

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->c:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->b:Lf/h/a/c/i1/z;

    invoke-virtual {v2, v8, v9}, Lf/h/a/c/i1/z;->b(J)J

    iput-boolean v5, v3, Lf/h/a/c/a1/e0/u$a;->f:Z

    :cond_1d
    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->b:Lf/h/a/c/i1/z;

    invoke-virtual {v2, v6, v7}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v5

    iput-wide v5, v3, Lf/h/a/c/a1/e0/u$a;->h:J

    :cond_1e
    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->a:Lf/h/a/c/a1/e0/j;

    iget-wide v5, v3, Lf/h/a/c/a1/e0/u$a;->h:J

    invoke-interface {v2, v5, v6, v4}, Lf/h/a/c/a1/e0/j;->f(JI)V

    iget-object v2, v3, Lf/h/a/c/a1/e0/u$a;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {v2, v1}, Lf/h/a/c/a1/e0/j;->b(Lf/h/a/c/i1/r;)V

    iget-object v1, v3, Lf/h/a/c/a1/e0/u$a;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {v1}, Lf/h/a/c/a1/e0/j;->d()V

    iget-object v1, v0, Lf/h/a/c/a1/e0/u;->c:Lf/h/a/c/i1/r;

    iget-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/r;->B(I)V

    :goto_c
    return v11
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/e0/u;->j:Lf/h/a/c/a1/i;

    return-void
.end method

.method public f(JJ)V
    .locals 6

    iget-object p1, p0, Lf/h/a/c/a1/e0/u;->a:Lf/h/a/c/i1/z;

    invoke-virtual {p1}, Lf/h/a/c/i1/z;->c()J

    move-result-wide p1

    const/4 v0, 0x0

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/a1/e0/u;->a:Lf/h/a/c/i1/z;

    iget-wide p1, p1, Lf/h/a/c/i1/z;->a:J

    const-wide/16 v3, 0x0

    cmp-long v5, p1, v3

    if-eqz v5, :cond_2

    cmp-long v3, p1, p3

    if-eqz v3, :cond_2

    :cond_1
    iget-object p1, p0, Lf/h/a/c/a1/e0/u;->a:Lf/h/a/c/i1/z;

    iput-wide v1, p1, Lf/h/a/c/i1/z;->c:J

    iget-object p1, p0, Lf/h/a/c/a1/e0/u;->a:Lf/h/a/c/i1/z;

    invoke-virtual {p1, p3, p4}, Lf/h/a/c/i1/z;->d(J)V

    :cond_2
    iget-object p1, p0, Lf/h/a/c/a1/e0/u;->i:Lf/h/a/c/a1/e0/s;

    if-eqz p1, :cond_3

    invoke-virtual {p1, p3, p4}, Lf/h/a/c/a1/a;->e(J)V

    :cond_3
    const/4 p1, 0x0

    :goto_1
    iget-object p2, p0, Lf/h/a/c/a1/e0/u;->b:Landroid/util/SparseArray;

    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result p2

    if-ge p1, p2, :cond_4

    iget-object p2, p0, Lf/h/a/c/a1/e0/u;->b:Landroid/util/SparseArray;

    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/a1/e0/u$a;

    iput-boolean v0, p2, Lf/h/a/c/a1/e0/u$a;->f:Z

    iget-object p2, p2, Lf/h/a/c/a1/e0/u$a;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {p2}, Lf/h/a/c/a1/e0/j;->c()V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/16 v0, 0xe

    new-array v1, v0, [B

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0, v2}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    const/16 v0, 0x1ba

    aget-byte v3, v1, v2

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    const/4 v4, 0x1

    aget-byte v5, v1, v4

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v3, v5

    const/4 v5, 0x2

    aget-byte v6, v1, v5

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x8

    shl-int/2addr v6, v7

    or-int/2addr v3, v6

    const/4 v6, 0x3

    aget-byte v8, v1, v6

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v3, v8

    if-eq v0, v3, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x4

    aget-byte v3, v1, v0

    and-int/lit16 v3, v3, 0xc4

    const/16 v8, 0x44

    if-eq v3, v8, :cond_1

    return v2

    :cond_1
    const/4 v3, 0x6

    aget-byte v3, v1, v3

    and-int/2addr v3, v0

    if-eq v3, v0, :cond_2

    return v2

    :cond_2
    aget-byte v3, v1, v7

    and-int/2addr v3, v0

    if-eq v3, v0, :cond_3

    return v2

    :cond_3
    const/16 v0, 0x9

    aget-byte v0, v1, v0

    and-int/2addr v0, v4

    if-eq v0, v4, :cond_4

    return v2

    :cond_4
    const/16 v0, 0xc

    aget-byte v0, v1, v0

    and-int/2addr v0, v6

    if-eq v0, v6, :cond_5

    return v2

    :cond_5
    const/16 v0, 0xd

    aget-byte v0, v1, v0

    and-int/lit8 v0, v0, 0x7

    invoke-virtual {p1, v0, v2}, Lf/h/a/c/a1/e;->a(IZ)Z

    invoke-virtual {p1, v1, v2, v6, v2}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    aget-byte p1, v1, v2

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x10

    aget-byte v0, v1, v4

    and-int/lit16 v0, v0, 0xff

    shl-int/2addr v0, v7

    or-int/2addr p1, v0

    aget-byte v0, v1, v5

    and-int/lit16 v0, v0, 0xff

    or-int/2addr p1, v0

    if-ne v4, p1, :cond_6

    const/4 v2, 0x1

    :cond_6
    return v2
.end method

.method public release()V
    .locals 0

    return-void
.end method
