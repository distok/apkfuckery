.class public final Lf/h/a/c/a1/e0/z$a;
.super Ljava/lang/Object;
.source "TsBinarySearchSeeker.java"

# interfaces
.implements Lf/h/a/c/a1/a$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/e0/z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/i1/z;

.field public final b:Lf/h/a/c/i1/r;

.field public final c:I


# direct methods
.method public constructor <init>(ILf/h/a/c/i1/z;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/h/a/c/a1/e0/z$a;->c:I

    iput-object p2, p0, Lf/h/a/c/a1/e0/z$a;->a:Lf/h/a/c/i1/z;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/z$a;->b:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;J)Lf/h/a/c/a1/a$e;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-wide v2, v1, Lf/h/a/c/a1/e;->d:J

    iget-wide v4, v1, Lf/h/a/c/a1/e;->c:J

    sub-long/2addr v4, v2

    const-wide/32 v6, 0x1b8a0

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v5, v4

    iget-object v4, v0, Lf/h/a/c/a1/e0/z$a;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v4, v0, Lf/h/a/c/a1/e0/z$a;->b:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6, v5, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v1, v0, Lf/h/a/c/a1/e0/z$a;->b:Lf/h/a/c/i1/r;

    iget v4, v1, Lf/h/a/c/i1/r;->c:I

    const-wide/16 v7, -0x1

    move-wide v9, v7

    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    :goto_0
    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v13

    const/16 v14, 0xbc

    if-lt v13, v14, :cond_6

    iget-object v13, v1, Lf/h/a/c/i1/r;->a:[B

    iget v14, v1, Lf/h/a/c/i1/r;->b:I

    :goto_1
    if-ge v14, v4, :cond_0

    aget-byte v15, v13, v14

    const/16 v5, 0x47

    if-eq v15, v5, :cond_0

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_0
    add-int/lit16 v5, v14, 0xbc

    if-le v5, v4, :cond_1

    goto :goto_2

    :cond_1
    iget v6, v0, Lf/h/a/c/a1/e0/z$a;->c:I

    invoke-static {v1, v14, v6}, Lf/g/j/k/a;->I0(Lf/h/a/c/i1/r;II)J

    move-result-wide v6

    const-wide v15, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v8, v6, v15

    if-eqz v8, :cond_5

    iget-object v8, v0, Lf/h/a/c/a1/e0/z$a;->a:Lf/h/a/c/i1/z;

    invoke-virtual {v8, v6, v7}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v6

    cmp-long v8, v6, p2

    if-lez v8, :cond_3

    cmp-long v1, v11, v15

    if-nez v1, :cond_2

    invoke-static {v6, v7, v2, v3}, Lf/h/a/c/a1/a$e;->a(JJ)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto :goto_3

    :cond_2
    add-long/2addr v2, v9

    invoke-static {v2, v3}, Lf/h/a/c/a1/a$e;->b(J)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto :goto_3

    :cond_3
    const-wide/32 v8, 0x186a0

    add-long/2addr v8, v6

    cmp-long v10, v8, p2

    if-lez v10, :cond_4

    int-to-long v4, v14

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lf/h/a/c/a1/a$e;->b(J)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto :goto_3

    :cond_4
    int-to-long v8, v14

    move-wide v11, v6

    move-wide v9, v8

    :cond_5
    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->C(I)V

    int-to-long v7, v5

    goto :goto_0

    :cond_6
    :goto_2
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v11, v4

    if-eqz v1, :cond_7

    add-long/2addr v2, v7

    invoke-static {v11, v12, v2, v3}, Lf/h/a/c/a1/a$e;->c(JJ)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto :goto_3

    :cond_7
    sget-object v1, Lf/h/a/c/a1/a$e;->d:Lf/h/a/c/a1/a$e;

    :goto_3
    return-object v1
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/e0/z$a;->b:Lf/h/a/c/i1/r;

    sget-object v1, Lf/h/a/c/i1/a0;->f:[B

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->z([B)V

    return-void
.end method
