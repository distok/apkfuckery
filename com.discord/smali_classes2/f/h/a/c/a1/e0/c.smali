.class public final Lf/h/a/c/a1/e0/c;
.super Ljava/lang/Object;
.source "Ac4Extractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# instance fields
.field public final a:Lf/h/a/c/a1/e0/d;

.field public final b:Lf/h/a/c/i1/r;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/a1/e0/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/h/a/c/a1/e0/d;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/c;->a:Lf/h/a/c/a1/e0/d;

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0x4000

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/c;->b:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object p2, p0, Lf/h/a/c/a1/e0/c;->b:Lf/h/a/c/i1/r;

    iget-object p2, p2, Lf/h/a/c/i1/r;->a:[B

    const/4 v0, 0x0

    const/16 v1, 0x4000

    invoke-virtual {p1, p2, v0, v1}, Lf/h/a/c/a1/e;->f([BII)I

    move-result p1

    const/4 p2, -0x1

    if-ne p1, p2, :cond_0

    return p2

    :cond_0
    iget-object p2, p0, Lf/h/a/c/a1/e0/c;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p2, v0}, Lf/h/a/c/i1/r;->C(I)V

    iget-object p2, p0, Lf/h/a/c/a1/e0/c;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p2, p1}, Lf/h/a/c/i1/r;->B(I)V

    iget-boolean p1, p0, Lf/h/a/c/a1/e0/c;->c:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/a1/e0/c;->a:Lf/h/a/c/a1/e0/d;

    const-wide/16 v1, 0x0

    iput-wide v1, p1, Lf/h/a/c/a1/e0/d;->m:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/a1/e0/c;->c:Z

    :cond_1
    iget-object p1, p0, Lf/h/a/c/a1/e0/c;->a:Lf/h/a/c/a1/e0/d;

    iget-object p2, p0, Lf/h/a/c/a1/e0/c;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p1, p2}, Lf/h/a/c/a1/e0/d;->b(Lf/h/a/c/i1/r;)V

    return v0
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 5

    iget-object v0, p0, Lf/h/a/c/a1/e0/c;->a:Lf/h/a/c/a1/e0/d;

    new-instance v1, Lf/h/a/c/a1/e0/c0$d;

    const/high16 v2, -0x80000000

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lf/h/a/c/a1/e0/c0$d;-><init>(III)V

    invoke-virtual {v0, p1, v1}, Lf/h/a/c/a1/e0/d;->e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    invoke-interface {p1}, Lf/h/a/c/a1/i;->k()V

    new-instance v0, Lf/h/a/c/a1/q$b;

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {p1, v0}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    return-void
.end method

.method public f(JJ)V
    .locals 0

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/c/a1/e0/c;->c:Z

    iget-object p1, p0, Lf/h/a/c/a1/e0/c;->a:Lf/h/a/c/a1/e0/d;

    invoke-virtual {p1}, Lf/h/a/c/a1/e0/d;->c()V

    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    iget-object v4, v0, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v4, v2, v1, v2}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->s()I

    move-result v4

    const v5, 0x494433

    const/4 v6, 0x3

    if-eq v4, v5, :cond_7

    iput v2, p1, Lf/h/a/c/a1/e;->f:I

    invoke-virtual {p1, v3, v2}, Lf/h/a/c/a1/e;->a(IZ)Z

    move v4, v3

    :goto_1
    const/4 v1, 0x0

    :goto_2
    iget-object v5, v0, Lf/h/a/c/i1/r;->a:[B

    const/4 v7, 0x7

    invoke-virtual {p1, v5, v2, v7, v2}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->v()I

    move-result v5

    const v8, 0xac40

    const v9, 0xac41

    if-eq v5, v8, :cond_1

    if-eq v5, v9, :cond_1

    iput v2, p1, Lf/h/a/c/a1/e;->f:I

    add-int/lit8 v4, v4, 0x1

    sub-int v1, v4, v3

    const/16 v5, 0x2000

    if-lt v1, v5, :cond_0

    return v2

    :cond_0
    invoke-virtual {p1, v4, v2}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto :goto_1

    :cond_1
    const/4 v8, 0x1

    add-int/2addr v1, v8

    const/4 v10, 0x4

    if-lt v1, v10, :cond_2

    return v8

    :cond_2
    iget-object v8, v0, Lf/h/a/c/i1/r;->a:[B

    array-length v11, v8

    const/4 v12, -0x1

    if-ge v11, v7, :cond_3

    const/4 v11, -0x1

    goto :goto_4

    :cond_3
    const/4 v11, 0x2

    aget-byte v11, v8, v11

    and-int/lit16 v11, v11, 0xff

    shl-int/lit8 v11, v11, 0x8

    aget-byte v13, v8, v6

    and-int/lit16 v13, v13, 0xff

    or-int/2addr v11, v13

    const v13, 0xffff

    if-ne v11, v13, :cond_4

    aget-byte v10, v8, v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x10

    const/4 v11, 0x5

    aget-byte v11, v8, v11

    and-int/lit16 v11, v11, 0xff

    shl-int/lit8 v11, v11, 0x8

    or-int/2addr v10, v11

    const/4 v11, 0x6

    aget-byte v8, v8, v11

    and-int/lit16 v8, v8, 0xff

    or-int v11, v10, v8

    goto :goto_3

    :cond_4
    const/4 v7, 0x4

    :goto_3
    if-ne v5, v9, :cond_5

    add-int/lit8 v7, v7, 0x2

    :cond_5
    add-int/2addr v11, v7

    :goto_4
    if-ne v11, v12, :cond_6

    return v2

    :cond_6
    add-int/lit8 v11, v11, -0x7

    invoke-virtual {p1, v11, v2}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto :goto_2

    :cond_7
    invoke-virtual {v0, v6}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->p()I

    move-result v4

    add-int/lit8 v5, v4, 0xa

    add-int/2addr v3, v5

    invoke-virtual {p1, v4, v2}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto/16 :goto_0
.end method

.method public release()V
    .locals 0

    return-void
.end method
