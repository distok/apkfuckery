.class public Lf/h/a/c/a1/e0/b0$b;
.super Ljava/lang/Object;
.source "TsExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/e0/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/e0/b0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/i1/q;

.field public final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/a1/e0/c0;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/util/SparseIntArray;

.field public final d:I

.field public final synthetic e:Lf/h/a/c/a1/e0/b0;


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/e0/b0;I)V
    .locals 1

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Lf/h/a/c/i1/q;

    const/4 v0, 0x5

    new-array v0, v0, [B

    invoke-direct {p1, v0}, Lf/h/a/c/i1/q;-><init>([B)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0$b;->b:Landroid/util/SparseArray;

    new-instance p1, Landroid/util/SparseIntArray;

    invoke-direct {p1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0$b;->c:Landroid/util/SparseIntArray;

    iput p2, p0, Lf/h/a/c/a1/e0/b0$b;->d:I

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/i1/z;Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 0

    return-void
.end method

.method public b(Lf/h/a/c/i1/r;)V
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    return-void

    :cond_0
    iget-object v2, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget v4, v2, Lf/h/a/c/a1/e0/b0;->a:I

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eq v4, v6, :cond_2

    if-eq v4, v3, :cond_2

    iget v4, v2, Lf/h/a/c/a1/e0/b0;->l:I

    if-ne v4, v6, :cond_1

    goto :goto_0

    :cond_1
    new-instance v4, Lf/h/a/c/i1/z;

    iget-object v2, v2, Lf/h/a/c/a1/e0/b0;->b:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/i1/z;

    iget-wide v7, v2, Lf/h/a/c/i1/z;->a:J

    invoke-direct {v4, v7, v8}, Lf/h/a/c/i1/z;-><init>(J)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v2, v2, Lf/h/a/c/a1/e0/b0;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v2, v2, Lf/h/a/c/a1/e0/b0;->b:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lf/h/a/c/i1/z;

    :goto_1
    invoke-virtual {v1, v3}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->v()I

    move-result v2

    const/4 v7, 0x3

    invoke-virtual {v1, v7}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v8, v3}, Lf/h/a/c/i1/r;->c(Lf/h/a/c/i1/q;I)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v8, v7}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v9, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    const/16 v10, 0xd

    invoke-virtual {v9, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v9

    iput v9, v8, Lf/h/a/c/a1/e0/b0;->r:I

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v8, v3}, Lf/h/a/c/i1/r;->c(Lf/h/a/c/i1/q;I)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    const/16 v11, 0xc

    invoke-virtual {v8, v11}, Lf/h/a/c/i1/q;->f(I)I

    move-result v8

    invoke-virtual {v1, v8}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget v12, v8, Lf/h/a/c/a1/e0/b0;->a:I

    const/16 v13, 0x15

    const/4 v14, 0x0

    const/16 v15, 0x2000

    if-ne v12, v3, :cond_3

    iget-object v8, v8, Lf/h/a/c/a1/e0/b0;->p:Lf/h/a/c/a1/e0/c0;

    if-nez v8, :cond_3

    new-instance v8, Lf/h/a/c/a1/e0/c0$b;

    sget-object v12, Lf/h/a/c/i1/a0;->f:[B

    invoke-direct {v8, v13, v14, v14, v12}, Lf/h/a/c/a1/e0/c0$b;-><init>(ILjava/lang/String;Ljava/util/List;[B)V

    iget-object v12, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v14, v12, Lf/h/a/c/a1/e0/b0;->e:Lf/h/a/c/a1/e0/c0$c;

    invoke-interface {v14, v13, v8}, Lf/h/a/c/a1/e0/c0$c;->a(ILf/h/a/c/a1/e0/c0$b;)Lf/h/a/c/a1/e0/c0;

    move-result-object v8

    iput-object v8, v12, Lf/h/a/c/a1/e0/b0;->p:Lf/h/a/c/a1/e0/c0;

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v12, v8, Lf/h/a/c/a1/e0/b0;->p:Lf/h/a/c/a1/e0/c0;

    iget-object v8, v8, Lf/h/a/c/a1/e0/b0;->k:Lf/h/a/c/a1/i;

    new-instance v14, Lf/h/a/c/a1/e0/c0$d;

    invoke-direct {v14, v2, v13, v15}, Lf/h/a/c/a1/e0/c0$d;-><init>(III)V

    invoke-interface {v12, v4, v8, v14}, Lf/h/a/c/a1/e0/c0;->a(Lf/h/a/c/i1/z;Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    :cond_3
    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->b:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->clear()V

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0$b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v8}, Landroid/util/SparseIntArray;->clear()V

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v8

    :goto_2
    if-lez v8, :cond_17

    iget-object v14, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    const/4 v12, 0x5

    invoke-virtual {v1, v14, v12}, Lf/h/a/c/i1/r;->c(Lf/h/a/c/i1/q;I)V

    iget-object v14, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    const/16 v6, 0x8

    invoke-virtual {v14, v6}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    iget-object v14, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v14, v7}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v14, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v14, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v14

    iget-object v10, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v10, v9}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v10, v0, Lf/h/a/c/a1/e0/b0$b;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v10, v11}, Lf/h/a/c/i1/q;->f(I)I

    move-result v10

    iget v11, v1, Lf/h/a/c/i1/r;->b:I

    add-int v15, v10, v11

    const/4 v3, -0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    :goto_3
    iget v5, v1, Lf/h/a/c/i1/r;->b:I

    if-ge v5, v15, :cond_f

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v16

    iget v9, v1, Lf/h/a/c/i1/r;->b:I

    add-int v9, v9, v16

    const/16 v7, 0x59

    if-ne v5, v12, :cond_7

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v19

    const-wide/32 v21, 0x41432d33

    cmp-long v5, v19, v21

    if-nez v5, :cond_4

    goto :goto_4

    :cond_4
    const-wide/32 v21, 0x45414333

    cmp-long v5, v19, v21

    if-nez v5, :cond_5

    goto :goto_5

    :cond_5
    const-wide/32 v21, 0x41432d34

    cmp-long v5, v19, v21

    if-nez v5, :cond_6

    goto :goto_6

    :cond_6
    const-wide/32 v21, 0x48455643

    cmp-long v5, v19, v21

    if-nez v5, :cond_a

    const/16 v3, 0x24

    goto :goto_7

    :cond_7
    const/16 v12, 0x6a

    if-ne v5, v12, :cond_8

    :goto_4
    const/16 v3, 0x81

    goto :goto_7

    :cond_8
    const/16 v12, 0x7a

    if-ne v5, v12, :cond_9

    :goto_5
    const/16 v3, 0x87

    goto :goto_7

    :cond_9
    const/16 v12, 0x7f

    if-ne v5, v12, :cond_b

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v5

    if-ne v5, v13, :cond_a

    :goto_6
    const/16 v3, 0xac

    :cond_a
    :goto_7
    move/from16 v21, v2

    move-object/from16 v20, v4

    move/from16 v22, v14

    const/4 v12, 0x4

    goto :goto_9

    :cond_b
    const/16 v12, 0x7b

    if-ne v5, v12, :cond_c

    const/16 v3, 0x8a

    goto :goto_7

    :cond_c
    const/16 v12, 0xa

    if-ne v5, v12, :cond_d

    const/4 v12, 0x3

    invoke-virtual {v1, v12}, Lf/h/a/c/i1/r;->n(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    goto :goto_7

    :cond_d
    const/4 v12, 0x3

    if-ne v5, v7, :cond_a

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :goto_8
    iget v5, v1, Lf/h/a/c/i1/r;->b:I

    if-ge v5, v9, :cond_e

    invoke-virtual {v1, v12}, Lf/h/a/c/i1/r;->n(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v7

    const/4 v12, 0x4

    new-array v13, v12, [B

    move-object/from16 v20, v4

    iget-object v4, v1, Lf/h/a/c/i1/r;->a:[B

    move/from16 v21, v2

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    move/from16 v22, v14

    const/4 v14, 0x0

    invoke-static {v4, v2, v13, v14, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v2, v12

    iput v2, v1, Lf/h/a/c/i1/r;->b:I

    new-instance v2, Lf/h/a/c/a1/e0/c0$a;

    invoke-direct {v2, v5, v7, v13}, Lf/h/a/c/a1/e0/c0$a;-><init>(Ljava/lang/String;I[B)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v4, v20

    move/from16 v2, v21

    move/from16 v14, v22

    const/16 v7, 0x59

    const/4 v12, 0x3

    const/16 v13, 0x15

    goto :goto_8

    :cond_e
    move/from16 v21, v2

    move-object/from16 v20, v4

    move/from16 v22, v14

    const/4 v12, 0x4

    move-object/from16 v18, v3

    const/16 v3, 0x59

    :goto_9
    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    sub-int/2addr v9, v2

    invoke-virtual {v1, v9}, Lf/h/a/c/i1/r;->D(I)V

    move-object/from16 v4, v20

    move/from16 v2, v21

    move/from16 v14, v22

    const/4 v7, 0x3

    const/4 v9, 0x4

    const/4 v12, 0x5

    const/16 v13, 0x15

    goto/16 :goto_3

    :cond_f
    move/from16 v21, v2

    move-object/from16 v20, v4

    move/from16 v22, v14

    const/4 v12, 0x4

    invoke-virtual {v1, v15}, Lf/h/a/c/i1/r;->C(I)V

    new-instance v2, Lf/h/a/c/a1/e0/c0$b;

    iget-object v4, v1, Lf/h/a/c/i1/r;->a:[B

    invoke-static {v4, v11, v15}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v4

    move-object/from16 v5, v17

    move-object/from16 v7, v18

    invoke-direct {v2, v3, v5, v7, v4}, Lf/h/a/c/a1/e0/c0$b;-><init>(ILjava/lang/String;Ljava/util/List;[B)V

    const/4 v4, 0x6

    if-ne v6, v4, :cond_10

    move v6, v3

    :cond_10
    add-int/lit8 v10, v10, 0x5

    sub-int/2addr v8, v10

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget v4, v3, Lf/h/a/c/a1/e0/b0;->a:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_11

    move v4, v6

    goto :goto_a

    :cond_11
    move/from16 v4, v22

    :goto_a
    iget-object v3, v3, Lf/h/a/c/a1/e0/b0;->g:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v7, 0x15

    goto :goto_d

    :cond_12
    iget-object v3, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget v7, v3, Lf/h/a/c/a1/e0/b0;->a:I

    if-ne v7, v5, :cond_13

    const/16 v7, 0x15

    if-ne v6, v7, :cond_14

    iget-object v2, v3, Lf/h/a/c/a1/e0/b0;->p:Lf/h/a/c/a1/e0/c0;

    goto :goto_b

    :cond_13
    const/16 v7, 0x15

    :cond_14
    iget-object v3, v3, Lf/h/a/c/a1/e0/b0;->e:Lf/h/a/c/a1/e0/c0$c;

    invoke-interface {v3, v6, v2}, Lf/h/a/c/a1/e0/c0$c;->a(ILf/h/a/c/a1/e0/c0$b;)Lf/h/a/c/a1/e0/c0;

    move-result-object v2

    :goto_b
    iget-object v3, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget v3, v3, Lf/h/a/c/a1/e0/b0;->a:I

    if-ne v3, v5, :cond_15

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0$b;->c:Landroid/util/SparseIntArray;

    const/16 v5, 0x2000

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    move/from16 v5, v22

    if-ge v5, v3, :cond_16

    goto :goto_c

    :cond_15
    move/from16 v5, v22

    :goto_c
    iget-object v3, v0, Lf/h/a/c/a1/e0/b0$b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0$b;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_16
    :goto_d
    move-object/from16 v4, v20

    move/from16 v2, v21

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x3

    const/4 v9, 0x4

    const/16 v10, 0xd

    const/16 v11, 0xc

    const/16 v13, 0x15

    const/16 v15, 0x2000

    goto/16 :goto_2

    :cond_17
    move/from16 v21, v2

    move-object/from16 v20, v4

    iget-object v1, v0, Lf/h/a/c/a1/e0/b0$b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    const/4 v14, 0x0

    :goto_e
    if-ge v14, v1, :cond_1a

    iget-object v2, v0, Lf/h/a/c/a1/e0/b0$b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v14}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0$b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v14}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    iget-object v4, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v4, v4, Lf/h/a/c/a1/e0/b0;->g:Landroid/util/SparseBooleanArray;

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    iget-object v4, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v4, v4, Lf/h/a/c/a1/e0/b0;->h:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    iget-object v4, v0, Lf/h/a/c/a1/e0/b0$b;->b:Landroid/util/SparseArray;

    invoke-virtual {v4, v14}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/a1/e0/c0;

    if-eqz v4, :cond_19

    iget-object v5, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v6, v5, Lf/h/a/c/a1/e0/b0;->p:Lf/h/a/c/a1/e0/c0;

    if-eq v4, v6, :cond_18

    iget-object v5, v5, Lf/h/a/c/a1/e0/b0;->k:Lf/h/a/c/a1/i;

    new-instance v6, Lf/h/a/c/a1/e0/c0$d;

    move/from16 v7, v21

    const/16 v8, 0x2000

    invoke-direct {v6, v7, v2, v8}, Lf/h/a/c/a1/e0/c0$d;-><init>(III)V

    move-object/from16 v2, v20

    invoke-interface {v4, v2, v5, v6}, Lf/h/a/c/a1/e0/c0;->a(Lf/h/a/c/i1/z;Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    goto :goto_f

    :cond_18
    move-object/from16 v2, v20

    move/from16 v7, v21

    const/16 v8, 0x2000

    :goto_f
    iget-object v5, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget-object v5, v5, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_10

    :cond_19
    move-object/from16 v2, v20

    move/from16 v7, v21

    const/16 v8, 0x2000

    :goto_10
    add-int/lit8 v14, v14, 0x1

    move-object/from16 v20, v2

    move/from16 v21, v7

    goto :goto_e

    :cond_1a
    iget-object v1, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget v2, v1, Lf/h/a/c/a1/e0/b0;->a:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1b

    iget-boolean v2, v1, Lf/h/a/c/a1/e0/b0;->m:Z

    if-nez v2, :cond_1d

    iget-object v1, v1, Lf/h/a/c/a1/e0/b0;->k:Lf/h/a/c/a1/i;

    invoke-interface {v1}, Lf/h/a/c/a1/i;->k()V

    iget-object v1, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    const/4 v2, 0x0

    iput v2, v1, Lf/h/a/c/a1/e0/b0;->l:I

    const/4 v3, 0x1

    iput-boolean v3, v1, Lf/h/a/c/a1/e0/b0;->m:Z

    goto :goto_12

    :cond_1b
    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v1, v1, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    iget v4, v0, Lf/h/a/c/a1/e0/b0$b;->d:I

    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->remove(I)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iget v4, v1, Lf/h/a/c/a1/e0/b0;->a:I

    if-ne v4, v3, :cond_1c

    const/4 v5, 0x0

    goto :goto_11

    :cond_1c
    iget v2, v1, Lf/h/a/c/a1/e0/b0;->l:I

    const/4 v4, -0x1

    add-int/lit8 v5, v2, -0x1

    :goto_11
    iput v5, v1, Lf/h/a/c/a1/e0/b0;->l:I

    if-nez v5, :cond_1d

    iget-object v1, v1, Lf/h/a/c/a1/e0/b0;->k:Lf/h/a/c/a1/i;

    invoke-interface {v1}, Lf/h/a/c/a1/i;->k()V

    iget-object v1, v0, Lf/h/a/c/a1/e0/b0$b;->e:Lf/h/a/c/a1/e0/b0;

    iput-boolean v3, v1, Lf/h/a/c/a1/e0/b0;->m:Z

    :cond_1d
    :goto_12
    return-void
.end method
