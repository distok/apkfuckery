.class public final Lf/h/a/c/a1/e0/m;
.super Ljava/lang/Object;
.source "H265Reader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/e0/m$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/e0/x;

.field public b:Ljava/lang/String;

.field public c:Lf/h/a/c/a1/s;

.field public d:Lf/h/a/c/a1/e0/m$a;

.field public e:Z

.field public final f:[Z

.field public final g:Lf/h/a/c/a1/e0/q;

.field public final h:Lf/h/a/c/a1/e0/q;

.field public final i:Lf/h/a/c/a1/e0/q;

.field public final j:Lf/h/a/c/a1/e0/q;

.field public final k:Lf/h/a/c/a1/e0/q;

.field public l:J

.field public m:J

.field public final n:Lf/h/a/c/i1/r;


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/e0/x;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->a:Lf/h/a/c/a1/e0/x;

    const/4 p1, 0x3

    new-array p1, p1, [Z

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->f:[Z

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/16 v0, 0x20

    const/16 v1, 0x80

    invoke-direct {p1, v0, v1}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->g:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/16 v0, 0x21

    invoke-direct {p1, v0, v1}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->h:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/16 v0, 0x22

    invoke-direct {p1, v0, v1}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->i:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/16 v0, 0x27

    invoke-direct {p1, v0, v1}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->j:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/a1/e0/q;

    const/16 v0, 0x28

    invoke-direct {p1, v0, v1}, Lf/h/a/c/a1/e0/q;-><init>(II)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->k:Lf/h/a/c/a1/e0/q;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/m;->n:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public final a([BII)V
    .locals 3

    iget-boolean v0, p0, Lf/h/a/c/a1/e0/m;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->d:Lf/h/a/c/a1/e0/m$a;

    iget-boolean v1, v0, Lf/h/a/c/a1/e0/m$a;->f:Z

    if-eqz v1, :cond_3

    add-int/lit8 v1, p2, 0x2

    iget v2, v0, Lf/h/a/c/a1/e0/m$a;->d:I

    sub-int/2addr v1, v2

    if-ge v1, p3, :cond_1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0x80

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lf/h/a/c/a1/e0/m$a;->g:Z

    iput-boolean v2, v0, Lf/h/a/c/a1/e0/m$a;->f:Z

    goto :goto_1

    :cond_1
    sub-int v1, p3, p2

    add-int/2addr v1, v2

    iput v1, v0, Lf/h/a/c/a1/e0/m$a;->d:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->g:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->h:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->i:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->j:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->k:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/c/a1/e0/q;->a([BII)V

    return-void
.end method

.method public b(Lf/h/a/c/i1/r;)V
    .locals 35

    move-object/from16 v0, p0

    :cond_0
    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-lez v2, :cond_30

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v3, v1, Lf/h/a/c/i1/r;->c:I

    iget-object v4, v1, Lf/h/a/c/i1/r;->a:[B

    iget-wide v5, v0, Lf/h/a/c/a1/e0/m;->l:J

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v5, v7

    iput-wide v5, v0, Lf/h/a/c/a1/e0/m;->l:J

    iget-object v5, v0, Lf/h/a/c/a1/e0/m;->c:Lf/h/a/c/a1/s;

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v6

    invoke-interface {v5, v1, v6}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v5, v0, Lf/h/a/c/a1/e0/m;->f:[Z

    invoke-static {v4, v2, v3, v5}, Lf/h/a/c/i1/p;->b([BII[Z)I

    move-result v5

    if-ne v5, v3, :cond_1

    invoke-virtual {v0, v4, v2, v3}, Lf/h/a/c/a1/e0/m;->a([BII)V

    return-void

    :cond_1
    add-int/lit8 v6, v5, 0x3

    aget-byte v7, v4, v6

    and-int/lit8 v7, v7, 0x7e

    const/4 v8, 0x1

    shr-int/2addr v7, v8

    sub-int v9, v5, v2

    if-lez v9, :cond_2

    invoke-virtual {v0, v4, v2, v5}, Lf/h/a/c/a1/e0/m;->a([BII)V

    :cond_2
    sub-int v2, v3, v5

    iget-wide v10, v0, Lf/h/a/c/a1/e0/m;->l:J

    int-to-long v12, v2

    sub-long/2addr v10, v12

    const/4 v5, 0x0

    if-gez v9, :cond_3

    neg-int v9, v9

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    :goto_1
    iget-wide v12, v0, Lf/h/a/c/a1/e0/m;->m:J

    iget-boolean v14, v0, Lf/h/a/c/a1/e0/m;->e:Z

    if-eqz v14, :cond_8

    iget-object v14, v0, Lf/h/a/c/a1/e0/m;->d:Lf/h/a/c/a1/e0/m$a;

    iget-boolean v15, v14, Lf/h/a/c/a1/e0/m$a;->j:Z

    if-eqz v15, :cond_4

    iget-boolean v15, v14, Lf/h/a/c/a1/e0/m$a;->g:Z

    if-eqz v15, :cond_4

    iget-boolean v8, v14, Lf/h/a/c/a1/e0/m$a;->c:Z

    iput-boolean v8, v14, Lf/h/a/c/a1/e0/m$a;->m:Z

    iput-boolean v5, v14, Lf/h/a/c/a1/e0/m$a;->j:Z

    goto :goto_2

    :cond_4
    iget-boolean v5, v14, Lf/h/a/c/a1/e0/m$a;->h:Z

    if-nez v5, :cond_6

    iget-boolean v5, v14, Lf/h/a/c/a1/e0/m$a;->g:Z

    if-eqz v5, :cond_5

    goto :goto_4

    :cond_5
    :goto_2
    move/from16 v31, v2

    move/from16 v28, v3

    move-object/from16 v29, v4

    move/from16 v30, v6

    move/from16 v32, v7

    move v5, v9

    :goto_3
    move-wide/from16 v33, v10

    goto/16 :goto_18

    :cond_6
    :goto_4
    iget-boolean v5, v14, Lf/h/a/c/a1/e0/m$a;->i:Z

    if-eqz v5, :cond_7

    move v5, v9

    iget-wide v8, v14, Lf/h/a/c/a1/e0/m$a;->b:J

    sub-long v8, v10, v8

    long-to-int v9, v8

    add-int/2addr v9, v2

    invoke-virtual {v14, v9}, Lf/h/a/c/a1/e0/m$a;->a(I)V

    goto :goto_5

    :cond_7
    move v5, v9

    :goto_5
    iget-wide v8, v14, Lf/h/a/c/a1/e0/m$a;->b:J

    iput-wide v8, v14, Lf/h/a/c/a1/e0/m$a;->k:J

    iget-wide v8, v14, Lf/h/a/c/a1/e0/m$a;->e:J

    iput-wide v8, v14, Lf/h/a/c/a1/e0/m$a;->l:J

    const/4 v8, 0x1

    iput-boolean v8, v14, Lf/h/a/c/a1/e0/m$a;->i:Z

    iget-boolean v8, v14, Lf/h/a/c/a1/e0/m$a;->c:Z

    iput-boolean v8, v14, Lf/h/a/c/a1/e0/m$a;->m:Z

    goto/16 :goto_17

    :cond_8
    move v5, v9

    iget-object v8, v0, Lf/h/a/c/a1/e0/m;->g:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v8, v5}, Lf/h/a/c/a1/e0/q;->b(I)Z

    iget-object v8, v0, Lf/h/a/c/a1/e0/m;->h:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v8, v5}, Lf/h/a/c/a1/e0/q;->b(I)Z

    iget-object v8, v0, Lf/h/a/c/a1/e0/m;->i:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v8, v5}, Lf/h/a/c/a1/e0/q;->b(I)Z

    iget-object v8, v0, Lf/h/a/c/a1/e0/m;->g:Lf/h/a/c/a1/e0/q;

    iget-boolean v9, v8, Lf/h/a/c/a1/e0/q;->c:Z

    if-eqz v9, :cond_27

    iget-object v9, v0, Lf/h/a/c/a1/e0/m;->h:Lf/h/a/c/a1/e0/q;

    iget-boolean v14, v9, Lf/h/a/c/a1/e0/q;->c:Z

    if-eqz v14, :cond_27

    iget-object v14, v0, Lf/h/a/c/a1/e0/m;->i:Lf/h/a/c/a1/e0/q;

    iget-boolean v15, v14, Lf/h/a/c/a1/e0/q;->c:Z

    if-eqz v15, :cond_27

    iget-object v15, v0, Lf/h/a/c/a1/e0/m;->c:Lf/h/a/c/a1/s;

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->b:Ljava/lang/String;

    move/from16 v28, v3

    iget v3, v8, Lf/h/a/c/a1/e0/q;->e:I

    move-object/from16 v29, v4

    iget v4, v9, Lf/h/a/c/a1/e0/q;->e:I

    add-int/2addr v4, v3

    move/from16 v30, v6

    iget v6, v14, Lf/h/a/c/a1/e0/q;->e:I

    add-int/2addr v4, v6

    new-array v4, v4, [B

    iget-object v6, v8, Lf/h/a/c/a1/e0/q;->d:[B

    move/from16 v31, v2

    const/4 v2, 0x0

    invoke-static {v6, v2, v4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, v9, Lf/h/a/c/a1/e0/q;->d:[B

    iget v6, v8, Lf/h/a/c/a1/e0/q;->e:I

    move/from16 v32, v7

    iget v7, v9, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v3, v2, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, v14, Lf/h/a/c/a1/e0/q;->d:[B

    iget v6, v8, Lf/h/a/c/a1/e0/q;->e:I

    iget v7, v9, Lf/h/a/c/a1/e0/q;->e:I

    add-int/2addr v6, v7

    iget v7, v14, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v3, v2, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v3, Lf/h/a/c/i1/s;

    iget-object v6, v9, Lf/h/a/c/a1/e0/q;->d:[B

    iget v7, v9, Lf/h/a/c/a1/e0/q;->e:I

    invoke-direct {v3, v6, v2, v7}, Lf/h/a/c/i1/s;-><init>([BII)V

    const/16 v2, 0x2c

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/s;->j(I)V

    const/4 v2, 0x3

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/s;->e(I)I

    move-result v6

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->i()V

    const/16 v7, 0x58

    invoke-virtual {v3, v7}, Lf/h/a/c/i1/s;->j(I)V

    const/16 v7, 0x8

    invoke-virtual {v3, v7}, Lf/h/a/c/i1/s;->j(I)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_6
    if-ge v7, v6, :cond_b

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v9

    if-eqz v9, :cond_9

    add-int/lit8 v8, v8, 0x59

    :cond_9
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v9

    if-eqz v9, :cond_a

    add-int/lit8 v8, v8, 0x8

    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    :cond_b
    invoke-virtual {v3, v8}, Lf/h/a/c/i1/s;->j(I)V

    const/4 v7, 0x2

    if-lez v6, :cond_c

    rsub-int/lit8 v8, v6, 0x8

    mul-int/lit8 v8, v8, 0x2

    invoke-virtual {v3, v8}, Lf/h/a/c/i1/s;->j(I)V

    :cond_c
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v8

    if-ne v8, v2, :cond_d

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->i()V

    :cond_d
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v2

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v9

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v14

    if-eqz v14, :cond_11

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v14

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v16

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v17

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v18

    move-wide/from16 v33, v10

    const/4 v10, 0x1

    if-eq v8, v10, :cond_f

    if-ne v8, v7, :cond_e

    goto :goto_7

    :cond_e
    const/4 v7, 0x1

    goto :goto_8

    :cond_f
    :goto_7
    const/4 v7, 0x2

    :goto_8
    if-ne v8, v10, :cond_10

    const/4 v8, 0x2

    goto :goto_9

    :cond_10
    const/4 v8, 0x1

    :goto_9
    add-int v14, v14, v16

    mul-int v14, v14, v7

    sub-int/2addr v2, v14

    add-int v17, v17, v18

    mul-int v17, v17, v8

    sub-int v9, v9, v17

    goto :goto_a

    :cond_11
    move-wide/from16 v33, v10

    :goto_a
    move/from16 v21, v2

    move/from16 v22, v9

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v2

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v7

    if-eqz v7, :cond_12

    const/4 v7, 0x0

    goto :goto_b

    :cond_12
    move v7, v6

    :goto_b
    if-gt v7, v6, :cond_13

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    add-int/lit8 v7, v7, 0x1

    goto :goto_b

    :cond_13
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v6

    const/4 v7, 0x4

    if-eqz v6, :cond_19

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v6

    if-eqz v6, :cond_19

    const/4 v6, 0x0

    :goto_c
    if-ge v6, v7, :cond_19

    const/4 v7, 0x0

    :goto_d
    const/4 v8, 0x6

    if-ge v7, v8, :cond_18

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v8

    if-nez v8, :cond_14

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    goto :goto_f

    :cond_14
    const/16 v8, 0x40

    shl-int/lit8 v9, v6, 0x1

    add-int/lit8 v9, v9, 0x4

    const/4 v10, 0x1

    shl-int v9, v10, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    if-le v6, v10, :cond_15

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->g()I

    :cond_15
    const/4 v9, 0x0

    :goto_e
    if-ge v9, v8, :cond_16

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->g()I

    add-int/lit8 v9, v9, 0x1

    goto :goto_e

    :cond_16
    :goto_f
    const/4 v8, 0x3

    if-ne v6, v8, :cond_17

    const/4 v8, 0x3

    goto :goto_10

    :cond_17
    const/4 v8, 0x1

    :goto_10
    add-int/2addr v7, v8

    goto :goto_d

    :cond_18
    add-int/lit8 v6, v6, 0x1

    const/4 v7, 0x4

    goto :goto_c

    :cond_19
    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Lf/h/a/c/i1/s;->j(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v6

    if-eqz v6, :cond_1a

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Lf/h/a/c/i1/s;->j(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->i()V

    :cond_1a
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_11
    if-ge v7, v6, :cond_21

    if-eqz v7, :cond_1b

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v8

    :cond_1b
    if-eqz v8, :cond_1d

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->i()V

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    const/4 v10, 0x0

    :goto_12
    if-gt v10, v9, :cond_20

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v11

    if-eqz v11, :cond_1c

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->i()V

    :cond_1c
    add-int/lit8 v10, v10, 0x1

    goto :goto_12

    :cond_1d
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v9

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v10

    add-int v11, v9, v10

    const/4 v14, 0x0

    :goto_13
    if-ge v14, v9, :cond_1e

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->i()V

    add-int/lit8 v14, v14, 0x1

    goto :goto_13

    :cond_1e
    const/4 v9, 0x0

    :goto_14
    if-ge v9, v10, :cond_1f

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->i()V

    add-int/lit8 v9, v9, 0x1

    goto :goto_14

    :cond_1f
    move v9, v11

    :cond_20
    add-int/lit8 v7, v7, 0x1

    goto :goto_11

    :cond_21
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v6

    if-eqz v6, :cond_22

    const/4 v6, 0x0

    :goto_15
    invoke-virtual {v3}, Lf/h/a/c/i1/s;->f()I

    move-result v7

    if-ge v6, v7, :cond_22

    add-int/lit8 v7, v2, 0x4

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v3, v7}, Lf/h/a/c/i1/s;->j(I)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_15

    :cond_22
    const/4 v2, 0x2

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/s;->j(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v2

    const/high16 v6, 0x3f800000    # 1.0f

    if-eqz v2, :cond_26

    invoke-virtual {v3}, Lf/h/a/c/i1/s;->d()Z

    move-result v2

    if-eqz v2, :cond_26

    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/s;->e(I)I

    move-result v2

    const/16 v7, 0xff

    if-ne v2, v7, :cond_24

    const/16 v2, 0x10

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/s;->e(I)I

    move-result v7

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/s;->e(I)I

    move-result v2

    if-eqz v7, :cond_23

    if-eqz v2, :cond_23

    int-to-float v3, v7

    int-to-float v2, v2

    div-float v6, v3, v2

    :cond_23
    move/from16 v26, v6

    goto :goto_16

    :cond_24
    sget-object v3, Lf/h/a/c/i1/p;->b:[F

    array-length v6, v3

    if-ge v2, v6, :cond_25

    aget v2, v3, v2

    move/from16 v26, v2

    goto :goto_16

    :cond_25
    const-string v3, "Unexpected aspect_ratio_idc value: "

    const-string v6, "H265Reader"

    invoke-static {v3, v2, v6}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    :cond_26
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v26, 0x3f800000    # 1.0f

    :goto_16
    const/16 v18, 0x0

    const/16 v19, -0x1

    const/16 v20, -0x1

    const/high16 v23, -0x40800000    # -1.0f

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v24

    const/16 v25, -0x1

    const/16 v27, 0x0

    const-string v17, "video/hevc"

    move-object/from16 v16, v1

    invoke-static/range {v16 .. v27}, Lcom/google/android/exoplayer2/Format;->m(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIFLjava/util/List;IFLcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    invoke-interface {v15, v1}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/m;->e:Z

    goto :goto_18

    :cond_27
    :goto_17
    move/from16 v31, v2

    move/from16 v28, v3

    move-object/from16 v29, v4

    move/from16 v30, v6

    move/from16 v32, v7

    goto/16 :goto_3

    :goto_18
    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->j:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v1, v5}, Lf/h/a/c/a1/e0/q;->b(I)Z

    move-result v1

    const/4 v2, 0x5

    if-eqz v1, :cond_28

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->j:Lf/h/a/c/a1/e0/q;

    iget-object v3, v1, Lf/h/a/c/a1/e0/q;->d:[B

    iget v1, v1, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v3, v1}, Lf/h/a/c/i1/p;->e([BI)I

    move-result v1

    iget-object v3, v0, Lf/h/a/c/a1/e0/m;->n:Lf/h/a/c/i1/r;

    iget-object v4, v0, Lf/h/a/c/a1/e0/m;->j:Lf/h/a/c/a1/e0/q;

    iget-object v4, v4, Lf/h/a/c/a1/e0/q;->d:[B

    invoke-virtual {v3, v4, v1}, Lf/h/a/c/i1/r;->A([BI)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->n:Lf/h/a/c/i1/r;

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->a:Lf/h/a/c/a1/e0/x;

    iget-object v3, v0, Lf/h/a/c/a1/e0/m;->n:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/a1/e0/x;->b:[Lf/h/a/c/a1/s;

    invoke-static {v12, v13, v3, v1}, Lf/g/j/k/a;->z(JLf/h/a/c/i1/r;[Lf/h/a/c/a1/s;)V

    :cond_28
    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->k:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v1, v5}, Lf/h/a/c/a1/e0/q;->b(I)Z

    move-result v1

    if-eqz v1, :cond_29

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->k:Lf/h/a/c/a1/e0/q;

    iget-object v3, v1, Lf/h/a/c/a1/e0/q;->d:[B

    iget v1, v1, Lf/h/a/c/a1/e0/q;->e:I

    invoke-static {v3, v1}, Lf/h/a/c/i1/p;->e([BI)I

    move-result v1

    iget-object v3, v0, Lf/h/a/c/a1/e0/m;->n:Lf/h/a/c/i1/r;

    iget-object v4, v0, Lf/h/a/c/a1/e0/m;->k:Lf/h/a/c/a1/e0/q;

    iget-object v4, v4, Lf/h/a/c/a1/e0/q;->d:[B

    invoke-virtual {v3, v4, v1}, Lf/h/a/c/i1/r;->A([BI)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->n:Lf/h/a/c/i1/r;

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->a:Lf/h/a/c/a1/e0/x;

    iget-object v2, v0, Lf/h/a/c/a1/e0/m;->n:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/a1/e0/x;->b:[Lf/h/a/c/a1/s;

    invoke-static {v12, v13, v2, v1}, Lf/g/j/k/a;->z(JLf/h/a/c/i1/r;[Lf/h/a/c/a1/s;)V

    :cond_29
    iget-wide v1, v0, Lf/h/a/c/a1/e0/m;->m:J

    iget-boolean v3, v0, Lf/h/a/c/a1/e0/m;->e:Z

    if-eqz v3, :cond_2f

    iget-object v3, v0, Lf/h/a/c/a1/e0/m;->d:Lf/h/a/c/a1/e0/m$a;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lf/h/a/c/a1/e0/m$a;->g:Z

    iput-boolean v4, v3, Lf/h/a/c/a1/e0/m$a;->h:Z

    iput-wide v1, v3, Lf/h/a/c/a1/e0/m$a;->e:J

    iput v4, v3, Lf/h/a/c/a1/e0/m$a;->d:I

    move-wide/from16 v10, v33

    iput-wide v10, v3, Lf/h/a/c/a1/e0/m$a;->b:J

    const/16 v1, 0x20

    move/from16 v2, v32

    if-lt v2, v1, :cond_2b

    iget-boolean v1, v3, Lf/h/a/c/a1/e0/m$a;->j:Z

    if-nez v1, :cond_2a

    iget-boolean v1, v3, Lf/h/a/c/a1/e0/m$a;->i:Z

    if-eqz v1, :cond_2a

    move/from16 v1, v31

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/m$a;->a(I)V

    iput-boolean v4, v3, Lf/h/a/c/a1/e0/m$a;->i:Z

    :cond_2a
    const/16 v1, 0x22

    if-gt v2, v1, :cond_2b

    iget-boolean v1, v3, Lf/h/a/c/a1/e0/m$a;->j:Z

    const/4 v4, 0x1

    xor-int/2addr v1, v4

    iput-boolean v1, v3, Lf/h/a/c/a1/e0/m$a;->h:Z

    iput-boolean v4, v3, Lf/h/a/c/a1/e0/m$a;->j:Z

    :cond_2b
    const/16 v1, 0x10

    if-lt v2, v1, :cond_2c

    const/16 v1, 0x15

    if-gt v2, v1, :cond_2c

    const/4 v1, 0x1

    goto :goto_19

    :cond_2c
    const/4 v1, 0x0

    :goto_19
    iput-boolean v1, v3, Lf/h/a/c/a1/e0/m$a;->c:Z

    if-nez v1, :cond_2e

    const/16 v1, 0x9

    if-gt v2, v1, :cond_2d

    goto :goto_1a

    :cond_2d
    const/4 v1, 0x0

    goto :goto_1b

    :cond_2e
    :goto_1a
    const/4 v1, 0x1

    :goto_1b
    iput-boolean v1, v3, Lf/h/a/c/a1/e0/m$a;->f:Z

    goto :goto_1c

    :cond_2f
    move/from16 v2, v32

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->g:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e0/q;->d(I)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->h:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e0/q;->d(I)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->i:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e0/q;->d(I)V

    :goto_1c
    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->j:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e0/q;->d(I)V

    iget-object v1, v0, Lf/h/a/c/a1/e0/m;->k:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e0/q;->d(I)V

    move-object/from16 v1, p1

    move/from16 v3, v28

    move-object/from16 v4, v29

    move/from16 v2, v30

    goto/16 :goto_0

    :cond_30
    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->f:[Z

    invoke-static {v0}, Lf/h/a/c/i1/p;->a([Z)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->g:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->h:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->i:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->j:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->k:Lf/h/a/c/a1/e0/q;

    invoke-virtual {v0}, Lf/h/a/c/a1/e0/q;->c()V

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->d:Lf/h/a/c/a1/e0/m$a;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/m$a;->f:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/m$a;->g:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/m$a;->h:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/m$a;->i:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/e0/m$a;->j:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lf/h/a/c/a1/e0/m;->l:J

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 2

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/m;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result v0

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/m;->c:Lf/h/a/c/a1/s;

    new-instance v1, Lf/h/a/c/a1/e0/m$a;

    invoke-direct {v1, v0}, Lf/h/a/c/a1/e0/m$a;-><init>(Lf/h/a/c/a1/s;)V

    iput-object v1, p0, Lf/h/a/c/a1/e0/m;->d:Lf/h/a/c/a1/e0/m$a;

    iget-object v0, p0, Lf/h/a/c/a1/e0/m;->a:Lf/h/a/c/a1/e0/x;

    invoke-virtual {v0, p1, p2}, Lf/h/a/c/a1/e0/x;->a(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    return-void
.end method

.method public f(JI)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/a1/e0/m;->m:J

    return-void
.end method
