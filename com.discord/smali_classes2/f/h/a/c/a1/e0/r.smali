.class public final Lf/h/a/c/a1/e0/r;
.super Ljava/lang/Object;
.source "PesReader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/c0;


# instance fields
.field public final a:Lf/h/a/c/a1/e0/j;

.field public final b:Lf/h/a/c/i1/q;

.field public c:I

.field public d:I

.field public e:Lf/h/a/c/i1/z;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:I

.field public j:I

.field public k:Z

.field public l:J


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/e0/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/r;->a:Lf/h/a/c/a1/e0/j;

    new-instance p1, Lf/h/a/c/i1/q;

    const/16 v0, 0xa

    new-array v0, v0, [B

    invoke-direct {p1, v0}, Lf/h/a/c/i1/q;-><init>([B)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/a1/e0/r;->c:I

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/i1/z;Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/e0/r;->e:Lf/h/a/c/i1/z;

    iget-object p1, p0, Lf/h/a/c/a1/e0/r;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {p1, p2, p3}, Lf/h/a/c/a1/e0/j;->e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    return-void
.end method

.method public final b(Lf/h/a/c/i1/r;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    and-int/lit8 v0, p2, 0x1

    const-string v1, "PesReader"

    const/4 v2, -0x1

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eqz v0, :cond_4

    iget v0, p0, Lf/h/a/c/a1/e0/r;->c:I

    if-eqz v0, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v4, :cond_2

    if-ne v0, v3, :cond_1

    iget v0, p0, Lf/h/a/c/a1/e0/r;->j:I

    if-eq v0, v2, :cond_0

    const-string v0, "Unexpected start indicator: expected "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, p0, Lf/h/a/c/a1/e0/r;->j:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " more bytes"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {v0}, Lf/h/a/c/a1/e0/j;->d()V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_2
    const-string v0, "Unexpected start indicator reading extended header"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_0
    invoke-virtual {p0, v5}, Lf/h/a/c/a1/e0/r;->e(I)V

    :cond_4
    :goto_1
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    if-lez v0, :cond_12

    iget v0, p0, Lf/h/a/c/a1/e0/r;->c:I

    if-eqz v0, :cond_10

    const/4 v6, 0x0

    if-eq v0, v5, :cond_c

    if-eq v0, v4, :cond_8

    if-ne v0, v3, :cond_7

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    iget v3, p0, Lf/h/a/c/a1/e0/r;->j:I

    if-ne v3, v2, :cond_5

    goto :goto_2

    :cond_5
    sub-int v6, v0, v3

    :goto_2
    if-lez v6, :cond_6

    sub-int/2addr v0, v6

    iget v3, p1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v3, v0

    invoke-virtual {p1, v3}, Lf/h/a/c/i1/r;->B(I)V

    :cond_6
    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {v3, p1}, Lf/h/a/c/a1/e0/j;->b(Lf/h/a/c/i1/r;)V

    iget v3, p0, Lf/h/a/c/a1/e0/r;->j:I

    if-eq v3, v2, :cond_11

    sub-int/2addr v3, v0

    iput v3, p0, Lf/h/a/c/a1/e0/r;->j:I

    if-nez v3, :cond_11

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {v0}, Lf/h/a/c/a1/e0/j;->d()V

    invoke-virtual {p0, v5}, Lf/h/a/c/a1/e0/r;->e(I)V

    goto/16 :goto_7

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_8
    const/16 v0, 0xa

    iget v2, p0, Lf/h/a/c/a1/e0/r;->i:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v2, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    iget-object v2, v2, Lf/h/a/c/i1/q;->a:[B

    invoke-virtual {p0, p1, v2, v0}, Lf/h/a/c/a1/e0/r;->d(Lf/h/a/c/i1/r;[BI)Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x0

    iget v2, p0, Lf/h/a/c/a1/e0/r;->i:I

    invoke-virtual {p0, p1, v0, v2}, Lf/h/a/c/a1/e0/r;->d(Lf/h/a/c/i1/r;[BI)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v6}, Lf/h/a/c/i1/q;->j(I)V

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v6, p0, Lf/h/a/c/a1/e0/r;->l:J

    iget-boolean v0, p0, Lf/h/a/c/a1/e0/r;->f:Z

    const/4 v2, 0x4

    if-eqz v0, :cond_a

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    int-to-long v6, v0

    const/16 v0, 0x1e

    shl-long/2addr v6, v0

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/16 v8, 0xf

    invoke-virtual {v0, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    shl-int/2addr v0, v8

    int-to-long v9, v0

    or-long/2addr v6, v9

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    int-to-long v9, v0

    or-long/2addr v6, v9

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-boolean v0, p0, Lf/h/a/c/a1/e0/r;->h:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lf/h/a/c/a1/e0/r;->g:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    int-to-long v2, v0

    const/16 v0, 0x1e

    shl-long/2addr v2, v0

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    shl-int/2addr v0, v8

    int-to-long v9, v0

    or-long/2addr v2, v9

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    int-to-long v8, v0

    or-long/2addr v2, v8

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v5}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->e:Lf/h/a/c/i1/z;

    invoke-virtual {v0, v2, v3}, Lf/h/a/c/i1/z;->b(J)J

    iput-boolean v5, p0, Lf/h/a/c/a1/e0/r;->h:Z

    :cond_9
    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->e:Lf/h/a/c/i1/z;

    invoke-virtual {v0, v6, v7}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v2

    iput-wide v2, p0, Lf/h/a/c/a1/e0/r;->l:J

    :cond_a
    iget-boolean v0, p0, Lf/h/a/c/a1/e0/r;->k:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x4

    goto :goto_3

    :cond_b
    const/4 v0, 0x0

    :goto_3
    or-int/2addr p2, v0

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->a:Lf/h/a/c/a1/e0/j;

    iget-wide v2, p0, Lf/h/a/c/a1/e0/r;->l:J

    invoke-interface {v0, v2, v3, p2}, Lf/h/a/c/a1/e0/j;->f(JI)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lf/h/a/c/a1/e0/r;->e(I)V

    goto/16 :goto_7

    :cond_c
    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    iget-object v0, v0, Lf/h/a/c/i1/q;->a:[B

    const/16 v2, 0x9

    invoke-virtual {p0, p1, v0, v2}, Lf/h/a/c/a1/e0/r;->d(Lf/h/a/c/i1/r;[BI)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/16 v2, 0x18

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    if-eq v0, v5, :cond_d

    const-string v2, "Unexpected start code prefix: "

    invoke-static {v2, v0, v1}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p0, Lf/h/a/c/a1/e0/r;->j:I

    const/4 v0, 0x0

    goto :goto_5

    :cond_d
    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/4 v6, 0x5

    invoke-virtual {v3, v6}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v3}, Lf/h/a/c/i1/q;->e()Z

    move-result v3

    iput-boolean v3, p0, Lf/h/a/c/a1/e0/r;->k:Z

    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v3, v4}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v3}, Lf/h/a/c/i1/q;->e()Z

    move-result v3

    iput-boolean v3, p0, Lf/h/a/c/a1/e0/r;->f:Z

    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v3}, Lf/h/a/c/i1/q;->e()Z

    move-result v3

    iput-boolean v3, p0, Lf/h/a/c/a1/e0/r;->g:Z

    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    const/4 v6, 0x6

    invoke-virtual {v3, v6}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v3, p0, Lf/h/a/c/a1/e0/r;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v3, v2}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iput v2, p0, Lf/h/a/c/a1/e0/r;->i:I

    if-nez v0, :cond_e

    const/4 v0, -0x1

    iput v0, p0, Lf/h/a/c/a1/e0/r;->j:I

    goto :goto_4

    :cond_e
    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x9

    sub-int/2addr v0, v2

    iput v0, p0, Lf/h/a/c/a1/e0/r;->j:I

    :goto_4
    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_f

    const/4 v0, 0x2

    goto :goto_6

    :cond_f
    const/4 v0, 0x0

    :goto_6
    invoke-virtual {p0, v0}, Lf/h/a/c/a1/e0/r;->e(I)V

    goto :goto_7

    :cond_10
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lf/h/a/c/i1/r;->D(I)V

    :cond_11
    :goto_7
    const/4 v2, -0x1

    const/4 v3, 0x3

    goto/16 :goto_1

    :cond_12
    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/r;->c:I

    iput v0, p0, Lf/h/a/c/a1/e0/r;->d:I

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/r;->h:Z

    iget-object v0, p0, Lf/h/a/c/a1/e0/r;->a:Lf/h/a/c/a1/e0/j;

    invoke-interface {v0}, Lf/h/a/c/a1/e0/j;->c()V

    return-void
.end method

.method public final d(Lf/h/a/c/i1/r;[BI)Z
    .locals 5

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    iget v1, p0, Lf/h/a/c/a1/e0/r;->d:I

    sub-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x1

    if-gtz v0, :cond_0

    return v1

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {p1, v0}, Lf/h/a/c/i1/r;->D(I)V

    goto :goto_0

    :cond_1
    iget v2, p0, Lf/h/a/c/a1/e0/r;->d:I

    iget-object v3, p1, Lf/h/a/c/i1/r;->a:[B

    iget v4, p1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v3, v4, p2, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p2, p1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr p2, v0

    iput p2, p1, Lf/h/a/c/i1/r;->b:I

    :goto_0
    iget p1, p0, Lf/h/a/c/a1/e0/r;->d:I

    add-int/2addr p1, v0

    iput p1, p0, Lf/h/a/c/a1/e0/r;->d:I

    if-ne p1, p3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final e(I)V
    .locals 0

    iput p1, p0, Lf/h/a/c/a1/e0/r;->c:I

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/a1/e0/r;->d:I

    return-void
.end method
