.class public final Lf/h/a/c/a1/e0/y;
.super Ljava/lang/Object;
.source "SpliceInfoSectionReader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/v;


# instance fields
.field public a:Lf/h/a/c/i1/z;

.field public b:Lf/h/a/c/a1/s;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/i1/z;Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 2

    iput-object p1, p0, Lf/h/a/c/a1/e0/y;->a:Lf/h/a/c/i1/z;

    invoke-virtual {p3}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p3}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result p1

    const/4 v0, 0x4

    invoke-interface {p2, p1, v0}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/e0/y;->b:Lf/h/a/c/a1/s;

    invoke-virtual {p3}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object p2

    const-string p3, "application/x-scte35"

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {p2, p3, v0, v1, v0}, Lcom/google/android/exoplayer2/Format;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object p2

    invoke-interface {p1, p2}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    return-void
.end method

.method public b(Lf/h/a/c/i1/r;)V
    .locals 10

    iget-boolean v0, p0, Lf/h/a/c/a1/e0/y;->c:Z

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/a/c/a1/e0/y;->a:Lf/h/a/c/i1/z;

    invoke-virtual {v0}, Lf/h/a/c/i1/z;->c()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a1/e0/y;->b:Lf/h/a/c/a1/s;

    const/4 v3, 0x0

    iget-object v4, p0, Lf/h/a/c/a1/e0/y;->a:Lf/h/a/c/i1/z;

    invoke-virtual {v4}, Lf/h/a/c/i1/z;->c()J

    move-result-wide v4

    const-string v6, "application/x-scte35"

    invoke-static {v3, v6, v4, v5}, Lcom/google/android/exoplayer2/Format;->i(Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    invoke-interface {v0, v3}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/y;->c:Z

    :cond_1
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v7

    iget-object v0, p0, Lf/h/a/c/a1/e0/y;->b:Lf/h/a/c/a1/s;

    invoke-interface {v0, p1, v7}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget-object v3, p0, Lf/h/a/c/a1/e0/y;->b:Lf/h/a/c/a1/s;

    iget-object p1, p0, Lf/h/a/c/a1/e0/y;->a:Lf/h/a/c/i1/z;

    iget-wide v4, p1, Lf/h/a/c/i1/z;->c:J

    cmp-long v0, v4, v1

    if-eqz v0, :cond_2

    iget-wide v0, p1, Lf/h/a/c/i1/z;->c:J

    iget-wide v4, p1, Lf/h/a/c/i1/z;->b:J

    add-long v1, v4, v0

    goto :goto_0

    :cond_2
    iget-wide v4, p1, Lf/h/a/c/i1/z;->a:J

    const-wide v8, 0x7fffffffffffffffL

    cmp-long p1, v4, v8

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    :goto_0
    move-wide v4, v1

    :goto_1
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    return-void
.end method
