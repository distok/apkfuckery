.class public final Lf/h/a/c/a1/e0/l$b;
.super Ljava/lang/Object;
.source "H264Reader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/e0/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/e0/l$b$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/s;

.field public final b:Z

.field public final c:Z

.field public final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/i1/p$b;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/i1/p$a;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lf/h/a/c/i1/s;

.field public g:[B

.field public h:I

.field public i:I

.field public j:J

.field public k:Z

.field public l:J

.field public m:Lf/h/a/c/a1/e0/l$b$a;

.field public n:Lf/h/a/c/a1/e0/l$b$a;

.field public o:Z

.field public p:J

.field public q:J

.field public r:Z


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/s;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l$b;->a:Lf/h/a/c/a1/s;

    iput-boolean p2, p0, Lf/h/a/c/a1/e0/l$b;->b:Z

    iput-boolean p3, p0, Lf/h/a/c/a1/e0/l$b;->c:Z

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l$b;->d:Landroid/util/SparseArray;

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l$b;->e:Landroid/util/SparseArray;

    new-instance p1, Lf/h/a/c/a1/e0/l$b$a;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lf/h/a/c/a1/e0/l$b$a;-><init>(Lf/h/a/c/a1/e0/l$a;)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l$b;->m:Lf/h/a/c/a1/e0/l$b$a;

    new-instance p1, Lf/h/a/c/a1/e0/l$b$a;

    invoke-direct {p1, p2}, Lf/h/a/c/a1/e0/l$b$a;-><init>(Lf/h/a/c/a1/e0/l$a;)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    const/16 p1, 0x80

    new-array p1, p1, [B

    iput-object p1, p0, Lf/h/a/c/a1/e0/l$b;->g:[B

    new-instance p2, Lf/h/a/c/i1/s;

    const/4 p3, 0x0

    invoke-direct {p2, p1, p3, p3}, Lf/h/a/c/i1/s;-><init>([BII)V

    iput-object p2, p0, Lf/h/a/c/a1/e0/l$b;->f:Lf/h/a/c/i1/s;

    iput-boolean p3, p0, Lf/h/a/c/a1/e0/l$b;->k:Z

    iput-boolean p3, p0, Lf/h/a/c/a1/e0/l$b;->o:Z

    iget-object p1, p0, Lf/h/a/c/a1/e0/l$b;->n:Lf/h/a/c/a1/e0/l$b$a;

    iput-boolean p3, p1, Lf/h/a/c/a1/e0/l$b$a;->b:Z

    iput-boolean p3, p1, Lf/h/a/c/a1/e0/l$b$a;->a:Z

    return-void
.end method
