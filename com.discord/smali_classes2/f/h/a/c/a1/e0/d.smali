.class public final Lf/h/a/c/a1/e0/d;
.super Ljava/lang/Object;
.source "Ac4Reader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/j;


# instance fields
.field public final a:Lf/h/a/c/i1/q;

.field public final b:Lf/h/a/c/i1/r;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lf/h/a/c/a1/s;

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field public j:J

.field public k:Lcom/google/android/exoplayer2/Format;

.field public l:I

.field public m:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/q;

    const/16 v1, 0x10

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lf/h/a/c/i1/q;-><init>([B)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/d;->a:Lf/h/a/c/i1/q;

    new-instance v1, Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/q;->a:[B

    invoke-direct {v1, v0}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object v1, p0, Lf/h/a/c/a1/e0/d;->b:Lf/h/a/c/i1/r;

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/d;->f:I

    iput v0, p0, Lf/h/a/c/a1/e0/d;->g:I

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/d;->h:Z

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/d;->i:Z

    iput-object p1, p0, Lf/h/a/c/a1/e0/d;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public b(Lf/h/a/c/i1/r;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-lez v2, :cond_e

    iget v2, v0, Lf/h/a/c/a1/e0/d;->f:I

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v2, :cond_6

    if-eq v2, v5, :cond_2

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    iget v3, v0, Lf/h/a/c/a1/e0/d;->l:I

    iget v5, v0, Lf/h/a/c/a1/e0/d;->g:I

    sub-int/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/d;->e:Lf/h/a/c/a1/s;

    invoke-interface {v3, v1, v2}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v3, v0, Lf/h/a/c/a1/e0/d;->g:I

    add-int/2addr v3, v2

    iput v3, v0, Lf/h/a/c/a1/e0/d;->g:I

    iget v9, v0, Lf/h/a/c/a1/e0/d;->l:I

    if-ne v3, v9, :cond_0

    iget-object v5, v0, Lf/h/a/c/a1/e0/d;->e:Lf/h/a/c/a1/s;

    iget-wide v6, v0, Lf/h/a/c/a1/e0/d;->m:J

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-interface/range {v5 .. v11}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iget-wide v2, v0, Lf/h/a/c/a1/e0/d;->m:J

    iget-wide v5, v0, Lf/h/a/c/a1/e0/d;->j:J

    add-long/2addr v2, v5

    iput-wide v2, v0, Lf/h/a/c/a1/e0/d;->m:J

    iput v4, v0, Lf/h/a/c/a1/e0/d;->f:I

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lf/h/a/c/a1/e0/d;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v6

    iget v7, v0, Lf/h/a/c/a1/e0/d;->g:I

    const/16 v8, 0x10

    rsub-int/lit8 v7, v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget v7, v0, Lf/h/a/c/a1/e0/d;->g:I

    iget-object v9, v1, Lf/h/a/c/i1/r;->a:[B

    iget v10, v1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v9, v10, v2, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v2, v6

    iput v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v2, v0, Lf/h/a/c/a1/e0/d;->g:I

    add-int/2addr v2, v6

    iput v2, v0, Lf/h/a/c/a1/e0/d;->g:I

    if-ne v2, v8, :cond_3

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    iget-object v2, v0, Lf/h/a/c/a1/e0/d;->a:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/d;->a:Lf/h/a/c/i1/q;

    invoke-static {v2}, Lf/h/a/c/w0/h;->b(Lf/h/a/c/i1/q;)Lf/h/a/c/w0/h$b;

    move-result-object v2

    iget-object v5, v0, Lf/h/a/c/a1/e0/d;->k:Lcom/google/android/exoplayer2/Format;

    if-eqz v5, :cond_4

    iget v6, v5, Lcom/google/android/exoplayer2/Format;->y:I

    if-ne v3, v6, :cond_4

    iget v6, v2, Lf/h/a/c/w0/h$b;->a:I

    iget v7, v5, Lcom/google/android/exoplayer2/Format;->z:I

    if-ne v6, v7, :cond_4

    iget-object v5, v5, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v6, "audio/ac4"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    iget-object v9, v0, Lf/h/a/c/a1/e0/d;->d:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, 0x2

    iget v15, v2, Lf/h/a/c/w0/h$b;->a:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    iget-object v5, v0, Lf/h/a/c/a1/e0/d;->c:Ljava/lang/String;

    const-string v10, "audio/ac4"

    move-object/from16 v19, v5

    invoke-static/range {v9 .. v19}, Lcom/google/android/exoplayer2/Format;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v5

    iput-object v5, v0, Lf/h/a/c/a1/e0/d;->k:Lcom/google/android/exoplayer2/Format;

    iget-object v6, v0, Lf/h/a/c/a1/e0/d;->e:Lf/h/a/c/a1/s;

    invoke-interface {v6, v5}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    :cond_5
    iget v5, v2, Lf/h/a/c/w0/h$b;->b:I

    iput v5, v0, Lf/h/a/c/a1/e0/d;->l:I

    const-wide/32 v5, 0xf4240

    iget v2, v2, Lf/h/a/c/w0/h$b;->c:I

    int-to-long v9, v2

    mul-long v9, v9, v5

    iget-object v2, v0, Lf/h/a/c/a1/e0/d;->k:Lcom/google/android/exoplayer2/Format;

    iget v2, v2, Lcom/google/android/exoplayer2/Format;->z:I

    int-to-long v5, v2

    div-long/2addr v9, v5

    iput-wide v9, v0, Lf/h/a/c/a1/e0/d;->j:J

    iget-object v2, v0, Lf/h/a/c/a1/e0/d;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/d;->e:Lf/h/a/c/a1/s;

    iget-object v4, v0, Lf/h/a/c/a1/e0/d;->b:Lf/h/a/c/i1/r;

    invoke-interface {v2, v4, v8}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iput v3, v0, Lf/h/a/c/a1/e0/d;->f:I

    goto/16 :goto_0

    :cond_6
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    const/16 v6, 0x41

    const/16 v7, 0x40

    if-lez v2, :cond_c

    iget-boolean v2, v0, Lf/h/a/c/a1/e0/d;->h:Z

    const/16 v8, 0xac

    if-nez v2, :cond_8

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    if-ne v2, v8, :cond_7

    const/4 v2, 0x1

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    :goto_3
    iput-boolean v2, v0, Lf/h/a/c/a1/e0/d;->h:Z

    goto :goto_2

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    if-ne v2, v8, :cond_9

    const/4 v8, 0x1

    goto :goto_4

    :cond_9
    const/4 v8, 0x0

    :goto_4
    iput-boolean v8, v0, Lf/h/a/c/a1/e0/d;->h:Z

    if-eq v2, v7, :cond_a

    if-ne v2, v6, :cond_6

    :cond_a
    if-ne v2, v6, :cond_b

    const/4 v2, 0x1

    goto :goto_5

    :cond_b
    const/4 v2, 0x0

    :goto_5
    iput-boolean v2, v0, Lf/h/a/c/a1/e0/d;->i:Z

    const/4 v2, 0x1

    goto :goto_6

    :cond_c
    const/4 v2, 0x0

    :goto_6
    if-eqz v2, :cond_0

    iput v5, v0, Lf/h/a/c/a1/e0/d;->f:I

    iget-object v2, v0, Lf/h/a/c/a1/e0/d;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    const/16 v8, -0x54

    aput-byte v8, v2, v4

    iget-boolean v4, v0, Lf/h/a/c/a1/e0/d;->i:Z

    if-eqz v4, :cond_d

    goto :goto_7

    :cond_d
    const/16 v6, 0x40

    :goto_7
    int-to-byte v4, v6

    aput-byte v4, v2, v5

    iput v3, v0, Lf/h/a/c/a1/e0/d;->g:I

    goto/16 :goto_0

    :cond_e
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/d;->f:I

    iput v0, p0, Lf/h/a/c/a1/e0/d;->g:I

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/d;->h:Z

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/d;->i:Z

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 1

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/d;->d:Ljava/lang/String;

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result p2

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/e0/d;->e:Lf/h/a/c/a1/s;

    return-void
.end method

.method public f(JI)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/a1/e0/d;->m:J

    return-void
.end method
