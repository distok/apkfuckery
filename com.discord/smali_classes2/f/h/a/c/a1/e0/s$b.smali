.class public final Lf/h/a/c/a1/e0/s$b;
.super Ljava/lang/Object;
.source "PsBinarySearchSeeker.java"

# interfaces
.implements Lf/h/a/c/a1/a$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/e0/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/i1/z;

.field public final b:Lf/h/a/c/i1/r;


# direct methods
.method public constructor <init>(Lf/h/a/c/i1/z;Lf/h/a/c/a1/e0/s$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/s$b;->a:Lf/h/a/c/i1/z;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/s$b;->b:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;J)Lf/h/a/c/a1/a$e;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-wide v2, v1, Lf/h/a/c/a1/e;->d:J

    iget-wide v4, v1, Lf/h/a/c/a1/e;->c:J

    sub-long/2addr v4, v2

    const-wide/16 v6, 0x4e20

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v5, v4

    iget-object v4, v0, Lf/h/a/c/a1/e0/s$b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v4, v0, Lf/h/a/c/a1/e0/s$b;->b:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6, v5, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v1, v0, Lf/h/a/c/a1/e0/s$b;->b:Lf/h/a/c/i1/r;

    const/4 v4, -0x1

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    move-wide v8, v5

    const/4 v7, -0x1

    :goto_0
    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    const/4 v11, 0x4

    if-lt v10, v11, :cond_e

    iget-object v10, v1, Lf/h/a/c/i1/r;->a:[B

    iget v12, v1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v10, v12}, Lf/h/a/c/a1/e0/s;->g([BI)I

    move-result v10

    const/4 v12, 0x1

    const/16 v13, 0x1ba

    if-eq v10, v13, :cond_0

    invoke-virtual {v1, v12}, Lf/h/a/c/i1/r;->D(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v11}, Lf/h/a/c/i1/r;->D(I)V

    invoke-static {v1}, Lf/h/a/c/a1/e0/t;->c(Lf/h/a/c/i1/r;)J

    move-result-wide v14

    cmp-long v4, v14, v5

    if-eqz v4, :cond_4

    iget-object v4, v0, Lf/h/a/c/a1/e0/s$b;->a:Lf/h/a/c/i1/z;

    invoke-virtual {v4, v14, v15}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v14

    cmp-long v4, v14, p2

    if-lez v4, :cond_2

    cmp-long v1, v8, v5

    if-nez v1, :cond_1

    invoke-static {v14, v15, v2, v3}, Lf/h/a/c/a1/a$e;->a(JJ)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto/16 :goto_3

    :cond_1
    int-to-long v4, v7

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lf/h/a/c/a1/a$e;->b(J)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto/16 :goto_3

    :cond_2
    const-wide/32 v7, 0x186a0

    add-long/2addr v7, v14

    cmp-long v4, v7, p2

    if-lez v4, :cond_3

    iget v1, v1, Lf/h/a/c/i1/r;->b:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lf/h/a/c/a1/a$e;->b(J)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto/16 :goto_3

    :cond_3
    iget v4, v1, Lf/h/a/c/i1/r;->b:I

    move v7, v4

    move-wide v8, v14

    :cond_4
    iget v4, v1, Lf/h/a/c/i1/r;->c:I

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    const/16 v14, 0xa

    if-ge v10, v14, :cond_5

    invoke-virtual {v1, v4}, Lf/h/a/c/i1/r;->C(I)V

    goto/16 :goto_2

    :cond_5
    const/16 v10, 0x9

    invoke-virtual {v1, v10}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->q()I

    move-result v10

    and-int/lit8 v10, v10, 0x7

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v14

    if-ge v14, v10, :cond_6

    invoke-virtual {v1, v4}, Lf/h/a/c/i1/r;->C(I)V

    goto :goto_2

    :cond_6
    invoke-virtual {v1, v10}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    if-ge v10, v11, :cond_7

    invoke-virtual {v1, v4}, Lf/h/a/c/i1/r;->C(I)V

    goto :goto_2

    :cond_7
    iget-object v10, v1, Lf/h/a/c/i1/r;->a:[B

    iget v14, v1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v10, v14}, Lf/h/a/c/a1/e0/s;->g([BI)I

    move-result v10

    const/16 v14, 0x1bb

    if-ne v10, v14, :cond_9

    invoke-virtual {v1, v11}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->v()I

    move-result v10

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v14

    if-ge v14, v10, :cond_8

    invoke-virtual {v1, v4}, Lf/h/a/c/i1/r;->C(I)V

    goto :goto_2

    :cond_8
    invoke-virtual {v1, v10}, Lf/h/a/c/i1/r;->D(I)V

    :cond_9
    :goto_1
    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    if-lt v10, v11, :cond_d

    iget-object v10, v1, Lf/h/a/c/i1/r;->a:[B

    iget v14, v1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v10, v14}, Lf/h/a/c/a1/e0/s;->g([BI)I

    move-result v10

    if-eq v10, v13, :cond_d

    const/16 v14, 0x1b9

    if-ne v10, v14, :cond_a

    goto :goto_2

    :cond_a
    ushr-int/lit8 v10, v10, 0x8

    if-eq v10, v12, :cond_b

    goto :goto_2

    :cond_b
    invoke-virtual {v1, v11}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    const/4 v14, 0x2

    if-ge v10, v14, :cond_c

    invoke-virtual {v1, v4}, Lf/h/a/c/i1/r;->C(I)V

    goto :goto_2

    :cond_c
    invoke-virtual {v1}, Lf/h/a/c/i1/r;->v()I

    move-result v10

    iget v14, v1, Lf/h/a/c/i1/r;->c:I

    iget v15, v1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v15, v10

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-virtual {v1, v10}, Lf/h/a/c/i1/r;->C(I)V

    goto :goto_1

    :cond_d
    :goto_2
    iget v4, v1, Lf/h/a/c/i1/r;->b:I

    goto/16 :goto_0

    :cond_e
    cmp-long v1, v8, v5

    if-eqz v1, :cond_f

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-static {v8, v9, v2, v3}, Lf/h/a/c/a1/a$e;->c(JJ)Lf/h/a/c/a1/a$e;

    move-result-object v1

    goto :goto_3

    :cond_f
    sget-object v1, Lf/h/a/c/a1/a$e;->d:Lf/h/a/c/a1/a$e;

    :goto_3
    return-object v1
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/e0/s$b;->b:Lf/h/a/c/i1/r;

    sget-object v1, Lf/h/a/c/i1/a0;->f:[B

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->z([B)V

    return-void
.end method
