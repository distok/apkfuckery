.class public final Lf/h/a/c/a1/e0/b0;
.super Ljava/lang/Object;
.source "TsExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/e0/b0$b;,
        Lf/h/a/c/a1/e0/b0$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/c/i1/z;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/c/i1/r;

.field public final d:Landroid/util/SparseIntArray;

.field public final e:Lf/h/a/c/a1/e0/c0$c;

.field public final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/a1/e0/c0;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/util/SparseBooleanArray;

.field public final h:Landroid/util/SparseBooleanArray;

.field public final i:Lf/h/a/c/a1/e0/a0;

.field public j:Lf/h/a/c/a1/e0/z;

.field public k:Lf/h/a/c/a1/i;

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Lf/h/a/c/a1/e0/c0;

.field public q:I

.field public r:I


# direct methods
.method public constructor <init>(II)V
    .locals 5

    new-instance v0, Lf/h/a/c/i1/z;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/a/c/i1/z;-><init>(J)V

    new-instance v1, Lf/h/a/c/a1/e0/g;

    invoke-direct {v1, p2}, Lf/h/a/c/a1/e0/g;-><init>(I)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lf/h/a/c/a1/e0/b0;->e:Lf/h/a/c/a1/e0/c0$c;

    iput p1, p0, Lf/h/a/c/a1/e0/b0;->a:I

    const/4 p2, 0x1

    if-eq p1, p2, :cond_1

    const/4 p2, 0x2

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0;->b:Ljava/util/List;

    :goto_1
    new-instance p1, Lf/h/a/c/i1/r;

    const/16 p2, 0x24b8

    new-array p2, p2, [B

    const/4 v0, 0x0

    invoke-direct {p1, p2, v0}, Lf/h/a/c/i1/r;-><init>([BI)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    new-instance p1, Landroid/util/SparseBooleanArray;

    invoke-direct {p1}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0;->g:Landroid/util/SparseBooleanArray;

    new-instance p2, Landroid/util/SparseBooleanArray;

    invoke-direct {p2}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object p2, p0, Lf/h/a/c/a1/e0/b0;->h:Landroid/util/SparseBooleanArray;

    new-instance p2, Landroid/util/SparseArray;

    invoke-direct {p2}, Landroid/util/SparseArray;-><init>()V

    iput-object p2, p0, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v2, p0, Lf/h/a/c/a1/e0/b0;->d:Landroid/util/SparseIntArray;

    new-instance v2, Lf/h/a/c/a1/e0/a0;

    invoke-direct {v2}, Lf/h/a/c/a1/e0/a0;-><init>()V

    iput-object v2, p0, Lf/h/a/c/a1/e0/b0;->i:Lf/h/a/c/a1/e0/a0;

    const/4 v2, -0x1

    iput v2, p0, Lf/h/a/c/a1/e0/b0;->r:I

    invoke-virtual {p1}, Landroid/util/SparseBooleanArray;->clear()V

    invoke-virtual {p2}, Landroid/util/SparseArray;->clear()V

    invoke-virtual {v1}, Lf/h/a/c/a1/e0/g;->b()Landroid/util/SparseArray;

    move-result-object p1

    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result p2

    const/4 v1, 0x0

    :goto_2
    if-ge v1, p2, :cond_2

    iget-object v2, p0, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    new-instance p2, Lf/h/a/c/a1/e0/w;

    new-instance v1, Lf/h/a/c/a1/e0/b0$a;

    invoke-direct {v1, p0}, Lf/h/a/c/a1/e0/b0$a;-><init>(Lf/h/a/c/a1/e0/b0;)V

    invoke-direct {p2, v1}, Lf/h/a/c/a1/e0/w;-><init>(Lf/h/a/c/a1/e0/v;)V

    invoke-virtual {p1, v0, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0;->p:Lf/h/a/c/a1/e0/c0;

    return-void
.end method


# virtual methods
.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    iget-wide v10, v1, Lf/h/a/c/a1/e;->c:J

    iget-boolean v3, v0, Lf/h/a/c/a1/e0/b0;->m:Z

    const/16 v12, 0x47

    const-wide/16 v13, -0x1

    const/4 v15, -0x1

    const/4 v9, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v3, :cond_12

    cmp-long v3, v10, v13

    if-eqz v3, :cond_0

    iget v3, v0, Lf/h/a/c/a1/e0/b0;->a:I

    if-eq v3, v9, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v3, :cond_e

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0;->i:Lf/h/a/c/a1/e0/a0;

    iget-boolean v6, v3, Lf/h/a/c/a1/e0/a0;->c:Z

    if-nez v6, :cond_e

    iget v6, v0, Lf/h/a/c/a1/e0/b0;->r:I

    if-gtz v6, :cond_1

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/a0;->a(Lf/h/a/c/a1/e;)I

    goto/16 :goto_6

    :cond_1
    iget-boolean v9, v3, Lf/h/a/c/a1/e0/a0;->e:Z

    const-wide/32 v13, 0x1b8a0

    if-nez v9, :cond_6

    invoke-static {v13, v14, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v13

    long-to-int v9, v13

    int-to-long v13, v9

    sub-long/2addr v10, v13

    iget-wide v13, v1, Lf/h/a/c/a1/e;->d:J

    cmp-long v16, v13, v10

    if-eqz v16, :cond_2

    iput-wide v10, v2, Lf/h/a/c/a1/p;->a:J

    :goto_1
    const/4 v7, 0x1

    goto/16 :goto_6

    :cond_2
    iget-object v2, v3, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/r;->y(I)V

    iput v7, v1, Lf/h/a/c/a1/e;->f:I

    iget-object v2, v3, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v2, v7, v9, v7}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v1, v3, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v9, v1, Lf/h/a/c/i1/r;->c:I

    :cond_3
    :goto_2
    add-int/2addr v9, v15

    if-lt v9, v2, :cond_5

    iget-object v10, v1, Lf/h/a/c/i1/r;->a:[B

    aget-byte v10, v10, v9

    if-eq v10, v12, :cond_4

    goto :goto_2

    :cond_4
    invoke-static {v1, v9, v6}, Lf/g/j/k/a;->I0(Lf/h/a/c/i1/r;II)J

    move-result-wide v10

    cmp-long v13, v10, v4

    if-eqz v13, :cond_3

    move-wide v4, v10

    :cond_5
    iput-wide v4, v3, Lf/h/a/c/a1/e0/a0;->g:J

    iput-boolean v8, v3, Lf/h/a/c/a1/e0/a0;->e:Z

    goto/16 :goto_6

    :cond_6
    iget-wide v8, v3, Lf/h/a/c/a1/e0/a0;->g:J

    cmp-long v15, v8, v4

    if-nez v15, :cond_7

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/a0;->a(Lf/h/a/c/a1/e;)I

    goto :goto_6

    :cond_7
    iget-boolean v8, v3, Lf/h/a/c/a1/e0/a0;->d:Z

    if-nez v8, :cond_c

    invoke-static {v13, v14, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    long-to-int v9, v8

    iget-wide v10, v1, Lf/h/a/c/a1/e;->d:J

    int-to-long v13, v7

    cmp-long v8, v10, v13

    if-eqz v8, :cond_8

    iput-wide v13, v2, Lf/h/a/c/a1/p;->a:J

    goto :goto_1

    :cond_8
    iget-object v2, v3, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/r;->y(I)V

    iput v7, v1, Lf/h/a/c/a1/e;->f:I

    iget-object v2, v3, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v1, v2, v7, v9, v7}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v1, v3, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v8, v1, Lf/h/a/c/i1/r;->c:I

    :goto_3
    if-ge v2, v8, :cond_b

    iget-object v9, v1, Lf/h/a/c/i1/r;->a:[B

    aget-byte v9, v9, v2

    if-eq v9, v12, :cond_9

    goto :goto_4

    :cond_9
    invoke-static {v1, v2, v6}, Lf/g/j/k/a;->I0(Lf/h/a/c/i1/r;II)J

    move-result-wide v9

    cmp-long v11, v9, v4

    if-eqz v11, :cond_a

    move-wide v4, v9

    goto :goto_5

    :cond_a
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_b
    :goto_5
    iput-wide v4, v3, Lf/h/a/c/a1/e0/a0;->f:J

    const/4 v1, 0x1

    iput-boolean v1, v3, Lf/h/a/c/a1/e0/a0;->d:Z

    goto :goto_6

    :cond_c
    iget-wide v8, v3, Lf/h/a/c/a1/e0/a0;->f:J

    cmp-long v2, v8, v4

    if-nez v2, :cond_d

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/a0;->a(Lf/h/a/c/a1/e;)I

    goto :goto_6

    :cond_d
    iget-object v2, v3, Lf/h/a/c/a1/e0/a0;->a:Lf/h/a/c/i1/z;

    invoke-virtual {v2, v8, v9}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v4

    iget-object v2, v3, Lf/h/a/c/a1/e0/a0;->a:Lf/h/a/c/i1/z;

    iget-wide v8, v3, Lf/h/a/c/a1/e0/a0;->g:J

    invoke-virtual {v2, v8, v9}, Lf/h/a/c/i1/z;->b(J)J

    move-result-wide v8

    sub-long/2addr v8, v4

    iput-wide v8, v3, Lf/h/a/c/a1/e0/a0;->h:J

    invoke-virtual {v3, v1}, Lf/h/a/c/a1/e0/a0;->a(Lf/h/a/c/a1/e;)I

    :goto_6
    return v7

    :cond_e
    iget-boolean v3, v0, Lf/h/a/c/a1/e0/b0;->n:Z

    const-wide/16 v13, 0x0

    if-nez v3, :cond_10

    const/4 v8, 0x1

    iput-boolean v8, v0, Lf/h/a/c/a1/e0/b0;->n:Z

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0;->i:Lf/h/a/c/a1/e0/a0;

    iget-wide v7, v3, Lf/h/a/c/a1/e0/a0;->h:J

    cmp-long v6, v7, v4

    if-eqz v6, :cond_f

    new-instance v5, Lf/h/a/c/a1/e0/z;

    iget-object v4, v3, Lf/h/a/c/a1/e0/a0;->a:Lf/h/a/c/i1/z;

    iget v6, v0, Lf/h/a/c/a1/e0/b0;->r:I

    move-object v3, v5

    move-object v12, v5

    move/from16 v17, v6

    move-wide v5, v7

    const/4 v15, 0x0

    move-wide v7, v10

    move/from16 v9, v17

    invoke-direct/range {v3 .. v9}, Lf/h/a/c/a1/e0/z;-><init>(Lf/h/a/c/i1/z;JJI)V

    iput-object v12, v0, Lf/h/a/c/a1/e0/b0;->j:Lf/h/a/c/a1/e0/z;

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0;->k:Lf/h/a/c/a1/i;

    iget-object v4, v12, Lf/h/a/c/a1/a;->a:Lf/h/a/c/a1/a$a;

    invoke-interface {v3, v4}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    goto :goto_7

    :cond_f
    const/4 v15, 0x0

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0;->k:Lf/h/a/c/a1/i;

    new-instance v4, Lf/h/a/c/a1/q$b;

    invoke-direct {v4, v7, v8, v13, v14}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {v3, v4}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    goto :goto_7

    :cond_10
    const/4 v15, 0x0

    :goto_7
    iget-boolean v3, v0, Lf/h/a/c/a1/e0/b0;->o:Z

    if-eqz v3, :cond_11

    iput-boolean v15, v0, Lf/h/a/c/a1/e0/b0;->o:Z

    invoke-virtual {v0, v13, v14, v13, v14}, Lf/h/a/c/a1/e0/b0;->f(JJ)V

    iget-wide v3, v1, Lf/h/a/c/a1/e;->d:J

    cmp-long v5, v3, v13

    if-eqz v5, :cond_11

    iput-wide v13, v2, Lf/h/a/c/a1/p;->a:J

    const/4 v3, 0x1

    return v3

    :cond_11
    const/4 v3, 0x1

    iget-object v4, v0, Lf/h/a/c/a1/e0/b0;->j:Lf/h/a/c/a1/e0/z;

    if-eqz v4, :cond_13

    invoke-virtual {v4}, Lf/h/a/c/a1/a;->b()Z

    move-result v4

    if-eqz v4, :cond_13

    iget-object v3, v0, Lf/h/a/c/a1/e0/b0;->j:Lf/h/a/c/a1/e0/z;

    invoke-virtual {v3, v1, v2}, Lf/h/a/c/a1/a;->a(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I

    move-result v1

    return v1

    :cond_12
    const/4 v3, 0x1

    const/4 v15, 0x0

    :cond_13
    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    iget-object v4, v2, Lf/h/a/c/i1/r;->a:[B

    iget v5, v2, Lf/h/a/c/i1/r;->b:I

    rsub-int v5, v5, 0x24b8

    const/16 v6, 0xbc

    if-ge v5, v6, :cond_15

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-lez v2, :cond_14

    iget-object v5, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    iget v5, v5, Lf/h/a/c/i1/r;->b:I

    invoke-static {v4, v5, v4, v15, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_14
    iget-object v5, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v4, v2}, Lf/h/a/c/i1/r;->A([BI)V

    :cond_15
    :goto_8
    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-ge v2, v6, :cond_17

    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    iget v2, v2, Lf/h/a/c/i1/r;->c:I

    rsub-int v5, v2, 0x24b8

    invoke-virtual {v1, v4, v2, v5}, Lf/h/a/c/a1/e;->f([BII)I

    move-result v5

    const/4 v7, -0x1

    if-ne v5, v7, :cond_16

    const/4 v1, 0x0

    goto :goto_9

    :cond_16
    iget-object v8, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    add-int/2addr v2, v5

    invoke-virtual {v8, v2}, Lf/h/a/c/i1/r;->B(I)V

    goto :goto_8

    :cond_17
    const/4 v7, -0x1

    const/4 v1, 0x1

    :goto_9
    if-nez v1, :cond_18

    return v7

    :cond_18
    iget-object v1, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v4, v1, Lf/h/a/c/i1/r;->c:I

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    move v5, v2

    :goto_a
    if-ge v5, v4, :cond_19

    aget-byte v6, v1, v5

    const/16 v7, 0x47

    if-eq v6, v7, :cond_19

    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :cond_19
    iget-object v1, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v1, v5}, Lf/h/a/c/i1/r;->C(I)V

    add-int/lit16 v1, v5, 0xbc

    if-le v1, v4, :cond_1b

    iget v4, v0, Lf/h/a/c/a1/e0/b0;->q:I

    sub-int/2addr v5, v2

    add-int/2addr v5, v4

    iput v5, v0, Lf/h/a/c/a1/e0/b0;->q:I

    iget v2, v0, Lf/h/a/c/a1/e0/b0;->a:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1c

    const/16 v2, 0x178

    if-gt v5, v2, :cond_1a

    goto :goto_b

    :cond_1a
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Cannot find sync byte. Most likely not a Transport Stream."

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1b
    const/4 v4, 0x2

    iput v15, v0, Lf/h/a/c/a1/e0/b0;->q:I

    :cond_1c
    :goto_b
    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    iget v5, v2, Lf/h/a/c/i1/r;->c:I

    if-le v1, v5, :cond_1d

    return v15

    :cond_1d
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v2

    const/high16 v6, 0x800000

    and-int/2addr v6, v2

    if-eqz v6, :cond_1e

    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v1}, Lf/h/a/c/i1/r;->C(I)V

    return v15

    :cond_1e
    const/high16 v6, 0x400000

    and-int/2addr v6, v2

    if-eqz v6, :cond_1f

    const/4 v7, 0x1

    goto :goto_c

    :cond_1f
    const/4 v7, 0x0

    :goto_c
    or-int/lit8 v6, v7, 0x0

    const v7, 0x1fff00

    and-int/2addr v7, v2

    shr-int/lit8 v7, v7, 0x8

    and-int/lit8 v8, v2, 0x20

    if-eqz v8, :cond_20

    const/4 v8, 0x1

    goto :goto_d

    :cond_20
    const/4 v8, 0x0

    :goto_d
    and-int/lit8 v9, v2, 0x10

    if-eqz v9, :cond_21

    const/4 v9, 0x1

    goto :goto_e

    :cond_21
    const/4 v9, 0x0

    :goto_e
    if-eqz v9, :cond_22

    iget-object v9, v0, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    invoke-virtual {v9, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/a/c/a1/e0/c0;

    goto :goto_f

    :cond_22
    const/4 v9, 0x0

    :goto_f
    if-nez v9, :cond_23

    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v1}, Lf/h/a/c/i1/r;->C(I)V

    return v15

    :cond_23
    iget v12, v0, Lf/h/a/c/a1/e0/b0;->a:I

    if-eq v12, v4, :cond_25

    and-int/lit8 v2, v2, 0xf

    iget-object v12, v0, Lf/h/a/c/a1/e0/b0;->d:Landroid/util/SparseIntArray;

    add-int/lit8 v13, v2, -0x1

    invoke-virtual {v12, v7, v13}, Landroid/util/SparseIntArray;->get(II)I

    move-result v12

    iget-object v13, v0, Lf/h/a/c/a1/e0/b0;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v13, v7, v2}, Landroid/util/SparseIntArray;->put(II)V

    if-ne v12, v2, :cond_24

    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v1}, Lf/h/a/c/i1/r;->C(I)V

    return v15

    :cond_24
    add-int/2addr v12, v3

    and-int/lit8 v12, v12, 0xf

    if-eq v2, v12, :cond_25

    invoke-interface {v9}, Lf/h/a/c/a1/e0/c0;->c()V

    :cond_25
    if-eqz v8, :cond_27

    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->q()I

    move-result v8

    and-int/lit8 v8, v8, 0x40

    if-eqz v8, :cond_26

    const/4 v8, 0x2

    goto :goto_10

    :cond_26
    const/4 v8, 0x0

    :goto_10
    or-int/2addr v6, v8

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    sub-int/2addr v2, v3

    invoke-virtual {v8, v2}, Lf/h/a/c/i1/r;->D(I)V

    :cond_27
    iget-boolean v2, v0, Lf/h/a/c/a1/e0/b0;->m:Z

    iget v8, v0, Lf/h/a/c/a1/e0/b0;->a:I

    if-eq v8, v4, :cond_29

    if-nez v2, :cond_29

    iget-object v8, v0, Lf/h/a/c/a1/e0/b0;->h:Landroid/util/SparseBooleanArray;

    invoke-virtual {v8, v7, v15}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v7

    if-nez v7, :cond_28

    goto :goto_11

    :cond_28
    const/4 v7, 0x0

    goto :goto_12

    :cond_29
    :goto_11
    const/4 v7, 0x1

    :goto_12
    if-eqz v7, :cond_2a

    iget-object v7, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v7, v1}, Lf/h/a/c/i1/r;->B(I)V

    iget-object v7, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-interface {v9, v7, v6}, Lf/h/a/c/a1/e0/c0;->b(Lf/h/a/c/i1/r;I)V

    iget-object v6, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v6, v5}, Lf/h/a/c/i1/r;->B(I)V

    :cond_2a
    iget v5, v0, Lf/h/a/c/a1/e0/b0;->a:I

    if-eq v5, v4, :cond_2b

    if-nez v2, :cond_2b

    iget-boolean v2, v0, Lf/h/a/c/a1/e0/b0;->m:Z

    if-eqz v2, :cond_2b

    const-wide/16 v4, -0x1

    cmp-long v2, v10, v4

    if-eqz v2, :cond_2b

    iput-boolean v3, v0, Lf/h/a/c/a1/e0/b0;->o:Z

    :cond_2b
    iget-object v2, v0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v1}, Lf/h/a/c/i1/r;->C(I)V

    return v15
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/e0/b0;->k:Lf/h/a/c/a1/i;

    return-void
.end method

.method public f(JJ)V
    .locals 10

    iget p1, p0, Lf/h/a/c/a1/e0/b0;->a:I

    const/4 p2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Lf/g/j/k/a;->s(Z)V

    iget-object p1, p0, Lf/h/a/c/a1/e0/b0;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x0

    :goto_1
    const-wide/16 v2, 0x0

    if-ge v1, p1, :cond_4

    iget-object v4, p0, Lf/h/a/c/a1/e0/b0;->b:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/i1/z;

    invoke-virtual {v4}, Lf/h/a/c/i1/z;->c()J

    move-result-wide v5

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v9, v5, v7

    if-nez v9, :cond_1

    const/4 v5, 0x1

    goto :goto_2

    :cond_1
    const/4 v5, 0x0

    :goto_2
    if-nez v5, :cond_2

    invoke-virtual {v4}, Lf/h/a/c/i1/z;->c()J

    move-result-wide v5

    cmp-long v9, v5, v2

    if-eqz v9, :cond_3

    iget-wide v2, v4, Lf/h/a/c/i1/z;->a:J

    cmp-long v5, v2, p3

    if-eqz v5, :cond_3

    :cond_2
    iput-wide v7, v4, Lf/h/a/c/i1/z;->c:J

    invoke-virtual {v4, p3, p4}, Lf/h/a/c/i1/z;->d(J)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    cmp-long p1, p3, v2

    if-eqz p1, :cond_5

    iget-object p1, p0, Lf/h/a/c/a1/e0/b0;->j:Lf/h/a/c/a1/e0/z;

    if-eqz p1, :cond_5

    invoke-virtual {p1, p3, p4}, Lf/h/a/c/a1/a;->e(J)V

    :cond_5
    iget-object p1, p0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->x()V

    iget-object p1, p0, Lf/h/a/c/a1/e0/b0;->d:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Landroid/util/SparseIntArray;->clear()V

    const/4 p1, 0x0

    :goto_3
    iget-object p2, p0, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result p2

    if-ge p1, p2, :cond_6

    iget-object p2, p0, Lf/h/a/c/a1/e0/b0;->f:Landroid/util/SparseArray;

    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/a1/e0/c0;

    invoke-interface {p2}, Lf/h/a/c/a1/e0/c0;->c()V

    add-int/lit8 p1, p1, 0x1

    goto :goto_3

    :cond_6
    iput v0, p0, Lf/h/a/c/a1/e0/b0;->q:I

    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/a1/e0/b0;->c:Lf/h/a/c/i1/r;

    iget-object v0, v0, Lf/h/a/c/i1/r;->a:[B

    const/4 v1, 0x0

    const/16 v2, 0x3ac

    invoke-virtual {p1, v0, v1, v2, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0xbc

    if-ge v2, v3, :cond_3

    const/4 v3, 0x0

    :goto_1
    const/4 v4, 0x5

    const/4 v5, 0x1

    if-ge v3, v4, :cond_1

    mul-int/lit16 v4, v3, 0xbc

    add-int/2addr v4, v2

    aget-byte v4, v0, v4

    const/16 v6, 0x47

    if-eq v4, v6, :cond_0

    const/4 v3, 0x0

    goto :goto_2

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_2

    invoke-virtual {p1, v2}, Lf/h/a/c/a1/e;->i(I)V

    return v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method

.method public release()V
    .locals 0

    return-void
.end method
