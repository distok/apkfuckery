.class public final Lf/h/a/c/a1/e0/h;
.super Ljava/lang/Object;
.source "DtsReader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/j;


# instance fields
.field public final a:Lf/h/a/c/i1/r;

.field public final b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lf/h/a/c/a1/s;

.field public e:I

.field public f:I

.field public g:I

.field public h:J

.field public i:Lcom/google/android/exoplayer2/Format;

.field public j:I

.field public k:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0x12

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/h;->a:Lf/h/a/c/i1/r;

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/h;->e:I

    iput-object p1, p0, Lf/h/a/c/a1/e0/h;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public b(Lf/h/a/c/i1/r;)V
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-lez v2, :cond_18

    iget v2, v0, Lf/h/a/c/a1/e0/h;->e:I

    const/16 v4, 0x8

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v2, :cond_14

    if-eq v2, v7, :cond_2

    if-ne v2, v5, :cond_1

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    iget v3, v0, Lf/h/a/c/a1/e0/h;->j:I

    iget v4, v0, Lf/h/a/c/a1/e0/h;->f:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/h;->d:Lf/h/a/c/a1/s;

    invoke-interface {v3, v1, v2}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v3, v0, Lf/h/a/c/a1/e0/h;->f:I

    add-int/2addr v3, v2

    iput v3, v0, Lf/h/a/c/a1/e0/h;->f:I

    iget v11, v0, Lf/h/a/c/a1/e0/h;->j:I

    if-ne v3, v11, :cond_0

    iget-object v7, v0, Lf/h/a/c/a1/e0/h;->d:Lf/h/a/c/a1/s;

    iget-wide v8, v0, Lf/h/a/c/a1/e0/h;->k:J

    const/4 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-interface/range {v7 .. v13}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iget-wide v2, v0, Lf/h/a/c/a1/e0/h;->k:J

    iget-wide v4, v0, Lf/h/a/c/a1/e0/h;->h:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lf/h/a/c/a1/e0/h;->k:J

    iput v6, v0, Lf/h/a/c/a1/e0/h;->e:I

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_2
    iget-object v2, v0, Lf/h/a/c/a1/e0/h;->a:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v9

    iget v10, v0, Lf/h/a/c/a1/e0/h;->f:I

    const/16 v11, 0x12

    rsub-int/lit8 v10, v10, 0x12

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    iget v10, v0, Lf/h/a/c/a1/e0/h;->f:I

    iget-object v12, v1, Lf/h/a/c/i1/r;->a:[B

    iget v13, v1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v12, v13, v2, v10, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, v1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v2, v9

    iput v2, v1, Lf/h/a/c/i1/r;->b:I

    iget v2, v0, Lf/h/a/c/a1/e0/h;->f:I

    add-int/2addr v2, v9

    iput v2, v0, Lf/h/a/c/a1/e0/h;->f:I

    if-ne v2, v11, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/h/a/c/a1/e0/h;->a:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    iget-object v9, v0, Lf/h/a/c/a1/e0/h;->i:Lcom/google/android/exoplayer2/Format;

    const/16 v10, 0xe

    const/4 v13, -0x2

    const/16 v14, 0x1f

    const/4 v11, -0x1

    if-nez v9, :cond_c

    iget-object v9, v0, Lf/h/a/c/a1/e0/h;->c:Ljava/lang/String;

    iget-object v15, v0, Lf/h/a/c/a1/e0/h;->b:Ljava/lang/String;

    const/16 v24, 0x0

    aget-byte v8, v2, v6

    const/16 v3, 0x7f

    if-ne v8, v3, :cond_4

    new-instance v3, Lf/h/a/c/i1/q;

    invoke-direct {v3, v2}, Lf/h/a/c/i1/q;-><init>([B)V

    goto/16 :goto_7

    :cond_4
    array-length v3, v2

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    aget-byte v8, v3, v6

    if-eq v8, v13, :cond_6

    aget-byte v8, v3, v6

    if-ne v8, v11, :cond_5

    goto :goto_2

    :cond_5
    const/4 v8, 0x0

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v8, 0x1

    :goto_3
    if-eqz v8, :cond_7

    const/4 v8, 0x0

    :goto_4
    array-length v13, v3

    sub-int/2addr v13, v7

    if-ge v8, v13, :cond_7

    aget-byte v13, v3, v8

    add-int/lit8 v16, v8, 0x1

    aget-byte v17, v3, v16

    aput-byte v17, v3, v8

    aput-byte v13, v3, v16

    add-int/lit8 v8, v8, 0x2

    goto :goto_4

    :cond_7
    new-instance v8, Lf/h/a/c/i1/q;

    invoke-direct {v8, v3}, Lf/h/a/c/i1/q;-><init>([B)V

    aget-byte v13, v3, v6

    if-ne v13, v14, :cond_9

    new-instance v13, Lf/h/a/c/i1/q;

    invoke-direct {v13, v3}, Lf/h/a/c/i1/q;-><init>([B)V

    :goto_5
    invoke-virtual {v13}, Lf/h/a/c/i1/q;->b()I

    move-result v14

    const/16 v12, 0x10

    if-lt v14, v12, :cond_9

    invoke-virtual {v13, v5}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v13, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v12

    and-int/lit16 v12, v12, 0x3fff

    iget v14, v8, Lf/h/a/c/i1/q;->c:I

    rsub-int/lit8 v14, v14, 0x8

    invoke-static {v14, v10}, Ljava/lang/Math;->min(II)I

    move-result v14

    iget v5, v8, Lf/h/a/c/i1/q;->c:I

    rsub-int/lit8 v16, v5, 0x8

    sub-int v16, v16, v14

    const v17, 0xff00

    shr-int v5, v17, v5

    shl-int v17, v7, v16

    add-int/lit8 v17, v17, -0x1

    or-int v5, v5, v17

    iget-object v6, v8, Lf/h/a/c/i1/q;->a:[B

    iget v11, v8, Lf/h/a/c/i1/q;->b:I

    aget-byte v17, v6, v11

    and-int v5, v5, v17

    int-to-byte v5, v5

    aput-byte v5, v6, v11

    rsub-int/lit8 v5, v14, 0xe

    ushr-int v14, v12, v5

    aget-byte v17, v6, v11

    shl-int v14, v14, v16

    or-int v14, v14, v17

    int-to-byte v14, v14

    aput-byte v14, v6, v11

    add-int/2addr v11, v7

    :goto_6
    if-le v5, v4, :cond_8

    iget-object v6, v8, Lf/h/a/c/i1/q;->a:[B

    add-int/lit8 v14, v11, 0x1

    add-int/lit8 v5, v5, -0x8

    ushr-int v4, v12, v5

    int-to-byte v4, v4

    aput-byte v4, v6, v11

    move v11, v14

    const/16 v4, 0x8

    goto :goto_6

    :cond_8
    rsub-int/lit8 v4, v5, 0x8

    iget-object v6, v8, Lf/h/a/c/i1/q;->a:[B

    aget-byte v14, v6, v11

    shl-int v16, v7, v4

    const/16 v17, -0x1

    add-int/lit8 v16, v16, -0x1

    and-int v14, v14, v16

    int-to-byte v14, v14

    aput-byte v14, v6, v11

    shl-int v5, v7, v5

    sub-int/2addr v5, v7

    and-int/2addr v5, v12

    aget-byte v12, v6, v11

    shl-int v4, v5, v4

    or-int/2addr v4, v12

    int-to-byte v4, v4

    aput-byte v4, v6, v11

    invoke-virtual {v8, v10}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v8}, Lf/h/a/c/i1/q;->a()V

    const/16 v4, 0x8

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v11, -0x1

    goto :goto_5

    :cond_9
    array-length v4, v3

    iput-object v3, v8, Lf/h/a/c/i1/q;->a:[B

    const/4 v3, 0x0

    iput v3, v8, Lf/h/a/c/i1/q;->b:I

    iput v3, v8, Lf/h/a/c/i1/q;->c:I

    iput v4, v8, Lf/h/a/c/i1/q;->d:I

    move-object v3, v8

    :goto_7
    const/16 v4, 0x3c

    invoke-virtual {v3, v4}, Lf/h/a/c/i1/q;->l(I)V

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    sget-object v4, Lf/h/a/c/w0/r;->a:[I

    aget v4, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    sget-object v5, Lf/h/a/c/w0/r;->b:[I

    aget v22, v5, v6

    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    sget-object v5, Lf/h/a/c/w0/r;->c:[I

    array-length v8, v5

    if-lt v6, v8, :cond_a

    const/4 v6, 0x2

    const/16 v19, -0x1

    goto :goto_8

    :cond_a
    aget v5, v5, v6

    mul-int/lit16 v5, v5, 0x3e8

    const/4 v6, 0x2

    div-int/2addr v5, v6

    move/from16 v19, v5

    :goto_8
    const/16 v5, 0xa

    invoke-virtual {v3, v5}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v3, v6}, Lf/h/a/c/i1/q;->f(I)I

    move-result v3

    if-lez v3, :cond_b

    const/4 v3, 0x1

    goto :goto_9

    :cond_b
    const/4 v3, 0x0

    :goto_9
    add-int v21, v4, v3

    const/16 v18, 0x0

    const/16 v20, -0x1

    const/16 v23, 0x0

    const/16 v25, 0x0

    const-string v17, "audio/vnd.dts"

    move-object/from16 v16, v9

    move-object/from16 v26, v15

    invoke-static/range {v16 .. v26}, Lcom/google/android/exoplayer2/Format;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    iput-object v3, v0, Lf/h/a/c/a1/e0/h;->i:Lcom/google/android/exoplayer2/Format;

    iget-object v4, v0, Lf/h/a/c/a1/e0/h;->d:Lf/h/a/c/a1/s;

    invoke-interface {v4, v3}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    :cond_c
    const/4 v3, 0x0

    aget-byte v4, v2, v3

    const/4 v3, 0x7

    const/4 v5, -0x2

    if-eq v4, v5, :cond_f

    const/4 v5, -0x1

    if-eq v4, v5, :cond_e

    const/16 v5, 0x1f

    if-eq v4, v5, :cond_d

    const/4 v4, 0x5

    aget-byte v5, v2, v4

    const/4 v4, 0x3

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0xc

    const/4 v5, 0x6

    aget-byte v6, v2, v5

    and-int/lit16 v6, v6, 0xff

    const/4 v8, 0x4

    shl-int/2addr v6, v8

    or-int/2addr v4, v6

    aget-byte v6, v2, v3

    goto :goto_b

    :cond_d
    const/4 v4, 0x3

    const/4 v5, 0x6

    const/4 v8, 0x4

    aget-byte v6, v2, v5

    and-int/2addr v4, v6

    shl-int/lit8 v4, v4, 0xc

    aget-byte v5, v2, v3

    and-int/lit16 v5, v5, 0xff

    shl-int/2addr v5, v8

    or-int/2addr v4, v5

    const/16 v5, 0x8

    aget-byte v5, v2, v5

    goto :goto_a

    :cond_e
    const/4 v4, 0x3

    const/4 v8, 0x4

    aget-byte v5, v2, v3

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0xc

    const/4 v5, 0x6

    aget-byte v6, v2, v5

    and-int/lit16 v5, v6, 0xff

    shl-int/2addr v5, v8

    or-int/2addr v4, v5

    const/16 v5, 0x9

    aget-byte v5, v2, v5

    :goto_a
    const/16 v6, 0x3c

    and-int/2addr v5, v6

    const/4 v6, 0x2

    shr-int/2addr v5, v6

    or-int/2addr v4, v5

    add-int/2addr v4, v7

    const/4 v5, 0x1

    goto :goto_c

    :cond_f
    const/4 v8, 0x4

    aget-byte v4, v2, v8

    const/4 v5, 0x3

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0xc

    aget-byte v5, v2, v3

    and-int/lit16 v5, v5, 0xff

    shl-int/2addr v5, v8

    or-int/2addr v4, v5

    const/4 v5, 0x6

    aget-byte v6, v2, v5

    :goto_b
    and-int/lit16 v5, v6, 0xf0

    shr-int/2addr v5, v8

    or-int/2addr v4, v5

    add-int/2addr v4, v7

    const/4 v5, 0x0

    :goto_c
    if-eqz v5, :cond_10

    mul-int/lit8 v4, v4, 0x10

    div-int/2addr v4, v10

    :cond_10
    iput v4, v0, Lf/h/a/c/a1/e0/h;->j:I

    const-wide/32 v4, 0xf4240

    const/4 v6, 0x0

    aget-byte v8, v2, v6

    const/4 v6, -0x2

    if-eq v8, v6, :cond_13

    const/4 v6, -0x1

    if-eq v8, v6, :cond_12

    const/16 v6, 0x1f

    if-eq v8, v6, :cond_11

    const/4 v6, 0x4

    aget-byte v3, v2, v6

    and-int/2addr v3, v7

    const/4 v8, 0x6

    shl-int/2addr v3, v8

    const/4 v9, 0x5

    aget-byte v2, v2, v9

    goto :goto_e

    :cond_11
    const/4 v6, 0x4

    const/4 v8, 0x6

    const/4 v9, 0x5

    aget-byte v9, v2, v9

    and-int/2addr v3, v9

    shl-int/2addr v3, v6

    aget-byte v2, v2, v8

    goto :goto_d

    :cond_12
    const/4 v6, 0x4

    aget-byte v8, v2, v6

    and-int/2addr v8, v3

    shl-int/lit8 v6, v8, 0x4

    aget-byte v2, v2, v3

    move v3, v6

    :goto_d
    const/16 v6, 0x3c

    and-int/2addr v2, v6

    goto :goto_f

    :cond_13
    const/4 v3, 0x5

    const/4 v6, 0x4

    const/4 v8, 0x6

    aget-byte v3, v2, v3

    and-int/2addr v3, v7

    shl-int/2addr v3, v8

    aget-byte v2, v2, v6

    :goto_e
    and-int/lit16 v2, v2, 0xfc

    :goto_f
    const/4 v6, 0x2

    shr-int/2addr v2, v6

    or-int/2addr v2, v3

    add-int/2addr v2, v7

    mul-int/lit8 v2, v2, 0x20

    int-to-long v2, v2

    mul-long v2, v2, v4

    iget-object v4, v0, Lf/h/a/c/a1/e0/h;->i:Lcom/google/android/exoplayer2/Format;

    iget v4, v4, Lcom/google/android/exoplayer2/Format;->z:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    long-to-int v3, v2

    int-to-long v2, v3

    iput-wide v2, v0, Lf/h/a/c/a1/e0/h;->h:J

    iget-object v2, v0, Lf/h/a/c/a1/e0/h;->a:Lf/h/a/c/i1/r;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/h;->d:Lf/h/a/c/a1/s;

    iget-object v3, v0, Lf/h/a/c/a1/e0/h;->a:Lf/h/a/c/i1/r;

    const/16 v4, 0x12

    invoke-interface {v2, v3, v4}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    const/4 v2, 0x2

    iput v2, v0, Lf/h/a/c/a1/e0/h;->e:I

    goto/16 :goto_0

    :cond_14
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-lez v2, :cond_17

    iget v2, v0, Lf/h/a/c/a1/e0/h;->g:I

    const/16 v3, 0x8

    shl-int/2addr v2, v3

    iput v2, v0, Lf/h/a/c/a1/e0/h;->g:I

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->q()I

    move-result v4

    or-int/2addr v2, v4

    iput v2, v0, Lf/h/a/c/a1/e0/h;->g:I

    const v4, 0x7ffe8001

    if-eq v2, v4, :cond_16

    const v4, -0x180fe80

    if-eq v2, v4, :cond_16

    const v4, 0x1fffe800

    if-eq v2, v4, :cond_16

    const v4, -0xe0ff18

    if-ne v2, v4, :cond_15

    goto :goto_10

    :cond_15
    const/4 v4, 0x0

    goto :goto_11

    :cond_16
    :goto_10
    const/4 v4, 0x1

    :goto_11
    if-eqz v4, :cond_14

    iget-object v3, v0, Lf/h/a/c/a1/e0/h;->a:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    shr-int/lit8 v4, v2, 0x18

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    const/4 v5, 0x0

    aput-byte v4, v3, v5

    shr-int/lit8 v4, v2, 0x10

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v7

    shr-int/lit8 v4, v2, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    const/4 v5, 0x2

    aput-byte v4, v3, v5

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    const/4 v4, 0x3

    aput-byte v2, v3, v4

    const/4 v2, 0x4

    iput v2, v0, Lf/h/a/c/a1/e0/h;->f:I

    const/4 v6, 0x0

    iput v6, v0, Lf/h/a/c/a1/e0/h;->g:I

    const/4 v6, 0x1

    goto :goto_12

    :cond_17
    const/4 v6, 0x0

    :goto_12
    if-eqz v6, :cond_0

    iput v7, v0, Lf/h/a/c/a1/e0/h;->e:I

    goto/16 :goto_0

    :cond_18
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/h;->e:I

    iput v0, p0, Lf/h/a/c/a1/e0/h;->f:I

    iput v0, p0, Lf/h/a/c/a1/e0/h;->g:I

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 1

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/h;->c:Ljava/lang/String;

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result p2

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/e0/h;->d:Lf/h/a/c/a1/s;

    return-void
.end method

.method public f(JI)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/a1/e0/h;->k:J

    return-void
.end method
