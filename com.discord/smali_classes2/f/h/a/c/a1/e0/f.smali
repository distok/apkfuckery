.class public final Lf/h/a/c/a1/e0/f;
.super Ljava/lang/Object;
.source "AdtsReader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/j;


# static fields
.field public static final v:[B


# instance fields
.field public final a:Z

.field public final b:Lf/h/a/c/i1/q;

.field public final c:Lf/h/a/c/i1/r;

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lf/h/a/c/a1/s;

.field public g:Lf/h/a/c/a1/s;

.field public h:I

.field public i:I

.field public j:I

.field public k:Z

.field public l:Z

.field public m:I

.field public n:I

.field public o:I

.field public p:Z

.field public q:J

.field public r:I

.field public s:J

.field public t:Lf/h/a/c/a1/s;

.field public u:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lf/h/a/c/a1/e0/f;->v:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x49t
        0x44t
        0x33t
    .end array-data
.end method

.method public constructor <init>(ZLjava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/q;

    const/4 v1, 0x7

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lf/h/a/c/i1/q;-><init>([B)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    new-instance v0, Lf/h/a/c/i1/r;

    sget-object v1, Lf/h/a/c/a1/e0/f;->v:[B

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/f;->c:Lf/h/a/c/i1/r;

    invoke-virtual {p0}, Lf/h/a/c/a1/e0/f;->h()V

    const/4 v0, -0x1

    iput v0, p0, Lf/h/a/c/a1/e0/f;->m:I

    iput v0, p0, Lf/h/a/c/a1/e0/f;->n:I

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lf/h/a/c/a1/e0/f;->q:J

    iput-boolean p1, p0, Lf/h/a/c/a1/e0/f;->a:Z

    iput-object p2, p0, Lf/h/a/c/a1/e0/f;->d:Ljava/lang/String;

    return-void
.end method

.method public static g(I)Z
    .locals 1

    const v0, 0xfff6

    and-int/2addr p0, v0

    const v0, 0xfff0

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public final a(Lf/h/a/c/i1/r;[BI)Z
    .locals 4

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    iget v1, p0, Lf/h/a/c/a1/e0/f;->i:I

    sub-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lf/h/a/c/a1/e0/f;->i:I

    iget-object v2, p1, Lf/h/a/c/i1/r;->a:[B

    iget v3, p1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v2, v3, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p2, p1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr p2, v0

    iput p2, p1, Lf/h/a/c/i1/r;->b:I

    iget p1, p0, Lf/h/a/c/a1/e0/f;->i:I

    add-int/2addr p1, v0

    iput p1, p0, Lf/h/a/c/a1/e0/f;->i:I

    if-ne p1, p3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b(Lf/h/a/c/i1/r;)V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-lez v2, :cond_21

    iget v2, v0, Lf/h/a/c/a1/e0/f;->h:I

    const/4 v3, 0x6

    const/16 v4, 0xd

    const/4 v5, -0x1

    const/4 v6, 0x3

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eqz v2, :cond_c

    if-eq v2, v10, :cond_8

    const/16 v5, 0xa

    if-eq v2, v9, :cond_7

    if-eq v2, v6, :cond_2

    if-ne v2, v8, :cond_1

    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    iget v3, v0, Lf/h/a/c/a1/e0/f;->r:I

    iget v4, v0, Lf/h/a/c/a1/e0/f;->i:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v0, Lf/h/a/c/a1/e0/f;->t:Lf/h/a/c/a1/s;

    invoke-interface {v3, v1, v2}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v3, v0, Lf/h/a/c/a1/e0/f;->i:I

    add-int/2addr v3, v2

    iput v3, v0, Lf/h/a/c/a1/e0/f;->i:I

    iget v8, v0, Lf/h/a/c/a1/e0/f;->r:I

    if-ne v3, v8, :cond_0

    iget-object v4, v0, Lf/h/a/c/a1/e0/f;->t:Lf/h/a/c/a1/s;

    iget-wide v5, v0, Lf/h/a/c/a1/e0/f;->s:J

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-interface/range {v4 .. v10}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iget-wide v2, v0, Lf/h/a/c/a1/e0/f;->s:J

    iget-wide v4, v0, Lf/h/a/c/a1/e0/f;->u:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lf/h/a/c/a1/e0/f;->s:J

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/e0/f;->h()V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_2
    iget-boolean v2, v0, Lf/h/a/c/a1/e0/f;->k:Z

    const/4 v3, 0x5

    const/4 v11, 0x7

    if-eqz v2, :cond_3

    const/4 v2, 0x7

    goto :goto_1

    :cond_3
    const/4 v2, 0x5

    :goto_1
    iget-object v12, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    iget-object v12, v12, Lf/h/a/c/i1/q;->a:[B

    invoke-virtual {v0, v1, v12, v2}, Lf/h/a/c/a1/e0/f;->a(Lf/h/a/c/i1/r;[BI)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v7}, Lf/h/a/c/i1/q;->j(I)V

    iget-boolean v2, v0, Lf/h/a/c/a1/e0/f;->p:Z

    if-nez v2, :cond_5

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    add-int/2addr v2, v10

    if-eq v2, v9, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Detected audio object type: "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", but assuming AAC LC."

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "AdtsReader"

    invoke-static {v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    :cond_4
    iget-object v5, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v5, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v5, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    iget v12, v0, Lf/h/a/c/a1/e0/f;->n:I

    new-array v13, v9, [B

    shl-int/2addr v2, v6

    and-int/lit16 v2, v2, 0xf8

    shr-int/lit8 v14, v12, 0x1

    and-int/2addr v14, v11

    or-int/2addr v2, v14

    int-to-byte v2, v2

    aput-byte v2, v13, v7

    shl-int/lit8 v2, v12, 0x7

    and-int/lit16 v2, v2, 0x80

    shl-int/2addr v5, v6

    and-int/lit8 v5, v5, 0x78

    or-int/2addr v2, v5

    int-to-byte v2, v2

    aput-byte v2, v13, v10

    new-instance v2, Lf/h/a/c/i1/q;

    invoke-direct {v2, v13}, Lf/h/a/c/i1/q;-><init>([B)V

    invoke-static {v2, v7}, Lf/h/a/c/i1/g;->b(Lf/h/a/c/i1/q;Z)Landroid/util/Pair;

    move-result-object v2

    iget-object v14, v0, Lf/h/a/c/a1/e0/f;->e:Ljava/lang/String;

    const/16 v16, 0x0

    const/16 v17, -0x1

    const/16 v18, -0x1

    iget-object v5, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v19

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v20

    invoke-static {v13}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->d:Ljava/lang/String;

    const-string v15, "audio/mp4a-latm"

    move-object/from16 v24, v2

    invoke-static/range {v14 .. v24}, Lcom/google/android/exoplayer2/Format;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v2

    const-wide/32 v5, 0x3d090000

    iget v11, v2, Lcom/google/android/exoplayer2/Format;->z:I

    int-to-long v11, v11

    div-long/2addr v5, v11

    iput-wide v5, v0, Lf/h/a/c/a1/e0/f;->q:J

    iget-object v5, v0, Lf/h/a/c/a1/e0/f;->f:Lf/h/a/c/a1/s;

    invoke-interface {v5, v2}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    iput-boolean v10, v0, Lf/h/a/c/a1/e0/f;->p:Z

    goto :goto_2

    :cond_5
    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->l(I)V

    :goto_2
    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v8}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    sub-int/2addr v2, v9

    sub-int/2addr v2, v3

    iget-boolean v3, v0, Lf/h/a/c/a1/e0/f;->k:Z

    if-eqz v3, :cond_6

    add-int/lit8 v2, v2, -0x2

    :cond_6
    iget-object v3, v0, Lf/h/a/c/a1/e0/f;->f:Lf/h/a/c/a1/s;

    iget-wide v4, v0, Lf/h/a/c/a1/e0/f;->q:J

    iput v8, v0, Lf/h/a/c/a1/e0/f;->h:I

    iput v7, v0, Lf/h/a/c/a1/e0/f;->i:I

    iput-object v3, v0, Lf/h/a/c/a1/e0/f;->t:Lf/h/a/c/a1/s;

    iput-wide v4, v0, Lf/h/a/c/a1/e0/f;->u:J

    iput v2, v0, Lf/h/a/c/a1/e0/f;->r:I

    goto/16 :goto_0

    :cond_7
    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->c:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v0, v1, v2, v5}, Lf/h/a/c/a1/e0/f;->a(Lf/h/a/c/i1/r;[BI)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->g:Lf/h/a/c/a1/s;

    iget-object v4, v0, Lf/h/a/c/a1/e0/f;->c:Lf/h/a/c/i1/r;

    invoke-interface {v2, v4, v5}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->g:Lf/h/a/c/a1/s;

    iget-object v3, v0, Lf/h/a/c/a1/e0/f;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->p()I

    move-result v3

    add-int/2addr v3, v5

    iput v8, v0, Lf/h/a/c/a1/e0/f;->h:I

    iput v5, v0, Lf/h/a/c/a1/e0/f;->i:I

    iput-object v2, v0, Lf/h/a/c/a1/e0/f;->t:Lf/h/a/c/a1/s;

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lf/h/a/c/a1/e0/f;->u:J

    iput v3, v0, Lf/h/a/c/a1/e0/f;->r:I

    goto/16 :goto_0

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lf/h/a/c/i1/r;->a()I

    move-result v2

    if-nez v2, :cond_9

    goto/16 :goto_0

    :cond_9
    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    iget-object v3, v2, Lf/h/a/c/i1/q;->a:[B

    iget-object v4, v1, Lf/h/a/c/i1/r;->a:[B

    iget v11, v1, Lf/h/a/c/i1/r;->b:I

    aget-byte v4, v4, v11

    aput-byte v4, v3, v7

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iget v3, v0, Lf/h/a/c/a1/e0/f;->n:I

    if-eq v3, v5, :cond_a

    if-eq v2, v3, :cond_a

    iput-boolean v7, v0, Lf/h/a/c/a1/e0/f;->l:Z

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/e0/f;->h()V

    goto/16 :goto_0

    :cond_a
    iget-boolean v3, v0, Lf/h/a/c/a1/e0/f;->l:Z

    if-nez v3, :cond_b

    iput-boolean v10, v0, Lf/h/a/c/a1/e0/f;->l:Z

    iget v3, v0, Lf/h/a/c/a1/e0/f;->o:I

    iput v3, v0, Lf/h/a/c/a1/e0/f;->m:I

    iput v2, v0, Lf/h/a/c/a1/e0/f;->n:I

    :cond_b
    iput v6, v0, Lf/h/a/c/a1/e0/f;->h:I

    iput v7, v0, Lf/h/a/c/a1/e0/f;->i:I

    goto/16 :goto_0

    :cond_c
    iget-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    iget v11, v1, Lf/h/a/c/i1/r;->b:I

    iget v12, v1, Lf/h/a/c/i1/r;->c:I

    :goto_3
    if-ge v11, v12, :cond_20

    add-int/lit8 v13, v11, 0x1

    aget-byte v11, v2, v11

    and-int/lit16 v11, v11, 0xff

    iget v14, v0, Lf/h/a/c/a1/e0/f;->j:I

    const/16 v15, 0x200

    if-ne v14, v15, :cond_1a

    int-to-byte v14, v11

    const v16, 0xff00

    and-int/lit16 v14, v14, 0xff

    or-int v14, v14, v16

    invoke-static {v14}, Lf/h/a/c/a1/e0/f;->g(I)Z

    move-result v14

    if-eqz v14, :cond_1a

    iget-boolean v14, v0, Lf/h/a/c/a1/e0/f;->l:Z

    if-nez v14, :cond_17

    add-int/lit8 v14, v13, -0x2

    add-int/lit8 v15, v14, 0x1

    invoke-virtual {v1, v15}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v15, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    iget-object v15, v15, Lf/h/a/c/i1/q;->a:[B

    invoke-virtual {v0, v1, v15, v10}, Lf/h/a/c/a1/e0/f;->i(Lf/h/a/c/i1/r;[BI)Z

    move-result v15

    if-nez v15, :cond_d

    goto/16 :goto_5

    :cond_d
    iget-object v15, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v15, v8}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v15, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v15, v10}, Lf/h/a/c/i1/q;->f(I)I

    move-result v15

    iget v7, v0, Lf/h/a/c/a1/e0/f;->m:I

    if-eq v7, v5, :cond_e

    if-eq v15, v7, :cond_e

    goto/16 :goto_5

    :cond_e
    iget v7, v0, Lf/h/a/c/a1/e0/f;->n:I

    if-eq v7, v5, :cond_11

    iget-object v7, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    iget-object v7, v7, Lf/h/a/c/i1/q;->a:[B

    invoke-virtual {v0, v1, v7, v10}, Lf/h/a/c/a1/e0/f;->i(Lf/h/a/c/i1/r;[BI)Z

    move-result v7

    if-nez v7, :cond_f

    goto :goto_4

    :cond_f
    iget-object v7, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v7, v9}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v7, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v7, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v7

    iget v9, v0, Lf/h/a/c/a1/e0/f;->n:I

    if-eq v7, v9, :cond_10

    goto :goto_5

    :cond_10
    add-int/lit8 v7, v14, 0x2

    invoke-virtual {v1, v7}, Lf/h/a/c/i1/r;->C(I)V

    :cond_11
    iget-object v7, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    iget-object v7, v7, Lf/h/a/c/i1/q;->a:[B

    invoke-virtual {v0, v1, v7, v8}, Lf/h/a/c/a1/e0/f;->i(Lf/h/a/c/i1/r;[BI)Z

    move-result v7

    if-nez v7, :cond_12

    goto :goto_4

    :cond_12
    iget-object v7, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    const/16 v9, 0xe

    invoke-virtual {v7, v9}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v7, v0, Lf/h/a/c/a1/e0/f;->b:Lf/h/a/c/i1/q;

    invoke-virtual {v7, v4}, Lf/h/a/c/i1/q;->f(I)I

    move-result v7

    if-gt v7, v3, :cond_13

    goto :goto_5

    :cond_13
    add-int/2addr v14, v7

    add-int/lit8 v7, v14, 0x1

    iget v9, v1, Lf/h/a/c/i1/r;->c:I

    if-lt v7, v9, :cond_14

    goto :goto_4

    :cond_14
    iget-object v9, v1, Lf/h/a/c/i1/r;->a:[B

    aget-byte v14, v9, v14

    aget-byte v9, v9, v7

    and-int/lit16 v14, v14, 0xff

    shl-int/lit8 v14, v14, 0x8

    and-int/lit16 v9, v9, 0xff

    or-int/2addr v9, v14

    invoke-static {v9}, Lf/h/a/c/a1/e0/f;->g(I)Z

    move-result v9

    if-eqz v9, :cond_16

    iget v9, v0, Lf/h/a/c/a1/e0/f;->m:I

    if-eq v9, v5, :cond_15

    iget-object v9, v1, Lf/h/a/c/i1/r;->a:[B

    aget-byte v7, v9, v7

    and-int/lit8 v7, v7, 0x8

    shr-int/2addr v7, v6

    if-ne v7, v15, :cond_16

    :cond_15
    :goto_4
    const/4 v7, 0x1

    goto :goto_6

    :cond_16
    :goto_5
    const/4 v7, 0x0

    :goto_6
    if-eqz v7, :cond_1a

    :cond_17
    and-int/lit8 v2, v11, 0x8

    shr-int/2addr v2, v6

    iput v2, v0, Lf/h/a/c/a1/e0/f;->o:I

    and-int/lit8 v2, v11, 0x1

    if-nez v2, :cond_18

    const/4 v2, 0x1

    goto :goto_7

    :cond_18
    const/4 v2, 0x0

    :goto_7
    iput-boolean v2, v0, Lf/h/a/c/a1/e0/f;->k:Z

    iget-boolean v2, v0, Lf/h/a/c/a1/e0/f;->l:Z

    if-nez v2, :cond_19

    iput v10, v0, Lf/h/a/c/a1/e0/f;->h:I

    const/4 v2, 0x0

    iput v2, v0, Lf/h/a/c/a1/e0/f;->i:I

    goto :goto_8

    :cond_19
    const/4 v2, 0x0

    iput v6, v0, Lf/h/a/c/a1/e0/f;->h:I

    iput v2, v0, Lf/h/a/c/a1/e0/f;->i:I

    :goto_8
    invoke-virtual {v1, v13}, Lf/h/a/c/i1/r;->C(I)V

    goto/16 :goto_0

    :cond_1a
    iget v7, v0, Lf/h/a/c/a1/e0/f;->j:I

    or-int v9, v11, v7

    const/16 v11, 0x149

    if-eq v9, v11, :cond_1f

    const/16 v11, 0x1ff

    if-eq v9, v11, :cond_1e

    const/16 v11, 0x344

    if-eq v9, v11, :cond_1d

    const/16 v11, 0x433

    if-eq v9, v11, :cond_1c

    const/16 v9, 0x100

    if-eq v7, v9, :cond_1b

    iput v9, v0, Lf/h/a/c/a1/e0/f;->j:I

    add-int/lit8 v13, v13, -0x1

    move v11, v13

    const/4 v7, 0x2

    const/4 v9, 0x0

    goto :goto_a

    :cond_1b
    const/4 v7, 0x2

    const/4 v9, 0x0

    goto :goto_9

    :cond_1c
    const/4 v7, 0x2

    iput v7, v0, Lf/h/a/c/a1/e0/f;->h:I

    sget-object v2, Lf/h/a/c/a1/e0/f;->v:[B

    array-length v2, v2

    iput v2, v0, Lf/h/a/c/a1/e0/f;->i:I

    const/4 v9, 0x0

    iput v9, v0, Lf/h/a/c/a1/e0/f;->r:I

    iget-object v2, v0, Lf/h/a/c/a1/e0/f;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v9}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v1, v13}, Lf/h/a/c/i1/r;->C(I)V

    goto/16 :goto_0

    :cond_1d
    const/4 v7, 0x2

    const/4 v9, 0x0

    const/16 v11, 0x400

    iput v11, v0, Lf/h/a/c/a1/e0/f;->j:I

    goto :goto_9

    :cond_1e
    const/4 v7, 0x2

    const/4 v9, 0x0

    const/16 v11, 0x200

    iput v11, v0, Lf/h/a/c/a1/e0/f;->j:I

    goto :goto_9

    :cond_1f
    const/4 v7, 0x2

    const/4 v9, 0x0

    const/16 v11, 0x300

    iput v11, v0, Lf/h/a/c/a1/e0/f;->j:I

    :goto_9
    move v11, v13

    :goto_a
    const/4 v7, 0x0

    const/4 v9, 0x2

    goto/16 :goto_3

    :cond_20
    invoke-virtual {v1, v11}, Lf/h/a/c/i1/r;->C(I)V

    goto/16 :goto_0

    :cond_21
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/f;->l:Z

    invoke-virtual {p0}, Lf/h/a/c/a1/e0/f;->h()V

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 3

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/f;->e:Ljava/lang/String;

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/a1/e0/f;->f:Lf/h/a/c/a1/s;

    iget-boolean v0, p0, Lf/h/a/c/a1/e0/f;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->a()V

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->c()I

    move-result v0

    const/4 v1, 0x4

    invoke-interface {p1, v0, v1}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/e0/f;->g:Lf/h/a/c/a1/s;

    invoke-virtual {p2}, Lf/h/a/c/a1/e0/c0$d;->b()Ljava/lang/String;

    move-result-object p2

    const/4 v0, -0x1

    const/4 v1, 0x0

    const-string v2, "application/id3"

    invoke-static {p2, v2, v1, v0, v1}, Lcom/google/android/exoplayer2/Format;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object p2

    invoke-interface {p1, p2}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lf/h/a/c/a1/g;

    invoke-direct {p1}, Lf/h/a/c/a1/g;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/f;->g:Lf/h/a/c/a1/s;

    :goto_0
    return-void
.end method

.method public f(JI)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/a1/e0/f;->s:J

    return-void
.end method

.method public final h()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/e0/f;->h:I

    iput v0, p0, Lf/h/a/c/a1/e0/f;->i:I

    const/16 v0, 0x100

    iput v0, p0, Lf/h/a/c/a1/e0/f;->j:I

    return-void
.end method

.method public final i(Lf/h/a/c/i1/r;[BI)Z
    .locals 3

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    const/4 v1, 0x0

    if-ge v0, p3, :cond_0

    return v1

    :cond_0
    iget-object v0, p1, Lf/h/a/c/i1/r;->a:[B

    iget v2, p1, Lf/h/a/c/i1/r;->b:I

    invoke-static {v0, v2, p2, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p2, p1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr p2, p3

    iput p2, p1, Lf/h/a/c/i1/r;->b:I

    const/4 p1, 0x1

    return p1
.end method
