.class public final Lf/h/a/c/a1/e0/w;
.super Ljava/lang/Object;
.source "SectionReader.java"

# interfaces
.implements Lf/h/a/c/a1/e0/c0;


# instance fields
.field public final a:Lf/h/a/c/a1/e0/v;

.field public final b:Lf/h/a/c/i1/r;

.field public c:I

.field public d:I

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/e0/v;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/e0/w;->a:Lf/h/a/c/a1/e0/v;

    new-instance p1, Lf/h/a/c/i1/r;

    const/16 v0, 0x20

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/i1/z;Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a1/e0/w;->a:Lf/h/a/c/a1/e0/v;

    invoke-interface {v0, p1, p2, p3}, Lf/h/a/c/a1/e0/v;->a(Lf/h/a/c/i1/z;Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/a1/e0/w;->f:Z

    return-void
.end method

.method public b(Lf/h/a/c/i1/r;I)V
    .locals 10

    const/4 v0, 0x1

    and-int/2addr p2, v0

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v2, -0x1

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->q()I

    move-result v3

    iget v4, p1, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v4, v3

    goto :goto_1

    :cond_1
    const/4 v4, -0x1

    :goto_1
    iget-boolean v3, p0, Lf/h/a/c/a1/e0/w;->f:Z

    if-eqz v3, :cond_3

    if-nez p2, :cond_2

    return-void

    :cond_2
    iput-boolean v1, p0, Lf/h/a/c/a1/e0/w;->f:Z

    invoke-virtual {p1, v4}, Lf/h/a/c/i1/r;->C(I)V

    iput v1, p0, Lf/h/a/c/a1/e0/w;->d:I

    :cond_3
    :goto_2
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result p2

    if-lez p2, :cond_a

    iget p2, p0, Lf/h/a/c/a1/e0/w;->d:I

    const/16 v3, 0xff

    const/4 v4, 0x3

    if-ge p2, v4, :cond_6

    if-nez p2, :cond_4

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->q()I

    move-result p2

    iget v5, p1, Lf/h/a/c/i1/r;->b:I

    sub-int/2addr v5, v0

    invoke-virtual {p1, v5}, Lf/h/a/c/i1/r;->C(I)V

    if-ne p2, v3, :cond_4

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/w;->f:Z

    return-void

    :cond_4
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result p2

    iget v3, p0, Lf/h/a/c/a1/e0/w;->d:I

    rsub-int/lit8 v3, v3, 0x3

    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v3, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    iget v5, p0, Lf/h/a/c/a1/e0/w;->d:I

    invoke-virtual {p1, v3, v5, p2}, Lf/h/a/c/i1/r;->d([BII)V

    iget v3, p0, Lf/h/a/c/a1/e0/w;->d:I

    add-int/2addr v3, p2

    iput v3, p0, Lf/h/a/c/a1/e0/w;->d:I

    if-ne v3, v4, :cond_3

    iget-object p2, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p2, v4}, Lf/h/a/c/i1/r;->y(I)V

    iget-object p2, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p2, v0}, Lf/h/a/c/i1/r;->D(I)V

    iget-object p2, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->q()I

    move-result p2

    iget-object v3, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v3

    and-int/lit16 v5, p2, 0x80

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    :goto_3
    iput-boolean v5, p0, Lf/h/a/c/a1/e0/w;->e:Z

    and-int/lit8 p2, p2, 0xf

    shl-int/lit8 p2, p2, 0x8

    or-int/2addr p2, v3

    add-int/2addr p2, v4

    iput p2, p0, Lf/h/a/c/a1/e0/w;->c:I

    iget-object v3, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    iget-object v5, v3, Lf/h/a/c/i1/r;->a:[B

    array-length v6, v5

    if-ge v6, p2, :cond_3

    const/16 v6, 0x1002

    array-length v7, v5

    mul-int/lit8 v7, v7, 0x2

    invoke-static {p2, v7}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-static {v6, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-virtual {v3, p2}, Lf/h/a/c/i1/r;->y(I)V

    iget-object p2, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    iget-object p2, p2, Lf/h/a/c/i1/r;->a:[B

    invoke-static {v5, v1, p2, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    :cond_6
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result p2

    iget v4, p0, Lf/h/a/c/a1/e0/w;->c:I

    iget v5, p0, Lf/h/a/c/a1/e0/w;->d:I

    sub-int/2addr v4, v5

    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v4, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    iget v5, p0, Lf/h/a/c/a1/e0/w;->d:I

    invoke-virtual {p1, v4, v5, p2}, Lf/h/a/c/i1/r;->d([BII)V

    iget v4, p0, Lf/h/a/c/a1/e0/w;->d:I

    add-int/2addr v4, p2

    iput v4, p0, Lf/h/a/c/a1/e0/w;->d:I

    iget p2, p0, Lf/h/a/c/a1/e0/w;->c:I

    if-ne v4, p2, :cond_3

    iget-boolean v4, p0, Lf/h/a/c/a1/e0/w;->e:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    sget v5, Lf/h/a/c/i1/a0;->a:I

    const/4 v5, 0x0

    const/4 v6, -0x1

    :goto_4
    if-ge v5, p2, :cond_7

    shl-int/lit8 v7, v6, 0x8

    sget-object v8, Lf/h/a/c/i1/a0;->k:[I

    ushr-int/lit8 v6, v6, 0x18

    aget-byte v9, v4, v5

    and-int/2addr v9, v3

    xor-int/2addr v6, v9

    and-int/2addr v6, v3

    aget v6, v8, v6

    xor-int/2addr v6, v7

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_7
    if-eqz v6, :cond_8

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/w;->f:Z

    return-void

    :cond_8
    iget-object p2, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    iget v3, p0, Lf/h/a/c/a1/e0/w;->c:I

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {p2, v3}, Lf/h/a/c/i1/r;->y(I)V

    goto :goto_5

    :cond_9
    iget-object v3, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v3, p2}, Lf/h/a/c/i1/r;->y(I)V

    :goto_5
    iget-object p2, p0, Lf/h/a/c/a1/e0/w;->a:Lf/h/a/c/a1/e0/v;

    iget-object v3, p0, Lf/h/a/c/a1/e0/w;->b:Lf/h/a/c/i1/r;

    invoke-interface {p2, v3}, Lf/h/a/c/a1/e0/v;->b(Lf/h/a/c/i1/r;)V

    iput v1, p0, Lf/h/a/c/a1/e0/w;->d:I

    goto/16 :goto_2

    :cond_a
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/w;->f:Z

    return-void
.end method
