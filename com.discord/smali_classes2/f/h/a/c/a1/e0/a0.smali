.class public final Lf/h/a/c/a1/e0/a0;
.super Ljava/lang/Object;
.source "TsDurationReader.java"


# instance fields
.field public final a:Lf/h/a/c/i1/z;

.field public final b:Lf/h/a/c/i1/r;

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:J

.field public g:J

.field public h:J


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/z;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/a/c/i1/z;-><init>(J)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/a0;->a:Lf/h/a/c/i1/z;

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lf/h/a/c/a1/e0/a0;->f:J

    iput-wide v0, p0, Lf/h/a/c/a1/e0/a0;->g:J

    iput-wide v0, p0, Lf/h/a/c/a1/e0/a0;->h:J

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/c/a1/e;)I
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/e0/a0;->b:Lf/h/a/c/i1/r;

    sget-object v1, Lf/h/a/c/i1/a0;->f:[B

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->z([B)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/a0;->c:Z

    const/4 v0, 0x0

    iput v0, p1, Lf/h/a/c/a1/e;->f:I

    return v0
.end method
