.class public final Lf/h/a/c/a1/e0/e;
.super Ljava/lang/Object;
.source "AdtsExtractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# instance fields
.field public final a:Lf/h/a/c/a1/e0/f;

.field public final b:Lf/h/a/c/i1/r;

.field public final c:Lf/h/a/c/i1/r;

.field public final d:Lf/h/a/c/i1/q;

.field public e:Lf/h/a/c/a1/i;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public f:J

.field public g:J

.field public h:I

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Lf/h/a/c/a1/e0/f;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lf/h/a/c/a1/e0/f;-><init>(ZLjava/lang/String;)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/e;->a:Lf/h/a/c/a1/e0/f;

    new-instance p1, Lf/h/a/c/i1/r;

    const/16 v0, 0x800

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/e;->b:Lf/h/a/c/i1/r;

    const/4 p1, -0x1

    iput p1, p0, Lf/h/a/c/a1/e0/e;->h:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/h/a/c/a1/e0/e;->g:J

    new-instance p1, Lf/h/a/c/i1/r;

    const/16 v0, 0xa

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/i1/q;

    iget-object p1, p1, Lf/h/a/c/i1/r;->a:[B

    invoke-direct {v0, p1}, Lf/h/a/c/i1/q;-><init>([B)V

    iput-object v0, p0, Lf/h/a/c/a1/e0/e;->d:Lf/h/a/c/i1/q;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/c/a1/e;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    const/16 v3, 0xa

    invoke-virtual {p1, v2, v0, v3, v0}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v2, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2, v0}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v2, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->s()I

    move-result v2

    const v3, 0x494433

    if-eq v2, v3, :cond_1

    iput v0, p1, Lf/h/a/c/a1/e;->f:I

    invoke-virtual {p1, v1, v0}, Lf/h/a/c/a1/e;->a(IZ)Z

    iget-wide v2, p0, Lf/h/a/c/a1/e0/e;->g:J

    const-wide/16 v4, -0x1

    cmp-long p1, v2, v4

    if-nez p1, :cond_0

    int-to-long v2, v1

    iput-wide v2, p0, Lf/h/a/c/a1/e0/e;->g:J

    :cond_0
    return v1

    :cond_1
    iget-object v2, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v2, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->p()I

    move-result v2

    add-int/lit8 v3, v2, 0xa

    add-int/2addr v1, v3

    invoke-virtual {p1, v2, v0}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto :goto_0
.end method

.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-wide v0, p1, Lf/h/a/c/a1/e;->c:J

    iget-object p2, p0, Lf/h/a/c/a1/e0/e;->b:Lf/h/a/c/i1/r;

    iget-object p2, p2, Lf/h/a/c/i1/r;->a:[B

    const/16 v0, 0x800

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1, v0}, Lf/h/a/c/a1/e;->f([BII)I

    move-result p1

    const/4 p2, -0x1

    const/4 v0, 0x1

    if-ne p1, p2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-boolean v3, p0, Lf/h/a/c/a1/e0/e;->j:Z

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    iget-object v5, p0, Lf/h/a/c/a1/e0/e;->e:Lf/h/a/c/a1/i;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Lf/h/a/c/a1/q$b;

    const-wide/16 v7, 0x0

    invoke-direct {v6, v3, v4, v7, v8}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {v5, v6}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/e;->j:Z

    :goto_1
    if-eqz v2, :cond_2

    return p2

    :cond_2
    iget-object p2, p0, Lf/h/a/c/a1/e0/e;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p2, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object p2, p0, Lf/h/a/c/a1/e0/e;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p2, p1}, Lf/h/a/c/i1/r;->B(I)V

    iget-boolean p1, p0, Lf/h/a/c/a1/e0/e;->i:Z

    if-nez p1, :cond_3

    iget-object p1, p0, Lf/h/a/c/a1/e0/e;->a:Lf/h/a/c/a1/e0/f;

    iget-wide v2, p0, Lf/h/a/c/a1/e0/e;->f:J

    iput-wide v2, p1, Lf/h/a/c/a1/e0/f;->s:J

    iput-boolean v0, p0, Lf/h/a/c/a1/e0/e;->i:Z

    :cond_3
    iget-object p1, p0, Lf/h/a/c/a1/e0/e;->a:Lf/h/a/c/a1/e0/f;

    iget-object p2, p0, Lf/h/a/c/a1/e0/e;->b:Lf/h/a/c/i1/r;

    invoke-virtual {p1, p2}, Lf/h/a/c/a1/e0/f;->b(Lf/h/a/c/i1/r;)V

    return v1
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 5

    iput-object p1, p0, Lf/h/a/c/a1/e0/e;->e:Lf/h/a/c/a1/i;

    iget-object v0, p0, Lf/h/a/c/a1/e0/e;->a:Lf/h/a/c/a1/e0/f;

    new-instance v1, Lf/h/a/c/a1/e0/c0$d;

    const/high16 v2, -0x80000000

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lf/h/a/c/a1/e0/c0$d;-><init>(III)V

    invoke-virtual {v0, p1, v1}, Lf/h/a/c/a1/e0/f;->e(Lf/h/a/c/a1/i;Lf/h/a/c/a1/e0/c0$d;)V

    invoke-interface {p1}, Lf/h/a/c/a1/i;->k()V

    return-void
.end method

.method public f(JJ)V
    .locals 0

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/c/a1/e0/e;->i:Z

    iget-object p1, p0, Lf/h/a/c/a1/e0/e;->a:Lf/h/a/c/a1/e0/f;

    invoke-virtual {p1}, Lf/h/a/c/a1/e0/f;->c()V

    iput-wide p3, p0, Lf/h/a/c/a1/e0/e;->f:J

    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lf/h/a/c/a1/e0/e;->a(Lf/h/a/c/a1/e;)I

    move-result v0

    const/4 v1, 0x0

    move v3, v0

    :goto_0
    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_1
    iget-object v5, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    const/4 v6, 0x2

    invoke-virtual {p1, v5, v1, v6, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v5, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v5, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->v()I

    move-result v5

    invoke-static {v5}, Lf/h/a/c/a1/e0/f;->g(I)Z

    move-result v5

    if-nez v5, :cond_1

    iput v1, p1, Lf/h/a/c/a1/e;->f:I

    add-int/lit8 v3, v3, 0x1

    sub-int v2, v3, v0

    const/16 v4, 0x2000

    if-lt v2, v4, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1, v3, v1}, Lf/h/a/c/a1/e;->a(IZ)Z

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    add-int/2addr v2, v5

    const/4 v6, 0x4

    if-lt v2, v6, :cond_2

    const/16 v7, 0xbc

    if-le v4, v7, :cond_2

    return v5

    :cond_2
    iget-object v5, p0, Lf/h/a/c/a1/e0/e;->c:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {p1, v5, v1, v6, v1}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v5, p0, Lf/h/a/c/a1/e0/e;->d:Lf/h/a/c/i1/q;

    const/16 v6, 0xe

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/q;->j(I)V

    iget-object v5, p0, Lf/h/a/c/a1/e0/e;->d:Lf/h/a/c/i1/q;

    const/16 v6, 0xd

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    const/4 v6, 0x6

    if-gt v5, v6, :cond_3

    return v1

    :cond_3
    add-int/lit8 v6, v5, -0x6

    invoke-virtual {p1, v6, v1}, Lf/h/a/c/a1/e;->a(IZ)Z

    add-int/2addr v4, v5

    goto :goto_1
.end method

.method public release()V
    .locals 0

    return-void
.end method
