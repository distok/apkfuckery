.class public final Lf/h/a/c/a1/l;
.super Ljava/lang/Object;
.source "FlacSeekTableSeekMap.java"

# interfaces
.implements Lf/h/a/c/a1/q;


# instance fields
.field public final a:Lf/h/a/c/i1/l;

.field public final b:J


# direct methods
.method public constructor <init>(Lf/h/a/c/i1/l;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/l;->a:Lf/h/a/c/i1/l;

    iput-wide p2, p0, Lf/h/a/c/a1/l;->b:J

    return-void
.end method


# virtual methods
.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final d(JJ)Lf/h/a/c/a1/r;
    .locals 2

    const-wide/32 v0, 0xf4240

    mul-long p1, p1, v0

    iget-object v0, p0, Lf/h/a/c/a1/l;->a:Lf/h/a/c/i1/l;

    iget v0, v0, Lf/h/a/c/i1/l;->e:I

    int-to-long v0, v0

    div-long/2addr p1, v0

    iget-wide v0, p0, Lf/h/a/c/a1/l;->b:J

    add-long/2addr v0, p3

    new-instance p3, Lf/h/a/c/a1/r;

    invoke-direct {p3, p1, p2, v0, v1}, Lf/h/a/c/a1/r;-><init>(JJ)V

    return-object p3
.end method

.method public g(J)Lf/h/a/c/a1/q$a;
    .locals 9

    iget-object v0, p0, Lf/h/a/c/a1/l;->a:Lf/h/a/c/i1/l;

    iget-object v0, v0, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/c/a1/l;->a:Lf/h/a/c/i1/l;

    iget-object v1, v0, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    iget-object v2, v1, Lf/h/a/c/i1/l$a;->a:[J

    iget-object v1, v1, Lf/h/a/c/i1/l$a;->b:[J

    invoke-virtual {v0, p1, p2}, Lf/h/a/c/i1/l;->g(J)J

    move-result-wide v3

    const/4 v0, 0x0

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5, v0}, Lf/h/a/c/i1/a0;->c([JJZZ)I

    move-result v0

    const-wide/16 v3, 0x0

    const/4 v6, -0x1

    if-ne v0, v6, :cond_0

    move-wide v7, v3

    goto :goto_0

    :cond_0
    aget-wide v7, v2, v0

    :goto_0
    if-ne v0, v6, :cond_1

    goto :goto_1

    :cond_1
    aget-wide v3, v1, v0

    :goto_1
    invoke-virtual {p0, v7, v8, v3, v4}, Lf/h/a/c/a1/l;->d(JJ)Lf/h/a/c/a1/r;

    move-result-object v3

    iget-wide v6, v3, Lf/h/a/c/a1/r;->a:J

    cmp-long v4, v6, p1

    if-eqz v4, :cond_3

    array-length p1, v2

    sub-int/2addr p1, v5

    if-ne v0, p1, :cond_2

    goto :goto_2

    :cond_2
    add-int/2addr v0, v5

    aget-wide p1, v2, v0

    aget-wide v0, v1, v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lf/h/a/c/a1/l;->d(JJ)Lf/h/a/c/a1/r;

    move-result-object p1

    new-instance p2, Lf/h/a/c/a1/q$a;

    invoke-direct {p2, v3, p1}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;Lf/h/a/c/a1/r;)V

    return-object p2

    :cond_3
    :goto_2
    new-instance p1, Lf/h/a/c/a1/q$a;

    invoke-direct {p1, v3}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;)V

    return-object p1
.end method

.method public i()J
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/l;->a:Lf/h/a/c/i1/l;

    invoke-virtual {v0}, Lf/h/a/c/i1/l;->d()J

    move-result-wide v0

    return-wide v0
.end method
