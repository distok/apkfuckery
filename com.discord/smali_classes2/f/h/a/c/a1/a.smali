.class public abstract Lf/h/a/c/a1/a;
.super Ljava/lang/Object;
.source "BinarySearchSeeker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/a$a;,
        Lf/h/a/c/a1/a$e;,
        Lf/h/a/c/a1/a$c;,
        Lf/h/a/c/a1/a$d;,
        Lf/h/a/c/a1/a$b;,
        Lf/h/a/c/a1/a$f;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/a$a;

.field public final b:Lf/h/a/c/a1/a$f;

.field public c:Lf/h/a/c/a1/a$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final d:I


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/a$d;Lf/h/a/c/a1/a$f;JJJJJJI)V
    .locals 16

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v1, p2

    iput-object v1, v0, Lf/h/a/c/a1/a;->b:Lf/h/a/c/a1/a$f;

    move/from16 v1, p15

    iput v1, v0, Lf/h/a/c/a1/a;->d:I

    new-instance v15, Lf/h/a/c/a1/a$a;

    move-object v1, v15

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    move-wide/from16 v11, p11

    move-wide/from16 v13, p13

    invoke-direct/range {v1 .. v14}, Lf/h/a/c/a1/a$a;-><init>(Lf/h/a/c/a1/a$d;JJJJJJ)V

    iput-object v15, v0, Lf/h/a/c/a1/a;->a:Lf/h/a/c/a1/a$a;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    iget-object v3, v0, Lf/h/a/c/a1/a;->b:Lf/h/a/c/a1/a$f;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v4, v0, Lf/h/a/c/a1/a;->c:Lf/h/a/c/a1/a$c;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v5, v4, Lf/h/a/c/a1/a$c;->f:J

    iget-wide v7, v4, Lf/h/a/c/a1/a$c;->g:J

    iget-wide v9, v4, Lf/h/a/c/a1/a$c;->h:J

    sub-long/2addr v7, v5

    iget v11, v0, Lf/h/a/c/a1/a;->d:I

    int-to-long v11, v11

    const/4 v13, 0x0

    cmp-long v14, v7, v11

    if-gtz v14, :cond_0

    invoke-virtual {v0, v13, v5, v6}, Lf/h/a/c/a1/a;->c(ZJ)V

    invoke-virtual {v0, v1, v5, v6, v2}, Lf/h/a/c/a1/a;->d(Lf/h/a/c/a1/e;JLf/h/a/c/a1/p;)I

    move-result v1

    return v1

    :cond_0
    invoke-virtual {v0, v1, v9, v10}, Lf/h/a/c/a1/a;->f(Lf/h/a/c/a1/e;J)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0, v1, v9, v10, v2}, Lf/h/a/c/a1/a;->d(Lf/h/a/c/a1/e;JLf/h/a/c/a1/p;)I

    move-result v1

    return v1

    :cond_1
    iput v13, v1, Lf/h/a/c/a1/e;->f:I

    iget-wide v5, v4, Lf/h/a/c/a1/a$c;->b:J

    invoke-interface {v3, v1, v5, v6}, Lf/h/a/c/a1/a$f;->a(Lf/h/a/c/a1/e;J)Lf/h/a/c/a1/a$e;

    move-result-object v5

    iget v6, v5, Lf/h/a/c/a1/a$e;->a:I

    const/4 v7, -0x3

    if-eq v6, v7, :cond_5

    const/4 v7, -0x2

    if-eq v6, v7, :cond_4

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    if-nez v6, :cond_2

    const/4 v3, 0x1

    iget-wide v6, v5, Lf/h/a/c/a1/a$e;->c:J

    invoke-virtual {v0, v3, v6, v7}, Lf/h/a/c/a1/a;->c(ZJ)V

    iget-wide v3, v5, Lf/h/a/c/a1/a$e;->c:J

    invoke-virtual {v0, v1, v3, v4}, Lf/h/a/c/a1/a;->f(Lf/h/a/c/a1/e;J)Z

    iget-wide v3, v5, Lf/h/a/c/a1/a$e;->c:J

    invoke-virtual {v0, v1, v3, v4, v2}, Lf/h/a/c/a1/a;->d(Lf/h/a/c/a1/e;JLf/h/a/c/a1/p;)I

    move-result v1

    return v1

    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid case"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    iget-wide v9, v5, Lf/h/a/c/a1/a$e;->b:J

    iget-wide v13, v5, Lf/h/a/c/a1/a$e;->c:J

    iput-wide v9, v4, Lf/h/a/c/a1/a$c;->e:J

    iput-wide v13, v4, Lf/h/a/c/a1/a$c;->g:J

    iget-wide v5, v4, Lf/h/a/c/a1/a$c;->b:J

    iget-wide v7, v4, Lf/h/a/c/a1/a$c;->d:J

    iget-wide v11, v4, Lf/h/a/c/a1/a$c;->f:J

    iget-wide v1, v4, Lf/h/a/c/a1/a$c;->c:J

    move-wide v15, v1

    invoke-static/range {v5 .. v16}, Lf/h/a/c/a1/a$c;->a(JJJJJJ)J

    move-result-wide v1

    iput-wide v1, v4, Lf/h/a/c/a1/a$c;->h:J

    goto :goto_1

    :cond_4
    iget-wide v7, v5, Lf/h/a/c/a1/a$e;->b:J

    iget-wide v11, v5, Lf/h/a/c/a1/a$e;->c:J

    iput-wide v7, v4, Lf/h/a/c/a1/a$c;->d:J

    iput-wide v11, v4, Lf/h/a/c/a1/a$c;->f:J

    iget-wide v5, v4, Lf/h/a/c/a1/a$c;->b:J

    iget-wide v9, v4, Lf/h/a/c/a1/a$c;->e:J

    iget-wide v13, v4, Lf/h/a/c/a1/a$c;->g:J

    iget-wide v1, v4, Lf/h/a/c/a1/a$c;->c:J

    move-wide v15, v1

    invoke-static/range {v5 .. v16}, Lf/h/a/c/a1/a$c;->a(JJJJJJ)J

    move-result-wide v1

    iput-wide v1, v4, Lf/h/a/c/a1/a$c;->h:J

    :goto_1
    move-object/from16 v1, p1

    move-object/from16 v2, p2

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v0, v13, v9, v10}, Lf/h/a/c/a1/a;->c(ZJ)V

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v9, v10, v2}, Lf/h/a/c/a1/a;->d(Lf/h/a/c/a1/e;JLf/h/a/c/a1/p;)I

    move-result v1

    return v1
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/c/a1/a;->c:Lf/h/a/c/a1/a$c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final c(ZJ)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/a1/a;->c:Lf/h/a/c/a1/a$c;

    iget-object p1, p0, Lf/h/a/c/a1/a;->b:Lf/h/a/c/a1/a$f;

    invoke-interface {p1}, Lf/h/a/c/a1/a$f;->b()V

    return-void
.end method

.method public final d(Lf/h/a/c/a1/e;JLf/h/a/c/a1/p;)I
    .locals 2

    iget-wide v0, p1, Lf/h/a/c/a1/e;->d:J

    cmp-long p1, p2, v0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iput-wide p2, p4, Lf/h/a/c/a1/p;->a:J

    const/4 p1, 0x1

    return p1
.end method

.method public final e(J)V
    .locals 19

    move-object/from16 v0, p0

    move-wide/from16 v2, p1

    iget-object v1, v0, Lf/h/a/c/a1/a;->c:Lf/h/a/c/a1/a$c;

    if-eqz v1, :cond_0

    iget-wide v4, v1, Lf/h/a/c/a1/a$c;->a:J

    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    return-void

    :cond_0
    new-instance v14, Lf/h/a/c/a1/a$c;

    iget-object v1, v0, Lf/h/a/c/a1/a;->a:Lf/h/a/c/a1/a$a;

    iget-object v1, v1, Lf/h/a/c/a1/a$a;->a:Lf/h/a/c/a1/a$d;

    invoke-interface {v1, v2, v3}, Lf/h/a/c/a1/a$d;->a(J)J

    move-result-wide v4

    iget-object v1, v0, Lf/h/a/c/a1/a;->a:Lf/h/a/c/a1/a$a;

    iget-wide v6, v1, Lf/h/a/c/a1/a$a;->c:J

    iget-wide v8, v1, Lf/h/a/c/a1/a$a;->d:J

    iget-wide v10, v1, Lf/h/a/c/a1/a$a;->e:J

    iget-wide v12, v1, Lf/h/a/c/a1/a$a;->f:J

    move-wide v15, v12

    iget-wide v12, v1, Lf/h/a/c/a1/a$a;->g:J

    move-object v1, v14

    move-wide/from16 v2, p1

    move-wide/from16 v17, v12

    move-wide v12, v15

    move-object v0, v14

    move-wide/from16 v14, v17

    invoke-direct/range {v1 .. v15}, Lf/h/a/c/a1/a$c;-><init>(JJJJJJJ)V

    move-object v1, v0

    move-object/from16 v0, p0

    iput-object v1, v0, Lf/h/a/c/a1/a;->c:Lf/h/a/c/a1/a$c;

    return-void
.end method

.method public final f(Lf/h/a/c/a1/e;J)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-wide v0, p1, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr p2, v0

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-ltz v2, :cond_0

    const-wide/32 v0, 0x40000

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    long-to-int p3, p2

    invoke-virtual {p1, p3}, Lf/h/a/c/a1/e;->i(I)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
