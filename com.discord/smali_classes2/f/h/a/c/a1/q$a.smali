.class public final Lf/h/a/c/a1/q$a;
.super Ljava/lang/Object;
.source "SeekMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/r;

.field public final b:Lf/h/a/c/a1/r;


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/r;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    iput-object p1, p0, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    return-void
.end method

.method public constructor <init>(Lf/h/a/c/a1/r;Lf/h/a/c/a1/r;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    iput-object p2, p0, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Lf/h/a/c/a1/q$a;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lf/h/a/c/a1/q$a;

    iget-object v2, p0, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    iget-object v3, p1, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    invoke-virtual {v2, v3}, Lf/h/a/c/a1/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    iget-object p1, p1, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    invoke-virtual {v2, p1}, Lf/h/a/c/a1/r;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    invoke-virtual {v0}, Lf/h/a/c/a1/r;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    invoke-virtual {v1}, Lf/h/a/c/a1/r;->hashCode()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "["

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    iget-object v2, p0, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    const-string v1, ", "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const-string v2, "]"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
