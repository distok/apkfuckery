.class public final Lf/h/a/c/a1/c0/b$c;
.super Ljava/lang/Object;
.source "AtomParsers.java"

# interfaces
.implements Lf/h/a/c/a1/c0/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/c0/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/i1/r;

.field public final b:I

.field public final c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/c0/a$b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object p1, p1, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    iput-object p1, p0, Lf/h/a/c/a1/c0/b$c;->a:Lf/h/a/c/i1/r;

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->t()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lf/h/a/c/a1/c0/b$c;->c:I

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->t()I

    move-result p1

    iput p1, p0, Lf/h/a/c/a1/c0/b$c;->b:I

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lf/h/a/c/a1/c0/b$c;->b:I

    return v0
.end method

.method public c()I
    .locals 2

    iget v0, p0, Lf/h/a/c/a1/c0/b$c;->c:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lf/h/a/c/a1/c0/b$c;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->q()I

    move-result v0

    return v0

    :cond_0
    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lf/h/a/c/a1/c0/b$c;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->v()I

    move-result v0

    return v0

    :cond_1
    iget v0, p0, Lf/h/a/c/a1/c0/b$c;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lf/h/a/c/a1/c0/b$c;->d:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/a1/c0/b$c;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->q()I

    move-result v0

    iput v0, p0, Lf/h/a/c/a1/c0/b$c;->e:I

    and-int/lit16 v0, v0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    return v0

    :cond_2
    iget v0, p0, Lf/h/a/c/a1/c0/b$c;->e:I

    and-int/lit8 v0, v0, 0xf

    return v0
.end method
