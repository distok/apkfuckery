.class public final Lf/h/a/c/a1/c0/d$b;
.super Ljava/lang/Object;
.source "FragmentedMp4Extractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/c0/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/s;

.field public final b:Lf/h/a/c/a1/c0/k;

.field public final c:Lf/h/a/c/i1/r;

.field public d:Lf/h/a/c/a1/c0/i;

.field public e:Lf/h/a/c/a1/c0/c;

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public final j:Lf/h/a/c/i1/r;

.field public final k:Lf/h/a/c/i1/r;


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/s;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    new-instance p1, Lf/h/a/c/a1/c0/k;

    invoke-direct {p1}, Lf/h/a/c/a1/c0/k;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d$b;->c:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d$b;->j:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d$b;->k:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public final a()Lf/h/a/c/a1/c0/j;
    .locals 2

    iget-object v0, p0, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v1, v0, Lf/h/a/c/a1/c0/k;->a:Lf/h/a/c/a1/c0/c;

    iget v1, v1, Lf/h/a/c/a1/c0/c;->a:I

    iget-object v0, v0, Lf/h/a/c/a1/c0/k;->n:Lf/h/a/c/a1/c0/j;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    invoke-virtual {v0, v1}, Lf/h/a/c/a1/c0/i;->a(I)Lf/h/a/c/a1/c0/j;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lf/h/a/c/a1/c0/j;->a:Z

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method public b(Lf/h/a/c/a1/c0/i;Lf/h/a/c/a1/c0/c;)V
    .locals 0

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lf/h/a/c/a1/c0/d$b;->e:Lf/h/a/c/a1/c0/c;

    iget-object p2, p0, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    iget-object p1, p1, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    invoke-interface {p2, p1}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    invoke-virtual {p0}, Lf/h/a/c/a1/c0/d$b;->e()V

    return-void
.end method

.method public c()Z
    .locals 4

    iget v0, p0, Lf/h/a/c/a1/c0/d$b;->f:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lf/h/a/c/a1/c0/d$b;->f:I

    iget v0, p0, Lf/h/a/c/a1/c0/d$b;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lf/h/a/c/a1/c0/d$b;->g:I

    iget-object v2, p0, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v2, v2, Lf/h/a/c/a1/c0/k;->g:[I

    iget v3, p0, Lf/h/a/c/a1/c0/d$b;->h:I

    aget v2, v2, v3

    if-ne v0, v2, :cond_0

    add-int/2addr v3, v1

    iput v3, p0, Lf/h/a/c/a1/c0/d$b;->h:I

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/c0/d$b;->g:I

    return v0

    :cond_0
    return v1
.end method

.method public d(II)I
    .locals 10

    invoke-virtual {p0}, Lf/h/a/c/a1/c0/d$b;->a()Lf/h/a/c/a1/c0/j;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v2, v0, Lf/h/a/c/a1/c0/j;->d:I

    if-eqz v2, :cond_1

    iget-object v0, p0, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v0, v0, Lf/h/a/c/a1/c0/k;->p:Lf/h/a/c/i1/r;

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lf/h/a/c/a1/c0/j;->e:[B

    iget-object v2, p0, Lf/h/a/c/a1/c0/d$b;->k:Lf/h/a/c/i1/r;

    array-length v3, v0

    iput-object v0, v2, Lf/h/a/c/i1/r;->a:[B

    iput v3, v2, Lf/h/a/c/i1/r;->c:I

    iput v1, v2, Lf/h/a/c/i1/r;->b:I

    array-length v0, v0

    move-object v9, v2

    move v2, v0

    move-object v0, v9

    :goto_0
    iget-object v3, p0, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget v4, p0, Lf/h/a/c/a1/c0/d$b;->f:I

    iget-boolean v5, v3, Lf/h/a/c/a1/c0/k;->l:Z

    const/4 v6, 0x1

    if-eqz v5, :cond_2

    iget-object v3, v3, Lf/h/a/c/a1/c0/k;->m:[Z

    aget-boolean v3, v3, v4

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-nez v3, :cond_4

    if-eqz p2, :cond_3

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v4, 0x1

    :goto_3
    iget-object v5, p0, Lf/h/a/c/a1/c0/d$b;->j:Lf/h/a/c/i1/r;

    iget-object v7, v5, Lf/h/a/c/i1/r;->a:[B

    if-eqz v4, :cond_5

    const/16 v8, 0x80

    goto :goto_4

    :cond_5
    const/4 v8, 0x0

    :goto_4
    or-int/2addr v8, v2

    int-to-byte v8, v8

    aput-byte v8, v7, v1

    invoke-virtual {v5, v1}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v5, p0, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    iget-object v7, p0, Lf/h/a/c/a1/c0/d$b;->j:Lf/h/a/c/i1/r;

    invoke-interface {v5, v7, v6}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget-object v5, p0, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    invoke-interface {v5, v0, v2}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    if-nez v4, :cond_6

    add-int/2addr v2, v6

    return v2

    :cond_6
    const/4 v0, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x6

    const/16 v7, 0x8

    if-nez v3, :cond_7

    iget-object v3, p0, Lf/h/a/c/a1/c0/d$b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v3, v7}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v3, p0, Lf/h/a/c/a1/c0/d$b;->c:Lf/h/a/c/i1/r;

    iget-object v8, v3, Lf/h/a/c/i1/r;->a:[B

    aput-byte v1, v8, v1

    aput-byte v6, v8, v6

    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v8, v4

    and-int/lit16 p2, p2, 0xff

    int-to-byte p2, p2

    aput-byte p2, v8, v0

    const/4 p2, 0x4

    shr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v8, p2

    const/4 p2, 0x5

    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v8, p2

    shr-int/lit8 p2, p1, 0x8

    and-int/lit16 p2, p2, 0xff

    int-to-byte p2, p2

    aput-byte p2, v8, v5

    const/4 p2, 0x7

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    aput-byte p1, v8, p2

    iget-object p1, p0, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    invoke-interface {p1, v3, v7}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    add-int/2addr v2, v6

    add-int/2addr v2, v7

    return v2

    :cond_7
    iget-object p1, p0, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object p1, p1, Lf/h/a/c/a1/c0/k;->p:Lf/h/a/c/i1/r;

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->v()I

    move-result v3

    const/4 v8, -0x2

    invoke-virtual {p1, v8}, Lf/h/a/c/i1/r;->D(I)V

    mul-int/lit8 v3, v3, 0x6

    add-int/2addr v3, v4

    if-eqz p2, :cond_8

    iget-object v5, p0, Lf/h/a/c/a1/c0/d$b;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v3}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v5, p0, Lf/h/a/c/a1/c0/d$b;->c:Lf/h/a/c/i1/r;

    iget-object v8, p1, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v5, v8, v1, v3}, Lf/h/a/c/i1/r;->d([BII)V

    invoke-virtual {p1, v3}, Lf/h/a/c/i1/r;->D(I)V

    iget-object p1, p0, Lf/h/a/c/a1/c0/d$b;->c:Lf/h/a/c/i1/r;

    iget-object v1, p1, Lf/h/a/c/i1/r;->a:[B

    aget-byte v5, v1, v4

    and-int/lit16 v5, v5, 0xff

    shl-int/2addr v5, v7

    aget-byte v7, v1, v0

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v5, v7

    add-int/2addr v5, p2

    shr-int/lit8 p2, v5, 0x8

    and-int/lit16 p2, p2, 0xff

    int-to-byte p2, p2

    aput-byte p2, v1, v4

    and-int/lit16 p2, v5, 0xff

    int-to-byte p2, p2

    aput-byte p2, v1, v0

    :cond_8
    iget-object p2, p0, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    invoke-interface {p2, p1, v3}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    add-int/2addr v2, v6

    add-int/2addr v2, v3

    return v2
.end method

.method public e()V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    const/4 v1, 0x0

    iput v1, v0, Lf/h/a/c/a1/c0/k;->d:I

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lf/h/a/c/a1/c0/k;->r:J

    iput-boolean v1, v0, Lf/h/a/c/a1/c0/k;->l:Z

    iput-boolean v1, v0, Lf/h/a/c/a1/c0/k;->q:Z

    const/4 v2, 0x0

    iput-object v2, v0, Lf/h/a/c/a1/c0/k;->n:Lf/h/a/c/a1/c0/j;

    iput v1, p0, Lf/h/a/c/a1/c0/d$b;->f:I

    iput v1, p0, Lf/h/a/c/a1/c0/d$b;->h:I

    iput v1, p0, Lf/h/a/c/a1/c0/d$b;->g:I

    iput v1, p0, Lf/h/a/c/a1/c0/d$b;->i:I

    return-void
.end method
