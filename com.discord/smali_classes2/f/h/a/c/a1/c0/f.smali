.class public final Lf/h/a/c/a1/c0/f;
.super Ljava/lang/Object;
.source "Mp4Extractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;
.implements Lf/h/a/c/a1/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/c0/f$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/i1/r;

.field public final b:Lf/h/a/c/i1/r;

.field public final c:Lf/h/a/c/i1/r;

.field public final d:Lf/h/a/c/i1/r;

.field public final e:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lf/h/a/c/a1/c0/a$a;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:I

.field public h:J

.field public i:I

.field public j:Lf/h/a/c/i1/r;

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:Lf/h/a/c/a1/i;

.field public p:[Lf/h/a/c/a1/c0/f$a;

.field public q:[[J

.field public r:I

.field public s:J

.field public t:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Lf/h/a/c/i1/r;

    const/16 v0, 0x10

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    new-instance p1, Lf/h/a/c/i1/r;

    sget-object v0, Lf/h/a/c/i1/p;->a:[B

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object p1, p0, Lf/h/a/c/a1/c0/f;->a:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    const/4 v0, 0x4

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/c0/f;->b:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/f;->c:Lf/h/a/c/i1/r;

    const/4 p1, -0x1

    iput p1, p0, Lf/h/a/c/a1/c0/f;->k:I

    return-void
.end method

.method public static k(Lf/h/a/c/a1/c0/l;JJ)J
    .locals 2

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/a1/c0/l;->a(J)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/a1/c0/l;->b(J)I

    move-result v0

    :cond_0
    if-ne v0, v1, :cond_1

    return-wide p3

    :cond_1
    iget-object p0, p0, Lf/h/a/c/a1/c0/l;->c:[J

    aget-wide p1, p0, v0

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p0

    return-wide p0
.end method


# virtual methods
.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 30
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    :cond_0
    iget v3, v0, Lf/h/a/c/a1/c0/f;->f:I

    const v4, 0x66747970

    const-wide/16 v5, 0x0

    const/16 v7, 0x8

    const/4 v9, -0x1

    const/4 v11, 0x1

    if-eqz v3, :cond_1f

    const-wide/32 v12, 0x40000

    const/4 v14, 0x2

    if-eq v3, v11, :cond_16

    if-ne v3, v14, :cond_15

    iget-wide v3, v1, Lf/h/a/c/a1/e;->d:J

    iget v7, v0, Lf/h/a/c/a1/c0/f;->k:I

    if-ne v7, v9, :cond_b

    const-wide v15, 0x7fffffffffffffffL

    move-wide/from16 v17, v15

    move-wide/from16 v20, v17

    move-wide/from16 v24, v20

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/16 v19, 0x1

    const/16 v22, -0x1

    const/16 v23, -0x1

    :goto_0
    iget-object v14, v0, Lf/h/a/c/a1/c0/f;->p:[Lf/h/a/c/a1/c0/f$a;

    array-length v10, v14

    if-ge v7, v10, :cond_8

    aget-object v10, v14, v7

    iget v14, v10, Lf/h/a/c/a1/c0/f$a;->d:I

    iget-object v10, v10, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    iget v11, v10, Lf/h/a/c/a1/c0/l;->b:I

    if-ne v14, v11, :cond_1

    goto :goto_3

    :cond_1
    iget-object v10, v10, Lf/h/a/c/a1/c0/l;->c:[J

    aget-wide v26, v10, v14

    iget-object v10, v0, Lf/h/a/c/a1/c0/f;->q:[[J

    aget-object v10, v10, v7

    aget-wide v28, v10, v14

    sub-long v26, v26, v3

    cmp-long v10, v26, v5

    if-ltz v10, :cond_3

    cmp-long v10, v26, v12

    if-ltz v10, :cond_2

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v10, 0x1

    :goto_2
    if-nez v10, :cond_4

    if-nez v8, :cond_5

    :cond_4
    if-ne v10, v8, :cond_6

    cmp-long v11, v26, v24

    if-gez v11, :cond_6

    :cond_5
    move/from16 v23, v7

    move v8, v10

    move-wide/from16 v24, v26

    move-wide/from16 v20, v28

    :cond_6
    cmp-long v11, v28, v17

    if-gez v11, :cond_7

    move/from16 v22, v7

    move/from16 v19, v10

    move-wide/from16 v17, v28

    :cond_7
    :goto_3
    add-int/lit8 v7, v7, 0x1

    const/4 v11, 0x1

    goto :goto_0

    :cond_8
    cmp-long v7, v17, v15

    if-eqz v7, :cond_a

    if-eqz v19, :cond_a

    const-wide/32 v7, 0xa00000

    add-long v17, v17, v7

    cmp-long v7, v20, v17

    if-gez v7, :cond_9

    goto :goto_4

    :cond_9
    move/from16 v7, v22

    goto :goto_5

    :cond_a
    :goto_4
    move/from16 v7, v23

    :goto_5
    iput v7, v0, Lf/h/a/c/a1/c0/f;->k:I

    if-ne v7, v9, :cond_b

    goto/16 :goto_b

    :cond_b
    iget-object v7, v0, Lf/h/a/c/a1/c0/f;->p:[Lf/h/a/c/a1/c0/f$a;

    iget v8, v0, Lf/h/a/c/a1/c0/f;->k:I

    aget-object v7, v7, v8

    iget-object v14, v7, Lf/h/a/c/a1/c0/f$a;->c:Lf/h/a/c/a1/s;

    iget v8, v7, Lf/h/a/c/a1/c0/f$a;->d:I

    iget-object v10, v7, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    iget-object v11, v10, Lf/h/a/c/a1/c0/l;->c:[J

    aget-wide v12, v11, v8

    iget-object v10, v10, Lf/h/a/c/a1/c0/l;->d:[I

    aget v10, v10, v8

    sub-long v3, v12, v3

    iget v11, v0, Lf/h/a/c/a1/c0/f;->l:I

    move/from16 v17, v10

    int-to-long v9, v11

    add-long/2addr v3, v9

    cmp-long v9, v3, v5

    if-ltz v9, :cond_14

    const-wide/32 v5, 0x40000

    cmp-long v9, v3, v5

    if-ltz v9, :cond_c

    goto/16 :goto_a

    :cond_c
    iget-object v2, v7, Lf/h/a/c/a1/c0/f$a;->a:Lf/h/a/c/a1/c0/i;

    iget v2, v2, Lf/h/a/c/a1/c0/i;->g:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_d

    const-wide/16 v5, 0x8

    add-long/2addr v3, v5

    add-int/lit8 v10, v17, -0x8

    goto :goto_6

    :cond_d
    move/from16 v10, v17

    :goto_6
    long-to-int v2, v3

    invoke-virtual {v1, v2}, Lf/h/a/c/a1/e;->i(I)V

    iget-object v2, v7, Lf/h/a/c/a1/c0/f$a;->a:Lf/h/a/c/a1/c0/i;

    iget v3, v2, Lf/h/a/c/a1/c0/i;->j:I

    if-eqz v3, :cond_10

    iget-object v2, v0, Lf/h/a/c/a1/c0/f;->b:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    const/4 v4, 0x0

    aput-byte v4, v2, v4

    const/4 v5, 0x1

    aput-byte v4, v2, v5

    const/4 v5, 0x2

    aput-byte v4, v2, v5

    rsub-int/lit8 v5, v3, 0x4

    :goto_7
    iget v6, v0, Lf/h/a/c/a1/c0/f;->m:I

    if-ge v6, v10, :cond_13

    iget v6, v0, Lf/h/a/c/a1/c0/f;->n:I

    if-nez v6, :cond_f

    invoke-virtual {v1, v2, v5, v3, v4}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v6, v0, Lf/h/a/c/a1/c0/f;->l:I

    add-int/2addr v6, v3

    iput v6, v0, Lf/h/a/c/a1/c0/f;->l:I

    iget-object v6, v0, Lf/h/a/c/a1/c0/f;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v6, v4}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v6, v0, Lf/h/a/c/a1/c0/f;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v6

    if-ltz v6, :cond_e

    iput v6, v0, Lf/h/a/c/a1/c0/f;->n:I

    iget-object v6, v0, Lf/h/a/c/a1/c0/f;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v6, v4}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v4, v0, Lf/h/a/c/a1/c0/f;->a:Lf/h/a/c/i1/r;

    const/4 v6, 0x4

    invoke-interface {v14, v4, v6}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v4, v0, Lf/h/a/c/a1/c0/f;->m:I

    add-int/2addr v4, v6

    iput v4, v0, Lf/h/a/c/a1/c0/f;->m:I

    add-int/2addr v10, v5

    goto :goto_8

    :cond_e
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Invalid NAL length"

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_f
    invoke-interface {v14, v1, v6, v4}, Lf/h/a/c/a1/s;->a(Lf/h/a/c/a1/e;IZ)I

    move-result v6

    iget v4, v0, Lf/h/a/c/a1/c0/f;->l:I

    add-int/2addr v4, v6

    iput v4, v0, Lf/h/a/c/a1/c0/f;->l:I

    iget v4, v0, Lf/h/a/c/a1/c0/f;->m:I

    add-int/2addr v4, v6

    iput v4, v0, Lf/h/a/c/a1/c0/f;->m:I

    iget v4, v0, Lf/h/a/c/a1/c0/f;->n:I

    sub-int/2addr v4, v6

    iput v4, v0, Lf/h/a/c/a1/c0/f;->n:I

    :goto_8
    const/4 v4, 0x0

    goto :goto_7

    :cond_10
    iget-object v2, v2, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget-object v2, v2, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v3, "audio/ac4"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    iget v2, v0, Lf/h/a/c/a1/c0/f;->m:I

    if-nez v2, :cond_11

    iget-object v2, v0, Lf/h/a/c/a1/c0/f;->c:Lf/h/a/c/i1/r;

    invoke-static {v10, v2}, Lf/h/a/c/w0/h;->a(ILf/h/a/c/i1/r;)V

    iget-object v2, v0, Lf/h/a/c/a1/c0/f;->c:Lf/h/a/c/i1/r;

    const/4 v3, 0x7

    invoke-interface {v14, v2, v3}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v2, v0, Lf/h/a/c/a1/c0/f;->m:I

    add-int/2addr v2, v3

    iput v2, v0, Lf/h/a/c/a1/c0/f;->m:I

    :cond_11
    add-int/lit8 v10, v10, 0x7

    :cond_12
    :goto_9
    iget v2, v0, Lf/h/a/c/a1/c0/f;->m:I

    if-ge v2, v10, :cond_13

    sub-int v2, v10, v2

    const/4 v3, 0x0

    invoke-interface {v14, v1, v2, v3}, Lf/h/a/c/a1/s;->a(Lf/h/a/c/a1/e;IZ)I

    move-result v2

    iget v3, v0, Lf/h/a/c/a1/c0/f;->l:I

    add-int/2addr v3, v2

    iput v3, v0, Lf/h/a/c/a1/c0/f;->l:I

    iget v3, v0, Lf/h/a/c/a1/c0/f;->m:I

    add-int/2addr v3, v2

    iput v3, v0, Lf/h/a/c/a1/c0/f;->m:I

    iget v3, v0, Lf/h/a/c/a1/c0/f;->n:I

    sub-int/2addr v3, v2

    iput v3, v0, Lf/h/a/c/a1/c0/f;->n:I

    goto :goto_9

    :cond_13
    move/from16 v18, v10

    iget-object v1, v7, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    iget-object v2, v1, Lf/h/a/c/a1/c0/l;->f:[J

    aget-wide v15, v2, v8

    iget-object v1, v1, Lf/h/a/c/a1/c0/l;->g:[I

    aget v17, v1, v8

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-interface/range {v14 .. v20}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    iget v1, v7, Lf/h/a/c/a1/c0/f$a;->d:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, v7, Lf/h/a/c/a1/c0/f$a;->d:I

    const/4 v1, -0x1

    iput v1, v0, Lf/h/a/c/a1/c0/f;->k:I

    const/4 v1, 0x0

    iput v1, v0, Lf/h/a/c/a1/c0/f;->l:I

    iput v1, v0, Lf/h/a/c/a1/c0/f;->m:I

    iput v1, v0, Lf/h/a/c/a1/c0/f;->n:I

    const/4 v9, 0x0

    goto :goto_b

    :cond_14
    :goto_a
    iput-wide v12, v2, Lf/h/a/c/a1/p;->a:J

    const/4 v9, 0x1

    :goto_b
    return v9

    :cond_15
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_16
    iget-wide v5, v0, Lf/h/a/c/a1/c0/f;->h:J

    iget v3, v0, Lf/h/a/c/a1/c0/f;->i:I

    int-to-long v8, v3

    sub-long/2addr v5, v8

    iget-wide v8, v1, Lf/h/a/c/a1/e;->d:J

    add-long/2addr v8, v5

    iget-object v10, v0, Lf/h/a/c/a1/c0/f;->j:Lf/h/a/c/i1/r;

    if-eqz v10, :cond_1b

    iget-object v10, v10, Lf/h/a/c/i1/r;->a:[B

    long-to-int v6, v5

    const/4 v5, 0x0

    invoke-virtual {v1, v10, v3, v6, v5}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v3, v0, Lf/h/a/c/a1/c0/f;->g:I

    if-ne v3, v4, :cond_1a

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v3, v7}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->e()I

    move-result v4

    const v5, 0x71742020

    if-ne v4, v5, :cond_17

    goto :goto_c

    :cond_17
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lf/h/a/c/i1/r;->D(I)V

    :cond_18
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->a()I

    move-result v4

    if-lez v4, :cond_19

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->e()I

    move-result v4

    if-ne v4, v5, :cond_18

    :goto_c
    const/4 v3, 0x1

    goto :goto_d

    :cond_19
    const/4 v3, 0x0

    :goto_d
    iput-boolean v3, v0, Lf/h/a/c/a1/c0/f;->t:Z

    goto :goto_e

    :cond_1a
    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1c

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/a1/c0/a$a;

    new-instance v4, Lf/h/a/c/a1/c0/a$b;

    iget v5, v0, Lf/h/a/c/a1/c0/f;->g:I

    iget-object v6, v0, Lf/h/a/c/a1/c0/f;->j:Lf/h/a/c/i1/r;

    invoke-direct {v4, v5, v6}, Lf/h/a/c/a1/c0/a$b;-><init>(ILf/h/a/c/i1/r;)V

    iget-object v3, v3, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    :cond_1b
    const-wide/32 v3, 0x40000

    cmp-long v7, v5, v3

    if-gez v7, :cond_1d

    long-to-int v3, v5

    invoke-virtual {v1, v3}, Lf/h/a/c/a1/e;->i(I)V

    :cond_1c
    :goto_e
    const/4 v3, 0x0

    goto :goto_f

    :cond_1d
    iput-wide v8, v2, Lf/h/a/c/a1/p;->a:J

    const/4 v3, 0x1

    :goto_f
    invoke-virtual {v0, v8, v9}, Lf/h/a/c/a1/c0/f;->l(J)V

    if-eqz v3, :cond_1e

    iget v3, v0, Lf/h/a/c/a1/c0/f;->f:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1e

    const/4 v10, 0x1

    goto :goto_10

    :cond_1e
    const/4 v10, 0x0

    :goto_10
    if-eqz v10, :cond_0

    const/4 v3, 0x1

    return v3

    :cond_1f
    const/4 v3, 0x1

    iget v8, v0, Lf/h/a/c/a1/c0/f;->i:I

    if-nez v8, :cond_21

    iget-object v8, v0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    iget-object v8, v8, Lf/h/a/c/i1/r;->a:[B

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9, v7, v3}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    move-result v8

    if-nez v8, :cond_20

    const/4 v10, 0x0

    goto/16 :goto_1b

    :cond_20
    iput v7, v0, Lf/h/a/c/a1/c0/f;->i:I

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    invoke-virtual {v3, v9}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v8

    iput-wide v8, v0, Lf/h/a/c/a1/c0/f;->h:J

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    iput v3, v0, Lf/h/a/c/a1/c0/f;->g:I

    :cond_21
    iget-wide v8, v0, Lf/h/a/c/a1/c0/f;->h:J

    const-wide/16 v10, 0x1

    cmp-long v3, v8, v10

    if-nez v3, :cond_22

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v7, v7, v5}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v3, v0, Lf/h/a/c/a1/c0/f;->i:I

    add-int/2addr v3, v7

    iput v3, v0, Lf/h/a/c/a1/c0/f;->i:I

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v5

    iput-wide v5, v0, Lf/h/a/c/a1/c0/f;->h:J

    goto :goto_11

    :cond_22
    cmp-long v3, v8, v5

    if-nez v3, :cond_24

    iget-wide v5, v1, Lf/h/a/c/a1/e;->c:J

    const-wide/16 v8, -0x1

    cmp-long v3, v5, v8

    if-nez v3, :cond_23

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_23

    iget-object v3, v0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/a1/c0/a$a;

    iget-wide v5, v3, Lf/h/a/c/a1/c0/a$a;->b:J

    :cond_23
    cmp-long v3, v5, v8

    if-eqz v3, :cond_24

    iget-wide v8, v1, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v5, v8

    iget v3, v0, Lf/h/a/c/a1/c0/f;->i:I

    int-to-long v8, v3

    add-long/2addr v5, v8

    iput-wide v5, v0, Lf/h/a/c/a1/c0/f;->h:J

    :cond_24
    :goto_11
    iget-wide v5, v0, Lf/h/a/c/a1/c0/f;->h:J

    iget v3, v0, Lf/h/a/c/a1/c0/f;->i:I

    int-to-long v8, v3

    cmp-long v10, v5, v8

    if-ltz v10, :cond_30

    iget v11, v0, Lf/h/a/c/a1/c0/f;->g:I

    const v12, 0x6d6f6f76

    const v13, 0x6d657461

    if-eq v11, v12, :cond_26

    const v12, 0x7472616b

    if-eq v11, v12, :cond_26

    const v12, 0x6d646961

    if-eq v11, v12, :cond_26

    const v12, 0x6d696e66

    if-eq v11, v12, :cond_26

    const v12, 0x7374626c

    if-eq v11, v12, :cond_26

    const v12, 0x65647473

    if-eq v11, v12, :cond_26

    if-ne v11, v13, :cond_25

    goto :goto_12

    :cond_25
    const/4 v12, 0x0

    goto :goto_13

    :cond_26
    :goto_12
    const/4 v12, 0x1

    :goto_13
    const v14, 0x68646c72    # 4.3148E24f

    if-eqz v12, :cond_2a

    iget-wide v3, v1, Lf/h/a/c/a1/e;->d:J

    add-long/2addr v3, v5

    sub-long/2addr v3, v8

    if-eqz v10, :cond_28

    if-ne v11, v13, :cond_28

    iget-object v5, v0, Lf/h/a/c/a1/c0/f;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v7}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v5, v0, Lf/h/a/c/a1/c0/f;->c:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v7, v6}, Lf/h/a/c/a1/e;->e([BIIZ)Z

    iget-object v5, v0, Lf/h/a/c/a1/c0/f;->c:Lf/h/a/c/i1/r;

    const/4 v7, 0x4

    invoke-virtual {v5, v7}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v5, v0, Lf/h/a/c/a1/c0/f;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->e()I

    move-result v5

    if-ne v5, v14, :cond_27

    iput v6, v1, Lf/h/a/c/a1/e;->f:I

    goto :goto_14

    :cond_27
    invoke-virtual {v1, v7}, Lf/h/a/c/a1/e;->i(I)V

    :cond_28
    :goto_14
    iget-object v5, v0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    new-instance v6, Lf/h/a/c/a1/c0/a$a;

    iget v7, v0, Lf/h/a/c/a1/c0/f;->g:I

    invoke-direct {v6, v7, v3, v4}, Lf/h/a/c/a1/c0/a$a;-><init>(IJ)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    iget-wide v5, v0, Lf/h/a/c/a1/c0/f;->h:J

    iget v7, v0, Lf/h/a/c/a1/c0/f;->i:I

    int-to-long v7, v7

    cmp-long v9, v5, v7

    if-nez v9, :cond_29

    invoke-virtual {v0, v3, v4}, Lf/h/a/c/a1/c0/f;->l(J)V

    goto :goto_15

    :cond_29
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/c0/f;->j()V

    :goto_15
    const/4 v3, 0x1

    goto/16 :goto_1a

    :cond_2a
    const v5, 0x6d646864

    if-eq v11, v5, :cond_2c

    const v5, 0x6d766864

    if-eq v11, v5, :cond_2c

    if-eq v11, v14, :cond_2c

    const v5, 0x73747364

    if-eq v11, v5, :cond_2c

    const v5, 0x73747473

    if-eq v11, v5, :cond_2c

    const v5, 0x73747373

    if-eq v11, v5, :cond_2c

    const v5, 0x63747473

    if-eq v11, v5, :cond_2c

    const v5, 0x656c7374

    if-eq v11, v5, :cond_2c

    const v5, 0x73747363

    if-eq v11, v5, :cond_2c

    const v5, 0x7374737a

    if-eq v11, v5, :cond_2c

    const v5, 0x73747a32

    if-eq v11, v5, :cond_2c

    const v5, 0x7374636f

    if-eq v11, v5, :cond_2c

    const v5, 0x636f3634

    if-eq v11, v5, :cond_2c

    const v5, 0x746b6864

    if-eq v11, v5, :cond_2c

    if-eq v11, v4, :cond_2c

    const v4, 0x75647461

    if-eq v11, v4, :cond_2c

    const v4, 0x6b657973

    if-eq v11, v4, :cond_2c

    const v4, 0x696c7374

    if-ne v11, v4, :cond_2b

    goto :goto_16

    :cond_2b
    const/4 v5, 0x0

    goto :goto_17

    :cond_2c
    :goto_16
    const/4 v5, 0x1

    :goto_17
    if-eqz v5, :cond_2f

    if-ne v3, v7, :cond_2d

    const/4 v5, 0x1

    goto :goto_18

    :cond_2d
    const/4 v5, 0x0

    :goto_18
    invoke-static {v5}, Lf/g/j/k/a;->s(Z)V

    iget-wide v3, v0, Lf/h/a/c/a1/c0/f;->h:J

    const-wide/32 v5, 0x7fffffff

    cmp-long v8, v3, v5

    if-gtz v8, :cond_2e

    const/4 v5, 0x1

    goto :goto_19

    :cond_2e
    const/4 v5, 0x0

    :goto_19
    invoke-static {v5}, Lf/g/j/k/a;->s(Z)V

    new-instance v3, Lf/h/a/c/i1/r;

    iget-wide v4, v0, Lf/h/a/c/a1/c0/f;->h:J

    long-to-int v5, v4

    invoke-direct {v3, v5}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v3, v0, Lf/h/a/c/a1/c0/f;->j:Lf/h/a/c/i1/r;

    iget-object v4, v0, Lf/h/a/c/a1/c0/f;->d:Lf/h/a/c/i1/r;

    iget-object v4, v4, Lf/h/a/c/i1/r;->a:[B

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    const/4 v5, 0x0

    invoke-static {v4, v5, v3, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v3, 0x1

    iput v3, v0, Lf/h/a/c/a1/c0/f;->f:I

    goto :goto_1a

    :cond_2f
    const/4 v3, 0x1

    const/4 v4, 0x0

    iput-object v4, v0, Lf/h/a/c/a1/c0/f;->j:Lf/h/a/c/i1/r;

    iput v3, v0, Lf/h/a/c/a1/c0/f;->f:I

    :goto_1a
    const/4 v10, 0x1

    :goto_1b
    if-nez v10, :cond_0

    const/4 v3, -0x1

    return v3

    :cond_30
    new-instance v1, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Atom size less than header length (unsupported)."

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/c0/f;->o:Lf/h/a/c/a1/i;

    return-void
.end method

.method public f(JJ)V
    .locals 5

    iget-object v0, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/c0/f;->i:I

    const/4 v1, -0x1

    iput v1, p0, Lf/h/a/c/a1/c0/f;->k:I

    iput v0, p0, Lf/h/a/c/a1/c0/f;->l:I

    iput v0, p0, Lf/h/a/c/a1/c0/f;->m:I

    iput v0, p0, Lf/h/a/c/a1/c0/f;->n:I

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v2

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/a1/c0/f;->j()V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lf/h/a/c/a1/c0/f;->p:[Lf/h/a/c/a1/c0/f$a;

    if-eqz p1, :cond_2

    array-length p2, p1

    :goto_0
    if-ge v0, p2, :cond_2

    aget-object v2, p1, v0

    iget-object v3, v2, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    invoke-virtual {v3, p3, p4}, Lf/h/a/c/a1/c0/l;->a(J)I

    move-result v4

    if-ne v4, v1, :cond_1

    invoke-virtual {v3, p3, p4}, Lf/h/a/c/a1/c0/l;->b(J)I

    move-result v4

    :cond_1
    iput v4, v2, Lf/h/a/c/a1/c0/f$a;->d:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public g(J)Lf/h/a/c/a1/q$a;
    .locals 17

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    sget-object v3, Lf/h/a/c/a1/r;->c:Lf/h/a/c/a1/r;

    iget-object v4, v0, Lf/h/a/c/a1/c0/f;->p:[Lf/h/a/c/a1/c0/f$a;

    array-length v5, v4

    if-nez v5, :cond_0

    new-instance v1, Lf/h/a/c/a1/q$a;

    invoke-direct {v1, v3}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;)V

    return-object v1

    :cond_0
    const-wide/16 v5, -0x1

    iget v7, v0, Lf/h/a/c/a1/c0/f;->r:I

    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v10, -0x1

    if-eq v7, v10, :cond_4

    aget-object v4, v4, v7

    iget-object v4, v4, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    invoke-virtual {v4, v1, v2}, Lf/h/a/c/a1/c0/l;->a(J)I

    move-result v7

    if-ne v7, v10, :cond_1

    invoke-virtual {v4, v1, v2}, Lf/h/a/c/a1/c0/l;->b(J)I

    move-result v7

    :cond_1
    if-ne v7, v10, :cond_2

    new-instance v1, Lf/h/a/c/a1/q$a;

    invoke-direct {v1, v3}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;)V

    return-object v1

    :cond_2
    iget-object v3, v4, Lf/h/a/c/a1/c0/l;->f:[J

    aget-wide v11, v3, v7

    iget-object v3, v4, Lf/h/a/c/a1/c0/l;->c:[J

    aget-wide v13, v3, v7

    cmp-long v3, v11, v1

    if-gez v3, :cond_3

    iget v3, v4, Lf/h/a/c/a1/c0/l;->b:I

    add-int/2addr v3, v10

    if-ge v7, v3, :cond_3

    invoke-virtual {v4, v1, v2}, Lf/h/a/c/a1/c0/l;->b(J)I

    move-result v1

    if-eq v1, v10, :cond_3

    if-eq v1, v7, :cond_3

    iget-object v2, v4, Lf/h/a/c/a1/c0/l;->f:[J

    aget-wide v5, v2, v1

    iget-object v2, v4, Lf/h/a/c/a1/c0/l;->c:[J

    aget-wide v1, v2, v1

    move-wide v15, v1

    move-wide v1, v5

    move-wide v5, v15

    goto :goto_0

    :cond_3
    move-wide v1, v8

    :goto_0
    move-wide v3, v1

    move-wide v1, v11

    goto :goto_1

    :cond_4
    const-wide v13, 0x7fffffffffffffffL

    move-wide v3, v8

    :goto_1
    const/4 v7, 0x0

    :goto_2
    iget-object v10, v0, Lf/h/a/c/a1/c0/f;->p:[Lf/h/a/c/a1/c0/f$a;

    array-length v11, v10

    if-ge v7, v11, :cond_7

    iget v11, v0, Lf/h/a/c/a1/c0/f;->r:I

    if-eq v7, v11, :cond_6

    aget-object v10, v10, v7

    iget-object v10, v10, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    invoke-static {v10, v1, v2, v13, v14}, Lf/h/a/c/a1/c0/f;->k(Lf/h/a/c/a1/c0/l;JJ)J

    move-result-wide v11

    cmp-long v13, v3, v8

    if-eqz v13, :cond_5

    invoke-static {v10, v3, v4, v5, v6}, Lf/h/a/c/a1/c0/f;->k(Lf/h/a/c/a1/c0/l;JJ)J

    move-result-wide v5

    :cond_5
    move-wide v13, v11

    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_7
    new-instance v7, Lf/h/a/c/a1/r;

    invoke-direct {v7, v1, v2, v13, v14}, Lf/h/a/c/a1/r;-><init>(JJ)V

    cmp-long v1, v3, v8

    if-nez v1, :cond_8

    new-instance v1, Lf/h/a/c/a1/q$a;

    invoke-direct {v1, v7}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;)V

    return-object v1

    :cond_8
    new-instance v1, Lf/h/a/c/a1/r;

    invoke-direct {v1, v3, v4, v5, v6}, Lf/h/a/c/a1/r;-><init>(JJ)V

    new-instance v2, Lf/h/a/c/a1/q$a;

    invoke-direct {v2, v7, v1}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;Lf/h/a/c/a1/r;)V

    return-object v2
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lf/h/a/c/a1/c0/h;->a(Lf/h/a/c/a1/e;Z)Z

    move-result p1

    return p1
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, Lf/h/a/c/a1/c0/f;->s:J

    return-wide v0
.end method

.method public final j()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/c0/f;->f:I

    iput v0, p0, Lf/h/a/c/a1/c0/f;->i:I

    return-void
.end method

.method public final l(J)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    const/4 v1, 0x2

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/a1/c0/a$a;

    iget-wide v2, v0, Lf/h/a/c/a1/c0/a$a;->b:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/a1/c0/a$a;

    iget v2, v0, Lf/h/a/c/a1/c0/a;->a:I

    const v3, 0x6d6f6f76

    if-ne v2, v3, :cond_1

    invoke-virtual {p0, v0}, Lf/h/a/c/a1/c0/f;->m(Lf/h/a/c/a1/c0/a$a;)V

    iget-object v0, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    iput v1, p0, Lf/h/a/c/a1/c0/f;->f:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/h/a/c/a1/c0/f;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/a1/c0/a$a;

    iget-object v1, v1, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget p1, p0, Lf/h/a/c/a1/c0/f;->f:I

    if-eq p1, v1, :cond_3

    invoke-virtual {p0}, Lf/h/a/c/a1/c0/f;->j()V

    :cond_3
    return-void
.end method

.method public final m(Lf/h/a/c/a1/c0/a$a;)V
    .locals 64
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lf/h/a/c/a1/m;

    invoke-direct {v3}, Lf/h/a/c/a1/m;-><init>()V

    const v4, 0x75647461

    invoke-virtual {v0, v4}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v4

    const v5, 0x696c7374

    const v6, 0x6d657461

    const/16 v7, 0xc

    const/16 v8, 0x8

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz v4, :cond_2d

    iget-boolean v12, v1, Lf/h/a/c/a1/c0/f;->t:Z

    sget-object v13, Lf/h/a/c/a1/c0/b;->a:[B

    if-eqz v12, :cond_0

    goto/16 :goto_d

    :cond_0
    iget-object v4, v4, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/r;->C(I)V

    :goto_0
    invoke-virtual {v4}, Lf/h/a/c/i1/r;->a()I

    move-result v12

    if-lt v12, v8, :cond_2c

    iget v12, v4, Lf/h/a/c/i1/r;->b:I

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v13

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v14

    if-ne v14, v6, :cond_2b

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/r;->C(I)V

    add-int/2addr v12, v13

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/r;->D(I)V

    :goto_1
    iget v6, v4, Lf/h/a/c/i1/r;->b:I

    if-ge v6, v12, :cond_2c

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v13

    if-ne v13, v5, :cond_2a

    invoke-virtual {v4, v6}, Lf/h/a/c/i1/r;->C(I)V

    add-int/2addr v6, v7

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/r;->D(I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    iget v7, v4, Lf/h/a/c/i1/r;->b:I

    if-ge v7, v6, :cond_28

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    add-int/2addr v8, v7

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    shr-int/lit8 v12, v7, 0x18

    and-int/lit16 v12, v12, 0xff

    const/16 v13, 0xa9

    const-string v14, "TCON"

    const-string v15, "MetadataUtil"

    if-eq v12, v13, :cond_19

    const/16 v13, 0xfd

    if-ne v12, v13, :cond_1

    goto/16 :goto_6

    :cond_1
    const v12, 0x676e7265

    if-ne v7, v12, :cond_4

    :try_start_0
    invoke-static {v4}, Lf/h/a/c/a1/c0/e;->g(Lf/h/a/c/i1/r;)I

    move-result v7

    if-lez v7, :cond_2

    sget-object v10, Lf/h/a/c/a1/c0/e;->a:[Ljava/lang/String;

    array-length v12, v10

    if-gt v7, v12, :cond_2

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v10, v7

    goto :goto_3

    :cond_2
    move-object v7, v9

    :goto_3
    if-eqz v7, :cond_3

    new-instance v10, Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    invoke-direct {v10, v14, v9, v7}, Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_3
    const-string v7, "Failed to parse standard genre code"

    invoke-static {v15, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    :cond_4
    const v12, 0x6469736b

    if-ne v7, v12, :cond_5

    const-string v9, "TPOS"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->d(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_5
    const v12, 0x74726b6e

    if-ne v7, v12, :cond_6

    const-string v9, "TRCK"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->d(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_6
    const v12, 0x746d706f

    if-ne v7, v12, :cond_7

    const-string v9, "TBPM"

    invoke-static {v7, v9, v4, v10, v11}, Lf/h/a/c/a1/c0/e;->f(ILjava/lang/String;Lf/h/a/c/i1/r;ZZ)Lcom/google/android/exoplayer2/metadata/id3/Id3Frame;

    move-result-object v10

    goto/16 :goto_a

    :cond_7
    const v12, 0x6370696c

    if-ne v7, v12, :cond_8

    const-string v9, "TCMP"

    invoke-static {v7, v9, v4, v10, v10}, Lf/h/a/c/a1/c0/e;->f(ILjava/lang/String;Lf/h/a/c/i1/r;ZZ)Lcom/google/android/exoplayer2/metadata/id3/Id3Frame;

    move-result-object v10

    goto/16 :goto_a

    :cond_8
    const v12, 0x636f7672

    if-ne v7, v12, :cond_9

    invoke-static {v4}, Lf/h/a/c/a1/c0/e;->c(Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/ApicFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_9
    const v12, 0x61415254

    if-ne v7, v12, :cond_a

    const-string v9, "TPE2"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_a
    const v12, 0x736f6e6d

    if-ne v7, v12, :cond_b

    const-string v9, "TSOT"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_b
    const v12, 0x736f616c

    if-ne v7, v12, :cond_c

    const-string v9, "TSO2"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_c
    const v12, 0x736f6172

    if-ne v7, v12, :cond_d

    const-string v9, "TSOA"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_d
    const v12, 0x736f6161

    if-ne v7, v12, :cond_e

    const-string v9, "TSOP"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_e
    const v12, 0x736f636f

    if-ne v7, v12, :cond_f

    const-string v9, "TSOC"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_f
    const v12, 0x72746e67

    if-ne v7, v12, :cond_10

    const-string v9, "ITUNESADVISORY"

    invoke-static {v7, v9, v4, v11, v11}, Lf/h/a/c/a1/c0/e;->f(ILjava/lang/String;Lf/h/a/c/i1/r;ZZ)Lcom/google/android/exoplayer2/metadata/id3/Id3Frame;

    move-result-object v10

    goto/16 :goto_a

    :cond_10
    const v12, 0x70676170

    if-ne v7, v12, :cond_11

    const-string v9, "ITUNESGAPLESS"

    invoke-static {v7, v9, v4, v11, v10}, Lf/h/a/c/a1/c0/e;->f(ILjava/lang/String;Lf/h/a/c/i1/r;ZZ)Lcom/google/android/exoplayer2/metadata/id3/Id3Frame;

    move-result-object v10

    goto/16 :goto_a

    :cond_11
    const v10, 0x736f736e

    if-ne v7, v10, :cond_12

    const-string v9, "TVSHOWSORT"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_12
    const v10, 0x74767368

    if-ne v7, v10, :cond_13

    const-string v9, "TVSHOW"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_13
    const v10, 0x2d2d2d2d

    if-ne v7, v10, :cond_23

    const/4 v7, -0x1

    const/4 v10, -0x1

    move-object v10, v9

    const/4 v12, -0x1

    :goto_4
    iget v13, v4, Lf/h/a/c/i1/r;->b:I

    if-ge v13, v8, :cond_17

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v14

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v15

    const/4 v11, 0x4

    invoke-virtual {v4, v11}, Lf/h/a/c/i1/r;->D(I)V

    const v11, 0x6d65616e

    if-ne v15, v11, :cond_14

    add-int/lit8 v14, v14, -0xc

    invoke-virtual {v4, v14}, Lf/h/a/c/i1/r;->m(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    :cond_14
    const v11, 0x6e616d65

    if-ne v15, v11, :cond_15

    add-int/lit8 v14, v14, -0xc

    invoke-virtual {v4, v14}, Lf/h/a/c/i1/r;->m(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_5

    :cond_15
    const v11, 0x64617461

    if-ne v15, v11, :cond_16

    move v7, v13

    move v12, v14

    :cond_16
    add-int/lit8 v14, v14, -0xc

    invoke-virtual {v4, v14}, Lf/h/a/c/i1/r;->D(I)V

    :goto_5
    const/4 v11, 0x0

    goto :goto_4

    :cond_17
    if-eqz v9, :cond_24

    if-eqz v10, :cond_24

    const/4 v11, -0x1

    if-ne v7, v11, :cond_18

    goto/16 :goto_7

    :cond_18
    invoke-virtual {v4, v7}, Lf/h/a/c/i1/r;->C(I)V

    const/16 v7, 0x10

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/r;->D(I)V

    add-int/lit8 v12, v12, -0x10

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/r;->m(I)Ljava/lang/String;

    move-result-object v7

    new-instance v11, Lcom/google/android/exoplayer2/metadata/id3/InternalFrame;

    invoke-direct {v11, v9, v10, v7}, Lcom/google/android/exoplayer2/metadata/id3/InternalFrame;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v10, v11

    goto/16 :goto_a

    :cond_19
    :goto_6
    const v9, 0xffffff

    and-int/2addr v9, v7

    const v10, 0x636d74

    if-ne v9, v10, :cond_1a

    invoke-static {v7, v4}, Lf/h/a/c/a1/c0/e;->b(ILf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/CommentFrame;

    move-result-object v10

    goto/16 :goto_a

    :catchall_0
    move-exception v0

    goto/16 :goto_b

    :cond_1a
    const v10, 0x6e616d

    if-eq v9, v10, :cond_26

    const v10, 0x74726b

    if-ne v9, v10, :cond_1b

    goto/16 :goto_9

    :cond_1b
    const v10, 0x636f6d

    if-eq v9, v10, :cond_25

    const v10, 0x777274

    if-ne v9, v10, :cond_1c

    goto/16 :goto_8

    :cond_1c
    const v10, 0x646179

    if-ne v9, v10, :cond_1d

    const-string v9, "TDRC"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto/16 :goto_a

    :cond_1d
    const v10, 0x415254

    if-ne v9, v10, :cond_1e

    const-string v9, "TPE1"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto :goto_a

    :cond_1e
    const v10, 0x746f6f

    if-ne v9, v10, :cond_1f

    const-string v9, "TSSE"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto :goto_a

    :cond_1f
    const v10, 0x616c62

    if-ne v9, v10, :cond_20

    const-string v9, "TALB"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto :goto_a

    :cond_20
    const v10, 0x6c7972

    if-ne v9, v10, :cond_21

    const-string v9, "USLT"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto :goto_a

    :cond_21
    const v10, 0x67656e

    if-ne v9, v10, :cond_22

    invoke-static {v7, v14, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto :goto_a

    :cond_22
    const v10, 0x677270

    if-ne v9, v10, :cond_23

    const-string v9, "TIT1"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto :goto_a

    :cond_23
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Skipped unknown metadata entry: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v7}, Lf/h/a/c/a1/c0/a;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v15, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_24
    :goto_7
    const/4 v10, 0x0

    goto :goto_a

    :cond_25
    :goto_8
    const-string v9, "TCOM"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10

    goto :goto_a

    :cond_26
    :goto_9
    const-string v9, "TIT2"

    invoke-static {v7, v9, v4}, Lf/h/a/c/a1/c0/e;->e(ILjava/lang/String;Lf/h/a/c/i1/r;)Lcom/google/android/exoplayer2/metadata/id3/TextInformationFrame;

    move-result-object v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_a
    invoke-virtual {v4, v8}, Lf/h/a/c/i1/r;->C(I)V

    if-eqz v10, :cond_27

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_27
    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    goto/16 :goto_2

    :goto_b
    invoke-virtual {v4, v8}, Lf/h/a/c/i1/r;->C(I)V

    throw v0

    :cond_28
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_29

    goto :goto_c

    :cond_29
    new-instance v9, Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-direct {v9, v5}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>(Ljava/util/List;)V

    goto :goto_d

    :cond_2a
    add-int/2addr v6, v7

    invoke-virtual {v4, v6}, Lf/h/a/c/i1/r;->C(I)V

    const v5, 0x696c7374

    const/16 v8, 0x8

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    goto/16 :goto_1

    :cond_2b
    add-int/2addr v12, v13

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/r;->C(I)V

    const v5, 0x696c7374

    const v6, 0x6d657461

    const/16 v7, 0xc

    const/16 v8, 0x8

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_2c
    :goto_c
    const/4 v9, 0x0

    :goto_d
    if-eqz v9, :cond_2e

    invoke-virtual {v3, v9}, Lf/h/a/c/a1/m;->b(Lcom/google/android/exoplayer2/metadata/Metadata;)Z

    goto :goto_e

    :cond_2d
    const/4 v9, 0x0

    :cond_2e
    :goto_e
    const v4, 0x6d657461

    invoke-virtual {v0, v4}, Lf/h/a/c/a1/c0/a$a;->b(I)Lf/h/a/c/a1/c0/a$a;

    move-result-object v4

    const-string v5, "AtomParsers"

    if-eqz v4, :cond_37

    sget-object v6, Lf/h/a/c/a1/c0/b;->a:[B

    const v6, 0x68646c72    # 4.3148E24f

    invoke-virtual {v4, v6}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v6

    const v7, 0x6b657973

    invoke-virtual {v4, v7}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v7

    const v8, 0x696c7374

    invoke-virtual {v4, v8}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v4

    if-eqz v6, :cond_37

    if-eqz v7, :cond_37

    if-eqz v4, :cond_37

    iget-object v6, v6, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/16 v8, 0x10

    invoke-virtual {v6, v8}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v6

    const v8, 0x6d647461

    if-eq v6, v8, :cond_2f

    goto/16 :goto_14

    :cond_2f
    iget-object v6, v7, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/16 v7, 0xc

    invoke-virtual {v6, v7}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    new-array v8, v7, [Ljava/lang/String;

    const/4 v10, 0x0

    :goto_f
    if-ge v10, v7, :cond_30

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v11

    const/4 v12, 0x4

    invoke-virtual {v6, v12}, Lf/h/a/c/i1/r;->D(I)V

    add-int/lit8 v11, v11, -0x8

    invoke-virtual {v6, v11}, Lf/h/a/c/i1/r;->n(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_f

    :cond_30
    const/16 v6, 0x8

    iget-object v4, v4, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v4, v6}, Lf/h/a/c/i1/r;->C(I)V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    :goto_10
    invoke-virtual {v4}, Lf/h/a/c/i1/r;->a()I

    move-result v11

    if-le v11, v6, :cond_35

    iget v6, v4, Lf/h/a/c/i1/r;->b:I

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v11

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ltz v12, :cond_33

    if-ge v12, v7, :cond_33

    aget-object v12, v8, v12

    add-int v13, v6, v11

    :goto_11
    iget v14, v4, Lf/h/a/c/i1/r;->b:I

    if-ge v14, v13, :cond_32

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v15

    move/from16 v16, v7

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    move-object/from16 v17, v8

    const v8, 0x64617461

    if-ne v7, v8, :cond_31

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    add-int/lit8 v15, v15, -0x10

    new-array v13, v15, [B

    iget-object v14, v4, Lf/h/a/c/i1/r;->a:[B

    move-object/from16 v18, v2

    iget v2, v4, Lf/h/a/c/i1/r;->b:I

    move-object/from16 v19, v9

    const/4 v9, 0x0

    invoke-static {v14, v2, v13, v9, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, v4, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v2, v15

    iput v2, v4, Lf/h/a/c/i1/r;->b:I

    new-instance v2, Lcom/google/android/exoplayer2/extractor/mp4/MdtaMetadataEntry;

    invoke-direct {v2, v12, v13, v8, v7}, Lcom/google/android/exoplayer2/extractor/mp4/MdtaMetadataEntry;-><init>(Ljava/lang/String;[BII)V

    goto :goto_12

    :cond_31
    move-object/from16 v18, v2

    move-object/from16 v19, v9

    add-int/2addr v14, v15

    invoke-virtual {v4, v14}, Lf/h/a/c/i1/r;->C(I)V

    move/from16 v7, v16

    move-object/from16 v8, v17

    goto :goto_11

    :cond_32
    move-object/from16 v18, v2

    move/from16 v16, v7

    move-object/from16 v17, v8

    move-object/from16 v19, v9

    const/4 v2, 0x0

    :goto_12
    if-eqz v2, :cond_34

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    :cond_33
    move-object/from16 v18, v2

    move/from16 v16, v7

    move-object/from16 v17, v8

    move-object/from16 v19, v9

    const-string v2, "Skipped metadata with unknown key index: "

    invoke-static {v2, v12, v5}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    :cond_34
    :goto_13
    add-int/2addr v6, v11

    invoke-virtual {v4, v6}, Lf/h/a/c/i1/r;->C(I)V

    const/16 v6, 0x8

    move/from16 v7, v16

    move-object/from16 v8, v17

    move-object/from16 v2, v18

    move-object/from16 v9, v19

    goto/16 :goto_10

    :cond_35
    move-object/from16 v18, v2

    move-object/from16 v19, v9

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_36

    goto :goto_15

    :cond_36
    new-instance v2, Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-direct {v2, v10}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>(Ljava/util/List;)V

    goto :goto_16

    :cond_37
    :goto_14
    move-object/from16 v18, v2

    move-object/from16 v19, v9

    :goto_15
    const/4 v2, 0x0

    :goto_16
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    :goto_17
    const/4 v12, 0x0

    iget-object v7, v0, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_79

    iget-object v7, v0, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v14, v7

    check-cast v14, Lf/h/a/c/a1/c0/a$a;

    iget v7, v14, Lf/h/a/c/a1/c0/a;->a:I

    const v8, 0x7472616b

    if-eq v7, v8, :cond_38

    goto :goto_18

    :cond_38
    const v7, 0x6d766864

    invoke-virtual {v0, v7}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v8

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v11, 0x0

    iget-boolean v13, v1, Lf/h/a/c/a1/c0/f;->t:Z

    move-object v7, v14

    invoke-static/range {v7 .. v13}, Lf/h/a/c/a1/c0/b;->d(Lf/h/a/c/a1/c0/a$a;Lf/h/a/c/a1/c0/a$b;JLcom/google/android/exoplayer2/drm/DrmInitData;ZZ)Lf/h/a/c/a1/c0/i;

    move-result-object v7

    if-nez v7, :cond_39

    :goto_18
    move-object/from16 v16, v2

    move-object/from16 v31, v3

    move-object v0, v4

    move-object/from16 v32, v5

    move/from16 v29, v6

    goto/16 :goto_4b

    :cond_39
    const v8, 0x6d646961

    invoke-virtual {v14, v8}, Lf/h/a/c/a1/c0/a$a;->b(I)Lf/h/a/c/a1/c0/a$a;

    move-result-object v8

    const v9, 0x6d696e66

    invoke-virtual {v8, v9}, Lf/h/a/c/a1/c0/a$a;->b(I)Lf/h/a/c/a1/c0/a$a;

    move-result-object v8

    const v9, 0x7374626c

    invoke-virtual {v8, v9}, Lf/h/a/c/a1/c0/a$a;->b(I)Lf/h/a/c/a1/c0/a$a;

    move-result-object v8

    const v9, 0x7374737a

    invoke-virtual {v8, v9}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v9

    if-eqz v9, :cond_3a

    new-instance v10, Lf/h/a/c/a1/c0/b$b;

    invoke-direct {v10, v9}, Lf/h/a/c/a1/c0/b$b;-><init>(Lf/h/a/c/a1/c0/a$b;)V

    goto :goto_19

    :cond_3a
    const v9, 0x73747a32

    invoke-virtual {v8, v9}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v9

    if-eqz v9, :cond_78

    new-instance v10, Lf/h/a/c/a1/c0/b$c;

    invoke-direct {v10, v9}, Lf/h/a/c/a1/c0/b$c;-><init>(Lf/h/a/c/a1/c0/a$b;)V

    :goto_19
    invoke-interface {v10}, Lf/h/a/c/a1/c0/b$a;->b()I

    move-result v9

    if-nez v9, :cond_3b

    new-instance v8, Lf/h/a/c/a1/c0/l;

    const/4 v9, 0x0

    new-array v10, v9, [J

    new-array v11, v9, [I

    const/16 v24, 0x0

    new-array v12, v9, [J

    new-array v9, v9, [I

    const-wide v27, -0x7fffffffffffffffL    # -4.9E-324

    move-object/from16 v20, v8

    move-object/from16 v21, v7

    move-object/from16 v22, v10

    move-object/from16 v23, v11

    move-object/from16 v25, v12

    move-object/from16 v26, v9

    invoke-direct/range {v20 .. v28}, Lf/h/a/c/a1/c0/l;-><init>(Lf/h/a/c/a1/c0/i;[J[II[J[IJ)V

    move-object/from16 v16, v2

    move-object/from16 v31, v3

    move-object/from16 v30, v4

    move-object/from16 v32, v5

    move/from16 v29, v6

    goto/16 :goto_4a

    :cond_3b
    const v11, 0x7374636f

    invoke-virtual {v8, v11}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v11

    if-nez v11, :cond_3c

    const v11, 0x636f3634

    invoke-virtual {v8, v11}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v11

    const/4 v12, 0x1

    goto :goto_1a

    :cond_3c
    const/4 v12, 0x0

    :goto_1a
    iget-object v11, v11, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const v13, 0x73747363

    invoke-virtual {v8, v13}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v13

    iget-object v13, v13, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const v14, 0x73747473

    invoke-virtual {v8, v14}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v14

    iget-object v14, v14, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const v15, 0x73747373

    invoke-virtual {v8, v15}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v15

    if-eqz v15, :cond_3d

    iget-object v15, v15, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    goto :goto_1b

    :cond_3d
    const/4 v15, 0x0

    :goto_1b
    const v0, 0x63747473

    invoke-virtual {v8, v0}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v0

    if-eqz v0, :cond_3e

    iget-object v0, v0, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    goto :goto_1c

    :cond_3e
    const/4 v0, 0x0

    :goto_1c
    const/16 v8, 0xc

    invoke-virtual {v11, v8}, Lf/h/a/c/i1/r;->C(I)V

    move-object/from16 v16, v2

    invoke-virtual {v11}, Lf/h/a/c/i1/r;->t()I

    move-result v2

    invoke-virtual {v13, v8}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->t()I

    move-result v8

    move/from16 v17, v8

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    const/4 v1, 0x1

    if-ne v8, v1, :cond_3f

    const/4 v1, 0x1

    goto :goto_1d

    :cond_3f
    const/4 v1, 0x0

    :goto_1d
    const-string v8, "first_chunk must be 1"

    invoke-static {v1, v8}, Lf/g/j/k/a;->t(ZLjava/lang/Object;)V

    const/16 v1, 0xc

    invoke-virtual {v14, v1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->t()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->t()I

    move-result v20

    move/from16 v29, v6

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->t()I

    move-result v6

    if-eqz v0, :cond_40

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->t()I

    move-result v21

    goto :goto_1e

    :cond_40
    const/16 v21, 0x0

    :goto_1e
    if-eqz v15, :cond_42

    invoke-virtual {v15, v1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v15}, Lf/h/a/c/i1/r;->t()I

    move-result v1

    if-lez v1, :cond_41

    invoke-virtual {v15}, Lf/h/a/c/i1/r;->t()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    goto :goto_20

    :cond_41
    const/4 v15, 0x0

    goto :goto_1f

    :cond_42
    const/4 v1, 0x0

    :goto_1f
    const/16 v22, -0x1

    :goto_20
    invoke-interface {v10}, Lf/h/a/c/a1/c0/b$a;->a()Z

    move-result v23

    if-eqz v23, :cond_43

    move-object/from16 v30, v4

    iget-object v4, v7, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget-object v4, v4, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    move-object/from16 v31, v3

    const-string v3, "audio/raw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_44

    if-nez v8, :cond_44

    if-nez v21, :cond_44

    if-nez v1, :cond_44

    const/4 v3, 0x1

    goto :goto_21

    :cond_43
    move-object/from16 v31, v3

    move-object/from16 v30, v4

    :cond_44
    const/4 v3, 0x0

    :goto_21
    if-nez v3, :cond_59

    new-array v3, v9, [J

    new-array v4, v9, [I

    move/from16 v23, v1

    new-array v1, v9, [J

    move/from16 v24, v8

    new-array v8, v9, [I

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, -0x1

    const-wide/16 v32, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const-wide/16 v36, 0x0

    const-wide/16 v38, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    move/from16 v34, v6

    move/from16 v35, v23

    const/4 v6, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    move/from16 v23, v21

    move-object/from16 v21, v7

    const/4 v7, 0x0

    move/from16 v63, v20

    move-object/from16 v20, v14

    move/from16 v14, v22

    move/from16 v22, v24

    move/from16 v24, v63

    :goto_22
    if-ge v6, v9, :cond_52

    const/16 v40, 0x1

    move/from16 v63, v26

    move/from16 v26, v9

    move/from16 v9, v63

    :goto_23
    if-nez v25, :cond_49

    move/from16 v41, v14

    add-int/lit8 v14, v28, 0x1

    if-ne v14, v2, :cond_45

    const/16 v28, 0x0

    const/16 v40, 0x0

    goto :goto_26

    :cond_45
    if-eqz v12, :cond_46

    invoke-virtual {v11}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v38

    goto :goto_24

    :cond_46
    invoke-virtual {v11}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v38

    :goto_24
    if-ne v14, v9, :cond_48

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->t()I

    move-result v27

    const/4 v9, 0x4

    invoke-virtual {v13, v9}, Lf/h/a/c/i1/r;->D(I)V

    add-int/lit8 v17, v17, -0x1

    if-lez v17, :cond_47

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->t()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    goto :goto_25

    :cond_47
    const/4 v9, -0x1

    :cond_48
    :goto_25
    const/16 v28, 0x1

    const/16 v40, 0x1

    :goto_26
    move/from16 v28, v14

    if-eqz v40, :cond_4a

    move/from16 v25, v27

    move-wide/from16 v36, v38

    move/from16 v14, v41

    goto :goto_23

    :cond_49
    move/from16 v41, v14

    :cond_4a
    if-nez v40, :cond_4b

    const-string v2, "Unexpected end of chunk data"

    invoke-static {v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v3, v6}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v3

    invoke-static {v4, v6}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v4

    invoke-static {v1, v6}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    invoke-static {v8, v6}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v8

    move v9, v6

    goto/16 :goto_2a

    :cond_4b
    if-eqz v0, :cond_4d

    :goto_27
    if-nez v43, :cond_4c

    if-lez v23, :cond_4c

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->t()I

    move-result v43

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v42

    add-int/lit8 v23, v23, -0x1

    goto :goto_27

    :cond_4c
    add-int/lit8 v43, v43, -0x1

    :cond_4d
    move/from16 v14, v42

    aput-wide v36, v3, v6

    invoke-interface {v10}, Lf/h/a/c/a1/c0/b$a;->c()I

    move-result v40

    aput v40, v4, v6

    move-object/from16 v40, v3

    aget v3, v4, v6

    if-le v3, v7, :cond_4e

    aget v3, v4, v6

    move v7, v3

    :cond_4e
    move/from16 v44, v9

    move-object v3, v10

    int-to-long v9, v14

    add-long v9, v32, v9

    aput-wide v9, v1, v6

    if-nez v15, :cond_4f

    const/4 v9, 0x1

    goto :goto_28

    :cond_4f
    const/4 v9, 0x0

    :goto_28
    aput v9, v8, v6

    move/from16 v9, v41

    if-ne v6, v9, :cond_50

    const/4 v10, 0x1

    aput v10, v8, v6

    add-int/lit8 v35, v35, -0x1

    if-lez v35, :cond_50

    invoke-virtual {v15}, Lf/h/a/c/i1/r;->t()I

    move-result v9

    sub-int/2addr v9, v10

    :cond_50
    move/from16 v41, v7

    move/from16 v10, v34

    move-object/from16 v34, v8

    int-to-long v7, v10

    add-long v32, v32, v7

    add-int/lit8 v24, v24, -0x1

    if-nez v24, :cond_51

    if-lez v22, :cond_51

    invoke-virtual/range {v20 .. v20}, Lf/h/a/c/i1/r;->t()I

    move-result v7

    invoke-virtual/range {v20 .. v20}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    add-int/lit8 v22, v22, -0x1

    move/from16 v24, v7

    goto :goto_29

    :cond_51
    move v8, v10

    :goto_29
    aget v7, v4, v6

    move-object/from16 v45, v3

    move-object v10, v4

    int-to-long v3, v7

    add-long v36, v36, v3

    add-int/lit8 v25, v25, -0x1

    add-int/lit8 v6, v6, 0x1

    move-object v4, v10

    move/from16 v42, v14

    move-object/from16 v3, v40

    move/from16 v7, v41

    move-object/from16 v10, v45

    move v14, v9

    move/from16 v9, v26

    move/from16 v26, v44

    move-object/from16 v63, v34

    move/from16 v34, v8

    move-object/from16 v8, v63

    goto/16 :goto_22

    :cond_52
    move-object/from16 v40, v3

    move-object v10, v4

    move-object/from16 v34, v8

    move/from16 v26, v9

    :goto_2a
    move/from16 v2, v25

    move/from16 v14, v42

    int-to-long v10, v14

    add-long v32, v32, v10

    :goto_2b
    if-lez v23, :cond_54

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->t()I

    move-result v6

    if-eqz v6, :cond_53

    const/4 v0, 0x0

    goto :goto_2c

    :cond_53
    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    add-int/lit8 v23, v23, -0x1

    goto :goto_2b

    :cond_54
    const/4 v0, 0x1

    :goto_2c
    if-nez v35, :cond_56

    if-nez v24, :cond_56

    if-nez v2, :cond_56

    if-nez v22, :cond_56

    move/from16 v6, v43

    if-nez v6, :cond_57

    if-nez v0, :cond_55

    goto :goto_2d

    :cond_55
    move-object/from16 v14, v21

    goto :goto_2f

    :cond_56
    move/from16 v6, v43

    :cond_57
    :goto_2d
    const-string v10, "Inconsistent stbl box for track "

    invoke-static {v10}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v14, v21

    iget v11, v14, Lf/h/a/c/a1/c0/i;->a:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v11, ": remainingSynchronizationSamples "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v11, v35

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v11, ", remainingSamplesAtTimestampDelta "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v11, v24

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v11, ", remainingSamplesInChunk "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", remainingTimestampDeltaChanges "

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v2, v22

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", remainingSamplesAtTimestampOffset "

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-nez v0, :cond_58

    const-string v0, ", ctts invalid"

    goto :goto_2e

    :cond_58
    const-string v0, ""

    :goto_2e
    invoke-static {v10, v0, v5}, Lf/e/c/a/a;->W(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2f
    move/from16 v24, v7

    goto/16 :goto_37

    :cond_59
    move-object v14, v7

    move/from16 v26, v9

    new-array v0, v2, [J

    new-array v1, v2, [I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    :goto_30
    add-int/lit8 v3, v3, 0x1

    if-ne v3, v2, :cond_5a

    const/4 v10, 0x0

    goto :goto_33

    :cond_5a
    if-eqz v12, :cond_5b

    invoke-virtual {v11}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v8

    goto :goto_31

    :cond_5b
    invoke-virtual {v11}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v8

    :goto_31
    if-ne v3, v4, :cond_5d

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->t()I

    move-result v7

    const/4 v4, 0x4

    invoke-virtual {v13, v4}, Lf/h/a/c/i1/r;->D(I)V

    add-int/lit8 v17, v17, -0x1

    if-lez v17, :cond_5c

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->t()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    goto :goto_32

    :cond_5c
    const/4 v4, -0x1

    :cond_5d
    :goto_32
    const/4 v10, 0x1

    :goto_33
    if-eqz v10, :cond_5e

    aput-wide v8, v0, v3

    aput v7, v1, v3

    goto :goto_30

    :cond_5e
    iget-object v3, v14, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget v4, v3, Lcom/google/android/exoplayer2/Format;->A:I

    iget v3, v3, Lcom/google/android/exoplayer2/Format;->y:I

    invoke-static {v4, v3}, Lf/h/a/c/i1/a0;->l(II)I

    move-result v3

    int-to-long v6, v6

    const/16 v4, 0x2000

    div-int/2addr v4, v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_34
    if-ge v8, v2, :cond_5f

    aget v10, v1, v8

    invoke-static {v10, v4}, Lf/h/a/c/i1/a0;->d(II)I

    move-result v10

    add-int/2addr v9, v10

    add-int/lit8 v8, v8, 0x1

    goto :goto_34

    :cond_5f
    new-array v8, v9, [J

    new-array v10, v9, [I

    new-array v11, v9, [J

    new-array v9, v9, [I

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    :goto_35
    if-ge v12, v2, :cond_61

    aget v20, v1, v12

    aget-wide v21, v0, v12

    move/from16 v63, v20

    move-object/from16 v20, v0

    move/from16 v0, v63

    :goto_36
    if-lez v0, :cond_60

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v23

    aput-wide v21, v8, v17

    mul-int v24, v3, v23

    aput v24, v10, v17

    move-object/from16 v24, v1

    aget v1, v10, v17

    invoke-static {v15, v1}, Ljava/lang/Math;->max(II)I

    move-result v15

    move/from16 v25, v2

    int-to-long v1, v13

    mul-long v1, v1, v6

    aput-wide v1, v11, v17

    const/4 v1, 0x1

    aput v1, v9, v17

    aget v1, v10, v17

    int-to-long v1, v1

    add-long v21, v21, v1

    add-int v13, v13, v23

    sub-int v0, v0, v23

    add-int/lit8 v17, v17, 0x1

    move-object/from16 v1, v24

    move/from16 v2, v25

    goto :goto_36

    :cond_60
    move-object/from16 v24, v1

    move/from16 v25, v2

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, v20

    goto :goto_35

    :cond_61
    int-to-long v0, v13

    mul-long v32, v6, v0

    move-object v3, v8

    move-object v8, v9

    move-object v4, v10

    move-object v1, v11

    move/from16 v24, v15

    move/from16 v9, v26

    :goto_37
    move-wide/from16 v6, v32

    const-wide/32 v34, 0xf4240

    iget-wide v10, v14, Lf/h/a/c/a1/c0/i;->c:J

    move-wide/from16 v32, v6

    move-wide/from16 v36, v10

    invoke-static/range {v32 .. v37}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v27

    iget-object v0, v14, Lf/h/a/c/a1/c0/i;->h:[J

    const-wide/32 v10, 0xf4240

    if-nez v0, :cond_62

    iget-wide v6, v14, Lf/h/a/c/a1/c0/i;->c:J

    invoke-static {v1, v10, v11, v6, v7}, Lf/h/a/c/i1/a0;->y([JJJ)V

    new-instance v0, Lf/h/a/c/a1/c0/l;

    move-object/from16 v20, v0

    move-object/from16 v21, v14

    move-object/from16 v22, v3

    move-object/from16 v23, v4

    move-object/from16 v25, v1

    move-object/from16 v26, v8

    invoke-direct/range {v20 .. v28}, Lf/h/a/c/a1/c0/l;-><init>(Lf/h/a/c/a1/c0/i;[J[II[J[IJ)V

    move-object v8, v0

    move-object/from16 v32, v5

    move-object/from16 v2, v31

    goto/16 :goto_3d

    :cond_62
    array-length v2, v0

    const/4 v10, 0x1

    if-ne v2, v10, :cond_66

    iget v2, v14, Lf/h/a/c/a1/c0/i;->b:I

    if-ne v2, v10, :cond_66

    array-length v2, v1

    const/4 v10, 0x2

    if-lt v2, v10, :cond_66

    iget-object v2, v14, Lf/h/a/c/a1/c0/i;->i:[J

    const/4 v10, 0x0

    aget-wide v11, v2, v10

    aget-wide v32, v0, v10

    move v0, v9

    iget-wide v9, v14, Lf/h/a/c/a1/c0/i;->c:J

    move-object v13, v4

    move-object v2, v5

    iget-wide v4, v14, Lf/h/a/c/a1/c0/i;->d:J

    move-wide/from16 v34, v9

    move-wide/from16 v36, v4

    invoke-static/range {v32 .. v37}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v4

    add-long/2addr v4, v11

    array-length v9, v1

    add-int/lit8 v9, v9, -0x1

    const/4 v10, 0x4

    const/4 v15, 0x0

    invoke-static {v10, v15, v9}, Lf/h/a/c/i1/a0;->f(III)I

    move-result v17

    move-object/from16 v32, v2

    array-length v2, v1

    sub-int/2addr v2, v10

    invoke-static {v2, v15, v9}, Lf/h/a/c/i1/a0;->f(III)I

    move-result v2

    aget-wide v9, v1, v15

    cmp-long v15, v9, v11

    if-gtz v15, :cond_63

    aget-wide v9, v1, v17

    cmp-long v15, v11, v9

    if-gez v15, :cond_63

    aget-wide v9, v1, v2

    cmp-long v2, v9, v4

    if-gez v2, :cond_63

    cmp-long v2, v4, v6

    if-gtz v2, :cond_63

    const/4 v2, 0x1

    goto :goto_38

    :cond_63
    const/4 v2, 0x0

    :goto_38
    if-eqz v2, :cond_65

    sub-long v33, v6, v4

    const/4 v2, 0x0

    aget-wide v4, v1, v2

    sub-long v35, v11, v4

    iget-object v2, v14, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget v2, v2, Lcom/google/android/exoplayer2/Format;->z:I

    int-to-long v4, v2

    iget-wide v9, v14, Lf/h/a/c/a1/c0/i;->c:J

    move-wide/from16 v37, v4

    move-wide/from16 v39, v9

    invoke-static/range {v35 .. v40}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v4

    iget-object v2, v14, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget v2, v2, Lcom/google/android/exoplayer2/Format;->z:I

    int-to-long v9, v2

    iget-wide v11, v14, Lf/h/a/c/a1/c0/i;->c:J

    move-wide/from16 v35, v9

    move-wide/from16 v37, v11

    invoke-static/range {v33 .. v38}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v2, v4, v11

    if-nez v2, :cond_64

    cmp-long v2, v9, v11

    if-eqz v2, :cond_65

    :cond_64
    const-wide/32 v11, 0x7fffffff

    cmp-long v2, v4, v11

    if-gtz v2, :cond_65

    cmp-long v2, v9, v11

    if-gtz v2, :cond_65

    long-to-int v0, v4

    move-object/from16 v2, v31

    iput v0, v2, Lf/h/a/c/a1/m;->a:I

    long-to-int v0, v9

    iput v0, v2, Lf/h/a/c/a1/m;->b:I

    iget-wide v4, v14, Lf/h/a/c/a1/c0/i;->c:J

    const-wide/32 v6, 0xf4240

    invoke-static {v1, v6, v7, v4, v5}, Lf/h/a/c/i1/a0;->y([JJJ)V

    iget-object v0, v14, Lf/h/a/c/a1/c0/i;->h:[J

    const/4 v4, 0x0

    aget-wide v33, v0, v4

    const-wide/32 v35, 0xf4240

    iget-wide v4, v14, Lf/h/a/c/a1/c0/i;->d:J

    move-wide/from16 v37, v4

    invoke-static/range {v33 .. v38}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v27

    new-instance v0, Lf/h/a/c/a1/c0/l;

    move-object/from16 v20, v0

    move-object/from16 v21, v14

    move-object/from16 v22, v3

    move-object/from16 v23, v13

    move-object/from16 v25, v1

    move-object/from16 v26, v8

    invoke-direct/range {v20 .. v28}, Lf/h/a/c/a1/c0/l;-><init>(Lf/h/a/c/a1/c0/i;[J[II[J[IJ)V

    goto :goto_3c

    :cond_65
    :goto_39
    move-object/from16 v2, v31

    goto :goto_3a

    :cond_66
    move-object v13, v4

    move-object/from16 v32, v5

    move v0, v9

    goto :goto_39

    :goto_3a
    iget-object v4, v14, Lf/h/a/c/a1/c0/i;->h:[J

    array-length v5, v4

    const/4 v9, 0x1

    if-ne v5, v9, :cond_68

    const/4 v5, 0x0

    aget-wide v9, v4, v5

    const-wide/16 v11, 0x0

    cmp-long v15, v9, v11

    if-nez v15, :cond_68

    iget-object v0, v14, Lf/h/a/c/a1/c0/i;->i:[J

    aget-wide v4, v0, v5

    const/4 v0, 0x0

    :goto_3b
    array-length v9, v1

    if-ge v0, v9, :cond_67

    aget-wide v9, v1, v0

    sub-long v33, v9, v4

    const-wide/32 v35, 0xf4240

    iget-wide v9, v14, Lf/h/a/c/a1/c0/i;->c:J

    move-wide/from16 v37, v9

    invoke-static/range {v33 .. v38}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v9

    aput-wide v9, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3b

    :cond_67
    sub-long v33, v6, v4

    const-wide/32 v35, 0xf4240

    iget-wide v4, v14, Lf/h/a/c/a1/c0/i;->c:J

    move-wide/from16 v37, v4

    invoke-static/range {v33 .. v38}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v27

    new-instance v0, Lf/h/a/c/a1/c0/l;

    move-object/from16 v20, v0

    move-object/from16 v21, v14

    move-object/from16 v22, v3

    move-object/from16 v23, v13

    move-object/from16 v25, v1

    move-object/from16 v26, v8

    invoke-direct/range {v20 .. v28}, Lf/h/a/c/a1/c0/l;-><init>(Lf/h/a/c/a1/c0/i;[J[II[J[IJ)V

    :goto_3c
    move-object v8, v0

    :goto_3d
    move-object/from16 v31, v2

    goto/16 :goto_4a

    :cond_68
    iget v5, v14, Lf/h/a/c/a1/c0/i;->b:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_69

    const/4 v5, 0x1

    goto :goto_3e

    :cond_69
    const/4 v5, 0x0

    :goto_3e
    array-length v6, v4

    new-array v6, v6, [I

    array-length v4, v4

    new-array v4, v4, [I

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_3f
    iget-object v12, v14, Lf/h/a/c/a1/c0/i;->h:[J

    array-length v15, v12

    if-ge v7, v15, :cond_6d

    iget-object v15, v14, Lf/h/a/c/a1/c0/i;->i:[J

    move-object/from16 v31, v2

    move-object/from16 v17, v3

    aget-wide v2, v15, v7

    const-wide/16 v20, -0x1

    cmp-long v15, v2, v20

    if-eqz v15, :cond_6c

    aget-wide v33, v12, v7

    move-object v15, v13

    iget-wide v12, v14, Lf/h/a/c/a1/c0/i;->c:J

    move/from16 v20, v10

    move/from16 v21, v11

    iget-wide v10, v14, Lf/h/a/c/a1/c0/i;->d:J

    move-wide/from16 v35, v12

    move-wide/from16 v37, v10

    invoke-static/range {v33 .. v38}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v10

    const/4 v12, 0x1

    invoke-static {v1, v2, v3, v12, v12}, Lf/h/a/c/i1/a0;->b([JJZZ)I

    move-result v13

    aput v13, v6, v7

    add-long/2addr v2, v10

    const/4 v10, 0x0

    invoke-static {v1, v2, v3, v5, v10}, Lf/h/a/c/i1/a0;->b([JJZZ)I

    move-result v2

    aput v2, v4, v7

    :goto_40
    aget v2, v6, v7

    aget v3, v4, v7

    if-ge v2, v3, :cond_6a

    aget v2, v6, v7

    aget v2, v8, v2

    and-int/2addr v2, v12

    if-nez v2, :cond_6a

    aget v2, v6, v7

    add-int/2addr v2, v12

    aput v2, v6, v7

    const/4 v12, 0x1

    goto :goto_40

    :cond_6a
    aget v2, v4, v7

    aget v3, v6, v7

    sub-int/2addr v2, v3

    add-int/2addr v2, v9

    aget v3, v6, v7

    move/from16 v11, v21

    if-eq v11, v3, :cond_6b

    const/4 v3, 0x1

    goto :goto_41

    :cond_6b
    const/4 v3, 0x0

    :goto_41
    or-int v3, v3, v20

    aget v9, v4, v7

    move v10, v3

    move v11, v9

    move v9, v2

    goto :goto_42

    :cond_6c
    move/from16 v20, v10

    move-object v15, v13

    :goto_42
    add-int/lit8 v7, v7, 0x1

    move-object v13, v15

    move-object/from16 v3, v17

    move-object/from16 v2, v31

    goto :goto_3f

    :cond_6d
    move-object/from16 v31, v2

    move-object/from16 v17, v3

    move/from16 v20, v10

    move-object v15, v13

    if-eq v9, v0, :cond_6e

    const/4 v0, 0x1

    goto :goto_43

    :cond_6e
    const/4 v0, 0x0

    :goto_43
    or-int v0, v0, v20

    if-eqz v0, :cond_6f

    new-array v2, v9, [J

    goto :goto_44

    :cond_6f
    move-object/from16 v2, v17

    :goto_44
    if-eqz v0, :cond_70

    new-array v3, v9, [I

    goto :goto_45

    :cond_70
    move-object v3, v15

    :goto_45
    if-eqz v0, :cond_71

    const/16 v24, 0x0

    :cond_71
    if-eqz v0, :cond_72

    new-array v5, v9, [I

    goto :goto_46

    :cond_72
    move-object v5, v8

    :goto_46
    new-array v7, v9, [J

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    move-object/from16 v20, v15

    move/from16 v13, v24

    :goto_47
    iget-object v15, v14, Lf/h/a/c/a1/c0/i;->h:[J

    array-length v15, v15

    if-ge v9, v15, :cond_76

    iget-object v15, v14, Lf/h/a/c/a1/c0/i;->i:[J

    aget-wide v26, v15, v9

    aget v15, v6, v9

    move-object/from16 v28, v6

    aget v6, v4, v9

    if-eqz v0, :cond_73

    move-object/from16 v33, v4

    sub-int v4, v6, v15

    move/from16 v34, v13

    move-object/from16 v13, v17

    invoke-static {v13, v15, v2, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v13, v20

    invoke-static {v13, v15, v3, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v8, v15, v5, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_48

    :cond_73
    move-object/from16 v33, v4

    move/from16 v34, v13

    move-object/from16 v13, v20

    :goto_48
    move/from16 v4, v34

    :goto_49
    if-ge v15, v6, :cond_75

    const-wide/32 v34, 0xf4240

    move-object/from16 v36, v5

    move/from16 v37, v6

    iget-wide v5, v14, Lf/h/a/c/a1/c0/i;->d:J

    move-wide/from16 v20, v11

    move-wide/from16 v22, v34

    move-wide/from16 v24, v5

    invoke-static/range {v20 .. v25}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v5

    aget-wide v20, v1, v15

    sub-long v20, v20, v26

    move-object/from16 v38, v1

    move-object/from16 v39, v2

    iget-wide v1, v14, Lf/h/a/c/a1/c0/i;->c:J

    move-wide/from16 v24, v1

    invoke-static/range {v20 .. v25}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v1

    add-long/2addr v5, v1

    aput-wide v5, v7, v10

    if-eqz v0, :cond_74

    aget v1, v3, v10

    if-le v1, v4, :cond_74

    aget v1, v13, v15

    move v4, v1

    :cond_74
    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v5, v36

    move/from16 v6, v37

    move-object/from16 v1, v38

    move-object/from16 v2, v39

    goto :goto_49

    :cond_75
    move-object/from16 v38, v1

    move-object/from16 v39, v2

    move-object/from16 v36, v5

    iget-object v1, v14, Lf/h/a/c/a1/c0/i;->h:[J

    aget-wide v5, v1, v9

    add-long/2addr v11, v5

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v20, v13

    move-object/from16 v6, v28

    move-object/from16 v5, v36

    move-object/from16 v1, v38

    move v13, v4

    move-object/from16 v4, v33

    goto/16 :goto_47

    :cond_76
    move-object/from16 v39, v2

    move-object/from16 v36, v5

    move/from16 v34, v13

    const-wide/32 v22, 0xf4240

    iget-wide v0, v14, Lf/h/a/c/a1/c0/i;->d:J

    move-wide/from16 v20, v11

    move-wide/from16 v24, v0

    invoke-static/range {v20 .. v25}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v27

    new-instance v8, Lf/h/a/c/a1/c0/l;

    move-object/from16 v20, v8

    move-object/from16 v21, v14

    move-object/from16 v22, v39

    move-object/from16 v23, v3

    move/from16 v24, v34

    move-object/from16 v25, v7

    move-object/from16 v26, v36

    invoke-direct/range {v20 .. v28}, Lf/h/a/c/a1/c0/l;-><init>(Lf/h/a/c/a1/c0/i;[J[II[J[IJ)V

    :goto_4a
    iget v0, v8, Lf/h/a/c/a1/c0/l;->b:I

    if-nez v0, :cond_77

    move-object/from16 v0, v30

    goto :goto_4b

    :cond_77
    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4b
    add-int/lit8 v6, v29, 0x1

    move-object/from16 v1, p0

    move-object v4, v0

    move-object/from16 v2, v16

    move-object/from16 v3, v31

    move-object/from16 v5, v32

    move-object/from16 v0, p1

    goto/16 :goto_17

    :cond_78
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Track has no sample table size information"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_79
    move-object/from16 v16, v2

    move-object/from16 v31, v3

    move-object v0, v4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-wide v4, v2

    const/4 v6, 0x0

    const/4 v7, -0x1

    :goto_4c
    if-ge v6, v1, :cond_7d

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/h/a/c/a1/c0/l;

    iget-object v9, v8, Lf/h/a/c/a1/c0/l;->a:Lf/h/a/c/a1/c0/i;

    iget-wide v10, v9, Lf/h/a/c/a1/c0/i;->e:J

    cmp-long v12, v10, v4

    if-eqz v12, :cond_7a

    goto :goto_4d

    :cond_7a
    iget-wide v10, v8, Lf/h/a/c/a1/c0/l;->h:J

    :goto_4d
    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    new-instance v4, Lf/h/a/c/a1/c0/f$a;

    move-object/from16 v5, p0

    iget-object v12, v5, Lf/h/a/c/a1/c0/f;->o:Lf/h/a/c/a1/i;

    iget v13, v9, Lf/h/a/c/a1/c0/i;->b:I

    invoke-interface {v12, v6, v13}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v12

    invoke-direct {v4, v9, v8, v12}, Lf/h/a/c/a1/c0/f$a;-><init>(Lf/h/a/c/a1/c0/i;Lf/h/a/c/a1/c0/l;Lf/h/a/c/a1/s;)V

    iget v12, v8, Lf/h/a/c/a1/c0/l;->e:I

    add-int/lit8 v42, v12, 0x1e

    iget-object v12, v9, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    new-instance v13, Lcom/google/android/exoplayer2/Format;

    move-object/from16 v32, v13

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->d:Ljava/lang/String;

    move-object/from16 v33, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->e:Ljava/lang/String;

    move-object/from16 v34, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->f:I

    move/from16 v35, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->g:I

    move/from16 v36, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->h:I

    move/from16 v37, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->i:Ljava/lang/String;

    move-object/from16 v38, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->j:Lcom/google/android/exoplayer2/metadata/Metadata;

    move-object/from16 v39, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->k:Ljava/lang/String;

    move-object/from16 v40, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    move-object/from16 v41, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->n:Ljava/util/List;

    move-object/from16 v43, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    move-object/from16 v44, v14

    iget-wide v14, v12, Lcom/google/android/exoplayer2/Format;->p:J

    move-wide/from16 v45, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->q:I

    move/from16 v47, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->r:I

    move/from16 v48, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->s:F

    move/from16 v49, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->t:I

    move/from16 v50, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->u:F

    move/from16 v51, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->w:[B

    move-object/from16 v52, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->v:I

    move/from16 v53, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->x:Lcom/google/android/exoplayer2/video/ColorInfo;

    move-object/from16 v54, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->y:I

    move/from16 v55, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->z:I

    move/from16 v56, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->A:I

    move/from16 v57, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->B:I

    move/from16 v58, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->C:I

    move/from16 v59, v14

    iget-object v14, v12, Lcom/google/android/exoplayer2/Format;->D:Ljava/lang/String;

    move-object/from16 v60, v14

    iget v14, v12, Lcom/google/android/exoplayer2/Format;->E:I

    move/from16 v61, v14

    iget-object v12, v12, Lcom/google/android/exoplayer2/Format;->F:Ljava/lang/Class;

    move-object/from16 v62, v12

    invoke-direct/range {v32 .. v62}, Lcom/google/android/exoplayer2/Format;-><init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Lcom/google/android/exoplayer2/metadata/Metadata;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;JIIFIF[BILcom/google/android/exoplayer2/video/ColorInfo;IIIIILjava/lang/String;ILjava/lang/Class;)V

    iget v12, v9, Lf/h/a/c/a1/c0/i;->b:I

    const/4 v14, 0x2

    if-ne v12, v14, :cond_7b

    const-wide/16 v14, 0x0

    cmp-long v12, v10, v14

    if-lez v12, :cond_7b

    iget v8, v8, Lf/h/a/c/a1/c0/l;->b:I

    const/4 v12, 0x1

    if-le v8, v12, :cond_7b

    int-to-float v8, v8

    long-to-float v10, v10

    const v11, 0x49742400    # 1000000.0f

    div-float/2addr v10, v11

    div-float/2addr v8, v10

    invoke-virtual {v13, v8}, Lcom/google/android/exoplayer2/Format;->b(F)Lcom/google/android/exoplayer2/Format;

    move-result-object v13

    :cond_7b
    iget v8, v9, Lf/h/a/c/a1/c0/i;->b:I

    move-object/from16 v12, v16

    move-object/from16 v11, v19

    move-object/from16 v10, v31

    invoke-static {v8, v13, v11, v12, v10}, Lf/h/a/c/a1/c0/e;->a(ILcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/metadata/Metadata;Lcom/google/android/exoplayer2/metadata/Metadata;Lf/h/a/c/a1/m;)Lcom/google/android/exoplayer2/Format;

    move-result-object v8

    iget-object v13, v4, Lf/h/a/c/a1/c0/f$a;->c:Lf/h/a/c/a1/s;

    invoke-interface {v13, v8}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    iget v8, v9, Lf/h/a/c/a1/c0/i;->b:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_7c

    const/4 v8, -0x1

    if-ne v7, v8, :cond_7c

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v7

    :cond_7c
    move-object/from16 v8, v18

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    const-wide v13, -0x7fffffffffffffffL    # -4.9E-324

    move-object/from16 v18, v8

    move-object/from16 v31, v10

    move-object/from16 v19, v11

    move-object/from16 v16, v12

    move-wide v4, v13

    goto/16 :goto_4c

    :cond_7d
    move-object/from16 v5, p0

    move-object/from16 v8, v18

    const-wide/16 v0, 0x0

    iput v7, v5, Lf/h/a/c/a1/c0/f;->r:I

    iput-wide v2, v5, Lf/h/a/c/a1/c0/f;->s:J

    const/4 v2, 0x0

    new-array v2, v2, [Lf/h/a/c/a1/c0/f$a;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lf/h/a/c/a1/c0/f$a;

    iput-object v2, v5, Lf/h/a/c/a1/c0/f;->p:[Lf/h/a/c/a1/c0/f$a;

    array-length v3, v2

    new-array v3, v3, [[J

    array-length v4, v2

    new-array v4, v4, [I

    array-length v6, v2

    new-array v6, v6, [J

    array-length v7, v2

    new-array v7, v7, [Z

    const/4 v8, 0x0

    :goto_4e
    array-length v9, v2

    if-ge v8, v9, :cond_7e

    aget-object v9, v2, v8

    iget-object v9, v9, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    iget v9, v9, Lf/h/a/c/a1/c0/l;->b:I

    new-array v9, v9, [J

    aput-object v9, v3, v8

    aget-object v9, v2, v8

    iget-object v9, v9, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    iget-object v9, v9, Lf/h/a/c/a1/c0/l;->f:[J

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    aput-wide v10, v6, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_4e

    :cond_7e
    const/4 v8, 0x0

    :goto_4f
    array-length v9, v2

    if-ge v8, v9, :cond_82

    const-wide v9, 0x7fffffffffffffffL

    const/4 v11, 0x0

    const/4 v12, -0x1

    :goto_50
    array-length v13, v2

    if-ge v11, v13, :cond_80

    aget-boolean v13, v7, v11

    if-nez v13, :cond_7f

    aget-wide v13, v6, v11

    cmp-long v15, v13, v9

    if-gtz v15, :cond_7f

    aget-wide v9, v6, v11

    move v12, v11

    :cond_7f
    add-int/lit8 v11, v11, 0x1

    goto :goto_50

    :cond_80
    aget v9, v4, v12

    aget-object v10, v3, v12

    aput-wide v0, v10, v9

    aget-object v10, v2, v12

    iget-object v10, v10, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    iget-object v10, v10, Lf/h/a/c/a1/c0/l;->d:[I

    aget v10, v10, v9

    int-to-long v10, v10

    add-long/2addr v0, v10

    const/4 v10, 0x1

    add-int/2addr v9, v10

    aput v9, v4, v12

    aget-object v11, v3, v12

    array-length v11, v11

    if-ge v9, v11, :cond_81

    aget-object v10, v2, v12

    iget-object v10, v10, Lf/h/a/c/a1/c0/f$a;->b:Lf/h/a/c/a1/c0/l;

    iget-object v10, v10, Lf/h/a/c/a1/c0/l;->f:[J

    aget-wide v9, v10, v9

    aput-wide v9, v6, v12

    goto :goto_4f

    :cond_81
    aput-boolean v10, v7, v12

    add-int/lit8 v8, v8, 0x1

    goto :goto_4f

    :cond_82
    iput-object v3, v5, Lf/h/a/c/a1/c0/f;->q:[[J

    iget-object v0, v5, Lf/h/a/c/a1/c0/f;->o:Lf/h/a/c/a1/i;

    invoke-interface {v0}, Lf/h/a/c/a1/i;->k()V

    iget-object v0, v5, Lf/h/a/c/a1/c0/f;->o:Lf/h/a/c/a1/i;

    invoke-interface {v0, v5}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    return-void
.end method

.method public release()V
    .locals 0

    return-void
.end method
