.class public Lf/h/a/c/a1/c0/d;
.super Ljava/lang/Object;
.source "FragmentedMp4Extractor.java"

# interfaces
.implements Lf/h/a/c/a1/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/a1/c0/d$b;,
        Lf/h/a/c/a1/c0/d$a;
    }
.end annotation


# static fields
.field public static final F:[B

.field public static final G:Lcom/google/android/exoplayer2/Format;


# instance fields
.field public A:Z

.field public B:Lf/h/a/c/a1/i;

.field public C:[Lf/h/a/c/a1/s;

.field public D:[Lf/h/a/c/a1/s;

.field public E:Z

.field public final a:I

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/Format;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/a1/c0/d$b;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lf/h/a/c/i1/r;

.field public final e:Lf/h/a/c/i1/r;

.field public final f:Lf/h/a/c/i1/r;

.field public final g:[B

.field public final h:Lf/h/a/c/i1/r;

.field public final i:Lf/h/a/c/c1/g/b;

.field public final j:Lf/h/a/c/i1/r;

.field public final k:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lf/h/a/c/a1/c0/a$a;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lf/h/a/c/a1/c0/d$a;",
            ">;"
        }
    .end annotation
.end field

.field public m:I

.field public n:I

.field public o:J

.field public p:I

.field public q:Lf/h/a/c/i1/r;

.field public r:J

.field public s:I

.field public t:J

.field public u:J

.field public v:J

.field public w:Lf/h/a/c/a1/c0/d$b;

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lf/h/a/c/a1/c0/d;->F:[B

    const/4 v0, 0x0

    const-string v1, "application/x-emsg"

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/Format;->i(Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    sput-object v0, Lf/h/a/c/a1/c0/d;->G:Lcom/google/android/exoplayer2/Format;

    return-void

    :array_0
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    or-int/lit8 p1, p1, 0x0

    iput p1, p0, Lf/h/a/c/a1/c0/d;->a:I

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->b:Ljava/util/List;

    new-instance p1, Lf/h/a/c/c1/g/b;

    invoke-direct {p1}, Lf/h/a/c/c1/g/b;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->i:Lf/h/a/c/c1/g/b;

    new-instance p1, Lf/h/a/c/i1/r;

    const/16 v0, 0x10

    invoke-direct {p1, v0}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    sget-object v1, Lf/h/a/c/i1/p;->a:[B

    invoke-direct {p1, v1}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->d:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    const/4 v1, 0x5

    invoke-direct {p1, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->e:Lf/h/a/c/i1/r;

    new-instance p1, Lf/h/a/c/i1/r;

    invoke-direct {p1}, Lf/h/a/c/i1/r;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    new-array p1, v0, [B

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->g:[B

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0, p1}, Lf/h/a/c/i1/r;-><init>([B)V

    iput-object v0, p0, Lf/h/a/c/a1/c0/d;->h:Lf/h/a/c/i1/r;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->l:Ljava/util/ArrayDeque;

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lf/h/a/c/a1/c0/d;->u:J

    iput-wide v0, p0, Lf/h/a/c/a1/c0/d;->t:J

    iput-wide v0, p0, Lf/h/a/c/a1/c0/d;->v:J

    invoke-virtual {p0}, Lf/h/a/c/a1/c0/d;->a()V

    return-void
.end method

.method public static c(Ljava/util/List;)Lcom/google/android/exoplayer2/drm/DrmInitData;
    .locals 14
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/c/a1/c0/a$b;",
            ">;)",
            "Lcom/google/android/exoplayer2/drm/DrmInitData;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v4, v2

    :goto_0
    if-ge v3, v0, :cond_a

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/a1/c0/a$b;

    iget v6, v5, Lf/h/a/c/a1/c0/a;->a:I

    const v7, 0x70737368    # 3.013775E29f

    if-ne v6, v7, :cond_9

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    iget-object v5, v5, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    new-instance v6, Lf/h/a/c/i1/r;

    invoke-direct {v6, v5}, Lf/h/a/c/i1/r;-><init>([B)V

    iget v8, v6, Lf/h/a/c/i1/r;->c:I

    const/16 v9, 0x20

    if-ge v8, v9, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v6, v1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->a()I

    move-result v9

    add-int/lit8 v9, v9, 0x4

    if-eq v8, v9, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    if-eq v8, v7, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {v6}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    shr-int/lit8 v7, v7, 0x18

    and-int/lit16 v7, v7, 0xff

    const/4 v8, 0x1

    if-le v7, v8, :cond_4

    const-string v6, "Unsupported pssh version: "

    const-string v8, "PsshAtomUtil"

    invoke-static {v6, v7, v8}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_1

    :cond_4
    new-instance v9, Ljava/util/UUID;

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->k()J

    move-result-wide v10

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->k()J

    move-result-wide v12

    invoke-direct {v9, v10, v11, v12, v13}, Ljava/util/UUID;-><init>(JJ)V

    if-ne v7, v8, :cond_5

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->t()I

    move-result v8

    mul-int/lit8 v8, v8, 0x10

    invoke-virtual {v6, v8}, Lf/h/a/c/i1/r;->D(I)V

    :cond_5
    invoke-virtual {v6}, Lf/h/a/c/i1/r;->t()I

    move-result v8

    invoke-virtual {v6}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    if-eq v8, v10, :cond_6

    :goto_1
    move-object v6, v2

    goto :goto_2

    :cond_6
    new-array v10, v8, [B

    iget-object v11, v6, Lf/h/a/c/i1/r;->a:[B

    iget v12, v6, Lf/h/a/c/i1/r;->b:I

    invoke-static {v11, v12, v10, v1, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v11, v6, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v11, v8

    iput v11, v6, Lf/h/a/c/i1/r;->b:I

    new-instance v6, Lf/h/a/c/a1/c0/g;

    invoke-direct {v6, v9, v7, v10}, Lf/h/a/c/a1/c0/g;-><init>(Ljava/util/UUID;I[B)V

    :goto_2
    if-nez v6, :cond_7

    move-object v6, v2

    goto :goto_3

    :cond_7
    iget-object v6, v6, Lf/h/a/c/a1/c0/g;->a:Ljava/util/UUID;

    :goto_3
    if-nez v6, :cond_8

    const-string v5, "FragmentedMp4Extractor"

    const-string v6, "Skipped pssh atom (failed to extract uuid)"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_8
    new-instance v7, Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;

    const-string v8, "video/mp4"

    invoke-direct {v7, v6, v8, v5}, Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;-><init>(Ljava/util/UUID;Ljava/lang/String;[B)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_a
    if-nez v4, :cond_b

    goto :goto_5

    :cond_b
    new-instance p0, Lcom/google/android/exoplayer2/drm/DrmInitData;

    new-array v0, v1, [Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/exoplayer2/drm/DrmInitData;-><init>(Ljava/lang/String;Z[Lcom/google/android/exoplayer2/drm/DrmInitData$SchemeData;)V

    move-object v2, p0

    :goto_5
    return-object v2
.end method

.method public static i(Lf/h/a/c/i1/r;ILf/h/a/c/a1/c0/k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    add-int/lit8 p1, p1, 0x8

    invoke-virtual {p0, p1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {p0}, Lf/h/a/c/i1/r;->e()I

    move-result p1

    const v0, 0xffffff

    and-int/2addr p1, v0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_2

    and-int/lit8 p1, p1, 0x2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0}, Lf/h/a/c/i1/r;->t()I

    move-result v1

    iget v2, p2, Lf/h/a/c/a1/c0/k;->e:I

    if-ne v1, v2, :cond_1

    iget-object v2, p2, Lf/h/a/c/a1/c0/k;->m:[Z

    invoke-static {v2, v0, v1, p1}, Ljava/util/Arrays;->fill([ZIIZ)V

    invoke-virtual {p0}, Lf/h/a/c/i1/r;->a()I

    move-result p1

    invoke-virtual {p2, p1}, Lf/h/a/c/a1/c0/k;->a(I)V

    iget-object p1, p2, Lf/h/a/c/a1/c0/k;->p:Lf/h/a/c/i1/r;

    iget-object p1, p1, Lf/h/a/c/i1/r;->a:[B

    iget v1, p2, Lf/h/a/c/a1/c0/k;->o:I

    invoke-virtual {p0, p1, v0, v1}, Lf/h/a/c/i1/r;->d([BII)V

    iget-object p0, p2, Lf/h/a/c/a1/c0/k;->p:Lf/h/a/c/i1/r;

    invoke-virtual {p0, v0}, Lf/h/a/c/i1/r;->C(I)V

    iput-boolean v0, p2, Lf/h/a/c/a1/c0/k;->q:Z

    return-void

    :cond_1
    new-instance p0, Lcom/google/android/exoplayer2/ParserException;

    const-string p1, "Length mismatch: "

    const-string v0, ", "

    invoke-static {p1, v1, v0}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget p2, p2, Lf/h/a/c/a1/c0/k;->e:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance p0, Lcom/google/android/exoplayer2/ParserException;

    const-string p1, "Overriding TrackEncryptionBox parameters is unsupported."

    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/a1/c0/d;->m:I

    iput v0, p0, Lf/h/a/c/a1/c0/d;->p:I

    return-void
.end method

.method public final b(Landroid/util/SparseArray;I)Lf/h/a/c/a1/c0/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Lf/h/a/c/a1/c0/c;",
            ">;I)",
            "Lf/h/a/c/a1/c0/c;"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/c/a1/c0/c;

    return-object p1

    :cond_0
    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/a1/c0/c;

    return-object p1
.end method

.method public d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    :goto_0
    iget v2, v1, Lf/h/a/c/a1/c0/d;->m:I

    const v3, 0x656d7367

    const v4, 0x73696478

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v2, :cond_32

    const-string v8, "FragmentedMp4Extractor"

    if-eq v2, v7, :cond_23

    const-wide v3, 0x7fffffffffffffffL

    const/4 v7, 0x3

    if-eq v2, v5, :cond_1d

    const/4 v5, 0x6

    if-ne v2, v7, :cond_f

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    if-nez v2, :cond_6

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v9

    const/4 v10, 0x0

    move-object v11, v6

    :goto_1
    if-ge v10, v9, :cond_2

    invoke-virtual {v2, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lf/h/a/c/a1/c0/d$b;

    iget v13, v12, Lf/h/a/c/a1/c0/d$b;->h:I

    iget-object v14, v12, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget v15, v14, Lf/h/a/c/a1/c0/k;->d:I

    if-ne v13, v15, :cond_0

    goto :goto_2

    :cond_0
    iget-object v14, v14, Lf/h/a/c/a1/c0/k;->f:[J

    aget-wide v13, v14, v13

    cmp-long v15, v13, v3

    if-gez v15, :cond_1

    move-object v11, v12

    move-wide v3, v13

    :cond_1
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_2
    if-nez v11, :cond_4

    iget-wide v2, v1, Lf/h/a/c/a1/c0/d;->r:J

    iget-wide v4, v0, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v2, v4

    long-to-int v3, v2

    if-ltz v3, :cond_3

    invoke-virtual {v0, v3}, Lf/h/a/c/a1/e;->i(I)V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/c0/d;->a()V

    const/4 v2, 0x0

    goto/16 :goto_f

    :cond_3
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Offset to end of mdat was negative."

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v2, v11, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v2, v2, Lf/h/a/c/a1/c0/k;->f:[J

    iget v3, v11, Lf/h/a/c/a1/c0/d$b;->h:I

    aget-wide v3, v2, v3

    iget-wide v9, v0, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v3, v9

    long-to-int v2, v3

    if-gez v2, :cond_5

    const-string v2, "Ignoring negative offset to sample data."

    invoke-static {v8, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :cond_5
    invoke-virtual {v0, v2}, Lf/h/a/c/a1/e;->i(I)V

    iput-object v11, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    :cond_6
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    iget-object v3, v2, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v3, v3, Lf/h/a/c/a1/c0/k;->h:[I

    iget v4, v2, Lf/h/a/c/a1/c0/d$b;->f:I

    aget v3, v3, v4

    iput v3, v1, Lf/h/a/c/a1/c0/d;->x:I

    iget v8, v2, Lf/h/a/c/a1/c0/d$b;->i:I

    if-ge v4, v8, :cond_c

    invoke-virtual {v0, v3}, Lf/h/a/c/a1/e;->i(I)V

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    invoke-virtual {v2}, Lf/h/a/c/a1/c0/d$b;->a()Lf/h/a/c/a1/c0/j;

    move-result-object v3

    if-nez v3, :cond_7

    goto :goto_4

    :cond_7
    iget-object v4, v2, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v4, v4, Lf/h/a/c/a1/c0/k;->p:Lf/h/a/c/i1/r;

    iget v3, v3, Lf/h/a/c/a1/c0/j;->d:I

    if-eqz v3, :cond_8

    invoke-virtual {v4, v3}, Lf/h/a/c/i1/r;->D(I)V

    :cond_8
    iget-object v3, v2, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget v2, v2, Lf/h/a/c/a1/c0/d$b;->f:I

    iget-boolean v5, v3, Lf/h/a/c/a1/c0/k;->l:Z

    if-eqz v5, :cond_9

    iget-object v3, v3, Lf/h/a/c/a1/c0/k;->m:[Z

    aget-boolean v2, v3, v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_a

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->v()I

    move-result v2

    mul-int/lit8 v2, v2, 0x6

    invoke-virtual {v4, v2}, Lf/h/a/c/i1/r;->D(I)V

    :cond_a
    :goto_4
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    invoke-virtual {v2}, Lf/h/a/c/a1/c0/d$b;->c()Z

    move-result v2

    if-nez v2, :cond_b

    iput-object v6, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    :cond_b
    iput v7, v1, Lf/h/a/c/a1/c0/d;->m:I

    goto/16 :goto_e

    :cond_c
    iget-object v2, v2, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    iget v2, v2, Lf/h/a/c/a1/c0/i;->g:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_d

    add-int/lit8 v3, v3, -0x8

    iput v3, v1, Lf/h/a/c/a1/c0/d;->x:I

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lf/h/a/c/a1/e;->i(I)V

    :cond_d
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    iget-object v2, v2, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    iget-object v2, v2, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget-object v2, v2, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v3, "audio/ac4"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    iget v3, v1, Lf/h/a/c/a1/c0/d;->x:I

    const/4 v4, 0x7

    invoke-virtual {v2, v3, v4}, Lf/h/a/c/a1/c0/d$b;->d(II)I

    move-result v2

    iput v2, v1, Lf/h/a/c/a1/c0/d;->y:I

    iget v2, v1, Lf/h/a/c/a1/c0/d;->x:I

    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->h:Lf/h/a/c/i1/r;

    invoke-static {v2, v3}, Lf/h/a/c/w0/h;->a(ILf/h/a/c/i1/r;)V

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    iget-object v2, v2, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->h:Lf/h/a/c/i1/r;

    invoke-interface {v2, v3, v4}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v2, v1, Lf/h/a/c/a1/c0/d;->y:I

    add-int/2addr v2, v4

    iput v2, v1, Lf/h/a/c/a1/c0/d;->y:I

    goto :goto_5

    :cond_e
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    iget v3, v1, Lf/h/a/c/a1/c0/d;->x:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lf/h/a/c/a1/c0/d$b;->d(II)I

    move-result v2

    iput v2, v1, Lf/h/a/c/a1/c0/d;->y:I

    :goto_5
    const/4 v2, 0x0

    iget v3, v1, Lf/h/a/c/a1/c0/d;->x:I

    iget v4, v1, Lf/h/a/c/a1/c0/d;->y:I

    add-int/2addr v3, v4

    iput v3, v1, Lf/h/a/c/a1/c0/d;->x:I

    const/4 v3, 0x4

    iput v3, v1, Lf/h/a/c/a1/c0/d;->m:I

    iput v2, v1, Lf/h/a/c/a1/c0/d;->z:I

    :cond_f
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    iget-object v3, v2, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v4, v2, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    iget-object v6, v2, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    iget v2, v2, Lf/h/a/c/a1/c0/d$b;->f:I

    iget-object v7, v3, Lf/h/a/c/a1/c0/k;->j:[J

    aget-wide v8, v7, v2

    iget-object v7, v3, Lf/h/a/c/a1/c0/k;->i:[I

    aget v7, v7, v2

    int-to-long v10, v7

    add-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    mul-long v13, v8, v10

    iget v7, v4, Lf/h/a/c/a1/c0/i;->j:I

    if-eqz v7, :cond_17

    iget-object v8, v1, Lf/h/a/c/a1/c0/d;->e:Lf/h/a/c/i1/r;

    iget-object v8, v8, Lf/h/a/c/i1/r;->a:[B

    const/4 v9, 0x0

    aput-byte v9, v8, v9

    const/4 v10, 0x1

    aput-byte v9, v8, v10

    const/4 v10, 0x2

    aput-byte v9, v8, v10

    add-int/lit8 v9, v7, 0x1

    rsub-int/lit8 v7, v7, 0x4

    :goto_6
    iget v10, v1, Lf/h/a/c/a1/c0/d;->y:I

    iget v11, v1, Lf/h/a/c/a1/c0/d;->x:I

    if-ge v10, v11, :cond_18

    iget v10, v1, Lf/h/a/c/a1/c0/d;->z:I

    const-string v11, "video/hevc"

    if-nez v10, :cond_15

    const/4 v10, 0x0

    invoke-virtual {v0, v8, v7, v9, v10}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object v12, v1, Lf/h/a/c/a1/c0/d;->e:Lf/h/a/c/i1/r;

    invoke-virtual {v12, v10}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v12, v1, Lf/h/a/c/a1/c0/d;->e:Lf/h/a/c/i1/r;

    invoke-virtual {v12}, Lf/h/a/c/i1/r;->e()I

    move-result v12

    const/4 v15, 0x1

    if-lt v12, v15, :cond_14

    add-int/lit8 v12, v12, -0x1

    iput v12, v1, Lf/h/a/c/a1/c0/d;->z:I

    iget-object v12, v1, Lf/h/a/c/a1/c0/d;->d:Lf/h/a/c/i1/r;

    invoke-virtual {v12, v10}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v10, v1, Lf/h/a/c/a1/c0/d;->d:Lf/h/a/c/i1/r;

    const/4 v12, 0x4

    invoke-interface {v6, v10, v12}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget-object v10, v1, Lf/h/a/c/a1/c0/d;->e:Lf/h/a/c/i1/r;

    invoke-interface {v6, v10, v15}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget-object v10, v1, Lf/h/a/c/a1/c0/d;->D:[Lf/h/a/c/a1/s;

    array-length v10, v10

    if-lez v10, :cond_13

    iget-object v10, v4, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget-object v10, v10, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    aget-byte v12, v8, v12

    sget-object v15, Lf/h/a/c/i1/p;->a:[B

    const-string v15, "video/avc"

    invoke-virtual {v15, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_10

    and-int/lit8 v15, v12, 0x1f

    if-eq v15, v5, :cond_11

    :cond_10
    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    and-int/lit8 v5, v12, 0x7e

    shr-int/lit8 v5, v5, 0x1

    const/16 v10, 0x27

    if-ne v5, v10, :cond_12

    :cond_11
    const/4 v5, 0x1

    goto :goto_7

    :cond_12
    const/4 v5, 0x0

    :goto_7
    if-eqz v5, :cond_13

    const/4 v5, 0x1

    goto :goto_8

    :cond_13
    const/4 v5, 0x0

    :goto_8
    iput-boolean v5, v1, Lf/h/a/c/a1/c0/d;->A:Z

    iget v5, v1, Lf/h/a/c/a1/c0/d;->y:I

    add-int/lit8 v5, v5, 0x5

    iput v5, v1, Lf/h/a/c/a1/c0/d;->y:I

    iget v5, v1, Lf/h/a/c/a1/c0/d;->x:I

    add-int/2addr v5, v7

    iput v5, v1, Lf/h/a/c/a1/c0/d;->x:I

    goto :goto_a

    :cond_14
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Invalid NAL length"

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    iget-boolean v5, v1, Lf/h/a/c/a1/c0/d;->A:Z

    if-eqz v5, :cond_16

    iget-object v5, v1, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v10}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v5, v1, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    iget v10, v1, Lf/h/a/c/a1/c0/d;->z:I

    const/4 v12, 0x0

    invoke-virtual {v0, v5, v12, v10, v12}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object v5, v1, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    iget v10, v1, Lf/h/a/c/a1/c0/d;->z:I

    invoke-interface {v6, v5, v10}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    iget v5, v1, Lf/h/a/c/a1/c0/d;->z:I

    iget-object v10, v1, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    iget-object v12, v10, Lf/h/a/c/i1/r;->a:[B

    iget v10, v10, Lf/h/a/c/i1/r;->c:I

    invoke-static {v12, v10}, Lf/h/a/c/i1/p;->e([BI)I

    move-result v10

    iget-object v12, v1, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    iget-object v15, v4, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    iget-object v15, v15, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-virtual {v11, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v12, v11}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v11, v1, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    invoke-virtual {v11, v10}, Lf/h/a/c/i1/r;->B(I)V

    iget-object v10, v1, Lf/h/a/c/a1/c0/d;->f:Lf/h/a/c/i1/r;

    iget-object v11, v1, Lf/h/a/c/a1/c0/d;->D:[Lf/h/a/c/a1/s;

    invoke-static {v13, v14, v10, v11}, Lf/g/j/k/a;->z(JLf/h/a/c/i1/r;[Lf/h/a/c/a1/s;)V

    goto :goto_9

    :cond_16
    const/4 v5, 0x0

    invoke-interface {v6, v0, v10, v5}, Lf/h/a/c/a1/s;->a(Lf/h/a/c/a1/e;IZ)I

    move-result v5

    :goto_9
    iget v10, v1, Lf/h/a/c/a1/c0/d;->y:I

    add-int/2addr v10, v5

    iput v10, v1, Lf/h/a/c/a1/c0/d;->y:I

    iget v10, v1, Lf/h/a/c/a1/c0/d;->z:I

    sub-int/2addr v10, v5

    iput v10, v1, Lf/h/a/c/a1/c0/d;->z:I

    :goto_a
    const/4 v5, 0x6

    goto/16 :goto_6

    :cond_17
    :goto_b
    iget v4, v1, Lf/h/a/c/a1/c0/d;->y:I

    iget v5, v1, Lf/h/a/c/a1/c0/d;->x:I

    if-ge v4, v5, :cond_18

    sub-int/2addr v5, v4

    const/4 v4, 0x0

    invoke-interface {v6, v0, v5, v4}, Lf/h/a/c/a1/s;->a(Lf/h/a/c/a1/e;IZ)I

    move-result v4

    iget v5, v1, Lf/h/a/c/a1/c0/d;->y:I

    add-int/2addr v5, v4

    iput v5, v1, Lf/h/a/c/a1/c0/d;->y:I

    goto :goto_b

    :cond_18
    iget-object v3, v3, Lf/h/a/c/a1/c0/k;->k:[Z

    aget-boolean v2, v3, v2

    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    invoke-virtual {v3}, Lf/h/a/c/a1/c0/d$b;->a()Lf/h/a/c/a1/c0/j;

    move-result-object v3

    if-eqz v3, :cond_19

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v2, v4

    iget-object v3, v3, Lf/h/a/c/a1/c0/j;->c:Lf/h/a/c/a1/s$a;

    goto :goto_c

    :cond_19
    const/4 v3, 0x0

    :goto_c
    move v9, v2

    move-object v12, v3

    iget v10, v1, Lf/h/a/c/a1/c0/d;->x:I

    const/4 v11, 0x0

    move-wide v7, v13

    invoke-interface/range {v6 .. v12}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    :cond_1a
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1b

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->l:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/a1/c0/d$a;

    iget v3, v1, Lf/h/a/c/a1/c0/d;->s:I

    iget v4, v2, Lf/h/a/c/a1/c0/d$a;->b:I

    sub-int/2addr v3, v4

    iput v3, v1, Lf/h/a/c/a1/c0/d;->s:I

    iget-wide v3, v2, Lf/h/a/c/a1/c0/d$a;->a:J

    add-long/2addr v3, v13

    iget-object v12, v1, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    array-length v15, v12

    const/4 v5, 0x0

    const/4 v11, 0x0

    :goto_d
    if-ge v11, v15, :cond_1a

    aget-object v5, v12, v11

    const/4 v8, 0x1

    iget v9, v2, Lf/h/a/c/a1/c0/d$a;->b:I

    iget v10, v1, Lf/h/a/c/a1/c0/d;->s:I

    const/16 v16, 0x0

    move-wide v6, v3

    move/from16 v17, v11

    move-object/from16 v11, v16

    invoke-interface/range {v5 .. v11}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    add-int/lit8 v11, v17, 0x1

    goto :goto_d

    :cond_1b
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    invoke-virtual {v2}, Lf/h/a/c/a1/c0/d$b;->c()Z

    move-result v2

    if-nez v2, :cond_1c

    const/4 v2, 0x0

    iput-object v2, v1, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    :cond_1c
    const/4 v2, 0x3

    iput v2, v1, Lf/h/a/c/a1/c0/d;->m:I

    :goto_e
    const/4 v2, 0x1

    :goto_f
    if-eqz v2, :cond_20

    const/4 v0, 0x0

    return v0

    :cond_1d
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_10
    if-ge v5, v2, :cond_1f

    iget-object v7, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v7, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/a/c/a1/c0/d$b;

    iget-object v7, v7, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-boolean v8, v7, Lf/h/a/c/a1/c0/k;->q:Z

    if-eqz v8, :cond_1e

    iget-wide v7, v7, Lf/h/a/c/a1/c0/k;->c:J

    cmp-long v9, v7, v3

    if-gez v9, :cond_1e

    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/a1/c0/d$b;

    move-object v6, v3

    move-wide v3, v7

    :cond_1e
    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    :cond_1f
    if-nez v6, :cond_21

    const/4 v2, 0x3

    iput v2, v1, Lf/h/a/c/a1/c0/d;->m:I

    :cond_20
    :goto_11
    move-object v5, v1

    goto/16 :goto_23

    :cond_21
    iget-wide v7, v0, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v3, v7

    long-to-int v2, v3

    if-ltz v2, :cond_22

    invoke-virtual {v0, v2}, Lf/h/a/c/a1/e;->i(I)V

    iget-object v2, v6, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v3, v2, Lf/h/a/c/a1/c0/k;->p:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/i1/r;->a:[B

    iget v4, v2, Lf/h/a/c/a1/c0/k;->o:I

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5, v4, v5}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget-object v3, v2, Lf/h/a/c/a1/c0/k;->p:Lf/h/a/c/i1/r;

    invoke-virtual {v3, v5}, Lf/h/a/c/i1/r;->C(I)V

    iput-boolean v5, v2, Lf/h/a/c/a1/c0/k;->q:Z

    goto :goto_11

    :cond_22
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Offset to encryption data was negative."

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_23
    iget-wide v5, v1, Lf/h/a/c/a1/c0/d;->o:J

    long-to-int v2, v5

    iget v5, v1, Lf/h/a/c/a1/c0/d;->p:I

    sub-int/2addr v2, v5

    iget-object v5, v1, Lf/h/a/c/a1/c0/d;->q:Lf/h/a/c/i1/r;

    if-eqz v5, :cond_31

    iget-object v5, v5, Lf/h/a/c/i1/r;->a:[B

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-virtual {v0, v5, v6, v2, v7}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    new-instance v2, Lf/h/a/c/a1/c0/a$b;

    iget v5, v1, Lf/h/a/c/a1/c0/d;->n:I

    iget-object v6, v1, Lf/h/a/c/a1/c0/d;->q:Lf/h/a/c/i1/r;

    invoke-direct {v2, v5, v6}, Lf/h/a/c/a1/c0/a$b;-><init>(ILf/h/a/c/i1/r;)V

    iget-wide v5, v0, Lf/h/a/c/a1/e;->d:J

    iget-object v7, v1, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v7}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_24

    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/a1/c0/a$a;

    iget-object v3, v3, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v1

    goto/16 :goto_1a

    :cond_24
    iget v0, v2, Lf/h/a/c/a1/c0/a;->a:I

    if-ne v0, v4, :cond_28

    iget-object v0, v2, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v2

    shr-int/lit8 v2, v2, 0x18

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v3

    if-nez v2, :cond_25

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v7

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v9

    goto :goto_12

    :cond_25
    invoke-virtual {v0}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v7

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v9

    :goto_12
    move-wide v13, v7

    add-long/2addr v5, v9

    const-wide/32 v9, 0xf4240

    move-wide v7, v13

    move-wide v11, v3

    invoke-static/range {v7 .. v12}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v15

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->v()I

    move-result v2

    new-array v11, v2, [I

    new-array v12, v2, [J

    new-array v9, v2, [J

    new-array v10, v2, [J

    const/4 v7, 0x0

    move-wide v7, v13

    move-wide/from16 v17, v15

    const/4 v13, 0x0

    :goto_13
    if-ge v13, v2, :cond_27

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v14

    const/high16 v19, -0x80000000

    and-int v19, v14, v19

    if-nez v19, :cond_26

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v19

    const v21, 0x7fffffff

    and-int v14, v14, v21

    aput v14, v11, v13

    aput-wide v5, v12, v13

    aput-wide v17, v10, v13

    add-long v17, v7, v19

    const-wide/32 v19, 0xf4240

    move-wide/from16 v7, v17

    move/from16 p2, v2

    move-object v14, v9

    move-object v2, v10

    move-wide/from16 v9, v19

    move-object v1, v11

    move-object/from16 v22, v12

    move-wide v11, v3

    invoke-static/range {v7 .. v12}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v7

    aget-wide v9, v2, v13

    sub-long v9, v7, v9

    aput-wide v9, v14, v13

    const/4 v9, 0x4

    invoke-virtual {v0, v9}, Lf/h/a/c/i1/r;->D(I)V

    aget v9, v1, v13

    int-to-long v9, v9

    add-long/2addr v5, v9

    add-int/lit8 v13, v13, 0x1

    move-object v11, v1

    move-object v10, v2

    move-object v9, v14

    move-object/from16 v12, v22

    move-object/from16 v1, p0

    move/from16 v2, p2

    move-wide/from16 v26, v7

    move-wide/from16 v7, v17

    move-wide/from16 v17, v26

    goto :goto_13

    :cond_26
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Unhandled indirect reference"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_27
    move-object v14, v9

    move-object v2, v10

    move-object v1, v11

    move-object/from16 v22, v12

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v3, Lf/h/a/c/a1/c;

    move-object/from16 v4, v22

    invoke-direct {v3, v1, v4, v14, v2}, Lf/h/a/c/a1/c;-><init>([I[J[J[J)V

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    move-object/from16 v5, p0

    iput-wide v1, v5, Lf/h/a/c/a1/c0/d;->v:J

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->B:Lf/h/a/c/a1/i;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lf/h/a/c/a1/q;

    invoke-interface {v1, v0}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    const/4 v0, 0x1

    iput-boolean v0, v5, Lf/h/a/c/a1/c0/d;->E:Z

    goto/16 :goto_19

    :cond_28
    move-object v5, v1

    if-ne v0, v3, :cond_30

    iget-object v0, v2, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    if-eqz v1, :cond_30

    array-length v1, v1

    if-nez v1, :cond_29

    goto/16 :goto_19

    :cond_29
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v1

    shr-int/lit8 v1, v1, 0x18

    and-int/lit16 v1, v1, 0xff

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v1, :cond_2b

    const/4 v4, 0x1

    if-eq v1, v4, :cond_2a

    const-string v0, "Skipping unsupported emsg version: "

    invoke-static {v0, v1, v8}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_19

    :cond_2a
    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v6

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v9

    const-wide/32 v11, 0xf4240

    move-wide v13, v6

    invoke-static/range {v9 .. v14}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v15

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    invoke-static/range {v9 .. v14}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v6

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v8

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v19, v1

    move-object/from16 v20, v4

    move-wide/from16 v21, v6

    move-wide/from16 v23, v8

    move-wide/from16 v16, v15

    move-wide v14, v2

    goto :goto_15

    :cond_2b
    invoke-virtual {v0}, Lf/h/a/c/i1/r;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v12

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    move-wide v10, v12

    invoke-static/range {v6 .. v11}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v14

    iget-wide v6, v5, Lf/h/a/c/a1/c0/d;->v:J

    cmp-long v8, v6, v2

    if-eqz v8, :cond_2c

    add-long/2addr v6, v14

    move-wide/from16 v16, v6

    goto :goto_14

    :cond_2c
    move-wide/from16 v16, v2

    :goto_14
    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    move-wide v10, v12

    invoke-static/range {v6 .. v11}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v6

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v8

    move-object/from16 v19, v1

    move-object/from16 v20, v4

    move-wide/from16 v21, v6

    move-wide/from16 v23, v8

    :goto_15
    invoke-virtual {v0}, Lf/h/a/c/i1/r;->a()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->a()I

    move-result v4

    iget-object v6, v0, Lf/h/a/c/i1/r;->a:[B

    iget v7, v0, Lf/h/a/c/i1/r;->b:I

    const/4 v8, 0x0

    invoke-static {v6, v7, v1, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v6, v0, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v6, v4

    iput v6, v0, Lf/h/a/c/i1/r;->b:I

    new-instance v0, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;

    move-object/from16 v18, v0

    move-object/from16 v25, v1

    invoke-direct/range {v18 .. v25}, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;-><init>(Ljava/lang/String;Ljava/lang/String;JJ[B)V

    new-instance v1, Lf/h/a/c/i1/r;

    iget-object v4, v5, Lf/h/a/c/a1/c0/d;->i:Lf/h/a/c/c1/g/b;

    iget-object v6, v4, Lf/h/a/c/c1/g/b;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    :try_start_0
    iget-object v6, v4, Lf/h/a/c/c1/g/b;->b:Ljava/io/DataOutputStream;

    iget-object v7, v0, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;->d:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    iget-object v6, v0, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;->e:Ljava/lang/String;

    if-eqz v6, :cond_2d

    goto :goto_16

    :cond_2d
    const-string v6, ""

    :goto_16
    iget-object v7, v4, Lf/h/a/c/c1/g/b;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v7, v6}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {v7, v6}, Ljava/io/DataOutputStream;->writeByte(I)V

    iget-object v6, v4, Lf/h/a/c/c1/g/b;->b:Ljava/io/DataOutputStream;

    iget-wide v7, v0, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;->f:J

    invoke-static {v6, v7, v8}, Lf/h/a/c/c1/g/b;->a(Ljava/io/DataOutputStream;J)V

    iget-object v6, v4, Lf/h/a/c/c1/g/b;->b:Ljava/io/DataOutputStream;

    iget-wide v7, v0, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;->g:J

    invoke-static {v6, v7, v8}, Lf/h/a/c/c1/g/b;->a(Ljava/io/DataOutputStream;J)V

    iget-object v6, v4, Lf/h/a/c/c1/g/b;->b:Ljava/io/DataOutputStream;

    iget-object v0, v0, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;->h:[B

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->write([B)V

    iget-object v0, v4, Lf/h/a/c/c1/g/b;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    iget-object v0, v4, Lf/h/a/c/c1/g/b;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {v1, v0}, Lf/h/a/c/i1/r;-><init>([B)V

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v0

    iget-object v4, v5, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    array-length v6, v4

    const/4 v7, 0x0

    :goto_17
    if-ge v7, v6, :cond_2e

    aget-object v8, v4, v7

    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Lf/h/a/c/i1/r;->C(I)V

    invoke-interface {v8, v1, v0}, Lf/h/a/c/a1/s;->b(Lf/h/a/c/i1/r;I)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_17

    :cond_2e
    cmp-long v1, v16, v2

    if-nez v1, :cond_2f

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->l:Ljava/util/ArrayDeque;

    new-instance v2, Lf/h/a/c/a1/c0/d$a;

    invoke-direct {v2, v14, v15, v0}, Lf/h/a/c/a1/c0/d$a;-><init>(JI)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    iget v1, v5, Lf/h/a/c/a1/c0/d;->s:I

    add-int/2addr v1, v0

    iput v1, v5, Lf/h/a/c/a1/c0/d;->s:I

    goto :goto_19

    :cond_2f
    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_18
    if-ge v3, v2, :cond_30

    aget-object v6, v1, v3

    const/4 v9, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v7, v16

    move v10, v0

    invoke-interface/range {v6 .. v12}, Lf/h/a/c/a1/s;->c(JIIILf/h/a/c/a1/s$a;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_30
    :goto_19
    move-object/from16 v0, p1

    goto :goto_1a

    :cond_31
    move-object v5, v1

    invoke-virtual {v0, v2}, Lf/h/a/c/a1/e;->i(I)V

    :goto_1a
    iget-wide v1, v0, Lf/h/a/c/a1/e;->d:J

    invoke-virtual {v5, v1, v2}, Lf/h/a/c/a1/c0/d;->j(J)V

    goto/16 :goto_23

    :cond_32
    move-object v5, v1

    iget v1, v5, Lf/h/a/c/a1/c0/d;->p:I

    if-nez v1, :cond_34

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    const/16 v2, 0x8

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v0, v1, v6, v2, v7}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    move-result v1

    if-nez v1, :cond_33

    const/4 v1, 0x0

    goto/16 :goto_22

    :cond_33
    iput v2, v5, Lf/h/a/c/a1/c0/d;->p:I

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v1, v6}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v1

    iput-wide v1, v5, Lf/h/a/c/a1/c0/d;->o:J

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->e()I

    move-result v1

    iput v1, v5, Lf/h/a/c/a1/c0/d;->n:I

    :cond_34
    iget-wide v1, v5, Lf/h/a/c/a1/c0/d;->o:J

    const-wide/16 v6, 0x1

    cmp-long v8, v1, v6

    if-nez v8, :cond_35

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    const/16 v2, 0x8

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v2, v2, v6}, Lf/h/a/c/a1/e;->h([BIIZ)Z

    iget v1, v5, Lf/h/a/c/a1/c0/d;->p:I

    add-int/2addr v1, v2

    iput v1, v5, Lf/h/a/c/a1/c0/d;->p:I

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v1

    iput-wide v1, v5, Lf/h/a/c/a1/c0/d;->o:J

    goto :goto_1b

    :cond_35
    const-wide/16 v6, 0x0

    cmp-long v8, v1, v6

    if-nez v8, :cond_37

    iget-wide v1, v0, Lf/h/a/c/a1/e;->c:J

    const-wide/16 v6, -0x1

    cmp-long v8, v1, v6

    if-nez v8, :cond_36

    iget-object v8, v5, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v8}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_36

    iget-object v1, v5, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/a1/c0/a$a;

    iget-wide v1, v1, Lf/h/a/c/a1/c0/a$a;->b:J

    :cond_36
    cmp-long v8, v1, v6

    if-eqz v8, :cond_37

    iget-wide v6, v0, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v1, v6

    iget v6, v5, Lf/h/a/c/a1/c0/d;->p:I

    int-to-long v6, v6

    add-long/2addr v1, v6

    iput-wide v1, v5, Lf/h/a/c/a1/c0/d;->o:J

    :cond_37
    :goto_1b
    iget-wide v1, v5, Lf/h/a/c/a1/c0/d;->o:J

    iget v6, v5, Lf/h/a/c/a1/c0/d;->p:I

    int-to-long v6, v6

    cmp-long v8, v1, v6

    if-ltz v8, :cond_46

    iget-wide v1, v0, Lf/h/a/c/a1/e;->d:J

    sub-long/2addr v1, v6

    iget v6, v5, Lf/h/a/c/a1/c0/d;->n:I

    const v7, 0x6d6f6f66

    if-ne v6, v7, :cond_38

    iget-object v6, v5, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    const/4 v8, 0x0

    :goto_1c
    if-ge v8, v6, :cond_38

    iget-object v9, v5, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v9, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/a/c/a1/c0/d$b;

    iget-object v9, v9, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-wide v1, v9, Lf/h/a/c/a1/c0/k;->c:J

    iput-wide v1, v9, Lf/h/a/c/a1/c0/k;->b:J

    add-int/lit8 v8, v8, 0x1

    goto :goto_1c

    :cond_38
    iget v6, v5, Lf/h/a/c/a1/c0/d;->n:I

    const v8, 0x6d646174

    if-ne v6, v8, :cond_3a

    const/4 v3, 0x0

    iput-object v3, v5, Lf/h/a/c/a1/c0/d;->w:Lf/h/a/c/a1/c0/d$b;

    iget-wide v3, v5, Lf/h/a/c/a1/c0/d;->o:J

    add-long/2addr v3, v1

    iput-wide v3, v5, Lf/h/a/c/a1/c0/d;->r:J

    iget-boolean v3, v5, Lf/h/a/c/a1/c0/d;->E:Z

    if-nez v3, :cond_39

    iget-object v3, v5, Lf/h/a/c/a1/c0/d;->B:Lf/h/a/c/a1/i;

    new-instance v4, Lf/h/a/c/a1/q$b;

    iget-wide v6, v5, Lf/h/a/c/a1/c0/d;->u:J

    invoke-direct {v4, v6, v7, v1, v2}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    invoke-interface {v3, v4}, Lf/h/a/c/a1/i;->a(Lf/h/a/c/a1/q;)V

    const/4 v1, 0x1

    iput-boolean v1, v5, Lf/h/a/c/a1/c0/d;->E:Z

    :cond_39
    const/4 v1, 0x2

    iput v1, v5, Lf/h/a/c/a1/c0/d;->m:I

    goto/16 :goto_21

    :cond_3a
    const v1, 0x6d6f6f76

    if-eq v6, v1, :cond_3c

    const v1, 0x7472616b

    if-eq v6, v1, :cond_3c

    const v1, 0x6d646961

    if-eq v6, v1, :cond_3c

    const v1, 0x6d696e66

    if-eq v6, v1, :cond_3c

    const v1, 0x7374626c

    if-eq v6, v1, :cond_3c

    if-eq v6, v7, :cond_3c

    const v1, 0x74726166

    if-eq v6, v1, :cond_3c

    const v1, 0x6d766578

    if-eq v6, v1, :cond_3c

    const v1, 0x65647473

    if-ne v6, v1, :cond_3b

    goto :goto_1d

    :cond_3b
    const/4 v1, 0x0

    goto :goto_1e

    :cond_3c
    :goto_1d
    const/4 v1, 0x1

    :goto_1e
    if-eqz v1, :cond_3e

    iget-wide v1, v0, Lf/h/a/c/a1/e;->d:J

    iget-wide v3, v5, Lf/h/a/c/a1/c0/d;->o:J

    add-long/2addr v1, v3

    const-wide/16 v3, 0x8

    sub-long/2addr v1, v3

    iget-object v3, v5, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    new-instance v4, Lf/h/a/c/a1/c0/a$a;

    invoke-direct {v4, v6, v1, v2}, Lf/h/a/c/a1/c0/a$a;-><init>(IJ)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    iget-wide v3, v5, Lf/h/a/c/a1/c0/d;->o:J

    iget v6, v5, Lf/h/a/c/a1/c0/d;->p:I

    int-to-long v6, v6

    cmp-long v8, v3, v6

    if-nez v8, :cond_3d

    invoke-virtual {v5, v1, v2}, Lf/h/a/c/a1/c0/d;->j(J)V

    goto/16 :goto_21

    :cond_3d
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/c0/d;->a()V

    goto/16 :goto_21

    :cond_3e
    const v1, 0x68646c72    # 4.3148E24f

    if-eq v6, v1, :cond_40

    const v1, 0x6d646864

    if-eq v6, v1, :cond_40

    const v1, 0x6d766864

    if-eq v6, v1, :cond_40

    if-eq v6, v4, :cond_40

    const v1, 0x73747364

    if-eq v6, v1, :cond_40

    const v1, 0x74666474

    if-eq v6, v1, :cond_40

    const v1, 0x74666864

    if-eq v6, v1, :cond_40

    const v1, 0x746b6864

    if-eq v6, v1, :cond_40

    const v1, 0x74726578

    if-eq v6, v1, :cond_40

    const v1, 0x7472756e

    if-eq v6, v1, :cond_40

    const v1, 0x70737368    # 3.013775E29f

    if-eq v6, v1, :cond_40

    const v1, 0x7361697a

    if-eq v6, v1, :cond_40

    const v1, 0x7361696f

    if-eq v6, v1, :cond_40

    const v1, 0x73656e63

    if-eq v6, v1, :cond_40

    const v1, 0x75756964

    if-eq v6, v1, :cond_40

    const v1, 0x73626770

    if-eq v6, v1, :cond_40

    const v1, 0x73677064

    if-eq v6, v1, :cond_40

    const v1, 0x656c7374

    if-eq v6, v1, :cond_40

    const v1, 0x6d656864

    if-eq v6, v1, :cond_40

    if-ne v6, v3, :cond_3f

    goto :goto_1f

    :cond_3f
    const/4 v1, 0x0

    goto :goto_20

    :cond_40
    :goto_1f
    const/4 v1, 0x1

    :goto_20
    const-wide/32 v2, 0x7fffffff

    if-eqz v1, :cond_43

    iget v1, v5, Lf/h/a/c/a1/c0/d;->p:I

    const/16 v4, 0x8

    if-ne v1, v4, :cond_42

    iget-wide v6, v5, Lf/h/a/c/a1/c0/d;->o:J

    cmp-long v1, v6, v2

    if-gtz v1, :cond_41

    new-instance v1, Lf/h/a/c/i1/r;

    long-to-int v2, v6

    invoke-direct {v1, v2}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v1, v5, Lf/h/a/c/a1/c0/d;->q:Lf/h/a/c/i1/r;

    iget-object v2, v5, Lf/h/a/c/a1/c0/d;->j:Lf/h/a/c/i1/r;

    iget-object v2, v2, Lf/h/a/c/i1/r;->a:[B

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    const/4 v3, 0x0

    invoke-static {v2, v3, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v1, 0x1

    iput v1, v5, Lf/h/a/c/a1/c0/d;->m:I

    goto :goto_21

    :cond_41
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Leaf atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_42
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Leaf atom defines extended atom size (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_43
    iget-wide v6, v5, Lf/h/a/c/a1/c0/d;->o:J

    cmp-long v1, v6, v2

    if-gtz v1, :cond_45

    const/4 v1, 0x0

    iput-object v1, v5, Lf/h/a/c/a1/c0/d;->q:Lf/h/a/c/i1/r;

    const/4 v1, 0x1

    iput v1, v5, Lf/h/a/c/a1/c0/d;->m:I

    :goto_21
    const/4 v1, 0x1

    :goto_22
    if-nez v1, :cond_44

    const/4 v0, -0x1

    return v0

    :cond_44
    :goto_23
    move-object v1, v5

    goto/16 :goto_0

    :cond_45
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Skipping atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_46
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Atom size less than header length (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e(Lf/h/a/c/a1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/a1/c0/d;->B:Lf/h/a/c/a1/i;

    return-void
.end method

.method public f(JJ)V
    .locals 2

    iget-object p1, p0, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result p1

    const/4 p2, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    iget-object v1, p0, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/a1/c0/d$b;

    invoke-virtual {v1}, Lf/h/a/c/a1/c0/d$b;->e()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/h/a/c/a1/c0/d;->l:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->clear()V

    iput p2, p0, Lf/h/a/c/a1/c0/d;->s:I

    iput-wide p3, p0, Lf/h/a/c/a1/c0/d;->t:J

    iget-object p1, p0, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->clear()V

    invoke-virtual {p0}, Lf/h/a/c/a1/c0/d;->a()V

    return-void
.end method

.method public final g()V
    .locals 7

    iget-object v0, p0, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Lf/h/a/c/a1/s;

    iput-object v0, p0, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    iget v3, p0, Lf/h/a/c/a1/c0/d;->a:I

    const/4 v4, 0x4

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    iget-object v3, p0, Lf/h/a/c/a1/c0/d;->B:Lf/h/a/c/a1/i;

    iget-object v5, p0, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    invoke-interface {v3, v5, v4}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    invoke-static {v3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/a/c/a1/s;

    iput-object v0, p0, Lf/h/a/c/a1/c0/d;->C:[Lf/h/a/c/a1/s;

    array-length v3, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_1

    aget-object v5, v0, v4

    sget-object v6, Lf/h/a/c/a1/c0/d;->G:Lcom/google/android/exoplayer2/Format;

    invoke-interface {v5, v6}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lf/h/a/c/a1/c0/d;->D:[Lf/h/a/c/a1/s;

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/a1/c0/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lf/h/a/c/a1/s;

    iput-object v0, p0, Lf/h/a/c/a1/c0/d;->D:[Lf/h/a/c/a1/s;

    :goto_2
    iget-object v0, p0, Lf/h/a/c/a1/c0/d;->D:[Lf/h/a/c/a1/s;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/a1/c0/d;->B:Lf/h/a/c/a1/i;

    iget-object v3, p0, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    add-int/2addr v3, v2

    add-int/2addr v3, v1

    const/4 v4, 0x3

    invoke-interface {v0, v3, v4}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v0

    iget-object v3, p0, Lf/h/a/c/a1/c0/d;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/Format;

    invoke-interface {v0, v3}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    iget-object v3, p0, Lf/h/a/c/a1/c0/d;->D:[Lf/h/a/c/a1/s;

    aput-object v0, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method public h(Lf/h/a/c/a1/e;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lf/h/a/c/a1/c0/h;->a(Lf/h/a/c/a1/e;Z)Z

    move-result p1

    return p1
.end method

.method public final j(J)V
    .locals 49
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object v1, v0

    :goto_0
    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_57

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/a1/c0/a$a;

    iget-wide v2, v2, Lf/h/a/c/a1/c0/a$a;->b:J

    cmp-long v4, v2, p1

    if-nez v4, :cond_57

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/a1/c0/a$a;

    iget v3, v2, Lf/h/a/c/a1/c0/a;->a:I

    const v4, 0x6d6f6f76

    const/16 v5, 0xc

    const/16 v6, 0x8

    const/4 v7, 0x1

    if-ne v3, v4, :cond_b

    const-string v3, "Unexpected moov box."

    invoke-static {v7, v3}, Lf/g/j/k/a;->t(ZLjava/lang/Object;)V

    iget-object v3, v2, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-static {v3}, Lf/h/a/c/a1/c0/d;->c(Ljava/util/List;)Lcom/google/android/exoplayer2/drm/DrmInitData;

    move-result-object v3

    const v4, 0x6d766578

    invoke-virtual {v2, v4}, Lf/h/a/c/a1/c0/a$a;->b(I)Lf/h/a/c/a1/c0/a$a;

    move-result-object v4

    new-instance v14, Landroid/util/SparseArray;

    invoke-direct {v14}, Landroid/util/SparseArray;-><init>()V

    iget-object v7, v4, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v10, 0x0

    move-wide v15, v8

    :goto_1
    if-ge v10, v7, :cond_3

    iget-object v8, v4, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/h/a/c/a1/c0/a$b;

    iget v9, v8, Lf/h/a/c/a1/c0/a;->a:I

    const v11, 0x74726578

    if-ne v9, v11, :cond_0

    iget-object v8, v8, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v8, v5}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->e()I

    move-result v5

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->t()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->t()I

    move-result v11

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->t()I

    move-result v12

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v13, Lf/h/a/c/a1/c0/c;

    invoke-direct {v13, v9, v11, v12, v8}, Lf/h/a/c/a1/c0/c;-><init>(IIII)V

    invoke-static {v5, v13}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    iget-object v8, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v14, v8, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_3

    :cond_0
    const v5, 0x6d656864

    if-ne v9, v5, :cond_2

    iget-object v5, v8, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->e()I

    move-result v8

    shr-int/lit8 v8, v8, 0x18

    and-int/lit16 v8, v8, 0xff

    if-nez v8, :cond_1

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v8

    goto :goto_2

    :cond_1
    invoke-virtual {v5}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v8

    :goto_2
    move-wide v15, v8

    :cond_2
    :goto_3
    add-int/lit8 v10, v10, 0x1

    const/16 v5, 0xc

    goto :goto_1

    :cond_3
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iget-object v5, v2, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v5, :cond_6

    iget-object v7, v2, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/a/c/a1/c0/a$a;

    iget v8, v7, Lf/h/a/c/a1/c0/a;->a:I

    const v9, 0x7472616b

    if-ne v8, v9, :cond_5

    const v8, 0x6d766864

    invoke-virtual {v2, v8}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v8

    iget v9, v1, Lf/h/a/c/a1/c0/d;->a:I

    and-int/lit8 v9, v9, 0x10

    if-eqz v9, :cond_4

    const/4 v9, 0x1

    const/4 v12, 0x1

    goto :goto_5

    :cond_4
    const/4 v9, 0x0

    const/4 v12, 0x0

    :goto_5
    const/4 v13, 0x0

    move-wide v9, v15

    move-object v11, v3

    invoke-static/range {v7 .. v13}, Lf/h/a/c/a1/c0/b;->d(Lf/h/a/c/a1/c0/a$a;Lf/h/a/c/a1/c0/a$b;JLcom/google/android/exoplayer2/drm/DrmInitData;ZZ)Lf/h/a/c/a1/c0/i;

    move-result-object v7

    if-eqz v7, :cond_5

    iget v8, v7, Lf/h/a/c/a1/c0/i;->a:I

    invoke-virtual {v4, v8, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v2

    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v2, :cond_7

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/a1/c0/i;

    new-instance v6, Lf/h/a/c/a1/c0/d$b;

    iget-object v7, v1, Lf/h/a/c/a1/c0/d;->B:Lf/h/a/c/a1/i;

    iget v8, v5, Lf/h/a/c/a1/c0/i;->b:I

    invoke-interface {v7, v3, v8}, Lf/h/a/c/a1/i;->p(II)Lf/h/a/c/a1/s;

    move-result-object v7

    invoke-direct {v6, v7}, Lf/h/a/c/a1/c0/d$b;-><init>(Lf/h/a/c/a1/s;)V

    iget v7, v5, Lf/h/a/c/a1/c0/i;->a:I

    invoke-virtual {v1, v14, v7}, Lf/h/a/c/a1/c0/d;->b(Landroid/util/SparseArray;I)Lf/h/a/c/a1/c0/c;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Lf/h/a/c/a1/c0/d$b;->b(Lf/h/a/c/a1/c0/i;Lf/h/a/c/a1/c0/c;)V

    iget-object v7, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    iget v8, v5, Lf/h/a/c/a1/c0/i;->a:I

    invoke-virtual {v7, v8, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-wide v6, v1, Lf/h/a/c/a1/c0/d;->u:J

    iget-wide v8, v5, Lf/h/a/c/a1/c0/i;->e:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    iput-wide v5, v1, Lf/h/a/c/a1/c0/d;->u:J

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/c0/d;->g()V

    iget-object v2, v1, Lf/h/a/c/a1/c0/d;->B:Lf/h/a/c/a1/i;

    invoke-interface {v2}, Lf/h/a/c/a1/i;->k()V

    goto :goto_9

    :cond_8
    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ne v3, v2, :cond_9

    const/4 v3, 0x1

    goto :goto_7

    :cond_9
    const/4 v3, 0x0

    :goto_7
    invoke-static {v3}, Lf/g/j/k/a;->s(Z)V

    const/4 v3, 0x0

    :goto_8
    if-ge v3, v2, :cond_a

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/a1/c0/i;

    iget-object v6, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    iget v7, v5, Lf/h/a/c/a1/c0/i;->a:I

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/a/c/a1/c0/d$b;

    iget v7, v5, Lf/h/a/c/a1/c0/i;->a:I

    invoke-virtual {v1, v14, v7}, Lf/h/a/c/a1/c0/d;->b(Landroid/util/SparseArray;I)Lf/h/a/c/a1/c0/c;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Lf/h/a/c/a1/c0/d$b;->b(Lf/h/a/c/a1/c0/i;Lf/h/a/c/a1/c0/c;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_a
    :goto_9
    move-object v3, v0

    goto/16 :goto_38

    :cond_b
    const v4, 0x6d6f6f66

    if-ne v3, v4, :cond_55

    iget-object v3, v1, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    iget v4, v1, Lf/h/a/c/a1/c0/d;->a:I

    iget-object v1, v1, Lf/h/a/c/a1/c0/d;->g:[B

    iget-object v5, v2, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v7, 0x0

    :goto_a
    if-ge v7, v5, :cond_4e

    iget-object v8, v2, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/h/a/c/a1/c0/a$a;

    iget v9, v8, Lf/h/a/c/a1/c0/a;->a:I

    const v10, 0x74726166

    if-ne v9, v10, :cond_4d

    const v9, 0x74666864

    invoke-virtual {v8, v9}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v9

    iget-object v9, v9, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v9, v6}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->e()I

    move-result v10

    const v11, 0xffffff

    and-int/2addr v10, v11

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->e()I

    move-result v11

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_c

    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lf/h/a/c/a1/c0/d$b;

    goto :goto_b

    :cond_c
    invoke-virtual {v3, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lf/h/a/c/a1/c0/d$b;

    :goto_b
    if-nez v11, :cond_d

    const/4 v11, 0x0

    goto :goto_10

    :cond_d
    and-int/lit8 v12, v10, 0x1

    if-eqz v12, :cond_e

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v12

    iget-object v14, v11, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iput-wide v12, v14, Lf/h/a/c/a1/c0/k;->b:J

    iput-wide v12, v14, Lf/h/a/c/a1/c0/k;->c:J

    :cond_e
    iget-object v12, v11, Lf/h/a/c/a1/c0/d$b;->e:Lf/h/a/c/a1/c0/c;

    and-int/lit8 v13, v10, 0x2

    if-eqz v13, :cond_f

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->t()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    goto :goto_c

    :cond_f
    iget v13, v12, Lf/h/a/c/a1/c0/c;->a:I

    :goto_c
    and-int/lit8 v14, v10, 0x8

    if-eqz v14, :cond_10

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->t()I

    move-result v14

    goto :goto_d

    :cond_10
    iget v14, v12, Lf/h/a/c/a1/c0/c;->b:I

    :goto_d
    and-int/lit8 v15, v10, 0x10

    if-eqz v15, :cond_11

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->t()I

    move-result v15

    goto :goto_e

    :cond_11
    iget v15, v12, Lf/h/a/c/a1/c0/c;->c:I

    :goto_e
    and-int/lit8 v10, v10, 0x20

    if-eqz v10, :cond_12

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->t()I

    move-result v9

    goto :goto_f

    :cond_12
    iget v9, v12, Lf/h/a/c/a1/c0/c;->d:I

    :goto_f
    iget-object v10, v11, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    new-instance v12, Lf/h/a/c/a1/c0/c;

    invoke-direct {v12, v13, v14, v15, v9}, Lf/h/a/c/a1/c0/c;-><init>(IIII)V

    iput-object v12, v10, Lf/h/a/c/a1/c0/k;->a:Lf/h/a/c/a1/c0/c;

    :goto_10
    if-nez v11, :cond_13

    goto/16 :goto_32

    :cond_13
    iget-object v9, v11, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-wide v12, v9, Lf/h/a/c/a1/c0/k;->r:J

    invoke-virtual {v11}, Lf/h/a/c/a1/c0/d$b;->e()V

    const v10, 0x74666474

    invoke-virtual {v8, v10}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v14

    if-eqz v14, :cond_15

    and-int/lit8 v14, v4, 0x2

    if-nez v14, :cond_15

    invoke-virtual {v8, v10}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v10

    iget-object v10, v10, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    invoke-virtual {v10, v6}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v10}, Lf/h/a/c/i1/r;->e()I

    move-result v6

    shr-int/lit8 v6, v6, 0x18

    and-int/lit16 v6, v6, 0xff

    const/4 v12, 0x1

    if-ne v6, v12, :cond_14

    invoke-virtual {v10}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v12

    goto :goto_11

    :cond_14
    invoke-virtual {v10}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v12

    :cond_15
    :goto_11
    iget-object v6, v8, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v16, v3

    move/from16 v17, v5

    const/4 v3, 0x0

    :goto_12
    const v5, 0x7472756e

    if-ge v15, v10, :cond_17

    invoke-interface {v6, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-wide/from16 v19, v12

    move-object/from16 v12, v18

    check-cast v12, Lf/h/a/c/a1/c0/a$b;

    iget v13, v12, Lf/h/a/c/a1/c0/a;->a:I

    if-ne v13, v5, :cond_16

    iget-object v5, v12, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/16 v12, 0xc

    invoke-virtual {v5, v12}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->t()I

    move-result v5

    if-lez v5, :cond_16

    add-int/2addr v14, v5

    add-int/lit8 v3, v3, 0x1

    :cond_16
    add-int/lit8 v15, v15, 0x1

    move-wide/from16 v12, v19

    goto :goto_12

    :cond_17
    move-wide/from16 v19, v12

    const/4 v12, 0x0

    iput v12, v11, Lf/h/a/c/a1/c0/d$b;->h:I

    iput v12, v11, Lf/h/a/c/a1/c0/d$b;->g:I

    iput v12, v11, Lf/h/a/c/a1/c0/d$b;->f:I

    iget-object v12, v11, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iput v3, v12, Lf/h/a/c/a1/c0/k;->d:I

    iput v14, v12, Lf/h/a/c/a1/c0/k;->e:I

    iget-object v13, v12, Lf/h/a/c/a1/c0/k;->g:[I

    if-eqz v13, :cond_18

    array-length v13, v13

    if-ge v13, v3, :cond_19

    :cond_18
    new-array v13, v3, [J

    iput-object v13, v12, Lf/h/a/c/a1/c0/k;->f:[J

    new-array v3, v3, [I

    iput-object v3, v12, Lf/h/a/c/a1/c0/k;->g:[I

    :cond_19
    iget-object v3, v12, Lf/h/a/c/a1/c0/k;->h:[I

    if-eqz v3, :cond_1a

    array-length v3, v3

    if-ge v3, v14, :cond_1b

    :cond_1a
    mul-int/lit8 v14, v14, 0x7d

    div-int/lit8 v14, v14, 0x64

    new-array v3, v14, [I

    iput-object v3, v12, Lf/h/a/c/a1/c0/k;->h:[I

    new-array v3, v14, [I

    iput-object v3, v12, Lf/h/a/c/a1/c0/k;->i:[I

    new-array v3, v14, [J

    iput-object v3, v12, Lf/h/a/c/a1/c0/k;->j:[J

    new-array v3, v14, [Z

    iput-object v3, v12, Lf/h/a/c/a1/c0/k;->k:[Z

    new-array v3, v14, [Z

    iput-object v3, v12, Lf/h/a/c/a1/c0/k;->m:[Z

    :cond_1b
    const/4 v3, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_13
    if-ge v3, v10, :cond_30

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v14, v18

    check-cast v14, Lf/h/a/c/a1/c0/a$b;

    iget v15, v14, Lf/h/a/c/a1/c0/a;->a:I

    if-ne v15, v5, :cond_2f

    add-int/lit8 v5, v12, 0x1

    iget-object v14, v14, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->e()I

    move-result v15

    const v18, 0xffffff

    and-int v15, v15, v18

    move/from16 v18, v5

    iget-object v5, v11, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    move-object/from16 v23, v6

    iget-object v6, v11, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    move/from16 v24, v10

    iget-object v10, v6, Lf/h/a/c/a1/c0/k;->a:Lf/h/a/c/a1/c0/c;

    iget-object v0, v6, Lf/h/a/c/a1/c0/k;->g:[I

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->t()I

    move-result v25

    aput v25, v0, v12

    iget-object v0, v6, Lf/h/a/c/a1/c0/k;->f:[J

    move-object/from16 v26, v1

    move-object/from16 v25, v2

    iget-wide v1, v6, Lf/h/a/c/a1/c0/k;->b:J

    aput-wide v1, v0, v12

    and-int/lit8 v1, v15, 0x1

    if-eqz v1, :cond_1c

    aget-wide v1, v0, v12

    move/from16 v27, v7

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->e()I

    move-result v7

    move-object/from16 v28, v8

    int-to-long v7, v7

    add-long/2addr v1, v7

    aput-wide v1, v0, v12

    goto :goto_14

    :cond_1c
    move/from16 v27, v7

    move-object/from16 v28, v8

    :goto_14
    and-int/lit8 v0, v15, 0x4

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    goto :goto_15

    :cond_1d
    const/4 v0, 0x0

    :goto_15
    iget v1, v10, Lf/h/a/c/a1/c0/c;->d:I

    if-eqz v0, :cond_1e

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->t()I

    move-result v1

    :cond_1e
    and-int/lit16 v2, v15, 0x100

    if-eqz v2, :cond_1f

    const/4 v2, 0x1

    goto :goto_16

    :cond_1f
    const/4 v2, 0x0

    :goto_16
    and-int/lit16 v7, v15, 0x200

    if-eqz v7, :cond_20

    const/4 v7, 0x1

    goto :goto_17

    :cond_20
    const/4 v7, 0x0

    :goto_17
    and-int/lit16 v8, v15, 0x400

    if-eqz v8, :cond_21

    const/4 v8, 0x1

    goto :goto_18

    :cond_21
    const/4 v8, 0x0

    :goto_18
    and-int/lit16 v15, v15, 0x800

    if-eqz v15, :cond_22

    const/4 v15, 0x1

    goto :goto_19

    :cond_22
    const/4 v15, 0x0

    :goto_19
    move/from16 v29, v1

    iget-object v1, v5, Lf/h/a/c/a1/c0/i;->h:[J

    if-eqz v1, :cond_24

    move-object/from16 v30, v9

    array-length v9, v1

    move-object/from16 v31, v11

    const/4 v11, 0x1

    if-ne v9, v11, :cond_23

    const/4 v9, 0x0

    aget-wide v32, v1, v9

    const-wide/16 v21, 0x0

    cmp-long v1, v32, v21

    if-nez v1, :cond_23

    iget-object v1, v5, Lf/h/a/c/a1/c0/i;->i:[J

    aget-wide v32, v1, v9

    const-wide/16 v34, 0x3e8

    move v1, v8

    iget-wide v8, v5, Lf/h/a/c/a1/c0/i;->c:J

    move-wide/from16 v36, v8

    invoke-static/range {v32 .. v37}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v8

    move-wide/from16 v21, v8

    goto :goto_1b

    :cond_23
    move v1, v8

    goto :goto_1a

    :cond_24
    move v1, v8

    move-object/from16 v30, v9

    move-object/from16 v31, v11

    :goto_1a
    const-wide/16 v21, 0x0

    :goto_1b
    iget-object v8, v6, Lf/h/a/c/a1/c0/k;->h:[I

    iget-object v9, v6, Lf/h/a/c/a1/c0/k;->i:[I

    iget-object v11, v6, Lf/h/a/c/a1/c0/k;->j:[J

    move/from16 v32, v3

    iget-object v3, v6, Lf/h/a/c/a1/c0/k;->k:[Z

    move-object/from16 v33, v3

    iget v3, v5, Lf/h/a/c/a1/c0/i;->b:I

    move-object/from16 v34, v8

    const/4 v8, 0x2

    if-ne v3, v8, :cond_25

    and-int/lit8 v3, v4, 0x1

    if-eqz v3, :cond_25

    const/4 v3, 0x1

    goto :goto_1c

    :cond_25
    const/4 v3, 0x0

    :goto_1c
    iget-object v8, v6, Lf/h/a/c/a1/c0/k;->g:[I

    aget v8, v8, v12

    add-int/2addr v8, v13

    move/from16 v41, v4

    iget-wide v4, v5, Lf/h/a/c/a1/c0/i;->c:J

    if-lez v12, :cond_26

    move/from16 v35, v13

    iget-wide v12, v6, Lf/h/a/c/a1/c0/k;->r:J

    move-object/from16 v42, v6

    goto :goto_1d

    :cond_26
    move/from16 v35, v13

    move-object/from16 v42, v6

    move-wide/from16 v12, v19

    :goto_1d
    move/from16 v6, v35

    :goto_1e
    if-ge v6, v8, :cond_2e

    if-eqz v2, :cond_27

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->t()I

    move-result v35

    move/from16 v43, v2

    move/from16 v2, v35

    goto :goto_1f

    :cond_27
    move/from16 v43, v2

    iget v2, v10, Lf/h/a/c/a1/c0/c;->b:I

    :goto_1f
    if-eqz v7, :cond_28

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->t()I

    move-result v35

    move/from16 v44, v7

    move/from16 v7, v35

    goto :goto_20

    :cond_28
    move/from16 v44, v7

    iget v7, v10, Lf/h/a/c/a1/c0/c;->c:I

    :goto_20
    if-nez v6, :cond_29

    if-eqz v0, :cond_29

    move/from16 v45, v0

    move/from16 v0, v29

    goto :goto_21

    :cond_29
    if-eqz v1, :cond_2a

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->e()I

    move-result v35

    move/from16 v45, v0

    move/from16 v0, v35

    goto :goto_21

    :cond_2a
    move/from16 v45, v0

    iget v0, v10, Lf/h/a/c/a1/c0/c;->d:I

    :goto_21
    if-eqz v15, :cond_2b

    move/from16 v46, v1

    invoke-virtual {v14}, Lf/h/a/c/i1/r;->e()I

    move-result v1

    move-object/from16 v47, v14

    move/from16 v48, v15

    int-to-long v14, v1

    const-wide/16 v35, 0x3e8

    mul-long v14, v14, v35

    div-long/2addr v14, v4

    long-to-int v1, v14

    aput v1, v9, v6

    goto :goto_22

    :cond_2b
    move/from16 v46, v1

    move-object/from16 v47, v14

    move/from16 v48, v15

    const/4 v1, 0x0

    aput v1, v9, v6

    :goto_22
    const-wide/16 v37, 0x3e8

    move-wide/from16 v35, v12

    move-wide/from16 v39, v4

    invoke-static/range {v35 .. v40}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide v14

    sub-long v14, v14, v21

    aput-wide v14, v11, v6

    aput v7, v34, v6

    shr-int/lit8 v0, v0, 0x10

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2d

    if-eqz v3, :cond_2c

    if-nez v6, :cond_2d

    :cond_2c
    const/4 v0, 0x1

    goto :goto_23

    :cond_2d
    const/4 v0, 0x0

    :goto_23
    aput-boolean v0, v33, v6

    int-to-long v0, v2

    add-long/2addr v12, v0

    add-int/lit8 v6, v6, 0x1

    move/from16 v2, v43

    move/from16 v7, v44

    move/from16 v0, v45

    move/from16 v1, v46

    move-object/from16 v14, v47

    move/from16 v15, v48

    goto/16 :goto_1e

    :cond_2e
    move-object/from16 v0, v42

    iput-wide v12, v0, Lf/h/a/c/a1/c0/k;->r:J

    move v13, v8

    move/from16 v12, v18

    goto :goto_24

    :cond_2f
    move-object/from16 v26, v1

    move-object/from16 v25, v2

    move/from16 v32, v3

    move/from16 v41, v4

    move-object/from16 v23, v6

    move/from16 v27, v7

    move-object/from16 v28, v8

    move-object/from16 v30, v9

    move/from16 v24, v10

    move-object/from16 v31, v11

    move/from16 v35, v13

    :goto_24
    add-int/lit8 v3, v32, 0x1

    const v5, 0x7472756e

    move-object/from16 v0, p0

    move-object/from16 v6, v23

    move/from16 v10, v24

    move-object/from16 v2, v25

    move-object/from16 v1, v26

    move/from16 v7, v27

    move-object/from16 v8, v28

    move-object/from16 v9, v30

    move-object/from16 v11, v31

    move/from16 v4, v41

    goto/16 :goto_13

    :cond_30
    move-object/from16 v26, v1

    move-object/from16 v25, v2

    move/from16 v41, v4

    move/from16 v27, v7

    move-object/from16 v28, v8

    move-object/from16 v30, v9

    iget-object v0, v11, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    move-object/from16 v1, v30

    iget-object v2, v1, Lf/h/a/c/a1/c0/k;->a:Lf/h/a/c/a1/c0/c;

    iget v2, v2, Lf/h/a/c/a1/c0/c;->a:I

    invoke-virtual {v0, v2}, Lf/h/a/c/a1/c0/i;->a(I)Lf/h/a/c/a1/c0/j;

    move-result-object v0

    const v2, 0x7361697a

    invoke-virtual {v8, v2}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v2

    if-eqz v2, :cond_37

    iget-object v2, v2, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    iget v3, v0, Lf/h/a/c/a1/c0/j;->d:I

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v5

    const v6, 0xffffff

    and-int/2addr v5, v6

    const/4 v6, 0x1

    and-int/2addr v5, v6

    if-ne v5, v6, :cond_31

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/r;->D(I)V

    :cond_31
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v4

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->t()I

    move-result v5

    iget v6, v1, Lf/h/a/c/a1/c0/k;->e:I

    if-ne v5, v6, :cond_36

    if-nez v4, :cond_33

    iget-object v4, v1, Lf/h/a/c/a1/c0/k;->m:[Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_25
    if-ge v6, v5, :cond_35

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v9

    add-int/2addr v7, v9

    if-le v9, v3, :cond_32

    const/4 v9, 0x1

    goto :goto_26

    :cond_32
    const/4 v9, 0x0

    :goto_26
    aput-boolean v9, v4, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_25

    :cond_33
    if-le v4, v3, :cond_34

    const/4 v2, 0x1

    goto :goto_27

    :cond_34
    const/4 v2, 0x0

    :goto_27
    mul-int v4, v4, v5

    const/4 v3, 0x0

    add-int/lit8 v7, v4, 0x0

    iget-object v4, v1, Lf/h/a/c/a1/c0/k;->m:[Z

    invoke-static {v4, v3, v5, v2}, Ljava/util/Arrays;->fill([ZIIZ)V

    :cond_35
    invoke-virtual {v1, v7}, Lf/h/a/c/a1/c0/k;->a(I)V

    goto :goto_28

    :cond_36
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v2, "Length mismatch: "

    const-string v3, ", "

    invoke-static {v2, v5, v3}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Lf/h/a/c/a1/c0/k;->e:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_37
    :goto_28
    const v2, 0x7361696f

    invoke-virtual {v8, v2}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v2

    if-eqz v2, :cond_3b

    iget-object v2, v2, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v4

    const v5, 0xffffff

    and-int/2addr v5, v4

    const/4 v6, 0x1

    and-int/2addr v5, v6

    if-ne v5, v6, :cond_38

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/r;->D(I)V

    :cond_38
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->t()I

    move-result v3

    if-ne v3, v6, :cond_3a

    shr-int/lit8 v3, v4, 0x18

    and-int/lit16 v3, v3, 0xff

    iget-wide v4, v1, Lf/h/a/c/a1/c0/k;->c:J

    if-nez v3, :cond_39

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v2

    goto :goto_29

    :cond_39
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->u()J

    move-result-wide v2

    :goto_29
    add-long/2addr v4, v2

    iput-wide v4, v1, Lf/h/a/c/a1/c0/k;->c:J

    goto :goto_2a

    :cond_3a
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Unexpected saio entry count: "

    invoke-static {v1, v3}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3b
    :goto_2a
    const v2, 0x73656e63

    invoke-virtual {v8, v2}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v2

    if-eqz v2, :cond_3c

    iget-object v2, v2, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/4 v3, 0x0

    invoke-static {v2, v3, v1}, Lf/h/a/c/a1/c0/d;->i(Lf/h/a/c/i1/r;ILf/h/a/c/a1/c0/k;)V

    :cond_3c
    const v2, 0x73626770

    invoke-virtual {v8, v2}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v2

    const v3, 0x73677064

    invoke-virtual {v8, v3}, Lf/h/a/c/a1/c0/a$a;->c(I)Lf/h/a/c/a1/c0/a$b;

    move-result-object v3

    if-eqz v2, :cond_49

    if-eqz v3, :cond_49

    iget-object v2, v2, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    iget-object v3, v3, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    if-eqz v0, :cond_3d

    iget-object v0, v0, Lf/h/a/c/a1/c0/j;->b:Ljava/lang/String;

    const/16 v4, 0x8

    goto :goto_2b

    :cond_3d
    const/16 v4, 0x8

    const/4 v0, 0x0

    :goto_2b
    move-object/from16 v30, v0

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v0

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v4

    const v5, 0x73656967

    if-eq v4, v5, :cond_3e

    goto/16 :goto_2f

    :cond_3e
    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    const/4 v4, 0x4

    const/4 v6, 0x1

    if-ne v0, v6, :cond_3f

    invoke-virtual {v2, v4}, Lf/h/a/c/i1/r;->D(I)V

    :cond_3f
    invoke-virtual {v2}, Lf/h/a/c/i1/r;->e()I

    move-result v0

    if-ne v0, v6, :cond_48

    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lf/h/a/c/i1/r;->C(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->e()I

    move-result v0

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->e()I

    move-result v2

    if-eq v2, v5, :cond_40

    goto/16 :goto_2f

    :cond_40
    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    if-ne v0, v6, :cond_42

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_41

    goto :goto_2c

    :cond_41
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Variable length description in sgpd found (unsupported)"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_42
    const/4 v2, 0x2

    if-lt v0, v2, :cond_43

    invoke-virtual {v3, v4}, Lf/h/a/c/i1/r;->D(I)V

    :cond_43
    :goto_2c
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_47

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    and-int/lit16 v4, v2, 0xf0

    shr-int/lit8 v33, v4, 0x4

    and-int/lit8 v34, v2, 0xf

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    if-ne v2, v0, :cond_44

    const/4 v0, 0x1

    const/16 v29, 0x1

    goto :goto_2d

    :cond_44
    const/4 v0, 0x0

    const/16 v29, 0x0

    :goto_2d
    if-nez v29, :cond_45

    goto :goto_2f

    :cond_45
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v31

    const/16 v0, 0x10

    new-array v2, v0, [B

    iget-object v4, v3, Lf/h/a/c/i1/r;->a:[B

    iget v5, v3, Lf/h/a/c/i1/r;->b:I

    const/4 v6, 0x0

    invoke-static {v4, v5, v2, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v4, v3, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v4, v0

    iput v4, v3, Lf/h/a/c/i1/r;->b:I

    if-nez v31, :cond_46

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v0

    new-array v4, v0, [B

    iget-object v5, v3, Lf/h/a/c/i1/r;->a:[B

    iget v7, v3, Lf/h/a/c/i1/r;->b:I

    invoke-static {v5, v7, v4, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v5, v3, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v5, v0

    iput v5, v3, Lf/h/a/c/i1/r;->b:I

    const/4 v0, 0x1

    move-object/from16 v35, v4

    goto :goto_2e

    :cond_46
    const/4 v0, 0x1

    const/4 v3, 0x0

    move-object/from16 v35, v3

    :goto_2e
    iput-boolean v0, v1, Lf/h/a/c/a1/c0/k;->l:Z

    new-instance v0, Lf/h/a/c/a1/c0/j;

    move-object/from16 v28, v0

    move-object/from16 v32, v2

    invoke-direct/range {v28 .. v35}, Lf/h/a/c/a1/c0/j;-><init>(ZLjava/lang/String;I[BII[B)V

    iput-object v0, v1, Lf/h/a/c/a1/c0/k;->n:Lf/h/a/c/a1/c0/j;

    goto :goto_2f

    :cond_47
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Entry count in sgpd != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_48
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Entry count in sbgp != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_49
    :goto_2f
    iget-object v0, v8, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_30
    if-ge v2, v0, :cond_4c

    iget-object v3, v8, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/a1/c0/a$b;

    iget v4, v3, Lf/h/a/c/a1/c0/a;->a:I

    const v5, 0x75756964

    if-ne v4, v5, :cond_4b

    iget-object v3, v3, Lf/h/a/c/a1/c0/a$b;->b:Lf/h/a/c/i1/r;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v4, v3, Lf/h/a/c/i1/r;->a:[B

    iget v5, v3, Lf/h/a/c/i1/r;->b:I

    const/4 v6, 0x0

    const/16 v7, 0x10

    move-object/from16 v9, v26

    invoke-static {v4, v5, v9, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v4, v3, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v4, v7

    iput v4, v3, Lf/h/a/c/i1/r;->b:I

    sget-object v4, Lf/h/a/c/a1/c0/d;->F:[B

    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_4a

    goto :goto_31

    :cond_4a
    invoke-static {v3, v7, v1}, Lf/h/a/c/a1/c0/d;->i(Lf/h/a/c/i1/r;ILf/h/a/c/a1/c0/k;)V

    goto :goto_31

    :cond_4b
    move-object/from16 v9, v26

    :goto_31
    add-int/lit8 v2, v2, 0x1

    move-object/from16 v26, v9

    goto :goto_30

    :cond_4c
    move-object/from16 v9, v26

    goto :goto_33

    :cond_4d
    :goto_32
    move-object v9, v1

    move-object/from16 v25, v2

    move-object/from16 v16, v3

    move/from16 v41, v4

    move/from16 v17, v5

    move/from16 v27, v7

    :goto_33
    add-int/lit8 v7, v27, 0x1

    const/16 v6, 0x8

    move-object/from16 v0, p0

    move-object v1, v9

    move-object/from16 v3, v16

    move/from16 v5, v17

    move-object/from16 v2, v25

    move/from16 v4, v41

    goto/16 :goto_a

    :cond_4e
    iget-object v0, v2, Lf/h/a/c/a1/c0/a$a;->c:Ljava/util/List;

    invoke-static {v0}, Lf/h/a/c/a1/c0/d;->c(Ljava/util/List;)Lcom/google/android/exoplayer2/drm/DrmInitData;

    move-result-object v0

    move-object/from16 v3, p0

    if-eqz v0, :cond_50

    iget-object v1, v3, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_34
    if-ge v2, v1, :cond_50

    iget-object v4, v3, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/a1/c0/d$b;

    iget-object v5, v4, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    iget-object v6, v4, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget-object v6, v6, Lf/h/a/c/a1/c0/k;->a:Lf/h/a/c/a1/c0/c;

    iget v6, v6, Lf/h/a/c/a1/c0/c;->a:I

    invoke-virtual {v5, v6}, Lf/h/a/c/a1/c0/i;->a(I)Lf/h/a/c/a1/c0/j;

    move-result-object v5

    if-eqz v5, :cond_4f

    iget-object v5, v5, Lf/h/a/c/a1/c0/j;->b:Ljava/lang/String;

    goto :goto_35

    :cond_4f
    const/4 v5, 0x0

    :goto_35
    iget-object v6, v4, Lf/h/a/c/a1/c0/d$b;->a:Lf/h/a/c/a1/s;

    iget-object v4, v4, Lf/h/a/c/a1/c0/d$b;->d:Lf/h/a/c/a1/c0/i;

    iget-object v4, v4, Lf/h/a/c/a1/c0/i;->f:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/drm/DrmInitData;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/drm/DrmInitData;

    move-result-object v5

    iget-object v7, v4, Lcom/google/android/exoplayer2/Format;->j:Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-virtual {v4, v5, v7}, Lcom/google/android/exoplayer2/Format;->a(Lcom/google/android/exoplayer2/drm/DrmInitData;Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format;

    move-result-object v4

    invoke-interface {v6, v4}, Lf/h/a/c/a1/s;->d(Lcom/google/android/exoplayer2/Format;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_34

    :cond_50
    iget-wide v0, v3, Lf/h/a/c/a1/c0/d;->t:J

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, v0, v4

    if-eqz v2, :cond_54

    iget-object v0, v3, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_36
    if-ge v1, v0, :cond_53

    iget-object v2, v3, Lf/h/a/c/a1/c0/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/a1/c0/d$b;

    iget-wide v4, v3, Lf/h/a/c/a1/c0/d;->t:J

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v4, v5}, Lf/h/a/c/u;->b(J)J

    move-result-wide v4

    iget v6, v2, Lf/h/a/c/a1/c0/d$b;->f:I

    :goto_37
    iget-object v7, v2, Lf/h/a/c/a1/c0/d$b;->b:Lf/h/a/c/a1/c0/k;

    iget v8, v7, Lf/h/a/c/a1/c0/k;->e:I

    if-ge v6, v8, :cond_52

    iget-object v8, v7, Lf/h/a/c/a1/c0/k;->j:[J

    aget-wide v9, v8, v6

    iget-object v8, v7, Lf/h/a/c/a1/c0/k;->i:[I

    aget v8, v8, v6

    int-to-long v11, v8

    add-long/2addr v9, v11

    cmp-long v8, v9, v4

    if-gez v8, :cond_52

    iget-object v7, v7, Lf/h/a/c/a1/c0/k;->k:[Z

    aget-boolean v7, v7, v6

    if-eqz v7, :cond_51

    iput v6, v2, Lf/h/a/c/a1/c0/d$b;->i:I

    :cond_51
    add-int/lit8 v6, v6, 0x1

    goto :goto_37

    :cond_52
    add-int/lit8 v1, v1, 0x1

    goto :goto_36

    :cond_53
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, v3, Lf/h/a/c/a1/c0/d;->t:J

    :cond_54
    move-object v1, v3

    goto :goto_38

    :cond_55
    move-object v3, v0

    iget-object v0, v1, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_56

    iget-object v0, v1, Lf/h/a/c/a1/c0/d;->k:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/a1/c0/a$a;

    iget-object v0, v0, Lf/h/a/c/a1/c0/a$a;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_56
    :goto_38
    move-object v0, v3

    goto/16 :goto_0

    :cond_57
    move-object v3, v0

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/a1/c0/d;->a()V

    return-void
.end method

.method public release()V
    .locals 0

    return-void
.end method
