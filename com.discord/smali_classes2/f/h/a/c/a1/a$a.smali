.class public Lf/h/a/c/a1/a$a;
.super Ljava/lang/Object;
.source "BinarySearchSeeker.java"

# interfaces
.implements Lf/h/a/c/a1/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/a1/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Lf/h/a/c/a1/a$d;

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:J


# direct methods
.method public constructor <init>(Lf/h/a/c/a1/a$d;JJJJJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/a1/a$a;->a:Lf/h/a/c/a1/a$d;

    iput-wide p2, p0, Lf/h/a/c/a1/a$a;->b:J

    iput-wide p4, p0, Lf/h/a/c/a1/a$a;->c:J

    iput-wide p6, p0, Lf/h/a/c/a1/a$a;->d:J

    iput-wide p8, p0, Lf/h/a/c/a1/a$a;->e:J

    iput-wide p10, p0, Lf/h/a/c/a1/a$a;->f:J

    iput-wide p12, p0, Lf/h/a/c/a1/a$a;->g:J

    return-void
.end method


# virtual methods
.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g(J)Lf/h/a/c/a1/q$a;
    .locals 13

    iget-object v0, p0, Lf/h/a/c/a1/a$a;->a:Lf/h/a/c/a1/a$d;

    invoke-interface {v0, p1, p2}, Lf/h/a/c/a1/a$d;->a(J)J

    move-result-wide v1

    iget-wide v3, p0, Lf/h/a/c/a1/a$a;->c:J

    iget-wide v5, p0, Lf/h/a/c/a1/a$a;->d:J

    iget-wide v7, p0, Lf/h/a/c/a1/a$a;->e:J

    iget-wide v9, p0, Lf/h/a/c/a1/a$a;->f:J

    iget-wide v11, p0, Lf/h/a/c/a1/a$a;->g:J

    invoke-static/range {v1 .. v12}, Lf/h/a/c/a1/a$c;->a(JJJJJJ)J

    move-result-wide v0

    new-instance v2, Lf/h/a/c/a1/q$a;

    new-instance v3, Lf/h/a/c/a1/r;

    invoke-direct {v3, p1, p2, v0, v1}, Lf/h/a/c/a1/r;-><init>(JJ)V

    invoke-direct {v2, v3}, Lf/h/a/c/a1/q$a;-><init>(Lf/h/a/c/a1/r;)V

    return-object v2
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, Lf/h/a/c/a1/a$a;->b:J

    return-wide v0
.end method
