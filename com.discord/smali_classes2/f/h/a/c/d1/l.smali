.class public final Lf/h/a/c/d1/l;
.super Ljava/lang/Object;
.source "ClippingMediaPeriod.java"

# interfaces
.implements Lf/h/a/c/d1/o;
.implements Lf/h/a/c/d1/o$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/d1/l$a;
    }
.end annotation


# instance fields
.field public final d:Lf/h/a/c/d1/o;

.field public e:Lf/h/a/c/d1/o$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public f:[Lf/h/a/c/d1/l$a;

.field public g:J

.field public h:J

.field public i:J


# direct methods
.method public constructor <init>(Lf/h/a/c/d1/o;ZJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    const/4 p1, 0x0

    new-array p1, p1, [Lf/h/a/c/d1/l$a;

    iput-object p1, p0, Lf/h/a/c/d1/l;->f:[Lf/h/a/c/d1/l$a;

    if-eqz p2, :cond_0

    move-wide p1, p3

    goto :goto_0

    :cond_0
    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    :goto_0
    iput-wide p1, p0, Lf/h/a/c/d1/l;->g:J

    iput-wide p3, p0, Lf/h/a/c/d1/l;->h:J

    iput-wide p5, p0, Lf/h/a/c/d1/l;->i:J

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 5

    iget-wide v0, p0, Lf/h/a/c/d1/l;->g:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b([Lf/h/a/c/f1/f;[Z[Lf/h/a/c/d1/v;[ZJ)J
    .locals 15

    move-object v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    array-length v1, v9

    new-array v1, v1, [Lf/h/a/c/d1/l$a;

    iput-object v1, v0, Lf/h/a/c/d1/l;->f:[Lf/h/a/c/d1/l$a;

    array-length v1, v9

    new-array v10, v1, [Lf/h/a/c/d1/v;

    const/4 v11, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v2, v9

    const/4 v12, 0x0

    if-ge v1, v2, :cond_1

    iget-object v2, v0, Lf/h/a/c/d1/l;->f:[Lf/h/a/c/d1/l$a;

    aget-object v3, v9, v1

    check-cast v3, Lf/h/a/c/d1/l$a;

    aput-object v3, v2, v1

    aget-object v3, v2, v1

    if-eqz v3, :cond_0

    aget-object v2, v2, v1

    iget-object v12, v2, Lf/h/a/c/d1/l$a;->a:Lf/h/a/c/d1/v;

    :cond_0
    aput-object v12, v10, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object v4, v10

    move-object/from16 v5, p4

    move-wide/from16 v6, p5

    invoke-interface/range {v1 .. v7}, Lf/h/a/c/d1/o;->b([Lf/h/a/c/f1/f;[Z[Lf/h/a/c/d1/v;[ZJ)J

    move-result-wide v1

    invoke-virtual {p0}, Lf/h/a/c/d1/l;->a()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_4

    iget-wide v5, v0, Lf/h/a/c/d1/l;->h:J

    cmp-long v3, p5, v5

    if-nez v3, :cond_4

    const-wide/16 v13, 0x0

    cmp-long v3, v5, v13

    if-eqz v3, :cond_3

    array-length v3, v8

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v3, :cond_3

    aget-object v6, v8, v5

    if-eqz v6, :cond_2

    invoke-interface {v6}, Lf/h/a/c/f1/f;->f()Lcom/google/android/exoplayer2/Format;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v6}, Lf/h/a/c/i1/o;->f(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_4

    move-wide v5, v1

    goto :goto_3

    :cond_4
    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    :goto_3
    iput-wide v5, v0, Lf/h/a/c/d1/l;->g:J

    cmp-long v3, v1, p5

    if-eqz v3, :cond_6

    iget-wide v5, v0, Lf/h/a/c/d1/l;->h:J

    cmp-long v3, v1, v5

    if-ltz v3, :cond_5

    iget-wide v5, v0, Lf/h/a/c/d1/l;->i:J

    const-wide/high16 v7, -0x8000000000000000L

    cmp-long v3, v5, v7

    if-eqz v3, :cond_6

    cmp-long v3, v1, v5

    if-gtz v3, :cond_5

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    :cond_6
    :goto_4
    invoke-static {v4}, Lf/g/j/k/a;->s(Z)V

    :goto_5
    array-length v3, v9

    if-ge v11, v3, :cond_a

    aget-object v3, v10, v11

    if-nez v3, :cond_7

    iget-object v3, v0, Lf/h/a/c/d1/l;->f:[Lf/h/a/c/d1/l$a;

    aput-object v12, v3, v11

    goto :goto_6

    :cond_7
    iget-object v3, v0, Lf/h/a/c/d1/l;->f:[Lf/h/a/c/d1/l$a;

    aget-object v4, v3, v11

    if-eqz v4, :cond_8

    aget-object v4, v3, v11

    iget-object v4, v4, Lf/h/a/c/d1/l$a;->a:Lf/h/a/c/d1/v;

    aget-object v5, v10, v11

    if-eq v4, v5, :cond_9

    :cond_8
    new-instance v4, Lf/h/a/c/d1/l$a;

    aget-object v5, v10, v11

    invoke-direct {v4, p0, v5}, Lf/h/a/c/d1/l$a;-><init>(Lf/h/a/c/d1/l;Lf/h/a/c/d1/v;)V

    aput-object v4, v3, v11

    :cond_9
    :goto_6
    iget-object v3, v0, Lf/h/a/c/d1/l;->f:[Lf/h/a/c/d1/l$a;

    aget-object v3, v3, v11

    aput-object v3, v9, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :cond_a
    return-wide v1
.end method

.method public c(Lf/h/a/c/d1/w;)V
    .locals 0

    iget-object p1, p0, Lf/h/a/c/d1/l;->e:Lf/h/a/c/d1/o$a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, p0}, Lf/h/a/c/d1/w$a;->c(Lf/h/a/c/d1/w;)V

    return-void
.end method

.method public d()J
    .locals 7

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->d()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    iget-wide v4, p0, Lf/h/a/c/d1/l;->i:J

    cmp-long v6, v4, v2

    if-eqz v6, :cond_0

    cmp-long v6, v0, v4

    if-ltz v6, :cond_0

    goto :goto_0

    :cond_0
    return-wide v0

    :cond_1
    :goto_0
    return-wide v2
.end method

.method public e(Lf/h/a/c/d1/o;)V
    .locals 0

    iget-object p1, p0, Lf/h/a/c/d1/l;->e:Lf/h/a/c/d1/o$a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, p0}, Lf/h/a/c/d1/o$a;->e(Lf/h/a/c/d1/o;)V

    return-void
.end method

.method public f()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->f()V

    return-void
.end method

.method public g(J)J
    .locals 6

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lf/h/a/c/d1/l;->g:J

    iget-object v0, p0, Lf/h/a/c/d1/l;->f:[Lf/h/a/c/d1/l$a;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    if-eqz v4, :cond_0

    iput-boolean v2, v4, Lf/h/a/c/d1/l$a;->b:Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0, p1, p2}, Lf/h/a/c/d1/o;->g(J)J

    move-result-wide v0

    cmp-long v3, v0, p1

    if-eqz v3, :cond_2

    iget-wide p1, p0, Lf/h/a/c/d1/l;->h:J

    cmp-long v3, v0, p1

    if-ltz v3, :cond_3

    iget-wide p1, p0, Lf/h/a/c/d1/l;->i:J

    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v5, p1, v3

    if-eqz v5, :cond_2

    cmp-long v3, v0, p1

    if-gtz v3, :cond_3

    :cond_2
    const/4 v2, 0x1

    :cond_3
    invoke-static {v2}, Lf/g/j/k/a;->s(Z)V

    return-wide v0
.end method

.method public h(J)Z
    .locals 1

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0, p1, p2}, Lf/h/a/c/d1/o;->h(J)Z

    move-result p1

    return p1
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->i()Z

    move-result v0

    return v0
.end method

.method public j(JLf/h/a/c/r0;)J
    .locals 9

    iget-wide v0, p0, Lf/h/a/c/d1/l;->h:J

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    return-wide v0

    :cond_0
    iget-wide v3, p3, Lf/h/a/c/r0;->a:J

    sub-long v7, p1, v0

    const-wide/16 v5, 0x0

    invoke-static/range {v3 .. v8}, Lf/h/a/c/i1/a0;->g(JJJ)J

    move-result-wide v0

    iget-wide v2, p3, Lf/h/a/c/r0;->b:J

    iget-wide v4, p0, Lf/h/a/c/d1/l;->i:J

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v8, v4, v6

    if-nez v8, :cond_1

    const-wide v4, 0x7fffffffffffffffL

    goto :goto_0

    :cond_1
    sub-long/2addr v4, p1

    :goto_0
    move-wide v6, v4

    const-wide/16 v4, 0x0

    invoke-static/range {v2 .. v7}, Lf/h/a/c/i1/a0;->g(JJJ)J

    move-result-wide v2

    iget-wide v4, p3, Lf/h/a/c/r0;->a:J

    cmp-long v6, v0, v4

    if-nez v6, :cond_2

    iget-wide v4, p3, Lf/h/a/c/r0;->b:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    goto :goto_1

    :cond_2
    new-instance p3, Lf/h/a/c/r0;

    invoke-direct {p3, v0, v1, v2, v3}, Lf/h/a/c/r0;-><init>(JJ)V

    :goto_1
    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0, p1, p2, p3}, Lf/h/a/c/d1/o;->j(JLf/h/a/c/r0;)J

    move-result-wide p1

    return-wide p1
.end method

.method public l()J
    .locals 9

    invoke-virtual {p0}, Lf/h/a/c/d1/l;->a()Z

    move-result v0

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v0, :cond_1

    iget-wide v3, p0, Lf/h/a/c/d1/l;->g:J

    iput-wide v1, p0, Lf/h/a/c/d1/l;->g:J

    invoke-virtual {p0}, Lf/h/a/c/d1/l;->l()J

    move-result-wide v5

    cmp-long v0, v5, v1

    if-eqz v0, :cond_0

    move-wide v3, v5

    :cond_0
    return-wide v3

    :cond_1
    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->l()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-nez v0, :cond_2

    return-wide v1

    :cond_2
    iget-wide v0, p0, Lf/h/a/c/d1/l;->h:J

    const/4 v2, 0x1

    const/4 v5, 0x0

    cmp-long v6, v3, v0

    if-ltz v6, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iget-wide v0, p0, Lf/h/a/c/d1/l;->i:J

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v8, v0, v6

    if-eqz v8, :cond_5

    cmp-long v6, v3, v0

    if-gtz v6, :cond_4

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :cond_5
    :goto_1
    invoke-static {v2}, Lf/g/j/k/a;->s(Z)V

    return-wide v3
.end method

.method public m(Lf/h/a/c/d1/o$a;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/d1/l;->e:Lf/h/a/c/d1/o$a;

    iget-object p1, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {p1, p0, p2, p3}, Lf/h/a/c/d1/o;->m(Lf/h/a/c/d1/o$a;J)V

    return-void
.end method

.method public n()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->n()Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v0

    return-object v0
.end method

.method public q()J
    .locals 7

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->q()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    iget-wide v4, p0, Lf/h/a/c/d1/l;->i:J

    cmp-long v6, v4, v2

    if-eqz v6, :cond_0

    cmp-long v6, v0, v4

    if-ltz v6, :cond_0

    goto :goto_0

    :cond_0
    return-wide v0

    :cond_1
    :goto_0
    return-wide v2
.end method

.method public r(JZ)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0, p1, p2, p3}, Lf/h/a/c/d1/o;->r(JZ)V

    return-void
.end method

.method public s(J)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v0, p1, p2}, Lf/h/a/c/d1/o;->s(J)V

    return-void
.end method
