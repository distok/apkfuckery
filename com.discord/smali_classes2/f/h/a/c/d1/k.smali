.class public abstract Lf/h/a/c/d1/k;
.super Ljava/lang/Object;
.source "BaseMediaSource.java"

# interfaces
.implements Lf/h/a/c/d1/p;


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lf/h/a/c/d1/p$b;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lf/h/a/c/d1/p$b;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/c/d1/q$a;

.field public d:Landroid/os/Looper;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public e:Lf/h/a/c/t0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/d1/k;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    new-instance v0, Lf/h/a/c/d1/q$a;

    invoke-direct {v0}, Lf/h/a/c/d1/q$a;-><init>()V

    iput-object v0, p0, Lf/h/a/c/d1/k;->c:Lf/h/a/c/d1/q$a;

    return-void
.end method


# virtual methods
.method public final b(Lf/h/a/c/d1/p$b;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/d1/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lf/h/a/c/d1/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/d1/k;->d:Landroid/os/Looper;

    iput-object p1, p0, Lf/h/a/c/d1/k;->e:Lf/h/a/c/t0;

    iget-object p1, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    move-object p1, p0

    check-cast p1, Lf/h/a/c/d1/s;

    iget-object p1, p1, Lf/h/a/c/d1/s;->i:Lf/h/a/c/z0/b;

    invoke-interface {p1}, Lf/h/a/c/z0/b;->release()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    if-eqz v0, :cond_1

    iget-object p1, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    :cond_1
    :goto_0
    return-void
.end method

.method public final c(Lf/h/a/c/d1/q;)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/d1/k;->c:Lf/h/a/c/d1/q$a;

    iget-object v1, v0, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/d1/q$a$a;

    iget-object v3, v2, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    if-ne v3, p1, :cond_0

    iget-object v3, v0, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final f(Lf/h/a/c/d1/p$b;Lf/h/a/c/h1/x;)V
    .locals 3
    .param p2    # Lf/h/a/c/h1/x;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/c/d1/k;->d:Landroid/os/Looper;

    if-eqz v1, :cond_1

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lf/g/j/k/a;->d(Z)V

    iget-object v1, p0, Lf/h/a/c/d1/k;->e:Lf/h/a/c/t0;

    iget-object v2, p0, Lf/h/a/c/d1/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lf/h/a/c/d1/k;->d:Landroid/os/Looper;

    if-nez v2, :cond_2

    iput-object v0, p0, Lf/h/a/c/d1/k;->d:Landroid/os/Looper;

    iget-object v0, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-object p1, p0

    check-cast p1, Lf/h/a/c/d1/s;

    iput-object p2, p1, Lf/h/a/c/d1/s;->q:Lf/h/a/c/h1/x;

    iget-object p2, p1, Lf/h/a/c/d1/s;->i:Lf/h/a/c/z0/b;

    invoke-interface {p2}, Lf/h/a/c/z0/b;->a()V

    iget-wide v0, p1, Lf/h/a/c/d1/s;->n:J

    iget-boolean p2, p1, Lf/h/a/c/d1/s;->o:Z

    iget-boolean v2, p1, Lf/h/a/c/d1/s;->p:Z

    invoke-virtual {p1, v0, v1, p2, v2}, Lf/h/a/c/d1/s;->h(JZZ)V

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_3

    iget-object p2, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    iget-object p2, p0, Lf/h/a/c/d1/k;->b:Ljava/util/HashSet;

    invoke-virtual {p2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    check-cast p1, Lf/h/a/c/b0;

    invoke-virtual {p1, p0, v1}, Lf/h/a/c/b0;->a(Lf/h/a/c/d1/p;Lf/h/a/c/t0;)V

    :cond_3
    :goto_2
    return-void
.end method

.method public final g(Landroid/os/Handler;Lf/h/a/c/d1/q;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/d1/k;->c:Lf/h/a/c/d1/q$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->d(Z)V

    iget-object v0, v0, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lf/h/a/c/d1/q$a$a;

    invoke-direct {v1, p1, p2}, Lf/h/a/c/d1/q$a$a;-><init>(Landroid/os/Handler;Lf/h/a/c/d1/q;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method
