.class public final Lf/h/a/c/d1/r$a;
.super Ljava/lang/Object;
.source "ProgressiveMediaPeriod.java"

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/Loader$e;
.implements Lf/h/a/c/d1/n$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/d1/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Lf/h/a/c/h1/v;

.field public final c:Lf/h/a/c/d1/r$b;

.field public final d:Lf/h/a/c/a1/i;

.field public final e:Lf/h/a/c/i1/i;

.field public final f:Lf/h/a/c/a1/p;

.field public volatile g:Z

.field public h:Z

.field public i:J

.field public j:Lf/h/a/c/h1/k;

.field public k:J

.field public l:Lf/h/a/c/a1/s;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public final synthetic n:Lf/h/a/c/d1/r;


# direct methods
.method public constructor <init>(Lf/h/a/c/d1/r;Landroid/net/Uri;Lf/h/a/c/h1/j;Lf/h/a/c/d1/r$b;Lf/h/a/c/a1/i;Lf/h/a/c/i1/i;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/h/a/c/d1/r$a;->a:Landroid/net/Uri;

    new-instance p1, Lf/h/a/c/h1/v;

    invoke-direct {p1, p3}, Lf/h/a/c/h1/v;-><init>(Lf/h/a/c/h1/j;)V

    iput-object p1, p0, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    iput-object p4, p0, Lf/h/a/c/d1/r$a;->c:Lf/h/a/c/d1/r$b;

    iput-object p5, p0, Lf/h/a/c/d1/r$a;->d:Lf/h/a/c/a1/i;

    iput-object p6, p0, Lf/h/a/c/d1/r$a;->e:Lf/h/a/c/i1/i;

    new-instance p1, Lf/h/a/c/a1/p;

    invoke-direct {p1}, Lf/h/a/c/a1/p;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/r$a;->f:Lf/h/a/c/a1/p;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/d1/r$a;->h:Z

    const-wide/16 p1, -0x1

    iput-wide p1, p0, Lf/h/a/c/d1/r$a;->k:J

    const-wide/16 p1, 0x0

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/d1/r$a;->a(J)Lf/h/a/c/h1/k;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/d1/r$a;->j:Lf/h/a/c/h1/k;

    return-void
.end method


# virtual methods
.method public final a(J)Lf/h/a/c/h1/k;
    .locals 15

    move-object v0, p0

    new-instance v14, Lf/h/a/c/h1/k;

    iget-object v2, v0, Lf/h/a/c/d1/r$a;->a:Landroid/net/Uri;

    iget-object v1, v0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    iget-object v11, v1, Lf/h/a/c/d1/r;->k:Ljava/lang/String;

    const/4 v12, 0x6

    sget-object v13, Lf/h/a/c/d1/r;->P:Ljava/util/Map;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-wide/16 v9, -0x1

    move-object v1, v14

    move-wide/from16 v5, p1

    move-wide/from16 v7, p1

    invoke-direct/range {v1 .. v13}, Lf/h/a/c/h1/k;-><init>(Landroid/net/Uri;I[BJJJLjava/lang/String;ILjava/util/Map;)V

    return-object v14
.end method

.method public b()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    if-nez v1, :cond_b

    iget-boolean v2, p0, Lf/h/a/c/d1/r$a;->g:Z

    if-nez v2, :cond_b

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_0
    iget-object v4, p0, Lf/h/a/c/d1/r$a;->f:Lf/h/a/c/a1/p;

    iget-wide v11, v4, Lf/h/a/c/a1/p;->a:J

    invoke-virtual {p0, v11, v12}, Lf/h/a/c/d1/r$a;->a(J)Lf/h/a/c/h1/k;

    move-result-object v4

    iput-object v4, p0, Lf/h/a/c/d1/r$a;->j:Lf/h/a/c/h1/k;

    iget-object v5, p0, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    invoke-virtual {v5, v4}, Lf/h/a/c/h1/v;->a(Lf/h/a/c/h1/k;)J

    move-result-wide v4

    iput-wide v4, p0, Lf/h/a/c/d1/r$a;->k:J

    const-wide/16 v6, -0x1

    cmp-long v8, v4, v6

    if-eqz v8, :cond_1

    add-long/2addr v4, v11

    iput-wide v4, p0, Lf/h/a/c/d1/r$a;->k:J

    :cond_1
    iget-object v4, p0, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    invoke-virtual {v4}, Lf/h/a/c/h1/v;->d()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    iget-object v6, p0, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    invoke-virtual {v6}, Lf/h/a/c/h1/v;->c()Ljava/util/Map;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;->a(Ljava/util/Map;)Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    move-result-object v6

    iput-object v6, v5, Lf/h/a/c/d1/r;->u:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    iget-object v5, p0, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    iget-object v6, p0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    iget-object v6, v6, Lf/h/a/c/d1/r;->u:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    if-eqz v6, :cond_2

    iget v6, v6, Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;->i:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    new-instance v7, Lf/h/a/c/d1/n;

    invoke-direct {v7, v5, v6, p0}, Lf/h/a/c/d1/n;-><init>(Lf/h/a/c/h1/j;ILf/h/a/c/d1/n$a;)V

    iget-object v5, p0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    new-instance v6, Lf/h/a/c/d1/r$f;

    invoke-direct {v6, v0, v3}, Lf/h/a/c/d1/r$f;-><init>(IZ)V

    invoke-virtual {v5, v6}, Lf/h/a/c/d1/r;->z(Lf/h/a/c/d1/r$f;)Lf/h/a/c/a1/s;

    move-result-object v5

    iput-object v5, p0, Lf/h/a/c/d1/r$a;->l:Lf/h/a/c/a1/s;

    sget-object v6, Lf/h/a/c/d1/r;->Q:Lcom/google/android/exoplayer2/Format;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    check-cast v5, Lf/h/a/c/d1/u;

    :try_start_1
    invoke-virtual {v5, v6}, Lf/h/a/c/d1/u;->d(Lcom/google/android/exoplayer2/Format;)V

    move-object v6, v7

    goto :goto_1

    :cond_2
    move-object v6, v5

    :goto_1
    new-instance v13, Lf/h/a/c/a1/e;

    iget-wide v9, p0, Lf/h/a/c/d1/r$a;->k:J

    move-object v5, v13

    move-wide v7, v11

    invoke-direct/range {v5 .. v10}, Lf/h/a/c/a1/e;-><init>(Lf/h/a/c/h1/j;JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    iget-object v2, p0, Lf/h/a/c/d1/r$a;->c:Lf/h/a/c/d1/r$b;

    iget-object v5, p0, Lf/h/a/c/d1/r$a;->d:Lf/h/a/c/a1/i;

    invoke-virtual {v2, v13, v5, v4}, Lf/h/a/c/d1/r$b;->a(Lf/h/a/c/a1/e;Lf/h/a/c/a1/i;Landroid/net/Uri;)Lf/h/a/c/a1/h;

    move-result-object v2

    iget-object v4, p0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    iget-object v4, v4, Lf/h/a/c/d1/r;->u:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    if-eqz v4, :cond_3

    instance-of v4, v2, Lf/h/a/c/a1/b0/d;

    if-eqz v4, :cond_3

    move-object v4, v2

    check-cast v4, Lf/h/a/c/a1/b0/d;

    iput-boolean v3, v4, Lf/h/a/c/a1/b0/d;->l:Z

    :cond_3
    iget-boolean v4, p0, Lf/h/a/c/d1/r$a;->h:Z

    if-eqz v4, :cond_4

    iget-wide v4, p0, Lf/h/a/c/d1/r$a;->i:J

    invoke-interface {v2, v11, v12, v4, v5}, Lf/h/a/c/a1/h;->f(JJ)V

    iput-boolean v0, p0, Lf/h/a/c/d1/r$a;->h:Z

    :cond_4
    :goto_2
    if-nez v1, :cond_6

    iget-boolean v4, p0, Lf/h/a/c/d1/r$a;->g:Z

    if-nez v4, :cond_6

    iget-object v4, p0, Lf/h/a/c/d1/r$a;->e:Lf/h/a/c/i1/i;

    monitor-enter v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :goto_3
    :try_start_3
    iget-boolean v5, v4, Lf/h/a/c/i1/i;->a:Z

    if-nez v5, :cond_5

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :cond_5
    :try_start_4
    monitor-exit v4

    iget-object v4, p0, Lf/h/a/c/d1/r$a;->f:Lf/h/a/c/a1/p;

    invoke-interface {v2, v13, v4}, Lf/h/a/c/a1/h;->d(Lf/h/a/c/a1/e;Lf/h/a/c/a1/p;)I

    move-result v1

    iget-wide v4, v13, Lf/h/a/c/a1/e;->d:J

    iget-object v6, p0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    iget-wide v6, v6, Lf/h/a/c/d1/r;->l:J

    add-long/2addr v6, v11

    cmp-long v8, v4, v6

    if-lez v8, :cond_4

    iget-object v6, p0, Lf/h/a/c/d1/r$a;->e:Lf/h/a/c/i1/i;

    monitor-enter v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    iput-boolean v0, v6, Lf/h/a/c/i1/i;->a:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    monitor-exit v6

    iget-object v6, p0, Lf/h/a/c/d1/r$a;->n:Lf/h/a/c/d1/r;

    iget-object v7, v6, Lf/h/a/c/d1/r;->r:Landroid/os/Handler;

    iget-object v6, v6, Lf/h/a/c/d1/r;->q:Ljava/lang/Runnable;

    invoke-virtual {v7, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-wide v11, v4

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :cond_6
    if-ne v1, v3, :cond_7

    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    iget-object v2, p0, Lf/h/a/c/d1/r$a;->f:Lf/h/a/c/a1/p;

    iget-wide v3, v13, Lf/h/a/c/a1/e;->d:J

    iput-wide v3, v2, Lf/h/a/c/a1/p;->a:J

    :goto_4
    iget-object v2, p0, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    if-eqz v2, :cond_0

    :try_start_7
    iget-object v2, v2, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v2}, Lf/h/a/c/h1/j;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_0

    :catch_0
    nop

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    move-object v2, v13

    goto :goto_5

    :catchall_3
    move-exception v0

    :goto_5
    if-ne v1, v3, :cond_8

    goto :goto_6

    :cond_8
    if-eqz v2, :cond_9

    iget-object v1, p0, Lf/h/a/c/d1/r$a;->f:Lf/h/a/c/a1/p;

    iget-wide v2, v2, Lf/h/a/c/a1/e;->d:J

    iput-wide v2, v1, Lf/h/a/c/a1/p;->a:J

    :cond_9
    :goto_6
    iget-object v1, p0, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    sget v2, Lf/h/a/c/i1/a0;->a:I

    if-eqz v1, :cond_a

    :try_start_8
    iget-object v1, v1, Lf/h/a/c/h1/v;->a:Lf/h/a/c/h1/j;

    invoke-interface {v1}, Lf/h/a/c/h1/j;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    :catch_1
    :cond_a
    throw v0

    :cond_b
    return-void
.end method
