.class public final Lf/h/a/c/d1/r;
.super Ljava/lang/Object;
.source "ProgressiveMediaPeriod.java"

# interfaces
.implements Lf/h/a/c/d1/o;
.implements Lf/h/a/c/a1/i;
.implements Lcom/google/android/exoplayer2/upstream/Loader$b;
.implements Lcom/google/android/exoplayer2/upstream/Loader$f;
.implements Lf/h/a/c/d1/u$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/d1/r$f;,
        Lf/h/a/c/d1/r$d;,
        Lf/h/a/c/d1/r$b;,
        Lf/h/a/c/d1/r$a;,
        Lf/h/a/c/d1/r$e;,
        Lf/h/a/c/d1/r$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/c/d1/o;",
        "Lf/h/a/c/a1/i;",
        "Lcom/google/android/exoplayer2/upstream/Loader$b<",
        "Lf/h/a/c/d1/r$a;",
        ">;",
        "Lcom/google/android/exoplayer2/upstream/Loader$f;",
        "Lf/h/a/c/d1/u$b;"
    }
.end annotation


# static fields
.field public static final P:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Q:Lcom/google/android/exoplayer2/Format;


# instance fields
.field public A:Z

.field public B:I

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:I

.field public G:J

.field public H:J

.field public I:Z

.field public J:J

.field public K:J

.field public L:Z

.field public M:I

.field public N:Z

.field public O:Z

.field public final d:Landroid/net/Uri;

.field public final e:Lf/h/a/c/h1/j;

.field public final f:Lf/h/a/c/z0/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/c/z0/b<",
            "*>;"
        }
    .end annotation
.end field

.field public final g:Lf/h/a/c/h1/u;

.field public final h:Lf/h/a/c/d1/q$a;

.field public final i:Lf/h/a/c/d1/r$c;

.field public final j:Lf/h/a/c/h1/d;

.field public final k:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final l:J

.field public final m:Lcom/google/android/exoplayer2/upstream/Loader;

.field public final n:Lf/h/a/c/d1/r$b;

.field public final o:Lf/h/a/c/i1/i;

.field public final p:Ljava/lang/Runnable;

.field public final q:Ljava/lang/Runnable;

.field public final r:Landroid/os/Handler;

.field public s:Lf/h/a/c/d1/o$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public t:Lf/h/a/c/a1/q;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public v:[Lf/h/a/c/d1/u;

.field public w:[Lf/h/a/c/d1/r$f;

.field public x:Z

.field public y:Z

.field public z:Lf/h/a/c/d1/r$d;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Icy-MetaData"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lf/h/a/c/d1/r;->P:Ljava/util/Map;

    const-wide v0, 0x7fffffffffffffffL

    const-string v2, "icy"

    const-string v3, "application/x-icy"

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/exoplayer2/Format;->i(Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    sput-object v0, Lf/h/a/c/d1/r;->Q:Lcom/google/android/exoplayer2/Format;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lf/h/a/c/h1/j;[Lf/h/a/c/a1/h;Lf/h/a/c/z0/b;Lf/h/a/c/h1/u;Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/r$c;Lf/h/a/c/h1/d;Ljava/lang/String;I)V
    .locals 0
    .param p9    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lf/h/a/c/h1/j;",
            "[",
            "Lf/h/a/c/a1/h;",
            "Lf/h/a/c/z0/b<",
            "*>;",
            "Lf/h/a/c/h1/u;",
            "Lf/h/a/c/d1/q$a;",
            "Lf/h/a/c/d1/r$c;",
            "Lf/h/a/c/h1/d;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/r;->d:Landroid/net/Uri;

    iput-object p2, p0, Lf/h/a/c/d1/r;->e:Lf/h/a/c/h1/j;

    iput-object p4, p0, Lf/h/a/c/d1/r;->f:Lf/h/a/c/z0/b;

    iput-object p5, p0, Lf/h/a/c/d1/r;->g:Lf/h/a/c/h1/u;

    iput-object p6, p0, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iput-object p7, p0, Lf/h/a/c/d1/r;->i:Lf/h/a/c/d1/r$c;

    iput-object p8, p0, Lf/h/a/c/d1/r;->j:Lf/h/a/c/h1/d;

    iput-object p9, p0, Lf/h/a/c/d1/r;->k:Ljava/lang/String;

    int-to-long p1, p10

    iput-wide p1, p0, Lf/h/a/c/d1/r;->l:J

    new-instance p1, Lcom/google/android/exoplayer2/upstream/Loader;

    const-string p2, "Loader:ProgressiveMediaPeriod"

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/upstream/Loader;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    new-instance p1, Lf/h/a/c/d1/r$b;

    invoke-direct {p1, p3}, Lf/h/a/c/d1/r$b;-><init>([Lf/h/a/c/a1/h;)V

    iput-object p1, p0, Lf/h/a/c/d1/r;->n:Lf/h/a/c/d1/r$b;

    new-instance p1, Lf/h/a/c/i1/i;

    invoke-direct {p1}, Lf/h/a/c/i1/i;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/r;->o:Lf/h/a/c/i1/i;

    new-instance p1, Lf/h/a/c/d1/i;

    invoke-direct {p1, p0}, Lf/h/a/c/d1/i;-><init>(Lf/h/a/c/d1/r;)V

    iput-object p1, p0, Lf/h/a/c/d1/r;->p:Ljava/lang/Runnable;

    new-instance p1, Lf/h/a/c/d1/j;

    invoke-direct {p1, p0}, Lf/h/a/c/d1/j;-><init>(Lf/h/a/c/d1/r;)V

    iput-object p1, p0, Lf/h/a/c/d1/r;->q:Ljava/lang/Runnable;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/r;->r:Landroid/os/Handler;

    const/4 p1, 0x0

    new-array p2, p1, [Lf/h/a/c/d1/r$f;

    iput-object p2, p0, Lf/h/a/c/d1/r;->w:[Lf/h/a/c/d1/r$f;

    new-array p1, p1, [Lf/h/a/c/d1/u;

    iput-object p1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lf/h/a/c/d1/r;->K:J

    const-wide/16 p3, -0x1

    iput-wide p3, p0, Lf/h/a/c/d1/r;->H:J

    iput-wide p1, p0, Lf/h/a/c/d1/r;->G:J

    const/4 p1, 0x1

    iput p1, p0, Lf/h/a/c/d1/r;->B:I

    iget-object p1, p6, Lf/h/a/c/d1/q$a;->b:Lf/h/a/c/d1/p$a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p6, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/h/a/c/d1/q$a$a;

    iget-object p4, p3, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object p3, p3, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance p5, Lf/h/a/c/d1/h;

    invoke-direct {p5, p6, p4, p1}, Lf/h/a/c/d1/h;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/p$a;)V

    invoke-virtual {p6, p3, p5}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 26

    move-object/from16 v8, p0

    new-instance v9, Lf/h/a/c/d1/r$a;

    iget-object v2, v8, Lf/h/a/c/d1/r;->d:Landroid/net/Uri;

    iget-object v3, v8, Lf/h/a/c/d1/r;->e:Lf/h/a/c/h1/j;

    iget-object v4, v8, Lf/h/a/c/d1/r;->n:Lf/h/a/c/d1/r$b;

    iget-object v6, v8, Lf/h/a/c/d1/r;->o:Lf/h/a/c/i1/i;

    move-object v0, v9

    move-object/from16 v1, p0

    move-object/from16 v5, p0

    invoke-direct/range {v0 .. v6}, Lf/h/a/c/d1/r$a;-><init>(Lf/h/a/c/d1/r;Landroid/net/Uri;Lf/h/a/c/h1/j;Lf/h/a/c/d1/r$b;Lf/h/a/c/a1/i;Lf/h/a/c/i1/i;)V

    iget-boolean v0, v8, Lf/h/a/c/d1/r;->y:Z

    if-eqz v0, :cond_1

    iget-object v0, v8, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->a:Lf/h/a/c/a1/q;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/d1/r;->v()Z

    move-result v1

    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iget-wide v1, v8, Lf/h/a/c/d1/r;->G:J

    const/4 v3, 0x1

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v6, v1, v4

    if-eqz v6, :cond_0

    iget-wide v6, v8, Lf/h/a/c/d1/r;->K:J

    cmp-long v10, v6, v1

    if-lez v10, :cond_0

    iput-boolean v3, v8, Lf/h/a/c/d1/r;->N:Z

    iput-wide v4, v8, Lf/h/a/c/d1/r;->K:J

    return-void

    :cond_0
    iget-wide v1, v8, Lf/h/a/c/d1/r;->K:J

    invoke-interface {v0, v1, v2}, Lf/h/a/c/a1/q;->g(J)Lf/h/a/c/a1/q$a;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    iget-wide v0, v0, Lf/h/a/c/a1/r;->b:J

    iget-wide v6, v8, Lf/h/a/c/d1/r;->K:J

    iget-object v2, v9, Lf/h/a/c/d1/r$a;->f:Lf/h/a/c/a1/p;

    iput-wide v0, v2, Lf/h/a/c/a1/p;->a:J

    iput-wide v6, v9, Lf/h/a/c/d1/r$a;->i:J

    iput-boolean v3, v9, Lf/h/a/c/d1/r$a;->h:Z

    const/4 v0, 0x0

    iput-boolean v0, v9, Lf/h/a/c/d1/r$a;->m:Z

    iput-wide v4, v8, Lf/h/a/c/d1/r;->K:J

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/d1/r;->t()I

    move-result v0

    iput v0, v8, Lf/h/a/c/d1/r;->M:I

    iget-object v1, v8, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    iget-object v0, v8, Lf/h/a/c/d1/r;->g:Lf/h/a/c/h1/u;

    iget v2, v8, Lf/h/a/c/d1/r;->B:I

    check-cast v0, Lf/h/a/c/h1/r;

    invoke-virtual {v0, v2}, Lf/h/a/c/h1/r;->a(I)I

    move-result v5

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {v2}, Lf/g/j/k/a;->w(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/exoplayer2/upstream/Loader;->c:Ljava/io/IOException;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    new-instance v10, Lcom/google/android/exoplayer2/upstream/Loader$d;

    move-object v0, v10

    move-object v3, v9

    move-object/from16 v4, p0

    move-wide v6, v14

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/upstream/Loader$d;-><init>(Lcom/google/android/exoplayer2/upstream/Loader;Landroid/os/Looper;Lcom/google/android/exoplayer2/upstream/Loader$e;Lcom/google/android/exoplayer2/upstream/Loader$b;IJ)V

    const-wide/16 v0, 0x0

    invoke-virtual {v10, v0, v1}, Lcom/google/android/exoplayer2/upstream/Loader$d;->b(J)V

    iget-object v0, v8, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iget-object v11, v9, Lf/h/a/c/d1/r$a;->j:Lf/h/a/c/h1/k;

    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    iget-wide v4, v9, Lf/h/a/c/d1/r$a;->i:J

    iget-wide v6, v8, Lf/h/a/c/d1/r;->G:J

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v9, Lf/h/a/c/d1/q$b;

    iget-object v12, v11, Lf/h/a/c/h1/k;->a:Landroid/net/Uri;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v13

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    move-object v10, v9

    invoke-direct/range {v10 .. v19}, Lf/h/a/c/d1/q$b;-><init>(Lf/h/a/c/h1/k;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    new-instance v10, Lf/h/a/c/d1/q$c;

    invoke-virtual {v0, v4, v5}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v22

    invoke-virtual {v0, v6, v7}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v24

    move-object/from16 v16, v10

    move/from16 v17, v1

    move/from16 v18, v2

    move-object/from16 v19, v3

    invoke-direct/range {v16 .. v25}, Lf/h/a/c/d1/q$c;-><init>(IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    iget-object v1, v0, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/d1/q$a$a;

    iget-object v3, v2, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object v2, v2, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance v4, Lf/h/a/c/d1/a;

    invoke-direct {v4, v0, v3, v9, v10}, Lf/h/a/c/d1/a;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/q$b;Lf/h/a/c/d1/q$c;)V

    invoke-virtual {v0, v2, v4}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final B()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/d1/r;->D:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public a(Lf/h/a/c/a1/q;)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/d1/r;->u:Lcom/google/android/exoplayer2/metadata/icy/IcyHeaders;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lf/h/a/c/a1/q$b;

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    const-wide/16 v2, 0x0

    invoke-direct {p1, v0, v1, v2, v3}, Lf/h/a/c/a1/q$b;-><init>(JJ)V

    :goto_0
    iput-object p1, p0, Lf/h/a/c/d1/r;->t:Lf/h/a/c/a1/q;

    iget-object p1, p0, Lf/h/a/c/d1/r;->r:Landroid/os/Handler;

    iget-object v0, p0, Lf/h/a/c/d1/r;->p:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b([Lf/h/a/c/f1/f;[Z[Lf/h/a/c/d1/v;[ZJ)J
    .locals 8

    iget-object v0, p0, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/h/a/c/d1/r$d;->b:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->d:[Z

    iget v2, p0, Lf/h/a/c/d1/r;->F:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    array-length v5, p1

    const/4 v6, 0x1

    if-ge v4, v5, :cond_2

    aget-object v5, p3, v4

    if-eqz v5, :cond_1

    aget-object v5, p1, v4

    if-eqz v5, :cond_0

    aget-boolean v5, p2, v4

    if-nez v5, :cond_1

    :cond_0
    aget-object v5, p3, v4

    check-cast v5, Lf/h/a/c/d1/r$e;

    iget v5, v5, Lf/h/a/c/d1/r$e;->a:I

    aget-boolean v7, v0, v5

    invoke-static {v7}, Lf/g/j/k/a;->s(Z)V

    iget v7, p0, Lf/h/a/c/d1/r;->F:I

    sub-int/2addr v7, v6

    iput v7, p0, Lf/h/a/c/d1/r;->F:I

    aput-boolean v3, v0, v5

    const/4 v5, 0x0

    aput-object v5, p3, v4

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    iget-boolean p2, p0, Lf/h/a/c/d1/r;->C:Z

    if-eqz p2, :cond_3

    if-nez v2, :cond_4

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0x0

    cmp-long p2, p5, v4

    if-eqz p2, :cond_4

    :goto_1
    const/4 p2, 0x1

    goto :goto_2

    :cond_4
    const/4 p2, 0x0

    :goto_2
    const/4 v2, 0x0

    :goto_3
    array-length v4, p1

    if-ge v2, v4, :cond_9

    aget-object v4, p3, v2

    if-nez v4, :cond_8

    aget-object v4, p1, v2

    if-eqz v4, :cond_8

    aget-object v4, p1, v2

    invoke-interface {v4}, Lf/h/a/c/f1/f;->length()I

    move-result v5

    if-ne v5, v6, :cond_5

    const/4 v5, 0x1

    goto :goto_4

    :cond_5
    const/4 v5, 0x0

    :goto_4
    invoke-static {v5}, Lf/g/j/k/a;->s(Z)V

    invoke-interface {v4, v3}, Lf/h/a/c/f1/f;->e(I)I

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x1

    goto :goto_5

    :cond_6
    const/4 v5, 0x0

    :goto_5
    invoke-static {v5}, Lf/g/j/k/a;->s(Z)V

    invoke-interface {v4}, Lf/h/a/c/f1/f;->a()Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->a(Lcom/google/android/exoplayer2/source/TrackGroup;)I

    move-result v4

    aget-boolean v5, v0, v4

    xor-int/2addr v5, v6

    invoke-static {v5}, Lf/g/j/k/a;->s(Z)V

    iget v5, p0, Lf/h/a/c/d1/r;->F:I

    add-int/2addr v5, v6

    iput v5, p0, Lf/h/a/c/d1/r;->F:I

    aput-boolean v6, v0, v4

    new-instance v5, Lf/h/a/c/d1/r$e;

    invoke-direct {v5, p0, v4}, Lf/h/a/c/d1/r$e;-><init>(Lf/h/a/c/d1/r;I)V

    aput-object v5, p3, v2

    aput-boolean v6, p4, v2

    if-nez p2, :cond_8

    iget-object p2, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object p2, p2, v4

    invoke-virtual {p2, p5, p6, v6}, Lf/h/a/c/d1/u;->q(JZ)Z

    move-result v4

    if-nez v4, :cond_7

    iget v4, p2, Lf/h/a/c/d1/u;->p:I

    iget p2, p2, Lf/h/a/c/d1/u;->r:I

    add-int/2addr v4, p2

    if-eqz v4, :cond_7

    const/4 p2, 0x1

    goto :goto_6

    :cond_7
    const/4 p2, 0x0

    :cond_8
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_9
    iget p1, p0, Lf/h/a/c/d1/r;->F:I

    if-nez p1, :cond_c

    iput-boolean v3, p0, Lf/h/a/c/d1/r;->L:Z

    iput-boolean v3, p0, Lf/h/a/c/d1/r;->D:Z

    iget-object p1, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/Loader;->b()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length p2, p1

    const/4 p3, 0x0

    :goto_7
    if-ge p3, p2, :cond_a

    aget-object p4, p1, p3

    invoke-virtual {p4}, Lf/h/a/c/d1/u;->f()V

    add-int/lit8 p3, p3, 0x1

    goto :goto_7

    :cond_a
    iget-object p1, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    iget-object p1, p1, Lcom/google/android/exoplayer2/upstream/Loader;->b:Lcom/google/android/exoplayer2/upstream/Loader$d;

    invoke-static {p1}, Lf/g/j/k/a;->w(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/upstream/Loader$d;->a(Z)V

    goto :goto_a

    :cond_b
    iget-object p1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length p2, p1

    const/4 p3, 0x0

    :goto_8
    if-ge p3, p2, :cond_e

    aget-object p4, p1, p3

    invoke-virtual {p4, v3}, Lf/h/a/c/d1/u;->p(Z)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_8

    :cond_c
    if-eqz p2, :cond_e

    invoke-virtual {p0, p5, p6}, Lf/h/a/c/d1/r;->g(J)J

    move-result-wide p5

    :goto_9
    array-length p1, p3

    if-ge v3, p1, :cond_e

    aget-object p1, p3, v3

    if-eqz p1, :cond_d

    aput-boolean v6, p4, v3

    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_e
    :goto_a
    iput-boolean v6, p0, Lf/h/a/c/d1/r;->C:Z

    return-wide p5
.end method

.method public c(Lcom/google/android/exoplayer2/upstream/Loader$e;JJZ)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    check-cast v1, Lf/h/a/c/d1/r$a;

    iget-object v2, v0, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iget-object v4, v1, Lf/h/a/c/d1/r$a;->j:Lf/h/a/c/h1/k;

    iget-object v3, v1, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    iget-object v5, v3, Lf/h/a/c/h1/v;->c:Landroid/net/Uri;

    iget-object v6, v3, Lf/h/a/c/h1/v;->d:Ljava/util/Map;

    iget-wide v11, v1, Lf/h/a/c/d1/r$a;->i:J

    iget-wide v9, v0, Lf/h/a/c/d1/r;->G:J

    iget-wide v7, v3, Lf/h/a/c/h1/v;->b:J

    new-instance v15, Lf/h/a/c/d1/q$b;

    move-object v3, v15

    move-wide/from16 v16, v7

    move-wide/from16 v7, p2

    move-object/from16 v18, v15

    move-wide v14, v9

    move-wide/from16 v9, p4

    move-wide/from16 p2, v14

    move-wide v13, v11

    move-wide/from16 v11, v16

    invoke-direct/range {v3 .. v12}, Lf/h/a/c/d1/q$b;-><init>(Lf/h/a/c/h1/k;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    new-instance v3, Lf/h/a/c/d1/q$c;

    invoke-virtual {v2, v13, v14}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v13

    move-wide/from16 v4, p2

    invoke-virtual {v2, v4, v5}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v15

    const/4 v8, 0x1

    const/4 v9, -0x1

    const/4 v10, 0x0

    move-object v7, v3

    const/4 v4, 0x0

    move v11, v4

    const/4 v4, 0x0

    move-object v12, v4

    move-object/from16 v4, v18

    invoke-direct/range {v7 .. v16}, Lf/h/a/c/d1/q$c;-><init>(IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    iget-object v5, v2, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/a/c/d1/q$a$a;

    iget-object v7, v6, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object v6, v6, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance v8, Lf/h/a/c/d1/d;

    invoke-direct {v8, v2, v7, v4, v3}, Lf/h/a/c/d1/d;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/q$b;Lf/h/a/c/d1/q$c;)V

    invoke-virtual {v2, v6, v8}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    if-nez p6, :cond_3

    iget-wide v2, v0, Lf/h/a/c/d1/r;->H:J

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    iget-wide v1, v1, Lf/h/a/c/d1/r$a;->k:J

    iput-wide v1, v0, Lf/h/a/c/d1/r;->H:J

    :cond_1
    iget-object v1, v0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_2

    aget-object v5, v1, v4

    invoke-virtual {v5, v3}, Lf/h/a/c/d1/u;->p(Z)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    iget v1, v0, Lf/h/a/c/d1/r;->F:I

    if-lez v1, :cond_3

    iget-object v1, v0, Lf/h/a/c/d1/r;->s:Lf/h/a/c/d1/o$a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v0}, Lf/h/a/c/d1/w$a;->c(Lf/h/a/c/d1/w;)V

    :cond_3
    return-void
.end method

.method public d()J
    .locals 2

    iget v0, p0, Lf/h/a/c/d1/r;->F:I

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lf/h/a/c/d1/r;->q()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public e(Lcom/google/android/exoplayer2/upstream/Loader$e;JJ)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    check-cast v1, Lf/h/a/c/d1/r$a;

    iget-wide v2, v0, Lf/h/a/c/d1/r;->G:J

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    iget-object v2, v0, Lf/h/a/c/d1/r;->t:Lf/h/a/c/a1/q;

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lf/h/a/c/a1/q;->b()Z

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/d1/r;->u()J

    move-result-wide v3

    const-wide/high16 v5, -0x8000000000000000L

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    const-wide/16 v3, 0x0

    goto :goto_0

    :cond_0
    const-wide/16 v5, 0x2710

    add-long/2addr v3, v5

    :goto_0
    iput-wide v3, v0, Lf/h/a/c/d1/r;->G:J

    iget-object v5, v0, Lf/h/a/c/d1/r;->i:Lf/h/a/c/d1/r$c;

    iget-boolean v6, v0, Lf/h/a/c/d1/r;->I:Z

    check-cast v5, Lf/h/a/c/d1/s;

    invoke-virtual {v5, v3, v4, v2, v6}, Lf/h/a/c/d1/s;->i(JZZ)V

    :cond_1
    iget-object v2, v0, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iget-object v4, v1, Lf/h/a/c/d1/r$a;->j:Lf/h/a/c/h1/k;

    iget-object v3, v1, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    iget-object v5, v3, Lf/h/a/c/h1/v;->c:Landroid/net/Uri;

    iget-object v6, v3, Lf/h/a/c/h1/v;->d:Ljava/util/Map;

    iget-wide v11, v1, Lf/h/a/c/d1/r$a;->i:J

    iget-wide v9, v0, Lf/h/a/c/d1/r;->G:J

    iget-wide v7, v3, Lf/h/a/c/h1/v;->b:J

    new-instance v15, Lf/h/a/c/d1/q$b;

    move-object v3, v15

    move-wide/from16 v16, v7

    move-wide/from16 v7, p2

    move-object/from16 v18, v15

    move-wide v14, v9

    move-wide/from16 v9, p4

    move-wide/from16 p2, v14

    move-wide v13, v11

    move-wide/from16 v11, v16

    invoke-direct/range {v3 .. v12}, Lf/h/a/c/d1/q$b;-><init>(Lf/h/a/c/h1/k;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    new-instance v3, Lf/h/a/c/d1/q$c;

    invoke-virtual {v2, v13, v14}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v13

    move-wide/from16 v4, p2

    invoke-virtual {v2, v4, v5}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v15

    const/4 v8, 0x1

    const/4 v9, -0x1

    const/4 v10, 0x0

    move-object v7, v3

    const/4 v4, 0x0

    move v11, v4

    const/4 v4, 0x0

    move-object v12, v4

    move-object/from16 v4, v18

    invoke-direct/range {v7 .. v16}, Lf/h/a/c/d1/q$c;-><init>(IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    iget-object v5, v2, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/a/c/d1/q$a$a;

    iget-object v7, v6, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object v6, v6, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance v8, Lf/h/a/c/d1/f;

    invoke-direct {v8, v2, v7, v4, v3}, Lf/h/a/c/d1/f;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/q$b;Lf/h/a/c/d1/q$c;)V

    invoke-virtual {v2, v6, v8}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    iget-wide v2, v0, Lf/h/a/c/d1/r;->H:J

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_3

    iget-wide v1, v1, Lf/h/a/c/d1/r$a;->k:J

    iput-wide v1, v0, Lf/h/a/c/d1/r;->H:J

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/a/c/d1/r;->N:Z

    iget-object v1, v0, Lf/h/a/c/d1/r;->s:Lf/h/a/c/d1/o$a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v0}, Lf/h/a/c/d1/w$a;->c(Lf/h/a/c/d1/w;)V

    return-void
.end method

.method public f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->y()V

    iget-boolean v0, p0, Lf/h/a/c/d1/r;->N:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lf/h/a/c/d1/r;->y:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Loading finished before preparation is complete."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public g(J)J
    .locals 5

    iget-object v0, p0, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/h/a/c/d1/r$d;->a:Lf/h/a/c/a1/q;

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->c:[Z

    invoke-interface {v1}, Lf/h/a/c/a1/q;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lf/h/a/c/d1/r;->D:Z

    iput-wide p1, p0, Lf/h/a/c/d1/r;->J:J

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->v()Z

    move-result v2

    if-eqz v2, :cond_1

    iput-wide p1, p0, Lf/h/a/c/d1/r;->K:J

    return-wide p1

    :cond_1
    iget v2, p0, Lf/h/a/c/d1/r;->B:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_5

    iget-object v2, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v2, v2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_4

    iget-object v4, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v4, v4, v3

    invoke-virtual {v4, p1, p2, v1}, Lf/h/a/c/d1/u;->q(JZ)Z

    move-result v4

    if-nez v4, :cond_3

    aget-boolean v4, v0, v3

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lf/h/a/c/d1/r;->A:Z

    if-nez v4, :cond_3

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    return-wide p1

    :cond_5
    iput-boolean v1, p0, Lf/h/a/c/d1/r;->L:Z

    iput-wide p1, p0, Lf/h/a/c/d1/r;->K:J

    iput-boolean v1, p0, Lf/h/a/c/d1/r;->N:Z

    iget-object v0, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/Loader;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    iget-object v0, v0, Lcom/google/android/exoplayer2/upstream/Loader;->b:Lcom/google/android/exoplayer2/upstream/Loader$d;

    invoke-static {v0}, Lf/g/j/k/a;->w(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/upstream/Loader$d;->a(Z)V

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/exoplayer2/upstream/Loader;->c:Ljava/io/IOException;

    iget-object v0, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v2, v0

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_7

    aget-object v4, v0, v3

    invoke-virtual {v4, v1}, Lf/h/a/c/d1/u;->p(Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    :goto_4
    return-wide p1
.end method

.method public h(J)Z
    .locals 1

    iget-boolean p1, p0, Lf/h/a/c/d1/r;->N:Z

    const/4 p2, 0x0

    if-nez p1, :cond_3

    iget-object p1, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    iget-object p1, p1, Lcom/google/android/exoplayer2/upstream/Loader;->c:Ljava/io/IOException;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_3

    iget-boolean p1, p0, Lf/h/a/c/d1/r;->L:Z

    if-nez p1, :cond_3

    iget-boolean p1, p0, Lf/h/a/c/d1/r;->y:Z

    if-eqz p1, :cond_1

    iget p1, p0, Lf/h/a/c/d1/r;->F:I

    if-nez p1, :cond_1

    goto :goto_2

    :cond_1
    iget-object p1, p0, Lf/h/a/c/d1/r;->o:Lf/h/a/c/i1/i;

    invoke-virtual {p1}, Lf/h/a/c/i1/i;->a()Z

    move-result p1

    iget-object p2, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/upstream/Loader;->b()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->A()V

    goto :goto_1

    :cond_2
    move v0, p1

    :goto_1
    return v0

    :cond_3
    :goto_2
    return p2
.end method

.method public i()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/Loader;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/d1/r;->o:Lf/h/a/c/i1/i;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, v0, Lf/h/a/c/i1/i;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public j(JLf/h/a/c/r0;)J
    .locals 20

    move-wide/from16 v0, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    iget-object v4, v3, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v4, Lf/h/a/c/d1/r$d;->a:Lf/h/a/c/a1/q;

    invoke-interface {v4}, Lf/h/a/c/a1/q;->b()Z

    move-result v5

    const-wide/16 v6, 0x0

    if-nez v5, :cond_0

    return-wide v6

    :cond_0
    invoke-interface {v4, v0, v1}, Lf/h/a/c/a1/q;->g(J)Lf/h/a/c/a1/q$a;

    move-result-object v4

    iget-object v5, v4, Lf/h/a/c/a1/q$a;->a:Lf/h/a/c/a1/r;

    iget-wide v8, v5, Lf/h/a/c/a1/r;->a:J

    iget-object v4, v4, Lf/h/a/c/a1/q$a;->b:Lf/h/a/c/a1/r;

    iget-wide v4, v4, Lf/h/a/c/a1/r;->a:J

    sget-object v10, Lf/h/a/c/r0;->c:Lf/h/a/c/r0;

    invoke-virtual {v10, v2}, Lf/h/a/c/r0;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    move-wide v4, v0

    goto :goto_4

    :cond_1
    iget-wide v10, v2, Lf/h/a/c/r0;->a:J

    const-wide/high16 v12, -0x8000000000000000L

    sub-long v14, v0, v10

    xor-long/2addr v10, v0

    xor-long v16, v0, v14

    and-long v10, v10, v16

    cmp-long v16, v10, v6

    if-gez v16, :cond_2

    goto :goto_0

    :cond_2
    move-wide v12, v14

    :goto_0
    iget-wide v10, v2, Lf/h/a/c/r0;->b:J

    const-wide v14, 0x7fffffffffffffffL

    add-long v16, v0, v10

    xor-long v18, v0, v16

    xor-long v10, v10, v16

    and-long v10, v10, v18

    cmp-long v2, v10, v6

    if-gez v2, :cond_3

    goto :goto_1

    :cond_3
    move-wide/from16 v14, v16

    :goto_1
    const/4 v2, 0x0

    const/4 v6, 0x1

    cmp-long v7, v12, v8

    if-gtz v7, :cond_4

    cmp-long v7, v8, v14

    if-gtz v7, :cond_4

    const/4 v7, 0x1

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    :goto_2
    cmp-long v10, v12, v4

    if-gtz v10, :cond_5

    cmp-long v10, v4, v14

    if-gtz v10, :cond_5

    const/4 v2, 0x1

    :cond_5
    if-eqz v7, :cond_6

    if-eqz v2, :cond_6

    sub-long v6, v8, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    sub-long v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    cmp-long v2, v6, v0

    if-gtz v2, :cond_9

    goto :goto_3

    :cond_6
    if-eqz v7, :cond_7

    :goto_3
    move-wide v4, v8

    goto :goto_4

    :cond_7
    if-eqz v2, :cond_8

    goto :goto_4

    :cond_8
    move-wide v4, v12

    :cond_9
    :goto_4
    return-wide v4
.end method

.method public k()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/d1/r;->x:Z

    iget-object v0, p0, Lf/h/a/c/d1/r;->r:Landroid/os/Handler;

    iget-object v1, p0, Lf/h/a/c/d1/r;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public l()J
    .locals 6

    iget-boolean v0, p0, Lf/h/a/c/d1/r;->E:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iget-object v1, v0, Lf/h/a/c/d1/q$a;->b:Lf/h/a/c/d1/p$a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/d1/q$a$a;

    iget-object v4, v3, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object v3, v3, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance v5, Lf/h/a/c/d1/g;

    invoke-direct {v5, v0, v4, v1}, Lf/h/a/c/d1/g;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/p$a;)V

    invoke-virtual {v0, v3, v5}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/d1/r;->E:Z

    :cond_1
    iget-boolean v0, p0, Lf/h/a/c/d1/r;->D:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lf/h/a/c/d1/r;->N:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->t()I

    move-result v0

    iget v1, p0, Lf/h/a/c/d1/r;->M:I

    if-le v0, v1, :cond_3

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/d1/r;->D:Z

    iget-wide v0, p0, Lf/h/a/c/d1/r;->J:J

    return-wide v0

    :cond_3
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0
.end method

.method public m(Lf/h/a/c/d1/o$a;J)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/d1/r;->s:Lf/h/a/c/d1/o$a;

    iget-object p1, p0, Lf/h/a/c/d1/r;->o:Lf/h/a/c/i1/i;

    invoke-virtual {p1}, Lf/h/a/c/i1/i;->a()Z

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->A()V

    return-void
.end method

.method public n()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->b:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    return-object v0
.end method

.method public o(Lcom/google/android/exoplayer2/upstream/Loader$e;JJLjava/io/IOException;I)Lcom/google/android/exoplayer2/upstream/Loader$c;
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    check-cast v1, Lf/h/a/c/d1/r$a;

    iget-wide v2, v0, Lf/h/a/c/d1/r;->H:J

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    iget-wide v2, v1, Lf/h/a/c/d1/r$a;->k:J

    iput-wide v2, v0, Lf/h/a/c/d1/r;->H:J

    :cond_0
    iget-object v2, v0, Lf/h/a/c/d1/r;->g:Lf/h/a/c/h1/u;

    iget v7, v0, Lf/h/a/c/d1/r;->B:I

    move-object v6, v2

    check-cast v6, Lf/h/a/c/h1/r;

    move-wide/from16 v8, p4

    move-object/from16 v10, p6

    move/from16 v11, p7

    invoke-virtual/range {v6 .. v11}, Lf/h/a/c/h1/r;->b(IJLjava/io/IOException;I)J

    move-result-wide v2

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v8, 0x1

    const/4 v9, 0x0

    cmp-long v10, v2, v6

    if-nez v10, :cond_1

    sget-object v2, Lcom/google/android/exoplayer2/upstream/Loader;->e:Lcom/google/android/exoplayer2/upstream/Loader$c;

    goto :goto_5

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/d1/r;->t()I

    move-result v10

    iget v11, v0, Lf/h/a/c/d1/r;->M:I

    if-le v10, v11, :cond_2

    const/4 v11, 0x1

    goto :goto_0

    :cond_2
    const/4 v11, 0x0

    :goto_0
    iget-wide v12, v0, Lf/h/a/c/d1/r;->H:J

    cmp-long v14, v12, v4

    if-nez v14, :cond_6

    iget-object v4, v0, Lf/h/a/c/d1/r;->t:Lf/h/a/c/a1/q;

    if-eqz v4, :cond_3

    invoke-interface {v4}, Lf/h/a/c/a1/q;->i()J

    move-result-wide v4

    cmp-long v12, v4, v6

    if-eqz v12, :cond_3

    goto :goto_2

    :cond_3
    iget-boolean v4, v0, Lf/h/a/c/d1/r;->y:Z

    if-eqz v4, :cond_4

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/d1/r;->B()Z

    move-result v4

    if-nez v4, :cond_4

    iput-boolean v8, v0, Lf/h/a/c/d1/r;->L:Z

    const/4 v4, 0x0

    goto :goto_4

    :cond_4
    iget-boolean v4, v0, Lf/h/a/c/d1/r;->y:Z

    iput-boolean v4, v0, Lf/h/a/c/d1/r;->D:Z

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lf/h/a/c/d1/r;->J:J

    iput v9, v0, Lf/h/a/c/d1/r;->M:I

    iget-object v6, v0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v7, v6

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v7, :cond_5

    aget-object v12, v6, v10

    invoke-virtual {v12, v9}, Lf/h/a/c/d1/u;->p(Z)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_5
    iget-object v6, v1, Lf/h/a/c/d1/r$a;->f:Lf/h/a/c/a1/p;

    iput-wide v4, v6, Lf/h/a/c/a1/p;->a:J

    iput-wide v4, v1, Lf/h/a/c/d1/r$a;->i:J

    iput-boolean v8, v1, Lf/h/a/c/d1/r$a;->h:Z

    iput-boolean v9, v1, Lf/h/a/c/d1/r$a;->m:Z

    goto :goto_3

    :cond_6
    :goto_2
    iput v10, v0, Lf/h/a/c/d1/r;->M:I

    :goto_3
    const/4 v4, 0x1

    :goto_4
    if-eqz v4, :cond_7

    invoke-static {v11, v2, v3}, Lcom/google/android/exoplayer2/upstream/Loader;->a(ZJ)Lcom/google/android/exoplayer2/upstream/Loader$c;

    move-result-object v2

    goto :goto_5

    :cond_7
    sget-object v2, Lcom/google/android/exoplayer2/upstream/Loader;->d:Lcom/google/android/exoplayer2/upstream/Loader$c;

    :goto_5
    iget-object v3, v0, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iget-object v11, v1, Lf/h/a/c/d1/r$a;->j:Lf/h/a/c/h1/k;

    iget-object v4, v1, Lf/h/a/c/d1/r$a;->b:Lf/h/a/c/h1/v;

    iget-object v12, v4, Lf/h/a/c/h1/v;->c:Landroid/net/Uri;

    iget-object v13, v4, Lf/h/a/c/h1/v;->d:Ljava/util/Map;

    iget-wide v14, v1, Lf/h/a/c/d1/r$a;->i:J

    iget-wide v6, v0, Lf/h/a/c/d1/r;->G:J

    move-wide/from16 v20, v6

    iget-wide v5, v4, Lf/h/a/c/h1/v;->b:J

    iget v1, v2, Lcom/google/android/exoplayer2/upstream/Loader$c;->a:I

    if-eqz v1, :cond_8

    if-ne v1, v8, :cond_9

    :cond_8
    const/4 v9, 0x1

    :cond_9
    xor-int/lit8 v1, v9, 0x1

    new-instance v4, Lf/h/a/c/d1/q$b;

    move-object v10, v4

    move-wide v7, v14

    move-wide/from16 v14, p2

    move-wide/from16 v16, p4

    move-wide/from16 v18, v5

    invoke-direct/range {v10 .. v19}, Lf/h/a/c/d1/q$b;-><init>(Lf/h/a/c/h1/k;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    new-instance v5, Lf/h/a/c/d1/q$c;

    invoke-virtual {v3, v7, v8}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v6

    move-wide/from16 v8, v20

    invoke-virtual {v3, v8, v9}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v22

    const/4 v15, 0x1

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object v14, v5

    const/4 v8, 0x0

    move/from16 v18, v8

    const/4 v8, 0x0

    move-object/from16 v19, v8

    move-wide/from16 v20, v6

    invoke-direct/range {v14 .. v23}, Lf/h/a/c/d1/q$c;-><init>(IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    iget-object v6, v3, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/a/c/d1/q$a$a;

    iget-object v12, v7, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object v7, v7, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance v8, Lf/h/a/c/d1/b;

    move-object v10, v8

    move-object v11, v3

    move-object v13, v4

    move-object v14, v5

    move-object/from16 v15, p6

    move/from16 v16, v1

    invoke-direct/range {v10 .. v16}, Lf/h/a/c/d1/b;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/q$b;Lf/h/a/c/d1/q$c;Ljava/io/IOException;Z)V

    invoke-virtual {v3, v7, v8}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_6

    :cond_a
    return-object v2
.end method

.method public p(II)Lf/h/a/c/a1/s;
    .locals 1

    new-instance p2, Lf/h/a/c/d1/r$f;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lf/h/a/c/d1/r$f;-><init>(IZ)V

    invoke-virtual {p0, p2}, Lf/h/a/c/d1/r;->z(Lf/h/a/c/d1/r$f;)Lf/h/a/c/a1/s;

    move-result-object p1

    return-object p1
.end method

.method public q()J
    .locals 11

    iget-object v0, p0, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->c:[Z

    iget-boolean v1, p0, Lf/h/a/c/d1/r;->N:Z

    const-wide/high16 v2, -0x8000000000000000L

    if-eqz v1, :cond_0

    return-wide v2

    :cond_0
    invoke-virtual {p0}, Lf/h/a/c/d1/r;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v0, p0, Lf/h/a/c/d1/r;->K:J

    return-wide v0

    :cond_1
    iget-boolean v1, p0, Lf/h/a/c/d1/r;->A:Z

    const-wide v4, 0x7fffffffffffffffL

    if-eqz v1, :cond_3

    iget-object v1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v1, v1

    const/4 v6, 0x0

    move-wide v7, v4

    :goto_0
    if-ge v6, v1, :cond_4

    aget-boolean v9, v0, v6

    if-eqz v9, :cond_2

    iget-object v9, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v9, v9, v6

    monitor-enter v9

    :try_start_0
    iget-boolean v10, v9, Lf/h/a/c/d1/u;->u:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v9

    if-nez v10, :cond_2

    iget-object v9, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Lf/h/a/c/d1/u;->h()J

    move-result-wide v9

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0

    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    move-wide v7, v4

    :cond_4
    cmp-long v0, v7, v4

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->u()J

    move-result-wide v7

    :cond_5
    cmp-long v0, v7, v2

    if-nez v0, :cond_6

    iget-wide v7, p0, Lf/h/a/c/d1/r;->J:J

    :cond_6
    return-wide v7
.end method

.method public r(JZ)V
    .locals 14

    move-object v1, p0

    invoke-virtual {p0}, Lf/h/a/c/d1/r;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, v1, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->d:[Z

    iget-object v2, v1, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v2, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_5

    iget-object v4, v1, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v4, v4, v3

    aget-boolean v5, v0, v3

    iget-object v11, v4, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    monitor-enter v4

    :try_start_0
    iget v6, v4, Lf/h/a/c/d1/u;->o:I

    const-wide/16 v12, -0x1

    if-eqz v6, :cond_4

    iget-object v7, v4, Lf/h/a/c/d1/u;->l:[J

    iget v8, v4, Lf/h/a/c/d1/u;->q:I

    aget-wide v9, v7, v8

    cmp-long v7, p1, v9

    if-gez v7, :cond_1

    goto :goto_2

    :cond_1
    if-eqz v5, :cond_2

    iget v5, v4, Lf/h/a/c/d1/u;->r:I

    if-eq v5, v6, :cond_2

    add-int/lit8 v5, v5, 0x1

    move v7, v5

    goto :goto_1

    :cond_2
    move v7, v6

    :goto_1
    move-object v5, v4

    move v6, v8

    move-wide v8, p1

    move/from16 v10, p3

    invoke-virtual/range {v5 .. v10}, Lf/h/a/c/d1/u;->g(IIJZ)I

    move-result v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v6, -0x1

    if-ne v5, v6, :cond_3

    monitor-exit v4

    goto :goto_3

    :cond_3
    :try_start_1
    invoke-virtual {v4, v5}, Lf/h/a/c/d1/u;->e(I)J

    move-result-wide v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v4

    goto :goto_3

    :cond_4
    :goto_2
    monitor-exit v4

    :goto_3
    invoke-virtual {v11, v12, v13}, Lf/h/a/c/d1/t;->a(J)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_5
    return-void
.end method

.method public s(J)V
    .locals 0

    return-void
.end method

.method public final t()I
    .locals 6

    iget-object v0, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v4, v0, v2

    iget v5, v4, Lf/h/a/c/d1/u;->p:I

    iget v4, v4, Lf/h/a/c/d1/u;->o:I

    add-int/2addr v5, v4

    add-int/2addr v3, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v3
.end method

.method public final u()J
    .locals 7

    iget-object v0, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v1, v0

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    aget-object v5, v0, v4

    invoke-virtual {v5}, Lf/h/a/c/d1/u;->h()J

    move-result-wide v5

    invoke-static {v2, v3, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    return-wide v2
.end method

.method public final v()Z
    .locals 5

    iget-wide v0, p0, Lf/h/a/c/d1/r;->K:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final w(I)V
    .locals 14

    iget-object v0, p0, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/h/a/c/d1/r$d;->e:[Z

    aget-boolean v2, v1, p1

    if-nez v2, :cond_1

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->b:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/TrackGroupArray;->e:[Lcom/google/android/exoplayer2/source/TrackGroup;

    aget-object v0, v0, p1

    const/4 v2, 0x0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/TrackGroup;->e:[Lcom/google/android/exoplayer2/Format;

    aget-object v6, v0, v2

    iget-object v0, p0, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iget-object v2, v6, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {v2}, Lf/h/a/c/i1/o;->e(Ljava/lang/String;)I

    move-result v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-wide v2, p0, Lf/h/a/c/d1/r;->J:J

    new-instance v13, Lf/h/a/c/d1/q$c;

    invoke-virtual {v0, v2, v3}, Lf/h/a/c/d1/q$a;->a(J)J

    move-result-wide v9

    const/4 v4, 0x1

    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    move-object v3, v13

    invoke-direct/range {v3 .. v12}, Lf/h/a/c/d1/q$c;-><init>(IILcom/google/android/exoplayer2/Format;ILjava/lang/Object;JJ)V

    iget-object v2, v0, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/a/c/d1/q$a$a;

    iget-object v4, v3, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object v3, v3, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance v5, Lf/h/a/c/d1/c;

    invoke-direct {v5, v0, v4, v13}, Lf/h/a/c/d1/c;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/q$c;)V

    invoke-virtual {v0, v3, v5}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    aput-boolean v0, v1, p1

    :cond_1
    return-void
.end method

.method public final x(I)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/d1/r;->z:Lf/h/a/c/d1/r$d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lf/h/a/c/d1/r$d;->c:[Z

    iget-boolean v1, p0, Lf/h/a/c/d1/r;->L:Z

    if-eqz v1, :cond_2

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object p1, v0, p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lf/h/a/c/d1/u;->m(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lf/h/a/c/d1/r;->K:J

    iput-boolean v0, p0, Lf/h/a/c/d1/r;->L:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/d1/r;->D:Z

    iput-wide v1, p0, Lf/h/a/c/d1/r;->J:J

    iput v0, p0, Lf/h/a/c/d1/r;->M:I

    iget-object p1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    invoke-virtual {v3, v0}, Lf/h/a/c/d1/u;->p(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/h/a/c/d1/r;->s:Lf/h/a/c/d1/o$a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, p0}, Lf/h/a/c/d1/w$a;->c(Lf/h/a/c/d1/w;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public y()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    iget-object v1, p0, Lf/h/a/c/d1/r;->g:Lf/h/a/c/h1/u;

    iget v2, p0, Lf/h/a/c/d1/r;->B:I

    check-cast v1, Lf/h/a/c/h1/r;

    invoke-virtual {v1, v2}, Lf/h/a/c/h1/r;->a(I)I

    move-result v1

    iget-object v2, v0, Lcom/google/android/exoplayer2/upstream/Loader;->c:Ljava/io/IOException;

    if-nez v2, :cond_3

    iget-object v0, v0, Lcom/google/android/exoplayer2/upstream/Loader;->b:Lcom/google/android/exoplayer2/upstream/Loader$d;

    if-eqz v0, :cond_2

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_0

    iget v1, v0, Lcom/google/android/exoplayer2/upstream/Loader$d;->d:I

    :cond_0
    iget-object v2, v0, Lcom/google/android/exoplayer2/upstream/Loader$d;->h:Ljava/io/IOException;

    if-eqz v2, :cond_2

    iget v0, v0, Lcom/google/android/exoplayer2/upstream/Loader$d;->i:I

    if-gt v0, v1, :cond_1

    goto :goto_0

    :cond_1
    throw v2

    :cond_2
    :goto_0
    return-void

    :cond_3
    throw v2
.end method

.method public final z(Lf/h/a/c/d1/r$f;)Lf/h/a/c/a1/s;
    .locals 4

    iget-object v0, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lf/h/a/c/d1/r;->w:[Lf/h/a/c/d1/r$f;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Lf/h/a/c/d1/r$f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object p1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object p1, p1, v1

    return-object p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lf/h/a/c/d1/u;

    iget-object v2, p0, Lf/h/a/c/d1/r;->j:Lf/h/a/c/h1/d;

    iget-object v3, p0, Lf/h/a/c/d1/r;->f:Lf/h/a/c/z0/b;

    invoke-direct {v1, v2, v3}, Lf/h/a/c/d1/u;-><init>(Lf/h/a/c/h1/d;Lf/h/a/c/z0/b;)V

    iput-object p0, v1, Lf/h/a/c/d1/u;->d:Lf/h/a/c/d1/u$b;

    iget-object v2, p0, Lf/h/a/c/d1/r;->w:[Lf/h/a/c/d1/r$f;

    add-int/lit8 v3, v0, 0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lf/h/a/c/d1/r$f;

    aput-object p1, v2, v0

    sget p1, Lf/h/a/c/i1/a0;->a:I

    iput-object v2, p0, Lf/h/a/c/d1/r;->w:[Lf/h/a/c/d1/r$f;

    iget-object p1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    invoke-static {p1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lf/h/a/c/d1/u;

    aput-object v1, p1, v0

    iput-object p1, p0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    return-object v1
.end method
