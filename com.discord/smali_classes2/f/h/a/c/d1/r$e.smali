.class public final Lf/h/a/c/d1/r$e;
.super Ljava/lang/Object;
.source "ProgressiveMediaPeriod.java"

# interfaces
.implements Lf/h/a/c/d1/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/d1/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "e"
.end annotation


# instance fields
.field public final a:I

.field public final synthetic b:Lf/h/a/c/d1/r;


# direct methods
.method public constructor <init>(Lf/h/a/c/d1/r;I)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/d1/r$e;->b:Lf/h/a/c/d1/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lf/h/a/c/d1/r$e;->a:I

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 3

    iget-object v0, p0, Lf/h/a/c/d1/r$e;->b:Lf/h/a/c/d1/r;

    iget v1, p0, Lf/h/a/c/d1/r$e;->a:I

    invoke-virtual {v0}, Lf/h/a/c/d1/r;->B()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v1, v2, v1

    iget-boolean v0, v0, Lf/h/a/c/d1/r;->N:Z

    invoke-virtual {v1, v0}, Lf/h/a/c/d1/u;->m(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b(Lf/h/a/c/d0;Lf/h/a/c/y0/e;Z)I
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    iget-object v3, v1, Lf/h/a/c/d1/r$e;->b:Lf/h/a/c/d1/r;

    iget v4, v1, Lf/h/a/c/d1/r$e;->a:I

    invoke-virtual {v3}, Lf/h/a/c/d1/r;->B()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v6, -0x3

    goto/16 :goto_15

    :cond_0
    invoke-virtual {v3, v4}, Lf/h/a/c/d1/r;->w(I)V

    iget-object v5, v3, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v5, v5, v4

    iget-boolean v7, v3, Lf/h/a/c/d1/r;->N:Z

    iget-wide v8, v3, Lf/h/a/c/d1/r;->J:J

    iget-object v10, v5, Lf/h/a/c/d1/u;->b:Lf/h/a/c/d1/u$a;

    monitor-enter v5

    const/4 v12, -0x1

    :goto_0
    :try_start_0
    invoke-virtual {v5}, Lf/h/a/c/d1/u;->l()Z

    move-result v13

    const/4 v14, 0x2

    const/4 v15, 0x1

    if-eqz v13, :cond_7

    iget v12, v5, Lf/h/a/c/d1/u;->r:I

    invoke-virtual {v5, v12}, Lf/h/a/c/d1/u;->j(I)I

    move-result v12

    iget-object v6, v5, Lf/h/a/c/d1/u;->l:[J

    aget-wide v17, v6, v12

    cmp-long v6, v17, v8

    if-gez v6, :cond_7

    iget-object v6, v5, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    aget-object v6, v6, v12

    iget-object v6, v6, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    sget-object v17, Lf/h/a/c/i1/o;->a:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v6, :cond_1

    goto :goto_3

    :cond_1
    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v17

    sparse-switch v17, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string v11, "audio/mpeg"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_1

    :cond_2
    const/4 v6, 0x3

    goto :goto_2

    :sswitch_1
    const-string v11, "audio/mp4a-latm"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    goto :goto_1

    :cond_3
    const/4 v6, 0x2

    goto :goto_2

    :sswitch_2
    const-string v11, "audio/mpeg-L2"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    goto :goto_1

    :cond_4
    const/4 v6, 0x1

    goto :goto_2

    :sswitch_3
    const-string v11, "audio/mpeg-L1"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    :goto_1
    const/4 v6, -0x1

    :goto_2
    if-eqz v6, :cond_6

    if-eq v6, v15, :cond_6

    if-eq v6, v14, :cond_6

    const/4 v11, 0x3

    if-eq v6, v11, :cond_6

    :goto_3
    const/4 v6, 0x0

    goto :goto_4

    :cond_6
    const/4 v6, 0x1

    :goto_4
    if-eqz v6, :cond_7

    :try_start_1
    iget v6, v5, Lf/h/a/c/d1/u;->r:I

    add-int/2addr v6, v15

    iput v6, v5, Lf/h/a/c/d1/u;->r:I

    goto :goto_0

    :cond_7
    const/4 v6, 0x4

    const/4 v11, -0x4

    if-nez v13, :cond_c

    if-nez v7, :cond_b

    iget-boolean v7, v5, Lf/h/a/c/d1/u;->u:Z

    if-eqz v7, :cond_8

    goto :goto_5

    :cond_8
    iget-object v7, v5, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;

    if-eqz v7, :cond_a

    if-nez p3, :cond_9

    iget-object v8, v5, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;

    if-eq v7, v8, :cond_a

    :cond_9
    invoke-virtual {v5, v7, v0}, Lf/h/a/c/d1/u;->o(Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d0;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v5

    goto :goto_a

    :cond_a
    monitor-exit v5

    goto :goto_6

    :cond_b
    :goto_5
    :try_start_2
    invoke-virtual {v2, v6}, Lf/h/a/c/y0/a;->setFlags(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v5

    goto :goto_8

    :cond_c
    if-nez p3, :cond_12

    :try_start_3
    iget-object v7, v5, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    aget-object v7, v7, v12

    iget-object v13, v5, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;

    if-eq v7, v13, :cond_d

    goto :goto_9

    :cond_d
    invoke-virtual {v5, v12}, Lf/h/a/c/d1/u;->n(I)Z

    move-result v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_e

    monitor-exit v5

    :goto_6
    const/4 v0, -0x3

    goto :goto_b

    :cond_e
    :try_start_4
    iget-object v0, v5, Lf/h/a/c/d1/u;->k:[I

    aget v0, v0, v12

    invoke-virtual {v2, v0}, Lf/h/a/c/y0/a;->setFlags(I)V

    iget-object v0, v5, Lf/h/a/c/d1/u;->l:[J

    aget-wide v6, v0, v12

    iput-wide v6, v2, Lf/h/a/c/y0/e;->f:J

    cmp-long v0, v6, v8

    if-gez v0, :cond_f

    const/high16 v0, -0x80000000

    invoke-virtual {v2, v0}, Lf/h/a/c/y0/a;->addFlag(I)V

    :cond_f
    iget-object v0, v2, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_10

    iget v0, v2, Lf/h/a/c/y0/e;->h:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    goto :goto_7

    :cond_10
    const/4 v0, 0x0

    :goto_7
    if-eqz v0, :cond_11

    monitor-exit v5

    goto :goto_8

    :cond_11
    :try_start_5
    iget-object v0, v5, Lf/h/a/c/d1/u;->j:[I

    aget v0, v0, v12

    iput v0, v10, Lf/h/a/c/d1/u$a;->a:I

    iget-object v0, v5, Lf/h/a/c/d1/u;->i:[J

    aget-wide v6, v0, v12

    iput-wide v6, v10, Lf/h/a/c/d1/u$a;->b:J

    iget-object v0, v5, Lf/h/a/c/d1/u;->m:[Lf/h/a/c/a1/s$a;

    aget-object v0, v0, v12

    iput-object v0, v10, Lf/h/a/c/d1/u$a;->c:Lf/h/a/c/a1/s$a;

    iget v0, v5, Lf/h/a/c/d1/u;->r:I

    add-int/2addr v0, v15

    iput v0, v5, Lf/h/a/c/d1/u;->r:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit v5

    :goto_8
    const/4 v0, -0x4

    goto :goto_b

    :cond_12
    :goto_9
    :try_start_6
    iget-object v6, v5, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    aget-object v6, v6, v12

    invoke-virtual {v5, v6, v0}, Lf/h/a/c/d1/u;->o(Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d0;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    monitor-exit v5

    :goto_a
    const/4 v0, -0x5

    :goto_b
    if-ne v0, v11, :cond_22

    invoke-virtual/range {p2 .. p2}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result v6

    if-nez v6, :cond_22

    iget-object v6, v2, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    if-nez v6, :cond_13

    iget v6, v2, Lf/h/a/c/y0/e;->h:I

    if-nez v6, :cond_13

    const/4 v6, 0x1

    goto :goto_c

    :cond_13
    const/4 v6, 0x0

    :goto_c
    if-nez v6, :cond_22

    iget-object v6, v5, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    iget-object v5, v5, Lf/h/a/c/d1/u;->b:Lf/h/a/c/d1/u$a;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-virtual {v2, v7}, Lf/h/a/c/y0/a;->getFlag(I)Z

    move-result v7

    if-eqz v7, :cond_1e

    iget-wide v7, v5, Lf/h/a/c/d1/u$a;->b:J

    iget-object v9, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v9, v15}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v9, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    iget-object v9, v9, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v6, v7, v8, v9, v15}, Lf/h/a/c/d1/t;->e(J[BI)V

    const-wide/16 v9, 0x1

    add-long/2addr v7, v9

    iget-object v9, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    iget-object v9, v9, Lf/h/a/c/i1/r;->a:[B

    const/4 v10, 0x0

    aget-byte v9, v9, v10

    and-int/lit16 v10, v9, 0x80

    if-eqz v10, :cond_14

    const/4 v10, 0x1

    goto :goto_d

    :cond_14
    const/4 v10, 0x0

    :goto_d
    and-int/lit8 v9, v9, 0x7f

    iget-object v11, v2, Lf/h/a/c/y0/e;->d:Lf/h/a/c/y0/b;

    iget-object v12, v11, Lf/h/a/c/y0/b;->a:[B

    if-nez v12, :cond_15

    const/16 v12, 0x10

    new-array v12, v12, [B

    iput-object v12, v11, Lf/h/a/c/y0/b;->a:[B

    goto :goto_e

    :cond_15
    const/4 v13, 0x0

    invoke-static {v12, v13}, Ljava/util/Arrays;->fill([BB)V

    :goto_e
    iget-object v12, v11, Lf/h/a/c/y0/b;->a:[B

    invoke-virtual {v6, v7, v8, v12, v9}, Lf/h/a/c/d1/t;->e(J[BI)V

    int-to-long v12, v9

    add-long/2addr v7, v12

    if-eqz v10, :cond_16

    iget-object v9, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v9, v14}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v9, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    iget-object v9, v9, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v6, v7, v8, v9, v14}, Lf/h/a/c/d1/t;->e(J[BI)V

    const-wide/16 v12, 0x2

    add-long/2addr v7, v12

    iget-object v9, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v9}, Lf/h/a/c/i1/r;->v()I

    move-result v15

    :cond_16
    iget-object v9, v11, Lf/h/a/c/y0/b;->b:[I

    if-eqz v9, :cond_17

    array-length v12, v9

    if-ge v12, v15, :cond_18

    :cond_17
    new-array v9, v15, [I

    :cond_18
    iget-object v12, v11, Lf/h/a/c/y0/b;->c:[I

    if-eqz v12, :cond_19

    array-length v13, v12

    if-ge v13, v15, :cond_1a

    :cond_19
    new-array v12, v15, [I

    :cond_1a
    if-eqz v10, :cond_1c

    mul-int/lit8 v10, v15, 0x6

    iget-object v13, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v13, v10}, Lf/h/a/c/i1/r;->y(I)V

    iget-object v13, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    iget-object v13, v13, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v6, v7, v8, v13, v10}, Lf/h/a/c/d1/t;->e(J[BI)V

    int-to-long v13, v10

    add-long/2addr v7, v13

    iget-object v10, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Lf/h/a/c/i1/r;->C(I)V

    const/4 v10, 0x0

    :goto_f
    if-ge v10, v15, :cond_1b

    iget-object v13, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->v()I

    move-result v13

    aput v13, v9, v10

    iget-object v13, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v13}, Lf/h/a/c/i1/r;->t()I

    move-result v13

    aput v13, v12, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_f

    :cond_1b
    move-object v14, v11

    goto :goto_10

    :cond_1c
    const/4 v10, 0x0

    aput v10, v9, v10

    iget v13, v5, Lf/h/a/c/d1/u$a;->a:I

    move-object v14, v11

    iget-wide v10, v5, Lf/h/a/c/d1/u$a;->b:J

    sub-long v10, v7, v10

    long-to-int v11, v10

    sub-int/2addr v13, v11

    const/4 v10, 0x0

    aput v13, v12, v10

    :goto_10
    iget-object v10, v5, Lf/h/a/c/d1/u$a;->c:Lf/h/a/c/a1/s$a;

    iget-object v11, v10, Lf/h/a/c/a1/s$a;->b:[B

    move-object v13, v14

    iget-object v14, v13, Lf/h/a/c/y0/b;->a:[B

    iget v1, v10, Lf/h/a/c/a1/s$a;->a:I

    move-object/from16 v16, v3

    iget v3, v10, Lf/h/a/c/a1/s$a;->c:I

    iget v10, v10, Lf/h/a/c/a1/s$a;->d:I

    iput-object v9, v13, Lf/h/a/c/y0/b;->b:[I

    iput-object v12, v13, Lf/h/a/c/y0/b;->c:[I

    iput-object v14, v13, Lf/h/a/c/y0/b;->a:[B

    move/from16 v17, v4

    iget-object v4, v13, Lf/h/a/c/y0/b;->d:Landroid/media/MediaCodec$CryptoInfo;

    iput v15, v4, Landroid/media/MediaCodec$CryptoInfo;->numSubSamples:I

    iput-object v9, v4, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    iput-object v12, v4, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfEncryptedData:[I

    iput-object v11, v4, Landroid/media/MediaCodec$CryptoInfo;->key:[B

    iput-object v14, v4, Landroid/media/MediaCodec$CryptoInfo;->iv:[B

    iput v1, v4, Landroid/media/MediaCodec$CryptoInfo;->mode:I

    sget v1, Lf/h/a/c/i1/a0;->a:I

    const/16 v4, 0x18

    if-lt v1, v4, :cond_1d

    iget-object v1, v13, Lf/h/a/c/y0/b;->e:Lf/h/a/c/y0/b$b;

    iget-object v4, v1, Lf/h/a/c/y0/b$b;->b:Landroid/media/MediaCodec$CryptoInfo$Pattern;

    invoke-virtual {v4, v3, v10}, Landroid/media/MediaCodec$CryptoInfo$Pattern;->set(II)V

    iget-object v3, v1, Lf/h/a/c/y0/b$b;->a:Landroid/media/MediaCodec$CryptoInfo;

    iget-object v1, v1, Lf/h/a/c/y0/b$b;->b:Landroid/media/MediaCodec$CryptoInfo$Pattern;

    invoke-virtual {v3, v1}, Landroid/media/MediaCodec$CryptoInfo;->setPattern(Landroid/media/MediaCodec$CryptoInfo$Pattern;)V

    :cond_1d
    iget-wide v3, v5, Lf/h/a/c/d1/u$a;->b:J

    sub-long/2addr v7, v3

    long-to-int v1, v7

    int-to-long v7, v1

    add-long/2addr v3, v7

    iput-wide v3, v5, Lf/h/a/c/d1/u$a;->b:J

    iget v3, v5, Lf/h/a/c/d1/u$a;->a:I

    sub-int/2addr v3, v1

    iput v3, v5, Lf/h/a/c/d1/u$a;->a:I

    goto :goto_11

    :cond_1e
    move-object/from16 v16, v3

    move/from16 v17, v4

    :goto_11
    invoke-virtual/range {p2 .. p2}, Lf/h/a/c/y0/a;->hasSupplementalData()Z

    move-result v1

    if-eqz v1, :cond_21

    iget-object v1, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/r;->y(I)V

    iget-wide v7, v5, Lf/h/a/c/d1/u$a;->b:J

    iget-object v1, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    iget-object v1, v1, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v6, v7, v8, v1, v3}, Lf/h/a/c/d1/t;->e(J[BI)V

    iget-object v1, v6, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->t()I

    move-result v1

    iget-wide v7, v5, Lf/h/a/c/d1/u$a;->b:J

    const-wide/16 v9, 0x4

    add-long/2addr v7, v9

    iput-wide v7, v5, Lf/h/a/c/d1/u$a;->b:J

    iget v4, v5, Lf/h/a/c/d1/u$a;->a:I

    sub-int/2addr v4, v3

    iput v4, v5, Lf/h/a/c/d1/u$a;->a:I

    invoke-virtual {v2, v1}, Lf/h/a/c/y0/e;->k(I)V

    iget-wide v3, v5, Lf/h/a/c/d1/u$a;->b:J

    iget-object v7, v2, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v3, v4, v7, v1}, Lf/h/a/c/d1/t;->d(JLjava/nio/ByteBuffer;I)V

    iget-wide v3, v5, Lf/h/a/c/d1/u$a;->b:J

    int-to-long v7, v1

    add-long/2addr v3, v7

    iput-wide v3, v5, Lf/h/a/c/d1/u$a;->b:J

    iget v3, v5, Lf/h/a/c/d1/u$a;->a:I

    sub-int/2addr v3, v1

    iput v3, v5, Lf/h/a/c/d1/u$a;->a:I

    iget-object v1, v2, Lf/h/a/c/y0/e;->g:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ge v1, v3, :cond_1f

    goto :goto_12

    :cond_1f
    iget-object v1, v2, Lf/h/a/c/y0/e;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_13

    :cond_20
    :goto_12
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, v2, Lf/h/a/c/y0/e;->g:Ljava/nio/ByteBuffer;

    :goto_13
    iget-wide v3, v5, Lf/h/a/c/d1/u$a;->b:J

    iget-object v1, v2, Lf/h/a/c/y0/e;->g:Ljava/nio/ByteBuffer;

    iget v2, v5, Lf/h/a/c/d1/u$a;->a:I

    invoke-virtual {v6, v3, v4, v1, v2}, Lf/h/a/c/d1/t;->d(JLjava/nio/ByteBuffer;I)V

    goto :goto_14

    :cond_21
    iget v1, v5, Lf/h/a/c/d1/u$a;->a:I

    invoke-virtual {v2, v1}, Lf/h/a/c/y0/e;->k(I)V

    iget-wide v3, v5, Lf/h/a/c/d1/u$a;->b:J

    iget-object v1, v2, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    iget v2, v5, Lf/h/a/c/d1/u$a;->a:I

    invoke-virtual {v6, v3, v4, v1, v2}, Lf/h/a/c/d1/t;->d(JLjava/nio/ByteBuffer;I)V

    goto :goto_14

    :cond_22
    move-object/from16 v16, v3

    move/from16 v17, v4

    :goto_14
    const/4 v1, -0x3

    if-ne v0, v1, :cond_23

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v1, v2}, Lf/h/a/c/d1/r;->x(I)V

    :cond_23
    move v6, v0

    :goto_15
    return v6

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :sswitch_data_0
    .sparse-switch
        -0x19cc928c -> :sswitch_3
        -0x19cc928b -> :sswitch_2
        -0x3313c2e -> :sswitch_1
        0x59b1e81e -> :sswitch_0
    .end sparse-switch
.end method

.method public c()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/d1/r$e;->b:Lf/h/a/c/d1/r;

    iget v1, p0, Lf/h/a/c/d1/r$e;->a:I

    iget-object v2, v0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v1, v2, v1

    iget-object v2, v1, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/google/android/exoplayer2/drm/DrmSession;->getState()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/drm/DrmSession;->c()Lcom/google/android/exoplayer2/drm/DrmSession$DrmSessionException;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {v0}, Lf/h/a/c/d1/r;->y()V

    return-void
.end method

.method public d(J)I
    .locals 10

    iget-object v0, p0, Lf/h/a/c/d1/r$e;->b:Lf/h/a/c/d1/r;

    iget v1, p0, Lf/h/a/c/d1/r$e;->a:I

    invoke-virtual {v0}, Lf/h/a/c/d1/r;->B()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {v0, v1}, Lf/h/a/c/d1/r;->w(I)V

    iget-object v2, v0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    aget-object v2, v2, v1

    iget-boolean v4, v0, Lf/h/a/c/d1/r;->N:Z

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lf/h/a/c/d1/u;->h()J

    move-result-wide v4

    cmp-long v6, p1, v4

    if-lez v6, :cond_1

    monitor-enter v2

    :try_start_0
    iget p1, v2, Lf/h/a/c/d1/u;->o:I

    iget p2, v2, Lf/h/a/c/d1/u;->r:I

    sub-int p2, p1, p2

    iput p1, v2, Lf/h/a/c/d1/u;->r:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    move v3, p2

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1

    :cond_1
    monitor-enter v2

    :try_start_1
    iget v4, v2, Lf/h/a/c/d1/u;->r:I

    invoke-virtual {v2, v4}, Lf/h/a/c/d1/u;->j(I)I

    move-result v5

    invoke-virtual {v2}, Lf/h/a/c/d1/u;->l()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v2, Lf/h/a/c/d1/u;->l:[J

    aget-wide v6, v4, v5

    cmp-long v4, p1, v6

    if-gez v4, :cond_2

    goto :goto_0

    :cond_2
    iget v4, v2, Lf/h/a/c/d1/u;->o:I

    iget v6, v2, Lf/h/a/c/d1/u;->r:I

    sub-int v6, v4, v6

    const/4 v9, 0x1

    move-object v4, v2

    move-wide v7, p1

    invoke-virtual/range {v4 .. v9}, Lf/h/a/c/d1/u;->g(IIJZ)I

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 p2, -0x1

    if-ne p1, p2, :cond_3

    monitor-exit v2

    goto :goto_1

    :cond_3
    :try_start_2
    iget p2, v2, Lf/h/a/c/d1/u;->r:I

    add-int/2addr p2, p1

    iput p2, v2, Lf/h/a/c/d1/u;->r:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v2

    move v3, p1

    goto :goto_1

    :cond_4
    :goto_0
    monitor-exit v2

    :goto_1
    if-nez v3, :cond_5

    invoke-virtual {v0, v1}, Lf/h/a/c/d1/r;->x(I)V

    :cond_5
    :goto_2
    return v3

    :catchall_1
    move-exception p1

    monitor-exit v2

    throw p1
.end method
