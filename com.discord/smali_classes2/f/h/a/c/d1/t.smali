.class public Lf/h/a/c/d1/t;
.super Ljava/lang/Object;
.source "SampleDataQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/d1/t$a;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/h1/d;

.field public final b:I

.field public final c:Lf/h/a/c/i1/r;

.field public d:Lf/h/a/c/d1/t$a;

.field public e:Lf/h/a/c/d1/t$a;

.field public f:Lf/h/a/c/d1/t$a;

.field public g:J


# direct methods
.method public constructor <init>(Lf/h/a/c/h1/d;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/t;->a:Lf/h/a/c/h1/d;

    check-cast p1, Lf/h/a/c/h1/l;

    iget p1, p1, Lf/h/a/c/h1/l;->b:I

    iput p1, p0, Lf/h/a/c/d1/t;->b:I

    new-instance v0, Lf/h/a/c/i1/r;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lf/h/a/c/i1/r;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/d1/t;->c:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/d1/t$a;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2, p1}, Lf/h/a/c/d1/t$a;-><init>(JI)V

    iput-object v0, p0, Lf/h/a/c/d1/t;->d:Lf/h/a/c/d1/t$a;

    iput-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iput-object v0, p0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 4

    const-wide/16 v0, -0x1

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    return-void

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/h/a/c/d1/t;->d:Lf/h/a/c/d1/t$a;

    iget-wide v1, v0, Lf/h/a/c/d1/t$a;->b:J

    cmp-long v3, p1, v1

    if-ltz v3, :cond_1

    iget-object v1, p0, Lf/h/a/c/d1/t;->a:Lf/h/a/c/h1/d;

    iget-object v0, v0, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    check-cast v1, Lf/h/a/c/h1/l;

    monitor-enter v1

    :try_start_0
    iget-object v2, v1, Lf/h/a/c/h1/l;->c:[Lf/h/a/c/h1/c;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lf/h/a/c/h1/l;->a([Lf/h/a/c/h1/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iget-object v0, p0, Lf/h/a/c/d1/t;->d:Lf/h/a/c/d1/t$a;

    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    iget-object v2, v0, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object v1, v0, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object v2, p0, Lf/h/a/c/d1/t;->d:Lf/h/a/c/d1/t$a;

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_1
    iget-object p1, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-wide p1, p1, Lf/h/a/c/d1/t$a;->a:J

    iget-wide v1, v0, Lf/h/a/c/d1/t$a;->a:J

    cmp-long v3, p1, v1

    if-gez v3, :cond_2

    iput-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    :cond_2
    return-void
.end method

.method public final b(I)V
    .locals 5

    iget-wide v0, p0, Lf/h/a/c/d1/t;->g:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lf/h/a/c/d1/t;->g:J

    iget-object p1, p0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iget-wide v2, p1, Lf/h/a/c/d1/t$a;->b:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object p1, p1, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object p1, p0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    :cond_0
    return-void
.end method

.method public final c(I)I
    .locals 8

    iget-object v0, p0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iget-boolean v1, v0, Lf/h/a/c/d1/t$a;->c:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/d1/t;->a:Lf/h/a/c/h1/d;

    check-cast v1, Lf/h/a/c/h1/l;

    monitor-enter v1

    :try_start_0
    iget v2, v1, Lf/h/a/c/h1/l;->e:I

    const/4 v3, 0x1

    add-int/2addr v2, v3

    iput v2, v1, Lf/h/a/c/h1/l;->e:I

    iget v2, v1, Lf/h/a/c/h1/l;->f:I

    if-lez v2, :cond_0

    iget-object v4, v1, Lf/h/a/c/h1/l;->g:[Lf/h/a/c/h1/c;

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lf/h/a/c/h1/l;->f:I

    aget-object v5, v4, v2

    const/4 v6, 0x0

    aput-object v6, v4, v2

    goto :goto_0

    :cond_0
    new-instance v5, Lf/h/a/c/h1/c;

    iget v2, v1, Lf/h/a/c/h1/l;->b:I

    new-array v2, v2, [B

    const/4 v4, 0x0

    invoke-direct {v5, v2, v4}, Lf/h/a/c/h1/c;-><init>([BI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    new-instance v1, Lf/h/a/c/d1/t$a;

    iget-object v2, p0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iget-wide v6, v2, Lf/h/a/c/d1/t$a;->b:J

    iget v2, p0, Lf/h/a/c/d1/t;->b:I

    invoke-direct {v1, v6, v7, v2}, Lf/h/a/c/d1/t$a;-><init>(JI)V

    iput-object v5, v0, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    iput-object v1, v0, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-boolean v3, v0, Lf/h/a/c/d1/t$a;->c:Z

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_1
    :goto_1
    iget-object v0, p0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iget-wide v0, v0, Lf/h/a/c/d1/t$a;->b:J

    iget-wide v2, p0, Lf/h/a/c/d1/t;->g:J

    sub-long/2addr v0, v2

    long-to-int v1, v0

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method public final d(JLjava/nio/ByteBuffer;I)V
    .locals 4

    :goto_0
    iget-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-wide v1, v0, Lf/h/a/c/d1/t$a;->b:J

    cmp-long v3, p1, v1

    if-ltz v3, :cond_0

    iget-object v0, v0, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    goto :goto_0

    :cond_0
    :goto_1
    if-lez p4, :cond_1

    iget-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-wide v0, v0, Lf/h/a/c/d1/t$a;->b:J

    sub-long/2addr v0, p1

    long-to-int v1, v0

    invoke-static {p4, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-object v2, v1, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    iget-object v2, v2, Lf/h/a/c/h1/c;->a:[B

    invoke-virtual {v1, p1, p2}, Lf/h/a/c/d1/t$a;->a(J)I

    move-result v1

    invoke-virtual {p3, v2, v1, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    sub-int/2addr p4, v0

    int-to-long v0, v0

    add-long/2addr p1, v0

    iget-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-wide v1, v0, Lf/h/a/c/d1/t$a;->b:J

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    iget-object v0, v0, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final e(J[BI)V
    .locals 5

    :goto_0
    iget-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-wide v1, v0, Lf/h/a/c/d1/t$a;->b:J

    cmp-long v3, p1, v1

    if-ltz v3, :cond_0

    iget-object v0, v0, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object v0, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    goto :goto_0

    :cond_0
    move v0, p4

    :cond_1
    :goto_1
    if-lez v0, :cond_2

    iget-object v1, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-wide v1, v1, Lf/h/a/c/d1/t$a;->b:J

    sub-long/2addr v1, p1

    long-to-int v2, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-object v3, v2, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    iget-object v3, v3, Lf/h/a/c/h1/c;->a:[B

    invoke-virtual {v2, p1, p2}, Lf/h/a/c/d1/t$a;->a(J)I

    move-result v2

    sub-int v4, p4, v0

    invoke-static {v3, v2, p3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sub-int/2addr v0, v1

    int-to-long v1, v1

    add-long/2addr p1, v1

    iget-object v1, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iget-wide v2, v1, Lf/h/a/c/d1/t$a;->b:J

    cmp-long v4, p1, v2

    if-nez v4, :cond_1

    iget-object v1, v1, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object v1, p0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    goto :goto_1

    :cond_2
    return-void
.end method
