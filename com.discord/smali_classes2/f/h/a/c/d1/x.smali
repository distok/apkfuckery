.class public final Lf/h/a/c/d1/x;
.super Lf/h/a/c/t0;
.source "SinglePeriodTimeline.java"


# static fields
.field public static final g:Ljava/lang/Object;


# instance fields
.field public final b:J

.field public final c:J

.field public final d:Z

.field public final e:Z

.field public final f:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf/h/a/c/d1/x;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(JZZZLjava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p6    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lf/h/a/c/t0;-><init>()V

    iput-wide p1, p0, Lf/h/a/c/d1/x;->b:J

    iput-wide p1, p0, Lf/h/a/c/d1/x;->c:J

    iput-boolean p3, p0, Lf/h/a/c/d1/x;->d:Z

    iput-boolean p5, p0, Lf/h/a/c/d1/x;->e:Z

    iput-object p7, p0, Lf/h/a/c/d1/x;->f:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/Object;)I
    .locals 1

    sget-object v0, Lf/h/a/c/d1/x;->g:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public g(ILf/h/a/c/t0$b;Z)Lf/h/a/c/t0$b;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lf/g/j/k/a;->l(III)I

    if-eqz p3, :cond_0

    sget-object p1, Lf/h/a/c/d1/x;->g:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-wide v1, p0, Lf/h/a/c/d1/x;->b:J

    const-wide/16 v3, 0x0

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p3, Lf/h/a/c/d1/y/a;->e:Lf/h/a/c/d1/y/a;

    iput-object p1, p2, Lf/h/a/c/t0$b;->a:Ljava/lang/Object;

    iput v0, p2, Lf/h/a/c/t0$b;->b:I

    iput-wide v1, p2, Lf/h/a/c/t0$b;->c:J

    iput-wide v3, p2, Lf/h/a/c/t0$b;->d:J

    iput-object p3, p2, Lf/h/a/c/t0$b;->e:Lf/h/a/c/d1/y/a;

    return-object p2
.end method

.method public i()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public l(I)Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lf/g/j/k/a;->l(III)I

    sget-object p1, Lf/h/a/c/d1/x;->g:Ljava/lang/Object;

    return-object p1
.end method

.method public n(ILf/h/a/c/t0$c;J)Lf/h/a/c/t0$c;
    .locals 4

    const/4 p3, 0x0

    const/4 p4, 0x1

    invoke-static {p1, p3, p4}, Lf/g/j/k/a;->l(III)I

    sget-object p1, Lf/h/a/c/t0$c;->k:Ljava/lang/Object;

    iget-object p4, p0, Lf/h/a/c/d1/x;->f:Ljava/lang/Object;

    const/4 v0, 0x0

    iget-boolean v1, p0, Lf/h/a/c/d1/x;->d:Z

    iget-wide v2, p0, Lf/h/a/c/d1/x;->c:J

    iput-object p1, p2, Lf/h/a/c/t0$c;->a:Ljava/lang/Object;

    iput-object p4, p2, Lf/h/a/c/t0$c;->b:Ljava/lang/Object;

    iput-object v0, p2, Lf/h/a/c/t0$c;->c:Ljava/lang/Object;

    iput-boolean v1, p2, Lf/h/a/c/t0$c;->d:Z

    iput-boolean p3, p2, Lf/h/a/c/t0$c;->e:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p2, Lf/h/a/c/t0$c;->h:J

    iput-wide v2, p2, Lf/h/a/c/t0$c;->i:J

    iput p3, p2, Lf/h/a/c/t0$c;->f:I

    iput p3, p2, Lf/h/a/c/t0$c;->g:I

    iput-wide v0, p2, Lf/h/a/c/t0$c;->j:J

    return-object p2
.end method

.method public o()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
