.class public final Lf/h/a/c/d1/r$b;
.super Ljava/lang/Object;
.source "ProgressiveMediaPeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/d1/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:[Lf/h/a/c/a1/h;

.field public b:Lf/h/a/c/a1/h;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>([Lf/h/a/c/a1/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/r$b;->a:[Lf/h/a/c/a1/h;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/a1/e;Lf/h/a/c/a1/i;Landroid/net/Uri;)Lf/h/a/c/a1/h;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/d1/r$b;->a:[Lf/h/a/c/a1/h;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    aget-object p1, v0, v2

    iput-object p1, p0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;

    goto :goto_3

    :cond_1
    array-length v1, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, v0, v3

    :try_start_0
    invoke-interface {v4, p1}, Lf/h/a/c/a1/h;->h(Lf/h/a/c/a1/e;)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-object v4, p0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput v2, p1, Lf/h/a/c/a1/e;->f:I

    goto :goto_1

    :catchall_0
    move-exception p2

    iput v2, p1, Lf/h/a/c/a1/e;->f:I

    throw p2

    :catch_0
    :cond_2
    iput v2, p1, Lf/h/a/c/a1/e;->f:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    iget-object p1, p0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;

    if-nez p1, :cond_6

    new-instance p1, Lcom/google/android/exoplayer2/source/UnrecognizedInputFormatException;

    const-string p2, "None of the available extractors ("

    invoke-static {p2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    iget-object v0, p0, Lf/h/a/c/d1/r$b;->a:[Lf/h/a/c/a1/h;

    sget v1, Lf/h/a/c/i1/a0;->a:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    array-length v3, v0

    if-ge v2, v3, :cond_5

    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_4

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ") could read the stream."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, p3}, Lcom/google/android/exoplayer2/source/UnrecognizedInputFormatException;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    throw p1

    :cond_6
    :goto_3
    iget-object p1, p0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;

    invoke-interface {p1, p2}, Lf/h/a/c/a1/h;->e(Lf/h/a/c/a1/i;)V

    iget-object p1, p0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;

    return-object p1
.end method
