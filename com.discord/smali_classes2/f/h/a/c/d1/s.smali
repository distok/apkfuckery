.class public final Lf/h/a/c/d1/s;
.super Lf/h/a/c/d1/k;
.source "ProgressiveMediaSource.java"

# interfaces
.implements Lf/h/a/c/d1/r$c;


# instance fields
.field public final f:Landroid/net/Uri;

.field public final g:Lf/h/a/c/h1/j$a;

.field public final h:Lf/h/a/c/a1/j;

.field public final i:Lf/h/a/c/z0/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/c/z0/b<",
            "*>;"
        }
    .end annotation
.end field

.field public final j:Lf/h/a/c/h1/u;

.field public final k:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final l:I

.field public final m:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:Z

.field public p:Z

.field public q:Lf/h/a/c/h1/x;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lf/h/a/c/h1/j$a;Lf/h/a/c/a1/j;Lf/h/a/c/z0/b;Lf/h/a/c/h1/u;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0
    .param p6    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lf/h/a/c/h1/j$a;",
            "Lf/h/a/c/a1/j;",
            "Lf/h/a/c/z0/b<",
            "*>;",
            "Lf/h/a/c/h1/u;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lf/h/a/c/d1/k;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/s;->f:Landroid/net/Uri;

    iput-object p2, p0, Lf/h/a/c/d1/s;->g:Lf/h/a/c/h1/j$a;

    iput-object p3, p0, Lf/h/a/c/d1/s;->h:Lf/h/a/c/a1/j;

    iput-object p4, p0, Lf/h/a/c/d1/s;->i:Lf/h/a/c/z0/b;

    iput-object p5, p0, Lf/h/a/c/d1/s;->j:Lf/h/a/c/h1/u;

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/d1/s;->k:Ljava/lang/String;

    iput p7, p0, Lf/h/a/c/d1/s;->l:I

    const-wide p2, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p2, p0, Lf/h/a/c/d1/s;->n:J

    iput-object p1, p0, Lf/h/a/c/d1/s;->m:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/d1/p$a;Lf/h/a/c/h1/d;J)Lf/h/a/c/d1/o;
    .locals 19

    move-object/from16 v11, p0

    iget-object v0, v11, Lf/h/a/c/d1/s;->g:Lf/h/a/c/h1/j$a;

    invoke-interface {v0}, Lf/h/a/c/h1/j$a;->a()Lf/h/a/c/h1/j;

    move-result-object v2

    iget-object v0, v11, Lf/h/a/c/d1/s;->q:Lf/h/a/c/h1/x;

    if-eqz v0, :cond_0

    invoke-interface {v2, v0}, Lf/h/a/c/h1/j;->b(Lf/h/a/c/h1/x;)V

    :cond_0
    new-instance v12, Lf/h/a/c/d1/r;

    iget-object v1, v11, Lf/h/a/c/d1/s;->f:Landroid/net/Uri;

    iget-object v0, v11, Lf/h/a/c/d1/s;->h:Lf/h/a/c/a1/j;

    invoke-interface {v0}, Lf/h/a/c/a1/j;->a()[Lf/h/a/c/a1/h;

    move-result-object v3

    iget-object v4, v11, Lf/h/a/c/d1/s;->i:Lf/h/a/c/z0/b;

    iget-object v5, v11, Lf/h/a/c/d1/s;->j:Lf/h/a/c/h1/u;

    iget-object v0, v11, Lf/h/a/c/d1/k;->c:Lf/h/a/c/d1/q$a;

    new-instance v6, Lf/h/a/c/d1/q$a;

    iget-object v14, v0, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    const/4 v15, 0x0

    const-wide/16 v17, 0x0

    move-object v13, v6

    move-object/from16 v16, p1

    invoke-direct/range {v13 .. v18}, Lf/h/a/c/d1/q$a;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;ILf/h/a/c/d1/p$a;J)V

    iget-object v9, v11, Lf/h/a/c/d1/s;->k:Ljava/lang/String;

    iget v10, v11, Lf/h/a/c/d1/s;->l:I

    move-object v0, v12

    move-object/from16 v7, p0

    move-object/from16 v8, p2

    invoke-direct/range {v0 .. v10}, Lf/h/a/c/d1/r;-><init>(Landroid/net/Uri;Lf/h/a/c/h1/j;[Lf/h/a/c/a1/h;Lf/h/a/c/z0/b;Lf/h/a/c/h1/u;Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/r$c;Lf/h/a/c/h1/d;Ljava/lang/String;I)V

    return-object v12
.end method

.method public d()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public e(Lf/h/a/c/d1/o;)V
    .locals 6

    check-cast p1, Lf/h/a/c/d1/r;

    iget-boolean v0, p1, Lf/h/a/c/d1/r;->y:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lf/h/a/c/d1/u;->f()V

    iget-object v5, v4, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v5, :cond_0

    invoke-interface {v5}, Lcom/google/android/exoplayer2/drm/DrmSession;->release()V

    iput-object v1, v4, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    iput-object v1, v4, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lf/h/a/c/d1/r;->m:Lcom/google/android/exoplayer2/upstream/Loader;

    iget-object v2, v0, Lcom/google/android/exoplayer2/upstream/Loader;->b:Lcom/google/android/exoplayer2/upstream/Loader$d;

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/upstream/Loader$d;->a(Z)V

    :cond_2
    iget-object v2, v0, Lcom/google/android/exoplayer2/upstream/Loader;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/google/android/exoplayer2/upstream/Loader$g;

    invoke-direct {v4, p1}, Lcom/google/android/exoplayer2/upstream/Loader$g;-><init>(Lcom/google/android/exoplayer2/upstream/Loader$f;)V

    invoke-interface {v2, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    iget-object v0, v0, Lcom/google/android/exoplayer2/upstream/Loader;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    iget-object v0, p1, Lf/h/a/c/d1/r;->r:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p1, Lf/h/a/c/d1/r;->s:Lf/h/a/c/d1/o$a;

    iput-boolean v3, p1, Lf/h/a/c/d1/r;->O:Z

    iget-object p1, p1, Lf/h/a/c/d1/r;->h:Lf/h/a/c/d1/q$a;

    iget-object v0, p1, Lf/h/a/c/d1/q$a;->b:Lf/h/a/c/d1/p$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/h/a/c/d1/q$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/d1/q$a$a;

    iget-object v3, v2, Lf/h/a/c/d1/q$a$a;->b:Lf/h/a/c/d1/q;

    iget-object v2, v2, Lf/h/a/c/d1/q$a$a;->a:Landroid/os/Handler;

    new-instance v4, Lf/h/a/c/d1/e;

    invoke-direct {v4, p1, v3, v0}, Lf/h/a/c/d1/e;-><init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/p$a;)V

    invoke-virtual {p1, v2, v4}, Lf/h/a/c/d1/q$a;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final h(JZZ)V
    .locals 8

    iput-wide p1, p0, Lf/h/a/c/d1/s;->n:J

    iput-boolean p3, p0, Lf/h/a/c/d1/s;->o:Z

    iput-boolean p4, p0, Lf/h/a/c/d1/s;->p:Z

    new-instance p1, Lf/h/a/c/d1/x;

    iget-wide v1, p0, Lf/h/a/c/d1/s;->n:J

    iget-boolean v3, p0, Lf/h/a/c/d1/s;->o:Z

    iget-boolean v5, p0, Lf/h/a/c/d1/s;->p:Z

    iget-object v7, p0, Lf/h/a/c/d1/s;->m:Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v7}, Lf/h/a/c/d1/x;-><init>(JZZZLjava/lang/Object;Ljava/lang/Object;)V

    iput-object p1, p0, Lf/h/a/c/d1/k;->e:Lf/h/a/c/t0;

    iget-object p2, p0, Lf/h/a/c/d1/k;->a:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/h/a/c/d1/p$b;

    invoke-interface {p3, p0, p1}, Lf/h/a/c/d1/p$b;->a(Lf/h/a/c/d1/p;Lf/h/a/c/t0;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public i(JZZ)V
    .locals 3

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    iget-wide p1, p0, Lf/h/a/c/d1/s;->n:J

    :cond_0
    iget-wide v0, p0, Lf/h/a/c/d1/s;->n:J

    cmp-long v2, v0, p1

    if-nez v2, :cond_1

    iget-boolean v0, p0, Lf/h/a/c/d1/s;->o:Z

    if-ne v0, p3, :cond_1

    iget-boolean v0, p0, Lf/h/a/c/d1/s;->p:Z

    if-ne v0, p4, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lf/h/a/c/d1/s;->h(JZZ)V

    return-void
.end method
