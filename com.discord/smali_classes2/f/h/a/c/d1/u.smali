.class public Lf/h/a/c/d1/u;
.super Ljava/lang/Object;
.source "SampleQueue.java"

# interfaces
.implements Lf/h/a/c/a1/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/d1/u$a;,
        Lf/h/a/c/d1/u$b;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/d1/t;

.field public final b:Lf/h/a/c/d1/u$a;

.field public final c:Lf/h/a/c/z0/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/c/z0/b<",
            "*>;"
        }
    .end annotation
.end field

.field public d:Lf/h/a/c/d1/u$b;

.field public e:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/google/android/exoplayer2/drm/DrmSession;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/exoplayer2/drm/DrmSession<",
            "*>;"
        }
    .end annotation
.end field

.field public g:I

.field public h:[I

.field public i:[J

.field public j:[I

.field public k:[I

.field public l:[J

.field public m:[Lf/h/a/c/a1/s$a;

.field public n:[Lcom/google/android/exoplayer2/Format;

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:J

.field public t:J

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Lcom/google/android/exoplayer2/Format;

.field public y:Lcom/google/android/exoplayer2/Format;

.field public z:Lcom/google/android/exoplayer2/Format;


# direct methods
.method public constructor <init>(Lf/h/a/c/h1/d;Lf/h/a/c/z0/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/c/h1/d;",
            "Lf/h/a/c/z0/b<",
            "*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/d1/t;

    invoke-direct {v0, p1}, Lf/h/a/c/d1/t;-><init>(Lf/h/a/c/h1/d;)V

    iput-object v0, p0, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    iput-object p2, p0, Lf/h/a/c/d1/u;->c:Lf/h/a/c/z0/b;

    new-instance p1, Lf/h/a/c/d1/u$a;

    invoke-direct {p1}, Lf/h/a/c/d1/u$a;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/u;->b:Lf/h/a/c/d1/u$a;

    const/16 p1, 0x3e8

    iput p1, p0, Lf/h/a/c/d1/u;->g:I

    new-array p2, p1, [I

    iput-object p2, p0, Lf/h/a/c/d1/u;->h:[I

    new-array p2, p1, [J

    iput-object p2, p0, Lf/h/a/c/d1/u;->i:[J

    new-array p2, p1, [J

    iput-object p2, p0, Lf/h/a/c/d1/u;->l:[J

    new-array p2, p1, [I

    iput-object p2, p0, Lf/h/a/c/d1/u;->k:[I

    new-array p2, p1, [I

    iput-object p2, p0, Lf/h/a/c/d1/u;->j:[I

    new-array p2, p1, [Lf/h/a/c/a1/s$a;

    iput-object p2, p0, Lf/h/a/c/d1/u;->m:[Lf/h/a/c/a1/s$a;

    new-array p1, p1, [Lcom/google/android/exoplayer2/Format;

    iput-object p1, p0, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    const-wide/high16 p1, -0x8000000000000000L

    iput-wide p1, p0, Lf/h/a/c/d1/u;->s:J

    iput-wide p1, p0, Lf/h/a/c/d1/u;->t:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/d1/u;->w:Z

    iput-boolean p1, p0, Lf/h/a/c/d1/u;->v:Z

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/c/a1/e;IZ)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    invoke-virtual {v0, p2}, Lf/h/a/c/d1/t;->c(I)I

    move-result p2

    iget-object v1, v0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iget-object v2, v1, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    iget-object v2, v2, Lf/h/a/c/h1/c;->a:[B

    iget-wide v3, v0, Lf/h/a/c/d1/t;->g:J

    invoke-virtual {v1, v3, v4}, Lf/h/a/c/d1/t$a;->a(J)I

    move-result v1

    invoke-virtual {p1, v2, v1, p2}, Lf/h/a/c/a1/e;->f([BII)I

    move-result p1

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    if-eqz p3, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_1
    invoke-virtual {v0, p1}, Lf/h/a/c/d1/t;->b(I)V

    :goto_0
    return p1
.end method

.method public final b(Lf/h/a/c/i1/r;I)V
    .locals 6

    iget-object v0, p0, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    if-lez p2, :cond_0

    invoke-virtual {v0, p2}, Lf/h/a/c/d1/t;->c(I)I

    move-result v1

    iget-object v2, v0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iget-object v3, v2, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    iget-object v3, v3, Lf/h/a/c/h1/c;->a:[B

    iget-wide v4, v0, Lf/h/a/c/d1/t;->g:J

    invoke-virtual {v2, v4, v5}, Lf/h/a/c/d1/t$a;->a(J)I

    move-result v2

    invoke-virtual {p1, v3, v2, v1}, Lf/h/a/c/i1/r;->d([BII)V

    sub-int/2addr p2, v1

    invoke-virtual {v0, v1}, Lf/h/a/c/d1/t;->b(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(JIIILf/h/a/c/a1/s$a;)V
    .locals 6
    .param p6    # Lf/h/a/c/a1/s$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const-wide/16 v0, 0x0

    add-long/2addr p1, v0

    iget-object v0, p0, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    iget-wide v0, v0, Lf/h/a/c/d1/t;->g:J

    int-to-long v2, p4

    sub-long/2addr v0, v2

    int-to-long v2, p5

    sub-long/2addr v0, v2

    monitor-enter p0

    :try_start_0
    iget-boolean p5, p0, Lf/h/a/c/d1/u;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    if-eqz p5, :cond_1

    and-int/lit8 p5, p3, 0x1

    if-nez p5, :cond_0

    monitor-exit p0

    goto/16 :goto_2

    :cond_0
    :try_start_1
    iput-boolean v2, p0, Lf/h/a/c/d1/u;->v:Z

    :cond_1
    iget-boolean p5, p0, Lf/h/a/c/d1/u;->w:Z

    const/4 v3, 0x1

    if-nez p5, :cond_2

    const/4 p5, 0x1

    goto :goto_0

    :cond_2
    const/4 p5, 0x0

    :goto_0
    invoke-static {p5}, Lf/g/j/k/a;->s(Z)V

    const/high16 p5, 0x20000000

    and-int/2addr p5, p3

    if-eqz p5, :cond_3

    const/4 p5, 0x1

    goto :goto_1

    :cond_3
    const/4 p5, 0x0

    :goto_1
    iput-boolean p5, p0, Lf/h/a/c/d1/u;->u:Z

    iget-wide v4, p0, Lf/h/a/c/d1/u;->t:J

    invoke-static {v4, v5, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lf/h/a/c/d1/u;->t:J

    iget p5, p0, Lf/h/a/c/d1/u;->o:I

    invoke-virtual {p0, p5}, Lf/h/a/c/d1/u;->j(I)I

    move-result p5

    iget-object v4, p0, Lf/h/a/c/d1/u;->l:[J

    aput-wide p1, v4, p5

    iget-object p1, p0, Lf/h/a/c/d1/u;->i:[J

    aput-wide v0, p1, p5

    iget-object p2, p0, Lf/h/a/c/d1/u;->j:[I

    aput p4, p2, p5

    iget-object p2, p0, Lf/h/a/c/d1/u;->k:[I

    aput p3, p2, p5

    iget-object p2, p0, Lf/h/a/c/d1/u;->m:[Lf/h/a/c/a1/s$a;

    aput-object p6, p2, p5

    iget-object p2, p0, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    iget-object p3, p0, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;

    aput-object p3, p2, p5

    iget-object p2, p0, Lf/h/a/c/d1/u;->h:[I

    aput v2, p2, p5

    iput-object p3, p0, Lf/h/a/c/d1/u;->y:Lcom/google/android/exoplayer2/Format;

    iget p2, p0, Lf/h/a/c/d1/u;->o:I

    add-int/2addr p2, v3

    iput p2, p0, Lf/h/a/c/d1/u;->o:I

    iget p3, p0, Lf/h/a/c/d1/u;->g:I

    if-ne p2, p3, :cond_4

    add-int/lit16 p2, p3, 0x3e8

    new-array p4, p2, [I

    new-array p5, p2, [J

    new-array p6, p2, [J

    new-array v0, p2, [I

    new-array v1, p2, [I

    new-array v3, p2, [Lf/h/a/c/a1/s$a;

    new-array v4, p2, [Lcom/google/android/exoplayer2/Format;

    iget v5, p0, Lf/h/a/c/d1/u;->q:I

    sub-int/2addr p3, v5

    invoke-static {p1, v5, p5, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lf/h/a/c/d1/u;->l:[J

    iget v5, p0, Lf/h/a/c/d1/u;->q:I

    invoke-static {p1, v5, p6, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lf/h/a/c/d1/u;->k:[I

    iget v5, p0, Lf/h/a/c/d1/u;->q:I

    invoke-static {p1, v5, v0, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lf/h/a/c/d1/u;->j:[I

    iget v5, p0, Lf/h/a/c/d1/u;->q:I

    invoke-static {p1, v5, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lf/h/a/c/d1/u;->m:[Lf/h/a/c/a1/s$a;

    iget v5, p0, Lf/h/a/c/d1/u;->q:I

    invoke-static {p1, v5, v3, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    iget v5, p0, Lf/h/a/c/d1/u;->q:I

    invoke-static {p1, v5, v4, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lf/h/a/c/d1/u;->h:[I

    iget v5, p0, Lf/h/a/c/d1/u;->q:I

    invoke-static {p1, v5, p4, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lf/h/a/c/d1/u;->q:I

    iget-object v5, p0, Lf/h/a/c/d1/u;->i:[J

    invoke-static {v5, v2, p5, p3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v5, p0, Lf/h/a/c/d1/u;->l:[J

    invoke-static {v5, v2, p6, p3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v5, p0, Lf/h/a/c/d1/u;->k:[I

    invoke-static {v5, v2, v0, p3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v5, p0, Lf/h/a/c/d1/u;->j:[I

    invoke-static {v5, v2, v1, p3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v5, p0, Lf/h/a/c/d1/u;->m:[Lf/h/a/c/a1/s$a;

    invoke-static {v5, v2, v3, p3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v5, p0, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    invoke-static {v5, v2, v4, p3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v5, p0, Lf/h/a/c/d1/u;->h:[I

    invoke-static {v5, v2, p4, p3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p5, p0, Lf/h/a/c/d1/u;->i:[J

    iput-object p6, p0, Lf/h/a/c/d1/u;->l:[J

    iput-object v0, p0, Lf/h/a/c/d1/u;->k:[I

    iput-object v1, p0, Lf/h/a/c/d1/u;->j:[I

    iput-object v3, p0, Lf/h/a/c/d1/u;->m:[Lf/h/a/c/a1/s$a;

    iput-object v4, p0, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    iput-object p4, p0, Lf/h/a/c/d1/u;->h:[I

    iput v2, p0, Lf/h/a/c/d1/u;->q:I

    iput p2, p0, Lf/h/a/c/d1/u;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    monitor-exit p0

    :goto_2
    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final d(Lcom/google/android/exoplayer2/Format;)V
    .locals 3

    iput-object p1, p0, Lf/h/a/c/d1/u;->z:Lcom/google/android/exoplayer2/Format;

    monitor-enter p0

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :try_start_0
    iput-boolean v0, p0, Lf/h/a/c/d1/u;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    goto :goto_0

    :cond_0
    :try_start_1
    iput-boolean v1, p0, Lf/h/a/c/d1/u;->w:Z

    iget-object v2, p0, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;

    invoke-static {p1, v2}, Lf/h/a/c/i1/a0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_1

    monitor-exit p0

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :try_start_2
    iget-object v1, p0, Lf/h/a/c/d1/u;->y:Lcom/google/android/exoplayer2/Format;

    invoke-static {p1, v1}, Lf/h/a/c/i1/a0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Lf/h/a/c/d1/u;->y:Lcom/google/android/exoplayer2/Format;

    iput-object p1, p0, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    goto :goto_1

    :cond_2
    :try_start_3
    iput-object p1, p0, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    :goto_1
    iget-object p1, p0, Lf/h/a/c/d1/u;->d:Lf/h/a/c/d1/u$b;

    if-eqz p1, :cond_3

    if-eqz v0, :cond_3

    check-cast p1, Lf/h/a/c/d1/r;

    iget-object v0, p1, Lf/h/a/c/d1/r;->r:Landroid/os/Handler;

    iget-object p1, p1, Lf/h/a/c/d1/r;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final e(I)J
    .locals 4

    iget-wide v0, p0, Lf/h/a/c/d1/u;->s:J

    invoke-virtual {p0, p1}, Lf/h/a/c/d1/u;->i(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/c/d1/u;->s:J

    iget v0, p0, Lf/h/a/c/d1/u;->o:I

    sub-int/2addr v0, p1

    iput v0, p0, Lf/h/a/c/d1/u;->o:I

    iget v1, p0, Lf/h/a/c/d1/u;->p:I

    add-int/2addr v1, p1

    iput v1, p0, Lf/h/a/c/d1/u;->p:I

    iget v1, p0, Lf/h/a/c/d1/u;->q:I

    add-int/2addr v1, p1

    iput v1, p0, Lf/h/a/c/d1/u;->q:I

    iget v2, p0, Lf/h/a/c/d1/u;->g:I

    if-lt v1, v2, :cond_0

    sub-int/2addr v1, v2

    iput v1, p0, Lf/h/a/c/d1/u;->q:I

    :cond_0
    iget v1, p0, Lf/h/a/c/d1/u;->r:I

    sub-int/2addr v1, p1

    iput v1, p0, Lf/h/a/c/d1/u;->r:I

    if-gez v1, :cond_1

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/d1/u;->r:I

    :cond_1
    if-nez v0, :cond_3

    iget p1, p0, Lf/h/a/c/d1/u;->q:I

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    move v2, p1

    :goto_0
    add-int/lit8 v2, v2, -0x1

    iget-object p1, p0, Lf/h/a/c/d1/u;->i:[J

    aget-wide v0, p1, v2

    iget-object p1, p0, Lf/h/a/c/d1/u;->j:[I

    aget p1, p1, v2

    int-to-long v2, p1

    add-long/2addr v0, v2

    return-wide v0

    :cond_3
    iget-object p1, p0, Lf/h/a/c/d1/u;->i:[J

    iget v0, p0, Lf/h/a/c/d1/u;->q:I

    aget-wide v0, p1, v0

    return-wide v0
.end method

.method public final f()V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lf/h/a/c/d1/u;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    const-wide/16 v1, -0x1

    monitor-exit p0

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-virtual {p0, v1}, Lf/h/a/c/d1/u;->e(I)J

    move-result-wide v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    :goto_0
    invoke-virtual {v0, v1, v2}, Lf/h/a/c/d1/t;->a(J)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g(IIJZ)I
    .locals 6

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p2, :cond_3

    iget-object v3, p0, Lf/h/a/c/d1/u;->l:[J

    aget-wide v4, v3, p1

    cmp-long v3, v4, p3

    if-gtz v3, :cond_3

    if-eqz p5, :cond_0

    iget-object v3, p0, Lf/h/a/c/d1/u;->k:[I

    aget v3, v3, p1

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    add-int/lit8 p1, p1, 0x1

    iget v3, p0, Lf/h/a/c/d1/u;->g:I

    if-ne p1, v3, :cond_2

    const/4 p1, 0x0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method

.method public final declared-synchronized h()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lf/h/a/c/d1/u;->t:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final i(I)J
    .locals 7

    const-wide/high16 v0, -0x8000000000000000L

    if-nez p1, :cond_0

    return-wide v0

    :cond_0
    add-int/lit8 v2, p1, -0x1

    invoke-virtual {p0, v2}, Lf/h/a/c/d1/u;->j(I)I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p1, :cond_3

    iget-object v4, p0, Lf/h/a/c/d1/u;->l:[J

    aget-wide v5, v4, v2

    invoke-static {v0, v1, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v4, p0, Lf/h/a/c/d1/u;->k:[I

    aget v4, v4, v2

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, -0x1

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    iget v2, p0, Lf/h/a/c/d1/u;->g:I

    add-int/lit8 v2, v2, -0x1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-wide v0
.end method

.method public final j(I)I
    .locals 1

    iget v0, p0, Lf/h/a/c/d1/u;->q:I

    add-int/2addr v0, p1

    iget p1, p0, Lf/h/a/c/d1/u;->g:I

    if-ge v0, p1, :cond_0

    goto :goto_0

    :cond_0
    sub-int/2addr v0, p1

    :goto_0
    return v0
.end method

.method public final declared-synchronized k()Lcom/google/android/exoplayer2/Format;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf/h/a/c/d1/u;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final l()Z
    .locals 2

    iget v0, p0, Lf/h/a/c/d1/u;->r:I

    iget v1, p0, Lf/h/a/c/d1/u;->o:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public declared-synchronized m(Z)Z
    .locals 3
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/c/d1/u;->l()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_2

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lf/h/a/c/d1/u;->u:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    :cond_2
    :try_start_1
    iget p1, p0, Lf/h/a/c/d1/u;->r:I

    invoke-virtual {p0, p1}, Lf/h/a/c/d1/u;->j(I)I

    move-result p1

    iget-object v0, p0, Lf/h/a/c/d1/u;->n:[Lcom/google/android/exoplayer2/Format;

    aget-object v0, v0, p1

    iget-object v2, p0, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v2, :cond_3

    monitor-exit p0

    return v1

    :cond_3
    :try_start_2
    invoke-virtual {p0, p1}, Lf/h/a/c/d1/u;->n(I)Z

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final n(I)Z
    .locals 3

    iget-object v0, p0, Lf/h/a/c/d1/u;->c:Lf/h/a/c/z0/b;

    sget-object v1, Lf/h/a/c/z0/b;->a:Lf/h/a/c/z0/b;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/exoplayer2/drm/DrmSession;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lf/h/a/c/d1/u;->k:[I

    aget p1, v0, p1

    const/high16 v0, 0x40000000    # 2.0f

    and-int/2addr p1, v0

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    invoke-interface {p1}, Lcom/google/android/exoplayer2/drm/DrmSession;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_0
    return v2
.end method

.method public final o(Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d0;)V
    .locals 5

    iput-object p1, p2, Lf/h/a/c/d0;->c:Lcom/google/android/exoplayer2/Format;

    iget-object v0, p0, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-object v0, v0, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    :goto_1
    iput-object p1, p0, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;

    iget-object v3, p0, Lf/h/a/c/d1/u;->c:Lf/h/a/c/z0/b;

    sget-object v4, Lf/h/a/c/z0/b;->a:Lf/h/a/c/z0/b;

    if-ne v3, v4, :cond_2

    return-void

    :cond_2
    iget-object v3, p1, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    iput-boolean v1, p2, Lf/h/a/c/d0;->a:Z

    iget-object v1, p0, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    iput-object v1, p2, Lf/h/a/c/d0;->b:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-nez v2, :cond_3

    invoke-static {v0, v3}, Lf/h/a/c/i1/a0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    return-void

    :cond_3
    iget-object v0, p0, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v3, :cond_4

    iget-object p1, p0, Lf/h/a/c/d1/u;->c:Lf/h/a/c/z0/b;

    invoke-interface {p1, v1, v3}, Lf/h/a/c/z0/b;->c(Landroid/os/Looper;Lcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/drm/DrmSession;

    move-result-object p1

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lf/h/a/c/d1/u;->c:Lf/h/a/c/z0/b;

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {p1}, Lf/h/a/c/i1/o;->e(Ljava/lang/String;)I

    move-result p1

    invoke-interface {v2, v1, p1}, Lf/h/a/c/z0/b;->b(Landroid/os/Looper;I)Lcom/google/android/exoplayer2/drm/DrmSession;

    move-result-object p1

    :goto_2
    iput-object p1, p0, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    iput-object p1, p2, Lf/h/a/c/d0;->b:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lcom/google/android/exoplayer2/drm/DrmSession;->release()V

    :cond_5
    return-void
.end method

.method public p(Z)V
    .locals 10
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    iget-object v1, v0, Lf/h/a/c/d1/t;->d:Lf/h/a/c/d1/t$a;

    iget-boolean v2, v1, Lf/h/a/c/d1/t$a;->c:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v2, v0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iget-boolean v5, v2, Lf/h/a/c/d1/t$a;->c:Z

    iget-wide v6, v2, Lf/h/a/c/d1/t$a;->a:J

    iget-wide v8, v1, Lf/h/a/c/d1/t$a;->a:J

    sub-long/2addr v6, v8

    long-to-int v2, v6

    iget v6, v0, Lf/h/a/c/d1/t;->b:I

    div-int/2addr v2, v6

    add-int/2addr v2, v5

    new-array v5, v2, [Lf/h/a/c/h1/c;

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_1

    iget-object v7, v1, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    aput-object v7, v5, v6

    iput-object v3, v1, Lf/h/a/c/d1/t$a;->d:Lf/h/a/c/h1/c;

    iget-object v7, v1, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    iput-object v3, v1, Lf/h/a/c/d1/t$a;->e:Lf/h/a/c/d1/t$a;

    add-int/lit8 v6, v6, 0x1

    move-object v1, v7

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lf/h/a/c/d1/t;->a:Lf/h/a/c/h1/d;

    check-cast v1, Lf/h/a/c/h1/l;

    invoke-virtual {v1, v5}, Lf/h/a/c/h1/l;->a([Lf/h/a/c/h1/c;)V

    :goto_1
    new-instance v1, Lf/h/a/c/d1/t$a;

    iget v2, v0, Lf/h/a/c/d1/t;->b:I

    const-wide/16 v5, 0x0

    invoke-direct {v1, v5, v6, v2}, Lf/h/a/c/d1/t$a;-><init>(JI)V

    iput-object v1, v0, Lf/h/a/c/d1/t;->d:Lf/h/a/c/d1/t$a;

    iput-object v1, v0, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;

    iput-object v1, v0, Lf/h/a/c/d1/t;->f:Lf/h/a/c/d1/t$a;

    iput-wide v5, v0, Lf/h/a/c/d1/t;->g:J

    iget-object v0, v0, Lf/h/a/c/d1/t;->a:Lf/h/a/c/h1/d;

    check-cast v0, Lf/h/a/c/h1/l;

    invoke-virtual {v0}, Lf/h/a/c/h1/l;->c()V

    iput v4, p0, Lf/h/a/c/d1/u;->o:I

    iput v4, p0, Lf/h/a/c/d1/u;->p:I

    iput v4, p0, Lf/h/a/c/d1/u;->q:I

    iput v4, p0, Lf/h/a/c/d1/u;->r:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/d1/u;->v:Z

    const-wide/high16 v1, -0x8000000000000000L

    iput-wide v1, p0, Lf/h/a/c/d1/u;->s:J

    iput-wide v1, p0, Lf/h/a/c/d1/u;->t:J

    iput-boolean v4, p0, Lf/h/a/c/d1/u;->u:Z

    iput-object v3, p0, Lf/h/a/c/d1/u;->y:Lcom/google/android/exoplayer2/Format;

    if-eqz p1, :cond_2

    iput-object v3, p0, Lf/h/a/c/d1/u;->z:Lcom/google/android/exoplayer2/Format;

    iput-object v3, p0, Lf/h/a/c/d1/u;->x:Lcom/google/android/exoplayer2/Format;

    iput-boolean v0, p0, Lf/h/a/c/d1/u;->w:Z

    :cond_2
    return-void
.end method

.method public final declared-synchronized q(JZ)Z
    .locals 9

    monitor-enter p0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lf/h/a/c/d1/u;->r:I

    iget-object v1, p0, Lf/h/a/c/d1/u;->a:Lf/h/a/c/d1/t;

    iget-object v2, v1, Lf/h/a/c/d1/t;->d:Lf/h/a/c/d1/t$a;

    iput-object v2, v1, Lf/h/a/c/d1/t;->e:Lf/h/a/c/d1/t$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit p0

    invoke-virtual {p0, v0}, Lf/h/a/c/d1/u;->j(I)I

    move-result v4

    invoke-virtual {p0}, Lf/h/a/c/d1/u;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lf/h/a/c/d1/u;->l:[J

    aget-wide v2, v1, v4

    cmp-long v1, p1, v2

    if-ltz v1, :cond_2

    iget-wide v1, p0, Lf/h/a/c/d1/u;->t:J

    cmp-long v3, p1, v1

    if-lez v3, :cond_0

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    iget p3, p0, Lf/h/a/c/d1/u;->o:I

    iget v1, p0, Lf/h/a/c/d1/u;->r:I

    sub-int v5, p3, v1

    const/4 v8, 0x1

    move-object v3, p0

    move-wide v6, p1

    invoke-virtual/range {v3 .. v8}, Lf/h/a/c/d1/u;->g(IIJZ)I

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    monitor-exit p0

    return v0

    :cond_1
    :try_start_3
    iget p2, p0, Lf/h/a/c/d1/u;->r:I

    add-int/2addr p2, p1

    iput p2, p0, Lf/h/a/c/d1/u;->r:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 p1, 0x1

    monitor-exit p0

    return p1

    :cond_2
    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catchall_1
    move-exception p1

    :try_start_4
    monitor-exit p0

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    monitor-exit p0

    throw p1
.end method
