.class public final synthetic Lf/h/a/c/d1/h;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/c/d1/q$a;

.field public final synthetic e:Lf/h/a/c/d1/q;

.field public final synthetic f:Lf/h/a/c/d1/p$a;


# direct methods
.method public synthetic constructor <init>(Lf/h/a/c/d1/q$a;Lf/h/a/c/d1/q;Lf/h/a/c/d1/p$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/d1/h;->d:Lf/h/a/c/d1/q$a;

    iput-object p2, p0, Lf/h/a/c/d1/h;->e:Lf/h/a/c/d1/q;

    iput-object p3, p0, Lf/h/a/c/d1/h;->f:Lf/h/a/c/d1/p$a;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    iget-object v0, p0, Lf/h/a/c/d1/h;->d:Lf/h/a/c/d1/q$a;

    iget-object v1, p0, Lf/h/a/c/d1/h;->e:Lf/h/a/c/d1/q;

    iget-object v2, p0, Lf/h/a/c/d1/h;->f:Lf/h/a/c/d1/p$a;

    iget v0, v0, Lf/h/a/c/d1/q$a;->a:I

    check-cast v1, Lf/h/a/c/v0/a;

    iget-object v3, v1, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v4, v3, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    iget-object v5, v2, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eq v4, v5, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    new-instance v8, Lf/h/a/c/v0/a$a;

    if-eqz v5, :cond_1

    iget-object v9, v3, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    goto :goto_1

    :cond_1
    sget-object v9, Lf/h/a/c/t0;->a:Lf/h/a/c/t0;

    :goto_1
    if-eqz v5, :cond_2

    iget-object v5, v3, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    iget-object v10, v3, Lf/h/a/c/v0/a$b;->c:Lf/h/a/c/t0$b;

    invoke-virtual {v5, v4, v10}, Lf/h/a/c/t0;->f(ILf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object v4

    iget v4, v4, Lf/h/a/c/t0$b;->b:I

    goto :goto_2

    :cond_2
    move v4, v0

    :goto_2
    invoke-direct {v8, v2, v9, v4}, Lf/h/a/c/v0/a$a;-><init>(Lf/h/a/c/d1/p$a;Lf/h/a/c/t0;I)V

    iget-object v4, v3, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v3, Lf/h/a/c/v0/a$b;->b:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v3, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/v0/a$a;

    iput-object v4, v3, Lf/h/a/c/v0/a$b;->d:Lf/h/a/c/v0/a$a;

    iget-object v4, v3, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v6, :cond_3

    iget-object v4, v3, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    invoke-virtual {v4}, Lf/h/a/c/t0;->p()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, v3, Lf/h/a/c/v0/a$b;->d:Lf/h/a/c/v0/a$a;

    iput-object v4, v3, Lf/h/a/c/v0/a$b;->e:Lf/h/a/c/v0/a$a;

    :cond_3
    invoke-virtual {v1, v0, v2}, Lf/h/a/c/v0/a;->H(ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;

    iget-object v0, v1, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/v0/b;

    invoke-interface {v1}, Lf/h/a/c/v0/b;->z()V

    goto :goto_3

    :cond_4
    return-void
.end method
