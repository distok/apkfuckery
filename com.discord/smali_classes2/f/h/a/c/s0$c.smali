.class public final Lf/h/a/c/s0$c;
.super Ljava/lang/Object;
.source "SimpleExoPlayer.java"

# interfaces
.implements Lf/h/a/c/j1/r;
.implements Lf/h/a/c/w0/l;
.implements Lf/h/a/c/e1/j;
.implements Lf/h/a/c/c1/e;
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lf/h/a/c/r$b;
.implements Lf/h/a/c/q$b;
.implements Lf/h/a/c/m0$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/s0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field public final synthetic d:Lf/h/a/c/s0;


# direct methods
.method public constructor <init>(Lf/h/a/c/s0;Lf/h/a/c/s0$a;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A(Lf/h/a/c/y0/d;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/r;

    invoke-interface {v1, p1}, Lf/h/a/c/j1/r;->A(Lf/h/a/c/y0/d;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public synthetic C(Lf/h/a/c/j0;)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->c(Lf/h/a/c/m0$a;Lf/h/a/c/j0;)V

    return-void
.end method

.method public synthetic D(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->a(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public synthetic a()V
    .locals 0

    invoke-static {p0}, Lf/h/a/c/l0;->h(Lf/h/a/c/m0$a;)V

    return-void
.end method

.method public b(I)V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget v1, v0, Lf/h/a/c/s0;->w:I

    if-ne v1, p1, :cond_0

    return-void

    :cond_0
    iput p1, v0, Lf/h/a/c/s0;->w:I

    iget-object v0, v0, Lf/h/a/c/s0;->g:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/w0/k;

    iget-object v2, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v2, v2, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1, p1}, Lf/h/a/c/w0/k;->b(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/w0/l;

    invoke-interface {v1, p1}, Lf/h/a/c/w0/l;->b(I)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public c(IIIF)V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/q;

    iget-object v2, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v2, v2, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1, p1, p2, p3, p4}, Lf/h/a/c/j1/q;->c(IIIF)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/r;

    invoke-interface {v1, p1, p2, p3, p4}, Lf/h/a/c/j1/r;->c(IIIF)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public d(I)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-virtual {v0}, Lf/h/a/c/s0;->f()Z

    move-result v1

    invoke-virtual {v0, v1, p1}, Lf/h/a/c/s0;->R(ZI)V

    return-void
.end method

.method public synthetic e(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->d(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public f(Z)V
    .locals 0

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public synthetic g(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->f(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public h(Lf/h/a/c/y0/d;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/w0/l;

    invoke-interface {v1, p1}, Lf/h/a/c/w0/l;->h(Lf/h/a/c/y0/d;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    const/4 v0, 0x0

    iput v0, p1, Lf/h/a/c/s0;->w:I

    return-void
.end method

.method public i(Lf/h/a/c/y0/d;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/w0/l;

    invoke-interface {v1, p1}, Lf/h/a/c/w0/l;->i(Lf/h/a/c/y0/d;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public j(Ljava/lang/String;JJ)V
    .locals 8

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lf/h/a/c/j1/r;

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v2 .. v7}, Lf/h/a/c/j1/r;->j(Ljava/lang/String;JJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic k(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->e(Lf/h/a/c/m0$a;Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    return-void
.end method

.method public synthetic l(Lf/h/a/c/t0;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lf/h/a/c/l0;->j(Lf/h/a/c/m0$a;Lf/h/a/c/t0;I)V

    return-void
.end method

.method public m(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iput-object p1, v0, Lf/h/a/c/s0;->z:Ljava/util/List;

    iget-object v0, v0, Lf/h/a/c/s0;->h:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/e1/j;

    invoke-interface {v1, p1}, Lf/h/a/c/e1/j;->m(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public n(Landroid/view/Surface;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v1, v0, Lf/h/a/c/s0;->q:Landroid/view/Surface;

    if-ne v1, p1, :cond_0

    iget-object v0, v0, Lf/h/a/c/s0;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/q;

    invoke-interface {v1}, Lf/h/a/c/j1/q;->d()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/r;

    invoke-interface {v1, p1}, Lf/h/a/c/j1/r;->n(Landroid/view/Surface;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public o(Ljava/lang/String;JJ)V
    .locals 8

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lf/h/a/c/w0/l;

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v2 .. v7}, Lf/h/a/c/w0/l;->o(Ljava/lang/String;JJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    const/4 p1, 0x1

    invoke-virtual {v0, v1, p1}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-virtual {p1, p2, p3}, Lf/h/a/c/s0;->J(II)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0}, Lf/h/a/c/s0;->J(II)V

    return v1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-virtual {p1, p2, p3}, Lf/h/a/c/s0;->J(II)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public synthetic p(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->i(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public q(Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->i:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/c1/e;

    invoke-interface {v1, p1}, Lf/h/a/c/c1/e;->q(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public r(IJ)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/r;

    invoke-interface {v1, p1, p2, p3}, Lf/h/a/c/j1/r;->r(IJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public s(ZI)V
    .locals 1

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    const/4 p1, 0x4

    if-eq p2, p1, :cond_1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object p2, p2, Lf/h/a/c/s0;->p:Lf/h/a/c/u0;

    iput-boolean p1, p2, Lf/h/a/c/u0;->a:Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object p1, p1, Lf/h/a/c/s0;->p:Lf/h/a/c/u0;

    const/4 p2, 0x0

    iput-boolean p2, p1, Lf/h/a/c/u0;->a:Z

    :goto_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-virtual {p1, p3, p4}, Lf/h/a/c/s0;->J(II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lf/h/a/c/s0;->P(Landroid/view/Surface;Z)V

    iget-object p1, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-virtual {p1, v1, v1}, Lf/h/a/c/s0;->J(II)V

    return-void
.end method

.method public synthetic t(Lf/h/a/c/t0;Ljava/lang/Object;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lf/h/a/c/l0;->k(Lf/h/a/c/m0$a;Lf/h/a/c/t0;Ljava/lang/Object;I)V

    return-void
.end method

.method public synthetic u(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->g(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public v(Lcom/google/android/exoplayer2/Format;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/r;

    invoke-interface {v1, p1}, Lf/h/a/c/j1/r;->v(Lcom/google/android/exoplayer2/Format;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public w(Lf/h/a/c/y0/d;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/j1/r;

    invoke-interface {v1, p1}, Lf/h/a/c/j1/r;->w(Lf/h/a/c/y0/d;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public x(Lcom/google/android/exoplayer2/Format;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/w0/l;

    invoke-interface {v1, p1}, Lf/h/a/c/w0/l;->x(Lcom/google/android/exoplayer2/Format;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public y(IJJ)V
    .locals 8

    iget-object v0, p0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    iget-object v0, v0, Lf/h/a/c/s0;->k:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lf/h/a/c/w0/l;

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v2 .. v7}, Lf/h/a/c/w0/l;->y(IJJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic z(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lf/h/a/c/l0;->l(Lf/h/a/c/m0$a;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V

    return-void
.end method
