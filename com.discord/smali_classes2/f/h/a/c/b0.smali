.class public final Lf/h/a/c/b0;
.super Ljava/lang/Object;
.source "ExoPlayerImplInternal.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lf/h/a/c/d1/o$a;
.implements Lf/h/a/c/d1/p$b;
.implements Lf/h/a/c/y$a;
.implements Lf/h/a/c/n0$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/b0$d;,
        Lf/h/a/c/b0$b;,
        Lf/h/a/c/b0$c;,
        Lf/h/a/c/b0$e;
    }
.end annotation


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:I

.field public E:Z

.field public F:Z

.field public G:I

.field public H:Lf/h/a/c/b0$e;

.field public I:J

.field public J:I

.field public K:Z

.field public final d:[Lf/h/a/c/p0;

.field public final e:[Lf/h/a/c/t;

.field public final f:Lf/h/a/c/f1/h;

.field public final g:Lf/h/a/c/f1/i;

.field public final h:Lf/h/a/c/e0;

.field public final i:Lf/h/a/c/h1/e;

.field public final j:Lf/h/a/c/i1/x;

.field public final k:Landroid/os/HandlerThread;

.field public final l:Landroid/os/Handler;

.field public final m:Lf/h/a/c/t0$c;

.field public final n:Lf/h/a/c/t0$b;

.field public final o:J

.field public final p:Z

.field public final q:Lf/h/a/c/y;

.field public final r:Lf/h/a/c/b0$d;

.field public final s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lf/h/a/c/b0$c;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Lf/h/a/c/i1/f;

.field public final u:Lf/h/a/c/h0;

.field public v:Lf/h/a/c/r0;

.field public w:Lf/h/a/c/i0;

.field public x:Lf/h/a/c/d1/p;

.field public y:[Lf/h/a/c/p0;

.field public z:Z


# direct methods
.method public constructor <init>([Lf/h/a/c/p0;Lf/h/a/c/f1/h;Lf/h/a/c/f1/i;Lf/h/a/c/e0;Lf/h/a/c/h1/e;ZIZLandroid/os/Handler;Lf/h/a/c/i1/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    iput-object p2, p0, Lf/h/a/c/b0;->f:Lf/h/a/c/f1/h;

    iput-object p3, p0, Lf/h/a/c/b0;->g:Lf/h/a/c/f1/i;

    iput-object p4, p0, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    iput-object p5, p0, Lf/h/a/c/b0;->i:Lf/h/a/c/h1/e;

    iput-boolean p6, p0, Lf/h/a/c/b0;->A:Z

    iput p7, p0, Lf/h/a/c/b0;->D:I

    iput-boolean p8, p0, Lf/h/a/c/b0;->E:Z

    iput-object p9, p0, Lf/h/a/c/b0;->l:Landroid/os/Handler;

    iput-object p10, p0, Lf/h/a/c/b0;->t:Lf/h/a/c/i1/f;

    new-instance p6, Lf/h/a/c/h0;

    invoke-direct {p6}, Lf/h/a/c/h0;-><init>()V

    iput-object p6, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    invoke-interface {p4}, Lf/h/a/c/e0;->b()J

    move-result-wide p6

    iput-wide p6, p0, Lf/h/a/c/b0;->o:J

    invoke-interface {p4}, Lf/h/a/c/e0;->a()Z

    move-result p4

    iput-boolean p4, p0, Lf/h/a/c/b0;->p:Z

    sget-object p4, Lf/h/a/c/r0;->d:Lf/h/a/c/r0;

    iput-object p4, p0, Lf/h/a/c/b0;->v:Lf/h/a/c/r0;

    const-wide p6, -0x7fffffffffffffffL    # -4.9E-324

    invoke-static {p6, p7, p3}, Lf/h/a/c/i0;->d(JLf/h/a/c/f1/i;)Lf/h/a/c/i0;

    move-result-object p3

    iput-object p3, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    new-instance p3, Lf/h/a/c/b0$d;

    const/4 p4, 0x0

    invoke-direct {p3, p4}, Lf/h/a/c/b0$d;-><init>(Lf/h/a/c/b0$a;)V

    iput-object p3, p0, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    array-length p3, p1

    new-array p3, p3, [Lf/h/a/c/t;

    iput-object p3, p0, Lf/h/a/c/b0;->e:[Lf/h/a/c/t;

    const/4 p3, 0x0

    const/4 p4, 0x0

    :goto_0
    array-length p6, p1

    if-ge p4, p6, :cond_0

    aget-object p6, p1, p4

    invoke-interface {p6, p4}, Lf/h/a/c/p0;->f(I)V

    iget-object p6, p0, Lf/h/a/c/b0;->e:[Lf/h/a/c/t;

    aget-object p7, p1, p4

    invoke-interface {p7}, Lf/h/a/c/p0;->m()Lf/h/a/c/t;

    move-result-object p7

    aput-object p7, p6, p4

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lf/h/a/c/y;

    invoke-direct {p1, p0, p10}, Lf/h/a/c/y;-><init>(Lf/h/a/c/y$a;Lf/h/a/c/i1/f;)V

    iput-object p1, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    new-array p1, p3, [Lf/h/a/c/p0;

    iput-object p1, p0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    new-instance p1, Lf/h/a/c/t0$c;

    invoke-direct {p1}, Lf/h/a/c/t0$c;-><init>()V

    iput-object p1, p0, Lf/h/a/c/b0;->m:Lf/h/a/c/t0$c;

    new-instance p1, Lf/h/a/c/t0$b;

    invoke-direct {p1}, Lf/h/a/c/t0$b;-><init>()V

    iput-object p1, p0, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    iput-object p5, p2, Lf/h/a/c/f1/h;->a:Lf/h/a/c/h1/e;

    new-instance p1, Landroid/os/HandlerThread;

    const/16 p2, -0x10

    const-string p3, "ExoPlayerImplInternal:Handler"

    invoke-direct {p1, p3, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object p1, p0, Lf/h/a/c/b0;->k:Landroid/os/HandlerThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-interface {p10, p1, p0}, Lf/h/a/c/i1/f;->b(Landroid/os/Looper;Landroid/os/Handler$Callback;)Lf/h/a/c/i1/x;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/b0;->K:Z

    return-void
.end method

.method public static i(Lf/h/a/c/f1/f;)[Lcom/google/android/exoplayer2/Format;
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lf/h/a/c/f1/f;->length()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    new-array v2, v1, [Lcom/google/android/exoplayer2/Format;

    :goto_1
    if-ge v0, v1, :cond_1

    invoke-interface {p0, v0}, Lf/h/a/c/f1/f;->c(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method


# virtual methods
.method public final A(ZZZZZ)V
    .locals 24

    move-object/from16 v1, p0

    sget-object v2, Lf/h/a/c/t0;->a:Lf/h/a/c/t0;

    iget-object v0, v1, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v0, v0, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v3, 0x0

    iput-boolean v3, v1, Lf/h/a/c/b0;->B:Z

    iget-object v0, v1, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    iput-boolean v3, v0, Lf/h/a/c/y;->i:Z

    iget-object v0, v0, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    iget-boolean v4, v0, Lf/h/a/c/i1/v;->e:Z

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lf/h/a/c/i1/v;->c()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lf/h/a/c/i1/v;->a(J)V

    iput-boolean v3, v0, Lf/h/a/c/i1/v;->e:Z

    :cond_0
    const-wide/16 v4, 0x0

    iput-wide v4, v1, Lf/h/a/c/b0;->I:J

    iget-object v4, v1, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v5, v4

    const/4 v6, 0x0

    :goto_0
    const-string v7, "ExoPlayerImplInternal"

    if-ge v6, v5, :cond_1

    aget-object v0, v4, v6

    :try_start_0
    invoke-virtual {v1, v0}, Lf/h/a/c/b0;->f(Lf/h/a/c/p0;)V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_1
    const-string v8, "Disable failed."

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v4, v1, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v5, v4

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v5, :cond_2

    aget-object v0, v4, v6

    :try_start_1
    invoke-interface {v0}, Lf/h/a/c/p0;->reset()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v8, v0

    const-string v0, "Reset failed."

    invoke-static {v7, v0, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_2
    new-array v0, v3, [Lf/h/a/c/p0;

    iput-object v0, v1, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    const/4 v0, 0x0

    if-eqz p3, :cond_3

    iput-object v0, v1, Lf/h/a/c/b0;->H:Lf/h/a/c/b0$e;

    goto :goto_5

    :cond_3
    if-eqz p4, :cond_5

    iget-object v4, v1, Lf/h/a/c/b0;->H:Lf/h/a/c/b0$e;

    if-nez v4, :cond_4

    iget-object v4, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v4, v4, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {v4}, Lf/h/a/c/t0;->p()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v5, v4, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v4, v4, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-object v4, v4, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iget-object v6, v1, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    invoke-virtual {v5, v4, v6}, Lf/h/a/c/t0;->h(Ljava/lang/Object;Lf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    iget-object v4, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v4, v4, Lf/h/a/c/i0;->m:J

    iget-object v6, v1, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    iget-wide v7, v6, Lf/h/a/c/t0$b;->d:J

    add-long/2addr v4, v7

    new-instance v7, Lf/h/a/c/b0$e;

    iget v6, v6, Lf/h/a/c/t0$b;->b:I

    invoke-direct {v7, v2, v6, v4, v5}, Lf/h/a/c/b0$e;-><init>(Lf/h/a/c/t0;IJ)V

    iput-object v7, v1, Lf/h/a/c/b0;->H:Lf/h/a/c/b0$e;

    :cond_4
    const/4 v4, 0x1

    goto :goto_6

    :cond_5
    :goto_5
    move/from16 v4, p3

    :goto_6
    iget-object v5, v1, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    xor-int/lit8 v6, p4, 0x1

    invoke-virtual {v5, v6}, Lf/h/a/c/h0;->b(Z)V

    iput-boolean v3, v1, Lf/h/a/c/b0;->C:Z

    if-eqz p4, :cond_7

    iget-object v5, v1, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iput-object v2, v5, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    iget-object v5, v1, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/h/a/c/b0$c;

    iget-object v6, v6, Lf/h/a/c/b0$c;->d:Lf/h/a/c/n0;

    invoke-virtual {v6, v3}, Lf/h/a/c/n0;->b(Z)V

    goto :goto_7

    :cond_6
    iget-object v5, v1, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    iput v3, v1, Lf/h/a/c/b0;->J:I

    :cond_7
    if-eqz v4, :cond_8

    iget-object v3, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-boolean v5, v1, Lf/h/a/c/b0;->E:Z

    iget-object v6, v1, Lf/h/a/c/b0;->m:Lf/h/a/c/t0$c;

    iget-object v7, v1, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    invoke-virtual {v3, v5, v6, v7}, Lf/h/a/c/i0;->e(ZLf/h/a/c/t0$c;Lf/h/a/c/t0$b;)Lf/h/a/c/d1/p$a;

    move-result-object v3

    goto :goto_8

    :cond_8
    iget-object v3, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v3, v3, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    :goto_8
    move-object/from16 v17, v3

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v4, :cond_9

    move-wide/from16 v22, v5

    goto :goto_9

    :cond_9
    iget-object v3, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v7, v3, Lf/h/a/c/i0;->m:J

    move-wide/from16 v22, v7

    :goto_9
    if-eqz v4, :cond_a

    move-wide v10, v5

    goto :goto_a

    :cond_a
    iget-object v3, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v3, v3, Lf/h/a/c/i0;->d:J

    move-wide v10, v3

    :goto_a
    new-instance v3, Lf/h/a/c/i0;

    if-eqz p4, :cond_b

    goto :goto_b

    :cond_b
    iget-object v2, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    :goto_b
    move-object v6, v2

    iget-object v2, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v12, v2, Lf/h/a/c/i0;->e:I

    if-eqz p5, :cond_c

    move-object v13, v0

    goto :goto_c

    :cond_c
    iget-object v4, v2, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-object v13, v4

    :goto_c
    const/4 v14, 0x0

    if-eqz p4, :cond_d

    sget-object v4, Lcom/google/android/exoplayer2/source/TrackGroupArray;->g:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    goto :goto_d

    :cond_d
    iget-object v4, v2, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    :goto_d
    move-object v15, v4

    if-eqz p4, :cond_e

    iget-object v2, v1, Lf/h/a/c/b0;->g:Lf/h/a/c/f1/i;

    goto :goto_e

    :cond_e
    iget-object v2, v2, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    :goto_e
    move-object/from16 v16, v2

    const-wide/16 v20, 0x0

    move-object v5, v3

    move-object/from16 v7, v17

    move-wide/from16 v8, v22

    move-wide/from16 v18, v22

    invoke-direct/range {v5 .. v23}, Lf/h/a/c/i0;-><init>(Lf/h/a/c/t0;Lf/h/a/c/d1/p$a;JJILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;Lf/h/a/c/d1/p$a;JJJ)V

    iput-object v3, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    if-eqz p2, :cond_f

    iget-object v2, v1, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    if-eqz v2, :cond_f

    invoke-interface {v2, v1}, Lf/h/a/c/d1/p;->b(Lf/h/a/c/d1/p$b;)V

    iput-object v0, v1, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    :cond_f
    return-void
.end method

.method public final B(J)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-wide v0, v0, Lf/h/a/c/f0;->n:J

    add-long/2addr p1, v0

    :goto_0
    iput-wide p1, p0, Lf/h/a/c/b0;->I:J

    iget-object v0, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    iget-object v0, v0, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v0, p1, p2}, Lf/h/a/c/i1/v;->a(J)V

    iget-object p1, p0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length p2, p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p2, :cond_1

    aget-object v2, p1, v1

    iget-wide v3, p0, Lf/h/a/c/b0;->I:J

    invoke-interface {v2, v3, v4}, Lf/h/a/c/p0;->r(J)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object p1, p1, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    :goto_2
    if-eqz p1, :cond_4

    iget-object p2, p1, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object p2, p2, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    invoke-virtual {p2}, Lf/h/a/c/f1/g;->a()[Lf/h/a/c/f1/f;

    move-result-object p2

    array-length v1, p2

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_3

    aget-object v3, p2, v2

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lf/h/a/c/f1/f;->i()V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    iget-object p1, p1, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    goto :goto_2

    :cond_4
    return-void
.end method

.method public final C(Lf/h/a/c/b0$c;)Z
    .locals 11

    iget-object v0, p1, Lf/h/a/c/b0$c;->g:Ljava/lang/Object;

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-nez v0, :cond_5

    iget-object v0, p1, Lf/h/a/c/b0$c;->d:Lf/h/a/c/n0;

    iget-object v3, v0, Lf/h/a/c/n0;->c:Lf/h/a/c/t0;

    iget v7, v0, Lf/h/a/c/n0;->g:I

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    invoke-static {v4, v5}, Lf/h/a/c/u;->a(J)J

    move-result-wide v8

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v4

    const/4 v10, 0x0

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Lf/h/a/c/t0;->p()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v3, v0

    :cond_1
    :try_start_0
    iget-object v5, p0, Lf/h/a/c/b0;->m:Lf/h/a/c/t0$c;

    iget-object v6, p0, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    move-object v4, v3

    invoke-virtual/range {v4 .. v9}, Lf/h/a/c/t0;->j(Lf/h/a/c/t0$c;Lf/h/a/c/t0$b;IJ)Landroid/util/Pair;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v3, :cond_2

    :goto_0
    move-object v10, v4

    goto :goto_1

    :cond_2
    iget-object v3, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v0

    if-eq v0, v1, :cond_3

    goto :goto_0

    :catch_0
    :cond_3
    :goto_1
    if-nez v10, :cond_4

    return v2

    :cond_4
    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v1, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    iput v0, p1, Lf/h/a/c/b0$c;->e:I

    iput-wide v1, p1, Lf/h/a/c/b0$c;->f:J

    iput-object v3, p1, Lf/h/a/c/b0$c;->g:Ljava/lang/Object;

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v3, v3, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {v3, v0}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, v1, :cond_6

    return v2

    :cond_6
    iput v0, p1, Lf/h/a/c/b0$c;->e:I

    :goto_2
    const/4 p1, 0x1

    return p1
.end method

.method public final D(Lf/h/a/c/b0$e;Z)Landroid/util/Pair;
    .locals 10
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/c/b0$e;",
            "Z)",
            "Landroid/util/Pair<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v1, p1, Lf/h/a/c/b0$e;->a:Lf/h/a/c/t0;

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    return-object v3

    :cond_0
    invoke-virtual {v1}, Lf/h/a/c/t0;->p()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v1, v0

    :cond_1
    :try_start_0
    iget-object v5, p0, Lf/h/a/c/b0;->m:Lf/h/a/c/t0$c;

    iget-object v6, p0, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    iget v7, p1, Lf/h/a/c/b0$e;->b:I

    iget-wide v8, p1, Lf/h/a/c/b0$e;->c:J

    move-object v4, v1

    invoke-virtual/range {v4 .. v9}, Lf/h/a/c/t0;->j(Lf/h/a/c/t0$c;Lf/h/a/c/t0$b;IJ)Landroid/util/Pair;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v1, :cond_2

    return-object p1

    :cond_2
    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    return-object p1

    :cond_3
    if-eqz p2, :cond_4

    iget-object p1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, p1, v1, v0}, Lf/h/a/c/b0;->E(Ljava/lang/Object;Lf/h/a/c/t0;Lf/h/a/c/t0;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p2, p0, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    invoke-virtual {v0, p1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result p1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lf/h/a/c/t0;->g(ILf/h/a/c/t0$b;Z)Lf/h/a/c/t0$b;

    move-result-object p1

    iget p1, p1, Lf/h/a/c/t0$b;->b:I

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    invoke-virtual {p0, v0, p1, v1, v2}, Lf/h/a/c/b0;->j(Lf/h/a/c/t0;IJ)Landroid/util/Pair;

    move-result-object p1

    return-object p1

    :catch_0
    :cond_4
    return-object v3
.end method

.method public final E(Ljava/lang/Object;Lf/h/a/c/t0;Lf/h/a/c/t0;)Ljava/lang/Object;
    .locals 9
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-virtual {p2, p1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {p2}, Lf/h/a/c/t0;->i()I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    move v4, p1

    const/4 p1, -0x1

    :goto_0
    if-ge v2, v0, :cond_1

    if-ne p1, v1, :cond_1

    iget-object v5, p0, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    iget-object v6, p0, Lf/h/a/c/b0;->m:Lf/h/a/c/t0$c;

    iget v7, p0, Lf/h/a/c/b0;->D:I

    iget-boolean v8, p0, Lf/h/a/c/b0;->E:Z

    move-object v3, p2

    invoke-virtual/range {v3 .. v8}, Lf/h/a/c/t0;->d(ILf/h/a/c/t0$b;Lf/h/a/c/t0$c;IZ)I

    move-result v4

    if-ne v4, v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p2, v4}, Lf/h/a/c/t0;->l(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p3, p1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result p1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-ne p1, v1, :cond_2

    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {p3, p1}, Lf/h/a/c/t0;->l(I)Ljava/lang/Object;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public final F(JJ)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v0, v0, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    add-long/2addr p1, p3

    iget-object p3, v0, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    invoke-virtual {p3, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    return-void
.end method

.method public final G(Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v0, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v2, v0, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v0, v0, Lf/h/a/c/i0;->m:J

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v0, v1, v3}, Lf/h/a/c/b0;->I(Lf/h/a/c/d1/p$a;JZ)J

    move-result-wide v3

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v0, v0, Lf/h/a/c/i0;->m:J

    cmp-long v5, v3, v0

    if-eqz v5, :cond_0

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v5, v0, Lf/h/a/c/i0;->d:J

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lf/h/a/c/b0$d;->b(I)V

    :cond_0
    return-void
.end method

.method public final H(Lf/h/a/c/b0$e;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v0, p1

    iget-object v1, v7, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lf/h/a/c/b0$d;->a(I)V

    invoke-virtual {v7, v0, v2}, Lf/h/a/c/b0;->D(Lf/h/a/c/b0$e;Z)Landroid/util/Pair;

    move-result-object v1

    const-wide/16 v4, 0x0

    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    if-nez v1, :cond_0

    iget-object v1, v7, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-boolean v6, v7, Lf/h/a/c/b0;->E:Z

    iget-object v10, v7, Lf/h/a/c/b0;->m:Lf/h/a/c/t0$c;

    iget-object v11, v7, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    invoke-virtual {v1, v6, v10, v11}, Lf/h/a/c/i0;->e(ZLf/h/a/c/t0$c;Lf/h/a/c/t0$b;)Lf/h/a/c/d1/p$a;

    move-result-object v1

    move-object v11, v1

    move-wide v12, v8

    move-wide v14, v12

    const/4 v10, 0x1

    goto :goto_2

    :cond_0
    iget-object v6, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v10, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v12, v7, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    invoke-virtual {v12, v6, v10, v11}, Lf/h/a/c/h0;->k(Ljava/lang/Object;J)Lf/h/a/c/d1/p$a;

    move-result-object v6

    invoke-virtual {v6}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v12

    if-eqz v12, :cond_1

    move-wide v12, v4

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-wide v14, v0, Lf/h/a/c/b0$e;->c:J

    cmp-long v1, v14, v8

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_1
    move-wide v14, v10

    move v10, v1

    move-object v11, v6

    :goto_2
    const/4 v6, 0x2

    :try_start_0
    iget-object v1, v7, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    if-eqz v1, :cond_b

    iget v1, v7, Lf/h/a/c/b0;->G:I

    if-lez v1, :cond_3

    goto/16 :goto_6

    :cond_3
    cmp-long v0, v12, v8

    if-nez v0, :cond_4

    const/4 v0, 0x4

    invoke-virtual {v7, v0}, Lf/h/a/c/b0;->R(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x1

    move-object/from16 v1, p0

    const/4 v8, 0x2

    move v6, v0

    :try_start_1
    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/b0;->A(ZZZZZ)V

    goto/16 :goto_7

    :cond_4
    const/4 v8, 0x2

    iget-object v0, v7, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    invoke-virtual {v11, v0}, Lf/h/a/c/d1/p$a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v7, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-eqz v0, :cond_5

    iget-boolean v1, v0, Lf/h/a/c/f0;->d:Z

    if-eqz v1, :cond_5

    cmp-long v1, v12, v4

    if-eqz v1, :cond_5

    iget-object v0, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    iget-object v1, v7, Lf/h/a/c/b0;->v:Lf/h/a/c/r0;

    invoke-interface {v0, v12, v13, v1}, Lf/h/a/c/d1/o;->j(JLf/h/a/c/r0;)J

    move-result-wide v0

    goto :goto_3

    :cond_5
    move-wide v0, v12

    :goto_3
    invoke-static {v0, v1}, Lf/h/a/c/u;->b(J)J

    move-result-wide v4

    iget-object v6, v7, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v2, v6, Lf/h/a/c/i0;->m:J

    invoke-static {v2, v3}, Lf/h/a/c/u;->b(J)J

    move-result-wide v2

    cmp-long v6, v4, v2

    if-nez v6, :cond_8

    iget-object v0, v7, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v3, v0, Lf/h/a/c/i0;->m:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v1, p0

    move-object v2, v11

    move-wide v5, v14

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v0

    iput-object v0, v7, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    if-eqz v10, :cond_6

    iget-object v0, v7, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    invoke-virtual {v0, v8}, Lf/h/a/c/b0$d;->b(I)V

    :cond_6
    return-void

    :cond_7
    move-wide v0, v12

    :cond_8
    :try_start_2
    iget-object v2, v7, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v3, v2, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v2, v2, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-eq v3, v2, :cond_9

    const/4 v2, 0x1

    goto :goto_4

    :cond_9
    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v7, v11, v0, v1, v2}, Lf/h/a/c/b0;->I(Lf/h/a/c/d1/p$a;JZ)J

    move-result-wide v0

    cmp-long v2, v12, v0

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    goto :goto_5

    :cond_a
    const/4 v2, 0x0

    :goto_5
    or-int/2addr v10, v2

    move-wide v3, v0

    goto :goto_8

    :cond_b
    :goto_6
    const/4 v8, 0x2

    iput-object v0, v7, Lf/h/a/c/b0;->H:Lf/h/a/c/b0$e;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_7
    move-wide v3, v12

    :goto_8
    move-object/from16 v1, p0

    move-object v2, v11

    move-wide v5, v14

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v0

    iput-object v0, v7, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    if-eqz v10, :cond_c

    iget-object v0, v7, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    invoke-virtual {v0, v8}, Lf/h/a/c/b0$d;->b(I)V

    :cond_c
    return-void

    :catchall_0
    move-exception v0

    goto :goto_9

    :catchall_1
    move-exception v0

    const/4 v8, 0x2

    :goto_9
    move-object/from16 v1, p0

    move-object v2, v11

    move-wide v3, v12

    move-wide v5, v14

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v1

    iput-object v1, v7, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    if-eqz v10, :cond_d

    iget-object v1, v7, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    invoke-virtual {v1, v8}, Lf/h/a/c/b0$d;->b(I)V

    :cond_d
    throw v0
.end method

.method public final I(Lf/h/a/c/d1/p$a;JZ)J
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/a/c/b0;->U()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/b0;->B:Z

    iget-object v1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v2, v1, Lf/h/a/c/i0;->e:I

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_0

    iget-object v1, v1, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {v1}, Lf/h/a/c/t0;->p()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v3}, Lf/h/a/c/b0;->R(I)V

    :cond_0
    iget-object v1, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v1, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_2

    iget-object v5, v2, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v5, v5, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {p1, v5}, Lf/h/a/c/d1/p$a;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-boolean v5, v2, Lf/h/a/c/f0;->d:Z

    if-eqz v5, :cond_1

    iget-object p1, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    invoke-virtual {p1, v2}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    invoke-virtual {v2}, Lf/h/a/c/h0;->a()Lf/h/a/c/f0;

    move-result-object v2

    goto :goto_0

    :cond_2
    :goto_1
    const-wide/16 v5, 0x0

    if-nez p4, :cond_3

    if-ne v1, v2, :cond_3

    if-eqz v2, :cond_5

    iget-wide v7, v2, Lf/h/a/c/f0;->n:J

    add-long/2addr v7, p2

    cmp-long p1, v7, v5

    if-gez p1, :cond_5

    :cond_3
    iget-object p1, p0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length p4, p1

    const/4 v1, 0x0

    :goto_2
    if-ge v1, p4, :cond_4

    aget-object v7, p1, v1

    invoke-virtual {p0, v7}, Lf/h/a/c/b0;->f(Lf/h/a/c/p0;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    new-array p1, v0, [Lf/h/a/c/p0;

    iput-object p1, p0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    const/4 v1, 0x0

    if-eqz v2, :cond_5

    iput-wide v5, v2, Lf/h/a/c/f0;->n:J

    :cond_5
    if-eqz v2, :cond_7

    invoke-virtual {p0, v1}, Lf/h/a/c/b0;->X(Lf/h/a/c/f0;)V

    iget-boolean p1, v2, Lf/h/a/c/f0;->e:Z

    if-eqz p1, :cond_6

    iget-object p1, v2, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {p1, p2, p3}, Lf/h/a/c/d1/o;->g(J)J

    move-result-wide p1

    iget-object p3, v2, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    iget-wide v1, p0, Lf/h/a/c/b0;->o:J

    sub-long v1, p1, v1

    iget-boolean p4, p0, Lf/h/a/c/b0;->p:Z

    invoke-interface {p3, v1, v2, p4}, Lf/h/a/c/d1/o;->r(JZ)V

    move-wide p2, p1

    :cond_6
    invoke-virtual {p0, p2, p3}, Lf/h/a/c/b0;->B(J)V

    invoke-virtual {p0}, Lf/h/a/c/b0;->v()V

    goto :goto_3

    :cond_7
    iget-object p1, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    invoke-virtual {p1, v4}, Lf/h/a/c/h0;->b(Z)V

    iget-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    sget-object p4, Lcom/google/android/exoplayer2/source/TrackGroupArray;->g:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v1, p0, Lf/h/a/c/b0;->g:Lf/h/a/c/f1/i;

    invoke-virtual {p1, p4, v1}, Lf/h/a/c/i0;->c(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;)Lf/h/a/c/i0;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p0, p2, p3}, Lf/h/a/c/b0;->B(J)V

    :goto_3
    invoke-virtual {p0, v0}, Lf/h/a/c/b0;->n(Z)V

    iget-object p1, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    invoke-virtual {p1, v3}, Lf/h/a/c/i1/x;->c(I)Z

    return-wide p2
.end method

.method public final J(Lf/h/a/c/n0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p1, Lf/h/a/c/n0;->f:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v1, v1, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->d(Lf/h/a/c/n0;)V

    iget-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget p1, p1, Lf/h/a/c/i0;->e:I

    const/4 v0, 0x3

    const/4 v1, 0x2

    if-eq p1, v0, :cond_0

    if-ne p1, v1, :cond_2

    :cond_0
    iget-object p1, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    invoke-virtual {p1, v1}, Lf/h/a/c/i1/x;->c(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lf/h/a/c/i1/x;->b(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final K(Lf/h/a/c/n0;)V
    .locals 2

    iget-object v0, p1, Lf/h/a/c/n0;->f:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "TAG"

    const-string v1, "Trying to send message on a dead thread."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lf/h/a/c/n0;->b(Z)V

    return-void

    :cond_0
    new-instance v1, Lf/h/a/c/o;

    invoke-direct {v1, p0, p1}, Lf/h/a/c/o;-><init>(Lf/h/a/c/b0;Lf/h/a/c/n0;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final L()V
    .locals 5

    iget-object v0, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-interface {v3}, Lf/h/a/c/p0;->i()Lf/h/a/c/d1/v;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Lf/h/a/c/p0;->l()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final M(ZLjava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 4
    .param p2    # Ljava/util/concurrent/atomic/AtomicBoolean;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-boolean v0, p0, Lf/h/a/c/b0;->F:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lf/h/a/c/b0;->F:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    invoke-interface {v2}, Lf/h/a/c/p0;->getState()I

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Lf/h/a/c/p0;->reset()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    monitor-enter p0

    const/4 p1, 0x1

    :try_start_0
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method public final N(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/b0;->B:Z

    iput-boolean p1, p0, Lf/h/a/c/b0;->A:Z

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/b0;->U()V

    invoke-virtual {p0}, Lf/h/a/c/b0;->W()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget p1, p1, Lf/h/a/c/i0;->e:I

    const/4 v0, 0x3

    const/4 v1, 0x2

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lf/h/a/c/b0;->S()V

    iget-object p1, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    invoke-virtual {p1, v1}, Lf/h/a/c/i1/x;->c(I)Z

    goto :goto_0

    :cond_1
    if-ne p1, v1, :cond_2

    iget-object p1, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    invoke-virtual {p1, v1}, Lf/h/a/c/i1/x;->c(I)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public final O(Lf/h/a/c/j0;)V
    .locals 4

    iget-object v0, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    invoke-virtual {v0, p1}, Lf/h/a/c/y;->e(Lf/h/a/c/j0;)V

    iget-object p1, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    invoke-virtual {p1}, Lf/h/a/c/y;->b()Lf/h/a/c/j0;

    move-result-object p1

    iget-object v0, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v0, v0, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    const/16 v1, 0x11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final P(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iput p1, p0, Lf/h/a/c/b0;->D:I

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iput p1, v0, Lf/h/a/c/h0;->e:I

    invoke-virtual {v0}, Lf/h/a/c/h0;->m()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->G(Z)V

    :cond_0
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->n(Z)V

    return-void
.end method

.method public final Q(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iput-boolean p1, p0, Lf/h/a/c/b0;->E:Z

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iput-boolean p1, v0, Lf/h/a/c/h0;->f:Z

    invoke-virtual {v0}, Lf/h/a/c/h0;->m()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->G(Z)V

    :cond_0
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->n(Z)V

    return-void
.end method

.method public final R(I)V
    .locals 24

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v2, v1, Lf/h/a/c/i0;->e:I

    move/from16 v10, p1

    if-eq v2, v10, :cond_0

    new-instance v2, Lf/h/a/c/i0;

    move-object v3, v2

    iget-object v4, v1, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v5, v1, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v6, v1, Lf/h/a/c/i0;->c:J

    iget-wide v8, v1, Lf/h/a/c/i0;->d:J

    iget-object v11, v1, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v12, v1, Lf/h/a/c/i0;->g:Z

    iget-object v13, v1, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v14, v1, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    iget-object v15, v1, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    move-object/from16 v22, v2

    move-object/from16 v23, v3

    iget-wide v2, v1, Lf/h/a/c/i0;->k:J

    move-wide/from16 v16, v2

    iget-wide v2, v1, Lf/h/a/c/i0;->l:J

    move-wide/from16 v18, v2

    iget-wide v1, v1, Lf/h/a/c/i0;->m:J

    move-wide/from16 v20, v1

    move/from16 v10, p1

    move-object/from16 v3, v23

    invoke-direct/range {v3 .. v21}, Lf/h/a/c/i0;-><init>(Lf/h/a/c/t0;Lf/h/a/c/d1/p$a;JJILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;Lf/h/a/c/d1/p$a;JJJ)V

    move-object/from16 v1, v22

    iput-object v1, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    :cond_0
    return-void
.end method

.method public final S()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/b0;->B:Z

    iget-object v1, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lf/h/a/c/y;->i:Z

    iget-object v1, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v1}, Lf/h/a/c/i1/v;->d()V

    iget-object v1, p0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3}, Lf/h/a/c/p0;->start()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final T(ZZZ)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lf/h/a/c/b0;->F:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    const/4 v4, 0x1

    move-object v2, p0

    move v5, p2

    move v6, p2

    move v7, p2

    invoke-virtual/range {v2 .. v7}, Lf/h/a/c/b0;->A(ZZZZZ)V

    iget-object p1, p0, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    iget p2, p0, Lf/h/a/c/b0;->G:I

    add-int/2addr p2, p3

    invoke-virtual {p1, p2}, Lf/h/a/c/b0$d;->a(I)V

    iput v0, p0, Lf/h/a/c/b0;->G:I

    iget-object p1, p0, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    invoke-interface {p1}, Lf/h/a/c/e0;->i()V

    invoke-virtual {p0, v1}, Lf/h/a/c/b0;->R(I)V

    return-void
.end method

.method public final U()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/h/a/c/y;->i:Z

    iget-object v0, v0, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    iget-boolean v2, v0, Lf/h/a/c/i1/v;->e:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lf/h/a/c/i1/v;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lf/h/a/c/i1/v;->a(J)V

    iput-boolean v1, v0, Lf/h/a/c/i1/v;->e:Z

    :cond_0
    iget-object v0, p0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    invoke-interface {v3}, Lf/h/a/c/p0;->getState()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    invoke-interface {v3}, Lf/h/a/c/p0;->stop()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final V()V
    .locals 23

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v1, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iget-boolean v2, v0, Lf/h/a/c/b0;->C:Z

    if-nez v2, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, v1, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v1}, Lf/h/a/c/d1/o;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v11, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    const/4 v11, 0x1

    :goto_1
    iget-object v1, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-boolean v2, v1, Lf/h/a/c/i0;->g:Z

    if-eq v11, v2, :cond_2

    new-instance v15, Lf/h/a/c/i0;

    move-object v2, v15

    iget-object v3, v1, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v4, v1, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v5, v1, Lf/h/a/c/i0;->c:J

    iget-wide v7, v1, Lf/h/a/c/i0;->d:J

    iget v9, v1, Lf/h/a/c/i0;->e:I

    iget-object v10, v1, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-object v12, v1, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v13, v1, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    iget-object v14, v1, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    move-object/from16 v21, v2

    move-object/from16 v22, v3

    iget-wide v2, v1, Lf/h/a/c/i0;->k:J

    move-object v0, v15

    move-wide v15, v2

    iget-wide v2, v1, Lf/h/a/c/i0;->l:J

    move-wide/from16 v17, v2

    iget-wide v1, v1, Lf/h/a/c/i0;->m:J

    move-wide/from16 v19, v1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-direct/range {v2 .. v20}, Lf/h/a/c/i0;-><init>(Lf/h/a/c/t0;Lf/h/a/c/d1/p$a;JJILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;Lf/h/a/c/d1/p$a;JJJ)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    goto :goto_2

    :cond_2
    move-object v1, v0

    :goto_2
    return-void
.end method

.method public final W()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v1, v0, Lf/h/a/c/f0;->d:Z

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v1, :cond_1

    iget-object v1, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v1}, Lf/h/a/c/d1/o;->l()J

    move-result-wide v4

    move-wide v8, v4

    goto :goto_0

    :cond_1
    move-wide v8, v2

    :goto_0
    cmp-long v1, v8, v2

    if-eqz v1, :cond_2

    invoke-virtual {p0, v8, v9}, Lf/h/a/c/b0;->B(J)V

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v0, v0, Lf/h/a/c/i0;->m:J

    cmp-long v2, v8, v0

    if-eqz v2, :cond_16

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v7, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v10, v0, Lf/h/a/c/i0;->d:J

    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, p0, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lf/h/a/c/b0$d;->b(I)V

    goto/16 :goto_a

    :cond_2
    iget-object v1, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    iget-object v2, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v2, v2, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq v0, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    iget-object v5, v1, Lf/h/a/c/y;->f:Lf/h/a/c/p0;

    if-eqz v5, :cond_5

    invoke-interface {v5}, Lf/h/a/c/p0;->g()Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, v1, Lf/h/a/c/y;->f:Lf/h/a/c/p0;

    invoke-interface {v5}, Lf/h/a/c/p0;->a()Z

    move-result v5

    if-nez v5, :cond_4

    if-nez v2, :cond_5

    iget-object v2, v1, Lf/h/a/c/y;->f:Lf/h/a/c/p0;

    invoke-interface {v2}, Lf/h/a/c/p0;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_6

    iput-boolean v3, v1, Lf/h/a/c/y;->h:Z

    iget-boolean v2, v1, Lf/h/a/c/y;->i:Z

    if-eqz v2, :cond_a

    iget-object v2, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v2}, Lf/h/a/c/i1/v;->d()V

    goto :goto_4

    :cond_6
    iget-object v2, v1, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    invoke-interface {v2}, Lf/h/a/c/i1/n;->c()J

    move-result-wide v5

    iget-boolean v2, v1, Lf/h/a/c/y;->h:Z

    if-eqz v2, :cond_8

    iget-object v2, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v2}, Lf/h/a/c/i1/v;->c()J

    move-result-wide v7

    cmp-long v2, v5, v7

    if-gez v2, :cond_7

    iget-object v2, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    iget-boolean v5, v2, Lf/h/a/c/i1/v;->e:Z

    if-eqz v5, :cond_a

    invoke-virtual {v2}, Lf/h/a/c/i1/v;->c()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lf/h/a/c/i1/v;->a(J)V

    iput-boolean v4, v2, Lf/h/a/c/i1/v;->e:Z

    goto :goto_4

    :cond_7
    iput-boolean v4, v1, Lf/h/a/c/y;->h:Z

    iget-boolean v2, v1, Lf/h/a/c/y;->i:Z

    if-eqz v2, :cond_8

    iget-object v2, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v2}, Lf/h/a/c/i1/v;->d()V

    :cond_8
    iget-object v2, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v2, v5, v6}, Lf/h/a/c/i1/v;->a(J)V

    iget-object v2, v1, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    invoke-interface {v2}, Lf/h/a/c/i1/n;->b()Lf/h/a/c/j0;

    move-result-object v2

    iget-object v5, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    iget-object v5, v5, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    invoke-virtual {v2, v5}, Lf/h/a/c/j0;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    iget-object v5, v1, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    iget-boolean v6, v5, Lf/h/a/c/i1/v;->e:Z

    if-eqz v6, :cond_9

    invoke-virtual {v5}, Lf/h/a/c/i1/v;->c()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lf/h/a/c/i1/v;->a(J)V

    :cond_9
    iput-object v2, v5, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    iget-object v5, v1, Lf/h/a/c/y;->e:Lf/h/a/c/y$a;

    check-cast v5, Lf/h/a/c/b0;

    iget-object v5, v5, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v5, v5, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    const/16 v6, 0x11

    invoke-virtual {v5, v6, v4, v4, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    :cond_a
    :goto_4
    invoke-virtual {v1}, Lf/h/a/c/y;->c()J

    move-result-wide v1

    iput-wide v1, p0, Lf/h/a/c/b0;->I:J

    iget-wide v5, v0, Lf/h/a/c/f0;->n:J

    sub-long/2addr v1, v5

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v5, v0, Lf/h/a/c/i0;->m:J

    iget-object v0, p0, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    invoke-virtual {v0}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    goto/16 :goto_9

    :cond_b
    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v7, v0, Lf/h/a/c/i0;->c:J

    cmp-long v9, v7, v5

    if-nez v9, :cond_c

    iget-boolean v7, p0, Lf/h/a/c/b0;->K:Z

    if-eqz v7, :cond_c

    const-wide/16 v7, 0x1

    sub-long/2addr v5, v7

    :cond_c
    iput-boolean v4, p0, Lf/h/a/c/b0;->K:Z

    iget-object v4, v0, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-object v0, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v4, v0}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v0

    iget v4, p0, Lf/h/a/c/b0;->J:I

    const/4 v7, 0x0

    if-lez v4, :cond_d

    iget-object v8, p0, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/b0$c;

    move-wide v10, v1

    move-wide v8, v5

    move-object v5, p0

    goto :goto_6

    :cond_d
    move-object v4, p0

    move-wide v8, v1

    :goto_5
    move-wide v10, v8

    move-wide v8, v5

    move-object v5, v4

    move-object v4, v7

    :goto_6
    if-eqz v4, :cond_10

    iget v6, v4, Lf/h/a/c/b0$c;->e:I

    if-gt v6, v0, :cond_e

    if-ne v6, v0, :cond_10

    iget-wide v12, v4, Lf/h/a/c/b0$c;->f:J

    cmp-long v4, v12, v8

    if-lez v4, :cond_10

    :cond_e
    iget v4, v5, Lf/h/a/c/b0;->J:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v5, Lf/h/a/c/b0;->J:I

    if-lez v4, :cond_f

    iget-object v6, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/b0$c;

    goto :goto_6

    :cond_f
    move-object v4, v5

    move-wide v5, v8

    move-wide v8, v10

    goto :goto_5

    :cond_10
    iget v4, v5, Lf/h/a/c/b0;->J:I

    iget-object v6, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_11

    iget-object v4, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    iget v6, v5, Lf/h/a/c/b0;->J:I

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/b0$c;

    goto :goto_7

    :cond_11
    move-object v4, v7

    :goto_7
    if-eqz v4, :cond_13

    iget-object v6, v4, Lf/h/a/c/b0$c;->g:Ljava/lang/Object;

    if-eqz v6, :cond_13

    iget v6, v4, Lf/h/a/c/b0$c;->e:I

    if-lt v6, v0, :cond_12

    if-ne v6, v0, :cond_13

    iget-wide v12, v4, Lf/h/a/c/b0$c;->f:J

    cmp-long v6, v12, v8

    if-gtz v6, :cond_13

    :cond_12
    iget v4, v5, Lf/h/a/c/b0;->J:I

    add-int/2addr v4, v3

    iput v4, v5, Lf/h/a/c/b0;->J:I

    iget-object v6, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_11

    iget-object v4, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    iget v6, v5, Lf/h/a/c/b0;->J:I

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/b0$c;

    goto :goto_7

    :cond_13
    :goto_8
    if-eqz v4, :cond_15

    iget-object v3, v4, Lf/h/a/c/b0$c;->g:Ljava/lang/Object;

    if-eqz v3, :cond_15

    iget v3, v4, Lf/h/a/c/b0$c;->e:I

    if-ne v3, v0, :cond_15

    iget-wide v12, v4, Lf/h/a/c/b0$c;->f:J

    cmp-long v3, v12, v8

    if-lez v3, :cond_15

    cmp-long v3, v12, v10

    if-gtz v3, :cond_15

    :try_start_0
    iget-object v3, v4, Lf/h/a/c/b0$c;->d:Lf/h/a/c/n0;

    invoke-virtual {v5, v3}, Lf/h/a/c/b0;->J(Lf/h/a/c/n0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, v4, Lf/h/a/c/b0$c;->d:Lf/h/a/c/n0;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    iget v4, v5, Lf/h/a/c/b0;->J:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget v3, v5, Lf/h/a/c/b0;->J:I

    iget-object v4, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_14

    iget-object v3, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    iget v4, v5, Lf/h/a/c/b0;->J:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lf/h/a/c/b0$c;

    goto :goto_8

    :cond_14
    move-object v4, v7

    goto :goto_8

    :catchall_0
    move-exception v0

    iget-object v1, v4, Lf/h/a/c/b0$c;->d:Lf/h/a/c/n0;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    iget v2, v5, Lf/h/a/c/b0;->J:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    throw v0

    :cond_15
    :goto_9
    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iput-wide v1, v0, Lf/h/a/c/i0;->m:J

    :cond_16
    :goto_a
    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iget-object v1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {v0}, Lf/h/a/c/f0;->d()J

    move-result-wide v2

    iput-wide v2, v1, Lf/h/a/c/i0;->k:J

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p0}, Lf/h/a/c/b0;->k()J

    move-result-wide v1

    iput-wide v1, v0, Lf/h/a/c/i0;->l:J

    return-void
.end method

.method public final X(Lf/h/a/c/f0;)V
    .locals 8
    .param p1    # Lf/h/a/c/f0;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-eqz v0, :cond_6

    if-ne p1, v0, :cond_0

    goto :goto_2

    :cond_0
    iget-object v1, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v1, v1

    new-array v1, v1, [Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    iget-object v5, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v6, v5

    if-ge v3, v6, :cond_5

    aget-object v5, v5, v3

    invoke-interface {v5}, Lf/h/a/c/p0;->getState()I

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    :goto_1
    aput-boolean v6, v1, v3

    iget-object v6, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    invoke-virtual {v6, v3}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v6

    if-eqz v6, :cond_2

    add-int/lit8 v4, v4, 0x1

    :cond_2
    aget-boolean v6, v1, v3

    if-eqz v6, :cond_4

    iget-object v6, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    invoke-virtual {v6, v3}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Lf/h/a/c/p0;->s()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Lf/h/a/c/p0;->i()Lf/h/a/c/d1/v;

    move-result-object v6

    iget-object v7, p1, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v7, v7, v3

    if-ne v6, v7, :cond_4

    :cond_3
    invoke-virtual {p0, v5}, Lf/h/a/c/b0;->f(Lf/h/a/c/p0;)V

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    iget-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v2, v0, Lf/h/a/c/f0;->l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v0, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    invoke-virtual {p1, v2, v0}, Lf/h/a/c/i0;->c(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;)Lf/h/a/c/i0;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p0, v1, v4}, Lf/h/a/c/b0;->h([ZI)V

    :cond_6
    :goto_2
    return-void
.end method

.method public a(Lf/h/a/c/d1/p;Lf/h/a/c/t0;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    new-instance v1, Lf/h/a/c/b0$b;

    invoke-direct {v1, p1, p2}, Lf/h/a/c/b0$b;-><init>(Lf/h/a/c/d1/p;Lf/h/a/c/t0;)V

    const/16 p1, 0x8

    invoke-virtual {v0, p1, v1}, Lf/h/a/c/i1/x;->b(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;
    .locals 9

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/b0;->K:Z

    iget-object v1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p0}, Lf/h/a/c/b0;->k()J

    move-result-wide v7

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-virtual/range {v1 .. v8}, Lf/h/a/c/i0;->a(Lf/h/a/c/d1/p$a;JJJ)Lf/h/a/c/i0;

    move-result-object p1

    return-object p1
.end method

.method public c(Lf/h/a/c/d1/w;)V
    .locals 2

    check-cast p1, Lf/h/a/c/d1/o;

    iget-object v0, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, Lf/h/a/c/i1/x;->b(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final d(Lf/h/a/c/n0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    invoke-virtual {p1}, Lf/h/a/c/n0;->a()Z

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p1, Lf/h/a/c/n0;->a:Lf/h/a/c/n0$b;

    iget v2, p1, Lf/h/a/c/n0;->d:I

    iget-object v3, p1, Lf/h/a/c/n0;->e:Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Lf/h/a/c/n0$b;->d(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1, v0}, Lf/h/a/c/n0;->b(Z)V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {p1, v0}, Lf/h/a/c/n0;->b(Z)V

    throw v1
.end method

.method public e(Lf/h/a/c/d1/o;)V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Lf/h/a/c/i1/x;->b(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final f(Lf/h/a/c/p0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    iget-object v1, v0, Lf/h/a/c/y;->f:Lf/h/a/c/p0;

    if-ne p1, v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    iput-object v1, v0, Lf/h/a/c/y;->f:Lf/h/a/c/p0;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/h/a/c/y;->h:Z

    :cond_0
    invoke-interface {p1}, Lf/h/a/c/p0;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Lf/h/a/c/p0;->stop()V

    :cond_1
    invoke-interface {p1}, Lf/h/a/c/p0;->h()V

    return-void
.end method

.method public final g()V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v6, p0

    iget-object v0, v6, Lf/h/a/c/b0;->t:Lf/h/a/c/i1/f;

    invoke-interface {v0}, Lf/h/a/c/i1/f;->a()J

    move-result-wide v7

    iget-object v0, v6, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v13, 0x1

    if-nez v0, :cond_0

    :goto_0
    const/4 v9, 0x0

    goto/16 :goto_14

    :cond_0
    iget v1, v6, Lf/h/a/c/b0;->G:I

    if-lez v1, :cond_1

    invoke-interface {v0}, Lf/h/a/c/d1/p;->d()V

    goto :goto_0

    :cond_1
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-wide v1, v6, Lf/h/a/c/b0;->I:J

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/h0;->i(J)V

    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-eqz v1, :cond_3

    iget-object v2, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-boolean v2, v2, Lf/h/a/c/g0;->g:Z

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lf/h/a/c/f0;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iget-object v1, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v1, v1, Lf/h/a/c/g0;->e:J

    cmp-long v3, v1, v9

    if-eqz v3, :cond_2

    iget v0, v0, Lf/h/a/c/h0;->j:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_d

    iget-object v14, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-wide v0, v6, Lf/h/a/c/b0;->I:J

    iget-object v2, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v3, v14, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-nez v3, :cond_4

    iget-object v15, v2, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v0, v2, Lf/h/a/c/i0;->d:J

    iget-wide v2, v2, Lf/h/a/c/i0;->c:J

    move-wide/from16 v16, v0

    move-wide/from16 v18, v2

    invoke-virtual/range {v14 .. v19}, Lf/h/a/c/h0;->d(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/g0;

    move-result-object v0

    goto :goto_3

    :cond_4
    invoke-virtual {v14, v3, v0, v1}, Lf/h/a/c/h0;->c(Lf/h/a/c/f0;J)Lf/h/a/c/g0;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_7

    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-eqz v0, :cond_6

    iget-object v0, v6, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v1, :cond_6

    aget-object v3, v0, v2

    invoke-interface {v3}, Lf/h/a/c/p0;->j()Z

    move-result v3

    if-nez v3, :cond_5

    goto/16 :goto_7

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, v6, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    invoke-interface {v0}, Lf/h/a/c/d1/p;->d()V

    goto/16 :goto_7

    :cond_7
    iget-object v1, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v15, v6, Lf/h/a/c/b0;->e:[Lf/h/a/c/t;

    iget-object v2, v6, Lf/h/a/c/b0;->f:Lf/h/a/c/f1/h;

    iget-object v3, v6, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    invoke-interface {v3}, Lf/h/a/c/e0;->h()Lf/h/a/c/h1/d;

    move-result-object v19

    iget-object v3, v6, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    iget-object v4, v6, Lf/h/a/c/b0;->g:Lf/h/a/c/f1/i;

    iget-object v5, v1, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-nez v5, :cond_9

    iget-object v5, v0, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {v5}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-wide v11, v0, Lf/h/a/c/g0;->c:J

    cmp-long v5, v11, v9

    if-eqz v5, :cond_8

    goto :goto_5

    :cond_8
    const-wide/16 v11, 0x0

    goto :goto_5

    :cond_9
    iget-wide v11, v5, Lf/h/a/c/f0;->n:J

    iget-object v5, v5, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v9, v5, Lf/h/a/c/g0;->e:J

    add-long/2addr v11, v9

    iget-wide v9, v0, Lf/h/a/c/g0;->b:J

    sub-long/2addr v11, v9

    :goto_5
    move-wide/from16 v16, v11

    new-instance v5, Lf/h/a/c/f0;

    move-object v14, v5

    move-object/from16 v18, v2

    move-object/from16 v20, v3

    move-object/from16 v21, v0

    move-object/from16 v22, v4

    invoke-direct/range {v14 .. v22}, Lf/h/a/c/f0;-><init>([Lf/h/a/c/t;JLf/h/a/c/f1/h;Lf/h/a/c/h1/d;Lf/h/a/c/d1/p;Lf/h/a/c/g0;Lf/h/a/c/f1/i;)V

    iget-object v2, v1, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-eqz v2, :cond_b

    iget-object v3, v2, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-ne v5, v3, :cond_a

    goto :goto_6

    :cond_a
    invoke-virtual {v2}, Lf/h/a/c/f0;->b()V

    iput-object v5, v2, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    invoke-virtual {v2}, Lf/h/a/c/f0;->c()V

    goto :goto_6

    :cond_b
    iput-object v5, v1, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iput-object v5, v1, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    :goto_6
    const/4 v2, 0x0

    iput-object v2, v1, Lf/h/a/c/h0;->k:Ljava/lang/Object;

    iput-object v5, v1, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iget v2, v1, Lf/h/a/c/h0;->j:I

    add-int/2addr v2, v13

    iput v2, v1, Lf/h/a/c/h0;->j:I

    iget-object v1, v5, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    iget-wide v2, v0, Lf/h/a/c/g0;->b:J

    invoke-interface {v1, v6, v2, v3}, Lf/h/a/c/d1/o;->m(Lf/h/a/c/d1/o$a;J)V

    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-ne v0, v5, :cond_c

    iget-object v0, v5, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v0, v0, Lf/h/a/c/g0;->b:J

    iget-wide v2, v5, Lf/h/a/c/f0;->n:J

    add-long/2addr v0, v2

    invoke-virtual {v6, v0, v1}, Lf/h/a/c/b0;->B(J)V

    :cond_c
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lf/h/a/c/b0;->n(Z)V

    goto :goto_8

    :cond_d
    :goto_7
    const/4 v9, 0x0

    :goto_8
    iget-boolean v0, v6, Lf/h/a/c/b0;->C:Z

    if-eqz v0, :cond_e

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->t()Z

    move-result v0

    iput-boolean v0, v6, Lf/h/a/c/b0;->C:Z

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->V()V

    goto :goto_9

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->v()V

    :goto_9
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-nez v0, :cond_f

    goto/16 :goto_f

    :cond_f
    iget-object v1, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-nez v1, :cond_11

    iget-object v1, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-boolean v1, v1, Lf/h/a/c/g0;->g:Z

    if-eqz v1, :cond_19

    const/4 v1, 0x0

    :goto_a
    iget-object v2, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v3, v2

    if-ge v1, v3, :cond_19

    aget-object v2, v2, v1

    iget-object v3, v0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v3, v3, v1

    if-eqz v3, :cond_10

    invoke-interface {v2}, Lf/h/a/c/p0;->i()Lf/h/a/c/d1/v;

    move-result-object v4

    if-ne v4, v3, :cond_10

    invoke-interface {v2}, Lf/h/a/c/p0;->j()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v2}, Lf/h/a/c/p0;->l()V

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->s()Z

    move-result v1

    if-nez v1, :cond_12

    goto/16 :goto_f

    :cond_12
    iget-object v1, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    iget-boolean v1, v1, Lf/h/a/c/f0;->d:Z

    if-nez v1, :cond_13

    goto/16 :goto_f

    :cond_13
    iget-object v0, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v1, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v2, v1, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-eqz v2, :cond_14

    iget-object v2, v2, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-eqz v2, :cond_14

    const/4 v2, 0x1

    goto :goto_b

    :cond_14
    const/4 v2, 0x0

    :goto_b
    invoke-static {v2}, Lf/g/j/k/a;->s(Z)V

    iget-object v2, v1, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    iget-object v2, v2, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    iput-object v2, v1, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    iget-object v1, v2, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v3, v2, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v3}, Lf/h/a/c/d1/o;->l()J

    move-result-wide v3

    const-wide v10, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v3, v10

    if-eqz v5, :cond_15

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->L()V

    goto :goto_f

    :cond_15
    const/4 v3, 0x0

    :goto_c
    iget-object v4, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v5, v4

    if-ge v3, v5, :cond_19

    aget-object v4, v4, v3

    invoke-virtual {v0, v3}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v5

    if-eqz v5, :cond_18

    invoke-interface {v4}, Lf/h/a/c/p0;->s()Z

    move-result v5

    if-nez v5, :cond_18

    iget-object v5, v1, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget-object v5, v5, Lf/h/a/c/f1/g;->b:[Lf/h/a/c/f1/f;

    aget-object v5, v5, v3

    invoke-virtual {v1, v3}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v10

    iget-object v11, v6, Lf/h/a/c/b0;->e:[Lf/h/a/c/t;

    aget-object v11, v11, v3

    iget v11, v11, Lf/h/a/c/t;->d:I

    const/4 v12, 0x6

    if-ne v11, v12, :cond_16

    const/4 v11, 0x1

    goto :goto_d

    :cond_16
    const/4 v11, 0x0

    :goto_d
    iget-object v12, v0, Lf/h/a/c/f1/i;->b:[Lf/h/a/c/q0;

    aget-object v12, v12, v3

    iget-object v14, v1, Lf/h/a/c/f1/i;->b:[Lf/h/a/c/q0;

    aget-object v14, v14, v3

    if-eqz v10, :cond_17

    invoke-virtual {v14, v12}, Lf/h/a/c/q0;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_17

    if-nez v11, :cond_17

    invoke-static {v5}, Lf/h/a/c/b0;->i(Lf/h/a/c/f1/f;)[Lcom/google/android/exoplayer2/Format;

    move-result-object v5

    iget-object v10, v2, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v10, v10, v3

    iget-wide v11, v2, Lf/h/a/c/f0;->n:J

    invoke-interface {v4, v5, v10, v11, v12}, Lf/h/a/c/p0;->v([Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d1/v;J)V

    goto :goto_e

    :cond_17
    invoke-interface {v4}, Lf/h/a/c/p0;->l()V

    :cond_18
    :goto_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_19
    :goto_f
    const/4 v0, 0x0

    :goto_10
    iget-boolean v1, v6, Lf/h/a/c/b0;->A:Z

    if-nez v1, :cond_1a

    goto :goto_11

    :cond_1a
    iget-object v1, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v2, v1, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-nez v2, :cond_1b

    goto :goto_11

    :cond_1b
    iget-object v3, v2, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-nez v3, :cond_1c

    goto :goto_11

    :cond_1c
    iget-object v1, v1, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-ne v2, v1, :cond_1d

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->s()Z

    move-result v1

    if-nez v1, :cond_1d

    goto :goto_11

    :cond_1d
    iget-wide v1, v6, Lf/h/a/c/b0;->I:J

    iget-object v4, v3, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v4, v4, Lf/h/a/c/g0;->b:J

    iget-wide v10, v3, Lf/h/a/c/f0;->n:J

    add-long/2addr v4, v10

    cmp-long v3, v1, v4

    if-ltz v3, :cond_1e

    const/4 v1, 0x1

    goto :goto_12

    :cond_1e
    :goto_11
    const/4 v1, 0x0

    :goto_12
    if-eqz v1, :cond_22

    if-eqz v0, :cond_1f

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->w()V

    :cond_1f
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v10, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v0, v0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-ne v10, v0, :cond_20

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->L()V

    :cond_20
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    invoke-virtual {v0}, Lf/h/a/c/h0;->a()Lf/h/a/c/f0;

    move-result-object v0

    invoke-virtual {v6, v10}, Lf/h/a/c/b0;->X(Lf/h/a/c/f0;)V

    iget-object v0, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v1, v0, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v2, v0, Lf/h/a/c/g0;->b:J

    iget-wide v4, v0, Lf/h/a/c/g0;->c:J

    move-object/from16 v0, p0

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v0

    iput-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v10, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-boolean v0, v0, Lf/h/a/c/g0;->f:Z

    if-eqz v0, :cond_21

    const/4 v0, 0x0

    goto :goto_13

    :cond_21
    const/4 v0, 0x3

    :goto_13
    iget-object v1, v6, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    invoke-virtual {v1, v0}, Lf/h/a/c/b0$d;->b(I)V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->W()V

    const/4 v0, 0x1

    goto :goto_10

    :cond_22
    :goto_14
    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    const/4 v1, 0x2

    if-eq v0, v13, :cond_40

    const/4 v2, 0x4

    if-ne v0, v2, :cond_23

    goto/16 :goto_23

    :cond_23
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    const-wide/16 v3, 0xa

    if-nez v0, :cond_24

    invoke-virtual {v6, v7, v8, v3, v4}, Lf/h/a/c/b0;->F(JJ)V

    return-void

    :cond_24
    const-string v5, "doSomeWork"

    invoke-static {v5}, Lf/g/j/k/a;->c(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->W()V

    iget-boolean v5, v0, Lf/h/a/c/f0;->d:Z

    const-wide/16 v10, 0x3e8

    if-eqz v5, :cond_2d

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    mul-long v14, v14, v10

    iget-object v5, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    iget-object v12, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v9, v12, Lf/h/a/c/i0;->m:J

    iget-wide v11, v6, Lf/h/a/c/b0;->o:J

    sub-long/2addr v9, v11

    iget-boolean v11, v6, Lf/h/a/c/b0;->p:Z

    invoke-interface {v5, v9, v10, v11}, Lf/h/a/c/d1/o;->r(JZ)V

    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x1

    :goto_15
    iget-object v11, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v12, v11

    if-ge v5, v12, :cond_2e

    aget-object v11, v11, v5

    invoke-interface {v11}, Lf/h/a/c/p0;->getState()I

    move-result v12

    if-nez v12, :cond_25

    goto :goto_1c

    :cond_25
    iget-wide v3, v6, Lf/h/a/c/b0;->I:J

    invoke-interface {v11, v3, v4, v14, v15}, Lf/h/a/c/p0;->n(JJ)V

    if-eqz v9, :cond_26

    invoke-interface {v11}, Lf/h/a/c/p0;->g()Z

    move-result v3

    if-eqz v3, :cond_26

    const/4 v9, 0x1

    goto :goto_16

    :cond_26
    const/4 v9, 0x0

    :goto_16
    iget-object v3, v0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v3, v3, v5

    invoke-interface {v11}, Lf/h/a/c/p0;->i()Lf/h/a/c/d1/v;

    move-result-object v4

    if-eq v3, v4, :cond_27

    const/4 v3, 0x1

    goto :goto_17

    :cond_27
    const/4 v3, 0x0

    :goto_17
    if-nez v3, :cond_28

    iget-object v4, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-eqz v4, :cond_28

    invoke-interface {v11}, Lf/h/a/c/p0;->j()Z

    move-result v4

    if-eqz v4, :cond_28

    const/4 v4, 0x1

    goto :goto_18

    :cond_28
    const/4 v4, 0x0

    :goto_18
    if-nez v3, :cond_2a

    if-nez v4, :cond_2a

    invoke-interface {v11}, Lf/h/a/c/p0;->a()Z

    move-result v3

    if-nez v3, :cond_2a

    invoke-interface {v11}, Lf/h/a/c/p0;->g()Z

    move-result v3

    if-eqz v3, :cond_29

    goto :goto_19

    :cond_29
    const/4 v3, 0x0

    goto :goto_1a

    :cond_2a
    :goto_19
    const/4 v3, 0x1

    :goto_1a
    if-eqz v10, :cond_2b

    if-eqz v3, :cond_2b

    const/4 v10, 0x1

    goto :goto_1b

    :cond_2b
    const/4 v10, 0x0

    :goto_1b
    if-nez v3, :cond_2c

    invoke-interface {v11}, Lf/h/a/c/p0;->p()V

    :cond_2c
    :goto_1c
    add-int/lit8 v5, v5, 0x1

    const-wide/16 v3, 0xa

    goto :goto_15

    :cond_2d
    iget-object v3, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v3}, Lf/h/a/c/d1/o;->f()V

    const/4 v9, 0x1

    const/4 v10, 0x1

    :cond_2e
    iget-object v3, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v3, v3, Lf/h/a/c/g0;->e:J

    if-eqz v9, :cond_30

    iget-boolean v5, v0, Lf/h/a/c/f0;->d:Z

    if-eqz v5, :cond_30

    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v3, v11

    if-eqz v5, :cond_2f

    iget-object v5, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v11, v5, Lf/h/a/c/i0;->m:J

    cmp-long v5, v3, v11

    if-gtz v5, :cond_30

    :cond_2f
    iget-object v0, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-boolean v0, v0, Lf/h/a/c/g0;->g:Z

    if-eqz v0, :cond_30

    invoke-virtual {v6, v2}, Lf/h/a/c/b0;->R(I)V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->U()V

    goto/16 :goto_20

    :cond_30
    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v3, v0, Lf/h/a/c/i0;->e:I

    if-ne v3, v1, :cond_37

    iget-object v3, v6, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v3, v3

    if-nez v3, :cond_31

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->u()Z

    move-result v13

    goto :goto_1f

    :cond_31
    if-nez v10, :cond_32

    goto :goto_1e

    :cond_32
    iget-boolean v0, v0, Lf/h/a/c/i0;->g:Z

    if-nez v0, :cond_33

    goto :goto_1f

    :cond_33
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    invoke-virtual {v0}, Lf/h/a/c/f0;->e()Z

    move-result v3

    if-eqz v3, :cond_34

    iget-object v0, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-boolean v0, v0, Lf/h/a/c/g0;->g:Z

    if-eqz v0, :cond_34

    const/4 v0, 0x1

    goto :goto_1d

    :cond_34
    const/4 v0, 0x0

    :goto_1d
    if-nez v0, :cond_36

    iget-object v0, v6, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->k()J

    move-result-wide v3

    iget-object v5, v6, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    invoke-virtual {v5}, Lf/h/a/c/y;->b()Lf/h/a/c/j0;

    move-result-object v5

    iget v5, v5, Lf/h/a/c/j0;->a:F

    iget-boolean v9, v6, Lf/h/a/c/b0;->B:Z

    invoke-interface {v0, v3, v4, v5, v9}, Lf/h/a/c/e0;->d(JFZ)Z

    move-result v0

    if-eqz v0, :cond_35

    goto :goto_1f

    :cond_35
    :goto_1e
    const/4 v13, 0x0

    :cond_36
    :goto_1f
    if-eqz v13, :cond_37

    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Lf/h/a/c/b0;->R(I)V

    iget-boolean v3, v6, Lf/h/a/c/b0;->A:Z

    if-eqz v3, :cond_3a

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->S()V

    goto :goto_20

    :cond_37
    const/4 v0, 0x3

    iget-object v3, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v3, v3, Lf/h/a/c/i0;->e:I

    if-ne v3, v0, :cond_3a

    iget-object v0, v6, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v0, v0

    if-nez v0, :cond_38

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->u()Z

    move-result v0

    if-eqz v0, :cond_39

    goto :goto_20

    :cond_38
    if-nez v10, :cond_3a

    :cond_39
    iget-boolean v0, v6, Lf/h/a/c/b0;->A:Z

    iput-boolean v0, v6, Lf/h/a/c/b0;->B:Z

    invoke-virtual {v6, v1}, Lf/h/a/c/b0;->R(I)V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->U()V

    :cond_3a
    :goto_20
    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    if-ne v0, v1, :cond_3b

    iget-object v0, v6, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v3, v0

    const/4 v12, 0x0

    :goto_21
    if-ge v12, v3, :cond_3b

    aget-object v4, v0, v12

    invoke-interface {v4}, Lf/h/a/c/p0;->p()V

    add-int/lit8 v12, v12, 0x1

    goto :goto_21

    :cond_3b
    iget-boolean v0, v6, Lf/h/a/c/b0;->A:Z

    if-eqz v0, :cond_3c

    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_3d

    :cond_3c
    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    if-ne v0, v1, :cond_3e

    :cond_3d
    const-wide/16 v0, 0xa

    invoke-virtual {v6, v7, v8, v0, v1}, Lf/h/a/c/b0;->F(JJ)V

    goto :goto_22

    :cond_3e
    iget-object v3, v6, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    array-length v3, v3

    if-eqz v3, :cond_3f

    if-eq v0, v2, :cond_3f

    const-wide/16 v2, 0x3e8

    invoke-virtual {v6, v7, v8, v2, v3}, Lf/h/a/c/b0;->F(JJ)V

    goto :goto_22

    :cond_3f
    iget-object v0, v6, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v0, v0, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :goto_22
    invoke-static {}, Lf/g/j/k/a;->R()V

    return-void

    :cond_40
    :goto_23
    iget-object v0, v6, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    iget-object v0, v0, Lf/h/a/c/i1/x;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public final h([ZI)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p2

    new-array v1, v1, [Lf/h/a/c/p0;

    iput-object v1, v0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    iget-object v1, v0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v1, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v1, v1, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    const/4 v3, 0x0

    :goto_0
    iget-object v4, v0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    invoke-virtual {v1, v3}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    aget-object v4, v4, v3

    invoke-interface {v4}, Lf/h/a/c/p0;->reset()V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    iget-object v5, v0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v5, v5

    if-ge v3, v5, :cond_9

    invoke-virtual {v1, v3}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v5

    if-eqz v5, :cond_8

    aget-boolean v5, p1, v3

    add-int/lit8 v6, v4, 0x1

    iget-object v7, v0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v7, v7, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v8, v0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    aget-object v8, v8, v3

    iget-object v9, v0, Lf/h/a/c/b0;->y:[Lf/h/a/c/p0;

    aput-object v8, v9, v4

    invoke-interface {v8}, Lf/h/a/c/p0;->getState()I

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, v7, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v9, v4, Lf/h/a/c/f1/i;->b:[Lf/h/a/c/q0;

    aget-object v10, v9, v3

    iget-object v4, v4, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget-object v4, v4, Lf/h/a/c/f1/g;->b:[Lf/h/a/c/f1/f;

    aget-object v4, v4, v3

    invoke-static {v4}, Lf/h/a/c/b0;->i(Lf/h/a/c/f1/f;)[Lcom/google/android/exoplayer2/Format;

    move-result-object v11

    iget-boolean v4, v0, Lf/h/a/c/b0;->A:Z

    const/4 v9, 0x1

    if-eqz v4, :cond_2

    iget-object v4, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v4, v4, Lf/h/a/c/i0;->e:I

    const/4 v12, 0x3

    if-ne v4, v12, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-nez v5, :cond_3

    if-eqz v4, :cond_3

    const/4 v15, 0x1

    goto :goto_3

    :cond_3
    const/4 v15, 0x0

    :goto_3
    iget-object v5, v7, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v12, v5, v3

    iget-wide v13, v0, Lf/h/a/c/b0;->I:J

    move v5, v3

    iget-wide v2, v7, Lf/h/a/c/f0;->n:J

    move-object v9, v8

    move-wide/from16 v16, v2

    invoke-interface/range {v9 .. v17}, Lf/h/a/c/p0;->k(Lf/h/a/c/q0;[Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d1/v;JZJ)V

    iget-object v2, v0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v8}, Lf/h/a/c/p0;->t()Lf/h/a/c/i1/n;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v7, v2, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    if-eq v3, v7, :cond_5

    if-nez v7, :cond_4

    iput-object v3, v2, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    iput-object v8, v2, Lf/h/a/c/y;->f:Lf/h/a/c/p0;

    iget-object v2, v2, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    iget-object v2, v2, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    invoke-interface {v3, v2}, Lf/h/a/c/i1/n;->e(Lf/h/a/c/j0;)V

    goto :goto_4

    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Multiple renderer media clocks enabled."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/exoplayer2/ExoPlaybackException;

    const/4 v3, 0x2

    invoke-direct {v2, v3, v1}, Lcom/google/android/exoplayer2/ExoPlaybackException;-><init>(ILjava/lang/Throwable;)V

    throw v2

    :cond_5
    :goto_4
    if-eqz v4, :cond_7

    invoke-interface {v8}, Lf/h/a/c/p0;->start()V

    goto :goto_5

    :cond_6
    move v5, v3

    :cond_7
    :goto_5
    move v4, v6

    goto :goto_6

    :cond_8
    move v5, v3

    :goto_6
    add-int/lit8 v3, v5, 0x1

    goto/16 :goto_1

    :cond_9
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    const-string v0, "ExoPlayerImplInternal"

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x4

    const/4 v4, 0x2

    :try_start_0
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    return v1

    :pswitch_0
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lf/h/a/c/j0;

    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, v5, p1}, Lf/h/a/c/b0;->p(Lf/h/a/c/j0;Z)V

    goto/16 :goto_7

    :pswitch_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/n0;

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->K(Lf/h/a/c/n0;)V

    goto/16 :goto_7

    :pswitch_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/n0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->J(Lf/h/a/c/n0;)V

    goto/16 :goto_7

    :pswitch_3
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p0, v5, p1}, Lf/h/a/c/b0;->M(ZLjava/util/concurrent/atomic/AtomicBoolean;)V

    goto/16 :goto_7

    :pswitch_4
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->Q(Z)V

    goto/16 :goto_7

    :pswitch_5
    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->P(I)V

    goto/16 :goto_7

    :pswitch_6
    invoke-virtual {p0}, Lf/h/a/c/b0;->z()V

    goto/16 :goto_7

    :pswitch_7
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/d1/o;

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->m(Lf/h/a/c/d1/o;)V

    goto :goto_7

    :pswitch_8
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/d1/o;

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->o(Lf/h/a/c/d1/o;)V

    goto :goto_7

    :pswitch_9
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/b0$b;

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->r(Lf/h/a/c/b0$b;)V

    goto :goto_7

    :pswitch_a
    invoke-virtual {p0}, Lf/h/a/c/b0;->y()V

    return v2

    :pswitch_b
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    invoke-virtual {p0, v1, p1, v2}, Lf/h/a/c/b0;->T(ZZZ)V

    goto :goto_7

    :pswitch_c
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/r0;

    iput-object p1, p0, Lf/h/a/c/b0;->v:Lf/h/a/c/r0;

    goto :goto_7

    :pswitch_d
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/j0;

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->O(Lf/h/a/c/j0;)V

    goto :goto_7

    :pswitch_e
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lf/h/a/c/b0$e;

    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->H(Lf/h/a/c/b0$e;)V

    goto :goto_7

    :pswitch_f
    invoke-virtual {p0}, Lf/h/a/c/b0;->g()V

    goto :goto_7

    :pswitch_10
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_4

    const/4 p1, 0x1

    goto :goto_4

    :cond_4
    const/4 p1, 0x0

    :goto_4
    invoke-virtual {p0, p1}, Lf/h/a/c/b0;->N(Z)V

    goto :goto_7

    :pswitch_11
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lf/h/a/c/d1/p;

    iget v6, p1, Landroid/os/Message;->arg1:I

    if-eqz v6, :cond_5

    const/4 v6, 0x1

    goto :goto_5

    :cond_5
    const/4 v6, 0x0

    :goto_5
    iget p1, p1, Landroid/os/Message;->arg2:I

    if-eqz p1, :cond_6

    const/4 p1, 0x1

    goto :goto_6

    :cond_6
    const/4 p1, 0x0

    :goto_6
    invoke-virtual {p0, v5, v6, p1}, Lf/h/a/c/b0;->x(Lf/h/a/c/d1/p;ZZ)V

    :goto_7
    invoke-virtual {p0}, Lf/h/a/c/b0;->w()V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_d

    :catch_0
    move-exception p1

    goto :goto_8

    :catch_1
    move-exception p1

    :goto_8
    const-string v5, "Internal runtime error."

    invoke-static {v0, v5, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    instance-of v0, p1, Ljava/lang/OutOfMemoryError;

    if-eqz v0, :cond_7

    check-cast p1, Ljava/lang/OutOfMemoryError;

    new-instance v0, Lcom/google/android/exoplayer2/ExoPlaybackException;

    invoke-direct {v0, v3, p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;-><init>(ILjava/lang/Throwable;)V

    goto :goto_9

    :cond_7
    check-cast p1, Ljava/lang/RuntimeException;

    new-instance v0, Lcom/google/android/exoplayer2/ExoPlaybackException;

    invoke-direct {v0, v4, p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;-><init>(ILjava/lang/Throwable;)V

    :goto_9
    invoke-virtual {p0, v2, v1, v1}, Lf/h/a/c/b0;->T(ZZZ)V

    iget-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p1, v0}, Lf/h/a/c/i0;->b(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lf/h/a/c/i0;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p0}, Lf/h/a/c/b0;->w()V

    goto/16 :goto_d

    :catch_2
    move-exception p1

    const-string v3, "Source error."

    invoke-static {v0, v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0, v1, v1, v1}, Lf/h/a/c/b0;->T(ZZZ)V

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    new-instance v3, Lcom/google/android/exoplayer2/ExoPlaybackException;

    invoke-direct {v3, v1, p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;-><init>(ILjava/lang/Throwable;)V

    invoke-virtual {v0, v3}, Lf/h/a/c/i0;->b(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lf/h/a/c/i0;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p0}, Lf/h/a/c/b0;->w()V

    goto/16 :goto_d

    :catch_3
    move-exception p1

    iget v5, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->type:I

    if-eq v5, v2, :cond_8

    const-string v3, "Playback error."

    goto/16 :goto_c

    :cond_8
    const-string v5, "Renderer error: index="

    invoke-static {v5}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->rendererIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ", type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    iget v7, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->rendererIndex:I

    aget-object v6, v6, v7

    invoke-interface {v6}, Lf/h/a/c/p0;->u()I

    move-result v6

    sget v7, Lf/h/a/c/i1/a0;->a:I

    packed-switch v6, :pswitch_data_1

    const/16 v7, 0x2710

    if-lt v6, v7, :cond_9

    const-string v7, "custom ("

    const-string v8, ")"

    invoke-static {v7, v6, v8}, Lf/e/c/a/a;->k(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_a

    :pswitch_12
    const-string v6, "none"

    goto :goto_a

    :pswitch_13
    const-string v6, "camera motion"

    goto :goto_a

    :pswitch_14
    const-string v6, "metadata"

    goto :goto_a

    :pswitch_15
    const-string v6, "text"

    goto :goto_a

    :pswitch_16
    const-string v6, "video"

    goto :goto_a

    :pswitch_17
    const-string v6, "audio"

    goto :goto_a

    :pswitch_18
    const-string v6, "default"

    goto :goto_a

    :cond_9
    const-string v6, "?"

    :goto_a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", format="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->rendererFormat:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, ", rendererSupport="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p1, Lcom/google/android/exoplayer2/ExoPlaybackException;->rendererFormatSupport:I

    if-eqz v6, :cond_e

    if-eq v6, v2, :cond_d

    if-eq v6, v4, :cond_c

    const/4 v4, 0x3

    if-eq v6, v4, :cond_b

    if-ne v6, v3, :cond_a

    const-string v3, "YES"

    goto :goto_b

    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_b
    const-string v3, "NO_EXCEEDS_CAPABILITIES"

    goto :goto_b

    :cond_c
    const-string v3, "NO_UNSUPPORTED_DRM"

    goto :goto_b

    :cond_d
    const-string v3, "NO_UNSUPPORTED_TYPE"

    goto :goto_b

    :cond_e
    const-string v3, "NO"

    :goto_b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_c
    invoke-static {v0, v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0, v2, v1, v1}, Lf/h/a/c/b0;->T(ZZZ)V

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {v0, p1}, Lf/h/a/c/i0;->b(Lcom/google/android/exoplayer2/ExoPlaybackException;)Lf/h/a/c/i0;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {p0}, Lf/h/a/c/b0;->w()V

    :goto_d
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch
.end method

.method public final j(Lf/h/a/c/t0;IJ)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/c/t0;",
            "IJ)",
            "Landroid/util/Pair<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lf/h/a/c/b0;->m:Lf/h/a/c/t0$c;

    iget-object v2, p0, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    move-object v0, p1

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/t0;->j(Lf/h/a/c/t0$c;Lf/h/a/c/t0$b;IJ)Landroid/util/Pair;

    move-result-object p1

    return-object p1
.end method

.method public final k()J
    .locals 2

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v0, v0, Lf/h/a/c/i0;->k:J

    invoke-virtual {p0, v0, v1}, Lf/h/a/c/b0;->l(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final l(J)J
    .locals 7

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    return-wide v1

    :cond_0
    iget-wide v3, p0, Lf/h/a/c/b0;->I:J

    iget-wide v5, v0, Lf/h/a/c/f0;->n:J

    sub-long/2addr v3, v5

    sub-long/2addr p1, v3

    invoke-static {v1, v2, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public final m(Lf/h/a/c/d1/o;)V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    if-ne v1, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    return-void

    :cond_1
    iget-wide v1, p0, Lf/h/a/c/b0;->I:J

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/h0;->i(J)V

    invoke-virtual {p0}, Lf/h/a/c/b0;->v()V

    return-void
.end method

.method public final n(Z)V
    .locals 25

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v1, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    if-nez v1, :cond_0

    iget-object v2, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    goto :goto_0

    :cond_0
    iget-object v2, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v2, v2, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    :goto_0
    move-object v15, v2

    iget-object v2, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v2, v2, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    invoke-virtual {v2, v15}, Lf/h/a/c/d1/p$a;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget-object v14, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    new-instance v13, Lf/h/a/c/i0;

    move-object v3, v13

    iget-object v4, v14, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v5, v14, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v6, v14, Lf/h/a/c/i0;->c:J

    iget-wide v8, v14, Lf/h/a/c/i0;->d:J

    iget v10, v14, Lf/h/a/c/i0;->e:I

    iget-object v11, v14, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget-boolean v12, v14, Lf/h/a/c/i0;->g:Z

    move-object/from16 v16, v13

    iget-object v13, v14, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move/from16 v22, v2

    move-object/from16 v2, v16

    move-object/from16 v23, v1

    iget-object v1, v14, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    move-object v0, v14

    move-object v14, v1

    move-object/from16 v24, v2

    iget-wide v1, v0, Lf/h/a/c/i0;->k:J

    move-wide/from16 v16, v1

    iget-wide v1, v0, Lf/h/a/c/i0;->l:J

    move-wide/from16 v18, v1

    iget-wide v0, v0, Lf/h/a/c/i0;->m:J

    move-wide/from16 v20, v0

    invoke-direct/range {v3 .. v21}, Lf/h/a/c/i0;-><init>(Lf/h/a/c/t0;Lf/h/a/c/d1/p$a;JJILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;Lf/h/a/c/d1/p$a;JJJ)V

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    iput-object v1, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    goto :goto_1

    :cond_1
    move-object/from16 v23, v1

    move/from16 v22, v2

    :goto_1
    iget-object v1, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    if-nez v23, :cond_2

    iget-wide v2, v1, Lf/h/a/c/i0;->m:J

    goto :goto_2

    :cond_2
    invoke-virtual/range {v23 .. v23}, Lf/h/a/c/f0;->d()J

    move-result-wide v2

    :goto_2
    iput-wide v2, v1, Lf/h/a/c/i0;->k:J

    iget-object v1, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->k()J

    move-result-wide v2

    iput-wide v2, v1, Lf/h/a/c/i0;->l:J

    if-nez v22, :cond_3

    if-eqz p1, :cond_4

    :cond_3
    if-eqz v23, :cond_4

    move-object/from16 v1, v23

    iget-boolean v2, v1, Lf/h/a/c/f0;->d:Z

    if-eqz v2, :cond_4

    iget-object v2, v1, Lf/h/a/c/f0;->l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v1, v1, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v3, v0, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    iget-object v4, v0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    iget-object v1, v1, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    invoke-interface {v3, v4, v2, v1}, Lf/h/a/c/e0;->f([Lf/h/a/c/p0;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V

    :cond_4
    return-void
.end method

.method public final o(Lf/h/a/c/d1/o;)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v1, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v3, v1, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    move-object/from16 v4, p1

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-nez v3, :cond_1

    return-void

    :cond_1
    iget-object v3, v0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    invoke-virtual {v3}, Lf/h/a/c/y;->b()Lf/h/a/c/j0;

    move-result-object v3

    iget v3, v3, Lf/h/a/c/j0;->a:F

    iget-object v4, v0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v4, v4, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iput-boolean v2, v1, Lf/h/a/c/f0;->d:Z

    iget-object v2, v1, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v2}, Lf/h/a/c/d1/o;->n()Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v2

    iput-object v2, v1, Lf/h/a/c/f0;->l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    invoke-virtual {v1, v3, v4}, Lf/h/a/c/f0;->h(FLf/h/a/c/t0;)Lf/h/a/c/f1/i;

    move-result-object v3

    iget-object v2, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v4, v2, Lf/h/a/c/g0;->b:J

    iget-object v2, v1, Lf/h/a/c/f0;->h:[Lf/h/a/c/t;

    array-length v2, v2

    new-array v7, v2, [Z

    const/4 v6, 0x0

    move-object v2, v1

    invoke-virtual/range {v2 .. v7}, Lf/h/a/c/f0;->a(Lf/h/a/c/f1/i;JZ[Z)J

    move-result-wide v10

    iget-wide v2, v1, Lf/h/a/c/f0;->n:J

    iget-object v4, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v5, v4, Lf/h/a/c/g0;->b:J

    sub-long v7, v5, v10

    add-long/2addr v7, v2

    iput-wide v7, v1, Lf/h/a/c/f0;->n:J

    cmp-long v2, v10, v5

    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    new-instance v2, Lf/h/a/c/g0;

    iget-object v9, v4, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-wide v12, v4, Lf/h/a/c/g0;->c:J

    iget-wide v14, v4, Lf/h/a/c/g0;->d:J

    iget-wide v5, v4, Lf/h/a/c/g0;->e:J

    iget-boolean v3, v4, Lf/h/a/c/g0;->f:Z

    iget-boolean v4, v4, Lf/h/a/c/g0;->g:Z

    move-object v8, v2

    move-wide/from16 v16, v5

    move/from16 v18, v3

    move/from16 v19, v4

    invoke-direct/range {v8 .. v19}, Lf/h/a/c/g0;-><init>(Lf/h/a/c/d1/p$a;JJJJZZ)V

    move-object v4, v2

    :goto_1
    iput-object v4, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v2, v1, Lf/h/a/c/f0;->l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v3, v1, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v4, v0, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    iget-object v5, v0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    iget-object v3, v3, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    invoke-interface {v4, v5, v2, v3}, Lf/h/a/c/e0;->f([Lf/h/a/c/p0;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V

    iget-object v2, v0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v2, v2, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-ne v1, v2, :cond_3

    iget-object v1, v1, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v1, v1, Lf/h/a/c/g0;->b:J

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/b0;->B(J)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/c/b0;->X(Lf/h/a/c/f0;)V

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->v()V

    return-void
.end method

.method public final p(Lf/h/a/c/j0;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/b0;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p2

    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    iget p2, p1, Lf/h/a/c/j0;->a:F

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v1, v1, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    invoke-virtual {v1}, Lf/h/a/c/f1/g;->a()[Lf/h/a/c/f1/f;

    move-result-object v1

    array-length v3, v1

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_1

    aget-object v5, v1, v4

    if-eqz v5, :cond_0

    invoke-interface {v5, p2}, Lf/h/a/c/f1/f;->g(F)V

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    goto :goto_0

    :cond_2
    iget-object p2, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v0, p2

    :goto_2
    if-ge v2, v0, :cond_4

    aget-object v1, p2, v2

    if-eqz v1, :cond_3

    iget v3, p1, Lf/h/a/c/j0;->a:F

    invoke-interface {v1, v3}, Lf/h/a/c/p0;->o(F)V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method public final q()V
    .locals 7

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lf/h/a/c/b0;->R(I)V

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/b0;->A(ZZZZZ)V

    return-void
.end method

.method public final r(Lf/h/a/c/b0$b;)V
    .locals 35
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v0, p1

    iget-object v1, v0, Lf/h/a/c/b0$b;->a:Lf/h/a/c/d1/p;

    iget-object v2, v6, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    if-eq v1, v2, :cond_0

    return-void

    :cond_0
    iget-object v1, v6, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    iget v2, v6, Lf/h/a/c/b0;->G:I

    invoke-virtual {v1, v2}, Lf/h/a/c/b0$d;->a(I)V

    const/4 v7, 0x0

    iput v7, v6, Lf/h/a/c/b0;->G:I

    iget-object v1, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v2, v1, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    iget-object v0, v0, Lf/h/a/c/b0$b;->b:Lf/h/a/c/t0;

    move-object v9, v0

    iget-object v3, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iput-object v0, v3, Lf/h/a/c/h0;->d:Lf/h/a/c/t0;

    new-instance v3, Lf/h/a/c/i0;

    move-object v8, v3

    iget-object v10, v1, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v11, v1, Lf/h/a/c/i0;->c:J

    iget-wide v13, v1, Lf/h/a/c/i0;->d:J

    iget v15, v1, Lf/h/a/c/i0;->e:I

    iget-object v4, v1, Lf/h/a/c/i0;->f:Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-object/from16 v16, v4

    iget-boolean v4, v1, Lf/h/a/c/i0;->g:Z

    move/from16 v17, v4

    iget-object v4, v1, Lf/h/a/c/i0;->h:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-object/from16 v18, v4

    iget-object v4, v1, Lf/h/a/c/i0;->i:Lf/h/a/c/f1/i;

    move-object/from16 v19, v4

    iget-object v4, v1, Lf/h/a/c/i0;->j:Lf/h/a/c/d1/p$a;

    move-object/from16 v20, v4

    iget-wide v4, v1, Lf/h/a/c/i0;->k:J

    move-wide/from16 v21, v4

    iget-wide v4, v1, Lf/h/a/c/i0;->l:J

    move-wide/from16 v23, v4

    iget-wide v4, v1, Lf/h/a/c/i0;->m:J

    move-wide/from16 v25, v4

    invoke-direct/range {v8 .. v26}, Lf/h/a/c/i0;-><init>(Lf/h/a/c/t0;Lf/h/a/c/d1/p$a;JJILcom/google/android/exoplayer2/ExoPlaybackException;ZLcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;Lf/h/a/c/d1/p$a;JJJ)V

    iput-object v3, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v1, v6, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, -0x1

    add-int/2addr v1, v3

    :goto_0
    if-ltz v1, :cond_2

    iget-object v4, v6, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/b0$c;

    invoke-virtual {v6, v4}, Lf/h/a/c/b0;->C(Lf/h/a/c/b0$c;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v6, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/a/c/b0$c;

    iget-object v4, v4, Lf/h/a/c/b0$c;->d:Lf/h/a/c/n0;

    invoke-virtual {v4, v7}, Lf/h/a/c/n0;->b(Z)V

    iget-object v4, v6, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    iget-object v1, v6, Lf/h/a/c/b0;->s:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v1, v1, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    invoke-virtual {v1}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v4, v4, Lf/h/a/c/i0;->d:J

    goto :goto_1

    :cond_3
    iget-object v4, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v4, v4, Lf/h/a/c/i0;->m:J

    :goto_1
    iget-object v8, v6, Lf/h/a/c/b0;->H:Lf/h/a/c/b0$e;

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v11, 0x0

    const/4 v12, 0x1

    if-eqz v8, :cond_5

    invoke-virtual {v6, v8, v12}, Lf/h/a/c/b0;->D(Lf/h/a/c/b0$e;Z)Landroid/util/Pair;

    move-result-object v0

    iput-object v11, v6, Lf/h/a/c/b0;->H:Lf/h/a/c/b0$e;

    if-nez v0, :cond_4

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->q()V

    return-void

    :cond_4
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v0, v1, v2}, Lf/h/a/c/h0;->k(Ljava/lang/Object;J)Lf/h/a/c/d1/p$a;

    move-result-object v0

    goto :goto_3

    :cond_5
    cmp-long v8, v4, v9

    if-nez v8, :cond_7

    invoke-virtual {v0}, Lf/h/a/c/t0;->p()Z

    move-result v8

    if-nez v8, :cond_7

    invoke-virtual {v0}, Lf/h/a/c/t0;->a()I

    move-result v1

    invoke-virtual {v6, v0, v1, v9, v10}, Lf/h/a/c/b0;->j(Lf/h/a/c/t0;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v1, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-virtual {v1, v2, v13, v14}, Lf/h/a/c/h0;->k(Ljava/lang/Object;J)Lf/h/a/c/d1/p$a;

    move-result-object v1

    invoke-virtual {v1}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_2

    :cond_6
    move-wide v2, v4

    :goto_2
    move-wide v13, v2

    goto :goto_4

    :cond_7
    iget-object v8, v1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v0, v8}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v8

    if-ne v8, v3, :cond_9

    iget-object v1, v1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v6, v1, v2, v0}, Lf/h/a/c/b0;->E(Ljava/lang/Object;Lf/h/a/c/t0;Lf/h/a/c/t0;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->q()V

    return-void

    :cond_8
    iget-object v2, v6, Lf/h/a/c/b0;->n:Lf/h/a/c/t0$b;

    invoke-virtual {v0, v1}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1, v2, v12}, Lf/h/a/c/t0;->g(ILf/h/a/c/t0$b;Z)Lf/h/a/c/t0$b;

    move-result-object v1

    iget v1, v1, Lf/h/a/c/t0$b;->b:I

    invoke-virtual {v6, v0, v1, v9, v10}, Lf/h/a/c/b0;->j(Lf/h/a/c/t0;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v0, v1, v2}, Lf/h/a/c/h0;->k(Ljava/lang/Object;J)Lf/h/a/c/d1/p$a;

    move-result-object v0

    :goto_3
    move-wide v13, v1

    move-object v1, v0

    goto :goto_4

    :cond_9
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v1, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v1, v1, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-object v1, v1, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v4, v5}, Lf/h/a/c/h0;->k(Ljava/lang/Object;J)Lf/h/a/c/d1/p$a;

    move-result-object v0

    iget-object v1, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v1, v1, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    invoke-virtual {v1}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    :cond_a
    move-object v1, v0

    move-wide v13, v4

    :goto_4
    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    invoke-virtual {v0, v1}, Lf/h/a/c/d1/p$a;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v2, 0x0

    if-eqz v0, :cond_1f

    cmp-long v0, v4, v13

    if-nez v0, :cond_1f

    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-wide v4, v6, Lf/h/a/c/b0;->I:J

    iget-object v1, v0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    const-wide/high16 v13, -0x8000000000000000L

    if-nez v1, :cond_b

    goto :goto_7

    :cond_b
    iget-wide v2, v1, Lf/h/a/c/f0;->n:J

    iget-boolean v8, v1, Lf/h/a/c/f0;->d:Z

    if-nez v8, :cond_c

    goto :goto_7

    :cond_c
    const/4 v8, 0x0

    :goto_5
    iget-object v15, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v11, v15

    if-ge v8, v11, :cond_10

    aget-object v11, v15, v8

    invoke-interface {v11}, Lf/h/a/c/p0;->getState()I

    move-result v11

    if-eqz v11, :cond_f

    iget-object v11, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    aget-object v11, v11, v8

    invoke-interface {v11}, Lf/h/a/c/p0;->i()Lf/h/a/c/d1/v;

    move-result-object v11

    iget-object v15, v1, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v15, v15, v8

    if-eq v11, v15, :cond_d

    goto :goto_6

    :cond_d
    iget-object v11, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    aget-object v11, v11, v8

    invoke-interface {v11}, Lf/h/a/c/p0;->q()J

    move-result-wide v9

    cmp-long v11, v9, v13

    if-nez v11, :cond_e

    move-wide v2, v13

    goto :goto_7

    :cond_e
    invoke-static {v9, v10, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    :cond_f
    :goto_6
    add-int/lit8 v8, v8, 0x1

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v11, 0x0

    goto :goto_5

    :cond_10
    :goto_7
    iget-object v1, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    move-object v11, v1

    const/4 v1, 0x0

    :goto_8
    if-eqz v11, :cond_1d

    iget-object v8, v11, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    if-nez v1, :cond_11

    invoke-virtual {v0, v8}, Lf/h/a/c/h0;->g(Lf/h/a/c/g0;)Lf/h/a/c/g0;

    move-result-object v1

    move-object v7, v8

    goto :goto_b

    :cond_11
    invoke-virtual {v0, v1, v4, v5}, Lf/h/a/c/h0;->c(Lf/h/a/c/f0;J)Lf/h/a/c/g0;

    move-result-object v9

    if-nez v9, :cond_12

    invoke-virtual {v0, v1}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    move-result v0

    goto :goto_a

    :cond_12
    iget-wide v13, v8, Lf/h/a/c/g0;->b:J

    move-object/from16 v19, v8

    iget-wide v7, v9, Lf/h/a/c/g0;->b:J

    cmp-long v20, v13, v7

    move-object/from16 v7, v19

    if-nez v20, :cond_13

    iget-object v8, v7, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-object v13, v9, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {v8, v13}, Lf/h/a/c/d1/p$a;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_13

    const/4 v8, 0x1

    goto :goto_9

    :cond_13
    const/4 v8, 0x0

    :goto_9
    if-nez v8, :cond_14

    invoke-virtual {v0, v1}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    move-result v0

    :goto_a
    xor-int/2addr v12, v0

    goto/16 :goto_12

    :cond_14
    move-object v1, v9

    :goto_b
    iget-wide v8, v7, Lf/h/a/c/g0;->c:J

    iget-wide v13, v1, Lf/h/a/c/g0;->c:J

    cmp-long v19, v8, v13

    if-nez v19, :cond_15

    move-object v13, v1

    move-wide/from16 v33, v4

    move-object v4, v11

    goto :goto_c

    :cond_15
    new-instance v13, Lf/h/a/c/g0;

    iget-object v14, v1, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    move-object/from16 v31, v11

    iget-wide v10, v1, Lf/h/a/c/g0;->b:J

    move-object/from16 v32, v13

    iget-wide v12, v1, Lf/h/a/c/g0;->d:J

    move-wide/from16 v33, v4

    iget-wide v4, v1, Lf/h/a/c/g0;->e:J

    iget-boolean v15, v1, Lf/h/a/c/g0;->f:Z

    iget-boolean v6, v1, Lf/h/a/c/g0;->g:Z

    move-object/from16 v19, v32

    move-object/from16 v20, v14

    move-wide/from16 v21, v10

    move-wide/from16 v23, v8

    move-wide/from16 v25, v12

    move-wide/from16 v27, v4

    move/from16 v29, v15

    move/from16 v30, v6

    invoke-direct/range {v19 .. v30}, Lf/h/a/c/g0;-><init>(Lf/h/a/c/d1/p$a;JJJJZZ)V

    move-object/from16 v4, v31

    move-object/from16 v13, v32

    :goto_c
    iput-object v13, v4, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v5, v7, Lf/h/a/c/g0;->e:J

    iget-wide v7, v1, Lf/h/a/c/g0;->e:J

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v5, v9

    if-eqz v1, :cond_17

    cmp-long v1, v5, v7

    if-nez v1, :cond_16

    goto :goto_d

    :cond_16
    const/4 v1, 0x0

    goto :goto_e

    :cond_17
    :goto_d
    const/4 v1, 0x1

    :goto_e
    if-nez v1, :cond_1c

    cmp-long v1, v7, v9

    if-nez v1, :cond_18

    const-wide v5, 0x7fffffffffffffffL

    goto :goto_f

    :cond_18
    iget-wide v5, v4, Lf/h/a/c/f0;->n:J

    add-long/2addr v5, v7

    :goto_f
    iget-object v1, v0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-ne v4, v1, :cond_1a

    const-wide/high16 v7, -0x8000000000000000L

    cmp-long v1, v2, v7

    if-eqz v1, :cond_19

    cmp-long v1, v2, v5

    if-ltz v1, :cond_1a

    :cond_19
    const/4 v10, 0x1

    goto :goto_10

    :cond_1a
    const/4 v10, 0x0

    :goto_10
    invoke-virtual {v0, v4}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    move-result v0

    if-nez v0, :cond_1b

    if-nez v10, :cond_1b

    goto :goto_11

    :cond_1b
    const/4 v12, 0x0

    goto :goto_12

    :cond_1c
    const-wide/high16 v7, -0x8000000000000000L

    iget-object v1, v4, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    move-object v11, v1

    move-object v1, v4

    move-wide v13, v7

    move-wide/from16 v4, v33

    const/4 v7, 0x0

    const/4 v12, 0x1

    move-object/from16 v6, p0

    goto/16 :goto_8

    :cond_1d
    :goto_11
    const/4 v12, 0x1

    :goto_12
    if-nez v12, :cond_1e

    const/4 v0, 0x0

    move-object/from16 v6, p0

    invoke-virtual {v6, v0}, Lf/h/a/c/b0;->G(Z)V

    goto :goto_17

    :cond_1e
    move-object/from16 v6, p0

    goto :goto_16

    :cond_1f
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    if-eqz v0, :cond_21

    :cond_20
    :goto_13
    iget-object v0, v0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-eqz v0, :cond_21

    iget-object v4, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v4, v4, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {v4, v1}, Lf/h/a/c/d1/p$a;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    iget-object v4, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v5, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    invoke-virtual {v4, v5}, Lf/h/a/c/h0;->g(Lf/h/a/c/g0;)Lf/h/a/c/g0;

    move-result-object v4

    iput-object v4, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    goto :goto_13

    :cond_21
    invoke-virtual {v1}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v0

    if-eqz v0, :cond_22

    goto :goto_14

    :cond_22
    move-wide v2, v13

    :goto_14
    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v4, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v0, v0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    if-eq v4, v0, :cond_23

    const/4 v12, 0x1

    goto :goto_15

    :cond_23
    const/4 v12, 0x0

    :goto_15
    invoke-virtual {v6, v1, v2, v3, v12}, Lf/h/a/c/b0;->I(Lf/h/a/c/d1/p$a;JZ)J

    move-result-wide v2

    move-object/from16 v0, p0

    move-wide v4, v13

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v0

    iput-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    :goto_16
    const/4 v0, 0x0

    :goto_17
    invoke-virtual {v6, v0}, Lf/h/a/c/b0;->n(Z)V

    return-void
.end method

.method public final s()Z
    .locals 6

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    iget-boolean v1, v0, Lf/h/a/c/f0;->d:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v4, v3

    if-ge v1, v4, :cond_3

    aget-object v3, v3, v1

    iget-object v4, v0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v4, v4, v1

    invoke-interface {v3}, Lf/h/a/c/p0;->i()Lf/h/a/c/d1/v;

    move-result-object v5

    if-ne v5, v4, :cond_2

    if-eqz v4, :cond_1

    invoke-interface {v3}, Lf/h/a/c/p0;->j()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v2

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public final t()Z
    .locals 6

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-boolean v2, v0, Lf/h/a/c/f0;->d:Z

    if-nez v2, :cond_1

    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->d()J

    move-result-wide v2

    :goto_0
    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    return v1

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public final u()Z
    .locals 5

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v1, v0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v1, v1, Lf/h/a/c/g0;->e:J

    iget-boolean v0, v0, Lf/h/a/c/f0;->d:Z

    if-eqz v0, :cond_1

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v3, v0, Lf/h/a/c/i0;->m:J

    cmp-long v0, v3, v1

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final v()V
    .locals 5

    invoke-virtual {p0}, Lf/h/a/c/b0;->t()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iget-boolean v1, v0, Lf/h/a/c/f0;->d:Z

    if-nez v1, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->d()J

    move-result-wide v0

    :goto_0
    invoke-virtual {p0, v0, v1}, Lf/h/a/c/b0;->l(J)J

    move-result-wide v0

    iget-object v2, p0, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    invoke-virtual {v2}, Lf/h/a/c/y;->b()Lf/h/a/c/j0;

    move-result-object v2

    iget v2, v2, Lf/h/a/c/j0;->a:F

    iget-object v3, p0, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    invoke-interface {v3, v0, v1, v2}, Lf/h/a/c/e0;->e(JF)Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lf/h/a/c/b0;->C:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v0, v0, Lf/h/a/c/h0;->i:Lf/h/a/c/f0;

    iget-wide v1, p0, Lf/h/a/c/b0;->I:J

    invoke-virtual {v0}, Lf/h/a/c/f0;->f()Z

    move-result v3

    invoke-static {v3}, Lf/g/j/k/a;->s(Z)V

    iget-wide v3, v0, Lf/h/a/c/f0;->n:J

    sub-long/2addr v1, v3

    iget-object v0, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v0, v1, v2}, Lf/h/a/c/d1/o;->h(J)Z

    :cond_2
    invoke-virtual {p0}, Lf/h/a/c/b0;->V()V

    return-void
.end method

.method public final w()V
    .locals 6

    iget-object v0, p0, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    iget-object v1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v2, v0, Lf/h/a/c/b0$d;->a:Lf/h/a/c/i0;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_1

    iget v2, v0, Lf/h/a/c/b0$d;->b:I

    if-gtz v2, :cond_1

    iget-boolean v2, v0, Lf/h/a/c/b0$d;->c:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    iget-object v2, p0, Lf/h/a/c/b0;->l:Landroid/os/Handler;

    iget v4, v0, Lf/h/a/c/b0$d;->b:I

    iget-boolean v5, v0, Lf/h/a/c/b0$d;->c:Z

    if-eqz v5, :cond_2

    iget v0, v0, Lf/h/a/c/b0$d;->d:I

    goto :goto_2

    :cond_2
    const/4 v0, -0x1

    :goto_2
    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    iget-object v0, p0, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    iget-object v1, p0, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iput-object v1, v0, Lf/h/a/c/b0$d;->a:Lf/h/a/c/i0;

    iput v3, v0, Lf/h/a/c/b0$d;->b:I

    iput-boolean v3, v0, Lf/h/a/c/b0$d;->c:Z

    :cond_3
    return-void
.end method

.method public final x(Lf/h/a/c/d1/p;ZZ)V
    .locals 7

    iget v0, p0, Lf/h/a/c/b0;->G:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/h/a/c/b0;->G:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x1

    move-object v1, p0

    move v4, p2

    move v5, p3

    invoke-virtual/range {v1 .. v6}, Lf/h/a/c/b0;->A(ZZZZZ)V

    iget-object p2, p0, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    invoke-interface {p2}, Lf/h/a/c/e0;->c()V

    iput-object p1, p0, Lf/h/a/c/b0;->x:Lf/h/a/c/d1/p;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Lf/h/a/c/b0;->R(I)V

    iget-object p3, p0, Lf/h/a/c/b0;->i:Lf/h/a/c/h1/e;

    invoke-interface {p3}, Lf/h/a/c/h1/e;->c()Lf/h/a/c/h1/x;

    move-result-object p3

    invoke-interface {p1, p0, p3}, Lf/h/a/c/d1/p;->f(Lf/h/a/c/d1/p$b;Lf/h/a/c/h1/x;)V

    iget-object p1, p0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    invoke-virtual {p1, p2}, Lf/h/a/c/i1/x;->c(I)Z

    return-void
.end method

.method public final y()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/b0;->A(ZZZZZ)V

    iget-object v0, p0, Lf/h/a/c/b0;->h:Lf/h/a/c/e0;

    invoke-interface {v0}, Lf/h/a/c/e0;->g()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lf/h/a/c/b0;->R(I)V

    iget-object v1, p0, Lf/h/a/c/b0;->k:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    monitor-enter p0

    :try_start_0
    iput-boolean v0, p0, Lf/h/a/c/b0;->z:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final z()V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    move-object/from16 v6, p0

    iget-object v0, v6, Lf/h/a/c/b0;->q:Lf/h/a/c/y;

    invoke-virtual {v0}, Lf/h/a/c/y;->b()Lf/h/a/c/j0;

    move-result-object v0

    iget v0, v0, Lf/h/a/c/j0;->a:F

    iget-object v1, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v2, v1, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    iget-object v1, v1, Lf/h/a/c/h0;->h:Lf/h/a/c/f0;

    const/4 v7, 0x1

    move-object v8, v2

    const/4 v2, 0x1

    :goto_0
    if-eqz v8, :cond_10

    iget-boolean v3, v8, Lf/h/a/c/f0;->d:Z

    if-nez v3, :cond_0

    goto/16 :goto_9

    :cond_0
    iget-object v3, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v3, v3, Lf/h/a/c/i0;->a:Lf/h/a/c/t0;

    invoke-virtual {v8, v0, v3}, Lf/h/a/c/f0;->h(FLf/h/a/c/t0;)Lf/h/a/c/f1/i;

    move-result-object v10

    iget-object v3, v8, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    const/4 v15, 0x0

    if-eqz v3, :cond_4

    iget-object v4, v3, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget v4, v4, Lf/h/a/c/f1/g;->a:I

    iget-object v5, v10, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget v5, v5, Lf/h/a/c/f1/g;->a:I

    if-eq v4, v5, :cond_1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    :goto_1
    iget-object v5, v10, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget v5, v5, Lf/h/a/c/f1/g;->a:I

    if-ge v4, v5, :cond_3

    invoke-virtual {v10, v3, v4}, Lf/h/a/c/f1/i;->a(Lf/h/a/c/f1/i;I)Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x1

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v3, 0x0

    :goto_3
    if-nez v3, :cond_e

    const/4 v4, 0x4

    if-eqz v2, :cond_b

    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    iget-object v8, v0, Lf/h/a/c/h0;->g:Lf/h/a/c/f0;

    invoke-virtual {v0, v8}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    move-result v13

    iget-object v0, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v0, v0

    new-array v5, v0, [Z

    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-wide v11, v0, Lf/h/a/c/i0;->m:J

    move-object v9, v8

    move-object v14, v5

    invoke-virtual/range {v9 .. v14}, Lf/h/a/c/f0;->a(Lf/h/a/c/f1/i;JZ[Z)J

    move-result-wide v9

    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v1, v0, Lf/h/a/c/i0;->e:I

    if-eq v1, v4, :cond_5

    iget-wide v0, v0, Lf/h/a/c/i0;->m:J

    cmp-long v2, v9, v0

    if-eqz v2, :cond_5

    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v1, v0, Lf/h/a/c/i0;->b:Lf/h/a/c/d1/p$a;

    iget-wide v11, v0, Lf/h/a/c/i0;->d:J

    move-object/from16 v0, p0

    move-wide v2, v9

    move-object v13, v5

    const/4 v14, 0x4

    move-wide v4, v11

    invoke-virtual/range {v0 .. v5}, Lf/h/a/c/b0;->b(Lf/h/a/c/d1/p$a;JJ)Lf/h/a/c/i0;

    move-result-object v0

    iput-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v0, v6, Lf/h/a/c/b0;->r:Lf/h/a/c/b0$d;

    invoke-virtual {v0, v14}, Lf/h/a/c/b0$d;->b(I)V

    invoke-virtual {v6, v9, v10}, Lf/h/a/c/b0;->B(J)V

    goto :goto_4

    :cond_5
    move-object v13, v5

    const/4 v14, 0x4

    :goto_4
    iget-object v0, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v0, v0

    new-array v0, v0, [Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_5
    iget-object v3, v6, Lf/h/a/c/b0;->d:[Lf/h/a/c/p0;

    array-length v4, v3

    if-ge v1, v4, :cond_a

    aget-object v3, v3, v1

    invoke-interface {v3}, Lf/h/a/c/p0;->getState()I

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    goto :goto_6

    :cond_6
    const/4 v4, 0x0

    :goto_6
    aput-boolean v4, v0, v1

    iget-object v4, v8, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    aget-object v4, v4, v1

    if-eqz v4, :cond_7

    add-int/lit8 v2, v2, 0x1

    :cond_7
    aget-boolean v5, v0, v1

    if-eqz v5, :cond_9

    invoke-interface {v3}, Lf/h/a/c/p0;->i()Lf/h/a/c/d1/v;

    move-result-object v5

    if-eq v4, v5, :cond_8

    invoke-virtual {v6, v3}, Lf/h/a/c/b0;->f(Lf/h/a/c/p0;)V

    goto :goto_7

    :cond_8
    aget-boolean v4, v13, v1

    if-eqz v4, :cond_9

    iget-wide v4, v6, Lf/h/a/c/b0;->I:J

    invoke-interface {v3, v4, v5}, Lf/h/a/c/p0;->r(J)V

    :cond_9
    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_a
    iget-object v1, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget-object v3, v8, Lf/h/a/c/f0;->l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v4, v8, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    invoke-virtual {v1, v3, v4}, Lf/h/a/c/i0;->c(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/i;)Lf/h/a/c/i0;

    move-result-object v1

    iput-object v1, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    invoke-virtual {v6, v0, v2}, Lf/h/a/c/b0;->h([ZI)V

    goto :goto_8

    :cond_b
    const/4 v14, 0x4

    iget-object v0, v6, Lf/h/a/c/b0;->u:Lf/h/a/c/h0;

    invoke-virtual {v0, v8}, Lf/h/a/c/h0;->j(Lf/h/a/c/f0;)Z

    iget-boolean v0, v8, Lf/h/a/c/f0;->d:Z

    if-eqz v0, :cond_c

    iget-object v0, v8, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v0, v0, Lf/h/a/c/g0;->b:J

    iget-wide v2, v6, Lf/h/a/c/b0;->I:J

    iget-wide v4, v8, Lf/h/a/c/f0;->n:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const/4 v12, 0x0

    iget-object v2, v8, Lf/h/a/c/f0;->h:[Lf/h/a/c/t;

    array-length v2, v2

    new-array v13, v2, [Z

    move-object v9, v10

    move-wide v10, v0

    invoke-virtual/range {v8 .. v13}, Lf/h/a/c/f0;->a(Lf/h/a/c/f1/i;JZ[Z)J

    :cond_c
    :goto_8
    invoke-virtual {v6, v7}, Lf/h/a/c/b0;->n(Z)V

    iget-object v0, v6, Lf/h/a/c/b0;->w:Lf/h/a/c/i0;

    iget v0, v0, Lf/h/a/c/i0;->e:I

    if-eq v0, v14, :cond_d

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->v()V

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/b0;->W()V

    iget-object v0, v6, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/x;->c(I)Z

    :cond_d
    return-void

    :cond_e
    if-ne v8, v1, :cond_f

    const/4 v2, 0x0

    :cond_f
    iget-object v8, v8, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    goto/16 :goto_0

    :cond_10
    :goto_9
    return-void
.end method
