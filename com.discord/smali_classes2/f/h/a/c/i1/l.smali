.class public final Lf/h/a/c/i1/l;
.super Ljava/lang/Object;
.source "FlacStreamMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/i1/l$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:J

.field public final k:Lf/h/a/c/i1/l$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final l:Lcom/google/android/exoplayer2/metadata/Metadata;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIIIIIIJLf/h/a/c/i1/l$a;Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 0
    .param p10    # Lf/h/a/c/i1/l$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/google/android/exoplayer2/metadata/Metadata;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/h/a/c/i1/l;->a:I

    iput p2, p0, Lf/h/a/c/i1/l;->b:I

    iput p3, p0, Lf/h/a/c/i1/l;->c:I

    iput p4, p0, Lf/h/a/c/i1/l;->d:I

    iput p5, p0, Lf/h/a/c/i1/l;->e:I

    invoke-static {p5}, Lf/h/a/c/i1/l;->h(I)I

    move-result p1

    iput p1, p0, Lf/h/a/c/i1/l;->f:I

    iput p6, p0, Lf/h/a/c/i1/l;->g:I

    iput p7, p0, Lf/h/a/c/i1/l;->h:I

    invoke-static {p7}, Lf/h/a/c/i1/l;->c(I)I

    move-result p1

    iput p1, p0, Lf/h/a/c/i1/l;->i:I

    iput-wide p8, p0, Lf/h/a/c/i1/l;->j:J

    iput-object p10, p0, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    iput-object p11, p0, Lf/h/a/c/i1/l;->l:Lcom/google/android/exoplayer2/metadata/Metadata;

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/h/a/c/i1/q;

    invoke-direct {v0, p1}, Lf/h/a/c/i1/q;-><init>([B)V

    mul-int/lit8 p2, p2, 0x8

    invoke-virtual {v0, p2}, Lf/h/a/c/i1/q;->j(I)V

    const/16 p1, 0x10

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result p2

    iput p2, p0, Lf/h/a/c/i1/l;->a:I

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result p1

    iput p1, p0, Lf/h/a/c/i1/l;->b:I

    const/16 p1, 0x18

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result p2

    iput p2, p0, Lf/h/a/c/i1/l;->c:I

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result p1

    iput p1, p0, Lf/h/a/c/i1/l;->d:I

    const/16 p1, 0x14

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result p1

    iput p1, p0, Lf/h/a/c/i1/l;->e:I

    invoke-static {p1}, Lf/h/a/c/i1/l;->h(I)I

    move-result p1

    iput p1, p0, Lf/h/a/c/i1/l;->f:I

    const/4 p1, 0x3

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lf/h/a/c/i1/l;->g:I

    const/4 p1, 0x5

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lf/h/a/c/i1/l;->h:I

    invoke-static {p1}, Lf/h/a/c/i1/l;->c(I)I

    move-result p1

    iput p1, p0, Lf/h/a/c/i1/l;->i:I

    const/16 p1, 0x20

    const/4 p2, 0x4

    invoke-virtual {v0, p2}, Lf/h/a/c/i1/q;->f(I)I

    move-result p2

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    invoke-static {p2}, Lf/h/a/c/i1/a0;->C(I)J

    move-result-wide v1

    shl-long p1, v1, p1

    invoke-static {v0}, Lf/h/a/c/i1/a0;->C(I)J

    move-result-wide v0

    or-long/2addr p1, v0

    iput-wide p1, p0, Lf/h/a/c/i1/l;->j:J

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/i1/l;->k:Lf/h/a/c/i1/l$a;

    iput-object p1, p0, Lf/h/a/c/i1/l;->l:Lcom/google/android/exoplayer2/metadata/Metadata;

    return-void
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 8
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/metadata/flac/PictureFrame;",
            ">;)",
            "Lcom/google/android/exoplayer2/metadata/Metadata;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "="

    invoke-static {v4, v5}, Lf/h/a/c/i1/a0;->A(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    const/4 v7, 0x2

    if-eq v6, v7, :cond_1

    const-string v5, "Failed to parse Vorbis comment: "

    const-string v6, "FlacStreamMetadata"

    invoke-static {v5, v4, v6}, Lf/e/c/a/a;->V(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    new-instance v4, Lcom/google/android/exoplayer2/metadata/flac/VorbisComment;

    aget-object v6, v5, v2

    const/4 v7, 0x1

    aget-object v5, v5, v7

    invoke-direct {v4, v6, v5}, Lcom/google/android/exoplayer2/metadata/flac/VorbisComment;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_3

    goto :goto_2

    :cond_3
    new-instance v1, Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>(Ljava/util/List;)V

    :goto_2
    return-object v1
.end method

.method public static c(I)I
    .locals 1

    const/16 v0, 0x8

    if-eq p0, v0, :cond_4

    const/16 v0, 0xc

    if-eq p0, v0, :cond_3

    const/16 v0, 0x10

    if-eq p0, v0, :cond_2

    const/16 v0, 0x14

    if-eq p0, v0, :cond_1

    const/16 v0, 0x18

    if-eq p0, v0, :cond_0

    const/4 p0, -0x1

    return p0

    :cond_0
    const/4 p0, 0x6

    return p0

    :cond_1
    const/4 p0, 0x5

    return p0

    :cond_2
    const/4 p0, 0x4

    return p0

    :cond_3
    const/4 p0, 0x2

    return p0

    :cond_4
    const/4 p0, 0x1

    return p0
.end method

.method public static h(I)I
    .locals 0

    sparse-switch p0, :sswitch_data_0

    const/4 p0, -0x1

    return p0

    :sswitch_0
    const/4 p0, 0x3

    return p0

    :sswitch_1
    const/4 p0, 0x2

    return p0

    :sswitch_2
    const/16 p0, 0xb

    return p0

    :sswitch_3
    const/4 p0, 0x1

    return p0

    :sswitch_4
    const/16 p0, 0xa

    return p0

    :sswitch_5
    const/16 p0, 0x9

    return p0

    :sswitch_6
    const/16 p0, 0x8

    return p0

    :sswitch_7
    const/4 p0, 0x7

    return p0

    :sswitch_8
    const/4 p0, 0x6

    return p0

    :sswitch_9
    const/4 p0, 0x5

    return p0

    :sswitch_a
    const/4 p0, 0x4

    return p0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1f40 -> :sswitch_a
        0x3e80 -> :sswitch_9
        0x5622 -> :sswitch_8
        0x5dc0 -> :sswitch_7
        0x7d00 -> :sswitch_6
        0xac44 -> :sswitch_5
        0xbb80 -> :sswitch_4
        0x15888 -> :sswitch_3
        0x17700 -> :sswitch_2
        0x2b110 -> :sswitch_1
        0x2ee00 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public b(Lf/h/a/c/i1/l$a;)Lf/h/a/c/i1/l;
    .locals 13
    .param p1    # Lf/h/a/c/i1/l$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v12, Lf/h/a/c/i1/l;

    iget v1, p0, Lf/h/a/c/i1/l;->a:I

    iget v2, p0, Lf/h/a/c/i1/l;->b:I

    iget v3, p0, Lf/h/a/c/i1/l;->c:I

    iget v4, p0, Lf/h/a/c/i1/l;->d:I

    iget v5, p0, Lf/h/a/c/i1/l;->e:I

    iget v6, p0, Lf/h/a/c/i1/l;->g:I

    iget v7, p0, Lf/h/a/c/i1/l;->h:I

    iget-wide v8, p0, Lf/h/a/c/i1/l;->j:J

    iget-object v11, p0, Lf/h/a/c/i1/l;->l:Lcom/google/android/exoplayer2/metadata/Metadata;

    move-object v0, v12

    move-object v10, p1

    invoke-direct/range {v0 .. v11}, Lf/h/a/c/i1/l;-><init>(IIIIIIIJLf/h/a/c/i1/l$a;Lcom/google/android/exoplayer2/metadata/Metadata;)V

    return-object v12
.end method

.method public d()J
    .locals 5

    iget-wide v0, p0, Lf/h/a/c/i1/l;->j:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0

    :cond_0
    const-wide/32 v2, 0xf4240

    mul-long v0, v0, v2

    iget v2, p0, Lf/h/a/c/i1/l;->e:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    :goto_0
    return-wide v0
.end method

.method public e([BLcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format;
    .locals 17
    .param p2    # Lcom/google/android/exoplayer2/metadata/Metadata;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v0, p0

    const/4 v1, 0x4

    const/16 v2, -0x80

    aput-byte v2, p1, v1

    iget v1, v0, Lf/h/a/c/i1/l;->d:I

    if-lez v1, :cond_0

    move v6, v1

    move-object/from16 v1, p2

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    move-object/from16 v1, p2

    const/4 v6, -0x1

    :goto_0
    invoke-virtual {v0, v1}, Lf/h/a/c/i1/l;->f(Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v16

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget v1, v0, Lf/h/a/c/i1/l;->h:I

    iget v8, v0, Lf/h/a/c/i1/l;->e:I

    mul-int v1, v1, v8

    iget v7, v0, Lf/h/a/c/i1/l;->g:I

    mul-int v5, v1, v7

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static/range {p1 .. p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-string v3, "audio/flac"

    invoke-static/range {v2 .. v16}, Lcom/google/android/exoplayer2/Format;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    return-object v1
.end method

.method public f(Lcom/google/android/exoplayer2/metadata/Metadata;)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 1
    .param p1    # Lcom/google/android/exoplayer2/metadata/Metadata;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/i1/l;->l:Lcom/google/android/exoplayer2/metadata/Metadata;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p1, :cond_1

    move-object p1, v0

    goto :goto_0

    :cond_1
    iget-object p1, p1, Lcom/google/android/exoplayer2/metadata/Metadata;->d:[Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/metadata/Metadata;->a([Lcom/google/android/exoplayer2/metadata/Metadata$Entry;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public g(J)J
    .locals 8

    iget v0, p0, Lf/h/a/c/i1/l;->e:I

    int-to-long v0, v0

    mul-long p1, p1, v0

    const-wide/32 v0, 0xf4240

    div-long v2, p1, v0

    iget-wide p1, p0, Lf/h/a/c/i1/l;->j:J

    const-wide/16 v0, 0x1

    sub-long v6, p1, v0

    const-wide/16 v4, 0x0

    invoke-static/range {v2 .. v7}, Lf/h/a/c/i1/a0;->g(JJJ)J

    move-result-wide p1

    return-wide p1
.end method
