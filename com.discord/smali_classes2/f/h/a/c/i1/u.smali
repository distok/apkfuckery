.class public Lf/h/a/c/i1/u;
.super Ljava/lang/Object;
.source "SlidingPercentile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/i1/u$b;
    }
.end annotation


# static fields
.field public static final synthetic h:I


# instance fields
.field public final a:I

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lf/h/a/c/i1/u$b;",
            ">;"
        }
    .end annotation
.end field

.field public final c:[Lf/h/a/c/i1/u$b;

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/h/a/c/i1/u;->a:I

    const/4 p1, 0x5

    new-array p1, p1, [Lf/h/a/c/i1/u$b;

    iput-object p1, p0, Lf/h/a/c/i1/u;->c:[Lf/h/a/c/i1/u$b;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    const/4 p1, -0x1

    iput p1, p0, Lf/h/a/c/i1/u;->d:I

    return-void
.end method


# virtual methods
.method public a(IF)V
    .locals 3

    iget v0, p0, Lf/h/a/c/i1/u;->d:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    sget-object v2, Lf/h/a/c/i1/b;->d:Lf/h/a/c/i1/b;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput v1, p0, Lf/h/a/c/i1/u;->d:I

    :cond_0
    iget v0, p0, Lf/h/a/c/i1/u;->g:I

    if-lez v0, :cond_1

    iget-object v2, p0, Lf/h/a/c/i1/u;->c:[Lf/h/a/c/i1/u$b;

    sub-int/2addr v0, v1

    iput v0, p0, Lf/h/a/c/i1/u;->g:I

    aget-object v0, v2, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lf/h/a/c/i1/u$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/h/a/c/i1/u$b;-><init>(Lf/h/a/c/i1/u$a;)V

    :goto_0
    iget v1, p0, Lf/h/a/c/i1/u;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lf/h/a/c/i1/u;->e:I

    iput v1, v0, Lf/h/a/c/i1/u$b;->a:I

    iput p1, v0, Lf/h/a/c/i1/u$b;->b:I

    iput p2, v0, Lf/h/a/c/i1/u$b;->c:F

    iget-object p2, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget p2, p0, Lf/h/a/c/i1/u;->f:I

    add-int/2addr p2, p1

    iput p2, p0, Lf/h/a/c/i1/u;->f:I

    :cond_2
    :goto_1
    iget p1, p0, Lf/h/a/c/i1/u;->f:I

    iget p2, p0, Lf/h/a/c/i1/u;->a:I

    if-le p1, p2, :cond_4

    sub-int/2addr p1, p2

    iget-object p2, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/i1/u$b;

    iget v1, p2, Lf/h/a/c/i1/u$b;->b:I

    if-gt v1, p1, :cond_3

    iget p1, p0, Lf/h/a/c/i1/u;->f:I

    sub-int/2addr p1, v1

    iput p1, p0, Lf/h/a/c/i1/u;->f:I

    iget-object p1, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget p1, p0, Lf/h/a/c/i1/u;->g:I

    const/4 v0, 0x5

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/i1/u;->c:[Lf/h/a/c/i1/u$b;

    add-int/lit8 v1, p1, 0x1

    iput v1, p0, Lf/h/a/c/i1/u;->g:I

    aput-object p2, v0, p1

    goto :goto_1

    :cond_3
    sub-int/2addr v1, p1

    iput v1, p2, Lf/h/a/c/i1/u$b;->b:I

    iget p2, p0, Lf/h/a/c/i1/u;->f:I

    sub-int/2addr p2, p1

    iput p2, p0, Lf/h/a/c/i1/u;->f:I

    goto :goto_1

    :cond_4
    return-void
.end method

.method public b(F)F
    .locals 4

    iget v0, p0, Lf/h/a/c/i1/u;->d:I

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    sget-object v2, Lf/h/a/c/i1/c;->d:Lf/h/a/c/i1/c;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput v1, p0, Lf/h/a/c/i1/u;->d:I

    :cond_0
    iget v0, p0, Lf/h/a/c/i1/u;->f:I

    int-to-float v0, v0

    mul-float p1, p1, v0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/i1/u$b;

    iget v3, v2, Lf/h/a/c/i1/u$b;->b:I

    add-int/2addr v0, v3

    int-to-float v3, v0

    cmpl-float v3, v3, p1

    if-ltz v3, :cond_1

    iget p1, v2, Lf/h/a/c/i1/u$b;->c:F

    return p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    const/high16 p1, 0x7fc00000    # Float.NaN

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lf/h/a/c/i1/u;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/c/i1/u$b;

    iget p1, p1, Lf/h/a/c/i1/u$b;->c:F

    :goto_1
    return p1
.end method
