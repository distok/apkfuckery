.class public final Lf/h/a/c/i1/v;
.super Ljava/lang/Object;
.source "StandaloneMediaClock.java"

# interfaces
.implements Lf/h/a/c/i1/n;


# instance fields
.field public final d:Lf/h/a/c/i1/f;

.field public e:Z

.field public f:J

.field public g:J

.field public h:Lf/h/a/c/j0;


# direct methods
.method public constructor <init>(Lf/h/a/c/i1/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/i1/v;->d:Lf/h/a/c/i1/f;

    sget-object p1, Lf/h/a/c/j0;->e:Lf/h/a/c/j0;

    iput-object p1, p0, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/i1/v;->f:J

    iget-boolean p1, p0, Lf/h/a/c/i1/v;->e:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/a/c/i1/v;->d:Lf/h/a/c/i1/f;

    invoke-interface {p1}, Lf/h/a/c/i1/f;->c()J

    move-result-wide p1

    iput-wide p1, p0, Lf/h/a/c/i1/v;->g:J

    :cond_0
    return-void
.end method

.method public b()Lf/h/a/c/j0;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    return-object v0
.end method

.method public c()J
    .locals 7

    iget-wide v0, p0, Lf/h/a/c/i1/v;->f:J

    iget-boolean v2, p0, Lf/h/a/c/i1/v;->e:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lf/h/a/c/i1/v;->d:Lf/h/a/c/i1/f;

    invoke-interface {v2}, Lf/h/a/c/i1/f;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lf/h/a/c/i1/v;->g:J

    sub-long/2addr v2, v4

    iget-object v4, p0, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    iget v5, v4, Lf/h/a/c/j0;->a:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    invoke-static {v2, v3}, Lf/h/a/c/u;->a(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    iget v4, v4, Lf/h/a/c/j0;->d:I

    int-to-long v4, v4

    mul-long v2, v2, v4

    add-long/2addr v0, v2

    :cond_1
    :goto_0
    return-wide v0
.end method

.method public d()V
    .locals 2

    iget-boolean v0, p0, Lf/h/a/c/i1/v;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/i1/v;->d:Lf/h/a/c/i1/f;

    invoke-interface {v0}, Lf/h/a/c/i1/f;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lf/h/a/c/i1/v;->g:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/i1/v;->e:Z

    :cond_0
    return-void
.end method

.method public e(Lf/h/a/c/j0;)V
    .locals 2

    iget-boolean v0, p0, Lf/h/a/c/i1/v;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/i1/v;->c()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lf/h/a/c/i1/v;->a(J)V

    :cond_0
    iput-object p1, p0, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    return-void
.end method
