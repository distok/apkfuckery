.class public final Lf/h/a/c/f0;
.super Ljava/lang/Object;
.source "MediaPeriodHolder.java"


# instance fields
.field public final a:Lf/h/a/c/d1/o;

.field public final b:Ljava/lang/Object;

.field public final c:[Lf/h/a/c/d1/v;

.field public d:Z

.field public e:Z

.field public f:Lf/h/a/c/g0;

.field public final g:[Z

.field public final h:[Lf/h/a/c/t;

.field public final i:Lf/h/a/c/f1/h;

.field public final j:Lf/h/a/c/d1/p;

.field public k:Lf/h/a/c/f0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

.field public m:Lf/h/a/c/f1/i;

.field public n:J


# direct methods
.method public constructor <init>([Lf/h/a/c/t;JLf/h/a/c/f1/h;Lf/h/a/c/h1/d;Lf/h/a/c/d1/p;Lf/h/a/c/g0;Lf/h/a/c/f1/i;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/f0;->h:[Lf/h/a/c/t;

    iput-wide p2, p0, Lf/h/a/c/f0;->n:J

    iput-object p4, p0, Lf/h/a/c/f0;->i:Lf/h/a/c/f1/h;

    iput-object p6, p0, Lf/h/a/c/f0;->j:Lf/h/a/c/d1/p;

    iget-object p2, p7, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    iget-object p3, p2, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    iput-object p3, p0, Lf/h/a/c/f0;->b:Ljava/lang/Object;

    iput-object p7, p0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    sget-object p3, Lcom/google/android/exoplayer2/source/TrackGroupArray;->g:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iput-object p3, p0, Lf/h/a/c/f0;->l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iput-object p8, p0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    array-length p3, p1

    new-array p3, p3, [Lf/h/a/c/d1/v;

    iput-object p3, p0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    array-length p1, p1

    new-array p1, p1, [Z

    iput-object p1, p0, Lf/h/a/c/f0;->g:[Z

    iget-wide p3, p7, Lf/h/a/c/g0;->b:J

    iget-wide v5, p7, Lf/h/a/c/g0;->d:J

    invoke-interface {p6, p2, p5, p3, p4}, Lf/h/a/c/d1/p;->a(Lf/h/a/c/d1/p$a;Lf/h/a/c/h1/d;J)Lf/h/a/c/d1/o;

    move-result-object v1

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p3, v5, p1

    if-eqz p3, :cond_0

    const-wide/high16 p1, -0x8000000000000000L

    cmp-long p3, v5, p1

    if-eqz p3, :cond_0

    new-instance p1, Lf/h/a/c/d1/l;

    const/4 v2, 0x1

    const-wide/16 v3, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lf/h/a/c/d1/l;-><init>(Lf/h/a/c/d1/o;ZJJ)V

    move-object v1, p1

    :cond_0
    iput-object v1, p0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/f1/i;JZ[Z)J
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    iget v4, v1, Lf/h/a/c/f1/i;->a:I

    const/4 v5, 0x1

    if-ge v3, v4, :cond_1

    iget-object v4, v0, Lf/h/a/c/f0;->g:[Z

    if-nez p4, :cond_0

    iget-object v6, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    invoke-virtual {v1, v6, v3}, Lf/h/a/c/f1/i;->a(Lf/h/a/c/f1/i;I)Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    aput-boolean v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    const/4 v4, 0x0

    :goto_2
    iget-object v6, v0, Lf/h/a/c/f0;->h:[Lf/h/a/c/t;

    array-length v7, v6

    const/4 v8, 0x6

    if-ge v4, v7, :cond_3

    aget-object v6, v6, v4

    iget v6, v6, Lf/h/a/c/t;->d:I

    if-ne v6, v8, :cond_2

    const/4 v6, 0x0

    aput-object v6, v3, v4

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/f0;->b()V

    iput-object v1, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    invoke-virtual/range {p0 .. p0}, Lf/h/a/c/f0;->c()V

    iget-object v3, v1, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget-object v9, v0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-virtual {v3}, Lf/h/a/c/f1/g;->a()[Lf/h/a/c/f1/f;

    move-result-object v10

    iget-object v11, v0, Lf/h/a/c/f0;->g:[Z

    iget-object v12, v0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    move-object/from16 v13, p5

    move-wide/from16 v14, p2

    invoke-interface/range {v9 .. v15}, Lf/h/a/c/d1/o;->b([Lf/h/a/c/f1/f;[Z[Lf/h/a/c/d1/v;[ZJ)J

    move-result-wide v6

    iget-object v4, v0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    const/4 v9, 0x0

    :goto_3
    iget-object v10, v0, Lf/h/a/c/f0;->h:[Lf/h/a/c/t;

    array-length v11, v10

    if-ge v9, v11, :cond_5

    aget-object v10, v10, v9

    iget v10, v10, Lf/h/a/c/t;->d:I

    if-ne v10, v8, :cond_4

    iget-object v10, v0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    invoke-virtual {v10, v9}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v10

    if-eqz v10, :cond_4

    new-instance v10, Lf/h/a/c/d1/m;

    invoke-direct {v10}, Lf/h/a/c/d1/m;-><init>()V

    aput-object v10, v4, v9

    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_5
    iput-boolean v2, v0, Lf/h/a/c/f0;->e:Z

    const/4 v4, 0x0

    :goto_4
    iget-object v9, v0, Lf/h/a/c/f0;->c:[Lf/h/a/c/d1/v;

    array-length v10, v9

    if-ge v4, v10, :cond_9

    aget-object v9, v9, v4

    if-eqz v9, :cond_6

    invoke-virtual {v1, v4}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v9

    invoke-static {v9}, Lf/g/j/k/a;->s(Z)V

    iget-object v9, v0, Lf/h/a/c/f0;->h:[Lf/h/a/c/t;

    aget-object v9, v9, v4

    iget v9, v9, Lf/h/a/c/t;->d:I

    if-eq v9, v8, :cond_8

    iput-boolean v5, v0, Lf/h/a/c/f0;->e:Z

    goto :goto_6

    :cond_6
    iget-object v9, v3, Lf/h/a/c/f1/g;->b:[Lf/h/a/c/f1/f;

    aget-object v9, v9, v4

    if-nez v9, :cond_7

    const/4 v9, 0x1

    goto :goto_5

    :cond_7
    const/4 v9, 0x0

    :goto_5
    invoke-static {v9}, Lf/g/j/k/a;->s(Z)V

    :cond_8
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_9
    return-wide v6
.end method

.method public final b()V
    .locals 3

    invoke-virtual {p0}, Lf/h/a/c/f0;->f()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget v2, v1, Lf/h/a/c/f1/i;->a:I

    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v1

    iget-object v2, p0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v2, v2, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget-object v2, v2, Lf/h/a/c/f1/g;->b:[Lf/h/a/c/f1/f;

    aget-object v2, v2, v0

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lf/h/a/c/f1/f;->h()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final c()V
    .locals 3

    invoke-virtual {p0}, Lf/h/a/c/f0;->f()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget v2, v1, Lf/h/a/c/f1/i;->a:I

    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lf/h/a/c/f1/i;->b(I)Z

    move-result v1

    iget-object v2, p0, Lf/h/a/c/f0;->m:Lf/h/a/c/f1/i;

    iget-object v2, v2, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget-object v2, v2, Lf/h/a/c/f1/g;->b:[Lf/h/a/c/f1/f;

    aget-object v2, v2, v0

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lf/h/a/c/f1/f;->d()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public d()J
    .locals 5

    iget-boolean v0, p0, Lf/h/a/c/f0;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v0, v0, Lf/h/a/c/g0;->b:J

    return-wide v0

    :cond_0
    iget-boolean v0, p0, Lf/h/a/c/f0;->e:Z

    const-wide/high16 v1, -0x8000000000000000L

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->q()J

    move-result-wide v3

    goto :goto_0

    :cond_1
    move-wide v3, v1

    :goto_0
    cmp-long v0, v3, v1

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v3, v0, Lf/h/a/c/g0;->e:J

    :cond_2
    return-wide v3
.end method

.method public e()Z
    .locals 5

    iget-boolean v0, p0, Lf/h/a/c/f0;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lf/h/a/c/f0;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    invoke-interface {v0}, Lf/h/a/c/d1/o;->q()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lf/h/a/c/f0;->k:Lf/h/a/c/f0;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g()V
    .locals 7

    invoke-virtual {p0}, Lf/h/a/c/f0;->b()V

    iget-object v0, p0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-wide v0, v0, Lf/h/a/c/g0;->d:J

    iget-object v2, p0, Lf/h/a/c/f0;->j:Lf/h/a/c/d1/p;

    iget-object v3, p0, Lf/h/a/c/f0;->a:Lf/h/a/c/d1/o;

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v6, v0, v4

    if-eqz v6, :cond_0

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v6, v0, v4

    if-eqz v6, :cond_0

    :try_start_0
    check-cast v3, Lf/h/a/c/d1/l;

    iget-object v0, v3, Lf/h/a/c/d1/l;->d:Lf/h/a/c/d1/o;

    invoke-interface {v2, v0}, Lf/h/a/c/d1/p;->e(Lf/h/a/c/d1/o;)V

    goto :goto_0

    :cond_0
    invoke-interface {v2, v3}, Lf/h/a/c/d1/p;->e(Lf/h/a/c/d1/o;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MediaPeriodHolder"

    const-string v2, "Period release failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public h(FLf/h/a/c/t0;)Lf/h/a/c/f1/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/f0;->i:Lf/h/a/c/f1/h;

    iget-object v1, p0, Lf/h/a/c/f0;->h:[Lf/h/a/c/t;

    iget-object v2, p0, Lf/h/a/c/f0;->l:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iget-object v3, p0, Lf/h/a/c/f0;->f:Lf/h/a/c/g0;

    iget-object v3, v3, Lf/h/a/c/g0;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {v0, v1, v2, v3, p2}, Lf/h/a/c/f1/h;->b([Lf/h/a/c/t;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/d1/p$a;Lf/h/a/c/t0;)Lf/h/a/c/f1/i;

    move-result-object p2

    iget-object v0, p2, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    invoke-virtual {v0}, Lf/h/a/c/f1/g;->a()[Lf/h/a/c/f1/f;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    if-eqz v3, :cond_0

    invoke-interface {v3, p1}, Lf/h/a/c/f1/f;->g(F)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object p2
.end method
