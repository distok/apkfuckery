.class public final Lf/h/a/c/q$a;
.super Landroid/content/BroadcastReceiver;
.source "AudioBecomingNoisyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final d:Lf/h/a/c/q$b;

.field public final e:Landroid/os/Handler;

.field public final synthetic f:Lf/h/a/c/q;


# direct methods
.method public constructor <init>(Lf/h/a/c/q;Landroid/os/Handler;Lf/h/a/c/q$b;)V
    .locals 0

    iput-object p1, p0, Lf/h/a/c/q$a;->f:Lf/h/a/c/q;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p2, p0, Lf/h/a/c/q$a;->e:Landroid/os/Handler;

    iput-object p3, p0, Lf/h/a/c/q$a;->d:Lf/h/a/c/q$b;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string p2, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/h/a/c/q$a;->e:Landroid/os/Handler;

    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/q$a;->f:Lf/h/a/c/q;

    iget-boolean v0, v0, Lf/h/a/c/q;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/q$a;->d:Lf/h/a/c/q$b;

    check-cast v0, Lf/h/a/c/s0$c;

    iget-object v0, v0, Lf/h/a/c/s0$c;->d:Lf/h/a/c/s0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/c/s0;->n(Z)V

    :cond_0
    return-void
.end method
