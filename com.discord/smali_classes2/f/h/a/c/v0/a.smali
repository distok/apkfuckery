.class public Lf/h/a/c/v0/a;
.super Ljava/lang/Object;
.source "AnalyticsCollector.java"

# interfaces
.implements Lf/h/a/c/m0$a;
.implements Lf/h/a/c/c1/e;
.implements Lf/h/a/c/w0/l;
.implements Lf/h/a/c/j1/r;
.implements Lf/h/a/c/d1/q;
.implements Lf/h/a/c/h1/e$a;
.implements Lf/h/a/c/j1/q;
.implements Lf/h/a/c/w0/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/v0/a$a;,
        Lf/h/a/c/v0/a$b;
    }
.end annotation


# instance fields
.field public final d:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lf/h/a/c/v0/b;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lf/h/a/c/i1/f;

.field public final f:Lf/h/a/c/t0$c;

.field public final g:Lf/h/a/c/v0/a$b;

.field public h:Lf/h/a/c/m0;


# direct methods
.method public constructor <init>(Lf/h/a/c/i1/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/v0/a;->e:Lf/h/a/c/i1/f;

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-instance p1, Lf/h/a/c/v0/a$b;

    invoke-direct {p1}, Lf/h/a/c/v0/a$b;-><init>()V

    iput-object p1, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    new-instance p1, Lf/h/a/c/t0$c;

    invoke-direct {p1}, Lf/h/a/c/t0$c;-><init>()V

    iput-object p1, p0, Lf/h/a/c/v0/a;->f:Lf/h/a/c/t0$c;

    return-void
.end method


# virtual methods
.method public final A(Lf/h/a/c/y0/d;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->G()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->g()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public B(II)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->i()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final C(Lf/h/a/c/j0;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->u()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public D(Z)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->p()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public E(Lf/h/a/c/t0;ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;
    .locals 12
    .param p3    # Lf/h/a/c/d1/p$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lorg/checkerframework/checker/nullness/qual/RequiresNonNull;
        value = {
            "player"
        }
    .end annotation

    invoke-virtual {p1}, Lf/h/a/c/t0;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p3, 0x0

    :cond_0
    move-object v5, p3

    iget-object p3, p0, Lf/h/a/c/v0/a;->e:Lf/h/a/c/i1/f;

    invoke-interface {p3}, Lf/h/a/c/i1/f;->c()J

    move-result-wide v1

    iget-object p3, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p3}, Lf/h/a/c/m0;->A()Lf/h/a/c/t0;

    move-result-object p3

    const/4 v0, 0x0

    const/4 v3, 0x1

    if-ne p1, p3, :cond_1

    iget-object p3, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p3}, Lf/h/a/c/m0;->m()I

    move-result p3

    if-ne p2, p3, :cond_1

    const/4 p3, 0x1

    goto :goto_0

    :cond_1
    const/4 p3, 0x0

    :goto_0
    const-wide/16 v6, 0x0

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lf/h/a/c/d1/p$a;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz p3, :cond_2

    iget-object p3, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p3}, Lf/h/a/c/m0;->t()I

    move-result p3

    iget v4, v5, Lf/h/a/c/d1/p$a;->b:I

    if-ne p3, v4, :cond_2

    iget-object p3, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p3}, Lf/h/a/c/m0;->k()I

    move-result p3

    iget v4, v5, Lf/h/a/c/d1/p$a;->c:I

    if-ne p3, v4, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eqz v0, :cond_6

    iget-object p3, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p3}, Lf/h/a/c/m0;->G()J

    move-result-wide v3

    goto :goto_1

    :cond_3
    if-eqz p3, :cond_4

    iget-object p3, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p3}, Lf/h/a/c/m0;->p()J

    move-result-wide v3

    :goto_1
    move-wide v6, v3

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lf/h/a/c/t0;->p()Z

    move-result p3

    if-eqz p3, :cond_5

    goto :goto_2

    :cond_5
    iget-object p3, p0, Lf/h/a/c/v0/a;->f:Lf/h/a/c/t0$c;

    invoke-virtual {p1, p2, p3, v6, v7}, Lf/h/a/c/t0;->n(ILf/h/a/c/t0$c;J)Lf/h/a/c/t0$c;

    move-result-object p3

    iget-wide v3, p3, Lf/h/a/c/t0$c;->h:J

    invoke-static {v3, v4}, Lf/h/a/c/u;->b(J)J

    move-result-wide v3

    goto :goto_1

    :cond_6
    :goto_2
    new-instance p3, Lf/h/a/c/v0/b$a;

    iget-object v0, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {v0}, Lf/h/a/c/m0;->G()J

    move-result-wide v8

    iget-object v0, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {v0}, Lf/h/a/c/m0;->d()J

    move-result-wide v10

    move-object v0, p3

    move-object v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v11}, Lf/h/a/c/v0/b$a;-><init>(JLf/h/a/c/t0;ILf/h/a/c/d1/p$a;JJJ)V

    return-object p3
.end method

.method public final F(Lf/h/a/c/v0/a$a;)Lf/h/a/c/v0/b$a;
    .locals 9
    .param p1    # Lf/h/a/c/v0/a$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p1, :cond_6

    iget-object p1, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p1}, Lf/h/a/c/m0;->m()I

    move-result p1

    iget-object v0, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v4, v2

    const/4 v3, 0x0

    :goto_0
    iget-object v5, v0, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    iget-object v5, v0, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/v0/a$a;

    iget-object v6, v0, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    iget-object v7, v5, Lf/h/a/c/v0/a$a;->a:Lf/h/a/c/d1/p$a;

    iget-object v7, v7, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    iget-object v7, v0, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    iget-object v8, v0, Lf/h/a/c/v0/a$b;->c:Lf/h/a/c/t0$b;

    invoke-virtual {v7, v6, v8}, Lf/h/a/c/t0;->f(ILf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object v6

    iget v6, v6, Lf/h/a/c/t0$b;->b:I

    if-ne v6, p1, :cond_1

    if-eqz v4, :cond_0

    move-object v4, v2

    goto :goto_1

    :cond_0
    move-object v4, v5

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-nez v4, :cond_5

    iget-object v0, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {v0}, Lf/h/a/c/m0;->A()Lf/h/a/c/t0;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/c/t0;->o()I

    move-result v3

    if-ge p1, v3, :cond_3

    const/4 v1, 0x1

    :cond_3
    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    sget-object v0, Lf/h/a/c/t0;->a:Lf/h/a/c/t0;

    :goto_2
    invoke-virtual {p0, v0, p1, v2}, Lf/h/a/c/v0/a;->E(Lf/h/a/c/t0;ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;

    move-result-object p1

    return-object p1

    :cond_5
    move-object p1, v4

    :cond_6
    iget-object v0, p1, Lf/h/a/c/v0/a$a;->b:Lf/h/a/c/t0;

    iget v1, p1, Lf/h/a/c/v0/a$a;->c:I

    iget-object p1, p1, Lf/h/a/c/v0/a$a;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {p0, v0, v1, p1}, Lf/h/a/c/v0/a;->E(Lf/h/a/c/t0;ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;

    move-result-object p1

    return-object p1
.end method

.method public final G()Lf/h/a/c/v0/b$a;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v0, v0, Lf/h/a/c/v0/a$b;->e:Lf/h/a/c/v0/a$a;

    invoke-virtual {p0, v0}, Lf/h/a/c/v0/a;->F(Lf/h/a/c/v0/a$a;)Lf/h/a/c/v0/b$a;

    move-result-object v0

    return-object v0
.end method

.method public final H(ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;
    .locals 2
    .param p2    # Lf/h/a/c/d1/p$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lf/h/a/c/t0;->a:Lf/h/a/c/t0;

    iget-object v1, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_1

    iget-object v1, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v1, v1, Lf/h/a/c/v0/a$b;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/v0/a$a;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lf/h/a/c/v0/a;->F(Lf/h/a/c/v0/a$a;)Lf/h/a/c/v0/b$a;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0, p1, p2}, Lf/h/a/c/v0/a;->E(Lf/h/a/c/t0;ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    iget-object p2, p0, Lf/h/a/c/v0/a;->h:Lf/h/a/c/m0;

    invoke-interface {p2}, Lf/h/a/c/m0;->A()Lf/h/a/c/t0;

    move-result-object p2

    invoke-virtual {p2}, Lf/h/a/c/t0;->o()I

    move-result v1

    if-ge p1, v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    move-object v0, p2

    :cond_3
    const/4 p2, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lf/h/a/c/v0/a;->E(Lf/h/a/c/t0;ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;

    move-result-object p1

    return-object p1
.end method

.method public final I()Lf/h/a/c/v0/b$a;
    .locals 2

    iget-object v0, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v1, v0, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    invoke-virtual {v1}, Lf/h/a/c/t0;->p()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lf/h/a/c/v0/a$b;->h:Z

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/a$a;

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0, v0}, Lf/h/a/c/v0/a;->F(Lf/h/a/c/v0/a$a;)Lf/h/a/c/v0/b$a;

    move-result-object v0

    return-object v0
.end method

.method public final J()Lf/h/a/c/v0/b$a;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v0, v0, Lf/h/a/c/v0/a$b;->f:Lf/h/a/c/v0/a$a;

    invoke-virtual {p0, v0}, Lf/h/a/c/v0/a;->F(Lf/h/a/c/v0/a$a;)Lf/h/a/c/v0/b$a;

    move-result-object v0

    return-object v0
.end method

.method public final K(ILf/h/a/c/d1/p$a;)V
    .locals 3

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/v0/a;->H(ILf/h/a/c/d1/p$a;)Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v0, p1, Lf/h/a/c/v0/a$b;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/a$a;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v2, p1, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p1, Lf/h/a/c/v0/a$b;->f:Lf/h/a/c/v0/a$a;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lf/h/a/c/v0/a$a;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {p2, v0}, Lf/h/a/c/d1/p$a;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p1, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    goto :goto_0

    :cond_1
    iget-object p2, p1, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/a$a;

    :goto_0
    iput-object p2, p1, Lf/h/a/c/v0/a$b;->f:Lf/h/a/c/v0/a$a;

    :cond_2
    iget-object p2, p1, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_3

    iget-object p2, p1, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/a$a;

    iput-object p2, p1, Lf/h/a/c/v0/a$b;->d:Lf/h/a/c/v0/a$a;

    :cond_3
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_4

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->q()V

    goto :goto_2

    :cond_4
    return-void
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-boolean v1, v0, Lf/h/a/c/v0/a$b;->h:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lf/h/a/c/v0/a$b;->h:Z

    iget-object v1, v0, Lf/h/a/c/v0/a$b;->d:Lf/h/a/c/v0/a$a;

    iput-object v1, v0, Lf/h/a/c/v0/a$b;->e:Lf/h/a/c/v0/a$a;

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object v0, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/v0/b;

    invoke-interface {v1}, Lf/h/a/c/v0/b;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->x()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(IIIF)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->J()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public e(I)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->E()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final f(Z)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->w()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final g(I)V
    .locals 1

    iget-object p1, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    iget-object v0, p1, Lf/h/a/c/v0/a$b;->d:Lf/h/a/c/v0/a$a;

    iput-object v0, p1, Lf/h/a/c/v0/a$b;->e:Lf/h/a/c/v0/a$a;

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->A()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final h(Lf/h/a/c/y0/d;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->G()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->g()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final i(Lf/h/a/c/y0/d;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->r()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final j(Ljava/lang/String;JJ)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->B()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final k(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->G()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->v()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final l(Lf/h/a/c/t0;I)V
    .locals 4

    iget-object p2, p0, Lf/h/a/c/v0/a;->g:Lf/h/a/c/v0/a$b;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p2, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p2, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/v0/a$a;

    invoke-virtual {p2, v1, p1}, Lf/h/a/c/v0/a$b;->a(Lf/h/a/c/v0/a$a;Lf/h/a/c/t0;)Lf/h/a/c/v0/a$a;

    move-result-object v1

    iget-object v2, p2, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p2, Lf/h/a/c/v0/a$b;->b:Ljava/util/HashMap;

    iget-object v3, v1, Lf/h/a/c/v0/a$a;->a:Lf/h/a/c/d1/p$a;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p2, Lf/h/a/c/v0/a$b;->f:Lf/h/a/c/v0/a$a;

    if-eqz v0, :cond_1

    invoke-virtual {p2, v0, p1}, Lf/h/a/c/v0/a$b;->a(Lf/h/a/c/v0/a$a;Lf/h/a/c/t0;)Lf/h/a/c/v0/a$a;

    move-result-object v0

    iput-object v0, p2, Lf/h/a/c/v0/a$b;->f:Lf/h/a/c/v0/a$a;

    :cond_1
    iput-object p1, p2, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    iget-object p1, p2, Lf/h/a/c/v0/a$b;->d:Lf/h/a/c/v0/a$a;

    iput-object p1, p2, Lf/h/a/c/v0/a$b;->e:Lf/h/a/c/v0/a$a;

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->n()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public m(F)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->C()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final n(Landroid/view/Surface;)V
    .locals 1
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->d()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final o(Ljava/lang/String;JJ)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->B()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final p(Z)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->j()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final q(Lcom/google/android/exoplayer2/metadata/Metadata;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->F()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final r(IJ)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->G()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->f()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final s(ZI)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->s()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic t(Lf/h/a/c/t0;Ljava/lang/Object;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lf/h/a/c/l0;->k(Lf/h/a/c/m0$a;Lf/h/a/c/t0;Ljava/lang/Object;I)V

    return-void
.end method

.method public final u(I)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->e()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final v(Lcom/google/android/exoplayer2/Format;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->m()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final w(Lf/h/a/c/y0/d;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->r()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final x(Lcom/google/android/exoplayer2/Format;)V
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/v0/b;

    invoke-interface {v0}, Lf/h/a/c/v0/b;->m()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final y(IJJ)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->J()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->o()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final z(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/v0/a;->I()Lf/h/a/c/v0/b$a;

    iget-object p1, p0, Lf/h/a/c/v0/a;->d:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lf/h/a/c/v0/b;

    invoke-interface {p2}, Lf/h/a/c/v0/b;->I()V

    goto :goto_0

    :cond_0
    return-void
.end method
