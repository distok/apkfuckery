.class public final Lf/h/a/c/v0/a$b;
.super Ljava/lang/Object;
.source "AnalyticsCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/v0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lf/h/a/c/v0/a$a;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lf/h/a/c/d1/p$a;",
            "Lf/h/a/c/v0/a$a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/h/a/c/t0$b;

.field public d:Lf/h/a/c/v0/a$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public e:Lf/h/a/c/v0/a$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public f:Lf/h/a/c/v0/a$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public g:Lf/h/a/c/t0;

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/a/c/v0/a$b;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h/a/c/v0/a$b;->b:Ljava/util/HashMap;

    new-instance v0, Lf/h/a/c/t0$b;

    invoke-direct {v0}, Lf/h/a/c/t0$b;-><init>()V

    iput-object v0, p0, Lf/h/a/c/v0/a$b;->c:Lf/h/a/c/t0$b;

    sget-object v0, Lf/h/a/c/t0;->a:Lf/h/a/c/t0;

    iput-object v0, p0, Lf/h/a/c/v0/a$b;->g:Lf/h/a/c/t0;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/c/v0/a$a;Lf/h/a/c/t0;)Lf/h/a/c/v0/a$a;
    .locals 2

    iget-object v0, p1, Lf/h/a/c/v0/a$a;->a:Lf/h/a/c/d1/p$a;

    iget-object v0, v0, Lf/h/a/c/d1/p$a;->a:Ljava/lang/Object;

    invoke-virtual {p2, v0}, Lf/h/a/c/t0;->b(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-object p1

    :cond_0
    iget-object v1, p0, Lf/h/a/c/v0/a$b;->c:Lf/h/a/c/t0$b;

    invoke-virtual {p2, v0, v1}, Lf/h/a/c/t0;->f(ILf/h/a/c/t0$b;)Lf/h/a/c/t0$b;

    move-result-object v0

    iget v0, v0, Lf/h/a/c/t0$b;->b:I

    new-instance v1, Lf/h/a/c/v0/a$a;

    iget-object p1, p1, Lf/h/a/c/v0/a$a;->a:Lf/h/a/c/d1/p$a;

    invoke-direct {v1, p1, p2, v0}, Lf/h/a/c/v0/a$a;-><init>(Lf/h/a/c/d1/p$a;Lf/h/a/c/t0;I)V

    return-object v1
.end method
