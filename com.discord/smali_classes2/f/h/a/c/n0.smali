.class public final Lf/h/a/c/n0;
.super Ljava/lang/Object;
.source "PlayerMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/n0$a;,
        Lf/h/a/c/n0$b;
    }
.end annotation


# instance fields
.field public final a:Lf/h/a/c/n0$b;

.field public final b:Lf/h/a/c/n0$a;

.field public final c:Lf/h/a/c/t0;

.field public d:I

.field public e:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public f:Landroid/os/Handler;

.field public g:I

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Lf/h/a/c/n0$a;Lf/h/a/c/n0$b;Lf/h/a/c/t0;ILandroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/n0;->b:Lf/h/a/c/n0$a;

    iput-object p2, p0, Lf/h/a/c/n0;->a:Lf/h/a/c/n0$b;

    iput-object p3, p0, Lf/h/a/c/n0;->c:Lf/h/a/c/t0;

    iput-object p5, p0, Lf/h/a/c/n0;->f:Landroid/os/Handler;

    iput p4, p0, Lf/h/a/c/n0;->g:I

    return-void
.end method


# virtual methods
.method public declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized b(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf/h/a/c/n0;->i:Z

    or-int/2addr p1, v0

    iput-boolean p1, p0, Lf/h/a/c/n0;->i:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/n0;->j:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public c()Lf/h/a/c/n0;
    .locals 3

    iget-boolean v0, p0, Lf/h/a/c/n0;->h:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    invoke-static {v1}, Lf/g/j/k/a;->d(Z)V

    iput-boolean v1, p0, Lf/h/a/c/n0;->h:Z

    iget-object v0, p0, Lf/h/a/c/n0;->b:Lf/h/a/c/n0$a;

    check-cast v0, Lf/h/a/c/b0;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, v0, Lf/h/a/c/b0;->z:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lf/h/a/c/b0;->k:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lf/h/a/c/b0;->j:Lf/h/a/c/i1/x;

    const/16 v2, 0xf

    invoke-virtual {v1, v2, p0}, Lf/h/a/c/i1/x;->b(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    goto :goto_1

    :cond_1
    :goto_0
    :try_start_1
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Ignoring messages sent after release."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lf/h/a/c/n0;->b(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    :goto_1
    return-object p0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public d(Ljava/lang/Object;)Lf/h/a/c/n0;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-boolean v0, p0, Lf/h/a/c/n0;->h:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iput-object p1, p0, Lf/h/a/c/n0;->e:Ljava/lang/Object;

    return-object p0
.end method

.method public e(I)Lf/h/a/c/n0;
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/n0;->h:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iput p1, p0, Lf/h/a/c/n0;->d:I

    return-object p0
.end method
