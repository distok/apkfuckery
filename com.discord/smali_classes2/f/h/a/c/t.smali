.class public abstract Lf/h/a/c/t;
.super Ljava/lang/Object;
.source "BaseRenderer.java"

# interfaces
.implements Lf/h/a/c/p0;


# instance fields
.field public final d:I

.field public final e:Lf/h/a/c/d0;

.field public f:Lf/h/a/c/q0;

.field public g:I

.field public h:I

.field public i:Lf/h/a/c/d1/v;

.field public j:[Lcom/google/android/exoplayer2/Format;

.field public k:J

.field public l:J

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/h/a/c/t;->d:I

    new-instance p1, Lf/h/a/c/d0;

    invoke-direct {p1}, Lf/h/a/c/d0;-><init>()V

    iput-object p1, p0, Lf/h/a/c/t;->e:Lf/h/a/c/d0;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lf/h/a/c/t;->l:J

    return-void
.end method

.method public static H(Lf/h/a/c/z0/b;Lcom/google/android/exoplayer2/drm/DrmInitData;)Z
    .locals 0
    .param p0    # Lf/h/a/c/z0/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/exoplayer2/drm/DrmInitData;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/a/c/z0/b<",
            "*>;",
            "Lcom/google/android/exoplayer2/drm/DrmInitData;",
            ")Z"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    if-nez p0, :cond_1

    const/4 p0, 0x0

    return p0

    :cond_1
    invoke-interface {p0, p1}, Lf/h/a/c/z0/b;->d(Lcom/google/android/exoplayer2/drm/DrmInitData;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public abstract A(JZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public B()V
    .locals 0

    return-void
.end method

.method public C()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    return-void
.end method

.method public D()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    return-void
.end method

.method public abstract E([Lcom/google/android/exoplayer2/Format;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public final F(Lf/h/a/c/d0;Lf/h/a/c/y0/e;Z)I
    .locals 5

    iget-object v0, p0, Lf/h/a/c/t;->i:Lf/h/a/c/d1/v;

    invoke-interface {v0, p1, p2, p3}, Lf/h/a/c/d1/v;->b(Lf/h/a/c/d0;Lf/h/a/c/y0/e;Z)I

    move-result p3

    const/4 v0, -0x4

    if-ne p3, v0, :cond_2

    invoke-virtual {p2}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result p1

    if-eqz p1, :cond_1

    const-wide/high16 p1, -0x8000000000000000L

    iput-wide p1, p0, Lf/h/a/c/t;->l:J

    iget-boolean p1, p0, Lf/h/a/c/t;->m:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_1
    iget-wide v0, p2, Lf/h/a/c/y0/e;->f:J

    iget-wide v2, p0, Lf/h/a/c/t;->k:J

    add-long/2addr v0, v2

    iput-wide v0, p2, Lf/h/a/c/y0/e;->f:J

    iget-wide p1, p0, Lf/h/a/c/t;->l:J

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    iput-wide p1, p0, Lf/h/a/c/t;->l:J

    goto :goto_1

    :cond_2
    const/4 p2, -0x5

    if-ne p3, p2, :cond_3

    iget-object p2, p1, Lf/h/a/c/d0;->c:Lcom/google/android/exoplayer2/Format;

    iget-wide v0, p2, Lcom/google/android/exoplayer2/Format;->p:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, v0, v2

    if-eqz v4, :cond_3

    iget-wide v2, p0, Lf/h/a/c/t;->k:J

    add-long/2addr v0, v2

    invoke-virtual {p2, v0, v1}, Lcom/google/android/exoplayer2/Format;->d(J)Lcom/google/android/exoplayer2/Format;

    move-result-object p2

    iput-object p2, p1, Lf/h/a/c/d0;->c:Lcom/google/android/exoplayer2/Format;

    :cond_3
    :goto_1
    return p3
.end method

.method public abstract G(Lcom/google/android/exoplayer2/Format;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public I()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public d(ILjava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    return-void
.end method

.method public final f(I)V
    .locals 0

    iput p1, p0, Lf/h/a/c/t;->g:I

    return-void
.end method

.method public final getState()I
    .locals 1

    iget v0, p0, Lf/h/a/c/t;->h:I

    return v0
.end method

.method public final h()V
    .locals 3

    iget v0, p0, Lf/h/a/c/t;->h:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iget-object v0, p0, Lf/h/a/c/t;->e:Lf/h/a/c/d0;

    invoke-virtual {v0}, Lf/h/a/c/d0;->a()V

    iput v2, p0, Lf/h/a/c/t;->h:I

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/t;->i:Lf/h/a/c/d1/v;

    iput-object v0, p0, Lf/h/a/c/t;->j:[Lcom/google/android/exoplayer2/Format;

    iput-boolean v2, p0, Lf/h/a/c/t;->m:Z

    invoke-virtual {p0}, Lf/h/a/c/t;->y()V

    return-void
.end method

.method public final i()Lf/h/a/c/d1/v;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/t;->i:Lf/h/a/c/d1/v;

    return-object v0
.end method

.method public final j()Z
    .locals 5

    iget-wide v0, p0, Lf/h/a/c/t;->l:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final k(Lf/h/a/c/q0;[Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d1/v;JZJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget v0, p0, Lf/h/a/c/t;->h:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iput-object p1, p0, Lf/h/a/c/t;->f:Lf/h/a/c/q0;

    iput v1, p0, Lf/h/a/c/t;->h:I

    invoke-virtual {p0, p6}, Lf/h/a/c/t;->z(Z)V

    iget-boolean p1, p0, Lf/h/a/c/t;->m:Z

    xor-int/2addr p1, v1

    invoke-static {p1}, Lf/g/j/k/a;->s(Z)V

    iput-object p3, p0, Lf/h/a/c/t;->i:Lf/h/a/c/d1/v;

    iput-wide p7, p0, Lf/h/a/c/t;->l:J

    iput-object p2, p0, Lf/h/a/c/t;->j:[Lcom/google/android/exoplayer2/Format;

    iput-wide p7, p0, Lf/h/a/c/t;->k:J

    invoke-virtual {p0, p2, p7, p8}, Lf/h/a/c/t;->E([Lcom/google/android/exoplayer2/Format;J)V

    invoke-virtual {p0, p4, p5, p6}, Lf/h/a/c/t;->A(JZ)V

    return-void
.end method

.method public final l()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/a/c/t;->m:Z

    return-void
.end method

.method public final m()Lf/h/a/c/t;
    .locals 0

    return-object p0
.end method

.method public synthetic o(F)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/o0;->a(Lf/h/a/c/p0;F)V

    return-void
.end method

.method public final p()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/t;->i:Lf/h/a/c/d1/v;

    invoke-interface {v0}, Lf/h/a/c/d1/v;->c()V

    return-void
.end method

.method public final q()J
    .locals 2

    iget-wide v0, p0, Lf/h/a/c/t;->l:J

    return-wide v0
.end method

.method public final r(J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/a/c/t;->m:Z

    iput-wide p1, p0, Lf/h/a/c/t;->l:J

    invoke-virtual {p0, p1, p2, v0}, Lf/h/a/c/t;->A(JZ)V

    return-void
.end method

.method public final reset()V
    .locals 1

    iget v0, p0, Lf/h/a/c/t;->h:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iget-object v0, p0, Lf/h/a/c/t;->e:Lf/h/a/c/d0;

    invoke-virtual {v0}, Lf/h/a/c/d0;->a()V

    invoke-virtual {p0}, Lf/h/a/c/t;->B()V

    return-void
.end method

.method public final s()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/t;->m:Z

    return v0
.end method

.method public final start()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget v0, p0, Lf/h/a/c/t;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    const/4 v0, 0x2

    iput v0, p0, Lf/h/a/c/t;->h:I

    invoke-virtual {p0}, Lf/h/a/c/t;->C()V

    return-void
.end method

.method public final stop()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget v0, p0, Lf/h/a/c/t;->h:I

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iput v1, p0, Lf/h/a/c/t;->h:I

    invoke-virtual {p0}, Lf/h/a/c/t;->D()V

    return-void
.end method

.method public t()Lf/h/a/c/i1/n;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final u()I
    .locals 1

    iget v0, p0, Lf/h/a/c/t;->d:I

    return v0
.end method

.method public final v([Lcom/google/android/exoplayer2/Format;Lf/h/a/c/d1/v;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-boolean v0, p0, Lf/h/a/c/t;->m:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iput-object p2, p0, Lf/h/a/c/t;->i:Lf/h/a/c/d1/v;

    iput-wide p3, p0, Lf/h/a/c/t;->l:J

    iput-object p1, p0, Lf/h/a/c/t;->j:[Lcom/google/android/exoplayer2/Format;

    iput-wide p3, p0, Lf/h/a/c/t;->k:J

    invoke-virtual {p0, p1, p3, p4}, Lf/h/a/c/t;->E([Lcom/google/android/exoplayer2/Format;J)V

    return-void
.end method

.method public final w(Ljava/lang/Exception;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/ExoPlaybackException;
    .locals 9
    .param p2    # Lcom/google/android/exoplayer2/Format;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x4

    if-eqz p2, :cond_0

    iget-boolean v1, p0, Lf/h/a/c/t;->n:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/h/a/c/t;->n:Z

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p2}, Lf/h/a/c/t;->G(Lcom/google/android/exoplayer2/Format;)I

    move-result v2
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit8 v2, v2, 0x7

    iput-boolean v1, p0, Lf/h/a/c/t;->n:Z

    goto :goto_0

    :catchall_0
    move-exception p1

    iput-boolean v1, p0, Lf/h/a/c/t;->n:Z

    throw p1

    :catch_0
    iput-boolean v1, p0, Lf/h/a/c/t;->n:Z

    :cond_0
    const/4 v2, 0x4

    :goto_0
    iget v6, p0, Lf/h/a/c/t;->g:I

    new-instance v1, Lcom/google/android/exoplayer2/ExoPlaybackException;

    if-nez p2, :cond_1

    const/4 v8, 0x4

    goto :goto_1

    :cond_1
    move v8, v2

    :goto_1
    const/4 v4, 0x1

    move-object v3, v1

    move-object v5, p1

    move-object v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/exoplayer2/ExoPlaybackException;-><init>(ILjava/lang/Throwable;ILcom/google/android/exoplayer2/Format;I)V

    return-object v1
.end method

.method public final x()Lf/h/a/c/d0;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/t;->e:Lf/h/a/c/d0;

    invoke-virtual {v0}, Lf/h/a/c/d0;->a()V

    iget-object v0, p0, Lf/h/a/c/t;->e:Lf/h/a/c/d0;

    return-object v0
.end method

.method public abstract y()V
.end method

.method public z(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    return-void
.end method
