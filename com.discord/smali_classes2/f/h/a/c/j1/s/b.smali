.class public Lf/h/a/c/j1/s/b;
.super Lf/h/a/c/t;
.source "CameraMotionRenderer.java"


# instance fields
.field public final o:Lf/h/a/c/y0/e;

.field public final p:Lf/h/a/c/i1/r;

.field public q:J

.field public r:Lf/h/a/c/j1/s/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public s:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lf/h/a/c/t;-><init>(I)V

    new-instance v0, Lf/h/a/c/y0/e;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lf/h/a/c/y0/e;-><init>(I)V

    iput-object v0, p0, Lf/h/a/c/j1/s/b;->o:Lf/h/a/c/y0/e;

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/j1/s/b;->p:Lf/h/a/c/i1/r;

    return-void
.end method


# virtual methods
.method public A(JZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lf/h/a/c/j1/s/b;->s:J

    iget-object p1, p0, Lf/h/a/c/j1/s/b;->r:Lf/h/a/c/j1/s/a;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lf/h/a/c/j1/s/a;->b()V

    :cond_0
    return-void
.end method

.method public E([Lcom/google/android/exoplayer2/Format;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iput-wide p2, p0, Lf/h/a/c/j1/s/b;->q:J

    return-void
.end method

.method public G(Lcom/google/android/exoplayer2/Format;)I
    .locals 1

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v0, "application/x-camera-motion"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d(ILjava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    check-cast p2, Lf/h/a/c/j1/s/a;

    iput-object p2, p0, Lf/h/a/c/j1/s/b;->r:Lf/h/a/c/j1/s/a;

    :cond_0
    return-void
.end method

.method public g()Z
    .locals 1

    invoke-virtual {p0}, Lf/h/a/c/t;->j()Z

    move-result v0

    return v0
.end method

.method public n(JJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lf/h/a/c/t;->j()Z

    move-result p3

    if-nez p3, :cond_4

    iget-wide p3, p0, Lf/h/a/c/j1/s/b;->s:J

    const-wide/32 v0, 0x186a0

    add-long/2addr v0, p1

    cmp-long v2, p3, v0

    if-gez v2, :cond_4

    iget-object p3, p0, Lf/h/a/c/j1/s/b;->o:Lf/h/a/c/y0/e;

    invoke-virtual {p3}, Lf/h/a/c/y0/e;->clear()V

    invoke-virtual {p0}, Lf/h/a/c/t;->x()Lf/h/a/c/d0;

    move-result-object p3

    iget-object p4, p0, Lf/h/a/c/j1/s/b;->o:Lf/h/a/c/y0/e;

    const/4 v0, 0x0

    invoke-virtual {p0, p3, p4, v0}, Lf/h/a/c/t;->F(Lf/h/a/c/d0;Lf/h/a/c/y0/e;Z)I

    move-result p3

    const/4 p4, -0x4

    if-ne p3, p4, :cond_4

    iget-object p3, p0, Lf/h/a/c/j1/s/b;->o:Lf/h/a/c/y0/e;

    invoke-virtual {p3}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result p3

    if-eqz p3, :cond_1

    goto :goto_3

    :cond_1
    iget-object p3, p0, Lf/h/a/c/j1/s/b;->o:Lf/h/a/c/y0/e;

    invoke-virtual {p3}, Lf/h/a/c/y0/e;->l()V

    iget-object p3, p0, Lf/h/a/c/j1/s/b;->o:Lf/h/a/c/y0/e;

    iget-wide v1, p3, Lf/h/a/c/y0/e;->f:J

    iput-wide v1, p0, Lf/h/a/c/j1/s/b;->s:J

    iget-object p4, p0, Lf/h/a/c/j1/s/b;->r:Lf/h/a/c/j1/s/a;

    if-eqz p4, :cond_0

    iget-object p3, p3, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    sget p4, Lf/h/a/c/i1/a0;->a:I

    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result p4

    const/16 v1, 0x10

    if-eq p4, v1, :cond_2

    const/4 p3, 0x0

    goto :goto_2

    :cond_2
    iget-object p4, p0, Lf/h/a/c/j1/s/b;->p:Lf/h/a/c/i1/r;

    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-virtual {p4, v1, v2}, Lf/h/a/c/i1/r;->A([BI)V

    iget-object p4, p0, Lf/h/a/c/j1/s/b;->p:Lf/h/a/c/i1/r;

    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result p3

    add-int/lit8 p3, p3, 0x4

    invoke-virtual {p4, p3}, Lf/h/a/c/i1/r;->C(I)V

    const/4 p3, 0x3

    new-array p4, p3, [F

    :goto_1
    if-ge v0, p3, :cond_3

    iget-object v1, p0, Lf/h/a/c/j1/s/b;->p:Lf/h/a/c/i1/r;

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    aput v1, p4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object p3, p4

    :goto_2
    if-eqz p3, :cond_0

    iget-object p4, p0, Lf/h/a/c/j1/s/b;->r:Lf/h/a/c/j1/s/a;

    iget-wide v0, p0, Lf/h/a/c/j1/s/b;->s:J

    iget-wide v2, p0, Lf/h/a/c/j1/s/b;->q:J

    sub-long/2addr v0, v2

    invoke-interface {p4, v0, v1, p3}, Lf/h/a/c/j1/s/a;->a(J[F)V

    goto/16 :goto_0

    :cond_4
    :goto_3
    return-void
.end method

.method public y()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lf/h/a/c/j1/s/b;->s:J

    iget-object v0, p0, Lf/h/a/c/j1/s/b;->r:Lf/h/a/c/j1/s/a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/h/a/c/j1/s/a;->b()V

    :cond_0
    return-void
.end method
