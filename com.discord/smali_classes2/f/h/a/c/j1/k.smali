.class public Lf/h/a/c/j1/k;
.super Landroid/opengl/GLSurfaceView;
.source "VideoDecoderGLSurfaceView.java"


# instance fields
.field public final d:Lf/h/a/c/j1/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Lf/h/a/c/j1/m;

    invoke-direct {p1, p0}, Lf/h/a/c/j1/m;-><init>(Landroid/opengl/GLSurfaceView;)V

    iput-object p1, p0, Lf/h/a/c/j1/k;->d:Lf/h/a/c/j1/m;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->setPreserveEGLContextOnPause(Z)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->setEGLContextClientVersion(I)V

    invoke-virtual {p0, p1}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    return-void
.end method


# virtual methods
.method public getVideoDecoderOutputBufferRenderer()Lf/h/a/c/j1/l;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/j1/k;->d:Lf/h/a/c/j1/m;

    return-object v0
.end method
