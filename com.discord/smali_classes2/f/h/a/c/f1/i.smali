.class public final Lf/h/a/c/f1/i;
.super Ljava/lang/Object;
.source "TrackSelectorResult.java"


# instance fields
.field public final a:I

.field public final b:[Lf/h/a/c/q0;

.field public final c:Lf/h/a/c/f1/g;

.field public final d:Ljava/lang/Object;


# direct methods
.method public constructor <init>([Lf/h/a/c/q0;[Lf/h/a/c/f1/f;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/f1/i;->b:[Lf/h/a/c/q0;

    new-instance v0, Lf/h/a/c/f1/g;

    invoke-direct {v0, p2}, Lf/h/a/c/f1/g;-><init>([Lf/h/a/c/f1/f;)V

    iput-object v0, p0, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iput-object p3, p0, Lf/h/a/c/f1/i;->d:Ljava/lang/Object;

    array-length p1, p1

    iput p1, p0, Lf/h/a/c/f1/i;->a:I

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/f1/i;I)Z
    .locals 3
    .param p1    # Lf/h/a/c/f1/i;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lf/h/a/c/f1/i;->b:[Lf/h/a/c/q0;

    aget-object v1, v1, p2

    iget-object v2, p1, Lf/h/a/c/f1/i;->b:[Lf/h/a/c/q0;

    aget-object v2, v2, p2

    invoke-static {v1, v2}, Lf/h/a/c/i1/a0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget-object v1, v1, Lf/h/a/c/f1/g;->b:[Lf/h/a/c/f1/f;

    aget-object v1, v1, p2

    iget-object p1, p1, Lf/h/a/c/f1/i;->c:Lf/h/a/c/f1/g;

    iget-object p1, p1, Lf/h/a/c/f1/g;->b:[Lf/h/a/c/f1/f;

    aget-object p1, p1, p2

    invoke-static {v1, p1}, Lf/h/a/c/i1/a0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public b(I)Z
    .locals 1

    iget-object v0, p0, Lf/h/a/c/f1/i;->b:[Lf/h/a/c/q0;

    aget-object p1, v0, p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
