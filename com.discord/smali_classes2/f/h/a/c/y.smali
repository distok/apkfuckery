.class public final Lf/h/a/c/y;
.super Ljava/lang/Object;
.source "DefaultMediaClock.java"

# interfaces
.implements Lf/h/a/c/i1/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/y$a;
    }
.end annotation


# instance fields
.field public final d:Lf/h/a/c/i1/v;

.field public final e:Lf/h/a/c/y$a;

.field public f:Lf/h/a/c/p0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public g:Lf/h/a/c/i1/n;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>(Lf/h/a/c/y$a;Lf/h/a/c/i1/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/y;->e:Lf/h/a/c/y$a;

    new-instance p1, Lf/h/a/c/i1/v;

    invoke-direct {p1, p2}, Lf/h/a/c/i1/v;-><init>(Lf/h/a/c/i1/f;)V

    iput-object p1, p0, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/h/a/c/y;->h:Z

    return-void
.end method


# virtual methods
.method public b()Lf/h/a/c/j0;
    .locals 1

    iget-object v0, p0, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/h/a/c/i1/n;->b()Lf/h/a/c/j0;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    iget-object v0, v0, Lf/h/a/c/i1/v;->h:Lf/h/a/c/j0;

    :goto_0
    return-object v0
.end method

.method public c()J
    .locals 2

    iget-boolean v0, p0, Lf/h/a/c/y;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v0}, Lf/h/a/c/i1/v;->c()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    invoke-interface {v0}, Lf/h/a/c/i1/n;->c()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public e(Lf/h/a/c/j0;)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lf/h/a/c/i1/n;->e(Lf/h/a/c/j0;)V

    iget-object p1, p0, Lf/h/a/c/y;->g:Lf/h/a/c/i1/n;

    invoke-interface {p1}, Lf/h/a/c/i1/n;->b()Lf/h/a/c/j0;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lf/h/a/c/y;->d:Lf/h/a/c/i1/v;

    invoke-virtual {v0, p1}, Lf/h/a/c/i1/v;->e(Lf/h/a/c/j0;)V

    return-void
.end method
