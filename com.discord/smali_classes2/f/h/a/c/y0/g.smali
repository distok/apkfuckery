.class public abstract Lf/h/a/c/y0/g;
.super Ljava/lang/Object;
.source "SimpleDecoder.java"

# interfaces
.implements Lf/h/a/c/y0/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Lf/h/a/c/y0/e;",
        "O:",
        "Lf/h/a/c/y0/f;",
        "E:",
        "Ljava/lang/Exception;",
        ">",
        "Ljava/lang/Object;",
        "Lf/h/a/c/y0/c<",
        "TI;TO;TE;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Thread;

.field public final b:Ljava/lang/Object;

.field public final c:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "TI;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "TO;>;"
        }
    .end annotation
.end field

.field public final e:[Lf/h/a/c/y0/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TI;"
        }
    .end annotation
.end field

.field public final f:[Lf/h/a/c/y0/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TO;"
        }
    .end annotation
.end field

.field public g:I

.field public h:I

.field public i:Lf/h/a/c/y0/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TI;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:I


# direct methods
.method public constructor <init>([Lf/h/a/c/y0/e;[Lf/h/a/c/y0/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TI;[TO;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lf/h/a/c/y0/g;->c:Ljava/util/ArrayDeque;

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lf/h/a/c/y0/g;->d:Ljava/util/ArrayDeque;

    iput-object p1, p0, Lf/h/a/c/y0/g;->e:[Lf/h/a/c/y0/e;

    array-length p1, p1

    iput p1, p0, Lf/h/a/c/y0/g;->g:I

    const/4 p1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lf/h/a/c/y0/g;->g:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/h/a/c/y0/g;->e:[Lf/h/a/c/y0/e;

    new-instance v2, Lf/h/a/c/e1/h;

    invoke-direct {v2}, Lf/h/a/c/e1/h;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object p2, p0, Lf/h/a/c/y0/g;->f:[Lf/h/a/c/y0/f;

    array-length p2, p2

    iput p2, p0, Lf/h/a/c/y0/g;->h:I

    :goto_1
    iget p2, p0, Lf/h/a/c/y0/g;->h:I

    if-ge p1, p2, :cond_1

    iget-object p2, p0, Lf/h/a/c/y0/g;->f:[Lf/h/a/c/y0/f;

    move-object v0, p0

    check-cast v0, Lf/h/a/c/e1/c;

    new-instance v1, Lf/h/a/c/e1/d;

    invoke-direct {v1, v0}, Lf/h/a/c/e1/d;-><init>(Lf/h/a/c/e1/c;)V

    aput-object v1, p2, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_1
    new-instance p1, Lf/h/a/c/y0/g$a;

    invoke-direct {p1, p0}, Lf/h/a/c/y0/g$a;-><init>(Lf/h/a/c/y0/g;)V

    iput-object p1, p0, Lf/h/a/c/y0/g;->a:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public b()Ljava/lang/Object;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/c/y0/g;->h()V

    iget-object v1, p0, Lf/h/a/c/y0/g;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    monitor-exit v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/h/a/c/y0/g;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/y0/f;

    monitor-exit v0

    :goto_0
    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public c()Ljava/lang/Object;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/c/y0/g;->h()V

    iget-object v1, p0, Lf/h/a/c/y0/g;->i:Lf/h/a/c/y0/e;

    const/4 v2, 0x1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iget v1, p0, Lf/h/a/c/y0/g;->g:I

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lf/h/a/c/y0/g;->e:[Lf/h/a/c/y0/e;

    sub-int/2addr v1, v2

    iput v1, p0, Lf/h/a/c/y0/g;->g:I

    aget-object v1, v3, v1

    :goto_1
    iput-object v1, p0, Lf/h/a/c/y0/g;->i:Lf/h/a/c/y0/e;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public d(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lf/h/a/c/y0/e;

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/c/y0/g;->h()V

    iget-object v1, p0, Lf/h/a/c/y0/g;->i:Lf/h/a/c/y0/e;

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lf/g/j/k/a;->d(Z)V

    iget-object v1, p0, Lf/h/a/c/y0/g;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lf/h/a/c/y0/g;->g()V

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/y0/g;->i:Lf/h/a/c/y0/e;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public abstract e(Lf/h/a/c/y0/e;Lf/h/a/c/y0/f;Z)Ljava/lang/Exception;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;TO;Z)TE;"
        }
    .end annotation
.end method

.method public final f()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lf/h/a/c/y0/g;->l:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/y0/g;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lf/h/a/c/y0/g;->h:I

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lf/h/a/c/y0/g;->l:Z

    if-eqz v1, :cond_2

    monitor-exit v0

    return v2

    :cond_2
    iget-object v1, p0, Lf/h/a/c/y0/g;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/y0/e;

    iget-object v4, p0, Lf/h/a/c/y0/g;->f:[Lf/h/a/c/y0/f;

    iget v5, p0, Lf/h/a/c/y0/g;->h:I

    sub-int/2addr v5, v3

    iput v5, p0, Lf/h/a/c/y0/g;->h:I

    aget-object v4, v4, v5

    iget-boolean v5, p0, Lf/h/a/c/y0/g;->k:Z

    iput-boolean v2, p0, Lf/h/a/c/y0/g;->k:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    invoke-virtual {v1}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Lf/h/a/c/y0/a;->addFlag(I)V

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Lf/h/a/c/y0/a;->isDecodeOnly()Z

    move-result v0

    if-eqz v0, :cond_4

    const/high16 v0, -0x80000000

    invoke-virtual {v4, v0}, Lf/h/a/c/y0/a;->addFlag(I)V

    :cond_4
    :try_start_1
    invoke-virtual {p0, v1, v4, v5}, Lf/h/a/c/y0/g;->e(Lf/h/a/c/y0/e;Lf/h/a/c/y0/f;Z)Ljava/lang/Exception;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/y0/g;->j:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v5, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;

    const-string v6, "Unexpected decode error"

    invoke-direct {v5, v6, v0}, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v5, p0, Lf/h/a/c/y0/g;->j:Ljava/lang/Exception;

    goto :goto_2

    :catch_1
    move-exception v0

    new-instance v5, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;

    const-string v6, "Unexpected decode error"

    invoke-direct {v5, v6, v0}, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v5, p0, Lf/h/a/c/y0/g;->j:Ljava/lang/Exception;

    :goto_2
    iget-object v0, p0, Lf/h/a/c/y0/g;->j:Ljava/lang/Exception;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_2
    monitor-exit v0

    return v2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_5
    :goto_3
    iget-object v5, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v5

    :try_start_3
    iget-boolean v0, p0, Lf/h/a/c/y0/g;->k:Z

    if-eqz v0, :cond_6

    invoke-virtual {v4}, Lf/h/a/c/y0/f;->release()V

    goto :goto_4

    :cond_6
    invoke-virtual {v4}, Lf/h/a/c/y0/a;->isDecodeOnly()Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lf/h/a/c/y0/g;->m:I

    add-int/2addr v0, v3

    iput v0, p0, Lf/h/a/c/y0/g;->m:I

    invoke-virtual {v4}, Lf/h/a/c/y0/f;->release()V

    goto :goto_4

    :cond_7
    iget v0, p0, Lf/h/a/c/y0/g;->m:I

    iput v0, v4, Lf/h/a/c/y0/f;->skippedOutputBufferCount:I

    iput v2, p0, Lf/h/a/c/y0/g;->m:I

    iget-object v0, p0, Lf/h/a/c/y0/g;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0, v4}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    :goto_4
    invoke-virtual {p0, v1}, Lf/h/a/c/y0/g;->i(Lf/h/a/c/y0/e;)V

    monitor-exit v5

    return v3

    :catchall_1
    move-exception v0

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :catchall_2
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1
.end method

.method public final flush()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lf/h/a/c/y0/g;->k:Z

    const/4 v1, 0x0

    iput v1, p0, Lf/h/a/c/y0/g;->m:I

    iget-object v1, p0, Lf/h/a/c/y0/g;->i:Lf/h/a/c/y0/e;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lf/h/a/c/y0/g;->i(Lf/h/a/c/y0/e;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lf/h/a/c/y0/g;->i:Lf/h/a/c/y0/e;

    :cond_0
    :goto_0
    iget-object v1, p0, Lf/h/a/c/y0/g;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/y0/g;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/y0/e;

    invoke-virtual {p0, v1}, Lf/h/a/c/y0/g;->i(Lf/h/a/c/y0/e;)V

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v1, p0, Lf/h/a/c/y0/g;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lf/h/a/c/y0/g;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/y0/f;

    invoke-virtual {v1}, Lf/h/a/c/y0/f;->release()V

    goto :goto_1

    :cond_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/y0/g;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lf/h/a/c/y0/g;->h:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    :cond_1
    return-void
.end method

.method public final h()V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V^TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/y0/g;->j:Ljava/lang/Exception;

    if-nez v0, :cond_0

    return-void

    :cond_0
    throw v0
.end method

.method public final i(Lf/h/a/c/y0/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lf/h/a/c/y0/e;->clear()V

    iget-object v0, p0, Lf/h/a/c/y0/g;->e:[Lf/h/a/c/y0/e;

    iget v1, p0, Lf/h/a/c/y0/g;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lf/h/a/c/y0/g;->g:I

    aput-object p1, v0, v1

    return-void
.end method

.method public release()V
    .locals 2
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lf/h/a/c/y0/g;->l:Z

    iget-object v1, p0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lf/h/a/c/y0/g;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
