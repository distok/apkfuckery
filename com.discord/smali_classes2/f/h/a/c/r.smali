.class public final Lf/h/a/c/r;
.super Ljava/lang/Object;
.source "AudioFocusManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/r$a;,
        Lf/h/a/c/r$b;
    }
.end annotation


# instance fields
.field public final a:Landroid/media/AudioManager;

.field public final b:Lf/h/a/c/r$a;

.field public final c:Lf/h/a/c/r$b;

.field public d:I

.field public e:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lf/h/a/c/r$b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lf/h/a/c/r;->e:F

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lf/h/a/c/r;->a:Landroid/media/AudioManager;

    iput-object p3, p0, Lf/h/a/c/r;->c:Lf/h/a/c/r$b;

    new-instance p1, Lf/h/a/c/r$a;

    invoke-direct {p1, p0, p2}, Lf/h/a/c/r$a;-><init>(Lf/h/a/c/r;Landroid/os/Handler;)V

    iput-object p1, p0, Lf/h/a/c/r;->b:Lf/h/a/c/r$a;

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/r;->d:I

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    iget p1, p0, Lf/h/a/c/r;->d:I

    if-nez p1, :cond_0

    return-void

    :cond_0
    sget p1, Lf/h/a/c/i1/a0;->a:I

    const/16 v0, 0x1a

    if-lt p1, v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/h/a/c/r;->a:Landroid/media/AudioManager;

    iget-object v0, p0, Lf/h/a/c/r;->b:Lf/h/a/c/r$a;

    invoke-virtual {p1, v0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :goto_0
    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/r;->d:I

    return-void
.end method
