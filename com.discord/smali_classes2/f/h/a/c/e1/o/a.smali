.class public final Lf/h/a/c/e1/o/a;
.super Lf/h/a/c/e1/c;
.source "SsaDecoder.java"


# static fields
.field public static final s:Ljava/util/regex/Pattern;


# instance fields
.field public final n:Z

.field public final o:Lf/h/a/c/e1/o/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lf/h/a/c/e1/o/c;",
            ">;"
        }
    .end annotation
.end field

.field public q:F

.field public r:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-string v0, "(?:(\\d+):)?(\\d+):(\\d+)[:.](\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lf/h/a/c/e1/o/a;->s:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;)V"
        }
    .end annotation

    const-string v0, "SsaDecoder"

    invoke-direct {p0, v0}, Lf/h/a/c/e1/c;-><init>(Ljava/lang/String;)V

    const v0, -0x800001

    iput v0, p0, Lf/h/a/c/e1/o/a;->q:F

    iput v0, p0, Lf/h/a/c/e1/o/a;->r:F

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/h/a/c/e1/o/a;->n:Z

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    sget v2, Lf/h/a/c/i1/a0;->a:I

    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    const-string v0, "Format:"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lf/g/j/k/a;->d(Z)V

    invoke-static {v2}, Lf/h/a/c/e1/o/b;->a(Ljava/lang/String;)Lf/h/a/c/e1/o/b;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p0, Lf/h/a/c/e1/o/a;->o:Lf/h/a/c/e1/o/b;

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    invoke-direct {v0, p1}, Lf/h/a/c/i1/r;-><init>([B)V

    invoke-virtual {p0, v0}, Lf/h/a/c/e1/o/a;->m(Lf/h/a/c/i1/r;)V

    goto :goto_0

    :cond_0
    iput-boolean v0, p0, Lf/h/a/c/e1/o/a;->n:Z

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/e1/o/a;->o:Lf/h/a/c/e1/o/b;

    :goto_0
    return-void
.end method

.method public static k(JLjava/util/List;Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;>;)I"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v3, v1, p0

    if-nez v3, :cond_0

    return v0

    :cond_0
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v3, v1, p0

    if-gez v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {p2, v0, p0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    new-instance p0, Ljava/util/ArrayList;

    if-nez v0, :cond_3

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_2

    :cond_3
    add-int/lit8 p1, v0, -0x1

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-direct {p0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_2
    invoke-interface {p3, v0, p0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return v0
.end method

.method public static l(I)F
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const p0, -0x800001

    return p0

    :cond_0
    const p0, 0x3f733333    # 0.95f

    return p0

    :cond_1
    const/high16 p0, 0x3f000000    # 0.5f

    return p0

    :cond_2
    const p0, 0x3d4ccccd    # 0.05f

    return p0
.end method

.method public static n(Ljava/lang/String;)J
    .locals 8

    sget-object v0, Lf/h/a/c/e1/o/a;->s:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lf/h/a/c/i1/a0;->a:I

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3c

    mul-long v0, v0, v2

    mul-long v0, v0, v2

    const-wide/32 v4, 0xf4240

    mul-long v0, v0, v4

    const/4 v6, 0x2

    invoke-virtual {p0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    mul-long v6, v6, v2

    mul-long v6, v6, v4

    add-long/2addr v6, v0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    mul-long v0, v0, v4

    add-long/2addr v0, v6

    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    mul-long v2, v2, v4

    add-long/2addr v2, v0

    return-wide v2
.end method


# virtual methods
.method public j([BIZ)Lf/h/a/c/e1/e;
    .locals 24

    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lf/h/a/c/i1/r;

    move-object/from16 v4, p1

    move/from16 v5, p2

    invoke-direct {v3, v4, v5}, Lf/h/a/c/i1/r;-><init>([BI)V

    iget-boolean v4, v0, Lf/h/a/c/e1/o/a;->n:Z

    if-nez v4, :cond_0

    invoke-virtual {v0, v3}, Lf/h/a/c/e1/o/a;->m(Lf/h/a/c/i1/r;)V

    :cond_0
    iget-boolean v4, v0, Lf/h/a/c/e1/o/a;->n:Z

    if-eqz v4, :cond_1

    iget-object v4, v0, Lf/h/a/c/e1/o/a;->o:Lf/h/a/c/e1/o/b;

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_11

    const-string v6, "Format:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v5}, Lf/h/a/c/e1/o/b;->a(Ljava/lang/String;)Lf/h/a/c/e1/o/b;

    move-result-object v4

    goto :goto_0

    :cond_2
    const-string v6, "Dialogue:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    const-string v7, "SsaDecoder"

    if-nez v4, :cond_3

    const-string v6, "Skipping dialogue line before complete format: "

    invoke-static {v6, v5, v7}, Lf/e/c/a/a;->V(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_3
    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v6}, Lf/g/j/k/a;->d(Z)V

    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    iget v8, v4, Lf/h/a/c/e1/o/b;->e:I

    const-string v9, ","

    invoke-virtual {v6, v9, v8}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v6

    array-length v8, v6

    iget v9, v4, Lf/h/a/c/e1/o/b;->e:I

    if-eq v8, v9, :cond_4

    const-string v6, "Skipping dialogue line with fewer columns than format: "

    invoke-static {v6, v5, v7}, Lf/e/c/a/a;->V(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_4
    iget v8, v4, Lf/h/a/c/e1/o/b;->a:I

    aget-object v8, v6, v8

    invoke-static {v8}, Lf/h/a/c/e1/o/a;->n(Ljava/lang/String;)J

    move-result-wide v8

    const-string v10, "Skipping invalid timing: "

    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v13, v8, v11

    if-nez v13, :cond_5

    invoke-static {v10, v5, v7}, Lf/e/c/a/a;->V(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_5
    iget v13, v4, Lf/h/a/c/e1/o/b;->b:I

    aget-object v13, v6, v13

    invoke-static {v13}, Lf/h/a/c/e1/o/a;->n(Ljava/lang/String;)J

    move-result-wide v13

    cmp-long v15, v13, v11

    if-nez v15, :cond_6

    invoke-static {v10, v5, v7}, Lf/e/c/a/a;->V(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_6
    iget-object v5, v0, Lf/h/a/c/e1/o/a;->p:Ljava/util/Map;

    const/4 v10, -0x1

    if-eqz v5, :cond_7

    iget v11, v4, Lf/h/a/c/e1/o/b;->c:I

    if-eq v11, v10, :cond_7

    aget-object v10, v6, v11

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/e1/o/c;

    goto :goto_1

    :cond_7
    const/4 v5, 0x0

    :goto_1
    iget v10, v4, Lf/h/a/c/e1/o/b;->d:I

    aget-object v6, v6, v10

    sget-object v10, Lf/h/a/c/e1/o/c$b;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, -0x1

    :goto_2
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v15

    move-object/from16 p3, v3

    const/4 v3, 0x1

    if-eqz v15, :cond_b

    invoke-virtual {v10, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-static {v3}, Lf/h/a/c/e1/o/c$b;->a(Ljava/lang/String;)Landroid/graphics/PointF;

    move-result-object v15
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v15, :cond_8

    move-object v11, v15

    :catch_0
    :cond_8
    :try_start_1
    sget-object v15, Lf/h/a/c/e1/o/c$b;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v15, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v15

    if-eqz v15, :cond_9

    const/4 v15, 0x1

    invoke-virtual {v3, v15}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lf/h/a/c/e1/o/c;->a(Ljava/lang/String;)I

    move-result v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :cond_9
    const/4 v3, -0x1

    :goto_3
    const/4 v15, -0x1

    if-eq v3, v15, :cond_a

    move v12, v3

    :catch_1
    :cond_a
    move-object/from16 v3, p3

    goto :goto_2

    :cond_b
    new-instance v3, Lf/h/a/c/e1/o/c$b;

    invoke-direct {v3, v12, v11}, Lf/h/a/c/e1/o/c$b;-><init>(ILandroid/graphics/PointF;)V

    sget-object v10, Lf/h/a/c/e1/o/c$b;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    const-string v10, ""

    invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v10, "\n"

    const-string v11, "\\\\N"

    invoke-virtual {v6, v11, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v11, "\\\\n"

    invoke-virtual {v6, v11, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    iget v6, v0, Lf/h/a/c/e1/o/a;->q:F

    iget v10, v0, Lf/h/a/c/e1/o/a;->r:F

    iget v11, v3, Lf/h/a/c/e1/o/c$b;->a:I

    const/4 v12, -0x1

    if-eq v11, v12, :cond_c

    goto :goto_4

    :cond_c
    if-eqz v5, :cond_d

    iget v11, v5, Lf/h/a/c/e1/o/c;->b:I

    goto :goto_4

    :cond_d
    const/4 v11, -0x1

    :goto_4
    const-string v5, "Unknown alignment: "

    packed-switch v11, :pswitch_data_0

    :pswitch_0
    invoke-static {v5, v11}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    goto :goto_5

    :pswitch_1
    const/4 v12, 0x2

    const/16 v22, 0x2

    goto :goto_6

    :pswitch_2
    const/4 v12, 0x1

    const/16 v22, 0x1

    goto :goto_6

    :pswitch_3
    const/4 v12, 0x0

    const/16 v22, 0x0

    goto :goto_6

    :goto_5
    invoke-static {v7, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :pswitch_4
    const/high16 v12, -0x80000000

    const/high16 v22, -0x80000000

    :goto_6
    packed-switch v11, :pswitch_data_1

    :pswitch_5
    invoke-static {v5, v11}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    goto :goto_7

    :pswitch_6
    const/4 v12, 0x0

    const/16 v20, 0x0

    goto :goto_8

    :pswitch_7
    const/4 v12, 0x1

    const/16 v20, 0x1

    goto :goto_8

    :pswitch_8
    const/4 v12, 0x2

    const/16 v20, 0x2

    goto :goto_8

    :goto_7
    invoke-static {v7, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :pswitch_9
    const/high16 v12, -0x80000000

    const/high16 v20, -0x80000000

    :goto_8
    iget-object v3, v3, Lf/h/a/c/e1/o/c$b;->b:Landroid/graphics/PointF;

    if-eqz v3, :cond_e

    const v12, -0x800001

    cmpl-float v15, v10, v12

    if-eqz v15, :cond_e

    cmpl-float v12, v6, v12

    if-eqz v12, :cond_e

    iget v12, v3, Landroid/graphics/PointF;->x:F

    div-float/2addr v12, v6

    iget v3, v3, Landroid/graphics/PointF;->y:F

    div-float/2addr v3, v10

    move/from16 v18, v3

    move/from16 v21, v12

    goto :goto_9

    :cond_e
    invoke-static/range {v22 .. v22}, Lf/h/a/c/e1/o/a;->l(I)F

    move-result v3

    invoke-static/range {v20 .. v20}, Lf/h/a/c/e1/o/a;->l(I)F

    move-result v6

    move/from16 v21, v3

    move/from16 v18, v6

    :goto_9
    new-instance v3, Lf/h/a/c/e1/b;

    packed-switch v11, :pswitch_data_2

    :pswitch_a
    invoke-static {v5, v11}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    goto :goto_a

    :pswitch_b
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_b

    :pswitch_c
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_b

    :pswitch_d
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_b

    :goto_a
    invoke-static {v7, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :pswitch_e
    const/4 v5, 0x0

    :goto_b
    move-object/from16 v17, v5

    const/16 v19, 0x0

    const v23, -0x800001

    move-object v15, v3

    invoke-direct/range {v15 .. v23}, Lf/h/a/c/e1/b;-><init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;FIIFIF)V

    invoke-static {v8, v9, v2, v1}, Lf/h/a/c/e1/o/a;->k(JLjava/util/List;Ljava/util/List;)I

    move-result v5

    invoke-static {v13, v14, v2, v1}, Lf/h/a/c/e1/o/a;->k(JLjava/util/List;Ljava/util/List;)I

    move-result v6

    :goto_c
    if-ge v5, v6, :cond_10

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    :cond_f
    :goto_d
    move-object/from16 p3, v3

    :cond_10
    move-object/from16 v3, p3

    goto/16 :goto_0

    :cond_11
    new-instance v3, Lf/h/a/c/e1/o/d;

    invoke-direct {v3, v1, v2}, Lf/h/a/c/e1/o/d;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v3

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_9
        :pswitch_5
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_e
        :pswitch_a
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_c
        :pswitch_b
    .end packed-switch
.end method

.method public final m(Lf/h/a/c/i1/r;)V
    .locals 14

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    const-string v1, "[Script Info]"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const/16 v2, 0x5b

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_5

    :goto_1
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->b()I

    move-result v1

    if-eq v1, v2, :cond_0

    :cond_1
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-eq v1, v3, :cond_2

    goto :goto_1

    :cond_2
    aget-object v1, v0, v5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf/h/a/c/i1/a0;->B(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    const-string v6, "playresx"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "playresy"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_1

    :cond_3
    :try_start_0
    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lf/h/a/c/e1/o/a;->r:F

    goto :goto_1

    :cond_4
    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lf/h/a/c/e1/o/a;->q:F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    goto :goto_1

    :cond_5
    const-string v1, "[V4+ Styles]"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const-string v6, "SsaDecoder"

    if-eqz v1, :cond_10

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, 0x0

    :cond_6
    move-object v7, v1

    :cond_7
    :goto_2
    invoke-virtual {p1}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_f

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result v9

    if-eqz v9, :cond_8

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->b()I

    move-result v9

    if-eq v9, v2, :cond_f

    :cond_8
    const-string v9, "Format:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    const-string v10, ","

    if-eqz v9, :cond_c

    const/4 v7, 0x7

    invoke-virtual {v8, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v10}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, -0x1

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v11, -0x1

    :goto_3
    array-length v12, v7

    if-ge v9, v12, :cond_b

    aget-object v12, v7, v9

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lf/h/a/c/i1/a0;->B(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    const-string v13, "name"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_a

    const-string v13, "alignment"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_9

    goto :goto_4

    :cond_9
    move v11, v9

    goto :goto_4

    :cond_a
    move v10, v9

    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_b
    if-eq v10, v8, :cond_6

    new-instance v8, Lf/h/a/c/e1/o/c$a;

    array-length v7, v7

    invoke-direct {v8, v10, v11, v7}, Lf/h/a/c/e1/o/c$a;-><init>(III)V

    move-object v7, v8

    goto :goto_2

    :cond_c
    const-string v9, "Style:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    if-nez v7, :cond_d

    const-string v9, "Skipping \'Style:\' line before \'Format:\' line: "

    invoke-static {v9, v8, v6}, Lf/e/c/a/a;->V(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_d
    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    invoke-static {v9}, Lf/g/j/k/a;->d(Z)V

    const/4 v9, 0x6

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v10}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v10, v9

    iget v11, v7, Lf/h/a/c/e1/o/c$a;->c:I

    const-string v12, "SsaStyle"

    if-eq v10, v11, :cond_e

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v5

    array-length v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v10, v4

    aput-object v8, v10, v3

    const-string v8, "Skipping malformed \'Style:\' line (expected %s values, found %s): \'%s\'"

    invoke-static {v8, v10}, Lf/h/a/c/i1/a0;->h(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v12, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_e
    :try_start_1
    new-instance v10, Lf/h/a/c/e1/o/c;

    iget v11, v7, Lf/h/a/c/e1/o/c$a;->a:I

    aget-object v11, v9, v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    iget v13, v7, Lf/h/a/c/e1/o/c$a;->b:I

    aget-object v9, v9, v13

    invoke-static {v9}, Lf/h/a/c/e1/o/c;->a(Ljava/lang/String;)I

    move-result v9

    invoke-direct {v10, v11, v9}, Lf/h/a/c/e1/o/c;-><init>(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_6

    :catch_1
    move-exception v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Skipping malformed \'Style:\' line: \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "\'"

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v12, v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    move-object v10, v1

    :goto_6
    if-eqz v10, :cond_7

    iget-object v8, v10, Lf/h/a/c/e1/o/c;->a:Ljava/lang/String;

    invoke-interface {v0, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_f
    iput-object v0, p0, Lf/h/a/c/e1/o/a;->p:Ljava/util/Map;

    goto/16 :goto_0

    :cond_10
    const-string v1, "[V4 Styles]"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v0, "[V4 Styles] are not supported"

    invoke-static {v6, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_11
    const-string v1, "[Events]"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_12
    return-void
.end method
