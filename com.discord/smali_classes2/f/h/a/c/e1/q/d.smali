.class public final Lf/h/a/c/e1/q/d;
.super Ljava/lang/Object;
.source "TtmlStyle.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:I

.field public e:Z

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:F

.field public l:Ljava/lang/String;

.field public m:Landroid/text/Layout$Alignment;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lf/h/a/c/e1/q/d;->f:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->g:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->h:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->i:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->j:I

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/e1/q/d;)Lf/h/a/c/e1/q/d;
    .locals 3

    if-eqz p1, :cond_8

    iget-boolean v0, p0, Lf/h/a/c/e1/q/d;->c:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lf/h/a/c/e1/q/d;->c:Z

    if-eqz v0, :cond_0

    iget v0, p1, Lf/h/a/c/e1/q/d;->b:I

    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iput v0, p0, Lf/h/a/c/e1/q/d;->b:I

    iput-boolean v1, p0, Lf/h/a/c/e1/q/d;->c:Z

    :cond_0
    iget v0, p0, Lf/h/a/c/e1/q/d;->h:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    iget v0, p1, Lf/h/a/c/e1/q/d;->h:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->h:I

    :cond_1
    iget v0, p0, Lf/h/a/c/e1/q/d;->i:I

    if-ne v0, v2, :cond_2

    iget v0, p1, Lf/h/a/c/e1/q/d;->i:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->i:I

    :cond_2
    iget-object v0, p0, Lf/h/a/c/e1/q/d;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, Lf/h/a/c/e1/q/d;->a:Ljava/lang/String;

    iput-object v0, p0, Lf/h/a/c/e1/q/d;->a:Ljava/lang/String;

    :cond_3
    iget v0, p0, Lf/h/a/c/e1/q/d;->f:I

    if-ne v0, v2, :cond_4

    iget v0, p1, Lf/h/a/c/e1/q/d;->f:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->f:I

    :cond_4
    iget v0, p0, Lf/h/a/c/e1/q/d;->g:I

    if-ne v0, v2, :cond_5

    iget v0, p1, Lf/h/a/c/e1/q/d;->g:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->g:I

    :cond_5
    iget-object v0, p0, Lf/h/a/c/e1/q/d;->m:Landroid/text/Layout$Alignment;

    if-nez v0, :cond_6

    iget-object v0, p1, Lf/h/a/c/e1/q/d;->m:Landroid/text/Layout$Alignment;

    iput-object v0, p0, Lf/h/a/c/e1/q/d;->m:Landroid/text/Layout$Alignment;

    :cond_6
    iget v0, p0, Lf/h/a/c/e1/q/d;->j:I

    if-ne v0, v2, :cond_7

    iget v0, p1, Lf/h/a/c/e1/q/d;->j:I

    iput v0, p0, Lf/h/a/c/e1/q/d;->j:I

    iget v0, p1, Lf/h/a/c/e1/q/d;->k:F

    iput v0, p0, Lf/h/a/c/e1/q/d;->k:F

    :cond_7
    iget-boolean v0, p0, Lf/h/a/c/e1/q/d;->e:Z

    if-nez v0, :cond_8

    iget-boolean v0, p1, Lf/h/a/c/e1/q/d;->e:Z

    if-eqz v0, :cond_8

    iget p1, p1, Lf/h/a/c/e1/q/d;->d:I

    iput p1, p0, Lf/h/a/c/e1/q/d;->d:I

    iput-boolean v1, p0, Lf/h/a/c/e1/q/d;->e:Z

    :cond_8
    return-object p0
.end method

.method public b()I
    .locals 4

    iget v0, p0, Lf/h/a/c/e1/q/d;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v2, p0, Lf/h/a/c/e1/q/d;->i:I

    if-ne v2, v1, :cond_0

    return v1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lf/h/a/c/e1/q/d;->i:I

    if-ne v3, v2, :cond_2

    const/4 v1, 0x2

    :cond_2
    or-int/2addr v0, v1

    return v0
.end method
