.class public final Lf/h/a/c/e1/k;
.super Lf/h/a/c/t;
.source "TextRenderer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public A:I

.field public final o:Landroid/os/Handler;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final p:Lf/h/a/c/e1/j;

.field public final q:Lf/h/a/c/e1/g;

.field public final r:Lf/h/a/c/d0;

.field public s:Z

.field public t:Z

.field public u:I

.field public v:Lcom/google/android/exoplayer2/Format;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public w:Lf/h/a/c/e1/f;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public x:Lf/h/a/c/e1/h;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public y:Lf/h/a/c/e1/i;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public z:Lf/h/a/c/e1/i;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/a/c/e1/j;Landroid/os/Looper;)V
    .locals 2
    .param p2    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lf/h/a/c/e1/g;->a:Lf/h/a/c/e1/g;

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lf/h/a/c/t;-><init>(I)V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/c/e1/k;->p:Lf/h/a/c/e1/j;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    sget p1, Lf/h/a/c/i1/a0;->a:I

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1, p2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    :goto_0
    iput-object p1, p0, Lf/h/a/c/e1/k;->o:Landroid/os/Handler;

    iput-object v0, p0, Lf/h/a/c/e1/k;->q:Lf/h/a/c/e1/g;

    new-instance p1, Lf/h/a/c/d0;

    invoke-direct {p1}, Lf/h/a/c/d0;-><init>()V

    iput-object p1, p0, Lf/h/a/c/e1/k;->r:Lf/h/a/c/d0;

    return-void
.end method


# virtual methods
.method public A(JZ)V
    .locals 0

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->J()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/a/c/e1/k;->s:Z

    iput-boolean p1, p0, Lf/h/a/c/e1/k;->t:Z

    iget p1, p0, Lf/h/a/c/e1/k;->u:I

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->M()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lf/h/a/c/e1/k;->L()V

    iget-object p1, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    invoke-interface {p1}, Lf/h/a/c/y0/c;->flush()V

    :goto_0
    return-void
.end method

.method public E([Lcom/google/android/exoplayer2/Format;J)V
    .locals 0

    const/4 p2, 0x0

    aget-object p1, p1, p2

    iput-object p1, p0, Lf/h/a/c/e1/k;->v:Lcom/google/android/exoplayer2/Format;

    iget-object p2, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    iput p1, p0, Lf/h/a/c/e1/k;->u:I

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lf/h/a/c/e1/k;->q:Lf/h/a/c/e1/g;

    check-cast p2, Lf/h/a/c/e1/g$a;

    invoke-virtual {p2, p1}, Lf/h/a/c/e1/g$a;->a(Lcom/google/android/exoplayer2/Format;)Lf/h/a/c/e1/f;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    :goto_0
    return-void
.end method

.method public G(Lcom/google/android/exoplayer2/Format;)I
    .locals 4

    iget-object v0, p0, Lf/h/a/c/e1/k;->q:Lf/h/a/c/e1/g;

    check-cast v0, Lf/h/a/c/e1/g$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    const-string v1, "text/vtt"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    const-string v1, "text/x-ssa"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/ttml+xml"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/x-mp4-vtt"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/x-subrip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/x-quicktime-tx3g"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/cea-608"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/x-mp4-cea-608"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/cea-708"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/dvbsubs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "application/pgs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    invoke-static {v0, p1}, Lf/h/a/c/t;->H(Lf/h/a/c/z0/b;Lcom/google/android/exoplayer2/drm/DrmInitData;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x4

    goto :goto_2

    :cond_2
    const/4 p1, 0x2

    :goto_2
    or-int/2addr p1, v2

    or-int/2addr p1, v2

    return p1

    :cond_3
    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->l:Ljava/lang/String;

    invoke-static {p1}, Lf/h/a/c/i1/o;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "text"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    return v3

    :cond_4
    return v2
.end method

.method public final J()V
    .locals 3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/c/e1/k;->o:Landroid/os/Handler;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/h/a/c/e1/k;->p:Lf/h/a/c/e1/j;

    invoke-interface {v1, v0}, Lf/h/a/c/e1/j;->m(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method public final K()J
    .locals 5

    iget v0, p0, Lf/h/a/c/e1/k;->A:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    iget-object v1, v1, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1}, Lf/h/a/c/e1/e;->i()I

    move-result v1

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    iget v1, p0, Lf/h/a/c/e1/k;->A:I

    iget-object v2, v0, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v2, v1}, Lf/h/a/c/e1/e;->g(I)J

    move-result-wide v1

    iget-wide v3, v0, Lf/h/a/c/e1/i;->e:J

    add-long/2addr v1, v3

    goto :goto_1

    :cond_1
    :goto_0
    const-wide v1, 0x7fffffffffffffffL

    :goto_1
    return-wide v1
.end method

.method public final L()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    const/4 v1, -0x1

    iput v1, p0, Lf/h/a/c/e1/k;->A:I

    iget-object v1, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lf/h/a/c/y0/f;->release()V

    iput-object v0, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    :cond_0
    iget-object v1, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lf/h/a/c/y0/f;->release()V

    iput-object v0, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;

    :cond_1
    return-void
.end method

.method public final M()V
    .locals 2

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->L()V

    iget-object v0, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    invoke-interface {v0}, Lf/h/a/c/y0/c;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/e1/k;->u:I

    iget-object v0, p0, Lf/h/a/c/e1/k;->q:Lf/h/a/c/e1/g;

    iget-object v1, p0, Lf/h/a/c/e1/k;->v:Lcom/google/android/exoplayer2/Format;

    check-cast v0, Lf/h/a/c/e1/g$a;

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/g$a;->a(Lcom/google/android/exoplayer2/Format;)Lf/h/a/c/e1/f;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/e1/k;->t:Z

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    iget-object v0, p0, Lf/h/a/c/e1/k;->p:Lf/h/a/c/e1/j;

    invoke-interface {v0, p1}, Lf/h/a/c/e1/j;->m(Ljava/util/List;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public n(JJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    iget-boolean p3, p0, Lf/h/a/c/e1/k;->t:Z

    if-eqz p3, :cond_0

    return-void

    :cond_0
    iget-object p3, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;

    if-nez p3, :cond_1

    iget-object p3, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    invoke-interface {p3, p1, p2}, Lf/h/a/c/e1/f;->a(J)V

    :try_start_0
    iget-object p3, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    invoke-interface {p3}, Lf/h/a/c/y0/c;->b()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/h/a/c/e1/i;

    iput-object p3, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;
    :try_end_0
    .catch Lcom/google/android/exoplayer2/text/SubtitleDecoderException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/h/a/c/e1/k;->v:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/t;->w(Ljava/lang/Exception;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    iget p3, p0, Lf/h/a/c/t;->h:I

    const/4 p4, 0x2

    if-eq p3, p4, :cond_2

    return-void

    :cond_2
    iget-object p3, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_3

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->K()J

    move-result-wide v2

    const/4 p3, 0x0

    :goto_1
    cmp-long v4, v2, p1

    if-gtz v4, :cond_4

    iget p3, p0, Lf/h/a/c/e1/k;->A:I

    add-int/2addr p3, v0

    iput p3, p0, Lf/h/a/c/e1/k;->A:I

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->K()J

    move-result-wide v2

    const/4 p3, 0x1

    goto :goto_1

    :cond_3
    const/4 p3, 0x0

    :cond_4
    iget-object v2, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;

    const/4 v3, 0x0

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result v2

    if-eqz v2, :cond_6

    if-nez p3, :cond_8

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->K()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v2, v4, v6

    if-nez v2, :cond_8

    iget v2, p0, Lf/h/a/c/e1/k;->u:I

    if-ne v2, p4, :cond_5

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->M()V

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lf/h/a/c/e1/k;->L()V

    iput-boolean v0, p0, Lf/h/a/c/e1/k;->t:Z

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;

    iget-wide v4, v2, Lf/h/a/c/y0/f;->timeUs:J

    cmp-long v2, v4, p1

    if-gtz v2, :cond_8

    iget-object p3, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    if-eqz p3, :cond_7

    invoke-virtual {p3}, Lf/h/a/c/y0/f;->release()V

    :cond_7
    iget-object p3, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;

    iput-object p3, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    iput-object v3, p0, Lf/h/a/c/e1/k;->z:Lf/h/a/c/e1/i;

    iget-object v2, p3, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v4, p3, Lf/h/a/c/e1/i;->e:J

    sub-long v4, p1, v4

    invoke-interface {v2, v4, v5}, Lf/h/a/c/e1/e;->f(J)I

    move-result p3

    iput p3, p0, Lf/h/a/c/e1/k;->A:I

    const/4 p3, 0x1

    :cond_8
    :goto_2
    if-eqz p3, :cond_a

    iget-object p3, p0, Lf/h/a/c/e1/k;->y:Lf/h/a/c/e1/i;

    iget-object v2, p3, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v4, p3, Lf/h/a/c/e1/i;->e:J

    sub-long/2addr p1, v4

    invoke-interface {v2, p1, p2}, Lf/h/a/c/e1/e;->h(J)Ljava/util/List;

    move-result-object p1

    iget-object p2, p0, Lf/h/a/c/e1/k;->o:Landroid/os/Handler;

    if-eqz p2, :cond_9

    invoke-virtual {p2, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_3

    :cond_9
    iget-object p2, p0, Lf/h/a/c/e1/k;->p:Lf/h/a/c/e1/j;

    invoke-interface {p2, p1}, Lf/h/a/c/e1/j;->m(Ljava/util/List;)V

    :cond_a
    :goto_3
    iget p1, p0, Lf/h/a/c/e1/k;->u:I

    if-ne p1, p4, :cond_b

    return-void

    :cond_b
    :goto_4
    :try_start_1
    iget-boolean p1, p0, Lf/h/a/c/e1/k;->s:Z

    if-nez p1, :cond_10

    iget-object p1, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    if-nez p1, :cond_c

    iget-object p1, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    invoke-interface {p1}, Lf/h/a/c/y0/c;->c()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/a/c/e1/h;

    iput-object p1, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    if-nez p1, :cond_c

    return-void

    :cond_c
    iget p1, p0, Lf/h/a/c/e1/k;->u:I

    if-ne p1, v0, :cond_d

    iget-object p1, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Lf/h/a/c/y0/a;->setFlags(I)V

    iget-object p1, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    iget-object p2, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    invoke-interface {p1, p2}, Lf/h/a/c/y0/c;->d(Ljava/lang/Object;)V

    iput-object v3, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    iput p4, p0, Lf/h/a/c/e1/k;->u:I

    return-void

    :cond_d
    iget-object p1, p0, Lf/h/a/c/e1/k;->r:Lf/h/a/c/d0;

    iget-object p2, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    invoke-virtual {p0, p1, p2, v1}, Lf/h/a/c/t;->F(Lf/h/a/c/d0;Lf/h/a/c/y0/e;Z)I

    move-result p1

    const/4 p2, -0x4

    if-ne p1, p2, :cond_f

    iget-object p1, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    invoke-virtual {p1}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result p1

    if-eqz p1, :cond_e

    iput-boolean v0, p0, Lf/h/a/c/e1/k;->s:Z

    goto :goto_5

    :cond_e
    iget-object p1, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    iget-object p2, p0, Lf/h/a/c/e1/k;->r:Lf/h/a/c/d0;

    iget-object p2, p2, Lf/h/a/c/d0;->c:Lcom/google/android/exoplayer2/Format;

    iget-wide p2, p2, Lcom/google/android/exoplayer2/Format;->p:J

    iput-wide p2, p1, Lf/h/a/c/e1/h;->i:J

    invoke-virtual {p1}, Lf/h/a/c/y0/e;->l()V

    :goto_5
    iget-object p1, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    iget-object p2, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;

    invoke-interface {p1, p2}, Lf/h/a/c/y0/c;->d(Ljava/lang/Object;)V

    iput-object v3, p0, Lf/h/a/c/e1/k;->x:Lf/h/a/c/e1/h;
    :try_end_1
    .catch Lcom/google/android/exoplayer2/text/SubtitleDecoderException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :cond_f
    const/4 p2, -0x3

    if-ne p1, p2, :cond_b

    :cond_10
    return-void

    :catch_1
    move-exception p1

    iget-object p2, p0, Lf/h/a/c/e1/k;->v:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0, p1, p2}, Lf/h/a/c/t;->w(Ljava/lang/Exception;Lcom/google/android/exoplayer2/Format;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object p1

    throw p1
.end method

.method public y()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/e1/k;->v:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->J()V

    invoke-virtual {p0}, Lf/h/a/c/e1/k;->L()V

    iget-object v1, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    invoke-interface {v1}, Lf/h/a/c/y0/c;->release()V

    iput-object v0, p0, Lf/h/a/c/e1/k;->w:Lf/h/a/c/e1/f;

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/e1/k;->u:I

    return-void
.end method
