.class public final Lf/h/a/c/e1/m/a;
.super Lf/h/a/c/e1/c;
.source "DvbDecoder.java"


# instance fields
.field public final n:Lf/h/a/c/e1/m/b;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;)V"
        }
    .end annotation

    const-string v0, "DvbDecoder"

    invoke-direct {p0, v0}, Lf/h/a/c/e1/c;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    array-length v1, p1

    const/4 v1, 0x0

    add-int/lit8 v1, v1, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    aget-byte p1, p1, v1

    and-int/lit16 p1, p1, 0xff

    or-int/2addr p1, v2

    new-instance v1, Lf/h/a/c/e1/m/b;

    invoke-direct {v1, v0, p1}, Lf/h/a/c/e1/m/b;-><init>(II)V

    iput-object v1, p0, Lf/h/a/c/e1/m/a;->n:Lf/h/a/c/e1/m/b;

    return-void
.end method


# virtual methods
.method public j([BIZ)Lf/h/a/c/e1/e;
    .locals 34

    move-object/from16 v0, p0

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    iget-object v2, v0, Lf/h/a/c/e1/m/a;->n:Lf/h/a/c/e1/m/b;

    iget-object v2, v2, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    iget-object v3, v2, Lf/h/a/c/e1/m/b$h;->c:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    iget-object v3, v2, Lf/h/a/c/e1/m/b$h;->d:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    iget-object v3, v2, Lf/h/a/c/e1/m/b$h;->e:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    iget-object v3, v2, Lf/h/a/c/e1/m/b$h;->f:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    iget-object v3, v2, Lf/h/a/c/e1/m/b$h;->g:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    iput-object v1, v2, Lf/h/a/c/e1/m/b$h;->h:Lf/h/a/c/e1/m/b$b;

    iput-object v1, v2, Lf/h/a/c/e1/m/b$h;->i:Lf/h/a/c/e1/m/b$d;

    :cond_0
    new-instance v2, Lf/h/a/c/e1/m/c;

    iget-object v3, v0, Lf/h/a/c/e1/m/a;->n:Lf/h/a/c/e1/m/b;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lf/h/a/c/i1/q;

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct {v4, v5, v6}, Lf/h/a/c/i1/q;-><init>([BI)V

    :goto_0
    invoke-virtual {v4}, Lf/h/a/c/i1/q;->b()I

    move-result v5

    const/16 v6, 0x30

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-lt v5, v6, :cond_f

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    const/16 v11, 0xf

    if-ne v6, v11, :cond_f

    iget-object v6, v3, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v11

    const/16 v12, 0x10

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v13

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v14

    iget v15, v4, Lf/h/a/c/i1/q;->c:I

    if-nez v15, :cond_1

    const/4 v15, 0x1

    goto :goto_1

    :cond_1
    const/4 v15, 0x0

    :goto_1
    invoke-static {v15}, Lf/g/j/k/a;->s(Z)V

    iget v15, v4, Lf/h/a/c/i1/q;->b:I

    add-int/2addr v15, v14

    mul-int/lit8 v1, v14, 0x8

    invoke-virtual {v4}, Lf/h/a/c/i1/q;->b()I

    move-result v10

    if-le v1, v10, :cond_2

    const-string v1, "DvbParser"

    const-string v5, "Data field length exceeds limit"

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Lf/h/a/c/i1/q;->b()I

    move-result v1

    invoke-virtual {v4, v1}, Lf/h/a/c/i1/q;->l(I)V

    :goto_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    packed-switch v11, :pswitch_data_0

    goto/16 :goto_9

    :pswitch_0
    iget v5, v6, Lf/h/a/c/e1/m/b$h;->a:I

    if-ne v13, v5, :cond_c

    invoke-virtual {v4, v1}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v4}, Lf/h/a/c/i1/q;->e()Z

    move-result v1

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v17

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v18

    if-eqz v1, :cond_3

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v7

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v8

    move/from16 v19, v1

    move/from16 v20, v5

    move/from16 v21, v7

    move/from16 v22, v8

    goto :goto_3

    :cond_3
    move/from16 v20, v17

    move/from16 v22, v18

    const/16 v19, 0x0

    const/16 v21, 0x0

    :goto_3
    new-instance v1, Lf/h/a/c/e1/m/b$b;

    move-object/from16 v16, v1

    invoke-direct/range {v16 .. v22}, Lf/h/a/c/e1/m/b$b;-><init>(IIIIII)V

    iput-object v1, v6, Lf/h/a/c/e1/m/b$h;->h:Lf/h/a/c/e1/m/b$b;

    goto/16 :goto_9

    :pswitch_1
    iget v1, v6, Lf/h/a/c/e1/m/b$h;->a:I

    if-ne v13, v1, :cond_4

    invoke-static {v4}, Lf/h/a/c/e1/m/b;->g(Lf/h/a/c/i1/q;)Lf/h/a/c/e1/m/b$c;

    move-result-object v1

    iget-object v5, v6, Lf/h/a/c/e1/m/b$h;->e:Landroid/util/SparseArray;

    iget v6, v1, Lf/h/a/c/e1/m/b$c;->a:I

    invoke-virtual {v5, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_9

    :cond_4
    iget v1, v6, Lf/h/a/c/e1/m/b$h;->b:I

    if-ne v13, v1, :cond_c

    invoke-static {v4}, Lf/h/a/c/e1/m/b;->g(Lf/h/a/c/i1/q;)Lf/h/a/c/e1/m/b$c;

    move-result-object v1

    iget-object v5, v6, Lf/h/a/c/e1/m/b$h;->g:Landroid/util/SparseArray;

    iget v6, v1, Lf/h/a/c/e1/m/b$c;->a:I

    invoke-virtual {v5, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_9

    :pswitch_2
    iget v1, v6, Lf/h/a/c/e1/m/b$h;->a:I

    if-ne v13, v1, :cond_5

    invoke-static {v4, v14}, Lf/h/a/c/e1/m/b;->f(Lf/h/a/c/i1/q;I)Lf/h/a/c/e1/m/b$a;

    move-result-object v1

    iget-object v5, v6, Lf/h/a/c/e1/m/b$h;->d:Landroid/util/SparseArray;

    iget v6, v1, Lf/h/a/c/e1/m/b$a;->a:I

    invoke-virtual {v5, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_9

    :cond_5
    iget v1, v6, Lf/h/a/c/e1/m/b$h;->b:I

    if-ne v13, v1, :cond_c

    invoke-static {v4, v14}, Lf/h/a/c/e1/m/b;->f(Lf/h/a/c/i1/q;I)Lf/h/a/c/e1/m/b$a;

    move-result-object v1

    iget-object v5, v6, Lf/h/a/c/e1/m/b$h;->f:Landroid/util/SparseArray;

    iget v6, v1, Lf/h/a/c/e1/m/b$a;->a:I

    invoke-virtual {v5, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_9

    :pswitch_3
    iget-object v10, v6, Lf/h/a/c/e1/m/b$h;->i:Lf/h/a/c/e1/m/b$d;

    iget v11, v6, Lf/h/a/c/e1/m/b$h;->a:I

    if-ne v13, v11, :cond_c

    if-eqz v10, :cond_c

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v11

    invoke-virtual {v4, v1}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v4}, Lf/h/a/c/i1/q;->e()Z

    move-result v18

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v19

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v20

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v21

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v22

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v23

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v24

    invoke-virtual {v4, v1}, Lf/h/a/c/i1/q;->f(I)I

    move-result v25

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v26

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/q;->l(I)V

    add-int/lit8 v14, v14, -0xa

    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    :goto_4
    if-lez v14, :cond_8

    invoke-virtual {v4, v12}, Lf/h/a/c/i1/q;->f(I)I

    move-result v13

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v12

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v29

    const/16 v5, 0xc

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v30

    invoke-virtual {v4, v1}, Lf/h/a/c/i1/q;->l(I)V

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v31

    add-int/lit8 v14, v14, -0x6

    if-eq v12, v9, :cond_7

    if-ne v12, v8, :cond_6

    goto :goto_5

    :cond_6
    const/16 v32, 0x0

    const/16 v33, 0x0

    goto :goto_6

    :cond_7
    :goto_5
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v16

    invoke-virtual {v4, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v17

    add-int/lit8 v14, v14, -0x2

    move/from16 v32, v16

    move/from16 v33, v17

    :goto_6
    new-instance v5, Lf/h/a/c/e1/m/b$g;

    move-object/from16 v27, v5

    move/from16 v28, v12

    invoke-direct/range {v27 .. v33}, Lf/h/a/c/e1/m/b$g;-><init>(IIIIII)V

    invoke-virtual {v7, v13, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v5, 0x8

    const/16 v12, 0x10

    goto :goto_4

    :cond_8
    new-instance v1, Lf/h/a/c/e1/m/b$f;

    move-object/from16 v16, v1

    move/from16 v17, v11

    move-object/from16 v27, v7

    invoke-direct/range {v16 .. v27}, Lf/h/a/c/e1/m/b$f;-><init>(IZIIIIIIIILandroid/util/SparseArray;)V

    iget v5, v10, Lf/h/a/c/e1/m/b$d;->b:I

    if-nez v5, :cond_9

    iget-object v5, v6, Lf/h/a/c/e1/m/b$h;->c:Landroid/util/SparseArray;

    invoke-virtual {v5, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/e1/m/b$f;

    if-eqz v5, :cond_9

    iget-object v5, v5, Lf/h/a/c/e1/m/b$f;->j:Landroid/util/SparseArray;

    const/4 v7, 0x0

    :goto_7
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v8

    if-ge v7, v8, :cond_9

    iget-object v8, v1, Lf/h/a/c/e1/m/b$f;->j:Landroid/util/SparseArray;

    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v10

    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v8, v10, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    :cond_9
    iget-object v5, v6, Lf/h/a/c/e1/m/b$h;->c:Landroid/util/SparseArray;

    iget v6, v1, Lf/h/a/c/e1/m/b$f;->a:I

    invoke-virtual {v5, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_9

    :pswitch_4
    iget v5, v6, Lf/h/a/c/e1/m/b$h;->a:I

    if-ne v13, v5, :cond_c

    iget-object v5, v6, Lf/h/a/c/e1/m/b$h;->i:Lf/h/a/c/e1/m/b$d;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v10

    invoke-virtual {v4, v1}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/q;->f(I)I

    move-result v11

    invoke-virtual {v4, v8}, Lf/h/a/c/i1/q;->l(I)V

    add-int/lit8 v14, v14, -0x2

    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    :goto_8
    if-lez v14, :cond_a

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v12

    invoke-virtual {v4, v7}, Lf/h/a/c/i1/q;->l(I)V

    const/16 v13, 0x10

    invoke-virtual {v4, v13}, Lf/h/a/c/i1/q;->f(I)I

    move-result v7

    invoke-virtual {v4, v13}, Lf/h/a/c/i1/q;->f(I)I

    move-result v9

    add-int/lit8 v14, v14, -0x6

    new-instance v13, Lf/h/a/c/e1/m/b$e;

    invoke-direct {v13, v7, v9}, Lf/h/a/c/e1/m/b$e;-><init>(II)V

    invoke-virtual {v8, v12, v13}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/16 v7, 0x8

    const/4 v9, 0x1

    goto :goto_8

    :cond_a
    new-instance v7, Lf/h/a/c/e1/m/b$d;

    invoke-direct {v7, v10, v1, v11, v8}, Lf/h/a/c/e1/m/b$d;-><init>(IIILandroid/util/SparseArray;)V

    if-eqz v11, :cond_b

    iput-object v7, v6, Lf/h/a/c/e1/m/b$h;->i:Lf/h/a/c/e1/m/b$d;

    iget-object v1, v6, Lf/h/a/c/e1/m/b$h;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    iget-object v1, v6, Lf/h/a/c/e1/m/b$h;->d:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    iget-object v1, v6, Lf/h/a/c/e1/m/b$h;->e:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    goto :goto_9

    :cond_b
    if-eqz v5, :cond_c

    iget v5, v5, Lf/h/a/c/e1/m/b$d;->a:I

    if-eq v5, v1, :cond_c

    iput-object v7, v6, Lf/h/a/c/e1/m/b$h;->i:Lf/h/a/c/e1/m/b$d;

    :cond_c
    :goto_9
    iget v1, v4, Lf/h/a/c/i1/q;->c:I

    if-nez v1, :cond_d

    const/4 v1, 0x1

    goto :goto_a

    :cond_d
    const/4 v1, 0x0

    :goto_a
    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iget v1, v4, Lf/h/a/c/i1/q;->b:I

    sub-int/2addr v15, v1

    iget v1, v4, Lf/h/a/c/i1/q;->c:I

    if-nez v1, :cond_e

    const/4 v9, 0x1

    goto :goto_b

    :cond_e
    const/4 v9, 0x0

    :goto_b
    invoke-static {v9}, Lf/g/j/k/a;->s(Z)V

    iget v1, v4, Lf/h/a/c/i1/q;->b:I

    add-int/2addr v1, v15

    iput v1, v4, Lf/h/a/c/i1/q;->b:I

    invoke-virtual {v4}, Lf/h/a/c/i1/q;->a()V

    goto/16 :goto_2

    :cond_f
    iget-object v1, v3, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    iget-object v4, v1, Lf/h/a/c/e1/m/b$h;->i:Lf/h/a/c/e1/m/b$d;

    if-nez v4, :cond_10

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object/from16 v26, v2

    goto/16 :goto_15

    :cond_10
    iget-object v1, v1, Lf/h/a/c/e1/m/b$h;->h:Lf/h/a/c/e1/m/b$b;

    if-eqz v1, :cond_11

    goto :goto_c

    :cond_11
    iget-object v1, v3, Lf/h/a/c/e1/m/b;->d:Lf/h/a/c/e1/m/b$b;

    :goto_c
    iget-object v5, v3, Lf/h/a/c/e1/m/b;->g:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_12

    iget v6, v1, Lf/h/a/c/e1/m/b$b;->a:I

    const/4 v9, 0x1

    add-int/2addr v6, v9

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v6, v5, :cond_13

    iget v5, v1, Lf/h/a/c/e1/m/b$b;->b:I

    add-int/2addr v5, v9

    iget-object v6, v3, Lf/h/a/c/e1/m/b;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-eq v5, v6, :cond_14

    goto :goto_d

    :cond_12
    const/4 v9, 0x1

    :cond_13
    :goto_d
    iget v5, v1, Lf/h/a/c/e1/m/b$b;->a:I

    add-int/2addr v5, v9

    iget v6, v1, Lf/h/a/c/e1/m/b$b;->b:I

    add-int/2addr v6, v9

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, v3, Lf/h/a/c/e1/m/b;->g:Landroid/graphics/Bitmap;

    iget-object v6, v3, Lf/h/a/c/e1/m/b;->c:Landroid/graphics/Canvas;

    invoke-virtual {v6, v5}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_14
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, v4, Lf/h/a/c/e1/m/b$d;->c:Landroid/util/SparseArray;

    const/4 v6, 0x0

    :goto_e
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-ge v6, v9, :cond_1f

    iget-object v9, v3, Lf/h/a/c/e1/m/b;->c:Landroid/graphics/Canvas;

    invoke-virtual {v9}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/h/a/c/e1/m/b$e;

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v10

    iget-object v11, v3, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    iget-object v11, v11, Lf/h/a/c/e1/m/b$h;->c:Landroid/util/SparseArray;

    invoke-virtual {v11, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lf/h/a/c/e1/m/b$f;

    iget v11, v9, Lf/h/a/c/e1/m/b$e;->a:I

    iget v12, v1, Lf/h/a/c/e1/m/b$b;->c:I

    add-int/2addr v11, v12

    iget v9, v9, Lf/h/a/c/e1/m/b$e;->b:I

    iget v12, v1, Lf/h/a/c/e1/m/b$b;->e:I

    add-int/2addr v9, v12

    iget v12, v10, Lf/h/a/c/e1/m/b$f;->c:I

    add-int/2addr v12, v11

    iget v13, v1, Lf/h/a/c/e1/m/b$b;->d:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v12

    iget v13, v10, Lf/h/a/c/e1/m/b$f;->d:I

    add-int/2addr v13, v9

    iget v14, v1, Lf/h/a/c/e1/m/b$b;->f:I

    invoke-static {v13, v14}, Ljava/lang/Math;->min(II)I

    move-result v13

    iget-object v14, v3, Lf/h/a/c/e1/m/b;->c:Landroid/graphics/Canvas;

    invoke-virtual {v14, v11, v9, v12, v13}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    iget-object v12, v3, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    iget-object v12, v12, Lf/h/a/c/e1/m/b$h;->d:Landroid/util/SparseArray;

    iget v13, v10, Lf/h/a/c/e1/m/b$f;->f:I

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lf/h/a/c/e1/m/b$a;

    if-nez v12, :cond_15

    iget-object v12, v3, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    iget-object v12, v12, Lf/h/a/c/e1/m/b$h;->f:Landroid/util/SparseArray;

    iget v13, v10, Lf/h/a/c/e1/m/b$f;->f:I

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lf/h/a/c/e1/m/b$a;

    if-nez v12, :cond_15

    iget-object v12, v3, Lf/h/a/c/e1/m/b;->e:Lf/h/a/c/e1/m/b$a;

    :cond_15
    iget-object v13, v10, Lf/h/a/c/e1/m/b$f;->j:Landroid/util/SparseArray;

    const/4 v14, 0x0

    :goto_f
    invoke-virtual {v13}, Landroid/util/SparseArray;->size()I

    move-result v15

    if-ge v14, v15, :cond_1b

    invoke-virtual {v13, v14}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v15

    invoke-virtual {v13, v14}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v8, v16

    check-cast v8, Lf/h/a/c/e1/m/b$g;

    iget-object v7, v3, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    iget-object v7, v7, Lf/h/a/c/e1/m/b$h;->e:Landroid/util/SparseArray;

    invoke-virtual {v7, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/a/c/e1/m/b$c;

    if-nez v7, :cond_16

    iget-object v7, v3, Lf/h/a/c/e1/m/b;->f:Lf/h/a/c/e1/m/b$h;

    iget-object v7, v7, Lf/h/a/c/e1/m/b$h;->g:Landroid/util/SparseArray;

    invoke-virtual {v7, v15}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/h/a/c/e1/m/b$c;

    :cond_16
    if-eqz v7, :cond_1a

    iget-boolean v15, v7, Lf/h/a/c/e1/m/b$c;->b:Z

    if-eqz v15, :cond_17

    const/4 v15, 0x0

    goto :goto_10

    :cond_17
    iget-object v15, v3, Lf/h/a/c/e1/m/b;->a:Landroid/graphics/Paint;

    :goto_10
    iget v0, v10, Lf/h/a/c/e1/m/b$f;->e:I

    move-object/from16 v24, v4

    iget v4, v8, Lf/h/a/c/e1/m/b$g;->a:I

    add-int/2addr v4, v11

    iget v8, v8, Lf/h/a/c/e1/m/b$g;->b:I

    add-int/2addr v8, v9

    move-object/from16 v25, v13

    iget-object v13, v3, Lf/h/a/c/e1/m/b;->c:Landroid/graphics/Canvas;

    move-object/from16 v26, v2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_18

    iget-object v2, v12, Lf/h/a/c/e1/m/b$a;->d:[I

    goto :goto_11

    :cond_18
    const/4 v2, 0x2

    if-ne v0, v2, :cond_19

    iget-object v2, v12, Lf/h/a/c/e1/m/b$a;->c:[I

    goto :goto_11

    :cond_19
    iget-object v2, v12, Lf/h/a/c/e1/m/b$a;->b:[I

    :goto_11
    move/from16 v27, v6

    iget-object v6, v7, Lf/h/a/c/e1/m/b$c;->c:[B

    move-object/from16 v17, v6

    move-object/from16 v18, v2

    move/from16 v19, v0

    move/from16 v20, v4

    move/from16 v21, v8

    move-object/from16 v22, v15

    move-object/from16 v23, v13

    invoke-static/range {v17 .. v23}, Lf/h/a/c/e1/m/b;->e([B[IIIILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v6, v7, Lf/h/a/c/e1/m/b$c;->d:[B

    const/4 v7, 0x1

    add-int/lit8 v21, v8, 0x1

    move-object/from16 v17, v6

    invoke-static/range {v17 .. v23}, Lf/h/a/c/e1/m/b;->e([B[IIIILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    goto :goto_12

    :cond_1a
    move-object/from16 v26, v2

    move-object/from16 v24, v4

    move/from16 v27, v6

    move-object/from16 v25, v13

    const/4 v7, 0x1

    :goto_12
    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    move-object/from16 v4, v24

    move-object/from16 v13, v25

    move-object/from16 v2, v26

    move/from16 v6, v27

    const/4 v7, 0x3

    const/4 v8, 0x2

    goto/16 :goto_f

    :cond_1b
    move-object/from16 v26, v2

    move-object/from16 v24, v4

    move/from16 v27, v6

    const/4 v7, 0x1

    iget-boolean v0, v10, Lf/h/a/c/e1/m/b$f;->b:Z

    if-eqz v0, :cond_1e

    iget v0, v10, Lf/h/a/c/e1/m/b$f;->e:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1c

    iget-object v0, v12, Lf/h/a/c/e1/m/b$a;->d:[I

    iget v4, v10, Lf/h/a/c/e1/m/b$f;->g:I

    aget v0, v0, v4

    const/4 v4, 0x2

    goto :goto_13

    :cond_1c
    const/4 v4, 0x2

    if-ne v0, v4, :cond_1d

    iget-object v0, v12, Lf/h/a/c/e1/m/b$a;->c:[I

    iget v6, v10, Lf/h/a/c/e1/m/b$f;->h:I

    aget v0, v0, v6

    goto :goto_13

    :cond_1d
    iget-object v0, v12, Lf/h/a/c/e1/m/b$a;->b:[I

    iget v6, v10, Lf/h/a/c/e1/m/b$f;->i:I

    aget v0, v0, v6

    :goto_13
    iget-object v6, v3, Lf/h/a/c/e1/m/b;->b:Landroid/graphics/Paint;

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v3, Lf/h/a/c/e1/m/b;->c:Landroid/graphics/Canvas;

    int-to-float v6, v11

    int-to-float v8, v9

    iget v12, v10, Lf/h/a/c/e1/m/b$f;->c:I

    add-int/2addr v12, v11

    int-to-float v12, v12

    iget v13, v10, Lf/h/a/c/e1/m/b$f;->d:I

    add-int/2addr v13, v9

    int-to-float v13, v13

    iget-object v14, v3, Lf/h/a/c/e1/m/b;->b:Landroid/graphics/Paint;

    move-object/from16 v16, v0

    move/from16 v17, v6

    move/from16 v18, v8

    move/from16 v19, v12

    move/from16 v20, v13

    move-object/from16 v21, v14

    invoke-virtual/range {v16 .. v21}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_14

    :cond_1e
    const/4 v2, 0x3

    const/4 v4, 0x2

    :goto_14
    iget-object v0, v3, Lf/h/a/c/e1/m/b;->g:Landroid/graphics/Bitmap;

    iget v6, v10, Lf/h/a/c/e1/m/b$f;->c:I

    iget v8, v10, Lf/h/a/c/e1/m/b$f;->d:I

    invoke-static {v0, v11, v9, v6, v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v17

    new-instance v0, Lf/h/a/c/e1/b;

    int-to-float v6, v11

    iget v8, v1, Lf/h/a/c/e1/m/b$b;->a:I

    int-to-float v8, v8

    div-float v18, v6, v8

    const/16 v19, 0x0

    int-to-float v6, v9

    iget v9, v1, Lf/h/a/c/e1/m/b$b;->b:I

    int-to-float v9, v9

    div-float v20, v6, v9

    const/16 v21, 0x0

    iget v6, v10, Lf/h/a/c/e1/m/b$f;->c:I

    int-to-float v6, v6

    div-float v22, v6, v8

    iget v6, v10, Lf/h/a/c/e1/m/b$f;->d:I

    int-to-float v6, v6

    div-float v23, v6, v9

    move-object/from16 v16, v0

    invoke-direct/range {v16 .. v23}, Lf/h/a/c/e1/b;-><init>(Landroid/graphics/Bitmap;FIFIFF)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v3, Lf/h/a/c/e1/m/b;->c:Landroid/graphics/Canvas;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    const/4 v8, 0x0

    invoke-virtual {v0, v8, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, v3, Lf/h/a/c/e1/m/b;->c:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v6, v27, 0x1

    move-object/from16 v0, p0

    move-object/from16 v4, v24

    move-object/from16 v2, v26

    const/4 v7, 0x3

    const/4 v8, 0x2

    goto/16 :goto_e

    :cond_1f
    move-object/from16 v26, v2

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    :goto_15
    move-object/from16 v0, v26

    invoke-direct {v0, v1}, Lf/h/a/c/e1/m/c;-><init>(Ljava/util/List;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
