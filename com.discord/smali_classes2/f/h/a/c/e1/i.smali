.class public abstract Lf/h/a/c/e1/i;
.super Lf/h/a/c/y0/f;
.source "SubtitleOutputBuffer.java"

# interfaces
.implements Lf/h/a/c/e1/e;


# instance fields
.field public d:Lf/h/a/c/e1/e;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/a/c/y0/f;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    invoke-super {p0}, Lf/h/a/c/y0/a;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    return-void
.end method

.method public f(J)I
    .locals 3

    iget-object v0, p0, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v1, p0, Lf/h/a/c/e1/i;->e:J

    sub-long/2addr p1, v1

    invoke-interface {v0, p1, p2}, Lf/h/a/c/e1/e;->f(J)I

    move-result p1

    return p1
.end method

.method public g(I)J
    .locals 4

    iget-object v0, p0, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, p1}, Lf/h/a/c/e1/e;->g(I)J

    move-result-wide v0

    iget-wide v2, p0, Lf/h/a/c/e1/i;->e:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public h(J)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v1, p0, Lf/h/a/c/e1/i;->e:J

    sub-long/2addr p1, v1

    invoke-interface {v0, p1, p2}, Lf/h/a/c/e1/e;->h(J)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Lf/h/a/c/e1/e;->i()I

    move-result v0

    return v0
.end method
