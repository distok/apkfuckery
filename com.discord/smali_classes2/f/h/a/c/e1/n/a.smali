.class public final Lf/h/a/c/e1/n/a;
.super Lf/h/a/c/e1/c;
.source "PgsDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/e1/n/a$a;
    }
.end annotation


# instance fields
.field public final n:Lf/h/a/c/i1/r;

.field public final o:Lf/h/a/c/i1/r;

.field public final p:Lf/h/a/c/e1/n/a$a;

.field public q:Ljava/util/zip/Inflater;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "PgsDecoder"

    invoke-direct {p0, v0}, Lf/h/a/c/e1/c;-><init>(Ljava/lang/String;)V

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/n/a;->n:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/n/a;->o:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/e1/n/a$a;

    invoke-direct {v0}, Lf/h/a/c/e1/n/a$a;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/n/a;->p:Lf/h/a/c/e1/n/a$a;

    return-void
.end method


# virtual methods
.method public j([BIZ)Lf/h/a/c/e1/e;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/text/SubtitleDecoderException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/h/a/c/e1/n/a;->n:Lf/h/a/c/i1/r;

    move-object/from16 v2, p1

    iput-object v2, v1, Lf/h/a/c/i1/r;->a:[B

    move/from16 v2, p2

    iput v2, v1, Lf/h/a/c/i1/r;->c:I

    const/4 v2, 0x0

    iput v2, v1, Lf/h/a/c/i1/r;->b:I

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->a()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {v1}, Lf/h/a/c/i1/r;->b()I

    move-result v3

    const/16 v4, 0x78

    if-ne v3, v4, :cond_1

    iget-object v3, v0, Lf/h/a/c/e1/n/a;->q:Ljava/util/zip/Inflater;

    if-nez v3, :cond_0

    new-instance v3, Ljava/util/zip/Inflater;

    invoke-direct {v3}, Ljava/util/zip/Inflater;-><init>()V

    iput-object v3, v0, Lf/h/a/c/e1/n/a;->q:Ljava/util/zip/Inflater;

    :cond_0
    iget-object v3, v0, Lf/h/a/c/e1/n/a;->o:Lf/h/a/c/i1/r;

    iget-object v4, v0, Lf/h/a/c/e1/n/a;->q:Ljava/util/zip/Inflater;

    invoke-static {v1, v3, v4}, Lf/h/a/c/i1/a0;->q(Lf/h/a/c/i1/r;Lf/h/a/c/i1/r;Ljava/util/zip/Inflater;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Lf/h/a/c/e1/n/a;->o:Lf/h/a/c/i1/r;

    iget-object v4, v3, Lf/h/a/c/i1/r;->a:[B

    iget v3, v3, Lf/h/a/c/i1/r;->c:I

    invoke-virtual {v1, v4, v3}, Lf/h/a/c/i1/r;->A([BI)V

    :cond_1
    iget-object v1, v0, Lf/h/a/c/e1/n/a;->p:Lf/h/a/c/e1/n/a$a;

    invoke-virtual {v1}, Lf/h/a/c/e1/n/a$a;->a()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object v3, v0, Lf/h/a/c/e1/n/a;->n:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->a()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_15

    iget-object v3, v0, Lf/h/a/c/e1/n/a;->n:Lf/h/a/c/i1/r;

    iget-object v5, v0, Lf/h/a/c/e1/n/a;->p:Lf/h/a/c/e1/n/a$a;

    iget v6, v3, Lf/h/a/c/i1/r;->c:I

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v7

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->v()I

    move-result v8

    iget v9, v3, Lf/h/a/c/i1/r;->b:I

    add-int/2addr v9, v8

    if-le v9, v6, :cond_2

    invoke-virtual {v3, v6}, Lf/h/a/c/i1/r;->C(I)V

    const/4 v10, 0x0

    const/4 v13, 0x0

    goto/16 :goto_c

    :cond_2
    const/16 v6, 0x80

    if-eq v7, v6, :cond_c

    packed-switch v7, :pswitch_data_0

    :cond_3
    :goto_1
    move-object v7, v3

    goto/16 :goto_4

    :pswitch_0
    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v4, 0x13

    if-ge v8, v4, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->v()I

    move-result v4

    iput v4, v5, Lf/h/a/c/e1/n/a$a;->d:I

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->v()I

    move-result v4

    iput v4, v5, Lf/h/a/c/e1/n/a$a;->e:I

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->v()I

    move-result v4

    iput v4, v5, Lf/h/a/c/e1/n/a$a;->f:I

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->v()I

    move-result v4

    iput v4, v5, Lf/h/a/c/e1/n/a$a;->g:I

    goto :goto_1

    :pswitch_1
    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v7, 0x4

    if-ge v8, v7, :cond_5

    goto :goto_1

    :cond_5
    invoke-virtual {v3, v4}, Lf/h/a/c/i1/r;->D(I)V

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v4

    and-int/2addr v4, v6

    if-eqz v4, :cond_6

    const/4 v11, 0x1

    goto :goto_2

    :cond_6
    const/4 v11, 0x0

    :goto_2
    add-int/lit8 v8, v8, -0x4

    if-eqz v11, :cond_9

    const/4 v4, 0x7

    if-ge v8, v4, :cond_7

    goto :goto_1

    :cond_7
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->s()I

    move-result v4

    if-ge v4, v7, :cond_8

    goto :goto_1

    :cond_8
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->v()I

    move-result v6

    iput v6, v5, Lf/h/a/c/e1/n/a$a;->h:I

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->v()I

    move-result v6

    iput v6, v5, Lf/h/a/c/e1/n/a$a;->i:I

    iget-object v6, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    add-int/lit8 v4, v4, -0x4

    invoke-virtual {v6, v4}, Lf/h/a/c/i1/r;->y(I)V

    add-int/lit8 v8, v8, -0x7

    :cond_9
    iget-object v4, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    iget v6, v4, Lf/h/a/c/i1/r;->b:I

    iget v4, v4, Lf/h/a/c/i1/r;->c:I

    if-ge v6, v4, :cond_3

    if-lez v8, :cond_3

    sub-int/2addr v4, v6

    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v7, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    iget-object v7, v7, Lf/h/a/c/i1/r;->a:[B

    invoke-virtual {v3, v7, v6, v4}, Lf/h/a/c/i1/r;->d([BII)V

    iget-object v5, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    add-int/2addr v6, v4

    invoke-virtual {v5, v6}, Lf/h/a/c/i1/r;->C(I)V

    goto :goto_1

    :pswitch_2
    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    rem-int/lit8 v4, v8, 0x5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_a

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v3, v6}, Lf/h/a/c/i1/r;->D(I)V

    iget-object v4, v5, Lf/h/a/c/e1/n/a$a;->b:[I

    invoke-static {v4, v2}, Ljava/util/Arrays;->fill([II)V

    div-int/lit8 v8, v8, 0x5

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v8, :cond_b

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v6

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v7

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v12

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v13

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->q()I

    move-result v14

    int-to-double v10, v7

    const-wide v15, 0x3ff66e978d4fdf3bL    # 1.402

    add-int/lit8 v12, v12, -0x80

    move-object v7, v3

    int-to-double v2, v12

    mul-double v15, v15, v2

    move-object v12, v1

    add-double v0, v15, v10

    double-to-int v0, v0

    const-wide v15, 0x3fd60663c74fb54aL    # 0.34414

    add-int/lit8 v13, v13, -0x80

    move-object v1, v12

    int-to-double v12, v13

    mul-double v15, v15, v12

    sub-double v15, v10, v15

    const-wide v17, 0x3fe6da3c21187e7cL    # 0.71414

    mul-double v2, v2, v17

    sub-double v2, v15, v2

    double-to-int v2, v2

    const-wide v15, 0x3ffc5a1cac083127L    # 1.772

    mul-double v12, v12, v15

    add-double/2addr v12, v10

    double-to-int v3, v12

    iget-object v10, v5, Lf/h/a/c/e1/n/a$a;->b:[I

    shl-int/lit8 v11, v14, 0x18

    const/16 v12, 0xff

    const/4 v13, 0x0

    invoke-static {v0, v13, v12}, Lf/h/a/c/i1/a0;->f(III)I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v11

    invoke-static {v2, v13, v12}, Lf/h/a/c/i1/a0;->f(III)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    invoke-static {v3, v13, v12}, Lf/h/a/c/i1/a0;->f(III)I

    move-result v2

    or-int/2addr v0, v2

    aput v0, v10, v6

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    move-object v3, v7

    const/4 v2, 0x0

    goto :goto_3

    :cond_b
    move-object v7, v3

    const/4 v0, 0x1

    iput-boolean v0, v5, Lf/h/a/c/e1/n/a$a;->c:Z

    :goto_4
    const/4 v10, 0x0

    const/4 v13, 0x0

    goto/16 :goto_b

    :cond_c
    move-object v7, v3

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->d:I

    if-eqz v0, :cond_13

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->e:I

    if-eqz v0, :cond_13

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->h:I

    if-eqz v0, :cond_13

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->i:I

    if-eqz v0, :cond_13

    iget-object v0, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    iget v2, v0, Lf/h/a/c/i1/r;->c:I

    if-eqz v2, :cond_13

    iget v3, v0, Lf/h/a/c/i1/r;->b:I

    if-ne v3, v2, :cond_13

    iget-boolean v2, v5, Lf/h/a/c/e1/n/a$a;->c:Z

    if-nez v2, :cond_d

    goto/16 :goto_9

    :cond_d
    const/4 v13, 0x0

    invoke-virtual {v0, v13}, Lf/h/a/c/i1/r;->C(I)V

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->h:I

    iget v2, v5, Lf/h/a/c/e1/n/a$a;->i:I

    mul-int v0, v0, v2

    new-array v2, v0, [I

    const/4 v3, 0x0

    :cond_e
    :goto_5
    if-ge v3, v0, :cond_12

    iget-object v4, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->q()I

    move-result v4

    if-eqz v4, :cond_f

    add-int/lit8 v6, v3, 0x1

    iget-object v8, v5, Lf/h/a/c/e1/n/a$a;->b:[I

    aget v4, v8, v4

    aput v4, v2, v3

    :goto_6
    move v3, v6

    goto :goto_5

    :cond_f
    iget-object v4, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->q()I

    move-result v4

    if-eqz v4, :cond_e

    and-int/lit8 v6, v4, 0x40

    if-nez v6, :cond_10

    and-int/lit8 v6, v4, 0x3f

    goto :goto_7

    :cond_10
    and-int/lit8 v6, v4, 0x3f

    shl-int/lit8 v6, v6, 0x8

    iget-object v8, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->q()I

    move-result v8

    or-int/2addr v6, v8

    :goto_7
    and-int/lit16 v4, v4, 0x80

    if-nez v4, :cond_11

    const/4 v4, 0x0

    goto :goto_8

    :cond_11
    iget-object v4, v5, Lf/h/a/c/e1/n/a$a;->b:[I

    iget-object v8, v5, Lf/h/a/c/e1/n/a$a;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->q()I

    move-result v8

    aget v4, v4, v8

    :goto_8
    add-int/2addr v6, v3

    invoke-static {v2, v3, v6, v4}, Ljava/util/Arrays;->fill([IIII)V

    goto :goto_6

    :cond_12
    iget v0, v5, Lf/h/a/c/e1/n/a$a;->h:I

    iget v3, v5, Lf/h/a/c/e1/n/a$a;->i:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v15

    new-instance v10, Lf/h/a/c/e1/b;

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->f:I

    int-to-float v0, v0

    iget v2, v5, Lf/h/a/c/e1/n/a$a;->d:I

    int-to-float v2, v2

    div-float v16, v0, v2

    const/16 v17, 0x0

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->g:I

    int-to-float v0, v0

    iget v3, v5, Lf/h/a/c/e1/n/a$a;->e:I

    int-to-float v3, v3

    div-float v18, v0, v3

    const/16 v19, 0x0

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->h:I

    int-to-float v0, v0

    div-float v20, v0, v2

    iget v0, v5, Lf/h/a/c/e1/n/a$a;->i:I

    int-to-float v0, v0

    div-float v21, v0, v3

    move-object v14, v10

    invoke-direct/range {v14 .. v21}, Lf/h/a/c/e1/b;-><init>(Landroid/graphics/Bitmap;FIFIFF)V

    goto :goto_a

    :cond_13
    :goto_9
    const/4 v13, 0x0

    const/4 v10, 0x0

    :goto_a
    invoke-virtual {v5}, Lf/h/a/c/e1/n/a$a;->a()V

    :goto_b
    invoke-virtual {v7, v9}, Lf/h/a/c/i1/r;->C(I)V

    :goto_c
    move-object v0, v1

    if-eqz v10, :cond_14

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_14
    move-object v1, v0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    goto/16 :goto_0

    :cond_15
    move-object v0, v1

    new-instance v1, Lf/h/a/c/e1/n/b;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Lf/h/a/c/e1/n/b;-><init>(Ljava/util/List;)V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
