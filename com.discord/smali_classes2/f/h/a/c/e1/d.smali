.class public final Lf/h/a/c/e1/d;
.super Lf/h/a/c/e1/i;
.source "SimpleSubtitleOutputBuffer.java"


# instance fields
.field public final f:Lf/h/a/c/e1/c;


# direct methods
.method public constructor <init>(Lf/h/a/c/e1/c;)V
    .locals 0

    invoke-direct {p0}, Lf/h/a/c/e1/i;-><init>()V

    iput-object p1, p0, Lf/h/a/c/e1/d;->f:Lf/h/a/c/e1/c;

    return-void
.end method


# virtual methods
.method public final release()V
    .locals 5

    iget-object v0, p0, Lf/h/a/c/e1/d;->f:Lf/h/a/c/e1/c;

    iget-object v1, v0, Lf/h/a/c/y0/g;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lf/h/a/c/e1/i;->clear()V

    iget-object v2, v0, Lf/h/a/c/y0/g;->f:[Lf/h/a/c/y0/f;

    iget v3, v0, Lf/h/a/c/y0/g;->h:I

    add-int/lit8 v4, v3, 0x1

    iput v4, v0, Lf/h/a/c/y0/g;->h:I

    aput-object p0, v2, v3

    invoke-virtual {v0}, Lf/h/a/c/y0/g;->g()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
