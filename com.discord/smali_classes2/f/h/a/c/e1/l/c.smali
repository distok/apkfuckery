.class public final Lf/h/a/c/e1/l/c;
.super Lf/h/a/c/e1/l/d;
.source "Cea708Decoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/e1/l/c$a;,
        Lf/h/a/c/e1/l/c$b;
    }
.end annotation


# instance fields
.field public final g:Lf/h/a/c/i1/r;

.field public final h:Lf/h/a/c/i1/q;

.field public final i:I

.field public final j:[Lf/h/a/c/e1/l/c$a;

.field public k:Lf/h/a/c/e1/l/c$a;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lf/h/a/c/e1/l/c$b;

.field public o:I


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "[B>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/h/a/c/e1/l/d;-><init>()V

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->g:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/i1/q;

    invoke-direct {v0}, Lf/h/a/c/i1/q;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    iput p1, p0, Lf/h/a/c/e1/l/c;->i:I

    const/16 p1, 0x8

    new-array v0, p1, [Lf/h/a/c/e1/l/c$a;

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    new-instance v3, Lf/h/a/c/e1/l/c$a;

    invoke-direct {v3}, Lf/h/a/c/e1/l/c$a;-><init>()V

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object p1, p1, v0

    iput-object p1, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->k()V

    return-void
.end method


# virtual methods
.method public e()Lf/h/a/c/e1/e;
    .locals 2

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->l:Ljava/util/List;

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->m:Ljava/util/List;

    new-instance v1, Lf/h/a/c/e1/l/e;

    invoke-direct {v1, v0}, Lf/h/a/c/e1/l/e;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method public f(Lf/h/a/c/e1/h;)V
    .locals 7

    iget-object v0, p1, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->g:Lf/h/a/c/i1/r;

    iget-object p1, p1, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result p1

    invoke-virtual {v1, v0, p1}, Lf/h/a/c/i1/r;->A([BI)V

    :cond_0
    :goto_0
    iget-object p1, p0, Lf/h/a/c/e1/l/c;->g:Lf/h/a/c/i1/r;

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->a()I

    move-result p1

    const/4 v0, 0x3

    if-lt p1, v0, :cond_8

    iget-object p1, p0, Lf/h/a/c/e1/l/c;->g:Lf/h/a/c/i1/r;

    invoke-virtual {p1}, Lf/h/a/c/i1/r;->q()I

    move-result p1

    and-int/lit8 p1, p1, 0x7

    and-int/lit8 v1, p1, 0x3

    const/4 v2, 0x4

    and-int/2addr p1, v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne p1, v2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iget-object v2, p0, Lf/h/a/c/e1/l/c;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v2}, Lf/h/a/c/i1/r;->q()I

    move-result v2

    int-to-byte v2, v2

    iget-object v5, p0, Lf/h/a/c/e1/l/c;->g:Lf/h/a/c/i1/r;

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->q()I

    move-result v5

    int-to-byte v5, v5

    const/4 v6, 0x2

    if-eq v1, v6, :cond_2

    if-eq v1, v0, :cond_2

    goto :goto_0

    :cond_2
    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    if-ne v1, v0, :cond_5

    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->i()V

    and-int/lit16 p1, v2, 0xc0

    shr-int/lit8 p1, p1, 0x6

    and-int/lit8 v0, v2, 0x3f

    if-nez v0, :cond_4

    const/16 v0, 0x40

    :cond_4
    new-instance v1, Lf/h/a/c/e1/l/c$b;

    invoke-direct {v1, p1, v0}, Lf/h/a/c/e1/l/c$b;-><init>(II)V

    iput-object v1, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    iget-object p1, v1, Lf/h/a/c/e1/l/c$b;->c:[B

    iget v0, v1, Lf/h/a/c/e1/l/c$b;->d:I

    add-int/lit8 v2, v0, 0x1

    iput v2, v1, Lf/h/a/c/e1/l/c$b;->d:I

    aput-byte v5, p1, v0

    goto :goto_2

    :cond_5
    if-ne v1, v6, :cond_6

    const/4 v3, 0x1

    :cond_6
    invoke-static {v3}, Lf/g/j/k/a;->d(Z)V

    iget-object p1, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    if-nez p1, :cond_7

    const-string p1, "Cea708Decoder"

    const-string v0, "Encountered DTVCC_PACKET_DATA before DTVCC_PACKET_START"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_7
    iget-object v0, p1, Lf/h/a/c/e1/l/c$b;->c:[B

    iget v1, p1, Lf/h/a/c/e1/l/c$b;->d:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p1, Lf/h/a/c/e1/l/c$b;->d:I

    aput-byte v2, v0, v1

    add-int/lit8 v1, v3, 0x1

    iput v1, p1, Lf/h/a/c/e1/l/c$b;->d:I

    aput-byte v5, v0, v3

    :goto_2
    iget-object p1, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    iget v0, p1, Lf/h/a/c/e1/l/c$b;->d:I

    iget p1, p1, Lf/h/a/c/e1/l/c$b;->b:I

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v4

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->i()V

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.method public flush()V
    .locals 3

    invoke-super {p0}, Lf/h/a/c/e1/l/d;->flush()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->l:Ljava/util/List;

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->m:Ljava/util/List;

    const/4 v1, 0x0

    iput v1, p0, Lf/h/a/c/e1/l/c;->o:I

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object v1, v2, v1

    iput-object v1, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->k()V

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    return-void
.end method

.method public g()Z
    .locals 2

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->l:Ljava/util/List;

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->m:Ljava/util/List;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final i()V
    .locals 15

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v1, v0, Lf/h/a/c/e1/l/c$b;->d:I

    iget v2, v0, Lf/h/a/c/e1/l/c$b;->b:I

    const/4 v3, 0x2

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    const-string v4, "Cea708Decoder"

    if-eq v1, v2, :cond_1

    const-string v0, "DtvCcPacket ended prematurely; size is "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    iget v1, v1, Lf/h/a/c/e1/l/c$b;->b:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", but current index is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    iget v1, v1, Lf/h/a/c/e1/l/c$b;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " (sequence number "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    iget v1, v1, Lf/h/a/c/e1/l/c$b;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "); ignoring packet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :cond_1
    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    iget-object v0, v0, Lf/h/a/c/e1/l/c$b;->c:[B

    invoke-virtual {v2, v0, v1}, Lf/h/a/c/i1/q;->i([BI)V

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/4 v5, 0x5

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    const/4 v5, 0x7

    const/4 v6, 0x6

    if-ne v0, v5, :cond_2

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v6}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    if-ge v0, v5, :cond_2

    const-string v7, "Invalid extended service number: "

    invoke-static {v7, v0, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    :cond_2
    if-nez v2, :cond_3

    if-eqz v0, :cond_35

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "serviceNumber is non-zero ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") when blockSize is 0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :cond_3
    iget v2, p0, Lf/h/a/c/e1/l/c;->i:I

    if-eq v0, v2, :cond_4

    goto/16 :goto_a

    :cond_4
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2}, Lf/h/a/c/i1/q;->b()I

    move-result v2

    if-lez v2, :cond_34

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/16 v7, 0x8

    invoke-virtual {v2, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    const/16 v8, 0x17

    const/16 v9, 0x9f

    const/16 v10, 0x18

    const/16 v11, 0x7f

    const/16 v12, 0x1f

    const/16 v13, 0x10

    if-eq v2, v13, :cond_1d

    const/16 v14, 0xa

    if-gt v2, v12, :cond_9

    if-eqz v2, :cond_33

    if-eq v2, v1, :cond_8

    if-eq v2, v7, :cond_7

    packed-switch v2, :pswitch_data_0

    const/16 v1, 0x11

    if-lt v2, v1, :cond_5

    if-gt v2, v8, :cond_5

    const-string v1, "Currently unsupported COMMAND_EXT1 Command: "

    invoke-static {v1, v2, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v7}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_9

    :pswitch_0
    iget-object v1, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    invoke-virtual {v1, v14}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_9

    :pswitch_1
    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->k()V

    goto/16 :goto_9

    :cond_5
    if-lt v2, v10, :cond_6

    if-gt v2, v12, :cond_6

    const-string v1, "Currently unsupported COMMAND_P16 Command: "

    invoke-static {v1, v2, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v13}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_9

    :cond_6
    const-string v1, "Invalid C0 command: "

    invoke-static {v1, v2, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_9

    :cond_7
    iget-object v1, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    iget-object v2, v1, Lf/h/a/c/e1/l/c$a;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_33

    iget-object v1, v1, Lf/h/a/c/e1/l/c$a;->b:Landroid/text/SpannableStringBuilder;

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v1, v5, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_9

    :cond_8
    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->j()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lf/h/a/c/e1/l/c;->l:Ljava/util/List;

    goto/16 :goto_9

    :cond_9
    if-gt v2, v11, :cond_b

    if-ne v2, v11, :cond_a

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x266b

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_a
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    and-int/lit16 v1, v2, 0xff

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_b
    if-gt v2, v9, :cond_1b

    const/4 v0, 0x4

    packed-switch v2, :pswitch_data_1

    :pswitch_2
    const-string v0, "Invalid C1 command: "

    invoke-static {v0, v2}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :pswitch_3
    add-int/lit16 v2, v2, -0x98

    iget-object v8, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object v8, v8, v2

    iget-object v9, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v9, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v9, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v9}, Lf/h/a/c/i1/q;->e()Z

    move-result v9

    iget-object v10, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v10}, Lf/h/a/c/i1/q;->e()Z

    move-result v10

    iget-object v11, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v11}, Lf/h/a/c/i1/q;->e()Z

    iget-object v11, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v11, v1}, Lf/h/a/c/i1/q;->f(I)I

    move-result v11

    iget-object v12, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v12}, Lf/h/a/c/i1/q;->e()Z

    move-result v12

    iget-object v13, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v13, v5}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    iget-object v13, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v13, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v7

    iget-object v13, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v13, v0}, Lf/h/a/c/i1/q;->f(I)I

    move-result v13

    iget-object v14, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v14, v0}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    iget-object v14, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v14, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v14, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v14, v6}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v6, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v6, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v6, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v6, v1}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    iget-object v14, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v14, v1}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    const/4 v14, 0x1

    iput-boolean v14, v8, Lf/h/a/c/e1/l/c$a;->c:Z

    iput-boolean v9, v8, Lf/h/a/c/e1/l/c$a;->d:Z

    iput-boolean v10, v8, Lf/h/a/c/e1/l/c$a;->k:Z

    iput v11, v8, Lf/h/a/c/e1/l/c$a;->e:I

    iput-boolean v12, v8, Lf/h/a/c/e1/l/c$a;->f:Z

    iput v5, v8, Lf/h/a/c/e1/l/c$a;->g:I

    iput v7, v8, Lf/h/a/c/e1/l/c$a;->h:I

    iput v13, v8, Lf/h/a/c/e1/l/c$a;->i:I

    iget v5, v8, Lf/h/a/c/e1/l/c$a;->j:I

    add-int/2addr v0, v14

    if-eq v5, v0, :cond_e

    iput v0, v8, Lf/h/a/c/e1/l/c$a;->j:I

    :goto_1
    if-eqz v10, :cond_c

    iget-object v0, v8, Lf/h/a/c/e1/l/c$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v5, v8, Lf/h/a/c/e1/l/c$a;->j:I

    if-ge v0, v5, :cond_d

    :cond_c
    iget-object v0, v8, Lf/h/a/c/e1/l/c$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v5, 0xf

    if-lt v0, v5, :cond_e

    :cond_d
    iget-object v0, v8, Lf/h/a/c/e1/l/c$a;->a:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_e
    if-eqz v6, :cond_f

    iget v0, v8, Lf/h/a/c/e1/l/c$a;->m:I

    if-eq v0, v6, :cond_f

    iput v6, v8, Lf/h/a/c/e1/l/c$a;->m:I

    add-int/lit8 v6, v6, -0x1

    sget-object v0, Lf/h/a/c/e1/l/c$a;->D:[I

    aget v0, v0, v6

    sget-object v5, Lf/h/a/c/e1/l/c$a;->C:[Z

    aget-boolean v5, v5, v6

    sget-object v5, Lf/h/a/c/e1/l/c$a;->A:[I

    aget v5, v5, v6

    sget-object v5, Lf/h/a/c/e1/l/c$a;->B:[I

    aget v5, v5, v6

    sget-object v5, Lf/h/a/c/e1/l/c$a;->z:[I

    aget v5, v5, v6

    iput v0, v8, Lf/h/a/c/e1/l/c$a;->o:I

    iput v5, v8, Lf/h/a/c/e1/l/c$a;->l:I

    :cond_f
    if-eqz v1, :cond_10

    iget v0, v8, Lf/h/a/c/e1/l/c$a;->n:I

    if-eq v0, v1, :cond_10

    iput v1, v8, Lf/h/a/c/e1/l/c$a;->n:I

    add-int/lit8 v1, v1, -0x1

    sget-object v0, Lf/h/a/c/e1/l/c$a;->F:[I

    aget v0, v0, v1

    sget-object v0, Lf/h/a/c/e1/l/c$a;->E:[I

    aget v0, v0, v1

    const/4 v0, 0x0

    invoke-virtual {v8, v0, v0}, Lf/h/a/c/e1/l/c$a;->g(ZZ)V

    sget v0, Lf/h/a/c/e1/l/c$a;->w:I

    sget-object v5, Lf/h/a/c/e1/l/c$a;->G:[I

    aget v1, v5, v1

    invoke-virtual {v8, v0, v1}, Lf/h/a/c/e1/l/c$a;->h(II)V

    :cond_10
    iget v0, p0, Lf/h/a/c/e1/l/c;->o:I

    if-eq v0, v2, :cond_31

    iput v2, p0, Lf/h/a/c/e1/l/c;->o:I

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object v0, v0, v2

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    goto/16 :goto_8

    :pswitch_4
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    iget-boolean v0, v0, Lf/h/a/c/e1/l/c$a;->c:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_8

    :cond_11
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iget-object v5, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v5, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    invoke-static {v1, v2, v5, v0}, Lf/h/a/c/e1/l/c$a;->d(IIII)I

    move-result v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iget-object v5, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v5, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-static {v1, v2, v5, v6}, Lf/h/a/c/e1/l/c$a;->d(IIII)I

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v7}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    iput v0, v2, Lf/h/a/c/e1/l/c$a;->o:I

    iput v1, v2, Lf/h/a/c/e1/l/c$a;->l:I

    goto/16 :goto_8

    :pswitch_5
    iget-object v1, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    iget-boolean v1, v1, Lf/h/a/c/e1/l/c$a;->c:Z

    if-nez v1, :cond_12

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v13}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_8

    :cond_12
    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v0}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v0}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    iget v2, v1, Lf/h/a/c/e1/l/c$a;->v:I

    if-eq v2, v0, :cond_13

    invoke-virtual {v1, v14}, Lf/h/a/c/e1/l/c$a;->a(C)V

    :cond_13
    iput v0, v1, Lf/h/a/c/e1/l/c$a;->v:I

    goto/16 :goto_8

    :pswitch_6
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    iget-boolean v0, v0, Lf/h/a/c/e1/l/c$a;->c:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v10}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_8

    :cond_14
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iget-object v5, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v5, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    invoke-static {v1, v2, v5, v0}, Lf/h/a/c/e1/l/c$a;->d(IIII)I

    move-result v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iget-object v5, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v5, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    iget-object v6, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v6, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    invoke-static {v2, v5, v6, v1}, Lf/h/a/c/e1/l/c$a;->d(IIII)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v2

    iget-object v5, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v5, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v5

    iget-object v6, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v6, v3}, Lf/h/a/c/i1/q;->f(I)I

    move-result v6

    const/4 v7, 0x0

    invoke-static {v2, v5, v6, v7}, Lf/h/a/c/e1/l/c$a;->d(IIII)I

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    invoke-virtual {v2, v0, v1}, Lf/h/a/c/e1/l/c$a;->h(II)V

    goto/16 :goto_8

    :pswitch_7
    iget-object v1, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    iget-boolean v1, v1, Lf/h/a/c/e1/l/c$a;->c:Z

    if-nez v1, :cond_15

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v13}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_8

    :cond_15
    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v0}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v3}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0}, Lf/h/a/c/i1/q;->e()Z

    move-result v0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v2, v5}, Lf/h/a/c/i1/q;->f(I)I

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    invoke-virtual {v2, v0, v1}, Lf/h/a/c/e1/l/c$a;->g(ZZ)V

    goto/16 :goto_8

    :pswitch_8
    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->k()V

    goto/16 :goto_8

    :pswitch_9
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v0, v7}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_8

    :pswitch_a
    const/4 v0, 0x1

    :goto_2
    if-gt v0, v7, :cond_31

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    move-result v1

    if-eqz v1, :cond_16

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    rsub-int/lit8 v2, v0, 0x8

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lf/h/a/c/e1/l/c$a;->f()V

    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_b
    const/4 v0, 0x1

    :goto_3
    if-gt v0, v7, :cond_31

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    move-result v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    rsub-int/lit8 v2, v0, 0x8

    aget-object v1, v1, v2

    iget-boolean v2, v1, Lf/h/a/c/e1/l/c$a;->d:Z

    xor-int/lit8 v2, v2, 0x1

    iput-boolean v2, v1, Lf/h/a/c/e1/l/c$a;->d:Z

    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_c
    const/4 v0, 0x1

    :goto_4
    if-gt v0, v7, :cond_31

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    rsub-int/lit8 v2, v0, 0x8

    aget-object v1, v1, v2

    const/4 v2, 0x0

    iput-boolean v2, v1, Lf/h/a/c/e1/l/c$a;->d:Z

    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :pswitch_d
    const/4 v0, 0x1

    :goto_5
    if-gt v0, v7, :cond_31

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    rsub-int/lit8 v2, v0, 0x8

    aget-object v1, v1, v2

    const/4 v2, 0x1

    iput-boolean v2, v1, Lf/h/a/c/e1/l/c$a;->d:Z

    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :pswitch_e
    const/4 v0, 0x1

    :goto_6
    if-gt v0, v7, :cond_31

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1}, Lf/h/a/c/i1/q;->e()Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    rsub-int/lit8 v2, v0, 0x8

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lf/h/a/c/e1/l/c$a;->c()V

    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :pswitch_f
    add-int/lit8 v2, v2, -0x80

    iget v0, p0, Lf/h/a/c/e1/l/c;->o:I

    if-eq v0, v2, :cond_31

    iput v2, p0, Lf/h/a/c/e1/l/c;->o:I

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object v0, v0, v2

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    goto/16 :goto_8

    :goto_7
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    :cond_1b
    const/16 v1, 0xff

    if-gt v2, v1, :cond_1c

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    and-int/lit16 v1, v2, 0xff

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_1c
    const-string v1, "Invalid base command: "

    invoke-static {v1, v2, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_9

    :cond_1d
    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v7}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    if-gt v1, v12, :cond_21

    const/4 v2, 0x7

    if-gt v1, v2, :cond_1e

    goto/16 :goto_9

    :cond_1e
    const/16 v2, 0xf

    if-gt v1, v2, :cond_1f

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v7}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_9

    :cond_1f
    if-gt v1, v8, :cond_20

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v13}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_9

    :cond_20
    if-gt v1, v12, :cond_33

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v10}, Lf/h/a/c/i1/q;->l(I)V

    goto/16 :goto_9

    :cond_21
    const/16 v2, 0xa0

    if-gt v1, v11, :cond_2c

    const/16 v0, 0x20

    if-eq v1, v0, :cond_2b

    const/16 v0, 0x21

    if-eq v1, v0, :cond_2a

    const/16 v0, 0x25

    if-eq v1, v0, :cond_29

    const/16 v0, 0x2a

    if-eq v1, v0, :cond_28

    const/16 v0, 0x2c

    if-eq v1, v0, :cond_27

    const/16 v0, 0x3f

    if-eq v1, v0, :cond_26

    const/16 v0, 0x39

    if-eq v1, v0, :cond_25

    const/16 v0, 0x3a

    if-eq v1, v0, :cond_24

    const/16 v0, 0x3c

    if-eq v1, v0, :cond_23

    const/16 v0, 0x3d

    if-eq v1, v0, :cond_22

    packed-switch v1, :pswitch_data_2

    packed-switch v1, :pswitch_data_3

    const-string v0, "Invalid G2 character: "

    invoke-static {v0, v1, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_8

    :pswitch_10
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2022

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_11
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x201d

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_12
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x201c

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_13
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2019

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_14
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2018

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_15
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2588

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_16
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x250c

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_17
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2518

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_18
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2500

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_19
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2514

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_1a
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2510

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_1b
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2502

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_1c
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x215e

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_1d
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x215d

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_1e
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x215c

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :pswitch_1f
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x215b

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_22
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2120

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_23
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x153

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_24
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x161

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_25
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2122

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_26
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x178

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_27
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x152

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto/16 :goto_8

    :cond_28
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x160

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto :goto_8

    :cond_29
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x2026

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto :goto_8

    :cond_2a
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    invoke-virtual {v0, v2}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto :goto_8

    :cond_2b
    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto :goto_8

    :cond_2c
    const/16 v5, 0x20

    if-gt v1, v9, :cond_2f

    const/16 v2, 0x87

    if-gt v1, v2, :cond_2d

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v5}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_9

    :cond_2d
    const/16 v2, 0x8f

    if-gt v1, v2, :cond_2e

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_9

    :cond_2e
    if-gt v1, v9, :cond_33

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    invoke-virtual {v1, v3}, Lf/h/a/c/i1/q;->l(I)V

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lf/h/a/c/i1/q;->f(I)I

    move-result v1

    iget-object v2, p0, Lf/h/a/c/e1/l/c;->h:Lf/h/a/c/i1/q;

    mul-int/lit8 v1, v1, 0x8

    invoke-virtual {v2, v1}, Lf/h/a/c/i1/q;->l(I)V

    goto :goto_9

    :cond_2f
    const/16 v5, 0xff

    if-gt v1, v5, :cond_32

    if-ne v1, v2, :cond_30

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x33c4

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    goto :goto_8

    :cond_30
    const-string v0, "Invalid G3 character: "

    invoke-static {v0, v1, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    iget-object v0, p0, Lf/h/a/c/e1/l/c;->k:Lf/h/a/c/e1/l/c$a;

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Lf/h/a/c/e1/l/c$a;->a(C)V

    :cond_31
    :goto_8
    :pswitch_20
    const/4 v0, 0x1

    goto :goto_9

    :cond_32
    const-string v2, "Invalid extended command: "

    invoke-static {v2, v1, v4}, Lf/e/c/a/a;->U(Ljava/lang/String;ILjava/lang/String;)V

    :cond_33
    :goto_9
    :pswitch_21
    const/4 v1, 0x3

    const/4 v5, 0x7

    const/4 v6, 0x6

    goto/16 :goto_0

    :cond_34
    if-eqz v0, :cond_35

    invoke-virtual {p0}, Lf/h/a/c/e1/l/c;->j()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->l:Ljava/util/List;

    :cond_35
    :goto_a
    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/e1/l/c;->n:Lf/h/a/c/e1/l/c$b;

    return-void

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_0
        :pswitch_21
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x80
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_20
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x30
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x76
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
    .end packed-switch
.end method

.method public final j()Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x8

    if-ge v2, v3, :cond_d

    iget-object v3, v0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lf/h/a/c/e1/l/c$a;->e()Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, v0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object v4, v3, v2

    iget-boolean v4, v4, Lf/h/a/c/e1/l/c$a;->d:Z

    if-eqz v4, :cond_c

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lf/h/a/c/e1/l/c$a;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto/16 :goto_8

    :cond_0
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v4, 0x0

    :goto_1
    iget-object v6, v3, Lf/h/a/c/e1/l/c$a;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_1

    iget-object v6, v3, Lf/h/a/c/e1/l/c$a;->a:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Lf/h/a/c/e1/l/c$a;->b()Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget v4, v3, Lf/h/a/c/e1/l/c$a;->l:I

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v4, :cond_5

    if-eq v4, v7, :cond_4

    if-eq v4, v6, :cond_3

    const/4 v6, 0x3

    if-ne v4, v6, :cond_2

    goto :goto_2

    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unexpected justification value: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v3, Lf/h/a/c/e1/l/c$a;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_3

    :cond_4
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_3

    :cond_5
    :goto_2
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    :goto_3
    move-object v6, v4

    iget-boolean v4, v3, Lf/h/a/c/e1/l/c$a;->f:Z

    if-eqz v4, :cond_6

    iget v4, v3, Lf/h/a/c/e1/l/c$a;->h:I

    int-to-float v4, v4

    const/high16 v8, 0x42c60000    # 99.0f

    div-float/2addr v4, v8

    iget v9, v3, Lf/h/a/c/e1/l/c$a;->g:I

    int-to-float v9, v9

    div-float/2addr v9, v8

    goto :goto_4

    :cond_6
    iget v4, v3, Lf/h/a/c/e1/l/c$a;->h:I

    int-to-float v4, v4

    const/high16 v8, 0x43510000    # 209.0f

    div-float/2addr v4, v8

    iget v8, v3, Lf/h/a/c/e1/l/c$a;->g:I

    int-to-float v8, v8

    const/high16 v9, 0x42940000    # 74.0f

    div-float v9, v8, v9

    :goto_4
    const v8, 0x3f666666    # 0.9f

    mul-float v4, v4, v8

    const v10, 0x3d4ccccd    # 0.05f

    add-float v11, v4, v10

    mul-float v9, v9, v8

    add-float v8, v9, v10

    iget v4, v3, Lf/h/a/c/e1/l/c$a;->i:I

    rem-int/lit8 v9, v4, 0x3

    if-nez v9, :cond_7

    const/4 v9, 0x0

    goto :goto_5

    :cond_7
    if-ne v9, v7, :cond_8

    const/4 v9, 0x1

    goto :goto_5

    :cond_8
    const/4 v9, 0x2

    :goto_5
    div-int/lit8 v4, v4, 0x3

    if-nez v4, :cond_9

    const/4 v4, 0x0

    const/4 v12, 0x0

    goto :goto_6

    :cond_9
    if-ne v4, v7, :cond_a

    const/4 v4, 0x1

    const/4 v12, 0x1

    goto :goto_6

    :cond_a
    const/4 v4, 0x2

    const/4 v12, 0x2

    :goto_6
    iget v14, v3, Lf/h/a/c/e1/l/c$a;->o:I

    sget v4, Lf/h/a/c/e1/l/c$a;->x:I

    if-eq v14, v4, :cond_b

    const/4 v4, 0x1

    const/4 v13, 0x1

    goto :goto_7

    :cond_b
    const/4 v4, 0x0

    const/4 v13, 0x0

    :goto_7
    new-instance v16, Lf/h/a/c/e1/l/b;

    const/4 v10, 0x0

    const v15, -0x800001

    iget v3, v3, Lf/h/a/c/e1/l/c$a;->e:I

    move-object/from16 v4, v16

    move v7, v8

    move v8, v10

    move v10, v11

    move v11, v12

    move v12, v15

    move v15, v3

    invoke-direct/range {v4 .. v15}, Lf/h/a/c/e1/l/b;-><init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;FIIFIFZII)V

    move-object/from16 v3, v16

    :goto_8
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_d
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public final k()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/h/a/c/e1/l/c;->j:[Lf/h/a/c/e1/l/c$a;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lf/h/a/c/e1/l/c$a;->f()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
