.class public abstract Lf/h/a/c/e1/l/d;
.super Ljava/lang/Object;
.source "CeaDecoder.java"

# interfaces
.implements Lf/h/a/c/e1/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/e1/l/d$c;,
        Lf/h/a/c/e1/l/d$b;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lf/h/a/c/e1/l/d$b;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lf/h/a/c/e1/i;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue<",
            "Lf/h/a/c/e1/l/d$b;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lf/h/a/c/e1/l/d$b;

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/l/d;->a:Ljava/util/ArrayDeque;

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0xa

    const/4 v3, 0x0

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lf/h/a/c/e1/l/d;->a:Ljava/util/ArrayDeque;

    new-instance v4, Lf/h/a/c/e1/l/d$b;

    invoke-direct {v4, v3}, Lf/h/a/c/e1/l/d$b;-><init>(Lf/h/a/c/e1/l/d$a;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, p0, Lf/h/a/c/e1/l/d;->b:Ljava/util/ArrayDeque;

    :goto_1
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lf/h/a/c/e1/l/d;->b:Ljava/util/ArrayDeque;

    new-instance v2, Lf/h/a/c/e1/l/d$c;

    invoke-direct {v2, p0, v3}, Lf/h/a/c/e1/l/d$c;-><init>(Lf/h/a/c/e1/l/d;Lf/h/a/c/e1/l/d$a;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/l/d;->c:Ljava/util/PriorityQueue;

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lf/h/a/c/e1/l/d;->e:J

    return-void
.end method

.method public b()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/h/a/c/e1/l/d;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/e1/l/d$b;

    iget-wide v2, v0, Lf/h/a/c/y0/e;->f:J

    iget-wide v4, p0, Lf/h/a/c/e1/l/d;->e:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_3

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/e1/l/d$b;

    invoke-virtual {v0}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lf/h/a/c/e1/l/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/e1/i;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lf/h/a/c/y0/a;->addFlag(I)V

    invoke-virtual {p0, v0}, Lf/h/a/c/e1/l/d;->h(Lf/h/a/c/e1/l/d$b;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v0}, Lf/h/a/c/e1/l/d;->f(Lf/h/a/c/e1/h;)V

    invoke-virtual {p0}, Lf/h/a/c/e1/l/d;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lf/h/a/c/e1/l/d;->e()Lf/h/a/c/e1/e;

    move-result-object v2

    invoke-virtual {v0}, Lf/h/a/c/y0/a;->isDecodeOnly()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v1, p0, Lf/h/a/c/e1/l/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/e1/i;

    iget-wide v3, v0, Lf/h/a/c/y0/e;->f:J

    iput-wide v3, v1, Lf/h/a/c/y0/f;->timeUs:J

    iput-object v2, v1, Lf/h/a/c/e1/i;->d:Lf/h/a/c/e1/e;

    iput-wide v3, v1, Lf/h/a/c/e1/i;->e:J

    invoke-virtual {p0, v0}, Lf/h/a/c/e1/l/d;->h(Lf/h/a/c/e1/l/d$b;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v0}, Lf/h/a/c/e1/l/d;->h(Lf/h/a/c/e1/l/d$b;)V

    goto :goto_0

    :cond_3
    :goto_1
    return-object v1
.end method

.method public c()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->s(Z)V

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lf/h/a/c/e1/l/d;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/e1/l/d$b;

    iput-object v0, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    :goto_1
    return-object v0
.end method

.method public d(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lf/h/a/c/e1/h;

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->d(Z)V

    invoke-virtual {p1}, Lf/h/a/c/y0/a;->isDecodeOnly()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    invoke-virtual {p0, p1}, Lf/h/a/c/e1/l/d;->h(Lf/h/a/c/e1/l/d$b;)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    iget-wide v0, p0, Lf/h/a/c/e1/l/d;->f:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lf/h/a/c/e1/l/d;->f:J

    iput-wide v0, p1, Lf/h/a/c/e1/l/d$b;->j:J

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    :goto_1
    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    return-void
.end method

.method public abstract e()Lf/h/a/c/e1/e;
.end method

.method public abstract f(Lf/h/a/c/e1/h;)V
.end method

.method public flush()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lf/h/a/c/e1/l/d;->f:J

    iput-wide v0, p0, Lf/h/a/c/e1/l/d;->e:J

    :goto_0
    iget-object v0, p0, Lf/h/a/c/e1/l/d;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->c:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/c/e1/l/d$b;

    invoke-virtual {p0, v0}, Lf/h/a/c/e1/l/d;->h(Lf/h/a/c/e1/l/d$b;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lf/h/a/c/e1/l/d;->h(Lf/h/a/c/e1/l/d$b;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/h/a/c/e1/l/d;->d:Lf/h/a/c/e1/l/d$b;

    :cond_1
    return-void
.end method

.method public abstract g()Z
.end method

.method public final h(Lf/h/a/c/e1/l/d$b;)V
    .locals 1

    invoke-virtual {p1}, Lf/h/a/c/y0/e;->clear()V

    iget-object v0, p0, Lf/h/a/c/e1/l/d;->a:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public release()V
    .locals 0

    return-void
.end method
