.class public final Lf/h/a/c/e1/l/b;
.super Lf/h/a/c/e1/b;
.source "Cea708Cue.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/c/e1/b;",
        "Ljava/lang/Comparable<",
        "Lf/h/a/c/e1/l/b;",
        ">;"
    }
.end annotation


# instance fields
.field public final s:I


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;FIIFIFZII)V
    .locals 0

    invoke-direct/range {p0 .. p10}, Lf/h/a/c/e1/b;-><init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;FIIFIFZI)V

    iput p11, p0, Lf/h/a/c/e1/l/b;->s:I

    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lf/h/a/c/e1/l/b;

    iget p1, p1, Lf/h/a/c/e1/l/b;->s:I

    iget v0, p0, Lf/h/a/c/e1/l/b;->s:I

    if-ge p1, v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    if-le p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
