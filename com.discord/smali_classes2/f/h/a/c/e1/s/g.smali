.class public final Lf/h/a/c/e1/s/g;
.super Lf/h/a/c/e1/c;
.source "WebvttDecoder.java"


# instance fields
.field public final n:Lf/h/a/c/e1/s/f;

.field public final o:Lf/h/a/c/i1/r;

.field public final p:Lf/h/a/c/e1/s/e$b;

.field public final q:Lf/h/a/c/e1/s/a;

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/c/e1/s/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "WebvttDecoder"

    invoke-direct {p0, v0}, Lf/h/a/c/e1/c;-><init>(Ljava/lang/String;)V

    new-instance v0, Lf/h/a/c/e1/s/f;

    invoke-direct {v0}, Lf/h/a/c/e1/s/f;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/s/g;->n:Lf/h/a/c/e1/s/f;

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/e1/s/e$b;

    invoke-direct {v0}, Lf/h/a/c/e1/s/e$b;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/s/g;->p:Lf/h/a/c/e1/s/e$b;

    new-instance v0, Lf/h/a/c/e1/s/a;

    invoke-direct {v0}, Lf/h/a/c/e1/s/a;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/s/g;->q:Lf/h/a/c/e1/s/a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/s/g;->r:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public j([BIZ)Lf/h/a/c/e1/e;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/text/SubtitleDecoderException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v0, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    move-object/from16 v2, p1

    iput-object v2, v0, Lf/h/a/c/i1/r;->a:[B

    move/from16 v2, p2

    iput v2, v0, Lf/h/a/c/i1/r;->c:I

    const/4 v2, 0x0

    iput v2, v0, Lf/h/a/c/i1/r;->b:I

    iget-object v0, v1, Lf/h/a/c/e1/s/g;->p:Lf/h/a/c/e1/s/e$b;

    invoke-virtual {v0}, Lf/h/a/c/e1/s/e$b;->b()V

    iget-object v0, v1, Lf/h/a/c/e1/s/g;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :try_start_0
    iget-object v0, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    invoke-static {v0}, Lf/h/a/c/e1/s/h;->c(Lf/h/a/c/i1/r;)V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    :goto_1
    iget-object v3, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, -0x1

    const/4 v7, 0x1

    const/4 v8, -0x1

    const/4 v9, 0x0

    :goto_2
    if-ne v8, v6, :cond_5

    iget v9, v3, Lf/h/a/c/i1/r;->b:I

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    const/4 v8, 0x0

    goto :goto_2

    :cond_2
    const-string v10, "STYLE"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v8, 0x2

    goto :goto_2

    :cond_3
    const-string v10, "NOTE"

    invoke-virtual {v8, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v8, 0x1

    goto :goto_2

    :cond_4
    const/4 v8, 0x3

    goto :goto_2

    :cond_5
    invoke-virtual {v3, v9}, Lf/h/a/c/i1/r;->C(I)V

    if-eqz v8, :cond_32

    if-ne v8, v7, :cond_6

    iget-object v3, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    :goto_3
    invoke-virtual {v3}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_3

    :cond_6
    if-ne v8, v5, :cond_2c

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2b

    iget-object v3, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    invoke-virtual {v3}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    iget-object v3, v1, Lf/h/a/c/e1/s/g;->r:Ljava/util/List;

    iget-object v4, v1, Lf/h/a/c/e1/s/g;->q:Lf/h/a/c/e1/s/a;

    iget-object v5, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    iget-object v8, v4, Lf/h/a/c/e1/s/a;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget v8, v5, Lf/h/a/c/i1/r;->b:I

    :cond_7
    invoke-virtual {v5}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, v4, Lf/h/a/c/e1/s/a;->a:Lf/h/a/c/i1/r;

    iget-object v10, v5, Lf/h/a/c/i1/r;->a:[B

    iget v5, v5, Lf/h/a/c/i1/r;->b:I

    invoke-virtual {v9, v10, v5}, Lf/h/a/c/i1/r;->A([BI)V

    iget-object v5, v4, Lf/h/a/c/e1/s/a;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v5, v8}, Lf/h/a/c/i1/r;->C(I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :goto_4
    iget-object v8, v4, Lf/h/a/c/e1/s/a;->a:Lf/h/a/c/i1/r;

    iget-object v9, v4, Lf/h/a/c/e1/s/a;->b:Ljava/lang/StringBuilder;

    invoke-static {v8}, Lf/h/a/c/e1/s/a;->c(Lf/h/a/c/i1/r;)V

    invoke-virtual {v8}, Lf/h/a/c/i1/r;->a()I

    move-result v10

    const/4 v11, 0x5

    const-string/jumbo v12, "{"

    const-string v13, ""

    if-ge v10, v11, :cond_8

    goto :goto_8

    :cond_8
    invoke-virtual {v8, v11}, Lf/h/a/c/i1/r;->n(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "::cue"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_9

    goto :goto_8

    :cond_9
    iget v10, v8, Lf/h/a/c/i1/r;->b:I

    invoke-static {v8, v9}, Lf/h/a/c/e1/s/a;->b(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_a

    goto :goto_8

    :cond_a
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    invoke-virtual {v8, v10}, Lf/h/a/c/i1/r;->C(I)V

    move-object v10, v13

    goto :goto_9

    :cond_b
    const-string v10, "("

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    iget v10, v8, Lf/h/a/c/i1/r;->b:I

    iget v11, v8, Lf/h/a/c/i1/r;->c:I

    const/4 v15, 0x0

    :goto_5
    if-ge v10, v11, :cond_d

    if-nez v15, :cond_d

    iget-object v15, v8, Lf/h/a/c/i1/r;->a:[B

    add-int/lit8 v16, v10, 0x1

    aget-byte v10, v15, v10

    int-to-char v10, v10

    const/16 v15, 0x29

    if-ne v10, v15, :cond_c

    const/4 v15, 0x1

    goto :goto_6

    :cond_c
    const/4 v15, 0x0

    :goto_6
    move/from16 v10, v16

    goto :goto_5

    :cond_d
    add-int/lit8 v10, v10, -0x1

    iget v11, v8, Lf/h/a/c/i1/r;->b:I

    sub-int/2addr v10, v11

    invoke-virtual {v8, v10}, Lf/h/a/c/i1/r;->n(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    goto :goto_7

    :cond_e
    const/4 v10, 0x0

    :goto_7
    invoke-static {v8, v9}, Lf/h/a/c/e1/s/a;->b(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_f

    :goto_8
    const/4 v10, 0x0

    :cond_f
    :goto_9
    if-eqz v10, :cond_2a

    iget-object v8, v4, Lf/h/a/c/e1/s/a;->a:Lf/h/a/c/i1/r;

    iget-object v9, v4, Lf/h/a/c/e1/s/a;->b:Ljava/lang/StringBuilder;

    invoke-static {v8, v9}, Lf/h/a/c/e1/s/a;->b(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_10

    goto/16 :goto_16

    :cond_10
    new-instance v8, Lf/h/a/c/e1/s/d;

    invoke-direct {v8}, Lf/h/a/c/e1/s/d;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    goto :goto_b

    :cond_11
    const/16 v9, 0x5b

    invoke-virtual {v10, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    if-eq v9, v6, :cond_13

    sget-object v11, Lf/h/a/c/e1/s/a;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-eqz v12, :cond_12

    invoke-virtual {v11, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lf/h/a/c/e1/s/d;->d:Ljava/lang/String;

    :cond_12
    invoke-virtual {v10, v2, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    :cond_13
    const-string v9, "\\."

    invoke-static {v10, v9}, Lf/h/a/c/i1/a0;->z(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    aget-object v10, v9, v2

    const/16 v11, 0x23

    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    if-eq v11, v6, :cond_14

    invoke-virtual {v10, v2, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v8, Lf/h/a/c/e1/s/d;->b:Ljava/lang/String;

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v8, Lf/h/a/c/e1/s/d;->a:Ljava/lang/String;

    goto :goto_a

    :cond_14
    iput-object v10, v8, Lf/h/a/c/e1/s/d;->b:Ljava/lang/String;

    :goto_a
    array-length v10, v9

    if-le v10, v7, :cond_15

    array-length v10, v9

    invoke-static {v9, v7, v10}, Lf/h/a/c/i1/a0;->w([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    iput-object v9, v8, Lf/h/a/c/e1/s/d;->c:Ljava/util/List;

    :cond_15
    :goto_b
    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_c
    const-string/jumbo v11, "}"

    if-nez v9, :cond_28

    iget-object v9, v4, Lf/h/a/c/e1/s/a;->a:Lf/h/a/c/i1/r;

    iget v10, v9, Lf/h/a/c/i1/r;->b:I

    iget-object v12, v4, Lf/h/a/c/e1/s/a;->b:Ljava/lang/StringBuilder;

    invoke-static {v9, v12}, Lf/h/a/c/e1/s/a;->b(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_17

    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_16

    goto :goto_d

    :cond_16
    const/4 v12, 0x0

    goto :goto_e

    :cond_17
    :goto_d
    const/4 v12, 0x1

    :goto_e
    if-nez v12, :cond_25

    iget-object v15, v4, Lf/h/a/c/e1/s/a;->a:Lf/h/a/c/i1/r;

    invoke-virtual {v15, v10}, Lf/h/a/c/i1/r;->C(I)V

    iget-object v10, v4, Lf/h/a/c/e1/s/a;->a:Lf/h/a/c/i1/r;

    iget-object v15, v4, Lf/h/a/c/e1/s/a;->b:Ljava/lang/StringBuilder;

    invoke-static {v10}, Lf/h/a/c/e1/s/a;->c(Lf/h/a/c/i1/r;)V

    invoke-static {v10, v15}, Lf/h/a/c/e1/s/a;->a(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_18

    goto/16 :goto_13

    :cond_18
    invoke-static {v10, v15}, Lf/h/a/c/e1/s/a;->b(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v6

    const-string v14, ":"

    invoke-virtual {v14, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_19

    goto/16 :goto_13

    :cond_19
    invoke-static {v10}, Lf/h/a/c/e1/s/a;->c(Lf/h/a/c/i1/r;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    :goto_f
    const-string v7, ";"

    if-nez v14, :cond_1d

    move-object/from16 v17, v4

    iget v4, v10, Lf/h/a/c/i1/r;->b:I

    move-object/from16 v18, v9

    invoke-static {v10, v15}, Lf/h/a/c/e1/s/a;->b(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_1a

    const/4 v4, 0x0

    goto :goto_11

    :cond_1a
    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1c

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1b

    goto :goto_10

    :cond_1b
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v4, v17

    move-object/from16 v9, v18

    goto :goto_f

    :cond_1c
    :goto_10
    invoke-virtual {v10, v4}, Lf/h/a/c/i1/r;->C(I)V

    move-object/from16 v4, v17

    move-object/from16 v9, v18

    const/4 v14, 0x1

    goto :goto_f

    :cond_1d
    move-object/from16 v17, v4

    move-object/from16 v18, v9

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_11
    if-eqz v4, :cond_26

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    goto/16 :goto_14

    :cond_1e
    iget v6, v10, Lf/h/a/c/i1/r;->b:I

    invoke-static {v10, v15}, Lf/h/a/c/e1/s/a;->b(Lf/h/a/c/i1/r;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1f

    goto :goto_12

    :cond_1f
    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_26

    invoke-virtual {v10, v6}, Lf/h/a/c/i1/r;->C(I)V

    :goto_12
    const-string v6, "color"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_20

    const/4 v6, 0x1

    invoke-static {v4, v6}, Lf/h/a/c/i1/h;->a(Ljava/lang/String;Z)I

    move-result v2

    iput v2, v8, Lf/h/a/c/e1/s/d;->f:I

    iput-boolean v6, v8, Lf/h/a/c/e1/s/d;->g:Z

    goto :goto_15

    :cond_20
    const/4 v6, 0x1

    const-string v7, "background-color"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_21

    invoke-static {v4, v6}, Lf/h/a/c/i1/h;->a(Ljava/lang/String;Z)I

    move-result v2

    iput v2, v8, Lf/h/a/c/e1/s/d;->h:I

    iput-boolean v6, v8, Lf/h/a/c/e1/s/d;->i:Z

    goto :goto_15

    :cond_21
    const-string v7, "text-decoration"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_22

    const-string v2, "underline"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    iput v6, v8, Lf/h/a/c/e1/s/d;->k:I

    goto :goto_15

    :cond_22
    const-string v6, "font-family"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_23

    invoke-static {v4}, Lf/h/a/c/i1/a0;->B(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lf/h/a/c/e1/s/d;->e:Ljava/lang/String;

    goto :goto_14

    :cond_23
    const-string v6, "font-weight"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_24

    const-string v2, "bold"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    const/4 v6, 0x1

    iput v6, v8, Lf/h/a/c/e1/s/d;->l:I

    goto :goto_15

    :cond_24
    const/4 v6, 0x1

    const-string v7, "font-style"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    const-string v2, "italic"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    iput v6, v8, Lf/h/a/c/e1/s/d;->m:I

    goto :goto_15

    :cond_25
    :goto_13
    move-object/from16 v17, v4

    move-object/from16 v18, v9

    :cond_26
    :goto_14
    const/4 v6, 0x1

    :cond_27
    :goto_15
    move v9, v12

    move-object/from16 v4, v17

    move-object/from16 v10, v18

    const/4 v2, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x1

    goto/16 :goto_c

    :cond_28
    move-object/from16 v17, v4

    const/4 v6, 0x1

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_29
    move-object/from16 v4, v17

    const/4 v2, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x1

    goto/16 :goto_4

    :cond_2a
    :goto_16
    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_19

    :cond_2b
    new-instance v0, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;

    const-string v2, "A style block was found after the first cue."

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2c
    if-ne v8, v4, :cond_31

    iget-object v2, v1, Lf/h/a/c/e1/s/g;->n:Lf/h/a/c/e1/s/f;

    iget-object v5, v1, Lf/h/a/c/e1/s/g;->o:Lf/h/a/c/i1/r;

    iget-object v6, v1, Lf/h/a/c/e1/s/g;->p:Lf/h/a/c/e1/s/e$b;

    iget-object v8, v1, Lf/h/a/c/e1/s/g;->r:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2e

    :cond_2d
    :goto_17
    const/4 v2, 0x0

    goto :goto_18

    :cond_2e
    sget-object v4, Lf/h/a/c/e1/s/f;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_2f

    const/4 v3, 0x0

    iget-object v2, v2, Lf/h/a/c/e1/s/f;->a:Ljava/lang/StringBuilder;

    move-object v4, v7

    move-object v7, v2

    invoke-static/range {v3 .. v8}, Lf/h/a/c/e1/s/f;->b(Ljava/lang/String;Ljava/util/regex/Matcher;Lf/h/a/c/i1/r;Lf/h/a/c/e1/s/e$b;Ljava/lang/StringBuilder;Ljava/util/List;)Z

    move-result v2

    goto :goto_18

    :cond_2f
    invoke-virtual {v5}, Lf/h/a/c/i1/r;->f()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_30

    goto :goto_17

    :cond_30
    invoke-virtual {v4, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_2d

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v7, v2, Lf/h/a/c/e1/s/f;->a:Ljava/lang/StringBuilder;

    invoke-static/range {v3 .. v8}, Lf/h/a/c/e1/s/f;->b(Ljava/lang/String;Ljava/util/regex/Matcher;Lf/h/a/c/i1/r;Lf/h/a/c/e1/s/e$b;Ljava/lang/StringBuilder;Ljava/util/List;)Z

    move-result v2

    :goto_18
    if-eqz v2, :cond_31

    iget-object v2, v1, Lf/h/a/c/e1/s/g;->p:Lf/h/a/c/e1/s/e$b;

    invoke-virtual {v2}, Lf/h/a/c/e1/s/e$b;->a()Lf/h/a/c/e1/s/e;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v1, Lf/h/a/c/e1/s/g;->p:Lf/h/a/c/e1/s/e$b;

    invoke-virtual {v2}, Lf/h/a/c/e1/s/e$b;->b()V

    :cond_31
    :goto_19
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_32
    new-instance v2, Lf/h/a/c/e1/s/i;

    invoke-direct {v2, v0}, Lf/h/a/c/e1/s/i;-><init>(Ljava/util/List;)V

    return-object v2

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;

    invoke-direct {v2, v0}, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;-><init>(Ljava/lang/Exception;)V

    throw v2
.end method
