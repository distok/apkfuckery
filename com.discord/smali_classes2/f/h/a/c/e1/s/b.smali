.class public final Lf/h/a/c/e1/s/b;
.super Lf/h/a/c/e1/c;
.source "Mp4WebvttDecoder.java"


# instance fields
.field public final n:Lf/h/a/c/i1/r;

.field public final o:Lf/h/a/c/e1/s/e$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Mp4WebvttDecoder"

    invoke-direct {p0, v0}, Lf/h/a/c/e1/c;-><init>(Ljava/lang/String;)V

    new-instance v0, Lf/h/a/c/i1/r;

    invoke-direct {v0}, Lf/h/a/c/i1/r;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    new-instance v0, Lf/h/a/c/e1/s/e$b;

    invoke-direct {v0}, Lf/h/a/c/e1/s/e$b;-><init>()V

    iput-object v0, p0, Lf/h/a/c/e1/s/b;->o:Lf/h/a/c/e1/s/e$b;

    return-void
.end method


# virtual methods
.method public j([BIZ)Lf/h/a/c/e1/e;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/text/SubtitleDecoderException;
        }
    .end annotation

    iget-object p3, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    iput-object p1, p3, Lf/h/a/c/i1/r;->a:[B

    iput p2, p3, Lf/h/a/c/i1/r;->c:I

    const/4 p1, 0x0

    iput p1, p3, Lf/h/a/c/i1/r;->b:I

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object p2, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->a()I

    move-result p2

    if-lez p2, :cond_6

    iget-object p2, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->a()I

    move-result p2

    const/16 p3, 0x8

    if-lt p2, p3, :cond_5

    iget-object p2, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    invoke-virtual {p2}, Lf/h/a/c/i1/r;->e()I

    move-result p2

    iget-object v0, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v0

    const v1, 0x76747463

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    iget-object v1, p0, Lf/h/a/c/e1/s/b;->o:Lf/h/a/c/e1/s/e$b;

    add-int/lit8 p2, p2, -0x8

    invoke-virtual {v1}, Lf/h/a/c/e1/s/e$b;->b()V

    :cond_0
    :goto_1
    if-lez p2, :cond_3

    if-lt p2, p3, :cond_2

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v2

    invoke-virtual {v0}, Lf/h/a/c/i1/r;->e()I

    move-result v3

    add-int/lit8 p2, p2, -0x8

    sub-int/2addr v2, p3

    iget-object v4, v0, Lf/h/a/c/i1/r;->a:[B

    iget v5, v0, Lf/h/a/c/i1/r;->b:I

    invoke-static {v4, v5, v2}, Lf/h/a/c/i1/a0;->i([BII)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2}, Lf/h/a/c/i1/r;->D(I)V

    sub-int/2addr p2, v2

    const v2, 0x73747467

    if-ne v3, v2, :cond_1

    invoke-static {v4, v1}, Lf/h/a/c/e1/s/f;->c(Ljava/lang/String;Lf/h/a/c/e1/s/e$b;)V

    goto :goto_1

    :cond_1
    const v2, 0x7061796c

    if-ne v3, v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-static {v2, v3, v1, v4}, Lf/h/a/c/e1/s/f;->d(Ljava/lang/String;Ljava/lang/String;Lf/h/a/c/e1/s/e$b;Ljava/util/List;)V

    goto :goto_1

    :cond_2
    new-instance p1, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;

    const-string p2, "Incomplete vtt cue box header found."

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-virtual {v1}, Lf/h/a/c/e1/s/e$b;->a()Lf/h/a/c/e1/s/e;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object p3, p0, Lf/h/a/c/e1/s/b;->n:Lf/h/a/c/i1/r;

    add-int/lit8 p2, p2, -0x8

    invoke-virtual {p3, p2}, Lf/h/a/c/i1/r;->D(I)V

    goto :goto_0

    :cond_5
    new-instance p1, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;

    const-string p2, "Incomplete Mp4Webvtt Top Level box header found."

    invoke-direct {p1, p2}, Lcom/google/android/exoplayer2/text/SubtitleDecoderException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p2, Lf/h/a/c/e1/s/c;

    invoke-direct {p2, p1}, Lf/h/a/c/e1/s/c;-><init>(Ljava/util/List;)V

    return-object p2
.end method
