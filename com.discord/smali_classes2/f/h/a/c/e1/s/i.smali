.class public final Lf/h/a/c/e1/s/i;
.super Ljava/lang/Object;
.source "WebvttSubtitle.java"

# interfaces
.implements Lf/h/a/c/e1/e;


# instance fields
.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/a/c/e1/s/e;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:[J

.field public final g:[J


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/c/e1/s/e;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/e1/s/i;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lf/h/a/c/e1/s/i;->e:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [J

    iput-object v0, p0, Lf/h/a/c/e1/s/i;->f:[J

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lf/h/a/c/e1/s/i;->e:I

    if-ge v0, v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/c/e1/s/e;

    mul-int/lit8 v2, v0, 0x2

    iget-object v3, p0, Lf/h/a/c/e1/s/i;->f:[J

    iget-wide v4, v1, Lf/h/a/c/e1/s/e;->s:J

    aput-wide v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    iget-wide v4, v1, Lf/h/a/c/e1/s/e;->t:J

    aput-wide v4, v3, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/h/a/c/e1/s/i;->f:[J

    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/e1/s/i;->g:[J

    invoke-static {p1}, Ljava/util/Arrays;->sort([J)V

    return-void
.end method


# virtual methods
.method public f(J)I
    .locals 2

    iget-object v0, p0, Lf/h/a/c/e1/s/i;->g:[J

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1, v1}, Lf/h/a/c/i1/a0;->b([JJZZ)I

    move-result p1

    iget-object p2, p0, Lf/h/a/c/e1/s/i;->g:[J

    array-length p2, p2

    if-ge p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public g(I)J
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ltz p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Lf/g/j/k/a;->d(Z)V

    iget-object v2, p0, Lf/h/a/c/e1/s/i;->g:[J

    array-length v2, v2

    if-ge p1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Lf/g/j/k/a;->d(Z)V

    iget-object v0, p0, Lf/h/a/c/e1/s/i;->g:[J

    aget-wide v1, v0, p1

    return-wide v1
.end method

.method public h(J)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v2

    const/4 v4, 0x0

    :goto_0
    iget v5, p0, Lf/h/a/c/e1/s/i;->e:I

    if-ge v4, v5, :cond_5

    iget-object v5, p0, Lf/h/a/c/e1/s/i;->f:[J

    mul-int/lit8 v6, v4, 0x2

    aget-wide v7, v5, v6

    cmp-long v9, v7, p1

    if-gtz v9, :cond_4

    add-int/lit8 v6, v6, 0x1

    aget-wide v6, v5, v6

    cmp-long v5, p1, v6

    if-gez v5, :cond_4

    iget-object v5, p0, Lf/h/a/c/e1/s/i;->d:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/h/a/c/e1/s/e;

    iget v6, v5, Lf/h/a/c/e1/b;->g:F

    const v7, -0x800001

    cmpl-float v6, v6, v7

    if-nez v6, :cond_0

    iget v6, v5, Lf/h/a/c/e1/b;->j:F

    const/high16 v7, 0x3f000000    # 0.5f

    cmpl-float v6, v6, v7

    if-nez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_3

    if-nez v3, :cond_1

    move-object v3, v5

    goto :goto_2

    :cond_1
    const-string v6, "\n"

    if-nez v2, :cond_2

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v7, v3, Lf/h/a/c/e1/b;->d:Ljava/lang/CharSequence;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    iget-object v5, v5, Lf/h/a/c/e1/b;->d:Ljava/lang/CharSequence;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_2
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    iget-object v5, v5, Lf/h/a/c/e1/b;->d:Ljava/lang/CharSequence;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_3
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    if-eqz v2, :cond_6

    new-instance p1, Lf/h/a/c/e1/s/e$b;

    invoke-direct {p1}, Lf/h/a/c/e1/s/e$b;-><init>()V

    iput-object v2, p1, Lf/h/a/c/e1/s/e$b;->c:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lf/h/a/c/e1/s/e$b;->a()Lf/h/a/c/e1/s/e;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_3
    return-object v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lf/h/a/c/e1/s/i;->g:[J

    array-length v0, v0

    return v0
.end method
