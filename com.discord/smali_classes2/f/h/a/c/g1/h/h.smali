.class public final Lf/h/a/c/g1/h/h;
.super Landroid/opengl/GLSurfaceView;
.source "SphericalGLSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/a/c/g1/h/h$a;
    }
.end annotation


# instance fields
.field public final d:Landroid/hardware/SensorManager;

.field public final e:Landroid/hardware/Sensor;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lf/h/a/c/g1/h/d;

.field public final g:Landroid/os/Handler;

.field public final h:Lf/h/a/c/g1/h/i;

.field public final i:Lf/h/a/c/g1/h/f;

.field public j:Landroid/graphics/SurfaceTexture;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public k:Landroid/view/Surface;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public l:Lf/h/a/c/m0$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lf/h/a/c/g1/h/h;->g:Landroid/os/Handler;

    const-string v1, "sensor"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lf/h/a/c/g1/h/h;->d:Landroid/hardware/SensorManager;

    sget v2, Lf/h/a/c/i1/a0;->a:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_0

    const/16 v0, 0xf

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    :cond_1
    iput-object v0, p0, Lf/h/a/c/g1/h/h;->e:Landroid/hardware/Sensor;

    new-instance v0, Lf/h/a/c/g1/h/f;

    invoke-direct {v0}, Lf/h/a/c/g1/h/f;-><init>()V

    iput-object v0, p0, Lf/h/a/c/g1/h/h;->i:Lf/h/a/c/g1/h/f;

    new-instance v1, Lf/h/a/c/g1/h/h$a;

    invoke-direct {v1, p0, v0}, Lf/h/a/c/g1/h/h$a;-><init>(Lf/h/a/c/g1/h/h;Lf/h/a/c/g1/h/f;)V

    new-instance v0, Lf/h/a/c/g1/h/i;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-direct {v0, p1, v1, v2}, Lf/h/a/c/g1/h/i;-><init>(Landroid/content/Context;Lf/h/a/c/g1/h/i$a;F)V

    iput-object v0, p0, Lf/h/a/c/g1/h/h;->h:Lf/h/a/c/g1/h/i;

    const-string/jumbo v2, "window"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/WindowManager;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    new-instance v2, Lf/h/a/c/g1/h/d;

    const/4 v3, 0x2

    new-array v4, v3, [Lf/h/a/c/g1/h/d$a;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-direct {v2, p1, v4}, Lf/h/a/c/g1/h/d;-><init>(Landroid/view/Display;[Lf/h/a/c/g1/h/d$a;)V

    iput-object v2, p0, Lf/h/a/c/g1/h/h;->f:Lf/h/a/c/g1/h/d;

    invoke-virtual {p0, v3}, Landroid/opengl/GLSurfaceView;->setEGLContextClientVersion(I)V

    invoke-virtual {p0, v1}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onDetachedFromWindow()V

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->g:Landroid/os/Handler;

    new-instance v1, Lf/h/a/c/g1/h/b;

    invoke-direct {v1, p0}, Lf/h/a/c/g1/h/b;-><init>(Lf/h/a/c/g1/h/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->e:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->d:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lf/h/a/c/g1/h/h;->f:Lf/h/a/c/g1/h/d;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onResume()V

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->e:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/h/a/c/g1/h/h;->d:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lf/h/a/c/g1/h/h;->f:Lf/h/a/c/g1/h/d;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_0
    return-void
.end method

.method public setDefaultStereoMode(I)V
    .locals 1

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->i:Lf/h/a/c/g1/h/f;

    iput p1, v0, Lf/h/a/c/g1/h/f;->k:I

    return-void
.end method

.method public setSingleTapListener(Lf/h/a/c/g1/h/g;)V
    .locals 1
    .param p1    # Lf/h/a/c/g1/h/g;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->h:Lf/h/a/c/g1/h/i;

    iput-object p1, v0, Lf/h/a/c/g1/h/i;->j:Lf/h/a/c/g1/h/g;

    return-void
.end method

.method public setVideoComponent(Lf/h/a/c/m0$c;)V
    .locals 12
    .param p1    # Lf/h/a/c/m0$c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x7

    const/4 v2, 0x5

    const/4 v3, 0x6

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eqz v0, :cond_7

    iget-object v6, p0, Lf/h/a/c/g1/h/h;->k:Landroid/view/Surface;

    if-eqz v6, :cond_1

    check-cast v0, Lf/h/a/c/s0;

    invoke-virtual {v0, v6}, Lf/h/a/c/s0;->I(Landroid/view/Surface;)V

    :cond_1
    iget-object v0, p0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    iget-object v6, p0, Lf/h/a/c/g1/h/h;->i:Lf/h/a/c/g1/h/f;

    check-cast v0, Lf/h/a/c/s0;

    invoke-virtual {v0}, Lf/h/a/c/s0;->S()V

    iget-object v7, v0, Lf/h/a/c/s0;->A:Lf/h/a/c/j1/n;

    const/4 v8, 0x0

    if-eq v7, v6, :cond_2

    goto :goto_1

    :cond_2
    iget-object v6, v0, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v7, v6

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v7, :cond_4

    aget-object v10, v6, v9

    invoke-interface {v10}, Lf/h/a/c/p0;->u()I

    move-result v11

    if-ne v11, v4, :cond_3

    iget-object v11, v0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v11, v10}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v10

    invoke-virtual {v10, v3}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    invoke-virtual {v10, v8}, Lf/h/a/c/n0;->d(Ljava/lang/Object;)Lf/h/a/c/n0;

    invoke-virtual {v10}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    iget-object v0, p0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    iget-object v6, p0, Lf/h/a/c/g1/h/h;->i:Lf/h/a/c/g1/h/f;

    check-cast v0, Lf/h/a/c/s0;

    invoke-virtual {v0}, Lf/h/a/c/s0;->S()V

    iget-object v7, v0, Lf/h/a/c/s0;->B:Lf/h/a/c/j1/s/a;

    if-eq v7, v6, :cond_5

    goto :goto_3

    :cond_5
    iget-object v6, v0, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v7, v6

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v7, :cond_7

    aget-object v10, v6, v9

    invoke-interface {v10}, Lf/h/a/c/p0;->u()I

    move-result v11

    if-ne v11, v2, :cond_6

    iget-object v11, v0, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v11, v10}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v10

    invoke-virtual {v10, v1}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    invoke-virtual {v10, v8}, Lf/h/a/c/n0;->d(Ljava/lang/Object;)Lf/h/a/c/n0;

    invoke-virtual {v10}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_7
    :goto_3
    iput-object p1, p0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    if-eqz p1, :cond_c

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->i:Lf/h/a/c/g1/h/f;

    check-cast p1, Lf/h/a/c/s0;

    invoke-virtual {p1}, Lf/h/a/c/s0;->S()V

    iput-object v0, p1, Lf/h/a/c/s0;->A:Lf/h/a/c/j1/n;

    iget-object v6, p1, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v7, v6

    const/4 v8, 0x0

    :goto_4
    if-ge v8, v7, :cond_9

    aget-object v9, v6, v8

    invoke-interface {v9}, Lf/h/a/c/p0;->u()I

    move-result v10

    if-ne v10, v4, :cond_8

    iget-object v10, p1, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v10, v9}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v9

    invoke-virtual {v9, v3}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    iget-boolean v10, v9, Lf/h/a/c/n0;->h:Z

    xor-int/lit8 v10, v10, 0x1

    invoke-static {v10}, Lf/g/j/k/a;->s(Z)V

    iput-object v0, v9, Lf/h/a/c/n0;->e:Ljava/lang/Object;

    invoke-virtual {v9}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_9
    iget-object p1, p0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->i:Lf/h/a/c/g1/h/f;

    check-cast p1, Lf/h/a/c/s0;

    invoke-virtual {p1}, Lf/h/a/c/s0;->S()V

    iput-object v0, p1, Lf/h/a/c/s0;->B:Lf/h/a/c/j1/s/a;

    iget-object v3, p1, Lf/h/a/c/s0;->b:[Lf/h/a/c/p0;

    array-length v4, v3

    :goto_5
    if-ge v5, v4, :cond_b

    aget-object v6, v3, v5

    invoke-interface {v6}, Lf/h/a/c/p0;->u()I

    move-result v7

    if-ne v7, v2, :cond_a

    iget-object v7, p1, Lf/h/a/c/s0;->c:Lf/h/a/c/a0;

    invoke-virtual {v7, v6}, Lf/h/a/c/a0;->a(Lf/h/a/c/n0$b;)Lf/h/a/c/n0;

    move-result-object v6

    invoke-virtual {v6, v1}, Lf/h/a/c/n0;->e(I)Lf/h/a/c/n0;

    iget-boolean v7, v6, Lf/h/a/c/n0;->h:Z

    xor-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Lf/g/j/k/a;->s(Z)V

    iput-object v0, v6, Lf/h/a/c/n0;->e:Ljava/lang/Object;

    invoke-virtual {v6}, Lf/h/a/c/n0;->c()Lf/h/a/c/n0;

    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_b
    iget-object p1, p0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    iget-object v0, p0, Lf/h/a/c/g1/h/h;->k:Landroid/view/Surface;

    check-cast p1, Lf/h/a/c/s0;

    invoke-virtual {p1, v0}, Lf/h/a/c/s0;->N(Landroid/view/Surface;)V

    :cond_c
    return-void
.end method
