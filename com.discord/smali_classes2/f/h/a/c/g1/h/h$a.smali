.class public Lf/h/a/c/g1/h/h$a;
.super Ljava/lang/Object;
.source "SphericalGLSurfaceView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Lf/h/a/c/g1/h/i$a;
.implements Lf/h/a/c/g1/h/d$a;


# annotations
.annotation build Landroidx/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/g1/h/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final d:Lf/h/a/c/g1/h/f;

.field public final e:[F

.field public final f:[F

.field public final g:[F

.field public final h:[F

.field public final i:[F

.field public j:F

.field public k:F

.field public final l:[F

.field public final m:[F

.field public final synthetic n:Lf/h/a/c/g1/h/h;


# direct methods
.method public constructor <init>(Lf/h/a/c/g1/h/h;Lf/h/a/c/g1/h/f;)V
    .locals 4

    iput-object p1, p0, Lf/h/a/c/g1/h/h$a;->n:Lf/h/a/c/g1/h/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 p1, 0x10

    new-array v0, p1, [F

    iput-object v0, p0, Lf/h/a/c/g1/h/h$a;->e:[F

    new-array v0, p1, [F

    iput-object v0, p0, Lf/h/a/c/g1/h/h$a;->f:[F

    new-array v0, p1, [F

    iput-object v0, p0, Lf/h/a/c/g1/h/h$a;->g:[F

    new-array v1, p1, [F

    iput-object v1, p0, Lf/h/a/c/g1/h/h$a;->h:[F

    new-array v2, p1, [F

    iput-object v2, p0, Lf/h/a/c/g1/h/h$a;->i:[F

    new-array v3, p1, [F

    iput-object v3, p0, Lf/h/a/c/g1/h/h$a;->l:[F

    new-array p1, p1, [F

    iput-object p1, p0, Lf/h/a/c/g1/h/h$a;->m:[F

    iput-object p2, p0, Lf/h/a/c/g1/h/h$a;->d:Lf/h/a/c/g1/h/f;

    const/4 p1, 0x0

    invoke-static {v0, p1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    invoke-static {v1, p1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    invoke-static {v2, p1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    const p1, 0x40490fdb    # (float)Math.PI

    iput p1, p0, Lf/h/a/c/g1/h/h$a;->k:F

    return-void
.end method


# virtual methods
.method public declared-synchronized a([FF)V
    .locals 3
    .annotation build Landroidx/annotation/BinderThread;
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/a/c/g1/h/h$a;->g:[F

    array-length v1, v0

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    neg-float p1, p2

    iput p1, p0, Lf/h/a/c/g1/h/h$a;->k:F

    invoke-virtual {p0}, Lf/h/a/c/g1/h/h$a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b()V
    .locals 6
    .annotation build Landroidx/annotation/AnyThread;
    .end annotation

    iget-object v0, p0, Lf/h/a/c/g1/h/h$a;->h:[F

    iget v1, p0, Lf/h/a/c/g1/h/h$a;->j:F

    neg-float v2, v1

    iget v1, p0, Lf/h/a/c/g1/h/h$a;->k:F

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    double-to-float v3, v3

    iget v1, p0, Lf/h/a/c/g1/h/h$a;->k:F

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 22

    move-object/from16 v1, p0

    monitor-enter p0

    :try_start_0
    iget-object v2, v1, Lf/h/a/c/g1/h/h$a;->m:[F

    const/4 v3, 0x0

    iget-object v4, v1, Lf/h/a/c/g1/h/h$a;->g:[F

    const/4 v5, 0x0

    iget-object v6, v1, Lf/h/a/c/g1/h/h$a;->i:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v8, v1, Lf/h/a/c/g1/h/h$a;->l:[F

    const/4 v9, 0x0

    iget-object v10, v1, Lf/h/a/c/g1/h/h$a;->h:[F

    const/4 v11, 0x0

    iget-object v12, v1, Lf/h/a/c/g1/h/h$a;->m:[F

    const/4 v13, 0x0

    invoke-static/range {v8 .. v13}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v2, v1, Lf/h/a/c/g1/h/h$a;->f:[F

    const/4 v3, 0x0

    iget-object v4, v1, Lf/h/a/c/g1/h/h$a;->e:[F

    const/4 v5, 0x0

    iget-object v6, v1, Lf/h/a/c/g1/h/h$a;->l:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v0, v1, Lf/h/a/c/g1/h/h$a;->d:Lf/h/a/c/g1/h/f;

    iget-object v4, v1, Lf/h/a/c/g1/h/h$a;->f:[F

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0x4000

    invoke-static {v2}, Landroid/opengl/GLES20;->glClear(I)V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget-object v2, v0, Lf/h/a/c/g1/h/f;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    const/4 v10, 0x2

    if-eqz v2, :cond_7

    iget-object v2, v0, Lf/h/a/c/g1/h/f;->j:Landroid/graphics/SurfaceTexture;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget-object v2, v0, Lf/h/a/c/g1/h/f;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lf/h/a/c/g1/h/f;->g:[F

    invoke-static {v2, v9}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    :cond_0
    iget-object v2, v0, Lf/h/a/c/g1/h/f;->j:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v2

    iget-object v5, v0, Lf/h/a/c/g1/h/f;->e:Lf/h/a/c/i1/y;

    monitor-enter v5

    :try_start_1
    invoke-virtual {v5, v2, v3, v9}, Lf/h/a/c/i1/y;->d(JZ)Ljava/lang/Object;

    move-result-object v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v5

    check-cast v6, Ljava/lang/Long;

    if-eqz v6, :cond_4

    iget-object v5, v0, Lf/h/a/c/g1/h/f;->d:Lf/h/a/c/j1/s/c;

    iget-object v11, v0, Lf/h/a/c/g1/h/f;->g:[F

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v12, v5, Lf/h/a/c/j1/s/c;->c:Lf/h/a/c/i1/y;

    invoke-virtual {v12, v6, v7}, Lf/h/a/c/i1/y;->e(J)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [F

    if-nez v6, :cond_1

    goto :goto_1

    :cond_1
    iget-object v12, v5, Lf/h/a/c/j1/s/c;->b:[F

    aget v7, v6, v9

    aget v13, v6, v8

    neg-float v13, v13

    aget v6, v6, v10

    neg-float v6, v6

    invoke-static {v7, v13, v6}, Landroid/opengl/Matrix;->length(FFF)F

    move-result v14

    const/4 v15, 0x0

    cmpl-float v15, v14, v15

    if-eqz v15, :cond_2

    move-object/from16 v18, v11

    float-to-double v10, v14

    invoke-static {v10, v11}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v10

    double-to-float v10, v10

    const/4 v11, 0x0

    div-float v15, v7, v14

    div-float v16, v13, v14

    div-float v17, v6, v14

    move v13, v11

    move v14, v10

    invoke-static/range {v12 .. v17}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    goto :goto_0

    :cond_2
    move-object/from16 v18, v11

    invoke-static {v12, v9}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    :goto_0
    iget-boolean v6, v5, Lf/h/a/c/j1/s/c;->d:Z

    if-nez v6, :cond_3

    iget-object v6, v5, Lf/h/a/c/j1/s/c;->a:[F

    iget-object v7, v5, Lf/h/a/c/j1/s/c;->b:[F

    invoke-static {v6, v7}, Lf/h/a/c/j1/s/c;->a([F[F)V

    iput-boolean v8, v5, Lf/h/a/c/j1/s/c;->d:Z

    :cond_3
    const/4 v12, 0x0

    iget-object v13, v5, Lf/h/a/c/j1/s/c;->a:[F

    const/4 v14, 0x0

    iget-object v15, v5, Lf/h/a/c/j1/s/c;->b:[F

    const/16 v16, 0x0

    move-object/from16 v11, v18

    invoke-static/range {v11 .. v16}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    :cond_4
    :goto_1
    iget-object v5, v0, Lf/h/a/c/g1/h/f;->f:Lf/h/a/c/i1/y;

    invoke-virtual {v5, v2, v3}, Lf/h/a/c/i1/y;->e(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/a/c/j1/s/d;

    if-eqz v2, :cond_7

    iget-object v3, v0, Lf/h/a/c/g1/h/f;->c:Lf/h/a/c/g1/h/e;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lf/h/a/c/g1/h/e;->a(Lf/h/a/c/j1/s/d;)Z

    move-result v5

    if-nez v5, :cond_5

    goto :goto_3

    :cond_5
    iget v5, v2, Lf/h/a/c/j1/s/d;->c:I

    iput v5, v3, Lf/h/a/c/g1/h/e;->a:I

    new-instance v5, Lf/h/a/c/g1/h/e$a;

    iget-object v6, v2, Lf/h/a/c/j1/s/d;->a:Lf/h/a/c/j1/s/d$a;

    iget-object v6, v6, Lf/h/a/c/j1/s/d$a;->a:[Lf/h/a/c/j1/s/d$b;

    aget-object v6, v6, v9

    invoke-direct {v5, v6}, Lf/h/a/c/g1/h/e$a;-><init>(Lf/h/a/c/j1/s/d$b;)V

    iput-object v5, v3, Lf/h/a/c/g1/h/e;->b:Lf/h/a/c/g1/h/e$a;

    iget-boolean v6, v2, Lf/h/a/c/j1/s/d;->d:Z

    if-eqz v6, :cond_6

    goto :goto_2

    :cond_6
    new-instance v5, Lf/h/a/c/g1/h/e$a;

    iget-object v2, v2, Lf/h/a/c/j1/s/d;->b:Lf/h/a/c/j1/s/d$a;

    iget-object v2, v2, Lf/h/a/c/j1/s/d$a;->a:[Lf/h/a/c/j1/s/d$b;

    aget-object v2, v2, v9

    invoke-direct {v5, v2}, Lf/h/a/c/g1/h/e$a;-><init>(Lf/h/a/c/j1/s/d$b;)V

    :goto_2
    iput-object v5, v3, Lf/h/a/c/g1/h/e;->c:Lf/h/a/c/g1/h/e$a;

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v2, v0

    monitor-exit v5

    throw v2

    :cond_7
    :goto_3
    iget-object v2, v0, Lf/h/a/c/g1/h/f;->h:[F

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v6, v0, Lf/h/a/c/g1/h/f;->g:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v2, v0, Lf/h/a/c/g1/h/f;->c:Lf/h/a/c/g1/h/e;

    iget v3, v0, Lf/h/a/c/g1/h/f;->i:I

    iget-object v0, v0, Lf/h/a/c/g1/h/f;->h:[F

    iget-object v4, v2, Lf/h/a/c/g1/h/e;->b:Lf/h/a/c/g1/h/e$a;

    if-nez v4, :cond_8

    goto/16 :goto_5

    :cond_8
    iget v5, v2, Lf/h/a/c/g1/h/e;->d:I

    invoke-static {v5}, Landroid/opengl/GLES20;->glUseProgram(I)V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget v5, v2, Lf/h/a/c/g1/h/e;->g:I

    invoke-static {v5}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    iget v5, v2, Lf/h/a/c/g1/h/e;->h:I

    invoke-static {v5}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget v5, v2, Lf/h/a/c/g1/h/e;->a:I

    if-ne v5, v8, :cond_9

    sget-object v5, Lf/h/a/c/g1/h/e;->m:[F

    goto :goto_4

    :cond_9
    const/4 v6, 0x2

    if-ne v5, v6, :cond_a

    sget-object v5, Lf/h/a/c/g1/h/e;->o:[F

    goto :goto_4

    :cond_a
    sget-object v5, Lf/h/a/c/g1/h/e;->l:[F

    :goto_4
    iget v6, v2, Lf/h/a/c/g1/h/e;->f:I

    invoke-static {v6, v8, v9, v5, v9}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    iget v5, v2, Lf/h/a/c/g1/h/e;->e:I

    invoke-static {v5, v8, v9, v0, v9}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    const v0, 0x8d65

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    iget v0, v2, Lf/h/a/c/g1/h/e;->i:I

    invoke-static {v0, v9}, Landroid/opengl/GLES20;->glUniform1i(II)V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget v10, v2, Lf/h/a/c/g1/h/e;->g:I

    const/4 v11, 0x3

    const/16 v12, 0x1406

    const/4 v13, 0x0

    const/16 v14, 0xc

    iget-object v15, v4, Lf/h/a/c/g1/h/e$a;->b:Ljava/nio/FloatBuffer;

    invoke-static/range {v10 .. v15}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget v0, v2, Lf/h/a/c/g1/h/e;->h:I

    const/16 v17, 0x2

    const/16 v18, 0x1406

    const/16 v19, 0x0

    const/16 v20, 0x8

    iget-object v3, v4, Lf/h/a/c/g1/h/e$a;->c:Ljava/nio/FloatBuffer;

    move/from16 v16, v0

    move-object/from16 v21, v3

    invoke-static/range {v16 .. v21}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget v0, v4, Lf/h/a/c/g1/h/e$a;->d:I

    iget v3, v4, Lf/h/a/c/g1/h/e$a;->a:I

    invoke-static {v0, v9, v3}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    invoke-static {}, Lf/g/j/k/a;->j()V

    iget v0, v2, Lf/h/a/c/g1/h/e;->g:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    iget v0, v2, Lf/h/a/c/g1/h/e;->h:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :goto_5
    return-void

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 6

    const/4 p1, 0x0

    invoke-static {p1, p1, p2, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    int-to-float p2, p2

    int-to-float p3, p3

    div-float v3, p2, p3

    const/high16 p2, 0x3f800000    # 1.0f

    cmpl-float p2, v3, p2

    if-lez p2, :cond_0

    const/4 p1, 0x1

    :cond_0
    if-eqz p1, :cond_1

    const-wide p1, 0x4046800000000000L    # 45.0

    invoke-static {p1, p2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Math;->tan(D)D

    move-result-wide p1

    float-to-double v0, v3

    div-double/2addr p1, v0

    invoke-static {p1, p2}, Ljava/lang/Math;->atan(D)D

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide p1

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    mul-double p1, p1, v0

    double-to-float p1, p1

    move v2, p1

    goto :goto_0

    :cond_1
    const/high16 p1, 0x42b40000    # 90.0f

    const/high16 v2, 0x42b40000    # 90.0f

    :goto_0
    iget-object v0, p0, Lf/h/a/c/g1/h/h$a;->e:[F

    const/4 v1, 0x0

    const v4, 0x3dcccccd    # 0.1f

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->perspectiveM([FIFFFF)V

    return-void
.end method

.method public declared-synchronized onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object p1, p0, Lf/h/a/c/g1/h/h$a;->n:Lf/h/a/c/g1/h/h;

    iget-object p2, p0, Lf/h/a/c/g1/h/h$a;->d:Lf/h/a/c/g1/h/f;

    invoke-virtual {p2}, Lf/h/a/c/g1/h/f;->d()Landroid/graphics/SurfaceTexture;

    move-result-object p2

    iget-object v0, p1, Lf/h/a/c/g1/h/h;->g:Landroid/os/Handler;

    new-instance v1, Lf/h/a/c/g1/h/c;

    invoke-direct {v1, p1, p2}, Lf/h/a/c/g1/h/c;-><init>(Lf/h/a/c/g1/h/h;Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
