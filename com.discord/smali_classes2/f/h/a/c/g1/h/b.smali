.class public final synthetic Lf/h/a/c/g1/h/b;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/c/g1/h/h;


# direct methods
.method public synthetic constructor <init>(Lf/h/a/c/g1/h/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/g1/h/b;->d:Lf/h/a/c/g1/h/h;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/h/a/c/g1/h/b;->d:Lf/h/a/c/g1/h/h;

    iget-object v1, v0, Lf/h/a/c/g1/h/h;->k:Landroid/view/Surface;

    if-eqz v1, :cond_3

    iget-object v2, v0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    if-eqz v2, :cond_0

    check-cast v2, Lf/h/a/c/s0;

    invoke-virtual {v2, v1}, Lf/h/a/c/s0;->I(Landroid/view/Surface;)V

    :cond_0
    iget-object v1, v0, Lf/h/a/c/g1/h/h;->j:Landroid/graphics/SurfaceTexture;

    iget-object v2, v0, Lf/h/a/c/g1/h/h;->k:Landroid/view/Surface;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->release()V

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/Surface;->release()V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, Lf/h/a/c/g1/h/h;->j:Landroid/graphics/SurfaceTexture;

    iput-object v1, v0, Lf/h/a/c/g1/h/h;->k:Landroid/view/Surface;

    :cond_3
    return-void
.end method
