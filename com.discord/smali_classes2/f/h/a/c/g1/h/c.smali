.class public final synthetic Lf/h/a/c/g1/h/c;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/h/a/c/g1/h/h;

.field public final synthetic e:Landroid/graphics/SurfaceTexture;


# direct methods
.method public synthetic constructor <init>(Lf/h/a/c/g1/h/h;Landroid/graphics/SurfaceTexture;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/g1/h/c;->d:Lf/h/a/c/g1/h/h;

    iput-object p2, p0, Lf/h/a/c/g1/h/c;->e:Landroid/graphics/SurfaceTexture;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lf/h/a/c/g1/h/c;->d:Lf/h/a/c/g1/h/h;

    iget-object v1, p0, Lf/h/a/c/g1/h/c;->e:Landroid/graphics/SurfaceTexture;

    iget-object v2, v0, Lf/h/a/c/g1/h/h;->j:Landroid/graphics/SurfaceTexture;

    iget-object v3, v0, Lf/h/a/c/g1/h/h;->k:Landroid/view/Surface;

    iput-object v1, v0, Lf/h/a/c/g1/h/h;->j:Landroid/graphics/SurfaceTexture;

    new-instance v4, Landroid/view/Surface;

    invoke-direct {v4, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v4, v0, Lf/h/a/c/g1/h/h;->k:Landroid/view/Surface;

    iget-object v0, v0, Lf/h/a/c/g1/h/h;->l:Lf/h/a/c/m0$c;

    if-eqz v0, :cond_0

    check-cast v0, Lf/h/a/c/s0;

    invoke-virtual {v0, v4}, Lf/h/a/c/s0;->N(Landroid/view/Surface;)V

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->release()V

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/Surface;->release()V

    :cond_2
    return-void
.end method
