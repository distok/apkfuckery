.class public final Lf/h/a/c/c1/g/a;
.super Ljava/lang/Object;
.source "EventMessageDecoder.java"

# interfaces
.implements Lf/h/a/c/c1/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/c1/d;)Lcom/google/android/exoplayer2/metadata/Metadata;
    .locals 13

    iget-object p1, p1, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result p1

    new-instance v1, Lcom/google/android/exoplayer2/metadata/Metadata;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    const/4 v3, 0x0

    new-instance v4, Lf/h/a/c/i1/r;

    invoke-direct {v4, v0, p1}, Lf/h/a/c/i1/r;-><init>([BI)V

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->l()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->l()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v8

    invoke-virtual {v4}, Lf/h/a/c/i1/r;->r()J

    move-result-wide v10

    iget-object p1, v4, Lf/h/a/c/i1/r;->a:[B

    iget v0, v4, Lf/h/a/c/i1/r;->b:I

    iget v4, v4, Lf/h/a/c/i1/r;->c:I

    invoke-static {p1, v0, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v12

    new-instance p1, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;

    move-object v5, p1

    invoke-direct/range {v5 .. v12}, Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;-><init>(Ljava/lang/String;Ljava/lang/String;JJ[B)V

    aput-object p1, v2, v3

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>([Lcom/google/android/exoplayer2/metadata/Metadata$Entry;)V

    return-object v1
.end method
