.class public final Lf/h/a/c/c1/f;
.super Lf/h/a/c/t;
.source "MetadataRenderer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public final o:Lf/h/a/c/c1/c;

.field public final p:Lf/h/a/c/c1/e;

.field public final q:Landroid/os/Handler;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final r:Lf/h/a/c/c1/d;

.field public final s:[Lcom/google/android/exoplayer2/metadata/Metadata;

.field public final t:[J

.field public u:I

.field public v:I

.field public w:Lf/h/a/c/c1/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field public y:J


# direct methods
.method public constructor <init>(Lf/h/a/c/c1/e;Landroid/os/Looper;)V
    .locals 2
    .param p2    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lf/h/a/c/c1/c;->a:Lf/h/a/c/c1/c;

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lf/h/a/c/t;-><init>(I)V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/h/a/c/c1/f;->p:Lf/h/a/c/c1/e;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    sget p1, Lf/h/a/c/i1/a0;->a:I

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1, p2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    :goto_0
    iput-object p1, p0, Lf/h/a/c/c1/f;->q:Landroid/os/Handler;

    iput-object v0, p0, Lf/h/a/c/c1/f;->o:Lf/h/a/c/c1/c;

    new-instance p1, Lf/h/a/c/c1/d;

    invoke-direct {p1}, Lf/h/a/c/c1/d;-><init>()V

    iput-object p1, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    const/4 p1, 0x5

    new-array p2, p1, [Lcom/google/android/exoplayer2/metadata/Metadata;

    iput-object p2, p0, Lf/h/a/c/c1/f;->s:[Lcom/google/android/exoplayer2/metadata/Metadata;

    new-array p1, p1, [J

    iput-object p1, p0, Lf/h/a/c/c1/f;->t:[J

    return-void
.end method


# virtual methods
.method public A(JZ)V
    .locals 0

    iget-object p1, p0, Lf/h/a/c/c1/f;->s:[Lcom/google/android/exoplayer2/metadata/Metadata;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p1, 0x0

    iput p1, p0, Lf/h/a/c/c1/f;->u:I

    iput p1, p0, Lf/h/a/c/c1/f;->v:I

    iput-boolean p1, p0, Lf/h/a/c/c1/f;->x:Z

    return-void
.end method

.method public E([Lcom/google/android/exoplayer2/Format;J)V
    .locals 0

    iget-object p2, p0, Lf/h/a/c/c1/f;->o:Lf/h/a/c/c1/c;

    const/4 p3, 0x0

    aget-object p1, p1, p3

    invoke-interface {p2, p1}, Lf/h/a/c/c1/c;->b(Lcom/google/android/exoplayer2/Format;)Lf/h/a/c/c1/b;

    move-result-object p1

    iput-object p1, p0, Lf/h/a/c/c1/f;->w:Lf/h/a/c/c1/b;

    return-void
.end method

.method public G(Lcom/google/android/exoplayer2/Format;)I
    .locals 2

    iget-object v0, p0, Lf/h/a/c/c1/f;->o:Lf/h/a/c/c1/c;

    invoke-interface {v0, p1}, Lf/h/a/c/c1/c;->a(Lcom/google/android/exoplayer2/Format;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object p1, p1, Lcom/google/android/exoplayer2/Format;->o:Lcom/google/android/exoplayer2/drm/DrmInitData;

    invoke-static {v0, p1}, Lf/h/a/c/t;->H(Lf/h/a/c/z0/b;Lcom/google/android/exoplayer2/drm/DrmInitData;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    or-int/2addr p1, v1

    or-int/2addr p1, v1

    return p1

    :cond_1
    return v1
.end method

.method public final J(Lcom/google/android/exoplayer2/metadata/Metadata;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/metadata/Metadata;",
            "Ljava/util/List<",
            "Lcom/google/android/exoplayer2/metadata/Metadata$Entry;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lcom/google/android/exoplayer2/metadata/Metadata;->d:[Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    array-length v2, v1

    if-ge v0, v2, :cond_2

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/exoplayer2/metadata/Metadata$Entry;->J()Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lf/h/a/c/c1/f;->o:Lf/h/a/c/c1/c;

    invoke-interface {v2, v1}, Lf/h/a/c/c1/c;->a(Lcom/google/android/exoplayer2/Format;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lf/h/a/c/c1/f;->o:Lf/h/a/c/c1/c;

    invoke-interface {v2, v1}, Lf/h/a/c/c1/c;->b(Lcom/google/android/exoplayer2/Format;)Lf/h/a/c/c1/b;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/exoplayer2/metadata/Metadata;->d:[Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/exoplayer2/metadata/Metadata$Entry;->D0()[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-virtual {v3}, Lf/h/a/c/y0/e;->clear()V

    iget-object v3, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    array-length v4, v2

    invoke-virtual {v3, v4}, Lf/h/a/c/y0/e;->k(I)V

    iget-object v3, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    iget-object v3, v3, Lf/h/a/c/y0/e;->e:Ljava/nio/ByteBuffer;

    sget v4, Lf/h/a/c/i1/a0;->a:I

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-virtual {v2}, Lf/h/a/c/y0/e;->l()V

    iget-object v2, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-interface {v1, v2}, Lf/h/a/c/c1/b;->a(Lf/h/a/c/c1/d;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1, p2}, Lf/h/a/c/c1/f;->J(Lcom/google/android/exoplayer2/metadata/Metadata;Ljava/util/List;)V

    goto :goto_1

    :cond_0
    iget-object v1, p1, Lcom/google/android/exoplayer2/metadata/Metadata;->d:[Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    aget-object v1, v1, v0

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lf/h/a/c/c1/f;->x:Z

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/google/android/exoplayer2/metadata/Metadata;

    iget-object v0, p0, Lf/h/a/c/c1/f;->p:Lf/h/a/c/c1/e;

    invoke-interface {v0, p1}, Lf/h/a/c/c1/e;->q(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public n(JJ)V
    .locals 6

    iget-boolean p3, p0, Lf/h/a/c/c1/f;->x:Z

    const/4 p4, 0x0

    const/4 v0, 0x5

    const/4 v1, 0x1

    if-nez p3, :cond_3

    iget p3, p0, Lf/h/a/c/c1/f;->v:I

    if-ge p3, v0, :cond_3

    iget-object p3, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-virtual {p3}, Lf/h/a/c/y0/e;->clear()V

    invoke-virtual {p0}, Lf/h/a/c/t;->x()Lf/h/a/c/d0;

    move-result-object p3

    iget-object v2, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-virtual {p0, p3, v2, p4}, Lf/h/a/c/t;->F(Lf/h/a/c/d0;Lf/h/a/c/y0/e;Z)I

    move-result v2

    const/4 v3, -0x4

    if-ne v2, v3, :cond_2

    iget-object p3, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-virtual {p3}, Lf/h/a/c/y0/a;->isEndOfStream()Z

    move-result p3

    if-eqz p3, :cond_0

    iput-boolean v1, p0, Lf/h/a/c/c1/f;->x:Z

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-virtual {p3}, Lf/h/a/c/y0/a;->isDecodeOnly()Z

    move-result p3

    if-eqz p3, :cond_1

    goto :goto_0

    :cond_1
    iget-object p3, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    iget-wide v2, p0, Lf/h/a/c/c1/f;->y:J

    iput-wide v2, p3, Lf/h/a/c/c1/d;->i:J

    invoke-virtual {p3}, Lf/h/a/c/y0/e;->l()V

    iget-object p3, p0, Lf/h/a/c/c1/f;->w:Lf/h/a/c/c1/b;

    sget v2, Lf/h/a/c/i1/a0;->a:I

    iget-object v2, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    invoke-interface {p3, v2}, Lf/h/a/c/c1/b;->a(Lf/h/a/c/c1/d;)Lcom/google/android/exoplayer2/metadata/Metadata;

    move-result-object p3

    if-eqz p3, :cond_3

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p3, Lcom/google/android/exoplayer2/metadata/Metadata;->d:[Lcom/google/android/exoplayer2/metadata/Metadata$Entry;

    array-length v3, v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0, p3, v2}, Lf/h/a/c/c1/f;->J(Lcom/google/android/exoplayer2/metadata/Metadata;Ljava/util/List;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p3

    if-nez p3, :cond_3

    new-instance p3, Lcom/google/android/exoplayer2/metadata/Metadata;

    invoke-direct {p3, v2}, Lcom/google/android/exoplayer2/metadata/Metadata;-><init>(Ljava/util/List;)V

    iget v2, p0, Lf/h/a/c/c1/f;->u:I

    iget v3, p0, Lf/h/a/c/c1/f;->v:I

    add-int/2addr v2, v3

    rem-int/2addr v2, v0

    iget-object v4, p0, Lf/h/a/c/c1/f;->s:[Lcom/google/android/exoplayer2/metadata/Metadata;

    aput-object p3, v4, v2

    iget-object p3, p0, Lf/h/a/c/c1/f;->t:[J

    iget-object v4, p0, Lf/h/a/c/c1/f;->r:Lf/h/a/c/c1/d;

    iget-wide v4, v4, Lf/h/a/c/y0/e;->f:J

    aput-wide v4, p3, v2

    add-int/2addr v3, v1

    iput v3, p0, Lf/h/a/c/c1/f;->v:I

    goto :goto_0

    :cond_2
    const/4 v3, -0x5

    if-ne v2, v3, :cond_3

    iget-object p3, p3, Lf/h/a/c/d0;->c:Lcom/google/android/exoplayer2/Format;

    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v2, p3, Lcom/google/android/exoplayer2/Format;->p:J

    iput-wide v2, p0, Lf/h/a/c/c1/f;->y:J

    :cond_3
    :goto_0
    iget p3, p0, Lf/h/a/c/c1/f;->v:I

    if-lez p3, :cond_5

    iget-object p3, p0, Lf/h/a/c/c1/f;->t:[J

    iget v2, p0, Lf/h/a/c/c1/f;->u:I

    aget-wide v3, p3, v2

    cmp-long p3, v3, p1

    if-gtz p3, :cond_5

    iget-object p1, p0, Lf/h/a/c/c1/f;->s:[Lcom/google/android/exoplayer2/metadata/Metadata;

    aget-object p1, p1, v2

    sget p2, Lf/h/a/c/i1/a0;->a:I

    iget-object p2, p0, Lf/h/a/c/c1/f;->q:Landroid/os/Handler;

    if-eqz p2, :cond_4

    invoke-virtual {p2, p4, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    :cond_4
    iget-object p2, p0, Lf/h/a/c/c1/f;->p:Lf/h/a/c/c1/e;

    invoke-interface {p2, p1}, Lf/h/a/c/c1/e;->q(Lcom/google/android/exoplayer2/metadata/Metadata;)V

    :goto_1
    iget-object p1, p0, Lf/h/a/c/c1/f;->s:[Lcom/google/android/exoplayer2/metadata/Metadata;

    iget p2, p0, Lf/h/a/c/c1/f;->u:I

    const/4 p3, 0x0

    aput-object p3, p1, p2

    add-int/2addr p2, v1

    rem-int/2addr p2, v0

    iput p2, p0, Lf/h/a/c/c1/f;->u:I

    iget p1, p0, Lf/h/a/c/c1/f;->v:I

    sub-int/2addr p1, v1

    iput p1, p0, Lf/h/a/c/c1/f;->v:I

    :cond_5
    return-void
.end method

.method public y()V
    .locals 2

    iget-object v0, p0, Lf/h/a/c/c1/f;->s:[Lcom/google/android/exoplayer2/metadata/Metadata;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput v0, p0, Lf/h/a/c/c1/f;->u:I

    iput v0, p0, Lf/h/a/c/c1/f;->v:I

    iput-object v1, p0, Lf/h/a/c/c1/f;->w:Lf/h/a/c/c1/b;

    return-void
.end method
