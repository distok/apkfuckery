.class public final Lf/h/a/c/s0$b;
.super Ljava/lang/Object;
.source "SimpleExoPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/a/c/s0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/a/c/z;

.field public c:Lf/h/a/c/i1/f;

.field public d:Lf/h/a/c/f1/h;

.field public e:Lf/h/a/c/e0;

.field public f:Lf/h/a/c/h1/e;

.field public g:Lf/h/a/c/v0/a;

.field public h:Landroid/os/Looper;

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 14

    new-instance v0, Lf/h/a/c/z;

    invoke-direct {v0, p1}, Lf/h/a/c/z;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;

    invoke-direct {v1, p1}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;-><init>(Landroid/content/Context;)V

    new-instance v13, Lf/h/a/c/x;

    new-instance v3, Lf/h/a/c/h1/l;

    const/4 v2, 0x1

    const/high16 v4, 0x10000

    invoke-direct {v3, v2, v4}, Lf/h/a/c/h1/l;-><init>(ZI)V

    const/16 v4, 0x3a98

    const v5, 0xc350

    const v6, 0xc350

    const/16 v7, 0x9c4

    const/16 v8, 0x1388

    const/4 v9, -0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v2, v13

    invoke-direct/range {v2 .. v12}, Lf/h/a/c/x;-><init>(Lf/h/a/c/h1/l;IIIIIIZIZ)V

    sget-object v2, Lf/h/a/c/h1/m;->n:Ljava/util/Map;

    const-class v2, Lf/h/a/c/h1/m;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lf/h/a/c/h1/m;->s:Lf/h/a/c/h1/m;

    if-nez v3, :cond_0

    new-instance v3, Lf/h/a/c/h1/m$a;

    invoke-direct {v3, p1}, Lf/h/a/c/h1/m$a;-><init>(Landroid/content/Context;)V

    new-instance v10, Lf/h/a/c/h1/m;

    iget-object v5, v3, Lf/h/a/c/h1/m$a;->a:Landroid/content/Context;

    iget-object v6, v3, Lf/h/a/c/h1/m$a;->b:Landroid/util/SparseArray;

    iget v7, v3, Lf/h/a/c/h1/m$a;->c:I

    iget-object v8, v3, Lf/h/a/c/h1/m$a;->d:Lf/h/a/c/i1/f;

    iget-boolean v9, v3, Lf/h/a/c/h1/m$a;->e:Z

    move-object v4, v10

    invoke-direct/range {v4 .. v9}, Lf/h/a/c/h1/m;-><init>(Landroid/content/Context;Landroid/util/SparseArray;ILf/h/a/c/i1/f;Z)V

    sput-object v10, Lf/h/a/c/h1/m;->s:Lf/h/a/c/h1/m;

    :cond_0
    sget-object v3, Lf/h/a/c/h1/m;->s:Lf/h/a/c/h1/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    :goto_0
    new-instance v4, Lf/h/a/c/v0/a;

    sget-object v5, Lf/h/a/c/i1/f;->a:Lf/h/a/c/i1/f;

    invoke-direct {v4, v5}, Lf/h/a/c/v0/a;-><init>(Lf/h/a/c/i1/f;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/a/c/s0$b;->a:Landroid/content/Context;

    iput-object v0, p0, Lf/h/a/c/s0$b;->b:Lf/h/a/c/z;

    iput-object v1, p0, Lf/h/a/c/s0$b;->d:Lf/h/a/c/f1/h;

    iput-object v13, p0, Lf/h/a/c/s0$b;->e:Lf/h/a/c/e0;

    iput-object v3, p0, Lf/h/a/c/s0$b;->f:Lf/h/a/c/h1/e;

    iput-object v2, p0, Lf/h/a/c/s0$b;->h:Landroid/os/Looper;

    iput-object v4, p0, Lf/h/a/c/s0$b;->g:Lf/h/a/c/v0/a;

    iput-object v5, p0, Lf/h/a/c/s0$b;->c:Lf/h/a/c/i1/f;

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1
.end method
