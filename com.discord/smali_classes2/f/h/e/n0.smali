.class public final Lf/h/e/n0;
.super Ljava/lang/Object;
.source "MessageSetSchema.java"

# interfaces
.implements Lf/h/e/x0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/h/e/x0<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/h/e/k0;

.field public final b:Lf/h/e/d1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/d1<",
            "**>;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Lf/h/e/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/l<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/e/d1;Lf/h/e/l;Lf/h/e/k0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/e/d1<",
            "**>;",
            "Lf/h/e/l<",
            "*>;",
            "Lf/h/e/k0;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    invoke-virtual {p2, p3}, Lf/h/e/l;->d(Lf/h/e/k0;)Z

    move-result p1

    iput-boolean p1, p0, Lf/h/e/n0;->c:Z

    iput-object p2, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    iput-object p3, p0, Lf/h/e/n0;->a:Lf/h/e/k0;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    sget-object v1, Lf/h/e/z0;->a:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Lf/h/e/d1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p2}, Lf/h/e/d1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lf/h/e/d1;->e(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lf/h/e/d1;->f(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-boolean v0, p0, Lf/h/e/n0;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-static {v0, p1, p2}, Lf/h/e/z0;->z(Lf/h/e/l;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Object;Lf/h/e/m1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lf/h/e/m1;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-virtual {v0, p1}, Lf/h/e/l;->b(Ljava/lang/Object;)Lf/h/e/o;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/e/o;->k()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/h/e/o$a;

    invoke-interface {v2}, Lf/h/e/o$a;->A0()Lf/h/e/l1;

    move-result-object v3

    sget-object v4, Lf/h/e/l1;->l:Lf/h/e/l1;

    if-ne v3, v4, :cond_1

    invoke-interface {v2}, Lf/h/e/o$a;->t()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v2}, Lf/h/e/o$a;->B0()Z

    move-result v3

    if-nez v3, :cond_1

    instance-of v3, v1, Lf/h/e/w$b;

    if-eqz v3, :cond_0

    invoke-interface {v2}, Lf/h/e/o$a;->getNumber()I

    move-result v2

    check-cast v1, Lf/h/e/w$b;

    iget-object v1, v1, Lf/h/e/w$b;->d:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/e/w;

    invoke-virtual {v1}, Lf/h/e/x;->b()Lf/h/e/g;

    move-result-object v1

    move-object v3, p2

    check-cast v3, Lf/h/e/i;

    invoke-virtual {v3, v2, v1}, Lf/h/e/i;->e(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Lf/h/e/o$a;->getNumber()I

    move-result v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    move-object v3, p2

    check-cast v3, Lf/h/e/i;

    invoke-virtual {v3, v2, v1}, Lf/h/e/i;->e(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Found invalid MessageSet item."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    iget-object v0, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    invoke-virtual {v0, p1}, Lf/h/e/d1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lf/h/e/d1;->g(Ljava/lang/Object;Lf/h/e/m1;)V

    return-void
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    invoke-virtual {v0, p1}, Lf/h/e/d1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    invoke-virtual {v1, p2}, Lf/h/e/d1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-boolean v0, p0, Lf/h/e/n0;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-virtual {v0, p1}, Lf/h/e/l;->b(Ljava/lang/Object;)Lf/h/e/o;

    move-result-object p1

    iget-object v0, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-virtual {v0, p2}, Lf/h/e/l;->b(Ljava/lang/Object;)Lf/h/e/o;

    move-result-object p2

    invoke-virtual {p1, p2}, Lf/h/e/o;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public d(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    invoke-virtual {v0, p1}, Lf/h/e/d1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-boolean v1, p0, Lf/h/e/n0;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-virtual {v1, p1}, Lf/h/e/l;->b(Ljava/lang/Object;)Lf/h/e/o;

    move-result-object p1

    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p1}, Lf/h/e/o;->hashCode()I

    move-result p1

    add-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method public e(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    invoke-virtual {v0, p1}, Lf/h/e/d1;->d(Ljava/lang/Object;)V

    iget-object v0, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-virtual {v0, p1}, Lf/h/e/l;->e(Ljava/lang/Object;)V

    return-void
.end method

.method public final f(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-virtual {v0, p1}, Lf/h/e/l;->b(Ljava/lang/Object;)Lf/h/e/o;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/e/o;->i()Z

    move-result p1

    return p1
.end method

.method public g(Ljava/lang/Object;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/n0;->b:Lf/h/e/d1;

    invoke-virtual {v0, p1}, Lf/h/e/d1;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/e/d1;->c(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    add-int/2addr v0, v1

    iget-boolean v2, p0, Lf/h/e/n0;->c:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf/h/e/n0;->d:Lf/h/e/l;

    invoke-virtual {v2, p1}, Lf/h/e/l;->b(Ljava/lang/Object;)Lf/h/e/o;

    move-result-object p1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p1, Lf/h/e/o;->a:Lf/h/e/b1;

    invoke-virtual {v3}, Lf/h/e/b1;->d()I

    move-result v3

    if-ge v1, v3, :cond_0

    iget-object v3, p1, Lf/h/e/o;->a:Lf/h/e/b1;

    invoke-virtual {v3, v1}, Lf/h/e/b1;->c(I)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-virtual {p1, v3}, Lf/h/e/o;->g(Ljava/util/Map$Entry;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lf/h/e/o;->a:Lf/h/e/b1;

    invoke-virtual {v1}, Lf/h/e/b1;->e()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-virtual {p1, v3}, Lf/h/e/o;->g(Ljava/util/Map$Entry;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_1
    add-int/2addr v0, v2

    :cond_2
    return v0
.end method
