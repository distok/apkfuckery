.class public final Lf/h/e/i;
.super Ljava/lang/Object;
.source "CodedOutputStreamWriter.java"

# interfaces
.implements Lf/h/e/m1;


# instance fields
.field public final a:Lcom/google/protobuf/CodedOutputStream;


# direct methods
.method public constructor <init>(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/h/e/t;->a:Ljava/nio/charset/Charset;

    iput-object p1, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    iput-object p0, p1, Lcom/google/protobuf/CodedOutputStream;->a:Lf/h/e/i;

    return-void
.end method


# virtual methods
.method public a(ID)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide p2

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/protobuf/CodedOutputStream;->K(IJ)V

    return-void
.end method

.method public b(IF)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result p2

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/CodedOutputStream;->I(II)V

    return-void
.end method

.method public c(ILjava/lang/Object;Lf/h/e/x0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    check-cast p2, Lf/h/e/k0;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/CodedOutputStream;->U(II)V

    iget-object v1, v0, Lcom/google/protobuf/CodedOutputStream;->a:Lf/h/e/i;

    invoke-interface {p3, p2, v1}, Lf/h/e/x0;->b(Ljava/lang/Object;Lf/h/e/m1;)V

    const/4 p2, 0x4

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/CodedOutputStream;->U(II)V

    return-void
.end method

.method public d(ILjava/lang/Object;Lf/h/e/x0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    check-cast p2, Lf/h/e/k0;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/protobuf/CodedOutputStream;->O(ILf/h/e/k0;Lf/h/e/x0;)V

    return-void
.end method

.method public final e(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    instance-of v0, p2, Lf/h/e/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    check-cast p2, Lf/h/e/g;

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/CodedOutputStream;->R(ILf/h/e/g;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    check-cast p2, Lf/h/e/k0;

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/CodedOutputStream;->Q(ILf/h/e/k0;)V

    :goto_0
    return-void
.end method

.method public f(II)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    invoke-static {p2}, Lcom/google/protobuf/CodedOutputStream;->B(I)I

    move-result p2

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/CodedOutputStream;->V(II)V

    return-void
.end method

.method public g(IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/i;->a:Lcom/google/protobuf/CodedOutputStream;

    invoke-static {p2, p3}, Lcom/google/protobuf/CodedOutputStream;->C(J)J

    move-result-wide p2

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/protobuf/CodedOutputStream;->X(IJ)V

    return-void
.end method
