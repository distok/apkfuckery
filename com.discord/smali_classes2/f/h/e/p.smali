.class public final enum Lf/h/e/p;
.super Ljava/lang/Enum;
.source "FieldType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/e/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/e/p;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lf/h/e/p;

.field public static final enum B:Lf/h/e/p;

.field public static final enum C:Lf/h/e/p;

.field public static final enum D:Lf/h/e/p;

.field public static final enum E:Lf/h/e/p;

.field public static final enum F:Lf/h/e/p;

.field public static final enum G:Lf/h/e/p;

.field public static final enum H:Lf/h/e/p;

.field public static final enum I:Lf/h/e/p;

.field public static final enum J:Lf/h/e/p;

.field public static final enum K:Lf/h/e/p;

.field public static final enum L:Lf/h/e/p;

.field public static final enum M:Lf/h/e/p;

.field public static final enum N:Lf/h/e/p;

.field public static final enum O:Lf/h/e/p;

.field public static final enum P:Lf/h/e/p;

.field public static final enum Q:Lf/h/e/p;

.field public static final enum R:Lf/h/e/p;

.field public static final enum S:Lf/h/e/p;

.field public static final enum T:Lf/h/e/p;

.field public static final enum U:Lf/h/e/p;

.field public static final enum V:Lf/h/e/p;

.field public static final enum W:Lf/h/e/p;

.field public static final enum X:Lf/h/e/p;

.field public static final enum Y:Lf/h/e/p;

.field public static final enum Z:Lf/h/e/p;

.field public static final enum a0:Lf/h/e/p;

.field public static final enum b0:Lf/h/e/p;

.field public static final c0:[Lf/h/e/p;

.field public static final enum d:Lf/h/e/p;

.field public static final synthetic d0:[Lf/h/e/p;

.field public static final enum e:Lf/h/e/p;

.field public static final enum f:Lf/h/e/p;

.field public static final enum g:Lf/h/e/p;

.field public static final enum h:Lf/h/e/p;

.field public static final enum i:Lf/h/e/p;

.field public static final enum j:Lf/h/e/p;

.field public static final enum k:Lf/h/e/p;

.field public static final enum l:Lf/h/e/p;

.field public static final enum m:Lf/h/e/p;

.field public static final enum n:Lf/h/e/p;

.field public static final enum o:Lf/h/e/p;

.field public static final enum p:Lf/h/e/p;

.field public static final enum q:Lf/h/e/p;

.field public static final enum r:Lf/h/e/p;

.field public static final enum s:Lf/h/e/p;

.field public static final enum t:Lf/h/e/p;

.field public static final enum u:Lf/h/e/p;

.field public static final enum v:Lf/h/e/p;

.field public static final enum w:Lf/h/e/p;

.field public static final enum x:Lf/h/e/p;

.field public static final enum y:Lf/h/e/p;

.field public static final enum z:Lf/h/e/p;


# instance fields
.field private final collection:Lf/h/e/p$a;

.field private final elementType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private final id:I

.field private final javaType:Lf/h/e/v;

.field private final primitiveScalar:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 17

    new-instance v6, Lf/h/e/p;

    sget-object v7, Lf/h/e/p$a;->d:Lf/h/e/p$a;

    sget-object v8, Lf/h/e/v;->h:Lf/h/e/v;

    const-string v1, "DOUBLE"

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, v6

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->d:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v9, Lf/h/e/v;->g:Lf/h/e/v;

    const-string v1, "FLOAT"

    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object v0, v6

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->e:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v10, Lf/h/e/v;->f:Lf/h/e/v;

    const-string v1, "INT64"

    const/4 v2, 0x2

    const/4 v3, 0x2

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->f:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "UINT64"

    const/4 v2, 0x3

    const/4 v3, 0x3

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->g:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v11, Lf/h/e/v;->e:Lf/h/e/v;

    const-string v1, "INT32"

    const/4 v2, 0x4

    const/4 v3, 0x4

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->h:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FIXED64"

    const/4 v2, 0x5

    const/4 v3, 0x5

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->i:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FIXED32"

    const/4 v2, 0x6

    const/4 v3, 0x6

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->j:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v12, Lf/h/e/v;->i:Lf/h/e/v;

    const-string v1, "BOOL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    move-object v0, v6

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->k:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v13, Lf/h/e/v;->j:Lf/h/e/v;

    const-string v1, "STRING"

    const/16 v2, 0x8

    const/16 v3, 0x8

    move-object v0, v6

    move-object v5, v13

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->l:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v14, Lf/h/e/v;->m:Lf/h/e/v;

    const-string v1, "MESSAGE"

    const/16 v2, 0x9

    const/16 v3, 0x9

    move-object v0, v6

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->m:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v15, Lf/h/e/v;->k:Lf/h/e/v;

    const-string v1, "BYTES"

    const/16 v2, 0xa

    const/16 v3, 0xa

    move-object v0, v6

    move-object v5, v15

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->n:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "UINT32"

    const/16 v2, 0xb

    const/16 v3, 0xb

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->o:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v16, Lf/h/e/v;->l:Lf/h/e/v;

    const-string v1, "ENUM"

    const/16 v2, 0xc

    const/16 v3, 0xc

    move-object v0, v6

    move-object/from16 v5, v16

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->p:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SFIXED32"

    const/16 v2, 0xd

    const/16 v3, 0xd

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->q:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SFIXED64"

    const/16 v2, 0xe

    const/16 v3, 0xe

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->r:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SINT32"

    const/16 v2, 0xf

    const/16 v3, 0xf

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->s:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SINT64"

    const/16 v2, 0x10

    const/16 v3, 0x10

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->t:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "GROUP"

    const/16 v2, 0x11

    const/16 v3, 0x11

    move-object v0, v6

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->u:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v7, Lf/h/e/p$a;->e:Lf/h/e/p$a;

    const-string v1, "DOUBLE_LIST"

    const/16 v2, 0x12

    const/16 v3, 0x12

    move-object v0, v6

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->v:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FLOAT_LIST"

    const/16 v2, 0x13

    const/16 v3, 0x13

    move-object v0, v6

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->w:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "INT64_LIST"

    const/16 v2, 0x14

    const/16 v3, 0x14

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->x:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "UINT64_LIST"

    const/16 v2, 0x15

    const/16 v3, 0x15

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->y:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "INT32_LIST"

    const/16 v2, 0x16

    const/16 v3, 0x16

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->z:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FIXED64_LIST"

    const/16 v2, 0x17

    const/16 v3, 0x17

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->A:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FIXED32_LIST"

    const/16 v2, 0x18

    const/16 v3, 0x18

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->B:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "BOOL_LIST"

    const/16 v2, 0x19

    const/16 v3, 0x19

    move-object v0, v6

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->C:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "STRING_LIST"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    move-object v0, v6

    move-object v5, v13

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->D:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "MESSAGE_LIST"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    move-object v0, v6

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->E:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "BYTES_LIST"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    move-object v0, v6

    move-object v5, v15

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->F:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "UINT32_LIST"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->G:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "ENUM_LIST"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    move-object v0, v6

    move-object/from16 v5, v16

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->H:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SFIXED32_LIST"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->I:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SFIXED64_LIST"

    const/16 v2, 0x20

    const/16 v3, 0x20

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->J:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SINT32_LIST"

    const/16 v2, 0x21

    const/16 v3, 0x21

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->K:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SINT64_LIST"

    const/16 v2, 0x22

    const/16 v3, 0x22

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->L:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v13, Lf/h/e/p$a;->f:Lf/h/e/p$a;

    const-string v1, "DOUBLE_LIST_PACKED"

    const/16 v2, 0x23

    const/16 v3, 0x23

    move-object v0, v6

    move-object v4, v13

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->M:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FLOAT_LIST_PACKED"

    const/16 v2, 0x24

    const/16 v3, 0x24

    move-object v0, v6

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->N:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "INT64_LIST_PACKED"

    const/16 v2, 0x25

    const/16 v3, 0x25

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->O:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "UINT64_LIST_PACKED"

    const/16 v2, 0x26

    const/16 v3, 0x26

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->P:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "INT32_LIST_PACKED"

    const/16 v2, 0x27

    const/16 v3, 0x27

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->Q:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FIXED64_LIST_PACKED"

    const/16 v2, 0x28

    const/16 v3, 0x28

    move-object v0, v6

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->R:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "FIXED32_LIST_PACKED"

    const/16 v2, 0x29

    const/16 v3, 0x29

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->S:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "BOOL_LIST_PACKED"

    const/16 v2, 0x2a

    const/16 v3, 0x2a

    move-object v0, v6

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->T:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "UINT32_LIST_PACKED"

    const/16 v2, 0x2b

    const/16 v3, 0x2b

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->U:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "ENUM_LIST_PACKED"

    const/16 v2, 0x2c

    const/16 v3, 0x2c

    move-object v0, v6

    move-object/from16 v5, v16

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->V:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SFIXED32_LIST_PACKED"

    const/16 v2, 0x2d

    const/16 v3, 0x2d

    move-object v0, v6

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->W:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v8, Lf/h/e/v;->f:Lf/h/e/v;

    const-string v1, "SFIXED64_LIST_PACKED"

    const/16 v2, 0x2e

    const/16 v3, 0x2e

    move-object v0, v6

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->X:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    sget-object v5, Lf/h/e/v;->e:Lf/h/e/v;

    const-string v1, "SINT32_LIST_PACKED"

    const/16 v2, 0x2f

    const/16 v3, 0x2f

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->Y:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "SINT64_LIST_PACKED"

    const/16 v2, 0x30

    const/16 v3, 0x30

    move-object v0, v6

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->Z:Lf/h/e/p;

    new-instance v6, Lf/h/e/p;

    const-string v1, "GROUP_LIST"

    const/16 v2, 0x31

    const/16 v3, 0x31

    move-object v0, v6

    move-object v4, v7

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v6, Lf/h/e/p;->a0:Lf/h/e/p;

    new-instance v0, Lf/h/e/p;

    sget-object v12, Lf/h/e/p$a;->g:Lf/h/e/p$a;

    sget-object v13, Lf/h/e/v;->d:Lf/h/e/v;

    const-string v9, "MAP"

    const/16 v10, 0x32

    const/16 v11, 0x32

    move-object v8, v0

    invoke-direct/range {v8 .. v13}, Lf/h/e/p;-><init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V

    sput-object v0, Lf/h/e/p;->b0:Lf/h/e/p;

    const/16 v0, 0x33

    new-array v1, v0, [Lf/h/e/p;

    sget-object v2, Lf/h/e/p;->d:Lf/h/e/p;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lf/h/e/p;->e:Lf/h/e/p;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->f:Lf/h/e/p;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->g:Lf/h/e/p;

    const/4 v4, 0x3

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->h:Lf/h/e/p;

    const/4 v4, 0x4

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->i:Lf/h/e/p;

    const/4 v4, 0x5

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->j:Lf/h/e/p;

    const/4 v4, 0x6

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->k:Lf/h/e/p;

    const/4 v4, 0x7

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->l:Lf/h/e/p;

    const/16 v4, 0x8

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->m:Lf/h/e/p;

    const/16 v4, 0x9

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->n:Lf/h/e/p;

    const/16 v4, 0xa

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->o:Lf/h/e/p;

    const/16 v4, 0xb

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->p:Lf/h/e/p;

    const/16 v4, 0xc

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->q:Lf/h/e/p;

    const/16 v4, 0xd

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->r:Lf/h/e/p;

    const/16 v4, 0xe

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->s:Lf/h/e/p;

    const/16 v4, 0xf

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->t:Lf/h/e/p;

    const/16 v4, 0x10

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->u:Lf/h/e/p;

    const/16 v4, 0x11

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->v:Lf/h/e/p;

    const/16 v4, 0x12

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->w:Lf/h/e/p;

    const/16 v4, 0x13

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->x:Lf/h/e/p;

    const/16 v4, 0x14

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->y:Lf/h/e/p;

    const/16 v4, 0x15

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->z:Lf/h/e/p;

    const/16 v4, 0x16

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->A:Lf/h/e/p;

    const/16 v4, 0x17

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->B:Lf/h/e/p;

    const/16 v4, 0x18

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->C:Lf/h/e/p;

    const/16 v4, 0x19

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->D:Lf/h/e/p;

    const/16 v4, 0x1a

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->E:Lf/h/e/p;

    const/16 v4, 0x1b

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->F:Lf/h/e/p;

    const/16 v4, 0x1c

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->G:Lf/h/e/p;

    const/16 v4, 0x1d

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->H:Lf/h/e/p;

    const/16 v4, 0x1e

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->I:Lf/h/e/p;

    const/16 v4, 0x1f

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->J:Lf/h/e/p;

    const/16 v4, 0x20

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->K:Lf/h/e/p;

    const/16 v4, 0x21

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->L:Lf/h/e/p;

    const/16 v4, 0x22

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->M:Lf/h/e/p;

    const/16 v4, 0x23

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->N:Lf/h/e/p;

    const/16 v4, 0x24

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->O:Lf/h/e/p;

    const/16 v4, 0x25

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->P:Lf/h/e/p;

    const/16 v4, 0x26

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->Q:Lf/h/e/p;

    const/16 v4, 0x27

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->R:Lf/h/e/p;

    const/16 v4, 0x28

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->S:Lf/h/e/p;

    const/16 v4, 0x29

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->T:Lf/h/e/p;

    const/16 v4, 0x2a

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->U:Lf/h/e/p;

    const/16 v4, 0x2b

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->V:Lf/h/e/p;

    const/16 v4, 0x2c

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->W:Lf/h/e/p;

    const/16 v4, 0x2d

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->X:Lf/h/e/p;

    const/16 v4, 0x2e

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->Y:Lf/h/e/p;

    const/16 v4, 0x2f

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->Z:Lf/h/e/p;

    const/16 v4, 0x30

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->a0:Lf/h/e/p;

    const/16 v4, 0x31

    aput-object v2, v1, v4

    sget-object v2, Lf/h/e/p;->b0:Lf/h/e/p;

    const/16 v4, 0x32

    aput-object v2, v1, v4

    sput-object v1, Lf/h/e/p;->d0:[Lf/h/e/p;

    invoke-static {}, Lf/h/e/p;->values()[Lf/h/e/p;

    move-result-object v1

    new-array v0, v0, [Lf/h/e/p;

    sput-object v0, Lf/h/e/p;->c0:[Lf/h/e/p;

    array-length v0, v1

    :goto_0
    if-ge v3, v0, :cond_0

    aget-object v2, v1, v3

    sget-object v4, Lf/h/e/p;->c0:[Lf/h/e/p;

    iget v5, v2, Lf/h/e/p;->id:I

    aput-object v2, v4, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILf/h/e/p$a;Lf/h/e/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/h/e/p$a;",
            "Lf/h/e/v;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/h/e/p;->id:I

    iput-object p4, p0, Lf/h/e/p;->collection:Lf/h/e/p$a;

    iput-object p5, p0, Lf/h/e/p;->javaType:Lf/h/e/v;

    invoke-virtual {p4}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_1

    const/4 p3, 0x3

    if-eq p1, p3, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lf/h/e/p;->elementType:Ljava/lang/Class;

    goto :goto_0

    :cond_0
    invoke-virtual {p5}, Lf/h/e/v;->f()Ljava/lang/Class;

    move-result-object p1

    iput-object p1, p0, Lf/h/e/p;->elementType:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    invoke-virtual {p5}, Lf/h/e/v;->f()Ljava/lang/Class;

    move-result-object p1

    iput-object p1, p0, Lf/h/e/p;->elementType:Ljava/lang/Class;

    :goto_0
    sget-object p1, Lf/h/e/p$a;->d:Lf/h/e/p$a;

    if-ne p4, p1, :cond_2

    invoke-virtual {p5}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p3, 0x6

    if-eq p1, p3, :cond_2

    const/4 p3, 0x7

    if-eq p1, p3, :cond_2

    const/16 p3, 0x9

    if-eq p1, p3, :cond_2

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    :goto_1
    iput-boolean p2, p0, Lf/h/e/p;->primitiveScalar:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/e/p;
    .locals 1

    const-class v0, Lf/h/e/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/e/p;

    return-object p0
.end method

.method public static values()[Lf/h/e/p;
    .locals 1

    sget-object v0, Lf/h/e/p;->d0:[Lf/h/e/p;

    invoke-virtual {v0}, [Lf/h/e/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/e/p;

    return-object v0
.end method


# virtual methods
.method public f()I
    .locals 1

    iget v0, p0, Lf/h/e/p;->id:I

    return v0
.end method
