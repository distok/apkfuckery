.class public final Lf/h/e/h$b;
.super Lf/h/e/h;
.source "CodedInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/e/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final b:[B

.field public final c:Z

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>([BIIZLf/h/e/h$a;)V
    .locals 0

    const/4 p5, 0x0

    invoke-direct {p0, p5}, Lf/h/e/h;-><init>(Lf/h/e/h$a;)V

    const p5, 0x7fffffff

    iput p5, p0, Lf/h/e/h$b;->h:I

    iput-object p1, p0, Lf/h/e/h$b;->b:[B

    add-int/2addr p3, p2

    iput p3, p0, Lf/h/e/h$b;->d:I

    iput p2, p0, Lf/h/e/h$b;->f:I

    iput p2, p0, Lf/h/e/h$b;->g:I

    iput-boolean p4, p0, Lf/h/e/h$b;->c:Z

    return-void
.end method


# virtual methods
.method public b()I
    .locals 2

    iget v0, p0, Lf/h/e/h$b;->f:I

    iget v1, p0, Lf/h/e/h$b;->g:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public c(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lf/h/e/h$b;->b()I

    move-result v0

    add-int/2addr v0, p1

    iget p1, p0, Lf/h/e/h$b;->h:I

    if-gt v0, p1, :cond_0

    iput v0, p0, Lf/h/e/h$b;->h:I

    invoke-virtual {p0}, Lf/h/e/h$b;->d()V

    return p1

    :cond_0
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->c()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->b()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
.end method

.method public final d()V
    .locals 3

    iget v0, p0, Lf/h/e/h$b;->d:I

    iget v1, p0, Lf/h/e/h$b;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lf/h/e/h$b;->d:I

    iget v1, p0, Lf/h/e/h$b;->g:I

    sub-int v1, v0, v1

    iget v2, p0, Lf/h/e/h$b;->h:I

    if-le v1, v2, :cond_0

    sub-int/2addr v1, v2

    iput v1, p0, Lf/h/e/h$b;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, Lf/h/e/h$b;->d:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lf/h/e/h$b;->e:I

    :goto_0
    return-void
.end method
