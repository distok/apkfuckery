.class public final enum Lf/h/e/r$e;
.super Ljava/lang/Enum;
.source "GeneratedMessageLite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/e/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/e/r$e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/e/r$e;

.field public static final enum e:Lf/h/e/r$e;

.field public static final enum f:Lf/h/e/r$e;

.field public static final enum g:Lf/h/e/r$e;

.field public static final enum h:Lf/h/e/r$e;

.field public static final enum i:Lf/h/e/r$e;

.field public static final enum j:Lf/h/e/r$e;

.field public static final synthetic k:[Lf/h/e/r$e;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    new-instance v0, Lf/h/e/r$e;

    const-string v1, "GET_MEMOIZED_IS_INITIALIZED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/e/r$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/e/r$e;->d:Lf/h/e/r$e;

    new-instance v1, Lf/h/e/r$e;

    const-string v3, "SET_MEMOIZED_IS_INITIALIZED"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/e/r$e;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/e/r$e;->e:Lf/h/e/r$e;

    new-instance v3, Lf/h/e/r$e;

    const-string v5, "BUILD_MESSAGE_INFO"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/h/e/r$e;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/h/e/r$e;->f:Lf/h/e/r$e;

    new-instance v5, Lf/h/e/r$e;

    const-string v7, "NEW_MUTABLE_INSTANCE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/h/e/r$e;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/h/e/r$e;->g:Lf/h/e/r$e;

    new-instance v7, Lf/h/e/r$e;

    const-string v9, "NEW_BUILDER"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lf/h/e/r$e;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lf/h/e/r$e;->h:Lf/h/e/r$e;

    new-instance v9, Lf/h/e/r$e;

    const-string v11, "GET_DEFAULT_INSTANCE"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lf/h/e/r$e;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lf/h/e/r$e;->i:Lf/h/e/r$e;

    new-instance v11, Lf/h/e/r$e;

    const-string v13, "GET_PARSER"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lf/h/e/r$e;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lf/h/e/r$e;->j:Lf/h/e/r$e;

    const/4 v13, 0x7

    new-array v13, v13, [Lf/h/e/r$e;

    aput-object v0, v13, v2

    aput-object v1, v13, v4

    aput-object v3, v13, v6

    aput-object v5, v13, v8

    aput-object v7, v13, v10

    aput-object v9, v13, v12

    aput-object v11, v13, v14

    sput-object v13, Lf/h/e/r$e;->k:[Lf/h/e/r$e;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/e/r$e;
    .locals 1

    const-class v0, Lf/h/e/r$e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/e/r$e;

    return-object p0
.end method

.method public static values()[Lf/h/e/r$e;
    .locals 1

    sget-object v0, Lf/h/e/r$e;->k:[Lf/h/e/r$e;

    invoke-virtual {v0}, [Lf/h/e/r$e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/e/r$e;

    return-object v0
.end method
