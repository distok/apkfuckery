.class public abstract Lf/h/e/r$a;
.super Lf/h/e/a$a;
.source "GeneratedMessageLite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/e/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lf/h/e/r<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Lf/h/e/r$a<",
        "TMessageType;TBuilderType;>;>",
        "Lf/h/e/a$a<",
        "TMessageType;TBuilderType;>;"
    }
.end annotation


# instance fields
.field public final d:Lf/h/e/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field public e:Lf/h/e/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(Lf/h/e/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/h/e/a$a;-><init>()V

    iput-object p1, p0, Lf/h/e/r$a;->d:Lf/h/e/r;

    sget-object v0, Lf/h/e/r$e;->g:Lf/h/e/r$e;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/e/r;

    iput-object p1, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/h/e/r$a;->f:Z

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/r$a;->d:Lf/h/e/r;

    invoke-virtual {v0}, Lf/h/e/r;->y()Lf/h/e/r$a;

    move-result-object v0

    invoke-virtual {p0}, Lf/h/e/r$a;->q()Lf/h/e/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/h/e/r$a;->s(Lf/h/e/r;)Lf/h/e/r$a;

    return-object v0
.end method

.method public f()Lf/h/e/k0;
    .locals 1

    iget-object v0, p0, Lf/h/e/r$a;->d:Lf/h/e/r;

    return-object v0
.end method

.method public final p()Lf/h/e/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/e/r$a;->q()Lf/h/e/r;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/e/r;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0
.end method

.method public q()Lf/h/e/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    iget-boolean v0, p0, Lf/h/e/r$a;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    return-object v0

    :cond_0
    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v1, v0}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v1

    invoke-interface {v1, v0}, Lf/h/e/x0;->e(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/h/e/r$a;->f:Z

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    return-object v0
.end method

.method public final r()V
    .locals 3

    iget-boolean v0, p0, Lf/h/e/r$a;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    sget-object v1, Lf/h/e/r$e;->g:Lf/h/e/r$e;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r;

    iget-object v1, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    sget-object v2, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v2, v0}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lf/h/e/x0;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h/e/r$a;->f:Z

    :cond_0
    return-void
.end method

.method public s(Lf/h/e/r;)Lf/h/e/r$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TBuilderType;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/h/e/r$a;->r()V

    iget-object v0, p0, Lf/h/e/r$a;->e:Lf/h/e/r;

    invoke-virtual {p0, v0, p1}, Lf/h/e/r$a;->u(Lf/h/e/r;Lf/h/e/r;)V

    return-object p0
.end method

.method public final u(Lf/h/e/r;Lf/h/e/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;TMessageType;)V"
        }
    .end annotation

    sget-object v0, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v0, p1}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lf/h/e/x0;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
