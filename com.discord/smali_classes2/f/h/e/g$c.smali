.class public final Lf/h/e/g$c;
.super Lf/h/e/g$f;
.source "ByteString.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/e/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final bytesLength:I

.field private final bytesOffset:I


# direct methods
.method public constructor <init>([BII)V
    .locals 1

    invoke-direct {p0, p1}, Lf/h/e/g$f;-><init>([B)V

    add-int v0, p2, p3

    array-length p1, p1

    invoke-static {p2, v0, p1}, Lf/h/e/g;->d(III)I

    iput p2, p0, Lf/h/e/g$c;->bytesOffset:I

    iput p3, p0, Lf/h/e/g$c;->bytesLength:I

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance p1, Ljava/io/InvalidObjectException;

    const-string v0, "BoundedByteStream instances are not to be serialized directly"

    invoke-direct {p1, v0}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public c(I)B
    .locals 4

    iget v0, p0, Lf/h/e/g$c;->bytesLength:I

    add-int/lit8 v1, p1, 0x1

    sub-int v1, v0, v1

    or-int/2addr v1, p1

    if-gez v1, :cond_1

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Index < 0: "

    invoke-static {v1, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v2, "Index > length: "

    const-string v3, ", "

    invoke-static {v2, p1, v3, v0}, Lf/e/c/a/a;->l(Ljava/lang/String;ILjava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v0, p0, Lf/h/e/g$f;->bytes:[B

    iget v1, p0, Lf/h/e/g$c;->bytesOffset:I

    add-int/2addr v1, p1

    aget-byte p1, v0, v1

    return p1
.end method

.method public e(I)B
    .locals 2

    iget-object v0, p0, Lf/h/e/g$f;->bytes:[B

    iget v1, p0, Lf/h/e/g$c;->bytesOffset:I

    add-int/2addr v1, p1

    aget-byte p1, v0, v1

    return p1
.end method

.method public s()I
    .locals 1

    iget v0, p0, Lf/h/e/g$c;->bytesOffset:I

    return v0
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lf/h/e/g$c;->bytesLength:I

    return v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 5

    iget v0, p0, Lf/h/e/g$c;->bytesLength:I

    if-nez v0, :cond_0

    sget-object v0, Lf/h/e/t;->b:[B

    goto :goto_0

    :cond_0
    new-array v1, v0, [B

    const/4 v2, 0x0

    iget-object v3, p0, Lf/h/e/g$f;->bytes:[B

    iget v4, p0, Lf/h/e/g$c;->bytesOffset:I

    add-int/2addr v4, v2

    invoke-static {v3, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    :goto_0
    new-instance v1, Lf/h/e/g$f;

    invoke-direct {v1, v0}, Lf/h/e/g$f;-><init>([B)V

    return-object v1
.end method
