.class public final enum Lf/h/e/t0;
.super Ljava/lang/Enum;
.source "ProtoSyntax.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/e/t0;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/e/t0;

.field public static final enum e:Lf/h/e/t0;

.field public static final synthetic f:[Lf/h/e/t0;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/h/e/t0;

    const-string v1, "PROTO2"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/h/e/t0;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/h/e/t0;->d:Lf/h/e/t0;

    new-instance v1, Lf/h/e/t0;

    const-string v3, "PROTO3"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/h/e/t0;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/h/e/t0;->e:Lf/h/e/t0;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/h/e/t0;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/h/e/t0;->f:[Lf/h/e/t0;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/e/t0;
    .locals 1

    const-class v0, Lf/h/e/t0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/e/t0;

    return-object p0
.end method

.method public static values()[Lf/h/e/t0;
    .locals 1

    sget-object v0, Lf/h/e/t0;->f:[Lf/h/e/t0;

    invoke-virtual {v0}, [Lf/h/e/t0;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/e/t0;

    return-object v0
.end method
