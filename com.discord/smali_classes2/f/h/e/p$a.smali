.class public final enum Lf/h/e/p$a;
.super Ljava/lang/Enum;
.source "FieldType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/e/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/e/p$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/e/p$a;

.field public static final enum e:Lf/h/e/p$a;

.field public static final enum f:Lf/h/e/p$a;

.field public static final enum g:Lf/h/e/p$a;

.field public static final synthetic h:[Lf/h/e/p$a;


# instance fields
.field private final isList:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/h/e/p$a;

    const-string v1, "SCALAR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/h/e/p$a;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lf/h/e/p$a;->d:Lf/h/e/p$a;

    new-instance v1, Lf/h/e/p$a;

    const-string v3, "VECTOR"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/h/e/p$a;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lf/h/e/p$a;->e:Lf/h/e/p$a;

    new-instance v3, Lf/h/e/p$a;

    const-string v5, "PACKED_VECTOR"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v4}, Lf/h/e/p$a;-><init>(Ljava/lang/String;IZ)V

    sput-object v3, Lf/h/e/p$a;->f:Lf/h/e/p$a;

    new-instance v5, Lf/h/e/p$a;

    const-string v7, "MAP"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v2}, Lf/h/e/p$a;-><init>(Ljava/lang/String;IZ)V

    sput-object v5, Lf/h/e/p$a;->g:Lf/h/e/p$a;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/h/e/p$a;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/h/e/p$a;->h:[Lf/h/e/p$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lf/h/e/p$a;->isList:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/e/p$a;
    .locals 1

    const-class v0, Lf/h/e/p$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/e/p$a;

    return-object p0
.end method

.method public static values()[Lf/h/e/p$a;
    .locals 1

    sget-object v0, Lf/h/e/p$a;->h:[Lf/h/e/p$a;

    invoke-virtual {v0}, [Lf/h/e/p$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/e/p$a;

    return-object v0
.end method
