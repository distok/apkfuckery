.class public final Lf/h/e/u0;
.super Ljava/lang/Object;
.source "Protobuf.java"


# static fields
.field public static final c:Lf/h/e/u0;


# instance fields
.field public final a:Lf/h/e/y0;

.field public final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/Class<",
            "*>;",
            "Lf/h/e/x0<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/e/u0;

    invoke-direct {v0}, Lf/h/e/u0;-><init>()V

    sput-object v0, Lf/h/e/u0;->c:Lf/h/e/u0;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lf/h/e/u0;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, Lf/h/e/c0;

    invoke-direct {v0}, Lf/h/e/c0;-><init>()V

    iput-object v0, p0, Lf/h/e/u0;->a:Lf/h/e/y0;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Lf/h/e/x0;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lf/h/e/x0<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lf/h/e/t;->a:Ljava/nio/charset/Charset;

    const-string v0, "messageType"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v0, p0, Lf/h/e/u0;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/x0;

    if-nez v0, :cond_b

    iget-object v0, p0, Lf/h/e/u0;->a:Lf/h/e/y0;

    check-cast v0, Lf/h/e/c0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v1, Lf/h/e/r;

    sget-object v2, Lf/h/e/z0;->a:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lf/h/e/z0;->a:Ljava/lang/Class;

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Message classes must extend GeneratedMessage or GeneratedMessageLite"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, v0, Lf/h/e/c0;->a:Lf/h/e/j0;

    invoke-interface {v0, p1}, Lf/h/e/j0;->a(Ljava/lang/Class;)Lf/h/e/i0;

    move-result-object v2

    invoke-interface {v2}, Lf/h/e/i0;->a()Z

    move-result v0

    const-string v3, "Protobuf runtime is not correctly loaded."

    if-eqz v0, :cond_4

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lf/h/e/z0;->d:Lf/h/e/d1;

    sget-object v1, Lf/h/e/n;->a:Lf/h/e/l;

    sget-object v1, Lf/h/e/n;->a:Lf/h/e/l;

    invoke-interface {v2}, Lf/h/e/i0;->b()Lf/h/e/k0;

    move-result-object v2

    new-instance v3, Lf/h/e/n0;

    invoke-direct {v3, v0, v1, v2}, Lf/h/e/n0;-><init>(Lf/h/e/d1;Lf/h/e/l;Lf/h/e/k0;)V

    :goto_1
    move-object v0, v3

    goto/16 :goto_4

    :cond_2
    sget-object v0, Lf/h/e/z0;->b:Lf/h/e/d1;

    sget-object v1, Lf/h/e/n;->b:Lf/h/e/l;

    if-eqz v1, :cond_3

    invoke-interface {v2}, Lf/h/e/i0;->b()Lf/h/e/k0;

    move-result-object v2

    new-instance v3, Lf/h/e/n0;

    invoke-direct {v3, v0, v1, v2}, Lf/h/e/n0;-><init>(Lf/h/e/d1;Lf/h/e/l;Lf/h/e/k0;)V

    goto :goto_1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    sget-object v0, Lf/h/e/t0;->d:Lf/h/e/t0;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_7

    invoke-interface {v2}, Lf/h/e/i0;->c()Lf/h/e/t0;

    move-result-object v1

    if-ne v1, v0, :cond_5

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_6

    sget-object v3, Lf/h/e/q0;->b:Lf/h/e/o0;

    sget-object v4, Lf/h/e/a0;->b:Lf/h/e/a0;

    sget-object v5, Lf/h/e/z0;->d:Lf/h/e/d1;

    sget-object v0, Lf/h/e/n;->a:Lf/h/e/l;

    sget-object v6, Lf/h/e/n;->a:Lf/h/e/l;

    sget-object v7, Lf/h/e/h0;->b:Lf/h/e/f0;

    invoke-static/range {v2 .. v7}, Lf/h/e/m0;->s(Lf/h/e/i0;Lf/h/e/o0;Lf/h/e/a0;Lf/h/e/d1;Lf/h/e/l;Lf/h/e/f0;)Lf/h/e/m0;

    move-result-object v0

    goto :goto_4

    :cond_6
    sget-object v3, Lf/h/e/q0;->b:Lf/h/e/o0;

    sget-object v4, Lf/h/e/a0;->b:Lf/h/e/a0;

    sget-object v5, Lf/h/e/z0;->d:Lf/h/e/d1;

    const/4 v6, 0x0

    sget-object v7, Lf/h/e/h0;->b:Lf/h/e/f0;

    invoke-static/range {v2 .. v7}, Lf/h/e/m0;->s(Lf/h/e/i0;Lf/h/e/o0;Lf/h/e/a0;Lf/h/e/d1;Lf/h/e/l;Lf/h/e/f0;)Lf/h/e/m0;

    move-result-object v0

    goto :goto_4

    :cond_7
    invoke-interface {v2}, Lf/h/e/i0;->c()Lf/h/e/t0;

    move-result-object v1

    if-ne v1, v0, :cond_8

    goto :goto_3

    :cond_8
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_a

    sget-object v0, Lf/h/e/q0;->a:Lf/h/e/o0;

    sget-object v4, Lf/h/e/a0;->a:Lf/h/e/a0;

    sget-object v5, Lf/h/e/z0;->b:Lf/h/e/d1;

    sget-object v6, Lf/h/e/n;->b:Lf/h/e/l;

    if-eqz v6, :cond_9

    sget-object v7, Lf/h/e/h0;->a:Lf/h/e/f0;

    move-object v3, v0

    invoke-static/range {v2 .. v7}, Lf/h/e/m0;->s(Lf/h/e/i0;Lf/h/e/o0;Lf/h/e/a0;Lf/h/e/d1;Lf/h/e/l;Lf/h/e/f0;)Lf/h/e/m0;

    move-result-object v0

    goto :goto_4

    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_a
    sget-object v3, Lf/h/e/q0;->a:Lf/h/e/o0;

    sget-object v4, Lf/h/e/a0;->a:Lf/h/e/a0;

    sget-object v5, Lf/h/e/z0;->c:Lf/h/e/d1;

    const/4 v6, 0x0

    sget-object v7, Lf/h/e/h0;->a:Lf/h/e/f0;

    invoke-static/range {v2 .. v7}, Lf/h/e/m0;->s(Lf/h/e/i0;Lf/h/e/o0;Lf/h/e/a0;Lf/h/e/d1;Lf/h/e/l;Lf/h/e/f0;)Lf/h/e/m0;

    move-result-object v0

    :goto_4
    iget-object v1, p0, Lf/h/e/u0;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/e/x0;

    if-eqz p1, :cond_b

    move-object v0, p1

    :cond_b
    return-object v0
.end method

.method public b(Ljava/lang/Object;)Lf/h/e/x0;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lf/h/e/x0<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/h/e/u0;->a(Ljava/lang/Class;)Lf/h/e/x0;

    move-result-object p1

    return-object p1
.end method
