.class public Lf/h/e/f1;
.super Lf/h/e/d1;
.source "UnknownFieldSetLiteSchema.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/d1<",
        "Lf/h/e/e1;",
        "Lf/h/e/e1;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/e/d1;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lf/h/e/r;

    iget-object p1, p1, Lf/h/e/r;->unknownFields:Lf/h/e/e1;

    return-object p1
.end method

.method public b(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lf/h/e/e1;

    invoke-virtual {p1}, Lf/h/e/e1;->a()I

    move-result p1

    return p1
.end method

.method public c(Ljava/lang/Object;)I
    .locals 7

    check-cast p1, Lf/h/e/e1;

    iget v0, p1, Lf/h/e/e1;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v2, p1, Lf/h/e/e1;->a:I

    if-ge v0, v2, :cond_1

    iget-object v2, p1, Lf/h/e/e1;->b:[I

    aget v2, v2, v0

    const/4 v3, 0x3

    ushr-int/2addr v2, v3

    iget-object v4, p1, Lf/h/e/e1;->c:[Ljava/lang/Object;

    aget-object v4, v4, v0

    check-cast v4, Lf/h/e/g;

    const/4 v5, 0x1

    invoke-static {v5}, Lcom/google/protobuf/CodedOutputStream;->w(I)I

    move-result v5

    const/4 v6, 0x2

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->x(II)I

    move-result v2

    add-int/2addr v2, v5

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->b(ILf/h/e/g;)I

    move-result v3

    add-int/2addr v3, v2

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iput v1, p1, Lf/h/e/e1;->d:I

    move v0, v1

    :goto_1
    return v0
.end method

.method public d(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lf/h/e/r;

    iget-object p1, p1, Lf/h/e/r;->unknownFields:Lf/h/e/e1;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lf/h/e/e1;->e:Z

    return-void
.end method

.method public e(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    check-cast p1, Lf/h/e/e1;

    check-cast p2, Lf/h/e/e1;

    sget-object v0, Lf/h/e/e1;->f:Lf/h/e/e1;

    invoke-virtual {p2, v0}, Lf/h/e/e1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p1, Lf/h/e/e1;->a:I

    iget v1, p2, Lf/h/e/e1;->a:I

    add-int/2addr v0, v1

    iget-object v1, p1, Lf/h/e/e1;->b:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iget-object v2, p2, Lf/h/e/e1;->b:[I

    iget v3, p1, Lf/h/e/e1;->a:I

    iget v4, p2, Lf/h/e/e1;->a:I

    const/4 v5, 0x0

    invoke-static {v2, v5, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p1, Lf/h/e/e1;->c:[Ljava/lang/Object;

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p2, Lf/h/e/e1;->c:[Ljava/lang/Object;

    iget p1, p1, Lf/h/e/e1;->a:I

    iget p2, p2, Lf/h/e/e1;->a:I

    invoke-static {v3, v5, v2, p1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance p1, Lf/h/e/e1;

    const/4 p2, 0x1

    invoke-direct {p1, v0, v1, v2, p2}, Lf/h/e/e1;-><init>(I[I[Ljava/lang/Object;Z)V

    :goto_0
    return-object p1
.end method

.method public f(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lf/h/e/e1;

    check-cast p1, Lf/h/e/r;

    iput-object p2, p1, Lf/h/e/r;->unknownFields:Lf/h/e/e1;

    return-void
.end method

.method public g(Ljava/lang/Object;Lf/h/e/m1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/e/e1;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    iget v1, p1, Lf/h/e/e1;->a:I

    if-ge v0, v1, :cond_0

    iget-object v1, p1, Lf/h/e/e1;->b:[I

    aget v1, v1, v0

    ushr-int/lit8 v1, v1, 0x3

    iget-object v2, p1, Lf/h/e/e1;->c:[Ljava/lang/Object;

    aget-object v2, v2, v0

    move-object v3, p2

    check-cast v3, Lf/h/e/i;

    invoke-virtual {v3, v1, v2}, Lf/h/e/i;->e(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public h(Ljava/lang/Object;Lf/h/e/m1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lf/h/e/e1;

    invoke-virtual {p1, p2}, Lf/h/e/e1;->c(Lf/h/e/m1;)V

    return-void
.end method
