.class public Lf/h/e/f;
.super Lf/h/e/g$a;
.source "ByteString.java"


# instance fields
.field public d:I

.field public final e:I

.field public final synthetic f:Lf/h/e/g;


# direct methods
.method public constructor <init>(Lf/h/e/g;)V
    .locals 1

    iput-object p1, p0, Lf/h/e/f;->f:Lf/h/e/g;

    invoke-direct {p0}, Lf/h/e/g$a;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lf/h/e/f;->d:I

    invoke-virtual {p1}, Lf/h/e/g;->size()I

    move-result p1

    iput p1, p0, Lf/h/e/f;->e:I

    return-void
.end method


# virtual methods
.method public a()B
    .locals 2

    iget v0, p0, Lf/h/e/f;->d:I

    iget v1, p0, Lf/h/e/f;->e:I

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lf/h/e/f;->d:I

    iget-object v1, p0, Lf/h/e/f;->f:Lf/h/e/g;

    invoke-virtual {v1, v0}, Lf/h/e/g;->e(I)B

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lf/h/e/f;->d:I

    iget v1, p0, Lf/h/e/f;->e:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
