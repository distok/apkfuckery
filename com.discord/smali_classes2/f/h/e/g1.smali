.class public Lf/h/e/g1;
.super Ljava/util/AbstractList;
.source "UnmodifiableLazyStringList.java"

# interfaces
.implements Lf/h/e/z;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList<",
        "Ljava/lang/String;",
        ">;",
        "Lf/h/e/z;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field public final d:Lf/h/e/z;


# direct methods
.method public constructor <init>(Lf/h/e/z;)V
    .locals 0

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    iput-object p1, p0, Lf/h/e/g1;->d:Lf/h/e/z;

    return-void
.end method


# virtual methods
.method public D(Lf/h/e/g;)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public U()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lf/h/e/g1;->d:Lf/h/e/z;

    invoke-interface {v0}, Lf/h/e/z;->U()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf/h/e/g1;->d:Lf/h/e/z;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/e/g1$b;

    invoke-direct {v0, p0}, Lf/h/e/g1$b;-><init>(Lf/h/e/g1;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/h/e/g1$a;

    invoke-direct {v0, p0, p1}, Lf/h/e/g1$a;-><init>(Lf/h/e/g1;I)V

    return-object v0
.end method

.method public p0()Lf/h/e/z;
    .locals 0

    return-object p0
.end method

.method public q(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf/h/e/g1;->d:Lf/h/e/z;

    invoke-interface {v0, p1}, Lf/h/e/z;->q(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lf/h/e/g1;->d:Lf/h/e/z;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
