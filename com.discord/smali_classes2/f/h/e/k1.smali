.class public enum Lf/h/e/k1;
.super Ljava/lang/Enum;
.source "WireFormat.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/h/e/k1;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/h/e/k1;

.field public static final enum e:Lf/h/e/k1;

.field public static final enum f:Lf/h/e/k1;

.field public static final enum g:Lf/h/e/k1;

.field public static final enum h:Lf/h/e/k1;

.field public static final enum i:Lf/h/e/k1;

.field public static final enum j:Lf/h/e/k1;

.field public static final enum k:Lf/h/e/k1;

.field public static final enum l:Lf/h/e/k1;

.field public static final enum m:Lf/h/e/k1;

.field public static final enum n:Lf/h/e/k1;

.field public static final enum o:Lf/h/e/k1;

.field public static final enum p:Lf/h/e/k1;

.field public static final enum q:Lf/h/e/k1;

.field public static final enum r:Lf/h/e/k1;

.field public static final enum s:Lf/h/e/k1;

.field public static final enum t:Lf/h/e/k1;

.field public static final enum u:Lf/h/e/k1;

.field public static final synthetic v:[Lf/h/e/k1;


# instance fields
.field private final javaType:Lf/h/e/l1;

.field private final wireType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 22

    new-instance v0, Lf/h/e/k1;

    sget-object v1, Lf/h/e/l1;->g:Lf/h/e/l1;

    const-string v2, "DOUBLE"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v0, v2, v3, v1, v4}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v0, Lf/h/e/k1;->d:Lf/h/e/k1;

    new-instance v1, Lf/h/e/k1;

    sget-object v2, Lf/h/e/l1;->f:Lf/h/e/l1;

    const-string v5, "FLOAT"

    const/4 v6, 0x5

    invoke-direct {v1, v5, v4, v2, v6}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v1, Lf/h/e/k1;->e:Lf/h/e/k1;

    new-instance v2, Lf/h/e/k1;

    sget-object v5, Lf/h/e/l1;->e:Lf/h/e/l1;

    const-string v7, "INT64"

    const/4 v8, 0x2

    invoke-direct {v2, v7, v8, v5, v3}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v2, Lf/h/e/k1;->f:Lf/h/e/k1;

    new-instance v7, Lf/h/e/k1;

    const-string v9, "UINT64"

    const/4 v10, 0x3

    invoke-direct {v7, v9, v10, v5, v3}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v7, Lf/h/e/k1;->g:Lf/h/e/k1;

    new-instance v9, Lf/h/e/k1;

    sget-object v11, Lf/h/e/l1;->d:Lf/h/e/l1;

    const-string v12, "INT32"

    const/4 v13, 0x4

    invoke-direct {v9, v12, v13, v11, v3}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v9, Lf/h/e/k1;->h:Lf/h/e/k1;

    new-instance v12, Lf/h/e/k1;

    const-string v14, "FIXED64"

    invoke-direct {v12, v14, v6, v5, v4}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v12, Lf/h/e/k1;->i:Lf/h/e/k1;

    new-instance v14, Lf/h/e/k1;

    const-string v15, "FIXED32"

    const/4 v13, 0x6

    invoke-direct {v14, v15, v13, v11, v6}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v14, Lf/h/e/k1;->j:Lf/h/e/k1;

    new-instance v15, Lf/h/e/k1;

    sget-object v13, Lf/h/e/l1;->h:Lf/h/e/l1;

    const-string v4, "BOOL"

    const/4 v6, 0x7

    invoke-direct {v15, v4, v6, v13, v3}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v15, Lf/h/e/k1;->k:Lf/h/e/k1;

    new-instance v4, Lf/h/e/k1$a;

    sget-object v13, Lf/h/e/l1;->i:Lf/h/e/l1;

    const-string v6, "STRING"

    const/16 v3, 0x8

    invoke-direct {v4, v6, v3, v13, v8}, Lf/h/e/k1$a;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v4, Lf/h/e/k1;->l:Lf/h/e/k1;

    new-instance v6, Lf/h/e/k1$b;

    sget-object v13, Lf/h/e/l1;->l:Lf/h/e/l1;

    const-string v3, "GROUP"

    const/16 v8, 0x9

    invoke-direct {v6, v3, v8, v13, v10}, Lf/h/e/k1$b;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v6, Lf/h/e/k1;->m:Lf/h/e/k1;

    new-instance v3, Lf/h/e/k1$c;

    const-string v8, "MESSAGE"

    const/16 v10, 0xa

    move-object/from16 v16, v6

    const/4 v6, 0x2

    invoke-direct {v3, v8, v10, v13, v6}, Lf/h/e/k1$c;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v3, Lf/h/e/k1;->n:Lf/h/e/k1;

    new-instance v8, Lf/h/e/k1$d;

    sget-object v13, Lf/h/e/l1;->j:Lf/h/e/l1;

    const-string v10, "BYTES"

    move-object/from16 v17, v3

    const/16 v3, 0xb

    invoke-direct {v8, v10, v3, v13, v6}, Lf/h/e/k1$d;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v8, Lf/h/e/k1;->o:Lf/h/e/k1;

    new-instance v6, Lf/h/e/k1;

    const-string v10, "UINT32"

    const/16 v13, 0xc

    const/4 v3, 0x0

    invoke-direct {v6, v10, v13, v11, v3}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v6, Lf/h/e/k1;->p:Lf/h/e/k1;

    new-instance v10, Lf/h/e/k1;

    sget-object v13, Lf/h/e/l1;->k:Lf/h/e/l1;

    move-object/from16 v18, v6

    const-string v6, "ENUM"

    move-object/from16 v19, v8

    const/16 v8, 0xd

    invoke-direct {v10, v6, v8, v13, v3}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v10, Lf/h/e/k1;->q:Lf/h/e/k1;

    new-instance v3, Lf/h/e/k1;

    const-string v6, "SFIXED32"

    const/16 v13, 0xe

    const/4 v8, 0x5

    invoke-direct {v3, v6, v13, v11, v8}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v3, Lf/h/e/k1;->r:Lf/h/e/k1;

    new-instance v6, Lf/h/e/k1;

    const-string v8, "SFIXED64"

    const/16 v13, 0xf

    move-object/from16 v20, v3

    const/4 v3, 0x1

    invoke-direct {v6, v8, v13, v5, v3}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v6, Lf/h/e/k1;->s:Lf/h/e/k1;

    new-instance v3, Lf/h/e/k1;

    const-string v8, "SINT32"

    const/16 v13, 0x10

    move-object/from16 v21, v6

    const/4 v6, 0x0

    invoke-direct {v3, v8, v13, v11, v6}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v3, Lf/h/e/k1;->t:Lf/h/e/k1;

    new-instance v8, Lf/h/e/k1;

    const-string v11, "SINT64"

    const/16 v13, 0x11

    invoke-direct {v8, v11, v13, v5, v6}, Lf/h/e/k1;-><init>(Ljava/lang/String;ILf/h/e/l1;I)V

    sput-object v8, Lf/h/e/k1;->u:Lf/h/e/k1;

    const/16 v5, 0x12

    new-array v5, v5, [Lf/h/e/k1;

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v1, v5, v0

    const/4 v0, 0x2

    aput-object v2, v5, v0

    const/4 v0, 0x3

    aput-object v7, v5, v0

    const/4 v0, 0x4

    aput-object v9, v5, v0

    const/4 v0, 0x5

    aput-object v12, v5, v0

    const/4 v0, 0x6

    aput-object v14, v5, v0

    const/4 v0, 0x7

    aput-object v15, v5, v0

    const/16 v0, 0x8

    aput-object v4, v5, v0

    const/16 v0, 0x9

    aput-object v16, v5, v0

    const/16 v0, 0xa

    aput-object v17, v5, v0

    const/16 v0, 0xb

    aput-object v19, v5, v0

    const/16 v0, 0xc

    aput-object v18, v5, v0

    const/16 v0, 0xd

    aput-object v10, v5, v0

    const/16 v0, 0xe

    aput-object v20, v5, v0

    const/16 v0, 0xf

    aput-object v21, v5, v0

    const/16 v0, 0x10

    aput-object v3, v5, v0

    aput-object v8, v5, v13

    sput-object v5, Lf/h/e/k1;->v:[Lf/h/e/k1;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILf/h/e/l1;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/e/l1;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lf/h/e/k1;->javaType:Lf/h/e/l1;

    iput p4, p0, Lf/h/e/k1;->wireType:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILf/h/e/l1;ILf/h/e/j1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lf/h/e/k1;->javaType:Lf/h/e/l1;

    iput p4, p0, Lf/h/e/k1;->wireType:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/h/e/k1;
    .locals 1

    const-class v0, Lf/h/e/k1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/h/e/k1;

    return-object p0
.end method

.method public static values()[Lf/h/e/k1;
    .locals 1

    sget-object v0, Lf/h/e/k1;->v:[Lf/h/e/k1;

    invoke-virtual {v0}, [Lf/h/e/k1;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/h/e/k1;

    return-object v0
.end method


# virtual methods
.method public f()Lf/h/e/l1;
    .locals 1

    iget-object v0, p0, Lf/h/e/k1;->javaType:Lf/h/e/l1;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lf/h/e/k1;->wireType:I

    return v0
.end method
