.class public final Lf/h/e/a0$c;
.super Lf/h/e/a0;
.source "ListFieldSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/e/a0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# direct methods
.method public constructor <init>(Lf/h/e/a0$a;)V
    .locals 0

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lf/h/e/a0;-><init>(Lf/h/e/a0$a;)V

    return-void
.end method

.method public static c(Ljava/lang/Object;J)Lf/h/e/t$d;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "J)",
            "Lf/h/e/t$d<",
            "TE;>;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lf/h/e/h1;->n(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lf/h/e/t$d;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/Object;J)V
    .locals 0

    invoke-static {p1, p2, p3}, Lf/h/e/a0$c;->c(Ljava/lang/Object;J)Lf/h/e/t$d;

    move-result-object p1

    invoke-interface {p1}, Lf/h/e/t$d;->w()V

    return-void
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "J)V"
        }
    .end annotation

    invoke-static {p1, p3, p4}, Lf/h/e/a0$c;->c(Ljava/lang/Object;J)Lf/h/e/t$d;

    move-result-object v0

    invoke-static {p2, p3, p4}, Lf/h/e/a0$c;->c(Ljava/lang/Object;J)Lf/h/e/t$d;

    move-result-object p2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v1, :cond_1

    if-lez v2, :cond_1

    invoke-interface {v0}, Lf/h/e/t$d;->G0()Z

    move-result v3

    if-nez v3, :cond_0

    add-int/2addr v2, v1

    invoke-interface {v0, v2}, Lf/h/e/t$d;->C(I)Lf/h/e/t$d;

    move-result-object v0

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    if-lez v1, :cond_2

    move-object p2, v0

    :cond_2
    sget-object v0, Lf/h/e/h1;->f:Lf/h/e/h1$e;

    invoke-virtual {v0, p1, p3, p4, p2}, Lf/h/e/h1$e;->q(Ljava/lang/Object;JLjava/lang/Object;)V

    return-void
.end method
