.class public final Lf/h/e/m;
.super Lf/h/e/l;
.source "ExtensionSchemaLite.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/e/l<",
        "Lf/h/e/r$d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/h/e/l;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map$Entry;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)I"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/e/r$d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    return p1
.end method

.method public b(Ljava/lang/Object;)Lf/h/e/o;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/e/o<",
            "Lf/h/e/r$d;",
            ">;"
        }
    .end annotation

    check-cast p1, Lf/h/e/r$c;

    iget-object p1, p1, Lf/h/e/r$c;->extensions:Lf/h/e/o;

    return-object p1
.end method

.method public c(Ljava/lang/Object;)Lf/h/e/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lf/h/e/o<",
            "Lf/h/e/r$d;",
            ">;"
        }
    .end annotation

    check-cast p1, Lf/h/e/r$c;

    iget-object v0, p1, Lf/h/e/r$c;->extensions:Lf/h/e/o;

    iget-boolean v1, v0, Lf/h/e/o;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lf/h/e/o;->a()Lf/h/e/o;

    move-result-object v0

    iput-object v0, p1, Lf/h/e/r$c;->extensions:Lf/h/e/o;

    :cond_0
    iget-object p1, p1, Lf/h/e/r$c;->extensions:Lf/h/e/o;

    return-object p1
.end method

.method public d(Lf/h/e/k0;)Z
    .locals 0

    instance-of p1, p1, Lf/h/e/r$c;

    return p1
.end method

.method public e(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lf/h/e/r$c;

    iget-object p1, p1, Lf/h/e/r$c;->extensions:Lf/h/e/o;

    invoke-virtual {p1}, Lf/h/e/o;->l()V

    return-void
.end method

.method public f(Lf/h/e/m1;Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/e/m1;",
            "Ljava/util/Map$Entry<",
            "**>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/h/e/r$d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    throw p1
.end method
