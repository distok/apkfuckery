.class public abstract Lf/h/e/r;
.super Lf/h/e/a;
.source "GeneratedMessageLite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/h/e/r$b;,
        Lf/h/e/r$d;,
        Lf/h/e/r$c;,
        Lf/h/e/r$a;,
        Lf/h/e/r$e;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lf/h/e/r<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Lf/h/e/r$a<",
        "TMessageType;TBuilderType;>;>",
        "Lf/h/e/a<",
        "TMessageType;TBuilderType;>;"
    }
.end annotation


# static fields
.field private static defaultInstanceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lf/h/e/r<",
            "**>;>;"
        }
    .end annotation
.end field


# instance fields
.field public memoizedSerializedSize:I

.field public unknownFields:Lf/h/e/e1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lf/h/e/r;->defaultInstanceMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/a;-><init>()V

    sget-object v0, Lf/h/e/e1;->f:Lf/h/e/e1;

    iput-object v0, p0, Lf/h/e/r;->unknownFields:Lf/h/e/e1;

    const/4 v0, -0x1

    iput v0, p0, Lf/h/e/r;->memoizedSerializedSize:I

    return-void
.end method

.method public static u(Ljava/lang/Class;)Lf/h/e/r;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lf/h/e/r<",
            "**>;>(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    sget-object v0, Lf/h/e/r;->defaultInstanceMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r;

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v0, Lf/h/e/r;->defaultInstanceMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r;

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Class initialization cannot fail."

    invoke-direct {v0, v1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    invoke-static {p0}, Lf/h/e/h1;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r;

    invoke-virtual {v0}, Lf/h/e/r;->v()Lf/h/e/r;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lf/h/e/r;->defaultInstanceMap:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0}, Ljava/lang/IllegalStateException;-><init>()V

    throw p0

    :cond_2
    :goto_1
    return-object v0
.end method

.method public static varargs w(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    instance-of p1, p0, Ljava/lang/RuntimeException;

    if-nez p1, :cond_1

    instance-of p1, p0, Ljava/lang/Error;

    if-eqz p1, :cond_0

    check-cast p0, Ljava/lang/Error;

    throw p0

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Unexpected exception thrown by generated accessor method."

    invoke-direct {p1, p2, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :cond_1
    check-cast p0, Ljava/lang/RuntimeException;

    throw p0

    :catch_1
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Couldn\'t use Java reflection to implement protocol message reflection."

    invoke-direct {p1, p2, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static x(Lf/h/e/t$d;)Lf/h/e/t$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/h/e/t$d<",
            "TE;>;)",
            "Lf/h/e/t$d<",
            "TE;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :cond_0
    mul-int/lit8 v0, v0, 0x2

    :goto_0
    invoke-interface {p0, v0}, Lf/h/e/t$d;->C(I)Lf/h/e/t$d;

    move-result-object p0

    return-object p0
.end method

.method public static z(Ljava/lang/Class;Lf/h/e/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lf/h/e/r<",
            "**>;>(",
            "Ljava/lang/Class<",
            "TT;>;TT;)V"
        }
    .end annotation

    sget-object v0, Lf/h/e/r;->defaultInstanceMap:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 4

    sget-object v0, Lf/h/e/r$e;->d:Lf/h/e/r$e;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_1

    :cond_0
    if-nez v0, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    sget-object v0, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v0, p0}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v0

    invoke-interface {v0, p0}, Lf/h/e/x0;->f(Ljava/lang/Object;)Z

    move-result v2

    sget-object v0, Lf/h/e/r$e;->e:Lf/h/e/r$e;

    if-eqz v2, :cond_2

    move-object v3, p0

    goto :goto_0

    :cond_2
    move-object v3, v1

    :goto_0
    invoke-virtual {p0, v0, v3, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return v2
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lf/h/e/r;->memoizedSerializedSize:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-virtual {p0}, Lf/h/e/r;->v()Lf/h/e/r;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    sget-object v0, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v0, p0}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v0

    check-cast p1, Lf/h/e/r;

    invoke-interface {v0, p0, p1}, Lf/h/e/x0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic f()Lf/h/e/k0;
    .locals 1

    invoke-virtual {p0}, Lf/h/e/r;->v()Lf/h/e/r;

    move-result-object v0

    return-object v0
.end method

.method public g()Lf/h/e/k0$a;
    .locals 2

    sget-object v0, Lf/h/e/r$e;->h:Lf/h/e/r$e;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r$a;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v1, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    invoke-virtual {v0, v1, p0}, Lf/h/e/r$a;->u(Lf/h/e/r;Lf/h/e/r;)V

    return-object v0
.end method

.method public h(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v0, p0}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v0

    iget-object v1, p1, Lcom/google/protobuf/CodedOutputStream;->a:Lf/h/e/i;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lf/h/e/i;

    invoke-direct {v1, p1}, Lf/h/e/i;-><init>(Lcom/google/protobuf/CodedOutputStream;)V

    :goto_0
    invoke-interface {v0, p0, v1}, Lf/h/e/x0;->b(Ljava/lang/Object;Lf/h/e/m1;)V

    return-void
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lf/h/e/a;->memoizedHashCode:I

    if-eqz v0, :cond_0

    return v0

    :cond_0
    sget-object v0, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v0, p0}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v0

    invoke-interface {v0, p0}, Lf/h/e/x0;->d(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lf/h/e/a;->memoizedHashCode:I

    return v0
.end method

.method public n()I
    .locals 2

    iget v0, p0, Lf/h/e/r;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lf/h/e/u0;->c:Lf/h/e/u0;

    invoke-virtual {v0, p0}, Lf/h/e/u0;->b(Ljava/lang/Object;)Lf/h/e/x0;

    move-result-object v0

    invoke-interface {v0, p0}, Lf/h/e/x0;->g(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lf/h/e/r;->memoizedSerializedSize:I

    :cond_0
    iget v0, p0, Lf/h/e/r;->memoizedSerializedSize:I

    return v0
.end method

.method public q(I)V
    .locals 0

    iput p1, p0, Lf/h/e/r;->memoizedSerializedSize:I

    return-void
.end method

.method public final s()Lf/h/e/r$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MessageType:",
            "Lf/h/e/r<",
            "TMessageType;TBuilderType;>;BuilderType:",
            "Lf/h/e/r$a<",
            "TMessageType;TBuilderType;>;>()TBuilderType;"
        }
    .end annotation

    sget-object v0, Lf/h/e/r$e;->h:Lf/h/e/r$e;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r$a;

    return-object v0
.end method

.method public abstract t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-static {p0, v1, v0}, Lf/h/a/f/f/n/g;->Z(Lf/h/e/k0;Ljava/lang/StringBuilder;I)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lf/h/e/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    sget-object v0, Lf/h/e/r$e;->i:Lf/h/e/r$e;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r;

    return-object v0
.end method

.method public final y()Lf/h/e/r$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation

    sget-object v0, Lf/h/e/r$e;->h:Lf/h/e/r$e;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r$a;

    return-object v0
.end method
