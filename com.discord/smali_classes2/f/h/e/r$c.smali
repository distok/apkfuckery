.class public abstract Lf/h/e/r$c;
.super Lf/h/e/r;
.source "GeneratedMessageLite.java"

# interfaces
.implements Lf/h/e/l0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/h/e/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lf/h/e/r$c<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Ljava/lang/Object<",
        "TMessageType;TBuilderType;>;>",
        "Lf/h/e/r<",
        "TMessageType;TBuilderType;>;",
        "Ljava/lang/Object<",
        "TMessageType;TBuilderType;>;"
    }
.end annotation


# instance fields
.field public extensions:Lf/h/e/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/e/o<",
            "Lf/h/e/r$d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/e/r;-><init>()V

    sget-object v0, Lf/h/e/o;->d:Lf/h/e/o;

    iput-object v0, p0, Lf/h/e/r$c;->extensions:Lf/h/e/o;

    return-void
.end method


# virtual methods
.method public bridge synthetic f()Lf/h/e/k0;
    .locals 1

    invoke-virtual {p0}, Lf/h/e/r;->v()Lf/h/e/r;

    move-result-object v0

    return-object v0
.end method

.method public g()Lf/h/e/k0$a;
    .locals 2

    sget-object v0, Lf/h/e/r$e;->h:Lf/h/e/r$e;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lf/h/e/r;->t(Lf/h/e/r$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/e/r$a;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v1, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    invoke-virtual {v0, v1, p0}, Lf/h/e/r$a;->u(Lf/h/e/r;Lf/h/e/r;)V

    return-object v0
.end method
