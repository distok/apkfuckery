.class public Lf/h/e/x;
.super Ljava/lang/Object;
.source "LazyFieldLite.java"


# instance fields
.field public volatile a:Lf/h/e/k0;

.field public volatile b:Lf/h/e/g;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lf/h/e/k;->a()Lf/h/e/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/h/e/k0;)Lf/h/e/k0;
    .locals 1

    iget-object v0, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    if-eqz v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_1
    :try_start_1
    iput-object p1, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    sget-object v0, Lf/h/e/g;->d:Lf/h/e/g;

    iput-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;
    :try_end_1
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    iput-object p1, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    sget-object p1, Lf/h/e/g;->d:Lf/h/e/g;

    iput-object p1, p0, Lf/h/e/x;->b:Lf/h/e/g;

    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    iget-object p1, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    return-object p1

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method

.method public b()Lf/h/e/g;
    .locals 1

    iget-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;

    return-object v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;

    monitor-exit p0

    return-object v0

    :cond_1
    iget-object v0, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    if-nez v0, :cond_2

    sget-object v0, Lf/h/e/g;->d:Lf/h/e/g;

    iput-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    invoke-interface {v0}, Lf/h/e/k0;->k()Lf/h/e/g;

    move-result-object v0

    iput-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;

    :goto_0
    iget-object v0, p0, Lf/h/e/x;->b:Lf/h/e/g;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lf/h/e/x;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    check-cast p1, Lf/h/e/x;

    iget-object v0, p0, Lf/h/e/x;->a:Lf/h/e/k0;

    iget-object v1, p1, Lf/h/e/x;->a:Lf/h/e/k0;

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lf/h/e/x;->b()Lf/h/e/g;

    move-result-object v0

    invoke-virtual {p1}, Lf/h/e/x;->b()Lf/h/e/g;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/e/g;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lf/h/e/l0;->f()Lf/h/e/k0;

    move-result-object v1

    invoke-virtual {p1, v1}, Lf/h/e/x;->a(Lf/h/e/k0;)Lf/h/e/k0;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_4
    invoke-interface {v1}, Lf/h/e/l0;->f()Lf/h/e/k0;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/h/e/x;->a(Lf/h/e/k0;)Lf/h/e/k0;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
