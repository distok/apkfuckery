.class public final enum Lf/g/b/a/b$a;
.super Ljava/lang/Enum;
.source "CacheEventListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/b/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/g/b/a/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/g/b/a/b$a;

.field public static final enum e:Lf/g/b/a/b$a;

.field public static final enum f:Lf/g/b/a/b$a;

.field public static final enum g:Lf/g/b/a/b$a;

.field public static final synthetic h:[Lf/g/b/a/b$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/g/b/a/b$a;

    const-string v1, "CACHE_FULL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/g/b/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/g/b/a/b$a;->d:Lf/g/b/a/b$a;

    new-instance v1, Lf/g/b/a/b$a;

    const-string v3, "CONTENT_STALE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/g/b/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/g/b/a/b$a;->e:Lf/g/b/a/b$a;

    new-instance v3, Lf/g/b/a/b$a;

    const-string v5, "USER_FORCED"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/g/b/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/g/b/a/b$a;->f:Lf/g/b/a/b$a;

    new-instance v5, Lf/g/b/a/b$a;

    const-string v7, "CACHE_MANAGER_TRIMMED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/g/b/a/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/g/b/a/b$a;->g:Lf/g/b/a/b$a;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/g/b/a/b$a;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/g/b/a/b$a;->h:[Lf/g/b/a/b$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/g/b/a/b$a;
    .locals 1

    const-class v0, Lf/g/b/a/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/g/b/a/b$a;

    return-object p0
.end method

.method public static values()[Lf/g/b/a/b$a;
    .locals 1

    sget-object v0, Lf/g/b/a/b$a;->h:[Lf/g/b/a/b$a;

    invoke-virtual {v0}, [Lf/g/b/a/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/g/b/a/b$a;

    return-object v0
.end method
