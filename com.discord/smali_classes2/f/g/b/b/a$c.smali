.class public Lf/g/b/b/a$c;
.super Ljava/lang/Object;
.source "DefaultDiskStorage.java"

# interfaces
.implements Lf/g/b/b/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/b/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lf/g/a/a;

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/File;Lf/g/b/b/a$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/b/b/a$c;->a:Ljava/lang/String;

    invoke-static {p2}, Lf/g/a/a;->a(Ljava/io/File;)Lf/g/a/a;

    move-result-object p1

    iput-object p1, p0, Lf/g/b/b/a$c;->b:Lf/g/a/a;

    const-wide/16 p1, -0x1

    iput-wide p1, p0, Lf/g/b/b/a$c;->c:J

    iput-wide p1, p0, Lf/g/b/b/a$c;->d:J

    return-void
.end method


# virtual methods
.method public a()J
    .locals 5

    iget-wide v0, p0, Lf/g/b/b/a$c;->d:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    iget-object v0, p0, Lf/g/b/b/a$c;->b:Lf/g/a/a;

    iget-object v0, v0, Lf/g/a/a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lf/g/b/b/a$c;->d:J

    :cond_0
    iget-wide v0, p0, Lf/g/b/b/a$c;->d:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/g/b/b/a$c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 5

    iget-wide v0, p0, Lf/g/b/b/a$c;->c:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    iget-object v0, p0, Lf/g/b/b/a$c;->b:Lf/g/a/a;

    invoke-virtual {v0}, Lf/g/a/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lf/g/b/b/a$c;->c:J

    :cond_0
    iget-wide v0, p0, Lf/g/b/b/a$c;->c:J

    return-wide v0
.end method
