.class public Lf/g/b/b/b$a;
.super Ljava/lang/Object;
.source "DefaultEntryEvictionComparatorSupplier.java"

# interfaces
.implements Lf/g/b/b/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/b/b/b;->get()Lf/g/b/b/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# direct methods
.method public constructor <init>(Lf/g/b/b/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    check-cast p1, Lf/g/b/b/c$a;

    check-cast p2, Lf/g/b/b/c$a;

    invoke-interface {p1}, Lf/g/b/b/c$a;->a()J

    move-result-wide v0

    invoke-interface {p2}, Lf/g/b/b/c$a;->a()J

    move-result-wide p1

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    cmp-long v2, p1, v0

    if-nez v2, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    return p1
.end method
