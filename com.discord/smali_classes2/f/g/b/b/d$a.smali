.class public Lf/g/b/b/d$a;
.super Ljava/lang/Object;
.source "DiskStorageCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/b/b/d;-><init>(Lf/g/b/b/c;Lf/g/b/b/g;Lf/g/b/b/d$c;Lf/g/b/a/b;Lf/g/b/a/a;Lf/g/d/a/a;Ljava/util/concurrent/Executor;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/g/b/b/d;


# direct methods
.method public constructor <init>(Lf/g/b/b/d;)V
    .locals 0

    iput-object p1, p0, Lf/g/b/b/d$a;->d:Lf/g/b/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lf/g/b/b/d$a;->d:Lf/g/b/b/d;

    iget-object v0, v0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/g/b/b/d$a;->d:Lf/g/b/b/d;

    invoke-virtual {v1}, Lf/g/b/b/d;->e()Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lf/g/b/b/d$a;->d:Lf/g/b/b/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/g/b/b/d$a;->d:Lf/g/b/b/d;

    iget-object v0, v0, Lf/g/b/b/d;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
