.class public Lf/g/b/b/e;
.super Ljava/lang/Object;
.source "DynamicDefaultDiskStorage.java"

# interfaces
.implements Lf/g/b/b/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/b/b/e$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Lf/g/b/a/a;

.field public volatile e:Lf/g/b/b/e$a;


# direct methods
.method public constructor <init>(ILcom/facebook/common/internal/Supplier;Ljava/lang/String;Lf/g/b/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/io/File;",
            ">;",
            "Ljava/lang/String;",
            "Lf/g/b/a/a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/g/b/b/e;->a:I

    iput-object p4, p0, Lf/g/b/b/e;->d:Lf/g/b/a/a;

    iput-object p2, p0, Lf/g/b/b/e;->b:Lcom/facebook/common/internal/Supplier;

    iput-object p3, p0, Lf/g/b/b/e;->c:Ljava/lang/String;

    new-instance p1, Lf/g/b/b/e$a;

    const/4 p2, 0x0

    invoke-direct {p1, p2, p2}, Lf/g/b/b/e$a;-><init>(Ljava/io/File;Lf/g/b/b/c;)V

    iput-object p1, p0, Lf/g/b/b/e;->e:Lf/g/b/b/e$a;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0}, Lf/g/b/b/c;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-class v1, Lf/g/b/b/e;

    const-string v2, "purgeUnexpectedResources"

    invoke-static {v1, v2, v0}, Lf/g/d/e/a;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/Object;)Lf/g/b/b/c$b;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lf/g/b/b/c;->b(Ljava/lang/String;Ljava/lang/Object;)Lf/g/b/b/c$b;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lf/g/b/b/c;->c(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public d(Ljava/lang/String;Ljava/lang/Object;)Lf/g/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lf/g/b/b/c;->d(Ljava/lang/String;Ljava/lang/Object;)Lf/g/a/a;

    move-result-object p1

    return-object p1
.end method

.method public e()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lf/g/b/b/c$a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0}, Lf/g/b/b/c;->e()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public f(Ljava/lang/String;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0, p1}, Lf/g/b/b/c;->f(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public g(Lf/g/b/b/c$a;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0, p1}, Lf/g/b/b/c;->g(Lf/g/b/b/c$a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lf/g/b/b/e;->b:Lcom/facebook/common/internal/Supplier;

    invoke-interface {v1}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iget-object v2, p0, Lf/g/b/b/e;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-static {v0}, Ls/a/b/b/a;->M(Ljava/io/File;)V
    :try_end_0
    .catch Lcom/facebook/common/file/FileUtils$CreateDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    sget v1, Lf/g/d/e/a;->a:I

    new-instance v1, Lf/g/b/b/a;

    iget v2, p0, Lf/g/b/b/e;->a:I

    iget-object v3, p0, Lf/g/b/b/e;->d:Lf/g/b/a/a;

    invoke-direct {v1, v0, v2, v3}, Lf/g/b/b/a;-><init>(Ljava/io/File;ILf/g/b/a/a;)V

    new-instance v2, Lf/g/b/b/e$a;

    invoke-direct {v2, v0, v1}, Lf/g/b/b/e$a;-><init>(Ljava/io/File;Lf/g/b/b/c;)V

    iput-object v2, p0, Lf/g/b/b/e;->e:Lf/g/b/b/e$a;

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/g/b/b/e;->d:Lf/g/b/a/a;

    check-cast v1, Lf/g/b/a/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method

.method public declared-synchronized i()Lf/g/b/b/c;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/b/b/e;->e:Lf/g/b/b/e$a;

    iget-object v1, v0, Lf/g/b/b/e$a;->a:Lf/g/b/b/c;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lf/g/b/b/e$a;->b:Ljava/io/File;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lf/g/b/b/e;->e:Lf/g/b/b/e$a;

    iget-object v0, v0, Lf/g/b/b/e$a;->a:Lf/g/b/b/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/g/b/b/e;->e:Lf/g/b/b/e$a;

    iget-object v0, v0, Lf/g/b/b/e$a;->b:Ljava/io/File;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/g/b/b/e;->e:Lf/g/b/b/e$a;

    iget-object v0, v0, Lf/g/b/b/e$a;->b:Ljava/io/File;

    invoke-static {v0}, Ls/a/b/b/a;->q(Ljava/io/File;)Z

    :cond_2
    invoke-virtual {p0}, Lf/g/b/b/e;->h()V

    :cond_3
    iget-object v0, p0, Lf/g/b/b/e;->e:Lf/g/b/b/e$a;

    iget-object v0, v0, Lf/g/b/b/e$a;->a:Lf/g/b/b/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isExternal()Z
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lf/g/b/b/e;->i()Lf/g/b/b/c;

    move-result-object v0

    invoke-interface {v0}, Lf/g/b/b/c;->isExternal()Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method
