.class public Lf/g/b/b/i;
.super Ljava/lang/Object;
.source "SettableCacheEvent.java"


# static fields
.field public static final c:Ljava/lang/Object;

.field public static d:Lf/g/b/b/i;

.field public static e:I


# instance fields
.field public a:Lcom/facebook/cache/common/CacheKey;

.field public b:Lf/g/b/b/i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf/g/b/b/i;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lf/g/b/b/i;
    .locals 3
    .annotation build Lcom/facebook/infer/annotation/ReturnsOwnership;
    .end annotation

    sget-object v0, Lf/g/b/b/i;->c:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/g/b/b/i;->d:Lf/g/b/b/i;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lf/g/b/b/i;->b:Lf/g/b/b/i;

    sput-object v2, Lf/g/b/b/i;->d:Lf/g/b/b/i;

    const/4 v2, 0x0

    iput-object v2, v1, Lf/g/b/b/i;->b:Lf/g/b/b/i;

    sget v2, Lf/g/b/b/i;->e:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lf/g/b/b/i;->e:I

    monitor-exit v0

    return-object v1

    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Lf/g/b/b/i;

    invoke-direct {v0}, Lf/g/b/b/i;-><init>()V

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public b()V
    .locals 3

    sget-object v0, Lf/g/b/b/i;->c:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget v1, Lf/g/b/b/i;->e:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    sput v1, Lf/g/b/b/i;->e:I

    sget-object v1, Lf/g/b/b/i;->d:Lf/g/b/b/i;

    if-eqz v1, :cond_0

    iput-object v1, p0, Lf/g/b/b/i;->b:Lf/g/b/b/i;

    :cond_0
    sput-object p0, Lf/g/b/b/i;->d:Lf/g/b/b/i;

    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
