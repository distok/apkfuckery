.class public Lf/g/b/b/d;
.super Ljava/lang/Object;
.source "DiskStorageCache.java"

# interfaces
.implements Lf/g/b/b/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/b/b/d$c;,
        Lf/g/b/b/d$b;
    }
.end annotation


# static fields
.field public static final p:J

.field public static final q:J


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/util/concurrent/CountDownLatch;

.field public d:J

.field public final e:Lf/g/b/a/b;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:J

.field public final h:Lf/g/d/i/a;

.field public final i:Lf/g/b/b/c;

.field public final j:Lf/g/b/b/g;

.field public final k:Lf/g/b/a/a;

.field public final l:Z

.field public final m:Lf/g/b/b/d$b;

.field public final n:Lf/g/d/k/a;

.field public final o:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lf/g/b/b/d;->p:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lf/g/b/b/d;->q:J

    return-void
.end method

.method public constructor <init>(Lf/g/b/b/c;Lf/g/b/b/g;Lf/g/b/b/d$c;Lf/g/b/a/b;Lf/g/b/a/a;Lf/g/d/a/a;Ljava/util/concurrent/Executor;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p6, Ljava/lang/Object;

    invoke-direct {p6}, Ljava/lang/Object;-><init>()V

    iput-object p6, p0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    iget-wide v0, p3, Lf/g/b/b/d$c;->b:J

    iput-wide v0, p0, Lf/g/b/b/d;->a:J

    iget-wide v0, p3, Lf/g/b/b/d$c;->c:J

    iput-wide v0, p0, Lf/g/b/b/d;->b:J

    iput-wide v0, p0, Lf/g/b/b/d;->d:J

    sget-object p3, Lf/g/d/i/a;->h:Lf/g/d/i/a;

    const-class p3, Lf/g/d/i/a;

    monitor-enter p3

    :try_start_0
    sget-object p6, Lf/g/d/i/a;->h:Lf/g/d/i/a;

    if-nez p6, :cond_0

    new-instance p6, Lf/g/d/i/a;

    invoke-direct {p6}, Lf/g/d/i/a;-><init>()V

    sput-object p6, Lf/g/d/i/a;->h:Lf/g/d/i/a;

    :cond_0
    sget-object p6, Lf/g/d/i/a;->h:Lf/g/d/i/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p3

    iput-object p6, p0, Lf/g/b/b/d;->h:Lf/g/d/i/a;

    iput-object p1, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    iput-object p2, p0, Lf/g/b/b/d;->j:Lf/g/b/b/g;

    const-wide/16 p1, -0x1

    iput-wide p1, p0, Lf/g/b/b/d;->g:J

    iput-object p4, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    iput-object p5, p0, Lf/g/b/b/d;->k:Lf/g/b/a/a;

    new-instance p1, Lf/g/b/b/d$b;

    invoke-direct {p1}, Lf/g/b/b/d$b;-><init>()V

    iput-object p1, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    sget-object p1, Lf/g/d/k/c;->a:Lf/g/d/k/c;

    iput-object p1, p0, Lf/g/b/b/d;->n:Lf/g/d/k/a;

    iput-boolean p8, p0, Lf/g/b/b/d;->l:Z

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lf/g/b/b/d;->f:Ljava/util/Set;

    if-eqz p8, :cond_1

    new-instance p1, Ljava/util/concurrent/CountDownLatch;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object p1, p0, Lf/g/b/b/d;->c:Ljava/util/concurrent/CountDownLatch;

    new-instance p1, Lf/g/b/b/d$a;

    invoke-direct {p1, p0}, Lf/g/b/b/d$a;-><init>(Lf/g/b/b/d;)V

    invoke-interface {p7, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/util/concurrent/CountDownLatch;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object p1, p0, Lf/g/b/b/d;->c:Ljava/util/concurrent/CountDownLatch;

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    monitor-exit p3

    throw p1
.end method


# virtual methods
.method public final a(JLf/g/b/a/b$a;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object p3, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {p3}, Lf/g/b/b/c;->e()Ljava/util/Collection;

    move-result-object p3

    invoke-virtual {p0, p3}, Lf/g/b/b/d;->c(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object p3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    invoke-virtual {v0}, Lf/g/b/b/d$b;->a()J

    move-result-wide v0

    sub-long/2addr v0, p1

    const/4 p1, 0x0

    check-cast p3, Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const-wide/16 v2, 0x0

    move-wide v4, v2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lf/g/b/b/c$a;

    cmp-long v6, v4, v0

    if-lez v6, :cond_1

    goto :goto_1

    :cond_1
    iget-object v6, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {v6, p3}, Lf/g/b/b/c;->g(Lf/g/b/b/c$a;)J

    move-result-wide v6

    iget-object v8, p0, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {p3}, Lf/g/b/b/c$a;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    cmp-long v8, v6, v2

    if-lez v8, :cond_0

    add-int/lit8 p1, p1, 0x1

    add-long/2addr v4, v6

    invoke-static {}, Lf/g/b/b/i;->a()Lf/g/b/b/i;

    move-result-object v6

    invoke-interface {p3}, Lf/g/b/b/c$a;->getId()Ljava/lang/String;

    iget-object p3, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast p3, Lf/g/b/a/e;

    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6}, Lf/g/b/b/i;->b()V

    goto :goto_0

    :cond_2
    :goto_1
    iget-object p2, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    neg-long v0, v4

    neg-int p1, p1

    int-to-long v2, p1

    invoke-virtual {p2, v0, v1, v2, v3}, Lf/g/b/b/d$b;->b(JJ)V

    iget-object p1, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {p1}, Lf/g/b/b/c;->a()V

    return-void

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/g/b/b/d;->k:Lf/g/b/a/a;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    check-cast p2, Lf/g/b/a/d;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    throw p1
.end method

.method public b(Lcom/facebook/cache/common/CacheKey;)Lf/g/a/a;
    .locals 9

    invoke-static {}, Lf/g/b/b/i;->a()Lf/g/b/b/i;

    move-result-object v0

    iput-object p1, v0, Lf/g/b/b/i;->a:Lcom/facebook/cache/common/CacheKey;

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {p1}, Ls/a/b/b/a;->B(Lcom/facebook/cache/common/CacheKey;)Ljava/util/List;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x0

    move-object v5, v1

    move-object v6, v5

    :goto_0
    move-object v7, v3

    check-cast v7, Ljava/util/ArrayList;

    :try_start_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_1

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {v6, v5, p1}, Lf/g/b/b/c;->d(Ljava/lang/String;Ljava/lang/Object;)Lf/g/a/a;

    move-result-object v6

    if-eqz v6, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-nez v6, :cond_2

    iget-object p1, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast p1, Lf/g/b/a/e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {p1, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast p1, Lf/g/b/a/e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {p1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v0}, Lf/g/b/b/i;->b()V

    return-object v6

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    goto :goto_3

    :catch_0
    :try_start_5
    iget-object p1, p0, Lf/g/b/b/d;->k:Lf/g/b/a/a;

    check-cast p1, Lf/g/b/a/d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast p1, Lf/g/b/a/e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-virtual {v0}, Lf/g/b/b/i;->b()V

    return-object v1

    :goto_3
    invoke-virtual {v0}, Lf/g/b/b/i;->b()V

    throw p1
.end method

.method public final c(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lf/g/b/b/c$a;",
            ">;)",
            "Ljava/util/Collection<",
            "Lf/g/b/b/c$a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/b/b/d;->n:Lf/g/d/k/a;

    check-cast v0, Lf/g/d/k/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lf/g/b/b/d;->p:J

    add-long/2addr v0, v2

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/g/b/b/c$a;

    invoke-interface {v4}, Lf/g/b/b/c$a;->a()J

    move-result-wide v5

    cmp-long v7, v5, v0

    if-lez v7, :cond_0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/g/b/b/d;->j:Lf/g/b/b/g;

    invoke-interface {p1}, Lf/g/b/b/g;->get()Lf/g/b/b/f;

    move-result-object p1

    invoke-static {v3, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v2
.end method

.method public d(Lcom/facebook/cache/common/CacheKey;Lf/g/b/a/g;)Lf/g/a/a;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-class v0, Lf/g/b/b/d;

    invoke-static {}, Lf/g/b/b/i;->a()Lf/g/b/b/i;

    move-result-object v1

    iput-object p1, v1, Lf/g/b/b/i;->a:Lcom/facebook/cache/common/CacheKey;

    iget-object v2, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast v2, Lf/g/b/a/e;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    instance-of v3, p1, Lf/g/b/a/c;

    if-nez v3, :cond_2

    invoke-static {p1}, Ls/a/b/b/a;->Y(Lcom/facebook/cache/common/CacheKey;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    invoke-virtual {p0, v3, p1}, Lf/g/b/b/d;->g(Ljava/lang/String;Lcom/facebook/cache/common/CacheKey;)Lf/g/b/b/c$b;

    move-result-object v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    check-cast v2, Lf/g/b/b/a$f;

    :try_start_3
    invoke-virtual {v2, p2, p1}, Lf/g/b/b/a$f;->c(Lf/g/b/a/g;Ljava/lang/Object;)V

    iget-object p2, p0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    monitor-enter p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v2, p1}, Lf/g/b/b/a$f;->b(Ljava/lang/Object;)Lf/g/a/a;

    move-result-object p1

    iget-object v4, p0, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    invoke-virtual {p1}, Lf/g/a/a;->b()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    invoke-virtual {v3, v4, v5, v6, v7}, Lf/g/b/b/d$b;->b(JJ)V

    monitor-exit p2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {p1}, Lf/g/a/a;->b()J

    iget-object p2, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    invoke-virtual {p2}, Lf/g/b/b/d$b;->a()J

    iget-object p2, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast p2, Lf/g/b/a/e;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    invoke-virtual {v2}, Lf/g/b/b/a$f;->a()Z

    move-result p2

    if-nez p2, :cond_0

    const-string p2, "Failed to delete temp file"

    invoke-static {v0, p2}, Lf/g/d/e/a;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :cond_0
    invoke-virtual {v1}, Lf/g/b/b/i;->b()V

    return-object p1

    :catchall_0
    move-exception p1

    :try_start_7
    monitor-exit p2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception p1

    :try_start_9
    invoke-virtual {v2}, Lf/g/b/b/a$f;->a()Z

    move-result p2

    if-nez p2, :cond_1

    const-string p2, "Failed to delete temp file"

    invoke-static {v0, p2}, Lf/g/d/e/a;->a(Ljava/lang/Class;Ljava/lang/String;)V

    :cond_1
    throw p1
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :catchall_2
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_a
    iget-object p2, p0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast p2, Lf/g/b/a/e;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "Failed inserting a file into the cache"

    invoke-static {v0, p2, p1}, Lf/g/d/e/a;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :goto_0
    invoke-virtual {v1}, Lf/g/b/b/i;->b()V

    throw p1

    :cond_2
    :try_start_b
    check-cast p1, Lf/g/b/a/c;
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    const/4 p1, 0x0

    :try_start_c
    throw p1

    :catchall_3
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :goto_1
    monitor-exit v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw p1
.end method

.method public final e()Z
    .locals 20

    move-object/from16 v1, p0

    iget-object v0, v1, Lf/g/b/b/d;->n:Lf/g/d/k/a;

    check-cast v0, Lf/g/d/k/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, v1, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    monitor-enter v4

    :try_start_0
    iget-boolean v0, v4, Lf/g/b/b/d$b;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    monitor-exit v4

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_1

    iget-wide v7, v1, Lf/g/b/b/d;->g:J

    cmp-long v0, v7, v4

    if-eqz v0, :cond_1

    sub-long/2addr v2, v7

    sget-wide v7, Lf/g/b/b/d;->q:J

    cmp-long v0, v2, v7

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    return v6

    :cond_1
    :goto_0
    iget-object v0, v1, Lf/g/b/b/d;->n:Lf/g/d/k/a;

    check-cast v0, Lf/g/d/k/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v7, Lf/g/b/b/d;->p:J

    add-long/2addr v7, v2

    iget-boolean v0, v1, Lf/g/b/b/d;->l:Z

    if-eqz v0, :cond_2

    iget-object v0, v1, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Lf/g/b/b/d;->f:Ljava/util/Set;

    goto :goto_1

    :cond_2
    iget-boolean v0, v1, Lf/g/b/b/d;->l:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    const/4 v9, 0x1

    :try_start_1
    iget-object v10, v1, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {v10}, Lf/g/b/b/c;->e()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lf/g/b/b/c$a;

    add-int/lit8 v14, v14, 0x1

    invoke-interface {v15}, Lf/g/b/b/c$a;->getSize()J

    move-result-wide v16

    add-long v11, v11, v16

    invoke-interface {v15}, Lf/g/b/b/c$a;->a()J

    move-result-wide v16

    cmp-long v18, v16, v7

    if-lez v18, :cond_4

    invoke-interface {v15}, Lf/g/b/b/c$a;->getSize()J

    invoke-interface {v15}, Lf/g/b/b/c$a;->a()J

    move-result-wide v15

    move-wide/from16 v18, v7

    sub-long v6, v15, v2

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    const/4 v13, 0x1

    goto :goto_3

    :cond_4
    move-wide/from16 v18, v7

    iget-boolean v6, v1, Lf/g/b/b/d;->l:Z

    if-eqz v6, :cond_5

    invoke-interface {v15}, Lf/g/b/b/c$a;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_3
    move-wide/from16 v7, v18

    const/4 v6, 0x0

    goto :goto_2

    :cond_6
    if-eqz v13, :cond_7

    iget-object v4, v1, Lf/g/b/b/d;->k:Lf/g/b/a/a;

    check-cast v4, Lf/g/b/a/d;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    iget-object v4, v1, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    monitor-enter v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-wide v5, v4, Lf/g/b/b/d$b;->c:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    monitor-exit v4

    int-to-long v7, v14

    cmp-long v4, v5, v7

    if-nez v4, :cond_8

    iget-object v4, v1, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    invoke-virtual {v4}, Lf/g/b/b/d$b;->a()J

    move-result-wide v4

    cmp-long v6, v4, v11

    if-eqz v6, :cond_a

    :cond_8
    iget-boolean v4, v1, Lf/g/b/b/d;->l:Z

    if-eqz v4, :cond_9

    iget-object v4, v1, Lf/g/b/b/d;->f:Ljava/util/Set;

    if-eq v4, v0, :cond_9

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    iget-object v4, v1, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_9
    iget-object v4, v1, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    monitor-enter v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    iput-wide v7, v4, Lf/g/b/b/d$b;->c:J

    iput-wide v11, v4, Lf/g/b/b/d$b;->b:J

    iput-boolean v9, v4, Lf/g/b/b/d$b;->a:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_a
    iput-wide v2, v1, Lf/g/b/b/d;->g:J

    const/4 v6, 0x1

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v4

    throw v0

    :catchall_1
    move-exception v0

    move-object v2, v0

    monitor-exit v4

    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    :catch_0
    move-exception v0

    iget-object v2, v1, Lf/g/b/b/d;->k:Lf/g/b/a/a;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    check-cast v2, Lf/g/b/a/d;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v6, 0x0

    :goto_4
    return v6

    :catchall_2
    move-exception v0

    move-object v2, v0

    monitor-exit v4

    throw v2
.end method

.method public f(Lcom/facebook/cache/common/CacheKey;)V
    .locals 4

    iget-object v0, p0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-static {p1}, Ls/a/b/b/a;->B(Lcom/facebook/cache/common/CacheKey;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    :goto_0
    move-object v2, p1

    check-cast v2, Ljava/util/ArrayList;

    :try_start_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {v3, v2}, Lf/g/b/b/c;->f(Ljava/lang/String;)J

    iget-object v3, p0, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    iget-object v1, p0, Lf/g/b/b/d;->k:Lf/g/b/a/a;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    check-cast v1, Lf/g/b/a/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public final g(Ljava/lang/String;Lcom/facebook/cache/common/CacheKey;)Lf/g/b/b/c$b;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lf/g/b/b/d;->e()Z

    move-result v1

    invoke-virtual {p0}, Lf/g/b/b/d;->h()V

    iget-object v2, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    invoke-virtual {v2}, Lf/g/b/b/d$b;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lf/g/b/b/d;->d:J

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v4, 0x0

    :try_start_1
    iput-boolean v4, v1, Lf/g/b/b/d$b;->a:Z

    const-wide/16 v4, -0x1

    iput-wide v4, v1, Lf/g/b/b/d$b;->c:J

    iput-wide v4, v1, Lf/g/b/b/d$b;->b:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    invoke-virtual {p0}, Lf/g/b/b/d;->e()Z

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_0
    :goto_0
    iget-wide v4, p0, Lf/g/b/b/d;->d:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    const-wide/16 v1, 0x9

    mul-long v4, v4, v1

    const-wide/16 v1, 0xa

    div-long/2addr v4, v1

    sget-object v1, Lf/g/b/a/b$a;->d:Lf/g/b/a/b$a;

    invoke-virtual {p0, v4, v5, v1}, Lf/g/b/b/d;->a(JLf/g/b/a/b$a;)V

    :cond_1
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v0, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {v0, p1, p2}, Lf/g/b/b/c;->b(Ljava/lang/String;Ljava/lang/Object;)Lf/g/b/b/c$b;

    move-result-object p1

    return-object p1

    :catchall_1
    move-exception p1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public final h()V
    .locals 10

    sget-object v0, Lf/g/d/i/a$a;->d:Lf/g/d/i/a$a;

    iget-object v1, p0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {v1}, Lf/g/b/b/c;->isExternal()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lf/g/d/i/a$a;->e:Lf/g/d/i/a$a;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iget-object v2, p0, Lf/g/b/b/d;->h:Lf/g/d/i/a;

    iget-wide v3, p0, Lf/g/b/b/d;->b:J

    iget-object v5, p0, Lf/g/b/b/d;->m:Lf/g/b/b/d$b;

    invoke-virtual {v5}, Lf/g/b/b/d$b;->a()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v2}, Lf/g/d/i/a;->a()V

    invoke-virtual {v2}, Lf/g/d/i/a;->a()V

    iget-object v5, v2, Lf/g/d/i/a;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v5

    if-eqz v5, :cond_2

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    iget-wide v7, v2, Lf/g/d/i/a;->e:J

    sub-long/2addr v5, v7

    sget-wide v7, Lf/g/d/i/a;->i:J

    cmp-long v9, v5, v7

    if-lez v9, :cond_1

    invoke-virtual {v2}, Lf/g/d/i/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v5, v2, Lf/g/d/i/a;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, v2, Lf/g/d/i/a;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    :goto_1
    if-ne v1, v0, :cond_3

    iget-object v0, v2, Lf/g/d/i/a;->a:Landroid/os/StatFs;

    goto :goto_2

    :cond_3
    iget-object v0, v2, Lf/g/d/i/a;->c:Landroid/os/StatFs;

    :goto_2
    const-wide/16 v1, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v5

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v7

    mul-long v7, v7, v5

    goto :goto_3

    :cond_4
    move-wide v7, v1

    :goto_3
    const/4 v0, 0x1

    cmp-long v5, v7, v1

    if-lez v5, :cond_6

    cmp-long v1, v7, v3

    if-gez v1, :cond_5

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    :cond_6
    :goto_4
    if-eqz v0, :cond_7

    iget-wide v0, p0, Lf/g/b/b/d;->a:J

    iput-wide v0, p0, Lf/g/b/b/d;->d:J

    goto :goto_5

    :cond_7
    iget-wide v0, p0, Lf/g/b/b/d;->b:J

    iput-wide v0, p0, Lf/g/b/b/d;->d:J

    :goto_5
    return-void
.end method
