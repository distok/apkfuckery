.class public Lf/g/b/b/a$g;
.super Ljava/lang/Object;
.source "DefaultDiskStorage.java"

# interfaces
.implements Lf/g/d/c/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/b/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "g"
.end annotation


# instance fields
.field public a:Z

.field public final synthetic b:Lf/g/b/b/a;


# direct methods
.method public constructor <init>(Lf/g/b/b/a;Lf/g/b/b/a$a;)V
    .locals 0

    iput-object p1, p0, Lf/g/b/b/a$g;->b:Lf/g/b/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/File;)V
    .locals 9

    iget-boolean v0, p0, Lf/g/b/b/a$g;->a:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/g/b/b/a$g;->b:Lf/g/b/b/a;

    invoke-static {v0, p1}, Lf/g/b/b/a;->h(Lf/g/b/b/a;Ljava/io/File;)Lf/g/b/b/a$d;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, v0, Lf/g/b/b/a$d;->a:Ljava/lang/String;

    const-string v3, ".tmp"

    if-ne v0, v3, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    iget-object v0, p0, Lf/g/b/b/a$g;->b:Lf/g/b/b/a;

    iget-object v0, v0, Lf/g/b/b/a;->e:Lf/g/d/k/a;

    check-cast v0, Lf/g/d/k/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sget-wide v7, Lf/g/b/b/a;->f:J

    sub-long/2addr v5, v7

    cmp-long v0, v3, v5

    if-lez v0, :cond_3

    goto :goto_0

    :cond_1
    const-string v3, ".cnt"

    if-ne v0, v3, :cond_2

    const/4 v1, 0x1

    :cond_2
    invoke-static {v1}, Ls/a/b/b/a;->j(Z)V

    :goto_0
    const/4 v1, 0x1

    :cond_3
    :goto_1
    if-nez v1, :cond_5

    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_5
    return-void
.end method

.method public b(Ljava/io/File;)V
    .locals 1

    iget-boolean v0, p0, Lf/g/b/b/a$g;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/b/b/a$g;->b:Lf/g/b/b/a;

    iget-object v0, v0, Lf/g/b/b/a;->c:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/g/b/b/a$g;->a:Z

    :cond_0
    return-void
.end method

.method public c(Ljava/io/File;)V
    .locals 1

    iget-object v0, p0, Lf/g/b/b/a$g;->b:Lf/g/b/b/a;

    iget-object v0, v0, Lf/g/b/b/a;->a:Ljava/io/File;

    invoke-virtual {v0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lf/g/b/b/a$g;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_0
    iget-boolean v0, p0, Lf/g/b/b/a$g;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/g/b/b/a$g;->b:Lf/g/b/b/a;

    iget-object v0, v0, Lf/g/b/b/a;->c:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/g/b/b/a$g;->a:Z

    :cond_1
    return-void
.end method
