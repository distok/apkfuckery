.class public Lf/g/b/b/a$f;
.super Ljava/lang/Object;
.source "DefaultDiskStorage.java"

# interfaces
.implements Lf/g/b/b/c$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/b/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "f"
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/io/File;

.field public final synthetic c:Lf/g/b/b/a;


# direct methods
.method public constructor <init>(Lf/g/b/b/a;Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lf/g/b/b/a$f;->c:Lf/g/b/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/g/b/b/a$f;->a:Ljava/lang/String;

    iput-object p3, p0, Lf/g/b/b/a$f;->b:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lf/g/b/b/a$f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/g/b/b/a$f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public b(Ljava/lang/Object;)Lf/g/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p1, p0, Lf/g/b/b/a$f;->c:Lf/g/b/b/a;

    iget-object v0, p0, Lf/g/b/b/a$f;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lf/g/b/b/a;->j(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    :try_start_0
    iget-object v0, p0, Lf/g/b/b/a$f;->b:Ljava/io/File;

    invoke-static {v0, p1}, Ls/a/b/b/a;->X(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Lcom/facebook/common/file/FileUtils$RenameException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/b/b/a$f;->c:Lf/g/b/b/a;

    iget-object v0, v0, Lf/g/b/b/a;->e:Lf/g/d/k/a;

    check-cast v0, Lf/g/d/k/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/io/File;->setLastModified(J)Z

    :cond_0
    invoke-static {p1}, Lf/g/a/a;->a(Ljava/io/File;)Lf/g/a/a;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/facebook/common/file/FileUtils$ParentDirNotFoundException;

    if-nez v1, :cond_1

    instance-of v0, v0, Ljava/io/FileNotFoundException;

    :cond_1
    iget-object v0, p0, Lf/g/b/b/a$f;->c:Lf/g/b/b/a;

    iget-object v0, v0, Lf/g/b/b/a;->d:Lf/g/b/a/a;

    sget v1, Lf/g/b/b/a;->g:I

    check-cast v0, Lf/g/b/a/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    throw p1
.end method

.method public c(Lf/g/b/a/g;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance p2, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lf/g/b/b/a$f;->b:Ljava/io/File;

    invoke-direct {p2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v0, Lf/g/d/d/b;

    invoke-direct {v0, p2}, Lf/g/d/d/b;-><init>(Ljava/io/OutputStream;)V

    check-cast p1, Lf/g/j/c/i;

    iget-object v1, p1, Lf/g/j/c/i;->b:Lf/g/j/c/g;

    iget-object v1, v1, Lf/g/j/c/g;->c:Lf/g/d/g/j;

    iget-object p1, p1, Lf/g/j/c/i;->a:Lf/g/j/j/e;

    invoke-virtual {p1}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Lf/g/d/g/j;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    invoke-virtual {v0}, Ljava/io/FilterOutputStream;->flush()V

    iget-wide v0, v0, Lf/g/d/d/b;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V

    iget-object p1, p0, Lf/g/b/b/a$f;->b:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide p1

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    return-void

    :cond_0
    new-instance p1, Lf/g/b/b/a$e;

    iget-object p2, p0, Lf/g/b/b/a$f;->b:Ljava/io/File;

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-direct {p1, v0, v1, v2, v3}, Lf/g/b/b/a$e;-><init>(JJ)V

    throw p1

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V

    throw p1

    :catch_0
    move-exception p1

    iget-object p2, p0, Lf/g/b/b/a$f;->c:Lf/g/b/b/a;

    iget-object p2, p2, Lf/g/b/b/a;->d:Lf/g/b/a/a;

    sget v0, Lf/g/b/b/a;->g:I

    check-cast p2, Lf/g/b/a/d;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    throw p1
.end method
