.class public Lf/g/h/a/b/f/b;
.super Ljava/lang/Object;
.source "AnimatedDrawableBackendFrameRenderer.java"

# interfaces
.implements Lf/g/h/a/b/c;


# instance fields
.field public final a:Lf/g/h/a/b/b;

.field public b:Lf/g/j/a/a/a;

.field public c:Lf/g/j/a/c/d;

.field public final d:Lf/g/j/a/c/d$a;


# direct methods
.method public constructor <init>(Lf/g/h/a/b/b;Lf/g/j/a/a/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/g/h/a/b/f/b$a;

    invoke-direct {v0, p0}, Lf/g/h/a/b/f/b$a;-><init>(Lf/g/h/a/b/f/b;)V

    iput-object v0, p0, Lf/g/h/a/b/f/b;->d:Lf/g/j/a/c/d$a;

    iput-object p1, p0, Lf/g/h/a/b/f/b;->a:Lf/g/h/a/b/b;

    iput-object p2, p0, Lf/g/h/a/b/f/b;->b:Lf/g/j/a/a/a;

    new-instance p1, Lf/g/j/a/c/d;

    invoke-direct {p1, p2, v0}, Lf/g/j/a/c/d;-><init>(Lf/g/j/a/a/a;Lf/g/j/a/c/d$a;)V

    iput-object p1, p0, Lf/g/h/a/b/f/b;->c:Lf/g/j/a/c/d;

    return-void
.end method


# virtual methods
.method public a(ILandroid/graphics/Bitmap;)Z
    .locals 3

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lf/g/h/a/b/f/b;->c:Lf/g/j/a/c/d;

    invoke-virtual {v1, p1, p2}, Lf/g/j/a/c/d;->d(ILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p2

    const-class v1, Lf/g/h/a/b/f/b;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const-string p1, "Rendering of frame unsuccessful. Frame number: %d"

    invoke-static {v1, p2, p1, v0}, Lf/g/d/e/a;->d(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return v2
.end method
