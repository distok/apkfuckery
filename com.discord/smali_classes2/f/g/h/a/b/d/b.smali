.class public Lf/g/h/a/b/d/b;
.super Ljava/lang/Object;
.source "FrescoFrameCache.java"

# interfaces
.implements Lf/g/h/a/b/b;


# instance fields
.field public final a:Lf/g/j/a/c/c;

.field public final b:Z

.field public final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/common/references/CloseableReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/a/c/c;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/h/a/b/d/b;->a:Lf/g/j/a/c/c;

    iput-boolean p2, p0, Lf/g/h/a/b/d/b;->b:Z

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    return-void
.end method

.method public static g(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    :try_start_0
    invoke-static {p0}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lf/g/j/j/d;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/j/d;

    if-eqz v0, :cond_0

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, v0, Lf/g/j/j/d;->f:Lcom/facebook/common/references/CloseableReference;

    invoke-static {v1}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lcom/facebook/common/references/CloseableReference;->close()V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    const/4 v0, 0x0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    return-object v0

    :goto_0
    sget-object v1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public declared-synchronized a(III)Lcom/facebook/common/references/CloseableReference;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean p1, p0, Lf/g/h/a/b/d/b;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    :cond_0
    :try_start_1
    iget-object p1, p0, Lf/g/h/a/b/d/b;->a:Lf/g/j/a/c/c;

    invoke-virtual {p1}, Lf/g/j/a/c/c;->b()Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    invoke-static {p1}, Lf/g/h/a/b/d/b;->g(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized b(ILcom/facebook/common/references/CloseableReference;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    monitor-enter p0

    const/4 p3, 0x0

    :try_start_0
    new-instance v0, Lf/g/j/j/d;

    sget-object v1, Lf/g/j/j/h;->d:Lf/g/j/j/i;

    const/4 v2, 0x0

    invoke-direct {v0, p2, v1, v2, v2}, Lf/g/j/j/d;-><init>(Lcom/facebook/common/references/CloseableReference;Lf/g/j/j/i;II)V

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p3, :cond_1

    if-eqz p3, :cond_0

    :try_start_1
    invoke-virtual {p3}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    iget-object p2, p0, Lf/g/h/a/b/d/b;->a:Lf/g/j/a/c/c;

    iget-object v0, p2, Lf/g/j/a/c/c;->b:Lf/g/j/c/m;

    new-instance v1, Lf/g/j/a/c/c$b;

    iget-object v2, p2, Lf/g/j/a/c/c;->a:Lcom/facebook/cache/common/CacheKey;

    invoke-direct {v1, v2, p1}, Lf/g/j/a/c/c$b;-><init>(Lcom/facebook/cache/common/CacheKey;I)V

    iget-object p2, p2, Lf/g/j/a/c/c;->c:Lf/g/j/c/m$c;

    invoke-virtual {v0, v1, p3, p2}, Lf/g/j/c/m;->c(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;Lf/g/j/c/m$c;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p2

    invoke-static {p2}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/references/CloseableReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    iget-object v0, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget p1, Lf/g/d/e/a;->a:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :try_start_3
    invoke-virtual {p3}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    if-eqz p3, :cond_4

    :try_start_4
    invoke-virtual {p3}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_4
    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized c(I)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/h/a/b/d/b;->a:Lf/g/j/a/c/c;

    invoke-virtual {v0, p1}, Lf/g/j/a/c/c;->a(I)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized clear()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/h/a/b/d/b;->d:Lcom/facebook/common/references/CloseableReference;

    sget-object v1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lf/g/h/a/b/d/b;->d:Lcom/facebook/common/references/CloseableReference;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/references/CloseableReference;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(I)Lcom/facebook/common/references/CloseableReference;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/h/a/b/d/b;->a:Lf/g/j/a/c/c;

    iget-object v1, v0, Lf/g/j/a/c/c;->b:Lf/g/j/c/m;

    new-instance v2, Lf/g/j/a/c/c$b;

    iget-object v0, v0, Lf/g/j/a/c/c;->a:Lcom/facebook/cache/common/CacheKey;

    invoke-direct {v2, v0, p1}, Lf/g/j/a/c/c$b;-><init>(Lcom/facebook/cache/common/CacheKey;I)V

    invoke-virtual {v1, v2}, Lf/g/j/c/m;->get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    invoke-static {p1}, Lf/g/h/a/b/d/b;->g(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized e(ILcom/facebook/common/references/CloseableReference;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    iget-object p3, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    invoke-virtual {p3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/facebook/common/references/CloseableReference;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lf/g/h/a/b/d/b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V

    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    invoke-virtual {p3}, Lcom/facebook/common/references/CloseableReference;->close()V

    sget p3, Lf/g/d/e/a;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 p3, 0x0

    :try_start_3
    new-instance v0, Lf/g/j/j/d;

    sget-object v1, Lf/g/j/j/h;->d:Lf/g/j/j/i;

    const/4 v2, 0x0

    invoke-direct {v0, p2, v1, v2, v2}, Lf/g/j/j/d;-><init>(Lcom/facebook/common/references/CloseableReference;Lf/g/j/j/i;II)V

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p3

    if-eqz p3, :cond_2

    iget-object p2, p0, Lf/g/h/a/b/d/b;->d:Lcom/facebook/common/references/CloseableReference;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    iget-object p2, p0, Lf/g/h/a/b/d/b;->a:Lf/g/j/a/c/c;

    iget-object v0, p2, Lf/g/j/a/c/c;->b:Lf/g/j/c/m;

    new-instance v1, Lf/g/j/a/c/c$b;

    iget-object v2, p2, Lf/g/j/a/c/c;->a:Lcom/facebook/cache/common/CacheKey;

    invoke-direct {v1, v2, p1}, Lf/g/j/a/c/c$b;-><init>(Lcom/facebook/cache/common/CacheKey;I)V

    iget-object p1, p2, Lf/g/j/a/c/c;->c:Lf/g/j/c/m$c;

    invoke-virtual {v0, v1, p3, p1}, Lf/g/j/c/m;->c(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;Lf/g/j/c/m$c;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    iput-object p1, p0, Lf/g/h/a/b/d/b;->d:Lcom/facebook/common/references/CloseableReference;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    if-eqz p3, :cond_3

    :try_start_4
    invoke-virtual {p3}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    if-eqz p3, :cond_4

    :try_start_5
    invoke-virtual {p3}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_4
    throw p1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized f(I)Lcom/facebook/common/references/CloseableReference;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object p1, p0, Lf/g/h/a/b/d/b;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    invoke-static {p1}, Lf/g/h/a/b/d/b;->g(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
