.class public Lf/g/h/a/b/e/c$a;
.super Ljava/lang/Object;
.source "DefaultBitmapFramePreparer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/h/a/b/e/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final d:Lf/g/h/a/b/b;

.field public final e:Lf/g/h/a/a/a;

.field public final f:I

.field public final g:I

.field public final synthetic h:Lf/g/h/a/b/e/c;


# direct methods
.method public constructor <init>(Lf/g/h/a/b/e/c;Lf/g/h/a/a/a;Lf/g/h/a/b/b;II)V
    .locals 0

    iput-object p1, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lf/g/h/a/b/e/c$a;->e:Lf/g/h/a/a/a;

    iput-object p3, p0, Lf/g/h/a/b/e/c$a;->d:Lf/g/h/a/b/b;

    iput p4, p0, Lf/g/h/a/b/e/c$a;->f:I

    iput p5, p0, Lf/g/h/a/b/e/c$a;->g:I

    return-void
.end method


# virtual methods
.method public final a(II)Z
    .locals 7

    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eq p2, v1, :cond_1

    const/4 v1, 0x0

    if-eq p2, v2, :cond_0

    sget-object p1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v2, v2, Lf/g/h/a/b/e/c;->a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iget-object v4, p0, Lf/g/h/a/b/e/c$a;->e:Lf/g/h/a/a/a;

    invoke-interface {v4}, Lf/g/h/a/a/a;->i()I

    move-result v4

    iget-object v5, p0, Lf/g/h/a/b/e/c$a;->e:Lf/g/h/a/a/a;

    invoke-interface {v5}, Lf/g/h/a/a/a;->g()I

    move-result v5

    iget-object v6, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v6, v6, Lf/g/h/a/b/e/c;->c:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v4, v5, v6}, Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;->a(IILandroid/graphics/Bitmap$Config;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v3, v1

    const/4 v2, -0x1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    const-class p2, Lf/g/h/a/b/e/c;

    const-string v0, "Failed to create frame bitmap"

    invoke-static {p2, v0, p1}, Lf/g/d/e/a;->l(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object p1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    return v1

    :cond_1
    :try_start_2
    iget-object v1, p0, Lf/g/h/a/b/e/c$a;->d:Lf/g/h/a/b/b;

    iget-object v4, p0, Lf/g/h/a/b/e/c$a;->e:Lf/g/h/a/a/a;

    invoke-interface {v4}, Lf/g/h/a/a/a;->i()I

    move-result v4

    iget-object v5, p0, Lf/g/h/a/b/e/c$a;->e:Lf/g/h/a/a/a;

    invoke-interface {v5}, Lf/g/h/a/a/a;->g()I

    move-result v5

    invoke-interface {v1, p1, v4, v5}, Lf/g/h/a/b/b;->a(III)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    move-object v3, v1

    :goto_0
    invoke-virtual {p0, p1, v3, p2}, Lf/g/h/a/b/e/c$a;->b(ILcom/facebook/common/references/CloseableReference;I)Z

    move-result p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    if-nez p2, :cond_4

    if-ne v2, v0, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p1, v2}, Lf/g/h/a/b/e/c$a;->a(II)Z

    move-result p1

    return p1

    :cond_4
    :goto_1
    return p2

    :catchall_0
    move-exception p1

    sget-object p2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_5
    throw p1
.end method

.method public final b(ILcom/facebook/common/references/CloseableReference;I)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;I)Z"
        }
    .end annotation

    invoke-static {p2}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v0, v0, Lf/g/h/a/b/e/c;->b:Lf/g/h/a/b/c;

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    check-cast v0, Lf/g/h/a/b/f/b;

    invoke-virtual {v0, p1, v2}, Lf/g/h/a/b/f/b;->a(ILandroid/graphics/Bitmap;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    sget p1, Lf/g/d/e/a;->a:I

    iget-object p1, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object p1, p1, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lf/g/h/a/b/e/c$a;->d:Lf/g/h/a/b/b;

    iget v1, p0, Lf/g/h/a/b/e/c$a;->f:I

    invoke-interface {v0, v1, p2, p3}, Lf/g/h/a/b/b;->b(ILcom/facebook/common/references/CloseableReference;I)V

    monitor-exit p1

    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p2
.end method

.method public run()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lf/g/h/a/b/e/c$a;->d:Lf/g/h/a/b/b;

    iget v1, p0, Lf/g/h/a/b/e/c$a;->f:I

    invoke-interface {v0, v1}, Lf/g/h/a/b/b;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lf/g/d/e/a;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    iget-object v0, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v0, v0, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    monitor-enter v0

    :try_start_1
    iget-object v1, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v1, v1, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    iget v2, p0, Lf/g/h/a/b/e/c$a;->g:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_0
    :try_start_2
    iget v0, p0, Lf/g/h/a/b/e/c$a;->f:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lf/g/h/a/b/e/c$a;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lf/g/d/e/a;->a:I

    goto :goto_0

    :cond_1
    const-class v0, Lf/g/h/a/b/e/c;

    const-string v2, "Could not prepare frame %d."

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lf/g/h/a/b/e/c$a;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v2, v1}, Lf/g/d/e/a;->c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :goto_0
    iget-object v0, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v0, v0, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    monitor-enter v0

    :try_start_3
    iget-object v1, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v1, v1, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    iget v2, p0, Lf/g/h/a/b/e/c$a;->g:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    monitor-exit v0

    return-void

    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    :catchall_2
    move-exception v0

    iget-object v1, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v1, v1, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_4
    iget-object v2, p0, Lf/g/h/a/b/e/c$a;->h:Lf/g/h/a/b/e/c;

    iget-object v2, v2, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    iget v3, p0, Lf/g/h/a/b/e/c$a;->g:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->remove(I)V

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v0

    :catchall_3
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v0
.end method
