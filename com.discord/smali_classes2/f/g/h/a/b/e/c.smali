.class public Lf/g/h/a/b/e/c;
.super Ljava/lang/Object;
.source "DefaultBitmapFramePreparer.java"

# interfaces
.implements Lf/g/h/a/b/e/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/h/a/b/e/c$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field public final b:Lf/g/h/a/b/c;

.field public final c:Landroid/graphics/Bitmap$Config;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Lf/g/h/a/b/c;Landroid/graphics/Bitmap$Config;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/h/a/b/e/c;->a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iput-object p2, p0, Lf/g/h/a/b/e/c;->b:Lf/g/h/a/b/c;

    iput-object p3, p0, Lf/g/h/a/b/e/c;->c:Landroid/graphics/Bitmap$Config;

    iput-object p4, p0, Lf/g/h/a/b/e/c;->d:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    return-void
.end method
