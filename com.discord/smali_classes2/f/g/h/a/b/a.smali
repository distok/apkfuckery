.class public Lf/g/h/a/b/a;
.super Ljava/lang/Object;
.source "BitmapAnimationBackend.java"

# interfaces
.implements Lf/g/h/a/a/a;
.implements Lf/g/h/a/a/c$b;


# instance fields
.field public final a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field public final b:Lf/g/h/a/b/b;

.field public final c:Lf/g/h/a/a/d;

.field public final d:Lf/g/h/a/b/c;

.field public final e:Lf/g/h/a/b/e/a;

.field public final f:Lf/g/h/a/b/e/b;

.field public final g:Landroid/graphics/Paint;

.field public h:Landroid/graphics/Rect;

.field public i:I

.field public j:I

.field public k:Landroid/graphics/Bitmap$Config;


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Lf/g/h/a/b/b;Lf/g/h/a/a/d;Lf/g/h/a/b/c;Lf/g/h/a/b/e/a;Lf/g/h/a/b/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lf/g/h/a/b/a;->k:Landroid/graphics/Bitmap$Config;

    iput-object p1, p0, Lf/g/h/a/b/a;->a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iput-object p2, p0, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    iput-object p3, p0, Lf/g/h/a/b/a;->c:Lf/g/h/a/a/d;

    iput-object p4, p0, Lf/g/h/a/b/a;->d:Lf/g/h/a/b/c;

    iput-object p5, p0, Lf/g/h/a/b/a;->e:Lf/g/h/a/b/e/a;

    iput-object p6, p0, Lf/g/h/a/b/a;->f:Lf/g/h/a/b/e/b;

    new-instance p1, Landroid/graphics/Paint;

    const/4 p2, 0x6

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lf/g/h/a/b/a;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lf/g/h/a/b/a;->n()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lf/g/h/a/b/a;->c:Lf/g/h/a/a/d;

    invoke-interface {v0}, Lf/g/h/a/a/d;->a()I

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lf/g/h/a/b/a;->c:Lf/g/h/a/a/d;

    invoke-interface {v0}, Lf/g/h/a/a/d;->b()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    invoke-interface {v0}, Lf/g/h/a/b/b;->clear()V

    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    invoke-interface {v0}, Lf/g/h/a/b/b;->clear()V

    return-void
.end method

.method public d(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Lf/g/h/a/b/a;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void
.end method

.method public e(I)I
    .locals 1

    iget-object v0, p0, Lf/g/h/a/b/a;->c:Lf/g/h/a/a/d;

    invoke-interface {v0, p1}, Lf/g/h/a/a/d;->e(I)I

    move-result p1

    return p1
.end method

.method public f(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/IntRange;
            from = 0x0L
            to = 0xffL
        .end annotation
    .end param

    iget-object v0, p0, Lf/g/h/a/b/a;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lf/g/h/a/b/a;->j:I

    return v0
.end method

.method public h(Landroid/graphics/Rect;)V
    .locals 5

    iput-object p1, p0, Lf/g/h/a/b/a;->h:Landroid/graphics/Rect;

    iget-object v0, p0, Lf/g/h/a/b/a;->d:Lf/g/h/a/b/c;

    check-cast v0, Lf/g/h/a/b/f/b;

    iget-object v1, v0, Lf/g/h/a/b/f/b;->b:Lf/g/j/a/a/a;

    check-cast v1, Lf/g/j/a/c/a;

    iget-object v2, v1, Lf/g/j/a/c/a;->c:Lf/g/j/a/a/c;

    invoke-static {v2, p1}, Lf/g/j/a/c/a;->a(Lf/g/j/a/a/c;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, v1, Lf/g/j/a/c/a;->d:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Lf/g/j/a/c/a;

    iget-object v3, v1, Lf/g/j/a/c/a;->a:Lf/g/j/a/d/a;

    iget-object v4, v1, Lf/g/j/a/c/a;->b:Lf/g/j/a/a/e;

    iget-boolean v1, v1, Lf/g/j/a/c/a;->i:Z

    invoke-direct {v2, v3, v4, p1, v1}, Lf/g/j/a/c/a;-><init>(Lf/g/j/a/d/a;Lf/g/j/a/a/e;Landroid/graphics/Rect;Z)V

    move-object v1, v2

    :goto_0
    iget-object p1, v0, Lf/g/h/a/b/f/b;->b:Lf/g/j/a/a/a;

    if-eq v1, p1, :cond_1

    iput-object v1, v0, Lf/g/h/a/b/f/b;->b:Lf/g/j/a/a/a;

    new-instance p1, Lf/g/j/a/c/d;

    iget-object v2, v0, Lf/g/h/a/b/f/b;->d:Lf/g/j/a/c/d$a;

    invoke-direct {p1, v1, v2}, Lf/g/j/a/c/d;-><init>(Lf/g/j/a/a/a;Lf/g/j/a/c/d$a;)V

    iput-object p1, v0, Lf/g/h/a/b/f/b;->c:Lf/g/j/a/c/d;

    :cond_1
    invoke-virtual {p0}, Lf/g/h/a/b/a;->n()V

    return-void
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lf/g/h/a/b/a;->i:I

    return v0
.end method

.method public j(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z
    .locals 16

    move-object/from16 v7, p0

    move/from16 v0, p3

    const/4 v1, 0x0

    move-object/from16 v2, p2

    invoke-virtual {v7, v2, v0, v1}, Lf/g/h/a/b/a;->l(Landroid/graphics/Canvas;II)Z

    move-result v8

    iget-object v1, v7, Lf/g/h/a/b/a;->e:Lf/g/h/a/b/e/a;

    if-eqz v1, :cond_2

    iget-object v9, v7, Lf/g/h/a/b/a;->f:Lf/g/h/a/b/e/b;

    if-eqz v9, :cond_2

    iget-object v10, v7, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    move-object v11, v1

    check-cast v11, Lf/g/h/a/b/e/d;

    const/4 v1, 0x1

    const/4 v12, 0x1

    :goto_0
    iget v1, v11, Lf/g/h/a/b/e/d;->a:I

    if-gt v12, v1, :cond_2

    add-int v1, v0, v12

    invoke-virtual/range {p0 .. p0}, Lf/g/h/a/b/a;->a()I

    move-result v2

    rem-int v5, v1, v2

    const/4 v1, 0x2

    invoke-static {v1}, Lf/g/d/e/a;->h(I)Z

    move-object v13, v9

    check-cast v13, Lf/g/h/a/b/e/c;

    invoke-static {v13}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int v14, v1, v5

    iget-object v15, v13, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    monitor-enter v15

    :try_start_0
    iget-object v1, v13, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v1, Lf/g/d/e/a;->a:I

    monitor-exit v15

    goto :goto_1

    :cond_0
    invoke-interface {v10, v5}, Lf/g/h/a/b/b;->c(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lf/g/d/e/a;->a:I

    monitor-exit v15

    goto :goto_1

    :cond_1
    new-instance v6, Lf/g/h/a/b/e/c$a;

    move-object v1, v6

    move-object v2, v13

    move-object/from16 v3, p0

    move-object v4, v10

    move-object v0, v6

    move v6, v14

    invoke-direct/range {v1 .. v6}, Lf/g/h/a/b/e/c$a;-><init>(Lf/g/h/a/b/e/c;Lf/g/h/a/a/a;Lf/g/h/a/b/b;II)V

    iget-object v1, v13, Lf/g/h/a/b/e/c;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v14, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, v13, Lf/g/h/a/b/e/c;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    monitor-exit v15

    :goto_1
    add-int/lit8 v12, v12, 0x1

    move/from16 v0, p3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    return v8
.end method

.method public final k(ILcom/facebook/common/references/CloseableReference;Landroid/graphics/Canvas;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Canvas;",
            "I)Z"
        }
    .end annotation

    invoke-static {p2}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, Lf/g/h/a/b/a;->h:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p0, Lf/g/h/a/b/a;->g:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p3, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iget-object v2, p0, Lf/g/h/a/b/a;->h:Landroid/graphics/Rect;

    iget-object v3, p0, Lf/g/h/a/b/a;->g:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_0
    const/4 p3, 0x3

    if-eq p4, p3, :cond_2

    iget-object p3, p0, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    invoke-interface {p3, p1, p2, p4}, Lf/g/h/a/b/b;->e(ILcom/facebook/common/references/CloseableReference;I)V

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public final l(Landroid/graphics/Canvas;II)Z
    .locals 9

    const/4 v0, 0x2

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz p3, :cond_5

    if-eq p3, v2, :cond_3

    if-eq p3, v0, :cond_1

    if-eq p3, v3, :cond_0

    sget-object p1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    return v4

    :cond_0
    :try_start_0
    iget-object p3, p0, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    invoke-interface {p3, p2}, Lf/g/h/a/b/b;->f(I)Lcom/facebook/common/references/CloseableReference;

    move-result-object v5

    invoke-virtual {p0, p2, v5, p1, v3}, Lf/g/h/a/b/a;->k(ILcom/facebook/common/references/CloseableReference;Landroid/graphics/Canvas;I)Z

    move-result p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, -0x1

    goto :goto_2

    :cond_1
    :try_start_1
    iget-object p3, p0, Lf/g/h/a/b/a;->a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iget v6, p0, Lf/g/h/a/b/a;->i:I

    iget v7, p0, Lf/g/h/a/b/a;->j:I

    iget-object v8, p0, Lf/g/h/a/b/a;->k:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p3, v6, v7, v8}, Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;->a(IILandroid/graphics/Bitmap$Config;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v5
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p0, p2, v5}, Lf/g/h/a/b/a;->m(ILcom/facebook/common/references/CloseableReference;)Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-virtual {p0, p2, v5, p1, v0}, Lf/g/h/a/b/a;->k(ILcom/facebook/common/references/CloseableReference;Landroid/graphics/Canvas;I)Z

    move-result p3

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    move p3, v2

    const/4 v0, 0x3

    goto :goto_2

    :catch_0
    move-exception p1

    const-class p2, Lf/g/h/a/b/a;

    const-string p3, "Failed to create frame bitmap"

    invoke-static {p2, p3, p1}, Lf/g/d/e/a;->l(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sget-object p1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    return v4

    :cond_3
    :try_start_3
    iget-object p3, p0, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    iget v3, p0, Lf/g/h/a/b/a;->i:I

    iget v6, p0, Lf/g/h/a/b/a;->j:I

    invoke-interface {p3, p2, v3, v6}, Lf/g/h/a/b/b;->a(III)Lcom/facebook/common/references/CloseableReference;

    move-result-object v5

    invoke-virtual {p0, p2, v5}, Lf/g/h/a/b/a;->m(ILcom/facebook/common/references/CloseableReference;)Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-virtual {p0, p2, v5, p1, v2}, Lf/g/h/a/b/a;->k(ILcom/facebook/common/references/CloseableReference;Landroid/graphics/Canvas;I)Z

    move-result p3

    if-eqz p3, :cond_4

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    move p3, v2

    goto :goto_2

    :cond_5
    iget-object p3, p0, Lf/g/h/a/b/a;->b:Lf/g/h/a/b/b;

    invoke-interface {p3, p2}, Lf/g/h/a/b/b;->d(I)Lcom/facebook/common/references/CloseableReference;

    move-result-object v5

    invoke-virtual {p0, p2, v5, p1, v4}, Lf/g/h/a/b/a;->k(ILcom/facebook/common/references/CloseableReference;Landroid/graphics/Canvas;I)Z

    move-result p3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x1

    :goto_2
    sget-object v2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_6
    if-nez p3, :cond_8

    if-ne v0, v1, :cond_7

    goto :goto_3

    :cond_7
    invoke-virtual {p0, p1, p2, v0}, Lf/g/h/a/b/a;->l(Landroid/graphics/Canvas;II)Z

    move-result p1

    return p1

    :cond_8
    :goto_3
    return p3

    :catchall_0
    move-exception p1

    sget-object p2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v5, :cond_9

    invoke-virtual {v5}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_9
    throw p1
.end method

.method public final m(ILcom/facebook/common/references/CloseableReference;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    invoke-static {p2}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, Lf/g/h/a/b/a;->d:Lf/g/h/a/b/c;

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    check-cast v0, Lf/g/h/a/b/f/b;

    invoke-virtual {v0, p1, v1}, Lf/g/h/a/b/f/b;->a(ILandroid/graphics/Bitmap;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    return p1
.end method

.method public final n()V
    .locals 2

    iget-object v0, p0, Lf/g/h/a/b/a;->d:Lf/g/h/a/b/c;

    check-cast v0, Lf/g/h/a/b/f/b;

    iget-object v0, v0, Lf/g/h/a/b/f/b;->b:Lf/g/j/a/a/a;

    check-cast v0, Lf/g/j/a/c/a;

    iget-object v0, v0, Lf/g/j/a/c/a;->c:Lf/g/j/a/a/c;

    invoke-interface {v0}, Lf/g/j/a/a/c;->getWidth()I

    move-result v0

    iput v0, p0, Lf/g/h/a/b/a;->i:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lf/g/h/a/b/a;->h:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    :goto_0
    iput v0, p0, Lf/g/h/a/b/a;->i:I

    :cond_1
    iget-object v0, p0, Lf/g/h/a/b/a;->d:Lf/g/h/a/b/c;

    check-cast v0, Lf/g/h/a/b/f/b;

    iget-object v0, v0, Lf/g/h/a/b/f/b;->b:Lf/g/j/a/a/a;

    check-cast v0, Lf/g/j/a/c/a;

    iget-object v0, v0, Lf/g/j/a/c/a;->c:Lf/g/j/a/a/c;

    invoke-interface {v0}, Lf/g/j/a/a/c;->getHeight()I

    move-result v0

    iput v0, p0, Lf/g/h/a/b/a;->j:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lf/g/h/a/b/a;->h:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    :goto_1
    iput v1, p0, Lf/g/h/a/b/a;->j:I

    :cond_3
    return-void
.end method
