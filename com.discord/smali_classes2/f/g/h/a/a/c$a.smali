.class public Lf/g/h/a/a/c$a;
.super Ljava/lang/Object;
.source "AnimationBackendDelegateWithInactivityCheck.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/h/a/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/g/h/a/a/c;


# direct methods
.method public constructor <init>(Lf/g/h/a/a/c;)V
    .locals 0

    iput-object p1, p0, Lf/g/h/a/a/c$a;->d:Lf/g/h/a/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lf/g/h/a/a/c$a;->d:Lf/g/h/a/a/c;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/g/h/a/a/c$a;->d:Lf/g/h/a/a/c;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lf/g/h/a/a/c;->d:Z

    iget-object v3, v1, Lf/g/h/a/a/c;->b:Lf/g/d/k/b;

    invoke-interface {v3}, Lf/g/d/k/b;->now()J

    move-result-wide v3

    iget-wide v5, v1, Lf/g/h/a/a/c;->e:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x7d0

    cmp-long v1, v3, v5

    if-lez v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    if-eqz v2, :cond_1

    iget-object v1, p0, Lf/g/h/a/a/c$a;->d:Lf/g/h/a/a/c;

    iget-object v1, v1, Lf/g/h/a/a/c;->f:Lf/g/h/a/a/c$b;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lf/g/h/a/a/c$b;->c()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lf/g/h/a/a/c$a;->d:Lf/g/h/a/a/c;

    invoke-virtual {v1}, Lf/g/h/a/a/c;->c()V

    :cond_2
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
