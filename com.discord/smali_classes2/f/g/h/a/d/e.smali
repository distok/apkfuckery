.class public Lf/g/h/a/d/e;
.super Ljava/lang/Object;
.source "ExperimentalBitmapAnimationDrawableFactory.java"

# interfaces
.implements Lf/g/j/i/a;


# instance fields
.field public final a:Lf/g/j/a/c/b;

.field public final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Lf/g/d/k/b;

.field public final e:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field public final f:Lf/g/j/c/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/a/c/b;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ExecutorService;Lf/g/d/k/b;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Lf/g/j/c/m;Lcom/facebook/common/internal/Supplier;Lcom/facebook/common/internal/Supplier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/a/c/b;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lf/g/d/k/b;",
            "Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;",
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/h/a/d/e;->a:Lf/g/j/a/c/b;

    iput-object p2, p0, Lf/g/h/a/d/e;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p3, p0, Lf/g/h/a/d/e;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lf/g/h/a/d/e;->d:Lf/g/d/k/b;

    iput-object p5, p0, Lf/g/h/a/d/e;->e:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iput-object p6, p0, Lf/g/h/a/d/e;->f:Lf/g/j/c/m;

    iput-object p7, p0, Lf/g/h/a/d/e;->g:Lcom/facebook/common/internal/Supplier;

    iput-object p8, p0, Lf/g/h/a/d/e;->h:Lcom/facebook/common/internal/Supplier;

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/j/c;)Z
    .locals 0

    instance-of p1, p1, Lf/g/j/j/a;

    return p1
.end method

.method public b(Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;
    .locals 9

    new-instance v0, Lf/g/h/a/c/a;

    check-cast p1, Lf/g/j/j/a;

    monitor-enter p1

    :try_start_0
    iget-object v1, p1, Lf/g/j/j/a;->f:Lf/g/j/a/a/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    iget-object p1, v1, Lf/g/j/a/a/e;->a:Lf/g/j/a/a/c;

    new-instance v2, Landroid/graphics/Rect;

    invoke-interface {p1}, Lf/g/j/a/a/c;->getWidth()I

    move-result v3

    invoke-interface {p1}, Lf/g/j/a/a/c;->getHeight()I

    move-result p1

    const/4 v4, 0x0

    invoke-direct {v2, v4, v4, v3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object p1, p0, Lf/g/h/a/d/e;->a:Lf/g/j/a/c/b;

    invoke-interface {p1, v1, v2}, Lf/g/j/a/c/b;->a(Lf/g/j/a/a/e;Landroid/graphics/Rect;)Lf/g/j/a/a/a;

    move-result-object p1

    iget-object v2, p0, Lf/g/h/a/d/e;->g:Lcom/facebook/common/internal/Supplier;

    invoke-interface {v2}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    const/4 v1, 0x3

    if-eq v2, v1, :cond_0

    new-instance v1, Lf/g/h/a/b/d/d;

    invoke-direct {v1}, Lf/g/h/a/b/d/d;-><init>()V

    goto :goto_1

    :cond_0
    new-instance v1, Lf/g/h/a/b/d/c;

    invoke-direct {v1}, Lf/g/h/a/b/d/c;-><init>()V

    goto :goto_1

    :cond_1
    new-instance v2, Lf/g/h/a/b/d/b;

    new-instance v3, Lf/g/j/a/c/c;

    new-instance v5, Lf/g/h/a/b/d/a;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-direct {v5, v1}, Lf/g/h/a/b/d/a;-><init>(I)V

    iget-object v1, p0, Lf/g/h/a/d/e;->f:Lf/g/j/c/m;

    invoke-direct {v3, v5, v1}, Lf/g/j/a/c/c;-><init>(Lcom/facebook/cache/common/CacheKey;Lf/g/j/c/m;)V

    invoke-direct {v2, v3, v4}, Lf/g/h/a/b/d/b;-><init>(Lf/g/j/a/c/c;Z)V

    goto :goto_0

    :cond_2
    new-instance v2, Lf/g/h/a/b/d/b;

    new-instance v4, Lf/g/j/a/c/c;

    new-instance v5, Lf/g/h/a/b/d/a;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-direct {v5, v1}, Lf/g/h/a/b/d/a;-><init>(I)V

    iget-object v1, p0, Lf/g/h/a/d/e;->f:Lf/g/j/c/m;

    invoke-direct {v4, v5, v1}, Lf/g/j/a/c/c;-><init>(Lcom/facebook/cache/common/CacheKey;Lf/g/j/c/m;)V

    invoke-direct {v2, v4, v3}, Lf/g/h/a/b/d/b;-><init>(Lf/g/j/a/c/c;Z)V

    :goto_0
    move-object v1, v2

    :goto_1
    move-object v4, v1

    new-instance v6, Lf/g/h/a/b/f/b;

    invoke-direct {v6, v4, p1}, Lf/g/h/a/b/f/b;-><init>(Lf/g/h/a/b/b;Lf/g/j/a/a/a;)V

    iget-object v1, p0, Lf/g/h/a/d/e;->h:Lcom/facebook/common/internal/Supplier;

    invoke-interface {v1}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_3

    new-instance v2, Lf/g/h/a/b/e/d;

    invoke-direct {v2, v1}, Lf/g/h/a/b/e/d;-><init>(I)V

    new-instance v1, Lf/g/h/a/b/e/c;

    iget-object v3, p0, Lf/g/h/a/d/e;->e:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iget-object v7, p0, Lf/g/h/a/d/e;->c:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v3, v6, v5, v7}, Lf/g/h/a/b/e/c;-><init>(Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Lf/g/h/a/b/c;Landroid/graphics/Bitmap$Config;Ljava/util/concurrent/ExecutorService;)V

    move-object v8, v1

    move-object v7, v2

    goto :goto_2

    :cond_3
    move-object v7, v2

    move-object v8, v7

    :goto_2
    new-instance v1, Lf/g/h/a/b/a;

    iget-object v3, p0, Lf/g/h/a/d/e;->e:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    new-instance v5, Lf/g/h/a/b/f/a;

    invoke-direct {v5, p1}, Lf/g/h/a/b/f/a;-><init>(Lf/g/j/a/a/a;)V

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lf/g/h/a/b/a;-><init>(Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Lf/g/h/a/b/b;Lf/g/h/a/a/d;Lf/g/h/a/b/c;Lf/g/h/a/b/e/a;Lf/g/h/a/b/e/b;)V

    iget-object p1, p0, Lf/g/h/a/d/e;->d:Lf/g/d/k/b;

    iget-object v2, p0, Lf/g/h/a/d/e;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lf/g/h/a/a/c;

    invoke-direct {v3, v1, v1, p1, v2}, Lf/g/h/a/a/c;-><init>(Lf/g/h/a/a/a;Lf/g/h/a/a/c$b;Lf/g/d/k/b;Ljava/util/concurrent/ScheduledExecutorService;)V

    invoke-direct {v0, v3}, Lf/g/h/a/c/a;-><init>(Lf/g/h/a/a/a;)V

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method
