.class public Lf/g/h/a/c/a;
.super Landroid/graphics/drawable/Drawable;
.source "AnimatedDrawable2.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements Lf/g/f/a/a;


# static fields
.field public static final s:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field public static final t:Lf/g/h/a/c/b;


# instance fields
.field public d:Lf/g/h/a/a/a;

.field public e:Lf/g/h/a/e/a;

.field public volatile f:Z

.field public g:J

.field public h:J

.field public i:J

.field public j:I

.field public k:J

.field public l:J

.field public m:I

.field public n:J

.field public o:I

.field public volatile p:Lf/g/h/a/c/b;

.field public q:Lf/g/g/e/e;

.field public final r:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lf/g/h/a/c/a;

    sput-object v0, Lf/g/h/a/c/a;->s:Ljava/lang/Class;

    new-instance v0, Lf/g/h/a/c/b;

    invoke-direct {v0}, Lf/g/h/a/c/b;-><init>()V

    sput-object v0, Lf/g/h/a/c/a;->t:Lf/g/h/a/c/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lf/g/h/a/c/a;-><init>(Lf/g/h/a/a/a;)V

    return-void
.end method

.method public constructor <init>(Lf/g/h/a/a/a;)V
    .locals 2

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    const-wide/16 v0, 0x8

    iput-wide v0, p0, Lf/g/h/a/c/a;->n:J

    sget-object v0, Lf/g/h/a/c/a;->t:Lf/g/h/a/c/b;

    iput-object v0, p0, Lf/g/h/a/c/a;->p:Lf/g/h/a/c/b;

    new-instance v0, Lf/g/h/a/c/a$a;

    invoke-direct {v0, p0}, Lf/g/h/a/c/a$a;-><init>(Lf/g/h/a/c/a;)V

    iput-object v0, p0, Lf/g/h/a/c/a;->r:Ljava/lang/Runnable;

    iput-object p1, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Lf/g/h/a/e/a;

    invoke-direct {v0, p1}, Lf/g/h/a/e/a;-><init>(Lf/g/h/a/a/d;)V

    move-object p1, v0

    :goto_0
    iput-object p1, p0, Lf/g/h/a/c/a;->e:Lf/g/h/a/e/a;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/g/h/a/a/a;->clear()V

    :cond_0
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-eqz v1, :cond_f

    iget-object v1, v0, Lf/g/h/a/c/a;->e:Lf/g/h/a/e/a;

    if-nez v1, :cond_0

    goto/16 :goto_9

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-boolean v3, v0, Lf/g/h/a/c/a;->f:Z

    const-wide/16 v4, 0x0

    if-eqz v3, :cond_1

    iget-wide v6, v0, Lf/g/h/a/c/a;->g:J

    sub-long v6, v1, v6

    add-long/2addr v6, v4

    goto :goto_0

    :cond_1
    iget-wide v6, v0, Lf/g/h/a/c/a;->h:J

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    :goto_0
    iget-object v3, v0, Lf/g/h/a/c/a;->e:Lf/g/h/a/e/a;

    invoke-virtual {v3}, Lf/g/h/a/e/a;->b()J

    move-result-wide v8

    const/4 v10, -0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    cmp-long v13, v8, v4

    if-nez v13, :cond_2

    invoke-virtual {v3, v4, v5}, Lf/g/h/a/e/a;->a(J)I

    move-result v3

    goto :goto_2

    :cond_2
    iget-object v13, v3, Lf/g/h/a/e/a;->a:Lf/g/h/a/a/d;

    invoke-interface {v13}, Lf/g/h/a/a/d;->b()I

    move-result v13

    if-nez v13, :cond_3

    const/4 v13, 0x1

    goto :goto_1

    :cond_3
    const/4 v13, 0x0

    :goto_1
    if-nez v13, :cond_4

    div-long v13, v6, v8

    iget-object v15, v3, Lf/g/h/a/e/a;->a:Lf/g/h/a/a/d;

    invoke-interface {v15}, Lf/g/h/a/a/d;->b()I

    move-result v15

    int-to-long v4, v15

    cmp-long v15, v13, v4

    if-ltz v15, :cond_4

    const/4 v3, -0x1

    goto :goto_2

    :cond_4
    rem-long v4, v6, v8

    invoke-virtual {v3, v4, v5}, Lf/g/h/a/e/a;->a(J)I

    move-result v3

    :goto_2
    if-ne v3, v10, :cond_5

    iget-object v1, v0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    invoke-interface {v1}, Lf/g/h/a/a/d;->a()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    iget-object v1, v0, Lf/g/h/a/c/a;->p:Lf/g/h/a/c/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v12, v0, Lf/g/h/a/c/a;->f:Z

    goto :goto_3

    :cond_5
    if-nez v3, :cond_6

    iget v4, v0, Lf/g/h/a/c/a;->j:I

    if-eq v4, v10, :cond_6

    iget-wide v4, v0, Lf/g/h/a/c/a;->i:J

    cmp-long v8, v1, v4

    if-ltz v8, :cond_6

    iget-object v1, v0, Lf/g/h/a/c/a;->p:Lf/g/h/a/c/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    :goto_3
    iget-object v1, v0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    move-object/from16 v2, p1

    invoke-interface {v1, v0, v2, v3}, Lf/g/h/a/a/a;->j(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v2, v0, Lf/g/h/a/c/a;->p:Lf/g/h/a/c/b;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput v3, v0, Lf/g/h/a/c/a;->j:I

    :cond_7
    if-nez v1, :cond_8

    iget v1, v0, Lf/g/h/a/c/a;->o:I

    add-int/2addr v1, v11

    iput v1, v0, Lf/g/h/a/c/a;->o:I

    const/4 v1, 0x2

    invoke-static {v1}, Lf/g/d/e/a;->h(I)Z

    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-boolean v3, v0, Lf/g/h/a/c/a;->f:Z

    if-eqz v3, :cond_e

    iget-object v3, v0, Lf/g/h/a/c/a;->e:Lf/g/h/a/e/a;

    iget-wide v4, v0, Lf/g/h/a/c/a;->g:J

    sub-long/2addr v1, v4

    invoke-virtual {v3}, Lf/g/h/a/e/a;->b()J

    move-result-wide v4

    const-wide/16 v8, -0x1

    const-wide/16 v13, 0x0

    cmp-long v10, v4, v13

    if-nez v10, :cond_9

    :goto_4
    move-wide/from16 v16, v8

    goto :goto_7

    :cond_9
    iget-object v10, v3, Lf/g/h/a/e/a;->a:Lf/g/h/a/a/d;

    invoke-interface {v10}, Lf/g/h/a/a/d;->b()I

    move-result v10

    if-nez v10, :cond_a

    goto :goto_5

    :cond_a
    const/4 v11, 0x0

    :goto_5
    if-nez v11, :cond_b

    invoke-virtual {v3}, Lf/g/h/a/e/a;->b()J

    move-result-wide v10

    div-long v10, v1, v10

    iget-object v15, v3, Lf/g/h/a/e/a;->a:Lf/g/h/a/a/d;

    invoke-interface {v15}, Lf/g/h/a/a/d;->b()I

    move-result v15

    int-to-long v13, v15

    cmp-long v15, v10, v13

    if-ltz v15, :cond_b

    goto :goto_4

    :cond_b
    rem-long v4, v1, v4

    iget-object v10, v3, Lf/g/h/a/e/a;->a:Lf/g/h/a/a/d;

    invoke-interface {v10}, Lf/g/h/a/a/d;->a()I

    move-result v10

    const/4 v11, 0x0

    const-wide/16 v16, 0x0

    :goto_6
    if-ge v11, v10, :cond_c

    cmp-long v13, v16, v4

    if-gtz v13, :cond_c

    iget-object v13, v3, Lf/g/h/a/e/a;->a:Lf/g/h/a/a/d;

    invoke-interface {v13, v11}, Lf/g/h/a/a/d;->e(I)I

    move-result v13

    int-to-long v13, v13

    add-long v16, v16, v13

    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    :cond_c
    sub-long v16, v16, v4

    add-long v16, v16, v1

    :goto_7
    cmp-long v1, v16, v8

    if-eqz v1, :cond_d

    iget-wide v1, v0, Lf/g/h/a/c/a;->n:J

    add-long v16, v16, v1

    iget-wide v1, v0, Lf/g/h/a/c/a;->g:J

    add-long v1, v1, v16

    iput-wide v1, v0, Lf/g/h/a/c/a;->i:J

    iget-object v3, v0, Lf/g/h/a/c/a;->r:Ljava/lang/Runnable;

    invoke-virtual {v0, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_8

    :cond_d
    iget-object v1, v0, Lf/g/h/a/c/a;->p:Lf/g/h/a/c/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v12, v0, Lf/g/h/a/c/a;->f:Z

    :cond_e
    :goto_8
    iput-wide v6, v0, Lf/g/h/a/c/a;->h:J

    :cond_f
    :goto_9
    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget-object v0, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0

    :cond_0
    invoke-interface {v0}, Lf/g/h/a/a/a;->g()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget-object v0, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0

    :cond_0
    invoke-interface {v0}, Lf/g/h/a/a/a;->i()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lf/g/h/a/c/a;->f:Z

    return v0
.end method

.method public onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lf/g/h/a/a/a;->h(Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method public onLevelChange(I)Z
    .locals 6

    iget-boolean v0, p0, Lf/g/h/a/c/a;->f:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-wide v2, p0, Lf/g/h/a/c/a;->h:J

    int-to-long v4, p1

    cmp-long p1, v2, v4

    if-eqz p1, :cond_1

    iput-wide v4, p0, Lf/g/h/a/c/a;->h:J

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Lf/g/h/a/c/a;->q:Lf/g/g/e/e;

    if-nez v0, :cond_0

    new-instance v0, Lf/g/g/e/e;

    invoke-direct {v0}, Lf/g/g/e/e;-><init>()V

    iput-object v0, p0, Lf/g/h/a/c/a;->q:Lf/g/g/e/e;

    :cond_0
    iget-object v0, p0, Lf/g/h/a/c/a;->q:Lf/g/g/e/e;

    iput p1, v0, Lf/g/g/e/e;->a:I

    iget-object v0, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lf/g/h/a/a/a;->f(I)V

    :cond_1
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    iget-object v0, p0, Lf/g/h/a/c/a;->q:Lf/g/g/e/e;

    if-nez v0, :cond_0

    new-instance v0, Lf/g/g/e/e;

    invoke-direct {v0}, Lf/g/g/e/e;-><init>()V

    iput-object v0, p0, Lf/g/h/a/c/a;->q:Lf/g/g/e/e;

    :cond_0
    iget-object v0, p0, Lf/g/h/a/c/a;->q:Lf/g/g/e/e;

    iput-object p1, v0, Lf/g/g/e/e;->c:Landroid/graphics/ColorFilter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/g/g/e/e;->b:Z

    iget-object v0, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lf/g/h/a/a/a;->d(Landroid/graphics/ColorFilter;)V

    :cond_1
    return-void
.end method

.method public start()V
    .locals 4

    iget-boolean v0, p0, Lf/g/h/a/c/a;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/g/h/a/c/a;->d:Lf/g/h/a/a/a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lf/g/h/a/a/d;->a()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Lf/g/h/a/c/a;->f:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lf/g/h/a/c/a;->k:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lf/g/h/a/c/a;->g:J

    iput-wide v2, p0, Lf/g/h/a/c/a;->i:J

    iget-wide v2, p0, Lf/g/h/a/c/a;->l:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lf/g/h/a/c/a;->h:J

    iget v0, p0, Lf/g/h/a/c/a;->m:I

    iput v0, p0, Lf/g/h/a/c/a;->j:I

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    iget-object v0, p0, Lf/g/h/a/c/a;->p:Lf/g/h/a/c/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method

.method public stop()V
    .locals 4

    iget-boolean v0, p0, Lf/g/h/a/c/a;->f:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lf/g/h/a/c/a;->g:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lf/g/h/a/c/a;->k:J

    iget-wide v2, p0, Lf/g/h/a/c/a;->h:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lf/g/h/a/c/a;->l:J

    iget v0, p0, Lf/g/h/a/c/a;->j:I

    iput v0, p0, Lf/g/h/a/c/a;->m:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/h/a/c/a;->f:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lf/g/h/a/c/a;->g:J

    iput-wide v0, p0, Lf/g/h/a/c/a;->i:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/g/h/a/c/a;->h:J

    const/4 v0, -0x1

    iput v0, p0, Lf/g/h/a/c/a;->j:I

    iget-object v0, p0, Lf/g/h/a/c/a;->r:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lf/g/h/a/c/a;->p:Lf/g/h/a/c/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
