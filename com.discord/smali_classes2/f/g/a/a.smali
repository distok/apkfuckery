.class public Lf/g/a/a;
.super Ljava/lang/Object;
.source "FileBinaryResource.java"


# instance fields
.field public final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/a/a;->a:Ljava/io/File;

    return-void
.end method

.method public static a(Ljava/io/File;)Lf/g/a/a;
    .locals 1

    new-instance v0, Lf/g/a/a;

    invoke-direct {v0, p0}, Lf/g/a/a;-><init>(Ljava/io/File;)V

    return-object v0
.end method


# virtual methods
.method public b()J
    .locals 2

    iget-object v0, p0, Lf/g/a/a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_1

    instance-of v0, p1, Lf/g/a/a;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    check-cast p1, Lf/g/a/a;

    iget-object v0, p0, Lf/g/a/a;->a:Ljava/io/File;

    iget-object p1, p1, Lf/g/a/a;->a:Ljava/io/File;

    invoke-virtual {v0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lf/g/a/a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->hashCode()I

    move-result v0

    return v0
.end method
