.class public Lf/g/n/a/a/a;
.super Landroid/text/style/ReplacementSpan;
.source "BetterImageSpan.java"


# instance fields
.field public d:I

.field public e:I

.field public f:Landroid/graphics/Rect;

.field public final g:I

.field public final h:Landroid/graphics/Paint$FontMetricsInt;

.field public final i:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    iput-object v0, p0, Lf/g/n/a/a/a;->h:Landroid/graphics/Paint$FontMetricsInt;

    iput-object p1, p0, Lf/g/n/a/a/a;->i:Landroid/graphics/drawable/Drawable;

    iput p2, p0, Lf/g/n/a/a/a;->g:I

    invoke-virtual {p0}, Lf/g/n/a/a/a;->b()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Paint$FontMetricsInt;)I
    .locals 3

    iget v0, p0, Lf/g/n/a/a/a;->g:I

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget p1, p0, Lf/g/n/a/a/a;->e:I

    neg-int p1, p1

    return p1

    :cond_0
    iget v0, p1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget p1, p1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v0, p1

    iget v2, p0, Lf/g/n/a/a/a;->e:I

    sub-int/2addr v0, v2

    div-int/2addr v0, v1

    add-int/2addr v0, p1

    return v0

    :cond_1
    iget p1, p1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v0, p0, Lf/g/n/a/a/a;->e:I

    sub-int/2addr p1, v0

    return p1
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lf/g/n/a/a/a;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lf/g/n/a/a/a;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lf/g/n/a/a/a;->d:I

    iget-object v0, p0, Lf/g/n/a/a/a;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lf/g/n/a/a/a;->e:I

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 0

    iget-object p2, p0, Lf/g/n/a/a/a;->h:Landroid/graphics/Paint$FontMetricsInt;

    invoke-virtual {p9, p2}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    iget-object p2, p0, Lf/g/n/a/a/a;->h:Landroid/graphics/Paint$FontMetricsInt;

    invoke-virtual {p0, p2}, Lf/g/n/a/a/a;->a(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result p2

    add-int/2addr p2, p7

    int-to-float p3, p2

    invoke-virtual {p1, p5, p3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object p3, p0, Lf/g/n/a/a/a;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    neg-float p3, p5

    neg-int p2, p2

    int-to-float p2, p2

    invoke-virtual {p1, p3, p2}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 0

    invoke-virtual {p0}, Lf/g/n/a/a/a;->b()V

    if-nez p5, :cond_0

    iget p1, p0, Lf/g/n/a/a/a;->d:I

    return p1

    :cond_0
    invoke-virtual {p0, p5}, Lf/g/n/a/a/a;->a(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result p1

    iget p2, p0, Lf/g/n/a/a/a;->e:I

    add-int/2addr p2, p1

    iget p3, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    if-ge p1, p3, :cond_1

    iput p1, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    :cond_1
    iget p3, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    if-ge p1, p3, :cond_2

    iput p1, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    :cond_2
    iget p1, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    if-le p2, p1, :cond_3

    iput p2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    :cond_3
    iget p1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    if-le p2, p1, :cond_4

    iput p2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    :cond_4
    iget p1, p0, Lf/g/n/a/a/a;->d:I

    return p1
.end method
