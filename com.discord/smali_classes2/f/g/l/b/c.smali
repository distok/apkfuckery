.class public Lf/g/l/b/c;
.super Ljava/lang/Object;
.source "DefaultZoomableController.java"

# interfaces
.implements Lf/g/l/b/e;
.implements Lf/g/l/a/b$a;


# instance fields
.field public a:Lf/g/l/a/b;

.field public b:Lf/g/l/b/e$a;

.field public c:Z

.field public final d:Landroid/graphics/RectF;

.field public final e:Landroid/graphics/RectF;

.field public final f:Landroid/graphics/RectF;

.field public final g:Landroid/graphics/Matrix;

.field public final h:Landroid/graphics/Matrix;

.field public final i:Landroid/graphics/Matrix;

.field public final j:[F

.field public final k:Landroid/graphics/RectF;

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-void
.end method

.method public constructor <init>(Lf/g/l/a/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/g/l/b/c;->b:Lf/g/l/b/e$a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/l/b/c;->c:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/g/l/b/c;->f:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/g/l/b/c;->g:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lf/g/l/b/c;->i:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lf/g/l/b/c;->j:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lf/g/l/b/c;->k:Landroid/graphics/RectF;

    iput-object p1, p0, Lf/g/l/b/c;->a:Lf/g/l/a/b;

    iput-object p0, p1, Lf/g/l/a/b;->b:Lf/g/l/a/b$a;

    return-void
.end method

.method public static l(II)Z
    .locals 0

    and-int/2addr p0, p1

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public a()Z
    .locals 5

    iget-object v0, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lf/g/l/b/c;->j:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lf/g/l/b/c;->j:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v2, 0x4

    aget v4, v0, v2

    sub-float/2addr v4, v3

    aput v4, v0, v2

    const/16 v2, 0x8

    aget v4, v0, v2

    sub-float/2addr v4, v3

    aput v4, v0, v2

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lf/g/l/b/c;->j:[F

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v3, 0x3a83126f    # 0.001f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method public b(Lf/g/l/a/b;)V
    .locals 10

    const-class p1, Lf/g/l/b/c;

    const-string v0, "onGestureUpdate"

    invoke-static {p1, v0}, Lf/g/d/e/a;->i(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object p1, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    iget-object v0, p0, Lf/g/l/b/c;->a:Lf/g/l/a/b;

    iget-object v1, p0, Lf/g/l/b/c;->g:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v1, v0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget v2, v1, Lf/g/l/a/a;->b:I

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-ge v2, v3, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    iget-object v2, v1, Lf/g/l/a/a;->d:[F

    const/4 v3, 0x1

    aget v5, v2, v3

    aget v2, v2, v4

    sub-float/2addr v5, v2

    iget-object v2, v1, Lf/g/l/a/a;->e:[F

    aget v6, v2, v3

    aget v2, v2, v4

    sub-float/2addr v6, v2

    iget-object v2, v1, Lf/g/l/a/a;->f:[F

    aget v7, v2, v3

    aget v2, v2, v4

    sub-float/2addr v7, v2

    iget-object v1, v1, Lf/g/l/a/a;->g:[F

    aget v2, v1, v3

    aget v1, v1, v4

    sub-float/2addr v2, v1

    float-to-double v8, v5

    float-to-double v5, v6

    invoke-static {v8, v9, v5, v6}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v5

    double-to-float v1, v5

    float-to-double v5, v7

    float-to-double v2, v2

    invoke-static {v5, v6, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v1, v2, v1

    :goto_0
    invoke-virtual {v0}, Lf/g/l/a/b;->b()F

    move-result v2

    invoke-virtual {v0}, Lf/g/l/a/b;->c()F

    move-result v3

    invoke-virtual {p1, v1, v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-virtual {v0}, Lf/g/l/a/b;->b()F

    move-result v1

    invoke-virtual {v0}, Lf/g/l/a/b;->c()F

    move-result v2

    const/4 v3, 0x7

    invoke-virtual {p0, p1, v1, v2, v3}, Lf/g/l/b/c;->f(Landroid/graphics/Matrix;FFI)Z

    move-result v1

    or-int/2addr v1, v4

    iget-object v2, v0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget-object v4, v2, Lf/g/l/a/a;->f:[F

    iget v2, v2, Lf/g/l/a/a;->b:I

    invoke-virtual {v0, v4, v2}, Lf/g/l/a/b;->a([FI)F

    move-result v2

    iget-object v4, v0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget-object v5, v4, Lf/g/l/a/a;->d:[F

    iget v4, v4, Lf/g/l/a/a;->b:I

    invoke-virtual {v0, v5, v4}, Lf/g/l/a/b;->a([FI)F

    move-result v4

    sub-float/2addr v2, v4

    iget-object v4, v0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget-object v5, v4, Lf/g/l/a/a;->g:[F

    iget v4, v4, Lf/g/l/a/a;->b:I

    invoke-virtual {v0, v5, v4}, Lf/g/l/a/b;->a([FI)F

    move-result v4

    iget-object v5, v0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget-object v6, v5, Lf/g/l/a/a;->e:[F

    iget v5, v5, Lf/g/l/a/a;->b:I

    invoke-virtual {v0, v6, v5}, Lf/g/l/a/b;->a([FI)F

    move-result v0

    sub-float/2addr v4, v0

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0, p1, v3}, Lf/g/l/b/c;->g(Landroid/graphics/Matrix;I)Z

    move-result p1

    or-int/2addr p1, v1

    invoke-virtual {p0}, Lf/g/l/b/c;->i()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lf/g/l/b/c;->a:Lf/g/l/a/b;

    invoke-virtual {v0}, Lf/g/l/a/b;->d()V

    :cond_1
    iput-boolean p1, p0, Lf/g/l/b/c;->l:Z

    return-void
.end method

.method public c(Lf/g/l/a/b;)V
    .locals 5

    const-class p1, Lf/g/l/b/c;

    const-string v0, "onGestureBegin"

    invoke-static {p1, v0}, Lf/g/d/e/a;->i(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object p1, p0, Lf/g/l/b/c;->g:Landroid/graphics/Matrix;

    iget-object v0, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object p1, p0, Lf/g/l/b/c;->f:Landroid/graphics/RectF;

    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    const v3, 0x3a83126f    # 0.001f

    sub-float/2addr v2, v3

    const/4 v4, 0x1

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v2, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v2, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    iget v0, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v3

    cmpl-float p1, p1, v0

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    xor-int/2addr p1, v4

    iput-boolean p1, p0, Lf/g/l/b/c;->l:Z

    return-void
.end method

.method public final d(FFFFF)F
    .locals 4

    sub-float v0, p2, p1

    sub-float v1, p4, p3

    sub-float v2, p5, p3

    sub-float v3, p4, p5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v2, v2, v3

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    add-float/2addr p2, p1

    div-float/2addr p2, v3

    sub-float/2addr p5, p2

    return p5

    :cond_0
    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    add-float v0, p3, p4

    div-float/2addr v0, v3

    cmpg-float p5, p5, v0

    if-gez p5, :cond_1

    sub-float/2addr p3, p1

    return p3

    :cond_1
    sub-float/2addr p4, p2

    return p4

    :cond_2
    cmpl-float p5, p1, p3

    if-lez p5, :cond_3

    sub-float/2addr p3, p1

    return p3

    :cond_3
    cmpg-float p1, p2, p4

    if-gez p1, :cond_4

    sub-float/2addr p4, p2

    return p4

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method public e()F
    .locals 2

    iget-object v0, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lf/g/l/b/c;->j:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lf/g/l/b/c;->j:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final f(Landroid/graphics/Matrix;FFI)Z
    .locals 3

    const/4 v0, 0x4

    invoke-static {p4, v0}, Lf/g/l/b/c;->l(II)Z

    move-result p4

    const/4 v0, 0x0

    if-nez p4, :cond_0

    return v0

    :cond_0
    iget-object p4, p0, Lf/g/l/b/c;->j:[F

    invoke-virtual {p1, p4}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object p4, p0, Lf/g/l/b/c;->j:[F

    aget p4, p4, v0

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, p4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    cmpl-float v2, v1, p4

    if-eqz v2, :cond_1

    div-float/2addr v1, p4

    invoke-virtual {p1, v1, v1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    const/4 p1, 0x1

    return p1

    :cond_1
    return v0
.end method

.method public final g(Landroid/graphics/Matrix;I)Z
    .locals 11

    const/4 v0, 0x3

    invoke-static {p2, v0}, Lf/g/l/b/c;->l(II)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lf/g/l/b/c;->k:Landroid/graphics/RectF;

    iget-object v2, p0, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    const/4 v2, 0x1

    invoke-static {p2, v2}, Lf/g/l/b/c;->l(II)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v0, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    iget v8, v3, Landroid/graphics/RectF;->left:F

    iget v9, v3, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v10

    move-object v5, p0

    invoke-virtual/range {v5 .. v10}, Lf/g/l/b/c;->d(FFFFF)F

    move-result v3

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    const/4 v5, 0x2

    invoke-static {p2, v5}, Lf/g/l/b/c;->l(II)Z

    move-result p2

    if-eqz p2, :cond_2

    iget v6, v0, Landroid/graphics/RectF;->top:F

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    iget-object p2, p0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    iget v8, p2, Landroid/graphics/RectF;->top:F

    iget v9, p2, Landroid/graphics/RectF;->bottom:F

    iget-object p2, p0, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v10

    move-object v5, p0

    invoke-virtual/range {v5 .. v10}, Lf/g/l/b/c;->d(FFFFF)F

    move-result p2

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    :goto_1
    cmpl-float v0, v3, v4

    if-nez v0, :cond_4

    cmpl-float v0, p2, v4

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    return v1

    :cond_4
    :goto_2
    invoke-virtual {p1, v3, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return v2
.end method

.method public h(Landroid/view/MotionEvent;)Z
    .locals 10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    sget v0, Lf/g/d/e/a;->a:I

    iget-boolean v0, p0, Lf/g/l/b/c;->c:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lf/g/l/b/c;->a:Lf/g/l/a/b;

    iget-object v0, v0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    const/4 v3, 0x6

    const/4 v4, 0x2

    const/4 v5, -0x1

    const/4 v6, 0x1

    if-eqz v2, :cond_5

    if-eq v2, v6, :cond_5

    if-eq v2, v4, :cond_1

    const/4 v7, 0x3

    if-eq v2, v7, :cond_0

    const/4 v7, 0x5

    if-eq v2, v7, :cond_5

    if-eq v2, v3, :cond_5

    goto/16 :goto_5

    :cond_0
    invoke-virtual {v0}, Lf/g/l/a/a;->c()V

    invoke-virtual {v0}, Lf/g/l/a/a;->a()V

    goto/16 :goto_5

    :cond_1
    :goto_0
    if-ge v1, v4, :cond_3

    iget-object v2, v0, Lf/g/l/a/a;->c:[I

    aget v2, v2, v1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    if-eq v2, v5, :cond_2

    iget-object v3, v0, Lf/g/l/a/a;->f:[F

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    aput v7, v3, v1

    iget-object v3, v0, Lf/g/l/a/a;->g:[F

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    aput v2, v3, v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-boolean p1, v0, Lf/g/l/a/a;->a:Z

    if-nez p1, :cond_4

    iget p1, v0, Lf/g/l/a/a;->b:I

    if-lez p1, :cond_4

    invoke-virtual {v0}, Lf/g/l/a/a;->b()V

    :cond_4
    iget-boolean p1, v0, Lf/g/l/a/a;->a:Z

    if-eqz p1, :cond_b

    iget-object p1, v0, Lf/g/l/a/a;->h:Lf/g/l/a/a$a;

    if-eqz p1, :cond_b

    check-cast p1, Lf/g/l/a/b;

    iget-object v0, p1, Lf/g/l/a/b;->b:Lf/g/l/a/b$a;

    if-eqz v0, :cond_b

    invoke-interface {v0, p1}, Lf/g/l/a/b$a;->b(Lf/g/l/a/b;)V

    goto :goto_5

    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    invoke-virtual {v0}, Lf/g/l/a/a;->c()V

    iput v1, v0, Lf/g/l/a/a;->b:I

    :goto_1
    if-ge v1, v4, :cond_a

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v8

    if-eq v7, v6, :cond_6

    if-ne v7, v3, :cond_7

    :cond_6
    if-lt v1, v8, :cond_7

    add-int/lit8 v7, v1, 0x1

    goto :goto_2

    :cond_7
    move v7, v1

    :goto_2
    if-ge v7, v2, :cond_8

    goto :goto_3

    :cond_8
    const/4 v7, -0x1

    :goto_3
    if-ne v7, v5, :cond_9

    iget-object v2, v0, Lf/g/l/a/a;->c:[I

    aput v5, v2, v1

    goto :goto_4

    :cond_9
    iget-object v2, v0, Lf/g/l/a/a;->c:[I

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v8

    aput v8, v2, v1

    iget-object v2, v0, Lf/g/l/a/a;->f:[F

    iget-object v8, v0, Lf/g/l/a/a;->d:[F

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    aput v9, v8, v1

    aput v9, v2, v1

    iget-object v2, v0, Lf/g/l/a/a;->g:[F

    iget-object v8, v0, Lf/g/l/a/a;->e:[F

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    aput v7, v8, v1

    aput v7, v2, v1

    iget v2, v0, Lf/g/l/a/a;->b:I

    add-int/2addr v2, v6

    iput v2, v0, Lf/g/l/a/a;->b:I

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_a
    iget p1, v0, Lf/g/l/a/a;->b:I

    if-lez p1, :cond_b

    invoke-virtual {v0}, Lf/g/l/a/a;->b()V

    :cond_b
    :goto_5
    return v6

    :cond_c
    return v1
.end method

.method public final i()V
    .locals 3

    iget-object v0, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lf/g/l/b/c;->f:Landroid/graphics/RectF;

    iget-object v2, p0, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lf/g/l/b/c;->b:Lf/g/l/b/e$a;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lf/g/l/b/c;->c:Z

    if-eqz v1, :cond_1

    check-cast v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView$b;

    iget-object v0, v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView$b;->a:Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    invoke-virtual {v0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    sget v1, Lf/g/d/e/a;->a:I

    iget-object v1, v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->g:Lcom/facebook/drawee/interfaces/DraweeController;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v1, Lf/g/l/b/c;

    invoke-virtual {v1}, Lf/g/l/b/c;->e()F

    move-result v1

    const v2, 0x3f8ccccd    # 1.1f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->g:Lcom/facebook/drawee/interfaces/DraweeController;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->a(Lcom/facebook/drawee/interfaces/DraweeController;Lcom/facebook/drawee/interfaces/DraweeController;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    :cond_1
    return-void
.end method

.method public j()V
    .locals 2

    const-class v0, Lf/g/l/b/c;

    const-string v1, "reset"

    invoke-static {v0, v1}, Lf/g/d/e/a;->i(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v0, p0, Lf/g/l/b/c;->a:Lf/g/l/a/b;

    iget-object v0, v0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    invoke-virtual {v0}, Lf/g/l/a/a;->a()V

    iget-object v0, p0, Lf/g/l/b/c;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    invoke-virtual {p0}, Lf/g/l/b/c;->i()V

    return-void
.end method

.method public k(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/g/l/b/c;->c:Z

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lf/g/l/b/c;->j()V

    :cond_0
    return-void
.end method
