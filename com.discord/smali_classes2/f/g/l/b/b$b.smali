.class public Lf/g/l/b/b$b;
.super Landroid/animation/AnimatorListenerAdapter;
.source "AnimatedZoomableController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/l/b/b;->n(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Runnable;

.field public final synthetic b:Lf/g/l/b/b;


# direct methods
.method public constructor <init>(Lf/g/l/b/b;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lf/g/l/b/b$b;->b:Lf/g/l/b/b;

    iput-object p2, p0, Lf/g/l/b/b$b;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    iget-object p1, p0, Lf/g/l/b/b$b;->b:Lf/g/l/b/b;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class p1, Lf/g/l/b/b;

    const-string v0, "setTransformAnimated: animation cancelled"

    invoke-static {p1, v0}, Lf/g/d/e/a;->i(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object p1, p0, Lf/g/l/b/b$b;->a:Ljava/lang/Runnable;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object p1, p0, Lf/g/l/b/b$b;->b:Lf/g/l/b/b;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lf/g/l/b/a;->m:Z

    iget-object p1, p1, Lf/g/l/b/c;->a:Lf/g/l/a/b;

    invoke-virtual {p1}, Lf/g/l/a/b;->d()V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    iget-object p1, p0, Lf/g/l/b/b$b;->b:Lf/g/l/b/b;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class p1, Lf/g/l/b/b;

    const-string v0, "setTransformAnimated: animation finished"

    invoke-static {p1, v0}, Lf/g/d/e/a;->i(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object p1, p0, Lf/g/l/b/b$b;->a:Ljava/lang/Runnable;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object p1, p0, Lf/g/l/b/b$b;->b:Lf/g/l/b/b;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lf/g/l/b/a;->m:Z

    iget-object p1, p1, Lf/g/l/b/c;->a:Lf/g/l/a/b;

    invoke-virtual {p1}, Lf/g/l/a/b;->d()V

    return-void
.end method
