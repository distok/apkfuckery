.class public Lf/g/l/b/b;
.super Lf/g/l/b/a;
.source "AnimatedZoomableController.java"


# instance fields
.field public final s:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Lf/g/l/a/b;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/g/l/b/a;-><init>(Lf/g/l/a/b;)V

    const/4 p1, 0x2

    new-array p1, p1, [F

    fill-array-data p1, :array_0

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    iput-object p1, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public m()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    const-class v0, Lf/g/l/b/b;

    return-object v0
.end method

.method public n(Landroid/graphics/Matrix;JLjava/lang/Runnable;)V
    .locals 4
    .param p4    # Ljava/lang/Runnable;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    sget v0, Lf/g/d/e/a;->a:I

    invoke-virtual {p0}, Lf/g/l/b/b;->o()V

    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    cmp-long v3, p2, v1

    if-lez v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ls/a/b/b/a;->g(Z)V

    iget-boolean v1, p0, Lf/g/l/b/a;->m:Z

    xor-int/2addr v1, v0

    invoke-static {v1}, Ls/a/b/b/a;->j(Z)V

    iput-boolean v0, p0, Lf/g/l/b/a;->m:Z

    iget-object v0, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object p2, p0, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    iget-object p3, p0, Lf/g/l/b/a;->n:[F

    invoke-virtual {p2, p3}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object p2, p0, Lf/g/l/b/a;->o:[F

    invoke-virtual {p1, p2}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object p1, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    new-instance p2, Lf/g/l/b/b$a;

    invoke-direct {p2, p0}, Lf/g/l/b/b$a;-><init>(Lf/g/l/b/b;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object p1, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    new-instance p2, Lf/g/l/b/b$b;

    invoke-direct {p2, p0, p4}, Lf/g/l/b/b$b;-><init>(Lf/g/l/b/b;Ljava/lang/Runnable;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object p1, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method public o()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    iget-boolean v0, p0, Lf/g/l/b/a;->m:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-class v0, Lf/g/l/b/b;

    const-string v1, "stopAnimation"

    invoke-static {v0, v1}, Lf/g/d/e/a;->i(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v0, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    iget-object v0, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v0, p0, Lf/g/l/b/b;->s:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    return-void
.end method
