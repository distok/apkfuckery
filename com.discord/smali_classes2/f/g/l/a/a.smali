.class public Lf/g/l/a/a;
.super Ljava/lang/Object;
.source "MultiPointerGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/l/a/a$a;
    }
.end annotation


# instance fields
.field public a:Z

.field public b:I

.field public final c:[I

.field public final d:[F

.field public final e:[F

.field public final f:[F

.field public final g:[F

.field public h:Lf/g/l/a/a$a;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v1, v0, [I

    iput-object v1, p0, Lf/g/l/a/a;->c:[I

    new-array v1, v0, [F

    iput-object v1, p0, Lf/g/l/a/a;->d:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lf/g/l/a/a;->e:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lf/g/l/a/a;->f:[F

    new-array v0, v0, [F

    iput-object v0, p0, Lf/g/l/a/a;->g:[F

    const/4 v0, 0x0

    iput-object v0, p0, Lf/g/l/a/a;->h:Lf/g/l/a/a$a;

    invoke-virtual {p0}, Lf/g/l/a/a;->a()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/l/a/a;->a:Z

    iput v0, p0, Lf/g/l/a/a;->b:I

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lf/g/l/a/a;->c:[I

    const/4 v2, -0x1

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Lf/g/l/a/a;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/g/l/a/a;->h:Lf/g/l/a/a$a;

    if-eqz v0, :cond_0

    check-cast v0, Lf/g/l/a/b;

    iget-object v1, v0, Lf/g/l/a/b;->b:Lf/g/l/a/b$a;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lf/g/l/a/b$a;->c(Lf/g/l/a/b;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/g/l/a/a;->a:Z

    :cond_1
    return-void
.end method

.method public final c()V
    .locals 2

    iget-boolean v0, p0, Lf/g/l/a/a;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/l/a/a;->a:Z

    iget-object v0, p0, Lf/g/l/a/a;->h:Lf/g/l/a/a$a;

    if-eqz v0, :cond_0

    check-cast v0, Lf/g/l/a/b;

    iget-object v0, v0, Lf/g/l/a/b;->b:Lf/g/l/a/b$a;

    if-eqz v0, :cond_0

    check-cast v0, Lf/g/l/b/c;

    const-class v0, Lf/g/l/b/c;

    const-string v1, "onGestureEnd"

    invoke-static {v0, v1}, Lf/g/d/e/a;->i(Ljava/lang/Class;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
