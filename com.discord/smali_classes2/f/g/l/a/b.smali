.class public Lf/g/l/a/b;
.super Ljava/lang/Object;
.source "TransformGestureDetector.java"

# interfaces
.implements Lf/g/l/a/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/l/a/b$a;
    }
.end annotation


# instance fields
.field public final a:Lf/g/l/a/a;

.field public b:Lf/g/l/a/b$a;


# direct methods
.method public constructor <init>(Lf/g/l/a/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/g/l/a/b;->b:Lf/g/l/a/b$a;

    iput-object p1, p0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iput-object p0, p1, Lf/g/l/a/a;->h:Lf/g/l/a/a$a;

    return-void
.end method


# virtual methods
.method public final a([FI)F
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    aget v3, p1, v1

    add-float/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-lez p2, :cond_1

    int-to-float p1, p2

    div-float v0, v2, p1

    :cond_1
    return v0
.end method

.method public b()F
    .locals 2

    iget-object v0, p0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget-object v1, v0, Lf/g/l/a/a;->d:[F

    iget v0, v0, Lf/g/l/a/a;->b:I

    invoke-virtual {p0, v1, v0}, Lf/g/l/a/b;->a([FI)F

    move-result v0

    return v0
.end method

.method public c()F
    .locals 2

    iget-object v0, p0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget-object v1, v0, Lf/g/l/a/a;->e:[F

    iget v0, v0, Lf/g/l/a/a;->b:I

    invoke-virtual {p0, v1, v0}, Lf/g/l/a/b;->a([FI)F

    move-result v0

    return v0
.end method

.method public d()V
    .locals 4

    iget-object v0, p0, Lf/g/l/a/b;->a:Lf/g/l/a/a;

    iget-boolean v1, v0, Lf/g/l/a/a;->a:Z

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lf/g/l/a/a;->c()V

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    iget-object v2, v0, Lf/g/l/a/a;->d:[F

    iget-object v3, v0, Lf/g/l/a/a;->f:[F

    aget v3, v3, v1

    aput v3, v2, v1

    iget-object v2, v0, Lf/g/l/a/a;->e:[F

    iget-object v3, v0, Lf/g/l/a/a;->g:[F

    aget v3, v3, v1

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lf/g/l/a/a;->b()V

    :goto_1
    return-void
.end method
