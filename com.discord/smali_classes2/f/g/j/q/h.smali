.class public Lf/g/j/q/h;
.super Ljava/lang/Object;
.source "BitmapMemoryCacheProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/c/j;

.field public final c:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;",
            "Lf/g/j/c/j;",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/h;->a:Lf/g/j/c/t;

    iput-object p2, p0, Lf/g/j/q/h;->b:Lf/g/j/c/j;

    iput-object p3, p0, Lf/g/j/q/h;->c:Lf/g/j/q/u0;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    invoke-interface {p2}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lf/g/j/q/h;->b:Lf/g/j/c/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast v3, Lf/g/j/c/n;

    :try_start_1
    invoke-virtual {v3, v1, v2}, Lf/g/j/c/n;->a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v1

    iget-object v2, p0, Lf/g/j/q/h;->a:Lf/g/j/c/t;

    invoke-interface {v2, v1}, Lf/g/j/c/t;->get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v3, 0x1

    const-string v4, "memory_bitmap"

    const-string v5, "cached_value_found"

    const/4 v6, 0x0

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/g/j/j/g;

    invoke-interface {v7}, Lf/g/j/j/g;->a()Ljava/util/Map;

    move-result-object v7

    invoke-interface {p2, v7}, Lf/g/j/q/v0;->j(Ljava/util/Map;)V

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lf/g/j/j/c;

    invoke-virtual {v7}, Lf/g/j/j/c;->b()Lf/g/j/j/i;

    move-result-object v7

    check-cast v7, Lf/g/j/j/h;

    iget-boolean v7, v7, Lf/g/j/j/h;->c:Z

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0, p2, v9}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "true"

    invoke-static {v5, v9}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v9

    goto :goto_0

    :cond_0
    move-object v9, v6

    :goto_0
    invoke-interface {v0, p2, v8, v9}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, p2, v8, v3}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lf/g/j/q/h;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2, v4, v8}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {p1, v8}, Lf/g/j/q/l;->a(F)V

    :cond_1
    invoke-interface {p1, v2, v7}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v7, :cond_2

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :cond_2
    :try_start_3
    invoke-interface {p2}, Lf/g/j/q/v0;->q()Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v2

    sget-object v7, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    invoke-virtual {v7}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v8, "false"

    if-lt v2, v7, :cond_4

    :try_start_4
    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p2, v2}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v5, v8}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    goto :goto_1

    :cond_3
    move-object v2, v6

    :goto_1
    invoke-interface {v0, p2, v1, v2}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, p2, v1, v2}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lf/g/j/q/h;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v4, v0}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v6, v3}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :cond_4
    :try_start_5
    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v2

    iget-boolean v2, v2, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    invoke-virtual {p0, p1, v1, v2}, Lf/g/j/q/h;->e(Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;Z)Lf/g/j/q/l;

    move-result-object p1

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lf/g/j/q/h;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p2, v2}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {v5, v8}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v6

    :cond_5
    invoke-interface {v0, p2, v1, v6}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/q/h;->c:Lf/g/j/q/u0;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const-string v0, "pipe_bg"

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    const-string v0, "BitmapMemoryCacheProducer"

    return-object v0
.end method

.method public e(Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;Z)Lf/g/j/q/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lcom/facebook/cache/common/CacheKey;",
            "Z)",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Lf/g/j/q/h$a;

    invoke-direct {v0, p0, p1, p2, p3}, Lf/g/j/q/h$a;-><init>(Lf/g/j/q/h;Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;Z)V

    return-object v0
.end method
