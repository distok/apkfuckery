.class public Lf/g/j/q/b0$a;
.super Ljava/lang/Object;
.source "JobScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/b0;-><init>(Ljava/util/concurrent/Executor;Lf/g/j/q/b0$c;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/g/j/q/b0;


# direct methods
.method public constructor <init>(Lf/g/j/q/b0;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/b0$a;->d:Lf/g/j/q/b0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/g/j/q/b0$a;->d:Lf/g/j/q/b0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    monitor-enter v0

    :try_start_0
    iget-object v3, v0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    iget v4, v0, Lf/g/j/q/b0;->g:I

    const/4 v5, 0x0

    iput-object v5, v0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    const/4 v5, 0x0

    iput v5, v0, Lf/g/j/q/b0;->g:I

    sget-object v5, Lf/g/j/q/b0$d;->f:Lf/g/j/q/b0$d;

    iput-object v5, v0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    iput-wide v1, v0, Lf/g/j/q/b0;->j:J

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {v3, v4}, Lf/g/j/q/b0;->e(Lf/g/j/j/e;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/g/j/q/b0;->b:Lf/g/j/q/b0$c;

    invoke-interface {v1, v3, v4}, Lf/g/j/q/b0$c;->a(Lf/g/j/j/e;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lf/g/j/j/e;->close()V

    :cond_1
    invoke-virtual {v0}, Lf/g/j/q/b0;->c()V

    return-void

    :catchall_0
    move-exception v1

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lf/g/j/j/e;->close()V

    :cond_2
    invoke-virtual {v0}, Lf/g/j/q/b0;->c()V

    throw v1

    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method
