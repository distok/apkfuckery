.class public Lf/g/j/q/u$a;
.super Lf/g/j/q/o;
.source "EncodedMemoryCacheProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/cache/common/CacheKey;

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(Lf/g/j/q/l;Lf/g/j/c/t;Lcom/facebook/cache/common/CacheKey;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;",
            "Lcom/facebook/cache/common/CacheKey;",
            "ZZ)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p2, p0, Lf/g/j/q/u$a;->c:Lf/g/j/c/t;

    iput-object p3, p0, Lf/g/j/q/u$a;->d:Lcom/facebook/cache/common/CacheKey;

    iput-boolean p4, p0, Lf/g/j/q/u$a;->e:Z

    iput-boolean p5, p0, Lf/g/j/q/u$a;->f:Z

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lf/g/j/j/e;

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    const/16 v0, 0xa

    invoke-static {p2, v0}, Lf/g/j/q/b;->l(II)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v0, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v1, Lf/g/i/c;->b:Lf/g/i/c;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lf/g/j/j/e;->c()Lcom/facebook/common/references/CloseableReference;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    :try_start_1
    iget-boolean v2, p0, Lf/g/j/q/u$a;->f:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lf/g/j/q/u$a;->e:Z

    if-eqz v2, :cond_1

    iget-object v1, p0, Lf/g/j/q/u$a;->c:Lf/g/j/c/t;

    iget-object v2, p0, Lf/g/j/q/u$a;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-interface {v1, v2, v0}, Lf/g/j/c/t;->a(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_1
    :try_start_2
    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-eqz v1, :cond_2

    :try_start_3
    new-instance v0, Lf/g/j/j/e;

    invoke-direct {v0, v1}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V

    invoke-virtual {v0, p1}, Lf/g/j/j/e;->b(Lf/g/j/j/e;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {p1, v1}, Lf/g/j/q/l;->a(F)V

    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p1, v0, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v0}, Lf/g/j/j/e;->close()V

    goto :goto_1

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Lf/g/j/j/e;->close()V

    throw p1

    :catchall_1
    move-exception p1

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p1

    :catchall_2
    move-exception p1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p1

    :cond_2
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_1

    :cond_3
    :goto_0
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :goto_1
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_3
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
