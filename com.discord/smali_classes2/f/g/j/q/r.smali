.class public Lf/g/j/q/r;
.super Ljava/lang/Object;
.source "DiskCacheReadProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/g;

.field public final b:Lf/g/j/c/g;

.field public final c:Lf/g/j/c/j;

.field public final d:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/r;->a:Lf/g/j/c/g;

    iput-object p2, p0, Lf/g/j/q/r;->b:Lf/g/j/c/g;

    iput-object p3, p0, Lf/g/j/q/r;->c:Lf/g/j/c/j;

    iput-object p4, p0, Lf/g/j/q/r;->d:Lf/g/j/q/u0;

    return-void
.end method

.method public static c(Lf/g/j/q/x0;Lf/g/j/q/v0;ZI)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/x0;",
            "Lf/g/j/q/v0;",
            "ZI)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "DiskCacheProducer"

    invoke-interface {p0, p1, v0}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string p0, "cached_value_found"

    if-eqz p2, :cond_1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "encodedImageSize"

    invoke-static {p0, p1, p3, p2}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-boolean v1, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    const/4 v2, 0x1

    if-nez v1, :cond_1

    invoke-interface {p2}, Lf/g/j/q/v0;->q()Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v0

    sget-object v1, Lcom/facebook/imagepipeline/request/ImageRequest$c;->e:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    invoke-virtual {v1}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v1

    if-lt v0, v1, :cond_0

    const-string v0, "disk"

    const-string v1, "nil-result_read"

    invoke-interface {p2, v0, v1}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x0

    invoke-interface {p1, p2, v2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/j/q/r;->d:Lf/g/j/q/u0;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :goto_0
    return-void

    :cond_1
    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v1

    const-string v3, "DiskCacheProducer"

    invoke-interface {v1, p2, v3}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    iget-object v1, p0, Lf/g/j/q/r;->c:Lf/g/j/c/j;

    invoke-interface {p2}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v1, Lf/g/j/c/n;

    invoke-virtual {v1, v0, v3}, Lf/g/j/c/n;->b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v1

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    sget-object v3, Lcom/facebook/imagepipeline/request/ImageRequest$b;->d:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    const/4 v4, 0x0

    if-ne v0, v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_3

    iget-object v0, p0, Lf/g/j/q/r;->b:Lf/g/j/c/g;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lf/g/j/q/r;->a:Lf/g/j/c/g;

    :goto_2
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Lf/g/j/c/g;->e(Lcom/facebook/cache/common/CacheKey;Ljava/util/concurrent/atomic/AtomicBoolean;)Lu/g;

    move-result-object v0

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v1

    new-instance v3, Lf/g/j/q/p;

    invoke-direct {v3, p0, v1, p2, p1}, Lf/g/j/q/p;-><init>(Lf/g/j/q/r;Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/q/l;)V

    invoke-virtual {v0, v3}, Lu/g;->b(Lu/c;)Lu/g;

    new-instance p1, Lf/g/j/q/q;

    invoke-direct {p1, p0, v2}, Lf/g/j/q/q;-><init>(Lf/g/j/q/r;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-interface {p2, p1}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    return-void
.end method
