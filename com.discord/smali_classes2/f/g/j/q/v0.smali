.class public interface abstract Lf/g/j/q/v0;
.super Ljava/lang/Object;
.source "ProducerContext.java"


# virtual methods
.method public abstract a()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b()Ljava/lang/Object;
.end method

.method public abstract c()Lf/g/j/d/d;
.end method

.method public abstract d(Ljava/lang/String;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TE;)V"
        }
    .end annotation
.end method

.method public abstract e()Lcom/facebook/imagepipeline/request/ImageRequest;
.end method

.method public abstract f(Lf/g/j/q/w0;)V
.end method

.method public abstract g()Lf/g/j/e/k;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract h(Lf/g/j/j/f;)V
.end method

.method public abstract i(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract j(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation
.end method

.method public abstract k()Z
.end method

.method public abstract l(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract m()Ljava/lang/String;
.end method

.method public abstract n(Ljava/lang/String;)V
.end method

.method public abstract o()Lf/g/j/q/x0;
.end method

.method public abstract p()Z
.end method

.method public abstract q()Lcom/facebook/imagepipeline/request/ImageRequest$c;
.end method
