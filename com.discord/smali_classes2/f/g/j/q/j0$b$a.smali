.class public Lf/g/j/q/j0$b$a;
.super Lf/g/j/q/b;
.source "MultiplexProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/j0$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/b<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic b:Lf/g/j/q/j0$b;


# direct methods
.method public constructor <init>(Lf/g/j/q/j0$b;Lf/g/j/q/j0$a;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/j0$b$a;->b:Lf/g/j/q/j0$b;

    invoke-direct {p0}, Lf/g/j/q/b;-><init>()V

    return-void
.end method


# virtual methods
.method public g()V
    .locals 3

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/q/j0$b$a;->b:Lf/g/j/q/j0$b;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, v0, Lf/g/j/q/j0$b;->g:Lf/g/j/q/j0$b$a;

    if-eq v1, p0, :cond_0

    monitor-exit v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, Lf/g/j/q/j0$b;->g:Lf/g/j/q/j0$b$a;

    iput-object v1, v0, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;

    iget-object v2, v0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    invoke-virtual {v0, v2}, Lf/g/j/q/j0$b;->b(Ljava/io/Closeable;)V

    iput-object v1, v0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    sget-object v1, Lf/g/d/l/a;->f:Lf/g/d/l/a;

    invoke-virtual {v0, v1}, Lf/g/j/q/j0$b;->i(Lf/g/d/l/a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw v0
.end method

.method public h(Ljava/lang/Throwable;)V
    .locals 1

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/q/j0$b$a;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v0, p0, p1}, Lf/g/j/q/j0$b;->f(Lf/g/j/q/j0$b$a;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 1

    check-cast p1, Ljava/io/Closeable;

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/q/j0$b$a;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v0, p0, p1, p2}, Lf/g/j/q/j0$b;->g(Lf/g/j/q/j0$b$a;Ljava/io/Closeable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public j(F)V
    .locals 1

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/q/j0$b$a;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v0, p0, p1}, Lf/g/j/q/j0$b;->h(Lf/g/j/q/j0$b$a;F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
