.class public Lf/g/j/q/d1;
.super Ljava/lang/Object;
.source "ThreadHandoffProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/q/e1;


# direct methods
.method public constructor <init>(Lf/g/j/q/u0;Lf/g/j/q/e1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "TT;>;",
            "Lf/g/j/q/e1;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/q/d1;->a:Lf/g/j/q/u0;

    iput-object p2, p0, Lf/g/j/q/d1;->b:Lf/g/j/q/e1;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "TT;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v6

    new-instance v9, Lf/g/j/q/d1$a;

    const-string v5, "BackgroundThreadHandoffProducer"

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, v6

    move-object v4, p2

    move-object v7, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lf/g/j/q/d1$a;-><init>(Lf/g/j/q/d1;Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/q/l;)V

    new-instance p1, Lf/g/j/q/d1$b;

    invoke-direct {p1, p0, v9}, Lf/g/j/q/d1$b;-><init>(Lf/g/j/q/d1;Lf/g/j/q/c1;)V

    invoke-interface {p2, p1}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    iget-object p1, p0, Lf/g/j/q/d1;->b:Lf/g/j/q/e1;

    check-cast p1, Lf/g/j/q/f1;

    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object p2, p1, Lf/g/j/q/f1;->b:Ljava/util/concurrent/Executor;

    invoke-interface {p2, v9}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p2

    :try_start_3
    monitor-exit p1

    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
