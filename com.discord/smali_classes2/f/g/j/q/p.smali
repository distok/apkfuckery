.class public Lf/g/j/q/p;
.super Ljava/lang/Object;
.source "DiskCacheReadProducer.java"

# interfaces
.implements Lu/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu/c<",
        "Lf/g/j/j/e;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/q/x0;

.field public final synthetic b:Lf/g/j/q/v0;

.field public final synthetic c:Lf/g/j/q/l;

.field public final synthetic d:Lf/g/j/q/r;


# direct methods
.method public constructor <init>(Lf/g/j/q/r;Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/q/l;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/p;->d:Lf/g/j/q/r;

    iput-object p2, p0, Lf/g/j/q/p;->a:Lf/g/j/q/x0;

    iput-object p3, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    iput-object p4, p0, Lf/g/j/q/p;->c:Lf/g/j/q/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lu/g;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p1, Lu/g;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p1, Lu/g;->c:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lu/g;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lu/g;->d()Ljava/lang/Exception;

    move-result-object v1

    instance-of v1, v1, Ljava/util/concurrent/CancellationException;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    const/4 v3, 0x0

    if-eqz v1, :cond_2

    iget-object p1, p0, Lf/g/j/q/p;->a:Lf/g/j/q/x0;

    iget-object v0, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    const-string v1, "DiskCacheProducer"

    invoke-interface {p1, v0, v1, v3}, Lf/g/j/q/x0;->d(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object p1, p0, Lf/g/j/q/p;->c:Lf/g/j/q/l;

    invoke-interface {p1}, Lf/g/j/q/l;->d()V

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lu/g;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lf/g/j/q/p;->a:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    const-string v2, "DiskCacheProducer"

    invoke-virtual {p1}, Lu/g;->d()Ljava/lang/Exception;

    move-result-object p1

    invoke-interface {v0, v1, v2, p1, v3}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    iget-object p1, p0, Lf/g/j/q/p;->d:Lf/g/j/q/r;

    iget-object p1, p1, Lf/g/j/q/r;->d:Lf/g/j/q/u0;

    iget-object v0, p0, Lf/g/j/q/p;->c:Lf/g/j/q/l;

    iget-object v1, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    invoke-interface {p1, v0, v1}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    goto :goto_2

    :cond_3
    iget-object v1, p1, Lu/g;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object p1, p1, Lu/g;->d:Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    check-cast p1, Lf/g/j/j/e;

    if-eqz p1, :cond_4

    iget-object v1, p0, Lf/g/j/q/p;->a:Lf/g/j/q/x0;

    iget-object v2, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    const-string v4, "DiskCacheProducer"

    invoke-virtual {p1}, Lf/g/j/j/e;->f()I

    move-result v5

    invoke-static {v1, v2, v0, v5}, Lf/g/j/q/r;->c(Lf/g/j/q/x0;Lf/g/j/q/v0;ZI)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v1, v2, v4, v5}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v1, p0, Lf/g/j/q/p;->a:Lf/g/j/q/x0;

    iget-object v2, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    const-string v4, "DiskCacheProducer"

    invoke-interface {v1, v2, v4, v0}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object v1, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    const-string v2, "disk"

    invoke-interface {v1, v2}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    iget-object v1, p0, Lf/g/j/q/p;->c:Lf/g/j/q/l;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, Lf/g/j/q/l;->a(F)V

    iget-object v1, p0, Lf/g/j/q/p;->c:Lf/g/j/q/l;

    invoke-interface {v1, p1, v0}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    goto :goto_2

    :cond_4
    iget-object p1, p0, Lf/g/j/q/p;->a:Lf/g/j/q/x0;

    iget-object v0, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    const-string v1, "DiskCacheProducer"

    invoke-static {p1, v0, v2, v2}, Lf/g/j/q/r;->c(Lf/g/j/q/x0;Lf/g/j/q/v0;ZI)Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object p1, p0, Lf/g/j/q/p;->d:Lf/g/j/q/r;

    iget-object p1, p1, Lf/g/j/q/r;->d:Lf/g/j/q/u0;

    iget-object v0, p0, Lf/g/j/q/p;->c:Lf/g/j/q/l;

    iget-object v1, p0, Lf/g/j/q/p;->b:Lf/g/j/q/v0;

    invoke-interface {p1, v0, v1}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :goto_2
    return-object v3

    :catchall_0
    move-exception p1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method
