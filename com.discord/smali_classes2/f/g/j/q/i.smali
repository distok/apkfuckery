.class public Lf/g/j/q/i;
.super Ljava/lang/Object;
.source "BitmapPrepareProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:I

.field public final d:Z


# direct methods
.method public constructor <init>(Lf/g/j/q/u0;IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;IIZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-gt p2, p3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/q/i;->a:Lf/g/j/q/u0;

    iput p2, p0, Lf/g/j/q/i;->b:I

    iput p3, p0, Lf/g/j/q/i;->c:I

    iput-boolean p4, p0, Lf/g/j/q/i;->d:Z

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lf/g/j/q/i;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/i;->a:Lf/g/j/q/u0;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/j/q/i;->a:Lf/g/j/q/u0;

    new-instance v1, Lf/g/j/q/i$a;

    iget v2, p0, Lf/g/j/q/i;->b:I

    iget v3, p0, Lf/g/j/q/i;->c:I

    invoke-direct {v1, p1, v2, v3}, Lf/g/j/q/i$a;-><init>(Lf/g/j/q/l;II)V

    invoke-interface {v0, v1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :goto_0
    return-void
.end method
