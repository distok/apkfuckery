.class public abstract Lf/g/j/q/c1;
.super Ljava/lang/Object;
.source "StatefulProducerRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lf/g/j/q/c1<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final e:Lf/g/j/q/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/l<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final f:Lf/g/j/q/x0;

.field public final g:Ljava/lang/String;

.field public final h:Lf/g/j/q/v0;


# direct methods
.method public constructor <init>(Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "TT;>;",
            "Lf/g/j/q/x0;",
            "Lf/g/j/q/v0;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lf/g/j/q/c1;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lf/g/j/q/c1;->e:Lf/g/j/q/l;

    iput-object p2, p0, Lf/g/j/q/c1;->f:Lf/g/j/q/x0;

    iput-object p4, p0, Lf/g/j/q/c1;->g:Ljava/lang/String;

    iput-object p3, p0, Lf/g/j/q/c1;->h:Lf/g/j/q/v0;

    invoke-interface {p2, p3, p4}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lf/g/j/q/c1;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/g/j/q/c1;->e()V

    :cond_0
    return-void
.end method

.method public abstract b(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public abstract d()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public e()V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/c1;->f:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/c1;->h:Lf/g/j/q/v0;

    iget-object v2, p0, Lf/g/j/q/c1;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v3

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lf/g/j/q/x0;->d(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/g/j/q/c1;->e:Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    return-void
.end method

.method public f(Ljava/lang/Exception;)V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/c1;->f:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/c1;->h:Lf/g/j/q/v0;

    iget-object v2, p0, Lf/g/j/q/c1;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v3

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, p1, v3}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    iget-object v0, p0, Lf/g/j/q/c1;->e:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public g(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/q/c1;->f:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/c1;->h:Lf/g/j/q/v0;

    iget-object v2, p0, Lf/g/j/q/c1;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, p1}, Lf/g/j/q/c1;->c(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0, v1, v2, v3}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/g/j/q/c1;->e:Lf/g/j/q/l;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    return-void
.end method

.method public final run()V
    .locals 3

    iget-object v0, p0, Lf/g/j/q/c1;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lf/g/j/q/c1;->d()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lf/g/j/q/c1;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    :try_start_1
    invoke-virtual {p0, v0}, Lf/g/j/q/c1;->g(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0, v0}, Lf/g/j/q/c1;->b(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {p0, v0}, Lf/g/j/q/c1;->b(Ljava/lang/Object;)V

    throw v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/g/j/q/c1;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    invoke-virtual {p0, v0}, Lf/g/j/q/c1;->f(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method
