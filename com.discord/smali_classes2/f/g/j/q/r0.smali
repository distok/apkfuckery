.class public Lf/g/j/q/r0;
.super Ljava/lang/Object;
.source "PostprocessorProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/r0$c;,
        Lf/g/j/q/r0$d;,
        Lf/g/j/q/r0$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field public final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lf/g/j/q/u0;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Ljava/util/concurrent/Executor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/q/r0;->a:Lf/g/j/q/u0;

    iput-object p2, p0, Lf/g/j/q/r0;->b:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lf/g/j/q/r0;->c:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v3

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v6, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    new-instance v9, Lf/g/j/q/r0$b;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v4, v6

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lf/g/j/q/r0$b;-><init>(Lf/g/j/q/r0;Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/r/b;Lf/g/j/q/v0;)V

    instance-of p1, v6, Lf/g/j/r/c;

    if-eqz p1, :cond_0

    new-instance p1, Lf/g/j/q/r0$c;

    move-object v10, v6

    check-cast v10, Lf/g/j/r/c;

    const/4 v12, 0x0

    move-object v7, p1

    move-object v8, p0

    move-object v11, p2

    invoke-direct/range {v7 .. v12}, Lf/g/j/q/r0$c;-><init>(Lf/g/j/q/r0;Lf/g/j/q/r0$b;Lf/g/j/r/c;Lf/g/j/q/v0;Lf/g/j/q/r0$a;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lf/g/j/q/r0$d;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v9, v0}, Lf/g/j/q/r0$d;-><init>(Lf/g/j/q/r0;Lf/g/j/q/r0$b;Lf/g/j/q/r0$a;)V

    :goto_0
    iget-object v0, p0, Lf/g/j/q/r0;->a:Lf/g/j/q/u0;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void
.end method
