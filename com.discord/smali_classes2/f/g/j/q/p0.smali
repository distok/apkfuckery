.class public Lf/g/j/q/p0;
.super Ljava/lang/Object;
.source "PartialDiskCacheProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/p0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/g;

.field public final b:Lf/g/j/c/j;

.field public final c:Lf/g/d/g/g;

.field public final d:Lf/g/d/g/a;

.field public final e:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/d/g/g;Lf/g/d/g/a;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lf/g/d/g/g;",
            "Lf/g/d/g/a;",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/p0;->a:Lf/g/j/c/g;

    iput-object p2, p0, Lf/g/j/q/p0;->b:Lf/g/j/c/j;

    iput-object p3, p0, Lf/g/j/q/p0;->c:Lf/g/d/g/g;

    iput-object p4, p0, Lf/g/j/q/p0;->d:Lf/g/d/g/a;

    iput-object p5, p0, Lf/g/j/q/p0;->e:Lf/g/j/q/u0;

    return-void
.end method

.method public static c(Lf/g/j/q/p0;Lf/g/j/q/l;Lf/g/j/q/v0;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V
    .locals 9

    new-instance v8, Lf/g/j/q/p0$a;

    iget-object v2, p0, Lf/g/j/q/p0;->a:Lf/g/j/c/g;

    iget-object v4, p0, Lf/g/j/q/p0;->c:Lf/g/d/g/g;

    iget-object v5, p0, Lf/g/j/q/p0;->d:Lf/g/d/g/a;

    const/4 v7, 0x0

    move-object v0, v8

    move-object v1, p1

    move-object v3, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lf/g/j/q/p0$a;-><init>(Lf/g/j/q/l;Lf/g/j/c/g;Lcom/facebook/cache/common/CacheKey;Lf/g/d/g/g;Lf/g/d/g/a;Lf/g/j/j/e;Lf/g/j/q/n0;)V

    iget-object p0, p0, Lf/g/j/q/p0;->e:Lf/g/j/q/u0;

    invoke-interface {p0, v8, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void
.end method

.method public static d(Lf/g/j/q/x0;Lf/g/j/q/v0;ZI)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/x0;",
            "Lf/g/j/q/v0;",
            "ZI)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "PartialDiskCacheProducer"

    invoke-interface {p0, p1, v0}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string p0, "cached_value_found"

    if-eqz p2, :cond_1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "encodedImageSize"

    invoke-static {p0, p1, p3, p2}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-boolean v1, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    if-nez v1, :cond_0

    iget-object v0, p0, Lf/g/j/q/p0;->e:Lf/g/j/q/u0;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void

    :cond_0
    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v1

    const-string v2, "PartialDiskCacheProducer"

    invoke-interface {v1, p2, v2}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "fresco_partial"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/q/p0;->b:Lf/g/j/c/j;

    invoke-interface {p2}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    check-cast v1, Lf/g/j/c/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lf/g/b/a/f;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Lf/g/b/a/f;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iget-object v1, p0, Lf/g/j/q/p0;->a:Lf/g/j/c/g;

    invoke-virtual {v1, v7, v0}, Lf/g/j/c/g;->e(Lcom/facebook/cache/common/CacheKey;Ljava/util/concurrent/atomic/AtomicBoolean;)Lu/g;

    move-result-object v1

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v4

    new-instance v8, Lf/g/j/q/n0;

    move-object v2, v8

    move-object v3, p0

    move-object v5, p2

    move-object v6, p1

    invoke-direct/range {v2 .. v7}, Lf/g/j/q/n0;-><init>(Lf/g/j/q/p0;Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;)V

    invoke-virtual {v1, v8}, Lu/g;->b(Lu/c;)Lu/g;

    new-instance p1, Lf/g/j/q/o0;

    invoke-direct {p1, p0, v0}, Lf/g/j/q/o0;-><init>(Lf/g/j/q/p0;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-interface {p2, p1}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    return-void
.end method
