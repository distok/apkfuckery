.class public Lf/g/j/q/x;
.super Ljava/lang/Object;
.source "HttpUrlConnectionNetworkFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/g/j/q/z$a;

.field public final synthetic e:Lf/g/j/q/m0$a;

.field public final synthetic f:Lf/g/j/q/z;


# direct methods
.method public constructor <init>(Lf/g/j/q/z;Lf/g/j/q/z$a;Lf/g/j/q/m0$a;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/x;->f:Lf/g/j/q/z;

    iput-object p2, p0, Lf/g/j/q/x;->d:Lf/g/j/q/z$a;

    iput-object p3, p0, Lf/g/j/q/x;->e:Lf/g/j/q/m0$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lf/g/j/q/x;->f:Lf/g/j/q/z;

    iget-object v1, p0, Lf/g/j/q/x;->d:Lf/g/j/q/z$a;

    iget-object v2, p0, Lf/g/j/q/x;->e:Lf/g/j/q/m0$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, v1, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    invoke-interface {v4}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    const/4 v5, 0x5

    invoke-virtual {v0, v4, v5}, Lf/g/j/q/z;->a(Landroid/net/Uri;I)Ljava/net/HttpURLConnection;

    move-result-object v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v0, Lf/g/j/q/z;->c:Lf/g/d/k/b;

    invoke-interface {v0}, Lf/g/d/k/b;->now()J

    move-result-wide v5

    iput-wide v5, v1, Lf/g/j/q/z$a;->e:J

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, -0x1

    move-object v1, v2

    check-cast v1, Lf/g/j/q/l0$a;

    :try_start_2
    invoke-virtual {v1, v3, v0}, Lf/g/j/q/l0$a;->b(Ljava/io/InputStream;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    if-eqz v3, :cond_1

    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    nop

    :cond_1
    :goto_0
    if-eqz v4, :cond_3

    goto :goto_3

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v4, v3

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v4, v3

    :goto_1
    check-cast v2, Lf/g/j/q/l0$a;

    :try_start_4
    invoke-virtual {v2, v0}, Lf/g/j/q/l0$a;->a(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v3, :cond_2

    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    nop

    :cond_2
    :goto_2
    if-eqz v4, :cond_3

    :goto_3
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    return-void

    :goto_4
    if-eqz v3, :cond_4

    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_5

    :catch_4
    nop

    :cond_4
    :goto_5
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    throw v0
.end method
