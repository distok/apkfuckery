.class public abstract Lf/g/j/q/j0;
.super Ljava/lang/Object;
.source "MultiplexProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/j0$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "T::",
        "Ljava/io/Closeable;",
        ">",
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "TK;",
            "Lf/g/j/q/j0<",
            "TK;TT;>.b;>;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lf/g/j/q/u0;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "TT;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/j0;->b:Lf/g/j/q/u0;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lf/g/j/q/j0;->a:Ljava/util/Map;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/g/j/q/j0;->c:Z

    iput-object p2, p0, Lf/g/j/q/j0;->d:Ljava/lang/String;

    iput-object p3, p0, Lf/g/j/q/j0;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lf/g/j/q/u0;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "TT;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/j0;->b:Lf/g/j/q/u0;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lf/g/j/q/j0;->a:Ljava/util/Map;

    iput-boolean p4, p0, Lf/g/j/q/j0;->c:Z

    iput-object p2, p0, Lf/g/j/q/j0;->d:Ljava/lang/String;

    iput-object p3, p0, Lf/g/j/q/j0;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "TT;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/q/j0;->d:Ljava/lang/String;

    invoke-interface {v0, p2, v1}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lf/g/j/q/j0;->d(Lf/g/j/q/v0;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    const/4 v1, 0x0

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v2, p0, Lf/g/j/q/j0;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/g/j/q/j0$b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    monitor-exit p0

    if-nez v2, :cond_1

    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    new-instance v2, Lf/g/j/q/j0$b;

    invoke-direct {v2, p0, v0}, Lf/g/j/q/j0$b;-><init>(Lf/g/j/q/j0;Ljava/lang/Object;)V

    iget-object v1, p0, Lf/g/j/q/j0;->a:Ljava/util/Map;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    monitor-exit p0

    const/4 v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    invoke-virtual {v2, p1, p2}, Lf/g/j/q/j0$b;->a(Lf/g/j/q/l;Lf/g/j/q/v0;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_2

    invoke-interface {p2}, Lf/g/j/q/v0;->k()Z

    move-result p1

    invoke-static {p1}, Lf/g/d/l/a;->f(Z)Lf/g/d/l/a;

    move-result-object p1

    invoke-virtual {v2, p1}, Lf/g/j/q/j0$b;->i(Lf/g/d/l/a;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :cond_2
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_1
    move-exception p1

    :try_start_7
    monitor-exit p0

    throw p1

    :catchall_2
    move-exception p1

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catchall_3
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public abstract c(Ljava/io/Closeable;)Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation
.end method

.method public abstract d(Lf/g/j/q/v0;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/v0;",
            ")TK;"
        }
    .end annotation
.end method

.method public declared-synchronized e(Ljava/lang/Object;Lf/g/j/q/j0$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lf/g/j/q/j0<",
            "TK;TT;>.b;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p2, :cond_0

    iget-object p2, p0, Lf/g/j/q/j0;->a:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
