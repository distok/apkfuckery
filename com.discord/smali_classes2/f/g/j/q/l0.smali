.class public Lf/g/j/q/l0;
.super Ljava/lang/Object;
.source "NetworkFetchProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/d/g/g;

.field public final b:Lf/g/d/g/a;

.field public final c:Lf/g/j/q/m0;


# direct methods
.method public constructor <init>(Lf/g/d/g/g;Lf/g/d/g/a;Lf/g/j/q/m0;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/l0;->a:Lf/g/d/g/g;

    iput-object p2, p0, Lf/g/j/q/l0;->b:Lf/g/d/g/a;

    iput-object p3, p0, Lf/g/j/q/l0;->c:Lf/g/j/q/m0;

    return-void
.end method

.method public static e(Lf/g/d/g/i;ILf/g/j/d/a;Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/d/g/i;",
            "I",
            "Lf/g/j/d/a;",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    check-cast p0, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;

    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;->b()Lf/g/j/m/t;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p0

    const/4 p2, 0x0

    :try_start_0
    new-instance v0, Lf/g/j/j/e;

    invoke-direct {v0, p0}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iput-object p2, v0, Lf/g/j/j/e;->m:Lf/g/j/d/a;

    invoke-virtual {v0}, Lf/g/j/j/e;->n()V

    sget-object p2, Lf/g/j/j/f;->e:Lf/g/j/j/f;

    invoke-interface {p4, p2}, Lf/g/j/q/v0;->h(Lf/g/j/j/f;)V

    invoke-interface {p3, v0, p1}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Lf/g/j/j/e;->close()V

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    move-object p2, v0

    goto :goto_0

    :catchall_1
    move-exception p1

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lf/g/j/j/e;->close()V

    :cond_1
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    throw p1
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    const-string v1, "NetworkFetchProducer"

    invoke-interface {v0, p2, v1}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    iget-object v0, p0, Lf/g/j/q/l0;->c:Lf/g/j/q/m0;

    check-cast v0, Lf/g/j/q/z;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/g/j/q/z$a;

    invoke-direct {v0, p1, p2}, Lf/g/j/q/z$a;-><init>(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    iget-object p1, p0, Lf/g/j/q/l0;->c:Lf/g/j/q/m0;

    new-instance p2, Lf/g/j/q/l0$a;

    invoke-direct {p2, p0, v0}, Lf/g/j/q/l0$a;-><init>(Lf/g/j/q/l0;Lf/g/j/q/w;)V

    check-cast p1, Lf/g/j/q/z;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lf/g/j/q/z;->c:Lf/g/d/k/b;

    invoke-interface {v1}, Lf/g/d/k/b;->now()J

    move-result-wide v1

    iput-wide v1, v0, Lf/g/j/q/z$a;->d:J

    iget-object v1, p1, Lf/g/j/q/z;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lf/g/j/q/x;

    invoke-direct {v2, p1, v0, p2}, Lf/g/j/q/x;-><init>(Lf/g/j/q/z;Lf/g/j/q/z$a;Lf/g/j/q/m0$a;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    iget-object v0, v0, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    new-instance v2, Lf/g/j/q/y;

    invoke-direct {v2, p1, v1, p2}, Lf/g/j/q/y;-><init>(Lf/g/j/q/z;Ljava/util/concurrent/Future;Lf/g/j/q/m0$a;)V

    invoke-interface {v0, v2}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    return-void
.end method

.method public c(Lf/g/d/g/i;Lf/g/j/q/w;)V
    .locals 9

    move-object v0, p1

    check-cast v0, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;

    iget v0, v0, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;->f:I

    invoke-virtual {p2}, Lf/g/j/q/w;->a()Lf/g/j/q/x0;

    move-result-object v1

    iget-object v2, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const-string v3, "NetworkFetchProducer"

    invoke-interface {v1, v2, v3}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v4, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/g/j/q/l0;->c:Lf/g/j/q/m0;

    check-cast v1, Lf/g/j/q/z;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, p2

    check-cast v1, Lf/g/j/q/z$a;

    new-instance v4, Ljava/util/HashMap;

    const/4 v5, 0x4

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    iget-wide v5, v1, Lf/g/j/q/z$a;->e:J

    iget-wide v7, v1, Lf/g/j/q/z$a;->d:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    const-string v6, "queue_time"

    invoke-virtual {v4, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v5, v1, Lf/g/j/q/z$a;->f:J

    iget-wide v7, v1, Lf/g/j/q/z$a;->e:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    const-string v6, "fetch_time"

    invoke-virtual {v4, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v5, v1, Lf/g/j/q/z$a;->f:J

    iget-wide v7, v1, Lf/g/j/q/z$a;->d:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    const-string v5, "total_time"

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "image_size"

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-virtual {p2}, Lf/g/j/q/w;->a()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v1, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    invoke-interface {v0, v1, v3, v4}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v1, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v3, v4}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object v0, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const-string v1, "network"

    invoke-interface {v0, v1}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    iget-object v0, p2, Lf/g/j/q/w;->a:Lf/g/j/q/l;

    iget-object p2, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    invoke-static {p1, v4, v2, v0, p2}, Lf/g/j/q/l0;->e(Lf/g/d/g/i;ILf/g/j/d/a;Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void
.end method

.method public d(Lf/g/d/g/i;Lf/g/j/q/w;)V
    .locals 8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    invoke-interface {v2}, Lf/g/j/q/v0;->p()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lf/g/j/q/l0;->c:Lf/g/j/q/m0;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_1

    iget-wide v4, p2, Lf/g/j/q/w;->c:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x64

    cmp-long v2, v4, v6

    if-ltz v2, :cond_1

    iput-wide v0, p2, Lf/g/j/q/w;->c:J

    invoke-virtual {p2}, Lf/g/j/q/w;->a()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v1, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const-string v2, "NetworkFetchProducer"

    const-string v4, "intermediate_result"

    invoke-interface {v0, v1, v2, v4}, Lf/g/j/q/x0;->a(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iget-object v1, p2, Lf/g/j/q/w;->a:Lf/g/j/q/l;

    iget-object p2, p2, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    invoke-static {p1, v3, v0, v1, p2}, Lf/g/j/q/l0;->e(Lf/g/d/g/i;ILf/g/j/d/a;Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :cond_1
    return-void
.end method
