.class public Lf/g/j/q/j;
.super Ljava/lang/Object;
.source "BitmapProbeProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/c/g;

.field public final c:Lf/g/j/c/g;

.field public final d:Lf/g/j/c/j;

.field public final e:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public final f:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/c/e;Lf/g/j/c/e;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;",
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/j;->a:Lf/g/j/c/t;

    iput-object p2, p0, Lf/g/j/q/j;->b:Lf/g/j/c/g;

    iput-object p3, p0, Lf/g/j/q/j;->c:Lf/g/j/c/g;

    iput-object p4, p0, Lf/g/j/q/j;->d:Lf/g/j/c/j;

    iput-object p5, p0, Lf/g/j/q/j;->f:Lf/g/j/c/e;

    iput-object p6, p0, Lf/g/j/q/j;->g:Lf/g/j/c/e;

    iput-object p7, p0, Lf/g/j/q/j;->e:Lf/g/j/q/u0;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    const-string v0, "BitmapProbeProducer"

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v1

    invoke-interface {v1, p2, v0}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    new-instance v11, Lf/g/j/q/j$a;

    iget-object v5, p0, Lf/g/j/q/j;->a:Lf/g/j/c/t;

    iget-object v6, p0, Lf/g/j/q/j;->b:Lf/g/j/c/g;

    iget-object v7, p0, Lf/g/j/q/j;->c:Lf/g/j/c/g;

    iget-object v8, p0, Lf/g/j/q/j;->d:Lf/g/j/c/j;

    iget-object v9, p0, Lf/g/j/q/j;->f:Lf/g/j/c/e;

    iget-object v10, p0, Lf/g/j/q/j;->g:Lf/g/j/c/e;

    move-object v2, v11

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v10}, Lf/g/j/q/j$a;-><init>(Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/c/e;Lf/g/j/c/e;)V

    const/4 p1, 0x0

    invoke-interface {v1, p2, v0, p1}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object p1, p0, Lf/g/j/q/j;->e:Lf/g/j/q/u0;

    invoke-interface {p1, v11, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
