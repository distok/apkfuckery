.class public Lf/g/j/q/h1;
.super Ljava/lang/Object;
.source "ThumbnailBranchProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/h1$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:[Lf/g/j/q/i1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lf/g/j/q/i1<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lf/g/j/q/i1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lf/g/j/q/i1<",
            "Lf/g/j/j/e;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/q/h1;->a:[Lf/g/j/q/i1;

    array-length p1, p1

    if-gtz p1, :cond_1

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ltz p1, :cond_0

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "index"

    aput-object v5, v3, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v2

    const-string p1, "%s (%s) must be less than size (%s)"

    invoke-static {p1, v3}, Ls/a/b/b/a;->t(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "negative size: "

    invoke-static {v1, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-interface {p1, v2, v1}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lf/g/j/q/h1;->c(ILf/g/j/q/l;Lf/g/j/q/v0;)Z

    move-result p2

    if-nez p2, :cond_1

    invoke-interface {p1, v2, v1}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final c(ILf/g/j/q/l;Lf/g/j/q/v0;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")Z"
        }
    .end annotation

    invoke-interface {p3}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    :goto_0
    iget-object v1, p0, Lf/g/j/q/h1;->a:[Lf/g/j/q/i1;

    array-length v2, v1

    const/4 v3, -0x1

    if-ge p1, v2, :cond_1

    aget-object v1, v1, p1

    invoke-interface {v1, v0}, Lf/g/j/q/i1;->a(Lf/g/j/d/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    :goto_1
    if-ne p1, v3, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    iget-object v0, p0, Lf/g/j/q/h1;->a:[Lf/g/j/q/i1;

    aget-object v0, v0, p1

    new-instance v1, Lf/g/j/q/h1$a;

    invoke-direct {v1, p0, p2, p3, p1}, Lf/g/j/q/h1$a;-><init>(Lf/g/j/q/h1;Lf/g/j/q/l;Lf/g/j/q/v0;I)V

    invoke-interface {v0, v1, p3}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    const/4 p1, 0x1

    return p1
.end method
