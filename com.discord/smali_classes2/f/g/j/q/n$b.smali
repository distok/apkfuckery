.class public Lf/g/j/q/n$b;
.super Lf/g/j/q/n$c;
.source "DecodeProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final i:Lf/g/j/h/d;

.field public final j:Lf/g/j/h/c;

.field public k:I


# direct methods
.method public constructor <init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/h/d;Lf/g/j/h/c;ZI)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            "Lf/g/j/h/d;",
            "Lf/g/j/h/c;",
            "ZI)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p6

    move v5, p7

    invoke-direct/range {v0 .. v5}, Lf/g/j/q/n$c;-><init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;ZI)V

    iput-object p4, p0, Lf/g/j/q/n$b;->i:Lf/g/j/h/d;

    invoke-static {p5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p5, p0, Lf/g/j/q/n$b;->j:Lf/g/j/h/c;

    const/4 p1, 0x0

    iput p1, p0, Lf/g/j/q/n$b;->k:I

    return-void
.end method


# virtual methods
.method public o(Lf/g/j/j/e;)I
    .locals 0

    iget-object p1, p0, Lf/g/j/q/n$b;->i:Lf/g/j/h/d;

    iget p1, p1, Lf/g/j/h/d;->f:I

    return p1
.end method

.method public p()Lf/g/j/j/i;
    .locals 2

    iget-object v0, p0, Lf/g/j/q/n$b;->j:Lf/g/j/h/c;

    iget-object v1, p0, Lf/g/j/q/n$b;->i:Lf/g/j/h/d;

    iget v1, v1, Lf/g/j/h/d;->e:I

    invoke-interface {v0, v1}, Lf/g/j/h/c;->a(I)Lf/g/j/j/i;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized w(Lf/g/j/j/e;I)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/n$c;->g:Lf/g/j/q/b0;

    invoke-virtual {v0, p1, p2}, Lf/g/j/q/b0;->f(Lf/g/j/j/e;I)Z

    move-result v0

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x8

    invoke-static {p2, v1}, Lf/g/j/q/b;->m(II)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x4

    invoke-static {p2, v1}, Lf/g/j/q/b;->m(II)Z

    move-result p2

    if-nez p2, :cond_4

    invoke-static {p1}, Lf/g/j/j/e;->m(Lf/g/j/j/e;)Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object p2, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v1, Lf/g/i/b;->a:Lf/g/i/c;

    if-ne p2, v1, :cond_4

    iget-object p2, p0, Lf/g/j/q/n$b;->i:Lf/g/j/h/d;

    invoke-virtual {p2, p1}, Lf/g/j/h/d;->b(Lf/g/j/j/e;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p2, 0x0

    if-nez p1, :cond_1

    monitor-exit p0

    return p2

    :cond_1
    :try_start_1
    iget-object p1, p0, Lf/g/j/q/n$b;->i:Lf/g/j/h/d;

    iget p1, p1, Lf/g/j/h/d;->e:I

    iget v1, p0, Lf/g/j/q/n$b;->k:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt p1, v1, :cond_2

    monitor-exit p0

    return p2

    :cond_2
    :try_start_2
    iget-object v2, p0, Lf/g/j/q/n$b;->j:Lf/g/j/h/c;

    invoke-interface {v2, v1}, Lf/g/j/h/c;->b(I)I

    move-result v1

    if-ge p1, v1, :cond_3

    iget-object v1, p0, Lf/g/j/q/n$b;->i:Lf/g/j/h/d;

    iget-boolean v1, v1, Lf/g/j/h/d;->g:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v1, :cond_3

    monitor-exit p0

    return p2

    :cond_3
    :try_start_3
    iput p1, p0, Lf/g/j/q/n$b;->k:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
