.class public Lf/g/j/q/h$a;
.super Lf/g/j/q/o;
.source "BitmapMemoryCacheProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/h;->e(Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;Z)Lf/g/j/q/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic c:Lcom/facebook/cache/common/CacheKey;

.field public final synthetic d:Z

.field public final synthetic e:Lf/g/j/q/h;


# direct methods
.method public constructor <init>(Lf/g/j/q/h;Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;Z)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/h$a;->e:Lf/g/j/q/h;

    iput-object p3, p0, Lf/g/j/q/h$a;->c:Lcom/facebook/cache/common/CacheKey;

    iput-boolean p4, p0, Lf/g/j/q/h$a;->d:Z

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 6

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez p1, :cond_1

    if-eqz v0, :cond_0

    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p1, v1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :cond_0
    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    goto/16 :goto_4

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/g/j/j/c;

    invoke-virtual {v2}, Lf/g/j/j/c;->d()Z

    move-result v2

    if-nez v2, :cond_a

    const/16 v2, 0x8

    invoke-static {p2, v2}, Lf/g/j/q/b;->m(II)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_3

    :cond_2
    if-nez v0, :cond_5

    iget-object v2, p0, Lf/g/j/q/h$a;->e:Lf/g/j/q/h;

    iget-object v2, v2, Lf/g/j/q/h;->a:Lf/g/j/c/t;

    iget-object v3, p0, Lf/g/j/q/h$a;->c:Lcom/facebook/cache/common/CacheKey;

    invoke-interface {v2, v3}, Lf/g/j/c/t;->get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-eqz v2, :cond_5

    :try_start_2
    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/g/j/j/c;

    invoke-virtual {v3}, Lf/g/j/j/c;->b()Lf/g/j/j/i;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/g/j/j/c;

    invoke-virtual {v4}, Lf/g/j/j/c;->b()Lf/g/j/j/i;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lf/g/j/j/h;

    iget-boolean v5, v5, Lf/g/j/j/h;->c:Z

    if-nez v5, :cond_4

    check-cast v4, Lf/g/j/j/h;

    iget v4, v4, Lf/g/j/j/h;->a:I

    check-cast v3, Lf/g/j/j/h;

    iget v3, v3, Lf/g/j/j/h;->a:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-lt v4, v3, :cond_3

    goto :goto_1

    :cond_3
    :try_start_3
    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :cond_4
    :goto_1
    :try_start_4
    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p1, v2, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p1

    :cond_5
    :goto_2
    iget-boolean v2, p0, Lf/g/j/q/h$a;->d:Z

    if-eqz v2, :cond_6

    iget-object v1, p0, Lf/g/j/q/h$a;->e:Lf/g/j/q/h;

    iget-object v1, v1, Lf/g/j/q/h;->a:Lf/g/j/c/t;

    iget-object v2, p0, Lf/g/j/q/h$a;->c:Lcom/facebook/cache/common/CacheKey;

    invoke-interface {v1, v2, p1}, Lf/g/j/c/t;->a(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_6
    if-eqz v0, :cond_7

    :try_start_6
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v2}, Lf/g/j/q/l;->a(F)V

    :cond_7
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    if-eqz v1, :cond_8

    move-object p1, v1

    :cond_8
    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_0

    :try_start_7
    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    goto/16 :goto_0

    :catchall_1
    move-exception p1

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_9
    throw p1

    :cond_a
    :goto_3
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto/16 :goto_0

    :goto_4
    return-void

    :catchall_2
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
