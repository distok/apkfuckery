.class public Lf/g/j/q/i$a;
.super Lf/g/j/q/o;
.source "BitmapPrepareProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(Lf/g/j/q/l;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;II)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput p2, p0, Lf/g/j/q/i$a;->c:I

    iput p3, p0, Lf/g/j/q/i$a;->d:I

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->o()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/j/c;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lf/g/j/j/c;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lf/g/j/j/d;

    if-eqz v1, :cond_5

    check-cast v0, Lf/g/j/j/d;

    iget-object v0, v0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int v2, v2, v1

    iget v1, p0, Lf/g/j/q/i$a;->c:I

    if-ge v2, v1, :cond_3

    goto :goto_0

    :cond_3
    iget v1, p0, Lf/g/j/q/i$a;->d:I

    if-le v2, v1, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->prepareToDraw()V

    :cond_5
    :goto_0
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    return-void
.end method
