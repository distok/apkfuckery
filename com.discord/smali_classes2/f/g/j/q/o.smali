.class public abstract Lf/g/j/q/o;
.super Lf/g/j/q/b;
.source "DelegatingConsumer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lf/g/j/q/b<",
        "TI;>;"
    }
.end annotation


# instance fields
.field public final b:Lf/g/j/q/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/l<",
            "TO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/q/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "TO;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/g/j/q/b;-><init>()V

    iput-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    return-void
.end method


# virtual methods
.method public g()V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    return-void
.end method

.method public h(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public j(F)V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->a(F)V

    return-void
.end method
