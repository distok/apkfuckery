.class public Lf/g/j/q/f0$a;
.super Lf/g/j/q/c1;
.source "LocalFetchProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/f0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/c1<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic i:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public final synthetic j:Lf/g/j/q/x0;

.field public final synthetic k:Lf/g/j/q/v0;

.field public final synthetic l:Lf/g/j/q/f0;


# direct methods
.method public constructor <init>(Lf/g/j/q/f0;Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;Lcom/facebook/imagepipeline/request/ImageRequest;Lf/g/j/q/x0;Lf/g/j/q/v0;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/f0$a;->l:Lf/g/j/q/f0;

    iput-object p6, p0, Lf/g/j/q/f0$a;->i:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object p7, p0, Lf/g/j/q/f0$a;->j:Lf/g/j/q/x0;

    iput-object p8, p0, Lf/g/j/q/f0$a;->k:Lf/g/j/q/v0;

    invoke-direct {p0, p2, p3, p4, p5}, Lf/g/j/q/c1;-><init>(Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lf/g/j/j/e;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    :cond_0
    return-void
.end method

.method public d()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/q/f0$a;->l:Lf/g/j/q/f0;

    iget-object v1, p0, Lf/g/j/q/f0$a;->i:Lcom/facebook/imagepipeline/request/ImageRequest;

    invoke-virtual {v0, v1}, Lf/g/j/q/f0;->d(Lcom/facebook/imagepipeline/request/ImageRequest;)Lf/g/j/j/e;

    move-result-object v0

    const-string v1, "local"

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/f0$a;->j:Lf/g/j/q/x0;

    iget-object v2, p0, Lf/g/j/q/f0$a;->k:Lf/g/j/q/v0;

    iget-object v3, p0, Lf/g/j/q/f0$a;->l:Lf/g/j/q/f0;

    invoke-virtual {v3}, Lf/g/j/q/f0;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object v0, p0, Lf/g/j/q/f0$a;->k:Lf/g/j/q/v0;

    invoke-interface {v0, v1}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lf/g/j/j/e;->n()V

    iget-object v2, p0, Lf/g/j/q/f0$a;->j:Lf/g/j/q/x0;

    iget-object v3, p0, Lf/g/j/q/f0$a;->k:Lf/g/j/q/v0;

    iget-object v4, p0, Lf/g/j/q/f0$a;->l:Lf/g/j/q/f0;

    invoke-virtual {v4}, Lf/g/j/q/f0;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v2, v3, v4, v5}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object v2, p0, Lf/g/j/q/f0$a;->k:Lf/g/j/q/v0;

    invoke-interface {v2, v1}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method
