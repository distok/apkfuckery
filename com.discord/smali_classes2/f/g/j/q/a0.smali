.class public Lf/g/j/q/a0;
.super Ljava/lang/Object;
.source "InternalRequestListener.java"

# interfaces
.implements Lf/g/j/l/d;
.implements Lf/g/j/q/x0;


# instance fields
.field public final a:Lf/g/j/q/y0;

.field public final b:Lf/g/j/q/x0;

.field public final c:Lf/g/j/l/e;

.field public final d:Lf/g/j/l/d;


# direct methods
.method public constructor <init>(Lf/g/j/l/e;Lf/g/j/l/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    iput-object p2, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    iput-object p1, p0, Lf/g/j/q/a0;->c:Lf/g/j/l/e;

    iput-object p2, p0, Lf/g/j/q/a0;->d:Lf/g/j/l/d;

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lf/g/j/q/y0;->h(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2, p3}, Lf/g/j/q/x0;->a(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public b(Lf/g/j/q/v0;)V
    .locals 5

    iget-object v0, p0, Lf/g/j/q/a0;->c:Lf/g/j/l/e;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    invoke-interface {p1}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lf/g/j/q/v0;->k()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lf/g/j/l/e;->a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->d:Lf/g/j/l/d;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lf/g/j/l/d;->b(Lf/g/j/q/v0;)V

    :cond_1
    return-void
.end method

.method public c(Lf/g/j/q/v0;Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lf/g/j/q/y0;->e(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2, p3}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method public d(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lf/g/j/q/y0;->d(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2, p3}, Lf/g/j/q/x0;->d(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method public e(Lf/g/j/q/v0;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lf/g/j/q/y0;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public f(Lf/g/j/q/v0;)V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/a0;->c:Lf/g/j/l/e;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lf/g/j/q/v0;->k()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lf/g/j/l/e;->c(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->d:Lf/g/j/l/d;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lf/g/j/l/d;->f(Lf/g/j/q/v0;)V

    :cond_1
    return-void
.end method

.method public g(Lf/g/j/q/v0;Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lf/g/j/q/y0;->f(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    if-eqz v1, :cond_1

    invoke-interface {v1, p1, p2}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v0

    :cond_1
    return v0
.end method

.method public h(Lf/g/j/q/v0;Ljava/lang/Throwable;)V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/a0;->c:Lf/g/j/l/e;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lf/g/j/q/v0;->k()Z

    move-result v3

    invoke-interface {v0, v1, v2, p2, v3}, Lf/g/j/l/e;->g(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/String;Ljava/lang/Throwable;Z)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->d:Lf/g/j/l/d;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Lf/g/j/l/d;->h(Lf/g/j/q/v0;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public i(Lf/g/j/q/v0;)V
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->c:Lf/g/j/l/e;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lf/g/j/l/e;->k(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->d:Lf/g/j/l/d;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lf/g/j/l/d;->i(Lf/g/j/q/v0;)V

    :cond_1
    return-void
.end method

.method public j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lf/g/j/q/y0;->i(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2, p3}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method public k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V
    .locals 2

    iget-object v0, p0, Lf/g/j/q/a0;->a:Lf/g/j/q/y0;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p4}, Lf/g/j/q/y0;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    :cond_0
    iget-object v0, p0, Lf/g/j/q/a0;->b:Lf/g/j/q/x0;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2, p3, p4}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    :cond_1
    return-void
.end method
