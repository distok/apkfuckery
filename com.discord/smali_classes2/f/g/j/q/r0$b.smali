.class public Lf/g/j/q/r0$b;
.super Lf/g/j/q/o;
.source "PostprocessorProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/r0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/q/x0;

.field public final d:Lf/g/j/q/v0;

.field public final e:Lf/g/j/r/b;

.field public f:Z

.field public g:Lcom/facebook/common/references/CloseableReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field public i:Z

.field public j:Z

.field public final synthetic k:Lf/g/j/q/r0;


# direct methods
.method public constructor <init>(Lf/g/j/q/r0;Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/r/b;Lf/g/j/q/v0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/x0;",
            "Lf/g/j/r/b;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lf/g/j/q/r0$b;->k:Lf/g/j/q/r0;

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    const/4 p2, 0x0

    iput-object p2, p0, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    const/4 p2, 0x0

    iput p2, p0, Lf/g/j/q/r0$b;->h:I

    iput-boolean p2, p0, Lf/g/j/q/r0$b;->i:Z

    iput-boolean p2, p0, Lf/g/j/q/r0$b;->j:Z

    iput-object p3, p0, Lf/g/j/q/r0$b;->c:Lf/g/j/q/x0;

    iput-object p4, p0, Lf/g/j/q/r0$b;->e:Lf/g/j/r/b;

    iput-object p5, p0, Lf/g/j/q/r0$b;->d:Lf/g/j/q/v0;

    new-instance p2, Lf/g/j/q/r0$b$a;

    invoke-direct {p2, p0, p1}, Lf/g/j/q/r0$b$a;-><init>(Lf/g/j/q/r0$b;Lf/g/j/q/r0;)V

    invoke-interface {p5, p2}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    return-void
.end method

.method public static n(Lf/g/j/q/r0$b;Lcom/facebook/common/references/CloseableReference;I)V
    .locals 4

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/j/c;

    instance-of v0, v0, Lf/g/j/j/d;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lf/g/j/q/r0$b;->q(Lcom/facebook/common/references/CloseableReference;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/j/q/r0$b;->c:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/r0$b;->d:Lf/g/j/q/v0;

    const-string v2, "PostprocessorProducer"

    invoke-interface {v0, v1, v2}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/g/j/j/c;

    invoke-virtual {p0, p1}, Lf/g/j/q/r0$b;->r(Lf/g/j/j/c;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object p1, p0, Lf/g/j/q/r0$b;->c:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/r0$b;->d:Lf/g/j/q/v0;

    iget-object v3, p0, Lf/g/j/q/r0$b;->e:Lf/g/j/r/b;

    invoke-virtual {p0, p1, v1, v3}, Lf/g/j/q/r0$b;->p(Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/r/b;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p0, v0, p2}, Lf/g/j/q/r0$b;->q(Lcom/facebook/common/references/CloseableReference;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    goto :goto_0

    :catchall_0
    move-exception p0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    iget-object p2, p0, Lf/g/j/q/r0$b;->c:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/r0$b;->d:Lf/g/j/q/v0;

    iget-object v3, p0, Lf/g/j/q/r0$b;->e:Lf/g/j/r/b;

    invoke-virtual {p0, p2, v1, v3}, Lf/g/j/q/r0$b;->p(Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/r/b;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {p2, v1, v2, p1, v3}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    invoke-virtual {p0}, Lf/g/j/q/r0$b;->o()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    return-void

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    throw p0
.end method


# virtual methods
.method public g()V
    .locals 1

    invoke-virtual {p0}, Lf/g/j/q/r0$b;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    :cond_0
    return-void
.end method

.method public h(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p0}, Lf/g/j/q/r0$b;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p2}, Lf/g/j/q/r0$b;->q(Lcom/facebook/common/references/CloseableReference;I)V

    goto :goto_0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf/g/j/q/r0$b;->f:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    iput p2, p0, Lf/g/j/q/r0$b;->h:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/g/j/q/r0$b;->i:Z

    invoke-virtual {p0}, Lf/g/j/q/r0$b;->s()Z

    move-result p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    if-eqz p1, :cond_3

    iget-object p1, p0, Lf/g/j/q/r0$b;->k:Lf/g/j/q/r0;

    iget-object p1, p1, Lf/g/j/q/r0;->c:Ljava/util/concurrent/Executor;

    new-instance p2, Lf/g/j/q/s0;

    invoke-direct {p2, p0}, Lf/g/j/q/s0;-><init>(Lf/g/j/q/r0$b;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_3
    :goto_0
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final o()Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf/g/j/q/r0$b;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    return v0

    :cond_0
    iget-object v0, p0, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    const/4 v1, 0x0

    iput-object v1, p0, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/g/j/q/r0$b;->f:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    return v1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final p(Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/r/b;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/x0;",
            "Lf/g/j/q/v0;",
            "Lf/g/j/r/b;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "PostprocessorProducer"

    invoke-interface {p1, p2, v0}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-interface {p3}, Lf/g/j/r/b;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Postprocessor"

    invoke-static {p2, p1}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public final q(Lcom/facebook/common/references/CloseableReference;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;I)V"
        }
    .end annotation

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lf/g/j/q/r0$b;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    if-eqz v1, :cond_1

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lf/g/j/q/r0$b;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    :cond_2
    return-void
.end method

.method public final r(Lf/g/j/j/c;)Lcom/facebook/common/references/CloseableReference;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/j/c;",
            ")",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation

    move-object v0, p1

    check-cast v0, Lf/g/j/j/d;

    iget-object v1, v0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lf/g/j/q/r0$b;->e:Lf/g/j/r/b;

    iget-object v3, p0, Lf/g/j/q/r0$b;->k:Lf/g/j/q/r0;

    iget-object v3, v3, Lf/g/j/q/r0;->b:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    invoke-interface {v2, v1, v3}, Lf/g/j/r/b;->process(Landroid/graphics/Bitmap;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    iget v2, v0, Lf/g/j/j/d;->i:I

    iget v3, v0, Lf/g/j/j/d;->j:I

    :try_start_0
    new-instance v4, Lf/g/j/j/d;

    invoke-virtual {p1}, Lf/g/j/j/c;->b()Lf/g/j/j/i;

    move-result-object p1

    invoke-direct {v4, v1, p1, v2, v3}, Lf/g/j/j/d;-><init>(Lcom/facebook/common/references/CloseableReference;Lf/g/j/j/i;II)V

    iget-object p1, v0, Lf/g/j/j/c;->d:Ljava/util/Map;

    invoke-virtual {v4, p1}, Lf/g/j/j/c;->e(Ljava/util/Map;)V

    invoke-static {v4}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-object p1

    :catchall_0
    move-exception p1

    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    throw p1
.end method

.method public final declared-synchronized s()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf/g/j/q/r0$b;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lf/g/j/q/r0$b;->i:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lf/g/j/q/r0$b;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/g/j/q/r0$b;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
