.class public Lf/g/j/q/b0;
.super Ljava/lang/Object;
.source "JobScheduler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/b0$d;,
        Lf/g/j/q/b0$c;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Lf/g/j/q/b0$c;

.field public final c:Ljava/lang/Runnable;

.field public final d:Ljava/lang/Runnable;

.field public final e:I

.field public f:Lf/g/j/j/e;

.field public g:I

.field public h:Lf/g/j/q/b0$d;

.field public i:J

.field public j:J


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lf/g/j/q/b0$c;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/b0;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lf/g/j/q/b0;->b:Lf/g/j/q/b0$c;

    iput p3, p0, Lf/g/j/q/b0;->e:I

    new-instance p1, Lf/g/j/q/b0$a;

    invoke-direct {p1, p0}, Lf/g/j/q/b0$a;-><init>(Lf/g/j/q/b0;)V

    iput-object p1, p0, Lf/g/j/q/b0;->c:Ljava/lang/Runnable;

    new-instance p1, Lf/g/j/q/b0$b;

    invoke-direct {p1, p0}, Lf/g/j/q/b0$b;-><init>(Lf/g/j/q/b0;)V

    iput-object p1, p0, Lf/g/j/q/b0;->d:Ljava/lang/Runnable;

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    const/4 p1, 0x0

    iput p1, p0, Lf/g/j/q/b0;->g:I

    sget-object p1, Lf/g/j/q/b0$d;->d:Lf/g/j/q/b0$d;

    iput-object p1, p0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lf/g/j/q/b0;->i:J

    iput-wide p1, p0, Lf/g/j/q/b0;->j:J

    return-void
.end method

.method public static e(Lf/g/j/j/e;I)Z
    .locals 1

    invoke-static {p1}, Lf/g/j/q/b;->e(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lf/g/j/q/b;->m(II)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {p0}, Lf/g/j/j/e;->m(Lf/g/j/j/e;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method


# virtual methods
.method public a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    const/4 v1, 0x0

    iput-object v1, p0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    const/4 v1, 0x0

    iput v1, p0, Lf/g/j/q/b0;->g:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/g/j/j/e;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(J)V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/b0;->d:Ljava/lang/Runnable;

    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-lez v3, :cond_1

    sget-object v1, Lf/g/j/k/a;->b:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    sput-object v1, Lf/g/j/k/a;->b:Ljava/util/concurrent/ScheduledExecutorService;

    :cond_0
    sget-object v1, Lf/g/j/k/a;->b:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method public final c()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    sget-object v3, Lf/g/j/q/b0$d;->g:Lf/g/j/q/b0$d;

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, Lf/g/j/q/b0;->j:J

    iget v4, p0, Lf/g/j/q/b0;->e:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    const/4 v4, 0x1

    iput-wide v0, p0, Lf/g/j/q/b0;->i:J

    sget-object v5, Lf/g/j/q/b0$d;->e:Lf/g/j/q/b0$d;

    iput-object v5, p0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    goto :goto_0

    :cond_0
    sget-object v2, Lf/g/j/q/b0$d;->d:Lf/g/j/q/b0$d;

    iput-object v2, p0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    sub-long/2addr v2, v0

    invoke-virtual {p0, v2, v3}, Lf/g/j/q/b0;->b(J)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public d()Z
    .locals 7

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    iget v3, p0, Lf/g/j/q/b0;->g:I

    invoke-static {v2, v3}, Lf/g/j/q/b0;->e(Lf/g/j/j/e;I)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    monitor-exit p0

    return v3

    :cond_0
    iget-object v2, p0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_2

    const/4 v5, 0x2

    if-eq v2, v5, :cond_1

    goto :goto_0

    :cond_1
    sget-object v2, Lf/g/j/q/b0$d;->g:Lf/g/j/q/b0$d;

    iput-object v2, p0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    :goto_0
    const-wide/16 v5, 0x0

    goto :goto_1

    :cond_2
    iget-wide v2, p0, Lf/g/j/q/b0;->j:J

    iget v5, p0, Lf/g/j/q/b0;->e:I

    int-to-long v5, v5

    add-long/2addr v2, v5

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    iput-wide v0, p0, Lf/g/j/q/b0;->i:J

    sget-object v2, Lf/g/j/q/b0$d;->e:Lf/g/j/q/b0$d;

    iput-object v2, p0, Lf/g/j/q/b0;->h:Lf/g/j/q/b0$d;

    const/4 v3, 0x1

    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_3

    sub-long/2addr v5, v0

    invoke-virtual {p0, v5, v6}, Lf/g/j/q/b0;->b(J)V

    :cond_3
    return v4

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public f(Lf/g/j/j/e;I)Z
    .locals 1

    invoke-static {p1, p2}, Lf/g/j/q/b0;->e(Lf/g/j/j/e;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    invoke-static {p1}, Lf/g/j/j/e;->a(Lf/g/j/j/e;)Lf/g/j/j/e;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/q/b0;->f:Lf/g/j/j/e;

    iput p2, p0, Lf/g/j/q/b0;->g:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/g/j/j/e;->close()V

    :cond_1
    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
