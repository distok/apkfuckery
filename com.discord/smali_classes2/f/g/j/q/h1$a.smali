.class public Lf/g/j/q/h1$a;
.super Lf/g/j/q/o;
.source "ThumbnailBranchProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/h1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/q/v0;

.field public final d:I

.field public final e:Lf/g/j/d/e;

.field public final synthetic f:Lf/g/j/q/h1;


# direct methods
.method public constructor <init>(Lf/g/j/q/h1;Lf/g/j/q/l;Lf/g/j/q/v0;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            "I)V"
        }
    .end annotation

    iput-object p1, p0, Lf/g/j/q/h1$a;->f:Lf/g/j/q/h1;

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p3, p0, Lf/g/j/q/h1$a;->c:Lf/g/j/q/v0;

    iput p4, p0, Lf/g/j/q/h1$a;->d:I

    invoke-interface {p3}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object p1

    iget-object p1, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    iput-object p1, p0, Lf/g/j/q/h1$a;->e:Lf/g/j/d/e;

    return-void
.end method


# virtual methods
.method public h(Ljava/lang/Throwable;)V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/h1$a;->f:Lf/g/j/q/h1;

    iget v1, p0, Lf/g/j/q/h1$a;->d:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    iget-object v3, p0, Lf/g/j/q/h1$a;->c:Lf/g/j/q/v0;

    invoke-virtual {v0, v1, v2, v3}, Lf/g/j/q/h1;->c(ILf/g/j/q/l;Lf/g/j/q/v0;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lf/g/j/j/e;

    if-eqz p1, :cond_1

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/h1$a;->e:Lf/g/j/d/e;

    invoke-static {p1, v0}, Lf/g/j/k/a;->q0(Lf/g/j/j/e;Lf/g/j/d/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result p2

    if-eqz p2, :cond_3

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    :cond_2
    iget-object p1, p0, Lf/g/j/q/h1$a;->f:Lf/g/j/q/h1;

    iget p2, p0, Lf/g/j/q/h1$a;->d:I

    const/4 v0, 0x1

    add-int/2addr p2, v0

    iget-object v1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    iget-object v2, p0, Lf/g/j/q/h1$a;->c:Lf/g/j/q/v0;

    invoke-virtual {p1, p2, v1, v2}, Lf/g/j/q/h1;->c(ILf/g/j/q/l;Lf/g/j/q/v0;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/4 p2, 0x0

    invoke-interface {p1, p2, v0}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    :cond_3
    :goto_0
    return-void
.end method
