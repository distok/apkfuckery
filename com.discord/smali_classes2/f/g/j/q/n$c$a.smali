.class public Lf/g/j/q/n$c$a;
.super Ljava/lang/Object;
.source "DecodeProducer.java"

# interfaces
.implements Lf/g/j/q/b0$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/n$c;-><init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/q/v0;

.field public final synthetic b:I

.field public final synthetic c:Lf/g/j/q/n$c;


# direct methods
.method public constructor <init>(Lf/g/j/q/n$c;Lf/g/j/q/n;Lf/g/j/q/v0;I)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/n$c$a;->c:Lf/g/j/q/n$c;

    iput-object p3, p0, Lf/g/j/q/n$c$a;->a:Lf/g/j/q/v0;

    iput p4, p0, Lf/g/j/q/n$c$a;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/j/e;I)V
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v0, p2

    if-eqz v2, :cond_d

    iget-object v3, v1, Lf/g/j/q/n$c$a;->c:Lf/g/j/q/n$c;

    iget-object v3, v3, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    const-string v4, "image_format"

    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->o()V

    iget-object v5, v2, Lf/g/j/j/e;->f:Lf/g/i/c;

    iget-object v5, v5, Lf/g/i/c;->a:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Lf/g/j/q/v0;->d(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v3, v1, Lf/g/j/q/n$c$a;->c:Lf/g/j/q/n$c;

    iget-object v3, v3, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    iget-boolean v3, v3, Lf/g/j/q/n;->f:Z

    if-nez v3, :cond_0

    const/16 v3, 0x10

    invoke-static {v0, v3}, Lf/g/j/q/b;->m(II)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    iget-object v3, v1, Lf/g/j/q/n$c$a;->a:Lf/g/j/q/v0;

    invoke-interface {v3}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v3

    iget-object v4, v1, Lf/g/j/q/n$c$a;->c:Lf/g/j/q/n$c;

    iget-object v4, v4, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    iget-boolean v4, v4, Lf/g/j/q/n;->g:Z

    if-nez v4, :cond_1

    iget-object v4, v3, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-static {v4}, Lf/g/d/l/b;->e(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    iget-object v4, v3, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-object v3, v3, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    iget v5, v1, Lf/g/j/q/n$c$a;->b:I

    invoke-static {v4, v3, v2, v5}, Lf/g/j/k/a;->O(Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/j/j/e;I)I

    move-result v3

    iput v3, v2, Lf/g/j/j/e;->k:I

    :cond_2
    iget-object v3, v1, Lf/g/j/q/n$c$a;->a:Lf/g/j/q/v0;

    invoke-interface {v3}, Lf/g/j/q/v0;->g()Lf/g/j/e/k;

    move-result-object v3

    iget-object v3, v3, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v1, Lf/g/j/q/n$c$a;->c:Lf/g/j/q/n$c;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->o()V

    iget-object v4, v2, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v5, Lf/g/i/b;->a:Lf/g/i/c;

    if-eq v4, v5, :cond_3

    invoke-static/range {p2 .. p2}, Lf/g/j/q/b;->f(I)Z

    move-result v4

    if-eqz v4, :cond_3

    goto/16 :goto_a

    :cond_3
    monitor-enter v3

    :try_start_0
    iget-boolean v4, v3, Lf/g/j/q/n$c;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    monitor-exit v3

    if-nez v4, :cond_d

    invoke-static/range {p1 .. p1}, Lf/g/j/j/e;->m(Lf/g/j/j/e;)Z

    move-result v4

    if-nez v4, :cond_4

    goto/16 :goto_a

    :cond_4
    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->o()V

    iget-object v4, v2, Lf/g/j/j/e;->f:Lf/g/i/c;

    if-eqz v4, :cond_5

    iget-object v4, v4, Lf/g/i/c;->a:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v4, "unknown"

    :goto_0
    move-object v10, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->o()V

    iget v5, v2, Lf/g/j/j/e;->i:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->o()V

    iget v5, v2, Lf/g/j/j/e;->j:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget v4, v2, Lf/g/j/j/e;->k:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p2 .. p2}, Lf/g/j/q/b;->e(I)Z

    move-result v9

    if-eqz v9, :cond_6

    const/16 v5, 0x8

    invoke-static {v0, v5}, Lf/g/j/q/b;->m(II)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x1

    goto :goto_1

    :cond_6
    const/4 v5, 0x0

    :goto_1
    const/4 v6, 0x4

    invoke-static {v0, v6}, Lf/g/j/q/b;->m(II)Z

    move-result v7

    iget-object v8, v3, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-interface {v8}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v8

    iget-object v8, v8, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    if-eqz v8, :cond_7

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget v15, v8, Lf/g/j/d/e;->a:I

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v15, "x"

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, v8, Lf/g/j/d/e;->b:I

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_7
    const-string v8, "unknown"

    :goto_2
    move-object v12, v8

    :try_start_1
    iget-object v8, v3, Lf/g/j/q/n$c;->g:Lf/g/j/q/b0;

    monitor-enter v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-wide v14, v8, Lf/g/j/q/b0;->j:J

    move/from16 v18, v7

    iget-wide v6, v8, Lf/g/j/q/b0;->i:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sub-long v6, v14, v6

    :try_start_3
    monitor-exit v8

    iget-object v8, v3, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-interface {v8}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v8

    iget-object v8, v8, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    if-nez v5, :cond_9

    if-eqz v18, :cond_8

    goto :goto_3

    :cond_8
    invoke-virtual {v3, v2}, Lf/g/j/q/n$c;->o(Lf/g/j/j/e;)I

    move-result v14

    goto :goto_4

    :cond_9
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->f()I

    move-result v14

    :goto_4
    if-nez v5, :cond_b

    if-eqz v18, :cond_a

    goto :goto_5

    :cond_a
    invoke-virtual {v3}, Lf/g/j/q/n$c;->p()Lf/g/j/j/i;

    move-result-object v5

    goto :goto_6

    :cond_b
    :goto_5
    sget-object v5, Lf/g/j/j/h;->d:Lf/g/j/j/i;

    :goto_6
    move-object v15, v5

    iget-object v5, v3, Lf/g/j/q/n$c;->d:Lf/g/j/q/x0;

    iget-object v4, v3, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    const-string v1, "DecodeProducer"

    invoke-interface {v5, v4, v1}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v3, v2, v14, v15}, Lf/g/j/q/n$c;->t(Lf/g/j/j/e;ILf/g/j/j/i;)Lf/g/j/j/c;

    move-result-object v1
    :try_end_4
    .catch Lcom/facebook/imagepipeline/decoder/DecodeException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    iget v4, v2, Lf/g/j/j/e;->k:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v5, 0x1

    if-eq v4, v5, :cond_c

    or-int/lit8 v0, v0, 0x10

    :cond_c
    move-object v4, v3

    move-object v5, v1

    move-object v8, v15

    :try_start_6
    invoke-virtual/range {v4 .. v13}, Lf/g/j/q/n$c;->n(Lf/g/j/j/c;JLf/g/j/j/i;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    iget-object v5, v3, Lf/g/j/q/n$c;->d:Lf/g/j/q/x0;

    iget-object v6, v3, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    const-string v7, "DecodeProducer"

    invoke-interface {v5, v6, v7, v4}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v3, v2, v1}, Lf/g/j/q/n$c;->v(Lf/g/j/j/e;Lf/g/j/j/c;)V

    invoke-virtual {v3, v1, v0}, Lf/g/j/q/n$c;->s(Lf/g/j/j/c;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_9

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    move-object v1, v0

    :try_start_7
    invoke-virtual {v1}, Lcom/facebook/imagepipeline/decoder/DecodeException;->a()Lf/g/j/j/e;

    move-result-object v0

    const-string v4, "ProgressiveDecoder"

    const-string v5, "%s, {uri: %s, firstEncodedBytes: %s, length: %d}"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    aput-object v17, v14, v18

    const/16 v16, 0x1

    aput-object v8, v14, v16

    const/16 v8, 0xa

    invoke-virtual {v0, v8}, Lf/g/j/j/e;->d(I)Ljava/lang/String;

    move-result-object v8

    const/16 v17, 0x2

    aput-object v8, v14, v17

    const/4 v8, 0x3

    invoke-virtual {v0}, Lf/g/j/j/e;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v14, v8

    invoke-static {v4, v5, v14}, Lf/g/d/e/a;->o(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_7
    const/4 v1, 0x0

    :goto_8
    move-object v5, v1

    move-object v4, v3

    move-object v8, v15

    :try_start_8
    invoke-virtual/range {v4 .. v13}, Lf/g/j/q/n$c;->n(Lf/g/j/j/c;JLf/g/j/j/i;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    iget-object v4, v3, Lf/g/j/q/n$c;->d:Lf/g/j/q/x0;

    iget-object v5, v3, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    const-string v6, "DecodeProducer"

    invoke-interface {v4, v5, v6, v0, v1}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Lf/g/j/q/n$c;->u(Z)V

    iget-object v1, v3, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v1, v0}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :goto_9
    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->close()V

    goto :goto_a

    :catchall_0
    move-exception v0

    :try_start_9
    monitor-exit v8

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->close()V

    throw v0

    :catchall_2
    move-exception v0

    move-object v1, v0

    monitor-exit v3

    throw v1

    :cond_d
    :goto_a
    return-void
.end method
