.class public Lf/g/j/q/n$a;
.super Lf/g/j/q/n$c;
.source "DecodeProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            "ZI)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Lf/g/j/q/n$c;-><init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;ZI)V

    return-void
.end method


# virtual methods
.method public o(Lf/g/j/j/e;)I
    .locals 0

    invoke-virtual {p1}, Lf/g/j/j/e;->f()I

    move-result p1

    return p1
.end method

.method public p()Lf/g/j/j/i;
    .locals 2

    new-instance v0, Lf/g/j/j/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1}, Lf/g/j/j/h;-><init>(IZZ)V

    return-object v0
.end method

.method public declared-synchronized w(Lf/g/j/j/e;I)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lf/g/j/q/n$c;->g:Lf/g/j/q/b0;

    invoke-virtual {v0, p1, p2}, Lf/g/j/q/b0;->f(Lf/g/j/j/e;I)Z

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
