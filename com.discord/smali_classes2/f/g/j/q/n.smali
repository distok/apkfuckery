.class public Lf/g/j/q/n;
.super Ljava/lang/Object;
.source "DecodeProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/n$b;,
        Lf/g/j/q/n$a;,
        Lf/g/j/q/n$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/d/g/a;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Lf/g/j/h/b;

.field public final d:Lf/g/j/h/c;

.field public final e:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:I

.field public final j:Lf/g/j/e/b;

.field public final k:Ljava/lang/Runnable;

.field public final l:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/d/g/a;Ljava/util/concurrent/Executor;Lf/g/j/h/b;Lf/g/j/h/c;ZZZLf/g/j/q/u0;ILf/g/j/e/b;Ljava/lang/Runnable;Lcom/facebook/common/internal/Supplier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/d/g/a;",
            "Ljava/util/concurrent/Executor;",
            "Lf/g/j/h/b;",
            "Lf/g/j/h/c;",
            "ZZZ",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;I",
            "Lf/g/j/e/b;",
            "Ljava/lang/Runnable;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/q/n;->a:Lf/g/d/g/a;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lf/g/j/q/n;->b:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lf/g/j/q/n;->c:Lf/g/j/h/b;

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p4, p0, Lf/g/j/q/n;->d:Lf/g/j/h/c;

    iput-boolean p5, p0, Lf/g/j/q/n;->f:Z

    iput-boolean p6, p0, Lf/g/j/q/n;->g:Z

    invoke-static {p8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p8, p0, Lf/g/j/q/n;->e:Lf/g/j/q/u0;

    iput-boolean p7, p0, Lf/g/j/q/n;->h:Z

    iput p9, p0, Lf/g/j/q/n;->i:I

    iput-object p10, p0, Lf/g/j/q/n;->j:Lf/g/j/e/b;

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/j/q/n;->k:Ljava/lang/Runnable;

    iput-object p12, p0, Lf/g/j/q/n;->l:Lcom/facebook/common/internal/Supplier;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-static {v0}, Lf/g/d/l/b;->e(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lf/g/j/q/n$a;

    iget-boolean v5, p0, Lf/g/j/q/n;->h:Z

    iget v6, p0, Lf/g/j/q/n;->i:I

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lf/g/j/q/n$a;-><init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;ZI)V

    goto :goto_0

    :cond_0
    new-instance v4, Lf/g/j/h/d;

    iget-object v0, p0, Lf/g/j/q/n;->a:Lf/g/d/g/a;

    invoke-direct {v4, v0}, Lf/g/j/h/d;-><init>(Lf/g/d/g/a;)V

    new-instance v8, Lf/g/j/q/n$b;

    iget-object v5, p0, Lf/g/j/q/n;->d:Lf/g/j/h/c;

    iget-boolean v6, p0, Lf/g/j/q/n;->h:Z

    iget v7, p0, Lf/g/j/q/n;->i:I

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lf/g/j/q/n$b;-><init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/h/d;Lf/g/j/h/c;ZI)V

    move-object v0, v8

    :goto_0
    iget-object p1, p0, Lf/g/j/q/n;->e:Lf/g/j/q/u0;

    invoke-interface {p1, v0, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
