.class public Lf/g/j/q/g1$b$a;
.super Ljava/lang/Object;
.source "ThrottlingProducer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/g1$b;->n()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Landroid/util/Pair;

.field public final synthetic e:Lf/g/j/q/g1$b;


# direct methods
.method public constructor <init>(Lf/g/j/q/g1$b;Landroid/util/Pair;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/g1$b$a;->e:Lf/g/j/q/g1$b;

    iput-object p2, p0, Lf/g/j/q/g1$b$a;->d:Landroid/util/Pair;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/g/j/q/g1$b$a;->e:Lf/g/j/q/g1$b;

    iget-object v0, v0, Lf/g/j/q/g1$b;->c:Lf/g/j/q/g1;

    iget-object v1, p0, Lf/g/j/q/g1$b$a;->d:Landroid/util/Pair;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lf/g/j/q/l;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lf/g/j/q/v0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v3

    const-string v4, "ThrottlingProducer"

    const/4 v5, 0x0

    invoke-interface {v3, v1, v4, v5}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v3, v0, Lf/g/j/q/g1;->a:Lf/g/j/q/u0;

    new-instance v4, Lf/g/j/q/g1$b;

    invoke-direct {v4, v0, v2, v5}, Lf/g/j/q/g1$b;-><init>(Lf/g/j/q/g1;Lf/g/j/q/l;Lf/g/j/q/g1$a;)V

    invoke-interface {v3, v4, v1}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void
.end method
