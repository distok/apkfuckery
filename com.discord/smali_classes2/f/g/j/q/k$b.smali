.class public Lf/g/j/q/k$b;
.super Lf/g/j/q/o;
.source "BranchOnSeparateImagesProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public c:Lf/g/j/q/v0;

.field public final synthetic d:Lf/g/j/q/k;


# direct methods
.method public constructor <init>(Lf/g/j/q/k;Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/q/k$a;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/k$b;->d:Lf/g/j/q/k;

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p3, p0, Lf/g/j/q/k$b;->c:Lf/g/j/q/v0;

    return-void
.end method


# virtual methods
.method public h(Ljava/lang/Throwable;)V
    .locals 2

    iget-object p1, p0, Lf/g/j/q/k$b;->d:Lf/g/j/q/k;

    iget-object p1, p1, Lf/g/j/q/k;->b:Lf/g/j/q/u0;

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    iget-object v1, p0, Lf/g/j/q/k$b;->c:Lf/g/j/q/v0;

    invoke-interface {p1, v0, v1}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lf/g/j/j/e;

    iget-object v0, p0, Lf/g/j/q/k$b;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result v1

    iget-object v2, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    invoke-static {p1, v2}, Lf/g/j/k/a;->q0(Lf/g/j/j/e;Lf/g/j/d/e;)Z

    move-result v2

    if-eqz p1, :cond_2

    if-nez v2, :cond_0

    iget-boolean v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->f:Z

    if-eqz v0, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_1
    and-int/lit8 p2, p2, -0x2

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    :cond_2
    :goto_0
    if-eqz v1, :cond_4

    if-nez v2, :cond_4

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    :cond_3
    iget-object p1, p0, Lf/g/j/q/k$b;->d:Lf/g/j/q/k;

    iget-object p1, p1, Lf/g/j/q/k;->b:Lf/g/j/q/u0;

    iget-object p2, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    iget-object v0, p0, Lf/g/j/q/k$b;->c:Lf/g/j/q/v0;

    invoke-interface {p1, p2, v0}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :cond_4
    return-void
.end method
