.class public Lf/g/j/q/q0$a;
.super Lf/g/j/q/o;
.source "PostprocessedBitmapMemoryCacheProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/q0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final c:Lcom/facebook/cache/common/CacheKey;

.field public final d:Z

.field public final e:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Z


# direct methods
.method public constructor <init>(Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;ZLf/g/j/c/t;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lcom/facebook/cache/common/CacheKey;",
            "Z",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p2, p0, Lf/g/j/q/q0$a;->c:Lcom/facebook/cache/common/CacheKey;

    iput-boolean p3, p0, Lf/g/j/q/q0$a;->d:Z

    iput-object p4, p0, Lf/g/j/q/q0$a;->e:Lf/g/j/c/t;

    iput-boolean p5, p0, Lf/g/j/q/q0$a;->f:Z

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p1, v0, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lf/g/j/q/q0$a;->d:Z

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lf/g/j/q/q0$a;->f:Z

    if-eqz v1, :cond_2

    iget-object v0, p0, Lf/g/j/q/q0$a;->e:Lf/g/j/c/t;

    iget-object v1, p0, Lf/g/j/q/q0$a;->c:Lcom/facebook/cache/common/CacheKey;

    invoke-interface {v0, v1, p1}, Lf/g/j/c/t;->a(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    :cond_2
    :try_start_0
    iget-object v1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, Lf/g/j/q/l;->a(F)V

    iget-object v1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    if-eqz v0, :cond_3

    move-object p1, v0

    :cond_3
    invoke-interface {v1, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object p1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_4
    :goto_0
    return-void

    :catchall_0
    move-exception p1

    sget-object p2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_5
    throw p1
.end method
