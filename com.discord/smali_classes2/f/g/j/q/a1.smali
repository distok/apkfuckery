.class public Lf/g/j/q/a1;
.super Ljava/lang/Object;
.source "ResizeAndRotateProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/a1$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Lf/g/d/g/g;

.field public final c:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Lf/g/j/t/c;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Lf/g/j/q/u0;ZLf/g/j/t/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf/g/d/g/g;",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;Z",
            "Lf/g/j/t/c;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/q/a1;->a:Ljava/util/concurrent/Executor;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lf/g/j/q/a1;->b:Lf/g/d/g/g;

    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lf/g/j/q/a1;->c:Lf/g/j/q/u0;

    invoke-static {p5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p5, p0, Lf/g/j/q/a1;->e:Lf/g/j/t/c;

    iput-boolean p4, p0, Lf/g/j/q/a1;->d:Z

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/q/a1;->c:Lf/g/j/q/u0;

    new-instance v7, Lf/g/j/q/a1$a;

    iget-boolean v5, p0, Lf/g/j/q/a1;->d:Z

    iget-object v6, p0, Lf/g/j/q/a1;->e:Lf/g/j/t/c;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lf/g/j/q/a1$a;-><init>(Lf/g/j/q/a1;Lf/g/j/q/l;Lf/g/j/q/v0;ZLf/g/j/t/c;)V

    invoke-interface {v0, v7, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void
.end method
