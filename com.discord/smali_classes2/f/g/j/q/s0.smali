.class public Lf/g/j/q/s0;
.super Ljava/lang/Object;
.source "PostprocessorProducer.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/g/j/q/r0$b;


# direct methods
.method public constructor <init>(Lf/g/j/q/r0$b;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/s0;->d:Lf/g/j/q/r0$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lf/g/j/q/s0;->d:Lf/g/j/q/r0$b;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/g/j/q/s0;->d:Lf/g/j/q/r0$b;

    iget-object v2, v1, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    iget v3, v1, Lf/g/j/q/r0$b;->h:I

    const/4 v4, 0x0

    iput-object v4, v1, Lf/g/j/q/r0$b;->g:Lcom/facebook/common/references/CloseableReference;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lf/g/j/q/r0$b;->i:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    invoke-static {v2}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lf/g/j/q/s0;->d:Lf/g/j/q/r0$b;

    invoke-static {v0, v2, v3}, Lf/g/j/q/r0$b;->n(Lf/g/j/q/r0$b;Lcom/facebook/common/references/CloseableReference;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    throw v0

    :cond_1
    :goto_0
    iget-object v1, p0, Lf/g/j/q/s0;->d:Lf/g/j/q/r0$b;

    monitor-enter v1

    :try_start_2
    iput-boolean v4, v1, Lf/g/j/q/r0$b;->j:Z

    invoke-virtual {v1}, Lf/g/j/q/r0$b;->s()Z

    move-result v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v0, :cond_2

    iget-object v0, v1, Lf/g/j/q/r0$b;->k:Lf/g/j/q/r0;

    iget-object v0, v0, Lf/g/j/q/r0;->c:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/g/j/q/s0;

    invoke-direct {v2, v1}, Lf/g/j/q/s0;-><init>(Lf/g/j/q/r0$b;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_2
    return-void

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :catchall_2
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1
.end method
