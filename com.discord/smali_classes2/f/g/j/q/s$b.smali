.class public Lf/g/j/q/s$b;
.super Lf/g/j/q/o;
.source "DiskCacheWriteProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/q/v0;

.field public final d:Lf/g/j/c/g;

.field public final e:Lf/g/j/c/g;

.field public final f:Lf/g/j/c/j;


# direct methods
.method public constructor <init>(Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/s$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p2, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    iput-object p3, p0, Lf/g/j/q/s$b;->d:Lf/g/j/c/g;

    iput-object p4, p0, Lf/g/j/q/s$b;->e:Lf/g/j/c/g;

    iput-object p5, p0, Lf/g/j/q/s$b;->f:Lf/g/j/c/j;

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 5

    check-cast p1, Lf/g/j/j/e;

    iget-object v0, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    const-string v2, "DiskCacheWriteProducer"

    invoke-interface {v0, v1, v2}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    const/16 v0, 0xa

    invoke-static {p2, v0}, Lf/g/j/q/b;->l(II)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v0, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v3, Lf/g/i/c;->b:Lf/g/i/c;

    if-ne v0, v3, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v3, p0, Lf/g/j/q/s$b;->f:Lf/g/j/c/j;

    iget-object v4, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    invoke-interface {v4}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v3, Lf/g/j/c/n;

    invoke-virtual {v3, v0, v4}, Lf/g/j/c/n;->b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v3

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    sget-object v4, Lcom/facebook/imagepipeline/request/ImageRequest$b;->d:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lf/g/j/q/s$b;->e:Lf/g/j/c/g;

    invoke-virtual {v0, v3, p1}, Lf/g/j/c/g;->f(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/g/j/q/s$b;->d:Lf/g/j/c/g;

    invoke-virtual {v0, v3, p1}, Lf/g/j/c/g;->f(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    :goto_0
    iget-object v0, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v3, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    invoke-interface {v0, v3, v2, v1}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_2

    :cond_2
    :goto_1
    iget-object v0, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v3, p0, Lf/g/j/q/s$b;->c:Lf/g/j/q/v0;

    invoke-interface {v0, v3, v2, v1}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    :goto_2
    return-void
.end method
