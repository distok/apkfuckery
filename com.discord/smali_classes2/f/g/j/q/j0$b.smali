.class public Lf/g/j/q/j0$b;
.super Ljava/lang/Object;
.source "MultiplexProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/j0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/j0$b$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Landroid/util/Pair<",
            "Lf/g/j/q/l<",
            "TT;>;",
            "Lf/g/j/q/v0;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public d:F

.field public e:I

.field public f:Lf/g/j/q/d;

.field public g:Lf/g/j/q/j0$b$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/j0<",
            "TK;TT;>.b.a;"
        }
    .end annotation
.end field

.field public final synthetic h:Lf/g/j/q/j0;


# direct methods
.method public constructor <init>(Lf/g/j/q/j0;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    iput-object p1, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    iput-object p2, p0, Lf/g/j/q/j0$b;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/q/l;Lf/g/j/q/v0;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "TT;>;",
            "Lf/g/j/q/v0;",
            ")Z"
        }
    .end annotation

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v2, p0, Lf/g/j/q/j0$b;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    iget-object v3, v1, Lf/g/j/q/j0;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/g/j/q/j0$b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    monitor-exit v1

    if-eq v2, p0, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return p1

    :cond_0
    iget-object v1, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lf/g/j/q/j0$b;->k()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lf/g/j/q/j0$b;->l()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lf/g/j/q/j0$b;->j()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    iget v5, p0, Lf/g/j/q/j0$b;->d:F

    iget v6, p0, Lf/g/j/q/j0$b;->e:I

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    invoke-static {v1}, Lf/g/j/q/d;->s(Ljava/util/List;)V

    invoke-static {v2}, Lf/g/j/q/d;->t(Ljava/util/List;)V

    invoke-static {v3}, Lf/g/j/q/d;->r(Ljava/util/List;)V

    monitor-enter v0

    :try_start_3
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v1, p0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    if-eq v4, v1, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_2

    iget-object v1, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    invoke-virtual {v1, v4}, Lf/g/j/q/j0;->c(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v4

    :cond_2
    :goto_0
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v4, :cond_4

    const/4 v1, 0x0

    cmpl-float v1, v5, v1

    if-lez v1, :cond_3

    :try_start_5
    invoke-interface {p1, v5}, Lf/g/j/q/l;->a(F)V

    :cond_3
    invoke-interface {p1, v4, v6}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    invoke-virtual {p0, v4}, Lf/g/j/q/j0$b;->b(Ljava/io/Closeable;)V

    :cond_4
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    new-instance p1, Lf/g/j/q/k0;

    invoke-direct {p1, p0, v0}, Lf/g/j/q/k0;-><init>(Lf/g/j/q/j0$b;Landroid/util/Pair;)V

    invoke-interface {p2, p1}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw p1

    :catchall_1
    move-exception p1

    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw p1

    :catchall_2
    move-exception p1

    :try_start_8
    monitor-exit v1

    throw p1

    :catchall_3
    move-exception p1

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw p1
.end method

.method public final b(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :goto_0
    return-void
.end method

.method public final declared-synchronized c()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lf/g/j/q/v0;

    invoke-interface {v1}, Lf/g/j/q/v0;->p()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lf/g/j/q/v0;

    invoke-interface {v1}, Lf/g/j/q/v0;->k()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lf/g/j/d/d;
    .locals 5

    monitor-enter p0

    :try_start_0
    sget-object v0, Lf/g/j/d/d;->d:Lf/g/j/d/d;

    iget-object v1, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lf/g/j/q/v0;

    invoke-interface {v2}, Lf/g/j/q/v0;->c()Lf/g/j/d/d;

    move-result-object v2

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-le v3, v4, :cond_0

    goto :goto_0

    :cond_3
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f(Lf/g/j/q/j0$b$a;Ljava/lang/Throwable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/j0<",
            "TK;TT;>.b.a;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->g:Lf/g/j/q/j0$b$a;

    if-eq v0, p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object p1, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    iget-object v0, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    iget-object v0, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v1, p0, Lf/g/j/q/j0$b;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p0}, Lf/g/j/q/j0;->e(Ljava/lang/Object;Lf/g/j/q/j0$b;)V

    iget-object v0, p0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    invoke-virtual {p0, v0}, Lf/g/j/q/j0$b;->b(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    monitor-enter v1

    :try_start_1
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lf/g/j/q/v0;

    invoke-interface {v2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v2

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lf/g/j/q/v0;

    iget-object v4, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v4, v4, Lf/g/j/q/j0;->d:Ljava/lang/String;

    invoke-interface {v2, v3, v4, p2, v0}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lf/g/j/q/l;

    invoke-interface {v2, p2}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_1
    return-void

    :catchall_1
    move-exception p1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1
.end method

.method public g(Lf/g/j/q/j0$b$a;Ljava/io/Closeable;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/j0<",
            "TK;TT;>.b.a;TT;I)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->g:Lf/g/j/q/j0$b$a;

    if-eq v0, p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object p1, p0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    invoke-virtual {p0, p1}, Lf/g/j/q/j0$b;->b(Ljava/io/Closeable;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    iget-object v0, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->size()I

    move-result v1

    invoke-static {p3}, Lf/g/j/q/b;->f(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    invoke-virtual {v2, p2}, Lf/g/j/q/j0;->c(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v2

    iput-object v2, p0, Lf/g/j/q/j0$b;->c:Ljava/io/Closeable;

    iput p3, p0, Lf/g/j/q/j0$b;->e:I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    iget-object v2, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v3, p0, Lf/g/j/q/j0$b;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3, p0}, Lf/g/j/q/j0;->e(Ljava/lang/Object;Lf/g/j/q/j0$b;)V

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    monitor-enter v2

    :try_start_1
    invoke-static {p3}, Lf/g/j/q/b;->e(I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lf/g/j/q/v0;

    invoke-interface {v3}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v3

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lf/g/j/q/v0;

    iget-object v5, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v5, v5, Lf/g/j/q/j0;->d:Ljava/lang/String;

    invoke-interface {v3, v4, v5, p1}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v3, p0, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;

    if-eqz v3, :cond_2

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lf/g/j/q/v0;

    iget-object v3, v3, Lf/g/j/q/d;->g:Ljava/util/Map;

    invoke-interface {v4, v3}, Lf/g/j/q/v0;->j(Ljava/util/Map;)V

    :cond_2
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lf/g/j/q/v0;

    iget-object v4, p0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v4, v4, Lf/g/j/q/j0;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lf/g/j/q/v0;->d(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lf/g/j/q/l;

    invoke-interface {v3, p2, p3}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_4
    return-void

    :catchall_1
    move-exception p1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1
.end method

.method public h(Lf/g/j/q/j0$b$a;F)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/j0<",
            "TK;TT;>.b.a;F)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->g:Lf/g/j/q/j0$b$a;

    if-eq v0, p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iput p2, p0, Lf/g/j/q/j0$b;->d:F

    iget-object p1, p0, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    monitor-enter v0

    :try_start_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lf/g/j/q/l;

    invoke-interface {v1, p2}, Lf/g/j/q/l;->a(F)V

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_1
    return-void

    :catchall_1
    move-exception p1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1
.end method

.method public final i(Lf/g/d/l/a;)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    monitor-enter p0

    :try_start_0
    iget-object v2, v1, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;

    const/4 v3, 0x1

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ls/a/b/b/a;->g(Z)V

    iget-object v2, v1, Lf/g/j/q/j0$b;->g:Lf/g/j/q/j0$b$a;

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ls/a/b/b/a;->g(Z)V

    iget-object v2, v1, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, v1, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v2, v1, Lf/g/j/q/j0$b;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lf/g/j/q/j0;->e(Ljava/lang/Object;Lf/g/j/q/j0$b;)V

    monitor-exit p0

    return-void

    :cond_2
    iget-object v2, v1, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lf/g/j/q/v0;

    new-instance v15, Lf/g/j/q/d;

    invoke-interface {v2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v6

    invoke-interface {v2}, Lf/g/j/q/v0;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v9

    invoke-interface {v2}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v2}, Lf/g/j/q/v0;->q()Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lf/g/j/q/j0$b;->d()Z

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lf/g/j/q/j0$b;->c()Z

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lf/g/j/q/j0$b;->e()Lf/g/j/d/d;

    move-result-object v14

    invoke-interface {v2}, Lf/g/j/q/v0;->g()Lf/g/j/e/k;

    move-result-object v16

    const/4 v8, 0x0

    move-object v5, v15

    move-object v4, v15

    move-object/from16 v15, v16

    invoke-direct/range {v5 .. v15}, Lf/g/j/q/d;-><init>(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/String;Ljava/lang/String;Lf/g/j/q/x0;Ljava/lang/Object;Lcom/facebook/imagepipeline/request/ImageRequest$c;ZZLf/g/j/d/d;Lf/g/j/e/k;)V

    iput-object v4, v1, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;

    invoke-interface {v2}, Lf/g/j/q/v0;->a()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v4, v2}, Lf/g/j/q/d;->j(Ljava/util/Map;)V

    invoke-static/range {p1 .. p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lf/g/d/l/a;->f:Lf/g/d/l/a;

    if-eq v0, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_7

    iget-object v2, v1, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;

    const-string v4, "started_as_prefetch"

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    if-eqz v5, :cond_6

    if-eq v5, v3, :cond_5

    const/4 v2, 0x2

    if-eq v5, v2, :cond_4

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unrecognized TriState value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "No boolean equivalent for UNSET"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v3, 0x0

    :cond_6
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lf/g/j/q/d;->d(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_7
    new-instance v0, Lf/g/j/q/j0$b$a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/g/j/q/j0$b$a;-><init>(Lf/g/j/q/j0$b;Lf/g/j/q/j0$a;)V

    iput-object v0, v1, Lf/g/j/q/j0$b;->g:Lf/g/j/q/j0$b$a;

    iget-object v2, v1, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, v1, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-object v3, v3, Lf/g/j/q/j0;->b:Lf/g/j/q/u0;

    invoke-interface {v3, v0, v2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized j()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/g/j/q/w0;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lf/g/j/q/j0$b;->c()Z

    move-result v2

    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-boolean v3, v0, Lf/g/j/q/d;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v2, v3, :cond_1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :cond_1
    :try_start_4
    iput-boolean v2, v0, Lf/g/j/q/d;->j:Z

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lf/g/j/q/d;->l:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_6
    monitor-exit v0

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/g/j/q/w0;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lf/g/j/q/j0$b;->d()Z

    move-result v2

    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-boolean v3, v0, Lf/g/j/q/d;->h:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v2, v3, :cond_1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :cond_1
    :try_start_4
    iput-boolean v2, v0, Lf/g/j/q/d;->h:Z

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lf/g/j/q/d;->l:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_6
    monitor-exit v0

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/g/j/q/w0;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lf/g/j/q/j0$b;->e()Lf/g/j/d/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/g/j/q/d;->v(Lf/g/j/d/d;)Ljava/util/List;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
