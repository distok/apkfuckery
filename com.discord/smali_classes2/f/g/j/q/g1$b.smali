.class public Lf/g/j/q/g1$b;
.super Lf/g/j/q/o;
.source "ThrottlingProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/g1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic c:Lf/g/j/q/g1;


# direct methods
.method public constructor <init>(Lf/g/j/q/g1;Lf/g/j/q/l;Lf/g/j/q/g1$a;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/g1$b;->c:Lf/g/j/q/g1;

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    return-void
.end method


# virtual methods
.method public g()V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    invoke-virtual {p0}, Lf/g/j/q/g1$b;->n()V

    return-void
.end method

.method public h(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lf/g/j/q/g1$b;->n()V

    return-void
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lf/g/j/q/g1$b;->n()V

    :cond_0
    return-void
.end method

.method public final n()V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/g1$b;->c:Lf/g/j/q/g1;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/g/j/q/g1$b;->c:Lf/g/j/q/g1;

    iget-object v1, v1, Lf/g/j/q/g1;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    if-nez v1, :cond_0

    iget-object v2, p0, Lf/g/j/q/g1$b;->c:Lf/g/j/q/g1;

    iget v3, v2, Lf/g/j/q/g1;->b:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lf/g/j/q/g1;->b:I

    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    iget-object v0, p0, Lf/g/j/q/g1$b;->c:Lf/g/j/q/g1;

    iget-object v0, v0, Lf/g/j/q/g1;->d:Ljava/util/concurrent/Executor;

    new-instance v2, Lf/g/j/q/g1$b$a;

    invoke-direct {v2, p0, v1}, Lf/g/j/q/g1$b$a;-><init>(Lf/g/j/q/g1$b;Landroid/util/Pair;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
