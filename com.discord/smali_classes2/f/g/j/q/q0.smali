.class public Lf/g/j/q/q0;
.super Ljava/lang/Object;
.source "PostprocessedBitmapMemoryCacheProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/q0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/c/j;

.field public final c:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;",
            "Lf/g/j/c/j;",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/q0;->a:Lf/g/j/c/t;

    iput-object p2, p0, Lf/g/j/q/q0;->b:Lf/g/j/c/j;

    iput-object p3, p0, Lf/g/j/q/q0;->c:Lf/g/j/q/u0;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    invoke-interface {p2}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, v1, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    if-eqz v3, :cond_4

    invoke-interface {v3}, Lf/g/j/r/b;->getPostprocessorCacheKey()Lcom/facebook/cache/common/CacheKey;

    move-result-object v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    const-string v4, "PostprocessedBitmapMemoryCacheProducer"

    invoke-interface {v0, p2, v4}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    iget-object v5, p0, Lf/g/j/q/q0;->b:Lf/g/j/c/j;

    check-cast v5, Lf/g/j/c/n;

    invoke-virtual {v5, v1, v2}, Lf/g/j/c/n;->c(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v8

    iget-object v1, p0, Lf/g/j/q/q0;->a:Lf/g/j/c/t;

    invoke-interface {v1, v8}, Lf/g/j/c/t;->get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    const-string v2, "cached_value_found"

    const/4 v5, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0, p2, v4}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "true"

    invoke-static {v2, v3}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    :cond_1
    invoke-interface {v0, p2, v4, v5}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    const/4 v2, 0x1

    invoke-interface {v0, p2, v4, v2}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    const-string v0, "memory_bitmap"

    const-string v3, "postprocessed"

    invoke-interface {p2, v0, v3}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {p1, p2}, Lf/g/j/q/l;->a(F)V

    invoke-interface {p1, v1, v2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    goto :goto_0

    :cond_2
    instance-of v9, v3, Lf/g/j/r/c;

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    iget-boolean v11, v1, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    new-instance v1, Lf/g/j/q/q0$a;

    iget-object v10, p0, Lf/g/j/q/q0;->a:Lf/g/j/c/t;

    move-object v6, v1

    move-object v7, p1

    invoke-direct/range {v6 .. v11}, Lf/g/j/q/q0$a;-><init>(Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;ZLf/g/j/c/t;Z)V

    invoke-interface {v0, p2, v4}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "false"

    invoke-static {v2, p1}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    :cond_3
    invoke-interface {v0, p2, v4, v5}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object p1, p0, Lf/g/j/q/q0;->c:Lf/g/j/q/u0;

    invoke-interface {p1, v1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :goto_0
    return-void

    :cond_4
    :goto_1
    iget-object v0, p0, Lf/g/j/q/q0;->c:Lf/g/j/q/u0;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    return-void
.end method
