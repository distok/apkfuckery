.class public Lf/g/j/q/g1;
.super Ljava/lang/Object;
.source "ThrottlingProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/g1$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "TT;>;"
        }
    .end annotation
.end field

.field public b:I

.field public final c:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Landroid/util/Pair<",
            "Lf/g/j/q/l<",
            "TT;>;",
            "Lf/g/j/q/v0;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(ILjava/util/concurrent/Executor;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/Executor;",
            "Lf/g/j/q/u0<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lf/g/j/q/g1;->d:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lf/g/j/q/g1;->a:Lf/g/j/q/u0;

    new-instance p1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object p1, p0, Lf/g/j/q/g1;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    const/4 p1, 0x0

    iput p1, p0, Lf/g/j/q/g1;->b:I

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "TT;>;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    const-string v1, "ThrottlingProducer"

    invoke-interface {v0, p2, v1}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lf/g/j/q/g1;->b:I

    const/4 v1, 0x5

    const/4 v2, 0x1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lf/g/j/q/g1;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    add-int/2addr v0, v2

    iput v0, p0, Lf/g/j/q/g1;->b:I

    const/4 v2, 0x0

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    const-string v1, "ThrottlingProducer"

    const/4 v2, 0x0

    invoke-interface {v0, p2, v1, v2}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/g/j/q/g1;->a:Lf/g/j/q/u0;

    new-instance v1, Lf/g/j/q/g1$b;

    invoke-direct {v1, p0, p1, v2}, Lf/g/j/q/g1$b;-><init>(Lf/g/j/q/g1;Lf/g/j/q/l;Lf/g/j/q/g1$a;)V

    invoke-interface {v0, v1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :cond_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
