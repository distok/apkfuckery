.class public Lf/g/j/q/k0;
.super Lf/g/j/q/e;
.source "MultiplexProducer.java"


# instance fields
.field public final synthetic a:Landroid/util/Pair;

.field public final synthetic b:Lf/g/j/q/j0$b;


# direct methods
.method public constructor <init>(Lf/g/j/q/j0$b;Landroid/util/Pair;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    iput-object p2, p0, Lf/g/j/q/k0;->a:Landroid/util/Pair;

    invoke-direct {p0}, Lf/g/j/q/e;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    iget-object v0, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    iget-object v1, v1, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    iget-object v2, p0, Lf/g/j/q/k0;->a:Landroid/util/Pair;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    iget-object v3, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    iget-object v3, v3, Lf/g/j/q/j0$b;->b:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    iget-object v3, v3, Lf/g/j/q/j0$b;->f:Lf/g/j/q/d;

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v3}, Lf/g/j/q/j0$b;->k()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v4}, Lf/g/j/q/j0$b;->l()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v5}, Lf/g/j/q/j0$b;->j()Ljava/util/List;

    move-result-object v5

    move-object v6, v3

    move-object v3, v2

    move-object v2, v6

    goto :goto_1

    :cond_1
    move-object v3, v2

    :goto_0
    move-object v4, v2

    move-object v5, v4

    :goto_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v2}, Lf/g/j/q/d;->s(Ljava/util/List;)V

    invoke-static {v4}, Lf/g/j/q/d;->t(Ljava/util/List;)V

    invoke-static {v5}, Lf/g/j/q/d;->r(Ljava/util/List;)V

    if-eqz v3, :cond_3

    iget-object v0, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    iget-object v0, v0, Lf/g/j/q/j0$b;->h:Lf/g/j/q/j0;

    iget-boolean v0, v0, Lf/g/j/q/j0;->c:Z

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lf/g/j/q/d;->k()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lf/g/j/d/d;->d:Lf/g/j/d/d;

    invoke-virtual {v3, v0}, Lf/g/j/q/d;->v(Lf/g/j/d/d;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lf/g/j/q/d;->t(Ljava/util/List;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Lf/g/j/q/d;->u()V

    :cond_3
    :goto_2
    if-eqz v1, :cond_4

    iget-object v0, p0, Lf/g/j/q/k0;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    :cond_4
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v0}, Lf/g/j/q/j0$b;->j()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lf/g/j/q/d;->r(Ljava/util/List;)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v0}, Lf/g/j/q/j0$b;->l()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lf/g/j/q/d;->t(Ljava/util/List;)V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lf/g/j/q/k0;->b:Lf/g/j/q/j0$b;

    invoke-virtual {v0}, Lf/g/j/q/j0$b;->k()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lf/g/j/q/d;->s(Ljava/util/List;)V

    return-void
.end method
