.class public Lf/g/j/q/l0$a;
.super Ljava/lang/Object;
.source "NetworkFetchProducer.java"

# interfaces
.implements Lf/g/j/q/m0$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/l0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/q/w;

.field public final synthetic b:Lf/g/j/q/l0;


# direct methods
.method public constructor <init>(Lf/g/j/q/l0;Lf/g/j/q/w;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/l0$a;->b:Lf/g/j/q/l0;

    iput-object p2, p0, Lf/g/j/q/l0$a;->a:Lf/g/j/q/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)V
    .locals 5

    iget-object v0, p0, Lf/g/j/q/l0$a;->b:Lf/g/j/q/l0;

    iget-object v1, p0, Lf/g/j/q/l0$a;->a:Lf/g/j/q/w;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lf/g/j/q/w;->a()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v2, v1, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const-string v3, "NetworkFetchProducer"

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, p1, v4}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    invoke-virtual {v1}, Lf/g/j/q/w;->a()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v2, v1, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object v0, v1, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const-string v2, "network"

    invoke-interface {v0, v2}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    iget-object v0, v1, Lf/g/j/q/w;->a:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public b(Ljava/io/InputStream;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/q/l0$a;->b:Lf/g/j/q/l0;

    iget-object v1, p0, Lf/g/j/q/l0$a;->a:Lf/g/j/q/w;

    if-lez p2, :cond_0

    iget-object v2, v0, Lf/g/j/q/l0;->a:Lf/g/d/g/g;

    invoke-interface {v2, p2}, Lf/g/d/g/g;->e(I)Lf/g/d/g/i;

    move-result-object v2

    goto :goto_0

    :cond_0
    iget-object v2, v0, Lf/g/j/q/l0;->a:Lf/g/d/g/g;

    invoke-interface {v2}, Lf/g/d/g/g;->a()Lf/g/d/g/i;

    move-result-object v2

    :goto_0
    iget-object v3, v0, Lf/g/j/q/l0;->b:Lf/g/d/g/a;

    const/16 v4, 0x4000

    invoke-interface {v3, v4}, Lf/g/d/g/e;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    :cond_1
    :goto_1
    :try_start_0
    invoke-virtual {p1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_3

    if-lez v4, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual {v0, v2, v1}, Lf/g/j/q/l0;->d(Lf/g/d/g/i;Lf/g/j/q/w;)V

    move-object v4, v2

    check-cast v4, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;

    iget v4, v4, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;->f:I

    if-lez p2, :cond_2

    int-to-float v4, v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    goto :goto_2

    :cond_2
    const/high16 v5, 0x3f800000    # 1.0f

    neg-int v4, v4

    int-to-double v6, v4

    const-wide v8, 0x40e86a0000000000L    # 50000.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    double-to-float v4, v6

    sub-float v4, v5, v4

    :goto_2
    iget-object v5, v1, Lf/g/j/q/w;->a:Lf/g/j/q/l;

    invoke-interface {v5, v4}, Lf/g/j/q/l;->a(F)V

    goto :goto_1

    :cond_3
    iget-object p1, v0, Lf/g/j/q/l0;->c:Lf/g/j/q/m0;

    move-object p2, v2

    check-cast p2, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;

    iget p2, p2, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;->f:I

    check-cast p1, Lf/g/j/q/z;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object p2, v1

    check-cast p2, Lf/g/j/q/z$a;

    iget-object p1, p1, Lf/g/j/q/z;->c:Lf/g/d/k/b;

    invoke-interface {p1}, Lf/g/d/k/b;->now()J

    move-result-wide v4

    iput-wide v4, p2, Lf/g/j/q/z$a;->f:J

    invoke-virtual {v0, v2, v1}, Lf/g/j/q/l0;->c(Lf/g/d/g/i;Lf/g/j/q/w;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, v0, Lf/g/j/q/l0;->b:Lf/g/d/g/a;

    invoke-interface {p1, v3}, Lf/g/d/g/e;->release(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lf/g/d/g/i;->close()V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    iget-object p2, v0, Lf/g/j/q/l0;->b:Lf/g/d/g/a;

    invoke-interface {p2, v3}, Lf/g/d/g/e;->release(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lf/g/d/g/i;->close()V

    throw p1
.end method
