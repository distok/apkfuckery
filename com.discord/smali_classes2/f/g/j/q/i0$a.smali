.class public Lf/g/j/q/i0$a;
.super Lf/g/j/q/c1;
.source "LocalVideoThumbnailProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/i0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/c1<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic i:Lf/g/j/q/x0;

.field public final synthetic j:Lf/g/j/q/v0;

.field public final synthetic k:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public final synthetic l:Lf/g/j/q/i0;


# direct methods
.method public constructor <init>(Lf/g/j/q/i0;Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;Lf/g/j/q/x0;Lf/g/j/q/v0;Lcom/facebook/imagepipeline/request/ImageRequest;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/i0$a;->l:Lf/g/j/q/i0;

    iput-object p6, p0, Lf/g/j/q/i0$a;->i:Lf/g/j/q/x0;

    iput-object p7, p0, Lf/g/j/q/i0$a;->j:Lf/g/j/q/v0;

    iput-object p8, p0, Lf/g/j/q/i0$a;->k:Lcom/facebook/imagepipeline/request/ImageRequest;

    invoke-direct {p0, p2, p3, p4, p5}, Lf/g/j/q/c1;-><init>(Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-void
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Map;
    .locals 1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    const-string v0, "createdThumbnail"

    invoke-static {v0, p1}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public d()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lf/g/j/q/i0$a;->l:Lf/g/j/q/i0;

    iget-object v2, p0, Lf/g/j/q/i0$a;->k:Lcom/facebook/imagepipeline/request/ImageRequest;

    invoke-static {v1, v2}, Lf/g/j/q/i0;->c(Lf/g/j/q/i0;Lcom/facebook/imagepipeline/request/ImageRequest;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_4

    iget-object v2, p0, Lf/g/j/q/i0$a;->k:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v2, v2, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    const/16 v3, 0x800

    if-eqz v2, :cond_0

    iget v4, v2, Lf/g/j/d/e;->a:I

    goto :goto_1

    :cond_0
    const/16 v4, 0x800

    :goto_1
    const/16 v5, 0x60

    if-gt v4, v5, :cond_3

    if-eqz v2, :cond_1

    iget v3, v2, Lf/g/j/d/e;->b:I

    :cond_1
    if-le v3, v5, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x3

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v2, 0x1

    :goto_3
    invoke-static {v1, v2}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_4

    :cond_4
    iget-object v1, p0, Lf/g/j/q/i0$a;->l:Lf/g/j/q/i0;

    iget-object v1, v1, Lf/g/j/q/i0;->b:Landroid/content/ContentResolver;

    iget-object v2, p0, Lf/g/j/q/i0$a;->k:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v2, v2, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    :try_start_1
    const-string v3, "r"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-object v1, v0

    :goto_4
    if-nez v1, :cond_5

    goto :goto_5

    :cond_5
    new-instance v0, Lf/g/j/j/d;

    invoke-static {}, Lf/g/j/b/b;->a()Lf/g/j/b/b;

    move-result-object v2

    sget-object v3, Lf/g/j/j/h;->d:Lf/g/j/j/i;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lf/g/j/j/d;-><init>(Landroid/graphics/Bitmap;Lf/g/d/h/f;Lf/g/j/j/i;I)V

    iget-object v1, p0, Lf/g/j/q/i0$a;->j:Lf/g/j/q/v0;

    const-string v2, "image_format"

    const-string v3, "thumbnail"

    invoke-interface {v1, v2, v3}, Lf/g/j/q/v0;->d(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lf/g/j/q/i0$a;->j:Lf/g/j/q/v0;

    invoke-interface {v1}, Lf/g/j/q/v0;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/g/j/j/c;->e(Ljava/util/Map;)V

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    :goto_5
    return-object v0
.end method

.method public f(Ljava/lang/Exception;)V
    .locals 3

    invoke-super {p0, p1}, Lf/g/j/q/c1;->f(Ljava/lang/Exception;)V

    iget-object p1, p0, Lf/g/j/q/i0$a;->i:Lf/g/j/q/x0;

    iget-object v0, p0, Lf/g/j/q/i0$a;->j:Lf/g/j/q/v0;

    const-string v1, "VideoThumbnailProducer"

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object p1, p0, Lf/g/j/q/i0$a;->j:Lf/g/j/q/v0;

    const-string v0, "local"

    invoke-interface {p1, v0}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    return-void
.end method

.method public g(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    invoke-super {p0, p1}, Lf/g/j/q/c1;->g(Ljava/lang/Object;)V

    iget-object v0, p0, Lf/g/j/q/i0$a;->i:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/i0$a;->j:Lf/g/j/q/v0;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string v2, "VideoThumbnailProducer"

    invoke-interface {v0, v1, v2, p1}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object p1, p0, Lf/g/j/q/i0$a;->j:Lf/g/j/q/v0;

    const-string v0, "local"

    invoke-interface {p1, v0}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    return-void
.end method
