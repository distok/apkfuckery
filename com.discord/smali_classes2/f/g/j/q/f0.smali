.class public abstract Lf/g/j/q/f0;
.super Ljava/lang/Object;
.source "LocalFetchProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Lf/g/d/g/g;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/f0;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lf/g/j/q/f0;->b:Lf/g/d/g/g;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v7

    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v6

    const-string v0, "local"

    const-string v1, "fetch"

    invoke-interface {p2, v0, v1}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v9, Lf/g/j/q/f0$a;

    invoke-virtual {p0}, Lf/g/j/q/f0;->e()Ljava/lang/String;

    move-result-object v5

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, v7

    move-object v4, p2

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lf/g/j/q/f0$a;-><init>(Lf/g/j/q/f0;Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;Lcom/facebook/imagepipeline/request/ImageRequest;Lf/g/j/q/x0;Lf/g/j/q/v0;)V

    new-instance p1, Lf/g/j/q/f0$b;

    invoke-direct {p1, p0, v9}, Lf/g/j/q/f0$b;-><init>(Lf/g/j/q/f0;Lf/g/j/q/c1;)V

    invoke-interface {p2, p1}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    iget-object p1, p0, Lf/g/j/q/f0;->a:Ljava/util/concurrent/Executor;

    invoke-interface {p1, v9}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c(Ljava/io/InputStream;I)Lf/g/j/j/e;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    if-gtz p2, :cond_0

    :try_start_0
    iget-object p2, p0, Lf/g/j/q/f0;->b:Lf/g/d/g/g;

    invoke-interface {p2, p1}, Lf/g/d/g/g;->c(Ljava/io/InputStream;)Lcom/facebook/common/memory/PooledByteBuffer;

    move-result-object p2

    invoke-static {p2}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/g/j/q/f0;->b:Lf/g/d/g/g;

    invoke-interface {v1, p1, p2}, Lf/g/d/g/g;->d(Ljava/io/InputStream;I)Lcom/facebook/common/memory/PooledByteBuffer;

    move-result-object p2

    invoke-static {p2}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p2

    :goto_0
    move-object v0, p2

    new-instance p2, Lf/g/j/j/e;

    invoke-direct {p2, v0}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p1}, Lf/g/d/d/a;->b(Ljava/io/InputStream;)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    return-object p2

    :catchall_0
    move-exception p2

    invoke-static {p1}, Lf/g/d/d/a;->b(Ljava/io/InputStream;)V

    sget-object p1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    throw p2
.end method

.method public abstract d(Lcom/facebook/imagepipeline/request/ImageRequest;)Lf/g/j/j/e;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
.end method
