.class public abstract Lf/g/j/q/n$c;
.super Lf/g/j/q/o;
.source "DecodeProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/q/v0;

.field public final d:Lf/g/j/q/x0;

.field public final e:Lf/g/j/d/b;

.field public f:Z

.field public final g:Lf/g/j/q/b0;

.field public final synthetic h:Lf/g/j/q/n;


# direct methods
.method public constructor <init>(Lf/g/j/q/n;Lf/g/j/q/l;Lf/g/j/q/v0;ZI)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            "ZI)V"
        }
    .end annotation

    iput-object p1, p0, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p3, p0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-interface {p3}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object p2

    iput-object p2, p0, Lf/g/j/q/n$c;->d:Lf/g/j/q/x0;

    invoke-interface {p3}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object p2

    iget-object p2, p2, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    iput-object p2, p0, Lf/g/j/q/n$c;->e:Lf/g/j/d/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/j/q/n$c;->f:Z

    new-instance v0, Lf/g/j/q/n$c$a;

    invoke-direct {v0, p0, p1, p3, p5}, Lf/g/j/q/n$c$a;-><init>(Lf/g/j/q/n$c;Lf/g/j/q/n;Lf/g/j/q/v0;I)V

    new-instance p5, Lf/g/j/q/b0;

    iget-object v1, p1, Lf/g/j/q/n;->b:Ljava/util/concurrent/Executor;

    iget p2, p2, Lf/g/j/d/b;->a:I

    invoke-direct {p5, v1, v0, p2}, Lf/g/j/q/b0;-><init>(Ljava/util/concurrent/Executor;Lf/g/j/q/b0$c;I)V

    iput-object p5, p0, Lf/g/j/q/n$c;->g:Lf/g/j/q/b0;

    new-instance p2, Lf/g/j/q/n$c$b;

    invoke-direct {p2, p0, p1, p4}, Lf/g/j/q/n$c$b;-><init>(Lf/g/j/q/n$c;Lf/g/j/q/n;Z)V

    invoke-interface {p3, p2}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    return-void
.end method


# virtual methods
.method public g()V
    .locals 0

    invoke-virtual {p0}, Lf/g/j/q/n$c;->q()V

    return-void
.end method

.method public h(Ljava/lang/Throwable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lf/g/j/q/n$c;->r(Ljava/lang/Throwable;)V

    return-void
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lf/g/j/j/e;

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-nez p1, :cond_0

    new-instance p1, Lcom/facebook/common/util/ExceptionWithNoStacktrace;

    const-string p2, "Encoded image is null."

    invoke-direct {p1, p2}, Lcom/facebook/common/util/ExceptionWithNoStacktrace;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lf/g/j/q/n$c;->u(Z)V

    iget-object p2, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p2, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lf/g/j/j/e;->i()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance p1, Lcom/facebook/common/util/ExceptionWithNoStacktrace;

    const-string p2, "Encoded image is not valid."

    invoke-direct {p1, p2}, Lcom/facebook/common/util/ExceptionWithNoStacktrace;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lf/g/j/q/n$c;->u(Z)V

    iget-object p2, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p2, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lf/g/j/q/n$c;->w(Lf/g/j/j/e;I)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_3

    :cond_2
    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    goto :goto_1

    :cond_3
    const/4 p1, 0x4

    :try_start_1
    invoke-static {p2, p1}, Lf/g/j/q/b;->m(II)Z

    move-result p1

    if-nez v0, :cond_4

    if-nez p1, :cond_4

    iget-object p1, p0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-interface {p1}, Lf/g/j/q/v0;->p()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_4
    iget-object p1, p0, Lf/g/j/q/n$c;->g:Lf/g/j/q/b0;

    invoke-virtual {p1}, Lf/g/j/q/b0;->d()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public j(F)V
    .locals 1

    const v0, 0x3f7d70a4    # 0.99f

    mul-float p1, p1, v0

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->a(F)V

    return-void
.end method

.method public final n(Lf/g/j/j/c;JLf/g/j/j/i;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/j/c;",
            "J",
            "Lf/g/j/j/i;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    iget-object v6, v0, Lf/g/j/q/n$c;->d:Lf/g/j/q/x0;

    iget-object v7, v0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    const-string v8, "DecodeProducer"

    invoke-interface {v6, v7, v8}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v1, 0x0

    return-object v1

    :cond_0
    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v7, p4

    check-cast v7, Lf/g/j/j/h;

    iget-boolean v7, v7, Lf/g/j/j/h;->b:Z

    invoke-static {v7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    instance-of v9, v1, Lf/g/j/j/d;

    const-string v10, "sampleSize"

    const-string v11, "requestedImageSize"

    const-string v12, "imageFormat"

    const-string v13, "encodedImageSize"

    const-string v14, "isFinal"

    const-string v15, "hasGoodQuality"

    const-string v0, "queueTime"

    if-eqz v9, :cond_1

    check-cast v1, Lf/g/j/j/d;

    iget-object v1, v1, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v5, "x"

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/util/HashMap;

    move-object/from16 p1, v1

    const/16 v1, 0x8

    invoke-direct {v9, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "bitmapSize"

    invoke-virtual {v9, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v9, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v9, v15, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v9, v14, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v9, v13, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v9, v12, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v9, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, p9

    invoke-virtual {v9, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "byteCount"

    invoke-virtual {v9, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/g/d/d/f;

    invoke-direct {v0, v9}, Lf/g/d/d/f;-><init>(Ljava/util/Map;)V

    return-object v0

    :cond_1
    move-object v1, v5

    new-instance v5, Ljava/util/HashMap;

    const/4 v9, 0x7

    invoke-direct {v5, v9}, Ljava/util/HashMap;-><init>(I)V

    invoke-virtual {v5, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v15, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v14, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v13, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v12, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/g/d/d/f;

    invoke-direct {v0, v5}, Lf/g/d/d/f;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public abstract o(Lf/g/j/j/e;)I
.end method

.method public abstract p()Lf/g/j/j/i;
.end method

.method public final q()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lf/g/j/q/n$c;->u(Z)V

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    return-void
.end method

.method public final r(Ljava/lang/Throwable;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lf/g/j/q/n$c;->u(Z)V

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final s(Lf/g/j/j/c;I)V
    .locals 4

    iget-object v0, p0, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    iget-object v0, v0, Lf/g/j/q/n;->j:Lf/g/j/e/b;

    iget-object v0, v0, Lf/g/j/e/b;->a:Lcom/facebook/common/references/CloseableReference$c;

    sget-object v1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/facebook/common/references/CloseableReference;->j:Lf/g/d/h/f;

    invoke-interface {v0}, Lcom/facebook/common/references/CloseableReference$c;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    :cond_1
    invoke-static {p1, v2, v0, v1}, Lcom/facebook/common/references/CloseableReference;->D(Ljava/lang/Object;Lf/g/d/h/f;Lcom/facebook/common/references/CloseableReference$c;Ljava/lang/Throwable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    :goto_0
    :try_start_0
    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result p1

    invoke-virtual {p0, p1}, Lf/g/j/q/n$c;->u(Z)V

    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p1, v1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    return-void

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_3
    throw p1
.end method

.method public final t(Lf/g/j/j/e;ILf/g/j/j/i;)Lf/g/j/j/c;
    .locals 3

    iget-object v0, p0, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    iget-object v1, v0, Lf/g/j/q/n;->k:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lf/g/j/q/n;->l:Lcom/facebook/common/internal/Supplier;

    invoke-interface {v0}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    iget-object v1, v1, Lf/g/j/q/n;->c:Lf/g/j/h/b;

    iget-object v2, p0, Lf/g/j/q/n$c;->e:Lf/g/j/d/b;

    invoke-interface {v1, p1, p2, p3, v2}, Lf/g/j/h/b;->a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    iget-object v0, v0, Lf/g/j/q/n;->k:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    invoke-static {}, Ljava/lang/System;->gc()V

    iget-object v0, p0, Lf/g/j/q/n$c;->h:Lf/g/j/q/n;

    iget-object v0, v0, Lf/g/j/q/n;->c:Lf/g/j/h/b;

    iget-object v1, p0, Lf/g/j/q/n$c;->e:Lf/g/j/d/b;

    invoke-interface {v0, p1, p2, p3, v1}, Lf/g/j/h/b;->a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_1
    throw v1
.end method

.method public final u(Z)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    iget-boolean p1, p0, Lf/g/j/q/n$c;->f:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p1, v0}, Lf/g/j/q/l;->a(F)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/g/j/q/n$c;->f:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lf/g/j/q/n$c;->g:Lf/g/j/q/b0;

    invoke-virtual {p1}, Lf/g/j/q/b0;->a()V

    return-void

    :cond_1
    :goto_0
    :try_start_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final v(Lf/g/j/j/e;Lf/g/j/j/c;)V
    .locals 3

    iget-object v0, p0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v1, p1, Lf/g/j/j/e;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "encoded_width"

    invoke-interface {v0, v2, v1}, Lf/g/j/q/v0;->d(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v1, p1, Lf/g/j/j/e;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "encoded_height"

    invoke-interface {v0, v2, v1}, Lf/g/j/q/v0;->d(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-virtual {p1}, Lf/g/j/j/e;->f()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "encoded_size"

    invoke-interface {v0, v1, p1}, Lf/g/j/q/v0;->d(Ljava/lang/String;Ljava/lang/Object;)V

    instance-of p1, p2, Lf/g/j/j/b;

    if-eqz p1, :cond_1

    move-object p1, p2

    check-cast p1, Lf/g/j/j/b;

    invoke-virtual {p1}, Lf/g/j/j/b;->f()Landroid/graphics/Bitmap;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object p1

    :goto_0
    iget-object v0, p0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "bitmap_config"

    invoke-interface {v0, v1, p1}, Lf/g/j/q/v0;->d(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    if-eqz p2, :cond_2

    iget-object p1, p0, Lf/g/j/q/n$c;->c:Lf/g/j/q/v0;

    invoke-interface {p1}, Lf/g/j/q/v0;->a()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p2, p1}, Lf/g/j/j/c;->e(Ljava/util/Map;)V

    :cond_2
    return-void
.end method

.method public abstract w(Lf/g/j/j/e;I)Z
.end method
