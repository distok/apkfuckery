.class public Lf/g/j/q/n0;
.super Ljava/lang/Object;
.source "PartialDiskCacheProducer.java"

# interfaces
.implements Lu/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu/c<",
        "Lf/g/j/j/e;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/q/x0;

.field public final synthetic b:Lf/g/j/q/v0;

.field public final synthetic c:Lf/g/j/q/l;

.field public final synthetic d:Lcom/facebook/cache/common/CacheKey;

.field public final synthetic e:Lf/g/j/q/p0;


# direct methods
.method public constructor <init>(Lf/g/j/q/p0;Lf/g/j/q/x0;Lf/g/j/q/v0;Lf/g/j/q/l;Lcom/facebook/cache/common/CacheKey;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/n0;->e:Lf/g/j/q/p0;

    iput-object p2, p0, Lf/g/j/q/n0;->a:Lf/g/j/q/x0;

    iput-object p3, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    iput-object p4, p0, Lf/g/j/q/n0;->c:Lf/g/j/q/l;

    iput-object p5, p0, Lf/g/j/q/n0;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lu/g;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p1, Lu/g;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p1, Lu/g;->c:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lu/g;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lu/g;->d()Ljava/lang/Exception;

    move-result-object v1

    instance-of v1, v1, Ljava/util/concurrent/CancellationException;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    const/4 v3, 0x0

    if-eqz v1, :cond_2

    iget-object p1, p0, Lf/g/j/q/n0;->a:Lf/g/j/q/x0;

    iget-object v0, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    const-string v1, "PartialDiskCacheProducer"

    invoke-interface {p1, v0, v1, v3}, Lf/g/j/q/x0;->d(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object p1, p0, Lf/g/j/q/n0;->c:Lf/g/j/q/l;

    invoke-interface {p1}, Lf/g/j/q/l;->d()V

    goto/16 :goto_5

    :cond_2
    invoke-virtual {p1}, Lu/g;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lf/g/j/q/n0;->a:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    const-string v2, "PartialDiskCacheProducer"

    invoke-virtual {p1}, Lu/g;->d()Ljava/lang/Exception;

    move-result-object p1

    invoke-interface {v0, v1, v2, p1, v3}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    iget-object p1, p0, Lf/g/j/q/n0;->e:Lf/g/j/q/p0;

    iget-object v0, p0, Lf/g/j/q/n0;->c:Lf/g/j/q/l;

    iget-object v1, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    iget-object v2, p0, Lf/g/j/q/n0;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-static {p1, v0, v1, v2, v3}, Lf/g/j/q/p0;->c(Lf/g/j/q/p0;Lf/g/j/q/l;Lf/g/j/q/v0;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    goto/16 :goto_5

    :cond_3
    iget-object v1, p1, Lu/g;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object p1, p1, Lu/g;->d:Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    check-cast p1, Lf/g/j/j/e;

    if-eqz p1, :cond_9

    iget-object v1, p0, Lf/g/j/q/n0;->a:Lf/g/j/q/x0;

    iget-object v4, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    const-string v5, "PartialDiskCacheProducer"

    invoke-virtual {p1}, Lf/g/j/j/e;->f()I

    move-result v6

    invoke-static {v1, v4, v2, v6}, Lf/g/j/q/p0;->d(Lf/g/j/q/x0;Lf/g/j/q/v0;ZI)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v1, v4, v5, v6}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p1}, Lf/g/j/j/e;->f()I

    move-result v1

    sub-int/2addr v1, v2

    if-lez v1, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    :goto_2
    invoke-static {v4}, Ls/a/b/b/a;->g(Z)V

    new-instance v4, Lf/g/j/d/a;

    invoke-direct {v4, v0, v1}, Lf/g/j/d/a;-><init>(II)V

    iput-object v4, p1, Lf/g/j/j/e;->m:Lf/g/j/d/a;

    invoke-virtual {p1}, Lf/g/j/j/e;->f()I

    move-result v1

    iget-object v5, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    invoke-interface {v5}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v5

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->j:Lf/g/j/d/a;

    if-nez v6, :cond_5

    goto :goto_3

    :cond_5
    iget v7, v4, Lf/g/j/d/a;->a:I

    iget v8, v6, Lf/g/j/d/a;->a:I

    if-gt v7, v8, :cond_6

    iget v4, v4, Lf/g/j/d/a;->b:I

    iget v6, v6, Lf/g/j/d/a;->b:I

    if-lt v4, v6, :cond_6

    const/4 v4, 0x1

    goto :goto_4

    :cond_6
    :goto_3
    const/4 v4, 0x0

    :goto_4
    if-eqz v4, :cond_7

    iget-object v0, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    const-string v1, "disk"

    const-string v4, "partial"

    invoke-interface {v0, v1, v4}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/g/j/q/n0;->a:Lf/g/j/q/x0;

    iget-object v1, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    const-string v4, "PartialDiskCacheProducer"

    invoke-interface {v0, v1, v4, v2}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    iget-object v0, p0, Lf/g/j/q/n0;->c:Lf/g/j/q/l;

    const/16 v1, 0x9

    invoke-interface {v0, p1, v1}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto/16 :goto_5

    :cond_7
    iget-object v4, p0, Lf/g/j/q/n0;->c:Lf/g/j/q/l;

    const/16 v6, 0x8

    invoke-interface {v4, p1, v6}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    iget-object v4, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-static {v4}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b(Landroid/net/Uri;)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object v4

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->e:Lf/g/j/d/b;

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->j:Lf/g/j/d/a;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->o:Lf/g/j/d/a;

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->f:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    iget-boolean v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->f:Z

    iput-boolean v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->h:Z

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->j:Lf/g/j/r/b;

    iget-boolean v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->e:Z

    iput-boolean v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->g:Z

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->k:Lf/g/j/d/d;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->i:Lf/g/j/d/d;

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->c:Lf/g/j/d/e;

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->q:Lf/g/j/l/e;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->n:Lf/g/j/l/e;

    iget-object v6, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iput-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->d:Lf/g/j/d/f;

    iget-object v5, v5, Lcom/facebook/imagepipeline/request/ImageRequest;->o:Ljava/lang/Boolean;

    iput-object v5, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->m:Ljava/lang/Boolean;

    sub-int/2addr v1, v2

    if-ltz v1, :cond_8

    const/4 v0, 0x1

    :cond_8
    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    new-instance v0, Lf/g/j/d/a;

    const v2, 0x7fffffff

    invoke-direct {v0, v1, v2}, Lf/g/j/d/a;-><init>(II)V

    iput-object v0, v4, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->o:Lf/g/j/d/a;

    invoke-virtual {v4}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    new-instance v1, Lf/g/j/q/b1;

    iget-object v2, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    invoke-direct {v1, v0, v2}, Lf/g/j/q/b1;-><init>(Lcom/facebook/imagepipeline/request/ImageRequest;Lf/g/j/q/v0;)V

    iget-object v0, p0, Lf/g/j/q/n0;->e:Lf/g/j/q/p0;

    iget-object v2, p0, Lf/g/j/q/n0;->c:Lf/g/j/q/l;

    iget-object v4, p0, Lf/g/j/q/n0;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-static {v0, v2, v1, v4, p1}, Lf/g/j/q/p0;->c(Lf/g/j/q/p0;Lf/g/j/q/l;Lf/g/j/q/v0;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    goto :goto_5

    :cond_9
    iget-object v1, p0, Lf/g/j/q/n0;->a:Lf/g/j/q/x0;

    iget-object v2, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    const-string v4, "PartialDiskCacheProducer"

    invoke-static {v1, v2, v0, v0}, Lf/g/j/q/p0;->d(Lf/g/j/q/x0;Lf/g/j/q/v0;ZI)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v1, v2, v4, v0}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lf/g/j/q/n0;->e:Lf/g/j/q/p0;

    iget-object v1, p0, Lf/g/j/q/n0;->c:Lf/g/j/q/l;

    iget-object v2, p0, Lf/g/j/q/n0;->b:Lf/g/j/q/v0;

    iget-object v4, p0, Lf/g/j/q/n0;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-static {v0, v1, v2, v4, p1}, Lf/g/j/q/p0;->c(Lf/g/j/q/p0;Lf/g/j/q/l;Lf/g/j/q/v0;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    :goto_5
    return-object v3

    :catchall_0
    move-exception p1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method
