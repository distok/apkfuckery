.class public Lf/g/j/q/p0$a;
.super Lf/g/j/q/o;
.source "PartialDiskCacheProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/p0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/c/g;

.field public final d:Lcom/facebook/cache/common/CacheKey;

.field public final e:Lf/g/d/g/g;

.field public final f:Lf/g/d/g/a;

.field public final g:Lf/g/j/j/e;


# direct methods
.method public constructor <init>(Lf/g/j/q/l;Lf/g/j/c/g;Lcom/facebook/cache/common/CacheKey;Lf/g/d/g/g;Lf/g/d/g/a;Lf/g/j/j/e;Lf/g/j/q/n0;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p2, p0, Lf/g/j/q/p0$a;->c:Lf/g/j/c/g;

    iput-object p3, p0, Lf/g/j/q/p0$a;->d:Lcom/facebook/cache/common/CacheKey;

    iput-object p4, p0, Lf/g/j/q/p0$a;->e:Lf/g/d/g/g;

    iput-object p5, p0, Lf/g/j/q/p0$a;->f:Lf/g/d/g/a;

    iput-object p6, p0, Lf/g/j/q/p0$a;->g:Lf/g/j/j/e;

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lf/g/j/j/e;

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v0, p0, Lf/g/j/q/p0$a;->g:Lf/g/j/j/e;

    if-eqz v0, :cond_1

    iget-object v1, p1, Lf/g/j/j/e;->m:Lf/g/j/d/a;

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lf/g/j/q/p0$a;->o(Lf/g/j/j/e;Lf/g/j/j/e;)Lf/g/d/g/i;

    move-result-object p2

    invoke-virtual {p0, p2}, Lf/g/j/q/p0$a;->p(Lf/g/d/g/i;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p2

    goto :goto_1

    :catch_0
    move-exception p2

    :try_start_1
    const-string v0, "PartialDiskCacheProducer"

    const-string v1, "Error while merging image data"

    invoke-static {v0, v1, p2}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p2}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    iget-object p1, p0, Lf/g/j/q/p0$a;->g:Lf/g/j/j/e;

    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    iget-object p1, p0, Lf/g/j/q/p0$a;->c:Lf/g/j/c/g;

    iget-object p2, p0, Lf/g/j/q/p0$a;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    invoke-virtual {v0, p2}, Lf/g/j/c/x;->c(Lcom/facebook/cache/common/CacheKey;)Z

    const/4 v0, 0x0

    :try_start_2
    new-instance v1, Lf/g/j/c/h;

    invoke-direct {v1, p1, v0, p2}, Lf/g/j/c/h;-><init>(Lf/g/j/c/g;Ljava/lang/Object;Lcom/facebook/cache/common/CacheKey;)V

    iget-object p1, p1, Lf/g/j/c/g;->e:Ljava/util/concurrent/Executor;

    invoke-static {v1, p1}, Lu/g;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)Lu/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    const-class v0, Lf/g/j/c/g;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p2}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, v2

    const-string p2, "Failed to schedule disk-cache remove for %s"

    invoke-static {v0, p1, p2, v1}, Lf/g/d/e/a;->n(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lu/g;->c(Ljava/lang/Exception;)Lu/g;

    goto :goto_2

    :goto_1
    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    iget-object p1, p0, Lf/g/j/q/p0$a;->g:Lf/g/j/j/e;

    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    throw p2

    :cond_1
    const/16 v0, 0x8

    invoke-static {p2, v0}, Lf/g/j/q/b;->m(II)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v0, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v1, Lf/g/i/c;->b:Lf/g/i/c;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lf/g/j/q/p0$a;->c:Lf/g/j/c/g;

    iget-object v1, p0, Lf/g/j/q/p0$a;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-virtual {v0, v1, p1}, Lf/g/j/c/g;->f(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    :goto_2
    return-void
.end method

.method public final n(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/q/p0$a;->f:Lf/g/d/g/a;

    const/16 v1, 0x4000

    invoke-interface {v0, v1}, Lf/g/d/g/e;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move v2, p3

    :cond_0
    :goto_0
    const/4 v3, 0x0

    if-lez v2, :cond_1

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {p1, v0, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    if-ltz v4, :cond_1

    if-lez v4, :cond_0

    invoke-virtual {p2, v0, v3, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int/2addr v2, v4

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lf/g/j/q/p0$a;->f:Lf/g/d/g/a;

    invoke-interface {p2, v0}, Lf/g/d/g/e;->release(Ljava/lang/Object;)V

    throw p1

    :cond_1
    iget-object p1, p0, Lf/g/j/q/p0$a;->f:Lf/g/d/g/a;

    invoke-interface {p1, v0}, Lf/g/d/g/e;->release(Ljava/lang/Object;)V

    if-gtz v2, :cond_2

    return-void

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const/4 p2, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v0, v3

    const/4 p3, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, p3

    const-string p3, "Failed to read %d bytes - finished %d short"

    invoke-static {p2, p3, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final o(Lf/g/j/j/e;Lf/g/j/j/e;)Lf/g/d/g/i;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p2}, Lf/g/j/j/e;->f()I

    move-result v0

    iget-object v1, p2, Lf/g/j/j/e;->m:Lf/g/j/d/a;

    iget v1, v1, Lf/g/j/d/a;->a:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lf/g/j/q/p0$a;->e:Lf/g/d/g/g;

    invoke-interface {v1, v0}, Lf/g/d/g/g;->e(I)Lf/g/d/g/i;

    move-result-object v0

    iget-object v1, p2, Lf/g/j/j/e;->m:Lf/g/j/d/a;

    iget v1, v1, Lf/g/j/d/a;->a:I

    invoke-virtual {p1}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {p0, p1, v0, v1}, Lf/g/j/q/p0$a;->n(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    invoke-virtual {p2}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {p2}, Lf/g/j/j/e;->f()I

    move-result p2

    invoke-virtual {p0, p1, v0, p2}, Lf/g/j/q/p0$a;->n(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    return-object v0
.end method

.method public final p(Lf/g/d/g/i;)V
    .locals 4

    check-cast p1, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;

    invoke-virtual {p1}, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;->b()Lf/g/j/m/t;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lf/g/j/j/e;

    invoke-direct {v1, p1}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lf/g/j/j/e;->n()V

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lf/g/j/j/e;->close()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lf/g/j/j/e;->close()V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    throw v0
.end method
