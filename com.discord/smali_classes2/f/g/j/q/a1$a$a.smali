.class public Lf/g/j/q/a1$a$a;
.super Ljava/lang/Object;
.source "ResizeAndRotateProducer.java"

# interfaces
.implements Lf/g/j/q/b0$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/q/a1$a;-><init>(Lf/g/j/q/a1;Lf/g/j/q/l;Lf/g/j/q/v0;ZLf/g/j/t/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/q/a1$a;


# direct methods
.method public constructor <init>(Lf/g/j/q/a1$a;Lf/g/j/q/a1;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/q/a1$a$a;->a:Lf/g/j/q/a1$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/j/e;I)V
    .locals 13

    iget-object v0, p0, Lf/g/j/q/a1$a$a;->a:Lf/g/j/q/a1$a;

    iget-object v1, v0, Lf/g/j/q/a1$a;->d:Lf/g/j/t/c;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v2, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    iget-object v3, p0, Lf/g/j/q/a1$a$a;->a:Lf/g/j/q/a1$a;

    iget-boolean v3, v3, Lf/g/j/q/a1$a;->c:Z

    invoke-interface {v1, v2, v3}, Lf/g/j/t/c;->createImageTranscoder(Lf/g/i/c;Z)Lf/g/j/t/b;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v2

    iget-object v3, v0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    const-string v11, "ResizeAndRotateProducer"

    invoke-interface {v2, v3, v11}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    iget-object v2, v0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v2

    iget-object v3, v0, Lf/g/j/q/a1$a;->h:Lf/g/j/q/a1;

    iget-object v3, v3, Lf/g/j/q/a1;->b:Lf/g/d/g/g;

    invoke-interface {v3}, Lf/g/d/g/g;->a()Lf/g/d/g/i;

    move-result-object v3

    const/4 v12, 0x0

    :try_start_0
    iget-object v7, v2, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-object v8, v2, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    const/4 v9, 0x0

    const/16 v4, 0x55

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object v4, v1

    move-object v5, p1

    move-object v6, v3

    invoke-interface/range {v4 .. v10}, Lf/g/j/t/b;->c(Lf/g/j/j/e;Ljava/io/OutputStream;Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/i/c;Ljava/lang/Integer;)Lf/g/j/t/a;

    move-result-object v4

    iget v5, v4, Lf/g/j/t/a;->a:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_2

    iget-object v2, v2, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    invoke-interface {v1}, Lf/g/j/t/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v2, v4, v1}, Lf/g/j/q/a1$a;->n(Lf/g/j/j/e;Lf/g/j/d/e;Lf/g/j/t/a;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v12

    move-object p1, v3

    check-cast p1, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;

    invoke-virtual {p1}, Lcom/facebook/imagepipeline/memory/MemoryPooledByteBufferOutputStream;->b()Lf/g/j/m/t;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v1, Lf/g/j/j/e;

    invoke-direct {v1, p1}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V

    sget-object v2, Lf/g/i/b;->a:Lf/g/i/c;

    iput-object v2, v1, Lf/g/j/j/e;->f:Lf/g/i/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1}, Lf/g/j/j/e;->n()V

    iget-object v2, v0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v2

    iget-object v5, v0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v2, v5, v11, v12}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget v2, v4, Lf/g/j/t/a;->a:I

    const/4 v4, 0x1

    if-eq v2, v4, :cond_0

    or-int/lit8 p2, p2, 0x10

    :cond_0
    iget-object v2, v0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v2, v1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v1}, Lf/g/j/j/e;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz p1, :cond_3

    :try_start_4
    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_5
    invoke-virtual {v1}, Lf/g/j/j/e;->close()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v1

    if-eqz p1, :cond_1

    :try_start_6
    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    throw v1

    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v1, "Error while transcoding the image"

    invoke-direct {p1, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_7
    iget-object v1, v0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v1}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v1

    iget-object v2, v0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v1, v2, v11, p1, v12}, Lf/g/j/q/x0;->k(Lf/g/j/q/v0;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result p2

    if-eqz p2, :cond_3

    iget-object p2, v0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {p2, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :cond_3
    :goto_0
    invoke-virtual {v3}, Lf/g/d/g/i;->close()V

    return-void

    :goto_1
    invoke-virtual {v3}, Lf/g/d/g/i;->close()V

    throw p1
.end method
