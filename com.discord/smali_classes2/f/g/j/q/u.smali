.class public Lf/g/j/q/u;
.super Ljava/lang/Object;
.source "EncodedMemoryCacheProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/u$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/c/j;

.field public final c:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;",
            "Lf/g/j/c/j;",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/u;->a:Lf/g/j/c/t;

    iput-object p2, p0, Lf/g/j/q/u;->b:Lf/g/j/c/j;

    iput-object p3, p0, Lf/g/j/q/u;->c:Lf/g/j/q/u0;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v8, p2

    const-string v9, "EncodedMemoryCacheProducer"

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-interface/range {p2 .. p2}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v10

    invoke-interface {v10, v8, v9}, Lf/g/j/q/x0;->e(Lf/g/j/q/v0;Ljava/lang/String;)V

    invoke-interface/range {p2 .. p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v2

    iget-object v3, v1, Lf/g/j/q/u;->b:Lf/g/j/c/j;

    invoke-interface/range {p2 .. p2}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    check-cast v3, Lf/g/j/c/n;

    :try_start_1
    invoke-virtual {v3, v2, v4}, Lf/g/j/c/n;->b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v5

    iget-object v2, v1, Lf/g/j/q/u;->a:Lf/g/j/c/t;

    invoke-interface {v2, v5}, Lf/g/j/c/t;->get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const-string v2, "memory_encoded"

    const/4 v3, 0x1

    const-string v12, "cached_value_found"

    const/4 v13, 0x0

    if-eqz v11, :cond_1

    :try_start_2
    new-instance v4, Lf/g/j/j/e;

    invoke-direct {v4, v11}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-interface {v10, v8, v9}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "true"

    invoke-static {v12, v5}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v13

    :cond_0
    invoke-interface {v10, v8, v9, v13}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    invoke-interface {v10, v8, v9, v3}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    invoke-interface {v8, v2}, Lf/g/j/q/v0;->n(Ljava/lang/String;)V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v2}, Lf/g/j/q/l;->a(F)V

    invoke-interface {v0, v4, v3}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v4}, Lf/g/j/j/e;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v11}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception v0

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    :try_start_6
    invoke-virtual {v4}, Lf/g/j/j/e;->close()V

    throw v0

    :cond_1
    invoke-interface/range {p2 .. p2}, Lf/g/j/q/v0;->q()Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v4

    sget-object v6, Lcom/facebook/imagepipeline/request/ImageRequest$c;->f:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    invoke-virtual {v6}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v14, "false"

    if-lt v4, v6, :cond_4

    :try_start_7
    invoke-interface {v10, v8, v9}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v12, v14}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v4

    goto :goto_0

    :cond_2
    move-object v4, v13

    :goto_0
    invoke-interface {v10, v8, v9, v4}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    const/4 v4, 0x0

    invoke-interface {v10, v8, v9, v4}, Lf/g/j/q/x0;->c(Lf/g/j/q/v0;Ljava/lang/String;Z)V

    const-string v4, "nil-result"

    invoke-interface {v8, v2, v4}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v13, v3}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v11, :cond_3

    invoke-virtual {v11}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :cond_3
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :cond_4
    :try_start_9
    invoke-interface/range {p2 .. p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v2

    iget-boolean v6, v2, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    new-instance v15, Lf/g/j/q/u$a;

    iget-object v4, v1, Lf/g/j/q/u;->a:Lf/g/j/c/t;

    invoke-interface/range {p2 .. p2}, Lf/g/j/q/v0;->g()Lf/g/j/e/k;

    move-result-object v2

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    iget-boolean v7, v2, Lf/g/j/e/l;->e:Z

    move-object v2, v15

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lf/g/j/q/u$a;-><init>(Lf/g/j/q/l;Lf/g/j/c/t;Lcom/facebook/cache/common/CacheKey;ZZ)V

    invoke-interface {v10, v8, v9}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v12, v14}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v13

    :cond_5
    invoke-interface {v10, v8, v9, v13}, Lf/g/j/q/x0;->j(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, v1, Lf/g/j/q/u;->c:Lf/g/j/q/u0;

    invoke-interface {v0, v15, v8}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v11, :cond_6

    invoke-virtual {v11}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :cond_6
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :goto_1
    :try_start_b
    sget-object v2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v11, :cond_7

    invoke-virtual {v11}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_7
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :catchall_2
    move-exception v0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw v0
.end method
