.class public Lf/g/j/q/s;
.super Ljava/lang/Object;
.source "DiskCacheWriteProducer.java"

# interfaces
.implements Lf/g/j/q/u0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/q/s$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/q/u0<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/g;

.field public final b:Lf/g/j/c/g;

.field public final c:Lf/g/j/c/j;

.field public final d:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/u0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/q/s;->a:Lf/g/j/c/g;

    iput-object p2, p0, Lf/g/j/q/s;->b:Lf/g/j/c/g;

    iput-object p3, p0, Lf/g/j/q/s;->c:Lf/g/j/c/j;

    iput-object p4, p0, Lf/g/j/q/s;->d:Lf/g/j/q/u0;

    return-void
.end method


# virtual methods
.method public b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            ")V"
        }
    .end annotation

    invoke-interface {p2}, Lf/g/j/q/v0;->q()Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v0

    sget-object v1, Lcom/facebook/imagepipeline/request/ImageRequest$c;->e:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    invoke-virtual {v1}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g()I

    move-result v1

    if-lt v0, v1, :cond_0

    const-string v0, "disk"

    const-string v1, "nil-result_write"

    invoke-interface {p2, v0, v1}, Lf/g/j/q/v0;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x0

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    if-eqz v0, :cond_1

    new-instance v0, Lf/g/j/q/s$b;

    iget-object v4, p0, Lf/g/j/q/s;->a:Lf/g/j/c/g;

    iget-object v5, p0, Lf/g/j/q/s;->b:Lf/g/j/c/g;

    iget-object v6, p0, Lf/g/j/q/s;->c:Lf/g/j/c/j;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lf/g/j/q/s$b;-><init>(Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/s$a;)V

    move-object p1, v0

    :cond_1
    iget-object v0, p0, Lf/g/j/q/s;->d:Lf/g/j/q/u0;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    :goto_0
    return-void
.end method
