.class public Lf/g/j/q/t;
.super Lf/g/j/q/j0;
.source "EncodedCacheKeyMultiplexProducer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/j0<",
        "Landroid/util/Pair<",
        "Lcom/facebook/cache/common/CacheKey;",
        "Lcom/facebook/imagepipeline/request/ImageRequest$c;",
        ">;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final f:Lf/g/j/c/j;


# direct methods
.method public constructor <init>(Lf/g/j/c/j;ZLf/g/j/q/u0;)V
    .locals 2

    const-string v0, "EncodedCacheKeyMultiplexProducer"

    const-string v1, "multiplex_enc_cnt"

    invoke-direct {p0, p3, v0, v1, p2}, Lf/g/j/q/j0;-><init>(Lf/g/j/q/u0;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object p1, p0, Lf/g/j/q/t;->f:Lf/g/j/c/j;

    return-void
.end method


# virtual methods
.method public c(Ljava/io/Closeable;)Ljava/io/Closeable;
    .locals 0

    check-cast p1, Lf/g/j/j/e;

    invoke-static {p1}, Lf/g/j/j/e;->a(Lf/g/j/j/e;)Lf/g/j/j/e;

    move-result-object p1

    return-object p1
.end method

.method public d(Lf/g/j/q/v0;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lf/g/j/q/t;->f:Lf/g/j/c/j;

    invoke-interface {p1}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    invoke-interface {p1}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v0, Lf/g/j/c/n;

    invoke-virtual {v0, v1, v2}, Lf/g/j/c/n;->b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v0

    invoke-interface {p1}, Lf/g/j/q/v0;->q()Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p1

    return-object p1
.end method
