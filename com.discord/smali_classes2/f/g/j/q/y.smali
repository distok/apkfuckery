.class public Lf/g/j/q/y;
.super Lf/g/j/q/e;
.source "HttpUrlConnectionNetworkFetcher.java"


# instance fields
.field public final synthetic a:Ljava/util/concurrent/Future;

.field public final synthetic b:Lf/g/j/q/m0$a;


# direct methods
.method public constructor <init>(Lf/g/j/q/z;Ljava/util/concurrent/Future;Lf/g/j/q/m0$a;)V
    .locals 0

    iput-object p2, p0, Lf/g/j/q/y;->a:Ljava/util/concurrent/Future;

    iput-object p3, p0, Lf/g/j/q/y;->b:Lf/g/j/q/m0$a;

    invoke-direct {p0}, Lf/g/j/q/e;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    iget-object v0, p0, Lf/g/j/q/y;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/y;->b:Lf/g/j/q/m0$a;

    check-cast v0, Lf/g/j/q/l0$a;

    iget-object v1, v0, Lf/g/j/q/l0$a;->b:Lf/g/j/q/l0;

    iget-object v0, v0, Lf/g/j/q/l0$a;->a:Lf/g/j/q/w;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/g/j/q/w;->a()Lf/g/j/q/x0;

    move-result-object v1

    iget-object v2, v0, Lf/g/j/q/w;->b:Lf/g/j/q/v0;

    const/4 v3, 0x0

    const-string v4, "NetworkFetchProducer"

    invoke-interface {v1, v2, v4, v3}, Lf/g/j/q/x0;->d(Lf/g/j/q/v0;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, v0, Lf/g/j/q/w;->a:Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    :cond_0
    return-void
.end method
