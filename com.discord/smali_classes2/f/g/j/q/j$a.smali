.class public Lf/g/j/q/j$a;
.super Lf/g/j/q/o;
.source "BitmapProbeProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/q/v0;

.field public final d:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lf/g/j/c/g;

.field public final f:Lf/g/j/c/g;

.field public final g:Lf/g/j/c/j;

.field public final h:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/c/e;Lf/g/j/c/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/v0;",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;",
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p2, p0, Lf/g/j/q/j$a;->c:Lf/g/j/q/v0;

    iput-object p3, p0, Lf/g/j/q/j$a;->d:Lf/g/j/c/t;

    iput-object p4, p0, Lf/g/j/q/j$a;->e:Lf/g/j/c/g;

    iput-object p5, p0, Lf/g/j/q/j$a;->f:Lf/g/j/c/g;

    iput-object p6, p0, Lf/g/j/q/j$a;->g:Lf/g/j/c/j;

    iput-object p7, p0, Lf/g/j/q/j$a;->h:Lf/g/j/c/e;

    iput-object p8, p0, Lf/g/j/q/j$a;->i:Lf/g/j/c/e;

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 3

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    const/16 v0, 0x8

    invoke-static {p2, v0}, Lf/g/j/q/b;->l(II)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/j/q/j$a;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/q/j$a;->g:Lf/g/j/c/j;

    iget-object v2, p0, Lf/g/j/q/j$a;->c:Lf/g/j/q/v0;

    invoke-interface {v2}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast v1, Lf/g/j/c/n;

    :try_start_1
    invoke-virtual {v1, v0, v2}, Lf/g/j/c/n;->b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    iget-object v0, p0, Lf/g/j/q/j$a;->c:Lf/g/j/q/v0;

    const-string v1, "origin"

    invoke-interface {v0, v1}, Lf/g/j/q/v0;->l(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "memory_bitmap"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/g/j/q/j$a;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->g()Lf/g/j/e/k;

    move-result-object v0

    iget-object v0, v0, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/g/j/q/j$a;->c:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->g()Lf/g/j/e/k;

    move-result-object v0

    iget-object v0, v0, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
