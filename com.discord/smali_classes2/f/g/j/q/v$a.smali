.class public Lf/g/j/q/v$a;
.super Lf/g/j/q/o;
.source "EncodedProbeProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lf/g/j/q/v0;

.field public final d:Lf/g/j/c/g;

.field public final e:Lf/g/j/c/g;

.field public final f:Lf/g/j/c/j;

.field public final g:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/q/l;Lf/g/j/q/v0;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/c/e;Lf/g/j/c/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;",
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    iput-object p2, p0, Lf/g/j/q/v$a;->c:Lf/g/j/q/v0;

    iput-object p3, p0, Lf/g/j/q/v$a;->d:Lf/g/j/c/g;

    iput-object p4, p0, Lf/g/j/q/v$a;->e:Lf/g/j/c/g;

    iput-object p5, p0, Lf/g/j/q/v$a;->f:Lf/g/j/c/j;

    iput-object p6, p0, Lf/g/j/q/v$a;->g:Lf/g/j/c/e;

    iput-object p7, p0, Lf/g/j/q/v$a;->h:Lf/g/j/c/e;

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 5

    check-cast p1, Lf/g/j/j/e;

    const-string v0, "origin"

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result v1

    if-nez v1, :cond_5

    if-eqz p1, :cond_5

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lf/g/j/q/b;->l(II)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v1, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v2, Lf/g/i/c;->b:Lf/g/i/c;

    if-ne v1, v2, :cond_0

    goto :goto_3

    :cond_0
    iget-object v1, p0, Lf/g/j/q/v$a;->c:Lf/g/j/q/v0;

    invoke-interface {v1}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    iget-object v2, p0, Lf/g/j/q/v$a;->f:Lf/g/j/c/j;

    iget-object v3, p0, Lf/g/j/q/v$a;->c:Lf/g/j/q/v0;

    invoke-interface {v3}, Lf/g/j/q/v0;->b()Ljava/lang/Object;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast v2, Lf/g/j/c/n;

    :try_start_1
    invoke-virtual {v2, v1, v3}, Lf/g/j/c/n;->b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v2

    iget-object v3, p0, Lf/g/j/q/v$a;->g:Lf/g/j/c/e;

    invoke-virtual {v3, v2}, Lf/g/j/c/e;->a(Ljava/lang/Object;)Z

    iget-object v3, p0, Lf/g/j/q/v$a;->c:Lf/g/j/q/v0;

    invoke-interface {v3, v0}, Lf/g/j/q/v0;->l(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "memory_encoded"

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lf/g/j/q/v$a;->h:Lf/g/j/c/e;

    invoke-virtual {v0, v2}, Lf/g/j/c/e;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v1, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    sget-object v1, Lcom/facebook/imagepipeline/request/ImageRequest$b;->d:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/g/j/q/v$a;->e:Lf/g/j/c/g;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lf/g/j/q/v$a;->d:Lf/g/j/c/g;

    :goto_1
    invoke-virtual {v0, v2}, Lf/g/j/c/g;->c(Lcom/facebook/cache/common/CacheKey;)V

    iget-object v0, p0, Lf/g/j/q/v$a;->h:Lf/g/j/c/e;

    invoke-virtual {v0, v2}, Lf/g/j/c/e;->a(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lf/g/j/q/v$a;->c:Lf/g/j/q/v0;

    invoke-interface {v1, v0}, Lf/g/j/q/v0;->l(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "disk"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/g/j/q/v$a;->h:Lf/g/j/c/e;

    invoke-virtual {v0, v2}, Lf/g/j/c/e;->a(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_4

    :cond_5
    :goto_3
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_4
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
