.class public Lf/g/j/q/r0$c;
.super Lf/g/j/q/o;
.source "PostprocessorProducer.java"

# interfaces
.implements Lf/g/j/r/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/r0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;",
        "Lf/g/j/r/d;"
    }
.end annotation


# instance fields
.field public c:Z

.field public d:Lcom/facebook/common/references/CloseableReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/g/j/q/r0;Lf/g/j/q/r0$b;Lf/g/j/r/c;Lf/g/j/q/v0;Lf/g/j/q/r0$a;)V
    .locals 0

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    const/4 p2, 0x0

    iput-boolean p2, p0, Lf/g/j/q/r0$c;->c:Z

    const/4 p2, 0x0

    iput-object p2, p0, Lf/g/j/q/r0$c;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-interface {p3, p0}, Lf/g/j/r/c;->a(Lf/g/j/r/d;)V

    new-instance p2, Lf/g/j/q/t0;

    invoke-direct {p2, p0, p1}, Lf/g/j/q/t0;-><init>(Lf/g/j/q/r0$c;Lf/g/j/q/r0;)V

    invoke-interface {p4, p2}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    return-void
.end method


# virtual methods
.method public g()V
    .locals 1

    invoke-virtual {p0}, Lf/g/j/q/r0$c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0}, Lf/g/j/q/l;->d()V

    :cond_0
    return-void
.end method

.method public h(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p0}, Lf/g/j/q/r0$c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1}, Lf/g/j/q/l;->c(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    invoke-static {p2}, Lf/g/j/q/b;->f(I)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean p2, p0, Lf/g/j/q/r0$c;->c:Z

    if-eqz p2, :cond_1

    monitor-exit p0

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lf/g/j/q/r0$c;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/q/r0$c;->d:Lcom/facebook/common/references/CloseableReference;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    :goto_0
    monitor-enter p0

    :try_start_1
    iget-boolean p1, p0, Lf/g/j/q/r0$c;->c:Z

    if-eqz p1, :cond_3

    monitor-exit p0

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lf/g/j/q/r0$c;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object p2, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_4
    :goto_1
    return-void

    :catchall_0
    move-exception p2

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_5
    throw p2

    :catchall_1
    move-exception p1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1

    :catchall_2
    move-exception p1

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw p1
.end method

.method public final n()Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf/g/j/q/r0$c;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    return v0

    :cond_0
    iget-object v0, p0, Lf/g/j/q/r0$c;->d:Lcom/facebook/common/references/CloseableReference;

    const/4 v1, 0x0

    iput-object v1, p0, Lf/g/j/q/r0$c;->d:Lcom/facebook/common/references/CloseableReference;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/g/j/q/r0$c;->c:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    return v1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
