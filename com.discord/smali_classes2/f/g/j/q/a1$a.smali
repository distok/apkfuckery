.class public Lf/g/j/q/a1$a;
.super Lf/g/j/q/o;
.source "ResizeAndRotateProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/q/a1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/o<",
        "Lf/g/j/j/e;",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Z

.field public final d:Lf/g/j/t/c;

.field public final e:Lf/g/j/q/v0;

.field public f:Z

.field public final g:Lf/g/j/q/b0;

.field public final synthetic h:Lf/g/j/q/a1;


# direct methods
.method public constructor <init>(Lf/g/j/q/a1;Lf/g/j/q/l;Lf/g/j/q/v0;ZLf/g/j/t/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/l<",
            "Lf/g/j/j/e;",
            ">;",
            "Lf/g/j/q/v0;",
            "Z",
            "Lf/g/j/t/c;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lf/g/j/q/a1$a;->h:Lf/g/j/q/a1;

    invoke-direct {p0, p2}, Lf/g/j/q/o;-><init>(Lf/g/j/q/l;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/j/q/a1$a;->f:Z

    iput-object p3, p0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {p3}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean p4, p0, Lf/g/j/q/a1$a;->c:Z

    iput-object p5, p0, Lf/g/j/q/a1$a;->d:Lf/g/j/t/c;

    new-instance p4, Lf/g/j/q/a1$a$a;

    invoke-direct {p4, p0, p1}, Lf/g/j/q/a1$a$a;-><init>(Lf/g/j/q/a1$a;Lf/g/j/q/a1;)V

    new-instance p5, Lf/g/j/q/b0;

    iget-object v0, p1, Lf/g/j/q/a1;->a:Ljava/util/concurrent/Executor;

    const/16 v1, 0x64

    invoke-direct {p5, v0, p4, v1}, Lf/g/j/q/b0;-><init>(Ljava/util/concurrent/Executor;Lf/g/j/q/b0$c;I)V

    iput-object p5, p0, Lf/g/j/q/a1$a;->g:Lf/g/j/q/b0;

    new-instance p4, Lf/g/j/q/a1$a$b;

    invoke-direct {p4, p0, p1, p2}, Lf/g/j/q/a1$a$b;-><init>(Lf/g/j/q/a1$a;Lf/g/j/q/a1;Lf/g/j/q/l;)V

    invoke-interface {p3, p4}, Lf/g/j/q/v0;->f(Lf/g/j/q/w0;)V

    return-void
.end method


# virtual methods
.method public i(Ljava/lang/Object;I)V
    .locals 9

    check-cast p1, Lf/g/j/j/e;

    sget-object v0, Lf/g/d/l/a;->f:Lf/g/d/l/a;

    iget-boolean v1, p0, Lf/g/j/q/a1$a;->f:Z

    if-eqz v1, :cond_0

    goto/16 :goto_7

    :cond_0
    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result v1

    const/4 v2, 0x1

    if-nez p1, :cond_1

    if-eqz v1, :cond_11

    iget-object p1, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    const/4 p2, 0x0

    invoke-interface {p1, p2, v2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto/16 :goto_7

    :cond_1
    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v3, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    iget-object v4, p0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v4}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v4

    iget-object v5, p0, Lf/g/j/q/a1$a;->d:Lf/g/j/t/c;

    iget-boolean v6, p0, Lf/g/j/q/a1$a;->c:Z

    invoke-interface {v5, v3, v6}, Lf/g/j/t/c;->createImageTranscoder(Lf/g/i/c;Z)Lf/g/j/t/b;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v6, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v7, Lf/g/i/c;->b:Lf/g/i/c;

    const/4 v8, 0x0

    if-ne v6, v7, :cond_2

    move-object v2, v0

    goto :goto_4

    :cond_2
    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v6, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    invoke-interface {v5, v6}, Lf/g/j/t/b;->d(Lf/g/i/c;)Z

    move-result v6

    if-nez v6, :cond_3

    sget-object v2, Lf/g/d/l/a;->e:Lf/g/d/l/a;

    goto :goto_4

    :cond_3
    iget-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-boolean v7, v6, Lf/g/j/d/f;->b:Z

    if-nez v7, :cond_7

    invoke-static {v6, p1}, Lf/g/j/t/d;->b(Lf/g/j/d/f;Lf/g/j/j/e;)I

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {v6}, Lf/g/j/d/f;->b()Z

    move-result v7

    if-eqz v7, :cond_5

    iget-boolean v6, v6, Lf/g/j/d/f;->b:Z

    if-eqz v6, :cond_4

    goto :goto_0

    :cond_4
    sget-object v6, Lf/g/j/t/d;->a:Lf/g/d/d/e;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v7, p1, Lf/g/j/j/e;->h:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    goto :goto_1

    :cond_5
    :goto_0
    iput v8, p1, Lf/g/j/j/e;->h:I

    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_7

    :cond_6
    const/4 v6, 0x1

    goto :goto_2

    :cond_7
    const/4 v6, 0x0

    :goto_2
    if-nez v6, :cond_9

    iget-object v6, v4, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-object v4, v4, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    invoke-interface {v5, p1, v6, v4}, Lf/g/j/t/b;->b(Lf/g/j/j/e;Lf/g/j/d/f;Lf/g/j/d/e;)Z

    move-result v4

    if-eqz v4, :cond_8

    goto :goto_3

    :cond_8
    const/4 v2, 0x0

    :cond_9
    :goto_3
    invoke-static {v2}, Lf/g/d/l/a;->f(Z)Lf/g/d/l/a;

    move-result-object v2

    :goto_4
    if-nez v1, :cond_a

    if-ne v2, v0, :cond_a

    goto/16 :goto_7

    :cond_a
    sget-object v0, Lf/g/d/l/a;->d:Lf/g/d/l/a;

    if-eq v2, v0, :cond_e

    sget-object v0, Lf/g/i/b;->a:Lf/g/i/c;

    if-eq v3, v0, :cond_c

    sget-object v0, Lf/g/i/b;->k:Lf/g/i/c;

    if-ne v3, v0, :cond_b

    goto :goto_5

    :cond_b
    iget-object v0, p0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    invoke-virtual {v0}, Lf/g/j/d/f;->c()Z

    move-result v1

    if-nez v1, :cond_d

    invoke-virtual {v0}, Lf/g/j/d/f;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lf/g/j/d/f;->a()I

    move-result v0

    invoke-static {p1}, Lf/g/j/j/e;->a(Lf/g/j/j/e;)Lf/g/j/j/e;

    move-result-object p1

    if-eqz p1, :cond_d

    iput v0, p1, Lf/g/j/j/e;->g:I

    goto :goto_6

    :cond_c
    :goto_5
    iget-object v0, p0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->e()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-boolean v0, v0, Lf/g/j/d/f;->b:Z

    if-nez v0, :cond_d

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v0, p1, Lf/g/j/j/e;->g:I

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v0, p1, Lf/g/j/j/e;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_d

    invoke-static {p1}, Lf/g/j/j/e;->a(Lf/g/j/j/e;)Lf/g/j/j/e;

    move-result-object p1

    if-eqz p1, :cond_d

    iput v8, p1, Lf/g/j/j/e;->g:I

    :cond_d
    :goto_6
    iget-object v0, p0, Lf/g/j/q/o;->b:Lf/g/j/q/l;

    invoke-interface {v0, p1, p2}, Lf/g/j/q/l;->b(Ljava/lang/Object;I)V

    goto :goto_7

    :cond_e
    iget-object v0, p0, Lf/g/j/q/a1$a;->g:Lf/g/j/q/b0;

    invoke-virtual {v0, p1, p2}, Lf/g/j/q/b0;->f(Lf/g/j/j/e;I)Z

    move-result p1

    if-nez p1, :cond_f

    goto :goto_7

    :cond_f
    if-nez v1, :cond_10

    iget-object p1, p0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {p1}, Lf/g/j/q/v0;->p()Z

    move-result p1

    if-eqz p1, :cond_11

    :cond_10
    iget-object p1, p0, Lf/g/j/q/a1$a;->g:Lf/g/j/q/b0;

    invoke-virtual {p1}, Lf/g/j/q/b0;->d()Z

    :cond_11
    :goto_7
    return-void
.end method

.method public final n(Lf/g/j/j/e;Lf/g/j/d/e;Lf/g/j/t/a;Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/j/e;",
            "Lf/g/j/d/e;",
            "Lf/g/j/t/a;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    invoke-interface {v0}, Lf/g/j/q/v0;->o()Lf/g/j/q/x0;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/q/a1$a;->e:Lf/g/j/q/v0;

    const-string v2, "ResizeAndRotateProducer"

    invoke-interface {v0, v1, v2}, Lf/g/j/q/x0;->g(Lf/g/j/q/v0;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v1, p1, Lf/g/j/j/e;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v1, p1, Lf/g/j/j/e;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p2, Lf/g/j/d/e;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p2, Lf/g/j/d/e;->b:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    const-string p2, "Unspecified"

    :goto_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "Image format"

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object p1, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "Original size"

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "Requested size"

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "queueTime"

    iget-object p2, p0, Lf/g/j/q/a1$a;->g:Lf/g/j/q/b0;

    monitor-enter p2

    :try_start_0
    iget-wide v2, p2, Lf/g/j/q/b0;->j:J

    iget-wide v4, p2, Lf/g/j/q/b0;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v2, v4

    monitor-exit p2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "Transcoder id"

    invoke-virtual {v1, p1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "Transcoding result"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lf/g/d/d/f;

    invoke-direct {p1, v1}, Lf/g/d/d/f;-><init>(Ljava/util/Map;)V

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p2

    throw p1
.end method
