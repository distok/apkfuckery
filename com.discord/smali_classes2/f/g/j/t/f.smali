.class public Lf/g/j/t/f;
.super Ljava/lang/Object;
.source "SimpleImageTranscoder.java"

# interfaces
.implements Lf/g/j/t/b;


# instance fields
.field public final a:Z

.field public final b:I


# direct methods
.method public constructor <init>(ZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lf/g/j/t/f;->a:Z

    iput p2, p0, Lf/g/j/t/f;->b:I

    return-void
.end method

.method public static e(Lf/g/i/c;)Landroid/graphics/Bitmap$CompressFormat;
    .locals 1

    if-nez p0, :cond_0

    sget-object p0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    return-object p0

    :cond_0
    sget-object v0, Lf/g/i/b;->a:Lf/g/i/c;

    if-ne p0, v0, :cond_1

    sget-object p0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    return-object p0

    :cond_1
    sget-object v0, Lf/g/i/b;->b:Lf/g/i/c;

    if-ne p0, v0, :cond_2

    sget-object p0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    return-object p0

    :cond_2
    invoke-static {p0}, Lf/g/i/b;->a(Lf/g/i/c;)Z

    move-result p0

    if-eqz p0, :cond_3

    sget-object p0, Landroid/graphics/Bitmap$CompressFormat;->WEBP:Landroid/graphics/Bitmap$CompressFormat;

    return-object p0

    :cond_3
    sget-object p0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "SimpleImageTranscoder"

    return-object v0
.end method

.method public b(Lf/g/j/j/e;Lf/g/j/d/f;Lf/g/j/d/e;)Z
    .locals 2

    if-nez p2, :cond_0

    sget-object p2, Lf/g/j/d/f;->c:Lf/g/j/d/f;

    :cond_0
    iget-boolean v0, p0, Lf/g/j/t/f;->a:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget v0, p0, Lf/g/j/t/f;->b:I

    invoke-static {p2, p3, p1, v0}, Lf/g/j/k/a;->O(Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/j/j/e;I)I

    move-result p1

    if-le p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public c(Lf/g/j/j/e;Ljava/io/OutputStream;Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/i/c;Ljava/lang/Integer;)Lf/g/j/t/a;
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    const-string v2, "Out-Of-Memory during transcode"

    const-string v3, "SimpleImageTranscoder"

    if-nez p6, :cond_0

    const/16 v4, 0x55

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object/from16 v4, p6

    :goto_0
    if-nez p3, :cond_1

    sget-object v5, Lf/g/j/d/f;->c:Lf/g/j/d/f;

    goto :goto_1

    :cond_1
    move-object/from16 v5, p3

    :goto_1
    iget-boolean v6, v1, Lf/g/j/t/f;->a:Z

    const/4 v7, 0x1

    if-nez v6, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    iget v6, v1, Lf/g/j/t/f;->b:I

    move-object/from16 v8, p4

    invoke-static {v5, v8, v0, v6}, Lf/g/j/k/a;->O(Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/j/j/e;I)I

    move-result v6

    :goto_2
    new-instance v8, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v8}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput v6, v8, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v9, 0x2

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11, v8}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v8, :cond_3

    const-string v0, "Couldn\'t decode the EncodedImage InputStream ! "

    invoke-static {v3, v0}, Lf/g/d/e/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lf/g/j/t/a;

    invoke-direct {v0, v9}, Lf/g/j/t/a;-><init>(I)V

    return-object v0

    :cond_3
    sget-object v10, Lf/g/j/t/d;->a:Lf/g/d/d/e;

    invoke-virtual/range {p1 .. p1}, Lf/g/j/j/e;->o()V

    iget v12, v0, Lf/g/j/j/e;->h:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-static {v5, v0}, Lf/g/j/t/d;->a(Lf/g/j/d/f;Lf/g/j/j/e;)I

    move-result v0

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v12, -0x40800000    # -1.0f

    if-eq v0, v9, :cond_7

    const/4 v13, 0x7

    if-eq v0, v13, :cond_6

    const/4 v13, 0x4

    if-eq v0, v13, :cond_5

    const/4 v13, 0x5

    if-eq v0, v13, :cond_4

    goto :goto_3

    :cond_4
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    invoke-virtual {v5, v12, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_4

    :cond_5
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    invoke-virtual {v5, v12, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_4

    :cond_6
    const/high16 v0, -0x3d4c0000    # -90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    invoke-virtual {v5, v12, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_4

    :cond_7
    invoke-virtual {v5, v12, v10}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_4

    :cond_8
    invoke-static {v5, v0}, Lf/g/j/t/d;->b(Lf/g/j/d/f;Lf/g/j/j/e;)I

    move-result v0

    if-eqz v0, :cond_9

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    goto :goto_4

    :cond_9
    :goto_3
    move-object v5, v11

    :goto_4
    move-object/from16 v17, v5

    if-eqz v17, :cond_a

    const/4 v13, 0x0

    const/4 v14, 0x0

    :try_start_1
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    const/16 v18, 0x0

    move-object v12, v8

    invoke-static/range {v12 .. v18}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v5, v0

    goto :goto_5

    :catchall_0
    move-exception v0

    move-object v5, v8

    goto :goto_7

    :catch_0
    move-exception v0

    move-object v5, v8

    goto :goto_6

    :cond_a
    move-object v5, v8

    :goto_5
    :try_start_2
    invoke-static {v11}, Lf/g/j/t/f;->e(Lf/g/i/c;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v10, p2

    invoke-virtual {v5, v0, v4, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    new-instance v0, Lf/g/j/t/a;

    if-le v6, v7, :cond_b

    const/4 v7, 0x0

    :cond_b
    invoke-direct {v0, v7}, Lf/g/j/t/a;-><init>(I)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    return-object v0

    :catchall_1
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    :goto_6
    :try_start_3
    invoke-static {v3, v2, v0}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v0, Lf/g/j/t/a;

    invoke-direct {v0, v9}, Lf/g/j/t/a;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    return-object v0

    :goto_7
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    throw v0

    :catch_2
    move-exception v0

    invoke-static {v3, v2, v0}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v0, Lf/g/j/t/a;

    invoke-direct {v0, v9}, Lf/g/j/t/a;-><init>(I)V

    return-object v0
.end method

.method public d(Lf/g/i/c;)Z
    .locals 1

    sget-object v0, Lf/g/i/b;->k:Lf/g/i/c;

    if-eq p1, v0, :cond_1

    sget-object v0, Lf/g/i/b;->a:Lf/g/i/c;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
