.class public Lf/g/j/t/e;
.super Ljava/lang/Object;
.source "MultiImageTranscoderFactory.java"

# interfaces
.implements Lf/g/j/t/c;


# instance fields
.field public final a:I

.field public final b:Z


# direct methods
.method public constructor <init>(IZLf/g/j/t/c;Ljava/lang/Integer;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/g/j/t/e;->a:I

    iput-boolean p5, p0, Lf/g/j/t/e;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Lf/g/i/c;Z)Lf/g/j/t/b;
    .locals 9

    iget v0, p0, Lf/g/j/t/e;->a:I

    iget-boolean v1, p0, Lf/g/j/t/e;->b:Z

    :try_start_0
    const-string v2, "com.facebook.imagepipeline.nativecode.NativeJpegTranscoderFactory"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x1

    aput-object v5, v4, v7

    const/4 v8, 0x2

    aput-object v5, v4, v8

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    aput-object v0, v3, v7

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/t/c;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-interface {v0, p1, p2}, Lf/g/j/t/c;->createImageTranscoder(Lf/g/i/c;Z)Lf/g/j/t/b;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_0

    :catch_2
    move-exception p1

    goto :goto_0

    :catch_3
    move-exception p1

    goto :goto_0

    :catch_4
    move-exception p1

    goto :goto_0

    :catch_5
    move-exception p1

    goto :goto_0

    :catch_6
    move-exception p1

    :goto_0
    new-instance p2, Ljava/lang/RuntimeException;

    const-string v0, "Dependency \':native-imagetranscoder\' is needed to use the default native image transcoder."

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public createImageTranscoder(Lf/g/i/c;Z)Lf/g/j/t/b;
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lf/g/j/e/n;->a:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lf/g/j/t/e;->a(Lf/g/i/c;Z)Lf/g/j/t/b;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    iget p1, p0, Lf/g/j/t/e;->a:I

    new-instance v0, Lf/g/j/t/f;

    invoke-direct {v0, p2, p1}, Lf/g/j/t/f;-><init>(ZI)V

    :cond_1
    return-object v0
.end method
