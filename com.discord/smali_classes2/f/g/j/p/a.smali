.class public Lf/g/j/p/a;
.super Lf/g/j/r/a;
.source "RoundAsCirclePostprocessor.java"


# static fields
.field private static final ENABLE_ANTI_ALIASING:Z = true


# instance fields
.field private mCacheKey:Lcom/facebook/cache/common/CacheKey;

.field private final mEnableAntiAliasing:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lf/g/j/p/a;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Lf/g/j/r/a;-><init>()V

    iput-boolean p1, p0, Lf/g/j/p/a;->mEnableAntiAliasing:Z

    return-void
.end method


# virtual methods
.method public getPostprocessorCacheKey()Lcom/facebook/cache/common/CacheKey;
    .locals 2

    iget-object v0, p0, Lf/g/j/p/a;->mCacheKey:Lcom/facebook/cache/common/CacheKey;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lf/g/j/p/a;->mEnableAntiAliasing:Z

    if-eqz v0, :cond_0

    new-instance v0, Lf/g/b/a/f;

    const-string v1, "RoundAsCirclePostprocessor#AntiAliased"

    invoke-direct {v0, v1}, Lf/g/b/a/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lf/g/j/p/a;->mCacheKey:Lcom/facebook/cache/common/CacheKey;

    goto :goto_0

    :cond_0
    new-instance v0, Lf/g/b/a/f;

    const-string v1, "RoundAsCirclePostprocessor"

    invoke-direct {v0, v1}, Lf/g/b/a/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lf/g/j/p/a;->mCacheKey:Lcom/facebook/cache/common/CacheKey;

    :cond_1
    :goto_0
    iget-object v0, p0, Lf/g/j/p/a;->mCacheKey:Lcom/facebook/cache/common/CacheKey;

    return-object v0
.end method

.method public process(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-boolean v0, p0, Lf/g/j/p/a;->mEnableAntiAliasing:Z

    invoke-static {p1, v0}, Lcom/facebook/imagepipeline/nativecode/NativeRoundingFilter;->toCircleFast(Landroid/graphics/Bitmap;Z)V

    return-void
.end method
