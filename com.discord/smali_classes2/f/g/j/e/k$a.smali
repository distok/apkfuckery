.class public Lf/g/j/e/k$a;
.super Ljava/lang/Object;
.source "ImagePipelineConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/e/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Lcom/facebook/imagepipeline/cache/MemoryCacheParams;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public c:Z

.field public d:Lcom/facebook/cache/disk/DiskCacheConfig;

.field public e:Lcom/facebook/cache/disk/DiskCacheConfig;

.field public final f:Lf/g/j/e/l$b;

.field public g:Z

.field public h:Lf/g/j/g/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/g/j/e/j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p2, 0x0

    iput-boolean p2, p0, Lf/g/j/e/k$a;->c:Z

    new-instance p2, Lf/g/j/e/l$b;

    invoke-direct {p2, p0}, Lf/g/j/e/l$b;-><init>(Lf/g/j/e/k$a;)V

    iput-object p2, p0, Lf/g/j/e/k$a;->f:Lf/g/j/e/l$b;

    const/4 p2, 0x1

    iput-boolean p2, p0, Lf/g/j/e/k$a;->g:Z

    new-instance p2, Lf/g/j/g/a;

    invoke-direct {p2}, Lf/g/j/g/a;-><init>()V

    iput-object p2, p0, Lf/g/j/e/k$a;->h:Lf/g/j/g/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/e/k$a;->b:Landroid/content/Context;

    return-void
.end method
