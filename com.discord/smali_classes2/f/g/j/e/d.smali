.class public Lf/g/j/e/d;
.super Ljava/lang/Object;
.source "DiskStorageCacheFactory.java"

# interfaces
.implements Lf/g/j/e/g;


# instance fields
.field public a:Lf/g/j/e/e;


# direct methods
.method public constructor <init>(Lf/g/j/e/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/e/d;->a:Lf/g/j/e/e;

    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/cache/disk/DiskCacheConfig;)Lf/g/b/b/h;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lf/g/j/e/d;->a:Lf/g/j/e/e;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lf/g/b/b/e;

    iget v2, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->a:I

    iget-object v3, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->c:Lcom/facebook/common/internal/Supplier;

    iget-object v5, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->b:Ljava/lang/String;

    iget-object v6, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->h:Lf/g/b/a/a;

    invoke-direct {v4, v2, v3, v5, v6}, Lf/g/b/b/e;-><init>(ILcom/facebook/common/internal/Supplier;Ljava/lang/String;Lf/g/b/a/a;)V

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    new-instance v6, Lf/g/b/b/d$c;

    iget-wide v12, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->f:J

    iget-wide v14, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->e:J

    iget-wide v2, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->d:J

    move-object v11, v6

    move-wide/from16 v16, v2

    invoke-direct/range {v11 .. v17}, Lf/g/b/b/d$c;-><init>(JJJ)V

    new-instance v2, Lf/g/b/b/d;

    iget-object v5, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->g:Lf/g/b/b/g;

    iget-object v7, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->i:Lf/g/b/a/b;

    iget-object v8, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->h:Lf/g/b/a/a;

    iget-object v9, v1, Lcom/facebook/cache/disk/DiskCacheConfig;->j:Lf/g/d/a/a;

    const/4 v11, 0x0

    move-object v3, v2

    invoke-direct/range {v3 .. v11}, Lf/g/b/b/d;-><init>(Lf/g/b/b/c;Lf/g/b/b/g;Lf/g/b/b/d$c;Lf/g/b/a/b;Lf/g/b/a/a;Lf/g/d/a/a;Ljava/util/concurrent/Executor;Z)V

    return-object v2
.end method
