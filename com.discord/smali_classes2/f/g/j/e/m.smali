.class public Lf/g/j/e/m;
.super Ljava/lang/Object;
.source "ImagePipelineFactory.java"


# static fields
.field public static t:Lf/g/j/e/m;


# instance fields
.field public final a:Lf/g/j/q/e1;

.field public final b:Lf/g/j/e/k;

.field public final c:Lf/g/j/e/b;

.field public d:Lf/g/j/c/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lf/g/j/c/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/s<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lf/g/j/c/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lf/g/j/c/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/s<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lf/g/j/c/g;

.field public i:Lf/g/b/b/h;

.field public j:Lf/g/j/h/b;

.field public k:Lf/g/j/e/i;

.field public l:Lf/g/j/t/c;

.field public m:Lf/g/j/e/p;

.field public n:Lf/g/j/e/q;

.field public o:Lf/g/j/c/g;

.field public p:Lf/g/b/b/h;

.field public q:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field public r:Lf/g/j/o/d;

.field public s:Lf/g/j/a/b/a;


# direct methods
.method public constructor <init>(Lf/g/j/e/k;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v0, p1, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/g/j/q/f1;

    iget-object v1, p1, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    invoke-interface {v1}, Lf/g/j/e/f;->b()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, v1}, Lf/g/j/q/f1;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lf/g/j/e/m;->a:Lf/g/j/q/e1;

    iget-object v0, p1, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    sput v0, Lcom/facebook/common/references/CloseableReference;->i:I

    new-instance v0, Lf/g/j/e/b;

    iget-object p1, p1, Lf/g/j/e/k;->x:Lf/g/j/g/a;

    invoke-direct {v0, p1}, Lf/g/j/e/b;-><init>(Lf/g/j/g/a;)V

    iput-object v0, p0, Lf/g/j/e/m;->c:Lf/g/j/e/b;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method

.method public static declared-synchronized j(Lf/g/j/e/k;)V
    .locals 2

    const-class v0, Lf/g/j/e/m;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/g/j/e/m;->t:Lf/g/j/e/m;

    if-eqz v1, :cond_0

    const-string v1, "ImagePipelineFactory has already been initialized! `ImagePipelineFactory.initialize(...)` should only be called once to avoid unexpected behavior."

    invoke-static {v0, v1}, Lf/g/d/e/a;->k(Ljava/lang/Class;Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lf/g/j/e/m;

    invoke-direct {v1, p0}, Lf/g/j/e/m;-><init>(Lf/g/j/e/k;)V

    sput-object v1, Lf/g/j/e/m;->t:Lf/g/j/e/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public final a()Lf/g/j/e/i;
    .locals 40

    move-object/from16 v0, p0

    new-instance v16, Lf/g/j/e/i;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_0

    iget-object v1, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 v10, 0x0

    iget-object v1, v0, Lf/g/j/e/m;->n:Lf/g/j/e/q;

    if-nez v1, :cond_5

    new-instance v1, Lf/g/j/e/q;

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v2, v0, Lf/g/j/e/m;->m:Lf/g/j/e/p;

    if-nez v2, :cond_3

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v4, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    iget-object v4, v4, Lf/g/j/e/l;->b:Lf/g/j/e/l$d;

    iget-object v5, v2, Lf/g/j/e/k;->e:Landroid/content/Context;

    iget-object v2, v2, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    invoke-virtual {v2}, Lf/g/j/m/x;->f()Lf/g/d/g/a;

    move-result-object v19

    iget-object v2, v0, Lf/g/j/e/m;->j:Lf/g/j/h/b;

    if-nez v2, :cond_2

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->b()Lf/g/j/a/b/a;

    move-result-object v2

    const/4 v6, 0x0

    if-eqz v2, :cond_1

    iget-object v6, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v6, v6, Lf/g/j/e/k;->a:Landroid/graphics/Bitmap$Config;

    invoke-interface {v2, v6}, Lf/g/j/a/b/a;->b(Landroid/graphics/Bitmap$Config;)Lf/g/j/h/b;

    move-result-object v6

    iget-object v7, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v7, v7, Lf/g/j/e/k;->a:Landroid/graphics/Bitmap$Config;

    invoke-interface {v2, v7}, Lf/g/j/a/b/a;->c(Landroid/graphics/Bitmap$Config;)Lf/g/j/h/b;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v2, v6

    :goto_0
    iget-object v7, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lf/g/j/h/a;

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->h()Lf/g/j/o/d;

    move-result-object v8

    invoke-direct {v7, v6, v2, v8}, Lf/g/j/h/a;-><init>(Lf/g/j/h/b;Lf/g/j/h/b;Lf/g/j/o/d;)V

    iput-object v7, v0, Lf/g/j/e/m;->j:Lf/g/j/h/b;

    :cond_2
    iget-object v2, v0, Lf/g/j/e/m;->j:Lf/g/j/h/b;

    move-object/from16 v20, v2

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v6, v2, Lf/g/j/e/k;->q:Lf/g/j/h/c;

    move-object/from16 v21, v6

    iget-boolean v6, v2, Lf/g/j/e/k;->f:Z

    move/from16 v22, v6

    iget-boolean v6, v2, Lf/g/j/e/k;->t:Z

    move/from16 v23, v6

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v6, v2, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    move-object/from16 v25, v6

    iget-object v2, v2, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lf/g/j/m/x;->d(I)Lf/g/d/g/g;

    move-result-object v26

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->d()Lf/g/j/c/s;

    move-result-object v27

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->e()Lf/g/j/c/s;

    move-result-object v28

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->f()Lf/g/j/c/g;

    move-result-object v29

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->i()Lf/g/j/c/g;

    move-result-object v30

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->d:Lf/g/j/c/j;

    move-object/from16 v31, v2

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->g()Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    move-result-object v32

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v33, 0x0

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v34, 0x0

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v35, 0x0

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    iget v6, v2, Lf/g/j/e/l;->a:I

    move/from16 v36, v6

    iget-object v6, v0, Lf/g/j/e/m;->c:Lf/g/j/e/b;

    move-object/from16 v37, v6

    const/16 v38, 0x0

    iget v2, v2, Lf/g/j/e/l;->g:I

    move/from16 v39, v2

    check-cast v4, Lf/g/j/e/l$c;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/g/j/e/p;

    move-object/from16 v17, v2

    const/16 v24, 0x0

    move-object/from16 v18, v5

    invoke-direct/range {v17 .. v39}, Lf/g/j/e/p;-><init>(Landroid/content/Context;Lf/g/d/g/a;Lf/g/j/h/b;Lf/g/j/h/c;ZZZLf/g/j/e/f;Lf/g/d/g/g;Lf/g/j/c/t;Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;IIZILf/g/j/e/b;ZI)V

    iput-object v2, v0, Lf/g/j/e/m;->m:Lf/g/j/e/p;

    :cond_3
    iget-object v4, v0, Lf/g/j/e/m;->m:Lf/g/j/e/p;

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v5, v2, Lf/g/j/e/k;->n:Lf/g/j/q/m0;

    iget-boolean v6, v2, Lf/g/j/e/k;->t:Z

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Lf/g/j/e/m;->a:Lf/g/j/q/e1;

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-boolean v9, v2, Lf/g/j/e/k;->f:Z

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-boolean v12, v2, Lf/g/j/e/k;->w:Z

    iget-object v13, v0, Lf/g/j/e/m;->l:Lf/g/j/t/c;

    if-nez v13, :cond_4

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/g/j/t/e;

    iget-object v13, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v13, v13, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    iget v14, v13, Lf/g/j/e/l;->a:I

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    iget-boolean v13, v13, Lf/g/j/e/l;->f:Z

    move-object/from16 v17, v2

    move/from16 v18, v14

    move/from16 v22, v13

    invoke-direct/range {v17 .. v22}, Lf/g/j/t/e;-><init>(IZLf/g/j/t/c;Ljava/lang/Integer;Z)V

    iput-object v2, v0, Lf/g/j/e/m;->l:Lf/g/j/t/c;

    :cond_4
    iget-object v13, v0, Lf/g/j/e/m;->l:Lf/g/j/t/c;

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v14, 0x0

    iget-object v2, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v2, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v15, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v15}, Lf/g/j/e/q;-><init>(Landroid/content/ContentResolver;Lf/g/j/e/p;Lf/g/j/q/m0;ZZLf/g/j/q/e1;ZZZZLf/g/j/t/c;ZZ)V

    iput-object v1, v0, Lf/g/j/e/m;->n:Lf/g/j/e/q;

    :cond_5
    iget-object v2, v0, Lf/g/j/e/m;->n:Lf/g/j/e/q;

    iget-object v1, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->r:Ljava/util/Set;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    iget-object v1, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->s:Ljava/util/Set;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    iget-object v1, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v5, v1, Lf/g/j/e/k;->k:Lcom/facebook/common/internal/Supplier;

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->d()Lf/g/j/c/s;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->e()Lf/g/j/c/s;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->f()Lf/g/j/c/g;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lf/g/j/e/m;->i()Lf/g/j/c/g;

    move-result-object v9

    iget-object v1, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v10, v1, Lf/g/j/e/k;->d:Lf/g/j/c/j;

    iget-object v11, v0, Lf/g/j/e/m;->a:Lf/g/j/q/e1;

    iget-object v12, v1, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    iget-object v12, v12, Lf/g/j/e/l;->d:Lcom/facebook/common/internal/Supplier;

    const/4 v13, 0x0

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v14, 0x0

    iget-object v15, v0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lf/g/j/e/i;-><init>(Lf/g/j/e/q;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/common/internal/Supplier;Lf/g/j/c/t;Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/e1;Lcom/facebook/common/internal/Supplier;Lcom/facebook/common/internal/Supplier;Lf/g/c/a;Lf/g/j/e/k;)V

    return-object v16
.end method

.method public final b()Lf/g/j/a/b/a;
    .locals 12

    iget-object v0, p0, Lf/g/j/e/m;->s:Lf/g/j/a/b/a;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lf/g/j/e/m;->g()Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    invoke-virtual {p0}, Lf/g/j/e/m;->c()Lf/g/j/c/m;

    move-result-object v2

    iget-object v3, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v3, v3, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    iget-boolean v3, v3, Lf/g/j/e/l;->c:Z

    sget-boolean v4, Lf/g/j/a/b/b;->a:Z

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :try_start_0
    const-string v5, "com.facebook.fresco.animation.factory.AnimatedFactoryV2Impl"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x4

    new-array v7, v6, [Ljava/lang/Class;

    const-class v8, Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    const/4 v9, 0x0

    aput-object v8, v7, v9

    const-class v8, Lf/g/j/e/f;

    aput-object v8, v7, v4

    const-class v8, Lf/g/j/c/m;

    const/4 v10, 0x2

    aput-object v8, v7, v10

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v11, 0x3

    aput-object v8, v7, v11

    invoke-virtual {v5, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v9

    aput-object v1, v6, v4

    aput-object v2, v6, v10

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v11

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/a/b/a;

    sput-object v0, Lf/g/j/a/b/b;->b:Lf/g/j/a/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    :goto_0
    sget-object v0, Lf/g/j/a/b/b;->b:Lf/g/j/a/b/a;

    if-eqz v0, :cond_0

    sput-boolean v4, Lf/g/j/a/b/b;->a:Z

    :cond_0
    sget-object v0, Lf/g/j/a/b/b;->b:Lf/g/j/a/b/a;

    iput-object v0, p0, Lf/g/j/e/m;->s:Lf/g/j/a/b/a;

    :cond_1
    iget-object v0, p0, Lf/g/j/e/m;->s:Lf/g/j/a/b/a;

    return-object v0
.end method

.method public c()Lf/g/j/c/m;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/e/m;->d:Lf/g/j/c/m;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v0, Lf/g/j/e/k;->b:Lcom/facebook/common/internal/Supplier;

    iget-object v2, v0, Lf/g/j/e/k;->m:Lf/g/d/g/c;

    iget-object v0, v0, Lf/g/j/e/k;->c:Lf/g/j/c/t$a;

    const/4 v3, 0x0

    new-instance v4, Lf/g/j/c/a;

    invoke-direct {v4}, Lf/g/j/c/a;-><init>()V

    new-instance v5, Lf/g/j/c/m;

    invoke-direct {v5, v4, v0, v1, v3}, Lf/g/j/c/m;-><init>(Lf/g/j/c/y;Lf/g/j/c/t$a;Lcom/facebook/common/internal/Supplier;Lf/g/j/c/m$c;)V

    invoke-interface {v2, v5}, Lf/g/d/g/c;->a(Lf/g/d/g/b;)V

    iput-object v5, p0, Lf/g/j/e/m;->d:Lf/g/j/c/m;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/m;->d:Lf/g/j/c/m;

    return-object v0
.end method

.method public d()Lf/g/j/c/s;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/c/s<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/e/m;->e:Lf/g/j/c/s;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lf/g/j/e/m;->c()Lf/g/j/c/m;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->j:Lf/g/j/c/r;

    move-object v2, v1

    check-cast v2, Lf/g/j/c/w;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/g/j/c/b;

    invoke-direct {v2, v1}, Lf/g/j/c/b;-><init>(Lf/g/j/c/r;)V

    new-instance v1, Lf/g/j/c/s;

    invoke-direct {v1, v0, v2}, Lf/g/j/c/s;-><init>(Lf/g/j/c/t;Lf/g/j/c/u;)V

    iput-object v1, p0, Lf/g/j/e/m;->e:Lf/g/j/c/s;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/m;->e:Lf/g/j/c/s;

    return-object v0
.end method

.method public e()Lf/g/j/c/s;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/c/s<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/e/m;->g:Lf/g/j/c/s;

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lf/g/j/e/m;->f:Lf/g/j/c/m;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v0, Lf/g/j/e/k;->h:Lcom/facebook/common/internal/Supplier;

    iget-object v0, v0, Lf/g/j/e/k;->m:Lf/g/d/g/c;

    new-instance v2, Lf/g/j/c/p;

    invoke-direct {v2}, Lf/g/j/c/p;-><init>()V

    new-instance v3, Lf/g/j/c/v;

    invoke-direct {v3}, Lf/g/j/c/v;-><init>()V

    new-instance v4, Lf/g/j/c/m;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v3, v1, v5}, Lf/g/j/c/m;-><init>(Lf/g/j/c/y;Lf/g/j/c/t$a;Lcom/facebook/common/internal/Supplier;Lf/g/j/c/m$c;)V

    invoke-interface {v0, v4}, Lf/g/d/g/c;->a(Lf/g/d/g/b;)V

    iput-object v4, p0, Lf/g/j/e/m;->f:Lf/g/j/c/m;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/m;->f:Lf/g/j/c/m;

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->j:Lf/g/j/c/r;

    move-object v2, v1

    check-cast v2, Lf/g/j/c/w;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/g/j/c/q;

    invoke-direct {v2, v1}, Lf/g/j/c/q;-><init>(Lf/g/j/c/r;)V

    new-instance v1, Lf/g/j/c/s;

    invoke-direct {v1, v0, v2}, Lf/g/j/c/s;-><init>(Lf/g/j/c/t;Lf/g/j/c/u;)V

    iput-object v1, p0, Lf/g/j/e/m;->g:Lf/g/j/c/s;

    :cond_1
    iget-object v0, p0, Lf/g/j/e/m;->g:Lf/g/j/c/s;

    return-object v0
.end method

.method public f()Lf/g/j/c/g;
    .locals 8

    iget-object v0, p0, Lf/g/j/e/m;->h:Lf/g/j/c/g;

    if-nez v0, :cond_1

    new-instance v0, Lf/g/j/c/g;

    iget-object v1, p0, Lf/g/j/e/m;->i:Lf/g/b/b/h;

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v1, Lf/g/j/e/k;->l:Lcom/facebook/cache/disk/DiskCacheConfig;

    iget-object v1, v1, Lf/g/j/e/k;->g:Lf/g/j/e/g;

    check-cast v1, Lf/g/j/e/d;

    invoke-virtual {v1, v2}, Lf/g/j/e/d;->a(Lcom/facebook/cache/disk/DiskCacheConfig;)Lf/g/b/b/h;

    move-result-object v1

    iput-object v1, p0, Lf/g/j/e/m;->i:Lf/g/b/b/h;

    :cond_0
    iget-object v2, p0, Lf/g/j/e/m;->i:Lf/g/b/b/h;

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v3, v1, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lf/g/j/m/x;->d(I)Lf/g/d/g/g;

    move-result-object v3

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    invoke-virtual {v1}, Lf/g/j/m/x;->e()Lf/g/d/g/j;

    move-result-object v4

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    invoke-interface {v1}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    invoke-interface {v1}, Lf/g/j/e/f;->d()Ljava/util/concurrent/Executor;

    move-result-object v6

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v7, v1, Lf/g/j/e/k;->j:Lf/g/j/c/r;

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lf/g/j/c/g;-><init>(Lf/g/b/b/h;Lf/g/d/g/g;Lf/g/d/g/j;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lf/g/j/c/r;)V

    iput-object v0, p0, Lf/g/j/e/m;->h:Lf/g/j/c/g;

    :cond_1
    iget-object v0, p0, Lf/g/j/e/m;->h:Lf/g/j/c/g;

    return-object v0
.end method

.method public g()Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;
    .locals 3

    iget-object v0, p0, Lf/g/j/e/m;->q:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v0, v0, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    invoke-virtual {p0}, Lf/g/j/e/m;->h()Lf/g/j/o/d;

    iget-object v1, p0, Lf/g/j/e/m;->c:Lf/g/j/e/b;

    new-instance v2, Lf/g/j/b/a;

    invoke-virtual {v0}, Lf/g/j/m/x;->a()Lf/g/j/m/d;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lf/g/j/b/a;-><init>(Lf/g/j/m/d;Lf/g/j/e/b;)V

    iput-object v2, p0, Lf/g/j/e/m;->q:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/m;->q:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    return-object v0
.end method

.method public h()Lf/g/j/o/d;
    .locals 4

    iget-object v0, p0, Lf/g/j/e/m;->r:Lf/g/j/o/d;

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v0, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    iget-object v0, v0, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v0, v2, :cond_0

    invoke-virtual {v1}, Lf/g/j/m/x;->b()I

    move-result v0

    new-instance v2, Lf/g/j/o/c;

    invoke-virtual {v1}, Lf/g/j/m/x;->a()Lf/g/j/m/d;

    move-result-object v1

    new-instance v3, Landroidx/core/util/Pools$SynchronizedPool;

    invoke-direct {v3, v0}, Landroidx/core/util/Pools$SynchronizedPool;-><init>(I)V

    invoke-direct {v2, v1, v0, v3}, Lf/g/j/o/c;-><init>(Lf/g/j/m/d;ILandroidx/core/util/Pools$SynchronizedPool;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lf/g/j/m/x;->b()I

    move-result v0

    new-instance v2, Lf/g/j/o/a;

    invoke-virtual {v1}, Lf/g/j/m/x;->a()Lf/g/j/m/d;

    move-result-object v1

    new-instance v3, Landroidx/core/util/Pools$SynchronizedPool;

    invoke-direct {v3, v0}, Landroidx/core/util/Pools$SynchronizedPool;-><init>(I)V

    invoke-direct {v2, v1, v0, v3}, Lf/g/j/o/a;-><init>(Lf/g/j/m/d;ILandroidx/core/util/Pools$SynchronizedPool;)V

    :goto_0
    iput-object v2, p0, Lf/g/j/e/m;->r:Lf/g/j/o/d;

    :cond_1
    iget-object v0, p0, Lf/g/j/e/m;->r:Lf/g/j/o/d;

    return-object v0
.end method

.method public final i()Lf/g/j/c/g;
    .locals 8

    iget-object v0, p0, Lf/g/j/e/m;->o:Lf/g/j/c/g;

    if-nez v0, :cond_1

    new-instance v0, Lf/g/j/c/g;

    iget-object v1, p0, Lf/g/j/e/m;->p:Lf/g/b/b/h;

    if-nez v1, :cond_0

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v2, v1, Lf/g/j/e/k;->u:Lcom/facebook/cache/disk/DiskCacheConfig;

    iget-object v1, v1, Lf/g/j/e/k;->g:Lf/g/j/e/g;

    check-cast v1, Lf/g/j/e/d;

    invoke-virtual {v1, v2}, Lf/g/j/e/d;->a(Lcom/facebook/cache/disk/DiskCacheConfig;)Lf/g/b/b/h;

    move-result-object v1

    iput-object v1, p0, Lf/g/j/e/m;->p:Lf/g/b/b/h;

    :cond_0
    iget-object v2, p0, Lf/g/j/e/m;->p:Lf/g/b/b/h;

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v3, v1, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lf/g/j/m/x;->d(I)Lf/g/d/g/g;

    move-result-object v3

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    invoke-virtual {v1}, Lf/g/j/m/x;->e()Lf/g/d/g/j;

    move-result-object v4

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    invoke-interface {v1}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v1, v1, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    invoke-interface {v1}, Lf/g/j/e/f;->d()Ljava/util/concurrent/Executor;

    move-result-object v6

    iget-object v1, p0, Lf/g/j/e/m;->b:Lf/g/j/e/k;

    iget-object v7, v1, Lf/g/j/e/k;->j:Lf/g/j/c/r;

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lf/g/j/c/g;-><init>(Lf/g/b/b/h;Lf/g/d/g/g;Lf/g/d/g/j;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lf/g/j/c/r;)V

    iput-object v0, p0, Lf/g/j/e/m;->o:Lf/g/j/c/g;

    :cond_1
    iget-object v0, p0, Lf/g/j/e/m;->o:Lf/g/j/c/g;

    return-object v0
.end method
