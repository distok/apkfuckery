.class public Lf/g/j/e/i;
.super Ljava/lang/Object;
.source "ImagePipeline.java"


# instance fields
.field public final a:Lf/g/j/e/q;

.field public final b:Lf/g/j/l/e;

.field public final c:Lf/g/j/l/d;

.field public final d:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lf/g/j/c/j;

.field public final h:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/concurrent/atomic/AtomicLong;

.field public final j:Lf/g/c/a;

.field public final k:Lf/g/j/e/k;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Prefetching is not enabled"

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lf/g/j/e/q;Ljava/util/Set;Ljava/util/Set;Lcom/facebook/common/internal/Supplier;Lf/g/j/c/t;Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/e1;Lcom/facebook/common/internal/Supplier;Lcom/facebook/common/internal/Supplier;Lf/g/c/a;Lf/g/j/e/k;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/e/q;",
            "Ljava/util/Set<",
            "Lf/g/j/l/e;",
            ">;",
            "Ljava/util/Set<",
            "Lf/g/j/l/d;",
            ">;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lf/g/j/q/e1;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lf/g/c/a;",
            "Lf/g/j/e/k;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p7, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p7}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p7, p0, Lf/g/j/e/i;->i:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object p1, p0, Lf/g/j/e/i;->a:Lf/g/j/e/q;

    new-instance p1, Lf/g/j/l/c;

    invoke-direct {p1, p2}, Lf/g/j/l/c;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lf/g/j/e/i;->b:Lf/g/j/l/e;

    new-instance p1, Lf/g/j/l/b;

    invoke-direct {p1, p3}, Lf/g/j/l/b;-><init>(Ljava/util/Set;)V

    iput-object p1, p0, Lf/g/j/e/i;->c:Lf/g/j/l/d;

    iput-object p4, p0, Lf/g/j/e/i;->d:Lcom/facebook/common/internal/Supplier;

    iput-object p5, p0, Lf/g/j/e/i;->e:Lf/g/j/c/t;

    iput-object p6, p0, Lf/g/j/e/i;->f:Lf/g/j/c/t;

    iput-object p9, p0, Lf/g/j/e/i;->g:Lf/g/j/c/j;

    iput-object p11, p0, Lf/g/j/e/i;->h:Lcom/facebook/common/internal/Supplier;

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/j/e/i;->j:Lf/g/c/a;

    iput-object p14, p0, Lf/g/j/e/i;->k:Lf/g/j/e/k;

    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Lcom/facebook/imagepipeline/request/ImageRequest$c;Lf/g/j/l/e;Ljava/lang/String;)Lcom/facebook/datasource/DataSource;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/request/ImageRequest;",
            "Ljava/lang/Object;",
            "Lcom/facebook/imagepipeline/request/ImageRequest$c;",
            "Lf/g/j/l/e;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/datasource/DataSource<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/g/j/e/i;->a:Lf/g/j/e/q;

    invoke-virtual {v0, p1}, Lf/g/j/e/q;->c(Lcom/facebook/imagepipeline/request/ImageRequest;)Lf/g/j/q/u0;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    move-object v6, p4

    move-object v7, p5

    invoke-virtual/range {v1 .. v7}, Lf/g/j/e/i;->b(Lf/g/j/q/u0;Lcom/facebook/imagepipeline/request/ImageRequest;Lcom/facebook/imagepipeline/request/ImageRequest$c;Ljava/lang/Object;Lf/g/j/l/e;Ljava/lang/String;)Lcom/facebook/datasource/DataSource;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Ls/a/b/b/a;->G(Ljava/lang/Throwable;)Lcom/facebook/datasource/DataSource;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lf/g/j/q/u0;Lcom/facebook/imagepipeline/request/ImageRequest;Lcom/facebook/imagepipeline/request/ImageRequest$c;Ljava/lang/Object;Lf/g/j/l/e;Ljava/lang/String;)Lcom/facebook/datasource/DataSource;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "TT;>;>;",
            "Lcom/facebook/imagepipeline/request/ImageRequest;",
            "Lcom/facebook/imagepipeline/request/ImageRequest$c;",
            "Ljava/lang/Object;",
            "Lf/g/j/l/e;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/datasource/DataSource<",
            "Lcom/facebook/common/references/CloseableReference<",
            "TT;>;>;"
        }
    .end annotation

    move-object v1, p0

    move-object/from16 v0, p2

    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance v13, Lf/g/j/q/a0;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    if-nez p5, :cond_1

    iget-object v5, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->q:Lf/g/j/l/e;

    if-nez v5, :cond_0

    iget-object v4, v1, Lf/g/j/e/i;->b:Lf/g/j/l/e;

    goto :goto_1

    :cond_0
    new-instance v6, Lf/g/j/l/c;

    new-array v4, v4, [Lf/g/j/l/e;

    iget-object v7, v1, Lf/g/j/e/i;->b:Lf/g/j/l/e;

    aput-object v7, v4, v2

    aput-object v5, v4, v3

    invoke-direct {v6, v4}, Lf/g/j/l/c;-><init>([Lf/g/j/l/e;)V

    goto :goto_0

    :cond_1
    iget-object v5, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->q:Lf/g/j/l/e;

    if-nez v5, :cond_2

    new-instance v5, Lf/g/j/l/c;

    new-array v4, v4, [Lf/g/j/l/e;

    iget-object v6, v1, Lf/g/j/e/i;->b:Lf/g/j/l/e;

    aput-object v6, v4, v2

    aput-object p5, v4, v3

    invoke-direct {v5, v4}, Lf/g/j/l/c;-><init>([Lf/g/j/l/e;)V

    move-object v4, v5

    goto :goto_1

    :cond_2
    new-instance v6, Lf/g/j/l/c;

    const/4 v7, 0x3

    new-array v7, v7, [Lf/g/j/l/e;

    iget-object v8, v1, Lf/g/j/e/i;->b:Lf/g/j/l/e;

    aput-object v8, v7, v2

    aput-object p5, v7, v3

    aput-object v5, v7, v4

    invoke-direct {v6, v7}, Lf/g/j/l/c;-><init>([Lf/g/j/l/e;)V

    :goto_0
    move-object v4, v6

    :goto_1
    iget-object v5, v1, Lf/g/j/e/i;->c:Lf/g/j/l/d;

    invoke-direct {v13, v4, v5}, Lf/g/j/q/a0;-><init>(Lf/g/j/l/e;Lf/g/j/l/d;)V

    iget-object v4, v1, Lf/g/j/e/i;->j:Lf/g/c/a;

    move-object/from16 v7, p4

    if-eqz v4, :cond_3

    invoke-interface {v4, v7, v2}, Lf/g/c/a;->a(Ljava/lang/Object;Z)V

    :cond_3
    :try_start_0
    iget-object v4, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-object/from16 v5, p3

    invoke-static {v4, v5}, Lcom/facebook/imagepipeline/request/ImageRequest$c;->f(Lcom/facebook/imagepipeline/request/ImageRequest$c;Lcom/facebook/imagepipeline/request/ImageRequest$c;)Lcom/facebook/imagepipeline/request/ImageRequest$c;

    move-result-object v8

    new-instance v14, Lf/g/j/q/b1;

    iget-object v4, v1, Lf/g/j/e/i;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x0

    iget-boolean v5, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->e:Z

    if-nez v5, :cond_5

    iget-object v5, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-static {v5}, Lf/g/d/l/b;->e(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_4

    goto :goto_2

    :cond_4
    const/4 v10, 0x0

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v10, 0x1

    :goto_3
    iget-object v11, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->k:Lf/g/j/d/d;

    iget-object v12, v1, Lf/g/j/e/i;->k:Lf/g/j/e/k;

    move-object v2, v14

    move-object/from16 v3, p2

    move-object/from16 v5, p6

    move-object v6, v13

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v12}, Lf/g/j/q/b1;-><init>(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/String;Ljava/lang/String;Lf/g/j/q/x0;Ljava/lang/Object;Lcom/facebook/imagepipeline/request/ImageRequest$c;ZZLf/g/j/d/d;Lf/g/j/e/k;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance v0, Lf/g/j/f/d;

    move-object/from16 v2, p1

    invoke-direct {v0, v2, v14, v13}, Lf/g/j/f/d;-><init>(Lf/g/j/q/u0;Lf/g/j/q/b1;Lf/g/j/l/d;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_4

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Ls/a/b/b/a;->G(Ljava/lang/Throwable;)Lcom/facebook/datasource/DataSource;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0

    :goto_4
    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw v0
.end method
