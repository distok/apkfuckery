.class public Lf/g/j/e/q;
.super Ljava/lang/Object;
.source "ProducerSequenceFactory.java"


# instance fields
.field public final a:Landroid/content/ContentResolver;

.field public final b:Lf/g/j/e/p;

.field public final c:Lf/g/j/q/m0;

.field public final d:Z

.field public final e:Z

.field public final f:Lf/g/j/q/e1;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Lf/g/j/t/c;

.field public final k:Z

.field public final l:Z

.field public m:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public n:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public r:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public s:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public t:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public u:Lf/g/j/q/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Lf/g/j/e/p;Lf/g/j/q/m0;ZZLf/g/j/q/e1;ZZZZLf/g/j/t/c;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/e/q;->a:Landroid/content/ContentResolver;

    iput-object p2, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    iput-object p3, p0, Lf/g/j/e/q;->c:Lf/g/j/q/m0;

    iput-boolean p4, p0, Lf/g/j/e/q;->d:Z

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lf/g/j/e/q;->v:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lf/g/j/e/q;->w:Ljava/util/Map;

    iput-object p6, p0, Lf/g/j/e/q;->f:Lf/g/j/q/e1;

    iput-boolean p7, p0, Lf/g/j/e/q;->g:Z

    iput-boolean p8, p0, Lf/g/j/e/q;->h:Z

    iput-boolean p9, p0, Lf/g/j/e/q;->e:Z

    iput-boolean p10, p0, Lf/g/j/e/q;->i:Z

    iput-object p11, p0, Lf/g/j/e/q;->j:Lf/g/j/t/c;

    iput-boolean p12, p0, Lf/g/j/e/q;->k:Z

    iput-boolean p13, p0, Lf/g/j/e/q;->l:Z

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lf/g/j/q/u0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/e/q;->n:Lf/g/j/q/u0;

    if-nez v0, :cond_1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    iget-object v1, p0, Lf/g/j/e/q;->c:Lf/g/j/q/m0;

    new-instance v2, Lf/g/j/q/l0;

    iget-object v3, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v0, v0, Lf/g/j/e/p;->d:Lf/g/d/g/a;

    invoke-direct {v2, v3, v0, v1}, Lf/g/j/q/l0;-><init>(Lf/g/d/g/g;Lf/g/d/g/a;Lf/g/j/q/m0;)V

    invoke-virtual {p0, v2}, Lf/g/j/e/q;->m(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    new-instance v1, Lf/g/j/q/a;

    invoke-direct {v1, v0}, Lf/g/j/q/a;-><init>(Lf/g/j/q/u0;)V

    iput-object v1, p0, Lf/g/j/e/q;->n:Lf/g/j/q/u0;

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    iget-boolean v2, p0, Lf/g/j/e/q;->d:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lf/g/j/e/q;->g:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lf/g/j/e/q;->j:Lf/g/j/t/c;

    invoke-virtual {v0, v1, v2, v3}, Lf/g/j/e/p;->a(Lf/g/j/q/u0;ZLf/g/j/t/c;)Lf/g/j/q/a1;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->n:Lf/g/j/q/u0;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    :cond_1
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/e/q;->n:Lf/g/j/q/u0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lf/g/j/q/u0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/e/q;->t:Lf/g/j/q/u0;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/m;

    iget-object v0, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    invoke-direct {v1, v0}, Lf/g/j/q/m;-><init>(Lf/g/d/g/g;)V

    sget-object v0, Lf/g/d/m/c;->a:Lf/g/d/m/b;

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v2, Lf/g/j/q/a;

    invoke-direct {v2, v1}, Lf/g/j/q/a;-><init>(Lf/g/j/q/u0;)V

    const/4 v1, 0x1

    iget-object v3, p0, Lf/g/j/e/q;->j:Lf/g/j/t/c;

    invoke-virtual {v0, v2, v1, v3}, Lf/g/j/e/p;->a(Lf/g/j/q/u0;ZLf/g/j/t/c;)Lf/g/j/q/a1;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/g/j/e/q;->j(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->t:Lf/g/j/q/u0;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/q;->t:Lf/g/j/q/u0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(Lcom/facebook/imagepipeline/request/ImageRequest;)Lf/g/j/q/u0;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/request/ImageRequest;",
            ")",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lf/g/j/s/b;->b()Z

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    const-string v1, "Uri is null."

    invoke-static {v0, v1}, Ls/a/b/b/a;->i(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->c:I

    if-eqz v1, :cond_4

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto :goto_1

    :pswitch_0
    invoke-virtual {p0}, Lf/g/j/e/q;->h()Lf/g/j/q/u0;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lf/g/j/e/q;->b()Lf/g/j/q/u0;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lf/g/j/e/q;->f()Lf/g/j/q/u0;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lf/g/j/e/q;->d()Lf/g/j/q/u0;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lf/g/j/e/q;->a:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lf/g/d/f/a;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    const-string v1, "video/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lf/g/j/e/q;->g()Lf/g/j/q/u0;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/g/j/e/q;->e()Lf/g/j/q/u0;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    :try_start_1
    iget-object v0, p0, Lf/g/j/e/q;->o:Lf/g/j/q/u0;

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/g0;

    iget-object v2, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v2}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v0, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    invoke-direct {v1, v2, v0}, Lf/g/j/q/g0;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;)V

    invoke-virtual {p0, v1}, Lf/g/j/e/q;->k(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->o:Lf/g/j/q/u0;

    :cond_2
    iget-object v0, p0, Lf/g/j/e/q;->o:Lf/g/j/q/u0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :pswitch_6
    invoke-virtual {p0}, Lf/g/j/e/q;->g()Lf/g/j/q/u0;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    goto :goto_2

    :goto_1
    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported uri scheme! Uri is: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x1e

    if-le v3, v4, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "..."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    :try_start_4
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/e/q;->m:Lf/g/j/q/u0;

    if-nez v0, :cond_5

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-virtual {p0}, Lf/g/j/e/q;->a()Lf/g/j/q/u0;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/g/j/e/q;->j(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->m:Lf/g/j/q/u0;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    :cond_5
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/e/q;->m:Lf/g/j/q/u0;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    goto :goto_0

    :goto_2
    iget-object p1, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    if-eqz p1, :cond_7

    monitor-enter p0

    :try_start_6
    iget-object p1, p0, Lf/g/j/e/q;->v:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/r0;

    iget-object v2, p1, Lf/g/j/e/p;->s:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iget-object p1, p1, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {p1}, Lf/g/j/e/f;->c()Ljava/util/concurrent/Executor;

    move-result-object p1

    invoke-direct {v1, v0, v2, p1}, Lf/g/j/q/r0;-><init>(Lf/g/j/q/u0;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Ljava/util/concurrent/Executor;)V

    iget-object p1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v2, Lf/g/j/q/q0;

    iget-object v3, p1, Lf/g/j/e/p;->o:Lf/g/j/c/t;

    iget-object p1, p1, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    invoke-direct {v2, v3, p1, v1}, Lf/g/j/q/q0;-><init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    iget-object p1, p0, Lf/g/j/e/q;->v:Ljava/util/Map;

    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    iget-object p1, p0, Lf/g/j/e/q;->v:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lf/g/j/q/u0;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    monitor-exit p0

    goto :goto_3

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_7
    :goto_3
    iget-boolean p1, p0, Lf/g/j/e/q;->h:Z

    if-eqz p1, :cond_9

    monitor-enter p0

    :try_start_7
    iget-object p1, p0, Lf/g/j/e/q;->w:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/g/j/q/u0;

    if-nez p1, :cond_8

    iget-object p1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/i;

    iget v2, p1, Lf/g/j/e/p;->t:I

    iget v3, p1, Lf/g/j/e/p;->u:I

    iget-boolean p1, p1, Lf/g/j/e/p;->v:Z

    invoke-direct {v1, v0, v2, v3, p1}, Lf/g/j/q/i;-><init>(Lf/g/j/q/u0;IIZ)V

    iget-object p1, p0, Lf/g/j/e/q;->w:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object v0, v1

    goto :goto_4

    :cond_8
    move-object v0, p1

    :goto_4
    monitor-exit p0

    goto :goto_5

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_9
    :goto_5
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0

    :catchall_3
    move-exception p1

    :try_start_8
    monitor-exit p0

    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :catchall_4
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized d()Lf/g/j/q/u0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/e/q;->s:Lf/g/j/q/u0;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/c0;

    iget-object v2, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v2}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v0, v0, Lf/g/j/e/p;->c:Landroid/content/res/AssetManager;

    invoke-direct {v1, v2, v3, v0}, Lf/g/j/q/c0;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Landroid/content/res/AssetManager;)V

    invoke-virtual {p0, v1}, Lf/g/j/e/q;->k(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->s:Lf/g/j/q/u0;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/q;->s:Lf/g/j/q/u0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lf/g/j/q/u0;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/e/q;->q:Lf/g/j/q/u0;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/d0;

    iget-object v2, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v2}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v0, v0, Lf/g/j/e/p;->a:Landroid/content/ContentResolver;

    invoke-direct {v1, v2, v3, v0}, Lf/g/j/q/d0;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Landroid/content/ContentResolver;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lf/g/j/q/i1;

    const/4 v2, 0x0

    iget-object v3, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lf/g/j/q/e0;

    iget-object v5, v3, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v5}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v6, v3, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v3, v3, Lf/g/j/e/p;->a:Landroid/content/ContentResolver;

    invoke-direct {v4, v5, v6, v3}, Lf/g/j/q/e0;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Landroid/content/ContentResolver;)V

    aput-object v4, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v4, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;

    iget-object v5, v3, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v5}, Lf/g/j/e/f;->f()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v6, v3, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v3, v3, Lf/g/j/e/p;->a:Landroid/content/ContentResolver;

    invoke-direct {v4, v5, v6, v3}, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Landroid/content/ContentResolver;)V

    aput-object v4, v0, v2

    invoke-virtual {p0, v1, v0}, Lf/g/j/e/q;->l(Lf/g/j/q/u0;[Lf/g/j/q/i1;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->q:Lf/g/j/q/u0;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/q;->q:Lf/g/j/q/u0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Lf/g/j/q/u0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/e/q;->r:Lf/g/j/q/u0;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/h0;

    iget-object v2, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v2}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v0, v0, Lf/g/j/e/p;->b:Landroid/content/res/Resources;

    invoke-direct {v1, v2, v3, v0}, Lf/g/j/q/h0;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Landroid/content/res/Resources;)V

    invoke-virtual {p0, v1}, Lf/g/j/e/q;->k(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->r:Lf/g/j/q/u0;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/q;->r:Lf/g/j/q/u0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Lf/g/j/q/u0;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/e/q;->p:Lf/g/j/q/u0;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/i0;

    iget-object v2, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v2}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v0, v0, Lf/g/j/e/p;->a:Landroid/content/ContentResolver;

    invoke-direct {v1, v2, v0}, Lf/g/j/q/i0;-><init>(Ljava/util/concurrent/Executor;Landroid/content/ContentResolver;)V

    invoke-virtual {p0, v1}, Lf/g/j/e/q;->i(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->p:Lf/g/j/q/u0;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/q;->p:Lf/g/j/q/u0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Lf/g/j/q/u0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/e/q;->u:Lf/g/j/q/u0;

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/z0;

    iget-object v2, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v2}, Lf/g/j/e/f;->e()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v0, v0, Lf/g/j/e/p;->a:Landroid/content/ContentResolver;

    invoke-direct {v1, v2, v3, v0}, Lf/g/j/q/z0;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Landroid/content/ContentResolver;)V

    invoke-virtual {p0, v1}, Lf/g/j/e/q;->k(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/q;->u:Lf/g/j/q/u0;

    :cond_0
    iget-object v0, p0, Lf/g/j/e/q;->u:Lf/g/j/q/u0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final i(Lf/g/j/q/u0;)Lf/g/j/q/u0;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;)",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/h;

    iget-object v2, v0, Lf/g/j/e/p;->o:Lf/g/j/c/t;

    iget-object v0, v0, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    invoke-direct {v1, v2, v0, p1}, Lf/g/j/q/h;-><init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    new-instance p1, Lf/g/j/q/g;

    invoke-direct {p1, v0, v1}, Lf/g/j/q/g;-><init>(Lf/g/j/c/j;Lf/g/j/q/u0;)V

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    iget-object v1, p0, Lf/g/j/e/q;->f:Lf/g/j/q/e1;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/g/j/q/d1;

    invoke-direct {v0, p1, v1}, Lf/g/j/q/d1;-><init>(Lf/g/j/q/u0;Lf/g/j/q/e1;)V

    iget-boolean p1, p0, Lf/g/j/e/q;->k:Z

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lf/g/j/e/q;->l:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/f;

    iget-object v2, p1, Lf/g/j/e/p;->o:Lf/g/j/c/t;

    iget-object p1, p1, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    invoke-direct {v1, v2, p1, v0}, Lf/g/j/q/f;-><init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    return-object v1

    :cond_1
    :goto_0
    iget-object p1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v8, Lf/g/j/q/f;

    iget-object v1, p1, Lf/g/j/e/p;->o:Lf/g/j/c/t;

    iget-object v5, p1, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    invoke-direct {v8, v1, v5, v0}, Lf/g/j/q/f;-><init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    new-instance v0, Lf/g/j/q/j;

    iget-object v2, p1, Lf/g/j/e/p;->n:Lf/g/j/c/t;

    iget-object v3, p1, Lf/g/j/e/p;->l:Lf/g/j/c/g;

    iget-object v4, p1, Lf/g/j/e/p;->m:Lf/g/j/c/g;

    iget-object v6, p1, Lf/g/j/e/p;->q:Lf/g/j/c/e;

    iget-object v7, p1, Lf/g/j/e/p;->r:Lf/g/j/c/e;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lf/g/j/q/j;-><init>(Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/c/e;Lf/g/j/c/e;Lf/g/j/q/u0;)V

    return-object v0
.end method

.method public final j(Lf/g/j/q/u0;)Lf/g/j/q/u0;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;)",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v1, v0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v15, Lf/g/j/q/n;

    iget-object v3, v1, Lf/g/j/e/p;->d:Lf/g/d/g/a;

    iget-object v2, v1, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v2}, Lf/g/j/e/f;->a()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, v1, Lf/g/j/e/p;->e:Lf/g/j/h/b;

    iget-object v6, v1, Lf/g/j/e/p;->f:Lf/g/j/h/c;

    iget-boolean v7, v1, Lf/g/j/e/p;->g:Z

    iget-boolean v8, v1, Lf/g/j/e/p;->h:Z

    iget-boolean v9, v1, Lf/g/j/e/p;->i:Z

    iget v11, v1, Lf/g/j/e/p;->x:I

    iget-object v12, v1, Lf/g/j/e/p;->w:Lf/g/j/e/b;

    sget-object v14, Lf/g/d/d/l;->a:Lcom/facebook/common/internal/Supplier;

    const/4 v13, 0x0

    move-object v2, v15

    move-object/from16 v10, p1

    invoke-direct/range {v2 .. v14}, Lf/g/j/q/n;-><init>(Lf/g/d/g/a;Ljava/util/concurrent/Executor;Lf/g/j/h/b;Lf/g/j/h/c;ZZZLf/g/j/q/u0;ILf/g/j/e/b;Ljava/lang/Runnable;Lcom/facebook/common/internal/Supplier;)V

    invoke-virtual {v0, v15}, Lf/g/j/e/q;->i(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object v1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v1
.end method

.method public final k(Lf/g/j/q/u0;)Lf/g/j/q/u0;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;)",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lf/g/j/q/i1;

    iget-object v1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v2, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;

    iget-object v3, v1, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v3}, Lf/g/j/e/f;->f()Ljava/util/concurrent/Executor;

    move-result-object v3

    iget-object v4, v1, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v1, v1, Lf/g/j/e/p;->a:Landroid/content/ContentResolver;

    invoke-direct {v2, v3, v4, v1}, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Landroid/content/ContentResolver;)V

    const/4 v1, 0x0

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0}, Lf/g/j/e/q;->l(Lf/g/j/q/u0;[Lf/g/j/q/i1;)Lf/g/j/q/u0;

    move-result-object p1

    return-object p1
.end method

.method public final l(Lf/g/j/q/u0;[Lf/g/j/q/i1;)Lf/g/j/q/u0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;[",
            "Lf/g/j/q/i1<",
            "Lf/g/j/j/e;",
            ">;)",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lf/g/j/e/q;->m(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object p1

    new-instance v0, Lf/g/j/q/a;

    invoke-direct {v0, p1}, Lf/g/j/q/a;-><init>(Lf/g/j/q/u0;)V

    iget-object p1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    iget-object v1, p0, Lf/g/j/e/q;->j:Lf/g/j/t/c;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2, v1}, Lf/g/j/e/p;->a(Lf/g/j/q/u0;ZLf/g/j/t/c;)Lf/g/j/q/a1;

    move-result-object p1

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/g1;

    iget-object v0, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v0}, Lf/g/j/e/f;->b()Ljava/util/concurrent/Executor;

    move-result-object v0

    const/4 v3, 0x5

    invoke-direct {v1, v3, v0, p1}, Lf/g/j/q/g1;-><init>(ILjava/util/concurrent/Executor;Lf/g/j/q/u0;)V

    iget-object p1, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lf/g/j/q/h1;

    invoke-direct {p1, p2}, Lf/g/j/q/h1;-><init>([Lf/g/j/q/i1;)V

    iget-object p2, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    iget-object v0, p0, Lf/g/j/e/q;->j:Lf/g/j/t/c;

    invoke-virtual {p2, p1, v2, v0}, Lf/g/j/e/p;->a(Lf/g/j/q/u0;ZLf/g/j/t/c;)Lf/g/j/q/a1;

    move-result-object p1

    new-instance p2, Lf/g/j/q/k;

    invoke-direct {p2, p1, v1}, Lf/g/j/q/k;-><init>(Lf/g/j/q/u0;Lf/g/j/q/u0;)V

    invoke-virtual {p0, p2}, Lf/g/j/e/q;->j(Lf/g/j/q/u0;)Lf/g/j/q/u0;

    move-result-object p1

    return-object p1
.end method

.method public final m(Lf/g/j/q/u0;)Lf/g/j/q/u0;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;)",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/g/d/m/c;->a:Lf/g/d/m/b;

    iget-boolean v0, p0, Lf/g/j/e/q;->i:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-boolean v0, p0, Lf/g/j/e/q;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v7, Lf/g/j/q/p0;

    iget-object v8, v0, Lf/g/j/e/p;->l:Lf/g/j/c/g;

    iget-object v9, v0, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    iget-object v4, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    iget-object v5, v0, Lf/g/j/e/p;->d:Lf/g/d/g/a;

    move-object v1, v7

    move-object v2, v8

    move-object v3, v9

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lf/g/j/q/p0;-><init>(Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/d/g/g;Lf/g/d/g/a;Lf/g/j/q/u0;)V

    new-instance p1, Lf/g/j/q/s;

    iget-object v0, v0, Lf/g/j/e/p;->m:Lf/g/j/c/g;

    invoke-direct {p1, v8, v0, v9, v7}, Lf/g/j/q/s;-><init>(Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/s;

    iget-object v2, v0, Lf/g/j/e/p;->l:Lf/g/j/c/g;

    iget-object v3, v0, Lf/g/j/e/p;->m:Lf/g/j/c/g;

    iget-object v0, v0, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    invoke-direct {v1, v2, v3, v0, p1}, Lf/g/j/q/s;-><init>(Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    move-object p1, v1

    :goto_0
    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v1, Lf/g/j/q/r;

    iget-object v2, v0, Lf/g/j/e/p;->l:Lf/g/j/c/g;

    iget-object v3, v0, Lf/g/j/e/p;->m:Lf/g/j/c/g;

    iget-object v0, v0, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    invoke-direct {v1, v2, v3, v0, p1}, Lf/g/j/q/r;-><init>(Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    move-object p1, v1

    :cond_1
    iget-object v0, p0, Lf/g/j/e/q;->b:Lf/g/j/e/p;

    new-instance v7, Lf/g/j/q/u;

    iget-object v1, v0, Lf/g/j/e/p;->n:Lf/g/j/c/t;

    iget-object v8, v0, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    invoke-direct {v7, v1, v8, p1}, Lf/g/j/q/u;-><init>(Lf/g/j/c/t;Lf/g/j/c/j;Lf/g/j/q/u0;)V

    iget-boolean p1, p0, Lf/g/j/e/q;->l:Z

    if-eqz p1, :cond_2

    new-instance p1, Lf/g/j/q/v;

    iget-object v2, v0, Lf/g/j/e/p;->l:Lf/g/j/c/g;

    iget-object v3, v0, Lf/g/j/e/p;->m:Lf/g/j/c/g;

    iget-object v5, v0, Lf/g/j/e/p;->q:Lf/g/j/c/e;

    iget-object v6, v0, Lf/g/j/e/p;->r:Lf/g/j/c/e;

    move-object v1, p1

    move-object v4, v8

    invoke-direct/range {v1 .. v7}, Lf/g/j/q/v;-><init>(Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lf/g/j/c/e;Lf/g/j/c/e;Lf/g/j/q/u0;)V

    new-instance v1, Lf/g/j/q/t;

    iget-boolean v0, v0, Lf/g/j/e/p;->y:Z

    invoke-direct {v1, v8, v0, p1}, Lf/g/j/q/t;-><init>(Lf/g/j/c/j;ZLf/g/j/q/u0;)V

    return-object v1

    :cond_2
    new-instance p1, Lf/g/j/q/t;

    iget-boolean v0, v0, Lf/g/j/e/p;->y:Z

    invoke-direct {p1, v8, v0, v7}, Lf/g/j/q/t;-><init>(Lf/g/j/c/j;ZLf/g/j/q/u0;)V

    return-object p1
.end method
