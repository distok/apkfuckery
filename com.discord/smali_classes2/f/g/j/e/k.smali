.class public Lf/g/j/e/k;
.super Ljava/lang/Object;
.source "ImagePipelineConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/e/k$a;,
        Lf/g/j/e/k$b;
    }
.end annotation


# static fields
.field public static y:Lf/g/j/e/k$b;


# instance fields
.field public final a:Landroid/graphics/Bitmap$Config;

.field public final b:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Lcom/facebook/imagepipeline/cache/MemoryCacheParams;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/g/j/c/t$a;

.field public final d:Lf/g/j/c/j;

.field public final e:Landroid/content/Context;

.field public final f:Z

.field public final g:Lf/g/j/e/g;

.field public final h:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Lcom/facebook/imagepipeline/cache/MemoryCacheParams;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lf/g/j/e/f;

.field public final j:Lf/g/j/c/r;

.field public final k:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lcom/facebook/cache/disk/DiskCacheConfig;

.field public final m:Lf/g/d/g/c;

.field public final n:Lf/g/j/q/m0;

.field public final o:I

.field public final p:Lf/g/j/m/x;

.field public final q:Lf/g/j/h/c;

.field public final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/g/j/l/e;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/g/j/l/d;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Z

.field public final u:Lcom/facebook/cache/disk/DiskCacheConfig;

.field public final v:Lf/g/j/e/l;

.field public final w:Z

.field public final x:Lf/g/j/g/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/g/j/e/k$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/g/j/e/k$b;-><init>(Lf/g/j/e/j;)V

    sput-object v0, Lf/g/j/e/k;->y:Lf/g/j/e/k$b;

    return-void
.end method

.method public constructor <init>(Lf/g/j/e/k$a;Lf/g/j/e/j;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object p2, p1, Lf/g/j/e/k$a;->f:Lf/g/j/e/l$b;

    new-instance v0, Lf/g/j/e/l;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lf/g/j/e/l;-><init>(Lf/g/j/e/l$b;Lf/g/j/e/l$a;)V

    iput-object v0, p0, Lf/g/j/e/k;->v:Lf/g/j/e/l;

    iget-object p2, p1, Lf/g/j/e/k$a;->a:Lcom/facebook/common/internal/Supplier;

    if-nez p2, :cond_0

    new-instance p2, Lcom/facebook/imagepipeline/cache/DefaultBitmapMemoryCacheParamsSupplier;

    iget-object v0, p1, Lf/g/j/e/k$a;->b:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-direct {p2, v0}, Lcom/facebook/imagepipeline/cache/DefaultBitmapMemoryCacheParamsSupplier;-><init>(Landroid/app/ActivityManager;)V

    :cond_0
    iput-object p2, p0, Lf/g/j/e/k;->b:Lcom/facebook/common/internal/Supplier;

    new-instance p2, Lf/g/j/c/d;

    invoke-direct {p2}, Lf/g/j/c/d;-><init>()V

    iput-object p2, p0, Lf/g/j/e/k;->c:Lf/g/j/c/t$a;

    sget-object p2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object p2, p0, Lf/g/j/e/k;->a:Landroid/graphics/Bitmap$Config;

    const-class p2, Lf/g/j/c/n;

    monitor-enter p2

    :try_start_0
    sget-object v0, Lf/g/j/c/n;->a:Lf/g/j/c/n;

    if-nez v0, :cond_1

    new-instance v0, Lf/g/j/c/n;

    invoke-direct {v0}, Lf/g/j/c/n;-><init>()V

    sput-object v0, Lf/g/j/c/n;->a:Lf/g/j/c/n;

    :cond_1
    sget-object v0, Lf/g/j/c/n;->a:Lf/g/j/c/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    monitor-exit p2

    iput-object v0, p0, Lf/g/j/e/k;->d:Lf/g/j/c/j;

    iget-object p2, p1, Lf/g/j/e/k$a;->b:Landroid/content/Context;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p2, p0, Lf/g/j/e/k;->e:Landroid/content/Context;

    new-instance p2, Lf/g/j/e/d;

    new-instance v0, Lf/g/j/e/e;

    invoke-direct {v0}, Lf/g/j/e/e;-><init>()V

    invoke-direct {p2, v0}, Lf/g/j/e/d;-><init>(Lf/g/j/e/e;)V

    iput-object p2, p0, Lf/g/j/e/k;->g:Lf/g/j/e/g;

    iget-boolean p2, p1, Lf/g/j/e/k$a;->c:Z

    iput-boolean p2, p0, Lf/g/j/e/k;->f:Z

    new-instance p2, Lf/g/j/c/o;

    invoke-direct {p2}, Lf/g/j/c/o;-><init>()V

    iput-object p2, p0, Lf/g/j/e/k;->h:Lcom/facebook/common/internal/Supplier;

    const-class p2, Lf/g/j/c/w;

    monitor-enter p2

    :try_start_1
    sget-object v0, Lf/g/j/c/w;->a:Lf/g/j/c/w;

    if-nez v0, :cond_2

    new-instance v0, Lf/g/j/c/w;

    invoke-direct {v0}, Lf/g/j/c/w;-><init>()V

    sput-object v0, Lf/g/j/c/w;->a:Lf/g/j/c/w;

    :cond_2
    sget-object v0, Lf/g/j/c/w;->a:Lf/g/j/c/w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p2

    iput-object v0, p0, Lf/g/j/e/k;->j:Lf/g/j/c/r;

    new-instance p2, Lf/g/j/e/j;

    invoke-direct {p2, p0}, Lf/g/j/e/j;-><init>(Lf/g/j/e/k;)V

    iput-object p2, p0, Lf/g/j/e/k;->k:Lcom/facebook/common/internal/Supplier;

    iget-object p2, p1, Lf/g/j/e/k$a;->d:Lcom/facebook/cache/disk/DiskCacheConfig;

    if-nez p2, :cond_3

    iget-object p2, p1, Lf/g/j/e/k$a;->b:Landroid/content/Context;

    :try_start_2
    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance v0, Lcom/facebook/cache/disk/DiskCacheConfig$b;

    invoke-direct {v0, p2, v1}, Lcom/facebook/cache/disk/DiskCacheConfig$b;-><init>(Landroid/content/Context;Lcom/facebook/cache/disk/DiskCacheConfig$a;)V

    new-instance p2, Lcom/facebook/cache/disk/DiskCacheConfig;

    invoke-direct {p2, v0}, Lcom/facebook/cache/disk/DiskCacheConfig;-><init>(Lcom/facebook/cache/disk/DiskCacheConfig$b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1

    :cond_3
    :goto_0
    iput-object p2, p0, Lf/g/j/e/k;->l:Lcom/facebook/cache/disk/DiskCacheConfig;

    invoke-static {}, Lf/g/d/g/d;->b()Lf/g/d/g/d;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/e/k;->m:Lf/g/d/g/c;

    const/16 v0, 0x7530

    iput v0, p0, Lf/g/j/e/k;->o:I

    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance v2, Lf/g/j/q/z;

    invoke-direct {v2, v0}, Lf/g/j/q/z;-><init>(I)V

    iput-object v2, p0, Lf/g/j/e/k;->n:Lf/g/j/q/m0;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance v0, Lf/g/j/m/x;

    new-instance v2, Lf/g/j/m/w$b;

    invoke-direct {v2, v1}, Lf/g/j/m/w$b;-><init>(Lf/g/j/m/w$a;)V

    new-instance v3, Lf/g/j/m/w;

    invoke-direct {v3, v2, v1}, Lf/g/j/m/w;-><init>(Lf/g/j/m/w$b;Lf/g/j/m/w$a;)V

    invoke-direct {v0, v3}, Lf/g/j/m/x;-><init>(Lf/g/j/m/w;)V

    iput-object v0, p0, Lf/g/j/e/k;->p:Lf/g/j/m/x;

    new-instance v1, Lf/g/j/h/e;

    invoke-direct {v1}, Lf/g/j/h/e;-><init>()V

    iput-object v1, p0, Lf/g/j/e/k;->q:Lf/g/j/h/c;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lf/g/j/e/k;->r:Ljava/util/Set;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lf/g/j/e/k;->s:Ljava/util/Set;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf/g/j/e/k;->t:Z

    iget-object v1, p1, Lf/g/j/e/k$a;->e:Lcom/facebook/cache/disk/DiskCacheConfig;

    if-nez v1, :cond_4

    goto :goto_1

    :cond_4
    move-object p2, v1

    :goto_1
    iput-object p2, p0, Lf/g/j/e/k;->u:Lcom/facebook/cache/disk/DiskCacheConfig;

    invoke-virtual {v0}, Lf/g/j/m/x;->b()I

    move-result p2

    new-instance v0, Lf/g/j/e/c;

    invoke-direct {v0, p2}, Lf/g/j/e/c;-><init>(I)V

    iput-object v0, p0, Lf/g/j/e/k;->i:Lf/g/j/e/f;

    iget-boolean p2, p1, Lf/g/j/e/k$a;->g:Z

    iput-boolean p2, p0, Lf/g/j/e/k;->w:Z

    iget-object p1, p1, Lf/g/j/e/k$a;->h:Lf/g/j/g/a;

    iput-object p1, p0, Lf/g/j/e/k;->x:Lf/g/j/g/a;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_1
    move-exception p1

    monitor-exit p2

    throw p1

    :catchall_2
    move-exception p1

    monitor-exit p2

    throw p1
.end method
