.class public Lf/g/j/e/l;
.super Ljava/lang/Object;
.source "ImagePipelineExperiments.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/e/l$c;,
        Lf/g/j/e/l$d;,
        Lf/g/j/e/l$b;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Lf/g/j/e/l$d;

.field public final c:Z

.field public final d:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public final f:Z

.field public final g:I


# direct methods
.method public constructor <init>(Lf/g/j/e/l$b;Lf/g/j/e/l$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 p2, 0x800

    iput p2, p0, Lf/g/j/e/l;->a:I

    new-instance p2, Lf/g/j/e/l$c;

    invoke-direct {p2}, Lf/g/j/e/l$c;-><init>()V

    iput-object p2, p0, Lf/g/j/e/l;->b:Lf/g/j/e/l$d;

    iget-boolean p2, p1, Lf/g/j/e/l$b;->b:Z

    iput-boolean p2, p0, Lf/g/j/e/l;->c:Z

    iget-object p1, p1, Lf/g/j/e/l$b;->c:Lcom/facebook/common/internal/Supplier;

    iput-object p1, p0, Lf/g/j/e/l;->d:Lcom/facebook/common/internal/Supplier;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/g/j/e/l;->e:Z

    iput-boolean p1, p0, Lf/g/j/e/l;->f:Z

    const/16 p1, 0x14

    iput p1, p0, Lf/g/j/e/l;->g:I

    return-void
.end method
