.class public Lf/g/j/e/p;
.super Ljava/lang/Object;
.source "ProducerFactory.java"


# instance fields
.field public a:Landroid/content/ContentResolver;

.field public b:Landroid/content/res/Resources;

.field public c:Landroid/content/res/AssetManager;

.field public final d:Lf/g/d/g/a;

.field public final e:Lf/g/j/h/b;

.field public final f:Lf/g/j/h/c;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Lf/g/j/e/f;

.field public final k:Lf/g/d/g/g;

.field public final l:Lf/g/j/c/g;

.field public final m:Lf/g/j/c/g;

.field public final n:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Lf/g/j/c/j;

.field public final q:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Lf/g/j/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/e<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field public final t:I

.field public final u:I

.field public v:Z

.field public final w:Lf/g/j/e/b;

.field public final x:I

.field public final y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/g/d/g/a;Lf/g/j/h/b;Lf/g/j/h/c;ZZZLf/g/j/e/f;Lf/g/d/g/g;Lf/g/j/c/t;Lf/g/j/c/t;Lf/g/j/c/g;Lf/g/j/c/g;Lf/g/j/c/j;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;IIZILf/g/j/e/b;ZI)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lf/g/d/g/a;",
            "Lf/g/j/h/b;",
            "Lf/g/j/h/c;",
            "ZZZ",
            "Lf/g/j/e/f;",
            "Lf/g/d/g/g;",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/g;",
            "Lf/g/j/c/j;",
            "Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;",
            "IIZI",
            "Lf/g/j/e/b;",
            "ZI)V"
        }
    .end annotation

    move-object v0, p0

    move/from16 v1, p22

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, v0, Lf/g/j/e/p;->a:Landroid/content/ContentResolver;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, v0, Lf/g/j/e/p;->b:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    iput-object v2, v0, Lf/g/j/e/p;->c:Landroid/content/res/AssetManager;

    move-object v2, p2

    iput-object v2, v0, Lf/g/j/e/p;->d:Lf/g/d/g/a;

    move-object v2, p3

    iput-object v2, v0, Lf/g/j/e/p;->e:Lf/g/j/h/b;

    move-object v2, p4

    iput-object v2, v0, Lf/g/j/e/p;->f:Lf/g/j/h/c;

    move v2, p5

    iput-boolean v2, v0, Lf/g/j/e/p;->g:Z

    move v2, p6

    iput-boolean v2, v0, Lf/g/j/e/p;->h:Z

    move v2, p7

    iput-boolean v2, v0, Lf/g/j/e/p;->i:Z

    move-object v2, p8

    iput-object v2, v0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    move-object v2, p9

    iput-object v2, v0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    move-object v2, p10

    iput-object v2, v0, Lf/g/j/e/p;->o:Lf/g/j/c/t;

    move-object v2, p11

    iput-object v2, v0, Lf/g/j/e/p;->n:Lf/g/j/c/t;

    move-object v2, p12

    iput-object v2, v0, Lf/g/j/e/p;->l:Lf/g/j/c/g;

    move-object/from16 v2, p13

    iput-object v2, v0, Lf/g/j/e/p;->m:Lf/g/j/c/g;

    move-object/from16 v2, p14

    iput-object v2, v0, Lf/g/j/e/p;->p:Lf/g/j/c/j;

    move-object/from16 v2, p15

    iput-object v2, v0, Lf/g/j/e/p;->s:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    new-instance v2, Lf/g/j/c/e;

    invoke-direct {v2, v1}, Lf/g/j/c/e;-><init>(I)V

    iput-object v2, v0, Lf/g/j/e/p;->q:Lf/g/j/c/e;

    new-instance v2, Lf/g/j/c/e;

    invoke-direct {v2, v1}, Lf/g/j/c/e;-><init>(I)V

    iput-object v2, v0, Lf/g/j/e/p;->r:Lf/g/j/c/e;

    move/from16 v1, p16

    iput v1, v0, Lf/g/j/e/p;->t:I

    move/from16 v1, p17

    iput v1, v0, Lf/g/j/e/p;->u:I

    move/from16 v1, p18

    iput-boolean v1, v0, Lf/g/j/e/p;->v:Z

    move/from16 v1, p19

    iput v1, v0, Lf/g/j/e/p;->x:I

    move-object/from16 v1, p20

    iput-object v1, v0, Lf/g/j/e/p;->w:Lf/g/j/e/b;

    move/from16 v1, p21

    iput-boolean v1, v0, Lf/g/j/e/p;->y:Z

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/q/u0;ZLf/g/j/t/c;)Lf/g/j/q/a1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lf/g/j/j/e;",
            ">;Z",
            "Lf/g/j/t/c;",
            ")",
            "Lf/g/j/q/a1;"
        }
    .end annotation

    new-instance v6, Lf/g/j/q/a1;

    iget-object v0, p0, Lf/g/j/e/p;->j:Lf/g/j/e/f;

    invoke-interface {v0}, Lf/g/j/e/f;->c()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Lf/g/j/e/p;->k:Lf/g/d/g/g;

    move-object v0, v6

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lf/g/j/q/a1;-><init>(Ljava/util/concurrent/Executor;Lf/g/d/g/g;Lf/g/j/q/u0;ZLf/g/j/t/c;)V

    return-object v6
.end method
