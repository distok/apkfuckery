.class public Lf/g/j/c/m;
.super Ljava/lang/Object;
.source "CountingMemoryCache.java"

# interfaces
.implements Lf/g/j/c/t;
.implements Lf/g/d/g/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/c/m$b;,
        Lf/g/j/c/m$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/g/j/c/t<",
        "TK;TV;>;",
        "Lf/g/d/g/b;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/m$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/m$c<",
            "TK;>;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/c/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/k<",
            "TK;",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final c:Lf/g/j/c/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/k<",
            "TK;",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final d:Lf/g/j/c/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/y<",
            "TV;>;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Lcom/facebook/imagepipeline/cache/MemoryCacheParams;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

.field public g:J


# direct methods
.method public constructor <init>(Lf/g/j/c/y;Lf/g/j/c/t$a;Lcom/facebook/common/internal/Supplier;Lf/g/j/c/m$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/y<",
            "TV;>;",
            "Lf/g/j/c/t$a;",
            "Lcom/facebook/common/internal/Supplier<",
            "Lcom/facebook/imagepipeline/cache/MemoryCacheParams;",
            ">;",
            "Lf/g/j/c/m$c<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p2, Ljava/util/WeakHashMap;

    invoke-direct {p2}, Ljava/util/WeakHashMap;-><init>()V

    iput-object p1, p0, Lf/g/j/c/m;->d:Lf/g/j/c/y;

    new-instance p2, Lf/g/j/c/k;

    new-instance v0, Lf/g/j/c/l;

    invoke-direct {v0, p0, p1}, Lf/g/j/c/l;-><init>(Lf/g/j/c/m;Lf/g/j/c/y;)V

    invoke-direct {p2, v0}, Lf/g/j/c/k;-><init>(Lf/g/j/c/y;)V

    iput-object p2, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    new-instance p2, Lf/g/j/c/k;

    new-instance v0, Lf/g/j/c/l;

    invoke-direct {v0, p0, p1}, Lf/g/j/c/l;-><init>(Lf/g/j/c/m;Lf/g/j/c/y;)V

    invoke-direct {p2, v0}, Lf/g/j/c/k;-><init>(Lf/g/j/c/y;)V

    iput-object p2, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    iput-object p3, p0, Lf/g/j/c/m;->e:Lcom/facebook/common/internal/Supplier;

    invoke-interface {p3}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iput-object p1, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lf/g/j/c/m;->g:J

    iput-object p4, p0, Lf/g/j/c/m;->a:Lf/g/j/c/m$c;

    return-void
.end method

.method public static j(Lf/g/j/c/m$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;)V"
        }
    .end annotation

    if-eqz p0, :cond_0

    iget-object v0, p0, Lf/g/j/c/m$b;->e:Lf/g/j/c/m$c;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lf/g/j/c/m$b;->a:Ljava/lang/Object;

    const/4 v1, 0x0

    check-cast v0, Lf/g/j/a/c/c$a;

    invoke-virtual {v0, p0, v1}, Lf/g/j/a/c/c$a;->a(Ljava/lang/Object;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/c/m;->a:Lf/g/j/c/m$c;

    invoke-virtual {p0, p1, p2, v0}, Lf/g/j/c/m;->c(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;Lf/g/j/c/m$c;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    return-object p1
.end method

.method public b(Lf/g/d/d/j;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/d/d/j<",
            "TK;>;)I"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v0, p1}, Lf/g/j/c/k;->f(Lf/g/d/d/j;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    invoke-virtual {v1, p1}, Lf/g/j/c/k;->f(Lf/g/d/d/j;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/g/j/c/m;->g(Ljava/util/ArrayList;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, p1}, Lf/g/j/c/m;->h(Ljava/util/ArrayList;)V

    invoke-virtual {p0, v0}, Lf/g/j/c/m;->k(Ljava/util/ArrayList;)V

    invoke-virtual {p0}, Lf/g/j/c/m;->l()V

    invoke-virtual {p0}, Lf/g/j/c/m;->i()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    return p1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public c(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;Lf/g/j/c/m$c;)Lcom/facebook/common/references/CloseableReference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;",
            "Lf/g/j/c/m$c<",
            "TK;>;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lf/g/j/c/m;->l()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v0, p1}, Lf/g/j/c/k;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/c/m$b;

    iget-object v1, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    invoke-virtual {v1, p1}, Lf/g/j/c/k;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/g/j/c/m$b;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lf/g/j/c/m;->f(Lf/g/j/c/m$b;)V

    invoke-virtual {p0, v1}, Lf/g/j/c/m;->n(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v3

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v4, p0, Lf/g/j/c/m;->d:Lf/g/j/c/y;

    invoke-interface {v4, v3}, Lf/g/j/c/y;->a(Ljava/lang/Object;)I

    move-result v3

    iget-object v4, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iget v4, v4, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->e:I

    const/4 v5, 0x1

    if-gt v3, v4, :cond_1

    invoke-virtual {p0}, Lf/g/j/c/m;->d()I

    move-result v4

    iget-object v6, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iget v6, v6, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->b:I

    sub-int/2addr v6, v5

    if-gt v4, v6, :cond_1

    invoke-virtual {p0}, Lf/g/j/c/m;->e()I

    move-result v4

    iget-object v6, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iget v6, v6, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-int/2addr v6, v3

    if-gt v4, v6, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    :try_start_2
    monitor-exit p0

    if-eqz v5, :cond_2

    new-instance v2, Lf/g/j/c/m$b;

    invoke-direct {v2, p1, p2, p3}, Lf/g/j/c/m$b;-><init>(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;Lf/g/j/c/m$c;)V

    iget-object p2, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    invoke-virtual {p2, p1, v2}, Lf/g/j/c/k;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v2}, Lf/g/j/c/m;->m(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v2

    :cond_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_3
    invoke-static {v0}, Lf/g/j/c/m;->j(Lf/g/j/c/m$b;)V

    invoke-virtual {p0}, Lf/g/j/c/m;->i()V

    return-object v2

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit p0

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public declared-synchronized d()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    invoke-virtual {v0}, Lf/g/j/c/k;->a()I

    move-result v0

    iget-object v1, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v1}, Lf/g/j/c/k;->a()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    invoke-virtual {v0}, Lf/g/j/c/k;->b()I

    move-result v0

    iget-object v1, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v1}, Lf/g/j/c/k;->b()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f(Lf/g/j/c/m$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p1, Lf/g/j/c/m$b;->d:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ls/a/b/b/a;->j(Z)V

    iput-boolean v1, p1, Lf/g/j/c/m$b;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized g(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;>;)V"
        }
    .end annotation

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/c/m$b;

    invoke-virtual {p0, v0}, Lf/g/j/c/m;->f(Lf/g/j/c/m$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v1, p1}, Lf/g/j/c/k;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/g/j/c/m$b;

    iget-object v2, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, v2, Lf/g/j/c/k;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v2

    check-cast p1, Lf/g/j/c/m$b;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lf/g/j/c/m;->m(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v1}, Lf/g/j/c/m;->j(Lf/g/j/c/m$b;)V

    invoke-virtual {p0}, Lf/g/j/c/m;->l()V

    invoke-virtual {p0}, Lf/g/j/c/m;->i()V

    return-object v0

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit v2

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public final h(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/c/m$b;

    invoke-virtual {p0, v0}, Lf/g/j/c/m;->n(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->i(Lcom/facebook/common/references/CloseableReference;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iget v1, v0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->d:I

    iget v0, v0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->b:I

    invoke-virtual {p0}, Lf/g/j/c/m;->d()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iget v2, v1, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->c:I

    iget v1, v1, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->a:I

    invoke-virtual {p0}, Lf/g/j/c/m;->e()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lf/g/j/c/m;->o(II)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/g/j/c/m;->g(Ljava/util/ArrayList;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Lf/g/j/c/m;->h(Ljava/util/ArrayList;)V

    invoke-virtual {p0, v0}, Lf/g/j/c/m;->k(Ljava/util/ArrayList;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final k(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/c/m$b;

    invoke-static {v0}, Lf/g/j/c/m;->j(Lf/g/j/c/m$b;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final declared-synchronized l()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lf/g/j/c/m;->g:J

    iget-object v2, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iget-wide v2, v2, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->f:J

    add-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lf/g/j/c/m;->g:J

    iget-object v0, p0, Lf/g/j/c/m;->e:Lcom/facebook/common/internal/Supplier;

    invoke-interface {v0}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;

    iput-object v0, p0, Lf/g/j/c/m;->f:Lcom/facebook/imagepipeline/cache/MemoryCacheParams;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v0, p1, Lf/g/j/c/m$b;->d:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ls/a/b/b/a;->j(Z)V

    iget v0, p1, Lf/g/j/c/m$b;->c:I

    add-int/2addr v0, v1

    iput v0, p1, Lf/g/j/c/m$b;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p0

    iget-object v0, p1, Lf/g/j/c/m$b;->b:Lcom/facebook/common/references/CloseableReference;

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lf/g/j/c/m$a;

    invoke-direct {v1, p0, p1}, Lf/g/j/c/m$a;-><init>(Lf/g/j/c/m;Lf/g/j/c/m$b;)V

    invoke-static {v0, v1}, Lcom/facebook/common/references/CloseableReference;->B(Ljava/lang/Object;Lf/g/d/h/f;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit p0

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized n(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p1, Lf/g/j/c/m$b;->d:Z

    if-eqz v0, :cond_0

    iget v0, p1, Lf/g/j/c/m$b;->c:I

    if-nez v0, :cond_0

    iget-object p1, p1, Lf/g/j/c/m$b;->b:Lcom/facebook/common/references/CloseableReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized o(II)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList<",
            "Lf/g/j/c/m$b<",
            "TK;TV;>;>;"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    iget-object v0, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v0}, Lf/g/j/c/k;->a()I

    move-result v0

    const/4 v1, 0x0

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v0}, Lf/g/j/c/k;->b()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-gt v0, p2, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object v2, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v2}, Lf/g/j/c/k;->a()I

    move-result v2

    if-gt v2, p1, :cond_2

    iget-object v2, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v2}, Lf/g/j/c/k;->b()I

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-le v2, p2, :cond_1

    goto :goto_1

    :cond_1
    monitor-exit p0

    return-object v0

    :cond_2
    :goto_1
    :try_start_2
    iget-object v2, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    monitor-enter v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v3, v2, Lf/g/j/c/k;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v3, v1

    goto :goto_2

    :cond_3
    iget-object v3, v2, Lf/g/j/c/k;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    :try_start_4
    monitor-exit v2

    iget-object v2, p0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v2, v3}, Lf/g/j/c/k;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    invoke-virtual {v2, v3}, Lf/g/j/c/k;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method
