.class public Lf/g/j/c/n;
.super Ljava/lang/Object;
.source "DefaultCacheKeyFactory.java"

# interfaces
.implements Lf/g/j/c/j;


# static fields
.field public static a:Lf/g/j/c/n;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;
    .locals 9

    new-instance v8, Lf/g/j/c/c;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    iget-object v3, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-object v4, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v8

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lf/g/j/c/c;-><init>(Ljava/lang/String;Lf/g/j/d/e;Lf/g/j/d/f;Lf/g/j/d/b;Lcom/facebook/cache/common/CacheKey;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v8
.end method

.method public b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;
    .locals 0

    iget-object p1, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    new-instance p2, Lf/g/b/a/f;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lf/g/b/a/f;-><init>(Ljava/lang/String;)V

    return-object p2
.end method

.method public c(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;
    .locals 10

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/g/j/r/b;->getPostprocessorCacheKey()Lcom/facebook/cache/common/CacheKey;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object v7, v1

    move-object v8, v7

    :goto_0
    new-instance v0, Lf/g/j/c/c;

    iget-object v1, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    iget-object v5, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-object v6, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    move-object v2, v0

    move-object v9, p2

    invoke-direct/range {v2 .. v9}, Lf/g/j/c/c;-><init>(Ljava/lang/String;Lf/g/j/d/e;Lf/g/j/d/f;Lf/g/j/d/b;Lcom/facebook/cache/common/CacheKey;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method
