.class public Lf/g/j/c/m$a;
.super Ljava/lang/Object;
.source "CountingMemoryCache.java"

# interfaces
.implements Lf/g/d/h/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/c/m;->m(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/d/h/f<",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/c/m$b;

.field public final synthetic b:Lf/g/j/c/m;


# direct methods
.method public constructor <init>(Lf/g/j/c/m;Lf/g/j/c/m$b;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/c/m$a;->b:Lf/g/j/c/m;

    iput-object p2, p0, Lf/g/j/c/m$a;->a:Lf/g/j/c/m$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public release(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    iget-object p1, p0, Lf/g/j/c/m$a;->b:Lf/g/j/c/m;

    iget-object v0, p0, Lf/g/j/c/m$a;->a:Lf/g/j/c/m$b;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    iget v1, v0, Lf/g/j/c/m$b;->c:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ls/a/b/b/a;->j(Z)V

    iget v1, v0, Lf/g/j/c/m$b;->c:I

    sub-int/2addr v1, v3

    iput v1, v0, Lf/g/j/c/m$b;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit p1

    monitor-enter p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-boolean v1, v0, Lf/g/j/c/m$b;->d:Z

    if-nez v1, :cond_1

    iget v1, v0, Lf/g/j/c/m$b;->c:I

    if-nez v1, :cond_1

    iget-object v1, p1, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    iget-object v2, v0, Lf/g/j/c/m$b;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Lf/g/j/c/k;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit p1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    monitor-exit p1

    :goto_1
    invoke-virtual {p1, v0}, Lf/g/j/c/m;->n(Lf/g/j/c/m$b;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    invoke-static {v1}, Lcom/facebook/common/references/CloseableReference;->i(Lcom/facebook/common/references/CloseableReference;)V

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_3

    iget-object v1, v0, Lf/g/j/c/m$b;->e:Lf/g/j/c/m$c;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lf/g/j/c/m$b;->a:Ljava/lang/Object;

    check-cast v1, Lf/g/j/a/c/c$a;

    invoke-virtual {v1, v0, v3}, Lf/g/j/a/c/c$a;->a(Ljava/lang/Object;Z)V

    :cond_3
    invoke-virtual {p1}, Lf/g/j/c/m;->l()V

    invoke-virtual {p1}, Lf/g/j/c/m;->i()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p1

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method
